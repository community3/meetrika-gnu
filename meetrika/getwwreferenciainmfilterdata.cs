/*
               File: GetWWReferenciaINMFilterData
        Description: Get WWReferencia INMFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:12:0.71
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwreferenciainmfilterdata : GXProcedure
   {
      public getwwreferenciainmfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwreferenciainmfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV15DDOName = aP0_DDOName;
         this.AV13SearchTxt = aP1_SearchTxt;
         this.AV14SearchTxtTo = aP2_SearchTxtTo;
         this.AV19OptionsJson = "" ;
         this.AV22OptionsDescJson = "" ;
         this.AV24OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV19OptionsJson;
         aP4_OptionsDescJson=this.AV22OptionsDescJson;
         aP5_OptionIndexesJson=this.AV24OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV15DDOName = aP0_DDOName;
         this.AV13SearchTxt = aP1_SearchTxt;
         this.AV14SearchTxtTo = aP2_SearchTxtTo;
         this.AV19OptionsJson = "" ;
         this.AV22OptionsDescJson = "" ;
         this.AV24OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV19OptionsJson;
         aP4_OptionsDescJson=this.AV22OptionsDescJson;
         aP5_OptionIndexesJson=this.AV24OptionIndexesJson;
         return AV24OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwreferenciainmfilterdata objgetwwreferenciainmfilterdata;
         objgetwwreferenciainmfilterdata = new getwwreferenciainmfilterdata();
         objgetwwreferenciainmfilterdata.AV15DDOName = aP0_DDOName;
         objgetwwreferenciainmfilterdata.AV13SearchTxt = aP1_SearchTxt;
         objgetwwreferenciainmfilterdata.AV14SearchTxtTo = aP2_SearchTxtTo;
         objgetwwreferenciainmfilterdata.AV19OptionsJson = "" ;
         objgetwwreferenciainmfilterdata.AV22OptionsDescJson = "" ;
         objgetwwreferenciainmfilterdata.AV24OptionIndexesJson = "" ;
         objgetwwreferenciainmfilterdata.context.SetSubmitInitialConfig(context);
         objgetwwreferenciainmfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwreferenciainmfilterdata);
         aP3_OptionsJson=this.AV19OptionsJson;
         aP4_OptionsDescJson=this.AV22OptionsDescJson;
         aP5_OptionIndexesJson=this.AV24OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwreferenciainmfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV18Options = (IGxCollection)(new GxSimpleCollection());
         AV21OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV23OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV15DDOName), "DDO_REFERENCIAINM_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADREFERENCIAINM_DESCRICAOOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV19OptionsJson = AV18Options.ToJSonString(false);
         AV22OptionsDescJson = AV21OptionsDesc.ToJSonString(false);
         AV24OptionIndexesJson = AV23OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV26Session.Get("WWReferenciaINMGridState"), "") == 0 )
         {
            AV28GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWReferenciaINMGridState"), "");
         }
         else
         {
            AV28GridState.FromXml(AV26Session.Get("WWReferenciaINMGridState"), "");
         }
         AV42GXV1 = 1;
         while ( AV42GXV1 <= AV28GridState.gxTpr_Filtervalues.Count )
         {
            AV29GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV28GridState.gxTpr_Filtervalues.Item(AV42GXV1));
            if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "REFERENCIAINM_AREATRABALHOCOD") == 0 )
            {
               AV31ReferenciaINM_AreaTrabalhoCod = (int)(NumberUtil.Val( AV29GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "TFREFERENCIAINM_DESCRICAO") == 0 )
            {
               AV10TFReferenciaINM_Descricao = AV29GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "TFREFERENCIAINM_DESCRICAO_SEL") == 0 )
            {
               AV11TFReferenciaINM_Descricao_Sel = AV29GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "TFREFERENCIAINM_ATIVO_SEL") == 0 )
            {
               AV12TFReferenciaINM_Ativo_Sel = (short)(NumberUtil.Val( AV29GridStateFilterValue.gxTpr_Value, "."));
            }
            AV42GXV1 = (int)(AV42GXV1+1);
         }
         if ( AV28GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV30GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV28GridState.gxTpr_Dynamicfilters.Item(1));
            AV32DynamicFiltersSelector1 = AV30GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "REFERENCIAINM_DESCRICAO") == 0 )
            {
               AV33ReferenciaINM_Descricao1 = AV30GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV28GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV34DynamicFiltersEnabled2 = true;
               AV30GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV28GridState.gxTpr_Dynamicfilters.Item(2));
               AV35DynamicFiltersSelector2 = AV30GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV35DynamicFiltersSelector2, "REFERENCIAINM_DESCRICAO") == 0 )
               {
                  AV36ReferenciaINM_Descricao2 = AV30GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV28GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV37DynamicFiltersEnabled3 = true;
                  AV30GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV28GridState.gxTpr_Dynamicfilters.Item(3));
                  AV38DynamicFiltersSelector3 = AV30GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV38DynamicFiltersSelector3, "REFERENCIAINM_DESCRICAO") == 0 )
                  {
                     AV39ReferenciaINM_Descricao3 = AV30GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADREFERENCIAINM_DESCRICAOOPTIONS' Routine */
         AV10TFReferenciaINM_Descricao = AV13SearchTxt;
         AV11TFReferenciaINM_Descricao_Sel = "";
         AV44WWReferenciaINMDS_1_Referenciainm_areatrabalhocod = AV31ReferenciaINM_AreaTrabalhoCod;
         AV45WWReferenciaINMDS_2_Dynamicfiltersselector1 = AV32DynamicFiltersSelector1;
         AV46WWReferenciaINMDS_3_Referenciainm_descricao1 = AV33ReferenciaINM_Descricao1;
         AV47WWReferenciaINMDS_4_Dynamicfiltersenabled2 = AV34DynamicFiltersEnabled2;
         AV48WWReferenciaINMDS_5_Dynamicfiltersselector2 = AV35DynamicFiltersSelector2;
         AV49WWReferenciaINMDS_6_Referenciainm_descricao2 = AV36ReferenciaINM_Descricao2;
         AV50WWReferenciaINMDS_7_Dynamicfiltersenabled3 = AV37DynamicFiltersEnabled3;
         AV51WWReferenciaINMDS_8_Dynamicfiltersselector3 = AV38DynamicFiltersSelector3;
         AV52WWReferenciaINMDS_9_Referenciainm_descricao3 = AV39ReferenciaINM_Descricao3;
         AV53WWReferenciaINMDS_10_Tfreferenciainm_descricao = AV10TFReferenciaINM_Descricao;
         AV54WWReferenciaINMDS_11_Tfreferenciainm_descricao_sel = AV11TFReferenciaINM_Descricao_Sel;
         AV55WWReferenciaINMDS_12_Tfreferenciainm_ativo_sel = AV12TFReferenciaINM_Ativo_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV45WWReferenciaINMDS_2_Dynamicfiltersselector1 ,
                                              AV46WWReferenciaINMDS_3_Referenciainm_descricao1 ,
                                              AV47WWReferenciaINMDS_4_Dynamicfiltersenabled2 ,
                                              AV48WWReferenciaINMDS_5_Dynamicfiltersselector2 ,
                                              AV49WWReferenciaINMDS_6_Referenciainm_descricao2 ,
                                              AV50WWReferenciaINMDS_7_Dynamicfiltersenabled3 ,
                                              AV51WWReferenciaINMDS_8_Dynamicfiltersselector3 ,
                                              AV52WWReferenciaINMDS_9_Referenciainm_descricao3 ,
                                              AV54WWReferenciaINMDS_11_Tfreferenciainm_descricao_sel ,
                                              AV53WWReferenciaINMDS_10_Tfreferenciainm_descricao ,
                                              AV55WWReferenciaINMDS_12_Tfreferenciainm_ativo_sel ,
                                              A710ReferenciaINM_Descricao ,
                                              A711ReferenciaINM_Ativo ,
                                              A712ReferenciaINM_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV46WWReferenciaINMDS_3_Referenciainm_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV46WWReferenciaINMDS_3_Referenciainm_descricao1), "%", "");
         lV49WWReferenciaINMDS_6_Referenciainm_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV49WWReferenciaINMDS_6_Referenciainm_descricao2), "%", "");
         lV52WWReferenciaINMDS_9_Referenciainm_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV52WWReferenciaINMDS_9_Referenciainm_descricao3), "%", "");
         lV53WWReferenciaINMDS_10_Tfreferenciainm_descricao = StringUtil.Concat( StringUtil.RTrim( AV53WWReferenciaINMDS_10_Tfreferenciainm_descricao), "%", "");
         /* Using cursor P00SA2 */
         pr_default.execute(0, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, lV46WWReferenciaINMDS_3_Referenciainm_descricao1, lV49WWReferenciaINMDS_6_Referenciainm_descricao2, lV52WWReferenciaINMDS_9_Referenciainm_descricao3, lV53WWReferenciaINMDS_10_Tfreferenciainm_descricao, AV54WWReferenciaINMDS_11_Tfreferenciainm_descricao_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKSA2 = false;
            A712ReferenciaINM_AreaTrabalhoCod = P00SA2_A712ReferenciaINM_AreaTrabalhoCod[0];
            A710ReferenciaINM_Descricao = P00SA2_A710ReferenciaINM_Descricao[0];
            A711ReferenciaINM_Ativo = P00SA2_A711ReferenciaINM_Ativo[0];
            A709ReferenciaINM_Codigo = P00SA2_A709ReferenciaINM_Codigo[0];
            AV25count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00SA2_A710ReferenciaINM_Descricao[0], A710ReferenciaINM_Descricao) == 0 ) )
            {
               BRKSA2 = false;
               A709ReferenciaINM_Codigo = P00SA2_A709ReferenciaINM_Codigo[0];
               AV25count = (long)(AV25count+1);
               BRKSA2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A710ReferenciaINM_Descricao)) )
            {
               AV17Option = A710ReferenciaINM_Descricao;
               AV18Options.Add(AV17Option, 0);
               AV23OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV25count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV18Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKSA2 )
            {
               BRKSA2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV18Options = new GxSimpleCollection();
         AV21OptionsDesc = new GxSimpleCollection();
         AV23OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV26Session = context.GetSession();
         AV28GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV29GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFReferenciaINM_Descricao = "";
         AV11TFReferenciaINM_Descricao_Sel = "";
         AV30GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV32DynamicFiltersSelector1 = "";
         AV33ReferenciaINM_Descricao1 = "";
         AV35DynamicFiltersSelector2 = "";
         AV36ReferenciaINM_Descricao2 = "";
         AV38DynamicFiltersSelector3 = "";
         AV39ReferenciaINM_Descricao3 = "";
         AV45WWReferenciaINMDS_2_Dynamicfiltersselector1 = "";
         AV46WWReferenciaINMDS_3_Referenciainm_descricao1 = "";
         AV48WWReferenciaINMDS_5_Dynamicfiltersselector2 = "";
         AV49WWReferenciaINMDS_6_Referenciainm_descricao2 = "";
         AV51WWReferenciaINMDS_8_Dynamicfiltersselector3 = "";
         AV52WWReferenciaINMDS_9_Referenciainm_descricao3 = "";
         AV53WWReferenciaINMDS_10_Tfreferenciainm_descricao = "";
         AV54WWReferenciaINMDS_11_Tfreferenciainm_descricao_sel = "";
         scmdbuf = "";
         lV46WWReferenciaINMDS_3_Referenciainm_descricao1 = "";
         lV49WWReferenciaINMDS_6_Referenciainm_descricao2 = "";
         lV52WWReferenciaINMDS_9_Referenciainm_descricao3 = "";
         lV53WWReferenciaINMDS_10_Tfreferenciainm_descricao = "";
         A710ReferenciaINM_Descricao = "";
         P00SA2_A712ReferenciaINM_AreaTrabalhoCod = new int[1] ;
         P00SA2_A710ReferenciaINM_Descricao = new String[] {""} ;
         P00SA2_A711ReferenciaINM_Ativo = new bool[] {false} ;
         P00SA2_A709ReferenciaINM_Codigo = new int[1] ;
         AV17Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwreferenciainmfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00SA2_A712ReferenciaINM_AreaTrabalhoCod, P00SA2_A710ReferenciaINM_Descricao, P00SA2_A711ReferenciaINM_Ativo, P00SA2_A709ReferenciaINM_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV12TFReferenciaINM_Ativo_Sel ;
      private short AV55WWReferenciaINMDS_12_Tfreferenciainm_ativo_sel ;
      private int AV42GXV1 ;
      private int AV31ReferenciaINM_AreaTrabalhoCod ;
      private int AV44WWReferenciaINMDS_1_Referenciainm_areatrabalhocod ;
      private int AV9WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A712ReferenciaINM_AreaTrabalhoCod ;
      private int A709ReferenciaINM_Codigo ;
      private long AV25count ;
      private String scmdbuf ;
      private bool returnInSub ;
      private bool AV34DynamicFiltersEnabled2 ;
      private bool AV37DynamicFiltersEnabled3 ;
      private bool AV47WWReferenciaINMDS_4_Dynamicfiltersenabled2 ;
      private bool AV50WWReferenciaINMDS_7_Dynamicfiltersenabled3 ;
      private bool A711ReferenciaINM_Ativo ;
      private bool BRKSA2 ;
      private String AV24OptionIndexesJson ;
      private String AV19OptionsJson ;
      private String AV22OptionsDescJson ;
      private String AV15DDOName ;
      private String AV13SearchTxt ;
      private String AV14SearchTxtTo ;
      private String AV10TFReferenciaINM_Descricao ;
      private String AV11TFReferenciaINM_Descricao_Sel ;
      private String AV32DynamicFiltersSelector1 ;
      private String AV33ReferenciaINM_Descricao1 ;
      private String AV35DynamicFiltersSelector2 ;
      private String AV36ReferenciaINM_Descricao2 ;
      private String AV38DynamicFiltersSelector3 ;
      private String AV39ReferenciaINM_Descricao3 ;
      private String AV45WWReferenciaINMDS_2_Dynamicfiltersselector1 ;
      private String AV46WWReferenciaINMDS_3_Referenciainm_descricao1 ;
      private String AV48WWReferenciaINMDS_5_Dynamicfiltersselector2 ;
      private String AV49WWReferenciaINMDS_6_Referenciainm_descricao2 ;
      private String AV51WWReferenciaINMDS_8_Dynamicfiltersselector3 ;
      private String AV52WWReferenciaINMDS_9_Referenciainm_descricao3 ;
      private String AV53WWReferenciaINMDS_10_Tfreferenciainm_descricao ;
      private String AV54WWReferenciaINMDS_11_Tfreferenciainm_descricao_sel ;
      private String lV46WWReferenciaINMDS_3_Referenciainm_descricao1 ;
      private String lV49WWReferenciaINMDS_6_Referenciainm_descricao2 ;
      private String lV52WWReferenciaINMDS_9_Referenciainm_descricao3 ;
      private String lV53WWReferenciaINMDS_10_Tfreferenciainm_descricao ;
      private String A710ReferenciaINM_Descricao ;
      private String AV17Option ;
      private IGxSession AV26Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00SA2_A712ReferenciaINM_AreaTrabalhoCod ;
      private String[] P00SA2_A710ReferenciaINM_Descricao ;
      private bool[] P00SA2_A711ReferenciaINM_Ativo ;
      private int[] P00SA2_A709ReferenciaINM_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV18Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV28GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV29GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV30GridStateDynamicFilter ;
   }

   public class getwwreferenciainmfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00SA2( IGxContext context ,
                                             String AV45WWReferenciaINMDS_2_Dynamicfiltersselector1 ,
                                             String AV46WWReferenciaINMDS_3_Referenciainm_descricao1 ,
                                             bool AV47WWReferenciaINMDS_4_Dynamicfiltersenabled2 ,
                                             String AV48WWReferenciaINMDS_5_Dynamicfiltersselector2 ,
                                             String AV49WWReferenciaINMDS_6_Referenciainm_descricao2 ,
                                             bool AV50WWReferenciaINMDS_7_Dynamicfiltersenabled3 ,
                                             String AV51WWReferenciaINMDS_8_Dynamicfiltersselector3 ,
                                             String AV52WWReferenciaINMDS_9_Referenciainm_descricao3 ,
                                             String AV54WWReferenciaINMDS_11_Tfreferenciainm_descricao_sel ,
                                             String AV53WWReferenciaINMDS_10_Tfreferenciainm_descricao ,
                                             short AV55WWReferenciaINMDS_12_Tfreferenciainm_ativo_sel ,
                                             String A710ReferenciaINM_Descricao ,
                                             bool A711ReferenciaINM_Ativo ,
                                             int A712ReferenciaINM_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [6] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [ReferenciaINM_AreaTrabalhoCod], [ReferenciaINM_Descricao], [ReferenciaINM_Ativo], [ReferenciaINM_Codigo] FROM [ReferenciaINM] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([ReferenciaINM_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV45WWReferenciaINMDS_2_Dynamicfiltersselector1, "REFERENCIAINM_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46WWReferenciaINMDS_3_Referenciainm_descricao1)) ) )
         {
            sWhereString = sWhereString + " and ([ReferenciaINM_Descricao] like '%' + @lV46WWReferenciaINMDS_3_Referenciainm_descricao1 + '%')";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV47WWReferenciaINMDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV48WWReferenciaINMDS_5_Dynamicfiltersselector2, "REFERENCIAINM_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWReferenciaINMDS_6_Referenciainm_descricao2)) ) )
         {
            sWhereString = sWhereString + " and ([ReferenciaINM_Descricao] like '%' + @lV49WWReferenciaINMDS_6_Referenciainm_descricao2 + '%')";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV50WWReferenciaINMDS_7_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV51WWReferenciaINMDS_8_Dynamicfiltersselector3, "REFERENCIAINM_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWReferenciaINMDS_9_Referenciainm_descricao3)) ) )
         {
            sWhereString = sWhereString + " and ([ReferenciaINM_Descricao] like '%' + @lV52WWReferenciaINMDS_9_Referenciainm_descricao3 + '%')";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV54WWReferenciaINMDS_11_Tfreferenciainm_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWReferenciaINMDS_10_Tfreferenciainm_descricao)) ) )
         {
            sWhereString = sWhereString + " and ([ReferenciaINM_Descricao] like @lV53WWReferenciaINMDS_10_Tfreferenciainm_descricao)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWReferenciaINMDS_11_Tfreferenciainm_descricao_sel)) )
         {
            sWhereString = sWhereString + " and ([ReferenciaINM_Descricao] = @AV54WWReferenciaINMDS_11_Tfreferenciainm_descricao_sel)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV55WWReferenciaINMDS_12_Tfreferenciainm_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and ([ReferenciaINM_Ativo] = 1)";
         }
         if ( AV55WWReferenciaINMDS_12_Tfreferenciainm_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and ([ReferenciaINM_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [ReferenciaINM_Descricao]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00SA2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (short)dynConstraints[10] , (String)dynConstraints[11] , (bool)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00SA2 ;
          prmP00SA2 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV46WWReferenciaINMDS_3_Referenciainm_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV49WWReferenciaINMDS_6_Referenciainm_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV52WWReferenciaINMDS_9_Referenciainm_descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV53WWReferenciaINMDS_10_Tfreferenciainm_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV54WWReferenciaINMDS_11_Tfreferenciainm_descricao_sel",SqlDbType.VarChar,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00SA2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00SA2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwreferenciainmfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwreferenciainmfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwreferenciainmfilterdata") )
          {
             return  ;
          }
          getwwreferenciainmfilterdata worker = new getwwreferenciainmfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
