/*
               File: PRC_PreencheChkLst
        Description: Preenche Check Lst
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:59.93
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_preenchechklst : GXProcedure
   {
      public prc_preenchechklst( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_preenchechklst( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( short aP0_CheckList_De ,
                           String aP1_Cumpre )
      {
         this.A1230CheckList_De = aP0_CheckList_De;
         this.AV12Cumpre = aP1_Cumpre;
         initialize();
         executePrivate();
      }

      public void executeSubmit( short aP0_CheckList_De ,
                                 String aP1_Cumpre )
      {
         prc_preenchechklst objprc_preenchechklst;
         objprc_preenchechklst = new prc_preenchechklst();
         objprc_preenchechklst.A1230CheckList_De = aP0_CheckList_De;
         objprc_preenchechklst.AV12Cumpre = aP1_Cumpre;
         objprc_preenchechklst.context.SetSubmitInitialConfig(context);
         objprc_preenchechklst.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_preenchechklst);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_preenchechklst)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P008T2 */
         pr_default.execute(0, new Object[] {n1230CheckList_De, A1230CheckList_De});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1151CheckList_Ativo = P008T2_A1151CheckList_Ativo[0];
            A758CheckList_Codigo = P008T2_A758CheckList_Codigo[0];
            A763CheckList_Descricao = P008T2_A763CheckList_Descricao[0];
            AV10SDT_Item.gxTpr_Codigo = A758CheckList_Codigo;
            AV10SDT_Item.gxTpr_Descricao = A763CheckList_Descricao;
            AV10SDT_Item.gxTpr_Cumpre = AV12Cumpre;
            AV9SDT_CheckList.Add(AV10SDT_Item, 0);
            AV10SDT_Item = new SdtSDT_CheckList_Item(context);
            pr_default.readNext(0);
         }
         pr_default.close(0);
         AV8WebSession.Set("CheckList", AV9SDT_CheckList.ToXml(false, true, "SDT_CheckList", "GxEv3Up14_Meetrika"));
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P008T2_A1230CheckList_De = new short[1] ;
         P008T2_n1230CheckList_De = new bool[] {false} ;
         P008T2_A1151CheckList_Ativo = new bool[] {false} ;
         P008T2_A758CheckList_Codigo = new int[1] ;
         P008T2_A763CheckList_Descricao = new String[] {""} ;
         A763CheckList_Descricao = "";
         AV10SDT_Item = new SdtSDT_CheckList_Item(context);
         AV9SDT_CheckList = new GxObjectCollection( context, "SDT_CheckList.Item", "GxEv3Up14_Meetrika", "SdtSDT_CheckList_Item", "GeneXus.Programs");
         AV8WebSession = context.GetSession();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_preenchechklst__default(),
            new Object[][] {
                new Object[] {
               P008T2_A1230CheckList_De, P008T2_n1230CheckList_De, P008T2_A1151CheckList_Ativo, P008T2_A758CheckList_Codigo, P008T2_A763CheckList_Descricao
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A1230CheckList_De ;
      private int A758CheckList_Codigo ;
      private String AV12Cumpre ;
      private String scmdbuf ;
      private bool n1230CheckList_De ;
      private bool A1151CheckList_Ativo ;
      private String A763CheckList_Descricao ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private short[] P008T2_A1230CheckList_De ;
      private bool[] P008T2_n1230CheckList_De ;
      private bool[] P008T2_A1151CheckList_Ativo ;
      private int[] P008T2_A758CheckList_Codigo ;
      private String[] P008T2_A763CheckList_Descricao ;
      private IGxSession AV8WebSession ;
      [ObjectCollection(ItemType=typeof( SdtSDT_CheckList_Item ))]
      private IGxCollection AV9SDT_CheckList ;
      private SdtSDT_CheckList_Item AV10SDT_Item ;
   }

   public class prc_preenchechklst__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP008T2 ;
          prmP008T2 = new Object[] {
          new Object[] {"@CheckList_De",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P008T2", "SELECT [CheckList_De], [CheckList_Ativo], [CheckList_Codigo], [CheckList_Descricao] FROM [CheckList] WITH (NOLOCK) WHERE ([CheckList_De] = @CheckList_De) AND ([CheckList_Ativo] = 1) ORDER BY [CheckList_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008T2,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(1, (short)parms[1]);
                }
                return;
       }
    }

 }

}
