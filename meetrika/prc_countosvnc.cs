/*
               File: PRC_CountOSVnc
        Description: Count OS Vnc
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:29.93
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_countosvnc : GXProcedure
   {
      public prc_countosvnc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_countosvnc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo ,
                           out short aP1_Count )
      {
         this.AV8ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV10Count = 0 ;
         initialize();
         executePrivate();
         aP1_Count=this.AV10Count;
      }

      public short executeUdp( int aP0_ContagemResultado_Codigo )
      {
         this.AV8ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV10Count = 0 ;
         initialize();
         executePrivate();
         aP1_Count=this.AV10Count;
         return AV10Count ;
      }

      public void executeSubmit( int aP0_ContagemResultado_Codigo ,
                                 out short aP1_Count )
      {
         prc_countosvnc objprc_countosvnc;
         objprc_countosvnc = new prc_countosvnc();
         objprc_countosvnc.AV8ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_countosvnc.AV10Count = 0 ;
         objprc_countosvnc.context.SetSubmitInitialConfig(context);
         objprc_countosvnc.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_countosvnc);
         aP1_Count=this.AV10Count;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_countosvnc)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Optimized group. */
         /* Using cursor P00662 */
         pr_default.execute(0, new Object[] {AV8ContagemResultado_Codigo});
         cV10Count = P00662_AV10Count[0];
         pr_default.close(0);
         AV10Count = (short)(AV10Count+cV10Count*1);
         /* End optimized group. */
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00662_AV10Count = new short[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_countosvnc__default(),
            new Object[][] {
                new Object[] {
               P00662_AV10Count
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV10Count ;
      private short cV10Count ;
      private int AV8ContagemResultado_Codigo ;
      private String scmdbuf ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private short[] P00662_AV10Count ;
      private short aP1_Count ;
   }

   public class prc_countosvnc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00662 ;
          prmP00662 = new Object[] {
          new Object[] {"@AV8ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00662", "SELECT COUNT(*) FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_OSVinculada] = @AV8ContagemResultado_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00662,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
