/*
               File: GetWWTipoRequisitoFilterData
        Description: Get WWTipo Requisito Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:13:37.25
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwtiporequisitofilterdata : GXProcedure
   {
      public getwwtiporequisitofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwtiporequisitofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV20DDOName = aP0_DDOName;
         this.AV18SearchTxt = aP1_SearchTxt;
         this.AV19SearchTxtTo = aP2_SearchTxtTo;
         this.AV24OptionsJson = "" ;
         this.AV27OptionsDescJson = "" ;
         this.AV29OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV20DDOName = aP0_DDOName;
         this.AV18SearchTxt = aP1_SearchTxt;
         this.AV19SearchTxtTo = aP2_SearchTxtTo;
         this.AV24OptionsJson = "" ;
         this.AV27OptionsDescJson = "" ;
         this.AV29OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
         return AV29OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwtiporequisitofilterdata objgetwwtiporequisitofilterdata;
         objgetwwtiporequisitofilterdata = new getwwtiporequisitofilterdata();
         objgetwwtiporequisitofilterdata.AV20DDOName = aP0_DDOName;
         objgetwwtiporequisitofilterdata.AV18SearchTxt = aP1_SearchTxt;
         objgetwwtiporequisitofilterdata.AV19SearchTxtTo = aP2_SearchTxtTo;
         objgetwwtiporequisitofilterdata.AV24OptionsJson = "" ;
         objgetwwtiporequisitofilterdata.AV27OptionsDescJson = "" ;
         objgetwwtiporequisitofilterdata.AV29OptionIndexesJson = "" ;
         objgetwwtiporequisitofilterdata.context.SetSubmitInitialConfig(context);
         objgetwwtiporequisitofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwtiporequisitofilterdata);
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwtiporequisitofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV23Options = (IGxCollection)(new GxSimpleCollection());
         AV26OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV28OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_TIPOREQUISITO_IDENTIFICADOR") == 0 )
         {
            /* Execute user subroutine: 'LOADTIPOREQUISITO_IDENTIFICADOROPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_TIPOREQUISITO_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADTIPOREQUISITO_DESCRICAOOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV24OptionsJson = AV23Options.ToJSonString(false);
         AV27OptionsDescJson = AV26OptionsDesc.ToJSonString(false);
         AV29OptionIndexesJson = AV28OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV31Session.Get("WWTipoRequisitoGridState"), "") == 0 )
         {
            AV33GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWTipoRequisitoGridState"), "");
         }
         else
         {
            AV33GridState.FromXml(AV31Session.Get("WWTipoRequisitoGridState"), "");
         }
         AV38GXV1 = 1;
         while ( AV38GXV1 <= AV33GridState.gxTpr_Filtervalues.Count )
         {
            AV34GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV33GridState.gxTpr_Filtervalues.Item(AV38GXV1));
            if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFTIPOREQUISITO_IDENTIFICADOR") == 0 )
            {
               AV10TFTipoRequisito_Identificador = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFTIPOREQUISITO_IDENTIFICADOR_SEL") == 0 )
            {
               AV11TFTipoRequisito_Identificador_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFTIPOREQUISITO_DESCRICAO") == 0 )
            {
               AV12TFTipoRequisito_Descricao = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFTIPOREQUISITO_DESCRICAO_SEL") == 0 )
            {
               AV13TFTipoRequisito_Descricao_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFTIPOREQUISITO_CLASSIFICACAO_SEL") == 0 )
            {
               AV14TFTipoRequisito_Classificacao_SelsJson = AV34GridStateFilterValue.gxTpr_Value;
               AV15TFTipoRequisito_Classificacao_Sels.FromJSonString(AV14TFTipoRequisito_Classificacao_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFTIPOREQUISITO_LINNEGCOD") == 0 )
            {
               AV16TFTipoRequisito_LinNegCod = (int)(NumberUtil.Val( AV34GridStateFilterValue.gxTpr_Value, "."));
               AV17TFTipoRequisito_LinNegCod_To = (int)(NumberUtil.Val( AV34GridStateFilterValue.gxTpr_Valueto, "."));
            }
            AV38GXV1 = (int)(AV38GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADTIPOREQUISITO_IDENTIFICADOROPTIONS' Routine */
         AV10TFTipoRequisito_Identificador = AV18SearchTxt;
         AV11TFTipoRequisito_Identificador_Sel = "";
         AV40WWTipoRequisitoDS_1_Tftiporequisito_identificador = AV10TFTipoRequisito_Identificador;
         AV41WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel = AV11TFTipoRequisito_Identificador_Sel;
         AV42WWTipoRequisitoDS_3_Tftiporequisito_descricao = AV12TFTipoRequisito_Descricao;
         AV43WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel = AV13TFTipoRequisito_Descricao_Sel;
         AV44WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels = AV15TFTipoRequisito_Classificacao_Sels;
         AV45WWTipoRequisitoDS_6_Tftiporequisito_linnegcod = AV16TFTipoRequisito_LinNegCod;
         AV46WWTipoRequisitoDS_7_Tftiporequisito_linnegcod_to = AV17TFTipoRequisito_LinNegCod_To;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A2044TipoRequisito_Classificacao ,
                                              AV44WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels ,
                                              AV41WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel ,
                                              AV40WWTipoRequisitoDS_1_Tftiporequisito_identificador ,
                                              AV43WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel ,
                                              AV42WWTipoRequisitoDS_3_Tftiporequisito_descricao ,
                                              AV44WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels.Count ,
                                              AV45WWTipoRequisitoDS_6_Tftiporequisito_linnegcod ,
                                              AV46WWTipoRequisitoDS_7_Tftiporequisito_linnegcod_to ,
                                              A2042TipoRequisito_Identificador ,
                                              A2043TipoRequisito_Descricao ,
                                              A2039TipoRequisito_LinNegCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV40WWTipoRequisitoDS_1_Tftiporequisito_identificador = StringUtil.Concat( StringUtil.RTrim( AV40WWTipoRequisitoDS_1_Tftiporequisito_identificador), "%", "");
         lV42WWTipoRequisitoDS_3_Tftiporequisito_descricao = StringUtil.Concat( StringUtil.RTrim( AV42WWTipoRequisitoDS_3_Tftiporequisito_descricao), "%", "");
         /* Using cursor P00WO2 */
         pr_default.execute(0, new Object[] {lV40WWTipoRequisitoDS_1_Tftiporequisito_identificador, AV41WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel, lV42WWTipoRequisitoDS_3_Tftiporequisito_descricao, AV43WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel, AV45WWTipoRequisitoDS_6_Tftiporequisito_linnegcod, AV46WWTipoRequisitoDS_7_Tftiporequisito_linnegcod_to});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKWO2 = false;
            A2042TipoRequisito_Identificador = P00WO2_A2042TipoRequisito_Identificador[0];
            A2039TipoRequisito_LinNegCod = P00WO2_A2039TipoRequisito_LinNegCod[0];
            n2039TipoRequisito_LinNegCod = P00WO2_n2039TipoRequisito_LinNegCod[0];
            A2044TipoRequisito_Classificacao = P00WO2_A2044TipoRequisito_Classificacao[0];
            n2044TipoRequisito_Classificacao = P00WO2_n2044TipoRequisito_Classificacao[0];
            A2043TipoRequisito_Descricao = P00WO2_A2043TipoRequisito_Descricao[0];
            n2043TipoRequisito_Descricao = P00WO2_n2043TipoRequisito_Descricao[0];
            A2041TipoRequisito_Codigo = P00WO2_A2041TipoRequisito_Codigo[0];
            AV30count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00WO2_A2042TipoRequisito_Identificador[0], A2042TipoRequisito_Identificador) == 0 ) )
            {
               BRKWO2 = false;
               A2041TipoRequisito_Codigo = P00WO2_A2041TipoRequisito_Codigo[0];
               AV30count = (long)(AV30count+1);
               BRKWO2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A2042TipoRequisito_Identificador)) )
            {
               AV22Option = A2042TipoRequisito_Identificador;
               AV23Options.Add(AV22Option, 0);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKWO2 )
            {
               BRKWO2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADTIPOREQUISITO_DESCRICAOOPTIONS' Routine */
         AV12TFTipoRequisito_Descricao = AV18SearchTxt;
         AV13TFTipoRequisito_Descricao_Sel = "";
         AV40WWTipoRequisitoDS_1_Tftiporequisito_identificador = AV10TFTipoRequisito_Identificador;
         AV41WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel = AV11TFTipoRequisito_Identificador_Sel;
         AV42WWTipoRequisitoDS_3_Tftiporequisito_descricao = AV12TFTipoRequisito_Descricao;
         AV43WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel = AV13TFTipoRequisito_Descricao_Sel;
         AV44WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels = AV15TFTipoRequisito_Classificacao_Sels;
         AV45WWTipoRequisitoDS_6_Tftiporequisito_linnegcod = AV16TFTipoRequisito_LinNegCod;
         AV46WWTipoRequisitoDS_7_Tftiporequisito_linnegcod_to = AV17TFTipoRequisito_LinNegCod_To;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A2044TipoRequisito_Classificacao ,
                                              AV44WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels ,
                                              AV41WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel ,
                                              AV40WWTipoRequisitoDS_1_Tftiporequisito_identificador ,
                                              AV43WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel ,
                                              AV42WWTipoRequisitoDS_3_Tftiporequisito_descricao ,
                                              AV44WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels.Count ,
                                              AV45WWTipoRequisitoDS_6_Tftiporequisito_linnegcod ,
                                              AV46WWTipoRequisitoDS_7_Tftiporequisito_linnegcod_to ,
                                              A2042TipoRequisito_Identificador ,
                                              A2043TipoRequisito_Descricao ,
                                              A2039TipoRequisito_LinNegCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV40WWTipoRequisitoDS_1_Tftiporequisito_identificador = StringUtil.Concat( StringUtil.RTrim( AV40WWTipoRequisitoDS_1_Tftiporequisito_identificador), "%", "");
         lV42WWTipoRequisitoDS_3_Tftiporequisito_descricao = StringUtil.Concat( StringUtil.RTrim( AV42WWTipoRequisitoDS_3_Tftiporequisito_descricao), "%", "");
         /* Using cursor P00WO3 */
         pr_default.execute(1, new Object[] {lV40WWTipoRequisitoDS_1_Tftiporequisito_identificador, AV41WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel, lV42WWTipoRequisitoDS_3_Tftiporequisito_descricao, AV43WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel, AV45WWTipoRequisitoDS_6_Tftiporequisito_linnegcod, AV46WWTipoRequisitoDS_7_Tftiporequisito_linnegcod_to});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKWO4 = false;
            A2043TipoRequisito_Descricao = P00WO3_A2043TipoRequisito_Descricao[0];
            n2043TipoRequisito_Descricao = P00WO3_n2043TipoRequisito_Descricao[0];
            A2039TipoRequisito_LinNegCod = P00WO3_A2039TipoRequisito_LinNegCod[0];
            n2039TipoRequisito_LinNegCod = P00WO3_n2039TipoRequisito_LinNegCod[0];
            A2044TipoRequisito_Classificacao = P00WO3_A2044TipoRequisito_Classificacao[0];
            n2044TipoRequisito_Classificacao = P00WO3_n2044TipoRequisito_Classificacao[0];
            A2042TipoRequisito_Identificador = P00WO3_A2042TipoRequisito_Identificador[0];
            A2041TipoRequisito_Codigo = P00WO3_A2041TipoRequisito_Codigo[0];
            AV30count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00WO3_A2043TipoRequisito_Descricao[0], A2043TipoRequisito_Descricao) == 0 ) )
            {
               BRKWO4 = false;
               A2041TipoRequisito_Codigo = P00WO3_A2041TipoRequisito_Codigo[0];
               AV30count = (long)(AV30count+1);
               BRKWO4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A2043TipoRequisito_Descricao)) )
            {
               AV22Option = A2043TipoRequisito_Descricao;
               AV23Options.Add(AV22Option, 0);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKWO4 )
            {
               BRKWO4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV23Options = new GxSimpleCollection();
         AV26OptionsDesc = new GxSimpleCollection();
         AV28OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV31Session = context.GetSession();
         AV33GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV34GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFTipoRequisito_Identificador = "";
         AV11TFTipoRequisito_Identificador_Sel = "";
         AV12TFTipoRequisito_Descricao = "";
         AV13TFTipoRequisito_Descricao_Sel = "";
         AV14TFTipoRequisito_Classificacao_SelsJson = "";
         AV15TFTipoRequisito_Classificacao_Sels = new GxSimpleCollection();
         AV40WWTipoRequisitoDS_1_Tftiporequisito_identificador = "";
         AV41WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel = "";
         AV42WWTipoRequisitoDS_3_Tftiporequisito_descricao = "";
         AV43WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel = "";
         AV44WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels = new GxSimpleCollection();
         scmdbuf = "";
         lV40WWTipoRequisitoDS_1_Tftiporequisito_identificador = "";
         lV42WWTipoRequisitoDS_3_Tftiporequisito_descricao = "";
         A2044TipoRequisito_Classificacao = "";
         A2042TipoRequisito_Identificador = "";
         A2043TipoRequisito_Descricao = "";
         P00WO2_A2042TipoRequisito_Identificador = new String[] {""} ;
         P00WO2_A2039TipoRequisito_LinNegCod = new int[1] ;
         P00WO2_n2039TipoRequisito_LinNegCod = new bool[] {false} ;
         P00WO2_A2044TipoRequisito_Classificacao = new String[] {""} ;
         P00WO2_n2044TipoRequisito_Classificacao = new bool[] {false} ;
         P00WO2_A2043TipoRequisito_Descricao = new String[] {""} ;
         P00WO2_n2043TipoRequisito_Descricao = new bool[] {false} ;
         P00WO2_A2041TipoRequisito_Codigo = new int[1] ;
         AV22Option = "";
         P00WO3_A2043TipoRequisito_Descricao = new String[] {""} ;
         P00WO3_n2043TipoRequisito_Descricao = new bool[] {false} ;
         P00WO3_A2039TipoRequisito_LinNegCod = new int[1] ;
         P00WO3_n2039TipoRequisito_LinNegCod = new bool[] {false} ;
         P00WO3_A2044TipoRequisito_Classificacao = new String[] {""} ;
         P00WO3_n2044TipoRequisito_Classificacao = new bool[] {false} ;
         P00WO3_A2042TipoRequisito_Identificador = new String[] {""} ;
         P00WO3_A2041TipoRequisito_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwtiporequisitofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00WO2_A2042TipoRequisito_Identificador, P00WO2_A2039TipoRequisito_LinNegCod, P00WO2_n2039TipoRequisito_LinNegCod, P00WO2_A2044TipoRequisito_Classificacao, P00WO2_n2044TipoRequisito_Classificacao, P00WO2_A2043TipoRequisito_Descricao, P00WO2_n2043TipoRequisito_Descricao, P00WO2_A2041TipoRequisito_Codigo
               }
               , new Object[] {
               P00WO3_A2043TipoRequisito_Descricao, P00WO3_n2043TipoRequisito_Descricao, P00WO3_A2039TipoRequisito_LinNegCod, P00WO3_n2039TipoRequisito_LinNegCod, P00WO3_A2044TipoRequisito_Classificacao, P00WO3_n2044TipoRequisito_Classificacao, P00WO3_A2042TipoRequisito_Identificador, P00WO3_A2041TipoRequisito_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV38GXV1 ;
      private int AV16TFTipoRequisito_LinNegCod ;
      private int AV17TFTipoRequisito_LinNegCod_To ;
      private int AV45WWTipoRequisitoDS_6_Tftiporequisito_linnegcod ;
      private int AV46WWTipoRequisitoDS_7_Tftiporequisito_linnegcod_to ;
      private int AV44WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels_Count ;
      private int A2039TipoRequisito_LinNegCod ;
      private int A2041TipoRequisito_Codigo ;
      private long AV30count ;
      private String scmdbuf ;
      private String A2044TipoRequisito_Classificacao ;
      private bool returnInSub ;
      private bool BRKWO2 ;
      private bool n2039TipoRequisito_LinNegCod ;
      private bool n2044TipoRequisito_Classificacao ;
      private bool n2043TipoRequisito_Descricao ;
      private bool BRKWO4 ;
      private String AV29OptionIndexesJson ;
      private String AV24OptionsJson ;
      private String AV27OptionsDescJson ;
      private String AV14TFTipoRequisito_Classificacao_SelsJson ;
      private String AV20DDOName ;
      private String AV18SearchTxt ;
      private String AV19SearchTxtTo ;
      private String AV10TFTipoRequisito_Identificador ;
      private String AV11TFTipoRequisito_Identificador_Sel ;
      private String AV12TFTipoRequisito_Descricao ;
      private String AV13TFTipoRequisito_Descricao_Sel ;
      private String AV40WWTipoRequisitoDS_1_Tftiporequisito_identificador ;
      private String AV41WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel ;
      private String AV42WWTipoRequisitoDS_3_Tftiporequisito_descricao ;
      private String AV43WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel ;
      private String lV40WWTipoRequisitoDS_1_Tftiporequisito_identificador ;
      private String lV42WWTipoRequisitoDS_3_Tftiporequisito_descricao ;
      private String A2042TipoRequisito_Identificador ;
      private String A2043TipoRequisito_Descricao ;
      private String AV22Option ;
      private IGxSession AV31Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00WO2_A2042TipoRequisito_Identificador ;
      private int[] P00WO2_A2039TipoRequisito_LinNegCod ;
      private bool[] P00WO2_n2039TipoRequisito_LinNegCod ;
      private String[] P00WO2_A2044TipoRequisito_Classificacao ;
      private bool[] P00WO2_n2044TipoRequisito_Classificacao ;
      private String[] P00WO2_A2043TipoRequisito_Descricao ;
      private bool[] P00WO2_n2043TipoRequisito_Descricao ;
      private int[] P00WO2_A2041TipoRequisito_Codigo ;
      private String[] P00WO3_A2043TipoRequisito_Descricao ;
      private bool[] P00WO3_n2043TipoRequisito_Descricao ;
      private int[] P00WO3_A2039TipoRequisito_LinNegCod ;
      private bool[] P00WO3_n2039TipoRequisito_LinNegCod ;
      private String[] P00WO3_A2044TipoRequisito_Classificacao ;
      private bool[] P00WO3_n2044TipoRequisito_Classificacao ;
      private String[] P00WO3_A2042TipoRequisito_Identificador ;
      private int[] P00WO3_A2041TipoRequisito_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV15TFTipoRequisito_Classificacao_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV44WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV28OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV33GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV34GridStateFilterValue ;
   }

   public class getwwtiporequisitofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00WO2( IGxContext context ,
                                             String A2044TipoRequisito_Classificacao ,
                                             IGxCollection AV44WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels ,
                                             String AV41WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel ,
                                             String AV40WWTipoRequisitoDS_1_Tftiporequisito_identificador ,
                                             String AV43WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel ,
                                             String AV42WWTipoRequisitoDS_3_Tftiporequisito_descricao ,
                                             int AV44WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels_Count ,
                                             int AV45WWTipoRequisitoDS_6_Tftiporequisito_linnegcod ,
                                             int AV46WWTipoRequisitoDS_7_Tftiporequisito_linnegcod_to ,
                                             String A2042TipoRequisito_Identificador ,
                                             String A2043TipoRequisito_Descricao ,
                                             int A2039TipoRequisito_LinNegCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [6] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [TipoRequisito_Identificador], [TipoRequisito_LinNegCod], [TipoRequisito_Classificacao], [TipoRequisito_Descricao], [TipoRequisito_Codigo] FROM [TipoRequisito] WITH (NOLOCK)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV41WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40WWTipoRequisitoDS_1_Tftiporequisito_identificador)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoRequisito_Identificador] like @lV40WWTipoRequisitoDS_1_Tftiporequisito_identificador)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoRequisito_Identificador] like @lV40WWTipoRequisitoDS_1_Tftiporequisito_identificador)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoRequisito_Identificador] = @AV41WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoRequisito_Identificador] = @AV41WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV43WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42WWTipoRequisitoDS_3_Tftiporequisito_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoRequisito_Descricao] like @lV42WWTipoRequisitoDS_3_Tftiporequisito_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoRequisito_Descricao] like @lV42WWTipoRequisitoDS_3_Tftiporequisito_descricao)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoRequisito_Descricao] = @AV43WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoRequisito_Descricao] = @AV43WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV44WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV44WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels, "[TipoRequisito_Classificacao] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV44WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels, "[TipoRequisito_Classificacao] IN (", ")") + ")";
            }
         }
         if ( ! (0==AV45WWTipoRequisitoDS_6_Tftiporequisito_linnegcod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoRequisito_LinNegCod] >= @AV45WWTipoRequisitoDS_6_Tftiporequisito_linnegcod)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoRequisito_LinNegCod] >= @AV45WWTipoRequisitoDS_6_Tftiporequisito_linnegcod)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! (0==AV46WWTipoRequisitoDS_7_Tftiporequisito_linnegcod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoRequisito_LinNegCod] <= @AV46WWTipoRequisitoDS_7_Tftiporequisito_linnegcod_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoRequisito_LinNegCod] <= @AV46WWTipoRequisitoDS_7_Tftiporequisito_linnegcod_to)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [TipoRequisito_Identificador]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00WO3( IGxContext context ,
                                             String A2044TipoRequisito_Classificacao ,
                                             IGxCollection AV44WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels ,
                                             String AV41WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel ,
                                             String AV40WWTipoRequisitoDS_1_Tftiporequisito_identificador ,
                                             String AV43WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel ,
                                             String AV42WWTipoRequisitoDS_3_Tftiporequisito_descricao ,
                                             int AV44WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels_Count ,
                                             int AV45WWTipoRequisitoDS_6_Tftiporequisito_linnegcod ,
                                             int AV46WWTipoRequisitoDS_7_Tftiporequisito_linnegcod_to ,
                                             String A2042TipoRequisito_Identificador ,
                                             String A2043TipoRequisito_Descricao ,
                                             int A2039TipoRequisito_LinNegCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [6] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [TipoRequisito_Descricao], [TipoRequisito_LinNegCod], [TipoRequisito_Classificacao], [TipoRequisito_Identificador], [TipoRequisito_Codigo] FROM [TipoRequisito] WITH (NOLOCK)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV41WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40WWTipoRequisitoDS_1_Tftiporequisito_identificador)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoRequisito_Identificador] like @lV40WWTipoRequisitoDS_1_Tftiporequisito_identificador)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoRequisito_Identificador] like @lV40WWTipoRequisitoDS_1_Tftiporequisito_identificador)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoRequisito_Identificador] = @AV41WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoRequisito_Identificador] = @AV41WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV43WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42WWTipoRequisitoDS_3_Tftiporequisito_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoRequisito_Descricao] like @lV42WWTipoRequisitoDS_3_Tftiporequisito_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoRequisito_Descricao] like @lV42WWTipoRequisitoDS_3_Tftiporequisito_descricao)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoRequisito_Descricao] = @AV43WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoRequisito_Descricao] = @AV43WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV44WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV44WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels, "[TipoRequisito_Classificacao] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV44WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels, "[TipoRequisito_Classificacao] IN (", ")") + ")";
            }
         }
         if ( ! (0==AV45WWTipoRequisitoDS_6_Tftiporequisito_linnegcod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoRequisito_LinNegCod] >= @AV45WWTipoRequisitoDS_6_Tftiporequisito_linnegcod)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoRequisito_LinNegCod] >= @AV45WWTipoRequisitoDS_6_Tftiporequisito_linnegcod)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ! (0==AV46WWTipoRequisitoDS_7_Tftiporequisito_linnegcod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoRequisito_LinNegCod] <= @AV46WWTipoRequisitoDS_7_Tftiporequisito_linnegcod_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoRequisito_LinNegCod] <= @AV46WWTipoRequisitoDS_7_Tftiporequisito_linnegcod_to)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [TipoRequisito_Descricao]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00WO2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (int)dynConstraints[7] , (int)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] );
               case 1 :
                     return conditional_P00WO3(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (int)dynConstraints[7] , (int)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00WO2 ;
          prmP00WO2 = new Object[] {
          new Object[] {"@lV40WWTipoRequisitoDS_1_Tftiporequisito_identificador",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV41WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV42WWTipoRequisitoDS_3_Tftiporequisito_descricao",SqlDbType.VarChar,250,0} ,
          new Object[] {"@AV43WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel",SqlDbType.VarChar,250,0} ,
          new Object[] {"@AV45WWTipoRequisitoDS_6_Tftiporequisito_linnegcod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV46WWTipoRequisitoDS_7_Tftiporequisito_linnegcod_to",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00WO3 ;
          prmP00WO3 = new Object[] {
          new Object[] {"@lV40WWTipoRequisitoDS_1_Tftiporequisito_identificador",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV41WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV42WWTipoRequisitoDS_3_Tftiporequisito_descricao",SqlDbType.VarChar,250,0} ,
          new Object[] {"@AV43WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel",SqlDbType.VarChar,250,0} ,
          new Object[] {"@AV45WWTipoRequisitoDS_6_Tftiporequisito_linnegcod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV46WWTipoRequisitoDS_7_Tftiporequisito_linnegcod_to",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00WO2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WO2,100,0,true,false )
             ,new CursorDef("P00WO3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WO3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getVarchar(4) ;
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwtiporequisitofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwtiporequisitofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwtiporequisitofilterdata") )
          {
             return  ;
          }
          getwwtiporequisitofilterdata worker = new getwwtiporequisitofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
