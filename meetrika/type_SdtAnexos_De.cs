/*
               File: type_SdtAnexos_De
        Description: Anexos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 23:53:4.15
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "Anexos.De" )]
   [XmlType(TypeName =  "Anexos.De" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtAnexos_De : GxSilentTrnSdt, IGxSilentTrnGridItem
   {
      public SdtAnexos_De( )
      {
         /* Constructor for serialization */
         gxTv_SdtAnexos_De_Mode = "";
      }

      public SdtAnexos_De( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "De");
         metadata.Set("BT", "AnexosDe");
         metadata.Set("PK", "[ \"AnexoDe_Id\",\"AnexoDe_Tabela\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"Anexo_Codigo\" ],\"FKMap\":[  ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Modified" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Anexode_id_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Anexode_tabela_Z" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtAnexos_De deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtAnexos_De)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtAnexos_De obj ;
         obj = this;
         obj.gxTpr_Anexode_id = deserialized.gxTpr_Anexode_id;
         obj.gxTpr_Anexode_tabela = deserialized.gxTpr_Anexode_tabela;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Modified = deserialized.gxTpr_Modified;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Anexode_id_Z = deserialized.gxTpr_Anexode_id_Z;
         obj.gxTpr_Anexode_tabela_Z = deserialized.gxTpr_Anexode_tabela_Z;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "AnexoDe_Id") )
               {
                  gxTv_SdtAnexos_De_Anexode_id = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AnexoDe_Tabela") )
               {
                  gxTv_SdtAnexos_De_Anexode_tabela = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtAnexos_De_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Modified") )
               {
                  gxTv_SdtAnexos_De_Modified = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtAnexos_De_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AnexoDe_Id_Z") )
               {
                  gxTv_SdtAnexos_De_Anexode_id_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AnexoDe_Tabela_Z") )
               {
                  gxTv_SdtAnexos_De_Anexode_tabela_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "Anexos.De";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("AnexoDe_Id", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAnexos_De_Anexode_id), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("AnexoDe_Tabela", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAnexos_De_Anexode_tabela), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtAnexos_De_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Modified", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAnexos_De_Modified), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAnexos_De_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("AnexoDe_Id_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAnexos_De_Anexode_id_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("AnexoDe_Tabela_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAnexos_De_Anexode_tabela_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("AnexoDe_Id", gxTv_SdtAnexos_De_Anexode_id, false);
         AddObjectProperty("AnexoDe_Tabela", gxTv_SdtAnexos_De_Anexode_tabela, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtAnexos_De_Mode, false);
            AddObjectProperty("Modified", gxTv_SdtAnexos_De_Modified, false);
            AddObjectProperty("Initialized", gxTv_SdtAnexos_De_Initialized, false);
            AddObjectProperty("AnexoDe_Id_Z", gxTv_SdtAnexos_De_Anexode_id_Z, false);
            AddObjectProperty("AnexoDe_Tabela_Z", gxTv_SdtAnexos_De_Anexode_tabela_Z, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "AnexoDe_Id" )]
      [  XmlElement( ElementName = "AnexoDe_Id"   )]
      public int gxTpr_Anexode_id
      {
         get {
            return gxTv_SdtAnexos_De_Anexode_id ;
         }

         set {
            gxTv_SdtAnexos_De_Anexode_id = (int)(value);
            gxTv_SdtAnexos_De_Modified = 1;
         }

      }

      [  SoapElement( ElementName = "AnexoDe_Tabela" )]
      [  XmlElement( ElementName = "AnexoDe_Tabela"   )]
      public int gxTpr_Anexode_tabela
      {
         get {
            return gxTv_SdtAnexos_De_Anexode_tabela ;
         }

         set {
            gxTv_SdtAnexos_De_Anexode_tabela = (int)(value);
            gxTv_SdtAnexos_De_Modified = 1;
         }

      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtAnexos_De_Mode ;
         }

         set {
            gxTv_SdtAnexos_De_Mode = (String)(value);
         }

      }

      public void gxTv_SdtAnexos_De_Mode_SetNull( )
      {
         gxTv_SdtAnexos_De_Mode = "";
         return  ;
      }

      public bool gxTv_SdtAnexos_De_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Modified" )]
      [  XmlElement( ElementName = "Modified"   )]
      public short gxTpr_Modified
      {
         get {
            return gxTv_SdtAnexos_De_Modified ;
         }

         set {
            gxTv_SdtAnexos_De_Modified = (short)(value);
         }

      }

      public void gxTv_SdtAnexos_De_Modified_SetNull( )
      {
         gxTv_SdtAnexos_De_Modified = 0;
         return  ;
      }

      public bool gxTv_SdtAnexos_De_Modified_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtAnexos_De_Initialized ;
         }

         set {
            gxTv_SdtAnexos_De_Initialized = (short)(value);
            gxTv_SdtAnexos_De_Modified = 1;
         }

      }

      public void gxTv_SdtAnexos_De_Initialized_SetNull( )
      {
         gxTv_SdtAnexos_De_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtAnexos_De_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AnexoDe_Id_Z" )]
      [  XmlElement( ElementName = "AnexoDe_Id_Z"   )]
      public int gxTpr_Anexode_id_Z
      {
         get {
            return gxTv_SdtAnexos_De_Anexode_id_Z ;
         }

         set {
            gxTv_SdtAnexos_De_Anexode_id_Z = (int)(value);
            gxTv_SdtAnexos_De_Modified = 1;
         }

      }

      public void gxTv_SdtAnexos_De_Anexode_id_Z_SetNull( )
      {
         gxTv_SdtAnexos_De_Anexode_id_Z = 0;
         return  ;
      }

      public bool gxTv_SdtAnexos_De_Anexode_id_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AnexoDe_Tabela_Z" )]
      [  XmlElement( ElementName = "AnexoDe_Tabela_Z"   )]
      public int gxTpr_Anexode_tabela_Z
      {
         get {
            return gxTv_SdtAnexos_De_Anexode_tabela_Z ;
         }

         set {
            gxTv_SdtAnexos_De_Anexode_tabela_Z = (int)(value);
            gxTv_SdtAnexos_De_Modified = 1;
         }

      }

      public void gxTv_SdtAnexos_De_Anexode_tabela_Z_SetNull( )
      {
         gxTv_SdtAnexos_De_Anexode_tabela_Z = 0;
         return  ;
      }

      public bool gxTv_SdtAnexos_De_Anexode_tabela_Z_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtAnexos_De_Mode = "";
         sTagName = "";
         return  ;
      }

      private short gxTv_SdtAnexos_De_Modified ;
      private short gxTv_SdtAnexos_De_Initialized ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtAnexos_De_Anexode_id ;
      private int gxTv_SdtAnexos_De_Anexode_tabela ;
      private int gxTv_SdtAnexos_De_Anexode_id_Z ;
      private int gxTv_SdtAnexos_De_Anexode_tabela_Z ;
      private String gxTv_SdtAnexos_De_Mode ;
      private String sTagName ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"Anexos.De", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtAnexos_De_RESTInterface : GxGenericCollectionItem<SdtAnexos_De>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtAnexos_De_RESTInterface( ) : base()
      {
      }

      public SdtAnexos_De_RESTInterface( SdtAnexos_De psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "AnexoDe_Id" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Anexode_id
      {
         get {
            return sdt.gxTpr_Anexode_id ;
         }

         set {
            sdt.gxTpr_Anexode_id = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "AnexoDe_Tabela" , Order = 1 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Anexode_tabela
      {
         get {
            return sdt.gxTpr_Anexode_tabela ;
         }

         set {
            sdt.gxTpr_Anexode_tabela = (int)(value.HasValue ? value.Value : 0);
         }

      }

      public SdtAnexos_De sdt
      {
         get {
            return (SdtAnexos_De)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtAnexos_De() ;
         }
      }

   }

}
