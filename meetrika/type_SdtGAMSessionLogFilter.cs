/*
               File: type_SdtGAMSessionLogFilter
        Description: GAMSessionLogFilter
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 3:44:41.38
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMSessionLogFilter : GxUserType, IGxExternalObject
   {
      public SdtGAMSessionLogFilter( )
      {
         initialize();
      }

      public SdtGAMSessionLogFilter( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMSessionLogFilter_externalReference == null )
         {
            GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
         }
         returntostring = "";
         returntostring = (String)(GAMSessionLogFilter_externalReference.ToString());
         return returntostring ;
      }

      public String gxTpr_Token
      {
         get {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            return GAMSessionLogFilter_externalReference.Token ;
         }

         set {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            GAMSessionLogFilter_externalReference.Token = value;
         }

      }

      public String gxTpr_Userguid
      {
         get {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            return GAMSessionLogFilter_externalReference.UserGUID ;
         }

         set {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            GAMSessionLogFilter_externalReference.UserGUID = value;
         }

      }

      public String gxTpr_Name
      {
         get {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            return GAMSessionLogFilter_externalReference.Name ;
         }

         set {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            GAMSessionLogFilter_externalReference.Name = value;
         }

      }

      public DateTime gxTpr_Datefrom
      {
         get {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            return GAMSessionLogFilter_externalReference.DateFrom ;
         }

         set {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            GAMSessionLogFilter_externalReference.DateFrom = value;
         }

      }

      public DateTime gxTpr_Dateto
      {
         get {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            return GAMSessionLogFilter_externalReference.DateTo ;
         }

         set {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            GAMSessionLogFilter_externalReference.DateTo = value;
         }

      }

      public String gxTpr_Status
      {
         get {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            return GAMSessionLogFilter_externalReference.Status ;
         }

         set {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            GAMSessionLogFilter_externalReference.Status = value;
         }

      }

      public short gxTpr_Type
      {
         get {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            return GAMSessionLogFilter_externalReference.Type ;
         }

         set {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            GAMSessionLogFilter_externalReference.Type = value;
         }

      }

      public String gxTpr_Initialurl
      {
         get {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            return GAMSessionLogFilter_externalReference.InitialURL ;
         }

         set {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            GAMSessionLogFilter_externalReference.InitialURL = value;
         }

      }

      public String gxTpr_Initialipaddress
      {
         get {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            return GAMSessionLogFilter_externalReference.InitialIPAddress ;
         }

         set {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            GAMSessionLogFilter_externalReference.InitialIPAddress = value;
         }

      }

      public String gxTpr_Initialdeviceid
      {
         get {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            return GAMSessionLogFilter_externalReference.InitialDeviceId ;
         }

         set {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            GAMSessionLogFilter_externalReference.InitialDeviceId = value;
         }

      }

      public short gxTpr_Browserid
      {
         get {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            return GAMSessionLogFilter_externalReference.BrowserId ;
         }

         set {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            GAMSessionLogFilter_externalReference.BrowserId = value;
         }

      }

      public DateTime gxTpr_Lastaccessfrom
      {
         get {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            return GAMSessionLogFilter_externalReference.LastAccessFrom ;
         }

         set {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            GAMSessionLogFilter_externalReference.LastAccessFrom = value;
         }

      }

      public DateTime gxTpr_Lastaccessto
      {
         get {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            return GAMSessionLogFilter_externalReference.LastAccessTo ;
         }

         set {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            GAMSessionLogFilter_externalReference.LastAccessTo = value;
         }

      }

      public String gxTpr_Lasturl
      {
         get {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            return GAMSessionLogFilter_externalReference.LastURL ;
         }

         set {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            GAMSessionLogFilter_externalReference.LastURL = value;
         }

      }

      public DateTime gxTpr_Logindatefrom
      {
         get {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            return GAMSessionLogFilter_externalReference.LoginDateFrom ;
         }

         set {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            GAMSessionLogFilter_externalReference.LoginDateFrom = value;
         }

      }

      public DateTime gxTpr_Logindateto
      {
         get {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            return GAMSessionLogFilter_externalReference.LoginDateTo ;
         }

         set {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            GAMSessionLogFilter_externalReference.LoginDateTo = value;
         }

      }

      public int gxTpr_Loginretrycount
      {
         get {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            return GAMSessionLogFilter_externalReference.LoginRetryCount ;
         }

         set {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            GAMSessionLogFilter_externalReference.LoginRetryCount = value;
         }

      }

      public String gxTpr_Anonymous
      {
         get {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            return GAMSessionLogFilter_externalReference.Anonymous ;
         }

         set {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            GAMSessionLogFilter_externalReference.Anonymous = value;
         }

      }

      public String gxTpr_Externaltoken
      {
         get {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            return GAMSessionLogFilter_externalReference.ExternalToken ;
         }

         set {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            GAMSessionLogFilter_externalReference.ExternalToken = value;
         }

      }

      public DateTime gxTpr_Endedfrom
      {
         get {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            return GAMSessionLogFilter_externalReference.EndedFrom ;
         }

         set {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            GAMSessionLogFilter_externalReference.EndedFrom = value;
         }

      }

      public DateTime gxTpr_Endedto
      {
         get {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            return GAMSessionLogFilter_externalReference.EndedTo ;
         }

         set {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            GAMSessionLogFilter_externalReference.EndedTo = value;
         }

      }

      public long gxTpr_Applicationid
      {
         get {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            return GAMSessionLogFilter_externalReference.ApplicationId ;
         }

         set {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            GAMSessionLogFilter_externalReference.ApplicationId = value;
         }

      }

      public String gxTpr_Applicationdata
      {
         get {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            return GAMSessionLogFilter_externalReference.ApplicationData ;
         }

         set {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            GAMSessionLogFilter_externalReference.ApplicationData = value;
         }

      }

      public DateTime gxTpr_Tokenexpiresfrom
      {
         get {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            return GAMSessionLogFilter_externalReference.TokenExpiresFrom ;
         }

         set {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            GAMSessionLogFilter_externalReference.TokenExpiresFrom = value;
         }

      }

      public DateTime gxTpr_Tokenexpiresto
      {
         get {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            return GAMSessionLogFilter_externalReference.TokenExpiresTo ;
         }

         set {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            GAMSessionLogFilter_externalReference.TokenExpiresTo = value;
         }

      }

      public String gxTpr_Refreshtoken
      {
         get {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            return GAMSessionLogFilter_externalReference.RefreshToken ;
         }

         set {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            GAMSessionLogFilter_externalReference.RefreshToken = value;
         }

      }

      public bool gxTpr_Loadloginretries
      {
         get {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            return GAMSessionLogFilter_externalReference.LoadLoginRetries ;
         }

         set {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            GAMSessionLogFilter_externalReference.LoadLoginRetries = value;
         }

      }

      public int gxTpr_Start
      {
         get {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            return GAMSessionLogFilter_externalReference.Start ;
         }

         set {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            GAMSessionLogFilter_externalReference.Start = value;
         }

      }

      public int gxTpr_Limit
      {
         get {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            return GAMSessionLogFilter_externalReference.Limit ;
         }

         set {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            GAMSessionLogFilter_externalReference.Limit = value;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMSessionLogFilter_externalReference == null )
            {
               GAMSessionLogFilter_externalReference = new Artech.Security.GAMSessionLogFilter(context);
            }
            return GAMSessionLogFilter_externalReference ;
         }

         set {
            GAMSessionLogFilter_externalReference = (Artech.Security.GAMSessionLogFilter)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMSessionLogFilter GAMSessionLogFilter_externalReference=null ;
   }

}
