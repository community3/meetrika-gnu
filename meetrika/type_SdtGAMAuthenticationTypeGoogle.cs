/*
               File: type_SdtGAMAuthenticationTypeGoogle
        Description: GAMAuthenticationTypeGoogle
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:36:43.60
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMAuthenticationTypeGoogle : GxUserType, IGxExternalObject
   {
      public SdtGAMAuthenticationTypeGoogle( )
      {
         initialize();
      }

      public SdtGAMAuthenticationTypeGoogle( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void load( String gxTp_Name )
      {
         if ( GAMAuthenticationTypeGoogle_externalReference == null )
         {
            GAMAuthenticationTypeGoogle_externalReference = new Artech.Security.GAMAuthenticationTypeGoogle(context);
         }
         GAMAuthenticationTypeGoogle_externalReference.Load(gxTp_Name);
         return  ;
      }

      public void save( )
      {
         if ( GAMAuthenticationTypeGoogle_externalReference == null )
         {
            GAMAuthenticationTypeGoogle_externalReference = new Artech.Security.GAMAuthenticationTypeGoogle(context);
         }
         GAMAuthenticationTypeGoogle_externalReference.Save();
         return  ;
      }

      public void delete( )
      {
         if ( GAMAuthenticationTypeGoogle_externalReference == null )
         {
            GAMAuthenticationTypeGoogle_externalReference = new Artech.Security.GAMAuthenticationTypeGoogle(context);
         }
         GAMAuthenticationTypeGoogle_externalReference.Delete();
         return  ;
      }

      public bool success( )
      {
         bool returnsuccess ;
         if ( GAMAuthenticationTypeGoogle_externalReference == null )
         {
            GAMAuthenticationTypeGoogle_externalReference = new Artech.Security.GAMAuthenticationTypeGoogle(context);
         }
         returnsuccess = false;
         returnsuccess = (bool)(GAMAuthenticationTypeGoogle_externalReference.Success());
         return returnsuccess ;
      }

      public bool fail( )
      {
         bool returnfail ;
         if ( GAMAuthenticationTypeGoogle_externalReference == null )
         {
            GAMAuthenticationTypeGoogle_externalReference = new Artech.Security.GAMAuthenticationTypeGoogle(context);
         }
         returnfail = false;
         returnfail = (bool)(GAMAuthenticationTypeGoogle_externalReference.Fail());
         return returnfail ;
      }

      public IGxCollection geterrors( )
      {
         IGxCollection returngeterrors ;
         if ( GAMAuthenticationTypeGoogle_externalReference == null )
         {
            GAMAuthenticationTypeGoogle_externalReference = new Artech.Security.GAMAuthenticationTypeGoogle(context);
         }
         returngeterrors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         System.Collections.Generic.List<Artech.Security.GAMError> externalParm0 ;
         externalParm0 = GAMAuthenticationTypeGoogle_externalReference.GetErrors();
         returngeterrors.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMError>), externalParm0);
         return returngeterrors ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMAuthenticationTypeGoogle_externalReference == null )
         {
            GAMAuthenticationTypeGoogle_externalReference = new Artech.Security.GAMAuthenticationTypeGoogle(context);
         }
         returntostring = "";
         returntostring = (String)(GAMAuthenticationTypeGoogle_externalReference.ToString());
         return returntostring ;
      }

      public String gxTpr_Name
      {
         get {
            if ( GAMAuthenticationTypeGoogle_externalReference == null )
            {
               GAMAuthenticationTypeGoogle_externalReference = new Artech.Security.GAMAuthenticationTypeGoogle(context);
            }
            return GAMAuthenticationTypeGoogle_externalReference.Name ;
         }

         set {
            if ( GAMAuthenticationTypeGoogle_externalReference == null )
            {
               GAMAuthenticationTypeGoogle_externalReference = new Artech.Security.GAMAuthenticationTypeGoogle(context);
            }
            GAMAuthenticationTypeGoogle_externalReference.Name = value;
         }

      }

      public bool gxTpr_Isenable
      {
         get {
            if ( GAMAuthenticationTypeGoogle_externalReference == null )
            {
               GAMAuthenticationTypeGoogle_externalReference = new Artech.Security.GAMAuthenticationTypeGoogle(context);
            }
            return GAMAuthenticationTypeGoogle_externalReference.IsEnable ;
         }

         set {
            if ( GAMAuthenticationTypeGoogle_externalReference == null )
            {
               GAMAuthenticationTypeGoogle_externalReference = new Artech.Security.GAMAuthenticationTypeGoogle(context);
            }
            GAMAuthenticationTypeGoogle_externalReference.IsEnable = value;
         }

      }

      public String gxTpr_Description
      {
         get {
            if ( GAMAuthenticationTypeGoogle_externalReference == null )
            {
               GAMAuthenticationTypeGoogle_externalReference = new Artech.Security.GAMAuthenticationTypeGoogle(context);
            }
            return GAMAuthenticationTypeGoogle_externalReference.Description ;
         }

         set {
            if ( GAMAuthenticationTypeGoogle_externalReference == null )
            {
               GAMAuthenticationTypeGoogle_externalReference = new Artech.Security.GAMAuthenticationTypeGoogle(context);
            }
            GAMAuthenticationTypeGoogle_externalReference.Description = value;
         }

      }

      public SdtGAMAuthenticationGoogle gxTpr_Google
      {
         get {
            if ( GAMAuthenticationTypeGoogle_externalReference == null )
            {
               GAMAuthenticationTypeGoogle_externalReference = new Artech.Security.GAMAuthenticationTypeGoogle(context);
            }
            SdtGAMAuthenticationGoogle intValue ;
            intValue = new SdtGAMAuthenticationGoogle(context);
            Artech.Security.GAMAuthenticationGoogle externalParm0 ;
            externalParm0 = GAMAuthenticationTypeGoogle_externalReference.Google;
            intValue.ExternalInstance = externalParm0;
            return intValue ;
         }

         set {
            if ( GAMAuthenticationTypeGoogle_externalReference == null )
            {
               GAMAuthenticationTypeGoogle_externalReference = new Artech.Security.GAMAuthenticationTypeGoogle(context);
            }
            SdtGAMAuthenticationGoogle intValue ;
            Artech.Security.GAMAuthenticationGoogle externalParm1 ;
            intValue = value;
            externalParm1 = (Artech.Security.GAMAuthenticationGoogle)(intValue.ExternalInstance);
            GAMAuthenticationTypeGoogle_externalReference.Google = externalParm1;
         }

      }

      public DateTime gxTpr_Datecreated
      {
         get {
            if ( GAMAuthenticationTypeGoogle_externalReference == null )
            {
               GAMAuthenticationTypeGoogle_externalReference = new Artech.Security.GAMAuthenticationTypeGoogle(context);
            }
            return GAMAuthenticationTypeGoogle_externalReference.DateCreated ;
         }

         set {
            if ( GAMAuthenticationTypeGoogle_externalReference == null )
            {
               GAMAuthenticationTypeGoogle_externalReference = new Artech.Security.GAMAuthenticationTypeGoogle(context);
            }
            GAMAuthenticationTypeGoogle_externalReference.DateCreated = value;
         }

      }

      public String gxTpr_Usercreated
      {
         get {
            if ( GAMAuthenticationTypeGoogle_externalReference == null )
            {
               GAMAuthenticationTypeGoogle_externalReference = new Artech.Security.GAMAuthenticationTypeGoogle(context);
            }
            return GAMAuthenticationTypeGoogle_externalReference.UserCreated ;
         }

         set {
            if ( GAMAuthenticationTypeGoogle_externalReference == null )
            {
               GAMAuthenticationTypeGoogle_externalReference = new Artech.Security.GAMAuthenticationTypeGoogle(context);
            }
            GAMAuthenticationTypeGoogle_externalReference.UserCreated = value;
         }

      }

      public DateTime gxTpr_Dateupdated
      {
         get {
            if ( GAMAuthenticationTypeGoogle_externalReference == null )
            {
               GAMAuthenticationTypeGoogle_externalReference = new Artech.Security.GAMAuthenticationTypeGoogle(context);
            }
            return GAMAuthenticationTypeGoogle_externalReference.DateUpdated ;
         }

         set {
            if ( GAMAuthenticationTypeGoogle_externalReference == null )
            {
               GAMAuthenticationTypeGoogle_externalReference = new Artech.Security.GAMAuthenticationTypeGoogle(context);
            }
            GAMAuthenticationTypeGoogle_externalReference.DateUpdated = value;
         }

      }

      public String gxTpr_Userupdated
      {
         get {
            if ( GAMAuthenticationTypeGoogle_externalReference == null )
            {
               GAMAuthenticationTypeGoogle_externalReference = new Artech.Security.GAMAuthenticationTypeGoogle(context);
            }
            return GAMAuthenticationTypeGoogle_externalReference.UserUpdated ;
         }

         set {
            if ( GAMAuthenticationTypeGoogle_externalReference == null )
            {
               GAMAuthenticationTypeGoogle_externalReference = new Artech.Security.GAMAuthenticationTypeGoogle(context);
            }
            GAMAuthenticationTypeGoogle_externalReference.UserUpdated = value;
         }

      }

      public IGxCollection gxTpr_Descriptions
      {
         get {
            if ( GAMAuthenticationTypeGoogle_externalReference == null )
            {
               GAMAuthenticationTypeGoogle_externalReference = new Artech.Security.GAMAuthenticationTypeGoogle(context);
            }
            IGxCollection intValue ;
            intValue = new GxExternalCollection( context, "SdtGAMDescription", "GeneXus.Programs");
            System.Collections.Generic.List<Artech.Security.GAMDescription> externalParm2 ;
            externalParm2 = GAMAuthenticationTypeGoogle_externalReference.Descriptions;
            intValue.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMDescription>), externalParm2);
            return intValue ;
         }

         set {
            if ( GAMAuthenticationTypeGoogle_externalReference == null )
            {
               GAMAuthenticationTypeGoogle_externalReference = new Artech.Security.GAMAuthenticationTypeGoogle(context);
            }
            IGxCollection intValue ;
            System.Collections.Generic.List<Artech.Security.GAMDescription> externalParm3 ;
            intValue = value;
            externalParm3 = (System.Collections.Generic.List<Artech.Security.GAMDescription>)CollectionUtils.ConvertToExternal( typeof(System.Collections.Generic.List<Artech.Security.GAMDescription>), intValue.ExternalInstance);
            GAMAuthenticationTypeGoogle_externalReference.Descriptions = externalParm3;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMAuthenticationTypeGoogle_externalReference == null )
            {
               GAMAuthenticationTypeGoogle_externalReference = new Artech.Security.GAMAuthenticationTypeGoogle(context);
            }
            return GAMAuthenticationTypeGoogle_externalReference ;
         }

         set {
            GAMAuthenticationTypeGoogle_externalReference = (Artech.Security.GAMAuthenticationTypeGoogle)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMAuthenticationTypeGoogle GAMAuthenticationTypeGoogle_externalReference=null ;
   }

}
