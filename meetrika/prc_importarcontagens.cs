/*
               File: PRC_ImportarContagens
        Description: Stub for PRC_ImportarContagens
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:14:35.39
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_importarcontagens : GXProcedure
   {
      public prc_importarcontagens( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public prc_importarcontagens( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_OQImportar ,
                           int aP1_ContagemResultado_ContratadaCod ,
                           int aP2_Servico ,
                           String aP3_Arquivo ,
                           String aP4_Aba ,
                           short aP5_ColDmnn ,
                           DateTime aP6_DataCnt ,
                           short aP7_ColDataCntn ,
                           short aP8_PraLinha ,
                           short aP9_ColContadorFMn ,
                           int aP10_ContagemResultado_ContadorFMCod ,
                           short aP11_ColPFBFSn ,
                           short aP12_ColPFLFSn ,
                           short aP13_ColPFBFMn ,
                           short aP14_ColPFLFMn ,
                           short aP15_ColParecern ,
                           bool aP16_Final ,
                           String aP17_FileName )
      {
         this.AV2OQImportar = aP0_OQImportar;
         this.AV3ContagemResultado_ContratadaCod = aP1_ContagemResultado_ContratadaCod;
         this.AV4Servico = aP2_Servico;
         this.AV5Arquivo = aP3_Arquivo;
         this.AV6Aba = aP4_Aba;
         this.AV7ColDmnn = aP5_ColDmnn;
         this.AV8DataCnt = aP6_DataCnt;
         this.AV9ColDataCntn = aP7_ColDataCntn;
         this.AV10PraLinha = aP8_PraLinha;
         this.AV11ColContadorFMn = aP9_ColContadorFMn;
         this.AV12ContagemResultado_ContadorFMCod = aP10_ContagemResultado_ContadorFMCod;
         this.AV13ColPFBFSn = aP11_ColPFBFSn;
         this.AV14ColPFLFSn = aP12_ColPFLFSn;
         this.AV15ColPFBFMn = aP13_ColPFBFMn;
         this.AV16ColPFLFMn = aP14_ColPFLFMn;
         this.AV17ColParecern = aP15_ColParecern;
         this.AV18Final = aP16_Final;
         this.AV19FileName = aP17_FileName;
         initialize();
         executePrivate();
      }

      public void executeSubmit( String aP0_OQImportar ,
                                 int aP1_ContagemResultado_ContratadaCod ,
                                 int aP2_Servico ,
                                 String aP3_Arquivo ,
                                 String aP4_Aba ,
                                 short aP5_ColDmnn ,
                                 DateTime aP6_DataCnt ,
                                 short aP7_ColDataCntn ,
                                 short aP8_PraLinha ,
                                 short aP9_ColContadorFMn ,
                                 int aP10_ContagemResultado_ContadorFMCod ,
                                 short aP11_ColPFBFSn ,
                                 short aP12_ColPFLFSn ,
                                 short aP13_ColPFBFMn ,
                                 short aP14_ColPFLFMn ,
                                 short aP15_ColParecern ,
                                 bool aP16_Final ,
                                 String aP17_FileName )
      {
         prc_importarcontagens objprc_importarcontagens;
         objprc_importarcontagens = new prc_importarcontagens();
         objprc_importarcontagens.AV2OQImportar = aP0_OQImportar;
         objprc_importarcontagens.AV3ContagemResultado_ContratadaCod = aP1_ContagemResultado_ContratadaCod;
         objprc_importarcontagens.AV4Servico = aP2_Servico;
         objprc_importarcontagens.AV5Arquivo = aP3_Arquivo;
         objprc_importarcontagens.AV6Aba = aP4_Aba;
         objprc_importarcontagens.AV7ColDmnn = aP5_ColDmnn;
         objprc_importarcontagens.AV8DataCnt = aP6_DataCnt;
         objprc_importarcontagens.AV9ColDataCntn = aP7_ColDataCntn;
         objprc_importarcontagens.AV10PraLinha = aP8_PraLinha;
         objprc_importarcontagens.AV11ColContadorFMn = aP9_ColContadorFMn;
         objprc_importarcontagens.AV12ContagemResultado_ContadorFMCod = aP10_ContagemResultado_ContadorFMCod;
         objprc_importarcontagens.AV13ColPFBFSn = aP11_ColPFBFSn;
         objprc_importarcontagens.AV14ColPFLFSn = aP12_ColPFLFSn;
         objprc_importarcontagens.AV15ColPFBFMn = aP13_ColPFBFMn;
         objprc_importarcontagens.AV16ColPFLFMn = aP14_ColPFLFMn;
         objprc_importarcontagens.AV17ColParecern = aP15_ColParecern;
         objprc_importarcontagens.AV18Final = aP16_Final;
         objprc_importarcontagens.AV19FileName = aP17_FileName;
         objprc_importarcontagens.context.SetSubmitInitialConfig(context);
         objprc_importarcontagens.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_importarcontagens);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_importarcontagens)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         args = new Object[] {(String)AV2OQImportar,(int)AV3ContagemResultado_ContratadaCod,(int)AV4Servico,(String)AV5Arquivo,(String)AV6Aba,(short)AV7ColDmnn,(DateTime)AV8DataCnt,(short)AV9ColDataCntn,(short)AV10PraLinha,(short)AV11ColContadorFMn,(int)AV12ContagemResultado_ContadorFMCod,(short)AV13ColPFBFSn,(short)AV14ColPFLFSn,(short)AV15ColPFBFMn,(short)AV16ColPFLFMn,(short)AV17ColParecern,(bool)AV18Final,(String)AV19FileName} ;
         ClassLoader.Execute("aprc_importarcontagens","GeneXus.Programs.aprc_importarcontagens", new Object[] {context }, "execute", args);
         if ( ( args != null ) && ( args.Length == 18 ) )
         {
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV7ColDmnn ;
      private short AV9ColDataCntn ;
      private short AV10PraLinha ;
      private short AV11ColContadorFMn ;
      private short AV13ColPFBFSn ;
      private short AV14ColPFLFSn ;
      private short AV15ColPFBFMn ;
      private short AV16ColPFLFMn ;
      private short AV17ColParecern ;
      private int AV3ContagemResultado_ContratadaCod ;
      private int AV4Servico ;
      private int AV12ContagemResultado_ContadorFMCod ;
      private String AV2OQImportar ;
      private String AV5Arquivo ;
      private String AV6Aba ;
      private String AV19FileName ;
      private DateTime AV8DataCnt ;
      private bool AV18Final ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private Object[] args ;
   }

}
