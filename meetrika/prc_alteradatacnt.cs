/*
               File: PRC_AlteraDataCnt
        Description: Altera data da contagem
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:8.4
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_alteradatacnt : GXProcedure
   {
      public prc_alteradatacnt( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_alteradatacnt( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo ,
                           DateTime aP1_ContagemResultado_DataCnt ,
                           String aP2_ContagemResultado_HoraCnt ,
                           DateTime aP3_Date )
      {
         this.AV13ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV14ContagemResultado_DataCnt = aP1_ContagemResultado_DataCnt;
         this.AV15ContagemResultado_HoraCnt = aP2_ContagemResultado_HoraCnt;
         this.AV12Date = aP3_Date;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_ContagemResultado_Codigo ,
                                 DateTime aP1_ContagemResultado_DataCnt ,
                                 String aP2_ContagemResultado_HoraCnt ,
                                 DateTime aP3_Date )
      {
         prc_alteradatacnt objprc_alteradatacnt;
         objprc_alteradatacnt = new prc_alteradatacnt();
         objprc_alteradatacnt.AV13ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_alteradatacnt.AV14ContagemResultado_DataCnt = aP1_ContagemResultado_DataCnt;
         objprc_alteradatacnt.AV15ContagemResultado_HoraCnt = aP2_ContagemResultado_HoraCnt;
         objprc_alteradatacnt.AV12Date = aP3_Date;
         objprc_alteradatacnt.context.SetSubmitInitialConfig(context);
         objprc_alteradatacnt.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_alteradatacnt);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_alteradatacnt)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P003Y2 */
         pr_default.execute(0, new Object[] {AV13ContagemResultado_Codigo, AV14ContagemResultado_DataCnt, AV15ContagemResultado_HoraCnt});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P003Y2_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P003Y2_n1553ContagemResultado_CntSrvCod[0];
            A1756ContagemResultado_NvlCnt = P003Y2_A1756ContagemResultado_NvlCnt[0];
            n1756ContagemResultado_NvlCnt = P003Y2_n1756ContagemResultado_NvlCnt[0];
            A901ContagemResultadoContagens_Prazo = P003Y2_A901ContagemResultadoContagens_Prazo[0];
            n901ContagemResultadoContagens_Prazo = P003Y2_n901ContagemResultadoContagens_Prazo[0];
            A854ContagemResultado_TipoPla = P003Y2_A854ContagemResultado_TipoPla[0];
            n854ContagemResultado_TipoPla = P003Y2_n854ContagemResultado_TipoPla[0];
            A853ContagemResultado_NomePla = P003Y2_A853ContagemResultado_NomePla[0];
            n853ContagemResultado_NomePla = P003Y2_n853ContagemResultado_NomePla[0];
            A833ContagemResultado_CstUntPrd = P003Y2_A833ContagemResultado_CstUntPrd[0];
            n833ContagemResultado_CstUntPrd = P003Y2_n833ContagemResultado_CstUntPrd[0];
            A800ContagemResultado_Deflator = P003Y2_A800ContagemResultado_Deflator[0];
            n800ContagemResultado_Deflator = P003Y2_n800ContagemResultado_Deflator[0];
            A517ContagemResultado_Ultima = P003Y2_A517ContagemResultado_Ultima[0];
            A482ContagemResultadoContagens_Esforco = P003Y2_A482ContagemResultadoContagens_Esforco[0];
            A483ContagemResultado_StatusCnt = P003Y2_A483ContagemResultado_StatusCnt[0];
            A469ContagemResultado_NaoCnfCntCod = P003Y2_A469ContagemResultado_NaoCnfCntCod[0];
            n469ContagemResultado_NaoCnfCntCod = P003Y2_n469ContagemResultado_NaoCnfCntCod[0];
            A470ContagemResultado_ContadorFMCod = P003Y2_A470ContagemResultado_ContadorFMCod[0];
            A463ContagemResultado_ParecerTcn = P003Y2_A463ContagemResultado_ParecerTcn[0];
            n463ContagemResultado_ParecerTcn = P003Y2_n463ContagemResultado_ParecerTcn[0];
            A462ContagemResultado_Divergencia = P003Y2_A462ContagemResultado_Divergencia[0];
            A461ContagemResultado_PFLFM = P003Y2_A461ContagemResultado_PFLFM[0];
            n461ContagemResultado_PFLFM = P003Y2_n461ContagemResultado_PFLFM[0];
            A460ContagemResultado_PFBFM = P003Y2_A460ContagemResultado_PFBFM[0];
            n460ContagemResultado_PFBFM = P003Y2_n460ContagemResultado_PFBFM[0];
            A459ContagemResultado_PFLFS = P003Y2_A459ContagemResultado_PFLFS[0];
            n459ContagemResultado_PFLFS = P003Y2_n459ContagemResultado_PFLFS[0];
            A458ContagemResultado_PFBFS = P003Y2_A458ContagemResultado_PFBFS[0];
            n458ContagemResultado_PFBFS = P003Y2_n458ContagemResultado_PFBFS[0];
            A481ContagemResultado_TimeCnt = P003Y2_A481ContagemResultado_TimeCnt[0];
            n481ContagemResultado_TimeCnt = P003Y2_n481ContagemResultado_TimeCnt[0];
            A511ContagemResultado_HoraCnt = P003Y2_A511ContagemResultado_HoraCnt[0];
            A473ContagemResultado_DataCnt = P003Y2_A473ContagemResultado_DataCnt[0];
            A456ContagemResultado_Codigo = P003Y2_A456ContagemResultado_Codigo[0];
            A1611ContagemResultado_PrzTpDias = P003Y2_A1611ContagemResultado_PrzTpDias[0];
            n1611ContagemResultado_PrzTpDias = P003Y2_n1611ContagemResultado_PrzTpDias[0];
            A852ContagemResultado_Planilha = P003Y2_A852ContagemResultado_Planilha[0];
            n852ContagemResultado_Planilha = P003Y2_n852ContagemResultado_Planilha[0];
            A1553ContagemResultado_CntSrvCod = P003Y2_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P003Y2_n1553ContagemResultado_CntSrvCod[0];
            A1611ContagemResultado_PrzTpDias = P003Y2_A1611ContagemResultado_PrzTpDias[0];
            n1611ContagemResultado_PrzTpDias = P003Y2_n1611ContagemResultado_PrzTpDias[0];
            W473ContagemResultado_DataCnt = A473ContagemResultado_DataCnt;
            W511ContagemResultado_HoraCnt = A511ContagemResultado_HoraCnt;
            /* Using cursor P003Y3 */
            pr_default.execute(1, new Object[] {A456ContagemResultado_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A1404ContagemResultadoExecucao_OSCod = P003Y3_A1404ContagemResultadoExecucao_OSCod[0];
               A1407ContagemResultadoExecucao_Fim = P003Y3_A1407ContagemResultadoExecucao_Fim[0];
               n1407ContagemResultadoExecucao_Fim = P003Y3_n1407ContagemResultadoExecucao_Fim[0];
               A1406ContagemResultadoExecucao_Inicio = P003Y3_A1406ContagemResultadoExecucao_Inicio[0];
               A1410ContagemResultadoExecucao_Dias = P003Y3_A1410ContagemResultadoExecucao_Dias[0];
               n1410ContagemResultadoExecucao_Dias = P003Y3_n1410ContagemResultadoExecucao_Dias[0];
               A1405ContagemResultadoExecucao_Codigo = P003Y3_A1405ContagemResultadoExecucao_Codigo[0];
               if ( DateTimeUtil.ResetTime( A1407ContagemResultadoExecucao_Fim) == A473ContagemResultado_DataCnt )
               {
                  if ( (Convert.ToDecimal( DateTimeUtil.Hour( A1407ContagemResultadoExecucao_Fim) ) == NumberUtil.Val( StringUtil.Substring( A511ContagemResultado_HoraCnt, 1, 2), ".") ) )
                  {
                     if ( (Convert.ToDecimal( DateTimeUtil.Minute( A1407ContagemResultadoExecucao_Fim) ) == NumberUtil.Val( StringUtil.Substring( A511ContagemResultado_HoraCnt, 4, 2), ".") ) )
                     {
                        A1407ContagemResultadoExecucao_Fim = DateTimeUtil.ResetTime( AV12Date ) ;
                        n1407ContagemResultadoExecucao_Fim = false;
                        GXt_int1 = A1410ContagemResultadoExecucao_Dias;
                        new prc_diasuteisentre(context ).execute(  DateTimeUtil.ResetTime( A1406ContagemResultadoExecucao_Inicio),  AV12Date,  A1611ContagemResultado_PrzTpDias, out  GXt_int1) ;
                        A1410ContagemResultadoExecucao_Dias = GXt_int1;
                        n1410ContagemResultadoExecucao_Dias = false;
                        /* Exit For each command. Update data (if necessary), close cursors & exit. */
                        /* Using cursor P003Y4 */
                        pr_default.execute(2, new Object[] {n1407ContagemResultadoExecucao_Fim, A1407ContagemResultadoExecucao_Fim, n1410ContagemResultadoExecucao_Dias, A1410ContagemResultadoExecucao_Dias, A1405ContagemResultadoExecucao_Codigo});
                        pr_default.close(2);
                        dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoExecucao") ;
                        if (true) break;
                        /* Using cursor P003Y5 */
                        pr_default.execute(3, new Object[] {n1407ContagemResultadoExecucao_Fim, A1407ContagemResultadoExecucao_Fim, n1410ContagemResultadoExecucao_Dias, A1410ContagemResultadoExecucao_Dias, A1405ContagemResultadoExecucao_Codigo});
                        pr_default.close(3);
                        dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoExecucao") ;
                     }
                  }
               }
               pr_default.readNext(1);
            }
            pr_default.close(1);
            /*
               INSERT RECORD ON TABLE ContagemResultadoContagens

            */
            W473ContagemResultado_DataCnt = A473ContagemResultado_DataCnt;
            W511ContagemResultado_HoraCnt = A511ContagemResultado_HoraCnt;
            A473ContagemResultado_DataCnt = AV12Date;
            A511ContagemResultado_HoraCnt = "24:00";
            /* Using cursor P003Y6 */
            A853ContagemResultado_NomePla = FileUtil.GetFileName( A852ContagemResultado_Planilha);
            n853ContagemResultado_NomePla = false;
            A854ContagemResultado_TipoPla = FileUtil.GetFileType( A852ContagemResultado_Planilha);
            n854ContagemResultado_TipoPla = false;
            pr_default.execute(4, new Object[] {A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt, n481ContagemResultado_TimeCnt, A481ContagemResultado_TimeCnt, n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, n460ContagemResultado_PFBFM, A460ContagemResultado_PFBFM, n461ContagemResultado_PFLFM, A461ContagemResultado_PFLFM, A462ContagemResultado_Divergencia, n463ContagemResultado_ParecerTcn, A463ContagemResultado_ParecerTcn, A470ContagemResultado_ContadorFMCod, n469ContagemResultado_NaoCnfCntCod, A469ContagemResultado_NaoCnfCntCod, A483ContagemResultado_StatusCnt, A482ContagemResultadoContagens_Esforco, A517ContagemResultado_Ultima, n800ContagemResultado_Deflator, A800ContagemResultado_Deflator, n833ContagemResultado_CstUntPrd, A833ContagemResultado_CstUntPrd, n852ContagemResultado_Planilha, A852ContagemResultado_Planilha, n853ContagemResultado_NomePla, A853ContagemResultado_NomePla, n854ContagemResultado_TipoPla, A854ContagemResultado_TipoPla, n901ContagemResultadoContagens_Prazo, A901ContagemResultadoContagens_Prazo, n1756ContagemResultado_NvlCnt, A1756ContagemResultado_NvlCnt});
            pr_default.close(4);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
            if ( (pr_default.getStatus(4) == 1) )
            {
               context.Gx_err = 1;
               Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
            }
            else
            {
               context.Gx_err = 0;
               Gx_emsg = "";
            }
            A473ContagemResultado_DataCnt = W473ContagemResultado_DataCnt;
            A511ContagemResultado_HoraCnt = W511ContagemResultado_HoraCnt;
            /* End Insert */
            /* Using cursor P003Y7 */
            pr_default.execute(5, new Object[] {A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
            pr_default.close(5);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
            A473ContagemResultado_DataCnt = W473ContagemResultado_DataCnt;
            A511ContagemResultado_HoraCnt = W511ContagemResultado_HoraCnt;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         /* Optimized UPDATE. */
         /* Using cursor P003Y8 */
         pr_default.execute(6, new Object[] {AV13ContagemResultado_Codigo});
         pr_default.close(6);
         dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoErro") ;
         dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoErro") ;
         /* End optimized UPDATE. */
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_AlteraDataCnt");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P003Y2_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P003Y2_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P003Y2_A1756ContagemResultado_NvlCnt = new short[1] ;
         P003Y2_n1756ContagemResultado_NvlCnt = new bool[] {false} ;
         P003Y2_A901ContagemResultadoContagens_Prazo = new DateTime[] {DateTime.MinValue} ;
         P003Y2_n901ContagemResultadoContagens_Prazo = new bool[] {false} ;
         P003Y2_A854ContagemResultado_TipoPla = new String[] {""} ;
         P003Y2_n854ContagemResultado_TipoPla = new bool[] {false} ;
         P003Y2_A853ContagemResultado_NomePla = new String[] {""} ;
         P003Y2_n853ContagemResultado_NomePla = new bool[] {false} ;
         P003Y2_A833ContagemResultado_CstUntPrd = new decimal[1] ;
         P003Y2_n833ContagemResultado_CstUntPrd = new bool[] {false} ;
         P003Y2_A800ContagemResultado_Deflator = new decimal[1] ;
         P003Y2_n800ContagemResultado_Deflator = new bool[] {false} ;
         P003Y2_A517ContagemResultado_Ultima = new bool[] {false} ;
         P003Y2_A482ContagemResultadoContagens_Esforco = new short[1] ;
         P003Y2_A483ContagemResultado_StatusCnt = new short[1] ;
         P003Y2_A469ContagemResultado_NaoCnfCntCod = new int[1] ;
         P003Y2_n469ContagemResultado_NaoCnfCntCod = new bool[] {false} ;
         P003Y2_A470ContagemResultado_ContadorFMCod = new int[1] ;
         P003Y2_A463ContagemResultado_ParecerTcn = new String[] {""} ;
         P003Y2_n463ContagemResultado_ParecerTcn = new bool[] {false} ;
         P003Y2_A462ContagemResultado_Divergencia = new decimal[1] ;
         P003Y2_A461ContagemResultado_PFLFM = new decimal[1] ;
         P003Y2_n461ContagemResultado_PFLFM = new bool[] {false} ;
         P003Y2_A460ContagemResultado_PFBFM = new decimal[1] ;
         P003Y2_n460ContagemResultado_PFBFM = new bool[] {false} ;
         P003Y2_A459ContagemResultado_PFLFS = new decimal[1] ;
         P003Y2_n459ContagemResultado_PFLFS = new bool[] {false} ;
         P003Y2_A458ContagemResultado_PFBFS = new decimal[1] ;
         P003Y2_n458ContagemResultado_PFBFS = new bool[] {false} ;
         P003Y2_A481ContagemResultado_TimeCnt = new DateTime[] {DateTime.MinValue} ;
         P003Y2_n481ContagemResultado_TimeCnt = new bool[] {false} ;
         P003Y2_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P003Y2_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P003Y2_A456ContagemResultado_Codigo = new int[1] ;
         P003Y2_A1611ContagemResultado_PrzTpDias = new String[] {""} ;
         P003Y2_n1611ContagemResultado_PrzTpDias = new bool[] {false} ;
         P003Y2_A852ContagemResultado_Planilha = new String[] {""} ;
         P003Y2_n852ContagemResultado_Planilha = new bool[] {false} ;
         A901ContagemResultadoContagens_Prazo = (DateTime)(DateTime.MinValue);
         A854ContagemResultado_TipoPla = "";
         A853ContagemResultado_NomePla = "";
         A463ContagemResultado_ParecerTcn = "";
         A481ContagemResultado_TimeCnt = (DateTime)(DateTime.MinValue);
         A511ContagemResultado_HoraCnt = "";
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A1611ContagemResultado_PrzTpDias = "";
         A852ContagemResultado_Planilha = "";
         W473ContagemResultado_DataCnt = DateTime.MinValue;
         W511ContagemResultado_HoraCnt = "";
         P003Y3_A1404ContagemResultadoExecucao_OSCod = new int[1] ;
         P003Y3_A1407ContagemResultadoExecucao_Fim = new DateTime[] {DateTime.MinValue} ;
         P003Y3_n1407ContagemResultadoExecucao_Fim = new bool[] {false} ;
         P003Y3_A1406ContagemResultadoExecucao_Inicio = new DateTime[] {DateTime.MinValue} ;
         P003Y3_A1410ContagemResultadoExecucao_Dias = new short[1] ;
         P003Y3_n1410ContagemResultadoExecucao_Dias = new bool[] {false} ;
         P003Y3_A1405ContagemResultadoExecucao_Codigo = new int[1] ;
         A1407ContagemResultadoExecucao_Fim = (DateTime)(DateTime.MinValue);
         A1406ContagemResultadoExecucao_Inicio = (DateTime)(DateTime.MinValue);
         Gx_emsg = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_alteradatacnt__default(),
            new Object[][] {
                new Object[] {
               P003Y2_A1553ContagemResultado_CntSrvCod, P003Y2_n1553ContagemResultado_CntSrvCod, P003Y2_A1756ContagemResultado_NvlCnt, P003Y2_n1756ContagemResultado_NvlCnt, P003Y2_A901ContagemResultadoContagens_Prazo, P003Y2_n901ContagemResultadoContagens_Prazo, P003Y2_A854ContagemResultado_TipoPla, P003Y2_n854ContagemResultado_TipoPla, P003Y2_A853ContagemResultado_NomePla, P003Y2_n853ContagemResultado_NomePla,
               P003Y2_A833ContagemResultado_CstUntPrd, P003Y2_n833ContagemResultado_CstUntPrd, P003Y2_A800ContagemResultado_Deflator, P003Y2_n800ContagemResultado_Deflator, P003Y2_A517ContagemResultado_Ultima, P003Y2_A482ContagemResultadoContagens_Esforco, P003Y2_A483ContagemResultado_StatusCnt, P003Y2_A469ContagemResultado_NaoCnfCntCod, P003Y2_n469ContagemResultado_NaoCnfCntCod, P003Y2_A470ContagemResultado_ContadorFMCod,
               P003Y2_A463ContagemResultado_ParecerTcn, P003Y2_n463ContagemResultado_ParecerTcn, P003Y2_A462ContagemResultado_Divergencia, P003Y2_A461ContagemResultado_PFLFM, P003Y2_n461ContagemResultado_PFLFM, P003Y2_A460ContagemResultado_PFBFM, P003Y2_n460ContagemResultado_PFBFM, P003Y2_A459ContagemResultado_PFLFS, P003Y2_n459ContagemResultado_PFLFS, P003Y2_A458ContagemResultado_PFBFS,
               P003Y2_n458ContagemResultado_PFBFS, P003Y2_A481ContagemResultado_TimeCnt, P003Y2_n481ContagemResultado_TimeCnt, P003Y2_A511ContagemResultado_HoraCnt, P003Y2_A473ContagemResultado_DataCnt, P003Y2_A456ContagemResultado_Codigo, P003Y2_A1611ContagemResultado_PrzTpDias, P003Y2_n1611ContagemResultado_PrzTpDias, P003Y2_A852ContagemResultado_Planilha, P003Y2_n852ContagemResultado_Planilha
               }
               , new Object[] {
               P003Y3_A1404ContagemResultadoExecucao_OSCod, P003Y3_A1407ContagemResultadoExecucao_Fim, P003Y3_n1407ContagemResultadoExecucao_Fim, P003Y3_A1406ContagemResultadoExecucao_Inicio, P003Y3_A1410ContagemResultadoExecucao_Dias, P003Y3_n1410ContagemResultadoExecucao_Dias, P003Y3_A1405ContagemResultadoExecucao_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A1756ContagemResultado_NvlCnt ;
      private short A482ContagemResultadoContagens_Esforco ;
      private short A483ContagemResultado_StatusCnt ;
      private short A1410ContagemResultadoExecucao_Dias ;
      private short GXt_int1 ;
      private int AV13ContagemResultado_Codigo ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A469ContagemResultado_NaoCnfCntCod ;
      private int A470ContagemResultado_ContadorFMCod ;
      private int A456ContagemResultado_Codigo ;
      private int A1404ContagemResultadoExecucao_OSCod ;
      private int A1405ContagemResultadoExecucao_Codigo ;
      private int GX_INS72 ;
      private decimal A833ContagemResultado_CstUntPrd ;
      private decimal A800ContagemResultado_Deflator ;
      private decimal A462ContagemResultado_Divergencia ;
      private decimal A461ContagemResultado_PFLFM ;
      private decimal A460ContagemResultado_PFBFM ;
      private decimal A459ContagemResultado_PFLFS ;
      private decimal A458ContagemResultado_PFBFS ;
      private String AV15ContagemResultado_HoraCnt ;
      private String scmdbuf ;
      private String A854ContagemResultado_TipoPla ;
      private String A853ContagemResultado_NomePla ;
      private String A511ContagemResultado_HoraCnt ;
      private String A1611ContagemResultado_PrzTpDias ;
      private String W511ContagemResultado_HoraCnt ;
      private String Gx_emsg ;
      private DateTime A901ContagemResultadoContagens_Prazo ;
      private DateTime A481ContagemResultado_TimeCnt ;
      private DateTime A1407ContagemResultadoExecucao_Fim ;
      private DateTime A1406ContagemResultadoExecucao_Inicio ;
      private DateTime AV14ContagemResultado_DataCnt ;
      private DateTime AV12Date ;
      private DateTime A473ContagemResultado_DataCnt ;
      private DateTime W473ContagemResultado_DataCnt ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1756ContagemResultado_NvlCnt ;
      private bool n901ContagemResultadoContagens_Prazo ;
      private bool n854ContagemResultado_TipoPla ;
      private bool n853ContagemResultado_NomePla ;
      private bool n833ContagemResultado_CstUntPrd ;
      private bool n800ContagemResultado_Deflator ;
      private bool A517ContagemResultado_Ultima ;
      private bool n469ContagemResultado_NaoCnfCntCod ;
      private bool n463ContagemResultado_ParecerTcn ;
      private bool n461ContagemResultado_PFLFM ;
      private bool n460ContagemResultado_PFBFM ;
      private bool n459ContagemResultado_PFLFS ;
      private bool n458ContagemResultado_PFBFS ;
      private bool n481ContagemResultado_TimeCnt ;
      private bool n1611ContagemResultado_PrzTpDias ;
      private bool n852ContagemResultado_Planilha ;
      private bool n1407ContagemResultadoExecucao_Fim ;
      private bool n1410ContagemResultadoExecucao_Dias ;
      private String A463ContagemResultado_ParecerTcn ;
      private String A852ContagemResultado_Planilha ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P003Y2_A1553ContagemResultado_CntSrvCod ;
      private bool[] P003Y2_n1553ContagemResultado_CntSrvCod ;
      private short[] P003Y2_A1756ContagemResultado_NvlCnt ;
      private bool[] P003Y2_n1756ContagemResultado_NvlCnt ;
      private DateTime[] P003Y2_A901ContagemResultadoContagens_Prazo ;
      private bool[] P003Y2_n901ContagemResultadoContagens_Prazo ;
      private String[] P003Y2_A854ContagemResultado_TipoPla ;
      private bool[] P003Y2_n854ContagemResultado_TipoPla ;
      private String[] P003Y2_A853ContagemResultado_NomePla ;
      private bool[] P003Y2_n853ContagemResultado_NomePla ;
      private decimal[] P003Y2_A833ContagemResultado_CstUntPrd ;
      private bool[] P003Y2_n833ContagemResultado_CstUntPrd ;
      private decimal[] P003Y2_A800ContagemResultado_Deflator ;
      private bool[] P003Y2_n800ContagemResultado_Deflator ;
      private bool[] P003Y2_A517ContagemResultado_Ultima ;
      private short[] P003Y2_A482ContagemResultadoContagens_Esforco ;
      private short[] P003Y2_A483ContagemResultado_StatusCnt ;
      private int[] P003Y2_A469ContagemResultado_NaoCnfCntCod ;
      private bool[] P003Y2_n469ContagemResultado_NaoCnfCntCod ;
      private int[] P003Y2_A470ContagemResultado_ContadorFMCod ;
      private String[] P003Y2_A463ContagemResultado_ParecerTcn ;
      private bool[] P003Y2_n463ContagemResultado_ParecerTcn ;
      private decimal[] P003Y2_A462ContagemResultado_Divergencia ;
      private decimal[] P003Y2_A461ContagemResultado_PFLFM ;
      private bool[] P003Y2_n461ContagemResultado_PFLFM ;
      private decimal[] P003Y2_A460ContagemResultado_PFBFM ;
      private bool[] P003Y2_n460ContagemResultado_PFBFM ;
      private decimal[] P003Y2_A459ContagemResultado_PFLFS ;
      private bool[] P003Y2_n459ContagemResultado_PFLFS ;
      private decimal[] P003Y2_A458ContagemResultado_PFBFS ;
      private bool[] P003Y2_n458ContagemResultado_PFBFS ;
      private DateTime[] P003Y2_A481ContagemResultado_TimeCnt ;
      private bool[] P003Y2_n481ContagemResultado_TimeCnt ;
      private String[] P003Y2_A511ContagemResultado_HoraCnt ;
      private DateTime[] P003Y2_A473ContagemResultado_DataCnt ;
      private int[] P003Y2_A456ContagemResultado_Codigo ;
      private String[] P003Y2_A1611ContagemResultado_PrzTpDias ;
      private bool[] P003Y2_n1611ContagemResultado_PrzTpDias ;
      private String[] P003Y2_A852ContagemResultado_Planilha ;
      private bool[] P003Y2_n852ContagemResultado_Planilha ;
      private int[] P003Y3_A1404ContagemResultadoExecucao_OSCod ;
      private DateTime[] P003Y3_A1407ContagemResultadoExecucao_Fim ;
      private bool[] P003Y3_n1407ContagemResultadoExecucao_Fim ;
      private DateTime[] P003Y3_A1406ContagemResultadoExecucao_Inicio ;
      private short[] P003Y3_A1410ContagemResultadoExecucao_Dias ;
      private bool[] P003Y3_n1410ContagemResultadoExecucao_Dias ;
      private int[] P003Y3_A1405ContagemResultadoExecucao_Codigo ;
   }

   public class prc_alteradatacnt__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new UpdateCursor(def[3])
         ,new UpdateCursor(def[4])
         ,new UpdateCursor(def[5])
         ,new UpdateCursor(def[6])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP003Y2 ;
          prmP003Y2 = new Object[] {
          new Object[] {"@AV13ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV14ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV15ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP003Y3 ;
          prmP003Y3 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003Y4 ;
          prmP003Y4 = new Object[] {
          new Object[] {"@ContagemResultadoExecucao_Fim",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoExecucao_Dias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoExecucao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003Y5 ;
          prmP003Y5 = new Object[] {
          new Object[] {"@ContagemResultadoExecucao_Fim",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoExecucao_Dias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoExecucao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003Y6 ;
          prmP003Y6 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0} ,
          new Object[] {"@ContagemResultado_TimeCnt",SqlDbType.DateTime,0,5} ,
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFBFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_ParecerTcn",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_NaoCnfCntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultadoContagens_Esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Deflator",SqlDbType.Decimal,6,3} ,
          new Object[] {"@ContagemResultado_CstUntPrd",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_Planilha",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@ContagemResultado_NomePla",SqlDbType.Char,50,0} ,
          new Object[] {"@ContagemResultado_TipoPla",SqlDbType.Char,10,0} ,
          new Object[] {"@ContagemResultadoContagens_Prazo",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_NvlCnt",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmP003Y7 ;
          prmP003Y7 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP003Y8 ;
          prmP003Y8 = new Object[] {
          new Object[] {"@AV13ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P003Y2", "SELECT T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_NvlCnt], T1.[ContagemResultadoContagens_Prazo], T1.[ContagemResultado_TipoPla], T1.[ContagemResultado_NomePla], T1.[ContagemResultado_CstUntPrd], T1.[ContagemResultado_Deflator], T1.[ContagemResultado_Ultima], T1.[ContagemResultadoContagens_Esforco], T1.[ContagemResultado_StatusCnt], T1.[ContagemResultado_NaoCnfCntCod], T1.[ContagemResultado_ContadorFMCod], T1.[ContagemResultado_ParecerTcn], T1.[ContagemResultado_Divergencia], T1.[ContagemResultado_PFLFM], T1.[ContagemResultado_PFBFM], T1.[ContagemResultado_PFLFS], T1.[ContagemResultado_PFBFS], T1.[ContagemResultado_TimeCnt], T1.[ContagemResultado_HoraCnt], T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_Codigo], T3.[ContratoServicos_PrazoTpDias] AS ContagemResultado_PrzTpDias, T1.[ContagemResultado_Planilha] FROM (([ContagemResultadoContagens] T1 WITH (UPDLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) WHERE T1.[ContagemResultado_Codigo] = @AV13ContagemResultado_Codigo and T1.[ContagemResultado_DataCnt] = @AV14ContagemResultado_DataCnt and T1.[ContagemResultado_HoraCnt] = @AV15ContagemResultado_HoraCnt ORDER BY T1.[ContagemResultado_Codigo], T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_HoraCnt] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003Y2,1,0,true,true )
             ,new CursorDef("P003Y3", "SELECT [ContagemResultadoExecucao_OSCod], [ContagemResultadoExecucao_Fim], [ContagemResultadoExecucao_Inicio], [ContagemResultadoExecucao_Dias], [ContagemResultadoExecucao_Codigo] FROM [ContagemResultadoExecucao] WITH (UPDLOCK) WHERE [ContagemResultadoExecucao_OSCod] = @ContagemResultado_Codigo ORDER BY [ContagemResultadoExecucao_OSCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003Y3,1,0,true,false )
             ,new CursorDef("P003Y4", "UPDATE [ContagemResultadoExecucao] SET [ContagemResultadoExecucao_Fim]=@ContagemResultadoExecucao_Fim, [ContagemResultadoExecucao_Dias]=@ContagemResultadoExecucao_Dias  WHERE [ContagemResultadoExecucao_Codigo] = @ContagemResultadoExecucao_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP003Y4)
             ,new CursorDef("P003Y5", "UPDATE [ContagemResultadoExecucao] SET [ContagemResultadoExecucao_Fim]=@ContagemResultadoExecucao_Fim, [ContagemResultadoExecucao_Dias]=@ContagemResultadoExecucao_Dias  WHERE [ContagemResultadoExecucao_Codigo] = @ContagemResultadoExecucao_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP003Y5)
             ,new CursorDef("P003Y6", "INSERT INTO [ContagemResultadoContagens]([ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt], [ContagemResultado_TimeCnt], [ContagemResultado_PFBFS], [ContagemResultado_PFLFS], [ContagemResultado_PFBFM], [ContagemResultado_PFLFM], [ContagemResultado_Divergencia], [ContagemResultado_ParecerTcn], [ContagemResultado_ContadorFMCod], [ContagemResultado_NaoCnfCntCod], [ContagemResultado_StatusCnt], [ContagemResultadoContagens_Esforco], [ContagemResultado_Ultima], [ContagemResultado_Deflator], [ContagemResultado_CstUntPrd], [ContagemResultado_Planilha], [ContagemResultado_NomePla], [ContagemResultado_TipoPla], [ContagemResultadoContagens_Prazo], [ContagemResultado_NvlCnt]) VALUES(@ContagemResultado_Codigo, @ContagemResultado_DataCnt, @ContagemResultado_HoraCnt, @ContagemResultado_TimeCnt, @ContagemResultado_PFBFS, @ContagemResultado_PFLFS, @ContagemResultado_PFBFM, @ContagemResultado_PFLFM, @ContagemResultado_Divergencia, @ContagemResultado_ParecerTcn, @ContagemResultado_ContadorFMCod, @ContagemResultado_NaoCnfCntCod, @ContagemResultado_StatusCnt, @ContagemResultadoContagens_Esforco, @ContagemResultado_Ultima, @ContagemResultado_Deflator, @ContagemResultado_CstUntPrd, @ContagemResultado_Planilha, @ContagemResultado_NomePla, @ContagemResultado_TipoPla, @ContagemResultadoContagens_Prazo, @ContagemResultado_NvlCnt)", GxErrorMask.GX_NOMASK,prmP003Y6)
             ,new CursorDef("P003Y7", "DELETE FROM [ContagemResultadoContagens]  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP003Y7)
             ,new CursorDef("P003Y8", "UPDATE [ContagemResultadoErro] SET [ContagemResultadoErro_Status]='C'  WHERE [ContagemResultado_Codigo] = @AV13ContagemResultado_Codigo and [ContagemResultadoErro_Tipo] = 'DA'", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP003Y8)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((short[]) buf[2])[0] = rslt.getShort(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[4])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 10) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((bool[]) buf[14])[0] = rslt.getBool(8) ;
                ((short[]) buf[15])[0] = rslt.getShort(9) ;
                ((short[]) buf[16])[0] = rslt.getShort(10) ;
                ((int[]) buf[17])[0] = rslt.getInt(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((int[]) buf[19])[0] = rslt.getInt(12) ;
                ((String[]) buf[20])[0] = rslt.getLongVarchar(13) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                ((decimal[]) buf[22])[0] = rslt.getDecimal(14) ;
                ((decimal[]) buf[23])[0] = rslt.getDecimal(15) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(15);
                ((decimal[]) buf[25])[0] = rslt.getDecimal(16) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(16);
                ((decimal[]) buf[27])[0] = rslt.getDecimal(17) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(17);
                ((decimal[]) buf[29])[0] = rslt.getDecimal(18) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(18);
                ((DateTime[]) buf[31])[0] = rslt.getGXDateTime(19) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(19);
                ((String[]) buf[33])[0] = rslt.getString(20, 5) ;
                ((DateTime[]) buf[34])[0] = rslt.getGXDate(21) ;
                ((int[]) buf[35])[0] = rslt.getInt(22) ;
                ((String[]) buf[36])[0] = rslt.getString(23, 1) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(23);
                ((String[]) buf[38])[0] = rslt.getBLOBFile(24, rslt.getString(4, 10), rslt.getString(5, 50)) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(24);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(3) ;
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(4, (DateTime)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(6, (decimal)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(7, (decimal)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 8 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(8, (decimal)parms[12]);
                }
                stmt.SetParameter(9, (decimal)parms[13]);
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 10 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[15]);
                }
                stmt.SetParameter(11, (int)parms[16]);
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 12 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(12, (int)parms[18]);
                }
                stmt.SetParameter(13, (short)parms[19]);
                stmt.SetParameter(14, (short)parms[20]);
                stmt.SetParameter(15, (bool)parms[21]);
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 16 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(16, (decimal)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 17 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(17, (decimal)parms[25]);
                }
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 18 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(18, (String)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 19 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(19, (String)parms[29]);
                }
                if ( (bool)parms[30] )
                {
                   stmt.setNull( 20 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(20, (String)parms[31]);
                }
                if ( (bool)parms[32] )
                {
                   stmt.setNull( 21 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(21, (DateTime)parms[33]);
                }
                if ( (bool)parms[34] )
                {
                   stmt.setNull( 22 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(22, (short)parms[35]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
