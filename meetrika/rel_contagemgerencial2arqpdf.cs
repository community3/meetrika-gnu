/*
               File: Rel_ContagemGerencial2ArqPDF
        Description: Contagem Gerencial2 Arq PDF
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:14:57.42
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class rel_contagemgerencial2arqpdf : GXProcedure
   {
      public rel_contagemgerencial2arqpdf( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public rel_contagemgerencial2arqpdf( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_AreaTrabalhoCod ,
                           int aP1_ContagemResultado_LoteAceite ,
                           String aP2_QrCodePath ,
                           String aP3_QrCodeUrl ,
                           String aP4_Verificador ,
                           String aP5_FileName )
      {
         this.AV98Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         this.AV40ContagemResultado_LoteAceite = aP1_ContagemResultado_LoteAceite;
         this.AV134QrCodePath = aP2_QrCodePath;
         this.AV136QrCodeUrl = aP3_QrCodeUrl;
         this.AV144Verificador = aP4_Verificador;
         this.AV145FileName = aP5_FileName;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Contratada_AreaTrabalhoCod ,
                                 int aP1_ContagemResultado_LoteAceite ,
                                 String aP2_QrCodePath ,
                                 String aP3_QrCodeUrl ,
                                 String aP4_Verificador ,
                                 String aP5_FileName )
      {
         rel_contagemgerencial2arqpdf objrel_contagemgerencial2arqpdf;
         objrel_contagemgerencial2arqpdf = new rel_contagemgerencial2arqpdf();
         objrel_contagemgerencial2arqpdf.AV98Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         objrel_contagemgerencial2arqpdf.AV40ContagemResultado_LoteAceite = aP1_ContagemResultado_LoteAceite;
         objrel_contagemgerencial2arqpdf.AV134QrCodePath = aP2_QrCodePath;
         objrel_contagemgerencial2arqpdf.AV136QrCodeUrl = aP3_QrCodeUrl;
         objrel_contagemgerencial2arqpdf.AV144Verificador = aP4_Verificador;
         objrel_contagemgerencial2arqpdf.AV145FileName = aP5_FileName;
         objrel_contagemgerencial2arqpdf.context.SetSubmitInitialConfig(context);
         objrel_contagemgerencial2arqpdf.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objrel_contagemgerencial2arqpdf);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((rel_contagemgerencial2arqpdf)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 4;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName(AV145FileName) ;
         getPrinter().GxSetDocFormat("PDF") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 2, 1, 12240, 15840, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*4));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV133WWPContext) ;
            GXt_boolean1 = AV123OSAutomatica;
            GXt_int2 = AV133WWPContext.gxTpr_Areatrabalho_codigo;
            new prc_osautomatica(context ).execute( ref  GXt_int2, out  GXt_boolean1) ;
            AV133WWPContext.gxTpr_Areatrabalho_codigo = GXt_int2;
            AV123OSAutomatica = GXt_boolean1;
            AV146TextoLink = StringUtil.Trim( AV136QrCodeUrl) + ", informando o c�digo " + StringUtil.Trim( AV144Verificador);
            AV104DescricaoLength = 40;
            AV168DescricaoGlsLength = 100;
            AV128SubTitulo = "";
            /* Using cursor P006O2 */
            pr_default.execute(0, new Object[] {AV40ContagemResultado_LoteAceite});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A596Lote_Codigo = P006O2_A596Lote_Codigo[0];
               A563Lote_Nome = P006O2_A563Lote_Nome[0];
               A562Lote_Numero = P006O2_A562Lote_Numero[0];
               AV128SubTitulo = A563Lote_Nome;
               AV100d = (decimal)(StringUtil.Len( A562Lote_Numero)-3);
               AV119NumeroRelatorio = StringUtil.Substring( A562Lote_Numero, (int)(AV100d), 4);
               AV100d = (decimal)(AV100d-2);
               AV119NumeroRelatorio = StringUtil.Substring( A562Lote_Numero, (int)(AV100d), 2) + "/" + AV119NumeroRelatorio;
               AV100d = (decimal)(AV100d-1);
               AV119NumeroRelatorio = "Lote " + StringUtil.Substring( AV119NumeroRelatorio, 4, 4) + "/" + StringUtil.PadL( StringUtil.Substring( A562Lote_Numero, 1, (int)(AV100d)), 3, "0");
               /* Using cursor P006O3 */
               pr_default.execute(1, new Object[] {A596Lote_Codigo});
               while ( (pr_default.getStatus(1) != 101) )
               {
                  A490ContagemResultado_ContratadaCod = P006O3_A490ContagemResultado_ContratadaCod[0];
                  n490ContagemResultado_ContratadaCod = P006O3_n490ContagemResultado_ContratadaCod[0];
                  A1553ContagemResultado_CntSrvCod = P006O3_A1553ContagemResultado_CntSrvCod[0];
                  n1553ContagemResultado_CntSrvCod = P006O3_n1553ContagemResultado_CntSrvCod[0];
                  A1603ContagemResultado_CntCod = P006O3_A1603ContagemResultado_CntCod[0];
                  n1603ContagemResultado_CntCod = P006O3_n1603ContagemResultado_CntCod[0];
                  A1604ContagemResultado_CntPrpCod = P006O3_A1604ContagemResultado_CntPrpCod[0];
                  n1604ContagemResultado_CntPrpCod = P006O3_n1604ContagemResultado_CntPrpCod[0];
                  A1605ContagemResultado_CntPrpPesCod = P006O3_A1605ContagemResultado_CntPrpPesCod[0];
                  n1605ContagemResultado_CntPrpPesCod = P006O3_n1605ContagemResultado_CntPrpPesCod[0];
                  A597ContagemResultado_LoteAceiteCod = P006O3_A597ContagemResultado_LoteAceiteCod[0];
                  n597ContagemResultado_LoteAceiteCod = P006O3_n597ContagemResultado_LoteAceiteCod[0];
                  A40001ContagemResultado_ContratadaLogo_GXI = P006O3_A40001ContagemResultado_ContratadaLogo_GXI[0];
                  n40001ContagemResultado_ContratadaLogo_GXI = P006O3_n40001ContagemResultado_ContratadaLogo_GXI[0];
                  A1612ContagemResultado_CntNum = P006O3_A1612ContagemResultado_CntNum[0];
                  n1612ContagemResultado_CntNum = P006O3_n1612ContagemResultado_CntNum[0];
                  A1606ContagemResultado_CntPrpPesNom = P006O3_A1606ContagemResultado_CntPrpPesNom[0];
                  n1606ContagemResultado_CntPrpPesNom = P006O3_n1606ContagemResultado_CntPrpPesNom[0];
                  A456ContagemResultado_Codigo = P006O3_A456ContagemResultado_Codigo[0];
                  A1816ContagemResultado_ContratadaLogo = P006O3_A1816ContagemResultado_ContratadaLogo[0];
                  n1816ContagemResultado_ContratadaLogo = P006O3_n1816ContagemResultado_ContratadaLogo[0];
                  A40001ContagemResultado_ContratadaLogo_GXI = P006O3_A40001ContagemResultado_ContratadaLogo_GXI[0];
                  n40001ContagemResultado_ContratadaLogo_GXI = P006O3_n40001ContagemResultado_ContratadaLogo_GXI[0];
                  A1816ContagemResultado_ContratadaLogo = P006O3_A1816ContagemResultado_ContratadaLogo[0];
                  n1816ContagemResultado_ContratadaLogo = P006O3_n1816ContagemResultado_ContratadaLogo[0];
                  A1603ContagemResultado_CntCod = P006O3_A1603ContagemResultado_CntCod[0];
                  n1603ContagemResultado_CntCod = P006O3_n1603ContagemResultado_CntCod[0];
                  A1604ContagemResultado_CntPrpCod = P006O3_A1604ContagemResultado_CntPrpCod[0];
                  n1604ContagemResultado_CntPrpCod = P006O3_n1604ContagemResultado_CntPrpCod[0];
                  A1612ContagemResultado_CntNum = P006O3_A1612ContagemResultado_CntNum[0];
                  n1612ContagemResultado_CntNum = P006O3_n1612ContagemResultado_CntNum[0];
                  A1605ContagemResultado_CntPrpPesCod = P006O3_A1605ContagemResultado_CntPrpPesCod[0];
                  n1605ContagemResultado_CntPrpPesCod = P006O3_n1605ContagemResultado_CntPrpPesCod[0];
                  A1606ContagemResultado_CntPrpPesNom = P006O3_A1606ContagemResultado_CntPrpPesNom[0];
                  n1606ContagemResultado_CntPrpPesNom = P006O3_n1606ContagemResultado_CntPrpPesNom[0];
                  AV9Contrato_Numero = A1612ContagemResultado_CntNum;
                  AV141Preposto = StringUtil.Trim( A1606ContagemResultado_CntPrpPesNom);
                  AV171Contratada_Logo = A1816ContagemResultado_ContratadaLogo;
                  A40000Contratada_Logo_GXI = A40001ContagemResultado_ContratadaLogo_GXI;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  pr_default.readNext(1);
               }
               pr_default.close(1);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            AV143AssinadoDtHr = context.localUtil.DToC( Gx_date, 2, "/") + " em " + StringUtil.Substring( Gx_time, 1, 5) + ", conforme hor�rio oficial de Brasilia.";
            AV137qrCode_Image = AV134QrCodePath;
            AV179Qrcode_image_GXI = GeneXus.Utils.GXDbFile.PathToUrl( AV134QrCodePath);
            /* Execute user subroutine: 'PRINTDATALOTE' */
            S111 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            H6O0( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'PRINTDATALOTE' Routine */
         /* Using cursor P006O5 */
         pr_default.execute(2, new Object[] {AV40ContagemResultado_LoteAceite});
         if ( (pr_default.getStatus(2) != 101) )
         {
            A40002GXC3 = P006O5_A40002GXC3[0];
         }
         else
         {
            A40002GXC3 = DateTime.MinValue;
         }
         pr_default.close(2);
         /* Using cursor P006O8 */
         pr_default.execute(3, new Object[] {AV40ContagemResultado_LoteAceite});
         if ( (pr_default.getStatus(3) != 101) )
         {
            A40003GXC4 = P006O8_A40003GXC4[0];
            A40004GXC5 = P006O8_A40004GXC5[0];
         }
         else
         {
            A40003GXC4 = DateTime.MinValue;
            A40004GXC5 = DateTime.MinValue;
         }
         pr_default.close(3);
         AV103DataInicio = A40002GXC3;
         AV102DataFim = A40003GXC4;
         AV170DataInicioEnt = A40004GXC5;
         AV169DataFimEnt = AV102DataFim;
         /* Using cursor P006O9 */
         pr_default.execute(4, new Object[] {AV40ContagemResultado_LoteAceite});
         while ( (pr_default.getStatus(4) != 101) )
         {
            A597ContagemResultado_LoteAceiteCod = P006O9_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = P006O9_n597ContagemResultado_LoteAceiteCod[0];
            A484ContagemResultado_StatusDmn = P006O9_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P006O9_n484ContagemResultado_StatusDmn[0];
            A1854ContagemResultado_VlrCnc = P006O9_A1854ContagemResultado_VlrCnc[0];
            n1854ContagemResultado_VlrCnc = P006O9_n1854ContagemResultado_VlrCnc[0];
            A1051ContagemResultado_GlsValor = P006O9_A1051ContagemResultado_GlsValor[0];
            n1051ContagemResultado_GlsValor = P006O9_n1051ContagemResultado_GlsValor[0];
            A456ContagemResultado_Codigo = P006O9_A456ContagemResultado_Codigo[0];
            A512ContagemResultado_ValorPF = P006O9_A512ContagemResultado_ValorPF[0];
            n512ContagemResultado_ValorPF = P006O9_n512ContagemResultado_ValorPF[0];
            GXt_decimal3 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal3) ;
            A574ContagemResultado_PFFinal = GXt_decimal3;
            A606ContagemResultado_ValorFinal = (decimal)(A574ContagemResultado_PFFinal*A512ContagemResultado_ValorPF);
            if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 )
            {
               AV63ContagemResultado_PFFaturar = (decimal)(AV63ContagemResultado_PFFaturar+A1854ContagemResultado_VlrCnc);
            }
            else
            {
               AV63ContagemResultado_PFFaturar = A606ContagemResultado_ValorFinal;
            }
            AV64ContagemResultado_PFTotal = (decimal)(AV64ContagemResultado_PFTotal+A574ContagemResultado_PFFinal);
            AV65ContagemResultado_PFTotalFaturar = (decimal)(AV65ContagemResultado_PFTotalFaturar+(AV63ContagemResultado_PFFaturar-A1051ContagemResultado_GlsValor));
            AV125Quantidade = (short)(AV125Quantidade+1);
            pr_default.readNext(4);
         }
         pr_default.close(4);
         if ( AV123OSAutomatica )
         {
            /* Using cursor P006O10 */
            pr_default.execute(5, new Object[] {AV40ContagemResultado_LoteAceite});
            while ( (pr_default.getStatus(5) != 101) )
            {
               BRK6O7 = false;
               A597ContagemResultado_LoteAceiteCod = P006O10_A597ContagemResultado_LoteAceiteCod[0];
               n597ContagemResultado_LoteAceiteCod = P006O10_n597ContagemResultado_LoteAceiteCod[0];
               A1452ContagemResultado_SS = P006O10_A1452ContagemResultado_SS[0];
               n1452ContagemResultado_SS = P006O10_n1452ContagemResultado_SS[0];
               A457ContagemResultado_Demanda = P006O10_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = P006O10_n457ContagemResultado_Demanda[0];
               A515ContagemResultado_SistemaCoord = P006O10_A515ContagemResultado_SistemaCoord[0];
               n515ContagemResultado_SistemaCoord = P006O10_n515ContagemResultado_SistemaCoord[0];
               A489ContagemResultado_SistemaCod = P006O10_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = P006O10_n489ContagemResultado_SistemaCod[0];
               A493ContagemResultado_DemandaFM = P006O10_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = P006O10_n493ContagemResultado_DemandaFM[0];
               A456ContagemResultado_Codigo = P006O10_A456ContagemResultado_Codigo[0];
               A515ContagemResultado_SistemaCoord = P006O10_A515ContagemResultado_SistemaCoord[0];
               n515ContagemResultado_SistemaCoord = P006O10_n515ContagemResultado_SistemaCoord[0];
               OV159ContagemResultado_SS = AV159ContagemResultado_SS;
               AV127Sistema_Coordenacao = A515ContagemResultado_SistemaCoord;
               AV126sistema_Codigo = A489ContagemResultado_SistemaCod;
               AV122Os = StringUtil.PadL( A493ContagemResultado_DemandaFM, 4, "0");
               AV158SS = StringUtil.PadL( StringUtil.Trim( StringUtil.Str( (decimal)(A1452ContagemResultado_SS), 8, 0)), 4, "0");
               AV159ContagemResultado_SS = A1452ContagemResultado_SS;
               AV38ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
               AV117i = 0;
               while ( (pr_default.getStatus(5) != 101) && ( P006O10_A597ContagemResultado_LoteAceiteCod[0] == A597ContagemResultado_LoteAceiteCod ) && ( P006O10_A1452ContagemResultado_SS[0] == A1452ContagemResultado_SS ) )
               {
                  BRK6O7 = false;
                  A457ContagemResultado_Demanda = P006O10_A457ContagemResultado_Demanda[0];
                  n457ContagemResultado_Demanda = P006O10_n457ContagemResultado_Demanda[0];
                  A456ContagemResultado_Codigo = P006O10_A456ContagemResultado_Codigo[0];
                  AV117i = (short)(AV117i+1);
                  BRK6O7 = true;
                  pr_default.readNext(5);
               }
               /* Execute user subroutine: 'CONTAGENSDOSISTEMALOTE' */
               S127 ();
               if ( returnInSub )
               {
                  pr_default.close(5);
                  returnInSub = true;
                  if (true) return;
               }
               if ( ! BRK6O7 )
               {
                  BRK6O7 = true;
                  pr_default.readNext(5);
               }
            }
            pr_default.close(5);
         }
         else
         {
            /* Using cursor P006O11 */
            pr_default.execute(6, new Object[] {AV40ContagemResultado_LoteAceite});
            while ( (pr_default.getStatus(6) != 101) )
            {
               BRK6O9 = false;
               A597ContagemResultado_LoteAceiteCod = P006O11_A597ContagemResultado_LoteAceiteCod[0];
               n597ContagemResultado_LoteAceiteCod = P006O11_n597ContagemResultado_LoteAceiteCod[0];
               A493ContagemResultado_DemandaFM = P006O11_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = P006O11_n493ContagemResultado_DemandaFM[0];
               A457ContagemResultado_Demanda = P006O11_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = P006O11_n457ContagemResultado_Demanda[0];
               A515ContagemResultado_SistemaCoord = P006O11_A515ContagemResultado_SistemaCoord[0];
               n515ContagemResultado_SistemaCoord = P006O11_n515ContagemResultado_SistemaCoord[0];
               A489ContagemResultado_SistemaCod = P006O11_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = P006O11_n489ContagemResultado_SistemaCod[0];
               A1452ContagemResultado_SS = P006O11_A1452ContagemResultado_SS[0];
               n1452ContagemResultado_SS = P006O11_n1452ContagemResultado_SS[0];
               A456ContagemResultado_Codigo = P006O11_A456ContagemResultado_Codigo[0];
               A515ContagemResultado_SistemaCoord = P006O11_A515ContagemResultado_SistemaCoord[0];
               n515ContagemResultado_SistemaCoord = P006O11_n515ContagemResultado_SistemaCoord[0];
               OV159ContagemResultado_SS = AV159ContagemResultado_SS;
               AV127Sistema_Coordenacao = A515ContagemResultado_SistemaCoord;
               AV126sistema_Codigo = A489ContagemResultado_SistemaCod;
               AV122Os = A493ContagemResultado_DemandaFM;
               AV158SS = StringUtil.Str( (decimal)(A1452ContagemResultado_SS), 8, 0);
               AV159ContagemResultado_SS = A1452ContagemResultado_SS;
               AV38ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
               AV117i = 0;
               while ( (pr_default.getStatus(6) != 101) && ( P006O11_A597ContagemResultado_LoteAceiteCod[0] == A597ContagemResultado_LoteAceiteCod ) && ( StringUtil.StrCmp(P006O11_A493ContagemResultado_DemandaFM[0], A493ContagemResultado_DemandaFM) == 0 ) )
               {
                  BRK6O9 = false;
                  A457ContagemResultado_Demanda = P006O11_A457ContagemResultado_Demanda[0];
                  n457ContagemResultado_Demanda = P006O11_n457ContagemResultado_Demanda[0];
                  A456ContagemResultado_Codigo = P006O11_A456ContagemResultado_Codigo[0];
                  AV117i = (short)(AV117i+1);
                  BRK6O9 = true;
                  pr_default.readNext(6);
               }
               /* Execute user subroutine: 'CONTAGENSDAOSLOTE' */
               S139 ();
               if ( returnInSub )
               {
                  pr_default.close(6);
                  returnInSub = true;
                  if (true) return;
               }
               if ( ! BRK6O9 )
               {
                  BRK6O9 = true;
                  pr_default.readNext(6);
               }
            }
            pr_default.close(6);
         }
         H6O0( false, 133) ;
         getPrinter().GxDrawRect(0, Gx_line+0, 1073, Gx_line+22, 1, 0, 0, 0, 1, 245, 245, 245, 0, 0, 0, 0, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(700, Gx_line+116, 967, Gx_line+116, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawBitMap(context.GetImagePath( "7ad3f77f-79a3-4554-aaa4-e84910e0d464", "", context.GetTheme( )), 0, Gx_line+50, 100, Gx_line+133) ;
         getPrinter().GxAttris("Arial", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV64ContagemResultado_PFTotal, "ZZ,ZZZ,ZZ9.999")), 844, Gx_line+1, 962, Gx_line+19, 2+256, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV65ContagemResultado_PFTotalFaturar, "ZZZ,ZZZ,ZZZ,ZZ9.99")), 973, Gx_line+1, 1067, Gx_line+19, 2, 0, 0, 0) ;
         getPrinter().GxAttris("Arial", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("Totais:", 781, Gx_line+0, 831, Gx_line+19, 0+256, 0, 0, 0) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 9, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("Gestor(a) Contratual", 775, Gx_line+116, 891, Gx_line+133, 0, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV143AssinadoDtHr, "")), 133, Gx_line+83, 447, Gx_line+100, 0+256, 0, 0, 0) ;
         getPrinter().GxDrawText("Documento assinado eletronicamente por", 133, Gx_line+67, 374, Gx_line+83, 0+256, 0, 0, 0) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 9, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV141Preposto, "@!")), 375, Gx_line+67, 1001, Gx_line+84, 0+256, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+133);
         H6O0( false, 91) ;
         getPrinter().GxDrawBitMap(AV137qrCode_Image, 0, Gx_line+0, 100, Gx_line+83) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 9, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("A autenticidade deste documento pode ser conferida no site", 133, Gx_line+17, 474, Gx_line+33, 0+256, 0, 0, 0) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 9, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV146TextoLink, "")), 133, Gx_line+33, 1072, Gx_line+50, 0+256, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+91);
      }

      protected void S127( )
      {
         /* 'CONTAGENSDOSISTEMALOTE' Routine */
         AV131TotalPontos = 0;
         AV130TotalFinal = 0;
         /* Using cursor P006O13 */
         pr_default.execute(7, new Object[] {AV40ContagemResultado_LoteAceite, AV159ContagemResultado_SS});
         while ( (pr_default.getStatus(7) != 101) )
         {
            A489ContagemResultado_SistemaCod = P006O13_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P006O13_n489ContagemResultado_SistemaCod[0];
            A597ContagemResultado_LoteAceiteCod = P006O13_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = P006O13_n597ContagemResultado_LoteAceiteCod[0];
            A1452ContagemResultado_SS = P006O13_A1452ContagemResultado_SS[0];
            n1452ContagemResultado_SS = P006O13_n1452ContagemResultado_SS[0];
            A484ContagemResultado_StatusDmn = P006O13_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P006O13_n484ContagemResultado_StatusDmn[0];
            A1854ContagemResultado_VlrCnc = P006O13_A1854ContagemResultado_VlrCnc[0];
            n1854ContagemResultado_VlrCnc = P006O13_n1854ContagemResultado_VlrCnc[0];
            A494ContagemResultado_Descricao = P006O13_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P006O13_n494ContagemResultado_Descricao[0];
            A490ContagemResultado_ContratadaCod = P006O13_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P006O13_n490ContagemResultado_ContratadaCod[0];
            A1237ContagemResultado_PrazoMaisDias = P006O13_A1237ContagemResultado_PrazoMaisDias[0];
            n1237ContagemResultado_PrazoMaisDias = P006O13_n1237ContagemResultado_PrazoMaisDias[0];
            A1227ContagemResultado_PrazoInicialDias = P006O13_A1227ContagemResultado_PrazoInicialDias[0];
            n1227ContagemResultado_PrazoInicialDias = P006O13_n1227ContagemResultado_PrazoInicialDias[0];
            A1351ContagemResultado_DataPrevista = P006O13_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = P006O13_n1351ContagemResultado_DataPrevista[0];
            A1349ContagemResultado_DataExecucao = P006O13_A1349ContagemResultado_DataExecucao[0];
            n1349ContagemResultado_DataExecucao = P006O13_n1349ContagemResultado_DataExecucao[0];
            A1051ContagemResultado_GlsValor = P006O13_A1051ContagemResultado_GlsValor[0];
            n1051ContagemResultado_GlsValor = P006O13_n1051ContagemResultado_GlsValor[0];
            A471ContagemResultado_DataDmn = P006O13_A471ContagemResultado_DataDmn[0];
            A509ContagemrResultado_SistemaSigla = P006O13_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P006O13_n509ContagemrResultado_SistemaSigla[0];
            A1050ContagemResultado_GlsDescricao = P006O13_A1050ContagemResultado_GlsDescricao[0];
            n1050ContagemResultado_GlsDescricao = P006O13_n1050ContagemResultado_GlsDescricao[0];
            A1049ContagemResultado_GlsData = P006O13_A1049ContagemResultado_GlsData[0];
            n1049ContagemResultado_GlsData = P006O13_n1049ContagemResultado_GlsData[0];
            A457ContagemResultado_Demanda = P006O13_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P006O13_n457ContagemResultado_Demanda[0];
            A566ContagemResultado_DataUltCnt = P006O13_A566ContagemResultado_DataUltCnt[0];
            n566ContagemResultado_DataUltCnt = P006O13_n566ContagemResultado_DataUltCnt[0];
            A456ContagemResultado_Codigo = P006O13_A456ContagemResultado_Codigo[0];
            A512ContagemResultado_ValorPF = P006O13_A512ContagemResultado_ValorPF[0];
            n512ContagemResultado_ValorPF = P006O13_n512ContagemResultado_ValorPF[0];
            A509ContagemrResultado_SistemaSigla = P006O13_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P006O13_n509ContagemrResultado_SistemaSigla[0];
            A566ContagemResultado_DataUltCnt = P006O13_A566ContagemResultado_DataUltCnt[0];
            n566ContagemResultado_DataUltCnt = P006O13_n566ContagemResultado_DataUltCnt[0];
            GXt_decimal3 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal3) ;
            A574ContagemResultado_PFFinal = GXt_decimal3;
            A606ContagemResultado_ValorFinal = (decimal)(A574ContagemResultado_PFFinal*A512ContagemResultado_ValorPF);
            if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 )
            {
               AV63ContagemResultado_PFFaturar = A1854ContagemResultado_VlrCnc;
            }
            else
            {
               AV63ContagemResultado_PFFaturar = A606ContagemResultado_ValorFinal;
            }
            AV101DataCnt = A566ContagemResultado_DataUltCnt;
            AV117i = 1;
            AV149DmnDescricao = A494ContagemResultado_Descricao;
            /* Execute user subroutine: 'LINHADADMNDESCRICAO' */
            S1411 ();
            if ( returnInSub )
            {
               pr_default.close(7);
               returnInSub = true;
               if (true) return;
            }
            AV163Codigo = A456ContagemResultado_Codigo;
            AV164Contratada = A490ContagemResultado_ContratadaCod;
            AV166Prazo = (short)(A1227ContagemResultado_PrazoInicialDias+A1237ContagemResultado_PrazoMaisDias);
            AV161Entregue = A566ContagemResultado_DataUltCnt;
            A1351ContagemResultado_DataPrevista = DateTimeUtil.ResetTime( AV165Previsto ) ;
            n1351ContagemResultado_DataPrevista = false;
            A1349ContagemResultado_DataExecucao = DateTimeUtil.ResetTime( AV161Entregue ) ;
            n1349ContagemResultado_DataExecucao = false;
            AV131TotalPontos = (decimal)(AV131TotalPontos+A574ContagemResultado_PFFinal);
            AV130TotalFinal = (decimal)(AV130TotalFinal+(AV63ContagemResultado_PFFaturar-A1051ContagemResultado_GlsValor));
            if ( A574ContagemResultado_PFFinal < 0.001m )
            {
               AV155strPFFinal = StringUtil.Str( A574ContagemResultado_PFFinal, 14, 4);
            }
            else
            {
               AV155strPFFinal = StringUtil.Str( A574ContagemResultado_PFFinal, 14, 3);
            }
            if ( AV123OSAutomatica )
            {
               H6O0( false, 16) ;
               getPrinter().GxAttris("Courier New", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV148Descricao, "")), 623, Gx_line+0, 905, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A457ContagemResultado_Demanda, "@!")), 48, Gx_line+0, 170, Gx_line+15, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A509ContagemrResultado_SistemaSigla, "@!")), 447, Gx_line+0, 621, Gx_line+15, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A512ContagemResultado_ValorPF, "ZZ,ZZZ,ZZZ,ZZ9.999")), 968, Gx_line+0, 1003, Gx_line+15, 2, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV63ContagemResultado_PFFaturar, "ZZ,ZZZ,ZZZ,ZZ9.999")), 1001, Gx_line+0, 1068, Gx_line+15, 2, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV122Os, "")), 175, Gx_line+0, 268, Gx_line+15, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV155strPFFinal, "")), 888, Gx_line+0, 962, Gx_line+15, 2+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV158SS, "")), 0, Gx_line+0, 50, Gx_line+15, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV127Sistema_Coordenacao, "@!")), 370, Gx_line+0, 447, Gx_line+15, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( AV161Entregue, "99/99/99"), 317, Gx_line+0, 366, Gx_line+15, 1+256, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( A471ContagemResultado_DataDmn, "99/99/99"), 264, Gx_line+0, 313, Gx_line+15, 1+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+16);
            }
            else
            {
               H6O0( false, 16) ;
               getPrinter().GxAttris("Courier New", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV148Descricao, "")), 623, Gx_line+0, 905, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV122Os, "")), 251, Gx_line+0, 344, Gx_line+15, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV63ContagemResultado_PFFaturar, "ZZ,ZZZ,ZZZ,ZZ9.999")), 1001, Gx_line+0, 1068, Gx_line+15, 2, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A512ContagemResultado_ValorPF, "ZZ,ZZZ,ZZZ,ZZ9.999")), 968, Gx_line+0, 1003, Gx_line+15, 2, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A509ContagemrResultado_SistemaSigla, "@!")), 450, Gx_line+0, 624, Gx_line+15, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A457ContagemResultado_Demanda, "@!")), 0, Gx_line+0, 250, Gx_line+15, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV155strPFFinal, "")), 888, Gx_line+0, 962, Gx_line+15, 2+256, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( AV161Entregue, "99/99/99"), 392, Gx_line+0, 441, Gx_line+15, 1+256, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( A471ContagemResultado_DataDmn, "99/99/99"), 339, Gx_line+0, 388, Gx_line+15, 1+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+16);
            }
            /* Execute user subroutine: 'PRINTDESCRICAO' */
            S1511 ();
            if ( returnInSub )
            {
               pr_default.close(7);
               returnInSub = true;
               if (true) return;
            }
            AV158SS = "";
            AV162GlsValor = A1051ContagemResultado_GlsValor;
            if ( ( A1051ContagemResultado_GlsValor > Convert.ToDecimal( 0 )) )
            {
               if ( ( A512ContagemResultado_ValorPF > Convert.ToDecimal( 0 )) )
               {
                  AV153GlsPF = (decimal)(A1051ContagemResultado_GlsValor/ (decimal)(A512ContagemResultado_ValorPF));
               }
               else
               {
                  AV153GlsPF = 0;
               }
               AV100d = (decimal)(1);
               AV150GlsDescricao = A1050ContagemResultado_GlsDescricao;
               /* Execute user subroutine: 'LINHADAGLSDESCRICAO' */
               S1611 ();
               if ( returnInSub )
               {
                  pr_default.close(7);
                  returnInSub = true;
                  if (true) return;
               }
               H6O0( false, 16) ;
               getPrinter().GxAttris("Courier New", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV148Descricao, "")), 417, Gx_line+0, 907, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Glosa", 92, Gx_line+0, 121, Gx_line+14, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A1051ContagemResultado_GlsValor, "ZZ,ZZZ,ZZZ,ZZ9.999")), 1000, Gx_line+0, 1067, Gx_line+15, 2, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( A1049ContagemResultado_GlsData, "99/99/99"), 264, Gx_line+0, 314, Gx_line+15, 2, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV153GlsPF, "ZZ,ZZZ,ZZ9.999")), 898, Gx_line+0, 962, Gx_line+15, 2, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A512ContagemResultado_ValorPF, "ZZ,ZZZ,ZZZ,ZZ9.999")), 967, Gx_line+0, 1002, Gx_line+15, 2, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+16);
               /* Execute user subroutine: 'PRINTGLOSADESC' */
               S1711 ();
               if ( returnInSub )
               {
                  pr_default.close(7);
                  returnInSub = true;
                  if (true) return;
               }
            }
            /* Using cursor P006O14 */
            pr_default.execute(8, new Object[] {n1351ContagemResultado_DataPrevista, A1351ContagemResultado_DataPrevista, n1349ContagemResultado_DataExecucao, A1349ContagemResultado_DataExecucao, A456ContagemResultado_Codigo});
            pr_default.close(8);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            pr_default.readNext(7);
         }
         pr_default.close(7);
         if ( AV131TotalPontos < 0.001m )
         {
            AV156strPFTotal = StringUtil.Str( AV131TotalPontos, 14, 4);
         }
         else
         {
            AV156strPFTotal = StringUtil.Str( AV131TotalPontos, 14, 3);
         }
         if ( AV130TotalFinal < 0.001m )
         {
            AV129TotalFaturar = NumberUtil.Round( AV130TotalFinal, 4);
            AV157strPFTotalFaturar = StringUtil.Str( AV129TotalFaturar, 14, 4);
         }
         else if ( AV130TotalFinal < 0.01m )
         {
            AV129TotalFaturar = NumberUtil.Round( AV130TotalFinal, 3);
            AV157strPFTotalFaturar = StringUtil.Str( AV129TotalFaturar, 14, 3);
         }
         else
         {
            AV157strPFTotalFaturar = StringUtil.Str( AV130TotalFinal, 14, 2);
         }
         AV129TotalFaturar = NumberUtil.Round( AV130TotalFinal, 2);
         H6O0( false, 40) ;
         getPrinter().GxDrawLine(0, Gx_line+3, 1070, Gx_line+3, 1, 0, 0, 0, 0) ;
         getPrinter().GxAttris("Arial", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV156strPFTotal, "")), 859, Gx_line+9, 962, Gx_line+24, 2, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV157strPFTotalFaturar, "")), 958, Gx_line+9, 1067, Gx_line+27, 2, 0, 0, 0) ;
         getPrinter().GxAttris("Arial", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("Subtotais:", 781, Gx_line+7, 856, Gx_line+26, 0+256, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+40);
      }

      protected void S139( )
      {
         /* 'CONTAGENSDAOSLOTE' Routine */
         AV131TotalPontos = 0;
         AV130TotalFinal = 0;
         if ( AV123OSAutomatica )
         {
            /* Using cursor P006O16 */
            pr_default.execute(9, new Object[] {AV40ContagemResultado_LoteAceite, AV159ContagemResultado_SS});
            while ( (pr_default.getStatus(9) != 101) )
            {
               A489ContagemResultado_SistemaCod = P006O16_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = P006O16_n489ContagemResultado_SistemaCod[0];
               A597ContagemResultado_LoteAceiteCod = P006O16_A597ContagemResultado_LoteAceiteCod[0];
               n597ContagemResultado_LoteAceiteCod = P006O16_n597ContagemResultado_LoteAceiteCod[0];
               A1452ContagemResultado_SS = P006O16_A1452ContagemResultado_SS[0];
               n1452ContagemResultado_SS = P006O16_n1452ContagemResultado_SS[0];
               A484ContagemResultado_StatusDmn = P006O16_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P006O16_n484ContagemResultado_StatusDmn[0];
               A1854ContagemResultado_VlrCnc = P006O16_A1854ContagemResultado_VlrCnc[0];
               n1854ContagemResultado_VlrCnc = P006O16_n1854ContagemResultado_VlrCnc[0];
               A494ContagemResultado_Descricao = P006O16_A494ContagemResultado_Descricao[0];
               n494ContagemResultado_Descricao = P006O16_n494ContagemResultado_Descricao[0];
               A490ContagemResultado_ContratadaCod = P006O16_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P006O16_n490ContagemResultado_ContratadaCod[0];
               A1237ContagemResultado_PrazoMaisDias = P006O16_A1237ContagemResultado_PrazoMaisDias[0];
               n1237ContagemResultado_PrazoMaisDias = P006O16_n1237ContagemResultado_PrazoMaisDias[0];
               A1227ContagemResultado_PrazoInicialDias = P006O16_A1227ContagemResultado_PrazoInicialDias[0];
               n1227ContagemResultado_PrazoInicialDias = P006O16_n1227ContagemResultado_PrazoInicialDias[0];
               A1351ContagemResultado_DataPrevista = P006O16_A1351ContagemResultado_DataPrevista[0];
               n1351ContagemResultado_DataPrevista = P006O16_n1351ContagemResultado_DataPrevista[0];
               A1349ContagemResultado_DataExecucao = P006O16_A1349ContagemResultado_DataExecucao[0];
               n1349ContagemResultado_DataExecucao = P006O16_n1349ContagemResultado_DataExecucao[0];
               A1051ContagemResultado_GlsValor = P006O16_A1051ContagemResultado_GlsValor[0];
               n1051ContagemResultado_GlsValor = P006O16_n1051ContagemResultado_GlsValor[0];
               A471ContagemResultado_DataDmn = P006O16_A471ContagemResultado_DataDmn[0];
               A509ContagemrResultado_SistemaSigla = P006O16_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = P006O16_n509ContagemrResultado_SistemaSigla[0];
               A1050ContagemResultado_GlsDescricao = P006O16_A1050ContagemResultado_GlsDescricao[0];
               n1050ContagemResultado_GlsDescricao = P006O16_n1050ContagemResultado_GlsDescricao[0];
               A1049ContagemResultado_GlsData = P006O16_A1049ContagemResultado_GlsData[0];
               n1049ContagemResultado_GlsData = P006O16_n1049ContagemResultado_GlsData[0];
               A457ContagemResultado_Demanda = P006O16_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = P006O16_n457ContagemResultado_Demanda[0];
               A566ContagemResultado_DataUltCnt = P006O16_A566ContagemResultado_DataUltCnt[0];
               n566ContagemResultado_DataUltCnt = P006O16_n566ContagemResultado_DataUltCnt[0];
               A456ContagemResultado_Codigo = P006O16_A456ContagemResultado_Codigo[0];
               A512ContagemResultado_ValorPF = P006O16_A512ContagemResultado_ValorPF[0];
               n512ContagemResultado_ValorPF = P006O16_n512ContagemResultado_ValorPF[0];
               A509ContagemrResultado_SistemaSigla = P006O16_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = P006O16_n509ContagemrResultado_SistemaSigla[0];
               A566ContagemResultado_DataUltCnt = P006O16_A566ContagemResultado_DataUltCnt[0];
               n566ContagemResultado_DataUltCnt = P006O16_n566ContagemResultado_DataUltCnt[0];
               GXt_decimal3 = A574ContagemResultado_PFFinal;
               new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal3) ;
               A574ContagemResultado_PFFinal = GXt_decimal3;
               A606ContagemResultado_ValorFinal = (decimal)(A574ContagemResultado_PFFinal*A512ContagemResultado_ValorPF);
               if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 )
               {
                  AV63ContagemResultado_PFFaturar = A1854ContagemResultado_VlrCnc;
               }
               else
               {
                  AV63ContagemResultado_PFFaturar = A606ContagemResultado_ValorFinal;
               }
               AV101DataCnt = A566ContagemResultado_DataUltCnt;
               AV117i = 1;
               AV149DmnDescricao = A494ContagemResultado_Descricao;
               /* Execute user subroutine: 'LINHADADMNDESCRICAO' */
               S1411 ();
               if ( returnInSub )
               {
                  pr_default.close(9);
                  returnInSub = true;
                  if (true) return;
               }
               AV163Codigo = A456ContagemResultado_Codigo;
               AV164Contratada = A490ContagemResultado_ContratadaCod;
               AV166Prazo = (short)(A1227ContagemResultado_PrazoInicialDias+A1237ContagemResultado_PrazoMaisDias);
               AV161Entregue = A566ContagemResultado_DataUltCnt;
               A1351ContagemResultado_DataPrevista = DateTimeUtil.ResetTime( AV165Previsto ) ;
               n1351ContagemResultado_DataPrevista = false;
               A1349ContagemResultado_DataExecucao = DateTimeUtil.ResetTime( AV161Entregue ) ;
               n1349ContagemResultado_DataExecucao = false;
               AV131TotalPontos = (decimal)(AV131TotalPontos+A574ContagemResultado_PFFinal);
               AV130TotalFinal = (decimal)(AV130TotalFinal+(AV63ContagemResultado_PFFaturar-A1051ContagemResultado_GlsValor));
               if ( A574ContagemResultado_PFFinal < 0.001m )
               {
                  AV155strPFFinal = StringUtil.Str( A574ContagemResultado_PFFinal, 14, 4);
               }
               else
               {
                  AV155strPFFinal = StringUtil.Str( A574ContagemResultado_PFFinal, 14, 3);
               }
               H6O0( false, 16) ;
               getPrinter().GxAttris("Courier New", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV148Descricao, "")), 623, Gx_line+0, 905, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A457ContagemResultado_Demanda, "@!")), 48, Gx_line+0, 170, Gx_line+15, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A509ContagemrResultado_SistemaSigla, "@!")), 447, Gx_line+0, 621, Gx_line+15, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A512ContagemResultado_ValorPF, "ZZ,ZZZ,ZZZ,ZZ9.999")), 968, Gx_line+0, 1003, Gx_line+15, 2, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV63ContagemResultado_PFFaturar, "ZZ,ZZZ,ZZZ,ZZ9.999")), 1001, Gx_line+0, 1068, Gx_line+15, 2, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV122Os, "")), 175, Gx_line+0, 268, Gx_line+15, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV155strPFFinal, "")), 888, Gx_line+0, 962, Gx_line+15, 2+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV158SS, "")), 0, Gx_line+0, 50, Gx_line+15, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV127Sistema_Coordenacao, "@!")), 370, Gx_line+0, 447, Gx_line+15, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( AV161Entregue, "99/99/99"), 317, Gx_line+0, 366, Gx_line+15, 1+256, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( A471ContagemResultado_DataDmn, "99/99/99"), 264, Gx_line+0, 313, Gx_line+15, 1+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+16);
               /* Execute user subroutine: 'PRINTDESCRICAO' */
               S1511 ();
               if ( returnInSub )
               {
                  pr_default.close(9);
                  returnInSub = true;
                  if (true) return;
               }
               AV158SS = "";
               AV162GlsValor = A1051ContagemResultado_GlsValor;
               if ( ( A1051ContagemResultado_GlsValor > Convert.ToDecimal( 0 )) )
               {
                  AV153GlsPF = (decimal)(A1051ContagemResultado_GlsValor/ (decimal)(A512ContagemResultado_ValorPF));
                  AV100d = (decimal)(1);
                  AV150GlsDescricao = A1050ContagemResultado_GlsDescricao;
                  /* Execute user subroutine: 'LINHADAGLSDESCRICAO' */
                  S1611 ();
                  if ( returnInSub )
                  {
                     pr_default.close(9);
                     returnInSub = true;
                     if (true) return;
                  }
                  H6O0( false, 16) ;
                  getPrinter().GxAttris("Courier New", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV148Descricao, "")), 417, Gx_line+0, 907, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Glosa", 92, Gx_line+0, 121, Gx_line+14, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A1051ContagemResultado_GlsValor, "ZZ,ZZZ,ZZZ,ZZ9.999")), 1000, Gx_line+0, 1067, Gx_line+15, 2, 0, 0, 0) ;
                  getPrinter().GxDrawText(context.localUtil.Format( A1049ContagemResultado_GlsData, "99/99/99"), 264, Gx_line+0, 314, Gx_line+15, 2, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV153GlsPF, "ZZ,ZZZ,ZZ9.999")), 898, Gx_line+0, 962, Gx_line+15, 2, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A512ContagemResultado_ValorPF, "ZZ,ZZZ,ZZZ,ZZ9.999")), 967, Gx_line+0, 1002, Gx_line+15, 2, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+16);
                  /* Execute user subroutine: 'PRINTGLOSADESC' */
                  S1711 ();
                  if ( returnInSub )
                  {
                     pr_default.close(9);
                     returnInSub = true;
                     if (true) return;
                  }
               }
               /* Using cursor P006O17 */
               pr_default.execute(10, new Object[] {n1351ContagemResultado_DataPrevista, A1351ContagemResultado_DataPrevista, n1349ContagemResultado_DataExecucao, A1349ContagemResultado_DataExecucao, A456ContagemResultado_Codigo});
               pr_default.close(10);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               pr_default.readNext(9);
            }
            pr_default.close(9);
         }
         else
         {
            /* Using cursor P006O19 */
            pr_default.execute(11, new Object[] {AV40ContagemResultado_LoteAceite, AV38ContagemResultado_DemandaFM});
            while ( (pr_default.getStatus(11) != 101) )
            {
               A489ContagemResultado_SistemaCod = P006O19_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = P006O19_n489ContagemResultado_SistemaCod[0];
               A597ContagemResultado_LoteAceiteCod = P006O19_A597ContagemResultado_LoteAceiteCod[0];
               n597ContagemResultado_LoteAceiteCod = P006O19_n597ContagemResultado_LoteAceiteCod[0];
               A493ContagemResultado_DemandaFM = P006O19_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = P006O19_n493ContagemResultado_DemandaFM[0];
               A484ContagemResultado_StatusDmn = P006O19_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P006O19_n484ContagemResultado_StatusDmn[0];
               A1854ContagemResultado_VlrCnc = P006O19_A1854ContagemResultado_VlrCnc[0];
               n1854ContagemResultado_VlrCnc = P006O19_n1854ContagemResultado_VlrCnc[0];
               A494ContagemResultado_Descricao = P006O19_A494ContagemResultado_Descricao[0];
               n494ContagemResultado_Descricao = P006O19_n494ContagemResultado_Descricao[0];
               A490ContagemResultado_ContratadaCod = P006O19_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P006O19_n490ContagemResultado_ContratadaCod[0];
               A1237ContagemResultado_PrazoMaisDias = P006O19_A1237ContagemResultado_PrazoMaisDias[0];
               n1237ContagemResultado_PrazoMaisDias = P006O19_n1237ContagemResultado_PrazoMaisDias[0];
               A1227ContagemResultado_PrazoInicialDias = P006O19_A1227ContagemResultado_PrazoInicialDias[0];
               n1227ContagemResultado_PrazoInicialDias = P006O19_n1227ContagemResultado_PrazoInicialDias[0];
               A1351ContagemResultado_DataPrevista = P006O19_A1351ContagemResultado_DataPrevista[0];
               n1351ContagemResultado_DataPrevista = P006O19_n1351ContagemResultado_DataPrevista[0];
               A1349ContagemResultado_DataExecucao = P006O19_A1349ContagemResultado_DataExecucao[0];
               n1349ContagemResultado_DataExecucao = P006O19_n1349ContagemResultado_DataExecucao[0];
               A1051ContagemResultado_GlsValor = P006O19_A1051ContagemResultado_GlsValor[0];
               n1051ContagemResultado_GlsValor = P006O19_n1051ContagemResultado_GlsValor[0];
               A471ContagemResultado_DataDmn = P006O19_A471ContagemResultado_DataDmn[0];
               A509ContagemrResultado_SistemaSigla = P006O19_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = P006O19_n509ContagemrResultado_SistemaSigla[0];
               A1050ContagemResultado_GlsDescricao = P006O19_A1050ContagemResultado_GlsDescricao[0];
               n1050ContagemResultado_GlsDescricao = P006O19_n1050ContagemResultado_GlsDescricao[0];
               A1049ContagemResultado_GlsData = P006O19_A1049ContagemResultado_GlsData[0];
               n1049ContagemResultado_GlsData = P006O19_n1049ContagemResultado_GlsData[0];
               A457ContagemResultado_Demanda = P006O19_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = P006O19_n457ContagemResultado_Demanda[0];
               A566ContagemResultado_DataUltCnt = P006O19_A566ContagemResultado_DataUltCnt[0];
               n566ContagemResultado_DataUltCnt = P006O19_n566ContagemResultado_DataUltCnt[0];
               A456ContagemResultado_Codigo = P006O19_A456ContagemResultado_Codigo[0];
               A512ContagemResultado_ValorPF = P006O19_A512ContagemResultado_ValorPF[0];
               n512ContagemResultado_ValorPF = P006O19_n512ContagemResultado_ValorPF[0];
               A509ContagemrResultado_SistemaSigla = P006O19_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = P006O19_n509ContagemrResultado_SistemaSigla[0];
               A566ContagemResultado_DataUltCnt = P006O19_A566ContagemResultado_DataUltCnt[0];
               n566ContagemResultado_DataUltCnt = P006O19_n566ContagemResultado_DataUltCnt[0];
               GXt_decimal3 = A574ContagemResultado_PFFinal;
               new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal3) ;
               A574ContagemResultado_PFFinal = GXt_decimal3;
               A606ContagemResultado_ValorFinal = (decimal)(A574ContagemResultado_PFFinal*A512ContagemResultado_ValorPF);
               if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 )
               {
                  AV63ContagemResultado_PFFaturar = A1854ContagemResultado_VlrCnc;
               }
               else
               {
                  AV63ContagemResultado_PFFaturar = A606ContagemResultado_ValorFinal;
               }
               AV101DataCnt = A566ContagemResultado_DataUltCnt;
               AV117i = 1;
               AV149DmnDescricao = A494ContagemResultado_Descricao;
               /* Execute user subroutine: 'LINHADADMNDESCRICAO' */
               S1411 ();
               if ( returnInSub )
               {
                  pr_default.close(11);
                  returnInSub = true;
                  if (true) return;
               }
               AV163Codigo = A456ContagemResultado_Codigo;
               AV164Contratada = A490ContagemResultado_ContratadaCod;
               AV166Prazo = (short)(A1227ContagemResultado_PrazoInicialDias+A1237ContagemResultado_PrazoMaisDias);
               AV161Entregue = A566ContagemResultado_DataUltCnt;
               A1351ContagemResultado_DataPrevista = DateTimeUtil.ResetTime( AV165Previsto ) ;
               n1351ContagemResultado_DataPrevista = false;
               A1349ContagemResultado_DataExecucao = DateTimeUtil.ResetTime( AV161Entregue ) ;
               n1349ContagemResultado_DataExecucao = false;
               AV131TotalPontos = (decimal)(AV131TotalPontos+A574ContagemResultado_PFFinal);
               AV130TotalFinal = (decimal)(AV130TotalFinal+(AV63ContagemResultado_PFFaturar-A1051ContagemResultado_GlsValor));
               if ( A574ContagemResultado_PFFinal < 0.001m )
               {
                  AV155strPFFinal = StringUtil.Str( A574ContagemResultado_PFFinal, 14, 4);
               }
               else
               {
                  AV155strPFFinal = StringUtil.Str( A574ContagemResultado_PFFinal, 14, 3);
               }
               H6O0( false, 16) ;
               getPrinter().GxAttris("Courier New", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV148Descricao, "")), 623, Gx_line+0, 905, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV122Os, "")), 251, Gx_line+0, 344, Gx_line+15, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV63ContagemResultado_PFFaturar, "ZZ,ZZZ,ZZZ,ZZ9.999")), 1001, Gx_line+0, 1068, Gx_line+15, 2, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A512ContagemResultado_ValorPF, "ZZ,ZZZ,ZZZ,ZZ9.999")), 968, Gx_line+0, 1003, Gx_line+15, 2, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A509ContagemrResultado_SistemaSigla, "@!")), 450, Gx_line+0, 624, Gx_line+15, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A457ContagemResultado_Demanda, "@!")), 0, Gx_line+0, 250, Gx_line+15, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV155strPFFinal, "")), 888, Gx_line+0, 962, Gx_line+15, 2+256, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( AV161Entregue, "99/99/99"), 392, Gx_line+0, 441, Gx_line+15, 1+256, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( A471ContagemResultado_DataDmn, "99/99/99"), 339, Gx_line+0, 388, Gx_line+15, 1+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+16);
               /* Execute user subroutine: 'PRINTDESCRICAO' */
               S1511 ();
               if ( returnInSub )
               {
                  pr_default.close(11);
                  returnInSub = true;
                  if (true) return;
               }
               AV122Os = "";
               AV162GlsValor = A1051ContagemResultado_GlsValor;
               if ( ( A1051ContagemResultado_GlsValor > Convert.ToDecimal( 0 )) )
               {
                  if ( ( A512ContagemResultado_ValorPF > Convert.ToDecimal( 0 )) )
                  {
                     AV153GlsPF = (decimal)(A1051ContagemResultado_GlsValor/ (decimal)(A512ContagemResultado_ValorPF));
                  }
                  else
                  {
                     AV162GlsValor = 0;
                  }
                  AV100d = (decimal)(1);
                  AV150GlsDescricao = A1050ContagemResultado_GlsDescricao;
                  /* Execute user subroutine: 'LINHADAGLSDESCRICAO' */
                  S1611 ();
                  if ( returnInSub )
                  {
                     pr_default.close(11);
                     returnInSub = true;
                     if (true) return;
                  }
                  H6O0( false, 16) ;
                  getPrinter().GxAttris("Courier New", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV148Descricao, "")), 417, Gx_line+0, 907, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Glosa", 92, Gx_line+0, 121, Gx_line+14, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A1051ContagemResultado_GlsValor, "ZZ,ZZZ,ZZZ,ZZ9.999")), 1000, Gx_line+0, 1067, Gx_line+15, 2, 0, 0, 0) ;
                  getPrinter().GxDrawText(context.localUtil.Format( A1049ContagemResultado_GlsData, "99/99/99"), 264, Gx_line+0, 314, Gx_line+15, 2, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV153GlsPF, "ZZ,ZZZ,ZZ9.999")), 898, Gx_line+0, 962, Gx_line+15, 2, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A512ContagemResultado_ValorPF, "ZZ,ZZZ,ZZZ,ZZ9.999")), 967, Gx_line+0, 1002, Gx_line+15, 2, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+16);
                  /* Execute user subroutine: 'PRINTGLOSADESC' */
                  S1711 ();
                  if ( returnInSub )
                  {
                     pr_default.close(11);
                     returnInSub = true;
                     if (true) return;
                  }
               }
               /* Using cursor P006O20 */
               pr_default.execute(12, new Object[] {n1351ContagemResultado_DataPrevista, A1351ContagemResultado_DataPrevista, n1349ContagemResultado_DataExecucao, A1349ContagemResultado_DataExecucao, A456ContagemResultado_Codigo});
               pr_default.close(12);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               pr_default.readNext(11);
            }
            pr_default.close(11);
         }
         if ( AV131TotalPontos < 0.001m )
         {
            AV156strPFTotal = StringUtil.Str( AV131TotalPontos, 14, 4);
         }
         else
         {
            AV156strPFTotal = StringUtil.Str( AV131TotalPontos, 14, 3);
         }
         if ( AV130TotalFinal < 0.001m )
         {
            AV129TotalFaturar = NumberUtil.Round( AV130TotalFinal, 4);
            AV157strPFTotalFaturar = StringUtil.Str( AV129TotalFaturar, 14, 4);
         }
         else if ( AV130TotalFinal < 0.01m )
         {
            AV129TotalFaturar = NumberUtil.Round( AV130TotalFinal, 3);
            AV157strPFTotalFaturar = StringUtil.Str( AV129TotalFaturar, 14, 3);
         }
         else
         {
            AV157strPFTotalFaturar = StringUtil.Str( AV130TotalFinal, 14, 2);
         }
         AV129TotalFaturar = NumberUtil.Round( AV130TotalFinal, 2);
         H6O0( false, 40) ;
         getPrinter().GxDrawLine(0, Gx_line+3, 1070, Gx_line+3, 1, 0, 0, 0, 0) ;
         getPrinter().GxAttris("Arial", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV156strPFTotal, "")), 859, Gx_line+9, 962, Gx_line+24, 2, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV157strPFTotalFaturar, "")), 958, Gx_line+9, 1067, Gx_line+27, 2, 0, 0, 0) ;
         getPrinter().GxAttris("Arial", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("Subtotais:", 781, Gx_line+7, 856, Gx_line+26, 0+256, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+40);
      }

      protected void S1511( )
      {
         /* 'PRINTDESCRICAO' Routine */
         while ( StringUtil.Len( AV149DmnDescricao) >= AV117i )
         {
            /* Execute user subroutine: 'LINHADADMNDESCRICAO' */
            S1411 ();
            if (returnInSub) return;
            H6O0( false, 16) ;
            getPrinter().GxAttris("Courier New", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV148Descricao, "")), 623, Gx_line+0, 905, Gx_line+16, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+16);
         }
         AV117i = 0;
      }

      protected void S1711( )
      {
         /* 'PRINTGLOSADESC' Routine */
         while ( (Convert.ToDecimal( StringUtil.Len( AV150GlsDescricao) ) >= AV100d ) )
         {
            /* Execute user subroutine: 'LINHADAGLSDESCRICAO' */
            S1611 ();
            if (returnInSub) return;
            H6O0( false, 16) ;
            getPrinter().GxAttris("Courier New", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV148Descricao, "")), 417, Gx_line+0, 907, Gx_line+16, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+16);
         }
      }

      protected void S1411( )
      {
         /* 'LINHADADMNDESCRICAO' Routine */
         AV151p = AV104DescricaoLength;
         AV148Descricao = StringUtil.Substring( AV149DmnDescricao, AV117i, AV151p);
         if ( ( ( AV117i < StringUtil.Len( AV149DmnDescricao) ) && ( StringUtil.StringSearch( " -+/*_.,:;\\()[]{}?%!$><", StringUtil.Substring( AV148Descricao, AV151p, 1), 1) == 0 ) ) || ( ( AV117i + AV151p <= StringUtil.Len( AV149DmnDescricao) ) && ( StringUtil.StringSearch( " -+/*_.,:;\\()[]{}?%!$><", StringUtil.Substring( AV148Descricao, AV151p+1, 1), 1) == 0 ) ) )
         {
            AV152p2 = AV104DescricaoLength;
            while ( AV152p2 >= 1 )
            {
               AV151p = (short)(StringUtil.StringSearch( " -+/*_.,:;\\()[]{}?%!$><", StringUtil.Substring( AV148Descricao, AV152p2, 1), 1));
               if ( AV151p > 0 )
               {
                  if ( AV151p == 1 )
                  {
                     AV151p = (short)(AV152p2-1);
                     AV148Descricao = StringUtil.Substring( AV149DmnDescricao, AV117i, AV151p);
                  }
                  else
                  {
                     AV151p = AV152p2;
                     AV148Descricao = StringUtil.Substring( AV149DmnDescricao, AV117i, AV151p);
                  }
                  AV117i = (short)(AV117i+AV152p2);
                  if (true) break;
               }
               else
               {
                  AV151p = AV152p2;
               }
               AV152p2 = (short)(AV152p2+-1);
            }
            if ( AV151p < 2 )
            {
               AV148Descricao = StringUtil.Substring( AV149DmnDescricao, AV117i, AV104DescricaoLength);
               AV117i = (short)(AV117i+AV104DescricaoLength);
            }
         }
         else
         {
            AV117i = (short)(AV117i+AV151p);
         }
      }

      protected void S1611( )
      {
         /* 'LINHADAGLSDESCRICAO' Routine */
         AV151p = (short)(50-StringUtil.Len( AV150GlsDescricao));
         if ( AV151p > 0 )
         {
            AV150GlsDescricao = AV150GlsDescricao + StringUtil.Space( AV151p);
         }
         AV151p = AV168DescricaoGlsLength;
         AV148Descricao = StringUtil.Substring( AV150GlsDescricao, (int)(AV100d), AV151p);
         if ( ( ( AV100d < Convert.ToDecimal( StringUtil.Len( AV150GlsDescricao) )) && ( StringUtil.StringSearch( " -+/*_.,:;\\()[]{}?%!$><", StringUtil.Substring( AV148Descricao, AV151p, 1), 1) == 0 ) ) || ( ( AV100d + AV151p <= Convert.ToDecimal( StringUtil.Len( AV150GlsDescricao) )) && ( StringUtil.StringSearch( " -+/*_.,:;\\()[]{}?%!$><", StringUtil.Substring( AV148Descricao, AV151p+1, 1), 1) == 0 ) ) )
         {
            AV152p2 = AV168DescricaoGlsLength;
            while ( AV152p2 >= 1 )
            {
               AV151p = (short)(StringUtil.StringSearch( " -+/*_.,:;\\()[]{}?%!$><", StringUtil.Substring( AV148Descricao, AV152p2, 1), 1));
               if ( AV151p > 0 )
               {
                  if ( AV151p == 1 )
                  {
                     AV151p = (short)(AV152p2-1);
                     AV148Descricao = StringUtil.Substring( AV150GlsDescricao, (int)(AV100d), AV151p);
                  }
                  else
                  {
                     AV151p = AV152p2;
                     AV148Descricao = StringUtil.Substring( AV150GlsDescricao, (int)(AV100d), AV151p);
                  }
                  AV100d = (decimal)(AV100d+AV152p2);
                  if (true) break;
               }
               else
               {
                  AV151p = AV152p2;
               }
               AV152p2 = (short)(AV152p2+-1);
            }
            if ( AV151p < 2 )
            {
               AV148Descricao = StringUtil.Substring( AV150GlsDescricao, (int)(AV100d), AV168DescricaoGlsLength);
               AV100d = (decimal)(AV100d+AV168DescricaoGlsLength);
            }
         }
         else
         {
            AV100d = (decimal)(AV100d+AV151p);
         }
      }

      protected void H6O0( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("P�gina", 942, Gx_line+0, 977, Gx_line+14, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("de", 1017, Gx_line+0, 1031, Gx_line+14, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("{{Pages}}", 1033, Gx_line+0, 1082, Gx_line+14, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(Gx_page), "ZZZZZ9")), 975, Gx_line+0, 1014, Gx_line+15, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV144Verificador, "")), 75, Gx_line+0, 180, Gx_line+15, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("Verificador n�", 0, Gx_line+0, 67, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(AV133WWPContext.gxTpr_Areatrabalho_descricao, 415, Gx_line+0, 676, Gx_line+15, 1+256, 0, 0, 1) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+17);
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               if ( AV123OSAutomatica )
               {
                  getPrinter().GxDrawRect(0, Gx_line+164, 1073, Gx_line+185, 1, 0, 0, 0, 1, 245, 245, 245, 0, 0, 0, 0, 0, 0, 0, 0) ;
                  getPrinter().GxDrawRect(0, Gx_line+116, 1073, Gx_line+137, 1, 0, 0, 0, 1, 245, 245, 245, 0, 0, 0, 0, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(317, Gx_line+217, 365, Gx_line+217, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(0, Gx_line+217, 40, Gx_line+217, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(50, Gx_line+217, 172, Gx_line+217, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(267, Gx_line+217, 313, Gx_line+217, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(367, Gx_line+217, 437, Gx_line+217, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(450, Gx_line+217, 619, Gx_line+217, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(625, Gx_line+217, 912, Gx_line+217, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(917, Gx_line+217, 962, Gx_line+217, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(967, Gx_line+217, 1008, Gx_line+217, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(1017, Gx_line+217, 1070, Gx_line+217, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(175, Gx_line+217, 258, Gx_line+217, 1, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Calibri", 9, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV125Quantidade), "ZZ9")), 450, Gx_line+143, 470, Gx_line+159, 2+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV64ContagemResultado_PFTotal, "ZZ,ZZZ,ZZ9.999")), 700, Gx_line+143, 789, Gx_line+159, 2+256, 0, 0, 0) ;
                  getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Total Bruto R$", 429, Gx_line+166, 533, Gx_line+185, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV65ContagemResultado_PFTotalFaturar, "ZZZ,ZZZ,ZZZ,ZZ9.99")), 566, Gx_line+166, 674, Gx_line+186, 1, 0, 0, 0) ;
                  getPrinter().GxDrawText("SUM�RIO A FATURAR", 476, Gx_line+116, 615, Gx_line+135, 0+256, 0, 0, 0) ;
                  getPrinter().GxAttris("Calibri", 16, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Relat�rio Gerencial", 433, Gx_line+8, 658, Gx_line+36, 1, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("entrega", 317, Gx_line+201, 356, Gx_line+215, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("Data de", 317, Gx_line+188, 364, Gx_line+202, 0, 0, 0, 0) ;
                  getPrinter().GxDrawBitMap(AV171Contratada_Logo, 0, Gx_line+0, 120, Gx_line+100) ;
                  getPrinter().GxDrawText("SS", 0, Gx_line+201, 61, Gx_line+215, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("Data", 267, Gx_line+201, 317, Gx_line+215, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("Refer�ncia", 50, Gx_line+201, 172, Gx_line+215, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("Coord.", 367, Gx_line+201, 402, Gx_line+215, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("Sistema", 450, Gx_line+201, 511, Gx_line+215, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("PF Final", 917, Gx_line+188, 959, Gx_line+202, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("Valor", 967, Gx_line+188, 997, Gx_line+202, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("Valor Final", 1017, Gx_line+188, 1075, Gx_line+202, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("Demanda", 625, Gx_line+201, 700, Gx_line+215, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("a Faturar", 917, Gx_line+201, 962, Gx_line+215, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("Unit�rio", 967, Gx_line+201, 1006, Gx_line+215, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("a Faturar", 1017, Gx_line+201, 1062, Gx_line+215, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("Demanda", 175, Gx_line+201, 236, Gx_line+215, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("a", 949, Gx_line+72, 956, Gx_line+86, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV119NumeroRelatorio, "")), 158, Gx_line+57, 263, Gx_line+72, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV9Contrato_Numero, "")), 377, Gx_line+57, 482, Gx_line+72, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("a", 949, Gx_line+57, 956, Gx_line+71, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("Referente ao Per�odo entre o Recebimento e a Entrega das Demandas", 492, Gx_line+57, 868, Gx_line+71, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("Contrato N�", 310, Gx_line+57, 368, Gx_line+71, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("Qtd de PF Contados", 575, Gx_line+143, 676, Gx_line+157, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("Qtd de Demandas", 325, Gx_line+143, 416, Gx_line+157, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("Referente ao Per�odo de Entrega das Demandas", 492, Gx_line+73, 759, Gx_line+87, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV128SubTitulo, "")), 498, Gx_line+34, 593, Gx_line+52, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("Presta��o de servi�os de inform�tica na aferi��o do dimensionamento de softwares, conforme a t�cnica de contagem de Pontos por Fun��o (APF).", 135, Gx_line+92, 1049, Gx_line+110, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV170DataInicioEnt, "99/99/99"), 883, Gx_line+72, 936, Gx_line+90, 2+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV169DataFimEnt, "99/99/99"), 967, Gx_line+72, 1020, Gx_line+90, 2+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV102DataFim, "99/99/99"), 967, Gx_line+57, 1020, Gx_line+75, 2+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV103DataInicio, "99/99/99"), 883, Gx_line+57, 936, Gx_line+75, 2+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+223);
                  if ( AV117i > AV104DescricaoLength * 1.1m )
                  {
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A509ContagemrResultado_SistemaSigla, "@!")), 450, Gx_line+0, 624, Gx_line+15, 0, 0, 0, 0) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV127Sistema_Coordenacao, "@!")), 367, Gx_line+0, 444, Gx_line+15, 0, 0, 0, 0) ;
                     getPrinter().GxDrawText(context.localUtil.Format( AV161Entregue, "99/99/99"), 317, Gx_line+0, 366, Gx_line+15, 1+256, 0, 0, 0) ;
                     getPrinter().GxDrawText(context.localUtil.Format( A471ContagemResultado_DataDmn, "99/99/99"), 264, Gx_line+0, 313, Gx_line+15, 1+256, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+15);
                     /* Noskip command */
                     Gx_line = Gx_OldLine;
                  }
               }
               else
               {
                  getPrinter().GxDrawRect(1, Gx_line+164, 1074, Gx_line+185, 1, 0, 0, 0, 1, 245, 245, 245, 0, 0, 0, 0, 0, 0, 0, 0) ;
                  getPrinter().GxDrawRect(0, Gx_line+116, 1073, Gx_line+137, 1, 0, 0, 0, 1, 245, 245, 245, 0, 0, 0, 0, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(340, Gx_line+218, 388, Gx_line+218, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(1, Gx_line+218, 245, Gx_line+218, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(393, Gx_line+218, 441, Gx_line+218, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(451, Gx_line+218, 620, Gx_line+218, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(626, Gx_line+218, 913, Gx_line+218, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(918, Gx_line+218, 963, Gx_line+218, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(968, Gx_line+218, 1009, Gx_line+218, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(1018, Gx_line+218, 1071, Gx_line+218, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(251, Gx_line+218, 334, Gx_line+218, 1, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Calibri", 9, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV64ContagemResultado_PFTotal, "ZZ,ZZZ,ZZ9.999")), 701, Gx_line+143, 790, Gx_line+159, 2+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV125Quantidade), "ZZ9")), 451, Gx_line+143, 471, Gx_line+159, 2+256, 0, 0, 0) ;
                  getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("SUM�RIO A FATURAR", 476, Gx_line+117, 615, Gx_line+136, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("Total Bruto R$", 429, Gx_line+166, 533, Gx_line+185, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV65ContagemResultado_PFTotalFaturar, "ZZZ,ZZZ,ZZZ,ZZ9.99")), 565, Gx_line+165, 673, Gx_line+185, 1, 0, 0, 0) ;
                  getPrinter().GxAttris("Calibri", 16, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Relat�rio Gerencial", 433, Gx_line+8, 658, Gx_line+36, 1, 0, 0, 0) ;
                  getPrinter().GxDrawBitMap(AV171Contratada_Logo, 0, Gx_line+0, 120, Gx_line+100) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("a", 940, Gx_line+74, 947, Gx_line+88, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("Referente ao Per�odo de Entrega das Demandas", 492, Gx_line+74, 758, Gx_line+88, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("Contrato N�", 309, Gx_line+59, 367, Gx_line+73, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("Referente ao Per�odo entre o Recebimento e a Entrega das Demandas", 492, Gx_line+59, 868, Gx_line+73, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("a", 940, Gx_line+59, 947, Gx_line+73, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV9Contrato_Numero, "")), 375, Gx_line+59, 480, Gx_line+74, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV119NumeroRelatorio, "")), 159, Gx_line+59, 264, Gx_line+74, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("Qtd de Demandas", 326, Gx_line+143, 417, Gx_line+157, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("Qtd de PF Contados", 576, Gx_line+143, 677, Gx_line+157, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("Data", 339, Gx_line+200, 386, Gx_line+214, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("Data de", 396, Gx_line+188, 457, Gx_line+202, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("entrega", 396, Gx_line+200, 435, Gx_line+214, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("Demanda / SS", 1, Gx_line+200, 123, Gx_line+214, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("Sistema", 450, Gx_line+200, 511, Gx_line+214, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("PF Final", 918, Gx_line+188, 960, Gx_line+202, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("Valor", 976, Gx_line+188, 1006, Gx_line+202, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("Valor Final", 1018, Gx_line+188, 1076, Gx_line+202, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("Demanda", 623, Gx_line+200, 698, Gx_line+214, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("a Faturar", 918, Gx_line+200, 963, Gx_line+214, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("Unit�rio", 968, Gx_line+200, 1007, Gx_line+214, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("a Faturar", 1018, Gx_line+200, 1063, Gx_line+214, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("N� OS", 251, Gx_line+200, 312, Gx_line+214, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV128SubTitulo, "")), 498, Gx_line+34, 593, Gx_line+52, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV169DataFimEnt, "99/99/99"), 958, Gx_line+74, 1011, Gx_line+92, 2+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV170DataInicioEnt, "99/99/99"), 875, Gx_line+74, 928, Gx_line+92, 2+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV103DataInicio, "99/99/99"), 875, Gx_line+59, 928, Gx_line+77, 2+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV102DataFim, "99/99/99"), 958, Gx_line+59, 1011, Gx_line+77, 2+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("Presta��o de servi�os de inform�tica na aferi��o do dimensionamento de softwares, conforme a t�cnica de contagem de Pontos por Fun��o (APF).", 134, Gx_line+92, 1048, Gx_line+110, 0+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+223);
               }
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
         add_metrics1( ) ;
         add_metrics2( ) ;
         add_metrics3( ) ;
         add_metrics4( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Arial", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      protected void add_metrics1( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics2( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      protected void add_metrics3( )
      {
         getPrinter().setMetrics("Courier New", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics4( )
      {
         getPrinter().setMetrics("Calibri", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "Rel_ContagemGerencial2ArqPDF");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV133WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV146TextoLink = "";
         AV128SubTitulo = "";
         scmdbuf = "";
         P006O2_A596Lote_Codigo = new int[1] ;
         P006O2_A563Lote_Nome = new String[] {""} ;
         P006O2_A562Lote_Numero = new String[] {""} ;
         A563Lote_Nome = "";
         A562Lote_Numero = "";
         AV119NumeroRelatorio = "";
         P006O3_A490ContagemResultado_ContratadaCod = new int[1] ;
         P006O3_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P006O3_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P006O3_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P006O3_A1603ContagemResultado_CntCod = new int[1] ;
         P006O3_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P006O3_A1604ContagemResultado_CntPrpCod = new int[1] ;
         P006O3_n1604ContagemResultado_CntPrpCod = new bool[] {false} ;
         P006O3_A1605ContagemResultado_CntPrpPesCod = new int[1] ;
         P006O3_n1605ContagemResultado_CntPrpPesCod = new bool[] {false} ;
         P006O3_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P006O3_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P006O3_A40001ContagemResultado_ContratadaLogo_GXI = new String[] {""} ;
         P006O3_n40001ContagemResultado_ContratadaLogo_GXI = new bool[] {false} ;
         P006O3_A1612ContagemResultado_CntNum = new String[] {""} ;
         P006O3_n1612ContagemResultado_CntNum = new bool[] {false} ;
         P006O3_A1606ContagemResultado_CntPrpPesNom = new String[] {""} ;
         P006O3_n1606ContagemResultado_CntPrpPesNom = new bool[] {false} ;
         P006O3_A456ContagemResultado_Codigo = new int[1] ;
         P006O3_A1816ContagemResultado_ContratadaLogo = new String[] {""} ;
         P006O3_n1816ContagemResultado_ContratadaLogo = new bool[] {false} ;
         A40001ContagemResultado_ContratadaLogo_GXI = "";
         A1612ContagemResultado_CntNum = "";
         A1606ContagemResultado_CntPrpPesNom = "";
         A1816ContagemResultado_ContratadaLogo = "";
         AV9Contrato_Numero = "";
         AV141Preposto = "";
         AV171Contratada_Logo = "";
         A40000Contratada_Logo_GXI = "";
         AV143AssinadoDtHr = "";
         Gx_date = DateTime.MinValue;
         Gx_time = "";
         AV137qrCode_Image = "";
         AV179Qrcode_image_GXI = "";
         P006O5_A40002GXC3 = new DateTime[] {DateTime.MinValue} ;
         A40002GXC3 = DateTime.MinValue;
         P006O8_A40003GXC4 = new DateTime[] {DateTime.MinValue} ;
         P006O8_A40004GXC5 = new DateTime[] {DateTime.MinValue} ;
         A40003GXC4 = DateTime.MinValue;
         A40004GXC5 = DateTime.MinValue;
         AV103DataInicio = DateTime.MinValue;
         AV102DataFim = DateTime.MinValue;
         AV170DataInicioEnt = DateTime.MinValue;
         AV169DataFimEnt = DateTime.MinValue;
         P006O9_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P006O9_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P006O9_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P006O9_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P006O9_A1854ContagemResultado_VlrCnc = new decimal[1] ;
         P006O9_n1854ContagemResultado_VlrCnc = new bool[] {false} ;
         P006O9_A1051ContagemResultado_GlsValor = new decimal[1] ;
         P006O9_n1051ContagemResultado_GlsValor = new bool[] {false} ;
         P006O9_A456ContagemResultado_Codigo = new int[1] ;
         P006O9_A512ContagemResultado_ValorPF = new decimal[1] ;
         P006O9_n512ContagemResultado_ValorPF = new bool[] {false} ;
         A484ContagemResultado_StatusDmn = "";
         P006O10_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P006O10_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P006O10_A1452ContagemResultado_SS = new int[1] ;
         P006O10_n1452ContagemResultado_SS = new bool[] {false} ;
         P006O10_A457ContagemResultado_Demanda = new String[] {""} ;
         P006O10_n457ContagemResultado_Demanda = new bool[] {false} ;
         P006O10_A515ContagemResultado_SistemaCoord = new String[] {""} ;
         P006O10_n515ContagemResultado_SistemaCoord = new bool[] {false} ;
         P006O10_A489ContagemResultado_SistemaCod = new int[1] ;
         P006O10_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P006O10_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P006O10_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P006O10_A456ContagemResultado_Codigo = new int[1] ;
         A457ContagemResultado_Demanda = "";
         A515ContagemResultado_SistemaCoord = "";
         A493ContagemResultado_DemandaFM = "";
         AV159ContagemResultado_SS = 0;
         AV127Sistema_Coordenacao = "";
         AV122Os = "";
         AV158SS = "";
         AV38ContagemResultado_DemandaFM = "";
         P006O11_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P006O11_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P006O11_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P006O11_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P006O11_A457ContagemResultado_Demanda = new String[] {""} ;
         P006O11_n457ContagemResultado_Demanda = new bool[] {false} ;
         P006O11_A515ContagemResultado_SistemaCoord = new String[] {""} ;
         P006O11_n515ContagemResultado_SistemaCoord = new bool[] {false} ;
         P006O11_A489ContagemResultado_SistemaCod = new int[1] ;
         P006O11_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P006O11_A1452ContagemResultado_SS = new int[1] ;
         P006O11_n1452ContagemResultado_SS = new bool[] {false} ;
         P006O11_A456ContagemResultado_Codigo = new int[1] ;
         AV137qrCode_Image = "";
         P006O13_A489ContagemResultado_SistemaCod = new int[1] ;
         P006O13_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P006O13_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P006O13_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P006O13_A1452ContagemResultado_SS = new int[1] ;
         P006O13_n1452ContagemResultado_SS = new bool[] {false} ;
         P006O13_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P006O13_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P006O13_A1854ContagemResultado_VlrCnc = new decimal[1] ;
         P006O13_n1854ContagemResultado_VlrCnc = new bool[] {false} ;
         P006O13_A494ContagemResultado_Descricao = new String[] {""} ;
         P006O13_n494ContagemResultado_Descricao = new bool[] {false} ;
         P006O13_A490ContagemResultado_ContratadaCod = new int[1] ;
         P006O13_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P006O13_A1237ContagemResultado_PrazoMaisDias = new short[1] ;
         P006O13_n1237ContagemResultado_PrazoMaisDias = new bool[] {false} ;
         P006O13_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         P006O13_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         P006O13_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P006O13_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P006O13_A1349ContagemResultado_DataExecucao = new DateTime[] {DateTime.MinValue} ;
         P006O13_n1349ContagemResultado_DataExecucao = new bool[] {false} ;
         P006O13_A1051ContagemResultado_GlsValor = new decimal[1] ;
         P006O13_n1051ContagemResultado_GlsValor = new bool[] {false} ;
         P006O13_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P006O13_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P006O13_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P006O13_A1050ContagemResultado_GlsDescricao = new String[] {""} ;
         P006O13_n1050ContagemResultado_GlsDescricao = new bool[] {false} ;
         P006O13_A1049ContagemResultado_GlsData = new DateTime[] {DateTime.MinValue} ;
         P006O13_n1049ContagemResultado_GlsData = new bool[] {false} ;
         P006O13_A457ContagemResultado_Demanda = new String[] {""} ;
         P006O13_n457ContagemResultado_Demanda = new bool[] {false} ;
         P006O13_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         P006O13_n566ContagemResultado_DataUltCnt = new bool[] {false} ;
         P006O13_A456ContagemResultado_Codigo = new int[1] ;
         P006O13_A512ContagemResultado_ValorPF = new decimal[1] ;
         P006O13_n512ContagemResultado_ValorPF = new bool[] {false} ;
         A494ContagemResultado_Descricao = "";
         A1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         A1349ContagemResultado_DataExecucao = (DateTime)(DateTime.MinValue);
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A509ContagemrResultado_SistemaSigla = "";
         A1050ContagemResultado_GlsDescricao = "";
         A1049ContagemResultado_GlsData = DateTime.MinValue;
         A566ContagemResultado_DataUltCnt = DateTime.MinValue;
         AV101DataCnt = DateTime.MinValue;
         AV149DmnDescricao = "";
         AV161Entregue = DateTime.MinValue;
         AV165Previsto = DateTime.MinValue;
         AV155strPFFinal = "";
         AV148Descricao = "";
         AV150GlsDescricao = "";
         AV156strPFTotal = "";
         AV157strPFTotalFaturar = "";
         P006O16_A489ContagemResultado_SistemaCod = new int[1] ;
         P006O16_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P006O16_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P006O16_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P006O16_A1452ContagemResultado_SS = new int[1] ;
         P006O16_n1452ContagemResultado_SS = new bool[] {false} ;
         P006O16_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P006O16_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P006O16_A1854ContagemResultado_VlrCnc = new decimal[1] ;
         P006O16_n1854ContagemResultado_VlrCnc = new bool[] {false} ;
         P006O16_A494ContagemResultado_Descricao = new String[] {""} ;
         P006O16_n494ContagemResultado_Descricao = new bool[] {false} ;
         P006O16_A490ContagemResultado_ContratadaCod = new int[1] ;
         P006O16_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P006O16_A1237ContagemResultado_PrazoMaisDias = new short[1] ;
         P006O16_n1237ContagemResultado_PrazoMaisDias = new bool[] {false} ;
         P006O16_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         P006O16_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         P006O16_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P006O16_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P006O16_A1349ContagemResultado_DataExecucao = new DateTime[] {DateTime.MinValue} ;
         P006O16_n1349ContagemResultado_DataExecucao = new bool[] {false} ;
         P006O16_A1051ContagemResultado_GlsValor = new decimal[1] ;
         P006O16_n1051ContagemResultado_GlsValor = new bool[] {false} ;
         P006O16_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P006O16_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P006O16_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P006O16_A1050ContagemResultado_GlsDescricao = new String[] {""} ;
         P006O16_n1050ContagemResultado_GlsDescricao = new bool[] {false} ;
         P006O16_A1049ContagemResultado_GlsData = new DateTime[] {DateTime.MinValue} ;
         P006O16_n1049ContagemResultado_GlsData = new bool[] {false} ;
         P006O16_A457ContagemResultado_Demanda = new String[] {""} ;
         P006O16_n457ContagemResultado_Demanda = new bool[] {false} ;
         P006O16_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         P006O16_n566ContagemResultado_DataUltCnt = new bool[] {false} ;
         P006O16_A456ContagemResultado_Codigo = new int[1] ;
         P006O16_A512ContagemResultado_ValorPF = new decimal[1] ;
         P006O16_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P006O19_A489ContagemResultado_SistemaCod = new int[1] ;
         P006O19_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P006O19_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P006O19_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P006O19_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P006O19_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P006O19_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P006O19_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P006O19_A1854ContagemResultado_VlrCnc = new decimal[1] ;
         P006O19_n1854ContagemResultado_VlrCnc = new bool[] {false} ;
         P006O19_A494ContagemResultado_Descricao = new String[] {""} ;
         P006O19_n494ContagemResultado_Descricao = new bool[] {false} ;
         P006O19_A490ContagemResultado_ContratadaCod = new int[1] ;
         P006O19_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P006O19_A1237ContagemResultado_PrazoMaisDias = new short[1] ;
         P006O19_n1237ContagemResultado_PrazoMaisDias = new bool[] {false} ;
         P006O19_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         P006O19_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         P006O19_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P006O19_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P006O19_A1349ContagemResultado_DataExecucao = new DateTime[] {DateTime.MinValue} ;
         P006O19_n1349ContagemResultado_DataExecucao = new bool[] {false} ;
         P006O19_A1051ContagemResultado_GlsValor = new decimal[1] ;
         P006O19_n1051ContagemResultado_GlsValor = new bool[] {false} ;
         P006O19_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P006O19_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P006O19_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P006O19_A1050ContagemResultado_GlsDescricao = new String[] {""} ;
         P006O19_n1050ContagemResultado_GlsDescricao = new bool[] {false} ;
         P006O19_A1049ContagemResultado_GlsData = new DateTime[] {DateTime.MinValue} ;
         P006O19_n1049ContagemResultado_GlsData = new bool[] {false} ;
         P006O19_A457ContagemResultado_Demanda = new String[] {""} ;
         P006O19_n457ContagemResultado_Demanda = new bool[] {false} ;
         P006O19_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         P006O19_n566ContagemResultado_DataUltCnt = new bool[] {false} ;
         P006O19_A456ContagemResultado_Codigo = new int[1] ;
         P006O19_A512ContagemResultado_ValorPF = new decimal[1] ;
         P006O19_n512ContagemResultado_ValorPF = new bool[] {false} ;
         AV171Contratada_Logo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.rel_contagemgerencial2arqpdf__default(),
            new Object[][] {
                new Object[] {
               P006O2_A596Lote_Codigo, P006O2_A563Lote_Nome, P006O2_A562Lote_Numero
               }
               , new Object[] {
               P006O3_A490ContagemResultado_ContratadaCod, P006O3_n490ContagemResultado_ContratadaCod, P006O3_A1553ContagemResultado_CntSrvCod, P006O3_n1553ContagemResultado_CntSrvCod, P006O3_A1603ContagemResultado_CntCod, P006O3_n1603ContagemResultado_CntCod, P006O3_A1604ContagemResultado_CntPrpCod, P006O3_n1604ContagemResultado_CntPrpCod, P006O3_A1605ContagemResultado_CntPrpPesCod, P006O3_n1605ContagemResultado_CntPrpPesCod,
               P006O3_A597ContagemResultado_LoteAceiteCod, P006O3_n597ContagemResultado_LoteAceiteCod, P006O3_A40001ContagemResultado_ContratadaLogo_GXI, P006O3_n40001ContagemResultado_ContratadaLogo_GXI, P006O3_A1612ContagemResultado_CntNum, P006O3_n1612ContagemResultado_CntNum, P006O3_A1606ContagemResultado_CntPrpPesNom, P006O3_n1606ContagemResultado_CntPrpPesNom, P006O3_A456ContagemResultado_Codigo, P006O3_A1816ContagemResultado_ContratadaLogo,
               P006O3_n1816ContagemResultado_ContratadaLogo
               }
               , new Object[] {
               P006O5_A40002GXC3
               }
               , new Object[] {
               P006O8_A40003GXC4, P006O8_A40004GXC5
               }
               , new Object[] {
               P006O9_A597ContagemResultado_LoteAceiteCod, P006O9_n597ContagemResultado_LoteAceiteCod, P006O9_A484ContagemResultado_StatusDmn, P006O9_n484ContagemResultado_StatusDmn, P006O9_A1854ContagemResultado_VlrCnc, P006O9_n1854ContagemResultado_VlrCnc, P006O9_A1051ContagemResultado_GlsValor, P006O9_n1051ContagemResultado_GlsValor, P006O9_A456ContagemResultado_Codigo, P006O9_A512ContagemResultado_ValorPF,
               P006O9_n512ContagemResultado_ValorPF
               }
               , new Object[] {
               P006O10_A597ContagemResultado_LoteAceiteCod, P006O10_n597ContagemResultado_LoteAceiteCod, P006O10_A1452ContagemResultado_SS, P006O10_n1452ContagemResultado_SS, P006O10_A457ContagemResultado_Demanda, P006O10_n457ContagemResultado_Demanda, P006O10_A515ContagemResultado_SistemaCoord, P006O10_n515ContagemResultado_SistemaCoord, P006O10_A489ContagemResultado_SistemaCod, P006O10_n489ContagemResultado_SistemaCod,
               P006O10_A493ContagemResultado_DemandaFM, P006O10_n493ContagemResultado_DemandaFM, P006O10_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P006O11_A597ContagemResultado_LoteAceiteCod, P006O11_n597ContagemResultado_LoteAceiteCod, P006O11_A493ContagemResultado_DemandaFM, P006O11_n493ContagemResultado_DemandaFM, P006O11_A457ContagemResultado_Demanda, P006O11_n457ContagemResultado_Demanda, P006O11_A515ContagemResultado_SistemaCoord, P006O11_n515ContagemResultado_SistemaCoord, P006O11_A489ContagemResultado_SistemaCod, P006O11_n489ContagemResultado_SistemaCod,
               P006O11_A1452ContagemResultado_SS, P006O11_n1452ContagemResultado_SS, P006O11_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P006O13_A489ContagemResultado_SistemaCod, P006O13_n489ContagemResultado_SistemaCod, P006O13_A597ContagemResultado_LoteAceiteCod, P006O13_n597ContagemResultado_LoteAceiteCod, P006O13_A1452ContagemResultado_SS, P006O13_n1452ContagemResultado_SS, P006O13_A484ContagemResultado_StatusDmn, P006O13_n484ContagemResultado_StatusDmn, P006O13_A1854ContagemResultado_VlrCnc, P006O13_n1854ContagemResultado_VlrCnc,
               P006O13_A494ContagemResultado_Descricao, P006O13_n494ContagemResultado_Descricao, P006O13_A490ContagemResultado_ContratadaCod, P006O13_n490ContagemResultado_ContratadaCod, P006O13_A1237ContagemResultado_PrazoMaisDias, P006O13_n1237ContagemResultado_PrazoMaisDias, P006O13_A1227ContagemResultado_PrazoInicialDias, P006O13_n1227ContagemResultado_PrazoInicialDias, P006O13_A1351ContagemResultado_DataPrevista, P006O13_n1351ContagemResultado_DataPrevista,
               P006O13_A1349ContagemResultado_DataExecucao, P006O13_n1349ContagemResultado_DataExecucao, P006O13_A1051ContagemResultado_GlsValor, P006O13_n1051ContagemResultado_GlsValor, P006O13_A471ContagemResultado_DataDmn, P006O13_A509ContagemrResultado_SistemaSigla, P006O13_n509ContagemrResultado_SistemaSigla, P006O13_A1050ContagemResultado_GlsDescricao, P006O13_n1050ContagemResultado_GlsDescricao, P006O13_A1049ContagemResultado_GlsData,
               P006O13_n1049ContagemResultado_GlsData, P006O13_A457ContagemResultado_Demanda, P006O13_n457ContagemResultado_Demanda, P006O13_A566ContagemResultado_DataUltCnt, P006O13_n566ContagemResultado_DataUltCnt, P006O13_A456ContagemResultado_Codigo, P006O13_A512ContagemResultado_ValorPF, P006O13_n512ContagemResultado_ValorPF
               }
               , new Object[] {
               }
               , new Object[] {
               P006O16_A489ContagemResultado_SistemaCod, P006O16_n489ContagemResultado_SistemaCod, P006O16_A597ContagemResultado_LoteAceiteCod, P006O16_n597ContagemResultado_LoteAceiteCod, P006O16_A1452ContagemResultado_SS, P006O16_n1452ContagemResultado_SS, P006O16_A484ContagemResultado_StatusDmn, P006O16_n484ContagemResultado_StatusDmn, P006O16_A1854ContagemResultado_VlrCnc, P006O16_n1854ContagemResultado_VlrCnc,
               P006O16_A494ContagemResultado_Descricao, P006O16_n494ContagemResultado_Descricao, P006O16_A490ContagemResultado_ContratadaCod, P006O16_n490ContagemResultado_ContratadaCod, P006O16_A1237ContagemResultado_PrazoMaisDias, P006O16_n1237ContagemResultado_PrazoMaisDias, P006O16_A1227ContagemResultado_PrazoInicialDias, P006O16_n1227ContagemResultado_PrazoInicialDias, P006O16_A1351ContagemResultado_DataPrevista, P006O16_n1351ContagemResultado_DataPrevista,
               P006O16_A1349ContagemResultado_DataExecucao, P006O16_n1349ContagemResultado_DataExecucao, P006O16_A1051ContagemResultado_GlsValor, P006O16_n1051ContagemResultado_GlsValor, P006O16_A471ContagemResultado_DataDmn, P006O16_A509ContagemrResultado_SistemaSigla, P006O16_n509ContagemrResultado_SistemaSigla, P006O16_A1050ContagemResultado_GlsDescricao, P006O16_n1050ContagemResultado_GlsDescricao, P006O16_A1049ContagemResultado_GlsData,
               P006O16_n1049ContagemResultado_GlsData, P006O16_A457ContagemResultado_Demanda, P006O16_n457ContagemResultado_Demanda, P006O16_A566ContagemResultado_DataUltCnt, P006O16_n566ContagemResultado_DataUltCnt, P006O16_A456ContagemResultado_Codigo, P006O16_A512ContagemResultado_ValorPF, P006O16_n512ContagemResultado_ValorPF
               }
               , new Object[] {
               }
               , new Object[] {
               P006O19_A489ContagemResultado_SistemaCod, P006O19_n489ContagemResultado_SistemaCod, P006O19_A597ContagemResultado_LoteAceiteCod, P006O19_n597ContagemResultado_LoteAceiteCod, P006O19_A493ContagemResultado_DemandaFM, P006O19_n493ContagemResultado_DemandaFM, P006O19_A484ContagemResultado_StatusDmn, P006O19_n484ContagemResultado_StatusDmn, P006O19_A1854ContagemResultado_VlrCnc, P006O19_n1854ContagemResultado_VlrCnc,
               P006O19_A494ContagemResultado_Descricao, P006O19_n494ContagemResultado_Descricao, P006O19_A490ContagemResultado_ContratadaCod, P006O19_n490ContagemResultado_ContratadaCod, P006O19_A1237ContagemResultado_PrazoMaisDias, P006O19_n1237ContagemResultado_PrazoMaisDias, P006O19_A1227ContagemResultado_PrazoInicialDias, P006O19_n1227ContagemResultado_PrazoInicialDias, P006O19_A1351ContagemResultado_DataPrevista, P006O19_n1351ContagemResultado_DataPrevista,
               P006O19_A1349ContagemResultado_DataExecucao, P006O19_n1349ContagemResultado_DataExecucao, P006O19_A1051ContagemResultado_GlsValor, P006O19_n1051ContagemResultado_GlsValor, P006O19_A471ContagemResultado_DataDmn, P006O19_A509ContagemrResultado_SistemaSigla, P006O19_n509ContagemrResultado_SistemaSigla, P006O19_A1050ContagemResultado_GlsDescricao, P006O19_n1050ContagemResultado_GlsDescricao, P006O19_A1049ContagemResultado_GlsData,
               P006O19_n1049ContagemResultado_GlsData, P006O19_A457ContagemResultado_Demanda, P006O19_n457ContagemResultado_Demanda, P006O19_A566ContagemResultado_DataUltCnt, P006O19_n566ContagemResultado_DataUltCnt, P006O19_A456ContagemResultado_Codigo, P006O19_A512ContagemResultado_ValorPF, P006O19_n512ContagemResultado_ValorPF
               }
               , new Object[] {
               }
            }
         );
         Gx_time = context.localUtil.Time( );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_line = 0;
         Gx_time = context.localUtil.Time( );
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      private short AV104DescricaoLength ;
      private short AV168DescricaoGlsLength ;
      private short AV125Quantidade ;
      private short AV117i ;
      private short A1237ContagemResultado_PrazoMaisDias ;
      private short A1227ContagemResultado_PrazoInicialDias ;
      private short AV166Prazo ;
      private short AV151p ;
      private short AV152p2 ;
      private int AV98Contratada_AreaTrabalhoCod ;
      private int AV40ContagemResultado_LoteAceite ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int GXt_int2 ;
      private int A596Lote_Codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1603ContagemResultado_CntCod ;
      private int A1604ContagemResultado_CntPrpCod ;
      private int A1605ContagemResultado_CntPrpPesCod ;
      private int A597ContagemResultado_LoteAceiteCod ;
      private int A456ContagemResultado_Codigo ;
      private int A1452ContagemResultado_SS ;
      private int A489ContagemResultado_SistemaCod ;
      private int OV159ContagemResultado_SS ;
      private int AV159ContagemResultado_SS ;
      private int AV126sistema_Codigo ;
      private int Gx_OldLine ;
      private int AV163Codigo ;
      private int AV164Contratada ;
      private decimal AV100d ;
      private decimal A1854ContagemResultado_VlrCnc ;
      private decimal A1051ContagemResultado_GlsValor ;
      private decimal A512ContagemResultado_ValorPF ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal A606ContagemResultado_ValorFinal ;
      private decimal AV63ContagemResultado_PFFaturar ;
      private decimal AV64ContagemResultado_PFTotal ;
      private decimal AV65ContagemResultado_PFTotalFaturar ;
      private decimal AV131TotalPontos ;
      private decimal AV130TotalFinal ;
      private decimal AV162GlsValor ;
      private decimal AV153GlsPF ;
      private decimal AV129TotalFaturar ;
      private decimal GXt_decimal3 ;
      private String AV144Verificador ;
      private String AV145FileName ;
      private String AV146TextoLink ;
      private String AV128SubTitulo ;
      private String scmdbuf ;
      private String A563Lote_Nome ;
      private String A562Lote_Numero ;
      private String AV119NumeroRelatorio ;
      private String A1612ContagemResultado_CntNum ;
      private String A1606ContagemResultado_CntPrpPesNom ;
      private String AV9Contrato_Numero ;
      private String AV141Preposto ;
      private String AV143AssinadoDtHr ;
      private String Gx_time ;
      private String A484ContagemResultado_StatusDmn ;
      private String AV122Os ;
      private String AV158SS ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String AV155strPFFinal ;
      private String AV148Descricao ;
      private String AV156strPFTotal ;
      private String AV157strPFTotalFaturar ;
      private DateTime A1351ContagemResultado_DataPrevista ;
      private DateTime A1349ContagemResultado_DataExecucao ;
      private DateTime Gx_date ;
      private DateTime A40002GXC3 ;
      private DateTime A40003GXC4 ;
      private DateTime A40004GXC5 ;
      private DateTime AV103DataInicio ;
      private DateTime AV102DataFim ;
      private DateTime AV170DataInicioEnt ;
      private DateTime AV169DataFimEnt ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime A1049ContagemResultado_GlsData ;
      private DateTime A566ContagemResultado_DataUltCnt ;
      private DateTime AV101DataCnt ;
      private DateTime AV161Entregue ;
      private DateTime AV165Previsto ;
      private bool AV123OSAutomatica ;
      private bool GXt_boolean1 ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n1604ContagemResultado_CntPrpCod ;
      private bool n1605ContagemResultado_CntPrpPesCod ;
      private bool n597ContagemResultado_LoteAceiteCod ;
      private bool n40001ContagemResultado_ContratadaLogo_GXI ;
      private bool n1612ContagemResultado_CntNum ;
      private bool n1606ContagemResultado_CntPrpPesNom ;
      private bool n1816ContagemResultado_ContratadaLogo ;
      private bool returnInSub ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n1854ContagemResultado_VlrCnc ;
      private bool n1051ContagemResultado_GlsValor ;
      private bool n512ContagemResultado_ValorPF ;
      private bool BRK6O7 ;
      private bool n1452ContagemResultado_SS ;
      private bool n457ContagemResultado_Demanda ;
      private bool n515ContagemResultado_SistemaCoord ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool BRK6O9 ;
      private bool n494ContagemResultado_Descricao ;
      private bool n1237ContagemResultado_PrazoMaisDias ;
      private bool n1227ContagemResultado_PrazoInicialDias ;
      private bool n1351ContagemResultado_DataPrevista ;
      private bool n1349ContagemResultado_DataExecucao ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool n1050ContagemResultado_GlsDescricao ;
      private bool n1049ContagemResultado_GlsData ;
      private bool n566ContagemResultado_DataUltCnt ;
      private String AV134QrCodePath ;
      private String A1050ContagemResultado_GlsDescricao ;
      private String AV150GlsDescricao ;
      private String AV136QrCodeUrl ;
      private String A40001ContagemResultado_ContratadaLogo_GXI ;
      private String A40000Contratada_Logo_GXI ;
      private String AV179Qrcode_image_GXI ;
      private String A457ContagemResultado_Demanda ;
      private String A515ContagemResultado_SistemaCoord ;
      private String A493ContagemResultado_DemandaFM ;
      private String AV127Sistema_Coordenacao ;
      private String AV38ContagemResultado_DemandaFM ;
      private String A494ContagemResultado_Descricao ;
      private String AV149DmnDescricao ;
      private String A1816ContagemResultado_ContratadaLogo ;
      private String AV171Contratada_Logo ;
      private String AV137qrCode_Image ;
      private String Qrcode_image ;
      private String Contratada_logo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P006O2_A596Lote_Codigo ;
      private String[] P006O2_A563Lote_Nome ;
      private String[] P006O2_A562Lote_Numero ;
      private int[] P006O3_A490ContagemResultado_ContratadaCod ;
      private bool[] P006O3_n490ContagemResultado_ContratadaCod ;
      private int[] P006O3_A1553ContagemResultado_CntSrvCod ;
      private bool[] P006O3_n1553ContagemResultado_CntSrvCod ;
      private int[] P006O3_A1603ContagemResultado_CntCod ;
      private bool[] P006O3_n1603ContagemResultado_CntCod ;
      private int[] P006O3_A1604ContagemResultado_CntPrpCod ;
      private bool[] P006O3_n1604ContagemResultado_CntPrpCod ;
      private int[] P006O3_A1605ContagemResultado_CntPrpPesCod ;
      private bool[] P006O3_n1605ContagemResultado_CntPrpPesCod ;
      private int[] P006O3_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P006O3_n597ContagemResultado_LoteAceiteCod ;
      private String[] P006O3_A40001ContagemResultado_ContratadaLogo_GXI ;
      private bool[] P006O3_n40001ContagemResultado_ContratadaLogo_GXI ;
      private String[] P006O3_A1612ContagemResultado_CntNum ;
      private bool[] P006O3_n1612ContagemResultado_CntNum ;
      private String[] P006O3_A1606ContagemResultado_CntPrpPesNom ;
      private bool[] P006O3_n1606ContagemResultado_CntPrpPesNom ;
      private int[] P006O3_A456ContagemResultado_Codigo ;
      private String[] P006O3_A1816ContagemResultado_ContratadaLogo ;
      private bool[] P006O3_n1816ContagemResultado_ContratadaLogo ;
      private DateTime[] P006O5_A40002GXC3 ;
      private DateTime[] P006O8_A40003GXC4 ;
      private DateTime[] P006O8_A40004GXC5 ;
      private int[] P006O9_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P006O9_n597ContagemResultado_LoteAceiteCod ;
      private String[] P006O9_A484ContagemResultado_StatusDmn ;
      private bool[] P006O9_n484ContagemResultado_StatusDmn ;
      private decimal[] P006O9_A1854ContagemResultado_VlrCnc ;
      private bool[] P006O9_n1854ContagemResultado_VlrCnc ;
      private decimal[] P006O9_A1051ContagemResultado_GlsValor ;
      private bool[] P006O9_n1051ContagemResultado_GlsValor ;
      private int[] P006O9_A456ContagemResultado_Codigo ;
      private decimal[] P006O9_A512ContagemResultado_ValorPF ;
      private bool[] P006O9_n512ContagemResultado_ValorPF ;
      private int[] P006O10_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P006O10_n597ContagemResultado_LoteAceiteCod ;
      private int[] P006O10_A1452ContagemResultado_SS ;
      private bool[] P006O10_n1452ContagemResultado_SS ;
      private String[] P006O10_A457ContagemResultado_Demanda ;
      private bool[] P006O10_n457ContagemResultado_Demanda ;
      private String[] P006O10_A515ContagemResultado_SistemaCoord ;
      private bool[] P006O10_n515ContagemResultado_SistemaCoord ;
      private int[] P006O10_A489ContagemResultado_SistemaCod ;
      private bool[] P006O10_n489ContagemResultado_SistemaCod ;
      private String[] P006O10_A493ContagemResultado_DemandaFM ;
      private bool[] P006O10_n493ContagemResultado_DemandaFM ;
      private int[] P006O10_A456ContagemResultado_Codigo ;
      private int[] P006O11_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P006O11_n597ContagemResultado_LoteAceiteCod ;
      private String[] P006O11_A493ContagemResultado_DemandaFM ;
      private bool[] P006O11_n493ContagemResultado_DemandaFM ;
      private String[] P006O11_A457ContagemResultado_Demanda ;
      private bool[] P006O11_n457ContagemResultado_Demanda ;
      private String[] P006O11_A515ContagemResultado_SistemaCoord ;
      private bool[] P006O11_n515ContagemResultado_SistemaCoord ;
      private int[] P006O11_A489ContagemResultado_SistemaCod ;
      private bool[] P006O11_n489ContagemResultado_SistemaCod ;
      private int[] P006O11_A1452ContagemResultado_SS ;
      private bool[] P006O11_n1452ContagemResultado_SS ;
      private int[] P006O11_A456ContagemResultado_Codigo ;
      private int[] P006O13_A489ContagemResultado_SistemaCod ;
      private bool[] P006O13_n489ContagemResultado_SistemaCod ;
      private int[] P006O13_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P006O13_n597ContagemResultado_LoteAceiteCod ;
      private int[] P006O13_A1452ContagemResultado_SS ;
      private bool[] P006O13_n1452ContagemResultado_SS ;
      private String[] P006O13_A484ContagemResultado_StatusDmn ;
      private bool[] P006O13_n484ContagemResultado_StatusDmn ;
      private decimal[] P006O13_A1854ContagemResultado_VlrCnc ;
      private bool[] P006O13_n1854ContagemResultado_VlrCnc ;
      private String[] P006O13_A494ContagemResultado_Descricao ;
      private bool[] P006O13_n494ContagemResultado_Descricao ;
      private int[] P006O13_A490ContagemResultado_ContratadaCod ;
      private bool[] P006O13_n490ContagemResultado_ContratadaCod ;
      private short[] P006O13_A1237ContagemResultado_PrazoMaisDias ;
      private bool[] P006O13_n1237ContagemResultado_PrazoMaisDias ;
      private short[] P006O13_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] P006O13_n1227ContagemResultado_PrazoInicialDias ;
      private DateTime[] P006O13_A1351ContagemResultado_DataPrevista ;
      private bool[] P006O13_n1351ContagemResultado_DataPrevista ;
      private DateTime[] P006O13_A1349ContagemResultado_DataExecucao ;
      private bool[] P006O13_n1349ContagemResultado_DataExecucao ;
      private decimal[] P006O13_A1051ContagemResultado_GlsValor ;
      private bool[] P006O13_n1051ContagemResultado_GlsValor ;
      private DateTime[] P006O13_A471ContagemResultado_DataDmn ;
      private String[] P006O13_A509ContagemrResultado_SistemaSigla ;
      private bool[] P006O13_n509ContagemrResultado_SistemaSigla ;
      private String[] P006O13_A1050ContagemResultado_GlsDescricao ;
      private bool[] P006O13_n1050ContagemResultado_GlsDescricao ;
      private DateTime[] P006O13_A1049ContagemResultado_GlsData ;
      private bool[] P006O13_n1049ContagemResultado_GlsData ;
      private String[] P006O13_A457ContagemResultado_Demanda ;
      private bool[] P006O13_n457ContagemResultado_Demanda ;
      private DateTime[] P006O13_A566ContagemResultado_DataUltCnt ;
      private bool[] P006O13_n566ContagemResultado_DataUltCnt ;
      private int[] P006O13_A456ContagemResultado_Codigo ;
      private decimal[] P006O13_A512ContagemResultado_ValorPF ;
      private bool[] P006O13_n512ContagemResultado_ValorPF ;
      private int[] P006O16_A489ContagemResultado_SistemaCod ;
      private bool[] P006O16_n489ContagemResultado_SistemaCod ;
      private int[] P006O16_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P006O16_n597ContagemResultado_LoteAceiteCod ;
      private int[] P006O16_A1452ContagemResultado_SS ;
      private bool[] P006O16_n1452ContagemResultado_SS ;
      private String[] P006O16_A484ContagemResultado_StatusDmn ;
      private bool[] P006O16_n484ContagemResultado_StatusDmn ;
      private decimal[] P006O16_A1854ContagemResultado_VlrCnc ;
      private bool[] P006O16_n1854ContagemResultado_VlrCnc ;
      private String[] P006O16_A494ContagemResultado_Descricao ;
      private bool[] P006O16_n494ContagemResultado_Descricao ;
      private int[] P006O16_A490ContagemResultado_ContratadaCod ;
      private bool[] P006O16_n490ContagemResultado_ContratadaCod ;
      private short[] P006O16_A1237ContagemResultado_PrazoMaisDias ;
      private bool[] P006O16_n1237ContagemResultado_PrazoMaisDias ;
      private short[] P006O16_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] P006O16_n1227ContagemResultado_PrazoInicialDias ;
      private DateTime[] P006O16_A1351ContagemResultado_DataPrevista ;
      private bool[] P006O16_n1351ContagemResultado_DataPrevista ;
      private DateTime[] P006O16_A1349ContagemResultado_DataExecucao ;
      private bool[] P006O16_n1349ContagemResultado_DataExecucao ;
      private decimal[] P006O16_A1051ContagemResultado_GlsValor ;
      private bool[] P006O16_n1051ContagemResultado_GlsValor ;
      private DateTime[] P006O16_A471ContagemResultado_DataDmn ;
      private String[] P006O16_A509ContagemrResultado_SistemaSigla ;
      private bool[] P006O16_n509ContagemrResultado_SistemaSigla ;
      private String[] P006O16_A1050ContagemResultado_GlsDescricao ;
      private bool[] P006O16_n1050ContagemResultado_GlsDescricao ;
      private DateTime[] P006O16_A1049ContagemResultado_GlsData ;
      private bool[] P006O16_n1049ContagemResultado_GlsData ;
      private String[] P006O16_A457ContagemResultado_Demanda ;
      private bool[] P006O16_n457ContagemResultado_Demanda ;
      private DateTime[] P006O16_A566ContagemResultado_DataUltCnt ;
      private bool[] P006O16_n566ContagemResultado_DataUltCnt ;
      private int[] P006O16_A456ContagemResultado_Codigo ;
      private decimal[] P006O16_A512ContagemResultado_ValorPF ;
      private bool[] P006O16_n512ContagemResultado_ValorPF ;
      private int[] P006O19_A489ContagemResultado_SistemaCod ;
      private bool[] P006O19_n489ContagemResultado_SistemaCod ;
      private int[] P006O19_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P006O19_n597ContagemResultado_LoteAceiteCod ;
      private String[] P006O19_A493ContagemResultado_DemandaFM ;
      private bool[] P006O19_n493ContagemResultado_DemandaFM ;
      private String[] P006O19_A484ContagemResultado_StatusDmn ;
      private bool[] P006O19_n484ContagemResultado_StatusDmn ;
      private decimal[] P006O19_A1854ContagemResultado_VlrCnc ;
      private bool[] P006O19_n1854ContagemResultado_VlrCnc ;
      private String[] P006O19_A494ContagemResultado_Descricao ;
      private bool[] P006O19_n494ContagemResultado_Descricao ;
      private int[] P006O19_A490ContagemResultado_ContratadaCod ;
      private bool[] P006O19_n490ContagemResultado_ContratadaCod ;
      private short[] P006O19_A1237ContagemResultado_PrazoMaisDias ;
      private bool[] P006O19_n1237ContagemResultado_PrazoMaisDias ;
      private short[] P006O19_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] P006O19_n1227ContagemResultado_PrazoInicialDias ;
      private DateTime[] P006O19_A1351ContagemResultado_DataPrevista ;
      private bool[] P006O19_n1351ContagemResultado_DataPrevista ;
      private DateTime[] P006O19_A1349ContagemResultado_DataExecucao ;
      private bool[] P006O19_n1349ContagemResultado_DataExecucao ;
      private decimal[] P006O19_A1051ContagemResultado_GlsValor ;
      private bool[] P006O19_n1051ContagemResultado_GlsValor ;
      private DateTime[] P006O19_A471ContagemResultado_DataDmn ;
      private String[] P006O19_A509ContagemrResultado_SistemaSigla ;
      private bool[] P006O19_n509ContagemrResultado_SistemaSigla ;
      private String[] P006O19_A1050ContagemResultado_GlsDescricao ;
      private bool[] P006O19_n1050ContagemResultado_GlsDescricao ;
      private DateTime[] P006O19_A1049ContagemResultado_GlsData ;
      private bool[] P006O19_n1049ContagemResultado_GlsData ;
      private String[] P006O19_A457ContagemResultado_Demanda ;
      private bool[] P006O19_n457ContagemResultado_Demanda ;
      private DateTime[] P006O19_A566ContagemResultado_DataUltCnt ;
      private bool[] P006O19_n566ContagemResultado_DataUltCnt ;
      private int[] P006O19_A456ContagemResultado_Codigo ;
      private decimal[] P006O19_A512ContagemResultado_ValorPF ;
      private bool[] P006O19_n512ContagemResultado_ValorPF ;
      private wwpbaseobjects.SdtWWPContext AV133WWPContext ;
   }

   public class rel_contagemgerencial2arqpdf__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new UpdateCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP006O2 ;
          prmP006O2 = new Object[] {
          new Object[] {"@AV40ContagemResultado_LoteAceite",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006O3 ;
          prmP006O3 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006O5 ;
          prmP006O5 = new Object[] {
          new Object[] {"@AV40ContagemResultado_LoteAceite",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006O8 ;
          prmP006O8 = new Object[] {
          new Object[] {"@AV40ContagemResultado_LoteAceite",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006O9 ;
          prmP006O9 = new Object[] {
          new Object[] {"@AV40ContagemResultado_LoteAceite",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006O10 ;
          prmP006O10 = new Object[] {
          new Object[] {"@AV40ContagemResultado_LoteAceite",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006O11 ;
          prmP006O11 = new Object[] {
          new Object[] {"@AV40ContagemResultado_LoteAceite",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006O13 ;
          prmP006O13 = new Object[] {
          new Object[] {"@AV40ContagemResultado_LoteAceite",SqlDbType.Int,6,0} ,
          new Object[] {"@AV159ContagemResultado_SS",SqlDbType.Int,8,0}
          } ;
          Object[] prmP006O14 ;
          prmP006O14 = new Object[] {
          new Object[] {"@ContagemResultado_DataPrevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_DataExecucao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006O16 ;
          prmP006O16 = new Object[] {
          new Object[] {"@AV40ContagemResultado_LoteAceite",SqlDbType.Int,6,0} ,
          new Object[] {"@AV159ContagemResultado_SS",SqlDbType.Int,8,0}
          } ;
          Object[] prmP006O17 ;
          prmP006O17 = new Object[] {
          new Object[] {"@ContagemResultado_DataPrevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_DataExecucao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006O19 ;
          prmP006O19 = new Object[] {
          new Object[] {"@AV40ContagemResultado_LoteAceite",SqlDbType.Int,6,0} ,
          new Object[] {"@AV38ContagemResultado_DemandaFM",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmP006O20 ;
          prmP006O20 = new Object[] {
          new Object[] {"@ContagemResultado_DataPrevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_DataExecucao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P006O2", "SELECT TOP 1 [Lote_Codigo], [Lote_Nome], [Lote_Numero] FROM [Lote] WITH (NOLOCK) WHERE [Lote_Codigo] = @AV40ContagemResultado_LoteAceite ORDER BY [Lote_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006O2,1,0,true,true )
             ,new CursorDef("P006O3", "SELECT TOP 1 T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T3.[Contrato_Codigo] AS ContagemResultado_CntCod, T4.[Contrato_PrepostoCod] AS ContagemResultado_CntPrpCod, T5.[Usuario_PessoaCod] AS ContagemResultado_CntPrpPesCod, T1.[ContagemResultado_LoteAceiteCod], T2.[Contratada_Logo_GXI] AS ContagemResultado_ContratadaLogo_GXI, T4.[Contrato_Numero] AS ContagemResultado_CntNum, T6.[Pessoa_Nome] AS ContagemResultado_CntPrpPesNom, T1.[ContagemResultado_Codigo], T2.[Contratada_Logo] AS ContagemResultado_ContratadaLogo FROM ((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contrato] T4 WITH (NOLOCK) ON T4.[Contrato_Codigo] = T3.[Contrato_Codigo]) LEFT JOIN [Usuario] T5 WITH (NOLOCK) ON T5.[Usuario_Codigo] = T4.[Contrato_PrepostoCod]) LEFT JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T5.[Usuario_PessoaCod]) WHERE T1.[ContagemResultado_LoteAceiteCod] = @Lote_Codigo ORDER BY T1.[ContagemResultado_LoteAceiteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006O3,1,0,false,true )
             ,new CursorDef("P006O5", "SELECT COALESCE( T1.[GXC3], convert( DATETIME, '17530101', 112 )) AS GXC3 FROM (SELECT MIN([ContagemResultado_DataDmn]) AS GXC3 FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_LoteAceiteCod] = @AV40ContagemResultado_LoteAceite ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006O5,1,0,true,true )
             ,new CursorDef("P006O8", "SELECT COALESCE( T1.[GXC4], convert( DATETIME, '17530101', 112 )) AS GXC4, COALESCE( T1.[GXC5], convert( DATETIME, '17530101', 112 )) AS GXC5 FROM (SELECT MAX(COALESCE( T3.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS GXC4, MIN(COALESCE( T3.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS GXC5 FROM ([ContagemResultado] T2 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T3 ON T3.[ContagemResultado_Codigo] = T2.[ContagemResultado_Codigo]) WHERE T2.[ContagemResultado_LoteAceiteCod] = @AV40ContagemResultado_LoteAceite ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006O8,1,0,true,true )
             ,new CursorDef("P006O9", "SELECT [ContagemResultado_LoteAceiteCod], [ContagemResultado_StatusDmn], [ContagemResultado_VlrCnc], [ContagemResultado_GlsValor], [ContagemResultado_Codigo], [ContagemResultado_ValorPF] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_LoteAceiteCod] = @AV40ContagemResultado_LoteAceite ORDER BY [ContagemResultado_LoteAceiteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006O9,100,0,true,false )
             ,new CursorDef("P006O10", "SELECT T1.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_SS], T1.[ContagemResultado_Demanda], T2.[Sistema_Coordenacao] AS ContagemResultado_SistemaCoord, T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Codigo] FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) WHERE T1.[ContagemResultado_LoteAceiteCod] = @AV40ContagemResultado_LoteAceite ORDER BY T1.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_SS], T1.[ContagemResultado_Demanda] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006O10,100,0,true,false )
             ,new CursorDef("P006O11", "SELECT T1.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Demanda], T2.[Sistema_Coordenacao] AS ContagemResultado_SistemaCoord, T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_SS], T1.[ContagemResultado_Codigo] FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) WHERE T1.[ContagemResultado_LoteAceiteCod] = @AV40ContagemResultado_LoteAceite ORDER BY T1.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Demanda] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006O11,100,0,true,false )
             ,new CursorDef("P006O13", "SELECT T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_SS], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_VlrCnc], T1.[ContagemResultado_Descricao], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_PrazoMaisDias], T1.[ContagemResultado_PrazoInicialDias], T1.[ContagemResultado_DataPrevista], T1.[ContagemResultado_DataExecucao], T1.[ContagemResultado_GlsValor], T1.[ContagemResultado_DataDmn], T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T1.[ContagemResultado_GlsDescricao], T1.[ContagemResultado_GlsData], T1.[ContagemResultado_Demanda], COALESCE( T3.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_ValorPF] FROM (([ContagemResultado] T1 WITH (UPDLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T3 ON T3.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE T1.[ContagemResultado_LoteAceiteCod] = @AV40ContagemResultado_LoteAceite and T1.[ContagemResultado_SS] = @AV159ContagemResultado_SS ORDER BY T1.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_SS], T1.[ContagemResultado_Demanda] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006O13,1,0,true,false )
             ,new CursorDef("P006O14", "UPDATE [ContagemResultado] SET [ContagemResultado_DataPrevista]=@ContagemResultado_DataPrevista, [ContagemResultado_DataExecucao]=@ContagemResultado_DataExecucao  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP006O14)
             ,new CursorDef("P006O16", "SELECT T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_SS], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_VlrCnc], T1.[ContagemResultado_Descricao], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_PrazoMaisDias], T1.[ContagemResultado_PrazoInicialDias], T1.[ContagemResultado_DataPrevista], T1.[ContagemResultado_DataExecucao], T1.[ContagemResultado_GlsValor], T1.[ContagemResultado_DataDmn], T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T1.[ContagemResultado_GlsDescricao], T1.[ContagemResultado_GlsData], T1.[ContagemResultado_Demanda], COALESCE( T3.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_ValorPF] FROM (([ContagemResultado] T1 WITH (UPDLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T3 ON T3.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE T1.[ContagemResultado_LoteAceiteCod] = @AV40ContagemResultado_LoteAceite and T1.[ContagemResultado_SS] = @AV159ContagemResultado_SS ORDER BY T1.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_SS], T1.[ContagemResultado_Demanda] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006O16,1,0,true,false )
             ,new CursorDef("P006O17", "UPDATE [ContagemResultado] SET [ContagemResultado_DataPrevista]=@ContagemResultado_DataPrevista, [ContagemResultado_DataExecucao]=@ContagemResultado_DataExecucao  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP006O17)
             ,new CursorDef("P006O19", "SELECT T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_VlrCnc], T1.[ContagemResultado_Descricao], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_PrazoMaisDias], T1.[ContagemResultado_PrazoInicialDias], T1.[ContagemResultado_DataPrevista], T1.[ContagemResultado_DataExecucao], T1.[ContagemResultado_GlsValor], T1.[ContagemResultado_DataDmn], T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T1.[ContagemResultado_GlsDescricao], T1.[ContagemResultado_GlsData], T1.[ContagemResultado_Demanda], COALESCE( T3.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_ValorPF] FROM (([ContagemResultado] T1 WITH (UPDLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T3 ON T3.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE T1.[ContagemResultado_LoteAceiteCod] = @AV40ContagemResultado_LoteAceite and T1.[ContagemResultado_DemandaFM] = @AV38ContagemResultado_DemandaFM ORDER BY T1.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Demanda] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006O19,1,0,true,false )
             ,new CursorDef("P006O20", "UPDATE [ContagemResultado] SET [ContagemResultado_DataPrevista]=@ContagemResultado_DataPrevista, [ContagemResultado_DataExecucao]=@ContagemResultado_DataExecucao  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP006O20)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 10) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getMultimediaUri(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((String[]) buf[14])[0] = rslt.getString(8, 20) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((String[]) buf[16])[0] = rslt.getString(9, 100) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((int[]) buf[18])[0] = rslt.getInt(10) ;
                ((String[]) buf[19])[0] = rslt.getMultimediaFile(11, rslt.getVarchar(7)) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                return;
             case 2 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                return;
             case 3 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((decimal[]) buf[9])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((short[]) buf[14])[0] = rslt.getShort(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((short[]) buf[16])[0] = rslt.getShort(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[18])[0] = rslt.getGXDateTime(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((DateTime[]) buf[20])[0] = rslt.getGXDateTime(11) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((decimal[]) buf[22])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(12);
                ((DateTime[]) buf[24])[0] = rslt.getGXDate(13) ;
                ((String[]) buf[25])[0] = rslt.getString(14, 25) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(14);
                ((String[]) buf[27])[0] = rslt.getLongVarchar(15) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(15);
                ((DateTime[]) buf[29])[0] = rslt.getGXDate(16) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(16);
                ((String[]) buf[31])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(17);
                ((DateTime[]) buf[33])[0] = rslt.getGXDate(18) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(18);
                ((int[]) buf[35])[0] = rslt.getInt(19) ;
                ((decimal[]) buf[36])[0] = rslt.getDecimal(20) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(20);
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((short[]) buf[14])[0] = rslt.getShort(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((short[]) buf[16])[0] = rslt.getShort(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[18])[0] = rslt.getGXDateTime(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((DateTime[]) buf[20])[0] = rslt.getGXDateTime(11) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((decimal[]) buf[22])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(12);
                ((DateTime[]) buf[24])[0] = rslt.getGXDate(13) ;
                ((String[]) buf[25])[0] = rslt.getString(14, 25) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(14);
                ((String[]) buf[27])[0] = rslt.getLongVarchar(15) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(15);
                ((DateTime[]) buf[29])[0] = rslt.getGXDate(16) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(16);
                ((String[]) buf[31])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(17);
                ((DateTime[]) buf[33])[0] = rslt.getGXDate(18) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(18);
                ((int[]) buf[35])[0] = rslt.getInt(19) ;
                ((decimal[]) buf[36])[0] = rslt.getDecimal(20) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(20);
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((short[]) buf[14])[0] = rslt.getShort(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((short[]) buf[16])[0] = rslt.getShort(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[18])[0] = rslt.getGXDateTime(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((DateTime[]) buf[20])[0] = rslt.getGXDateTime(11) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((decimal[]) buf[22])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(12);
                ((DateTime[]) buf[24])[0] = rslt.getGXDate(13) ;
                ((String[]) buf[25])[0] = rslt.getString(14, 25) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(14);
                ((String[]) buf[27])[0] = rslt.getLongVarchar(15) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(15);
                ((DateTime[]) buf[29])[0] = rslt.getGXDate(16) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(16);
                ((String[]) buf[31])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(17);
                ((DateTime[]) buf[33])[0] = rslt.getGXDate(18) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(18);
                ((int[]) buf[35])[0] = rslt.getInt(19) ;
                ((decimal[]) buf[36])[0] = rslt.getDecimal(20) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(20);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(2, (DateTime)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(2, (DateTime)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(2, (DateTime)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
       }
    }

 }

}
