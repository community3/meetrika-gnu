/*
               File: GetContratoContratoOcorrenciaWCFilterData
        Description: Get Contrato Contrato Ocorrencia WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/28/2019 16:48:35.25
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getcontratocontratoocorrenciawcfilterdata : GXProcedure
   {
      public getcontratocontratoocorrenciawcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getcontratocontratoocorrenciawcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
         return AV25OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getcontratocontratoocorrenciawcfilterdata objgetcontratocontratoocorrenciawcfilterdata;
         objgetcontratocontratoocorrenciawcfilterdata = new getcontratocontratoocorrenciawcfilterdata();
         objgetcontratocontratoocorrenciawcfilterdata.AV16DDOName = aP0_DDOName;
         objgetcontratocontratoocorrenciawcfilterdata.AV14SearchTxt = aP1_SearchTxt;
         objgetcontratocontratoocorrenciawcfilterdata.AV15SearchTxtTo = aP2_SearchTxtTo;
         objgetcontratocontratoocorrenciawcfilterdata.AV20OptionsJson = "" ;
         objgetcontratocontratoocorrenciawcfilterdata.AV23OptionsDescJson = "" ;
         objgetcontratocontratoocorrenciawcfilterdata.AV25OptionIndexesJson = "" ;
         objgetcontratocontratoocorrenciawcfilterdata.context.SetSubmitInitialConfig(context);
         objgetcontratocontratoocorrenciawcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetcontratocontratoocorrenciawcfilterdata);
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getcontratocontratoocorrenciawcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV19Options = (IGxCollection)(new GxSimpleCollection());
         AV22OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV24OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV16DDOName), "DDO_CONTRATOOCORRENCIA_NAOCNFCOD") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOOCORRENCIA_NAOCNFCODOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV16DDOName), "DDO_CONTRATOOCORRENCIA_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOOCORRENCIA_DESCRICAOOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV20OptionsJson = AV19Options.ToJSonString(false);
         AV23OptionsDescJson = AV22OptionsDesc.ToJSonString(false);
         AV25OptionIndexesJson = AV24OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV27Session.Get("ContratoContratoOcorrenciaWCGridState"), "") == 0 )
         {
            AV29GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "ContratoContratoOcorrenciaWCGridState"), "");
         }
         else
         {
            AV29GridState.FromXml(AV27Session.Get("ContratoContratoOcorrenciaWCGridState"), "");
         }
         AV39GXV1 = 1;
         while ( AV39GXV1 <= AV29GridState.gxTpr_Filtervalues.Count )
         {
            AV30GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV29GridState.gxTpr_Filtervalues.Item(AV39GXV1));
            if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "CONTRATOOCORRENCIA_DESCRICAO") == 0 )
            {
               AV32ContratoOcorrencia_Descricao = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIA_DATA") == 0 )
            {
               AV10TFContratoOcorrencia_Data = context.localUtil.CToD( AV30GridStateFilterValue.gxTpr_Value, 2);
               AV11TFContratoOcorrencia_Data_To = context.localUtil.CToD( AV30GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIA_NAOCNFCOD") == 0 )
            {
               AV34TFContratoOcorrencia_NaoCnfCod = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIA_NAOCNFCOD_SEL") == 0 )
            {
               AV36TFContratoOcorrencia_NaoCnfCod_Sel = (int)(NumberUtil.Val( AV30GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIA_DESCRICAO") == 0 )
            {
               AV12TFContratoOcorrencia_Descricao = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIA_DESCRICAO_SEL") == 0 )
            {
               AV13TFContratoOcorrencia_Descricao_Sel = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "PARM_&CONTRATO_CODIGO") == 0 )
            {
               AV33Contrato_Codigo = (int)(NumberUtil.Val( AV30GridStateFilterValue.gxTpr_Value, "."));
            }
            AV39GXV1 = (int)(AV39GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATOOCORRENCIA_NAOCNFCODOPTIONS' Routine */
         AV34TFContratoOcorrencia_NaoCnfCod = AV14SearchTxt;
         AV36TFContratoOcorrencia_NaoCnfCod_Sel = 0;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV32ContratoOcorrencia_Descricao ,
                                              AV10TFContratoOcorrencia_Data ,
                                              AV11TFContratoOcorrencia_Data_To ,
                                              AV36TFContratoOcorrencia_NaoCnfCod_Sel ,
                                              AV34TFContratoOcorrencia_NaoCnfCod ,
                                              AV13TFContratoOcorrencia_Descricao_Sel ,
                                              AV12TFContratoOcorrencia_Descricao ,
                                              A296ContratoOcorrencia_Descricao ,
                                              A295ContratoOcorrencia_Data ,
                                              A427NaoConformidade_Nome ,
                                              A2027ContratoOcorrencia_NaoCnfCod ,
                                              AV33Contrato_Codigo ,
                                              A74Contrato_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV32ContratoOcorrencia_Descricao = StringUtil.Concat( StringUtil.RTrim( AV32ContratoOcorrencia_Descricao), "%", "");
         lV34TFContratoOcorrencia_NaoCnfCod = StringUtil.PadR( StringUtil.RTrim( AV34TFContratoOcorrencia_NaoCnfCod), 50, "%");
         lV12TFContratoOcorrencia_Descricao = StringUtil.Concat( StringUtil.RTrim( AV12TFContratoOcorrencia_Descricao), "%", "");
         /* Using cursor P00UG2 */
         pr_default.execute(0, new Object[] {AV33Contrato_Codigo, lV32ContratoOcorrencia_Descricao, AV10TFContratoOcorrencia_Data, AV11TFContratoOcorrencia_Data_To, lV34TFContratoOcorrencia_NaoCnfCod, AV36TFContratoOcorrencia_NaoCnfCod_Sel, lV12TFContratoOcorrencia_Descricao, AV13TFContratoOcorrencia_Descricao_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKUG2 = false;
            A74Contrato_Codigo = P00UG2_A74Contrato_Codigo[0];
            A2027ContratoOcorrencia_NaoCnfCod = P00UG2_A2027ContratoOcorrencia_NaoCnfCod[0];
            n2027ContratoOcorrencia_NaoCnfCod = P00UG2_n2027ContratoOcorrencia_NaoCnfCod[0];
            A427NaoConformidade_Nome = P00UG2_A427NaoConformidade_Nome[0];
            n427NaoConformidade_Nome = P00UG2_n427NaoConformidade_Nome[0];
            A295ContratoOcorrencia_Data = P00UG2_A295ContratoOcorrencia_Data[0];
            A296ContratoOcorrencia_Descricao = P00UG2_A296ContratoOcorrencia_Descricao[0];
            A294ContratoOcorrencia_Codigo = P00UG2_A294ContratoOcorrencia_Codigo[0];
            A427NaoConformidade_Nome = P00UG2_A427NaoConformidade_Nome[0];
            n427NaoConformidade_Nome = P00UG2_n427NaoConformidade_Nome[0];
            AV26count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00UG2_A74Contrato_Codigo[0] == A74Contrato_Codigo ) && ( P00UG2_A2027ContratoOcorrencia_NaoCnfCod[0] == A2027ContratoOcorrencia_NaoCnfCod ) )
            {
               BRKUG2 = false;
               A294ContratoOcorrencia_Codigo = P00UG2_A294ContratoOcorrencia_Codigo[0];
               AV26count = (long)(AV26count+1);
               BRKUG2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A427NaoConformidade_Nome)) )
            {
               AV18Option = StringUtil.Str( (decimal)(A2027ContratoOcorrencia_NaoCnfCod), 6, 0);
               AV21OptionDesc = A427NaoConformidade_Nome;
               AV17InsertIndex = 1;
               while ( ( AV17InsertIndex <= AV19Options.Count ) && ( StringUtil.StrCmp(((String)AV22OptionsDesc.Item(AV17InsertIndex)), AV21OptionDesc) < 0 ) )
               {
                  AV17InsertIndex = (int)(AV17InsertIndex+1);
               }
               AV19Options.Add(AV18Option, AV17InsertIndex);
               AV22OptionsDesc.Add(AV21OptionDesc, AV17InsertIndex);
               AV24OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV26count), "Z,ZZZ,ZZZ,ZZ9")), AV17InsertIndex);
            }
            if ( AV19Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUG2 )
            {
               BRKUG2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATOOCORRENCIA_DESCRICAOOPTIONS' Routine */
         AV12TFContratoOcorrencia_Descricao = AV14SearchTxt;
         AV13TFContratoOcorrencia_Descricao_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV32ContratoOcorrencia_Descricao ,
                                              AV10TFContratoOcorrencia_Data ,
                                              AV11TFContratoOcorrencia_Data_To ,
                                              AV36TFContratoOcorrencia_NaoCnfCod_Sel ,
                                              AV34TFContratoOcorrencia_NaoCnfCod ,
                                              AV13TFContratoOcorrencia_Descricao_Sel ,
                                              AV12TFContratoOcorrencia_Descricao ,
                                              A296ContratoOcorrencia_Descricao ,
                                              A295ContratoOcorrencia_Data ,
                                              A427NaoConformidade_Nome ,
                                              A2027ContratoOcorrencia_NaoCnfCod ,
                                              AV33Contrato_Codigo ,
                                              A74Contrato_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV32ContratoOcorrencia_Descricao = StringUtil.Concat( StringUtil.RTrim( AV32ContratoOcorrencia_Descricao), "%", "");
         lV34TFContratoOcorrencia_NaoCnfCod = StringUtil.PadR( StringUtil.RTrim( AV34TFContratoOcorrencia_NaoCnfCod), 50, "%");
         lV12TFContratoOcorrencia_Descricao = StringUtil.Concat( StringUtil.RTrim( AV12TFContratoOcorrencia_Descricao), "%", "");
         /* Using cursor P00UG3 */
         pr_default.execute(1, new Object[] {AV33Contrato_Codigo, lV32ContratoOcorrencia_Descricao, AV10TFContratoOcorrencia_Data, AV11TFContratoOcorrencia_Data_To, lV34TFContratoOcorrencia_NaoCnfCod, AV36TFContratoOcorrencia_NaoCnfCod_Sel, lV12TFContratoOcorrencia_Descricao, AV13TFContratoOcorrencia_Descricao_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKUG4 = false;
            A74Contrato_Codigo = P00UG3_A74Contrato_Codigo[0];
            A296ContratoOcorrencia_Descricao = P00UG3_A296ContratoOcorrencia_Descricao[0];
            A2027ContratoOcorrencia_NaoCnfCod = P00UG3_A2027ContratoOcorrencia_NaoCnfCod[0];
            n2027ContratoOcorrencia_NaoCnfCod = P00UG3_n2027ContratoOcorrencia_NaoCnfCod[0];
            A427NaoConformidade_Nome = P00UG3_A427NaoConformidade_Nome[0];
            n427NaoConformidade_Nome = P00UG3_n427NaoConformidade_Nome[0];
            A295ContratoOcorrencia_Data = P00UG3_A295ContratoOcorrencia_Data[0];
            A294ContratoOcorrencia_Codigo = P00UG3_A294ContratoOcorrencia_Codigo[0];
            A427NaoConformidade_Nome = P00UG3_A427NaoConformidade_Nome[0];
            n427NaoConformidade_Nome = P00UG3_n427NaoConformidade_Nome[0];
            AV26count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( P00UG3_A74Contrato_Codigo[0] == A74Contrato_Codigo ) && ( StringUtil.StrCmp(P00UG3_A296ContratoOcorrencia_Descricao[0], A296ContratoOcorrencia_Descricao) == 0 ) )
            {
               BRKUG4 = false;
               A294ContratoOcorrencia_Codigo = P00UG3_A294ContratoOcorrencia_Codigo[0];
               AV26count = (long)(AV26count+1);
               BRKUG4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A296ContratoOcorrencia_Descricao)) )
            {
               AV18Option = A296ContratoOcorrencia_Descricao;
               AV19Options.Add(AV18Option, 0);
               AV24OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV26count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV19Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUG4 )
            {
               BRKUG4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV19Options = new GxSimpleCollection();
         AV22OptionsDesc = new GxSimpleCollection();
         AV24OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV27Session = context.GetSession();
         AV29GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV30GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV32ContratoOcorrencia_Descricao = "";
         AV10TFContratoOcorrencia_Data = DateTime.MinValue;
         AV11TFContratoOcorrencia_Data_To = DateTime.MinValue;
         AV34TFContratoOcorrencia_NaoCnfCod = "";
         AV12TFContratoOcorrencia_Descricao = "";
         AV13TFContratoOcorrencia_Descricao_Sel = "";
         scmdbuf = "";
         lV32ContratoOcorrencia_Descricao = "";
         lV34TFContratoOcorrencia_NaoCnfCod = "";
         lV12TFContratoOcorrencia_Descricao = "";
         A296ContratoOcorrencia_Descricao = "";
         A295ContratoOcorrencia_Data = DateTime.MinValue;
         A427NaoConformidade_Nome = "";
         P00UG2_A74Contrato_Codigo = new int[1] ;
         P00UG2_A2027ContratoOcorrencia_NaoCnfCod = new int[1] ;
         P00UG2_n2027ContratoOcorrencia_NaoCnfCod = new bool[] {false} ;
         P00UG2_A427NaoConformidade_Nome = new String[] {""} ;
         P00UG2_n427NaoConformidade_Nome = new bool[] {false} ;
         P00UG2_A295ContratoOcorrencia_Data = new DateTime[] {DateTime.MinValue} ;
         P00UG2_A296ContratoOcorrencia_Descricao = new String[] {""} ;
         P00UG2_A294ContratoOcorrencia_Codigo = new int[1] ;
         AV18Option = "";
         AV21OptionDesc = "";
         P00UG3_A74Contrato_Codigo = new int[1] ;
         P00UG3_A296ContratoOcorrencia_Descricao = new String[] {""} ;
         P00UG3_A2027ContratoOcorrencia_NaoCnfCod = new int[1] ;
         P00UG3_n2027ContratoOcorrencia_NaoCnfCod = new bool[] {false} ;
         P00UG3_A427NaoConformidade_Nome = new String[] {""} ;
         P00UG3_n427NaoConformidade_Nome = new bool[] {false} ;
         P00UG3_A295ContratoOcorrencia_Data = new DateTime[] {DateTime.MinValue} ;
         P00UG3_A294ContratoOcorrencia_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getcontratocontratoocorrenciawcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00UG2_A74Contrato_Codigo, P00UG2_A2027ContratoOcorrencia_NaoCnfCod, P00UG2_n2027ContratoOcorrencia_NaoCnfCod, P00UG2_A427NaoConformidade_Nome, P00UG2_n427NaoConformidade_Nome, P00UG2_A295ContratoOcorrencia_Data, P00UG2_A296ContratoOcorrencia_Descricao, P00UG2_A294ContratoOcorrencia_Codigo
               }
               , new Object[] {
               P00UG3_A74Contrato_Codigo, P00UG3_A296ContratoOcorrencia_Descricao, P00UG3_A2027ContratoOcorrencia_NaoCnfCod, P00UG3_n2027ContratoOcorrencia_NaoCnfCod, P00UG3_A427NaoConformidade_Nome, P00UG3_n427NaoConformidade_Nome, P00UG3_A295ContratoOcorrencia_Data, P00UG3_A294ContratoOcorrencia_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV39GXV1 ;
      private int AV36TFContratoOcorrencia_NaoCnfCod_Sel ;
      private int AV33Contrato_Codigo ;
      private int A2027ContratoOcorrencia_NaoCnfCod ;
      private int A74Contrato_Codigo ;
      private int A294ContratoOcorrencia_Codigo ;
      private int AV17InsertIndex ;
      private long AV26count ;
      private String AV34TFContratoOcorrencia_NaoCnfCod ;
      private String scmdbuf ;
      private String lV34TFContratoOcorrencia_NaoCnfCod ;
      private String A427NaoConformidade_Nome ;
      private DateTime AV10TFContratoOcorrencia_Data ;
      private DateTime AV11TFContratoOcorrencia_Data_To ;
      private DateTime A295ContratoOcorrencia_Data ;
      private bool returnInSub ;
      private bool BRKUG2 ;
      private bool n2027ContratoOcorrencia_NaoCnfCod ;
      private bool n427NaoConformidade_Nome ;
      private bool BRKUG4 ;
      private String AV25OptionIndexesJson ;
      private String AV20OptionsJson ;
      private String AV23OptionsDescJson ;
      private String AV16DDOName ;
      private String AV14SearchTxt ;
      private String AV15SearchTxtTo ;
      private String AV32ContratoOcorrencia_Descricao ;
      private String AV12TFContratoOcorrencia_Descricao ;
      private String AV13TFContratoOcorrencia_Descricao_Sel ;
      private String lV32ContratoOcorrencia_Descricao ;
      private String lV12TFContratoOcorrencia_Descricao ;
      private String A296ContratoOcorrencia_Descricao ;
      private String AV18Option ;
      private String AV21OptionDesc ;
      private IGxSession AV27Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00UG2_A74Contrato_Codigo ;
      private int[] P00UG2_A2027ContratoOcorrencia_NaoCnfCod ;
      private bool[] P00UG2_n2027ContratoOcorrencia_NaoCnfCod ;
      private String[] P00UG2_A427NaoConformidade_Nome ;
      private bool[] P00UG2_n427NaoConformidade_Nome ;
      private DateTime[] P00UG2_A295ContratoOcorrencia_Data ;
      private String[] P00UG2_A296ContratoOcorrencia_Descricao ;
      private int[] P00UG2_A294ContratoOcorrencia_Codigo ;
      private int[] P00UG3_A74Contrato_Codigo ;
      private String[] P00UG3_A296ContratoOcorrencia_Descricao ;
      private int[] P00UG3_A2027ContratoOcorrencia_NaoCnfCod ;
      private bool[] P00UG3_n2027ContratoOcorrencia_NaoCnfCod ;
      private String[] P00UG3_A427NaoConformidade_Nome ;
      private bool[] P00UG3_n427NaoConformidade_Nome ;
      private DateTime[] P00UG3_A295ContratoOcorrencia_Data ;
      private int[] P00UG3_A294ContratoOcorrencia_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV19Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV29GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV30GridStateFilterValue ;
   }

   public class getcontratocontratoocorrenciawcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00UG2( IGxContext context ,
                                             String AV32ContratoOcorrencia_Descricao ,
                                             DateTime AV10TFContratoOcorrencia_Data ,
                                             DateTime AV11TFContratoOcorrencia_Data_To ,
                                             int AV36TFContratoOcorrencia_NaoCnfCod_Sel ,
                                             String AV34TFContratoOcorrencia_NaoCnfCod ,
                                             String AV13TFContratoOcorrencia_Descricao_Sel ,
                                             String AV12TFContratoOcorrencia_Descricao ,
                                             String A296ContratoOcorrencia_Descricao ,
                                             DateTime A295ContratoOcorrencia_Data ,
                                             String A427NaoConformidade_Nome ,
                                             int A2027ContratoOcorrencia_NaoCnfCod ,
                                             int AV33Contrato_Codigo ,
                                             int A74Contrato_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [8] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contrato_Codigo], T1.[ContratoOcorrencia_NaoCnfCod] AS ContratoOcorrencia_NaoCnfCod, T2.[NaoConformidade_Nome], T1.[ContratoOcorrencia_Data], T1.[ContratoOcorrencia_Descricao], T1.[ContratoOcorrencia_Codigo] FROM ([ContratoOcorrencia] T1 WITH (NOLOCK) LEFT JOIN [NaoConformidade] T2 WITH (NOLOCK) ON T2.[NaoConformidade_Codigo] = T1.[ContratoOcorrencia_NaoCnfCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Contrato_Codigo] = @AV33Contrato_Codigo)";
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32ContratoOcorrencia_Descricao)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like @lV32ContratoOcorrencia_Descricao)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ! (DateTime.MinValue==AV10TFContratoOcorrencia_Data) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] >= @AV10TFContratoOcorrencia_Data)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ! (DateTime.MinValue==AV11TFContratoOcorrencia_Data_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] <= @AV11TFContratoOcorrencia_Data_To)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( (0==AV36TFContratoOcorrencia_NaoCnfCod_Sel) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34TFContratoOcorrencia_NaoCnfCod)) ) )
         {
            sWhereString = sWhereString + " and (T2.[NaoConformidade_Nome] like @lV34TFContratoOcorrencia_NaoCnfCod)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! (0==AV36TFContratoOcorrencia_NaoCnfCod_Sel) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_NaoCnfCod] = @AV36TFContratoOcorrencia_NaoCnfCod_Sel)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoOcorrencia_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContratoOcorrencia_Descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like @lV12TFContratoOcorrencia_Descricao)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoOcorrencia_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] = @AV13TFContratoOcorrencia_Descricao_Sel)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Contrato_Codigo], T1.[ContratoOcorrencia_NaoCnfCod]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00UG3( IGxContext context ,
                                             String AV32ContratoOcorrencia_Descricao ,
                                             DateTime AV10TFContratoOcorrencia_Data ,
                                             DateTime AV11TFContratoOcorrencia_Data_To ,
                                             int AV36TFContratoOcorrencia_NaoCnfCod_Sel ,
                                             String AV34TFContratoOcorrencia_NaoCnfCod ,
                                             String AV13TFContratoOcorrencia_Descricao_Sel ,
                                             String AV12TFContratoOcorrencia_Descricao ,
                                             String A296ContratoOcorrencia_Descricao ,
                                             DateTime A295ContratoOcorrencia_Data ,
                                             String A427NaoConformidade_Nome ,
                                             int A2027ContratoOcorrencia_NaoCnfCod ,
                                             int AV33Contrato_Codigo ,
                                             int A74Contrato_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [8] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contrato_Codigo], T1.[ContratoOcorrencia_Descricao], T1.[ContratoOcorrencia_NaoCnfCod] AS ContratoOcorrencia_NaoCnfCod, T2.[NaoConformidade_Nome], T1.[ContratoOcorrencia_Data], T1.[ContratoOcorrencia_Codigo] FROM ([ContratoOcorrencia] T1 WITH (NOLOCK) LEFT JOIN [NaoConformidade] T2 WITH (NOLOCK) ON T2.[NaoConformidade_Codigo] = T1.[ContratoOcorrencia_NaoCnfCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Contrato_Codigo] = @AV33Contrato_Codigo)";
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32ContratoOcorrencia_Descricao)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like @lV32ContratoOcorrencia_Descricao)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ! (DateTime.MinValue==AV10TFContratoOcorrencia_Data) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] >= @AV10TFContratoOcorrencia_Data)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ! (DateTime.MinValue==AV11TFContratoOcorrencia_Data_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] <= @AV11TFContratoOcorrencia_Data_To)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( (0==AV36TFContratoOcorrencia_NaoCnfCod_Sel) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34TFContratoOcorrencia_NaoCnfCod)) ) )
         {
            sWhereString = sWhereString + " and (T2.[NaoConformidade_Nome] like @lV34TFContratoOcorrencia_NaoCnfCod)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ! (0==AV36TFContratoOcorrencia_NaoCnfCod_Sel) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_NaoCnfCod] = @AV36TFContratoOcorrencia_NaoCnfCod_Sel)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoOcorrencia_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContratoOcorrencia_Descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like @lV12TFContratoOcorrencia_Descricao)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoOcorrencia_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] = @AV13TFContratoOcorrencia_Descricao_Sel)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Contrato_Codigo], T1.[ContratoOcorrencia_Descricao]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00UG2(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (int)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (DateTime)dynConstraints[8] , (String)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] );
               case 1 :
                     return conditional_P00UG3(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (int)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (DateTime)dynConstraints[8] , (String)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00UG2 ;
          prmP00UG2 = new Object[] {
          new Object[] {"@AV33Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV32ContratoOcorrencia_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV10TFContratoOcorrencia_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV11TFContratoOcorrencia_Data_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV34TFContratoOcorrencia_NaoCnfCod",SqlDbType.Char,50,0} ,
          new Object[] {"@AV36TFContratoOcorrencia_NaoCnfCod_Sel",SqlDbType.Int,6,0} ,
          new Object[] {"@lV12TFContratoOcorrencia_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV13TFContratoOcorrencia_Descricao_Sel",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmP00UG3 ;
          prmP00UG3 = new Object[] {
          new Object[] {"@AV33Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV32ContratoOcorrencia_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV10TFContratoOcorrencia_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV11TFContratoOcorrencia_Data_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV34TFContratoOcorrencia_NaoCnfCod",SqlDbType.Char,50,0} ,
          new Object[] {"@AV36TFContratoOcorrencia_NaoCnfCod_Sel",SqlDbType.Int,6,0} ,
          new Object[] {"@lV12TFContratoOcorrencia_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV13TFContratoOcorrencia_Descricao_Sel",SqlDbType.VarChar,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00UG2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UG2,100,0,true,false )
             ,new CursorDef("P00UG3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UG3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(4) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(5) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[10]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[11]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[10]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[11]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getcontratocontratoocorrenciawcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getcontratocontratoocorrenciawcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getcontratocontratoocorrenciawcfilterdata") )
          {
             return  ;
          }
          getcontratocontratoocorrenciawcfilterdata worker = new getcontratocontratoocorrenciawcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
