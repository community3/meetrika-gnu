/*
               File: LoteArquivosAnexosWC
        Description: Lote Arquivos Anexos WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/19/2020 21:12:17.75
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class lotearquivosanexoswc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public lotearquivosanexoswc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public lotearquivosanexoswc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_LoteArquivoAnexo_LoteCod )
      {
         this.AV19LoteArquivoAnexo_LoteCod = aP0_LoteArquivoAnexo_LoteCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         dynavLotearquivoanexo_lotecod_selected = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV19LoteArquivoAnexo_LoteCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19LoteArquivoAnexo_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19LoteArquivoAnexo_LoteCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV19LoteArquivoAnexo_LoteCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_29 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_29_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_29_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
                  AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
                  AV19LoteArquivoAnexo_LoteCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19LoteArquivoAnexo_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19LoteArquivoAnexo_LoteCod), 6, 0)));
                  A596Lote_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A596Lote_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A596Lote_Codigo), 6, 0)));
                  A673Lote_NFe = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n673Lote_NFe = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A673Lote_NFe", StringUtil.LTrim( StringUtil.Str( (decimal)(A673Lote_NFe), 6, 0)));
                  A678Lote_NFeArq = GetNextPar( );
                  n678Lote_NFeArq = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A678Lote_NFeArq", A678Lote_NFeArq);
                  AV54Pgmname = GetNextPar( );
                  A841LoteArquivoAnexo_LoteCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A836LoteArquivoAnexo_Data = context.localUtil.ParseDTimeParm( GetNextPar( ));
                  A838LoteArquivoAnexo_Arquivo = GetNextPar( );
                  n838LoteArquivoAnexo_Arquivo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A838LoteArquivoAnexo_Arquivo", A838LoteArquivoAnexo_Arquivo);
                  A839LoteArquivoAnexo_NomeArq = GetNextPar( );
                  n839LoteArquivoAnexo_NomeArq = false;
                  A840LoteArquivoAnexo_TipoArq = GetNextPar( );
                  n840LoteArquivoAnexo_TipoArq = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A840LoteArquivoAnexo_TipoArq", A840LoteArquivoAnexo_TipoArq);
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV19LoteArquivoAnexo_LoteCod, A596Lote_Codigo, A673Lote_NFe, A678Lote_NFeArq, AV54Pgmname, A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data, A838LoteArquivoAnexo_Arquivo, A839LoteArquivoAnexo_NomeArq, A840LoteArquivoAnexo_TipoArq, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAEU2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV54Pgmname = "LoteArquivosAnexosWC";
               context.Gx_err = 0;
               edtavLote_nfe_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavLote_nfe_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_nfe_Enabled), 5, 0)));
               edtavLote_nfearq_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavLote_nfearq_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_nfearq_Enabled), 5, 0)));
               WSEU2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Lote Arquivos Anexos WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202031921121794");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Uploadify/swfobject.js", "");
         context.AddJavascriptSource("Uploadify/jquery.uploadify.v2.1.4.min.js", "");
         context.AddJavascriptSource("Uploadify/UploadifyRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("lotearquivosanexoswc.aspx") + "?" + UrlEncode("" +AV19LoteArquivoAnexo_LoteCod)+"\">") ;
               GxWebStd.gx_hidden_field( context, "_EventName", "");
               GxWebStd.gx_hidden_field( context, "_EventGridId", "");
               GxWebStd.gx_hidden_field( context, "_EventRowId", "");
               context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
            }
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_29", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_29), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vUPLOADEDFILES", AV26UploadedFiles);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vUPLOADEDFILES", AV26UploadedFiles);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vQUEUEDFILES", AV28QueuedFiles);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vQUEUEDFILES", AV28QueuedFiles);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV19LoteArquivoAnexo_LoteCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV19LoteArquivoAnexo_LoteCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vLOTEARQUIVOANEXO_LOTECOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV19LoteArquivoAnexo_LoteCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"LOTE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A596Lote_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"LOTE_NFE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A673Lote_NFe), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"LOTE_NFEARQ", A678Lote_NFeArq);
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV54Pgmname));
         GxWebStd.gx_hidden_field( context, sPrefix+"LOTEARQUIVOANEXO_ARQUIVO", A838LoteArquivoAnexo_Arquivo);
         GxWebStd.gx_hidden_field( context, sPrefix+"LOTEARQUIVOANEXO_TIPOARQ", StringUtil.RTrim( A840LoteArquivoAnexo_TipoArq));
         GxWebStd.gx_hidden_field( context, sPrefix+"vMSG", StringUtil.RTrim( Gx_msg));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vFILE", AV27File);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vFILE", AV27File);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"UPLOAD_Width", StringUtil.RTrim( Upload_Width));
         GxWebStd.gx_hidden_field( context, sPrefix+"UPLOAD_Multi", StringUtil.BoolToStr( Upload_Multi));
         GxWebStd.gx_hidden_field( context, sPrefix+"UPLOAD_Buttontext", StringUtil.RTrim( Upload_Buttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"UPLOAD_Buttonimage", StringUtil.RTrim( Upload_Buttonimage));
         GxWebStd.gx_hidden_field( context, sPrefix+"UPLOAD_Sizelimit", StringUtil.LTrim( StringUtil.NToC( (decimal)(Upload_Sizelimit), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"UPLOAD_Simultaneousuploadlimit", StringUtil.RTrim( Upload_Simultaneousuploadlimit));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Title", StringUtil.RTrim( Dvelop_confirmpanel_apagar_Title));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Confirmationtext", StringUtil.RTrim( Dvelop_confirmpanel_apagar_Confirmationtext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Yesbuttoncaption", StringUtil.RTrim( Dvelop_confirmpanel_apagar_Yesbuttoncaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Nobuttoncaption", StringUtil.RTrim( Dvelop_confirmpanel_apagar_Nobuttoncaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Cancelbuttoncaption", StringUtil.RTrim( Dvelop_confirmpanel_apagar_Cancelbuttoncaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Yesbuttonposition", StringUtil.RTrim( Dvelop_confirmpanel_apagar_Yesbuttonposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Confirmtype", StringUtil.RTrim( Dvelop_confirmpanel_apagar_Confirmtype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Result", StringUtil.RTrim( Dvelop_confirmpanel_apagar_Result));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"UPLOAD_File", Upload_File);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"UPLOAD_File", Upload_File);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"UPLOAD_Error", StringUtil.RTrim( Upload_Error));
         GxWebStd.gx_hidden_field( context, sPrefix+"UPLOAD_Sizelimit", StringUtil.LTrim( StringUtil.NToC( (decimal)(Upload_Sizelimit), 9, 0, ".", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormEU2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("lotearquivosanexoswc.js", "?202031921121811");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "</form>") ;
            }
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "LoteArquivosAnexosWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Lote Arquivos Anexos WC" ;
      }

      protected void WBEU0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "lotearquivosanexoswc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Uploadify/swfobject.js", "");
               context.AddJavascriptSource("Uploadify/jquery.uploadify.v2.1.4.min.js", "");
               context.AddJavascriptSource("Uploadify/UploadifyRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
            }
            wb_table1_2_EU2( true) ;
         }
         else
         {
            wb_table1_2_EU2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_EU2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'" + sPrefix + "',false,'" + sGXsfl_29_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavLotearquivoanexo_lotecod_selected, dynavLotearquivoanexo_lotecod_selected_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV22LoteArquivoAnexo_LoteCod_Selected), 6, 0)), 1, dynavLotearquivoanexo_lotecod_selected_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", dynavLotearquivoanexo_lotecod_selected.Visible, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,43);\"", "", true, "HLP_LoteArquivosAnexosWC.htm");
            dynavLotearquivoanexo_lotecod_selected.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22LoteArquivoAnexo_LoteCod_Selected), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavLotearquivoanexo_lotecod_selected_Internalname, "Values", (String)(dynavLotearquivoanexo_lotecod_selected.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'" + sPrefix + "',false,'" + sGXsfl_29_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLotearquivoanexo_data_selected_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLotearquivoanexo_data_selected_Internalname, context.localUtil.TToC( AV23LoteArquivoAnexo_Data_Selected, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV23LoteArquivoAnexo_Data_Selected, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,44);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLotearquivoanexo_data_selected_Jsonclick, 0, "Attribute", "", "", "", edtavLotearquivoanexo_data_selected_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_LoteArquivosAnexosWC.htm");
            GxWebStd.gx_bitmap( context, edtavLotearquivoanexo_data_selected_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavLotearquivoanexo_data_selected_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_LoteArquivosAnexosWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'" + sPrefix + "',false,'" + sGXsfl_29_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLotearquivoanexo_nomearq_selected_Internalname, StringUtil.RTrim( AV25LoteArquivoAnexo_NomeArq_Selected), StringUtil.RTrim( context.localUtil.Format( AV25LoteArquivoAnexo_NomeArq_Selected, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,45);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLotearquivoanexo_nomearq_selected_Jsonclick, 0, "Attribute", "", "", "", edtavLotearquivoanexo_nomearq_selected_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_LoteArquivosAnexosWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'" + sPrefix + "',false,'" + sGXsfl_29_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrderedby_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV13OrderedBy), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,46);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrderedby_Jsonclick, 0, "Attribute", "", "", "", edtavOrderedby_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_LoteArquivosAnexosWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'" + sPrefix + "',false,'" + sGXsfl_29_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,47);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_LoteArquivosAnexosWC.htm");
            wb_table2_48_EU2( true) ;
         }
         else
         {
            wb_table2_48_EU2( false) ;
         }
         return  ;
      }

      protected void wb_table2_48_EU2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTEU2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Lote Arquivos Anexos WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPEU0( ) ;
            }
         }
      }

      protected void WSEU2( )
      {
         STARTEU2( ) ;
         EVTEU2( ) ;
      }

      protected void EVTEU2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPEU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "UPLOAD.ONALLCOMPLETE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPEU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11EU2 */
                                    E11EU2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DVELOP_CONFIRMPANEL_APAGAR.CLOSE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPEU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12EU2 */
                                    E12EU2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPEU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavLote_nfe_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPEU0( ) ;
                              }
                              sEvt = cgiGet( sPrefix+"GRIDPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgrid_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgrid_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgrid_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgrid_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPEU0( ) ;
                              }
                              nGXsfl_29_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_29_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_29_idx), 4, 0)), 4, "0");
                              SubsflControlProps_292( ) ;
                              AV17Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV17Update)) ? AV51Update_GXI : context.convertURL( context.PathToRelativeUrl( AV17Update))));
                              AV21Apagar = cgiGet( edtavApagar_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavApagar_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV21Apagar)) ? AV52Apagar_GXI : context.convertURL( context.PathToRelativeUrl( AV21Apagar))));
                              AV24Acessar = cgiGet( edtavAcessar_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAcessar_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV24Acessar)) ? AV53Acessar_GXI : context.convertURL( context.PathToRelativeUrl( AV24Acessar))));
                              A841LoteArquivoAnexo_LoteCod = (int)(context.localUtil.CToN( cgiGet( edtLoteArquivoAnexo_LoteCod_Internalname), ",", "."));
                              A646TipoDocumento_Nome = StringUtil.Upper( cgiGet( edtTipoDocumento_Nome_Internalname));
                              A836LoteArquivoAnexo_Data = context.localUtil.CToT( cgiGet( edtLoteArquivoAnexo_Data_Internalname), 0);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavLote_nfe_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E13EU2 */
                                          E13EU2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavLote_nfe_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E14EU2 */
                                          E14EU2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavLote_nfe_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E15EU2 */
                                          E15EU2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavLote_nfe_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPEU0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavLote_nfe_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEEU2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormEU2( ) ;
            }
         }
      }

      protected void PAEU2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            dynavLotearquivoanexo_lotecod_selected.Name = "vLOTEARQUIVOANEXO_LOTECOD_SELECTED";
            dynavLotearquivoanexo_lotecod_selected.WebTags = "";
            dynavLotearquivoanexo_lotecod_selected.removeAllItems();
            /* Using cursor H00EU2 */
            pr_default.execute(0);
            while ( (pr_default.getStatus(0) != 101) )
            {
               dynavLotearquivoanexo_lotecod_selected.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(H00EU2_A596Lote_Codigo[0]), 6, 0)), H00EU2_A563Lote_Nome[0], 0);
               pr_default.readNext(0);
            }
            pr_default.close(0);
            if ( dynavLotearquivoanexo_lotecod_selected.ItemCount > 0 )
            {
               AV22LoteArquivoAnexo_LoteCod_Selected = (int)(NumberUtil.Val( dynavLotearquivoanexo_lotecod_selected.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22LoteArquivoAnexo_LoteCod_Selected), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22LoteArquivoAnexo_LoteCod_Selected", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22LoteArquivoAnexo_LoteCod_Selected), 6, 0)));
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavLote_nfe_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvLOTEARQUIVOANEXO_LOTECOD_SELECTEDEU1( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvLOTEARQUIVOANEXO_LOTECOD_SELECTED_dataEU1( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvLOTEARQUIVOANEXO_LOTECOD_SELECTED_htmlEU1( )
      {
         int gxdynajaxvalue ;
         GXDLVvLOTEARQUIVOANEXO_LOTECOD_SELECTED_dataEU1( ) ;
         gxdynajaxindex = 1;
         dynavLotearquivoanexo_lotecod_selected.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavLotearquivoanexo_lotecod_selected.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavLotearquivoanexo_lotecod_selected.ItemCount > 0 )
         {
            AV22LoteArquivoAnexo_LoteCod_Selected = (int)(NumberUtil.Val( dynavLotearquivoanexo_lotecod_selected.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22LoteArquivoAnexo_LoteCod_Selected), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22LoteArquivoAnexo_LoteCod_Selected", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22LoteArquivoAnexo_LoteCod_Selected), 6, 0)));
         }
      }

      protected void GXDLVvLOTEARQUIVOANEXO_LOTECOD_SELECTED_dataEU1( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor H00EU3 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00EU3_A596Lote_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00EU3_A563Lote_Nome[0]));
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_292( ) ;
         while ( nGXsfl_29_idx <= nRC_GXsfl_29 )
         {
            sendrow_292( ) ;
            nGXsfl_29_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_29_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_29_idx+1));
            sGXsfl_29_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_29_idx), 4, 0)), 4, "0");
            SubsflControlProps_292( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       int AV19LoteArquivoAnexo_LoteCod ,
                                       int A596Lote_Codigo ,
                                       int A673Lote_NFe ,
                                       String A678Lote_NFeArq ,
                                       String AV54Pgmname ,
                                       int A841LoteArquivoAnexo_LoteCod ,
                                       DateTime A836LoteArquivoAnexo_Data ,
                                       String A838LoteArquivoAnexo_Arquivo ,
                                       String A839LoteArquivoAnexo_NomeArq ,
                                       String A840LoteArquivoAnexo_TipoArq ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFEU2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_LOTEARQUIVOANEXO_LOTECOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A841LoteArquivoAnexo_LoteCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"LOTEARQUIVOANEXO_LOTECOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A841LoteArquivoAnexo_LoteCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_LOTEARQUIVOANEXO_DATA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A836LoteArquivoAnexo_Data, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"LOTEARQUIVOANEXO_DATA", context.localUtil.TToC( A836LoteArquivoAnexo_Data, 10, 8, 0, 3, "/", ":", " "));
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavLotearquivoanexo_lotecod_selected.ItemCount > 0 )
         {
            AV22LoteArquivoAnexo_LoteCod_Selected = (int)(NumberUtil.Val( dynavLotearquivoanexo_lotecod_selected.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22LoteArquivoAnexo_LoteCod_Selected), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22LoteArquivoAnexo_LoteCod_Selected", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22LoteArquivoAnexo_LoteCod_Selected), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFEU2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV54Pgmname = "LoteArquivosAnexosWC";
         context.Gx_err = 0;
         edtavLote_nfe_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavLote_nfe_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_nfe_Enabled), 5, 0)));
         edtavLote_nfearq_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavLote_nfearq_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_nfearq_Enabled), 5, 0)));
      }

      protected void RFEU2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 29;
         /* Execute user event: E14EU2 */
         E14EU2 ();
         nGXsfl_29_idx = 1;
         sGXsfl_29_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_29_idx), 4, 0)), 4, "0");
         SubsflControlProps_292( ) ;
         nGXsfl_29_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_292( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(2, new Object[]{ new Object[]{
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A841LoteArquivoAnexo_LoteCod ,
                                                 AV19LoteArquivoAnexo_LoteCod },
                                                 new int[] {
                                                 TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            /* Using cursor H00EU4 */
            pr_default.execute(2, new Object[] {AV19LoteArquivoAnexo_LoteCod, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_29_idx = 1;
            while ( ( (pr_default.getStatus(2) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A645TipoDocumento_Codigo = H00EU4_A645TipoDocumento_Codigo[0];
               n645TipoDocumento_Codigo = H00EU4_n645TipoDocumento_Codigo[0];
               A839LoteArquivoAnexo_NomeArq = H00EU4_A839LoteArquivoAnexo_NomeArq[0];
               n839LoteArquivoAnexo_NomeArq = H00EU4_n839LoteArquivoAnexo_NomeArq[0];
               A838LoteArquivoAnexo_Arquivo_Filename = A839LoteArquivoAnexo_NomeArq;
               A840LoteArquivoAnexo_TipoArq = H00EU4_A840LoteArquivoAnexo_TipoArq[0];
               n840LoteArquivoAnexo_TipoArq = H00EU4_n840LoteArquivoAnexo_TipoArq[0];
               A838LoteArquivoAnexo_Arquivo_Filetype = A840LoteArquivoAnexo_TipoArq;
               A836LoteArquivoAnexo_Data = H00EU4_A836LoteArquivoAnexo_Data[0];
               A646TipoDocumento_Nome = H00EU4_A646TipoDocumento_Nome[0];
               A841LoteArquivoAnexo_LoteCod = H00EU4_A841LoteArquivoAnexo_LoteCod[0];
               A838LoteArquivoAnexo_Arquivo = H00EU4_A838LoteArquivoAnexo_Arquivo[0];
               n838LoteArquivoAnexo_Arquivo = H00EU4_n838LoteArquivoAnexo_Arquivo[0];
               A646TipoDocumento_Nome = H00EU4_A646TipoDocumento_Nome[0];
               /* Execute user event: E15EU2 */
               E15EU2 ();
               pr_default.readNext(2);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(2) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(2);
            wbEnd = 29;
            WBEU0( ) ;
         }
         nGXsfl_29_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              A841LoteArquivoAnexo_LoteCod ,
                                              AV19LoteArquivoAnexo_LoteCod },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         /* Using cursor H00EU5 */
         pr_default.execute(3, new Object[] {AV19LoteArquivoAnexo_LoteCod});
         GRID_nRecordCount = H00EU5_AGRID_nRecordCount[0];
         pr_default.close(3);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV19LoteArquivoAnexo_LoteCod, A596Lote_Codigo, A673Lote_NFe, A678Lote_NFeArq, AV54Pgmname, A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data, A838LoteArquivoAnexo_Arquivo, A839LoteArquivoAnexo_NomeArq, A840LoteArquivoAnexo_TipoArq, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV19LoteArquivoAnexo_LoteCod, A596Lote_Codigo, A673Lote_NFe, A678Lote_NFeArq, AV54Pgmname, A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data, A838LoteArquivoAnexo_Arquivo, A839LoteArquivoAnexo_NomeArq, A840LoteArquivoAnexo_TipoArq, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV19LoteArquivoAnexo_LoteCod, A596Lote_Codigo, A673Lote_NFe, A678Lote_NFeArq, AV54Pgmname, A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data, A838LoteArquivoAnexo_Arquivo, A839LoteArquivoAnexo_NomeArq, A840LoteArquivoAnexo_TipoArq, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV19LoteArquivoAnexo_LoteCod, A596Lote_Codigo, A673Lote_NFe, A678Lote_NFeArq, AV54Pgmname, A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data, A838LoteArquivoAnexo_Arquivo, A839LoteArquivoAnexo_NomeArq, A840LoteArquivoAnexo_TipoArq, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV19LoteArquivoAnexo_LoteCod, A596Lote_Codigo, A673Lote_NFe, A678Lote_NFeArq, AV54Pgmname, A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data, A838LoteArquivoAnexo_Arquivo, A839LoteArquivoAnexo_NomeArq, A840LoteArquivoAnexo_TipoArq, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPEU0( )
      {
         /* Before Start, stand alone formulas. */
         AV54Pgmname = "LoteArquivosAnexosWC";
         context.Gx_err = 0;
         edtavLote_nfe_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavLote_nfe_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_nfe_Enabled), 5, 0)));
         edtavLote_nfearq_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavLote_nfearq_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_nfearq_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E13EU2 */
         E13EU2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vUPLOADEDFILES"), AV26UploadedFiles);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vQUEUEDFILES"), AV28QueuedFiles);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vFILE"), AV27File);
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtavLote_nfe_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavLote_nfe_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vLOTE_NFE");
               GX_FocusControl = edtavLote_nfe_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV46Lote_NFe = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV46Lote_NFe", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46Lote_NFe), 6, 0)));
            }
            else
            {
               AV46Lote_NFe = (int)(context.localUtil.CToN( cgiGet( edtavLote_nfe_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV46Lote_NFe", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46Lote_NFe), 6, 0)));
            }
            AV47Lote_NFeArq = cgiGet( edtavLote_nfearq_Internalname);
            dynavLotearquivoanexo_lotecod_selected.Name = dynavLotearquivoanexo_lotecod_selected_Internalname;
            dynavLotearquivoanexo_lotecod_selected.CurrentValue = cgiGet( dynavLotearquivoanexo_lotecod_selected_Internalname);
            AV22LoteArquivoAnexo_LoteCod_Selected = (int)(NumberUtil.Val( cgiGet( dynavLotearquivoanexo_lotecod_selected_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22LoteArquivoAnexo_LoteCod_Selected", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22LoteArquivoAnexo_LoteCod_Selected), 6, 0)));
            if ( context.localUtil.VCDateTime( cgiGet( edtavLotearquivoanexo_data_selected_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Lote Arquivo Anexo_Data_Selected"}), 1, "vLOTEARQUIVOANEXO_DATA_SELECTED");
               GX_FocusControl = edtavLotearquivoanexo_data_selected_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV23LoteArquivoAnexo_Data_Selected = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23LoteArquivoAnexo_Data_Selected", context.localUtil.TToC( AV23LoteArquivoAnexo_Data_Selected, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV23LoteArquivoAnexo_Data_Selected = context.localUtil.CToT( cgiGet( edtavLotearquivoanexo_data_selected_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23LoteArquivoAnexo_Data_Selected", context.localUtil.TToC( AV23LoteArquivoAnexo_Data_Selected, 8, 5, 0, 3, "/", ":", " "));
            }
            AV25LoteArquivoAnexo_NomeArq_Selected = cgiGet( edtavLotearquivoanexo_nomearq_selected_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25LoteArquivoAnexo_NomeArq_Selected", AV25LoteArquivoAnexo_NomeArq_Selected);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vORDEREDBY");
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV13OrderedBy = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            else
            {
               AV13OrderedBy = (short)(context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            /* Read saved values. */
            nRC_GXsfl_29 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_29"), ",", "."));
            wcpOAV19LoteArquivoAnexo_LoteCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV19LoteArquivoAnexo_LoteCod"), ",", "."));
            Gx_msg = cgiGet( sPrefix+"vMSG");
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Upload_Width = cgiGet( sPrefix+"UPLOAD_Width");
            Upload_Multi = StringUtil.StrToBool( cgiGet( sPrefix+"UPLOAD_Multi"));
            Upload_Buttontext = cgiGet( sPrefix+"UPLOAD_Buttontext");
            Upload_Buttonimage = cgiGet( sPrefix+"UPLOAD_Buttonimage");
            Upload_Sizelimit = (int)(context.localUtil.CToN( cgiGet( sPrefix+"UPLOAD_Sizelimit"), ",", "."));
            Upload_Simultaneousuploadlimit = cgiGet( sPrefix+"UPLOAD_Simultaneousuploadlimit");
            Dvelop_confirmpanel_apagar_Title = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Title");
            Dvelop_confirmpanel_apagar_Confirmationtext = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Confirmationtext");
            Dvelop_confirmpanel_apagar_Yesbuttoncaption = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Yesbuttoncaption");
            Dvelop_confirmpanel_apagar_Nobuttoncaption = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Nobuttoncaption");
            Dvelop_confirmpanel_apagar_Cancelbuttoncaption = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Cancelbuttoncaption");
            Dvelop_confirmpanel_apagar_Yesbuttonposition = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Yesbuttonposition");
            Dvelop_confirmpanel_apagar_Confirmtype = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Confirmtype");
            Dvelop_confirmpanel_apagar_Result = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Result");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E13EU2 */
         E13EU2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E13EU2( )
      {
         /* Start Routine */
         dynavLotearquivoanexo_lotecod_selected.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavLotearquivoanexo_lotecod_selected_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavLotearquivoanexo_lotecod_selected.Visible), 5, 0)));
         edtavLotearquivoanexo_data_selected_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavLotearquivoanexo_data_selected_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLotearquivoanexo_data_selected_Visible), 5, 0)));
         edtavLotearquivoanexo_nomearq_selected_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavLotearquivoanexo_nomearq_selected_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLotearquivoanexo_nomearq_selected_Visible), 5, 0)));
         subGrid_Rows = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrderedby_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrderedby_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrderedby_Visible), 5, 0)));
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
      }

      protected void E14EU2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtLoteArquivoAnexo_NomeArq_Titleformat = 2;
         edtLoteArquivoAnexo_NomeArq_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 2);' >%5</span>", ((AV13OrderedBy==2) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, edtavOrderedby_Internalname, "Arquivo", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtLoteArquivoAnexo_NomeArq_Internalname, "Title", edtLoteArquivoAnexo_NomeArq_Title);
         edtTipoDocumento_Nome_Titleformat = 2;
         edtTipoDocumento_Nome_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 3);' >%5</span>", ((AV13OrderedBy==3) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, edtavOrderedby_Internalname, "Tipo de Documento", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtTipoDocumento_Nome_Internalname, "Title", edtTipoDocumento_Nome_Title);
         edtLoteArquivoAnexo_Data_Titleformat = 2;
         edtLoteArquivoAnexo_Data_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 1);' >%5</span>", ((AV13OrderedBy==1) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, edtavOrderedby_Internalname, "Anexado", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtLoteArquivoAnexo_Data_Internalname, "Title", edtLoteArquivoAnexo_Data_Title);
         edtavUpdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUpdate_Visible), 5, 0)));
         edtavApagar_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavApagar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavApagar_Visible), 5, 0)));
         edtavAcessar_Visible = (AV6WWPContext.gxTpr_Display ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAcessar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAcessar_Visible), 5, 0)));
         edtavLote_nfearq_Display = 1;
         /* Using cursor H00EU6 */
         pr_default.execute(4, new Object[] {AV19LoteArquivoAnexo_LoteCod});
         while ( (pr_default.getStatus(4) != 101) )
         {
            A596Lote_Codigo = H00EU6_A596Lote_Codigo[0];
            A679Lote_NFeNomeArq = H00EU6_A679Lote_NFeNomeArq[0];
            n679Lote_NFeNomeArq = H00EU6_n679Lote_NFeNomeArq[0];
            A678Lote_NFeArq_Filename = A679Lote_NFeNomeArq;
            A680Lote_NFeTipoArq = H00EU6_A680Lote_NFeTipoArq[0];
            n680Lote_NFeTipoArq = H00EU6_n680Lote_NFeTipoArq[0];
            A678Lote_NFeArq_Filetype = A680Lote_NFeTipoArq;
            A673Lote_NFe = H00EU6_A673Lote_NFe[0];
            n673Lote_NFe = H00EU6_n673Lote_NFe[0];
            A678Lote_NFeArq = H00EU6_A678Lote_NFeArq[0];
            n678Lote_NFeArq = H00EU6_n678Lote_NFeArq[0];
            AV46Lote_NFe = A673Lote_NFe;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV46Lote_NFe", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46Lote_NFe), 6, 0)));
            AV47Lote_NFeArq = A678Lote_NFeArq;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavLote_nfearq_Internalname, "URL", context.PathToRelativeUrl( AV47Lote_NFeArq));
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(4);
      }

      private void E15EU2( )
      {
         /* Grid_Load Routine */
         AV17Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV17Update);
         AV51Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("lotearquivoanexo.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A841LoteArquivoAnexo_LoteCod) + "," + UrlEncode(DateTimeUtil.FormatDateTimeParm(A836LoteArquivoAnexo_Data));
         AV21Apagar = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavApagar_Internalname, AV21Apagar);
         AV52Apagar_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavApagar_Tooltiptext = "";
         AV24Acessar = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavAcessar_Internalname, AV24Acessar);
         AV53Acessar_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
         edtavAcessar_Tooltiptext = "Acessar arquivo";
         edtavAcessar_Link = formatLink("aprc_downloadfile.aspx") + "?" + UrlEncode(StringUtil.RTrim(A838LoteArquivoAnexo_Arquivo)) + "," + UrlEncode(StringUtil.RTrim("")) + "," + UrlEncode(StringUtil.RTrim(StringUtil.Trim( A839LoteArquivoAnexo_NomeArq)+"."+StringUtil.Trim( A840LoteArquivoAnexo_TipoArq)));
         edtavAcessar_Linktarget = "_blank";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 29;
         }
         sendrow_292( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_29_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(29, GridRow);
         }
      }

      protected void E12EU2( )
      {
         /* Dvelop_confirmpanel_apagar_Close Routine */
         if ( StringUtil.StrCmp(Dvelop_confirmpanel_apagar_Result, "Yes") == 0 )
         {
            /* Execute user subroutine: 'DO APAGAR' */
            S142 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         dynavLotearquivoanexo_lotecod_selected.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22LoteArquivoAnexo_LoteCod_Selected), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavLotearquivoanexo_lotecod_selected_Internalname, "Values", dynavLotearquivoanexo_lotecod_selected.ToJavascriptSource());
      }

      protected void S142( )
      {
         /* 'DO APAGAR' Routine */
         new prc_lotearquivoanexodlt(context ).execute( ref  AV22LoteArquivoAnexo_LoteCod_Selected, ref  AV23LoteArquivoAnexo_Data_Selected, ref  AV25LoteArquivoAnexo_NomeArq_Selected) ;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22LoteArquivoAnexo_LoteCod_Selected", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22LoteArquivoAnexo_LoteCod_Selected), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23LoteArquivoAnexo_Data_Selected", context.localUtil.TToC( AV23LoteArquivoAnexo_Data_Selected, 8, 5, 0, 3, "/", ":", " "));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25LoteArquivoAnexo_NomeArq_Selected", AV25LoteArquivoAnexo_NomeArq_Selected);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV19LoteArquivoAnexo_LoteCod, A596Lote_Codigo, A673Lote_NFe, A678Lote_NFeArq, AV54Pgmname, A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data, A838LoteArquivoAnexo_Arquivo, A839LoteArquivoAnexo_NomeArq, A840LoteArquivoAnexo_TipoArq, sPrefix) ;
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV16Session.Get(AV54Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV54Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV16Session.Get(AV54Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
      }

      protected void S132( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV16Session.Get(AV54Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         new wwpbaseobjects.savegridstate(context ).execute(  AV54Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV54Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "LoteArquivoAnexo";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "LoteArquivoAnexo_LoteCod";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV19LoteArquivoAnexo_LoteCod), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV16Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void E11EU2( )
      {
         /* Upload_Onallcomplete Routine */
         AV30ServerNow = DateTimeUtil.ServerNow( context, "DEFAULT");
         AV55GXV1 = 1;
         while ( AV55GXV1 <= AV26UploadedFiles.Count )
         {
            AV27File = ((SdtUploadifyOutput)AV26UploadedFiles.Item(AV55GXV1));
            AV29LoteArquivoAnexo = new SdtLoteArquivoAnexo(context);
            AV29LoteArquivoAnexo.gxTpr_Lotearquivoanexo_lotecod = AV19LoteArquivoAnexo_LoteCod;
            AV29LoteArquivoAnexo.gxTpr_Lotearquivoanexo_data = AV30ServerNow;
            AV29LoteArquivoAnexo.gxTpr_Lotearquivoanexo_arquivo = AV27File.gxTpr_Temporalfilename;
            AV29LoteArquivoAnexo.gxTpr_Lotearquivoanexo_nomearq = AV27File.gxTpr_Originalfilename;
            AV29LoteArquivoAnexo.gxTpr_Lotearquivoanexo_tipoarq = AV27File.gxTpr_Filetype;
            AV29LoteArquivoAnexo.gxTv_SdtLoteArquivoAnexo_Tipodocumento_codigo_SetNull();
            AV29LoteArquivoAnexo.Save();
            AV30ServerNow = DateTimeUtil.TAdd( AV30ServerNow, 1);
            AV55GXV1 = (int)(AV55GXV1+1);
         }
         context.CommitDataStores( "LoteArquivosAnexosWC");
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV19LoteArquivoAnexo_LoteCod, A596Lote_Codigo, A673Lote_NFe, A678Lote_NFeArq, AV54Pgmname, A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data, A838LoteArquivoAnexo_Arquivo, A839LoteArquivoAnexo_NomeArq, A840LoteArquivoAnexo_TipoArq, sPrefix) ;
      }

      protected void wb_table2_48_EU2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledvelop_confirmpanel_apagar_Internalname, tblTabledvelop_confirmpanel_apagar_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DVELOP_CONFIRMPANEL_APAGARContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+sPrefix+"DVELOP_CONFIRMPANEL_APAGARContainer"+"Body"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_48_EU2e( true) ;
         }
         else
         {
            wb_table2_48_EU2e( false) ;
         }
      }

      protected void wb_table1_2_EU2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableSearchCell'>") ;
            wb_table3_8_EU2( true) ;
         }
         else
         {
            wb_table3_8_EU2( false) ;
         }
         return  ;
      }

      protected void wb_table3_8_EU2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_13_EU2( true) ;
         }
         else
         {
            wb_table4_13_EU2( false) ;
         }
         return  ;
      }

      protected void wb_table4_13_EU2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_26_EU2( true) ;
         }
         else
         {
            wb_table5_26_EU2( false) ;
         }
         return  ;
      }

      protected void wb_table5_26_EU2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_EU2e( true) ;
         }
         else
         {
            wb_table1_2_EU2e( false) ;
         }
      }

      protected void wb_table5_26_EU2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable3_Internalname, tblUnnamedtable3_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"29\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavApagar_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavAcessar_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Lote Arquivo Anexo_Lote Cod") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtLoteArquivoAnexo_NomeArq_Titleformat == 0 )
               {
                  context.SendWebValue( edtLoteArquivoAnexo_NomeArq_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtLoteArquivoAnexo_NomeArq_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtTipoDocumento_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtTipoDocumento_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtTipoDocumento_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtLoteArquivoAnexo_Data_Titleformat == 0 )
               {
                  context.SendWebValue( edtLoteArquivoAnexo_Data_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtLoteArquivoAnexo_Data_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV17Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV21Apagar));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavApagar_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavApagar_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV24Acessar));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavAcessar_Link));
               GridColumn.AddObjectProperty("Linktarget", StringUtil.RTrim( edtavAcessar_Linktarget));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavAcessar_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavAcessar_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A841LoteArquivoAnexo_LoteCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A839LoteArquivoAnexo_NomeArq));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtLoteArquivoAnexo_NomeArq_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLoteArquivoAnexo_NomeArq_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A646TipoDocumento_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtTipoDocumento_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTipoDocumento_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A836LoteArquivoAnexo_Data, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtLoteArquivoAnexo_Data_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLoteArquivoAnexo_Data_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 29 )
         {
            wbEnd = 0;
            nRC_GXsfl_29 = (short)(nGXsfl_29_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GridContainer.AddObjectProperty("GRID_nEOF", GRID_nEOF);
               GridContainer.AddObjectProperty("GRID_nFirstRecordOnPage", GRID_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table6_38_EU2( true) ;
         }
         else
         {
            wb_table6_38_EU2( false) ;
         }
         return  ;
      }

      protected void wb_table6_38_EU2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_26_EU2e( true) ;
         }
         else
         {
            wb_table5_26_EU2e( false) ;
         }
      }

      protected void wb_table6_38_EU2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUsertbl_Internalname, tblUsertbl_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"UPLOADContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_38_EU2e( true) ;
         }
         else
         {
            wb_table6_38_EU2e( false) ;
         }
      }

      protected void wb_table4_13_EU2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_nfe_Internalname, "NFe", "", "", lblTextblocklote_nfe_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_LoteArquivosAnexosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table7_18_EU2( true) ;
         }
         else
         {
            wb_table7_18_EU2( false) ;
         }
         return  ;
      }

      protected void wb_table7_18_EU2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_EU2e( true) ;
         }
         else
         {
            wb_table4_13_EU2e( false) ;
         }
      }

      protected void wb_table7_18_EU2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedlote_nfe_Internalname, tblTablemergedlote_nfe_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'" + sPrefix + "',false,'" + sGXsfl_29_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLote_nfe_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV46Lote_NFe), 6, 0, ",", "")), ((edtavLote_nfe_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV46Lote_NFe), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV46Lote_NFe), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,21);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_nfe_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavLote_nfe_Enabled, 0, "text", "", 54, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_LoteArquivosAnexosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            ClassString = "BootstrapAttribute";
            StyleString = "";
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'" + sPrefix + "',false,'" + sGXsfl_29_idx + "',0)\"";
            edtavLote_nfearq_Filetype = "tmp";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavLote_nfearq_Internalname, "Filetype", edtavLote_nfearq_Filetype);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47Lote_NFeArq)) )
            {
               gxblobfileaux.Source = AV47Lote_NFeArq;
               if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtavLote_nfearq_Filetype, "tmp") != 0 ) )
               {
                  gxblobfileaux.SetExtension(StringUtil.Trim( edtavLote_nfearq_Filetype));
               }
               if ( gxblobfileaux.ErrCode == 0 )
               {
                  AV47Lote_NFeArq = gxblobfileaux.GetAbsoluteName();
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavLote_nfearq_Internalname, "URL", context.PathToRelativeUrl( AV47Lote_NFeArq));
                  edtavLote_nfearq_Filetype = gxblobfileaux.GetExtension();
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavLote_nfearq_Internalname, "Filetype", edtavLote_nfearq_Filetype);
               }
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavLote_nfearq_Internalname, "URL", context.PathToRelativeUrl( AV47Lote_NFeArq));
            }
            GxWebStd.gx_blob_field( context, edtavLote_nfearq_Internalname, StringUtil.RTrim( AV47Lote_NFeArq), context.PathToRelativeUrl( AV47Lote_NFeArq), (String.IsNullOrEmpty(StringUtil.RTrim( edtavLote_nfearq_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtavLote_nfearq_Filetype)) ? AV47Lote_NFeArq : edtavLote_nfearq_Filetype)) : edtavLote_nfearq_Contenttype), false, "", edtavLote_nfearq_Parameters, edtavLote_nfearq_Display, edtavLote_nfearq_Enabled, 1, "", "", 0, -1, 250, "px", 60, "px", 0, 0, 0, edtavLote_nfearq_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", StyleString, ClassString, "", ""+TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"", "", "", "HLP_LoteArquivosAnexosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_18_EU2e( true) ;
         }
         else
         {
            wb_table7_18_EU2e( false) ;
         }
      }

      protected void wb_table3_8_EU2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_8_EU2e( true) ;
         }
         else
         {
            wb_table3_8_EU2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV19LoteArquivoAnexo_LoteCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19LoteArquivoAnexo_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19LoteArquivoAnexo_LoteCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAEU2( ) ;
         WSEU2( ) ;
         WEEU2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV19LoteArquivoAnexo_LoteCod = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAEU2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "lotearquivosanexoswc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAEU2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV19LoteArquivoAnexo_LoteCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19LoteArquivoAnexo_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19LoteArquivoAnexo_LoteCod), 6, 0)));
         }
         wcpOAV19LoteArquivoAnexo_LoteCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV19LoteArquivoAnexo_LoteCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV19LoteArquivoAnexo_LoteCod != wcpOAV19LoteArquivoAnexo_LoteCod ) ) )
         {
            setjustcreated();
         }
         wcpOAV19LoteArquivoAnexo_LoteCod = AV19LoteArquivoAnexo_LoteCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV19LoteArquivoAnexo_LoteCod = cgiGet( sPrefix+"AV19LoteArquivoAnexo_LoteCod_CTRL");
         if ( StringUtil.Len( sCtrlAV19LoteArquivoAnexo_LoteCod) > 0 )
         {
            AV19LoteArquivoAnexo_LoteCod = (int)(context.localUtil.CToN( cgiGet( sCtrlAV19LoteArquivoAnexo_LoteCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19LoteArquivoAnexo_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19LoteArquivoAnexo_LoteCod), 6, 0)));
         }
         else
         {
            AV19LoteArquivoAnexo_LoteCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV19LoteArquivoAnexo_LoteCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAEU2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSEU2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSEU2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV19LoteArquivoAnexo_LoteCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV19LoteArquivoAnexo_LoteCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV19LoteArquivoAnexo_LoteCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV19LoteArquivoAnexo_LoteCod_CTRL", StringUtil.RTrim( sCtrlAV19LoteArquivoAnexo_LoteCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEEU2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("Uploadify/uploadify.css", "");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202031921121931");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("lotearquivosanexoswc.js", "?202031921121931");
            context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
            context.AddJavascriptSource("Uploadify/swfobject.js", "");
            context.AddJavascriptSource("Uploadify/jquery.uploadify.v2.1.4.min.js", "");
            context.AddJavascriptSource("Uploadify/UploadifyRender.js", "");
            context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         }
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_292( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_29_idx;
         edtavApagar_Internalname = sPrefix+"vAPAGAR_"+sGXsfl_29_idx;
         edtavAcessar_Internalname = sPrefix+"vACESSAR_"+sGXsfl_29_idx;
         edtLoteArquivoAnexo_LoteCod_Internalname = sPrefix+"LOTEARQUIVOANEXO_LOTECOD_"+sGXsfl_29_idx;
         edtLoteArquivoAnexo_NomeArq_Internalname = sPrefix+"LOTEARQUIVOANEXO_NOMEARQ_"+sGXsfl_29_idx;
         edtTipoDocumento_Nome_Internalname = sPrefix+"TIPODOCUMENTO_NOME_"+sGXsfl_29_idx;
         edtLoteArquivoAnexo_Data_Internalname = sPrefix+"LOTEARQUIVOANEXO_DATA_"+sGXsfl_29_idx;
      }

      protected void SubsflControlProps_fel_292( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_29_fel_idx;
         edtavApagar_Internalname = sPrefix+"vAPAGAR_"+sGXsfl_29_fel_idx;
         edtavAcessar_Internalname = sPrefix+"vACESSAR_"+sGXsfl_29_fel_idx;
         edtLoteArquivoAnexo_LoteCod_Internalname = sPrefix+"LOTEARQUIVOANEXO_LOTECOD_"+sGXsfl_29_fel_idx;
         edtLoteArquivoAnexo_NomeArq_Internalname = sPrefix+"LOTEARQUIVOANEXO_NOMEARQ_"+sGXsfl_29_fel_idx;
         edtTipoDocumento_Nome_Internalname = sPrefix+"TIPODOCUMENTO_NOME_"+sGXsfl_29_fel_idx;
         edtLoteArquivoAnexo_Data_Internalname = sPrefix+"LOTEARQUIVOANEXO_DATA_"+sGXsfl_29_fel_idx;
      }

      protected void sendrow_292( )
      {
         SubsflControlProps_292( ) ;
         WBEU0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_29_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_29_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_29_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV17Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV17Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV51Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV17Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV17Update)) ? AV51Update_GXI : context.PathToRelativeUrl( AV17Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavUpdate_Visible,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV17Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavApagar_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavApagar_Enabled!=0)&&(edtavApagar_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 31,'"+sPrefix+"',false,'',29)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV21Apagar_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV21Apagar))&&String.IsNullOrEmpty(StringUtil.RTrim( AV52Apagar_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV21Apagar)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavApagar_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV21Apagar)) ? AV52Apagar_GXI : context.PathToRelativeUrl( AV21Apagar)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavApagar_Visible,(short)1,(String)"",(String)edtavApagar_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavApagar_Jsonclick,(String)"'"+sPrefix+"'"+",false,"+"'"+"e16eu2_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV21Apagar_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavAcessar_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV24Acessar_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV24Acessar))&&String.IsNullOrEmpty(StringUtil.RTrim( AV53Acessar_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV24Acessar)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavAcessar_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV24Acessar)) ? AV53Acessar_GXI : context.PathToRelativeUrl( AV24Acessar)),(String)edtavAcessar_Link,(String)edtavAcessar_Linktarget,(String)"",context.GetTheme( ),(int)edtavAcessar_Visible,(short)1,(String)"",(String)edtavAcessar_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV24Acessar_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLoteArquivoAnexo_LoteCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A841LoteArquivoAnexo_LoteCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A841LoteArquivoAnexo_LoteCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLoteArquivoAnexo_LoteCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)29,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLoteArquivoAnexo_NomeArq_Internalname,StringUtil.RTrim( A839LoteArquivoAnexo_NomeArq),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLoteArquivoAnexo_NomeArq_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)29,(short)1,(short)-1,(short)-1,(bool)true,(String)"NomeArq",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTipoDocumento_Nome_Internalname,StringUtil.RTrim( A646TipoDocumento_Nome),StringUtil.RTrim( context.localUtil.Format( A646TipoDocumento_Nome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTipoDocumento_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)29,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLoteArquivoAnexo_Data_Internalname,context.localUtil.TToC( A836LoteArquivoAnexo_Data, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A836LoteArquivoAnexo_Data, "99/99/99 99:99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLoteArquivoAnexo_Data_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)29,(short)1,(short)-1,(short)0,(bool)true,(String)"DataHora",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_LOTEARQUIVOANEXO_LOTECOD"+"_"+sGXsfl_29_idx, GetSecureSignedToken( sPrefix+sGXsfl_29_idx, context.localUtil.Format( (decimal)(A841LoteArquivoAnexo_LoteCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_LOTEARQUIVOANEXO_DATA"+"_"+sGXsfl_29_idx, GetSecureSignedToken( sPrefix+sGXsfl_29_idx, context.localUtil.Format( A836LoteArquivoAnexo_Data, "99/99/99 99:99")));
            GridContainer.AddRow(GridRow);
            nGXsfl_29_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_29_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_29_idx+1));
            sGXsfl_29_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_29_idx), 4, 0)), 4, "0");
            SubsflControlProps_292( ) ;
         }
         /* End function sendrow_292 */
      }

      protected void init_default_properties( )
      {
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         lblTextblocklote_nfe_Internalname = sPrefix+"TEXTBLOCKLOTE_NFE";
         edtavLote_nfe_Internalname = sPrefix+"vLOTE_NFE";
         edtavLote_nfearq_Internalname = sPrefix+"vLOTE_NFEARQ";
         tblTablemergedlote_nfe_Internalname = sPrefix+"TABLEMERGEDLOTE_NFE";
         tblUnnamedtable2_Internalname = sPrefix+"UNNAMEDTABLE2";
         edtavUpdate_Internalname = sPrefix+"vUPDATE";
         edtavApagar_Internalname = sPrefix+"vAPAGAR";
         edtavAcessar_Internalname = sPrefix+"vACESSAR";
         edtLoteArquivoAnexo_LoteCod_Internalname = sPrefix+"LOTEARQUIVOANEXO_LOTECOD";
         edtLoteArquivoAnexo_NomeArq_Internalname = sPrefix+"LOTEARQUIVOANEXO_NOMEARQ";
         edtTipoDocumento_Nome_Internalname = sPrefix+"TIPODOCUMENTO_NOME";
         edtLoteArquivoAnexo_Data_Internalname = sPrefix+"LOTEARQUIVOANEXO_DATA";
         Upload_Internalname = sPrefix+"UPLOAD";
         tblUsertbl_Internalname = sPrefix+"USERTBL";
         tblUnnamedtable3_Internalname = sPrefix+"UNNAMEDTABLE3";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         dynavLotearquivoanexo_lotecod_selected_Internalname = sPrefix+"vLOTEARQUIVOANEXO_LOTECOD_SELECTED";
         edtavLotearquivoanexo_data_selected_Internalname = sPrefix+"vLOTEARQUIVOANEXO_DATA_SELECTED";
         edtavLotearquivoanexo_nomearq_selected_Internalname = sPrefix+"vLOTEARQUIVOANEXO_NOMEARQ_SELECTED";
         edtavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         Dvelop_confirmpanel_apagar_Internalname = sPrefix+"DVELOP_CONFIRMPANEL_APAGAR";
         tblTabledvelop_confirmpanel_apagar_Internalname = sPrefix+"TABLEDVELOP_CONFIRMPANEL_APAGAR";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtLoteArquivoAnexo_Data_Jsonclick = "";
         edtTipoDocumento_Nome_Jsonclick = "";
         edtLoteArquivoAnexo_NomeArq_Jsonclick = "";
         edtLoteArquivoAnexo_LoteCod_Jsonclick = "";
         edtavApagar_Jsonclick = "";
         edtavApagar_Enabled = 1;
         edtavLote_nfearq_Jsonclick = "";
         edtavLote_nfearq_Parameters = "";
         edtavLote_nfearq_Contenttype = "";
         edtavLote_nfearq_Filetype = "";
         edtavLote_nfearq_Display = 0;
         edtavLote_nfearq_Enabled = 1;
         edtavLote_nfe_Jsonclick = "";
         edtavLote_nfe_Enabled = 1;
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavAcessar_Tooltiptext = "Acessar arquivo";
         edtavAcessar_Linktarget = "";
         edtavAcessar_Link = "";
         edtavApagar_Tooltiptext = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtLoteArquivoAnexo_Data_Titleformat = 0;
         edtTipoDocumento_Nome_Titleformat = 0;
         edtLoteArquivoAnexo_NomeArq_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavAcessar_Visible = -1;
         edtavApagar_Visible = -1;
         edtavUpdate_Visible = -1;
         edtLoteArquivoAnexo_Data_Title = "Anexado";
         edtTipoDocumento_Nome_Title = "Tipo de Documento";
         edtLoteArquivoAnexo_NomeArq_Title = "Arquivo";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         edtavOrderedby_Jsonclick = "";
         edtavOrderedby_Visible = 1;
         edtavLotearquivoanexo_nomearq_selected_Jsonclick = "";
         edtavLotearquivoanexo_nomearq_selected_Visible = 1;
         edtavLotearquivoanexo_data_selected_Jsonclick = "";
         edtavLotearquivoanexo_data_selected_Visible = 1;
         dynavLotearquivoanexo_lotecod_selected_Jsonclick = "";
         dynavLotearquivoanexo_lotecod_selected.Visible = 1;
         Dvelop_confirmpanel_apagar_Confirmtype = "1";
         Dvelop_confirmpanel_apagar_Yesbuttonposition = "left";
         Dvelop_confirmpanel_apagar_Cancelbuttoncaption = "Cancelar";
         Dvelop_confirmpanel_apagar_Nobuttoncaption = "N�o";
         Dvelop_confirmpanel_apagar_Yesbuttoncaption = "Sim";
         Dvelop_confirmpanel_apagar_Confirmationtext = "Confirma apagar o arquivo anexado?";
         Dvelop_confirmpanel_apagar_Title = "Aten��o";
         Upload_Simultaneousuploadlimit = "10";
         Upload_Sizelimit = 30720;
         Upload_Buttonimage = "\\Resources\\Anexar.png";
         Upload_Buttontext = "";
         Upload_Multi = Convert.ToBoolean( -1);
         Upload_Width = "30";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A841LoteArquivoAnexo_LoteCod',fld:'LOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A836LoteArquivoAnexo_Data',fld:'LOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A838LoteArquivoAnexo_Arquivo',fld:'LOTEARQUIVOANEXO_ARQUIVO',pic:'',nv:''},{av:'A839LoteArquivoAnexo_NomeArq',fld:'LOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''},{av:'A840LoteArquivoAnexo_TipoArq',fld:'LOTEARQUIVOANEXO_TIPOARQ',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'A596Lote_Codigo',fld:'LOTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19LoteArquivoAnexo_LoteCod',fld:'vLOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',nv:0},{av:'A673Lote_NFe',fld:'LOTE_NFE',pic:'ZZZZZ9',nv:0},{av:'A678Lote_NFeArq',fld:'LOTE_NFEARQ',pic:'',nv:''},{av:'AV54Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'edtLoteArquivoAnexo_NomeArq_Titleformat',ctrl:'LOTEARQUIVOANEXO_NOMEARQ',prop:'Titleformat'},{av:'edtLoteArquivoAnexo_NomeArq_Title',ctrl:'LOTEARQUIVOANEXO_NOMEARQ',prop:'Title'},{av:'edtTipoDocumento_Nome_Titleformat',ctrl:'TIPODOCUMENTO_NOME',prop:'Titleformat'},{av:'edtTipoDocumento_Nome_Title',ctrl:'TIPODOCUMENTO_NOME',prop:'Title'},{av:'edtLoteArquivoAnexo_Data_Titleformat',ctrl:'LOTEARQUIVOANEXO_DATA',prop:'Titleformat'},{av:'edtLoteArquivoAnexo_Data_Title',ctrl:'LOTEARQUIVOANEXO_DATA',prop:'Title'},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavApagar_Visible',ctrl:'vAPAGAR',prop:'Visible'},{av:'edtavAcessar_Visible',ctrl:'vACESSAR',prop:'Visible'},{av:'edtavLote_nfearq_Display',ctrl:'vLOTE_NFEARQ',prop:'Display'},{av:'AV46Lote_NFe',fld:'vLOTE_NFE',pic:'ZZZZZ9',nv:0},{av:'AV47Lote_NFeArq',fld:'vLOTE_NFEARQ',pic:'',nv:''}]}");
         setEventMetadata("GRID.LOAD","{handler:'E15EU2',iparms:[{av:'A841LoteArquivoAnexo_LoteCod',fld:'LOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A836LoteArquivoAnexo_Data',fld:'LOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A838LoteArquivoAnexo_Arquivo',fld:'LOTEARQUIVOANEXO_ARQUIVO',pic:'',nv:''},{av:'A839LoteArquivoAnexo_NomeArq',fld:'LOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''},{av:'A840LoteArquivoAnexo_TipoArq',fld:'LOTEARQUIVOANEXO_TIPOARQ',pic:'',nv:''}],oparms:[{av:'AV17Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV21Apagar',fld:'vAPAGAR',pic:'',nv:''},{av:'edtavApagar_Tooltiptext',ctrl:'vAPAGAR',prop:'Tooltiptext'},{av:'AV24Acessar',fld:'vACESSAR',pic:'',nv:''},{av:'edtavAcessar_Tooltiptext',ctrl:'vACESSAR',prop:'Tooltiptext'},{av:'edtavAcessar_Link',ctrl:'vACESSAR',prop:'Link'},{av:'edtavAcessar_Linktarget',ctrl:'vACESSAR',prop:'Linktarget'}]}");
         setEventMetadata("DVELOP_CONFIRMPANEL_APAGAR.CLOSE","{handler:'E12EU2',iparms:[{av:'Dvelop_confirmpanel_apagar_Result',ctrl:'DVELOP_CONFIRMPANEL_APAGAR',prop:'Result'},{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV19LoteArquivoAnexo_LoteCod',fld:'vLOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',nv:0},{av:'A596Lote_Codigo',fld:'LOTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A673Lote_NFe',fld:'LOTE_NFE',pic:'ZZZZZ9',nv:0},{av:'A678Lote_NFeArq',fld:'LOTE_NFEARQ',pic:'',nv:''},{av:'AV54Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A841LoteArquivoAnexo_LoteCod',fld:'LOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A836LoteArquivoAnexo_Data',fld:'LOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A838LoteArquivoAnexo_Arquivo',fld:'LOTEARQUIVOANEXO_ARQUIVO',pic:'',nv:''},{av:'A839LoteArquivoAnexo_NomeArq',fld:'LOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''},{av:'A840LoteArquivoAnexo_TipoArq',fld:'LOTEARQUIVOANEXO_TIPOARQ',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'AV22LoteArquivoAnexo_LoteCod_Selected',fld:'vLOTEARQUIVOANEXO_LOTECOD_SELECTED',pic:'ZZZZZ9',nv:0},{av:'AV23LoteArquivoAnexo_Data_Selected',fld:'vLOTEARQUIVOANEXO_DATA_SELECTED',pic:'99/99/99 99:99',nv:''},{av:'AV25LoteArquivoAnexo_NomeArq_Selected',fld:'vLOTEARQUIVOANEXO_NOMEARQ_SELECTED',pic:'',nv:''}],oparms:[{av:'AV25LoteArquivoAnexo_NomeArq_Selected',fld:'vLOTEARQUIVOANEXO_NOMEARQ_SELECTED',pic:'',nv:''},{av:'AV23LoteArquivoAnexo_Data_Selected',fld:'vLOTEARQUIVOANEXO_DATA_SELECTED',pic:'99/99/99 99:99',nv:''},{av:'AV22LoteArquivoAnexo_LoteCod_Selected',fld:'vLOTEARQUIVOANEXO_LOTECOD_SELECTED',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VAPAGAR.CLICK","{handler:'E16EU2',iparms:[{av:'A841LoteArquivoAnexo_LoteCod',fld:'LOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A836LoteArquivoAnexo_Data',fld:'LOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A839LoteArquivoAnexo_NomeArq',fld:'LOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''}],oparms:[{av:'AV22LoteArquivoAnexo_LoteCod_Selected',fld:'vLOTEARQUIVOANEXO_LOTECOD_SELECTED',pic:'ZZZZZ9',nv:0},{av:'AV23LoteArquivoAnexo_Data_Selected',fld:'vLOTEARQUIVOANEXO_DATA_SELECTED',pic:'99/99/99 99:99',nv:''},{av:'AV25LoteArquivoAnexo_NomeArq_Selected',fld:'vLOTEARQUIVOANEXO_NOMEARQ_SELECTED',pic:'',nv:''}]}");
         setEventMetadata("UPLOAD.ONALLCOMPLETE","{handler:'E11EU2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV19LoteArquivoAnexo_LoteCod',fld:'vLOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',nv:0},{av:'A596Lote_Codigo',fld:'LOTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A673Lote_NFe',fld:'LOTE_NFE',pic:'ZZZZZ9',nv:0},{av:'A678Lote_NFeArq',fld:'LOTE_NFEARQ',pic:'',nv:''},{av:'AV54Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A841LoteArquivoAnexo_LoteCod',fld:'LOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A836LoteArquivoAnexo_Data',fld:'LOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A838LoteArquivoAnexo_Arquivo',fld:'LOTEARQUIVOANEXO_ARQUIVO',pic:'',nv:''},{av:'A839LoteArquivoAnexo_NomeArq',fld:'LOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''},{av:'A840LoteArquivoAnexo_TipoArq',fld:'LOTEARQUIVOANEXO_TIPOARQ',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'AV26UploadedFiles',fld:'vUPLOADEDFILES',pic:'',nv:null}],oparms:[]}");
         setEventMetadata("GRID_FIRSTPAGE","{handler:'subgrid_firstpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A841LoteArquivoAnexo_LoteCod',fld:'LOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A836LoteArquivoAnexo_Data',fld:'LOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A838LoteArquivoAnexo_Arquivo',fld:'LOTEARQUIVOANEXO_ARQUIVO',pic:'',nv:''},{av:'A839LoteArquivoAnexo_NomeArq',fld:'LOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''},{av:'A840LoteArquivoAnexo_TipoArq',fld:'LOTEARQUIVOANEXO_TIPOARQ',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'A596Lote_Codigo',fld:'LOTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19LoteArquivoAnexo_LoteCod',fld:'vLOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',nv:0},{av:'A673Lote_NFe',fld:'LOTE_NFE',pic:'ZZZZZ9',nv:0},{av:'A678Lote_NFeArq',fld:'LOTE_NFEARQ',pic:'',nv:''},{av:'AV54Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'edtLoteArquivoAnexo_NomeArq_Titleformat',ctrl:'LOTEARQUIVOANEXO_NOMEARQ',prop:'Titleformat'},{av:'edtLoteArquivoAnexo_NomeArq_Title',ctrl:'LOTEARQUIVOANEXO_NOMEARQ',prop:'Title'},{av:'edtTipoDocumento_Nome_Titleformat',ctrl:'TIPODOCUMENTO_NOME',prop:'Titleformat'},{av:'edtTipoDocumento_Nome_Title',ctrl:'TIPODOCUMENTO_NOME',prop:'Title'},{av:'edtLoteArquivoAnexo_Data_Titleformat',ctrl:'LOTEARQUIVOANEXO_DATA',prop:'Titleformat'},{av:'edtLoteArquivoAnexo_Data_Title',ctrl:'LOTEARQUIVOANEXO_DATA',prop:'Title'},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavApagar_Visible',ctrl:'vAPAGAR',prop:'Visible'},{av:'edtavAcessar_Visible',ctrl:'vACESSAR',prop:'Visible'},{av:'edtavLote_nfearq_Display',ctrl:'vLOTE_NFEARQ',prop:'Display'},{av:'AV46Lote_NFe',fld:'vLOTE_NFE',pic:'ZZZZZ9',nv:0},{av:'AV47Lote_NFeArq',fld:'vLOTE_NFEARQ',pic:'',nv:''}]}");
         setEventMetadata("GRID_PREVPAGE","{handler:'subgrid_previouspage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A841LoteArquivoAnexo_LoteCod',fld:'LOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A836LoteArquivoAnexo_Data',fld:'LOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A838LoteArquivoAnexo_Arquivo',fld:'LOTEARQUIVOANEXO_ARQUIVO',pic:'',nv:''},{av:'A839LoteArquivoAnexo_NomeArq',fld:'LOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''},{av:'A840LoteArquivoAnexo_TipoArq',fld:'LOTEARQUIVOANEXO_TIPOARQ',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'A596Lote_Codigo',fld:'LOTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19LoteArquivoAnexo_LoteCod',fld:'vLOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',nv:0},{av:'A673Lote_NFe',fld:'LOTE_NFE',pic:'ZZZZZ9',nv:0},{av:'A678Lote_NFeArq',fld:'LOTE_NFEARQ',pic:'',nv:''},{av:'AV54Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'edtLoteArquivoAnexo_NomeArq_Titleformat',ctrl:'LOTEARQUIVOANEXO_NOMEARQ',prop:'Titleformat'},{av:'edtLoteArquivoAnexo_NomeArq_Title',ctrl:'LOTEARQUIVOANEXO_NOMEARQ',prop:'Title'},{av:'edtTipoDocumento_Nome_Titleformat',ctrl:'TIPODOCUMENTO_NOME',prop:'Titleformat'},{av:'edtTipoDocumento_Nome_Title',ctrl:'TIPODOCUMENTO_NOME',prop:'Title'},{av:'edtLoteArquivoAnexo_Data_Titleformat',ctrl:'LOTEARQUIVOANEXO_DATA',prop:'Titleformat'},{av:'edtLoteArquivoAnexo_Data_Title',ctrl:'LOTEARQUIVOANEXO_DATA',prop:'Title'},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavApagar_Visible',ctrl:'vAPAGAR',prop:'Visible'},{av:'edtavAcessar_Visible',ctrl:'vACESSAR',prop:'Visible'},{av:'edtavLote_nfearq_Display',ctrl:'vLOTE_NFEARQ',prop:'Display'},{av:'AV46Lote_NFe',fld:'vLOTE_NFE',pic:'ZZZZZ9',nv:0},{av:'AV47Lote_NFeArq',fld:'vLOTE_NFEARQ',pic:'',nv:''}]}");
         setEventMetadata("GRID_NEXTPAGE","{handler:'subgrid_nextpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A841LoteArquivoAnexo_LoteCod',fld:'LOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A836LoteArquivoAnexo_Data',fld:'LOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A838LoteArquivoAnexo_Arquivo',fld:'LOTEARQUIVOANEXO_ARQUIVO',pic:'',nv:''},{av:'A839LoteArquivoAnexo_NomeArq',fld:'LOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''},{av:'A840LoteArquivoAnexo_TipoArq',fld:'LOTEARQUIVOANEXO_TIPOARQ',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'A596Lote_Codigo',fld:'LOTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19LoteArquivoAnexo_LoteCod',fld:'vLOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',nv:0},{av:'A673Lote_NFe',fld:'LOTE_NFE',pic:'ZZZZZ9',nv:0},{av:'A678Lote_NFeArq',fld:'LOTE_NFEARQ',pic:'',nv:''},{av:'AV54Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'edtLoteArquivoAnexo_NomeArq_Titleformat',ctrl:'LOTEARQUIVOANEXO_NOMEARQ',prop:'Titleformat'},{av:'edtLoteArquivoAnexo_NomeArq_Title',ctrl:'LOTEARQUIVOANEXO_NOMEARQ',prop:'Title'},{av:'edtTipoDocumento_Nome_Titleformat',ctrl:'TIPODOCUMENTO_NOME',prop:'Titleformat'},{av:'edtTipoDocumento_Nome_Title',ctrl:'TIPODOCUMENTO_NOME',prop:'Title'},{av:'edtLoteArquivoAnexo_Data_Titleformat',ctrl:'LOTEARQUIVOANEXO_DATA',prop:'Titleformat'},{av:'edtLoteArquivoAnexo_Data_Title',ctrl:'LOTEARQUIVOANEXO_DATA',prop:'Title'},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavApagar_Visible',ctrl:'vAPAGAR',prop:'Visible'},{av:'edtavAcessar_Visible',ctrl:'vACESSAR',prop:'Visible'},{av:'edtavLote_nfearq_Display',ctrl:'vLOTE_NFEARQ',prop:'Display'},{av:'AV46Lote_NFe',fld:'vLOTE_NFE',pic:'ZZZZZ9',nv:0},{av:'AV47Lote_NFeArq',fld:'vLOTE_NFEARQ',pic:'',nv:''}]}");
         setEventMetadata("GRID_LASTPAGE","{handler:'subgrid_lastpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A841LoteArquivoAnexo_LoteCod',fld:'LOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A836LoteArquivoAnexo_Data',fld:'LOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A838LoteArquivoAnexo_Arquivo',fld:'LOTEARQUIVOANEXO_ARQUIVO',pic:'',nv:''},{av:'A839LoteArquivoAnexo_NomeArq',fld:'LOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''},{av:'A840LoteArquivoAnexo_TipoArq',fld:'LOTEARQUIVOANEXO_TIPOARQ',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'A596Lote_Codigo',fld:'LOTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19LoteArquivoAnexo_LoteCod',fld:'vLOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',nv:0},{av:'A673Lote_NFe',fld:'LOTE_NFE',pic:'ZZZZZ9',nv:0},{av:'A678Lote_NFeArq',fld:'LOTE_NFEARQ',pic:'',nv:''},{av:'AV54Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'edtLoteArquivoAnexo_NomeArq_Titleformat',ctrl:'LOTEARQUIVOANEXO_NOMEARQ',prop:'Titleformat'},{av:'edtLoteArquivoAnexo_NomeArq_Title',ctrl:'LOTEARQUIVOANEXO_NOMEARQ',prop:'Title'},{av:'edtTipoDocumento_Nome_Titleformat',ctrl:'TIPODOCUMENTO_NOME',prop:'Titleformat'},{av:'edtTipoDocumento_Nome_Title',ctrl:'TIPODOCUMENTO_NOME',prop:'Title'},{av:'edtLoteArquivoAnexo_Data_Titleformat',ctrl:'LOTEARQUIVOANEXO_DATA',prop:'Titleformat'},{av:'edtLoteArquivoAnexo_Data_Title',ctrl:'LOTEARQUIVOANEXO_DATA',prop:'Title'},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavApagar_Visible',ctrl:'vAPAGAR',prop:'Visible'},{av:'edtavAcessar_Visible',ctrl:'vACESSAR',prop:'Visible'},{av:'edtavLote_nfearq_Display',ctrl:'vLOTE_NFEARQ',prop:'Display'},{av:'AV46Lote_NFe',fld:'vLOTE_NFE',pic:'ZZZZZ9',nv:0},{av:'AV47Lote_NFeArq',fld:'vLOTE_NFEARQ',pic:'',nv:''}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Dvelop_confirmpanel_apagar_Result = "";
         Upload_File = new SdtUploadifyOutput(context);
         Upload_Error = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         A678Lote_NFeArq = "";
         AV54Pgmname = "";
         A836LoteArquivoAnexo_Data = (DateTime)(DateTime.MinValue);
         A838LoteArquivoAnexo_Arquivo = "";
         A839LoteArquivoAnexo_NomeArq = "";
         A840LoteArquivoAnexo_TipoArq = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV26UploadedFiles = new GxObjectCollection( context, "UploadifyOutput", "GxEv3Up14_Meetrika", "SdtUploadifyOutput", "GeneXus.Programs");
         AV28QueuedFiles = new GxObjectCollection( context, "UploadifyOutput", "GxEv3Up14_Meetrika", "SdtUploadifyOutput", "GeneXus.Programs");
         Gx_msg = "";
         AV27File = new SdtUploadifyOutput(context);
         GX_FocusControl = "";
         TempTags = "";
         AV23LoteArquivoAnexo_Data_Selected = DateTimeUtil.ServerNow( context, "DEFAULT");
         AV25LoteArquivoAnexo_NomeArq_Selected = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV17Update = "";
         AV51Update_GXI = "";
         AV21Apagar = "";
         AV52Apagar_GXI = "";
         AV24Acessar = "";
         AV53Acessar_GXI = "";
         A646TipoDocumento_Nome = "";
         scmdbuf = "";
         H00EU2_A596Lote_Codigo = new int[1] ;
         H00EU2_A563Lote_Nome = new String[] {""} ;
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         H00EU3_A596Lote_Codigo = new int[1] ;
         H00EU3_A563Lote_Nome = new String[] {""} ;
         GridContainer = new GXWebGrid( context);
         H00EU4_A645TipoDocumento_Codigo = new int[1] ;
         H00EU4_n645TipoDocumento_Codigo = new bool[] {false} ;
         H00EU4_A839LoteArquivoAnexo_NomeArq = new String[] {""} ;
         H00EU4_n839LoteArquivoAnexo_NomeArq = new bool[] {false} ;
         H00EU4_A840LoteArquivoAnexo_TipoArq = new String[] {""} ;
         H00EU4_n840LoteArquivoAnexo_TipoArq = new bool[] {false} ;
         H00EU4_A836LoteArquivoAnexo_Data = new DateTime[] {DateTime.MinValue} ;
         H00EU4_A646TipoDocumento_Nome = new String[] {""} ;
         H00EU4_A841LoteArquivoAnexo_LoteCod = new int[1] ;
         H00EU4_A838LoteArquivoAnexo_Arquivo = new String[] {""} ;
         H00EU4_n838LoteArquivoAnexo_Arquivo = new bool[] {false} ;
         A838LoteArquivoAnexo_Arquivo_Filename = "";
         A838LoteArquivoAnexo_Arquivo_Filetype = "";
         H00EU5_AGRID_nRecordCount = new long[1] ;
         AV47Lote_NFeArq = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         H00EU6_A596Lote_Codigo = new int[1] ;
         H00EU6_A679Lote_NFeNomeArq = new String[] {""} ;
         H00EU6_n679Lote_NFeNomeArq = new bool[] {false} ;
         H00EU6_A680Lote_NFeTipoArq = new String[] {""} ;
         H00EU6_n680Lote_NFeTipoArq = new bool[] {false} ;
         H00EU6_A673Lote_NFe = new int[1] ;
         H00EU6_n673Lote_NFe = new bool[] {false} ;
         H00EU6_A678Lote_NFeArq = new String[] {""} ;
         H00EU6_n678Lote_NFeArq = new bool[] {false} ;
         A679Lote_NFeNomeArq = "";
         A678Lote_NFeArq_Filename = "";
         A680Lote_NFeTipoArq = "";
         A678Lote_NFeArq_Filetype = "";
         GridRow = new GXWebRow();
         AV16Session = context.GetSession();
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV30ServerNow = (DateTime)(DateTime.MinValue);
         AV29LoteArquivoAnexo = new SdtLoteArquivoAnexo(context);
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblTextblocklote_nfe_Jsonclick = "";
         gxblobfileaux = new GxFile(context.GetPhysicalPath());
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV19LoteArquivoAnexo_LoteCod = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.lotearquivosanexoswc__default(),
            new Object[][] {
                new Object[] {
               H00EU2_A596Lote_Codigo, H00EU2_A563Lote_Nome
               }
               , new Object[] {
               H00EU3_A596Lote_Codigo, H00EU3_A563Lote_Nome
               }
               , new Object[] {
               H00EU4_A645TipoDocumento_Codigo, H00EU4_n645TipoDocumento_Codigo, H00EU4_A839LoteArquivoAnexo_NomeArq, H00EU4_n839LoteArquivoAnexo_NomeArq, H00EU4_A840LoteArquivoAnexo_TipoArq, H00EU4_n840LoteArquivoAnexo_TipoArq, H00EU4_A836LoteArquivoAnexo_Data, H00EU4_A646TipoDocumento_Nome, H00EU4_A841LoteArquivoAnexo_LoteCod, H00EU4_A838LoteArquivoAnexo_Arquivo,
               H00EU4_n838LoteArquivoAnexo_Arquivo
               }
               , new Object[] {
               H00EU5_AGRID_nRecordCount
               }
               , new Object[] {
               H00EU6_A596Lote_Codigo, H00EU6_A679Lote_NFeNomeArq, H00EU6_n679Lote_NFeNomeArq, H00EU6_A680Lote_NFeTipoArq, H00EU6_n680Lote_NFeTipoArq, H00EU6_A673Lote_NFe, H00EU6_n673Lote_NFe, H00EU6_A678Lote_NFeArq, H00EU6_n678Lote_NFeArq
               }
            }
         );
         AV54Pgmname = "LoteArquivosAnexosWC";
         /* GeneXus formulas. */
         AV54Pgmname = "LoteArquivosAnexosWC";
         context.Gx_err = 0;
         edtavLote_nfe_Enabled = 0;
         edtavLote_nfearq_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_29 ;
      private short nGXsfl_29_idx=1 ;
      private short AV13OrderedBy ;
      private short initialized ;
      private short nGXWrapped ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_29_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtLoteArquivoAnexo_NomeArq_Titleformat ;
      private short edtTipoDocumento_Nome_Titleformat ;
      private short edtLoteArquivoAnexo_Data_Titleformat ;
      private short edtavLote_nfearq_Display ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short subGrid_Backstyle ;
      private int AV19LoteArquivoAnexo_LoteCod ;
      private int wcpOAV19LoteArquivoAnexo_LoteCod ;
      private int Upload_Sizelimit ;
      private int subGrid_Rows ;
      private int A596Lote_Codigo ;
      private int A673Lote_NFe ;
      private int A841LoteArquivoAnexo_LoteCod ;
      private int edtavLote_nfe_Enabled ;
      private int edtavLote_nfearq_Enabled ;
      private int AV22LoteArquivoAnexo_LoteCod_Selected ;
      private int edtavLotearquivoanexo_data_selected_Visible ;
      private int edtavLotearquivoanexo_nomearq_selected_Visible ;
      private int edtavOrderedby_Visible ;
      private int edtavOrdereddsc_Visible ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int A645TipoDocumento_Codigo ;
      private int AV46Lote_NFe ;
      private int edtavUpdate_Visible ;
      private int edtavApagar_Visible ;
      private int edtavAcessar_Visible ;
      private int AV55GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavApagar_Enabled ;
      private long GRID_nFirstRecordOnPage ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Dvelop_confirmpanel_apagar_Result ;
      private String Upload_Error ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_29_idx="0001" ;
      private String AV54Pgmname ;
      private String A839LoteArquivoAnexo_NomeArq ;
      private String A840LoteArquivoAnexo_TipoArq ;
      private String GXKey ;
      private String edtavLote_nfe_Internalname ;
      private String edtavLote_nfearq_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gx_msg ;
      private String Upload_Width ;
      private String Upload_Buttontext ;
      private String Upload_Buttonimage ;
      private String Upload_Simultaneousuploadlimit ;
      private String Dvelop_confirmpanel_apagar_Title ;
      private String Dvelop_confirmpanel_apagar_Confirmationtext ;
      private String Dvelop_confirmpanel_apagar_Yesbuttoncaption ;
      private String Dvelop_confirmpanel_apagar_Nobuttoncaption ;
      private String Dvelop_confirmpanel_apagar_Cancelbuttoncaption ;
      private String Dvelop_confirmpanel_apagar_Yesbuttonposition ;
      private String Dvelop_confirmpanel_apagar_Confirmtype ;
      private String GX_FocusControl ;
      private String TempTags ;
      private String dynavLotearquivoanexo_lotecod_selected_Internalname ;
      private String dynavLotearquivoanexo_lotecod_selected_Jsonclick ;
      private String edtavLotearquivoanexo_data_selected_Internalname ;
      private String edtavLotearquivoanexo_data_selected_Jsonclick ;
      private String edtavLotearquivoanexo_nomearq_selected_Internalname ;
      private String AV25LoteArquivoAnexo_NomeArq_Selected ;
      private String edtavLotearquivoanexo_nomearq_selected_Jsonclick ;
      private String edtavOrderedby_Internalname ;
      private String edtavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavApagar_Internalname ;
      private String edtavAcessar_Internalname ;
      private String edtLoteArquivoAnexo_LoteCod_Internalname ;
      private String A646TipoDocumento_Nome ;
      private String edtTipoDocumento_Nome_Internalname ;
      private String edtLoteArquivoAnexo_Data_Internalname ;
      private String scmdbuf ;
      private String gxwrpcisep ;
      private String A838LoteArquivoAnexo_Arquivo_Filename ;
      private String A838LoteArquivoAnexo_Arquivo_Filetype ;
      private String edtLoteArquivoAnexo_NomeArq_Title ;
      private String edtLoteArquivoAnexo_NomeArq_Internalname ;
      private String edtTipoDocumento_Nome_Title ;
      private String edtLoteArquivoAnexo_Data_Title ;
      private String A679Lote_NFeNomeArq ;
      private String A678Lote_NFeArq_Filename ;
      private String A680Lote_NFeTipoArq ;
      private String A678Lote_NFeArq_Filetype ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavApagar_Tooltiptext ;
      private String edtavAcessar_Tooltiptext ;
      private String edtavAcessar_Link ;
      private String edtavAcessar_Linktarget ;
      private String sStyleString ;
      private String tblTabledvelop_confirmpanel_apagar_Internalname ;
      private String tblUnnamedtable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblUnnamedtable3_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblUsertbl_Internalname ;
      private String tblUnnamedtable2_Internalname ;
      private String lblTextblocklote_nfe_Internalname ;
      private String lblTextblocklote_nfe_Jsonclick ;
      private String tblTablemergedlote_nfe_Internalname ;
      private String edtavLote_nfe_Jsonclick ;
      private String edtavLote_nfearq_Filetype ;
      private String edtavLote_nfearq_Contenttype ;
      private String edtavLote_nfearq_Parameters ;
      private String edtavLote_nfearq_Jsonclick ;
      private String tblTablesearch_Internalname ;
      private String sCtrlAV19LoteArquivoAnexo_LoteCod ;
      private String sGXsfl_29_fel_idx="0001" ;
      private String edtavApagar_Jsonclick ;
      private String ROClassString ;
      private String edtLoteArquivoAnexo_LoteCod_Jsonclick ;
      private String edtLoteArquivoAnexo_NomeArq_Jsonclick ;
      private String edtTipoDocumento_Nome_Jsonclick ;
      private String edtLoteArquivoAnexo_Data_Jsonclick ;
      private String Upload_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private String Dvelop_confirmpanel_apagar_Internalname ;
      private DateTime A836LoteArquivoAnexo_Data ;
      private DateTime AV23LoteArquivoAnexo_Data_Selected ;
      private DateTime AV30ServerNow ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool n673Lote_NFe ;
      private bool n678Lote_NFeArq ;
      private bool n838LoteArquivoAnexo_Arquivo ;
      private bool n839LoteArquivoAnexo_NomeArq ;
      private bool n840LoteArquivoAnexo_TipoArq ;
      private bool toggleJsOutput ;
      private bool Upload_Multi ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n645TipoDocumento_Codigo ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool n679Lote_NFeNomeArq ;
      private bool n680Lote_NFeTipoArq ;
      private bool AV17Update_IsBlob ;
      private bool AV21Apagar_IsBlob ;
      private bool AV24Acessar_IsBlob ;
      private String AV51Update_GXI ;
      private String AV52Apagar_GXI ;
      private String AV53Acessar_GXI ;
      private String AV17Update ;
      private String AV21Apagar ;
      private String AV24Acessar ;
      private String A678Lote_NFeArq ;
      private String A838LoteArquivoAnexo_Arquivo ;
      private String AV47Lote_NFeArq ;
      private IGxSession AV16Session ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxFile gxblobfileaux ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavLotearquivoanexo_lotecod_selected ;
      private IDataStoreProvider pr_default ;
      private int[] H00EU2_A596Lote_Codigo ;
      private String[] H00EU2_A563Lote_Nome ;
      private int[] H00EU3_A596Lote_Codigo ;
      private String[] H00EU3_A563Lote_Nome ;
      private int[] H00EU4_A645TipoDocumento_Codigo ;
      private bool[] H00EU4_n645TipoDocumento_Codigo ;
      private String[] H00EU4_A839LoteArquivoAnexo_NomeArq ;
      private bool[] H00EU4_n839LoteArquivoAnexo_NomeArq ;
      private String[] H00EU4_A840LoteArquivoAnexo_TipoArq ;
      private bool[] H00EU4_n840LoteArquivoAnexo_TipoArq ;
      private DateTime[] H00EU4_A836LoteArquivoAnexo_Data ;
      private String[] H00EU4_A646TipoDocumento_Nome ;
      private int[] H00EU4_A841LoteArquivoAnexo_LoteCod ;
      private String[] H00EU4_A838LoteArquivoAnexo_Arquivo ;
      private bool[] H00EU4_n838LoteArquivoAnexo_Arquivo ;
      private long[] H00EU5_AGRID_nRecordCount ;
      private int[] H00EU6_A596Lote_Codigo ;
      private String[] H00EU6_A679Lote_NFeNomeArq ;
      private bool[] H00EU6_n679Lote_NFeNomeArq ;
      private String[] H00EU6_A680Lote_NFeTipoArq ;
      private bool[] H00EU6_n680Lote_NFeTipoArq ;
      private int[] H00EU6_A673Lote_NFe ;
      private bool[] H00EU6_n673Lote_NFe ;
      private String[] H00EU6_A678Lote_NFeArq ;
      private bool[] H00EU6_n678Lote_NFeArq ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( SdtUploadifyOutput ))]
      private IGxCollection AV26UploadedFiles ;
      [ObjectCollection(ItemType=typeof( SdtUploadifyOutput ))]
      private IGxCollection AV28QueuedFiles ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private SdtUploadifyOutput Upload_File ;
      private SdtUploadifyOutput AV27File ;
      private SdtLoteArquivoAnexo AV29LoteArquivoAnexo ;
   }

   public class lotearquivosanexoswc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00EU4( IGxContext context ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A841LoteArquivoAnexo_LoteCod ,
                                             int AV19LoteArquivoAnexo_LoteCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [6] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[TipoDocumento_Codigo], T1.[LoteArquivoAnexo_NomeArq], T1.[LoteArquivoAnexo_TipoArq], T1.[LoteArquivoAnexo_Data], T2.[TipoDocumento_Nome], T1.[LoteArquivoAnexo_LoteCod], T1.[LoteArquivoAnexo_Arquivo]";
         sFromString = " FROM ([LoteArquivoAnexo] T1 WITH (NOLOCK) LEFT JOIN [TipoDocumento] T2 WITH (NOLOCK) ON T2.[TipoDocumento_Codigo] = T1.[TipoDocumento_Codigo])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[LoteArquivoAnexo_LoteCod] = @AV19LoteArquivoAnexo_LoteCod)";
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[LoteArquivoAnexo_LoteCod], T1.[LoteArquivoAnexo_Data]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[LoteArquivoAnexo_LoteCod] DESC, T1.[LoteArquivoAnexo_Data] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[LoteArquivoAnexo_LoteCod], T1.[LoteArquivoAnexo_NomeArq]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[LoteArquivoAnexo_LoteCod] DESC, T1.[LoteArquivoAnexo_NomeArq] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[LoteArquivoAnexo_LoteCod], T2.[TipoDocumento_Nome]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[LoteArquivoAnexo_LoteCod] DESC, T2.[TipoDocumento_Nome] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[LoteArquivoAnexo_LoteCod], T1.[LoteArquivoAnexo_Data]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_H00EU5( IGxContext context ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A841LoteArquivoAnexo_LoteCod ,
                                             int AV19LoteArquivoAnexo_LoteCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [1] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([LoteArquivoAnexo] T1 WITH (NOLOCK) LEFT JOIN [TipoDocumento] T2 WITH (NOLOCK) ON T2.[TipoDocumento_Codigo] = T1.[TipoDocumento_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[LoteArquivoAnexo_LoteCod] = @AV19LoteArquivoAnexo_LoteCod)";
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 2 :
                     return conditional_H00EU4(context, (short)dynConstraints[0] , (bool)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] );
               case 3 :
                     return conditional_H00EU5(context, (short)dynConstraints[0] , (bool)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00EU2 ;
          prmH00EU2 = new Object[] {
          } ;
          Object[] prmH00EU3 ;
          prmH00EU3 = new Object[] {
          } ;
          Object[] prmH00EU6 ;
          prmH00EU6 = new Object[] {
          new Object[] {"@AV19LoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00EU4 ;
          prmH00EU4 = new Object[] {
          new Object[] {"@AV19LoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00EU5 ;
          prmH00EU5 = new Object[] {
          new Object[] {"@AV19LoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00EU2", "SELECT [Lote_Codigo], [Lote_Nome] FROM [Lote] WITH (NOLOCK) ORDER BY [Lote_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EU2,0,0,true,false )
             ,new CursorDef("H00EU3", "SELECT [Lote_Codigo], [Lote_Nome] FROM [Lote] WITH (NOLOCK) ORDER BY [Lote_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EU3,0,0,true,false )
             ,new CursorDef("H00EU4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EU4,11,0,true,false )
             ,new CursorDef("H00EU5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EU5,1,0,true,false )
             ,new CursorDef("H00EU6", "SELECT TOP 1 [Lote_Codigo], [Lote_NFeNomeArq], [Lote_NFeTipoArq], [Lote_NFe], [Lote_NFeArq] FROM [Lote] WITH (NOLOCK) WHERE [Lote_Codigo] = @AV19LoteArquivoAnexo_LoteCod ORDER BY [Lote_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EU6,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[6])[0] = rslt.getGXDateTime(4) ;
                ((String[]) buf[7])[0] = rslt.getString(5, 50) ;
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((String[]) buf[9])[0] = rslt.getBLOBFile(7, rslt.getString(3, 10), rslt.getString(2, 50)) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                return;
             case 3 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getBLOBFile(5, rslt.getString(3, 10), rslt.getString(2, 50)) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[1]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
