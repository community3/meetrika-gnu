/*
               File: GetTecnologiaAmbienteTecnologicoWCFilterData
        Description: Get Tecnologia Ambiente Tecnologico WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:29.12
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gettecnologiaambientetecnologicowcfilterdata : GXProcedure
   {
      public gettecnologiaambientetecnologicowcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public gettecnologiaambientetecnologicowcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV15DDOName = aP0_DDOName;
         this.AV13SearchTxt = aP1_SearchTxt;
         this.AV14SearchTxtTo = aP2_SearchTxtTo;
         this.AV19OptionsJson = "" ;
         this.AV22OptionsDescJson = "" ;
         this.AV24OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV19OptionsJson;
         aP4_OptionsDescJson=this.AV22OptionsDescJson;
         aP5_OptionIndexesJson=this.AV24OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV15DDOName = aP0_DDOName;
         this.AV13SearchTxt = aP1_SearchTxt;
         this.AV14SearchTxtTo = aP2_SearchTxtTo;
         this.AV19OptionsJson = "" ;
         this.AV22OptionsDescJson = "" ;
         this.AV24OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV19OptionsJson;
         aP4_OptionsDescJson=this.AV22OptionsDescJson;
         aP5_OptionIndexesJson=this.AV24OptionIndexesJson;
         return AV24OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         gettecnologiaambientetecnologicowcfilterdata objgettecnologiaambientetecnologicowcfilterdata;
         objgettecnologiaambientetecnologicowcfilterdata = new gettecnologiaambientetecnologicowcfilterdata();
         objgettecnologiaambientetecnologicowcfilterdata.AV15DDOName = aP0_DDOName;
         objgettecnologiaambientetecnologicowcfilterdata.AV13SearchTxt = aP1_SearchTxt;
         objgettecnologiaambientetecnologicowcfilterdata.AV14SearchTxtTo = aP2_SearchTxtTo;
         objgettecnologiaambientetecnologicowcfilterdata.AV19OptionsJson = "" ;
         objgettecnologiaambientetecnologicowcfilterdata.AV22OptionsDescJson = "" ;
         objgettecnologiaambientetecnologicowcfilterdata.AV24OptionIndexesJson = "" ;
         objgettecnologiaambientetecnologicowcfilterdata.context.SetSubmitInitialConfig(context);
         objgettecnologiaambientetecnologicowcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgettecnologiaambientetecnologicowcfilterdata);
         aP3_OptionsJson=this.AV19OptionsJson;
         aP4_OptionsDescJson=this.AV22OptionsDescJson;
         aP5_OptionIndexesJson=this.AV24OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((gettecnologiaambientetecnologicowcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV18Options = (IGxCollection)(new GxSimpleCollection());
         AV21OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV23OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV15DDOName), "DDO_AMBIENTETECNOLOGICO_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADAMBIENTETECNOLOGICO_DESCRICAOOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV19OptionsJson = AV18Options.ToJSonString(false);
         AV22OptionsDescJson = AV21OptionsDesc.ToJSonString(false);
         AV24OptionIndexesJson = AV23OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV26Session.Get("TecnologiaAmbienteTecnologicoWCGridState"), "") == 0 )
         {
            AV28GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "TecnologiaAmbienteTecnologicoWCGridState"), "");
         }
         else
         {
            AV28GridState.FromXml(AV26Session.Get("TecnologiaAmbienteTecnologicoWCGridState"), "");
         }
         AV40GXV1 = 1;
         while ( AV40GXV1 <= AV28GridState.gxTpr_Filtervalues.Count )
         {
            AV29GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV28GridState.gxTpr_Filtervalues.Item(AV40GXV1));
            if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "AREATRABALHO_CODIGO") == 0 )
            {
               AV31AreaTrabalho_Codigo = (int)(NumberUtil.Val( AV29GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "TFAMBIENTETECNOLOGICO_DESCRICAO") == 0 )
            {
               AV10TFAmbienteTecnologico_Descricao = AV29GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "TFAMBIENTETECNOLOGICO_DESCRICAO_SEL") == 0 )
            {
               AV11TFAmbienteTecnologico_Descricao_Sel = AV29GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "TFAMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_SEL") == 0 )
            {
               AV12TFAmbienteTecnologicoTecnologias_Ativo_Sel = (short)(NumberUtil.Val( AV29GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "PARM_&TECNOLOGIA_CODIGO") == 0 )
            {
               AV37Tecnologia_Codigo = (int)(NumberUtil.Val( AV29GridStateFilterValue.gxTpr_Value, "."));
            }
            AV40GXV1 = (int)(AV40GXV1+1);
         }
         if ( AV28GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV30GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV28GridState.gxTpr_Dynamicfilters.Item(1));
            AV32DynamicFiltersSelector1 = AV30GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 )
            {
               AV33AmbienteTecnologico_Descricao1 = AV30GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV28GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV34DynamicFiltersEnabled2 = true;
               AV30GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV28GridState.gxTpr_Dynamicfilters.Item(2));
               AV35DynamicFiltersSelector2 = AV30GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV35DynamicFiltersSelector2, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 )
               {
                  AV36AmbienteTecnologico_Descricao2 = AV30GridStateDynamicFilter.gxTpr_Value;
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADAMBIENTETECNOLOGICO_DESCRICAOOPTIONS' Routine */
         AV10TFAmbienteTecnologico_Descricao = AV13SearchTxt;
         AV11TFAmbienteTecnologico_Descricao_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV31AreaTrabalho_Codigo ,
                                              AV32DynamicFiltersSelector1 ,
                                              AV33AmbienteTecnologico_Descricao1 ,
                                              AV34DynamicFiltersEnabled2 ,
                                              AV35DynamicFiltersSelector2 ,
                                              AV36AmbienteTecnologico_Descricao2 ,
                                              AV11TFAmbienteTecnologico_Descricao_Sel ,
                                              AV10TFAmbienteTecnologico_Descricao ,
                                              AV12TFAmbienteTecnologicoTecnologias_Ativo_Sel ,
                                              A5AreaTrabalho_Codigo ,
                                              A352AmbienteTecnologico_Descricao ,
                                              A354AmbienteTecnologicoTecnologias_Ativo ,
                                              AV37Tecnologia_Codigo ,
                                              A131Tecnologia_Codigo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV33AmbienteTecnologico_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV33AmbienteTecnologico_Descricao1), "%", "");
         lV36AmbienteTecnologico_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV36AmbienteTecnologico_Descricao2), "%", "");
         lV10TFAmbienteTecnologico_Descricao = StringUtil.Concat( StringUtil.RTrim( AV10TFAmbienteTecnologico_Descricao), "%", "");
         /* Using cursor P00HC2 */
         pr_default.execute(0, new Object[] {AV37Tecnologia_Codigo, lV33AmbienteTecnologico_Descricao1, lV36AmbienteTecnologico_Descricao2, lV10TFAmbienteTecnologico_Descricao, AV11TFAmbienteTecnologico_Descricao_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKHC2 = false;
            A131Tecnologia_Codigo = P00HC2_A131Tecnologia_Codigo[0];
            A351AmbienteTecnologico_Codigo = P00HC2_A351AmbienteTecnologico_Codigo[0];
            A354AmbienteTecnologicoTecnologias_Ativo = P00HC2_A354AmbienteTecnologicoTecnologias_Ativo[0];
            A352AmbienteTecnologico_Descricao = P00HC2_A352AmbienteTecnologico_Descricao[0];
            A352AmbienteTecnologico_Descricao = P00HC2_A352AmbienteTecnologico_Descricao[0];
            AV25count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00HC2_A131Tecnologia_Codigo[0] == A131Tecnologia_Codigo ) && ( P00HC2_A351AmbienteTecnologico_Codigo[0] == A351AmbienteTecnologico_Codigo ) )
            {
               BRKHC2 = false;
               AV25count = (long)(AV25count+1);
               BRKHC2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A352AmbienteTecnologico_Descricao)) )
            {
               AV17Option = A352AmbienteTecnologico_Descricao;
               AV16InsertIndex = 1;
               while ( ( AV16InsertIndex <= AV18Options.Count ) && ( StringUtil.StrCmp(((String)AV18Options.Item(AV16InsertIndex)), AV17Option) < 0 ) )
               {
                  AV16InsertIndex = (int)(AV16InsertIndex+1);
               }
               AV18Options.Add(AV17Option, AV16InsertIndex);
               AV23OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV25count), "Z,ZZZ,ZZZ,ZZ9")), AV16InsertIndex);
            }
            if ( AV18Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKHC2 )
            {
               BRKHC2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV18Options = new GxSimpleCollection();
         AV21OptionsDesc = new GxSimpleCollection();
         AV23OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV26Session = context.GetSession();
         AV28GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV29GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFAmbienteTecnologico_Descricao = "";
         AV11TFAmbienteTecnologico_Descricao_Sel = "";
         AV30GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV32DynamicFiltersSelector1 = "";
         AV33AmbienteTecnologico_Descricao1 = "";
         AV35DynamicFiltersSelector2 = "";
         AV36AmbienteTecnologico_Descricao2 = "";
         scmdbuf = "";
         lV33AmbienteTecnologico_Descricao1 = "";
         lV36AmbienteTecnologico_Descricao2 = "";
         lV10TFAmbienteTecnologico_Descricao = "";
         A352AmbienteTecnologico_Descricao = "";
         P00HC2_A131Tecnologia_Codigo = new int[1] ;
         P00HC2_A351AmbienteTecnologico_Codigo = new int[1] ;
         P00HC2_A354AmbienteTecnologicoTecnologias_Ativo = new bool[] {false} ;
         P00HC2_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         AV17Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.gettecnologiaambientetecnologicowcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00HC2_A131Tecnologia_Codigo, P00HC2_A351AmbienteTecnologico_Codigo, P00HC2_A354AmbienteTecnologicoTecnologias_Ativo, P00HC2_A352AmbienteTecnologico_Descricao
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV12TFAmbienteTecnologicoTecnologias_Ativo_Sel ;
      private int AV40GXV1 ;
      private int AV31AreaTrabalho_Codigo ;
      private int AV37Tecnologia_Codigo ;
      private int A5AreaTrabalho_Codigo ;
      private int A131Tecnologia_Codigo ;
      private int A351AmbienteTecnologico_Codigo ;
      private int AV16InsertIndex ;
      private long AV25count ;
      private String scmdbuf ;
      private bool returnInSub ;
      private bool AV34DynamicFiltersEnabled2 ;
      private bool A354AmbienteTecnologicoTecnologias_Ativo ;
      private bool BRKHC2 ;
      private String AV24OptionIndexesJson ;
      private String AV19OptionsJson ;
      private String AV22OptionsDescJson ;
      private String AV15DDOName ;
      private String AV13SearchTxt ;
      private String AV14SearchTxtTo ;
      private String AV10TFAmbienteTecnologico_Descricao ;
      private String AV11TFAmbienteTecnologico_Descricao_Sel ;
      private String AV32DynamicFiltersSelector1 ;
      private String AV33AmbienteTecnologico_Descricao1 ;
      private String AV35DynamicFiltersSelector2 ;
      private String AV36AmbienteTecnologico_Descricao2 ;
      private String lV33AmbienteTecnologico_Descricao1 ;
      private String lV36AmbienteTecnologico_Descricao2 ;
      private String lV10TFAmbienteTecnologico_Descricao ;
      private String A352AmbienteTecnologico_Descricao ;
      private String AV17Option ;
      private IGxSession AV26Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00HC2_A131Tecnologia_Codigo ;
      private int[] P00HC2_A351AmbienteTecnologico_Codigo ;
      private bool[] P00HC2_A354AmbienteTecnologicoTecnologias_Ativo ;
      private String[] P00HC2_A352AmbienteTecnologico_Descricao ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV18Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV28GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV29GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV30GridStateDynamicFilter ;
   }

   public class gettecnologiaambientetecnologicowcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00HC2( IGxContext context ,
                                             int AV31AreaTrabalho_Codigo ,
                                             String AV32DynamicFiltersSelector1 ,
                                             String AV33AmbienteTecnologico_Descricao1 ,
                                             bool AV34DynamicFiltersEnabled2 ,
                                             String AV35DynamicFiltersSelector2 ,
                                             String AV36AmbienteTecnologico_Descricao2 ,
                                             String AV11TFAmbienteTecnologico_Descricao_Sel ,
                                             String AV10TFAmbienteTecnologico_Descricao ,
                                             short AV12TFAmbienteTecnologicoTecnologias_Ativo_Sel ,
                                             int A5AreaTrabalho_Codigo ,
                                             String A352AmbienteTecnologico_Descricao ,
                                             bool A354AmbienteTecnologicoTecnologias_Ativo ,
                                             int AV37Tecnologia_Codigo ,
                                             int A131Tecnologia_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [5] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Tecnologia_Codigo], T1.[AmbienteTecnologico_Codigo], T1.[AmbienteTecnologicoTecnologias_Ativo], T2.[AmbienteTecnologico_Descricao] FROM ([AmbienteTecnologicoTecnologias] T1 WITH (NOLOCK) INNER JOIN [AmbienteTecnologico] T2 WITH (NOLOCK) ON T2.[AmbienteTecnologico_Codigo] = T1.[AmbienteTecnologico_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[Tecnologia_Codigo] = @AV37Tecnologia_Codigo)";
         if ( ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33AmbienteTecnologico_Descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[AmbienteTecnologico_Descricao] like @lV33AmbienteTecnologico_Descricao1 + '%')";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV34DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV35DynamicFiltersSelector2, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36AmbienteTecnologico_Descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[AmbienteTecnologico_Descricao] like @lV36AmbienteTecnologico_Descricao2 + '%')";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFAmbienteTecnologico_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFAmbienteTecnologico_Descricao)) ) )
         {
            sWhereString = sWhereString + " and (T2.[AmbienteTecnologico_Descricao] like @lV10TFAmbienteTecnologico_Descricao)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFAmbienteTecnologico_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[AmbienteTecnologico_Descricao] = @AV11TFAmbienteTecnologico_Descricao_Sel)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV12TFAmbienteTecnologicoTecnologias_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[AmbienteTecnologicoTecnologias_Ativo] = 1)";
         }
         if ( AV12TFAmbienteTecnologicoTecnologias_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[AmbienteTecnologicoTecnologias_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Tecnologia_Codigo], T1.[AmbienteTecnologico_Codigo]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00HC2(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (int)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00HC2 ;
          prmP00HC2 = new Object[] {
          new Object[] {"@AV37Tecnologia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV33AmbienteTecnologico_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV36AmbienteTecnologico_Descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV10TFAmbienteTecnologico_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV11TFAmbienteTecnologico_Descricao_Sel",SqlDbType.VarChar,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00HC2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00HC2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.gettecnologiaambientetecnologicowcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class gettecnologiaambientetecnologicowcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("gettecnologiaambientetecnologicowcfilterdata") )
          {
             return  ;
          }
          gettecnologiaambientetecnologicowcfilterdata worker = new gettecnologiaambientetecnologicowcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
