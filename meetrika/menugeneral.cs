/*
               File: MenuGeneral
        Description: Menu General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:19:34.21
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class menugeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public menugeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public menugeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Menu_Codigo )
      {
         this.A277Menu_Codigo = aP0_Menu_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbMenu_Tipo = new GXCombobox();
         chkMenu_Ativo = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A277Menu_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A277Menu_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A277Menu_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A277Menu_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA612( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV14Pgmname = "MenuGeneral";
               context.Gx_err = 0;
               WS612( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Menu General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117193428");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("menugeneral.aspx") + "?" + UrlEncode("" +A277Menu_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA277Menu_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA277Menu_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"MENU_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A277Menu_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_MENU_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A278Menu_Nome, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_MENU_DESCRICAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A279Menu_Descricao, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_MENU_TIPO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A280Menu_Tipo), "Z9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_MENU_LINK", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A281Menu_Link, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_MENU_ORDEM", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A283Menu_Ordem), "ZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_MENU_ATIVO", GetSecureSignedToken( sPrefix, A284Menu_Ativo));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm612( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("menugeneral.js", "?20203117193431");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "MenuGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Menu General" ;
      }

      protected void WB610( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "menugeneral.aspx");
            }
            wb_table1_2_612( true) ;
         }
         else
         {
            wb_table1_2_612( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_612e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void START612( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Menu General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP610( ) ;
            }
         }
      }

      protected void WS612( )
      {
         START612( ) ;
         EVT612( ) ;
      }

      protected void EVT612( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP610( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP610( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11612 */
                                    E11612 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP610( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12612 */
                                    E12612 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP610( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13612 */
                                    E13612 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP610( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14612 */
                                    E14612 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP610( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP610( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE612( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm612( ) ;
            }
         }
      }

      protected void PA612( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbMenu_Tipo.Name = "MENU_TIPO";
            cmbMenu_Tipo.WebTags = "";
            cmbMenu_Tipo.addItem("1", "Superior", 0);
            cmbMenu_Tipo.addItem("2", "Acesso R�pido", 0);
            if ( cmbMenu_Tipo.ItemCount > 0 )
            {
               A280Menu_Tipo = (short)(NumberUtil.Val( cmbMenu_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A280Menu_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_MENU_TIPO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A280Menu_Tipo), "Z9")));
            }
            chkMenu_Ativo.Name = "MENU_ATIVO";
            chkMenu_Ativo.WebTags = "";
            chkMenu_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkMenu_Ativo_Internalname, "TitleCaption", chkMenu_Ativo.Caption);
            chkMenu_Ativo.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbMenu_Tipo.ItemCount > 0 )
         {
            A280Menu_Tipo = (short)(NumberUtil.Val( cmbMenu_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A280Menu_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_MENU_TIPO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A280Menu_Tipo), "Z9")));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF612( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV14Pgmname = "MenuGeneral";
         context.Gx_err = 0;
      }

      protected void RF612( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00612 */
            pr_default.execute(0, new Object[] {A277Menu_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A285Menu_PaiCod = H00612_A285Menu_PaiCod[0];
               n285Menu_PaiCod = H00612_n285Menu_PaiCod[0];
               A284Menu_Ativo = H00612_A284Menu_Ativo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A284Menu_Ativo", A284Menu_Ativo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_MENU_ATIVO", GetSecureSignedToken( sPrefix, A284Menu_Ativo));
               A286Menu_PaiNom = H00612_A286Menu_PaiNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A286Menu_PaiNom", A286Menu_PaiNom);
               n286Menu_PaiNom = H00612_n286Menu_PaiNom[0];
               A283Menu_Ordem = H00612_A283Menu_Ordem[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A283Menu_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A283Menu_Ordem), 3, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_MENU_ORDEM", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A283Menu_Ordem), "ZZ9")));
               A40000Menu_Imagem_GXI = H00612_A40000Menu_Imagem_GXI[0];
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgMenu_Imagem_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A282Menu_Imagem)) ? A40000Menu_Imagem_GXI : context.convertURL( context.PathToRelativeUrl( A282Menu_Imagem))));
               n40000Menu_Imagem_GXI = H00612_n40000Menu_Imagem_GXI[0];
               A281Menu_Link = H00612_A281Menu_Link[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A281Menu_Link", A281Menu_Link);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_MENU_LINK", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A281Menu_Link, ""))));
               n281Menu_Link = H00612_n281Menu_Link[0];
               A280Menu_Tipo = H00612_A280Menu_Tipo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A280Menu_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_MENU_TIPO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A280Menu_Tipo), "Z9")));
               A279Menu_Descricao = H00612_A279Menu_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A279Menu_Descricao", A279Menu_Descricao);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_MENU_DESCRICAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A279Menu_Descricao, "@!"))));
               n279Menu_Descricao = H00612_n279Menu_Descricao[0];
               A278Menu_Nome = H00612_A278Menu_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A278Menu_Nome", A278Menu_Nome);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_MENU_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A278Menu_Nome, ""))));
               A282Menu_Imagem = H00612_A282Menu_Imagem[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A282Menu_Imagem", A282Menu_Imagem);
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgMenu_Imagem_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A282Menu_Imagem)) ? A40000Menu_Imagem_GXI : context.convertURL( context.PathToRelativeUrl( A282Menu_Imagem))));
               n282Menu_Imagem = H00612_n282Menu_Imagem[0];
               A286Menu_PaiNom = H00612_A286Menu_PaiNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A286Menu_PaiNom", A286Menu_PaiNom);
               n286Menu_PaiNom = H00612_n286Menu_PaiNom[0];
               /* Execute user event: E12612 */
               E12612 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WB610( ) ;
         }
      }

      protected void STRUP610( )
      {
         /* Before Start, stand alone formulas. */
         AV14Pgmname = "MenuGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11612 */
         E11612 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A278Menu_Nome = cgiGet( edtMenu_Nome_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A278Menu_Nome", A278Menu_Nome);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_MENU_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A278Menu_Nome, ""))));
            A279Menu_Descricao = StringUtil.Upper( cgiGet( edtMenu_Descricao_Internalname));
            n279Menu_Descricao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A279Menu_Descricao", A279Menu_Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_MENU_DESCRICAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A279Menu_Descricao, "@!"))));
            cmbMenu_Tipo.CurrentValue = cgiGet( cmbMenu_Tipo_Internalname);
            A280Menu_Tipo = (short)(NumberUtil.Val( cgiGet( cmbMenu_Tipo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A280Menu_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_MENU_TIPO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A280Menu_Tipo), "Z9")));
            A281Menu_Link = cgiGet( edtMenu_Link_Internalname);
            n281Menu_Link = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A281Menu_Link", A281Menu_Link);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_MENU_LINK", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A281Menu_Link, ""))));
            A282Menu_Imagem = cgiGet( imgMenu_Imagem_Internalname);
            n282Menu_Imagem = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A282Menu_Imagem", A282Menu_Imagem);
            A283Menu_Ordem = (short)(context.localUtil.CToN( cgiGet( edtMenu_Ordem_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A283Menu_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A283Menu_Ordem), 3, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_MENU_ORDEM", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A283Menu_Ordem), "ZZ9")));
            A286Menu_PaiNom = cgiGet( edtMenu_PaiNom_Internalname);
            n286Menu_PaiNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A286Menu_PaiNom", A286Menu_PaiNom);
            A284Menu_Ativo = StringUtil.StrToBool( cgiGet( chkMenu_Ativo_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A284Menu_Ativo", A284Menu_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_MENU_ATIVO", GetSecureSignedToken( sPrefix, A284Menu_Ativo));
            /* Read saved values. */
            wcpOA277Menu_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA277Menu_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11612 */
         E11612 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11612( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E12612( )
      {
         /* Load Routine */
         if ( ! ( ( AV6WWPContext.gxTpr_Update ) ) )
         {
            bttBtnupdate_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Enabled), 5, 0)));
         }
         if ( ! ( ( AV6WWPContext.gxTpr_Delete ) ) )
         {
            bttBtndelete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Enabled), 5, 0)));
         }
      }

      protected void E13612( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("menu.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A277Menu_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E14612( )
      {
         /* 'DoDelete' Routine */
         new prc_dltmenuperfil(context ).execute( ref  A277Menu_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A277Menu_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A277Menu_Codigo), 6, 0)));
         context.wjLoc = formatLink("wwmenu.aspx") ;
         context.wjLocDisableFrm = 1;
         if ( false )
         {
            context.wjLoc = formatLink("menu.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A277Menu_Codigo);
            context.wjLocDisableFrm = 1;
         }
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV14Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Menu";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "Menu_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Menu_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_612( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_612( true) ;
         }
         else
         {
            wb_table2_8_612( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_612e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_51_612( true) ;
         }
         else
         {
            wb_table3_51_612( false) ;
         }
         return  ;
      }

      protected void wb_table3_51_612e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_612e( true) ;
         }
         else
         {
            wb_table1_2_612e( false) ;
         }
      }

      protected void wb_table3_51_612( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, 1, bttBtnupdate_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_MenuGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, 1, bttBtndelete_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_MenuGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_51_612e( true) ;
         }
         else
         {
            wb_table3_51_612e( false) ;
         }
      }

      protected void wb_table2_8_612( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmenu_nome_Internalname, "Nome", "", "", lblTextblockmenu_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_MenuGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtMenu_Nome_Internalname, StringUtil.RTrim( A278Menu_Nome), StringUtil.RTrim( context.localUtil.Format( A278Menu_Nome, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtMenu_Nome_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "NomeMenu30", "left", true, "HLP_MenuGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmenu_descricao_Internalname, "Descri��o", "", "", lblTextblockmenu_descricao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_MenuGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtMenu_Descricao_Internalname, A279Menu_Descricao, StringUtil.RTrim( context.localUtil.Format( A279Menu_Descricao, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtMenu_Descricao_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_MenuGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmenu_tipo_Internalname, "Tipo", "", "", lblTextblockmenu_tipo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_MenuGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbMenu_Tipo, cmbMenu_Tipo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0)), 1, cmbMenu_Tipo_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_MenuGeneral.htm");
            cmbMenu_Tipo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbMenu_Tipo_Internalname, "Values", (String)(cmbMenu_Tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmenu_link_Internalname, "Link do Menu", "", "", lblTextblockmenu_link_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_MenuGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtMenu_Link_Internalname, A281Menu_Link, StringUtil.RTrim( context.localUtil.Format( A281Menu_Link, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtMenu_Link_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "LinkMenu", "left", true, "HLP_MenuGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmenu_imagem_Internalname, "Imagem", "", "", lblTextblockmenu_imagem_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_MenuGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Static Bitmap Variable */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            A282Menu_Imagem_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( A282Menu_Imagem))&&String.IsNullOrEmpty(StringUtil.RTrim( A40000Menu_Imagem_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( A282Menu_Imagem)));
            GxWebStd.gx_bitmap( context, imgMenu_Imagem_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( A282Menu_Imagem)) ? A40000Menu_Imagem_GXI : context.PathToRelativeUrl( A282Menu_Imagem)), "", "", "", context.GetTheme( ), 1, 0, "", "", 1, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, A282Menu_Imagem_IsBlob, true, "HLP_MenuGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmenu_ordem_Internalname, "Ordena��o", "", "", lblTextblockmenu_ordem_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_MenuGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtMenu_Ordem_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A283Menu_Ordem), 3, 0, ",", "")), context.localUtil.Format( (decimal)(A283Menu_Ordem), "ZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtMenu_Ordem_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Ordem", "right", false, "HLP_MenuGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmenu_painom_Internalname, "Nome", "", "", lblTextblockmenu_painom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_MenuGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtMenu_PaiNom_Internalname, StringUtil.RTrim( A286Menu_PaiNom), StringUtil.RTrim( context.localUtil.Format( A286Menu_PaiNom, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtMenu_PaiNom_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "NomeMenu30", "left", true, "HLP_MenuGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmenu_ativo_Internalname, "Ativo", "", "", lblTextblockmenu_ativo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_MenuGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Check box */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkMenu_Ativo_Internalname, StringUtil.BoolToStr( A284Menu_Ativo), "", "", 1, 0, "true", "", StyleString, ClassString, "", "");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_612e( true) ;
         }
         else
         {
            wb_table2_8_612e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A277Menu_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A277Menu_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A277Menu_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA612( ) ;
         WS612( ) ;
         WE612( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA277Menu_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA612( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "menugeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA612( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A277Menu_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A277Menu_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A277Menu_Codigo), 6, 0)));
         }
         wcpOA277Menu_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA277Menu_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A277Menu_Codigo != wcpOA277Menu_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA277Menu_Codigo = A277Menu_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA277Menu_Codigo = cgiGet( sPrefix+"A277Menu_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA277Menu_Codigo) > 0 )
         {
            A277Menu_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA277Menu_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A277Menu_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A277Menu_Codigo), 6, 0)));
         }
         else
         {
            A277Menu_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A277Menu_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA612( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS612( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS612( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A277Menu_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A277Menu_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA277Menu_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A277Menu_Codigo_CTRL", StringUtil.RTrim( sCtrlA277Menu_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE612( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117193484");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("menugeneral.js", "?20203117193484");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockmenu_nome_Internalname = sPrefix+"TEXTBLOCKMENU_NOME";
         edtMenu_Nome_Internalname = sPrefix+"MENU_NOME";
         lblTextblockmenu_descricao_Internalname = sPrefix+"TEXTBLOCKMENU_DESCRICAO";
         edtMenu_Descricao_Internalname = sPrefix+"MENU_DESCRICAO";
         lblTextblockmenu_tipo_Internalname = sPrefix+"TEXTBLOCKMENU_TIPO";
         cmbMenu_Tipo_Internalname = sPrefix+"MENU_TIPO";
         lblTextblockmenu_link_Internalname = sPrefix+"TEXTBLOCKMENU_LINK";
         edtMenu_Link_Internalname = sPrefix+"MENU_LINK";
         lblTextblockmenu_imagem_Internalname = sPrefix+"TEXTBLOCKMENU_IMAGEM";
         imgMenu_Imagem_Internalname = sPrefix+"MENU_IMAGEM";
         lblTextblockmenu_ordem_Internalname = sPrefix+"TEXTBLOCKMENU_ORDEM";
         edtMenu_Ordem_Internalname = sPrefix+"MENU_ORDEM";
         lblTextblockmenu_painom_Internalname = sPrefix+"TEXTBLOCKMENU_PAINOM";
         edtMenu_PaiNom_Internalname = sPrefix+"MENU_PAINOM";
         lblTextblockmenu_ativo_Internalname = sPrefix+"TEXTBLOCKMENU_ATIVO";
         chkMenu_Ativo_Internalname = sPrefix+"MENU_ATIVO";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtMenu_PaiNom_Jsonclick = "";
         edtMenu_Ordem_Jsonclick = "";
         edtMenu_Link_Jsonclick = "";
         cmbMenu_Tipo_Jsonclick = "";
         edtMenu_Descricao_Jsonclick = "";
         edtMenu_Nome_Jsonclick = "";
         bttBtndelete_Enabled = 1;
         bttBtnupdate_Enabled = 1;
         chkMenu_Ativo.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13612',iparms:[{av:'A277Menu_Codigo',fld:'MENU_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E14612',iparms:[{av:'A277Menu_Codigo',fld:'MENU_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A277Menu_Codigo',fld:'MENU_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV14Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A278Menu_Nome = "";
         A279Menu_Descricao = "";
         A281Menu_Link = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00612_A285Menu_PaiCod = new int[1] ;
         H00612_n285Menu_PaiCod = new bool[] {false} ;
         H00612_A277Menu_Codigo = new int[1] ;
         H00612_A284Menu_Ativo = new bool[] {false} ;
         H00612_A286Menu_PaiNom = new String[] {""} ;
         H00612_n286Menu_PaiNom = new bool[] {false} ;
         H00612_A283Menu_Ordem = new short[1] ;
         H00612_A40000Menu_Imagem_GXI = new String[] {""} ;
         H00612_n40000Menu_Imagem_GXI = new bool[] {false} ;
         H00612_A281Menu_Link = new String[] {""} ;
         H00612_n281Menu_Link = new bool[] {false} ;
         H00612_A280Menu_Tipo = new short[1] ;
         H00612_A279Menu_Descricao = new String[] {""} ;
         H00612_n279Menu_Descricao = new bool[] {false} ;
         H00612_A278Menu_Nome = new String[] {""} ;
         H00612_A282Menu_Imagem = new String[] {""} ;
         H00612_n282Menu_Imagem = new bool[] {false} ;
         A286Menu_PaiNom = "";
         A40000Menu_Imagem_GXI = "";
         A282Menu_Imagem = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblockmenu_nome_Jsonclick = "";
         lblTextblockmenu_descricao_Jsonclick = "";
         lblTextblockmenu_tipo_Jsonclick = "";
         lblTextblockmenu_link_Jsonclick = "";
         lblTextblockmenu_imagem_Jsonclick = "";
         lblTextblockmenu_ordem_Jsonclick = "";
         lblTextblockmenu_painom_Jsonclick = "";
         lblTextblockmenu_ativo_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA277Menu_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.menugeneral__default(),
            new Object[][] {
                new Object[] {
               H00612_A285Menu_PaiCod, H00612_n285Menu_PaiCod, H00612_A277Menu_Codigo, H00612_A284Menu_Ativo, H00612_A286Menu_PaiNom, H00612_n286Menu_PaiNom, H00612_A283Menu_Ordem, H00612_A40000Menu_Imagem_GXI, H00612_n40000Menu_Imagem_GXI, H00612_A281Menu_Link,
               H00612_n281Menu_Link, H00612_A280Menu_Tipo, H00612_A279Menu_Descricao, H00612_n279Menu_Descricao, H00612_A278Menu_Nome, H00612_A282Menu_Imagem, H00612_n282Menu_Imagem
               }
            }
         );
         AV14Pgmname = "MenuGeneral";
         /* GeneXus formulas. */
         AV14Pgmname = "MenuGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short A280Menu_Tipo ;
      private short A283Menu_Ordem ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A277Menu_Codigo ;
      private int wcpOA277Menu_Codigo ;
      private int A285Menu_PaiCod ;
      private int bttBtnupdate_Enabled ;
      private int bttBtndelete_Enabled ;
      private int AV7Menu_Codigo ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV14Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A278Menu_Nome ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String chkMenu_Ativo_Internalname ;
      private String scmdbuf ;
      private String A286Menu_PaiNom ;
      private String imgMenu_Imagem_Internalname ;
      private String edtMenu_Nome_Internalname ;
      private String edtMenu_Descricao_Internalname ;
      private String cmbMenu_Tipo_Internalname ;
      private String edtMenu_Link_Internalname ;
      private String edtMenu_Ordem_Internalname ;
      private String edtMenu_PaiNom_Internalname ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockmenu_nome_Internalname ;
      private String lblTextblockmenu_nome_Jsonclick ;
      private String edtMenu_Nome_Jsonclick ;
      private String lblTextblockmenu_descricao_Internalname ;
      private String lblTextblockmenu_descricao_Jsonclick ;
      private String edtMenu_Descricao_Jsonclick ;
      private String lblTextblockmenu_tipo_Internalname ;
      private String lblTextblockmenu_tipo_Jsonclick ;
      private String cmbMenu_Tipo_Jsonclick ;
      private String lblTextblockmenu_link_Internalname ;
      private String lblTextblockmenu_link_Jsonclick ;
      private String edtMenu_Link_Jsonclick ;
      private String lblTextblockmenu_imagem_Internalname ;
      private String lblTextblockmenu_imagem_Jsonclick ;
      private String lblTextblockmenu_ordem_Internalname ;
      private String lblTextblockmenu_ordem_Jsonclick ;
      private String edtMenu_Ordem_Jsonclick ;
      private String lblTextblockmenu_painom_Internalname ;
      private String lblTextblockmenu_painom_Jsonclick ;
      private String edtMenu_PaiNom_Jsonclick ;
      private String lblTextblockmenu_ativo_Internalname ;
      private String lblTextblockmenu_ativo_Jsonclick ;
      private String sCtrlA277Menu_Codigo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool A284Menu_Ativo ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n285Menu_PaiCod ;
      private bool n286Menu_PaiNom ;
      private bool n40000Menu_Imagem_GXI ;
      private bool n281Menu_Link ;
      private bool n279Menu_Descricao ;
      private bool n282Menu_Imagem ;
      private bool returnInSub ;
      private bool A282Menu_Imagem_IsBlob ;
      private String A279Menu_Descricao ;
      private String A281Menu_Link ;
      private String A40000Menu_Imagem_GXI ;
      private String A282Menu_Imagem ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbMenu_Tipo ;
      private GXCheckbox chkMenu_Ativo ;
      private IDataStoreProvider pr_default ;
      private int[] H00612_A285Menu_PaiCod ;
      private bool[] H00612_n285Menu_PaiCod ;
      private int[] H00612_A277Menu_Codigo ;
      private bool[] H00612_A284Menu_Ativo ;
      private String[] H00612_A286Menu_PaiNom ;
      private bool[] H00612_n286Menu_PaiNom ;
      private short[] H00612_A283Menu_Ordem ;
      private String[] H00612_A40000Menu_Imagem_GXI ;
      private bool[] H00612_n40000Menu_Imagem_GXI ;
      private String[] H00612_A281Menu_Link ;
      private bool[] H00612_n281Menu_Link ;
      private short[] H00612_A280Menu_Tipo ;
      private String[] H00612_A279Menu_Descricao ;
      private bool[] H00612_n279Menu_Descricao ;
      private String[] H00612_A278Menu_Nome ;
      private String[] H00612_A282Menu_Imagem ;
      private bool[] H00612_n282Menu_Imagem ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class menugeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00612 ;
          prmH00612 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00612", "SELECT T1.[Menu_PaiCod] AS Menu_PaiCod, T1.[Menu_Codigo], T1.[Menu_Ativo], T2.[Menu_Nome] AS Menu_PaiNom, T1.[Menu_Ordem], T1.[Menu_Imagem_GXI], T1.[Menu_Link], T1.[Menu_Tipo], T1.[Menu_Descricao], T1.[Menu_Nome], T1.[Menu_Imagem] FROM ([Menu] T1 WITH (NOLOCK) LEFT JOIN [Menu] T2 WITH (NOLOCK) ON T2.[Menu_Codigo] = T1.[Menu_PaiCod]) WHERE T1.[Menu_Codigo] = @Menu_Codigo ORDER BY T1.[Menu_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00612,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 30) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((short[]) buf[6])[0] = rslt.getShort(5) ;
                ((String[]) buf[7])[0] = rslt.getMultimediaUri(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((short[]) buf[11])[0] = rslt.getShort(8) ;
                ((String[]) buf[12])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((String[]) buf[14])[0] = rslt.getString(10, 30) ;
                ((String[]) buf[15])[0] = rslt.getMultimediaFile(11, rslt.getVarchar(6)) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
