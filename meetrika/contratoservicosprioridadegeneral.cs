/*
               File: ContratoServicosPrioridadeGeneral
        Description: Contrato Servicos Prioridade General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:27:54.86
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratoservicosprioridadegeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contratoservicosprioridadegeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contratoservicosprioridadegeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratoServicosPrioridade_Codigo )
      {
         this.A1336ContratoServicosPrioridade_Codigo = aP0_ContratoServicosPrioridade_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A1336ContratoServicosPrioridade_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1336ContratoServicosPrioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1336ContratoServicosPrioridade_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A1336ContratoServicosPrioridade_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAK32( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV14Pgmname = "ContratoServicosPrioridadeGeneral";
               context.Gx_err = 0;
               WSK32( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contrato Servicos Prioridade General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117275491");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratoservicosprioridadegeneral.aspx") + "?" + UrlEncode("" +A1336ContratoServicosPrioridade_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA1336ContratoServicosPrioridade_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA1336ContratoServicosPrioridade_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSPRIORIDADE_ORDEM", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2066ContratoServicosPrioridade_Ordem), "ZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSPRIORIDADE_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1337ContratoServicosPrioridade_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSPRIORIDADE_PERCVALORB", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1338ContratoServicosPrioridade_PercValorB, "ZZ9.99 %")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1339ContratoServicosPrioridade_PercPrazo, "ZZ9.99 %")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSPRIORIDADE_FINALIDADE", GetSecureSignedToken( sPrefix, A1359ContratoServicosPrioridade_Finalidade));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSPRIORIDADE_PESO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2067ContratoServicosPrioridade_Peso), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSPRIORIDADE_CNTSRVCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1335ContratoServicosPrioridade_CntSrvCod), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormK32( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contratoservicosprioridadegeneral.js", "?20203117275493");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContratoServicosPrioridadeGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contrato Servicos Prioridade General" ;
      }

      protected void WBK30( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contratoservicosprioridadegeneral.aspx");
            }
            wb_table1_2_K32( true) ;
         }
         else
         {
            wb_table1_2_K32( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_K32e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosPrioridade_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1336ContratoServicosPrioridade_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1336ContratoServicosPrioridade_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosPrioridade_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContratoServicosPrioridade_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoServicosPrioridadeGeneral.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosPrioridade_CntSrvCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1335ContratoServicosPrioridade_CntSrvCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1335ContratoServicosPrioridade_CntSrvCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosPrioridade_CntSrvCod_Jsonclick, 0, "Attribute", "", "", "", edtContratoServicosPrioridade_CntSrvCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoServicosPrioridadeGeneral.htm");
         }
         wbLoad = true;
      }

      protected void STARTK32( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contrato Servicos Prioridade General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPK30( ) ;
            }
         }
      }

      protected void WSK32( )
      {
         STARTK32( ) ;
         EVTK32( ) ;
      }

      protected void EVTK32( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPK30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPK30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11K32 */
                                    E11K32 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPK30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12K32 */
                                    E12K32 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPK30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13K32 */
                                    E13K32 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPK30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14K32 */
                                    E14K32 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPK30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPK30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEK32( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormK32( ) ;
            }
         }
      }

      protected void PAK32( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFK32( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV14Pgmname = "ContratoServicosPrioridadeGeneral";
         context.Gx_err = 0;
      }

      protected void RFK32( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00K32 */
            pr_default.execute(0, new Object[] {A1336ContratoServicosPrioridade_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A1335ContratoServicosPrioridade_CntSrvCod = H00K32_A1335ContratoServicosPrioridade_CntSrvCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1335ContratoServicosPrioridade_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1335ContratoServicosPrioridade_CntSrvCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSPRIORIDADE_CNTSRVCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1335ContratoServicosPrioridade_CntSrvCod), "ZZZZZ9")));
               A2067ContratoServicosPrioridade_Peso = H00K32_A2067ContratoServicosPrioridade_Peso[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2067ContratoServicosPrioridade_Peso", StringUtil.LTrim( StringUtil.Str( (decimal)(A2067ContratoServicosPrioridade_Peso), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSPRIORIDADE_PESO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2067ContratoServicosPrioridade_Peso), "ZZZ9")));
               n2067ContratoServicosPrioridade_Peso = H00K32_n2067ContratoServicosPrioridade_Peso[0];
               A1359ContratoServicosPrioridade_Finalidade = H00K32_A1359ContratoServicosPrioridade_Finalidade[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1359ContratoServicosPrioridade_Finalidade", A1359ContratoServicosPrioridade_Finalidade);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSPRIORIDADE_FINALIDADE", GetSecureSignedToken( sPrefix, A1359ContratoServicosPrioridade_Finalidade));
               n1359ContratoServicosPrioridade_Finalidade = H00K32_n1359ContratoServicosPrioridade_Finalidade[0];
               A1339ContratoServicosPrioridade_PercPrazo = H00K32_A1339ContratoServicosPrioridade_PercPrazo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1339ContratoServicosPrioridade_PercPrazo", StringUtil.LTrim( StringUtil.Str( A1339ContratoServicosPrioridade_PercPrazo, 6, 2)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1339ContratoServicosPrioridade_PercPrazo, "ZZ9.99 %")));
               n1339ContratoServicosPrioridade_PercPrazo = H00K32_n1339ContratoServicosPrioridade_PercPrazo[0];
               A1338ContratoServicosPrioridade_PercValorB = H00K32_A1338ContratoServicosPrioridade_PercValorB[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1338ContratoServicosPrioridade_PercValorB", StringUtil.LTrim( StringUtil.Str( A1338ContratoServicosPrioridade_PercValorB, 6, 2)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSPRIORIDADE_PERCVALORB", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1338ContratoServicosPrioridade_PercValorB, "ZZ9.99 %")));
               n1338ContratoServicosPrioridade_PercValorB = H00K32_n1338ContratoServicosPrioridade_PercValorB[0];
               A1337ContratoServicosPrioridade_Nome = H00K32_A1337ContratoServicosPrioridade_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1337ContratoServicosPrioridade_Nome", A1337ContratoServicosPrioridade_Nome);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSPRIORIDADE_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1337ContratoServicosPrioridade_Nome, "@!"))));
               A2066ContratoServicosPrioridade_Ordem = H00K32_A2066ContratoServicosPrioridade_Ordem[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2066ContratoServicosPrioridade_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A2066ContratoServicosPrioridade_Ordem), 3, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSPRIORIDADE_ORDEM", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2066ContratoServicosPrioridade_Ordem), "ZZ9")));
               n2066ContratoServicosPrioridade_Ordem = H00K32_n2066ContratoServicosPrioridade_Ordem[0];
               /* Execute user event: E12K32 */
               E12K32 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WBK30( ) ;
         }
      }

      protected void STRUPK30( )
      {
         /* Before Start, stand alone formulas. */
         AV14Pgmname = "ContratoServicosPrioridadeGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11K32 */
         E11K32 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A2066ContratoServicosPrioridade_Ordem = (short)(context.localUtil.CToN( cgiGet( edtContratoServicosPrioridade_Ordem_Internalname), ",", "."));
            n2066ContratoServicosPrioridade_Ordem = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2066ContratoServicosPrioridade_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A2066ContratoServicosPrioridade_Ordem), 3, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSPRIORIDADE_ORDEM", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2066ContratoServicosPrioridade_Ordem), "ZZ9")));
            A1337ContratoServicosPrioridade_Nome = StringUtil.Upper( cgiGet( edtContratoServicosPrioridade_Nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1337ContratoServicosPrioridade_Nome", A1337ContratoServicosPrioridade_Nome);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSPRIORIDADE_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1337ContratoServicosPrioridade_Nome, "@!"))));
            A1338ContratoServicosPrioridade_PercValorB = context.localUtil.CToN( cgiGet( edtContratoServicosPrioridade_PercValorB_Internalname), ",", ".");
            n1338ContratoServicosPrioridade_PercValorB = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1338ContratoServicosPrioridade_PercValorB", StringUtil.LTrim( StringUtil.Str( A1338ContratoServicosPrioridade_PercValorB, 6, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSPRIORIDADE_PERCVALORB", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1338ContratoServicosPrioridade_PercValorB, "ZZ9.99 %")));
            A1339ContratoServicosPrioridade_PercPrazo = context.localUtil.CToN( cgiGet( edtContratoServicosPrioridade_PercPrazo_Internalname), ",", ".");
            n1339ContratoServicosPrioridade_PercPrazo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1339ContratoServicosPrioridade_PercPrazo", StringUtil.LTrim( StringUtil.Str( A1339ContratoServicosPrioridade_PercPrazo, 6, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1339ContratoServicosPrioridade_PercPrazo, "ZZ9.99 %")));
            A1359ContratoServicosPrioridade_Finalidade = cgiGet( edtContratoServicosPrioridade_Finalidade_Internalname);
            n1359ContratoServicosPrioridade_Finalidade = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1359ContratoServicosPrioridade_Finalidade", A1359ContratoServicosPrioridade_Finalidade);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSPRIORIDADE_FINALIDADE", GetSecureSignedToken( sPrefix, A1359ContratoServicosPrioridade_Finalidade));
            A2067ContratoServicosPrioridade_Peso = (short)(context.localUtil.CToN( cgiGet( edtContratoServicosPrioridade_Peso_Internalname), ",", "."));
            n2067ContratoServicosPrioridade_Peso = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2067ContratoServicosPrioridade_Peso", StringUtil.LTrim( StringUtil.Str( (decimal)(A2067ContratoServicosPrioridade_Peso), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSPRIORIDADE_PESO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2067ContratoServicosPrioridade_Peso), "ZZZ9")));
            A1335ContratoServicosPrioridade_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosPrioridade_CntSrvCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1335ContratoServicosPrioridade_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1335ContratoServicosPrioridade_CntSrvCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSPRIORIDADE_CNTSRVCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1335ContratoServicosPrioridade_CntSrvCod), "ZZZZZ9")));
            /* Read saved values. */
            wcpOA1336ContratoServicosPrioridade_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1336ContratoServicosPrioridade_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11K32 */
         E11K32 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11K32( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         bttBtnupdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Visible), 5, 0)));
         bttBtndelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Visible), 5, 0)));
      }

      protected void nextLoad( )
      {
      }

      protected void E12K32( )
      {
         /* Load Routine */
         edtContratoServicosPrioridade_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicosPrioridade_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrioridade_Codigo_Visible), 5, 0)));
         edtContratoServicosPrioridade_CntSrvCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicosPrioridade_CntSrvCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrioridade_CntSrvCod_Visible), 5, 0)));
      }

      protected void E13K32( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("contratoservicosprioridade.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1336ContratoServicosPrioridade_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E14K32( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("contratoservicosprioridade.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A1336ContratoServicosPrioridade_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV14Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ContratoServicosPrioridade";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "ContratoServicosPrioridade_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ContratoServicosPrioridade_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_K32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_K32( true) ;
         }
         else
         {
            wb_table2_8_K32( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_K32e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_54_K32( true) ;
         }
         else
         {
            wb_table3_54_K32( false) ;
         }
         return  ;
      }

      protected void wb_table3_54_K32e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_K32e( true) ;
         }
         else
         {
            wb_table1_2_K32e( false) ;
         }
      }

      protected void wb_table3_54_K32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, bttBtnupdate_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosPrioridadeGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtndelete_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosPrioridadeGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_54_K32e( true) ;
         }
         else
         {
            wb_table3_54_K32e( false) ;
         }
      }

      protected void wb_table2_8_K32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosprioridade_ordem_Internalname, "Ordem", "", "", lblTextblockcontratoservicosprioridade_ordem_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosPrioridadeGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosPrioridade_Ordem_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A2066ContratoServicosPrioridade_Ordem), 3, 0, ",", "")), context.localUtil.Format( (decimal)(A2066ContratoServicosPrioridade_Ordem), "ZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosPrioridade_Ordem_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 40, "px", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Ordem", "right", false, "HLP_ContratoServicosPrioridadeGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosprioridade_nome_Internalname, "Nome", "", "", lblTextblockcontratoservicosprioridade_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosPrioridadeGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosPrioridade_Nome_Internalname, StringUtil.RTrim( A1337ContratoServicosPrioridade_Nome), StringUtil.RTrim( context.localUtil.Format( A1337ContratoServicosPrioridade_Nome, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosPrioridade_Nome_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 0, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ContratoServicosPrioridadeGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosprioridade_percvalorb_Internalname, "% do Valor B", "", "", lblTextblockcontratoservicosprioridade_percvalorb_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosPrioridadeGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosPrioridade_PercValorB_Internalname, StringUtil.LTrim( StringUtil.NToC( A1338ContratoServicosPrioridade_PercValorB, 8, 2, ",", "")), context.localUtil.Format( A1338ContratoServicosPrioridade_PercValorB, "ZZ9.99 %"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosPrioridade_PercValorB_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosPrioridadeGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosprioridade_percprazo_Internalname, "% do Prazo", "", "", lblTextblockcontratoservicosprioridade_percprazo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosPrioridadeGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosPrioridade_PercPrazo_Internalname, StringUtil.LTrim( StringUtil.NToC( A1339ContratoServicosPrioridade_PercPrazo, 8, 2, ",", "")), context.localUtil.Format( A1339ContratoServicosPrioridade_PercPrazo, "ZZ9.99 %"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosPrioridade_PercPrazo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosPrioridadeGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosprioridade_finalidade_Internalname, "Finalidade", "", "", lblTextblockcontratoservicosprioridade_finalidade_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosPrioridadeGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContratoServicosPrioridade_Finalidade_Internalname, A1359ContratoServicosPrioridade_Finalidade, "", "", 0, 1, 0, 0, 100, "%", 3, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "DescricaoLonga", "HLP_ContratoServicosPrioridadeGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosprioridade_peso_Internalname, "Peso", "", "", lblTextblockcontratoservicosprioridade_peso_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosPrioridadeGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosPrioridade_Peso_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A2067ContratoServicosPrioridade_Peso), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A2067ContratoServicosPrioridade_Peso), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosPrioridade_Peso_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosPrioridadeGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_K32e( true) ;
         }
         else
         {
            wb_table2_8_K32e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A1336ContratoServicosPrioridade_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1336ContratoServicosPrioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1336ContratoServicosPrioridade_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAK32( ) ;
         WSK32( ) ;
         WEK32( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA1336ContratoServicosPrioridade_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAK32( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contratoservicosprioridadegeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAK32( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A1336ContratoServicosPrioridade_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1336ContratoServicosPrioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1336ContratoServicosPrioridade_Codigo), 6, 0)));
         }
         wcpOA1336ContratoServicosPrioridade_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1336ContratoServicosPrioridade_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A1336ContratoServicosPrioridade_Codigo != wcpOA1336ContratoServicosPrioridade_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA1336ContratoServicosPrioridade_Codigo = A1336ContratoServicosPrioridade_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA1336ContratoServicosPrioridade_Codigo = cgiGet( sPrefix+"A1336ContratoServicosPrioridade_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA1336ContratoServicosPrioridade_Codigo) > 0 )
         {
            A1336ContratoServicosPrioridade_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA1336ContratoServicosPrioridade_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1336ContratoServicosPrioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1336ContratoServicosPrioridade_Codigo), 6, 0)));
         }
         else
         {
            A1336ContratoServicosPrioridade_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A1336ContratoServicosPrioridade_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAK32( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSK32( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSK32( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A1336ContratoServicosPrioridade_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1336ContratoServicosPrioridade_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA1336ContratoServicosPrioridade_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A1336ContratoServicosPrioridade_Codigo_CTRL", StringUtil.RTrim( sCtrlA1336ContratoServicosPrioridade_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEK32( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117275519");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contratoservicosprioridadegeneral.js", "?20203117275519");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontratoservicosprioridade_ordem_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOSPRIORIDADE_ORDEM";
         edtContratoServicosPrioridade_Ordem_Internalname = sPrefix+"CONTRATOSERVICOSPRIORIDADE_ORDEM";
         lblTextblockcontratoservicosprioridade_nome_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOSPRIORIDADE_NOME";
         edtContratoServicosPrioridade_Nome_Internalname = sPrefix+"CONTRATOSERVICOSPRIORIDADE_NOME";
         lblTextblockcontratoservicosprioridade_percvalorb_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOSPRIORIDADE_PERCVALORB";
         edtContratoServicosPrioridade_PercValorB_Internalname = sPrefix+"CONTRATOSERVICOSPRIORIDADE_PERCVALORB";
         lblTextblockcontratoservicosprioridade_percprazo_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOSPRIORIDADE_PERCPRAZO";
         edtContratoServicosPrioridade_PercPrazo_Internalname = sPrefix+"CONTRATOSERVICOSPRIORIDADE_PERCPRAZO";
         lblTextblockcontratoservicosprioridade_finalidade_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOSPRIORIDADE_FINALIDADE";
         edtContratoServicosPrioridade_Finalidade_Internalname = sPrefix+"CONTRATOSERVICOSPRIORIDADE_FINALIDADE";
         lblTextblockcontratoservicosprioridade_peso_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOSPRIORIDADE_PESO";
         edtContratoServicosPrioridade_Peso_Internalname = sPrefix+"CONTRATOSERVICOSPRIORIDADE_PESO";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtContratoServicosPrioridade_Codigo_Internalname = sPrefix+"CONTRATOSERVICOSPRIORIDADE_CODIGO";
         edtContratoServicosPrioridade_CntSrvCod_Internalname = sPrefix+"CONTRATOSERVICOSPRIORIDADE_CNTSRVCOD";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtContratoServicosPrioridade_Peso_Jsonclick = "";
         edtContratoServicosPrioridade_PercPrazo_Jsonclick = "";
         edtContratoServicosPrioridade_PercValorB_Jsonclick = "";
         edtContratoServicosPrioridade_Nome_Jsonclick = "";
         edtContratoServicosPrioridade_Ordem_Jsonclick = "";
         bttBtndelete_Visible = 1;
         bttBtnupdate_Visible = 1;
         edtContratoServicosPrioridade_CntSrvCod_Jsonclick = "";
         edtContratoServicosPrioridade_CntSrvCod_Visible = 1;
         edtContratoServicosPrioridade_Codigo_Jsonclick = "";
         edtContratoServicosPrioridade_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13K32',iparms:[{av:'A1336ContratoServicosPrioridade_Codigo',fld:'CONTRATOSERVICOSPRIORIDADE_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E14K32',iparms:[{av:'A1336ContratoServicosPrioridade_Codigo',fld:'CONTRATOSERVICOSPRIORIDADE_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV14Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A1337ContratoServicosPrioridade_Nome = "";
         A1359ContratoServicosPrioridade_Finalidade = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00K32_A1336ContratoServicosPrioridade_Codigo = new int[1] ;
         H00K32_A1335ContratoServicosPrioridade_CntSrvCod = new int[1] ;
         H00K32_A2067ContratoServicosPrioridade_Peso = new short[1] ;
         H00K32_n2067ContratoServicosPrioridade_Peso = new bool[] {false} ;
         H00K32_A1359ContratoServicosPrioridade_Finalidade = new String[] {""} ;
         H00K32_n1359ContratoServicosPrioridade_Finalidade = new bool[] {false} ;
         H00K32_A1339ContratoServicosPrioridade_PercPrazo = new decimal[1] ;
         H00K32_n1339ContratoServicosPrioridade_PercPrazo = new bool[] {false} ;
         H00K32_A1338ContratoServicosPrioridade_PercValorB = new decimal[1] ;
         H00K32_n1338ContratoServicosPrioridade_PercValorB = new bool[] {false} ;
         H00K32_A1337ContratoServicosPrioridade_Nome = new String[] {""} ;
         H00K32_A2066ContratoServicosPrioridade_Ordem = new short[1] ;
         H00K32_n2066ContratoServicosPrioridade_Ordem = new bool[] {false} ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblockcontratoservicosprioridade_ordem_Jsonclick = "";
         lblTextblockcontratoservicosprioridade_nome_Jsonclick = "";
         lblTextblockcontratoservicosprioridade_percvalorb_Jsonclick = "";
         lblTextblockcontratoservicosprioridade_percprazo_Jsonclick = "";
         lblTextblockcontratoservicosprioridade_finalidade_Jsonclick = "";
         lblTextblockcontratoservicosprioridade_peso_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA1336ContratoServicosPrioridade_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratoservicosprioridadegeneral__default(),
            new Object[][] {
                new Object[] {
               H00K32_A1336ContratoServicosPrioridade_Codigo, H00K32_A1335ContratoServicosPrioridade_CntSrvCod, H00K32_A2067ContratoServicosPrioridade_Peso, H00K32_n2067ContratoServicosPrioridade_Peso, H00K32_A1359ContratoServicosPrioridade_Finalidade, H00K32_n1359ContratoServicosPrioridade_Finalidade, H00K32_A1339ContratoServicosPrioridade_PercPrazo, H00K32_n1339ContratoServicosPrioridade_PercPrazo, H00K32_A1338ContratoServicosPrioridade_PercValorB, H00K32_n1338ContratoServicosPrioridade_PercValorB,
               H00K32_A1337ContratoServicosPrioridade_Nome, H00K32_A2066ContratoServicosPrioridade_Ordem, H00K32_n2066ContratoServicosPrioridade_Ordem
               }
            }
         );
         AV14Pgmname = "ContratoServicosPrioridadeGeneral";
         /* GeneXus formulas. */
         AV14Pgmname = "ContratoServicosPrioridadeGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short A2066ContratoServicosPrioridade_Ordem ;
      private short A2067ContratoServicosPrioridade_Peso ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A1336ContratoServicosPrioridade_Codigo ;
      private int wcpOA1336ContratoServicosPrioridade_Codigo ;
      private int A1335ContratoServicosPrioridade_CntSrvCod ;
      private int edtContratoServicosPrioridade_Codigo_Visible ;
      private int edtContratoServicosPrioridade_CntSrvCod_Visible ;
      private int bttBtnupdate_Visible ;
      private int bttBtndelete_Visible ;
      private int AV7ContratoServicosPrioridade_Codigo ;
      private int idxLst ;
      private decimal A1338ContratoServicosPrioridade_PercValorB ;
      private decimal A1339ContratoServicosPrioridade_PercPrazo ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV14Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A1337ContratoServicosPrioridade_Nome ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String edtContratoServicosPrioridade_Codigo_Internalname ;
      private String edtContratoServicosPrioridade_Codigo_Jsonclick ;
      private String edtContratoServicosPrioridade_CntSrvCod_Internalname ;
      private String edtContratoServicosPrioridade_CntSrvCod_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String edtContratoServicosPrioridade_Ordem_Internalname ;
      private String edtContratoServicosPrioridade_Nome_Internalname ;
      private String edtContratoServicosPrioridade_PercValorB_Internalname ;
      private String edtContratoServicosPrioridade_PercPrazo_Internalname ;
      private String edtContratoServicosPrioridade_Finalidade_Internalname ;
      private String edtContratoServicosPrioridade_Peso_Internalname ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcontratoservicosprioridade_ordem_Internalname ;
      private String lblTextblockcontratoservicosprioridade_ordem_Jsonclick ;
      private String edtContratoServicosPrioridade_Ordem_Jsonclick ;
      private String lblTextblockcontratoservicosprioridade_nome_Internalname ;
      private String lblTextblockcontratoservicosprioridade_nome_Jsonclick ;
      private String edtContratoServicosPrioridade_Nome_Jsonclick ;
      private String lblTextblockcontratoservicosprioridade_percvalorb_Internalname ;
      private String lblTextblockcontratoservicosprioridade_percvalorb_Jsonclick ;
      private String edtContratoServicosPrioridade_PercValorB_Jsonclick ;
      private String lblTextblockcontratoservicosprioridade_percprazo_Internalname ;
      private String lblTextblockcontratoservicosprioridade_percprazo_Jsonclick ;
      private String edtContratoServicosPrioridade_PercPrazo_Jsonclick ;
      private String lblTextblockcontratoservicosprioridade_finalidade_Internalname ;
      private String lblTextblockcontratoservicosprioridade_finalidade_Jsonclick ;
      private String lblTextblockcontratoservicosprioridade_peso_Internalname ;
      private String lblTextblockcontratoservicosprioridade_peso_Jsonclick ;
      private String edtContratoServicosPrioridade_Peso_Jsonclick ;
      private String sCtrlA1336ContratoServicosPrioridade_Codigo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n2067ContratoServicosPrioridade_Peso ;
      private bool n1359ContratoServicosPrioridade_Finalidade ;
      private bool n1339ContratoServicosPrioridade_PercPrazo ;
      private bool n1338ContratoServicosPrioridade_PercValorB ;
      private bool n2066ContratoServicosPrioridade_Ordem ;
      private bool returnInSub ;
      private String A1359ContratoServicosPrioridade_Finalidade ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00K32_A1336ContratoServicosPrioridade_Codigo ;
      private int[] H00K32_A1335ContratoServicosPrioridade_CntSrvCod ;
      private short[] H00K32_A2067ContratoServicosPrioridade_Peso ;
      private bool[] H00K32_n2067ContratoServicosPrioridade_Peso ;
      private String[] H00K32_A1359ContratoServicosPrioridade_Finalidade ;
      private bool[] H00K32_n1359ContratoServicosPrioridade_Finalidade ;
      private decimal[] H00K32_A1339ContratoServicosPrioridade_PercPrazo ;
      private bool[] H00K32_n1339ContratoServicosPrioridade_PercPrazo ;
      private decimal[] H00K32_A1338ContratoServicosPrioridade_PercValorB ;
      private bool[] H00K32_n1338ContratoServicosPrioridade_PercValorB ;
      private String[] H00K32_A1337ContratoServicosPrioridade_Nome ;
      private short[] H00K32_A2066ContratoServicosPrioridade_Ordem ;
      private bool[] H00K32_n2066ContratoServicosPrioridade_Ordem ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class contratoservicosprioridadegeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00K32 ;
          prmH00K32 = new Object[] {
          new Object[] {"@ContratoServicosPrioridade_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00K32", "SELECT [ContratoServicosPrioridade_Codigo], [ContratoServicosPrioridade_CntSrvCod], [ContratoServicosPrioridade_Peso], [ContratoServicosPrioridade_Finalidade], [ContratoServicosPrioridade_PercPrazo], [ContratoServicosPrioridade_PercValorB], [ContratoServicosPrioridade_Nome], [ContratoServicosPrioridade_Ordem] FROM [ContratoServicosPrioridade] WITH (NOLOCK) WHERE [ContratoServicosPrioridade_Codigo] = @ContratoServicosPrioridade_Codigo ORDER BY [ContratoServicosPrioridade_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00K32,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 50) ;
                ((short[]) buf[11])[0] = rslt.getShort(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
