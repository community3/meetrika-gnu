/*
               File: PRC_EmailInstanciaDestinatario_New_by_Email
        Description: PRC_Email Instancia Destinatario_New_by_Email
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:29:38.30
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_emailinstanciadestinatario_new_by_email : GXProcedure
   {
      public prc_emailinstanciadestinatario_new_by_email( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_emailinstanciadestinatario_new_by_email( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( Guid aP0_Email_Instancia_Guid ,
                           String aP1_Email_Instancia_Destinatarios_Email )
      {
         this.AV8Email_Instancia_Guid = aP0_Email_Instancia_Guid;
         this.AV10Email_Instancia_Destinatarios_Email = aP1_Email_Instancia_Destinatarios_Email;
         initialize();
         executePrivate();
      }

      public void executeSubmit( Guid aP0_Email_Instancia_Guid ,
                                 String aP1_Email_Instancia_Destinatarios_Email )
      {
         prc_emailinstanciadestinatario_new_by_email objprc_emailinstanciadestinatario_new_by_email;
         objprc_emailinstanciadestinatario_new_by_email = new prc_emailinstanciadestinatario_new_by_email();
         objprc_emailinstanciadestinatario_new_by_email.AV8Email_Instancia_Guid = aP0_Email_Instancia_Guid;
         objprc_emailinstanciadestinatario_new_by_email.AV10Email_Instancia_Destinatarios_Email = aP1_Email_Instancia_Destinatarios_Email;
         objprc_emailinstanciadestinatario_new_by_email.context.SetSubmitInitialConfig(context);
         objprc_emailinstanciadestinatario_new_by_email.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_emailinstanciadestinatario_new_by_email);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_emailinstanciadestinatario_new_by_email)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV9Email_Instancia_Destinatarios.Load(AV8Email_Instancia_Guid, Guid.Empty);
         AV9Email_Instancia_Destinatarios.gxTpr_Email_instancia_destinatarios_email = StringUtil.Trim( AV10Email_Instancia_Destinatarios_Email);
         AV9Email_Instancia_Destinatarios.Save();
         if ( AV9Email_Instancia_Destinatarios.Success() )
         {
            context.CommitDataStores( "PRC_EmailInstanciaDestinatario_New_by_Email");
         }
         else
         {
            new gravalogbc(context ).execute(  "Email",  "Email_Instancia_Destinatarios",  AV9Email_Instancia_Destinatarios.ToJSonString(true),  AV9Email_Instancia_Destinatarios.GetMessages().ToJSonString(false)) ;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV9Email_Instancia_Destinatarios = new SdtEmail_Instancia_Destinatarios(context);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_emailinstanciadestinatario_new_by_email__default(),
            new Object[][] {
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private String AV10Email_Instancia_Destinatarios_Email ;
      private Guid AV8Email_Instancia_Guid ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private SdtEmail_Instancia_Destinatarios AV9Email_Instancia_Destinatarios ;
   }

   public class prc_emailinstanciadestinatario_new_by_email__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
