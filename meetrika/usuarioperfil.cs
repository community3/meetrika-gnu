/*
               File: UsuarioPerfil
        Description: Usuario x Perfil
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:13:58.88
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class usuarioperfil : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_9") == 0 )
         {
            A1Usuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_9( A1Usuario_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_11") == 0 )
         {
            A57Usuario_PessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A57Usuario_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A57Usuario_PessoaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_11( A57Usuario_PessoaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_10") == 0 )
         {
            A3Perfil_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_10( A3Perfil_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Usuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Usuario_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSUARIO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Usuario_Codigo), "ZZZZZ9")));
               AV8Perfil_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Perfil_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPERFIL_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8Perfil_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbPerfil_Tipo.Name = "PERFIL_TIPO";
         cmbPerfil_Tipo.WebTags = "";
         cmbPerfil_Tipo.addItem("0", "Desconhecido", 0);
         cmbPerfil_Tipo.addItem("1", "Auditor", 0);
         cmbPerfil_Tipo.addItem("2", "Contador", 0);
         cmbPerfil_Tipo.addItem("3", "Contratada", 0);
         cmbPerfil_Tipo.addItem("4", "Contratante", 0);
         cmbPerfil_Tipo.addItem("5", "Financeiro", 0);
         cmbPerfil_Tipo.addItem("6", "Usuario", 0);
         cmbPerfil_Tipo.addItem("10", "Licensiado", 0);
         cmbPerfil_Tipo.addItem("99", "Administrador GAM", 0);
         if ( cmbPerfil_Tipo.ItemCount > 0 )
         {
            A275Perfil_Tipo = (short)(NumberUtil.Val( cmbPerfil_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A275Perfil_Tipo), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A275Perfil_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A275Perfil_Tipo), 4, 0)));
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Usuario x Perfil", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtUsuario_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public usuarioperfil( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public usuarioperfil( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Usuario_Codigo ,
                           int aP2_Perfil_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Usuario_Codigo = aP1_Usuario_Codigo;
         this.AV8Perfil_Codigo = aP2_Perfil_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbPerfil_Tipo = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbPerfil_Tipo.ItemCount > 0 )
         {
            A275Perfil_Tipo = (short)(NumberUtil.Val( cmbPerfil_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A275Perfil_Tipo), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A275Perfil_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A275Perfil_Tipo), 4, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_023( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_023e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtUsuario_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A1Usuario_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUsuario_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtUsuario_Codigo_Visible, edtUsuario_Codigo_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_UsuarioPerfil.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtUsuario_PessoaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A57Usuario_PessoaCod), 6, 0, ",", "")), ((edtUsuario_PessoaCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A57Usuario_PessoaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A57Usuario_PessoaCod), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUsuario_PessoaCod_Jsonclick, 0, "Attribute", "", "", "", edtUsuario_PessoaCod_Visible, edtUsuario_PessoaCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_UsuarioPerfil.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtPerfil_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A3Perfil_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A3Perfil_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,51);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPerfil_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtPerfil_Codigo_Visible, edtPerfil_Codigo_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_UsuarioPerfil.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_023( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_023( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_023e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_41_023( true) ;
         }
         return  ;
      }

      protected void wb_table3_41_023e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_023e( true) ;
         }
         else
         {
            wb_table1_2_023e( false) ;
         }
      }

      protected void wb_table3_41_023( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_UsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_UsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_UsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_41_023e( true) ;
         }
         else
         {
            wb_table3_41_023e( false) ;
         }
      }

      protected void wb_table2_5_023( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_023( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_023e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_023e( true) ;
         }
         else
         {
            wb_table2_5_023e( false) ;
         }
      }

      protected void wb_table4_13_023( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_nome_Internalname, "Usu�rio", "", "", lblTextblockusuario_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_UsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtUsuario_Nome_Internalname, StringUtil.RTrim( A2Usuario_Nome), StringUtil.RTrim( context.localUtil.Format( A2Usuario_Nome, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUsuario_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtUsuario_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_UsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_pessoanom_Internalname, "Pessoa", "", "", lblTextblockusuario_pessoanom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_UsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtUsuario_PessoaNom_Internalname, StringUtil.RTrim( A58Usuario_PessoaNom), StringUtil.RTrim( context.localUtil.Format( A58Usuario_PessoaNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUsuario_PessoaNom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtUsuario_PessoaNom_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_UsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockperfil_nome_Internalname, "Perfil", "", "", lblTextblockperfil_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_UsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtPerfil_Nome_Internalname, StringUtil.RTrim( A4Perfil_Nome), StringUtil.RTrim( context.localUtil.Format( A4Perfil_Nome, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPerfil_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtPerfil_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_UsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockperfil_tipo_Internalname, "Tipo do Perfil", "", "", lblTextblockperfil_tipo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_UsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbPerfil_Tipo, cmbPerfil_Tipo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A275Perfil_Tipo), 4, 0)), 1, cmbPerfil_Tipo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbPerfil_Tipo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_UsuarioPerfil.htm");
            cmbPerfil_Tipo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A275Perfil_Tipo), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbPerfil_Tipo_Internalname, "Values", (String)(cmbPerfil_Tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockperfil_gamid_Internalname, "Gam Id", "", "", lblTextblockperfil_gamid_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_UsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtPerfil_GamId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A329Perfil_GamId), 12, 0, ",", "")), ((edtPerfil_GamId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A329Perfil_GamId), "ZZZZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A329Perfil_GamId), "ZZZZZZZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPerfil_GamId_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtPerfil_GamId_Enabled, 0, "text", "", 12, "chr", 1, "row", 12, 0, 0, 0, 1, -1, 0, true, "GAMKeyNumLong", "right", false, "HLP_UsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_023e( true) ;
         }
         else
         {
            wb_table4_13_023e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11022 */
         E11022 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A2Usuario_Nome = StringUtil.Upper( cgiGet( edtUsuario_Nome_Internalname));
               n2Usuario_Nome = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2Usuario_Nome", A2Usuario_Nome);
               A58Usuario_PessoaNom = StringUtil.Upper( cgiGet( edtUsuario_PessoaNom_Internalname));
               n58Usuario_PessoaNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A58Usuario_PessoaNom", A58Usuario_PessoaNom);
               A4Perfil_Nome = StringUtil.Upper( cgiGet( edtPerfil_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4Perfil_Nome", A4Perfil_Nome);
               cmbPerfil_Tipo.CurrentValue = cgiGet( cmbPerfil_Tipo_Internalname);
               A275Perfil_Tipo = (short)(NumberUtil.Val( cgiGet( cmbPerfil_Tipo_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A275Perfil_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A275Perfil_Tipo), 4, 0)));
               A329Perfil_GamId = (long)(context.localUtil.CToN( cgiGet( edtPerfil_GamId_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A329Perfil_GamId", StringUtil.LTrim( StringUtil.Str( (decimal)(A329Perfil_GamId), 12, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtUsuario_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtUsuario_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "USUARIO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtUsuario_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1Usuario_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
               }
               else
               {
                  A1Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( edtUsuario_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
               }
               A57Usuario_PessoaCod = (int)(context.localUtil.CToN( cgiGet( edtUsuario_PessoaCod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A57Usuario_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A57Usuario_PessoaCod), 6, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtPerfil_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtPerfil_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "PERFIL_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtPerfil_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A3Perfil_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
               }
               else
               {
                  A3Perfil_Codigo = (int)(context.localUtil.CToN( cgiGet( edtPerfil_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
               }
               /* Read saved values. */
               Z1Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1Usuario_Codigo"), ",", "."));
               Z3Perfil_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z3Perfil_Codigo"), ",", "."));
               Z543UsuarioPerfil_Display = StringUtil.StrToBool( cgiGet( "Z543UsuarioPerfil_Display"));
               n543UsuarioPerfil_Display = ((false==A543UsuarioPerfil_Display) ? true : false);
               Z544UsuarioPerfil_Insert = StringUtil.StrToBool( cgiGet( "Z544UsuarioPerfil_Insert"));
               n544UsuarioPerfil_Insert = ((false==A544UsuarioPerfil_Insert) ? true : false);
               Z659UsuarioPerfil_Update = StringUtil.StrToBool( cgiGet( "Z659UsuarioPerfil_Update"));
               n659UsuarioPerfil_Update = ((false==A659UsuarioPerfil_Update) ? true : false);
               Z546UsuarioPerfil_Delete = StringUtil.StrToBool( cgiGet( "Z546UsuarioPerfil_Delete"));
               n546UsuarioPerfil_Delete = ((false==A546UsuarioPerfil_Delete) ? true : false);
               A543UsuarioPerfil_Display = StringUtil.StrToBool( cgiGet( "Z543UsuarioPerfil_Display"));
               n543UsuarioPerfil_Display = false;
               n543UsuarioPerfil_Display = ((false==A543UsuarioPerfil_Display) ? true : false);
               A544UsuarioPerfil_Insert = StringUtil.StrToBool( cgiGet( "Z544UsuarioPerfil_Insert"));
               n544UsuarioPerfil_Insert = false;
               n544UsuarioPerfil_Insert = ((false==A544UsuarioPerfil_Insert) ? true : false);
               A659UsuarioPerfil_Update = StringUtil.StrToBool( cgiGet( "Z659UsuarioPerfil_Update"));
               n659UsuarioPerfil_Update = false;
               n659UsuarioPerfil_Update = ((false==A659UsuarioPerfil_Update) ? true : false);
               A546UsuarioPerfil_Delete = StringUtil.StrToBool( cgiGet( "Z546UsuarioPerfil_Delete"));
               n546UsuarioPerfil_Delete = false;
               n546UsuarioPerfil_Delete = ((false==A546UsuarioPerfil_Delete) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               AV7Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( "vUSUARIO_CODIGO"), ",", "."));
               AV8Perfil_Codigo = (int)(context.localUtil.CToN( cgiGet( "vPERFIL_CODIGO"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A543UsuarioPerfil_Display = StringUtil.StrToBool( cgiGet( "USUARIOPERFIL_DISPLAY"));
               n543UsuarioPerfil_Display = ((false==A543UsuarioPerfil_Display) ? true : false);
               A544UsuarioPerfil_Insert = StringUtil.StrToBool( cgiGet( "USUARIOPERFIL_INSERT"));
               n544UsuarioPerfil_Insert = ((false==A544UsuarioPerfil_Insert) ? true : false);
               A659UsuarioPerfil_Update = StringUtil.StrToBool( cgiGet( "USUARIOPERFIL_UPDATE"));
               n659UsuarioPerfil_Update = ((false==A659UsuarioPerfil_Update) ? true : false);
               A546UsuarioPerfil_Delete = StringUtil.StrToBool( cgiGet( "USUARIOPERFIL_DELETE"));
               n546UsuarioPerfil_Delete = ((false==A546UsuarioPerfil_Delete) ? true : false);
               A341Usuario_UserGamGuid = cgiGet( "USUARIO_USERGAMGUID");
               A276Perfil_Ativo = StringUtil.StrToBool( cgiGet( "PERFIL_ATIVO"));
               A7Perfil_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "PERFIL_AREATRABALHOCOD"), ",", "."));
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "UsuarioPerfil";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A543UsuarioPerfil_Display);
               forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A544UsuarioPerfil_Insert);
               forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A659UsuarioPerfil_Update);
               forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A546UsuarioPerfil_Delete);
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A1Usuario_Codigo != Z1Usuario_Codigo ) || ( A3Perfil_Codigo != Z3Perfil_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("usuarioperfil:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("usuarioperfil:[SecurityCheckFailed value for]"+"UsuarioPerfil_Display:"+StringUtil.BoolToStr( A543UsuarioPerfil_Display));
                  GXUtil.WriteLog("usuarioperfil:[SecurityCheckFailed value for]"+"UsuarioPerfil_Insert:"+StringUtil.BoolToStr( A544UsuarioPerfil_Insert));
                  GXUtil.WriteLog("usuarioperfil:[SecurityCheckFailed value for]"+"UsuarioPerfil_Update:"+StringUtil.BoolToStr( A659UsuarioPerfil_Update));
                  GXUtil.WriteLog("usuarioperfil:[SecurityCheckFailed value for]"+"UsuarioPerfil_Delete:"+StringUtil.BoolToStr( A546UsuarioPerfil_Delete));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A1Usuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
                  A3Perfil_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode3 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode3;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound3 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_020( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "USUARIO_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtUsuario_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11022 */
                           E11022 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12022 */
                           E12022 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E12022 */
            E12022 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll023( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes023( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_020( )
      {
         BeforeValidate023( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls023( ) ;
            }
            else
            {
               CheckExtendedTable023( ) ;
               CloseExtendedTableCursors023( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption020( )
      {
      }

      protected void E11022( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         AV10TrnContext.FromXml(AV11WebSession.Get("TrnContext"), "");
         edtUsuario_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_Codigo_Visible), 5, 0)));
         edtUsuario_PessoaCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_PessoaCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_PessoaCod_Visible), 5, 0)));
         edtPerfil_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPerfil_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPerfil_Codigo_Visible), 5, 0)));
      }

      protected void E12022( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV10TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwusuarioperfil.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM023( short GX_JID )
      {
         if ( ( GX_JID == 8 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z543UsuarioPerfil_Display = T00023_A543UsuarioPerfil_Display[0];
               Z544UsuarioPerfil_Insert = T00023_A544UsuarioPerfil_Insert[0];
               Z659UsuarioPerfil_Update = T00023_A659UsuarioPerfil_Update[0];
               Z546UsuarioPerfil_Delete = T00023_A546UsuarioPerfil_Delete[0];
            }
            else
            {
               Z543UsuarioPerfil_Display = A543UsuarioPerfil_Display;
               Z544UsuarioPerfil_Insert = A544UsuarioPerfil_Insert;
               Z659UsuarioPerfil_Update = A659UsuarioPerfil_Update;
               Z546UsuarioPerfil_Delete = A546UsuarioPerfil_Delete;
            }
         }
         if ( GX_JID == -8 )
         {
            Z543UsuarioPerfil_Display = A543UsuarioPerfil_Display;
            Z544UsuarioPerfil_Insert = A544UsuarioPerfil_Insert;
            Z659UsuarioPerfil_Update = A659UsuarioPerfil_Update;
            Z546UsuarioPerfil_Delete = A546UsuarioPerfil_Delete;
            Z1Usuario_Codigo = A1Usuario_Codigo;
            Z3Perfil_Codigo = A3Perfil_Codigo;
            Z2Usuario_Nome = A2Usuario_Nome;
            Z341Usuario_UserGamGuid = A341Usuario_UserGamGuid;
            Z57Usuario_PessoaCod = A57Usuario_PessoaCod;
            Z58Usuario_PessoaNom = A58Usuario_PessoaNom;
            Z4Perfil_Nome = A4Perfil_Nome;
            Z275Perfil_Tipo = A275Perfil_Tipo;
            Z329Perfil_GamId = A329Perfil_GamId;
            Z276Perfil_Ativo = A276Perfil_Ativo;
            Z7Perfil_AreaTrabalhoCod = A7Perfil_AreaTrabalhoCod;
         }
      }

      protected void standaloneNotModal( )
      {
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7Usuario_Codigo) )
         {
            A1Usuario_Codigo = AV7Usuario_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
         }
         if ( ! (0==AV7Usuario_Codigo) )
         {
            edtUsuario_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_Codigo_Enabled), 5, 0)));
         }
         else
         {
            edtUsuario_Codigo_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_Codigo_Enabled), 5, 0)));
         }
         if ( ! (0==AV7Usuario_Codigo) )
         {
            edtUsuario_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_Codigo_Enabled), 5, 0)));
         }
         if ( ! (0==AV8Perfil_Codigo) )
         {
            A3Perfil_Codigo = AV8Perfil_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
         }
         if ( ! (0==AV8Perfil_Codigo) )
         {
            edtPerfil_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPerfil_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPerfil_Codigo_Enabled), 5, 0)));
         }
         else
         {
            edtPerfil_Codigo_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPerfil_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPerfil_Codigo_Enabled), 5, 0)));
         }
         if ( ! (0==AV8Perfil_Codigo) )
         {
            edtPerfil_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPerfil_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPerfil_Codigo_Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A543UsuarioPerfil_Display) && ( Gx_BScreen == 0 ) )
         {
            A543UsuarioPerfil_Display = true;
            n543UsuarioPerfil_Display = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A543UsuarioPerfil_Display", A543UsuarioPerfil_Display);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T00024 */
            pr_default.execute(2, new Object[] {A1Usuario_Codigo});
            A2Usuario_Nome = T00024_A2Usuario_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2Usuario_Nome", A2Usuario_Nome);
            n2Usuario_Nome = T00024_n2Usuario_Nome[0];
            A341Usuario_UserGamGuid = T00024_A341Usuario_UserGamGuid[0];
            A57Usuario_PessoaCod = T00024_A57Usuario_PessoaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A57Usuario_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A57Usuario_PessoaCod), 6, 0)));
            pr_default.close(2);
            /* Using cursor T00026 */
            pr_default.execute(4, new Object[] {A57Usuario_PessoaCod});
            A58Usuario_PessoaNom = T00026_A58Usuario_PessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A58Usuario_PessoaNom", A58Usuario_PessoaNom);
            n58Usuario_PessoaNom = T00026_n58Usuario_PessoaNom[0];
            pr_default.close(4);
            /* Using cursor T00025 */
            pr_default.execute(3, new Object[] {A3Perfil_Codigo});
            A4Perfil_Nome = T00025_A4Perfil_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4Perfil_Nome", A4Perfil_Nome);
            A275Perfil_Tipo = T00025_A275Perfil_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A275Perfil_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A275Perfil_Tipo), 4, 0)));
            A329Perfil_GamId = T00025_A329Perfil_GamId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A329Perfil_GamId", StringUtil.LTrim( StringUtil.Str( (decimal)(A329Perfil_GamId), 12, 0)));
            A276Perfil_Ativo = T00025_A276Perfil_Ativo[0];
            A7Perfil_AreaTrabalhoCod = T00025_A7Perfil_AreaTrabalhoCod[0];
            pr_default.close(3);
         }
      }

      protected void Load023( )
      {
         /* Using cursor T00027 */
         pr_default.execute(5, new Object[] {A1Usuario_Codigo, A3Perfil_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound3 = 1;
            A58Usuario_PessoaNom = T00027_A58Usuario_PessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A58Usuario_PessoaNom", A58Usuario_PessoaNom);
            n58Usuario_PessoaNom = T00027_n58Usuario_PessoaNom[0];
            A2Usuario_Nome = T00027_A2Usuario_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2Usuario_Nome", A2Usuario_Nome);
            n2Usuario_Nome = T00027_n2Usuario_Nome[0];
            A341Usuario_UserGamGuid = T00027_A341Usuario_UserGamGuid[0];
            A4Perfil_Nome = T00027_A4Perfil_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4Perfil_Nome", A4Perfil_Nome);
            A275Perfil_Tipo = T00027_A275Perfil_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A275Perfil_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A275Perfil_Tipo), 4, 0)));
            A329Perfil_GamId = T00027_A329Perfil_GamId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A329Perfil_GamId", StringUtil.LTrim( StringUtil.Str( (decimal)(A329Perfil_GamId), 12, 0)));
            A543UsuarioPerfil_Display = T00027_A543UsuarioPerfil_Display[0];
            n543UsuarioPerfil_Display = T00027_n543UsuarioPerfil_Display[0];
            A544UsuarioPerfil_Insert = T00027_A544UsuarioPerfil_Insert[0];
            n544UsuarioPerfil_Insert = T00027_n544UsuarioPerfil_Insert[0];
            A659UsuarioPerfil_Update = T00027_A659UsuarioPerfil_Update[0];
            n659UsuarioPerfil_Update = T00027_n659UsuarioPerfil_Update[0];
            A546UsuarioPerfil_Delete = T00027_A546UsuarioPerfil_Delete[0];
            n546UsuarioPerfil_Delete = T00027_n546UsuarioPerfil_Delete[0];
            A276Perfil_Ativo = T00027_A276Perfil_Ativo[0];
            A57Usuario_PessoaCod = T00027_A57Usuario_PessoaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A57Usuario_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A57Usuario_PessoaCod), 6, 0)));
            A7Perfil_AreaTrabalhoCod = T00027_A7Perfil_AreaTrabalhoCod[0];
            ZM023( -8) ;
         }
         pr_default.close(5);
         OnLoadActions023( ) ;
      }

      protected void OnLoadActions023( )
      {
      }

      protected void CheckExtendedTable023( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         /* Using cursor T00024 */
         pr_default.execute(2, new Object[] {A1Usuario_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Usuario'.", "ForeignKeyNotFound", 1, "USUARIO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtUsuario_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A2Usuario_Nome = T00024_A2Usuario_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2Usuario_Nome", A2Usuario_Nome);
         n2Usuario_Nome = T00024_n2Usuario_Nome[0];
         A341Usuario_UserGamGuid = T00024_A341Usuario_UserGamGuid[0];
         A57Usuario_PessoaCod = T00024_A57Usuario_PessoaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A57Usuario_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A57Usuario_PessoaCod), 6, 0)));
         pr_default.close(2);
         /* Using cursor T00026 */
         pr_default.execute(4, new Object[] {A57Usuario_PessoaCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Usuario_Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A58Usuario_PessoaNom = T00026_A58Usuario_PessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A58Usuario_PessoaNom", A58Usuario_PessoaNom);
         n58Usuario_PessoaNom = T00026_n58Usuario_PessoaNom[0];
         pr_default.close(4);
         /* Using cursor T00025 */
         pr_default.execute(3, new Object[] {A3Perfil_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Perfil'.", "ForeignKeyNotFound", 1, "PERFIL_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtPerfil_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A4Perfil_Nome = T00025_A4Perfil_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4Perfil_Nome", A4Perfil_Nome);
         A275Perfil_Tipo = T00025_A275Perfil_Tipo[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A275Perfil_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A275Perfil_Tipo), 4, 0)));
         A329Perfil_GamId = T00025_A329Perfil_GamId[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A329Perfil_GamId", StringUtil.LTrim( StringUtil.Str( (decimal)(A329Perfil_GamId), 12, 0)));
         A276Perfil_Ativo = T00025_A276Perfil_Ativo[0];
         A7Perfil_AreaTrabalhoCod = T00025_A7Perfil_AreaTrabalhoCod[0];
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors023( )
      {
         pr_default.close(2);
         pr_default.close(4);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_9( int A1Usuario_Codigo )
      {
         /* Using cursor T00028 */
         pr_default.execute(6, new Object[] {A1Usuario_Codigo});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Usuario'.", "ForeignKeyNotFound", 1, "USUARIO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtUsuario_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A2Usuario_Nome = T00028_A2Usuario_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2Usuario_Nome", A2Usuario_Nome);
         n2Usuario_Nome = T00028_n2Usuario_Nome[0];
         A341Usuario_UserGamGuid = T00028_A341Usuario_UserGamGuid[0];
         A57Usuario_PessoaCod = T00028_A57Usuario_PessoaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A57Usuario_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A57Usuario_PessoaCod), 6, 0)));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A2Usuario_Nome))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A341Usuario_UserGamGuid))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A57Usuario_PessoaCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void gxLoad_11( int A57Usuario_PessoaCod )
      {
         /* Using cursor T00029 */
         pr_default.execute(7, new Object[] {A57Usuario_PessoaCod});
         if ( (pr_default.getStatus(7) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Usuario_Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A58Usuario_PessoaNom = T00029_A58Usuario_PessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A58Usuario_PessoaNom", A58Usuario_PessoaNom);
         n58Usuario_PessoaNom = T00029_n58Usuario_PessoaNom[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A58Usuario_PessoaNom))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(7) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(7);
      }

      protected void gxLoad_10( int A3Perfil_Codigo )
      {
         /* Using cursor T000210 */
         pr_default.execute(8, new Object[] {A3Perfil_Codigo});
         if ( (pr_default.getStatus(8) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Perfil'.", "ForeignKeyNotFound", 1, "PERFIL_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtPerfil_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A4Perfil_Nome = T000210_A4Perfil_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4Perfil_Nome", A4Perfil_Nome);
         A275Perfil_Tipo = T000210_A275Perfil_Tipo[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A275Perfil_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A275Perfil_Tipo), 4, 0)));
         A329Perfil_GamId = T000210_A329Perfil_GamId[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A329Perfil_GamId", StringUtil.LTrim( StringUtil.Str( (decimal)(A329Perfil_GamId), 12, 0)));
         A276Perfil_Ativo = T000210_A276Perfil_Ativo[0];
         A7Perfil_AreaTrabalhoCod = T000210_A7Perfil_AreaTrabalhoCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A4Perfil_Nome))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A275Perfil_Tipo), 4, 0, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A329Perfil_GamId), 12, 0, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.BoolToStr( A276Perfil_Ativo))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A7Perfil_AreaTrabalhoCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(8) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(8);
      }

      protected void GetKey023( )
      {
         /* Using cursor T000211 */
         pr_default.execute(9, new Object[] {A1Usuario_Codigo, A3Perfil_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            RcdFound3 = 1;
         }
         else
         {
            RcdFound3 = 0;
         }
         pr_default.close(9);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00023 */
         pr_default.execute(1, new Object[] {A1Usuario_Codigo, A3Perfil_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM023( 8) ;
            RcdFound3 = 1;
            A543UsuarioPerfil_Display = T00023_A543UsuarioPerfil_Display[0];
            n543UsuarioPerfil_Display = T00023_n543UsuarioPerfil_Display[0];
            A544UsuarioPerfil_Insert = T00023_A544UsuarioPerfil_Insert[0];
            n544UsuarioPerfil_Insert = T00023_n544UsuarioPerfil_Insert[0];
            A659UsuarioPerfil_Update = T00023_A659UsuarioPerfil_Update[0];
            n659UsuarioPerfil_Update = T00023_n659UsuarioPerfil_Update[0];
            A546UsuarioPerfil_Delete = T00023_A546UsuarioPerfil_Delete[0];
            n546UsuarioPerfil_Delete = T00023_n546UsuarioPerfil_Delete[0];
            A1Usuario_Codigo = T00023_A1Usuario_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
            A3Perfil_Codigo = T00023_A3Perfil_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
            Z1Usuario_Codigo = A1Usuario_Codigo;
            Z3Perfil_Codigo = A3Perfil_Codigo;
            sMode3 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load023( ) ;
            if ( AnyError == 1 )
            {
               RcdFound3 = 0;
               InitializeNonKey023( ) ;
            }
            Gx_mode = sMode3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound3 = 0;
            InitializeNonKey023( ) ;
            sMode3 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey023( ) ;
         if ( RcdFound3 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound3 = 0;
         /* Using cursor T000212 */
         pr_default.execute(10, new Object[] {A1Usuario_Codigo, A3Perfil_Codigo});
         if ( (pr_default.getStatus(10) != 101) )
         {
            while ( (pr_default.getStatus(10) != 101) && ( ( T000212_A1Usuario_Codigo[0] < A1Usuario_Codigo ) || ( T000212_A1Usuario_Codigo[0] == A1Usuario_Codigo ) && ( T000212_A3Perfil_Codigo[0] < A3Perfil_Codigo ) ) )
            {
               pr_default.readNext(10);
            }
            if ( (pr_default.getStatus(10) != 101) && ( ( T000212_A1Usuario_Codigo[0] > A1Usuario_Codigo ) || ( T000212_A1Usuario_Codigo[0] == A1Usuario_Codigo ) && ( T000212_A3Perfil_Codigo[0] > A3Perfil_Codigo ) ) )
            {
               A1Usuario_Codigo = T000212_A1Usuario_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
               A3Perfil_Codigo = T000212_A3Perfil_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
               RcdFound3 = 1;
            }
         }
         pr_default.close(10);
      }

      protected void move_previous( )
      {
         RcdFound3 = 0;
         /* Using cursor T000213 */
         pr_default.execute(11, new Object[] {A1Usuario_Codigo, A3Perfil_Codigo});
         if ( (pr_default.getStatus(11) != 101) )
         {
            while ( (pr_default.getStatus(11) != 101) && ( ( T000213_A1Usuario_Codigo[0] > A1Usuario_Codigo ) || ( T000213_A1Usuario_Codigo[0] == A1Usuario_Codigo ) && ( T000213_A3Perfil_Codigo[0] > A3Perfil_Codigo ) ) )
            {
               pr_default.readNext(11);
            }
            if ( (pr_default.getStatus(11) != 101) && ( ( T000213_A1Usuario_Codigo[0] < A1Usuario_Codigo ) || ( T000213_A1Usuario_Codigo[0] == A1Usuario_Codigo ) && ( T000213_A3Perfil_Codigo[0] < A3Perfil_Codigo ) ) )
            {
               A1Usuario_Codigo = T000213_A1Usuario_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
               A3Perfil_Codigo = T000213_A3Perfil_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
               RcdFound3 = 1;
            }
         }
         pr_default.close(11);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey023( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtUsuario_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert023( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound3 == 1 )
            {
               if ( ( A1Usuario_Codigo != Z1Usuario_Codigo ) || ( A3Perfil_Codigo != Z3Perfil_Codigo ) )
               {
                  A1Usuario_Codigo = Z1Usuario_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
                  A3Perfil_Codigo = Z3Perfil_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "USUARIO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtUsuario_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtUsuario_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update023( ) ;
                  GX_FocusControl = edtUsuario_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A1Usuario_Codigo != Z1Usuario_Codigo ) || ( A3Perfil_Codigo != Z3Perfil_Codigo ) )
               {
                  /* Insert record */
                  GX_FocusControl = edtUsuario_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert023( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "USUARIO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtUsuario_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtUsuario_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert023( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( ( A1Usuario_Codigo != Z1Usuario_Codigo ) || ( A3Perfil_Codigo != Z3Perfil_Codigo ) )
         {
            A1Usuario_Codigo = Z1Usuario_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
            A3Perfil_Codigo = Z3Perfil_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "USUARIO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtUsuario_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtUsuario_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency023( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00022 */
            pr_default.execute(0, new Object[] {A1Usuario_Codigo, A3Perfil_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"UsuarioPerfil"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z543UsuarioPerfil_Display != T00022_A543UsuarioPerfil_Display[0] ) || ( Z544UsuarioPerfil_Insert != T00022_A544UsuarioPerfil_Insert[0] ) || ( Z659UsuarioPerfil_Update != T00022_A659UsuarioPerfil_Update[0] ) || ( Z546UsuarioPerfil_Delete != T00022_A546UsuarioPerfil_Delete[0] ) )
            {
               if ( Z543UsuarioPerfil_Display != T00022_A543UsuarioPerfil_Display[0] )
               {
                  GXUtil.WriteLog("usuarioperfil:[seudo value changed for attri]"+"UsuarioPerfil_Display");
                  GXUtil.WriteLogRaw("Old: ",Z543UsuarioPerfil_Display);
                  GXUtil.WriteLogRaw("Current: ",T00022_A543UsuarioPerfil_Display[0]);
               }
               if ( Z544UsuarioPerfil_Insert != T00022_A544UsuarioPerfil_Insert[0] )
               {
                  GXUtil.WriteLog("usuarioperfil:[seudo value changed for attri]"+"UsuarioPerfil_Insert");
                  GXUtil.WriteLogRaw("Old: ",Z544UsuarioPerfil_Insert);
                  GXUtil.WriteLogRaw("Current: ",T00022_A544UsuarioPerfil_Insert[0]);
               }
               if ( Z659UsuarioPerfil_Update != T00022_A659UsuarioPerfil_Update[0] )
               {
                  GXUtil.WriteLog("usuarioperfil:[seudo value changed for attri]"+"UsuarioPerfil_Update");
                  GXUtil.WriteLogRaw("Old: ",Z659UsuarioPerfil_Update);
                  GXUtil.WriteLogRaw("Current: ",T00022_A659UsuarioPerfil_Update[0]);
               }
               if ( Z546UsuarioPerfil_Delete != T00022_A546UsuarioPerfil_Delete[0] )
               {
                  GXUtil.WriteLog("usuarioperfil:[seudo value changed for attri]"+"UsuarioPerfil_Delete");
                  GXUtil.WriteLogRaw("Old: ",Z546UsuarioPerfil_Delete);
                  GXUtil.WriteLogRaw("Current: ",T00022_A546UsuarioPerfil_Delete[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"UsuarioPerfil"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert023( )
      {
         BeforeValidate023( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable023( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM023( 0) ;
            CheckOptimisticConcurrency023( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm023( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert023( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000214 */
                     pr_default.execute(12, new Object[] {n543UsuarioPerfil_Display, A543UsuarioPerfil_Display, n544UsuarioPerfil_Insert, A544UsuarioPerfil_Insert, n659UsuarioPerfil_Update, A659UsuarioPerfil_Update, n546UsuarioPerfil_Delete, A546UsuarioPerfil_Delete, A1Usuario_Codigo, A3Perfil_Codigo});
                     pr_default.close(12);
                     dsDefault.SmartCacheProvider.SetUpdated("UsuarioPerfil") ;
                     if ( (pr_default.getStatus(12) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption020( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load023( ) ;
            }
            EndLevel023( ) ;
         }
         CloseExtendedTableCursors023( ) ;
      }

      protected void Update023( )
      {
         BeforeValidate023( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable023( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency023( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm023( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate023( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000215 */
                     pr_default.execute(13, new Object[] {n543UsuarioPerfil_Display, A543UsuarioPerfil_Display, n544UsuarioPerfil_Insert, A544UsuarioPerfil_Insert, n659UsuarioPerfil_Update, A659UsuarioPerfil_Update, n546UsuarioPerfil_Delete, A546UsuarioPerfil_Delete, A1Usuario_Codigo, A3Perfil_Codigo});
                     pr_default.close(13);
                     dsDefault.SmartCacheProvider.SetUpdated("UsuarioPerfil") ;
                     if ( (pr_default.getStatus(13) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"UsuarioPerfil"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate023( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel023( ) ;
         }
         CloseExtendedTableCursors023( ) ;
      }

      protected void DeferredUpdate023( )
      {
      }

      protected void delete( )
      {
         BeforeValidate023( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency023( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls023( ) ;
            AfterConfirm023( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete023( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000216 */
                  pr_default.execute(14, new Object[] {A1Usuario_Codigo, A3Perfil_Codigo});
                  pr_default.close(14);
                  dsDefault.SmartCacheProvider.SetUpdated("UsuarioPerfil") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode3 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel023( ) ;
         Gx_mode = sMode3;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls023( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T000217 */
            pr_default.execute(15, new Object[] {A1Usuario_Codigo});
            A2Usuario_Nome = T000217_A2Usuario_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2Usuario_Nome", A2Usuario_Nome);
            n2Usuario_Nome = T000217_n2Usuario_Nome[0];
            A341Usuario_UserGamGuid = T000217_A341Usuario_UserGamGuid[0];
            A57Usuario_PessoaCod = T000217_A57Usuario_PessoaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A57Usuario_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A57Usuario_PessoaCod), 6, 0)));
            pr_default.close(15);
            /* Using cursor T000218 */
            pr_default.execute(16, new Object[] {A57Usuario_PessoaCod});
            A58Usuario_PessoaNom = T000218_A58Usuario_PessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A58Usuario_PessoaNom", A58Usuario_PessoaNom);
            n58Usuario_PessoaNom = T000218_n58Usuario_PessoaNom[0];
            pr_default.close(16);
            /* Using cursor T000219 */
            pr_default.execute(17, new Object[] {A3Perfil_Codigo});
            A4Perfil_Nome = T000219_A4Perfil_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4Perfil_Nome", A4Perfil_Nome);
            A275Perfil_Tipo = T000219_A275Perfil_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A275Perfil_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A275Perfil_Tipo), 4, 0)));
            A329Perfil_GamId = T000219_A329Perfil_GamId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A329Perfil_GamId", StringUtil.LTrim( StringUtil.Str( (decimal)(A329Perfil_GamId), 12, 0)));
            A276Perfil_Ativo = T000219_A276Perfil_Ativo[0];
            A7Perfil_AreaTrabalhoCod = T000219_A7Perfil_AreaTrabalhoCod[0];
            pr_default.close(17);
         }
      }

      protected void EndLevel023( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete023( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(15);
            pr_default.close(17);
            pr_default.close(16);
            context.CommitDataStores( "UsuarioPerfil");
            if ( AnyError == 0 )
            {
               ConfirmValues020( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(15);
            pr_default.close(17);
            pr_default.close(16);
            context.RollbackDataStores( "UsuarioPerfil");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart023( )
      {
         /* Scan By routine */
         /* Using cursor T000220 */
         pr_default.execute(18);
         RcdFound3 = 0;
         if ( (pr_default.getStatus(18) != 101) )
         {
            RcdFound3 = 1;
            A1Usuario_Codigo = T000220_A1Usuario_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
            A3Perfil_Codigo = T000220_A3Perfil_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext023( )
      {
         /* Scan next routine */
         pr_default.readNext(18);
         RcdFound3 = 0;
         if ( (pr_default.getStatus(18) != 101) )
         {
            RcdFound3 = 1;
            A1Usuario_Codigo = T000220_A1Usuario_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
            A3Perfil_Codigo = T000220_A3Perfil_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd023( )
      {
         pr_default.close(18);
      }

      protected void AfterConfirm023( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert023( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate023( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete023( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete023( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate023( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes023( )
      {
         edtUsuario_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_Nome_Enabled), 5, 0)));
         edtUsuario_PessoaNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_PessoaNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_PessoaNom_Enabled), 5, 0)));
         edtPerfil_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPerfil_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPerfil_Nome_Enabled), 5, 0)));
         cmbPerfil_Tipo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbPerfil_Tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbPerfil_Tipo.Enabled), 5, 0)));
         edtPerfil_GamId_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPerfil_GamId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPerfil_GamId_Enabled), 5, 0)));
         edtUsuario_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_Codigo_Enabled), 5, 0)));
         edtUsuario_PessoaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_PessoaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_PessoaCod_Enabled), 5, 0)));
         edtPerfil_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPerfil_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPerfil_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues020( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311714043");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("usuarioperfil.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Usuario_Codigo) + "," + UrlEncode("" +AV8Perfil_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1Usuario_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1Usuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z3Perfil_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z3Perfil_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z543UsuarioPerfil_Display", Z543UsuarioPerfil_Display);
         GxWebStd.gx_boolean_hidden_field( context, "Z544UsuarioPerfil_Insert", Z544UsuarioPerfil_Insert);
         GxWebStd.gx_boolean_hidden_field( context, "Z659UsuarioPerfil_Update", Z659UsuarioPerfil_Update);
         GxWebStd.gx_boolean_hidden_field( context, "Z546UsuarioPerfil_Delete", Z546UsuarioPerfil_Delete);
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV10TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV10TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vUSUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Usuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPERFIL_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8Perfil_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "USUARIOPERFIL_DISPLAY", A543UsuarioPerfil_Display);
         GxWebStd.gx_boolean_hidden_field( context, "USUARIOPERFIL_INSERT", A544UsuarioPerfil_Insert);
         GxWebStd.gx_boolean_hidden_field( context, "USUARIOPERFIL_UPDATE", A659UsuarioPerfil_Update);
         GxWebStd.gx_boolean_hidden_field( context, "USUARIOPERFIL_DELETE", A546UsuarioPerfil_Delete);
         GxWebStd.gx_hidden_field( context, "USUARIO_USERGAMGUID", StringUtil.RTrim( A341Usuario_UserGamGuid));
         GxWebStd.gx_boolean_hidden_field( context, "PERFIL_ATIVO", A276Perfil_Ativo);
         GxWebStd.gx_hidden_field( context, "PERFIL_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A7Perfil_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vUSUARIO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Usuario_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPERFIL_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8Perfil_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "UsuarioPerfil";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A543UsuarioPerfil_Display);
         forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A544UsuarioPerfil_Insert);
         forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A659UsuarioPerfil_Update);
         forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A546UsuarioPerfil_Delete);
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("usuarioperfil:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("usuarioperfil:[SendSecurityCheck value for]"+"UsuarioPerfil_Display:"+StringUtil.BoolToStr( A543UsuarioPerfil_Display));
         GXUtil.WriteLog("usuarioperfil:[SendSecurityCheck value for]"+"UsuarioPerfil_Insert:"+StringUtil.BoolToStr( A544UsuarioPerfil_Insert));
         GXUtil.WriteLog("usuarioperfil:[SendSecurityCheck value for]"+"UsuarioPerfil_Update:"+StringUtil.BoolToStr( A659UsuarioPerfil_Update));
         GXUtil.WriteLog("usuarioperfil:[SendSecurityCheck value for]"+"UsuarioPerfil_Delete:"+StringUtil.BoolToStr( A546UsuarioPerfil_Delete));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("usuarioperfil.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Usuario_Codigo) + "," + UrlEncode("" +AV8Perfil_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "UsuarioPerfil" ;
      }

      public override String GetPgmdesc( )
      {
         return "Usuario x Perfil" ;
      }

      protected void InitializeNonKey023( )
      {
         A57Usuario_PessoaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A57Usuario_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A57Usuario_PessoaCod), 6, 0)));
         A58Usuario_PessoaNom = "";
         n58Usuario_PessoaNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A58Usuario_PessoaNom", A58Usuario_PessoaNom);
         A2Usuario_Nome = "";
         n2Usuario_Nome = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2Usuario_Nome", A2Usuario_Nome);
         A341Usuario_UserGamGuid = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A341Usuario_UserGamGuid", A341Usuario_UserGamGuid);
         A7Perfil_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A7Perfil_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A7Perfil_AreaTrabalhoCod), 6, 0)));
         A4Perfil_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4Perfil_Nome", A4Perfil_Nome);
         A275Perfil_Tipo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A275Perfil_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A275Perfil_Tipo), 4, 0)));
         A329Perfil_GamId = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A329Perfil_GamId", StringUtil.LTrim( StringUtil.Str( (decimal)(A329Perfil_GamId), 12, 0)));
         A544UsuarioPerfil_Insert = false;
         n544UsuarioPerfil_Insert = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A544UsuarioPerfil_Insert", A544UsuarioPerfil_Insert);
         A659UsuarioPerfil_Update = false;
         n659UsuarioPerfil_Update = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A659UsuarioPerfil_Update", A659UsuarioPerfil_Update);
         A546UsuarioPerfil_Delete = false;
         n546UsuarioPerfil_Delete = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A546UsuarioPerfil_Delete", A546UsuarioPerfil_Delete);
         A276Perfil_Ativo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A276Perfil_Ativo", A276Perfil_Ativo);
         A543UsuarioPerfil_Display = true;
         n543UsuarioPerfil_Display = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A543UsuarioPerfil_Display", A543UsuarioPerfil_Display);
         Z543UsuarioPerfil_Display = false;
         Z544UsuarioPerfil_Insert = false;
         Z659UsuarioPerfil_Update = false;
         Z546UsuarioPerfil_Delete = false;
      }

      protected void InitAll023( )
      {
         A1Usuario_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
         A3Perfil_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
         InitializeNonKey023( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A543UsuarioPerfil_Display = i543UsuarioPerfil_Display;
         n543UsuarioPerfil_Display = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A543UsuarioPerfil_Display", A543UsuarioPerfil_Display);
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311714067");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("usuarioperfil.js", "?2020311714067");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockusuario_nome_Internalname = "TEXTBLOCKUSUARIO_NOME";
         edtUsuario_Nome_Internalname = "USUARIO_NOME";
         lblTextblockusuario_pessoanom_Internalname = "TEXTBLOCKUSUARIO_PESSOANOM";
         edtUsuario_PessoaNom_Internalname = "USUARIO_PESSOANOM";
         lblTextblockperfil_nome_Internalname = "TEXTBLOCKPERFIL_NOME";
         edtPerfil_Nome_Internalname = "PERFIL_NOME";
         lblTextblockperfil_tipo_Internalname = "TEXTBLOCKPERFIL_TIPO";
         cmbPerfil_Tipo_Internalname = "PERFIL_TIPO";
         lblTextblockperfil_gamid_Internalname = "TEXTBLOCKPERFIL_GAMID";
         edtPerfil_GamId_Internalname = "PERFIL_GAMID";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtUsuario_Codigo_Internalname = "USUARIO_CODIGO";
         edtUsuario_PessoaCod_Internalname = "USUARIO_PESSOACOD";
         edtPerfil_Codigo_Internalname = "PERFIL_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Usuario x Perfil";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Usuario x Perfil";
         edtPerfil_GamId_Jsonclick = "";
         edtPerfil_GamId_Enabled = 0;
         cmbPerfil_Tipo_Jsonclick = "";
         cmbPerfil_Tipo.Enabled = 0;
         edtPerfil_Nome_Jsonclick = "";
         edtPerfil_Nome_Enabled = 0;
         edtUsuario_PessoaNom_Jsonclick = "";
         edtUsuario_PessoaNom_Enabled = 0;
         edtUsuario_Nome_Jsonclick = "";
         edtUsuario_Nome_Enabled = 0;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtPerfil_Codigo_Jsonclick = "";
         edtPerfil_Codigo_Enabled = 1;
         edtPerfil_Codigo_Visible = 1;
         edtUsuario_PessoaCod_Jsonclick = "";
         edtUsuario_PessoaCod_Enabled = 0;
         edtUsuario_PessoaCod_Visible = 1;
         edtUsuario_Codigo_Jsonclick = "";
         edtUsuario_Codigo_Enabled = 1;
         edtUsuario_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public void Valid_Usuario_codigo( int GX_Parm1 ,
                                        int GX_Parm2 ,
                                        String GX_Parm3 ,
                                        String GX_Parm4 ,
                                        String GX_Parm5 )
      {
         A1Usuario_Codigo = GX_Parm1;
         A57Usuario_PessoaCod = GX_Parm2;
         A2Usuario_Nome = GX_Parm3;
         n2Usuario_Nome = false;
         A341Usuario_UserGamGuid = GX_Parm4;
         A58Usuario_PessoaNom = GX_Parm5;
         n58Usuario_PessoaNom = false;
         /* Using cursor T000217 */
         pr_default.execute(15, new Object[] {A1Usuario_Codigo});
         if ( (pr_default.getStatus(15) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Usuario'.", "ForeignKeyNotFound", 1, "USUARIO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtUsuario_Codigo_Internalname;
         }
         A2Usuario_Nome = T000217_A2Usuario_Nome[0];
         n2Usuario_Nome = T000217_n2Usuario_Nome[0];
         A341Usuario_UserGamGuid = T000217_A341Usuario_UserGamGuid[0];
         A57Usuario_PessoaCod = T000217_A57Usuario_PessoaCod[0];
         pr_default.close(15);
         /* Using cursor T000218 */
         pr_default.execute(16, new Object[] {A57Usuario_PessoaCod});
         if ( (pr_default.getStatus(16) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Usuario_Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A58Usuario_PessoaNom = T000218_A58Usuario_PessoaNom[0];
         n58Usuario_PessoaNom = T000218_n58Usuario_PessoaNom[0];
         pr_default.close(16);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A2Usuario_Nome = "";
            n2Usuario_Nome = false;
            A341Usuario_UserGamGuid = "";
            A57Usuario_PessoaCod = 0;
            A58Usuario_PessoaNom = "";
            n58Usuario_PessoaNom = false;
         }
         isValidOutput.Add(StringUtil.RTrim( A2Usuario_Nome));
         isValidOutput.Add(StringUtil.RTrim( A341Usuario_UserGamGuid));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A57Usuario_PessoaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A58Usuario_PessoaNom));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Perfil_codigo( int GX_Parm1 ,
                                       String GX_Parm2 ,
                                       GXCombobox cmbGX_Parm3 ,
                                       long GX_Parm4 ,
                                       bool GX_Parm5 ,
                                       int GX_Parm6 )
      {
         A3Perfil_Codigo = GX_Parm1;
         A4Perfil_Nome = GX_Parm2;
         cmbPerfil_Tipo = cmbGX_Parm3;
         A275Perfil_Tipo = (short)(NumberUtil.Val( cmbPerfil_Tipo.CurrentValue, "."));
         cmbPerfil_Tipo.CurrentValue = StringUtil.LTrim( StringUtil.Str( (decimal)(A275Perfil_Tipo), 4, 0));
         A329Perfil_GamId = GX_Parm4;
         A276Perfil_Ativo = GX_Parm5;
         A7Perfil_AreaTrabalhoCod = GX_Parm6;
         /* Using cursor T000219 */
         pr_default.execute(17, new Object[] {A3Perfil_Codigo});
         if ( (pr_default.getStatus(17) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Perfil'.", "ForeignKeyNotFound", 1, "PERFIL_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtPerfil_Codigo_Internalname;
         }
         A4Perfil_Nome = T000219_A4Perfil_Nome[0];
         A275Perfil_Tipo = T000219_A275Perfil_Tipo[0];
         cmbPerfil_Tipo.CurrentValue = StringUtil.LTrim( StringUtil.Str( (decimal)(A275Perfil_Tipo), 4, 0));
         A329Perfil_GamId = T000219_A329Perfil_GamId[0];
         A276Perfil_Ativo = T000219_A276Perfil_Ativo[0];
         A7Perfil_AreaTrabalhoCod = T000219_A7Perfil_AreaTrabalhoCod[0];
         pr_default.close(17);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A4Perfil_Nome = "";
            A275Perfil_Tipo = 0;
            cmbPerfil_Tipo.CurrentValue = StringUtil.LTrim( StringUtil.Str( (decimal)(A275Perfil_Tipo), 4, 0));
            A329Perfil_GamId = 0;
            A276Perfil_Ativo = false;
            A7Perfil_AreaTrabalhoCod = 0;
         }
         isValidOutput.Add(StringUtil.RTrim( A4Perfil_Nome));
         cmbPerfil_Tipo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A275Perfil_Tipo), 4, 0));
         isValidOutput.Add(cmbPerfil_Tipo);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A329Perfil_GamId), 12, 0, ".", "")));
         isValidOutput.Add(A276Perfil_Ativo);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A7Perfil_AreaTrabalhoCod), 6, 0, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV8Perfil_Codigo',fld:'vPERFIL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E12022',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV10TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(15);
         pr_default.close(17);
         pr_default.close(16);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         TempTags = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         lblTextblockusuario_nome_Jsonclick = "";
         A2Usuario_Nome = "";
         lblTextblockusuario_pessoanom_Jsonclick = "";
         A58Usuario_PessoaNom = "";
         lblTextblockperfil_nome_Jsonclick = "";
         A4Perfil_Nome = "";
         lblTextblockperfil_tipo_Jsonclick = "";
         lblTextblockperfil_gamid_Jsonclick = "";
         A341Usuario_UserGamGuid = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode3 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV10TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11WebSession = context.GetSession();
         Z2Usuario_Nome = "";
         Z341Usuario_UserGamGuid = "";
         Z58Usuario_PessoaNom = "";
         Z4Perfil_Nome = "";
         T00024_A2Usuario_Nome = new String[] {""} ;
         T00024_n2Usuario_Nome = new bool[] {false} ;
         T00024_A341Usuario_UserGamGuid = new String[] {""} ;
         T00024_A57Usuario_PessoaCod = new int[1] ;
         T00026_A58Usuario_PessoaNom = new String[] {""} ;
         T00026_n58Usuario_PessoaNom = new bool[] {false} ;
         T00025_A4Perfil_Nome = new String[] {""} ;
         T00025_A275Perfil_Tipo = new short[1] ;
         T00025_A329Perfil_GamId = new long[1] ;
         T00025_A276Perfil_Ativo = new bool[] {false} ;
         T00025_A7Perfil_AreaTrabalhoCod = new int[1] ;
         T00027_A58Usuario_PessoaNom = new String[] {""} ;
         T00027_n58Usuario_PessoaNom = new bool[] {false} ;
         T00027_A2Usuario_Nome = new String[] {""} ;
         T00027_n2Usuario_Nome = new bool[] {false} ;
         T00027_A341Usuario_UserGamGuid = new String[] {""} ;
         T00027_A4Perfil_Nome = new String[] {""} ;
         T00027_A275Perfil_Tipo = new short[1] ;
         T00027_A329Perfil_GamId = new long[1] ;
         T00027_A543UsuarioPerfil_Display = new bool[] {false} ;
         T00027_n543UsuarioPerfil_Display = new bool[] {false} ;
         T00027_A544UsuarioPerfil_Insert = new bool[] {false} ;
         T00027_n544UsuarioPerfil_Insert = new bool[] {false} ;
         T00027_A659UsuarioPerfil_Update = new bool[] {false} ;
         T00027_n659UsuarioPerfil_Update = new bool[] {false} ;
         T00027_A546UsuarioPerfil_Delete = new bool[] {false} ;
         T00027_n546UsuarioPerfil_Delete = new bool[] {false} ;
         T00027_A276Perfil_Ativo = new bool[] {false} ;
         T00027_A1Usuario_Codigo = new int[1] ;
         T00027_A3Perfil_Codigo = new int[1] ;
         T00027_A57Usuario_PessoaCod = new int[1] ;
         T00027_A7Perfil_AreaTrabalhoCod = new int[1] ;
         T00028_A2Usuario_Nome = new String[] {""} ;
         T00028_n2Usuario_Nome = new bool[] {false} ;
         T00028_A341Usuario_UserGamGuid = new String[] {""} ;
         T00028_A57Usuario_PessoaCod = new int[1] ;
         T00029_A58Usuario_PessoaNom = new String[] {""} ;
         T00029_n58Usuario_PessoaNom = new bool[] {false} ;
         T000210_A4Perfil_Nome = new String[] {""} ;
         T000210_A275Perfil_Tipo = new short[1] ;
         T000210_A329Perfil_GamId = new long[1] ;
         T000210_A276Perfil_Ativo = new bool[] {false} ;
         T000210_A7Perfil_AreaTrabalhoCod = new int[1] ;
         T000211_A1Usuario_Codigo = new int[1] ;
         T000211_A3Perfil_Codigo = new int[1] ;
         T00023_A543UsuarioPerfil_Display = new bool[] {false} ;
         T00023_n543UsuarioPerfil_Display = new bool[] {false} ;
         T00023_A544UsuarioPerfil_Insert = new bool[] {false} ;
         T00023_n544UsuarioPerfil_Insert = new bool[] {false} ;
         T00023_A659UsuarioPerfil_Update = new bool[] {false} ;
         T00023_n659UsuarioPerfil_Update = new bool[] {false} ;
         T00023_A546UsuarioPerfil_Delete = new bool[] {false} ;
         T00023_n546UsuarioPerfil_Delete = new bool[] {false} ;
         T00023_A1Usuario_Codigo = new int[1] ;
         T00023_A3Perfil_Codigo = new int[1] ;
         T000212_A1Usuario_Codigo = new int[1] ;
         T000212_A3Perfil_Codigo = new int[1] ;
         T000213_A1Usuario_Codigo = new int[1] ;
         T000213_A3Perfil_Codigo = new int[1] ;
         T00022_A543UsuarioPerfil_Display = new bool[] {false} ;
         T00022_n543UsuarioPerfil_Display = new bool[] {false} ;
         T00022_A544UsuarioPerfil_Insert = new bool[] {false} ;
         T00022_n544UsuarioPerfil_Insert = new bool[] {false} ;
         T00022_A659UsuarioPerfil_Update = new bool[] {false} ;
         T00022_n659UsuarioPerfil_Update = new bool[] {false} ;
         T00022_A546UsuarioPerfil_Delete = new bool[] {false} ;
         T00022_n546UsuarioPerfil_Delete = new bool[] {false} ;
         T00022_A1Usuario_Codigo = new int[1] ;
         T00022_A3Perfil_Codigo = new int[1] ;
         T000217_A2Usuario_Nome = new String[] {""} ;
         T000217_n2Usuario_Nome = new bool[] {false} ;
         T000217_A341Usuario_UserGamGuid = new String[] {""} ;
         T000217_A57Usuario_PessoaCod = new int[1] ;
         T000218_A58Usuario_PessoaNom = new String[] {""} ;
         T000218_n58Usuario_PessoaNom = new bool[] {false} ;
         T000219_A4Perfil_Nome = new String[] {""} ;
         T000219_A275Perfil_Tipo = new short[1] ;
         T000219_A329Perfil_GamId = new long[1] ;
         T000219_A276Perfil_Ativo = new bool[] {false} ;
         T000219_A7Perfil_AreaTrabalhoCod = new int[1] ;
         T000220_A1Usuario_Codigo = new int[1] ;
         T000220_A3Perfil_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.usuarioperfil__default(),
            new Object[][] {
                new Object[] {
               T00022_A543UsuarioPerfil_Display, T00022_n543UsuarioPerfil_Display, T00022_A544UsuarioPerfil_Insert, T00022_n544UsuarioPerfil_Insert, T00022_A659UsuarioPerfil_Update, T00022_n659UsuarioPerfil_Update, T00022_A546UsuarioPerfil_Delete, T00022_n546UsuarioPerfil_Delete, T00022_A1Usuario_Codigo, T00022_A3Perfil_Codigo
               }
               , new Object[] {
               T00023_A543UsuarioPerfil_Display, T00023_n543UsuarioPerfil_Display, T00023_A544UsuarioPerfil_Insert, T00023_n544UsuarioPerfil_Insert, T00023_A659UsuarioPerfil_Update, T00023_n659UsuarioPerfil_Update, T00023_A546UsuarioPerfil_Delete, T00023_n546UsuarioPerfil_Delete, T00023_A1Usuario_Codigo, T00023_A3Perfil_Codigo
               }
               , new Object[] {
               T00024_A2Usuario_Nome, T00024_n2Usuario_Nome, T00024_A341Usuario_UserGamGuid, T00024_A57Usuario_PessoaCod
               }
               , new Object[] {
               T00025_A4Perfil_Nome, T00025_A275Perfil_Tipo, T00025_A329Perfil_GamId, T00025_A276Perfil_Ativo, T00025_A7Perfil_AreaTrabalhoCod
               }
               , new Object[] {
               T00026_A58Usuario_PessoaNom, T00026_n58Usuario_PessoaNom
               }
               , new Object[] {
               T00027_A58Usuario_PessoaNom, T00027_n58Usuario_PessoaNom, T00027_A2Usuario_Nome, T00027_n2Usuario_Nome, T00027_A341Usuario_UserGamGuid, T00027_A4Perfil_Nome, T00027_A275Perfil_Tipo, T00027_A329Perfil_GamId, T00027_A543UsuarioPerfil_Display, T00027_n543UsuarioPerfil_Display,
               T00027_A544UsuarioPerfil_Insert, T00027_n544UsuarioPerfil_Insert, T00027_A659UsuarioPerfil_Update, T00027_n659UsuarioPerfil_Update, T00027_A546UsuarioPerfil_Delete, T00027_n546UsuarioPerfil_Delete, T00027_A276Perfil_Ativo, T00027_A1Usuario_Codigo, T00027_A3Perfil_Codigo, T00027_A57Usuario_PessoaCod,
               T00027_A7Perfil_AreaTrabalhoCod
               }
               , new Object[] {
               T00028_A2Usuario_Nome, T00028_n2Usuario_Nome, T00028_A341Usuario_UserGamGuid, T00028_A57Usuario_PessoaCod
               }
               , new Object[] {
               T00029_A58Usuario_PessoaNom, T00029_n58Usuario_PessoaNom
               }
               , new Object[] {
               T000210_A4Perfil_Nome, T000210_A275Perfil_Tipo, T000210_A329Perfil_GamId, T000210_A276Perfil_Ativo, T000210_A7Perfil_AreaTrabalhoCod
               }
               , new Object[] {
               T000211_A1Usuario_Codigo, T000211_A3Perfil_Codigo
               }
               , new Object[] {
               T000212_A1Usuario_Codigo, T000212_A3Perfil_Codigo
               }
               , new Object[] {
               T000213_A1Usuario_Codigo, T000213_A3Perfil_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000217_A2Usuario_Nome, T000217_n2Usuario_Nome, T000217_A341Usuario_UserGamGuid, T000217_A57Usuario_PessoaCod
               }
               , new Object[] {
               T000218_A58Usuario_PessoaNom, T000218_n58Usuario_PessoaNom
               }
               , new Object[] {
               T000219_A4Perfil_Nome, T000219_A275Perfil_Tipo, T000219_A329Perfil_GamId, T000219_A276Perfil_Ativo, T000219_A7Perfil_AreaTrabalhoCod
               }
               , new Object[] {
               T000220_A1Usuario_Codigo, T000220_A3Perfil_Codigo
               }
            }
         );
         Z543UsuarioPerfil_Display = true;
         n543UsuarioPerfil_Display = false;
         A543UsuarioPerfil_Display = true;
         n543UsuarioPerfil_Display = false;
         i543UsuarioPerfil_Display = true;
         n543UsuarioPerfil_Display = false;
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short A275Perfil_Tipo ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short Gx_BScreen ;
      private short RcdFound3 ;
      private short GX_JID ;
      private short Z275Perfil_Tipo ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7Usuario_Codigo ;
      private int wcpOAV8Perfil_Codigo ;
      private int Z1Usuario_Codigo ;
      private int Z3Perfil_Codigo ;
      private int A1Usuario_Codigo ;
      private int A57Usuario_PessoaCod ;
      private int A3Perfil_Codigo ;
      private int AV7Usuario_Codigo ;
      private int AV8Perfil_Codigo ;
      private int trnEnded ;
      private int edtUsuario_Codigo_Visible ;
      private int edtUsuario_Codigo_Enabled ;
      private int edtUsuario_PessoaCod_Enabled ;
      private int edtUsuario_PessoaCod_Visible ;
      private int edtPerfil_Codigo_Visible ;
      private int edtPerfil_Codigo_Enabled ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int edtUsuario_Nome_Enabled ;
      private int edtUsuario_PessoaNom_Enabled ;
      private int edtPerfil_Nome_Enabled ;
      private int edtPerfil_GamId_Enabled ;
      private int A7Perfil_AreaTrabalhoCod ;
      private int Z57Usuario_PessoaCod ;
      private int Z7Perfil_AreaTrabalhoCod ;
      private int idxLst ;
      private long A329Perfil_GamId ;
      private long Z329Perfil_GamId ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtUsuario_Codigo_Internalname ;
      private String TempTags ;
      private String edtUsuario_Codigo_Jsonclick ;
      private String edtUsuario_PessoaCod_Internalname ;
      private String edtUsuario_PessoaCod_Jsonclick ;
      private String edtPerfil_Codigo_Internalname ;
      private String edtPerfil_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockusuario_nome_Internalname ;
      private String lblTextblockusuario_nome_Jsonclick ;
      private String edtUsuario_Nome_Internalname ;
      private String A2Usuario_Nome ;
      private String edtUsuario_Nome_Jsonclick ;
      private String lblTextblockusuario_pessoanom_Internalname ;
      private String lblTextblockusuario_pessoanom_Jsonclick ;
      private String edtUsuario_PessoaNom_Internalname ;
      private String A58Usuario_PessoaNom ;
      private String edtUsuario_PessoaNom_Jsonclick ;
      private String lblTextblockperfil_nome_Internalname ;
      private String lblTextblockperfil_nome_Jsonclick ;
      private String edtPerfil_Nome_Internalname ;
      private String A4Perfil_Nome ;
      private String edtPerfil_Nome_Jsonclick ;
      private String lblTextblockperfil_tipo_Internalname ;
      private String lblTextblockperfil_tipo_Jsonclick ;
      private String cmbPerfil_Tipo_Internalname ;
      private String cmbPerfil_Tipo_Jsonclick ;
      private String lblTextblockperfil_gamid_Internalname ;
      private String lblTextblockperfil_gamid_Jsonclick ;
      private String edtPerfil_GamId_Internalname ;
      private String edtPerfil_GamId_Jsonclick ;
      private String A341Usuario_UserGamGuid ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode3 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z2Usuario_Nome ;
      private String Z341Usuario_UserGamGuid ;
      private String Z58Usuario_PessoaNom ;
      private String Z4Perfil_Nome ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private bool Z543UsuarioPerfil_Display ;
      private bool Z544UsuarioPerfil_Insert ;
      private bool Z659UsuarioPerfil_Update ;
      private bool Z546UsuarioPerfil_Delete ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n2Usuario_Nome ;
      private bool n58Usuario_PessoaNom ;
      private bool n543UsuarioPerfil_Display ;
      private bool A543UsuarioPerfil_Display ;
      private bool n544UsuarioPerfil_Insert ;
      private bool A544UsuarioPerfil_Insert ;
      private bool n659UsuarioPerfil_Update ;
      private bool A659UsuarioPerfil_Update ;
      private bool n546UsuarioPerfil_Delete ;
      private bool A546UsuarioPerfil_Delete ;
      private bool A276Perfil_Ativo ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool Z276Perfil_Ativo ;
      private bool i543UsuarioPerfil_Display ;
      private IGxSession AV11WebSession ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbPerfil_Tipo ;
      private IDataStoreProvider pr_default ;
      private String[] T00024_A2Usuario_Nome ;
      private bool[] T00024_n2Usuario_Nome ;
      private String[] T00024_A341Usuario_UserGamGuid ;
      private int[] T00024_A57Usuario_PessoaCod ;
      private String[] T00026_A58Usuario_PessoaNom ;
      private bool[] T00026_n58Usuario_PessoaNom ;
      private String[] T00025_A4Perfil_Nome ;
      private short[] T00025_A275Perfil_Tipo ;
      private long[] T00025_A329Perfil_GamId ;
      private bool[] T00025_A276Perfil_Ativo ;
      private int[] T00025_A7Perfil_AreaTrabalhoCod ;
      private String[] T00027_A58Usuario_PessoaNom ;
      private bool[] T00027_n58Usuario_PessoaNom ;
      private String[] T00027_A2Usuario_Nome ;
      private bool[] T00027_n2Usuario_Nome ;
      private String[] T00027_A341Usuario_UserGamGuid ;
      private String[] T00027_A4Perfil_Nome ;
      private short[] T00027_A275Perfil_Tipo ;
      private long[] T00027_A329Perfil_GamId ;
      private bool[] T00027_A543UsuarioPerfil_Display ;
      private bool[] T00027_n543UsuarioPerfil_Display ;
      private bool[] T00027_A544UsuarioPerfil_Insert ;
      private bool[] T00027_n544UsuarioPerfil_Insert ;
      private bool[] T00027_A659UsuarioPerfil_Update ;
      private bool[] T00027_n659UsuarioPerfil_Update ;
      private bool[] T00027_A546UsuarioPerfil_Delete ;
      private bool[] T00027_n546UsuarioPerfil_Delete ;
      private bool[] T00027_A276Perfil_Ativo ;
      private int[] T00027_A1Usuario_Codigo ;
      private int[] T00027_A3Perfil_Codigo ;
      private int[] T00027_A57Usuario_PessoaCod ;
      private int[] T00027_A7Perfil_AreaTrabalhoCod ;
      private String[] T00028_A2Usuario_Nome ;
      private bool[] T00028_n2Usuario_Nome ;
      private String[] T00028_A341Usuario_UserGamGuid ;
      private int[] T00028_A57Usuario_PessoaCod ;
      private String[] T00029_A58Usuario_PessoaNom ;
      private bool[] T00029_n58Usuario_PessoaNom ;
      private String[] T000210_A4Perfil_Nome ;
      private short[] T000210_A275Perfil_Tipo ;
      private long[] T000210_A329Perfil_GamId ;
      private bool[] T000210_A276Perfil_Ativo ;
      private int[] T000210_A7Perfil_AreaTrabalhoCod ;
      private int[] T000211_A1Usuario_Codigo ;
      private int[] T000211_A3Perfil_Codigo ;
      private bool[] T00023_A543UsuarioPerfil_Display ;
      private bool[] T00023_n543UsuarioPerfil_Display ;
      private bool[] T00023_A544UsuarioPerfil_Insert ;
      private bool[] T00023_n544UsuarioPerfil_Insert ;
      private bool[] T00023_A659UsuarioPerfil_Update ;
      private bool[] T00023_n659UsuarioPerfil_Update ;
      private bool[] T00023_A546UsuarioPerfil_Delete ;
      private bool[] T00023_n546UsuarioPerfil_Delete ;
      private int[] T00023_A1Usuario_Codigo ;
      private int[] T00023_A3Perfil_Codigo ;
      private int[] T000212_A1Usuario_Codigo ;
      private int[] T000212_A3Perfil_Codigo ;
      private int[] T000213_A1Usuario_Codigo ;
      private int[] T000213_A3Perfil_Codigo ;
      private bool[] T00022_A543UsuarioPerfil_Display ;
      private bool[] T00022_n543UsuarioPerfil_Display ;
      private bool[] T00022_A544UsuarioPerfil_Insert ;
      private bool[] T00022_n544UsuarioPerfil_Insert ;
      private bool[] T00022_A659UsuarioPerfil_Update ;
      private bool[] T00022_n659UsuarioPerfil_Update ;
      private bool[] T00022_A546UsuarioPerfil_Delete ;
      private bool[] T00022_n546UsuarioPerfil_Delete ;
      private int[] T00022_A1Usuario_Codigo ;
      private int[] T00022_A3Perfil_Codigo ;
      private String[] T000217_A2Usuario_Nome ;
      private bool[] T000217_n2Usuario_Nome ;
      private String[] T000217_A341Usuario_UserGamGuid ;
      private int[] T000217_A57Usuario_PessoaCod ;
      private String[] T000218_A58Usuario_PessoaNom ;
      private bool[] T000218_n58Usuario_PessoaNom ;
      private String[] T000219_A4Perfil_Nome ;
      private short[] T000219_A275Perfil_Tipo ;
      private long[] T000219_A329Perfil_GamId ;
      private bool[] T000219_A276Perfil_Ativo ;
      private int[] T000219_A7Perfil_AreaTrabalhoCod ;
      private int[] T000220_A1Usuario_Codigo ;
      private int[] T000220_A3Perfil_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV10TrnContext ;
   }

   public class usuarioperfil__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new UpdateCursor(def[13])
         ,new UpdateCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00027 ;
          prmT00027 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00024 ;
          prmT00024 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00026 ;
          prmT00026 = new Object[] {
          new Object[] {"@Usuario_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00025 ;
          prmT00025 = new Object[] {
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00028 ;
          prmT00028 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00029 ;
          prmT00029 = new Object[] {
          new Object[] {"@Usuario_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000210 ;
          prmT000210 = new Object[] {
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000211 ;
          prmT000211 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00023 ;
          prmT00023 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000212 ;
          prmT000212 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000213 ;
          prmT000213 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00022 ;
          prmT00022 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000214 ;
          prmT000214 = new Object[] {
          new Object[] {"@UsuarioPerfil_Display",SqlDbType.Bit,4,0} ,
          new Object[] {"@UsuarioPerfil_Insert",SqlDbType.Bit,4,0} ,
          new Object[] {"@UsuarioPerfil_Update",SqlDbType.Bit,4,0} ,
          new Object[] {"@UsuarioPerfil_Delete",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000215 ;
          prmT000215 = new Object[] {
          new Object[] {"@UsuarioPerfil_Display",SqlDbType.Bit,4,0} ,
          new Object[] {"@UsuarioPerfil_Insert",SqlDbType.Bit,4,0} ,
          new Object[] {"@UsuarioPerfil_Update",SqlDbType.Bit,4,0} ,
          new Object[] {"@UsuarioPerfil_Delete",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000216 ;
          prmT000216 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000220 ;
          prmT000220 = new Object[] {
          } ;
          Object[] prmT000217 ;
          prmT000217 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000218 ;
          prmT000218 = new Object[] {
          new Object[] {"@Usuario_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000219 ;
          prmT000219 = new Object[] {
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00022", "SELECT [UsuarioPerfil_Display], [UsuarioPerfil_Insert], [UsuarioPerfil_Update], [UsuarioPerfil_Delete], [Usuario_Codigo], [Perfil_Codigo] FROM [UsuarioPerfil] WITH (UPDLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo AND [Perfil_Codigo] = @Perfil_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00022,1,0,true,false )
             ,new CursorDef("T00023", "SELECT [UsuarioPerfil_Display], [UsuarioPerfil_Insert], [UsuarioPerfil_Update], [UsuarioPerfil_Delete], [Usuario_Codigo], [Perfil_Codigo] FROM [UsuarioPerfil] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo AND [Perfil_Codigo] = @Perfil_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00023,1,0,true,false )
             ,new CursorDef("T00024", "SELECT [Usuario_Nome], [Usuario_UserGamGuid], [Usuario_PessoaCod] AS Usuario_PessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00024,1,0,true,false )
             ,new CursorDef("T00025", "SELECT [Perfil_Nome], [Perfil_Tipo], [Perfil_GamId], [Perfil_Ativo], [Perfil_AreaTrabalhoCod] FROM [Perfil] WITH (NOLOCK) WHERE [Perfil_Codigo] = @Perfil_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00025,1,0,true,false )
             ,new CursorDef("T00026", "SELECT [Pessoa_Nome] AS Usuario_PessoaNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Usuario_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00026,1,0,true,false )
             ,new CursorDef("T00027", "SELECT T3.[Pessoa_Nome] AS Usuario_PessoaNom, T2.[Usuario_Nome], T2.[Usuario_UserGamGuid], T4.[Perfil_Nome], T4.[Perfil_Tipo], T4.[Perfil_GamId], TM1.[UsuarioPerfil_Display], TM1.[UsuarioPerfil_Insert], TM1.[UsuarioPerfil_Update], TM1.[UsuarioPerfil_Delete], T4.[Perfil_Ativo], TM1.[Usuario_Codigo], TM1.[Perfil_Codigo], T2.[Usuario_PessoaCod] AS Usuario_PessoaCod, T4.[Perfil_AreaTrabalhoCod] FROM ((([UsuarioPerfil] TM1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = TM1.[Usuario_Codigo]) INNER JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) INNER JOIN [Perfil] T4 WITH (NOLOCK) ON T4.[Perfil_Codigo] = TM1.[Perfil_Codigo]) WHERE TM1.[Usuario_Codigo] = @Usuario_Codigo and TM1.[Perfil_Codigo] = @Perfil_Codigo ORDER BY TM1.[Usuario_Codigo], TM1.[Perfil_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00027,100,0,true,false )
             ,new CursorDef("T00028", "SELECT [Usuario_Nome], [Usuario_UserGamGuid], [Usuario_PessoaCod] AS Usuario_PessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00028,1,0,true,false )
             ,new CursorDef("T00029", "SELECT [Pessoa_Nome] AS Usuario_PessoaNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Usuario_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00029,1,0,true,false )
             ,new CursorDef("T000210", "SELECT [Perfil_Nome], [Perfil_Tipo], [Perfil_GamId], [Perfil_Ativo], [Perfil_AreaTrabalhoCod] FROM [Perfil] WITH (NOLOCK) WHERE [Perfil_Codigo] = @Perfil_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000210,1,0,true,false )
             ,new CursorDef("T000211", "SELECT [Usuario_Codigo], [Perfil_Codigo] FROM [UsuarioPerfil] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo AND [Perfil_Codigo] = @Perfil_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000211,1,0,true,false )
             ,new CursorDef("T000212", "SELECT TOP 1 [Usuario_Codigo], [Perfil_Codigo] FROM [UsuarioPerfil] WITH (NOLOCK) WHERE ( [Usuario_Codigo] > @Usuario_Codigo or [Usuario_Codigo] = @Usuario_Codigo and [Perfil_Codigo] > @Perfil_Codigo) ORDER BY [Usuario_Codigo], [Perfil_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000212,1,0,true,true )
             ,new CursorDef("T000213", "SELECT TOP 1 [Usuario_Codigo], [Perfil_Codigo] FROM [UsuarioPerfil] WITH (NOLOCK) WHERE ( [Usuario_Codigo] < @Usuario_Codigo or [Usuario_Codigo] = @Usuario_Codigo and [Perfil_Codigo] < @Perfil_Codigo) ORDER BY [Usuario_Codigo] DESC, [Perfil_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000213,1,0,true,true )
             ,new CursorDef("T000214", "INSERT INTO [UsuarioPerfil]([UsuarioPerfil_Display], [UsuarioPerfil_Insert], [UsuarioPerfil_Update], [UsuarioPerfil_Delete], [Usuario_Codigo], [Perfil_Codigo]) VALUES(@UsuarioPerfil_Display, @UsuarioPerfil_Insert, @UsuarioPerfil_Update, @UsuarioPerfil_Delete, @Usuario_Codigo, @Perfil_Codigo)", GxErrorMask.GX_NOMASK,prmT000214)
             ,new CursorDef("T000215", "UPDATE [UsuarioPerfil] SET [UsuarioPerfil_Display]=@UsuarioPerfil_Display, [UsuarioPerfil_Insert]=@UsuarioPerfil_Insert, [UsuarioPerfil_Update]=@UsuarioPerfil_Update, [UsuarioPerfil_Delete]=@UsuarioPerfil_Delete  WHERE [Usuario_Codigo] = @Usuario_Codigo AND [Perfil_Codigo] = @Perfil_Codigo", GxErrorMask.GX_NOMASK,prmT000215)
             ,new CursorDef("T000216", "DELETE FROM [UsuarioPerfil]  WHERE [Usuario_Codigo] = @Usuario_Codigo AND [Perfil_Codigo] = @Perfil_Codigo", GxErrorMask.GX_NOMASK,prmT000216)
             ,new CursorDef("T000217", "SELECT [Usuario_Nome], [Usuario_UserGamGuid], [Usuario_PessoaCod] AS Usuario_PessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000217,1,0,true,false )
             ,new CursorDef("T000218", "SELECT [Pessoa_Nome] AS Usuario_PessoaNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Usuario_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000218,1,0,true,false )
             ,new CursorDef("T000219", "SELECT [Perfil_Nome], [Perfil_Tipo], [Perfil_GamId], [Perfil_Ativo], [Perfil_AreaTrabalhoCod] FROM [Perfil] WITH (NOLOCK) WHERE [Perfil_Codigo] = @Perfil_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000219,1,0,true,false )
             ,new CursorDef("T000220", "SELECT [Usuario_Codigo], [Perfil_Codigo] FROM [UsuarioPerfil] WITH (NOLOCK) ORDER BY [Usuario_Codigo], [Perfil_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000220,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((bool[]) buf[4])[0] = rslt.getBool(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((bool[]) buf[6])[0] = rslt.getBool(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((bool[]) buf[4])[0] = rslt.getBool(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((bool[]) buf[6])[0] = rslt.getBool(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 40) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((long[]) buf[2])[0] = rslt.getLong(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 40) ;
                ((String[]) buf[5])[0] = rslt.getString(4, 50) ;
                ((short[]) buf[6])[0] = rslt.getShort(5) ;
                ((long[]) buf[7])[0] = rslt.getLong(6) ;
                ((bool[]) buf[8])[0] = rslt.getBool(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((bool[]) buf[10])[0] = rslt.getBool(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((bool[]) buf[12])[0] = rslt.getBool(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((bool[]) buf[14])[0] = rslt.getBool(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((bool[]) buf[16])[0] = rslt.getBool(11) ;
                ((int[]) buf[17])[0] = rslt.getInt(12) ;
                ((int[]) buf[18])[0] = rslt.getInt(13) ;
                ((int[]) buf[19])[0] = rslt.getInt(14) ;
                ((int[]) buf[20])[0] = rslt.getInt(15) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 40) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((long[]) buf[2])[0] = rslt.getLong(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 15 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 40) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 16 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 17 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((long[]) buf[2])[0] = rslt.getLong(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(1, (bool)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(2, (bool)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(3, (bool)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(4, (bool)parms[7]);
                }
                stmt.SetParameter(5, (int)parms[8]);
                stmt.SetParameter(6, (int)parms[9]);
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(1, (bool)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(2, (bool)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(3, (bool)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(4, (bool)parms[7]);
                }
                stmt.SetParameter(5, (int)parms[8]);
                stmt.SetParameter(6, (int)parms[9]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
