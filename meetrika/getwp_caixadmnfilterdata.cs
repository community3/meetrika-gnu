/*
               File: GetWP_CaixaDmnFilterData
        Description: Get WP_Caixa Dmn Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:17:57.35
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwp_caixadmnfilterdata : GXProcedure
   {
      public getwp_caixadmnfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwp_caixadmnfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV14DDOName = aP0_DDOName;
         this.AV12SearchTxt = aP1_SearchTxt;
         this.AV13SearchTxtTo = aP2_SearchTxtTo;
         this.AV18OptionsJson = "" ;
         this.AV21OptionsDescJson = "" ;
         this.AV23OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV14DDOName = aP0_DDOName;
         this.AV12SearchTxt = aP1_SearchTxt;
         this.AV13SearchTxtTo = aP2_SearchTxtTo;
         this.AV18OptionsJson = "" ;
         this.AV21OptionsDescJson = "" ;
         this.AV23OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
         return AV23OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwp_caixadmnfilterdata objgetwp_caixadmnfilterdata;
         objgetwp_caixadmnfilterdata = new getwp_caixadmnfilterdata();
         objgetwp_caixadmnfilterdata.AV14DDOName = aP0_DDOName;
         objgetwp_caixadmnfilterdata.AV12SearchTxt = aP1_SearchTxt;
         objgetwp_caixadmnfilterdata.AV13SearchTxtTo = aP2_SearchTxtTo;
         objgetwp_caixadmnfilterdata.AV18OptionsJson = "" ;
         objgetwp_caixadmnfilterdata.AV21OptionsDescJson = "" ;
         objgetwp_caixadmnfilterdata.AV23OptionIndexesJson = "" ;
         objgetwp_caixadmnfilterdata.context.SetSubmitInitialConfig(context);
         objgetwp_caixadmnfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwp_caixadmnfilterdata);
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwp_caixadmnfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV17Options = (IGxCollection)(new GxSimpleCollection());
         AV20OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV22OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV14DDOName), "DDO_CONTAGEMRESULTADO_DEMANDA") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADO_DEMANDAOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV18OptionsJson = AV17Options.ToJSonString(false);
         AV21OptionsDescJson = AV20OptionsDesc.ToJSonString(false);
         AV23OptionIndexesJson = AV22OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV25Session.Get("WP_CaixaDmnGridState"), "") == 0 )
         {
            AV27GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WP_CaixaDmnGridState"), "");
         }
         else
         {
            AV27GridState.FromXml(AV25Session.Get("WP_CaixaDmnGridState"), "");
         }
         AV157GXV1 = 1;
         while ( AV157GXV1 <= AV27GridState.gxTpr_Filtervalues.Count )
         {
            AV28GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV27GridState.gxTpr_Filtervalues.Item(AV157GXV1));
            if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DEMANDA") == 0 )
            {
               AV10TFContagemResultado_Demanda = AV28GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DEMANDA_SEL") == 0 )
            {
               AV11TFContagemResultado_Demanda_Sel = AV28GridStateFilterValue.gxTpr_Value;
            }
            AV157GXV1 = (int)(AV157GXV1+1);
         }
         if ( AV27GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV29GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV27GridState.gxTpr_Dynamicfilters.Item(1));
            AV30DynamicFiltersSelector1 = AV29GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV30DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDA") == 0 )
            {
               AV31DynamicFiltersOperator1 = AV29GridStateDynamicFilter.gxTpr_Operator;
               AV32ContagemResultado_Demanda1 = AV29GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 )
            {
               AV33ContagemResultado_DataDmn1 = context.localUtil.CToD( AV29GridStateDynamicFilter.gxTpr_Value, 2);
               AV34ContagemResultado_DataDmn_To1 = context.localUtil.CToD( AV29GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
            {
               AV35ContagemResultado_DataPrevista1 = context.localUtil.CToT( AV29GridStateDynamicFilter.gxTpr_Value, 2);
               AV36ContagemResultado_DataPrevista_To1 = context.localUtil.CToT( AV29GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
            {
               AV37ContagemResultado_ContratadaCod1 = (int)(NumberUtil.Val( AV29GridStateDynamicFilter.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector1, "CONTAGEMRESULTADO_SERVICO") == 0 )
            {
               AV38ContagemResultado_Servico1 = (int)(NumberUtil.Val( AV29GridStateDynamicFilter.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
            {
               AV39ContagemResultado_SistemaCod1 = (int)(NumberUtil.Val( AV29GridStateDynamicFilter.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector1, "CONTAGEMRESULTADO_RESPONSAVEL") == 0 )
            {
               AV40ContagemResultado_Responsavel1 = (int)(NumberUtil.Val( AV29GridStateDynamicFilter.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
            {
               AV31DynamicFiltersOperator1 = AV29GridStateDynamicFilter.gxTpr_Operator;
               AV41ContagemResultado_Agrupador1 = AV29GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
            {
               AV42ContagemResultado_StatusDmn1 = AV29GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV27GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV43DynamicFiltersEnabled2 = true;
               AV29GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV27GridState.gxTpr_Dynamicfilters.Item(2));
               AV44DynamicFiltersSelector2 = AV29GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDA") == 0 )
               {
                  AV45DynamicFiltersOperator2 = AV29GridStateDynamicFilter.gxTpr_Operator;
                  AV46ContagemResultado_Demanda2 = AV29GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 )
               {
                  AV47ContagemResultado_DataDmn2 = context.localUtil.CToD( AV29GridStateDynamicFilter.gxTpr_Value, 2);
                  AV48ContagemResultado_DataDmn_To2 = context.localUtil.CToD( AV29GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
               {
                  AV49ContagemResultado_DataPrevista2 = context.localUtil.CToT( AV29GridStateDynamicFilter.gxTpr_Value, 2);
                  AV50ContagemResultado_DataPrevista_To2 = context.localUtil.CToT( AV29GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
               {
                  AV51ContagemResultado_ContratadaCod2 = (int)(NumberUtil.Val( AV29GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "CONTAGEMRESULTADO_SERVICO") == 0 )
               {
                  AV52ContagemResultado_Servico2 = (int)(NumberUtil.Val( AV29GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
               {
                  AV53ContagemResultado_SistemaCod2 = (int)(NumberUtil.Val( AV29GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "CONTAGEMRESULTADO_RESPONSAVEL") == 0 )
               {
                  AV54ContagemResultado_Responsavel2 = (int)(NumberUtil.Val( AV29GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
               {
                  AV45DynamicFiltersOperator2 = AV29GridStateDynamicFilter.gxTpr_Operator;
                  AV55ContagemResultado_Agrupador2 = AV29GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
               {
                  AV56ContagemResultado_StatusDmn2 = AV29GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV27GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV57DynamicFiltersEnabled3 = true;
                  AV29GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV27GridState.gxTpr_Dynamicfilters.Item(3));
                  AV58DynamicFiltersSelector3 = AV29GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDA") == 0 )
                  {
                     AV59DynamicFiltersOperator3 = AV29GridStateDynamicFilter.gxTpr_Operator;
                     AV60ContagemResultado_Demanda3 = AV29GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 )
                  {
                     AV61ContagemResultado_DataDmn3 = context.localUtil.CToD( AV29GridStateDynamicFilter.gxTpr_Value, 2);
                     AV62ContagemResultado_DataDmn_To3 = context.localUtil.CToD( AV29GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
                  else if ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
                  {
                     AV63ContagemResultado_DataPrevista3 = context.localUtil.CToT( AV29GridStateDynamicFilter.gxTpr_Value, 2);
                     AV64ContagemResultado_DataPrevista_To3 = context.localUtil.CToT( AV29GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
                  else if ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                  {
                     AV65ContagemResultado_ContratadaCod3 = (int)(NumberUtil.Val( AV29GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_SERVICO") == 0 )
                  {
                     AV66ContagemResultado_Servico3 = (int)(NumberUtil.Val( AV29GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                  {
                     AV67ContagemResultado_SistemaCod3 = (int)(NumberUtil.Val( AV29GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_RESPONSAVEL") == 0 )
                  {
                     AV68ContagemResultado_Responsavel3 = (int)(NumberUtil.Val( AV29GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
                  {
                     AV59DynamicFiltersOperator3 = AV29GridStateDynamicFilter.gxTpr_Operator;
                     AV69ContagemResultado_Agrupador3 = AV29GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                  {
                     AV70ContagemResultado_StatusDmn3 = AV29GridStateDynamicFilter.gxTpr_Value;
                  }
                  if ( AV27GridState.gxTpr_Dynamicfilters.Count >= 4 )
                  {
                     AV71DynamicFiltersEnabled4 = true;
                     AV29GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV27GridState.gxTpr_Dynamicfilters.Item(4));
                     AV72DynamicFiltersSelector4 = AV29GridStateDynamicFilter.gxTpr_Selected;
                     if ( StringUtil.StrCmp(AV72DynamicFiltersSelector4, "CONTAGEMRESULTADO_DEMANDA") == 0 )
                     {
                        AV73DynamicFiltersOperator4 = AV29GridStateDynamicFilter.gxTpr_Operator;
                        AV74ContagemResultado_Demanda4 = AV29GridStateDynamicFilter.gxTpr_Value;
                     }
                     else if ( StringUtil.StrCmp(AV72DynamicFiltersSelector4, "CONTAGEMRESULTADO_DATADMN") == 0 )
                     {
                        AV75ContagemResultado_DataDmn4 = context.localUtil.CToD( AV29GridStateDynamicFilter.gxTpr_Value, 2);
                        AV76ContagemResultado_DataDmn_To4 = context.localUtil.CToD( AV29GridStateDynamicFilter.gxTpr_Valueto, 2);
                     }
                     else if ( StringUtil.StrCmp(AV72DynamicFiltersSelector4, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
                     {
                        AV77ContagemResultado_DataPrevista4 = context.localUtil.CToT( AV29GridStateDynamicFilter.gxTpr_Value, 2);
                        AV78ContagemResultado_DataPrevista_To4 = context.localUtil.CToT( AV29GridStateDynamicFilter.gxTpr_Valueto, 2);
                     }
                     else if ( StringUtil.StrCmp(AV72DynamicFiltersSelector4, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                     {
                        AV79ContagemResultado_ContratadaCod4 = (int)(NumberUtil.Val( AV29GridStateDynamicFilter.gxTpr_Value, "."));
                     }
                     else if ( StringUtil.StrCmp(AV72DynamicFiltersSelector4, "CONTAGEMRESULTADO_SERVICO") == 0 )
                     {
                        AV80ContagemResultado_Servico4 = (int)(NumberUtil.Val( AV29GridStateDynamicFilter.gxTpr_Value, "."));
                     }
                     else if ( StringUtil.StrCmp(AV72DynamicFiltersSelector4, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                     {
                        AV81ContagemResultado_SistemaCod4 = (int)(NumberUtil.Val( AV29GridStateDynamicFilter.gxTpr_Value, "."));
                     }
                     else if ( StringUtil.StrCmp(AV72DynamicFiltersSelector4, "CONTAGEMRESULTADO_RESPONSAVEL") == 0 )
                     {
                        AV82ContagemResultado_Responsavel4 = (int)(NumberUtil.Val( AV29GridStateDynamicFilter.gxTpr_Value, "."));
                     }
                     else if ( StringUtil.StrCmp(AV72DynamicFiltersSelector4, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
                     {
                        AV73DynamicFiltersOperator4 = AV29GridStateDynamicFilter.gxTpr_Operator;
                        AV83ContagemResultado_Agrupador4 = AV29GridStateDynamicFilter.gxTpr_Value;
                     }
                     else if ( StringUtil.StrCmp(AV72DynamicFiltersSelector4, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                     {
                        AV84ContagemResultado_StatusDmn4 = AV29GridStateDynamicFilter.gxTpr_Value;
                     }
                     if ( AV27GridState.gxTpr_Dynamicfilters.Count >= 5 )
                     {
                        AV85DynamicFiltersEnabled5 = true;
                        AV29GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV27GridState.gxTpr_Dynamicfilters.Item(5));
                        AV86DynamicFiltersSelector5 = AV29GridStateDynamicFilter.gxTpr_Selected;
                        if ( StringUtil.StrCmp(AV86DynamicFiltersSelector5, "CONTAGEMRESULTADO_DEMANDA") == 0 )
                        {
                           AV87DynamicFiltersOperator5 = AV29GridStateDynamicFilter.gxTpr_Operator;
                           AV88ContagemResultado_Demanda5 = AV29GridStateDynamicFilter.gxTpr_Value;
                        }
                        else if ( StringUtil.StrCmp(AV86DynamicFiltersSelector5, "CONTAGEMRESULTADO_DATADMN") == 0 )
                        {
                           AV89ContagemResultado_DataDmn5 = context.localUtil.CToD( AV29GridStateDynamicFilter.gxTpr_Value, 2);
                           AV90ContagemResultado_DataDmn_To5 = context.localUtil.CToD( AV29GridStateDynamicFilter.gxTpr_Valueto, 2);
                        }
                        else if ( StringUtil.StrCmp(AV86DynamicFiltersSelector5, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
                        {
                           AV91ContagemResultado_DataPrevista5 = context.localUtil.CToT( AV29GridStateDynamicFilter.gxTpr_Value, 2);
                           AV92ContagemResultado_DataPrevista_To5 = context.localUtil.CToT( AV29GridStateDynamicFilter.gxTpr_Valueto, 2);
                        }
                        else if ( StringUtil.StrCmp(AV86DynamicFiltersSelector5, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                        {
                           AV93ContagemResultado_ContratadaCod5 = (int)(NumberUtil.Val( AV29GridStateDynamicFilter.gxTpr_Value, "."));
                        }
                        else if ( StringUtil.StrCmp(AV86DynamicFiltersSelector5, "CONTAGEMRESULTADO_SERVICO") == 0 )
                        {
                           AV94ContagemResultado_Servico5 = (int)(NumberUtil.Val( AV29GridStateDynamicFilter.gxTpr_Value, "."));
                        }
                        else if ( StringUtil.StrCmp(AV86DynamicFiltersSelector5, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                        {
                           AV95ContagemResultado_SistemaCod5 = (int)(NumberUtil.Val( AV29GridStateDynamicFilter.gxTpr_Value, "."));
                        }
                        else if ( StringUtil.StrCmp(AV86DynamicFiltersSelector5, "CONTAGEMRESULTADO_RESPONSAVEL") == 0 )
                        {
                           AV96ContagemResultado_Responsavel5 = (int)(NumberUtil.Val( AV29GridStateDynamicFilter.gxTpr_Value, "."));
                        }
                        else if ( StringUtil.StrCmp(AV86DynamicFiltersSelector5, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
                        {
                           AV87DynamicFiltersOperator5 = AV29GridStateDynamicFilter.gxTpr_Operator;
                           AV97ContagemResultado_Agrupador5 = AV29GridStateDynamicFilter.gxTpr_Value;
                        }
                        else if ( StringUtil.StrCmp(AV86DynamicFiltersSelector5, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                        {
                           AV98ContagemResultado_StatusDmn5 = AV29GridStateDynamicFilter.gxTpr_Value;
                        }
                        if ( AV27GridState.gxTpr_Dynamicfilters.Count >= 6 )
                        {
                           AV99DynamicFiltersEnabled6 = true;
                           AV29GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV27GridState.gxTpr_Dynamicfilters.Item(6));
                           AV100DynamicFiltersSelector6 = AV29GridStateDynamicFilter.gxTpr_Selected;
                           if ( StringUtil.StrCmp(AV100DynamicFiltersSelector6, "CONTAGEMRESULTADO_DEMANDA") == 0 )
                           {
                              AV101DynamicFiltersOperator6 = AV29GridStateDynamicFilter.gxTpr_Operator;
                              AV102ContagemResultado_Demanda6 = AV29GridStateDynamicFilter.gxTpr_Value;
                           }
                           else if ( StringUtil.StrCmp(AV100DynamicFiltersSelector6, "CONTAGEMRESULTADO_DATADMN") == 0 )
                           {
                              AV103ContagemResultado_DataDmn6 = context.localUtil.CToD( AV29GridStateDynamicFilter.gxTpr_Value, 2);
                              AV104ContagemResultado_DataDmn_To6 = context.localUtil.CToD( AV29GridStateDynamicFilter.gxTpr_Valueto, 2);
                           }
                           else if ( StringUtil.StrCmp(AV100DynamicFiltersSelector6, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
                           {
                              AV105ContagemResultado_DataPrevista6 = context.localUtil.CToT( AV29GridStateDynamicFilter.gxTpr_Value, 2);
                              AV106ContagemResultado_DataPrevista_To6 = context.localUtil.CToT( AV29GridStateDynamicFilter.gxTpr_Valueto, 2);
                           }
                           else if ( StringUtil.StrCmp(AV100DynamicFiltersSelector6, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                           {
                              AV107ContagemResultado_ContratadaCod6 = (int)(NumberUtil.Val( AV29GridStateDynamicFilter.gxTpr_Value, "."));
                           }
                           else if ( StringUtil.StrCmp(AV100DynamicFiltersSelector6, "CONTAGEMRESULTADO_SERVICO") == 0 )
                           {
                              AV108ContagemResultado_Servico6 = (int)(NumberUtil.Val( AV29GridStateDynamicFilter.gxTpr_Value, "."));
                           }
                           else if ( StringUtil.StrCmp(AV100DynamicFiltersSelector6, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                           {
                              AV109ContagemResultado_SistemaCod6 = (int)(NumberUtil.Val( AV29GridStateDynamicFilter.gxTpr_Value, "."));
                           }
                           else if ( StringUtil.StrCmp(AV100DynamicFiltersSelector6, "CONTAGEMRESULTADO_RESPONSAVEL") == 0 )
                           {
                              AV110ContagemResultado_Responsavel6 = (int)(NumberUtil.Val( AV29GridStateDynamicFilter.gxTpr_Value, "."));
                           }
                           else if ( StringUtil.StrCmp(AV100DynamicFiltersSelector6, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
                           {
                              AV101DynamicFiltersOperator6 = AV29GridStateDynamicFilter.gxTpr_Operator;
                              AV111ContagemResultado_Agrupador6 = AV29GridStateDynamicFilter.gxTpr_Value;
                           }
                           else if ( StringUtil.StrCmp(AV100DynamicFiltersSelector6, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                           {
                              AV112ContagemResultado_StatusDmn6 = AV29GridStateDynamicFilter.gxTpr_Value;
                           }
                           if ( AV27GridState.gxTpr_Dynamicfilters.Count >= 7 )
                           {
                              AV113DynamicFiltersEnabled7 = true;
                              AV29GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV27GridState.gxTpr_Dynamicfilters.Item(7));
                              AV114DynamicFiltersSelector7 = AV29GridStateDynamicFilter.gxTpr_Selected;
                              if ( StringUtil.StrCmp(AV114DynamicFiltersSelector7, "CONTAGEMRESULTADO_DEMANDA") == 0 )
                              {
                                 AV115DynamicFiltersOperator7 = AV29GridStateDynamicFilter.gxTpr_Operator;
                                 AV116ContagemResultado_Demanda7 = AV29GridStateDynamicFilter.gxTpr_Value;
                              }
                              else if ( StringUtil.StrCmp(AV114DynamicFiltersSelector7, "CONTAGEMRESULTADO_DATADMN") == 0 )
                              {
                                 AV117ContagemResultado_DataDmn7 = context.localUtil.CToD( AV29GridStateDynamicFilter.gxTpr_Value, 2);
                                 AV118ContagemResultado_DataDmn_To7 = context.localUtil.CToD( AV29GridStateDynamicFilter.gxTpr_Valueto, 2);
                              }
                              else if ( StringUtil.StrCmp(AV114DynamicFiltersSelector7, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
                              {
                                 AV119ContagemResultado_DataPrevista7 = context.localUtil.CToT( AV29GridStateDynamicFilter.gxTpr_Value, 2);
                                 AV120ContagemResultado_DataPrevista_To7 = context.localUtil.CToT( AV29GridStateDynamicFilter.gxTpr_Valueto, 2);
                              }
                              else if ( StringUtil.StrCmp(AV114DynamicFiltersSelector7, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                              {
                                 AV121ContagemResultado_ContratadaCod7 = (int)(NumberUtil.Val( AV29GridStateDynamicFilter.gxTpr_Value, "."));
                              }
                              else if ( StringUtil.StrCmp(AV114DynamicFiltersSelector7, "CONTAGEMRESULTADO_SERVICO") == 0 )
                              {
                                 AV122ContagemResultado_Servico7 = (int)(NumberUtil.Val( AV29GridStateDynamicFilter.gxTpr_Value, "."));
                              }
                              else if ( StringUtil.StrCmp(AV114DynamicFiltersSelector7, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                              {
                                 AV123ContagemResultado_SistemaCod7 = (int)(NumberUtil.Val( AV29GridStateDynamicFilter.gxTpr_Value, "."));
                              }
                              else if ( StringUtil.StrCmp(AV114DynamicFiltersSelector7, "CONTAGEMRESULTADO_RESPONSAVEL") == 0 )
                              {
                                 AV124ContagemResultado_Responsavel7 = (int)(NumberUtil.Val( AV29GridStateDynamicFilter.gxTpr_Value, "."));
                              }
                              else if ( StringUtil.StrCmp(AV114DynamicFiltersSelector7, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
                              {
                                 AV115DynamicFiltersOperator7 = AV29GridStateDynamicFilter.gxTpr_Operator;
                                 AV125ContagemResultado_Agrupador7 = AV29GridStateDynamicFilter.gxTpr_Value;
                              }
                              else if ( StringUtil.StrCmp(AV114DynamicFiltersSelector7, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                              {
                                 AV126ContagemResultado_StatusDmn7 = AV29GridStateDynamicFilter.gxTpr_Value;
                              }
                              if ( AV27GridState.gxTpr_Dynamicfilters.Count >= 8 )
                              {
                                 AV127DynamicFiltersEnabled8 = true;
                                 AV29GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV27GridState.gxTpr_Dynamicfilters.Item(8));
                                 AV128DynamicFiltersSelector8 = AV29GridStateDynamicFilter.gxTpr_Selected;
                                 if ( StringUtil.StrCmp(AV128DynamicFiltersSelector8, "CONTAGEMRESULTADO_DEMANDA") == 0 )
                                 {
                                    AV129DynamicFiltersOperator8 = AV29GridStateDynamicFilter.gxTpr_Operator;
                                    AV130ContagemResultado_Demanda8 = AV29GridStateDynamicFilter.gxTpr_Value;
                                 }
                                 else if ( StringUtil.StrCmp(AV128DynamicFiltersSelector8, "CONTAGEMRESULTADO_DATADMN") == 0 )
                                 {
                                    AV131ContagemResultado_DataDmn8 = context.localUtil.CToD( AV29GridStateDynamicFilter.gxTpr_Value, 2);
                                    AV132ContagemResultado_DataDmn_To8 = context.localUtil.CToD( AV29GridStateDynamicFilter.gxTpr_Valueto, 2);
                                 }
                                 else if ( StringUtil.StrCmp(AV128DynamicFiltersSelector8, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
                                 {
                                    AV133ContagemResultado_DataPrevista8 = context.localUtil.CToT( AV29GridStateDynamicFilter.gxTpr_Value, 2);
                                    AV134ContagemResultado_DataPrevista_To8 = context.localUtil.CToT( AV29GridStateDynamicFilter.gxTpr_Valueto, 2);
                                 }
                                 else if ( StringUtil.StrCmp(AV128DynamicFiltersSelector8, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                                 {
                                    AV135ContagemResultado_ContratadaCod8 = (int)(NumberUtil.Val( AV29GridStateDynamicFilter.gxTpr_Value, "."));
                                 }
                                 else if ( StringUtil.StrCmp(AV128DynamicFiltersSelector8, "CONTAGEMRESULTADO_SERVICO") == 0 )
                                 {
                                    AV136ContagemResultado_Servico8 = (int)(NumberUtil.Val( AV29GridStateDynamicFilter.gxTpr_Value, "."));
                                 }
                                 else if ( StringUtil.StrCmp(AV128DynamicFiltersSelector8, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                                 {
                                    AV137ContagemResultado_SistemaCod8 = (int)(NumberUtil.Val( AV29GridStateDynamicFilter.gxTpr_Value, "."));
                                 }
                                 else if ( StringUtil.StrCmp(AV128DynamicFiltersSelector8, "CONTAGEMRESULTADO_RESPONSAVEL") == 0 )
                                 {
                                    AV138ContagemResultado_Responsavel8 = (int)(NumberUtil.Val( AV29GridStateDynamicFilter.gxTpr_Value, "."));
                                 }
                                 else if ( StringUtil.StrCmp(AV128DynamicFiltersSelector8, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
                                 {
                                    AV129DynamicFiltersOperator8 = AV29GridStateDynamicFilter.gxTpr_Operator;
                                    AV139ContagemResultado_Agrupador8 = AV29GridStateDynamicFilter.gxTpr_Value;
                                 }
                                 else if ( StringUtil.StrCmp(AV128DynamicFiltersSelector8, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                                 {
                                    AV140ContagemResultado_StatusDmn8 = AV29GridStateDynamicFilter.gxTpr_Value;
                                 }
                                 if ( AV27GridState.gxTpr_Dynamicfilters.Count >= 9 )
                                 {
                                    AV141DynamicFiltersEnabled9 = true;
                                    AV29GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV27GridState.gxTpr_Dynamicfilters.Item(9));
                                    AV142DynamicFiltersSelector9 = AV29GridStateDynamicFilter.gxTpr_Selected;
                                    if ( StringUtil.StrCmp(AV142DynamicFiltersSelector9, "CONTAGEMRESULTADO_DEMANDA") == 0 )
                                    {
                                       AV143DynamicFiltersOperator9 = AV29GridStateDynamicFilter.gxTpr_Operator;
                                       AV144ContagemResultado_Demanda9 = AV29GridStateDynamicFilter.gxTpr_Value;
                                    }
                                    else if ( StringUtil.StrCmp(AV142DynamicFiltersSelector9, "CONTAGEMRESULTADO_DATADMN") == 0 )
                                    {
                                       AV145ContagemResultado_DataDmn9 = context.localUtil.CToD( AV29GridStateDynamicFilter.gxTpr_Value, 2);
                                       AV146ContagemResultado_DataDmn_To9 = context.localUtil.CToD( AV29GridStateDynamicFilter.gxTpr_Valueto, 2);
                                    }
                                    else if ( StringUtil.StrCmp(AV142DynamicFiltersSelector9, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
                                    {
                                       AV147ContagemResultado_DataPrevista9 = context.localUtil.CToT( AV29GridStateDynamicFilter.gxTpr_Value, 2);
                                       AV148ContagemResultado_DataPrevista_To9 = context.localUtil.CToT( AV29GridStateDynamicFilter.gxTpr_Valueto, 2);
                                    }
                                    else if ( StringUtil.StrCmp(AV142DynamicFiltersSelector9, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                                    {
                                       AV149ContagemResultado_ContratadaCod9 = (int)(NumberUtil.Val( AV29GridStateDynamicFilter.gxTpr_Value, "."));
                                    }
                                    else if ( StringUtil.StrCmp(AV142DynamicFiltersSelector9, "CONTAGEMRESULTADO_SERVICO") == 0 )
                                    {
                                       AV150ContagemResultado_Servico9 = (int)(NumberUtil.Val( AV29GridStateDynamicFilter.gxTpr_Value, "."));
                                    }
                                    else if ( StringUtil.StrCmp(AV142DynamicFiltersSelector9, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                                    {
                                       AV151ContagemResultado_SistemaCod9 = (int)(NumberUtil.Val( AV29GridStateDynamicFilter.gxTpr_Value, "."));
                                    }
                                    else if ( StringUtil.StrCmp(AV142DynamicFiltersSelector9, "CONTAGEMRESULTADO_RESPONSAVEL") == 0 )
                                    {
                                       AV152ContagemResultado_Responsavel9 = (int)(NumberUtil.Val( AV29GridStateDynamicFilter.gxTpr_Value, "."));
                                    }
                                    else if ( StringUtil.StrCmp(AV142DynamicFiltersSelector9, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
                                    {
                                       AV143DynamicFiltersOperator9 = AV29GridStateDynamicFilter.gxTpr_Operator;
                                       AV153ContagemResultado_Agrupador9 = AV29GridStateDynamicFilter.gxTpr_Value;
                                    }
                                    else if ( StringUtil.StrCmp(AV142DynamicFiltersSelector9, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                                    {
                                       AV154ContagemResultado_StatusDmn9 = AV29GridStateDynamicFilter.gxTpr_Value;
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTAGEMRESULTADO_DEMANDAOPTIONS' Routine */
         AV10TFContagemResultado_Demanda = AV12SearchTxt;
         AV11TFContagemResultado_Demanda_Sel = "";
         AV159WP_CaixaDmnDS_1_Dynamicfiltersselector1 = AV30DynamicFiltersSelector1;
         AV160WP_CaixaDmnDS_2_Dynamicfiltersoperator1 = AV31DynamicFiltersOperator1;
         AV161WP_CaixaDmnDS_3_Contagemresultado_demanda1 = AV32ContagemResultado_Demanda1;
         AV162WP_CaixaDmnDS_4_Contagemresultado_datadmn1 = AV33ContagemResultado_DataDmn1;
         AV163WP_CaixaDmnDS_5_Contagemresultado_datadmn_to1 = AV34ContagemResultado_DataDmn_To1;
         AV164WP_CaixaDmnDS_6_Contagemresultado_dataprevista1 = AV35ContagemResultado_DataPrevista1;
         AV165WP_CaixaDmnDS_7_Contagemresultado_dataprevista_to1 = AV36ContagemResultado_DataPrevista_To1;
         AV166WP_CaixaDmnDS_8_Contagemresultado_contratadacod1 = AV37ContagemResultado_ContratadaCod1;
         AV167WP_CaixaDmnDS_9_Contagemresultado_servico1 = AV38ContagemResultado_Servico1;
         AV168WP_CaixaDmnDS_10_Contagemresultado_sistemacod1 = AV39ContagemResultado_SistemaCod1;
         AV169WP_CaixaDmnDS_11_Contagemresultado_responsavel1 = AV40ContagemResultado_Responsavel1;
         AV170WP_CaixaDmnDS_12_Contagemresultado_agrupador1 = AV41ContagemResultado_Agrupador1;
         AV171WP_CaixaDmnDS_13_Contagemresultado_statusdmn1 = AV42ContagemResultado_StatusDmn1;
         AV172WP_CaixaDmnDS_14_Dynamicfiltersenabled2 = AV43DynamicFiltersEnabled2;
         AV173WP_CaixaDmnDS_15_Dynamicfiltersselector2 = AV44DynamicFiltersSelector2;
         AV174WP_CaixaDmnDS_16_Dynamicfiltersoperator2 = AV45DynamicFiltersOperator2;
         AV175WP_CaixaDmnDS_17_Contagemresultado_demanda2 = AV46ContagemResultado_Demanda2;
         AV176WP_CaixaDmnDS_18_Contagemresultado_datadmn2 = AV47ContagemResultado_DataDmn2;
         AV177WP_CaixaDmnDS_19_Contagemresultado_datadmn_to2 = AV48ContagemResultado_DataDmn_To2;
         AV178WP_CaixaDmnDS_20_Contagemresultado_dataprevista2 = AV49ContagemResultado_DataPrevista2;
         AV179WP_CaixaDmnDS_21_Contagemresultado_dataprevista_to2 = AV50ContagemResultado_DataPrevista_To2;
         AV180WP_CaixaDmnDS_22_Contagemresultado_contratadacod2 = AV51ContagemResultado_ContratadaCod2;
         AV181WP_CaixaDmnDS_23_Contagemresultado_servico2 = AV52ContagemResultado_Servico2;
         AV182WP_CaixaDmnDS_24_Contagemresultado_sistemacod2 = AV53ContagemResultado_SistemaCod2;
         AV183WP_CaixaDmnDS_25_Contagemresultado_responsavel2 = AV54ContagemResultado_Responsavel2;
         AV184WP_CaixaDmnDS_26_Contagemresultado_agrupador2 = AV55ContagemResultado_Agrupador2;
         AV185WP_CaixaDmnDS_27_Contagemresultado_statusdmn2 = AV56ContagemResultado_StatusDmn2;
         AV186WP_CaixaDmnDS_28_Dynamicfiltersenabled3 = AV57DynamicFiltersEnabled3;
         AV187WP_CaixaDmnDS_29_Dynamicfiltersselector3 = AV58DynamicFiltersSelector3;
         AV188WP_CaixaDmnDS_30_Dynamicfiltersoperator3 = AV59DynamicFiltersOperator3;
         AV189WP_CaixaDmnDS_31_Contagemresultado_demanda3 = AV60ContagemResultado_Demanda3;
         AV190WP_CaixaDmnDS_32_Contagemresultado_datadmn3 = AV61ContagemResultado_DataDmn3;
         AV191WP_CaixaDmnDS_33_Contagemresultado_datadmn_to3 = AV62ContagemResultado_DataDmn_To3;
         AV192WP_CaixaDmnDS_34_Contagemresultado_dataprevista3 = AV63ContagemResultado_DataPrevista3;
         AV193WP_CaixaDmnDS_35_Contagemresultado_dataprevista_to3 = AV64ContagemResultado_DataPrevista_To3;
         AV194WP_CaixaDmnDS_36_Contagemresultado_contratadacod3 = AV65ContagemResultado_ContratadaCod3;
         AV195WP_CaixaDmnDS_37_Contagemresultado_servico3 = AV66ContagemResultado_Servico3;
         AV196WP_CaixaDmnDS_38_Contagemresultado_sistemacod3 = AV67ContagemResultado_SistemaCod3;
         AV197WP_CaixaDmnDS_39_Contagemresultado_responsavel3 = AV68ContagemResultado_Responsavel3;
         AV198WP_CaixaDmnDS_40_Contagemresultado_agrupador3 = AV69ContagemResultado_Agrupador3;
         AV199WP_CaixaDmnDS_41_Contagemresultado_statusdmn3 = AV70ContagemResultado_StatusDmn3;
         AV200WP_CaixaDmnDS_42_Dynamicfiltersenabled4 = AV71DynamicFiltersEnabled4;
         AV201WP_CaixaDmnDS_43_Dynamicfiltersselector4 = AV72DynamicFiltersSelector4;
         AV202WP_CaixaDmnDS_44_Dynamicfiltersoperator4 = AV73DynamicFiltersOperator4;
         AV203WP_CaixaDmnDS_45_Contagemresultado_demanda4 = AV74ContagemResultado_Demanda4;
         AV204WP_CaixaDmnDS_46_Contagemresultado_datadmn4 = AV75ContagemResultado_DataDmn4;
         AV205WP_CaixaDmnDS_47_Contagemresultado_datadmn_to4 = AV76ContagemResultado_DataDmn_To4;
         AV206WP_CaixaDmnDS_48_Contagemresultado_dataprevista4 = AV77ContagemResultado_DataPrevista4;
         AV207WP_CaixaDmnDS_49_Contagemresultado_dataprevista_to4 = AV78ContagemResultado_DataPrevista_To4;
         AV208WP_CaixaDmnDS_50_Contagemresultado_contratadacod4 = AV79ContagemResultado_ContratadaCod4;
         AV209WP_CaixaDmnDS_51_Contagemresultado_servico4 = AV80ContagemResultado_Servico4;
         AV210WP_CaixaDmnDS_52_Contagemresultado_sistemacod4 = AV81ContagemResultado_SistemaCod4;
         AV211WP_CaixaDmnDS_53_Contagemresultado_responsavel4 = AV82ContagemResultado_Responsavel4;
         AV212WP_CaixaDmnDS_54_Contagemresultado_agrupador4 = AV83ContagemResultado_Agrupador4;
         AV213WP_CaixaDmnDS_55_Contagemresultado_statusdmn4 = AV84ContagemResultado_StatusDmn4;
         AV214WP_CaixaDmnDS_56_Dynamicfiltersenabled5 = AV85DynamicFiltersEnabled5;
         AV215WP_CaixaDmnDS_57_Dynamicfiltersselector5 = AV86DynamicFiltersSelector5;
         AV216WP_CaixaDmnDS_58_Dynamicfiltersoperator5 = AV87DynamicFiltersOperator5;
         AV217WP_CaixaDmnDS_59_Contagemresultado_demanda5 = AV88ContagemResultado_Demanda5;
         AV218WP_CaixaDmnDS_60_Contagemresultado_datadmn5 = AV89ContagemResultado_DataDmn5;
         AV219WP_CaixaDmnDS_61_Contagemresultado_datadmn_to5 = AV90ContagemResultado_DataDmn_To5;
         AV220WP_CaixaDmnDS_62_Contagemresultado_dataprevista5 = AV91ContagemResultado_DataPrevista5;
         AV221WP_CaixaDmnDS_63_Contagemresultado_dataprevista_to5 = AV92ContagemResultado_DataPrevista_To5;
         AV222WP_CaixaDmnDS_64_Contagemresultado_contratadacod5 = AV93ContagemResultado_ContratadaCod5;
         AV223WP_CaixaDmnDS_65_Contagemresultado_servico5 = AV94ContagemResultado_Servico5;
         AV224WP_CaixaDmnDS_66_Contagemresultado_sistemacod5 = AV95ContagemResultado_SistemaCod5;
         AV225WP_CaixaDmnDS_67_Contagemresultado_responsavel5 = AV96ContagemResultado_Responsavel5;
         AV226WP_CaixaDmnDS_68_Contagemresultado_agrupador5 = AV97ContagemResultado_Agrupador5;
         AV227WP_CaixaDmnDS_69_Contagemresultado_statusdmn5 = AV98ContagemResultado_StatusDmn5;
         AV228WP_CaixaDmnDS_70_Dynamicfiltersenabled6 = AV99DynamicFiltersEnabled6;
         AV229WP_CaixaDmnDS_71_Dynamicfiltersselector6 = AV100DynamicFiltersSelector6;
         AV230WP_CaixaDmnDS_72_Dynamicfiltersoperator6 = AV101DynamicFiltersOperator6;
         AV231WP_CaixaDmnDS_73_Contagemresultado_demanda6 = AV102ContagemResultado_Demanda6;
         AV232WP_CaixaDmnDS_74_Contagemresultado_datadmn6 = AV103ContagemResultado_DataDmn6;
         AV233WP_CaixaDmnDS_75_Contagemresultado_datadmn_to6 = AV104ContagemResultado_DataDmn_To6;
         AV234WP_CaixaDmnDS_76_Contagemresultado_dataprevista6 = AV105ContagemResultado_DataPrevista6;
         AV235WP_CaixaDmnDS_77_Contagemresultado_dataprevista_to6 = AV106ContagemResultado_DataPrevista_To6;
         AV236WP_CaixaDmnDS_78_Contagemresultado_contratadacod6 = AV107ContagemResultado_ContratadaCod6;
         AV237WP_CaixaDmnDS_79_Contagemresultado_servico6 = AV108ContagemResultado_Servico6;
         AV238WP_CaixaDmnDS_80_Contagemresultado_sistemacod6 = AV109ContagemResultado_SistemaCod6;
         AV239WP_CaixaDmnDS_81_Contagemresultado_responsavel6 = AV110ContagemResultado_Responsavel6;
         AV240WP_CaixaDmnDS_82_Contagemresultado_agrupador6 = AV111ContagemResultado_Agrupador6;
         AV241WP_CaixaDmnDS_83_Contagemresultado_statusdmn6 = AV112ContagemResultado_StatusDmn6;
         AV242WP_CaixaDmnDS_84_Dynamicfiltersenabled7 = AV113DynamicFiltersEnabled7;
         AV243WP_CaixaDmnDS_85_Dynamicfiltersselector7 = AV114DynamicFiltersSelector7;
         AV244WP_CaixaDmnDS_86_Dynamicfiltersoperator7 = AV115DynamicFiltersOperator7;
         AV245WP_CaixaDmnDS_87_Contagemresultado_demanda7 = AV116ContagemResultado_Demanda7;
         AV246WP_CaixaDmnDS_88_Contagemresultado_datadmn7 = AV117ContagemResultado_DataDmn7;
         AV247WP_CaixaDmnDS_89_Contagemresultado_datadmn_to7 = AV118ContagemResultado_DataDmn_To7;
         AV248WP_CaixaDmnDS_90_Contagemresultado_dataprevista7 = AV119ContagemResultado_DataPrevista7;
         AV249WP_CaixaDmnDS_91_Contagemresultado_dataprevista_to7 = AV120ContagemResultado_DataPrevista_To7;
         AV250WP_CaixaDmnDS_92_Contagemresultado_contratadacod7 = AV121ContagemResultado_ContratadaCod7;
         AV251WP_CaixaDmnDS_93_Contagemresultado_servico7 = AV122ContagemResultado_Servico7;
         AV252WP_CaixaDmnDS_94_Contagemresultado_sistemacod7 = AV123ContagemResultado_SistemaCod7;
         AV253WP_CaixaDmnDS_95_Contagemresultado_responsavel7 = AV124ContagemResultado_Responsavel7;
         AV254WP_CaixaDmnDS_96_Contagemresultado_agrupador7 = AV125ContagemResultado_Agrupador7;
         AV255WP_CaixaDmnDS_97_Contagemresultado_statusdmn7 = AV126ContagemResultado_StatusDmn7;
         AV256WP_CaixaDmnDS_98_Dynamicfiltersenabled8 = AV127DynamicFiltersEnabled8;
         AV257WP_CaixaDmnDS_99_Dynamicfiltersselector8 = AV128DynamicFiltersSelector8;
         AV258WP_CaixaDmnDS_100_Dynamicfiltersoperator8 = AV129DynamicFiltersOperator8;
         AV259WP_CaixaDmnDS_101_Contagemresultado_demanda8 = AV130ContagemResultado_Demanda8;
         AV260WP_CaixaDmnDS_102_Contagemresultado_datadmn8 = AV131ContagemResultado_DataDmn8;
         AV261WP_CaixaDmnDS_103_Contagemresultado_datadmn_to8 = AV132ContagemResultado_DataDmn_To8;
         AV262WP_CaixaDmnDS_104_Contagemresultado_dataprevista8 = AV133ContagemResultado_DataPrevista8;
         AV263WP_CaixaDmnDS_105_Contagemresultado_dataprevista_to8 = AV134ContagemResultado_DataPrevista_To8;
         AV264WP_CaixaDmnDS_106_Contagemresultado_contratadacod8 = AV135ContagemResultado_ContratadaCod8;
         AV265WP_CaixaDmnDS_107_Contagemresultado_servico8 = AV136ContagemResultado_Servico8;
         AV266WP_CaixaDmnDS_108_Contagemresultado_sistemacod8 = AV137ContagemResultado_SistemaCod8;
         AV267WP_CaixaDmnDS_109_Contagemresultado_responsavel8 = AV138ContagemResultado_Responsavel8;
         AV268WP_CaixaDmnDS_110_Contagemresultado_agrupador8 = AV139ContagemResultado_Agrupador8;
         AV269WP_CaixaDmnDS_111_Contagemresultado_statusdmn8 = AV140ContagemResultado_StatusDmn8;
         AV270WP_CaixaDmnDS_112_Dynamicfiltersenabled9 = AV141DynamicFiltersEnabled9;
         AV271WP_CaixaDmnDS_113_Dynamicfiltersselector9 = AV142DynamicFiltersSelector9;
         AV272WP_CaixaDmnDS_114_Dynamicfiltersoperator9 = AV143DynamicFiltersOperator9;
         AV273WP_CaixaDmnDS_115_Contagemresultado_demanda9 = AV144ContagemResultado_Demanda9;
         AV274WP_CaixaDmnDS_116_Contagemresultado_datadmn9 = AV145ContagemResultado_DataDmn9;
         AV275WP_CaixaDmnDS_117_Contagemresultado_datadmn_to9 = AV146ContagemResultado_DataDmn_To9;
         AV276WP_CaixaDmnDS_118_Contagemresultado_dataprevista9 = AV147ContagemResultado_DataPrevista9;
         AV277WP_CaixaDmnDS_119_Contagemresultado_dataprevista_to9 = AV148ContagemResultado_DataPrevista_To9;
         AV278WP_CaixaDmnDS_120_Contagemresultado_contratadacod9 = AV149ContagemResultado_ContratadaCod9;
         AV279WP_CaixaDmnDS_121_Contagemresultado_servico9 = AV150ContagemResultado_Servico9;
         AV280WP_CaixaDmnDS_122_Contagemresultado_sistemacod9 = AV151ContagemResultado_SistemaCod9;
         AV281WP_CaixaDmnDS_123_Contagemresultado_responsavel9 = AV152ContagemResultado_Responsavel9;
         AV282WP_CaixaDmnDS_124_Contagemresultado_agrupador9 = AV153ContagemResultado_Agrupador9;
         AV283WP_CaixaDmnDS_125_Contagemresultado_statusdmn9 = AV154ContagemResultado_StatusDmn9;
         AV284WP_CaixaDmnDS_126_Tfcontagemresultado_demanda = AV10TFContagemResultado_Demanda;
         AV285WP_CaixaDmnDS_127_Tfcontagemresultado_demanda_sel = AV11TFContagemResultado_Demanda_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV159WP_CaixaDmnDS_1_Dynamicfiltersselector1 ,
                                              AV160WP_CaixaDmnDS_2_Dynamicfiltersoperator1 ,
                                              AV161WP_CaixaDmnDS_3_Contagemresultado_demanda1 ,
                                              AV162WP_CaixaDmnDS_4_Contagemresultado_datadmn1 ,
                                              AV163WP_CaixaDmnDS_5_Contagemresultado_datadmn_to1 ,
                                              AV164WP_CaixaDmnDS_6_Contagemresultado_dataprevista1 ,
                                              AV165WP_CaixaDmnDS_7_Contagemresultado_dataprevista_to1 ,
                                              AV166WP_CaixaDmnDS_8_Contagemresultado_contratadacod1 ,
                                              AV167WP_CaixaDmnDS_9_Contagemresultado_servico1 ,
                                              AV168WP_CaixaDmnDS_10_Contagemresultado_sistemacod1 ,
                                              AV169WP_CaixaDmnDS_11_Contagemresultado_responsavel1 ,
                                              AV170WP_CaixaDmnDS_12_Contagemresultado_agrupador1 ,
                                              AV171WP_CaixaDmnDS_13_Contagemresultado_statusdmn1 ,
                                              AV172WP_CaixaDmnDS_14_Dynamicfiltersenabled2 ,
                                              AV173WP_CaixaDmnDS_15_Dynamicfiltersselector2 ,
                                              AV174WP_CaixaDmnDS_16_Dynamicfiltersoperator2 ,
                                              AV175WP_CaixaDmnDS_17_Contagemresultado_demanda2 ,
                                              AV176WP_CaixaDmnDS_18_Contagemresultado_datadmn2 ,
                                              AV177WP_CaixaDmnDS_19_Contagemresultado_datadmn_to2 ,
                                              AV178WP_CaixaDmnDS_20_Contagemresultado_dataprevista2 ,
                                              AV179WP_CaixaDmnDS_21_Contagemresultado_dataprevista_to2 ,
                                              AV180WP_CaixaDmnDS_22_Contagemresultado_contratadacod2 ,
                                              AV181WP_CaixaDmnDS_23_Contagemresultado_servico2 ,
                                              AV182WP_CaixaDmnDS_24_Contagemresultado_sistemacod2 ,
                                              AV183WP_CaixaDmnDS_25_Contagemresultado_responsavel2 ,
                                              AV184WP_CaixaDmnDS_26_Contagemresultado_agrupador2 ,
                                              AV185WP_CaixaDmnDS_27_Contagemresultado_statusdmn2 ,
                                              AV186WP_CaixaDmnDS_28_Dynamicfiltersenabled3 ,
                                              AV187WP_CaixaDmnDS_29_Dynamicfiltersselector3 ,
                                              AV188WP_CaixaDmnDS_30_Dynamicfiltersoperator3 ,
                                              AV189WP_CaixaDmnDS_31_Contagemresultado_demanda3 ,
                                              AV190WP_CaixaDmnDS_32_Contagemresultado_datadmn3 ,
                                              AV191WP_CaixaDmnDS_33_Contagemresultado_datadmn_to3 ,
                                              AV192WP_CaixaDmnDS_34_Contagemresultado_dataprevista3 ,
                                              AV193WP_CaixaDmnDS_35_Contagemresultado_dataprevista_to3 ,
                                              AV194WP_CaixaDmnDS_36_Contagemresultado_contratadacod3 ,
                                              AV195WP_CaixaDmnDS_37_Contagemresultado_servico3 ,
                                              AV196WP_CaixaDmnDS_38_Contagemresultado_sistemacod3 ,
                                              AV197WP_CaixaDmnDS_39_Contagemresultado_responsavel3 ,
                                              AV198WP_CaixaDmnDS_40_Contagemresultado_agrupador3 ,
                                              AV199WP_CaixaDmnDS_41_Contagemresultado_statusdmn3 ,
                                              AV200WP_CaixaDmnDS_42_Dynamicfiltersenabled4 ,
                                              AV201WP_CaixaDmnDS_43_Dynamicfiltersselector4 ,
                                              AV202WP_CaixaDmnDS_44_Dynamicfiltersoperator4 ,
                                              AV203WP_CaixaDmnDS_45_Contagemresultado_demanda4 ,
                                              AV204WP_CaixaDmnDS_46_Contagemresultado_datadmn4 ,
                                              AV205WP_CaixaDmnDS_47_Contagemresultado_datadmn_to4 ,
                                              AV206WP_CaixaDmnDS_48_Contagemresultado_dataprevista4 ,
                                              AV207WP_CaixaDmnDS_49_Contagemresultado_dataprevista_to4 ,
                                              AV208WP_CaixaDmnDS_50_Contagemresultado_contratadacod4 ,
                                              AV209WP_CaixaDmnDS_51_Contagemresultado_servico4 ,
                                              AV210WP_CaixaDmnDS_52_Contagemresultado_sistemacod4 ,
                                              AV211WP_CaixaDmnDS_53_Contagemresultado_responsavel4 ,
                                              AV212WP_CaixaDmnDS_54_Contagemresultado_agrupador4 ,
                                              AV213WP_CaixaDmnDS_55_Contagemresultado_statusdmn4 ,
                                              AV214WP_CaixaDmnDS_56_Dynamicfiltersenabled5 ,
                                              AV215WP_CaixaDmnDS_57_Dynamicfiltersselector5 ,
                                              AV216WP_CaixaDmnDS_58_Dynamicfiltersoperator5 ,
                                              AV217WP_CaixaDmnDS_59_Contagemresultado_demanda5 ,
                                              AV218WP_CaixaDmnDS_60_Contagemresultado_datadmn5 ,
                                              AV219WP_CaixaDmnDS_61_Contagemresultado_datadmn_to5 ,
                                              AV220WP_CaixaDmnDS_62_Contagemresultado_dataprevista5 ,
                                              AV221WP_CaixaDmnDS_63_Contagemresultado_dataprevista_to5 ,
                                              AV222WP_CaixaDmnDS_64_Contagemresultado_contratadacod5 ,
                                              AV223WP_CaixaDmnDS_65_Contagemresultado_servico5 ,
                                              AV224WP_CaixaDmnDS_66_Contagemresultado_sistemacod5 ,
                                              AV225WP_CaixaDmnDS_67_Contagemresultado_responsavel5 ,
                                              AV226WP_CaixaDmnDS_68_Contagemresultado_agrupador5 ,
                                              AV227WP_CaixaDmnDS_69_Contagemresultado_statusdmn5 ,
                                              AV228WP_CaixaDmnDS_70_Dynamicfiltersenabled6 ,
                                              AV229WP_CaixaDmnDS_71_Dynamicfiltersselector6 ,
                                              AV230WP_CaixaDmnDS_72_Dynamicfiltersoperator6 ,
                                              AV231WP_CaixaDmnDS_73_Contagemresultado_demanda6 ,
                                              AV232WP_CaixaDmnDS_74_Contagemresultado_datadmn6 ,
                                              AV233WP_CaixaDmnDS_75_Contagemresultado_datadmn_to6 ,
                                              AV234WP_CaixaDmnDS_76_Contagemresultado_dataprevista6 ,
                                              AV235WP_CaixaDmnDS_77_Contagemresultado_dataprevista_to6 ,
                                              AV236WP_CaixaDmnDS_78_Contagemresultado_contratadacod6 ,
                                              AV237WP_CaixaDmnDS_79_Contagemresultado_servico6 ,
                                              AV238WP_CaixaDmnDS_80_Contagemresultado_sistemacod6 ,
                                              AV239WP_CaixaDmnDS_81_Contagemresultado_responsavel6 ,
                                              AV240WP_CaixaDmnDS_82_Contagemresultado_agrupador6 ,
                                              AV241WP_CaixaDmnDS_83_Contagemresultado_statusdmn6 ,
                                              AV242WP_CaixaDmnDS_84_Dynamicfiltersenabled7 ,
                                              AV243WP_CaixaDmnDS_85_Dynamicfiltersselector7 ,
                                              AV244WP_CaixaDmnDS_86_Dynamicfiltersoperator7 ,
                                              AV245WP_CaixaDmnDS_87_Contagemresultado_demanda7 ,
                                              AV246WP_CaixaDmnDS_88_Contagemresultado_datadmn7 ,
                                              AV247WP_CaixaDmnDS_89_Contagemresultado_datadmn_to7 ,
                                              AV248WP_CaixaDmnDS_90_Contagemresultado_dataprevista7 ,
                                              AV249WP_CaixaDmnDS_91_Contagemresultado_dataprevista_to7 ,
                                              AV250WP_CaixaDmnDS_92_Contagemresultado_contratadacod7 ,
                                              AV251WP_CaixaDmnDS_93_Contagemresultado_servico7 ,
                                              AV252WP_CaixaDmnDS_94_Contagemresultado_sistemacod7 ,
                                              AV253WP_CaixaDmnDS_95_Contagemresultado_responsavel7 ,
                                              AV254WP_CaixaDmnDS_96_Contagemresultado_agrupador7 ,
                                              AV255WP_CaixaDmnDS_97_Contagemresultado_statusdmn7 ,
                                              AV256WP_CaixaDmnDS_98_Dynamicfiltersenabled8 ,
                                              AV257WP_CaixaDmnDS_99_Dynamicfiltersselector8 ,
                                              AV258WP_CaixaDmnDS_100_Dynamicfiltersoperator8 ,
                                              AV259WP_CaixaDmnDS_101_Contagemresultado_demanda8 ,
                                              AV260WP_CaixaDmnDS_102_Contagemresultado_datadmn8 ,
                                              AV261WP_CaixaDmnDS_103_Contagemresultado_datadmn_to8 ,
                                              AV262WP_CaixaDmnDS_104_Contagemresultado_dataprevista8 ,
                                              AV263WP_CaixaDmnDS_105_Contagemresultado_dataprevista_to8 ,
                                              AV264WP_CaixaDmnDS_106_Contagemresultado_contratadacod8 ,
                                              AV265WP_CaixaDmnDS_107_Contagemresultado_servico8 ,
                                              AV266WP_CaixaDmnDS_108_Contagemresultado_sistemacod8 ,
                                              AV267WP_CaixaDmnDS_109_Contagemresultado_responsavel8 ,
                                              AV268WP_CaixaDmnDS_110_Contagemresultado_agrupador8 ,
                                              AV269WP_CaixaDmnDS_111_Contagemresultado_statusdmn8 ,
                                              AV270WP_CaixaDmnDS_112_Dynamicfiltersenabled9 ,
                                              AV271WP_CaixaDmnDS_113_Dynamicfiltersselector9 ,
                                              AV272WP_CaixaDmnDS_114_Dynamicfiltersoperator9 ,
                                              AV273WP_CaixaDmnDS_115_Contagemresultado_demanda9 ,
                                              AV274WP_CaixaDmnDS_116_Contagemresultado_datadmn9 ,
                                              AV275WP_CaixaDmnDS_117_Contagemresultado_datadmn_to9 ,
                                              AV276WP_CaixaDmnDS_118_Contagemresultado_dataprevista9 ,
                                              AV277WP_CaixaDmnDS_119_Contagemresultado_dataprevista_to9 ,
                                              AV278WP_CaixaDmnDS_120_Contagemresultado_contratadacod9 ,
                                              AV279WP_CaixaDmnDS_121_Contagemresultado_servico9 ,
                                              AV280WP_CaixaDmnDS_122_Contagemresultado_sistemacod9 ,
                                              AV281WP_CaixaDmnDS_123_Contagemresultado_responsavel9 ,
                                              AV282WP_CaixaDmnDS_124_Contagemresultado_agrupador9 ,
                                              AV283WP_CaixaDmnDS_125_Contagemresultado_statusdmn9 ,
                                              AV285WP_CaixaDmnDS_127_Tfcontagemresultado_demanda_sel ,
                                              AV284WP_CaixaDmnDS_126_Tfcontagemresultado_demanda ,
                                              A457ContagemResultado_Demanda ,
                                              A471ContagemResultado_DataDmn ,
                                              A1351ContagemResultado_DataPrevista ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A601ContagemResultado_Servico ,
                                              A489ContagemResultado_SistemaCod ,
                                              A890ContagemResultado_Responsavel ,
                                              A1046ContagemResultado_Agrupador ,
                                              A484ContagemResultado_StatusDmn },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV161WP_CaixaDmnDS_3_Contagemresultado_demanda1 = StringUtil.Concat( StringUtil.RTrim( AV161WP_CaixaDmnDS_3_Contagemresultado_demanda1), "%", "");
         lV161WP_CaixaDmnDS_3_Contagemresultado_demanda1 = StringUtil.Concat( StringUtil.RTrim( AV161WP_CaixaDmnDS_3_Contagemresultado_demanda1), "%", "");
         lV170WP_CaixaDmnDS_12_Contagemresultado_agrupador1 = StringUtil.PadR( StringUtil.RTrim( AV170WP_CaixaDmnDS_12_Contagemresultado_agrupador1), 15, "%");
         lV170WP_CaixaDmnDS_12_Contagemresultado_agrupador1 = StringUtil.PadR( StringUtil.RTrim( AV170WP_CaixaDmnDS_12_Contagemresultado_agrupador1), 15, "%");
         lV175WP_CaixaDmnDS_17_Contagemresultado_demanda2 = StringUtil.Concat( StringUtil.RTrim( AV175WP_CaixaDmnDS_17_Contagemresultado_demanda2), "%", "");
         lV175WP_CaixaDmnDS_17_Contagemresultado_demanda2 = StringUtil.Concat( StringUtil.RTrim( AV175WP_CaixaDmnDS_17_Contagemresultado_demanda2), "%", "");
         lV184WP_CaixaDmnDS_26_Contagemresultado_agrupador2 = StringUtil.PadR( StringUtil.RTrim( AV184WP_CaixaDmnDS_26_Contagemresultado_agrupador2), 15, "%");
         lV184WP_CaixaDmnDS_26_Contagemresultado_agrupador2 = StringUtil.PadR( StringUtil.RTrim( AV184WP_CaixaDmnDS_26_Contagemresultado_agrupador2), 15, "%");
         lV189WP_CaixaDmnDS_31_Contagemresultado_demanda3 = StringUtil.Concat( StringUtil.RTrim( AV189WP_CaixaDmnDS_31_Contagemresultado_demanda3), "%", "");
         lV189WP_CaixaDmnDS_31_Contagemresultado_demanda3 = StringUtil.Concat( StringUtil.RTrim( AV189WP_CaixaDmnDS_31_Contagemresultado_demanda3), "%", "");
         lV198WP_CaixaDmnDS_40_Contagemresultado_agrupador3 = StringUtil.PadR( StringUtil.RTrim( AV198WP_CaixaDmnDS_40_Contagemresultado_agrupador3), 15, "%");
         lV198WP_CaixaDmnDS_40_Contagemresultado_agrupador3 = StringUtil.PadR( StringUtil.RTrim( AV198WP_CaixaDmnDS_40_Contagemresultado_agrupador3), 15, "%");
         lV203WP_CaixaDmnDS_45_Contagemresultado_demanda4 = StringUtil.Concat( StringUtil.RTrim( AV203WP_CaixaDmnDS_45_Contagemresultado_demanda4), "%", "");
         lV203WP_CaixaDmnDS_45_Contagemresultado_demanda4 = StringUtil.Concat( StringUtil.RTrim( AV203WP_CaixaDmnDS_45_Contagemresultado_demanda4), "%", "");
         lV212WP_CaixaDmnDS_54_Contagemresultado_agrupador4 = StringUtil.PadR( StringUtil.RTrim( AV212WP_CaixaDmnDS_54_Contagemresultado_agrupador4), 15, "%");
         lV212WP_CaixaDmnDS_54_Contagemresultado_agrupador4 = StringUtil.PadR( StringUtil.RTrim( AV212WP_CaixaDmnDS_54_Contagemresultado_agrupador4), 15, "%");
         lV217WP_CaixaDmnDS_59_Contagemresultado_demanda5 = StringUtil.Concat( StringUtil.RTrim( AV217WP_CaixaDmnDS_59_Contagemresultado_demanda5), "%", "");
         lV217WP_CaixaDmnDS_59_Contagemresultado_demanda5 = StringUtil.Concat( StringUtil.RTrim( AV217WP_CaixaDmnDS_59_Contagemresultado_demanda5), "%", "");
         lV226WP_CaixaDmnDS_68_Contagemresultado_agrupador5 = StringUtil.PadR( StringUtil.RTrim( AV226WP_CaixaDmnDS_68_Contagemresultado_agrupador5), 15, "%");
         lV226WP_CaixaDmnDS_68_Contagemresultado_agrupador5 = StringUtil.PadR( StringUtil.RTrim( AV226WP_CaixaDmnDS_68_Contagemresultado_agrupador5), 15, "%");
         lV231WP_CaixaDmnDS_73_Contagemresultado_demanda6 = StringUtil.Concat( StringUtil.RTrim( AV231WP_CaixaDmnDS_73_Contagemresultado_demanda6), "%", "");
         lV231WP_CaixaDmnDS_73_Contagemresultado_demanda6 = StringUtil.Concat( StringUtil.RTrim( AV231WP_CaixaDmnDS_73_Contagemresultado_demanda6), "%", "");
         lV240WP_CaixaDmnDS_82_Contagemresultado_agrupador6 = StringUtil.PadR( StringUtil.RTrim( AV240WP_CaixaDmnDS_82_Contagemresultado_agrupador6), 15, "%");
         lV240WP_CaixaDmnDS_82_Contagemresultado_agrupador6 = StringUtil.PadR( StringUtil.RTrim( AV240WP_CaixaDmnDS_82_Contagemresultado_agrupador6), 15, "%");
         lV245WP_CaixaDmnDS_87_Contagemresultado_demanda7 = StringUtil.Concat( StringUtil.RTrim( AV245WP_CaixaDmnDS_87_Contagemresultado_demanda7), "%", "");
         lV245WP_CaixaDmnDS_87_Contagemresultado_demanda7 = StringUtil.Concat( StringUtil.RTrim( AV245WP_CaixaDmnDS_87_Contagemresultado_demanda7), "%", "");
         lV254WP_CaixaDmnDS_96_Contagemresultado_agrupador7 = StringUtil.PadR( StringUtil.RTrim( AV254WP_CaixaDmnDS_96_Contagemresultado_agrupador7), 15, "%");
         lV254WP_CaixaDmnDS_96_Contagemresultado_agrupador7 = StringUtil.PadR( StringUtil.RTrim( AV254WP_CaixaDmnDS_96_Contagemresultado_agrupador7), 15, "%");
         lV259WP_CaixaDmnDS_101_Contagemresultado_demanda8 = StringUtil.Concat( StringUtil.RTrim( AV259WP_CaixaDmnDS_101_Contagemresultado_demanda8), "%", "");
         lV259WP_CaixaDmnDS_101_Contagemresultado_demanda8 = StringUtil.Concat( StringUtil.RTrim( AV259WP_CaixaDmnDS_101_Contagemresultado_demanda8), "%", "");
         lV268WP_CaixaDmnDS_110_Contagemresultado_agrupador8 = StringUtil.PadR( StringUtil.RTrim( AV268WP_CaixaDmnDS_110_Contagemresultado_agrupador8), 15, "%");
         lV268WP_CaixaDmnDS_110_Contagemresultado_agrupador8 = StringUtil.PadR( StringUtil.RTrim( AV268WP_CaixaDmnDS_110_Contagemresultado_agrupador8), 15, "%");
         lV273WP_CaixaDmnDS_115_Contagemresultado_demanda9 = StringUtil.Concat( StringUtil.RTrim( AV273WP_CaixaDmnDS_115_Contagemresultado_demanda9), "%", "");
         lV273WP_CaixaDmnDS_115_Contagemresultado_demanda9 = StringUtil.Concat( StringUtil.RTrim( AV273WP_CaixaDmnDS_115_Contagemresultado_demanda9), "%", "");
         lV282WP_CaixaDmnDS_124_Contagemresultado_agrupador9 = StringUtil.PadR( StringUtil.RTrim( AV282WP_CaixaDmnDS_124_Contagemresultado_agrupador9), 15, "%");
         lV282WP_CaixaDmnDS_124_Contagemresultado_agrupador9 = StringUtil.PadR( StringUtil.RTrim( AV282WP_CaixaDmnDS_124_Contagemresultado_agrupador9), 15, "%");
         lV284WP_CaixaDmnDS_126_Tfcontagemresultado_demanda = StringUtil.Concat( StringUtil.RTrim( AV284WP_CaixaDmnDS_126_Tfcontagemresultado_demanda), "%", "");
         /* Using cursor P00V92 */
         pr_default.execute(0, new Object[] {AV161WP_CaixaDmnDS_3_Contagemresultado_demanda1, lV161WP_CaixaDmnDS_3_Contagemresultado_demanda1, lV161WP_CaixaDmnDS_3_Contagemresultado_demanda1, AV162WP_CaixaDmnDS_4_Contagemresultado_datadmn1, AV163WP_CaixaDmnDS_5_Contagemresultado_datadmn_to1, AV164WP_CaixaDmnDS_6_Contagemresultado_dataprevista1, AV165WP_CaixaDmnDS_7_Contagemresultado_dataprevista_to1, AV166WP_CaixaDmnDS_8_Contagemresultado_contratadacod1, AV167WP_CaixaDmnDS_9_Contagemresultado_servico1, AV168WP_CaixaDmnDS_10_Contagemresultado_sistemacod1, AV169WP_CaixaDmnDS_11_Contagemresultado_responsavel1, AV170WP_CaixaDmnDS_12_Contagemresultado_agrupador1, lV170WP_CaixaDmnDS_12_Contagemresultado_agrupador1, lV170WP_CaixaDmnDS_12_Contagemresultado_agrupador1, AV171WP_CaixaDmnDS_13_Contagemresultado_statusdmn1, AV175WP_CaixaDmnDS_17_Contagemresultado_demanda2, lV175WP_CaixaDmnDS_17_Contagemresultado_demanda2, lV175WP_CaixaDmnDS_17_Contagemresultado_demanda2, AV176WP_CaixaDmnDS_18_Contagemresultado_datadmn2, AV177WP_CaixaDmnDS_19_Contagemresultado_datadmn_to2, AV178WP_CaixaDmnDS_20_Contagemresultado_dataprevista2, AV179WP_CaixaDmnDS_21_Contagemresultado_dataprevista_to2, AV180WP_CaixaDmnDS_22_Contagemresultado_contratadacod2, AV181WP_CaixaDmnDS_23_Contagemresultado_servico2, AV182WP_CaixaDmnDS_24_Contagemresultado_sistemacod2, AV183WP_CaixaDmnDS_25_Contagemresultado_responsavel2, AV184WP_CaixaDmnDS_26_Contagemresultado_agrupador2, lV184WP_CaixaDmnDS_26_Contagemresultado_agrupador2, lV184WP_CaixaDmnDS_26_Contagemresultado_agrupador2, AV185WP_CaixaDmnDS_27_Contagemresultado_statusdmn2, AV189WP_CaixaDmnDS_31_Contagemresultado_demanda3, lV189WP_CaixaDmnDS_31_Contagemresultado_demanda3, lV189WP_CaixaDmnDS_31_Contagemresultado_demanda3, AV190WP_CaixaDmnDS_32_Contagemresultado_datadmn3, AV191WP_CaixaDmnDS_33_Contagemresultado_datadmn_to3, AV192WP_CaixaDmnDS_34_Contagemresultado_dataprevista3, AV193WP_CaixaDmnDS_35_Contagemresultado_dataprevista_to3, AV194WP_CaixaDmnDS_36_Contagemresultado_contratadacod3, AV195WP_CaixaDmnDS_37_Contagemresultado_servico3, AV196WP_CaixaDmnDS_38_Contagemresultado_sistemacod3, AV197WP_CaixaDmnDS_39_Contagemresultado_responsavel3, AV198WP_CaixaDmnDS_40_Contagemresultado_agrupador3, lV198WP_CaixaDmnDS_40_Contagemresultado_agrupador3, lV198WP_CaixaDmnDS_40_Contagemresultado_agrupador3, AV199WP_CaixaDmnDS_41_Contagemresultado_statusdmn3, AV203WP_CaixaDmnDS_45_Contagemresultado_demanda4, lV203WP_CaixaDmnDS_45_Contagemresultado_demanda4, lV203WP_CaixaDmnDS_45_Contagemresultado_demanda4, AV204WP_CaixaDmnDS_46_Contagemresultado_datadmn4, AV205WP_CaixaDmnDS_47_Contagemresultado_datadmn_to4, AV206WP_CaixaDmnDS_48_Contagemresultado_dataprevista4, AV207WP_CaixaDmnDS_49_Contagemresultado_dataprevista_to4, AV208WP_CaixaDmnDS_50_Contagemresultado_contratadacod4, AV209WP_CaixaDmnDS_51_Contagemresultado_servico4, AV210WP_CaixaDmnDS_52_Contagemresultado_sistemacod4, AV211WP_CaixaDmnDS_53_Contagemresultado_responsavel4, AV212WP_CaixaDmnDS_54_Contagemresultado_agrupador4, lV212WP_CaixaDmnDS_54_Contagemresultado_agrupador4, lV212WP_CaixaDmnDS_54_Contagemresultado_agrupador4, AV213WP_CaixaDmnDS_55_Contagemresultado_statusdmn4, AV217WP_CaixaDmnDS_59_Contagemresultado_demanda5, lV217WP_CaixaDmnDS_59_Contagemresultado_demanda5, lV217WP_CaixaDmnDS_59_Contagemresultado_demanda5, AV218WP_CaixaDmnDS_60_Contagemresultado_datadmn5, AV219WP_CaixaDmnDS_61_Contagemresultado_datadmn_to5, AV220WP_CaixaDmnDS_62_Contagemresultado_dataprevista5, AV221WP_CaixaDmnDS_63_Contagemresultado_dataprevista_to5, AV222WP_CaixaDmnDS_64_Contagemresultado_contratadacod5, AV223WP_CaixaDmnDS_65_Contagemresultado_servico5, AV224WP_CaixaDmnDS_66_Contagemresultado_sistemacod5, AV225WP_CaixaDmnDS_67_Contagemresultado_responsavel5, AV226WP_CaixaDmnDS_68_Contagemresultado_agrupador5, lV226WP_CaixaDmnDS_68_Contagemresultado_agrupador5, lV226WP_CaixaDmnDS_68_Contagemresultado_agrupador5, AV227WP_CaixaDmnDS_69_Contagemresultado_statusdmn5, AV231WP_CaixaDmnDS_73_Contagemresultado_demanda6, lV231WP_CaixaDmnDS_73_Contagemresultado_demanda6, lV231WP_CaixaDmnDS_73_Contagemresultado_demanda6, AV232WP_CaixaDmnDS_74_Contagemresultado_datadmn6, AV233WP_CaixaDmnDS_75_Contagemresultado_datadmn_to6, AV234WP_CaixaDmnDS_76_Contagemresultado_dataprevista6, AV235WP_CaixaDmnDS_77_Contagemresultado_dataprevista_to6, AV236WP_CaixaDmnDS_78_Contagemresultado_contratadacod6, AV237WP_CaixaDmnDS_79_Contagemresultado_servico6, AV238WP_CaixaDmnDS_80_Contagemresultado_sistemacod6, AV239WP_CaixaDmnDS_81_Contagemresultado_responsavel6, AV240WP_CaixaDmnDS_82_Contagemresultado_agrupador6, lV240WP_CaixaDmnDS_82_Contagemresultado_agrupador6, lV240WP_CaixaDmnDS_82_Contagemresultado_agrupador6, AV241WP_CaixaDmnDS_83_Contagemresultado_statusdmn6, AV245WP_CaixaDmnDS_87_Contagemresultado_demanda7, lV245WP_CaixaDmnDS_87_Contagemresultado_demanda7, lV245WP_CaixaDmnDS_87_Contagemresultado_demanda7, AV246WP_CaixaDmnDS_88_Contagemresultado_datadmn7, AV247WP_CaixaDmnDS_89_Contagemresultado_datadmn_to7, AV248WP_CaixaDmnDS_90_Contagemresultado_dataprevista7, AV249WP_CaixaDmnDS_91_Contagemresultado_dataprevista_to7, AV250WP_CaixaDmnDS_92_Contagemresultado_contratadacod7, AV251WP_CaixaDmnDS_93_Contagemresultado_servico7, AV252WP_CaixaDmnDS_94_Contagemresultado_sistemacod7, AV253WP_CaixaDmnDS_95_Contagemresultado_responsavel7, AV254WP_CaixaDmnDS_96_Contagemresultado_agrupador7, lV254WP_CaixaDmnDS_96_Contagemresultado_agrupador7, lV254WP_CaixaDmnDS_96_Contagemresultado_agrupador7, AV255WP_CaixaDmnDS_97_Contagemresultado_statusdmn7, AV259WP_CaixaDmnDS_101_Contagemresultado_demanda8, lV259WP_CaixaDmnDS_101_Contagemresultado_demanda8, lV259WP_CaixaDmnDS_101_Contagemresultado_demanda8, AV260WP_CaixaDmnDS_102_Contagemresultado_datadmn8, AV261WP_CaixaDmnDS_103_Contagemresultado_datadmn_to8, AV262WP_CaixaDmnDS_104_Contagemresultado_dataprevista8, AV263WP_CaixaDmnDS_105_Contagemresultado_dataprevista_to8, AV264WP_CaixaDmnDS_106_Contagemresultado_contratadacod8, AV265WP_CaixaDmnDS_107_Contagemresultado_servico8, AV266WP_CaixaDmnDS_108_Contagemresultado_sistemacod8, AV267WP_CaixaDmnDS_109_Contagemresultado_responsavel8, AV268WP_CaixaDmnDS_110_Contagemresultado_agrupador8, lV268WP_CaixaDmnDS_110_Contagemresultado_agrupador8, lV268WP_CaixaDmnDS_110_Contagemresultado_agrupador8, AV269WP_CaixaDmnDS_111_Contagemresultado_statusdmn8, AV273WP_CaixaDmnDS_115_Contagemresultado_demanda9, lV273WP_CaixaDmnDS_115_Contagemresultado_demanda9,
         lV273WP_CaixaDmnDS_115_Contagemresultado_demanda9, AV274WP_CaixaDmnDS_116_Contagemresultado_datadmn9, AV275WP_CaixaDmnDS_117_Contagemresultado_datadmn_to9, AV276WP_CaixaDmnDS_118_Contagemresultado_dataprevista9, AV277WP_CaixaDmnDS_119_Contagemresultado_dataprevista_to9, AV278WP_CaixaDmnDS_120_Contagemresultado_contratadacod9, AV279WP_CaixaDmnDS_121_Contagemresultado_servico9, AV280WP_CaixaDmnDS_122_Contagemresultado_sistemacod9, AV281WP_CaixaDmnDS_123_Contagemresultado_responsavel9, AV282WP_CaixaDmnDS_124_Contagemresultado_agrupador9, lV282WP_CaixaDmnDS_124_Contagemresultado_agrupador9, lV282WP_CaixaDmnDS_124_Contagemresultado_agrupador9, AV283WP_CaixaDmnDS_125_Contagemresultado_statusdmn9, lV284WP_CaixaDmnDS_126_Tfcontagemresultado_demanda, AV285WP_CaixaDmnDS_127_Tfcontagemresultado_demanda_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKV92 = false;
            A1553ContagemResultado_CntSrvCod = P00V92_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00V92_n1553ContagemResultado_CntSrvCod[0];
            A457ContagemResultado_Demanda = P00V92_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00V92_n457ContagemResultado_Demanda[0];
            A484ContagemResultado_StatusDmn = P00V92_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00V92_n484ContagemResultado_StatusDmn[0];
            A1046ContagemResultado_Agrupador = P00V92_A1046ContagemResultado_Agrupador[0];
            n1046ContagemResultado_Agrupador = P00V92_n1046ContagemResultado_Agrupador[0];
            A890ContagemResultado_Responsavel = P00V92_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = P00V92_n890ContagemResultado_Responsavel[0];
            A489ContagemResultado_SistemaCod = P00V92_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00V92_n489ContagemResultado_SistemaCod[0];
            A601ContagemResultado_Servico = P00V92_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00V92_n601ContagemResultado_Servico[0];
            A490ContagemResultado_ContratadaCod = P00V92_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00V92_n490ContagemResultado_ContratadaCod[0];
            A1351ContagemResultado_DataPrevista = P00V92_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = P00V92_n1351ContagemResultado_DataPrevista[0];
            A471ContagemResultado_DataDmn = P00V92_A471ContagemResultado_DataDmn[0];
            A456ContagemResultado_Codigo = P00V92_A456ContagemResultado_Codigo[0];
            A601ContagemResultado_Servico = P00V92_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00V92_n601ContagemResultado_Servico[0];
            AV24count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00V92_A457ContagemResultado_Demanda[0], A457ContagemResultado_Demanda) == 0 ) )
            {
               BRKV92 = false;
               A456ContagemResultado_Codigo = P00V92_A456ContagemResultado_Codigo[0];
               AV24count = (long)(AV24count+1);
               BRKV92 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A457ContagemResultado_Demanda)) )
            {
               AV16Option = A457ContagemResultado_Demanda;
               AV19OptionDesc = StringUtil.Trim( StringUtil.RTrim( context.localUtil.Format( A457ContagemResultado_Demanda, "@!")));
               AV17Options.Add(AV16Option, 0);
               AV20OptionsDesc.Add(AV19OptionDesc, 0);
               AV22OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV24count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV17Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKV92 )
            {
               BRKV92 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV17Options = new GxSimpleCollection();
         AV20OptionsDesc = new GxSimpleCollection();
         AV22OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV25Session = context.GetSession();
         AV27GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV28GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContagemResultado_Demanda = "";
         AV11TFContagemResultado_Demanda_Sel = "";
         AV29GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV30DynamicFiltersSelector1 = "";
         AV32ContagemResultado_Demanda1 = "";
         AV33ContagemResultado_DataDmn1 = DateTime.MinValue;
         AV34ContagemResultado_DataDmn_To1 = DateTime.MinValue;
         AV35ContagemResultado_DataPrevista1 = (DateTime)(DateTime.MinValue);
         AV36ContagemResultado_DataPrevista_To1 = (DateTime)(DateTime.MinValue);
         AV41ContagemResultado_Agrupador1 = "";
         AV42ContagemResultado_StatusDmn1 = "";
         AV44DynamicFiltersSelector2 = "";
         AV46ContagemResultado_Demanda2 = "";
         AV47ContagemResultado_DataDmn2 = DateTime.MinValue;
         AV48ContagemResultado_DataDmn_To2 = DateTime.MinValue;
         AV49ContagemResultado_DataPrevista2 = (DateTime)(DateTime.MinValue);
         AV50ContagemResultado_DataPrevista_To2 = (DateTime)(DateTime.MinValue);
         AV55ContagemResultado_Agrupador2 = "";
         AV56ContagemResultado_StatusDmn2 = "";
         AV58DynamicFiltersSelector3 = "";
         AV60ContagemResultado_Demanda3 = "";
         AV61ContagemResultado_DataDmn3 = DateTime.MinValue;
         AV62ContagemResultado_DataDmn_To3 = DateTime.MinValue;
         AV63ContagemResultado_DataPrevista3 = (DateTime)(DateTime.MinValue);
         AV64ContagemResultado_DataPrevista_To3 = (DateTime)(DateTime.MinValue);
         AV69ContagemResultado_Agrupador3 = "";
         AV70ContagemResultado_StatusDmn3 = "";
         AV72DynamicFiltersSelector4 = "";
         AV74ContagemResultado_Demanda4 = "";
         AV75ContagemResultado_DataDmn4 = DateTime.MinValue;
         AV76ContagemResultado_DataDmn_To4 = DateTime.MinValue;
         AV77ContagemResultado_DataPrevista4 = (DateTime)(DateTime.MinValue);
         AV78ContagemResultado_DataPrevista_To4 = (DateTime)(DateTime.MinValue);
         AV83ContagemResultado_Agrupador4 = "";
         AV84ContagemResultado_StatusDmn4 = "";
         AV86DynamicFiltersSelector5 = "";
         AV88ContagemResultado_Demanda5 = "";
         AV89ContagemResultado_DataDmn5 = DateTime.MinValue;
         AV90ContagemResultado_DataDmn_To5 = DateTime.MinValue;
         AV91ContagemResultado_DataPrevista5 = (DateTime)(DateTime.MinValue);
         AV92ContagemResultado_DataPrevista_To5 = (DateTime)(DateTime.MinValue);
         AV97ContagemResultado_Agrupador5 = "";
         AV98ContagemResultado_StatusDmn5 = "";
         AV100DynamicFiltersSelector6 = "";
         AV102ContagemResultado_Demanda6 = "";
         AV103ContagemResultado_DataDmn6 = DateTime.MinValue;
         AV104ContagemResultado_DataDmn_To6 = DateTime.MinValue;
         AV105ContagemResultado_DataPrevista6 = (DateTime)(DateTime.MinValue);
         AV106ContagemResultado_DataPrevista_To6 = (DateTime)(DateTime.MinValue);
         AV111ContagemResultado_Agrupador6 = "";
         AV112ContagemResultado_StatusDmn6 = "";
         AV114DynamicFiltersSelector7 = "";
         AV116ContagemResultado_Demanda7 = "";
         AV117ContagemResultado_DataDmn7 = DateTime.MinValue;
         AV118ContagemResultado_DataDmn_To7 = DateTime.MinValue;
         AV119ContagemResultado_DataPrevista7 = (DateTime)(DateTime.MinValue);
         AV120ContagemResultado_DataPrevista_To7 = (DateTime)(DateTime.MinValue);
         AV125ContagemResultado_Agrupador7 = "";
         AV126ContagemResultado_StatusDmn7 = "";
         AV128DynamicFiltersSelector8 = "";
         AV130ContagemResultado_Demanda8 = "";
         AV131ContagemResultado_DataDmn8 = DateTime.MinValue;
         AV132ContagemResultado_DataDmn_To8 = DateTime.MinValue;
         AV133ContagemResultado_DataPrevista8 = (DateTime)(DateTime.MinValue);
         AV134ContagemResultado_DataPrevista_To8 = (DateTime)(DateTime.MinValue);
         AV139ContagemResultado_Agrupador8 = "";
         AV140ContagemResultado_StatusDmn8 = "";
         AV142DynamicFiltersSelector9 = "";
         AV144ContagemResultado_Demanda9 = "";
         AV145ContagemResultado_DataDmn9 = DateTime.MinValue;
         AV146ContagemResultado_DataDmn_To9 = DateTime.MinValue;
         AV147ContagemResultado_DataPrevista9 = (DateTime)(DateTime.MinValue);
         AV148ContagemResultado_DataPrevista_To9 = (DateTime)(DateTime.MinValue);
         AV153ContagemResultado_Agrupador9 = "";
         AV154ContagemResultado_StatusDmn9 = "";
         AV159WP_CaixaDmnDS_1_Dynamicfiltersselector1 = "";
         AV161WP_CaixaDmnDS_3_Contagemresultado_demanda1 = "";
         AV162WP_CaixaDmnDS_4_Contagemresultado_datadmn1 = DateTime.MinValue;
         AV163WP_CaixaDmnDS_5_Contagemresultado_datadmn_to1 = DateTime.MinValue;
         AV164WP_CaixaDmnDS_6_Contagemresultado_dataprevista1 = (DateTime)(DateTime.MinValue);
         AV165WP_CaixaDmnDS_7_Contagemresultado_dataprevista_to1 = (DateTime)(DateTime.MinValue);
         AV170WP_CaixaDmnDS_12_Contagemresultado_agrupador1 = "";
         AV171WP_CaixaDmnDS_13_Contagemresultado_statusdmn1 = "";
         AV173WP_CaixaDmnDS_15_Dynamicfiltersselector2 = "";
         AV175WP_CaixaDmnDS_17_Contagemresultado_demanda2 = "";
         AV176WP_CaixaDmnDS_18_Contagemresultado_datadmn2 = DateTime.MinValue;
         AV177WP_CaixaDmnDS_19_Contagemresultado_datadmn_to2 = DateTime.MinValue;
         AV178WP_CaixaDmnDS_20_Contagemresultado_dataprevista2 = (DateTime)(DateTime.MinValue);
         AV179WP_CaixaDmnDS_21_Contagemresultado_dataprevista_to2 = (DateTime)(DateTime.MinValue);
         AV184WP_CaixaDmnDS_26_Contagemresultado_agrupador2 = "";
         AV185WP_CaixaDmnDS_27_Contagemresultado_statusdmn2 = "";
         AV187WP_CaixaDmnDS_29_Dynamicfiltersselector3 = "";
         AV189WP_CaixaDmnDS_31_Contagemresultado_demanda3 = "";
         AV190WP_CaixaDmnDS_32_Contagemresultado_datadmn3 = DateTime.MinValue;
         AV191WP_CaixaDmnDS_33_Contagemresultado_datadmn_to3 = DateTime.MinValue;
         AV192WP_CaixaDmnDS_34_Contagemresultado_dataprevista3 = (DateTime)(DateTime.MinValue);
         AV193WP_CaixaDmnDS_35_Contagemresultado_dataprevista_to3 = (DateTime)(DateTime.MinValue);
         AV198WP_CaixaDmnDS_40_Contagemresultado_agrupador3 = "";
         AV199WP_CaixaDmnDS_41_Contagemresultado_statusdmn3 = "";
         AV201WP_CaixaDmnDS_43_Dynamicfiltersselector4 = "";
         AV203WP_CaixaDmnDS_45_Contagemresultado_demanda4 = "";
         AV204WP_CaixaDmnDS_46_Contagemresultado_datadmn4 = DateTime.MinValue;
         AV205WP_CaixaDmnDS_47_Contagemresultado_datadmn_to4 = DateTime.MinValue;
         AV206WP_CaixaDmnDS_48_Contagemresultado_dataprevista4 = (DateTime)(DateTime.MinValue);
         AV207WP_CaixaDmnDS_49_Contagemresultado_dataprevista_to4 = (DateTime)(DateTime.MinValue);
         AV212WP_CaixaDmnDS_54_Contagemresultado_agrupador4 = "";
         AV213WP_CaixaDmnDS_55_Contagemresultado_statusdmn4 = "";
         AV215WP_CaixaDmnDS_57_Dynamicfiltersselector5 = "";
         AV217WP_CaixaDmnDS_59_Contagemresultado_demanda5 = "";
         AV218WP_CaixaDmnDS_60_Contagemresultado_datadmn5 = DateTime.MinValue;
         AV219WP_CaixaDmnDS_61_Contagemresultado_datadmn_to5 = DateTime.MinValue;
         AV220WP_CaixaDmnDS_62_Contagemresultado_dataprevista5 = (DateTime)(DateTime.MinValue);
         AV221WP_CaixaDmnDS_63_Contagemresultado_dataprevista_to5 = (DateTime)(DateTime.MinValue);
         AV226WP_CaixaDmnDS_68_Contagemresultado_agrupador5 = "";
         AV227WP_CaixaDmnDS_69_Contagemresultado_statusdmn5 = "";
         AV229WP_CaixaDmnDS_71_Dynamicfiltersselector6 = "";
         AV231WP_CaixaDmnDS_73_Contagemresultado_demanda6 = "";
         AV232WP_CaixaDmnDS_74_Contagemresultado_datadmn6 = DateTime.MinValue;
         AV233WP_CaixaDmnDS_75_Contagemresultado_datadmn_to6 = DateTime.MinValue;
         AV234WP_CaixaDmnDS_76_Contagemresultado_dataprevista6 = (DateTime)(DateTime.MinValue);
         AV235WP_CaixaDmnDS_77_Contagemresultado_dataprevista_to6 = (DateTime)(DateTime.MinValue);
         AV240WP_CaixaDmnDS_82_Contagemresultado_agrupador6 = "";
         AV241WP_CaixaDmnDS_83_Contagemresultado_statusdmn6 = "";
         AV243WP_CaixaDmnDS_85_Dynamicfiltersselector7 = "";
         AV245WP_CaixaDmnDS_87_Contagemresultado_demanda7 = "";
         AV246WP_CaixaDmnDS_88_Contagemresultado_datadmn7 = DateTime.MinValue;
         AV247WP_CaixaDmnDS_89_Contagemresultado_datadmn_to7 = DateTime.MinValue;
         AV248WP_CaixaDmnDS_90_Contagemresultado_dataprevista7 = (DateTime)(DateTime.MinValue);
         AV249WP_CaixaDmnDS_91_Contagemresultado_dataprevista_to7 = (DateTime)(DateTime.MinValue);
         AV254WP_CaixaDmnDS_96_Contagemresultado_agrupador7 = "";
         AV255WP_CaixaDmnDS_97_Contagemresultado_statusdmn7 = "";
         AV257WP_CaixaDmnDS_99_Dynamicfiltersselector8 = "";
         AV259WP_CaixaDmnDS_101_Contagemresultado_demanda8 = "";
         AV260WP_CaixaDmnDS_102_Contagemresultado_datadmn8 = DateTime.MinValue;
         AV261WP_CaixaDmnDS_103_Contagemresultado_datadmn_to8 = DateTime.MinValue;
         AV262WP_CaixaDmnDS_104_Contagemresultado_dataprevista8 = (DateTime)(DateTime.MinValue);
         AV263WP_CaixaDmnDS_105_Contagemresultado_dataprevista_to8 = (DateTime)(DateTime.MinValue);
         AV268WP_CaixaDmnDS_110_Contagemresultado_agrupador8 = "";
         AV269WP_CaixaDmnDS_111_Contagemresultado_statusdmn8 = "";
         AV271WP_CaixaDmnDS_113_Dynamicfiltersselector9 = "";
         AV273WP_CaixaDmnDS_115_Contagemresultado_demanda9 = "";
         AV274WP_CaixaDmnDS_116_Contagemresultado_datadmn9 = DateTime.MinValue;
         AV275WP_CaixaDmnDS_117_Contagemresultado_datadmn_to9 = DateTime.MinValue;
         AV276WP_CaixaDmnDS_118_Contagemresultado_dataprevista9 = (DateTime)(DateTime.MinValue);
         AV277WP_CaixaDmnDS_119_Contagemresultado_dataprevista_to9 = (DateTime)(DateTime.MinValue);
         AV282WP_CaixaDmnDS_124_Contagemresultado_agrupador9 = "";
         AV283WP_CaixaDmnDS_125_Contagemresultado_statusdmn9 = "";
         AV284WP_CaixaDmnDS_126_Tfcontagemresultado_demanda = "";
         AV285WP_CaixaDmnDS_127_Tfcontagemresultado_demanda_sel = "";
         scmdbuf = "";
         lV161WP_CaixaDmnDS_3_Contagemresultado_demanda1 = "";
         lV170WP_CaixaDmnDS_12_Contagemresultado_agrupador1 = "";
         lV175WP_CaixaDmnDS_17_Contagemresultado_demanda2 = "";
         lV184WP_CaixaDmnDS_26_Contagemresultado_agrupador2 = "";
         lV189WP_CaixaDmnDS_31_Contagemresultado_demanda3 = "";
         lV198WP_CaixaDmnDS_40_Contagemresultado_agrupador3 = "";
         lV203WP_CaixaDmnDS_45_Contagemresultado_demanda4 = "";
         lV212WP_CaixaDmnDS_54_Contagemresultado_agrupador4 = "";
         lV217WP_CaixaDmnDS_59_Contagemresultado_demanda5 = "";
         lV226WP_CaixaDmnDS_68_Contagemresultado_agrupador5 = "";
         lV231WP_CaixaDmnDS_73_Contagemresultado_demanda6 = "";
         lV240WP_CaixaDmnDS_82_Contagemresultado_agrupador6 = "";
         lV245WP_CaixaDmnDS_87_Contagemresultado_demanda7 = "";
         lV254WP_CaixaDmnDS_96_Contagemresultado_agrupador7 = "";
         lV259WP_CaixaDmnDS_101_Contagemresultado_demanda8 = "";
         lV268WP_CaixaDmnDS_110_Contagemresultado_agrupador8 = "";
         lV273WP_CaixaDmnDS_115_Contagemresultado_demanda9 = "";
         lV282WP_CaixaDmnDS_124_Contagemresultado_agrupador9 = "";
         lV284WP_CaixaDmnDS_126_Tfcontagemresultado_demanda = "";
         A457ContagemResultado_Demanda = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         A1046ContagemResultado_Agrupador = "";
         A484ContagemResultado_StatusDmn = "";
         P00V92_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00V92_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00V92_A457ContagemResultado_Demanda = new String[] {""} ;
         P00V92_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00V92_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00V92_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00V92_A1046ContagemResultado_Agrupador = new String[] {""} ;
         P00V92_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         P00V92_A890ContagemResultado_Responsavel = new int[1] ;
         P00V92_n890ContagemResultado_Responsavel = new bool[] {false} ;
         P00V92_A489ContagemResultado_SistemaCod = new int[1] ;
         P00V92_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00V92_A601ContagemResultado_Servico = new int[1] ;
         P00V92_n601ContagemResultado_Servico = new bool[] {false} ;
         P00V92_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00V92_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00V92_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P00V92_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P00V92_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00V92_A456ContagemResultado_Codigo = new int[1] ;
         AV16Option = "";
         AV19OptionDesc = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwp_caixadmnfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00V92_A1553ContagemResultado_CntSrvCod, P00V92_n1553ContagemResultado_CntSrvCod, P00V92_A457ContagemResultado_Demanda, P00V92_n457ContagemResultado_Demanda, P00V92_A484ContagemResultado_StatusDmn, P00V92_n484ContagemResultado_StatusDmn, P00V92_A1046ContagemResultado_Agrupador, P00V92_n1046ContagemResultado_Agrupador, P00V92_A890ContagemResultado_Responsavel, P00V92_n890ContagemResultado_Responsavel,
               P00V92_A489ContagemResultado_SistemaCod, P00V92_n489ContagemResultado_SistemaCod, P00V92_A601ContagemResultado_Servico, P00V92_n601ContagemResultado_Servico, P00V92_A490ContagemResultado_ContratadaCod, P00V92_n490ContagemResultado_ContratadaCod, P00V92_A1351ContagemResultado_DataPrevista, P00V92_n1351ContagemResultado_DataPrevista, P00V92_A471ContagemResultado_DataDmn, P00V92_A456ContagemResultado_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV31DynamicFiltersOperator1 ;
      private short AV45DynamicFiltersOperator2 ;
      private short AV59DynamicFiltersOperator3 ;
      private short AV73DynamicFiltersOperator4 ;
      private short AV87DynamicFiltersOperator5 ;
      private short AV101DynamicFiltersOperator6 ;
      private short AV115DynamicFiltersOperator7 ;
      private short AV129DynamicFiltersOperator8 ;
      private short AV143DynamicFiltersOperator9 ;
      private short AV160WP_CaixaDmnDS_2_Dynamicfiltersoperator1 ;
      private short AV174WP_CaixaDmnDS_16_Dynamicfiltersoperator2 ;
      private short AV188WP_CaixaDmnDS_30_Dynamicfiltersoperator3 ;
      private short AV202WP_CaixaDmnDS_44_Dynamicfiltersoperator4 ;
      private short AV216WP_CaixaDmnDS_58_Dynamicfiltersoperator5 ;
      private short AV230WP_CaixaDmnDS_72_Dynamicfiltersoperator6 ;
      private short AV244WP_CaixaDmnDS_86_Dynamicfiltersoperator7 ;
      private short AV258WP_CaixaDmnDS_100_Dynamicfiltersoperator8 ;
      private short AV272WP_CaixaDmnDS_114_Dynamicfiltersoperator9 ;
      private int AV157GXV1 ;
      private int AV37ContagemResultado_ContratadaCod1 ;
      private int AV38ContagemResultado_Servico1 ;
      private int AV39ContagemResultado_SistemaCod1 ;
      private int AV40ContagemResultado_Responsavel1 ;
      private int AV51ContagemResultado_ContratadaCod2 ;
      private int AV52ContagemResultado_Servico2 ;
      private int AV53ContagemResultado_SistemaCod2 ;
      private int AV54ContagemResultado_Responsavel2 ;
      private int AV65ContagemResultado_ContratadaCod3 ;
      private int AV66ContagemResultado_Servico3 ;
      private int AV67ContagemResultado_SistemaCod3 ;
      private int AV68ContagemResultado_Responsavel3 ;
      private int AV79ContagemResultado_ContratadaCod4 ;
      private int AV80ContagemResultado_Servico4 ;
      private int AV81ContagemResultado_SistemaCod4 ;
      private int AV82ContagemResultado_Responsavel4 ;
      private int AV93ContagemResultado_ContratadaCod5 ;
      private int AV94ContagemResultado_Servico5 ;
      private int AV95ContagemResultado_SistemaCod5 ;
      private int AV96ContagemResultado_Responsavel5 ;
      private int AV107ContagemResultado_ContratadaCod6 ;
      private int AV108ContagemResultado_Servico6 ;
      private int AV109ContagemResultado_SistemaCod6 ;
      private int AV110ContagemResultado_Responsavel6 ;
      private int AV121ContagemResultado_ContratadaCod7 ;
      private int AV122ContagemResultado_Servico7 ;
      private int AV123ContagemResultado_SistemaCod7 ;
      private int AV124ContagemResultado_Responsavel7 ;
      private int AV135ContagemResultado_ContratadaCod8 ;
      private int AV136ContagemResultado_Servico8 ;
      private int AV137ContagemResultado_SistemaCod8 ;
      private int AV138ContagemResultado_Responsavel8 ;
      private int AV149ContagemResultado_ContratadaCod9 ;
      private int AV150ContagemResultado_Servico9 ;
      private int AV151ContagemResultado_SistemaCod9 ;
      private int AV152ContagemResultado_Responsavel9 ;
      private int AV166WP_CaixaDmnDS_8_Contagemresultado_contratadacod1 ;
      private int AV167WP_CaixaDmnDS_9_Contagemresultado_servico1 ;
      private int AV168WP_CaixaDmnDS_10_Contagemresultado_sistemacod1 ;
      private int AV169WP_CaixaDmnDS_11_Contagemresultado_responsavel1 ;
      private int AV180WP_CaixaDmnDS_22_Contagemresultado_contratadacod2 ;
      private int AV181WP_CaixaDmnDS_23_Contagemresultado_servico2 ;
      private int AV182WP_CaixaDmnDS_24_Contagemresultado_sistemacod2 ;
      private int AV183WP_CaixaDmnDS_25_Contagemresultado_responsavel2 ;
      private int AV194WP_CaixaDmnDS_36_Contagemresultado_contratadacod3 ;
      private int AV195WP_CaixaDmnDS_37_Contagemresultado_servico3 ;
      private int AV196WP_CaixaDmnDS_38_Contagemresultado_sistemacod3 ;
      private int AV197WP_CaixaDmnDS_39_Contagemresultado_responsavel3 ;
      private int AV208WP_CaixaDmnDS_50_Contagemresultado_contratadacod4 ;
      private int AV209WP_CaixaDmnDS_51_Contagemresultado_servico4 ;
      private int AV210WP_CaixaDmnDS_52_Contagemresultado_sistemacod4 ;
      private int AV211WP_CaixaDmnDS_53_Contagemresultado_responsavel4 ;
      private int AV222WP_CaixaDmnDS_64_Contagemresultado_contratadacod5 ;
      private int AV223WP_CaixaDmnDS_65_Contagemresultado_servico5 ;
      private int AV224WP_CaixaDmnDS_66_Contagemresultado_sistemacod5 ;
      private int AV225WP_CaixaDmnDS_67_Contagemresultado_responsavel5 ;
      private int AV236WP_CaixaDmnDS_78_Contagemresultado_contratadacod6 ;
      private int AV237WP_CaixaDmnDS_79_Contagemresultado_servico6 ;
      private int AV238WP_CaixaDmnDS_80_Contagemresultado_sistemacod6 ;
      private int AV239WP_CaixaDmnDS_81_Contagemresultado_responsavel6 ;
      private int AV250WP_CaixaDmnDS_92_Contagemresultado_contratadacod7 ;
      private int AV251WP_CaixaDmnDS_93_Contagemresultado_servico7 ;
      private int AV252WP_CaixaDmnDS_94_Contagemresultado_sistemacod7 ;
      private int AV253WP_CaixaDmnDS_95_Contagemresultado_responsavel7 ;
      private int AV264WP_CaixaDmnDS_106_Contagemresultado_contratadacod8 ;
      private int AV265WP_CaixaDmnDS_107_Contagemresultado_servico8 ;
      private int AV266WP_CaixaDmnDS_108_Contagemresultado_sistemacod8 ;
      private int AV267WP_CaixaDmnDS_109_Contagemresultado_responsavel8 ;
      private int AV278WP_CaixaDmnDS_120_Contagemresultado_contratadacod9 ;
      private int AV279WP_CaixaDmnDS_121_Contagemresultado_servico9 ;
      private int AV280WP_CaixaDmnDS_122_Contagemresultado_sistemacod9 ;
      private int AV281WP_CaixaDmnDS_123_Contagemresultado_responsavel9 ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A601ContagemResultado_Servico ;
      private int A489ContagemResultado_SistemaCod ;
      private int A890ContagemResultado_Responsavel ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A456ContagemResultado_Codigo ;
      private long AV24count ;
      private String AV41ContagemResultado_Agrupador1 ;
      private String AV42ContagemResultado_StatusDmn1 ;
      private String AV55ContagemResultado_Agrupador2 ;
      private String AV56ContagemResultado_StatusDmn2 ;
      private String AV69ContagemResultado_Agrupador3 ;
      private String AV70ContagemResultado_StatusDmn3 ;
      private String AV83ContagemResultado_Agrupador4 ;
      private String AV84ContagemResultado_StatusDmn4 ;
      private String AV97ContagemResultado_Agrupador5 ;
      private String AV98ContagemResultado_StatusDmn5 ;
      private String AV111ContagemResultado_Agrupador6 ;
      private String AV112ContagemResultado_StatusDmn6 ;
      private String AV125ContagemResultado_Agrupador7 ;
      private String AV126ContagemResultado_StatusDmn7 ;
      private String AV139ContagemResultado_Agrupador8 ;
      private String AV140ContagemResultado_StatusDmn8 ;
      private String AV153ContagemResultado_Agrupador9 ;
      private String AV154ContagemResultado_StatusDmn9 ;
      private String AV170WP_CaixaDmnDS_12_Contagemresultado_agrupador1 ;
      private String AV171WP_CaixaDmnDS_13_Contagemresultado_statusdmn1 ;
      private String AV184WP_CaixaDmnDS_26_Contagemresultado_agrupador2 ;
      private String AV185WP_CaixaDmnDS_27_Contagemresultado_statusdmn2 ;
      private String AV198WP_CaixaDmnDS_40_Contagemresultado_agrupador3 ;
      private String AV199WP_CaixaDmnDS_41_Contagemresultado_statusdmn3 ;
      private String AV212WP_CaixaDmnDS_54_Contagemresultado_agrupador4 ;
      private String AV213WP_CaixaDmnDS_55_Contagemresultado_statusdmn4 ;
      private String AV226WP_CaixaDmnDS_68_Contagemresultado_agrupador5 ;
      private String AV227WP_CaixaDmnDS_69_Contagemresultado_statusdmn5 ;
      private String AV240WP_CaixaDmnDS_82_Contagemresultado_agrupador6 ;
      private String AV241WP_CaixaDmnDS_83_Contagemresultado_statusdmn6 ;
      private String AV254WP_CaixaDmnDS_96_Contagemresultado_agrupador7 ;
      private String AV255WP_CaixaDmnDS_97_Contagemresultado_statusdmn7 ;
      private String AV268WP_CaixaDmnDS_110_Contagemresultado_agrupador8 ;
      private String AV269WP_CaixaDmnDS_111_Contagemresultado_statusdmn8 ;
      private String AV282WP_CaixaDmnDS_124_Contagemresultado_agrupador9 ;
      private String AV283WP_CaixaDmnDS_125_Contagemresultado_statusdmn9 ;
      private String scmdbuf ;
      private String lV170WP_CaixaDmnDS_12_Contagemresultado_agrupador1 ;
      private String lV184WP_CaixaDmnDS_26_Contagemresultado_agrupador2 ;
      private String lV198WP_CaixaDmnDS_40_Contagemresultado_agrupador3 ;
      private String lV212WP_CaixaDmnDS_54_Contagemresultado_agrupador4 ;
      private String lV226WP_CaixaDmnDS_68_Contagemresultado_agrupador5 ;
      private String lV240WP_CaixaDmnDS_82_Contagemresultado_agrupador6 ;
      private String lV254WP_CaixaDmnDS_96_Contagemresultado_agrupador7 ;
      private String lV268WP_CaixaDmnDS_110_Contagemresultado_agrupador8 ;
      private String lV282WP_CaixaDmnDS_124_Contagemresultado_agrupador9 ;
      private String A1046ContagemResultado_Agrupador ;
      private String A484ContagemResultado_StatusDmn ;
      private DateTime AV35ContagemResultado_DataPrevista1 ;
      private DateTime AV36ContagemResultado_DataPrevista_To1 ;
      private DateTime AV49ContagemResultado_DataPrevista2 ;
      private DateTime AV50ContagemResultado_DataPrevista_To2 ;
      private DateTime AV63ContagemResultado_DataPrevista3 ;
      private DateTime AV64ContagemResultado_DataPrevista_To3 ;
      private DateTime AV77ContagemResultado_DataPrevista4 ;
      private DateTime AV78ContagemResultado_DataPrevista_To4 ;
      private DateTime AV91ContagemResultado_DataPrevista5 ;
      private DateTime AV92ContagemResultado_DataPrevista_To5 ;
      private DateTime AV105ContagemResultado_DataPrevista6 ;
      private DateTime AV106ContagemResultado_DataPrevista_To6 ;
      private DateTime AV119ContagemResultado_DataPrevista7 ;
      private DateTime AV120ContagemResultado_DataPrevista_To7 ;
      private DateTime AV133ContagemResultado_DataPrevista8 ;
      private DateTime AV134ContagemResultado_DataPrevista_To8 ;
      private DateTime AV147ContagemResultado_DataPrevista9 ;
      private DateTime AV148ContagemResultado_DataPrevista_To9 ;
      private DateTime AV164WP_CaixaDmnDS_6_Contagemresultado_dataprevista1 ;
      private DateTime AV165WP_CaixaDmnDS_7_Contagemresultado_dataprevista_to1 ;
      private DateTime AV178WP_CaixaDmnDS_20_Contagemresultado_dataprevista2 ;
      private DateTime AV179WP_CaixaDmnDS_21_Contagemresultado_dataprevista_to2 ;
      private DateTime AV192WP_CaixaDmnDS_34_Contagemresultado_dataprevista3 ;
      private DateTime AV193WP_CaixaDmnDS_35_Contagemresultado_dataprevista_to3 ;
      private DateTime AV206WP_CaixaDmnDS_48_Contagemresultado_dataprevista4 ;
      private DateTime AV207WP_CaixaDmnDS_49_Contagemresultado_dataprevista_to4 ;
      private DateTime AV220WP_CaixaDmnDS_62_Contagemresultado_dataprevista5 ;
      private DateTime AV221WP_CaixaDmnDS_63_Contagemresultado_dataprevista_to5 ;
      private DateTime AV234WP_CaixaDmnDS_76_Contagemresultado_dataprevista6 ;
      private DateTime AV235WP_CaixaDmnDS_77_Contagemresultado_dataprevista_to6 ;
      private DateTime AV248WP_CaixaDmnDS_90_Contagemresultado_dataprevista7 ;
      private DateTime AV249WP_CaixaDmnDS_91_Contagemresultado_dataprevista_to7 ;
      private DateTime AV262WP_CaixaDmnDS_104_Contagemresultado_dataprevista8 ;
      private DateTime AV263WP_CaixaDmnDS_105_Contagemresultado_dataprevista_to8 ;
      private DateTime AV276WP_CaixaDmnDS_118_Contagemresultado_dataprevista9 ;
      private DateTime AV277WP_CaixaDmnDS_119_Contagemresultado_dataprevista_to9 ;
      private DateTime A1351ContagemResultado_DataPrevista ;
      private DateTime AV33ContagemResultado_DataDmn1 ;
      private DateTime AV34ContagemResultado_DataDmn_To1 ;
      private DateTime AV47ContagemResultado_DataDmn2 ;
      private DateTime AV48ContagemResultado_DataDmn_To2 ;
      private DateTime AV61ContagemResultado_DataDmn3 ;
      private DateTime AV62ContagemResultado_DataDmn_To3 ;
      private DateTime AV75ContagemResultado_DataDmn4 ;
      private DateTime AV76ContagemResultado_DataDmn_To4 ;
      private DateTime AV89ContagemResultado_DataDmn5 ;
      private DateTime AV90ContagemResultado_DataDmn_To5 ;
      private DateTime AV103ContagemResultado_DataDmn6 ;
      private DateTime AV104ContagemResultado_DataDmn_To6 ;
      private DateTime AV117ContagemResultado_DataDmn7 ;
      private DateTime AV118ContagemResultado_DataDmn_To7 ;
      private DateTime AV131ContagemResultado_DataDmn8 ;
      private DateTime AV132ContagemResultado_DataDmn_To8 ;
      private DateTime AV145ContagemResultado_DataDmn9 ;
      private DateTime AV146ContagemResultado_DataDmn_To9 ;
      private DateTime AV162WP_CaixaDmnDS_4_Contagemresultado_datadmn1 ;
      private DateTime AV163WP_CaixaDmnDS_5_Contagemresultado_datadmn_to1 ;
      private DateTime AV176WP_CaixaDmnDS_18_Contagemresultado_datadmn2 ;
      private DateTime AV177WP_CaixaDmnDS_19_Contagemresultado_datadmn_to2 ;
      private DateTime AV190WP_CaixaDmnDS_32_Contagemresultado_datadmn3 ;
      private DateTime AV191WP_CaixaDmnDS_33_Contagemresultado_datadmn_to3 ;
      private DateTime AV204WP_CaixaDmnDS_46_Contagemresultado_datadmn4 ;
      private DateTime AV205WP_CaixaDmnDS_47_Contagemresultado_datadmn_to4 ;
      private DateTime AV218WP_CaixaDmnDS_60_Contagemresultado_datadmn5 ;
      private DateTime AV219WP_CaixaDmnDS_61_Contagemresultado_datadmn_to5 ;
      private DateTime AV232WP_CaixaDmnDS_74_Contagemresultado_datadmn6 ;
      private DateTime AV233WP_CaixaDmnDS_75_Contagemresultado_datadmn_to6 ;
      private DateTime AV246WP_CaixaDmnDS_88_Contagemresultado_datadmn7 ;
      private DateTime AV247WP_CaixaDmnDS_89_Contagemresultado_datadmn_to7 ;
      private DateTime AV260WP_CaixaDmnDS_102_Contagemresultado_datadmn8 ;
      private DateTime AV261WP_CaixaDmnDS_103_Contagemresultado_datadmn_to8 ;
      private DateTime AV274WP_CaixaDmnDS_116_Contagemresultado_datadmn9 ;
      private DateTime AV275WP_CaixaDmnDS_117_Contagemresultado_datadmn_to9 ;
      private DateTime A471ContagemResultado_DataDmn ;
      private bool returnInSub ;
      private bool AV43DynamicFiltersEnabled2 ;
      private bool AV57DynamicFiltersEnabled3 ;
      private bool AV71DynamicFiltersEnabled4 ;
      private bool AV85DynamicFiltersEnabled5 ;
      private bool AV99DynamicFiltersEnabled6 ;
      private bool AV113DynamicFiltersEnabled7 ;
      private bool AV127DynamicFiltersEnabled8 ;
      private bool AV141DynamicFiltersEnabled9 ;
      private bool AV172WP_CaixaDmnDS_14_Dynamicfiltersenabled2 ;
      private bool AV186WP_CaixaDmnDS_28_Dynamicfiltersenabled3 ;
      private bool AV200WP_CaixaDmnDS_42_Dynamicfiltersenabled4 ;
      private bool AV214WP_CaixaDmnDS_56_Dynamicfiltersenabled5 ;
      private bool AV228WP_CaixaDmnDS_70_Dynamicfiltersenabled6 ;
      private bool AV242WP_CaixaDmnDS_84_Dynamicfiltersenabled7 ;
      private bool AV256WP_CaixaDmnDS_98_Dynamicfiltersenabled8 ;
      private bool AV270WP_CaixaDmnDS_112_Dynamicfiltersenabled9 ;
      private bool BRKV92 ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n457ContagemResultado_Demanda ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n1046ContagemResultado_Agrupador ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n1351ContagemResultado_DataPrevista ;
      private String AV23OptionIndexesJson ;
      private String AV18OptionsJson ;
      private String AV21OptionsDescJson ;
      private String AV14DDOName ;
      private String AV12SearchTxt ;
      private String AV13SearchTxtTo ;
      private String AV10TFContagemResultado_Demanda ;
      private String AV11TFContagemResultado_Demanda_Sel ;
      private String AV30DynamicFiltersSelector1 ;
      private String AV32ContagemResultado_Demanda1 ;
      private String AV44DynamicFiltersSelector2 ;
      private String AV46ContagemResultado_Demanda2 ;
      private String AV58DynamicFiltersSelector3 ;
      private String AV60ContagemResultado_Demanda3 ;
      private String AV72DynamicFiltersSelector4 ;
      private String AV74ContagemResultado_Demanda4 ;
      private String AV86DynamicFiltersSelector5 ;
      private String AV88ContagemResultado_Demanda5 ;
      private String AV100DynamicFiltersSelector6 ;
      private String AV102ContagemResultado_Demanda6 ;
      private String AV114DynamicFiltersSelector7 ;
      private String AV116ContagemResultado_Demanda7 ;
      private String AV128DynamicFiltersSelector8 ;
      private String AV130ContagemResultado_Demanda8 ;
      private String AV142DynamicFiltersSelector9 ;
      private String AV144ContagemResultado_Demanda9 ;
      private String AV159WP_CaixaDmnDS_1_Dynamicfiltersselector1 ;
      private String AV161WP_CaixaDmnDS_3_Contagemresultado_demanda1 ;
      private String AV173WP_CaixaDmnDS_15_Dynamicfiltersselector2 ;
      private String AV175WP_CaixaDmnDS_17_Contagemresultado_demanda2 ;
      private String AV187WP_CaixaDmnDS_29_Dynamicfiltersselector3 ;
      private String AV189WP_CaixaDmnDS_31_Contagemresultado_demanda3 ;
      private String AV201WP_CaixaDmnDS_43_Dynamicfiltersselector4 ;
      private String AV203WP_CaixaDmnDS_45_Contagemresultado_demanda4 ;
      private String AV215WP_CaixaDmnDS_57_Dynamicfiltersselector5 ;
      private String AV217WP_CaixaDmnDS_59_Contagemresultado_demanda5 ;
      private String AV229WP_CaixaDmnDS_71_Dynamicfiltersselector6 ;
      private String AV231WP_CaixaDmnDS_73_Contagemresultado_demanda6 ;
      private String AV243WP_CaixaDmnDS_85_Dynamicfiltersselector7 ;
      private String AV245WP_CaixaDmnDS_87_Contagemresultado_demanda7 ;
      private String AV257WP_CaixaDmnDS_99_Dynamicfiltersselector8 ;
      private String AV259WP_CaixaDmnDS_101_Contagemresultado_demanda8 ;
      private String AV271WP_CaixaDmnDS_113_Dynamicfiltersselector9 ;
      private String AV273WP_CaixaDmnDS_115_Contagemresultado_demanda9 ;
      private String AV284WP_CaixaDmnDS_126_Tfcontagemresultado_demanda ;
      private String AV285WP_CaixaDmnDS_127_Tfcontagemresultado_demanda_sel ;
      private String lV161WP_CaixaDmnDS_3_Contagemresultado_demanda1 ;
      private String lV175WP_CaixaDmnDS_17_Contagemresultado_demanda2 ;
      private String lV189WP_CaixaDmnDS_31_Contagemresultado_demanda3 ;
      private String lV203WP_CaixaDmnDS_45_Contagemresultado_demanda4 ;
      private String lV217WP_CaixaDmnDS_59_Contagemresultado_demanda5 ;
      private String lV231WP_CaixaDmnDS_73_Contagemresultado_demanda6 ;
      private String lV245WP_CaixaDmnDS_87_Contagemresultado_demanda7 ;
      private String lV259WP_CaixaDmnDS_101_Contagemresultado_demanda8 ;
      private String lV273WP_CaixaDmnDS_115_Contagemresultado_demanda9 ;
      private String lV284WP_CaixaDmnDS_126_Tfcontagemresultado_demanda ;
      private String A457ContagemResultado_Demanda ;
      private String AV16Option ;
      private String AV19OptionDesc ;
      private IGxSession AV25Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00V92_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00V92_n1553ContagemResultado_CntSrvCod ;
      private String[] P00V92_A457ContagemResultado_Demanda ;
      private bool[] P00V92_n457ContagemResultado_Demanda ;
      private String[] P00V92_A484ContagemResultado_StatusDmn ;
      private bool[] P00V92_n484ContagemResultado_StatusDmn ;
      private String[] P00V92_A1046ContagemResultado_Agrupador ;
      private bool[] P00V92_n1046ContagemResultado_Agrupador ;
      private int[] P00V92_A890ContagemResultado_Responsavel ;
      private bool[] P00V92_n890ContagemResultado_Responsavel ;
      private int[] P00V92_A489ContagemResultado_SistemaCod ;
      private bool[] P00V92_n489ContagemResultado_SistemaCod ;
      private int[] P00V92_A601ContagemResultado_Servico ;
      private bool[] P00V92_n601ContagemResultado_Servico ;
      private int[] P00V92_A490ContagemResultado_ContratadaCod ;
      private bool[] P00V92_n490ContagemResultado_ContratadaCod ;
      private DateTime[] P00V92_A1351ContagemResultado_DataPrevista ;
      private bool[] P00V92_n1351ContagemResultado_DataPrevista ;
      private DateTime[] P00V92_A471ContagemResultado_DataDmn ;
      private int[] P00V92_A456ContagemResultado_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV17Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV20OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV27GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV28GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV29GridStateDynamicFilter ;
   }

   public class getwp_caixadmnfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00V92( IGxContext context ,
                                             String AV159WP_CaixaDmnDS_1_Dynamicfiltersselector1 ,
                                             short AV160WP_CaixaDmnDS_2_Dynamicfiltersoperator1 ,
                                             String AV161WP_CaixaDmnDS_3_Contagemresultado_demanda1 ,
                                             DateTime AV162WP_CaixaDmnDS_4_Contagemresultado_datadmn1 ,
                                             DateTime AV163WP_CaixaDmnDS_5_Contagemresultado_datadmn_to1 ,
                                             DateTime AV164WP_CaixaDmnDS_6_Contagemresultado_dataprevista1 ,
                                             DateTime AV165WP_CaixaDmnDS_7_Contagemresultado_dataprevista_to1 ,
                                             int AV166WP_CaixaDmnDS_8_Contagemresultado_contratadacod1 ,
                                             int AV167WP_CaixaDmnDS_9_Contagemresultado_servico1 ,
                                             int AV168WP_CaixaDmnDS_10_Contagemresultado_sistemacod1 ,
                                             int AV169WP_CaixaDmnDS_11_Contagemresultado_responsavel1 ,
                                             String AV170WP_CaixaDmnDS_12_Contagemresultado_agrupador1 ,
                                             String AV171WP_CaixaDmnDS_13_Contagemresultado_statusdmn1 ,
                                             bool AV172WP_CaixaDmnDS_14_Dynamicfiltersenabled2 ,
                                             String AV173WP_CaixaDmnDS_15_Dynamicfiltersselector2 ,
                                             short AV174WP_CaixaDmnDS_16_Dynamicfiltersoperator2 ,
                                             String AV175WP_CaixaDmnDS_17_Contagemresultado_demanda2 ,
                                             DateTime AV176WP_CaixaDmnDS_18_Contagemresultado_datadmn2 ,
                                             DateTime AV177WP_CaixaDmnDS_19_Contagemresultado_datadmn_to2 ,
                                             DateTime AV178WP_CaixaDmnDS_20_Contagemresultado_dataprevista2 ,
                                             DateTime AV179WP_CaixaDmnDS_21_Contagemresultado_dataprevista_to2 ,
                                             int AV180WP_CaixaDmnDS_22_Contagemresultado_contratadacod2 ,
                                             int AV181WP_CaixaDmnDS_23_Contagemresultado_servico2 ,
                                             int AV182WP_CaixaDmnDS_24_Contagemresultado_sistemacod2 ,
                                             int AV183WP_CaixaDmnDS_25_Contagemresultado_responsavel2 ,
                                             String AV184WP_CaixaDmnDS_26_Contagemresultado_agrupador2 ,
                                             String AV185WP_CaixaDmnDS_27_Contagemresultado_statusdmn2 ,
                                             bool AV186WP_CaixaDmnDS_28_Dynamicfiltersenabled3 ,
                                             String AV187WP_CaixaDmnDS_29_Dynamicfiltersselector3 ,
                                             short AV188WP_CaixaDmnDS_30_Dynamicfiltersoperator3 ,
                                             String AV189WP_CaixaDmnDS_31_Contagemresultado_demanda3 ,
                                             DateTime AV190WP_CaixaDmnDS_32_Contagemresultado_datadmn3 ,
                                             DateTime AV191WP_CaixaDmnDS_33_Contagemresultado_datadmn_to3 ,
                                             DateTime AV192WP_CaixaDmnDS_34_Contagemresultado_dataprevista3 ,
                                             DateTime AV193WP_CaixaDmnDS_35_Contagemresultado_dataprevista_to3 ,
                                             int AV194WP_CaixaDmnDS_36_Contagemresultado_contratadacod3 ,
                                             int AV195WP_CaixaDmnDS_37_Contagemresultado_servico3 ,
                                             int AV196WP_CaixaDmnDS_38_Contagemresultado_sistemacod3 ,
                                             int AV197WP_CaixaDmnDS_39_Contagemresultado_responsavel3 ,
                                             String AV198WP_CaixaDmnDS_40_Contagemresultado_agrupador3 ,
                                             String AV199WP_CaixaDmnDS_41_Contagemresultado_statusdmn3 ,
                                             bool AV200WP_CaixaDmnDS_42_Dynamicfiltersenabled4 ,
                                             String AV201WP_CaixaDmnDS_43_Dynamicfiltersselector4 ,
                                             short AV202WP_CaixaDmnDS_44_Dynamicfiltersoperator4 ,
                                             String AV203WP_CaixaDmnDS_45_Contagemresultado_demanda4 ,
                                             DateTime AV204WP_CaixaDmnDS_46_Contagemresultado_datadmn4 ,
                                             DateTime AV205WP_CaixaDmnDS_47_Contagemresultado_datadmn_to4 ,
                                             DateTime AV206WP_CaixaDmnDS_48_Contagemresultado_dataprevista4 ,
                                             DateTime AV207WP_CaixaDmnDS_49_Contagemresultado_dataprevista_to4 ,
                                             int AV208WP_CaixaDmnDS_50_Contagemresultado_contratadacod4 ,
                                             int AV209WP_CaixaDmnDS_51_Contagemresultado_servico4 ,
                                             int AV210WP_CaixaDmnDS_52_Contagemresultado_sistemacod4 ,
                                             int AV211WP_CaixaDmnDS_53_Contagemresultado_responsavel4 ,
                                             String AV212WP_CaixaDmnDS_54_Contagemresultado_agrupador4 ,
                                             String AV213WP_CaixaDmnDS_55_Contagemresultado_statusdmn4 ,
                                             bool AV214WP_CaixaDmnDS_56_Dynamicfiltersenabled5 ,
                                             String AV215WP_CaixaDmnDS_57_Dynamicfiltersselector5 ,
                                             short AV216WP_CaixaDmnDS_58_Dynamicfiltersoperator5 ,
                                             String AV217WP_CaixaDmnDS_59_Contagemresultado_demanda5 ,
                                             DateTime AV218WP_CaixaDmnDS_60_Contagemresultado_datadmn5 ,
                                             DateTime AV219WP_CaixaDmnDS_61_Contagemresultado_datadmn_to5 ,
                                             DateTime AV220WP_CaixaDmnDS_62_Contagemresultado_dataprevista5 ,
                                             DateTime AV221WP_CaixaDmnDS_63_Contagemresultado_dataprevista_to5 ,
                                             int AV222WP_CaixaDmnDS_64_Contagemresultado_contratadacod5 ,
                                             int AV223WP_CaixaDmnDS_65_Contagemresultado_servico5 ,
                                             int AV224WP_CaixaDmnDS_66_Contagemresultado_sistemacod5 ,
                                             int AV225WP_CaixaDmnDS_67_Contagemresultado_responsavel5 ,
                                             String AV226WP_CaixaDmnDS_68_Contagemresultado_agrupador5 ,
                                             String AV227WP_CaixaDmnDS_69_Contagemresultado_statusdmn5 ,
                                             bool AV228WP_CaixaDmnDS_70_Dynamicfiltersenabled6 ,
                                             String AV229WP_CaixaDmnDS_71_Dynamicfiltersselector6 ,
                                             short AV230WP_CaixaDmnDS_72_Dynamicfiltersoperator6 ,
                                             String AV231WP_CaixaDmnDS_73_Contagemresultado_demanda6 ,
                                             DateTime AV232WP_CaixaDmnDS_74_Contagemresultado_datadmn6 ,
                                             DateTime AV233WP_CaixaDmnDS_75_Contagemresultado_datadmn_to6 ,
                                             DateTime AV234WP_CaixaDmnDS_76_Contagemresultado_dataprevista6 ,
                                             DateTime AV235WP_CaixaDmnDS_77_Contagemresultado_dataprevista_to6 ,
                                             int AV236WP_CaixaDmnDS_78_Contagemresultado_contratadacod6 ,
                                             int AV237WP_CaixaDmnDS_79_Contagemresultado_servico6 ,
                                             int AV238WP_CaixaDmnDS_80_Contagemresultado_sistemacod6 ,
                                             int AV239WP_CaixaDmnDS_81_Contagemresultado_responsavel6 ,
                                             String AV240WP_CaixaDmnDS_82_Contagemresultado_agrupador6 ,
                                             String AV241WP_CaixaDmnDS_83_Contagemresultado_statusdmn6 ,
                                             bool AV242WP_CaixaDmnDS_84_Dynamicfiltersenabled7 ,
                                             String AV243WP_CaixaDmnDS_85_Dynamicfiltersselector7 ,
                                             short AV244WP_CaixaDmnDS_86_Dynamicfiltersoperator7 ,
                                             String AV245WP_CaixaDmnDS_87_Contagemresultado_demanda7 ,
                                             DateTime AV246WP_CaixaDmnDS_88_Contagemresultado_datadmn7 ,
                                             DateTime AV247WP_CaixaDmnDS_89_Contagemresultado_datadmn_to7 ,
                                             DateTime AV248WP_CaixaDmnDS_90_Contagemresultado_dataprevista7 ,
                                             DateTime AV249WP_CaixaDmnDS_91_Contagemresultado_dataprevista_to7 ,
                                             int AV250WP_CaixaDmnDS_92_Contagemresultado_contratadacod7 ,
                                             int AV251WP_CaixaDmnDS_93_Contagemresultado_servico7 ,
                                             int AV252WP_CaixaDmnDS_94_Contagemresultado_sistemacod7 ,
                                             int AV253WP_CaixaDmnDS_95_Contagemresultado_responsavel7 ,
                                             String AV254WP_CaixaDmnDS_96_Contagemresultado_agrupador7 ,
                                             String AV255WP_CaixaDmnDS_97_Contagemresultado_statusdmn7 ,
                                             bool AV256WP_CaixaDmnDS_98_Dynamicfiltersenabled8 ,
                                             String AV257WP_CaixaDmnDS_99_Dynamicfiltersselector8 ,
                                             short AV258WP_CaixaDmnDS_100_Dynamicfiltersoperator8 ,
                                             String AV259WP_CaixaDmnDS_101_Contagemresultado_demanda8 ,
                                             DateTime AV260WP_CaixaDmnDS_102_Contagemresultado_datadmn8 ,
                                             DateTime AV261WP_CaixaDmnDS_103_Contagemresultado_datadmn_to8 ,
                                             DateTime AV262WP_CaixaDmnDS_104_Contagemresultado_dataprevista8 ,
                                             DateTime AV263WP_CaixaDmnDS_105_Contagemresultado_dataprevista_to8 ,
                                             int AV264WP_CaixaDmnDS_106_Contagemresultado_contratadacod8 ,
                                             int AV265WP_CaixaDmnDS_107_Contagemresultado_servico8 ,
                                             int AV266WP_CaixaDmnDS_108_Contagemresultado_sistemacod8 ,
                                             int AV267WP_CaixaDmnDS_109_Contagemresultado_responsavel8 ,
                                             String AV268WP_CaixaDmnDS_110_Contagemresultado_agrupador8 ,
                                             String AV269WP_CaixaDmnDS_111_Contagemresultado_statusdmn8 ,
                                             bool AV270WP_CaixaDmnDS_112_Dynamicfiltersenabled9 ,
                                             String AV271WP_CaixaDmnDS_113_Dynamicfiltersselector9 ,
                                             short AV272WP_CaixaDmnDS_114_Dynamicfiltersoperator9 ,
                                             String AV273WP_CaixaDmnDS_115_Contagemresultado_demanda9 ,
                                             DateTime AV274WP_CaixaDmnDS_116_Contagemresultado_datadmn9 ,
                                             DateTime AV275WP_CaixaDmnDS_117_Contagemresultado_datadmn_to9 ,
                                             DateTime AV276WP_CaixaDmnDS_118_Contagemresultado_dataprevista9 ,
                                             DateTime AV277WP_CaixaDmnDS_119_Contagemresultado_dataprevista_to9 ,
                                             int AV278WP_CaixaDmnDS_120_Contagemresultado_contratadacod9 ,
                                             int AV279WP_CaixaDmnDS_121_Contagemresultado_servico9 ,
                                             int AV280WP_CaixaDmnDS_122_Contagemresultado_sistemacod9 ,
                                             int AV281WP_CaixaDmnDS_123_Contagemresultado_responsavel9 ,
                                             String AV282WP_CaixaDmnDS_124_Contagemresultado_agrupador9 ,
                                             String AV283WP_CaixaDmnDS_125_Contagemresultado_statusdmn9 ,
                                             String AV285WP_CaixaDmnDS_127_Tfcontagemresultado_demanda_sel ,
                                             String AV284WP_CaixaDmnDS_126_Tfcontagemresultado_demanda ,
                                             String A457ContagemResultado_Demanda ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             DateTime A1351ContagemResultado_DataPrevista ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             int A601ContagemResultado_Servico ,
                                             int A489ContagemResultado_SistemaCod ,
                                             int A890ContagemResultado_Responsavel ,
                                             String A1046ContagemResultado_Agrupador ,
                                             String A484ContagemResultado_StatusDmn )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [137] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Demanda], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_Agrupador], T1.[ContagemResultado_Responsavel], T1.[ContagemResultado_SistemaCod], T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_ContratadaCod], T1.[ContagemResultado_DataPrevista], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_Codigo] FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod])";
         if ( ( StringUtil.StrCmp(AV159WP_CaixaDmnDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV160WP_CaixaDmnDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV161WP_CaixaDmnDS_3_Contagemresultado_demanda1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV161WP_CaixaDmnDS_3_Contagemresultado_demanda1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Demanda] = @AV161WP_CaixaDmnDS_3_Contagemresultado_demanda1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV159WP_CaixaDmnDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV160WP_CaixaDmnDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV161WP_CaixaDmnDS_3_Contagemresultado_demanda1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV161WP_CaixaDmnDS_3_Contagemresultado_demanda1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Demanda] like @lV161WP_CaixaDmnDS_3_Contagemresultado_demanda1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV159WP_CaixaDmnDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV160WP_CaixaDmnDS_2_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV161WP_CaixaDmnDS_3_Contagemresultado_demanda1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV161WP_CaixaDmnDS_3_Contagemresultado_demanda1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Demanda] like '%' + @lV161WP_CaixaDmnDS_3_Contagemresultado_demanda1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV159WP_CaixaDmnDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV162WP_CaixaDmnDS_4_Contagemresultado_datadmn1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV162WP_CaixaDmnDS_4_Contagemresultado_datadmn1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataDmn] >= @AV162WP_CaixaDmnDS_4_Contagemresultado_datadmn1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV159WP_CaixaDmnDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV163WP_CaixaDmnDS_5_Contagemresultado_datadmn_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV163WP_CaixaDmnDS_5_Contagemresultado_datadmn_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataDmn] <= @AV163WP_CaixaDmnDS_5_Contagemresultado_datadmn_to1)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV159WP_CaixaDmnDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV164WP_CaixaDmnDS_6_Contagemresultado_dataprevista1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV164WP_CaixaDmnDS_6_Contagemresultado_dataprevista1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataPrevista] >= @AV164WP_CaixaDmnDS_6_Contagemresultado_dataprevista1)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV159WP_CaixaDmnDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV165WP_CaixaDmnDS_7_Contagemresultado_dataprevista_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] <= @AV165WP_CaixaDmnDS_7_Contagemresultado_dataprevista_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataPrevista] <= @AV165WP_CaixaDmnDS_7_Contagemresultado_dataprevista_to1)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV159WP_CaixaDmnDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ! (0==AV166WP_CaixaDmnDS_8_Contagemresultado_contratadacod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV166WP_CaixaDmnDS_8_Contagemresultado_contratadacod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_ContratadaCod] = @AV166WP_CaixaDmnDS_8_Contagemresultado_contratadacod1)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV159WP_CaixaDmnDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV167WP_CaixaDmnDS_9_Contagemresultado_servico1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV167WP_CaixaDmnDS_9_Contagemresultado_servico1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV167WP_CaixaDmnDS_9_Contagemresultado_servico1)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ( StringUtil.StrCmp(AV159WP_CaixaDmnDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV168WP_CaixaDmnDS_10_Contagemresultado_sistemacod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV168WP_CaixaDmnDS_10_Contagemresultado_sistemacod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_SistemaCod] = @AV168WP_CaixaDmnDS_10_Contagemresultado_sistemacod1)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ( StringUtil.StrCmp(AV159WP_CaixaDmnDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_RESPONSAVEL") == 0 ) && ( ! (0==AV169WP_CaixaDmnDS_11_Contagemresultado_responsavel1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Responsavel] = @AV169WP_CaixaDmnDS_11_Contagemresultado_responsavel1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Responsavel] = @AV169WP_CaixaDmnDS_11_Contagemresultado_responsavel1)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ( StringUtil.StrCmp(AV159WP_CaixaDmnDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV160WP_CaixaDmnDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV170WP_CaixaDmnDS_12_Contagemresultado_agrupador1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV170WP_CaixaDmnDS_12_Contagemresultado_agrupador1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Agrupador] = @AV170WP_CaixaDmnDS_12_Contagemresultado_agrupador1)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ( StringUtil.StrCmp(AV159WP_CaixaDmnDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV160WP_CaixaDmnDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV170WP_CaixaDmnDS_12_Contagemresultado_agrupador1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like @lV170WP_CaixaDmnDS_12_Contagemresultado_agrupador1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Agrupador] like @lV170WP_CaixaDmnDS_12_Contagemresultado_agrupador1)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ( StringUtil.StrCmp(AV159WP_CaixaDmnDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV160WP_CaixaDmnDS_2_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV170WP_CaixaDmnDS_12_Contagemresultado_agrupador1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like '%' + @lV170WP_CaixaDmnDS_12_Contagemresultado_agrupador1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Agrupador] like '%' + @lV170WP_CaixaDmnDS_12_Contagemresultado_agrupador1)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ( StringUtil.StrCmp(AV159WP_CaixaDmnDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV171WP_CaixaDmnDS_13_Contagemresultado_statusdmn1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV171WP_CaixaDmnDS_13_Contagemresultado_statusdmn1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_StatusDmn] = @AV171WP_CaixaDmnDS_13_Contagemresultado_statusdmn1)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( AV172WP_CaixaDmnDS_14_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV173WP_CaixaDmnDS_15_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV174WP_CaixaDmnDS_16_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV175WP_CaixaDmnDS_17_Contagemresultado_demanda2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV175WP_CaixaDmnDS_17_Contagemresultado_demanda2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Demanda] = @AV175WP_CaixaDmnDS_17_Contagemresultado_demanda2)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( AV172WP_CaixaDmnDS_14_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV173WP_CaixaDmnDS_15_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV174WP_CaixaDmnDS_16_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV175WP_CaixaDmnDS_17_Contagemresultado_demanda2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV175WP_CaixaDmnDS_17_Contagemresultado_demanda2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Demanda] like @lV175WP_CaixaDmnDS_17_Contagemresultado_demanda2)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( AV172WP_CaixaDmnDS_14_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV173WP_CaixaDmnDS_15_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV174WP_CaixaDmnDS_16_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV175WP_CaixaDmnDS_17_Contagemresultado_demanda2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV175WP_CaixaDmnDS_17_Contagemresultado_demanda2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Demanda] like '%' + @lV175WP_CaixaDmnDS_17_Contagemresultado_demanda2)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( AV172WP_CaixaDmnDS_14_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV173WP_CaixaDmnDS_15_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV176WP_CaixaDmnDS_18_Contagemresultado_datadmn2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV176WP_CaixaDmnDS_18_Contagemresultado_datadmn2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataDmn] >= @AV176WP_CaixaDmnDS_18_Contagemresultado_datadmn2)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( AV172WP_CaixaDmnDS_14_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV173WP_CaixaDmnDS_15_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV177WP_CaixaDmnDS_19_Contagemresultado_datadmn_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV177WP_CaixaDmnDS_19_Contagemresultado_datadmn_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataDmn] <= @AV177WP_CaixaDmnDS_19_Contagemresultado_datadmn_to2)";
            }
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( AV172WP_CaixaDmnDS_14_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV173WP_CaixaDmnDS_15_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV178WP_CaixaDmnDS_20_Contagemresultado_dataprevista2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV178WP_CaixaDmnDS_20_Contagemresultado_dataprevista2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataPrevista] >= @AV178WP_CaixaDmnDS_20_Contagemresultado_dataprevista2)";
            }
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( AV172WP_CaixaDmnDS_14_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV173WP_CaixaDmnDS_15_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV179WP_CaixaDmnDS_21_Contagemresultado_dataprevista_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] <= @AV179WP_CaixaDmnDS_21_Contagemresultado_dataprevista_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataPrevista] <= @AV179WP_CaixaDmnDS_21_Contagemresultado_dataprevista_to2)";
            }
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( AV172WP_CaixaDmnDS_14_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV173WP_CaixaDmnDS_15_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ! (0==AV180WP_CaixaDmnDS_22_Contagemresultado_contratadacod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV180WP_CaixaDmnDS_22_Contagemresultado_contratadacod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_ContratadaCod] = @AV180WP_CaixaDmnDS_22_Contagemresultado_contratadacod2)";
            }
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( AV172WP_CaixaDmnDS_14_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV173WP_CaixaDmnDS_15_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV181WP_CaixaDmnDS_23_Contagemresultado_servico2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV181WP_CaixaDmnDS_23_Contagemresultado_servico2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV181WP_CaixaDmnDS_23_Contagemresultado_servico2)";
            }
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( AV172WP_CaixaDmnDS_14_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV173WP_CaixaDmnDS_15_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV182WP_CaixaDmnDS_24_Contagemresultado_sistemacod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV182WP_CaixaDmnDS_24_Contagemresultado_sistemacod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_SistemaCod] = @AV182WP_CaixaDmnDS_24_Contagemresultado_sistemacod2)";
            }
         }
         else
         {
            GXv_int1[24] = 1;
         }
         if ( AV172WP_CaixaDmnDS_14_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV173WP_CaixaDmnDS_15_Dynamicfiltersselector2, "CONTAGEMRESULTADO_RESPONSAVEL") == 0 ) && ( ! (0==AV183WP_CaixaDmnDS_25_Contagemresultado_responsavel2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Responsavel] = @AV183WP_CaixaDmnDS_25_Contagemresultado_responsavel2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Responsavel] = @AV183WP_CaixaDmnDS_25_Contagemresultado_responsavel2)";
            }
         }
         else
         {
            GXv_int1[25] = 1;
         }
         if ( AV172WP_CaixaDmnDS_14_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV173WP_CaixaDmnDS_15_Dynamicfiltersselector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV174WP_CaixaDmnDS_16_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV184WP_CaixaDmnDS_26_Contagemresultado_agrupador2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV184WP_CaixaDmnDS_26_Contagemresultado_agrupador2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Agrupador] = @AV184WP_CaixaDmnDS_26_Contagemresultado_agrupador2)";
            }
         }
         else
         {
            GXv_int1[26] = 1;
         }
         if ( AV172WP_CaixaDmnDS_14_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV173WP_CaixaDmnDS_15_Dynamicfiltersselector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV174WP_CaixaDmnDS_16_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV184WP_CaixaDmnDS_26_Contagemresultado_agrupador2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like @lV184WP_CaixaDmnDS_26_Contagemresultado_agrupador2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Agrupador] like @lV184WP_CaixaDmnDS_26_Contagemresultado_agrupador2)";
            }
         }
         else
         {
            GXv_int1[27] = 1;
         }
         if ( AV172WP_CaixaDmnDS_14_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV173WP_CaixaDmnDS_15_Dynamicfiltersselector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV174WP_CaixaDmnDS_16_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV184WP_CaixaDmnDS_26_Contagemresultado_agrupador2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like '%' + @lV184WP_CaixaDmnDS_26_Contagemresultado_agrupador2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Agrupador] like '%' + @lV184WP_CaixaDmnDS_26_Contagemresultado_agrupador2)";
            }
         }
         else
         {
            GXv_int1[28] = 1;
         }
         if ( AV172WP_CaixaDmnDS_14_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV173WP_CaixaDmnDS_15_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV185WP_CaixaDmnDS_27_Contagemresultado_statusdmn2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV185WP_CaixaDmnDS_27_Contagemresultado_statusdmn2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_StatusDmn] = @AV185WP_CaixaDmnDS_27_Contagemresultado_statusdmn2)";
            }
         }
         else
         {
            GXv_int1[29] = 1;
         }
         if ( AV186WP_CaixaDmnDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV187WP_CaixaDmnDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV188WP_CaixaDmnDS_30_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV189WP_CaixaDmnDS_31_Contagemresultado_demanda3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV189WP_CaixaDmnDS_31_Contagemresultado_demanda3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Demanda] = @AV189WP_CaixaDmnDS_31_Contagemresultado_demanda3)";
            }
         }
         else
         {
            GXv_int1[30] = 1;
         }
         if ( AV186WP_CaixaDmnDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV187WP_CaixaDmnDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV188WP_CaixaDmnDS_30_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV189WP_CaixaDmnDS_31_Contagemresultado_demanda3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV189WP_CaixaDmnDS_31_Contagemresultado_demanda3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Demanda] like @lV189WP_CaixaDmnDS_31_Contagemresultado_demanda3)";
            }
         }
         else
         {
            GXv_int1[31] = 1;
         }
         if ( AV186WP_CaixaDmnDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV187WP_CaixaDmnDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV188WP_CaixaDmnDS_30_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV189WP_CaixaDmnDS_31_Contagemresultado_demanda3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV189WP_CaixaDmnDS_31_Contagemresultado_demanda3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Demanda] like '%' + @lV189WP_CaixaDmnDS_31_Contagemresultado_demanda3)";
            }
         }
         else
         {
            GXv_int1[32] = 1;
         }
         if ( AV186WP_CaixaDmnDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV187WP_CaixaDmnDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV190WP_CaixaDmnDS_32_Contagemresultado_datadmn3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV190WP_CaixaDmnDS_32_Contagemresultado_datadmn3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataDmn] >= @AV190WP_CaixaDmnDS_32_Contagemresultado_datadmn3)";
            }
         }
         else
         {
            GXv_int1[33] = 1;
         }
         if ( AV186WP_CaixaDmnDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV187WP_CaixaDmnDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV191WP_CaixaDmnDS_33_Contagemresultado_datadmn_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV191WP_CaixaDmnDS_33_Contagemresultado_datadmn_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataDmn] <= @AV191WP_CaixaDmnDS_33_Contagemresultado_datadmn_to3)";
            }
         }
         else
         {
            GXv_int1[34] = 1;
         }
         if ( AV186WP_CaixaDmnDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV187WP_CaixaDmnDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV192WP_CaixaDmnDS_34_Contagemresultado_dataprevista3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV192WP_CaixaDmnDS_34_Contagemresultado_dataprevista3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataPrevista] >= @AV192WP_CaixaDmnDS_34_Contagemresultado_dataprevista3)";
            }
         }
         else
         {
            GXv_int1[35] = 1;
         }
         if ( AV186WP_CaixaDmnDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV187WP_CaixaDmnDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV193WP_CaixaDmnDS_35_Contagemresultado_dataprevista_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] <= @AV193WP_CaixaDmnDS_35_Contagemresultado_dataprevista_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataPrevista] <= @AV193WP_CaixaDmnDS_35_Contagemresultado_dataprevista_to3)";
            }
         }
         else
         {
            GXv_int1[36] = 1;
         }
         if ( AV186WP_CaixaDmnDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV187WP_CaixaDmnDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ! (0==AV194WP_CaixaDmnDS_36_Contagemresultado_contratadacod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV194WP_CaixaDmnDS_36_Contagemresultado_contratadacod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_ContratadaCod] = @AV194WP_CaixaDmnDS_36_Contagemresultado_contratadacod3)";
            }
         }
         else
         {
            GXv_int1[37] = 1;
         }
         if ( AV186WP_CaixaDmnDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV187WP_CaixaDmnDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV195WP_CaixaDmnDS_37_Contagemresultado_servico3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV195WP_CaixaDmnDS_37_Contagemresultado_servico3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV195WP_CaixaDmnDS_37_Contagemresultado_servico3)";
            }
         }
         else
         {
            GXv_int1[38] = 1;
         }
         if ( AV186WP_CaixaDmnDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV187WP_CaixaDmnDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV196WP_CaixaDmnDS_38_Contagemresultado_sistemacod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV196WP_CaixaDmnDS_38_Contagemresultado_sistemacod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_SistemaCod] = @AV196WP_CaixaDmnDS_38_Contagemresultado_sistemacod3)";
            }
         }
         else
         {
            GXv_int1[39] = 1;
         }
         if ( AV186WP_CaixaDmnDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV187WP_CaixaDmnDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_RESPONSAVEL") == 0 ) && ( ! (0==AV197WP_CaixaDmnDS_39_Contagemresultado_responsavel3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Responsavel] = @AV197WP_CaixaDmnDS_39_Contagemresultado_responsavel3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Responsavel] = @AV197WP_CaixaDmnDS_39_Contagemresultado_responsavel3)";
            }
         }
         else
         {
            GXv_int1[40] = 1;
         }
         if ( AV186WP_CaixaDmnDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV187WP_CaixaDmnDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV188WP_CaixaDmnDS_30_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV198WP_CaixaDmnDS_40_Contagemresultado_agrupador3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV198WP_CaixaDmnDS_40_Contagemresultado_agrupador3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Agrupador] = @AV198WP_CaixaDmnDS_40_Contagemresultado_agrupador3)";
            }
         }
         else
         {
            GXv_int1[41] = 1;
         }
         if ( AV186WP_CaixaDmnDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV187WP_CaixaDmnDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV188WP_CaixaDmnDS_30_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV198WP_CaixaDmnDS_40_Contagemresultado_agrupador3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like @lV198WP_CaixaDmnDS_40_Contagemresultado_agrupador3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Agrupador] like @lV198WP_CaixaDmnDS_40_Contagemresultado_agrupador3)";
            }
         }
         else
         {
            GXv_int1[42] = 1;
         }
         if ( AV186WP_CaixaDmnDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV187WP_CaixaDmnDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV188WP_CaixaDmnDS_30_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV198WP_CaixaDmnDS_40_Contagemresultado_agrupador3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like '%' + @lV198WP_CaixaDmnDS_40_Contagemresultado_agrupador3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Agrupador] like '%' + @lV198WP_CaixaDmnDS_40_Contagemresultado_agrupador3)";
            }
         }
         else
         {
            GXv_int1[43] = 1;
         }
         if ( AV186WP_CaixaDmnDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV187WP_CaixaDmnDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV199WP_CaixaDmnDS_41_Contagemresultado_statusdmn3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV199WP_CaixaDmnDS_41_Contagemresultado_statusdmn3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_StatusDmn] = @AV199WP_CaixaDmnDS_41_Contagemresultado_statusdmn3)";
            }
         }
         else
         {
            GXv_int1[44] = 1;
         }
         if ( AV200WP_CaixaDmnDS_42_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV201WP_CaixaDmnDS_43_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV202WP_CaixaDmnDS_44_Dynamicfiltersoperator4 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV203WP_CaixaDmnDS_45_Contagemresultado_demanda4)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV203WP_CaixaDmnDS_45_Contagemresultado_demanda4)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Demanda] = @AV203WP_CaixaDmnDS_45_Contagemresultado_demanda4)";
            }
         }
         else
         {
            GXv_int1[45] = 1;
         }
         if ( AV200WP_CaixaDmnDS_42_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV201WP_CaixaDmnDS_43_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV202WP_CaixaDmnDS_44_Dynamicfiltersoperator4 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV203WP_CaixaDmnDS_45_Contagemresultado_demanda4)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV203WP_CaixaDmnDS_45_Contagemresultado_demanda4)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Demanda] like @lV203WP_CaixaDmnDS_45_Contagemresultado_demanda4)";
            }
         }
         else
         {
            GXv_int1[46] = 1;
         }
         if ( AV200WP_CaixaDmnDS_42_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV201WP_CaixaDmnDS_43_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV202WP_CaixaDmnDS_44_Dynamicfiltersoperator4 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV203WP_CaixaDmnDS_45_Contagemresultado_demanda4)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV203WP_CaixaDmnDS_45_Contagemresultado_demanda4)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Demanda] like '%' + @lV203WP_CaixaDmnDS_45_Contagemresultado_demanda4)";
            }
         }
         else
         {
            GXv_int1[47] = 1;
         }
         if ( AV200WP_CaixaDmnDS_42_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV201WP_CaixaDmnDS_43_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV204WP_CaixaDmnDS_46_Contagemresultado_datadmn4) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV204WP_CaixaDmnDS_46_Contagemresultado_datadmn4)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataDmn] >= @AV204WP_CaixaDmnDS_46_Contagemresultado_datadmn4)";
            }
         }
         else
         {
            GXv_int1[48] = 1;
         }
         if ( AV200WP_CaixaDmnDS_42_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV201WP_CaixaDmnDS_43_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV205WP_CaixaDmnDS_47_Contagemresultado_datadmn_to4) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV205WP_CaixaDmnDS_47_Contagemresultado_datadmn_to4)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataDmn] <= @AV205WP_CaixaDmnDS_47_Contagemresultado_datadmn_to4)";
            }
         }
         else
         {
            GXv_int1[49] = 1;
         }
         if ( AV200WP_CaixaDmnDS_42_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV201WP_CaixaDmnDS_43_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV206WP_CaixaDmnDS_48_Contagemresultado_dataprevista4) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV206WP_CaixaDmnDS_48_Contagemresultado_dataprevista4)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataPrevista] >= @AV206WP_CaixaDmnDS_48_Contagemresultado_dataprevista4)";
            }
         }
         else
         {
            GXv_int1[50] = 1;
         }
         if ( AV200WP_CaixaDmnDS_42_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV201WP_CaixaDmnDS_43_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV207WP_CaixaDmnDS_49_Contagemresultado_dataprevista_to4) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] <= @AV207WP_CaixaDmnDS_49_Contagemresultado_dataprevista_to4)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataPrevista] <= @AV207WP_CaixaDmnDS_49_Contagemresultado_dataprevista_to4)";
            }
         }
         else
         {
            GXv_int1[51] = 1;
         }
         if ( AV200WP_CaixaDmnDS_42_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV201WP_CaixaDmnDS_43_Dynamicfiltersselector4, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ! (0==AV208WP_CaixaDmnDS_50_Contagemresultado_contratadacod4) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV208WP_CaixaDmnDS_50_Contagemresultado_contratadacod4)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_ContratadaCod] = @AV208WP_CaixaDmnDS_50_Contagemresultado_contratadacod4)";
            }
         }
         else
         {
            GXv_int1[52] = 1;
         }
         if ( AV200WP_CaixaDmnDS_42_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV201WP_CaixaDmnDS_43_Dynamicfiltersselector4, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV209WP_CaixaDmnDS_51_Contagemresultado_servico4) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV209WP_CaixaDmnDS_51_Contagemresultado_servico4)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV209WP_CaixaDmnDS_51_Contagemresultado_servico4)";
            }
         }
         else
         {
            GXv_int1[53] = 1;
         }
         if ( AV200WP_CaixaDmnDS_42_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV201WP_CaixaDmnDS_43_Dynamicfiltersselector4, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV210WP_CaixaDmnDS_52_Contagemresultado_sistemacod4) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV210WP_CaixaDmnDS_52_Contagemresultado_sistemacod4)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_SistemaCod] = @AV210WP_CaixaDmnDS_52_Contagemresultado_sistemacod4)";
            }
         }
         else
         {
            GXv_int1[54] = 1;
         }
         if ( AV200WP_CaixaDmnDS_42_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV201WP_CaixaDmnDS_43_Dynamicfiltersselector4, "CONTAGEMRESULTADO_RESPONSAVEL") == 0 ) && ( ! (0==AV211WP_CaixaDmnDS_53_Contagemresultado_responsavel4) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Responsavel] = @AV211WP_CaixaDmnDS_53_Contagemresultado_responsavel4)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Responsavel] = @AV211WP_CaixaDmnDS_53_Contagemresultado_responsavel4)";
            }
         }
         else
         {
            GXv_int1[55] = 1;
         }
         if ( AV200WP_CaixaDmnDS_42_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV201WP_CaixaDmnDS_43_Dynamicfiltersselector4, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV202WP_CaixaDmnDS_44_Dynamicfiltersoperator4 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV212WP_CaixaDmnDS_54_Contagemresultado_agrupador4)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV212WP_CaixaDmnDS_54_Contagemresultado_agrupador4)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Agrupador] = @AV212WP_CaixaDmnDS_54_Contagemresultado_agrupador4)";
            }
         }
         else
         {
            GXv_int1[56] = 1;
         }
         if ( AV200WP_CaixaDmnDS_42_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV201WP_CaixaDmnDS_43_Dynamicfiltersselector4, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV202WP_CaixaDmnDS_44_Dynamicfiltersoperator4 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV212WP_CaixaDmnDS_54_Contagemresultado_agrupador4)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like @lV212WP_CaixaDmnDS_54_Contagemresultado_agrupador4)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Agrupador] like @lV212WP_CaixaDmnDS_54_Contagemresultado_agrupador4)";
            }
         }
         else
         {
            GXv_int1[57] = 1;
         }
         if ( AV200WP_CaixaDmnDS_42_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV201WP_CaixaDmnDS_43_Dynamicfiltersselector4, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV202WP_CaixaDmnDS_44_Dynamicfiltersoperator4 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV212WP_CaixaDmnDS_54_Contagemresultado_agrupador4)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like '%' + @lV212WP_CaixaDmnDS_54_Contagemresultado_agrupador4)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Agrupador] like '%' + @lV212WP_CaixaDmnDS_54_Contagemresultado_agrupador4)";
            }
         }
         else
         {
            GXv_int1[58] = 1;
         }
         if ( AV200WP_CaixaDmnDS_42_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV201WP_CaixaDmnDS_43_Dynamicfiltersselector4, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV213WP_CaixaDmnDS_55_Contagemresultado_statusdmn4)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV213WP_CaixaDmnDS_55_Contagemresultado_statusdmn4)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_StatusDmn] = @AV213WP_CaixaDmnDS_55_Contagemresultado_statusdmn4)";
            }
         }
         else
         {
            GXv_int1[59] = 1;
         }
         if ( AV214WP_CaixaDmnDS_56_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV215WP_CaixaDmnDS_57_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV216WP_CaixaDmnDS_58_Dynamicfiltersoperator5 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV217WP_CaixaDmnDS_59_Contagemresultado_demanda5)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV217WP_CaixaDmnDS_59_Contagemresultado_demanda5)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Demanda] = @AV217WP_CaixaDmnDS_59_Contagemresultado_demanda5)";
            }
         }
         else
         {
            GXv_int1[60] = 1;
         }
         if ( AV214WP_CaixaDmnDS_56_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV215WP_CaixaDmnDS_57_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV216WP_CaixaDmnDS_58_Dynamicfiltersoperator5 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV217WP_CaixaDmnDS_59_Contagemresultado_demanda5)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV217WP_CaixaDmnDS_59_Contagemresultado_demanda5)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Demanda] like @lV217WP_CaixaDmnDS_59_Contagemresultado_demanda5)";
            }
         }
         else
         {
            GXv_int1[61] = 1;
         }
         if ( AV214WP_CaixaDmnDS_56_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV215WP_CaixaDmnDS_57_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV216WP_CaixaDmnDS_58_Dynamicfiltersoperator5 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV217WP_CaixaDmnDS_59_Contagemresultado_demanda5)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV217WP_CaixaDmnDS_59_Contagemresultado_demanda5)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Demanda] like '%' + @lV217WP_CaixaDmnDS_59_Contagemresultado_demanda5)";
            }
         }
         else
         {
            GXv_int1[62] = 1;
         }
         if ( AV214WP_CaixaDmnDS_56_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV215WP_CaixaDmnDS_57_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV218WP_CaixaDmnDS_60_Contagemresultado_datadmn5) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV218WP_CaixaDmnDS_60_Contagemresultado_datadmn5)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataDmn] >= @AV218WP_CaixaDmnDS_60_Contagemresultado_datadmn5)";
            }
         }
         else
         {
            GXv_int1[63] = 1;
         }
         if ( AV214WP_CaixaDmnDS_56_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV215WP_CaixaDmnDS_57_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV219WP_CaixaDmnDS_61_Contagemresultado_datadmn_to5) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV219WP_CaixaDmnDS_61_Contagemresultado_datadmn_to5)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataDmn] <= @AV219WP_CaixaDmnDS_61_Contagemresultado_datadmn_to5)";
            }
         }
         else
         {
            GXv_int1[64] = 1;
         }
         if ( AV214WP_CaixaDmnDS_56_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV215WP_CaixaDmnDS_57_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV220WP_CaixaDmnDS_62_Contagemresultado_dataprevista5) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV220WP_CaixaDmnDS_62_Contagemresultado_dataprevista5)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataPrevista] >= @AV220WP_CaixaDmnDS_62_Contagemresultado_dataprevista5)";
            }
         }
         else
         {
            GXv_int1[65] = 1;
         }
         if ( AV214WP_CaixaDmnDS_56_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV215WP_CaixaDmnDS_57_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV221WP_CaixaDmnDS_63_Contagemresultado_dataprevista_to5) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] <= @AV221WP_CaixaDmnDS_63_Contagemresultado_dataprevista_to5)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataPrevista] <= @AV221WP_CaixaDmnDS_63_Contagemresultado_dataprevista_to5)";
            }
         }
         else
         {
            GXv_int1[66] = 1;
         }
         if ( AV214WP_CaixaDmnDS_56_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV215WP_CaixaDmnDS_57_Dynamicfiltersselector5, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ! (0==AV222WP_CaixaDmnDS_64_Contagemresultado_contratadacod5) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV222WP_CaixaDmnDS_64_Contagemresultado_contratadacod5)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_ContratadaCod] = @AV222WP_CaixaDmnDS_64_Contagemresultado_contratadacod5)";
            }
         }
         else
         {
            GXv_int1[67] = 1;
         }
         if ( AV214WP_CaixaDmnDS_56_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV215WP_CaixaDmnDS_57_Dynamicfiltersselector5, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV223WP_CaixaDmnDS_65_Contagemresultado_servico5) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV223WP_CaixaDmnDS_65_Contagemresultado_servico5)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV223WP_CaixaDmnDS_65_Contagemresultado_servico5)";
            }
         }
         else
         {
            GXv_int1[68] = 1;
         }
         if ( AV214WP_CaixaDmnDS_56_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV215WP_CaixaDmnDS_57_Dynamicfiltersselector5, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV224WP_CaixaDmnDS_66_Contagemresultado_sistemacod5) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV224WP_CaixaDmnDS_66_Contagemresultado_sistemacod5)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_SistemaCod] = @AV224WP_CaixaDmnDS_66_Contagemresultado_sistemacod5)";
            }
         }
         else
         {
            GXv_int1[69] = 1;
         }
         if ( AV214WP_CaixaDmnDS_56_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV215WP_CaixaDmnDS_57_Dynamicfiltersselector5, "CONTAGEMRESULTADO_RESPONSAVEL") == 0 ) && ( ! (0==AV225WP_CaixaDmnDS_67_Contagemresultado_responsavel5) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Responsavel] = @AV225WP_CaixaDmnDS_67_Contagemresultado_responsavel5)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Responsavel] = @AV225WP_CaixaDmnDS_67_Contagemresultado_responsavel5)";
            }
         }
         else
         {
            GXv_int1[70] = 1;
         }
         if ( AV214WP_CaixaDmnDS_56_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV215WP_CaixaDmnDS_57_Dynamicfiltersselector5, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV216WP_CaixaDmnDS_58_Dynamicfiltersoperator5 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV226WP_CaixaDmnDS_68_Contagemresultado_agrupador5)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV226WP_CaixaDmnDS_68_Contagemresultado_agrupador5)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Agrupador] = @AV226WP_CaixaDmnDS_68_Contagemresultado_agrupador5)";
            }
         }
         else
         {
            GXv_int1[71] = 1;
         }
         if ( AV214WP_CaixaDmnDS_56_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV215WP_CaixaDmnDS_57_Dynamicfiltersselector5, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV216WP_CaixaDmnDS_58_Dynamicfiltersoperator5 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV226WP_CaixaDmnDS_68_Contagemresultado_agrupador5)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like @lV226WP_CaixaDmnDS_68_Contagemresultado_agrupador5)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Agrupador] like @lV226WP_CaixaDmnDS_68_Contagemresultado_agrupador5)";
            }
         }
         else
         {
            GXv_int1[72] = 1;
         }
         if ( AV214WP_CaixaDmnDS_56_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV215WP_CaixaDmnDS_57_Dynamicfiltersselector5, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV216WP_CaixaDmnDS_58_Dynamicfiltersoperator5 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV226WP_CaixaDmnDS_68_Contagemresultado_agrupador5)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like '%' + @lV226WP_CaixaDmnDS_68_Contagemresultado_agrupador5)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Agrupador] like '%' + @lV226WP_CaixaDmnDS_68_Contagemresultado_agrupador5)";
            }
         }
         else
         {
            GXv_int1[73] = 1;
         }
         if ( AV214WP_CaixaDmnDS_56_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV215WP_CaixaDmnDS_57_Dynamicfiltersselector5, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV227WP_CaixaDmnDS_69_Contagemresultado_statusdmn5)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV227WP_CaixaDmnDS_69_Contagemresultado_statusdmn5)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_StatusDmn] = @AV227WP_CaixaDmnDS_69_Contagemresultado_statusdmn5)";
            }
         }
         else
         {
            GXv_int1[74] = 1;
         }
         if ( AV228WP_CaixaDmnDS_70_Dynamicfiltersenabled6 && ( StringUtil.StrCmp(AV229WP_CaixaDmnDS_71_Dynamicfiltersselector6, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV230WP_CaixaDmnDS_72_Dynamicfiltersoperator6 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV231WP_CaixaDmnDS_73_Contagemresultado_demanda6)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV231WP_CaixaDmnDS_73_Contagemresultado_demanda6)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Demanda] = @AV231WP_CaixaDmnDS_73_Contagemresultado_demanda6)";
            }
         }
         else
         {
            GXv_int1[75] = 1;
         }
         if ( AV228WP_CaixaDmnDS_70_Dynamicfiltersenabled6 && ( StringUtil.StrCmp(AV229WP_CaixaDmnDS_71_Dynamicfiltersselector6, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV230WP_CaixaDmnDS_72_Dynamicfiltersoperator6 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV231WP_CaixaDmnDS_73_Contagemresultado_demanda6)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV231WP_CaixaDmnDS_73_Contagemresultado_demanda6)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Demanda] like @lV231WP_CaixaDmnDS_73_Contagemresultado_demanda6)";
            }
         }
         else
         {
            GXv_int1[76] = 1;
         }
         if ( AV228WP_CaixaDmnDS_70_Dynamicfiltersenabled6 && ( StringUtil.StrCmp(AV229WP_CaixaDmnDS_71_Dynamicfiltersselector6, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV230WP_CaixaDmnDS_72_Dynamicfiltersoperator6 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV231WP_CaixaDmnDS_73_Contagemresultado_demanda6)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV231WP_CaixaDmnDS_73_Contagemresultado_demanda6)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Demanda] like '%' + @lV231WP_CaixaDmnDS_73_Contagemresultado_demanda6)";
            }
         }
         else
         {
            GXv_int1[77] = 1;
         }
         if ( AV228WP_CaixaDmnDS_70_Dynamicfiltersenabled6 && ( StringUtil.StrCmp(AV229WP_CaixaDmnDS_71_Dynamicfiltersselector6, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV232WP_CaixaDmnDS_74_Contagemresultado_datadmn6) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV232WP_CaixaDmnDS_74_Contagemresultado_datadmn6)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataDmn] >= @AV232WP_CaixaDmnDS_74_Contagemresultado_datadmn6)";
            }
         }
         else
         {
            GXv_int1[78] = 1;
         }
         if ( AV228WP_CaixaDmnDS_70_Dynamicfiltersenabled6 && ( StringUtil.StrCmp(AV229WP_CaixaDmnDS_71_Dynamicfiltersselector6, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV233WP_CaixaDmnDS_75_Contagemresultado_datadmn_to6) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV233WP_CaixaDmnDS_75_Contagemresultado_datadmn_to6)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataDmn] <= @AV233WP_CaixaDmnDS_75_Contagemresultado_datadmn_to6)";
            }
         }
         else
         {
            GXv_int1[79] = 1;
         }
         if ( AV228WP_CaixaDmnDS_70_Dynamicfiltersenabled6 && ( StringUtil.StrCmp(AV229WP_CaixaDmnDS_71_Dynamicfiltersselector6, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV234WP_CaixaDmnDS_76_Contagemresultado_dataprevista6) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV234WP_CaixaDmnDS_76_Contagemresultado_dataprevista6)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataPrevista] >= @AV234WP_CaixaDmnDS_76_Contagemresultado_dataprevista6)";
            }
         }
         else
         {
            GXv_int1[80] = 1;
         }
         if ( AV228WP_CaixaDmnDS_70_Dynamicfiltersenabled6 && ( StringUtil.StrCmp(AV229WP_CaixaDmnDS_71_Dynamicfiltersselector6, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV235WP_CaixaDmnDS_77_Contagemresultado_dataprevista_to6) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] <= @AV235WP_CaixaDmnDS_77_Contagemresultado_dataprevista_to6)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataPrevista] <= @AV235WP_CaixaDmnDS_77_Contagemresultado_dataprevista_to6)";
            }
         }
         else
         {
            GXv_int1[81] = 1;
         }
         if ( AV228WP_CaixaDmnDS_70_Dynamicfiltersenabled6 && ( StringUtil.StrCmp(AV229WP_CaixaDmnDS_71_Dynamicfiltersselector6, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ! (0==AV236WP_CaixaDmnDS_78_Contagemresultado_contratadacod6) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV236WP_CaixaDmnDS_78_Contagemresultado_contratadacod6)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_ContratadaCod] = @AV236WP_CaixaDmnDS_78_Contagemresultado_contratadacod6)";
            }
         }
         else
         {
            GXv_int1[82] = 1;
         }
         if ( AV228WP_CaixaDmnDS_70_Dynamicfiltersenabled6 && ( StringUtil.StrCmp(AV229WP_CaixaDmnDS_71_Dynamicfiltersselector6, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV237WP_CaixaDmnDS_79_Contagemresultado_servico6) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV237WP_CaixaDmnDS_79_Contagemresultado_servico6)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV237WP_CaixaDmnDS_79_Contagemresultado_servico6)";
            }
         }
         else
         {
            GXv_int1[83] = 1;
         }
         if ( AV228WP_CaixaDmnDS_70_Dynamicfiltersenabled6 && ( StringUtil.StrCmp(AV229WP_CaixaDmnDS_71_Dynamicfiltersselector6, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV238WP_CaixaDmnDS_80_Contagemresultado_sistemacod6) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV238WP_CaixaDmnDS_80_Contagemresultado_sistemacod6)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_SistemaCod] = @AV238WP_CaixaDmnDS_80_Contagemresultado_sistemacod6)";
            }
         }
         else
         {
            GXv_int1[84] = 1;
         }
         if ( AV228WP_CaixaDmnDS_70_Dynamicfiltersenabled6 && ( StringUtil.StrCmp(AV229WP_CaixaDmnDS_71_Dynamicfiltersselector6, "CONTAGEMRESULTADO_RESPONSAVEL") == 0 ) && ( ! (0==AV239WP_CaixaDmnDS_81_Contagemresultado_responsavel6) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Responsavel] = @AV239WP_CaixaDmnDS_81_Contagemresultado_responsavel6)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Responsavel] = @AV239WP_CaixaDmnDS_81_Contagemresultado_responsavel6)";
            }
         }
         else
         {
            GXv_int1[85] = 1;
         }
         if ( AV228WP_CaixaDmnDS_70_Dynamicfiltersenabled6 && ( StringUtil.StrCmp(AV229WP_CaixaDmnDS_71_Dynamicfiltersselector6, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV230WP_CaixaDmnDS_72_Dynamicfiltersoperator6 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV240WP_CaixaDmnDS_82_Contagemresultado_agrupador6)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV240WP_CaixaDmnDS_82_Contagemresultado_agrupador6)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Agrupador] = @AV240WP_CaixaDmnDS_82_Contagemresultado_agrupador6)";
            }
         }
         else
         {
            GXv_int1[86] = 1;
         }
         if ( AV228WP_CaixaDmnDS_70_Dynamicfiltersenabled6 && ( StringUtil.StrCmp(AV229WP_CaixaDmnDS_71_Dynamicfiltersselector6, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV230WP_CaixaDmnDS_72_Dynamicfiltersoperator6 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV240WP_CaixaDmnDS_82_Contagemresultado_agrupador6)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like @lV240WP_CaixaDmnDS_82_Contagemresultado_agrupador6)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Agrupador] like @lV240WP_CaixaDmnDS_82_Contagemresultado_agrupador6)";
            }
         }
         else
         {
            GXv_int1[87] = 1;
         }
         if ( AV228WP_CaixaDmnDS_70_Dynamicfiltersenabled6 && ( StringUtil.StrCmp(AV229WP_CaixaDmnDS_71_Dynamicfiltersselector6, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV230WP_CaixaDmnDS_72_Dynamicfiltersoperator6 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV240WP_CaixaDmnDS_82_Contagemresultado_agrupador6)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like '%' + @lV240WP_CaixaDmnDS_82_Contagemresultado_agrupador6)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Agrupador] like '%' + @lV240WP_CaixaDmnDS_82_Contagemresultado_agrupador6)";
            }
         }
         else
         {
            GXv_int1[88] = 1;
         }
         if ( AV228WP_CaixaDmnDS_70_Dynamicfiltersenabled6 && ( StringUtil.StrCmp(AV229WP_CaixaDmnDS_71_Dynamicfiltersselector6, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV241WP_CaixaDmnDS_83_Contagemresultado_statusdmn6)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV241WP_CaixaDmnDS_83_Contagemresultado_statusdmn6)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_StatusDmn] = @AV241WP_CaixaDmnDS_83_Contagemresultado_statusdmn6)";
            }
         }
         else
         {
            GXv_int1[89] = 1;
         }
         if ( AV242WP_CaixaDmnDS_84_Dynamicfiltersenabled7 && ( StringUtil.StrCmp(AV243WP_CaixaDmnDS_85_Dynamicfiltersselector7, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV244WP_CaixaDmnDS_86_Dynamicfiltersoperator7 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV245WP_CaixaDmnDS_87_Contagemresultado_demanda7)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV245WP_CaixaDmnDS_87_Contagemresultado_demanda7)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Demanda] = @AV245WP_CaixaDmnDS_87_Contagemresultado_demanda7)";
            }
         }
         else
         {
            GXv_int1[90] = 1;
         }
         if ( AV242WP_CaixaDmnDS_84_Dynamicfiltersenabled7 && ( StringUtil.StrCmp(AV243WP_CaixaDmnDS_85_Dynamicfiltersselector7, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV244WP_CaixaDmnDS_86_Dynamicfiltersoperator7 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV245WP_CaixaDmnDS_87_Contagemresultado_demanda7)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV245WP_CaixaDmnDS_87_Contagemresultado_demanda7)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Demanda] like @lV245WP_CaixaDmnDS_87_Contagemresultado_demanda7)";
            }
         }
         else
         {
            GXv_int1[91] = 1;
         }
         if ( AV242WP_CaixaDmnDS_84_Dynamicfiltersenabled7 && ( StringUtil.StrCmp(AV243WP_CaixaDmnDS_85_Dynamicfiltersselector7, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV244WP_CaixaDmnDS_86_Dynamicfiltersoperator7 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV245WP_CaixaDmnDS_87_Contagemresultado_demanda7)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV245WP_CaixaDmnDS_87_Contagemresultado_demanda7)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Demanda] like '%' + @lV245WP_CaixaDmnDS_87_Contagemresultado_demanda7)";
            }
         }
         else
         {
            GXv_int1[92] = 1;
         }
         if ( AV242WP_CaixaDmnDS_84_Dynamicfiltersenabled7 && ( StringUtil.StrCmp(AV243WP_CaixaDmnDS_85_Dynamicfiltersselector7, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV246WP_CaixaDmnDS_88_Contagemresultado_datadmn7) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV246WP_CaixaDmnDS_88_Contagemresultado_datadmn7)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataDmn] >= @AV246WP_CaixaDmnDS_88_Contagemresultado_datadmn7)";
            }
         }
         else
         {
            GXv_int1[93] = 1;
         }
         if ( AV242WP_CaixaDmnDS_84_Dynamicfiltersenabled7 && ( StringUtil.StrCmp(AV243WP_CaixaDmnDS_85_Dynamicfiltersselector7, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV247WP_CaixaDmnDS_89_Contagemresultado_datadmn_to7) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV247WP_CaixaDmnDS_89_Contagemresultado_datadmn_to7)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataDmn] <= @AV247WP_CaixaDmnDS_89_Contagemresultado_datadmn_to7)";
            }
         }
         else
         {
            GXv_int1[94] = 1;
         }
         if ( AV242WP_CaixaDmnDS_84_Dynamicfiltersenabled7 && ( StringUtil.StrCmp(AV243WP_CaixaDmnDS_85_Dynamicfiltersselector7, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV248WP_CaixaDmnDS_90_Contagemresultado_dataprevista7) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV248WP_CaixaDmnDS_90_Contagemresultado_dataprevista7)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataPrevista] >= @AV248WP_CaixaDmnDS_90_Contagemresultado_dataprevista7)";
            }
         }
         else
         {
            GXv_int1[95] = 1;
         }
         if ( AV242WP_CaixaDmnDS_84_Dynamicfiltersenabled7 && ( StringUtil.StrCmp(AV243WP_CaixaDmnDS_85_Dynamicfiltersselector7, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV249WP_CaixaDmnDS_91_Contagemresultado_dataprevista_to7) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] <= @AV249WP_CaixaDmnDS_91_Contagemresultado_dataprevista_to7)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataPrevista] <= @AV249WP_CaixaDmnDS_91_Contagemresultado_dataprevista_to7)";
            }
         }
         else
         {
            GXv_int1[96] = 1;
         }
         if ( AV242WP_CaixaDmnDS_84_Dynamicfiltersenabled7 && ( StringUtil.StrCmp(AV243WP_CaixaDmnDS_85_Dynamicfiltersselector7, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ! (0==AV250WP_CaixaDmnDS_92_Contagemresultado_contratadacod7) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV250WP_CaixaDmnDS_92_Contagemresultado_contratadacod7)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_ContratadaCod] = @AV250WP_CaixaDmnDS_92_Contagemresultado_contratadacod7)";
            }
         }
         else
         {
            GXv_int1[97] = 1;
         }
         if ( AV242WP_CaixaDmnDS_84_Dynamicfiltersenabled7 && ( StringUtil.StrCmp(AV243WP_CaixaDmnDS_85_Dynamicfiltersselector7, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV251WP_CaixaDmnDS_93_Contagemresultado_servico7) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV251WP_CaixaDmnDS_93_Contagemresultado_servico7)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV251WP_CaixaDmnDS_93_Contagemresultado_servico7)";
            }
         }
         else
         {
            GXv_int1[98] = 1;
         }
         if ( AV242WP_CaixaDmnDS_84_Dynamicfiltersenabled7 && ( StringUtil.StrCmp(AV243WP_CaixaDmnDS_85_Dynamicfiltersselector7, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV252WP_CaixaDmnDS_94_Contagemresultado_sistemacod7) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV252WP_CaixaDmnDS_94_Contagemresultado_sistemacod7)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_SistemaCod] = @AV252WP_CaixaDmnDS_94_Contagemresultado_sistemacod7)";
            }
         }
         else
         {
            GXv_int1[99] = 1;
         }
         if ( AV242WP_CaixaDmnDS_84_Dynamicfiltersenabled7 && ( StringUtil.StrCmp(AV243WP_CaixaDmnDS_85_Dynamicfiltersselector7, "CONTAGEMRESULTADO_RESPONSAVEL") == 0 ) && ( ! (0==AV253WP_CaixaDmnDS_95_Contagemresultado_responsavel7) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Responsavel] = @AV253WP_CaixaDmnDS_95_Contagemresultado_responsavel7)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Responsavel] = @AV253WP_CaixaDmnDS_95_Contagemresultado_responsavel7)";
            }
         }
         else
         {
            GXv_int1[100] = 1;
         }
         if ( AV242WP_CaixaDmnDS_84_Dynamicfiltersenabled7 && ( StringUtil.StrCmp(AV243WP_CaixaDmnDS_85_Dynamicfiltersselector7, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV244WP_CaixaDmnDS_86_Dynamicfiltersoperator7 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV254WP_CaixaDmnDS_96_Contagemresultado_agrupador7)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV254WP_CaixaDmnDS_96_Contagemresultado_agrupador7)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Agrupador] = @AV254WP_CaixaDmnDS_96_Contagemresultado_agrupador7)";
            }
         }
         else
         {
            GXv_int1[101] = 1;
         }
         if ( AV242WP_CaixaDmnDS_84_Dynamicfiltersenabled7 && ( StringUtil.StrCmp(AV243WP_CaixaDmnDS_85_Dynamicfiltersselector7, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV244WP_CaixaDmnDS_86_Dynamicfiltersoperator7 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV254WP_CaixaDmnDS_96_Contagemresultado_agrupador7)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like @lV254WP_CaixaDmnDS_96_Contagemresultado_agrupador7)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Agrupador] like @lV254WP_CaixaDmnDS_96_Contagemresultado_agrupador7)";
            }
         }
         else
         {
            GXv_int1[102] = 1;
         }
         if ( AV242WP_CaixaDmnDS_84_Dynamicfiltersenabled7 && ( StringUtil.StrCmp(AV243WP_CaixaDmnDS_85_Dynamicfiltersselector7, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV244WP_CaixaDmnDS_86_Dynamicfiltersoperator7 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV254WP_CaixaDmnDS_96_Contagemresultado_agrupador7)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like '%' + @lV254WP_CaixaDmnDS_96_Contagemresultado_agrupador7)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Agrupador] like '%' + @lV254WP_CaixaDmnDS_96_Contagemresultado_agrupador7)";
            }
         }
         else
         {
            GXv_int1[103] = 1;
         }
         if ( AV242WP_CaixaDmnDS_84_Dynamicfiltersenabled7 && ( StringUtil.StrCmp(AV243WP_CaixaDmnDS_85_Dynamicfiltersselector7, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV255WP_CaixaDmnDS_97_Contagemresultado_statusdmn7)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV255WP_CaixaDmnDS_97_Contagemresultado_statusdmn7)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_StatusDmn] = @AV255WP_CaixaDmnDS_97_Contagemresultado_statusdmn7)";
            }
         }
         else
         {
            GXv_int1[104] = 1;
         }
         if ( AV256WP_CaixaDmnDS_98_Dynamicfiltersenabled8 && ( StringUtil.StrCmp(AV257WP_CaixaDmnDS_99_Dynamicfiltersselector8, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV258WP_CaixaDmnDS_100_Dynamicfiltersoperator8 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV259WP_CaixaDmnDS_101_Contagemresultado_demanda8)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV259WP_CaixaDmnDS_101_Contagemresultado_demanda8)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Demanda] = @AV259WP_CaixaDmnDS_101_Contagemresultado_demanda8)";
            }
         }
         else
         {
            GXv_int1[105] = 1;
         }
         if ( AV256WP_CaixaDmnDS_98_Dynamicfiltersenabled8 && ( StringUtil.StrCmp(AV257WP_CaixaDmnDS_99_Dynamicfiltersselector8, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV258WP_CaixaDmnDS_100_Dynamicfiltersoperator8 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV259WP_CaixaDmnDS_101_Contagemresultado_demanda8)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV259WP_CaixaDmnDS_101_Contagemresultado_demanda8)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Demanda] like @lV259WP_CaixaDmnDS_101_Contagemresultado_demanda8)";
            }
         }
         else
         {
            GXv_int1[106] = 1;
         }
         if ( AV256WP_CaixaDmnDS_98_Dynamicfiltersenabled8 && ( StringUtil.StrCmp(AV257WP_CaixaDmnDS_99_Dynamicfiltersselector8, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV258WP_CaixaDmnDS_100_Dynamicfiltersoperator8 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV259WP_CaixaDmnDS_101_Contagemresultado_demanda8)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV259WP_CaixaDmnDS_101_Contagemresultado_demanda8)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Demanda] like '%' + @lV259WP_CaixaDmnDS_101_Contagemresultado_demanda8)";
            }
         }
         else
         {
            GXv_int1[107] = 1;
         }
         if ( AV256WP_CaixaDmnDS_98_Dynamicfiltersenabled8 && ( StringUtil.StrCmp(AV257WP_CaixaDmnDS_99_Dynamicfiltersselector8, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV260WP_CaixaDmnDS_102_Contagemresultado_datadmn8) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV260WP_CaixaDmnDS_102_Contagemresultado_datadmn8)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataDmn] >= @AV260WP_CaixaDmnDS_102_Contagemresultado_datadmn8)";
            }
         }
         else
         {
            GXv_int1[108] = 1;
         }
         if ( AV256WP_CaixaDmnDS_98_Dynamicfiltersenabled8 && ( StringUtil.StrCmp(AV257WP_CaixaDmnDS_99_Dynamicfiltersselector8, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV261WP_CaixaDmnDS_103_Contagemresultado_datadmn_to8) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV261WP_CaixaDmnDS_103_Contagemresultado_datadmn_to8)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataDmn] <= @AV261WP_CaixaDmnDS_103_Contagemresultado_datadmn_to8)";
            }
         }
         else
         {
            GXv_int1[109] = 1;
         }
         if ( AV256WP_CaixaDmnDS_98_Dynamicfiltersenabled8 && ( StringUtil.StrCmp(AV257WP_CaixaDmnDS_99_Dynamicfiltersselector8, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV262WP_CaixaDmnDS_104_Contagemresultado_dataprevista8) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV262WP_CaixaDmnDS_104_Contagemresultado_dataprevista8)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataPrevista] >= @AV262WP_CaixaDmnDS_104_Contagemresultado_dataprevista8)";
            }
         }
         else
         {
            GXv_int1[110] = 1;
         }
         if ( AV256WP_CaixaDmnDS_98_Dynamicfiltersenabled8 && ( StringUtil.StrCmp(AV257WP_CaixaDmnDS_99_Dynamicfiltersselector8, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV263WP_CaixaDmnDS_105_Contagemresultado_dataprevista_to8) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] <= @AV263WP_CaixaDmnDS_105_Contagemresultado_dataprevista_to8)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataPrevista] <= @AV263WP_CaixaDmnDS_105_Contagemresultado_dataprevista_to8)";
            }
         }
         else
         {
            GXv_int1[111] = 1;
         }
         if ( AV256WP_CaixaDmnDS_98_Dynamicfiltersenabled8 && ( StringUtil.StrCmp(AV257WP_CaixaDmnDS_99_Dynamicfiltersselector8, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ! (0==AV264WP_CaixaDmnDS_106_Contagemresultado_contratadacod8) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV264WP_CaixaDmnDS_106_Contagemresultado_contratadacod8)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_ContratadaCod] = @AV264WP_CaixaDmnDS_106_Contagemresultado_contratadacod8)";
            }
         }
         else
         {
            GXv_int1[112] = 1;
         }
         if ( AV256WP_CaixaDmnDS_98_Dynamicfiltersenabled8 && ( StringUtil.StrCmp(AV257WP_CaixaDmnDS_99_Dynamicfiltersselector8, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV265WP_CaixaDmnDS_107_Contagemresultado_servico8) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV265WP_CaixaDmnDS_107_Contagemresultado_servico8)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV265WP_CaixaDmnDS_107_Contagemresultado_servico8)";
            }
         }
         else
         {
            GXv_int1[113] = 1;
         }
         if ( AV256WP_CaixaDmnDS_98_Dynamicfiltersenabled8 && ( StringUtil.StrCmp(AV257WP_CaixaDmnDS_99_Dynamicfiltersselector8, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV266WP_CaixaDmnDS_108_Contagemresultado_sistemacod8) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV266WP_CaixaDmnDS_108_Contagemresultado_sistemacod8)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_SistemaCod] = @AV266WP_CaixaDmnDS_108_Contagemresultado_sistemacod8)";
            }
         }
         else
         {
            GXv_int1[114] = 1;
         }
         if ( AV256WP_CaixaDmnDS_98_Dynamicfiltersenabled8 && ( StringUtil.StrCmp(AV257WP_CaixaDmnDS_99_Dynamicfiltersselector8, "CONTAGEMRESULTADO_RESPONSAVEL") == 0 ) && ( ! (0==AV267WP_CaixaDmnDS_109_Contagemresultado_responsavel8) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Responsavel] = @AV267WP_CaixaDmnDS_109_Contagemresultado_responsavel8)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Responsavel] = @AV267WP_CaixaDmnDS_109_Contagemresultado_responsavel8)";
            }
         }
         else
         {
            GXv_int1[115] = 1;
         }
         if ( AV256WP_CaixaDmnDS_98_Dynamicfiltersenabled8 && ( StringUtil.StrCmp(AV257WP_CaixaDmnDS_99_Dynamicfiltersselector8, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV258WP_CaixaDmnDS_100_Dynamicfiltersoperator8 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV268WP_CaixaDmnDS_110_Contagemresultado_agrupador8)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV268WP_CaixaDmnDS_110_Contagemresultado_agrupador8)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Agrupador] = @AV268WP_CaixaDmnDS_110_Contagemresultado_agrupador8)";
            }
         }
         else
         {
            GXv_int1[116] = 1;
         }
         if ( AV256WP_CaixaDmnDS_98_Dynamicfiltersenabled8 && ( StringUtil.StrCmp(AV257WP_CaixaDmnDS_99_Dynamicfiltersselector8, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV258WP_CaixaDmnDS_100_Dynamicfiltersoperator8 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV268WP_CaixaDmnDS_110_Contagemresultado_agrupador8)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like @lV268WP_CaixaDmnDS_110_Contagemresultado_agrupador8)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Agrupador] like @lV268WP_CaixaDmnDS_110_Contagemresultado_agrupador8)";
            }
         }
         else
         {
            GXv_int1[117] = 1;
         }
         if ( AV256WP_CaixaDmnDS_98_Dynamicfiltersenabled8 && ( StringUtil.StrCmp(AV257WP_CaixaDmnDS_99_Dynamicfiltersselector8, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV258WP_CaixaDmnDS_100_Dynamicfiltersoperator8 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV268WP_CaixaDmnDS_110_Contagemresultado_agrupador8)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like '%' + @lV268WP_CaixaDmnDS_110_Contagemresultado_agrupador8)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Agrupador] like '%' + @lV268WP_CaixaDmnDS_110_Contagemresultado_agrupador8)";
            }
         }
         else
         {
            GXv_int1[118] = 1;
         }
         if ( AV256WP_CaixaDmnDS_98_Dynamicfiltersenabled8 && ( StringUtil.StrCmp(AV257WP_CaixaDmnDS_99_Dynamicfiltersselector8, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV269WP_CaixaDmnDS_111_Contagemresultado_statusdmn8)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV269WP_CaixaDmnDS_111_Contagemresultado_statusdmn8)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_StatusDmn] = @AV269WP_CaixaDmnDS_111_Contagemresultado_statusdmn8)";
            }
         }
         else
         {
            GXv_int1[119] = 1;
         }
         if ( AV270WP_CaixaDmnDS_112_Dynamicfiltersenabled9 && ( StringUtil.StrCmp(AV271WP_CaixaDmnDS_113_Dynamicfiltersselector9, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV272WP_CaixaDmnDS_114_Dynamicfiltersoperator9 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV273WP_CaixaDmnDS_115_Contagemresultado_demanda9)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV273WP_CaixaDmnDS_115_Contagemresultado_demanda9)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Demanda] = @AV273WP_CaixaDmnDS_115_Contagemresultado_demanda9)";
            }
         }
         else
         {
            GXv_int1[120] = 1;
         }
         if ( AV270WP_CaixaDmnDS_112_Dynamicfiltersenabled9 && ( StringUtil.StrCmp(AV271WP_CaixaDmnDS_113_Dynamicfiltersselector9, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV272WP_CaixaDmnDS_114_Dynamicfiltersoperator9 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV273WP_CaixaDmnDS_115_Contagemresultado_demanda9)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV273WP_CaixaDmnDS_115_Contagemresultado_demanda9)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Demanda] like @lV273WP_CaixaDmnDS_115_Contagemresultado_demanda9)";
            }
         }
         else
         {
            GXv_int1[121] = 1;
         }
         if ( AV270WP_CaixaDmnDS_112_Dynamicfiltersenabled9 && ( StringUtil.StrCmp(AV271WP_CaixaDmnDS_113_Dynamicfiltersselector9, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV272WP_CaixaDmnDS_114_Dynamicfiltersoperator9 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV273WP_CaixaDmnDS_115_Contagemresultado_demanda9)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV273WP_CaixaDmnDS_115_Contagemresultado_demanda9)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Demanda] like '%' + @lV273WP_CaixaDmnDS_115_Contagemresultado_demanda9)";
            }
         }
         else
         {
            GXv_int1[122] = 1;
         }
         if ( AV270WP_CaixaDmnDS_112_Dynamicfiltersenabled9 && ( StringUtil.StrCmp(AV271WP_CaixaDmnDS_113_Dynamicfiltersselector9, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV274WP_CaixaDmnDS_116_Contagemresultado_datadmn9) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV274WP_CaixaDmnDS_116_Contagemresultado_datadmn9)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataDmn] >= @AV274WP_CaixaDmnDS_116_Contagemresultado_datadmn9)";
            }
         }
         else
         {
            GXv_int1[123] = 1;
         }
         if ( AV270WP_CaixaDmnDS_112_Dynamicfiltersenabled9 && ( StringUtil.StrCmp(AV271WP_CaixaDmnDS_113_Dynamicfiltersselector9, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV275WP_CaixaDmnDS_117_Contagemresultado_datadmn_to9) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV275WP_CaixaDmnDS_117_Contagemresultado_datadmn_to9)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataDmn] <= @AV275WP_CaixaDmnDS_117_Contagemresultado_datadmn_to9)";
            }
         }
         else
         {
            GXv_int1[124] = 1;
         }
         if ( AV270WP_CaixaDmnDS_112_Dynamicfiltersenabled9 && ( StringUtil.StrCmp(AV271WP_CaixaDmnDS_113_Dynamicfiltersselector9, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV276WP_CaixaDmnDS_118_Contagemresultado_dataprevista9) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV276WP_CaixaDmnDS_118_Contagemresultado_dataprevista9)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataPrevista] >= @AV276WP_CaixaDmnDS_118_Contagemresultado_dataprevista9)";
            }
         }
         else
         {
            GXv_int1[125] = 1;
         }
         if ( AV270WP_CaixaDmnDS_112_Dynamicfiltersenabled9 && ( StringUtil.StrCmp(AV271WP_CaixaDmnDS_113_Dynamicfiltersselector9, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV277WP_CaixaDmnDS_119_Contagemresultado_dataprevista_to9) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] <= @AV277WP_CaixaDmnDS_119_Contagemresultado_dataprevista_to9)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataPrevista] <= @AV277WP_CaixaDmnDS_119_Contagemresultado_dataprevista_to9)";
            }
         }
         else
         {
            GXv_int1[126] = 1;
         }
         if ( AV270WP_CaixaDmnDS_112_Dynamicfiltersenabled9 && ( StringUtil.StrCmp(AV271WP_CaixaDmnDS_113_Dynamicfiltersselector9, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ! (0==AV278WP_CaixaDmnDS_120_Contagemresultado_contratadacod9) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV278WP_CaixaDmnDS_120_Contagemresultado_contratadacod9)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_ContratadaCod] = @AV278WP_CaixaDmnDS_120_Contagemresultado_contratadacod9)";
            }
         }
         else
         {
            GXv_int1[127] = 1;
         }
         if ( AV270WP_CaixaDmnDS_112_Dynamicfiltersenabled9 && ( StringUtil.StrCmp(AV271WP_CaixaDmnDS_113_Dynamicfiltersselector9, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV279WP_CaixaDmnDS_121_Contagemresultado_servico9) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV279WP_CaixaDmnDS_121_Contagemresultado_servico9)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV279WP_CaixaDmnDS_121_Contagemresultado_servico9)";
            }
         }
         else
         {
            GXv_int1[128] = 1;
         }
         if ( AV270WP_CaixaDmnDS_112_Dynamicfiltersenabled9 && ( StringUtil.StrCmp(AV271WP_CaixaDmnDS_113_Dynamicfiltersselector9, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV280WP_CaixaDmnDS_122_Contagemresultado_sistemacod9) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV280WP_CaixaDmnDS_122_Contagemresultado_sistemacod9)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_SistemaCod] = @AV280WP_CaixaDmnDS_122_Contagemresultado_sistemacod9)";
            }
         }
         else
         {
            GXv_int1[129] = 1;
         }
         if ( AV270WP_CaixaDmnDS_112_Dynamicfiltersenabled9 && ( StringUtil.StrCmp(AV271WP_CaixaDmnDS_113_Dynamicfiltersselector9, "CONTAGEMRESULTADO_RESPONSAVEL") == 0 ) && ( ! (0==AV281WP_CaixaDmnDS_123_Contagemresultado_responsavel9) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Responsavel] = @AV281WP_CaixaDmnDS_123_Contagemresultado_responsavel9)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Responsavel] = @AV281WP_CaixaDmnDS_123_Contagemresultado_responsavel9)";
            }
         }
         else
         {
            GXv_int1[130] = 1;
         }
         if ( AV270WP_CaixaDmnDS_112_Dynamicfiltersenabled9 && ( StringUtil.StrCmp(AV271WP_CaixaDmnDS_113_Dynamicfiltersselector9, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV272WP_CaixaDmnDS_114_Dynamicfiltersoperator9 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV282WP_CaixaDmnDS_124_Contagemresultado_agrupador9)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV282WP_CaixaDmnDS_124_Contagemresultado_agrupador9)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Agrupador] = @AV282WP_CaixaDmnDS_124_Contagemresultado_agrupador9)";
            }
         }
         else
         {
            GXv_int1[131] = 1;
         }
         if ( AV270WP_CaixaDmnDS_112_Dynamicfiltersenabled9 && ( StringUtil.StrCmp(AV271WP_CaixaDmnDS_113_Dynamicfiltersselector9, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV272WP_CaixaDmnDS_114_Dynamicfiltersoperator9 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV282WP_CaixaDmnDS_124_Contagemresultado_agrupador9)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like @lV282WP_CaixaDmnDS_124_Contagemresultado_agrupador9)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Agrupador] like @lV282WP_CaixaDmnDS_124_Contagemresultado_agrupador9)";
            }
         }
         else
         {
            GXv_int1[132] = 1;
         }
         if ( AV270WP_CaixaDmnDS_112_Dynamicfiltersenabled9 && ( StringUtil.StrCmp(AV271WP_CaixaDmnDS_113_Dynamicfiltersselector9, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV272WP_CaixaDmnDS_114_Dynamicfiltersoperator9 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV282WP_CaixaDmnDS_124_Contagemresultado_agrupador9)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like '%' + @lV282WP_CaixaDmnDS_124_Contagemresultado_agrupador9)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Agrupador] like '%' + @lV282WP_CaixaDmnDS_124_Contagemresultado_agrupador9)";
            }
         }
         else
         {
            GXv_int1[133] = 1;
         }
         if ( AV270WP_CaixaDmnDS_112_Dynamicfiltersenabled9 && ( StringUtil.StrCmp(AV271WP_CaixaDmnDS_113_Dynamicfiltersselector9, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV283WP_CaixaDmnDS_125_Contagemresultado_statusdmn9)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV283WP_CaixaDmnDS_125_Contagemresultado_statusdmn9)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_StatusDmn] = @AV283WP_CaixaDmnDS_125_Contagemresultado_statusdmn9)";
            }
         }
         else
         {
            GXv_int1[134] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV285WP_CaixaDmnDS_127_Tfcontagemresultado_demanda_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV284WP_CaixaDmnDS_126_Tfcontagemresultado_demanda)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV284WP_CaixaDmnDS_126_Tfcontagemresultado_demanda)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Demanda] like @lV284WP_CaixaDmnDS_126_Tfcontagemresultado_demanda)";
            }
         }
         else
         {
            GXv_int1[135] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV285WP_CaixaDmnDS_127_Tfcontagemresultado_demanda_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV285WP_CaixaDmnDS_127_Tfcontagemresultado_demanda_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Demanda] = @AV285WP_CaixaDmnDS_127_Tfcontagemresultado_demanda_sel)";
            }
         }
         else
         {
            GXv_int1[136] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Demanda]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00V92(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (int)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (bool)dynConstraints[27] , (String)dynConstraints[28] , (short)dynConstraints[29] , (String)dynConstraints[30] , (DateTime)dynConstraints[31] , (DateTime)dynConstraints[32] , (DateTime)dynConstraints[33] , (DateTime)dynConstraints[34] , (int)dynConstraints[35] , (int)dynConstraints[36] , (int)dynConstraints[37] , (int)dynConstraints[38] , (String)dynConstraints[39] , (String)dynConstraints[40] , (bool)dynConstraints[41] , (String)dynConstraints[42] , (short)dynConstraints[43] , (String)dynConstraints[44] , (DateTime)dynConstraints[45] , (DateTime)dynConstraints[46] , (DateTime)dynConstraints[47] , (DateTime)dynConstraints[48] , (int)dynConstraints[49] , (int)dynConstraints[50] , (int)dynConstraints[51] , (int)dynConstraints[52] , (String)dynConstraints[53] , (String)dynConstraints[54] , (bool)dynConstraints[55] , (String)dynConstraints[56] , (short)dynConstraints[57] , (String)dynConstraints[58] , (DateTime)dynConstraints[59] , (DateTime)dynConstraints[60] , (DateTime)dynConstraints[61] , (DateTime)dynConstraints[62] , (int)dynConstraints[63] , (int)dynConstraints[64] , (int)dynConstraints[65] , (int)dynConstraints[66] , (String)dynConstraints[67] , (String)dynConstraints[68] , (bool)dynConstraints[69] , (String)dynConstraints[70] , (short)dynConstraints[71] , (String)dynConstraints[72] , (DateTime)dynConstraints[73] , (DateTime)dynConstraints[74] , (DateTime)dynConstraints[75] , (DateTime)dynConstraints[76] , (int)dynConstraints[77] , (int)dynConstraints[78] , (int)dynConstraints[79] , (int)dynConstraints[80] , (String)dynConstraints[81] , (String)dynConstraints[82] , (bool)dynConstraints[83] , (String)dynConstraints[84] , (short)dynConstraints[85] , (String)dynConstraints[86] , (DateTime)dynConstraints[87] , (DateTime)dynConstraints[88] , (DateTime)dynConstraints[89] , (DateTime)dynConstraints[90] , (int)dynConstraints[91] , (int)dynConstraints[92] , (int)dynConstraints[93] , (int)dynConstraints[94] , (String)dynConstraints[95] , (String)dynConstraints[96] , (bool)dynConstraints[97] , (String)dynConstraints[98] , (short)dynConstraints[99] , (String)dynConstraints[100] , (DateTime)dynConstraints[101] , (DateTime)dynConstraints[102] , (DateTime)dynConstraints[103] , (DateTime)dynConstraints[104] , (int)dynConstraints[105] , (int)dynConstraints[106] , (int)dynConstraints[107] , (int)dynConstraints[108] , (String)dynConstraints[109] , (String)dynConstraints[110] , (bool)dynConstraints[111] , (String)dynConstraints[112] , (short)dynConstraints[113] , (String)dynConstraints[114] , (DateTime)dynConstraints[115] , (DateTime)dynConstraints[116] , (DateTime)dynConstraints[117] , (DateTime)dynConstraints[118] , (int)dynConstraints[119] , (int)dynConstraints[120] , (int)dynConstraints[121] , (int)dynConstraints[122] , (String)dynConstraints[123] , (String)dynConstraints[124] , (String)dynConstraints[125] , (String)dynConstraints[126] , (String)dynConstraints[127] , (DateTime)dynConstraints[128] , (DateTime)dynConstraints[129] , (int)dynConstraints[130] , (int)dynConstraints[131] , (int)dynConstraints[132] , (int)dynConstraints[133] , (String)dynConstraints[134] , (String)dynConstraints[135] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00V92 ;
          prmP00V92 = new Object[] {
          new Object[] {"@AV161WP_CaixaDmnDS_3_Contagemresultado_demanda1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV161WP_CaixaDmnDS_3_Contagemresultado_demanda1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV161WP_CaixaDmnDS_3_Contagemresultado_demanda1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV162WP_CaixaDmnDS_4_Contagemresultado_datadmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV163WP_CaixaDmnDS_5_Contagemresultado_datadmn_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV164WP_CaixaDmnDS_6_Contagemresultado_dataprevista1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV165WP_CaixaDmnDS_7_Contagemresultado_dataprevista_to1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV166WP_CaixaDmnDS_8_Contagemresultado_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV167WP_CaixaDmnDS_9_Contagemresultado_servico1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV168WP_CaixaDmnDS_10_Contagemresultado_sistemacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV169WP_CaixaDmnDS_11_Contagemresultado_responsavel1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV170WP_CaixaDmnDS_12_Contagemresultado_agrupador1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV170WP_CaixaDmnDS_12_Contagemresultado_agrupador1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV170WP_CaixaDmnDS_12_Contagemresultado_agrupador1",SqlDbType.Char,15,0} ,
          new Object[] {"@AV171WP_CaixaDmnDS_13_Contagemresultado_statusdmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV175WP_CaixaDmnDS_17_Contagemresultado_demanda2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV175WP_CaixaDmnDS_17_Contagemresultado_demanda2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV175WP_CaixaDmnDS_17_Contagemresultado_demanda2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV176WP_CaixaDmnDS_18_Contagemresultado_datadmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV177WP_CaixaDmnDS_19_Contagemresultado_datadmn_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV178WP_CaixaDmnDS_20_Contagemresultado_dataprevista2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV179WP_CaixaDmnDS_21_Contagemresultado_dataprevista_to2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV180WP_CaixaDmnDS_22_Contagemresultado_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV181WP_CaixaDmnDS_23_Contagemresultado_servico2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV182WP_CaixaDmnDS_24_Contagemresultado_sistemacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV183WP_CaixaDmnDS_25_Contagemresultado_responsavel2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV184WP_CaixaDmnDS_26_Contagemresultado_agrupador2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV184WP_CaixaDmnDS_26_Contagemresultado_agrupador2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV184WP_CaixaDmnDS_26_Contagemresultado_agrupador2",SqlDbType.Char,15,0} ,
          new Object[] {"@AV185WP_CaixaDmnDS_27_Contagemresultado_statusdmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV189WP_CaixaDmnDS_31_Contagemresultado_demanda3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV189WP_CaixaDmnDS_31_Contagemresultado_demanda3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV189WP_CaixaDmnDS_31_Contagemresultado_demanda3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV190WP_CaixaDmnDS_32_Contagemresultado_datadmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV191WP_CaixaDmnDS_33_Contagemresultado_datadmn_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV192WP_CaixaDmnDS_34_Contagemresultado_dataprevista3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV193WP_CaixaDmnDS_35_Contagemresultado_dataprevista_to3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV194WP_CaixaDmnDS_36_Contagemresultado_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV195WP_CaixaDmnDS_37_Contagemresultado_servico3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV196WP_CaixaDmnDS_38_Contagemresultado_sistemacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV197WP_CaixaDmnDS_39_Contagemresultado_responsavel3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV198WP_CaixaDmnDS_40_Contagemresultado_agrupador3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV198WP_CaixaDmnDS_40_Contagemresultado_agrupador3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV198WP_CaixaDmnDS_40_Contagemresultado_agrupador3",SqlDbType.Char,15,0} ,
          new Object[] {"@AV199WP_CaixaDmnDS_41_Contagemresultado_statusdmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV203WP_CaixaDmnDS_45_Contagemresultado_demanda4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV203WP_CaixaDmnDS_45_Contagemresultado_demanda4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV203WP_CaixaDmnDS_45_Contagemresultado_demanda4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV204WP_CaixaDmnDS_46_Contagemresultado_datadmn4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV205WP_CaixaDmnDS_47_Contagemresultado_datadmn_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV206WP_CaixaDmnDS_48_Contagemresultado_dataprevista4",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV207WP_CaixaDmnDS_49_Contagemresultado_dataprevista_to4",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV208WP_CaixaDmnDS_50_Contagemresultado_contratadacod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV209WP_CaixaDmnDS_51_Contagemresultado_servico4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV210WP_CaixaDmnDS_52_Contagemresultado_sistemacod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV211WP_CaixaDmnDS_53_Contagemresultado_responsavel4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV212WP_CaixaDmnDS_54_Contagemresultado_agrupador4",SqlDbType.Char,15,0} ,
          new Object[] {"@lV212WP_CaixaDmnDS_54_Contagemresultado_agrupador4",SqlDbType.Char,15,0} ,
          new Object[] {"@lV212WP_CaixaDmnDS_54_Contagemresultado_agrupador4",SqlDbType.Char,15,0} ,
          new Object[] {"@AV213WP_CaixaDmnDS_55_Contagemresultado_statusdmn4",SqlDbType.Char,1,0} ,
          new Object[] {"@AV217WP_CaixaDmnDS_59_Contagemresultado_demanda5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV217WP_CaixaDmnDS_59_Contagemresultado_demanda5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV217WP_CaixaDmnDS_59_Contagemresultado_demanda5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV218WP_CaixaDmnDS_60_Contagemresultado_datadmn5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV219WP_CaixaDmnDS_61_Contagemresultado_datadmn_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV220WP_CaixaDmnDS_62_Contagemresultado_dataprevista5",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV221WP_CaixaDmnDS_63_Contagemresultado_dataprevista_to5",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV222WP_CaixaDmnDS_64_Contagemresultado_contratadacod5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV223WP_CaixaDmnDS_65_Contagemresultado_servico5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV224WP_CaixaDmnDS_66_Contagemresultado_sistemacod5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV225WP_CaixaDmnDS_67_Contagemresultado_responsavel5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV226WP_CaixaDmnDS_68_Contagemresultado_agrupador5",SqlDbType.Char,15,0} ,
          new Object[] {"@lV226WP_CaixaDmnDS_68_Contagemresultado_agrupador5",SqlDbType.Char,15,0} ,
          new Object[] {"@lV226WP_CaixaDmnDS_68_Contagemresultado_agrupador5",SqlDbType.Char,15,0} ,
          new Object[] {"@AV227WP_CaixaDmnDS_69_Contagemresultado_statusdmn5",SqlDbType.Char,1,0} ,
          new Object[] {"@AV231WP_CaixaDmnDS_73_Contagemresultado_demanda6",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV231WP_CaixaDmnDS_73_Contagemresultado_demanda6",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV231WP_CaixaDmnDS_73_Contagemresultado_demanda6",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV232WP_CaixaDmnDS_74_Contagemresultado_datadmn6",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV233WP_CaixaDmnDS_75_Contagemresultado_datadmn_to6",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV234WP_CaixaDmnDS_76_Contagemresultado_dataprevista6",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV235WP_CaixaDmnDS_77_Contagemresultado_dataprevista_to6",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV236WP_CaixaDmnDS_78_Contagemresultado_contratadacod6",SqlDbType.Int,6,0} ,
          new Object[] {"@AV237WP_CaixaDmnDS_79_Contagemresultado_servico6",SqlDbType.Int,6,0} ,
          new Object[] {"@AV238WP_CaixaDmnDS_80_Contagemresultado_sistemacod6",SqlDbType.Int,6,0} ,
          new Object[] {"@AV239WP_CaixaDmnDS_81_Contagemresultado_responsavel6",SqlDbType.Int,6,0} ,
          new Object[] {"@AV240WP_CaixaDmnDS_82_Contagemresultado_agrupador6",SqlDbType.Char,15,0} ,
          new Object[] {"@lV240WP_CaixaDmnDS_82_Contagemresultado_agrupador6",SqlDbType.Char,15,0} ,
          new Object[] {"@lV240WP_CaixaDmnDS_82_Contagemresultado_agrupador6",SqlDbType.Char,15,0} ,
          new Object[] {"@AV241WP_CaixaDmnDS_83_Contagemresultado_statusdmn6",SqlDbType.Char,1,0} ,
          new Object[] {"@AV245WP_CaixaDmnDS_87_Contagemresultado_demanda7",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV245WP_CaixaDmnDS_87_Contagemresultado_demanda7",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV245WP_CaixaDmnDS_87_Contagemresultado_demanda7",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV246WP_CaixaDmnDS_88_Contagemresultado_datadmn7",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV247WP_CaixaDmnDS_89_Contagemresultado_datadmn_to7",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV248WP_CaixaDmnDS_90_Contagemresultado_dataprevista7",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV249WP_CaixaDmnDS_91_Contagemresultado_dataprevista_to7",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV250WP_CaixaDmnDS_92_Contagemresultado_contratadacod7",SqlDbType.Int,6,0} ,
          new Object[] {"@AV251WP_CaixaDmnDS_93_Contagemresultado_servico7",SqlDbType.Int,6,0} ,
          new Object[] {"@AV252WP_CaixaDmnDS_94_Contagemresultado_sistemacod7",SqlDbType.Int,6,0} ,
          new Object[] {"@AV253WP_CaixaDmnDS_95_Contagemresultado_responsavel7",SqlDbType.Int,6,0} ,
          new Object[] {"@AV254WP_CaixaDmnDS_96_Contagemresultado_agrupador7",SqlDbType.Char,15,0} ,
          new Object[] {"@lV254WP_CaixaDmnDS_96_Contagemresultado_agrupador7",SqlDbType.Char,15,0} ,
          new Object[] {"@lV254WP_CaixaDmnDS_96_Contagemresultado_agrupador7",SqlDbType.Char,15,0} ,
          new Object[] {"@AV255WP_CaixaDmnDS_97_Contagemresultado_statusdmn7",SqlDbType.Char,1,0} ,
          new Object[] {"@AV259WP_CaixaDmnDS_101_Contagemresultado_demanda8",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV259WP_CaixaDmnDS_101_Contagemresultado_demanda8",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV259WP_CaixaDmnDS_101_Contagemresultado_demanda8",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV260WP_CaixaDmnDS_102_Contagemresultado_datadmn8",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV261WP_CaixaDmnDS_103_Contagemresultado_datadmn_to8",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV262WP_CaixaDmnDS_104_Contagemresultado_dataprevista8",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV263WP_CaixaDmnDS_105_Contagemresultado_dataprevista_to8",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV264WP_CaixaDmnDS_106_Contagemresultado_contratadacod8",SqlDbType.Int,6,0} ,
          new Object[] {"@AV265WP_CaixaDmnDS_107_Contagemresultado_servico8",SqlDbType.Int,6,0} ,
          new Object[] {"@AV266WP_CaixaDmnDS_108_Contagemresultado_sistemacod8",SqlDbType.Int,6,0} ,
          new Object[] {"@AV267WP_CaixaDmnDS_109_Contagemresultado_responsavel8",SqlDbType.Int,6,0} ,
          new Object[] {"@AV268WP_CaixaDmnDS_110_Contagemresultado_agrupador8",SqlDbType.Char,15,0} ,
          new Object[] {"@lV268WP_CaixaDmnDS_110_Contagemresultado_agrupador8",SqlDbType.Char,15,0} ,
          new Object[] {"@lV268WP_CaixaDmnDS_110_Contagemresultado_agrupador8",SqlDbType.Char,15,0} ,
          new Object[] {"@AV269WP_CaixaDmnDS_111_Contagemresultado_statusdmn8",SqlDbType.Char,1,0} ,
          new Object[] {"@AV273WP_CaixaDmnDS_115_Contagemresultado_demanda9",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV273WP_CaixaDmnDS_115_Contagemresultado_demanda9",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV273WP_CaixaDmnDS_115_Contagemresultado_demanda9",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV274WP_CaixaDmnDS_116_Contagemresultado_datadmn9",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV275WP_CaixaDmnDS_117_Contagemresultado_datadmn_to9",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV276WP_CaixaDmnDS_118_Contagemresultado_dataprevista9",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV277WP_CaixaDmnDS_119_Contagemresultado_dataprevista_to9",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV278WP_CaixaDmnDS_120_Contagemresultado_contratadacod9",SqlDbType.Int,6,0} ,
          new Object[] {"@AV279WP_CaixaDmnDS_121_Contagemresultado_servico9",SqlDbType.Int,6,0} ,
          new Object[] {"@AV280WP_CaixaDmnDS_122_Contagemresultado_sistemacod9",SqlDbType.Int,6,0} ,
          new Object[] {"@AV281WP_CaixaDmnDS_123_Contagemresultado_responsavel9",SqlDbType.Int,6,0} ,
          new Object[] {"@AV282WP_CaixaDmnDS_124_Contagemresultado_agrupador9",SqlDbType.Char,15,0} ,
          new Object[] {"@lV282WP_CaixaDmnDS_124_Contagemresultado_agrupador9",SqlDbType.Char,15,0} ,
          new Object[] {"@lV282WP_CaixaDmnDS_124_Contagemresultado_agrupador9",SqlDbType.Char,15,0} ,
          new Object[] {"@AV283WP_CaixaDmnDS_125_Contagemresultado_statusdmn9",SqlDbType.Char,1,0} ,
          new Object[] {"@lV284WP_CaixaDmnDS_126_Tfcontagemresultado_demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV285WP_CaixaDmnDS_127_Tfcontagemresultado_demanda_sel",SqlDbType.VarChar,30,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00V92", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00V92,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[16])[0] = rslt.getGXDateTime(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[18])[0] = rslt.getGXDate(10) ;
                ((int[]) buf[19])[0] = rslt.getInt(11) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[137]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[138]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[139]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[140]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[141]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[142]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[143]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[144]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[145]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[146]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[147]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[148]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[149]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[150]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[151]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[152]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[153]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[154]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[155]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[156]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[157]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[158]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[159]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[160]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[161]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[162]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[163]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[164]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[165]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[166]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[167]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[168]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[169]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[170]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[171]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[172]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[173]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[174]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[175]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[176]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[177]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[178]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[179]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[180]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[181]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[182]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[183]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[184]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[185]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[186]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[187]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[188]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[189]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[190]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[191]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[192]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[193]);
                }
                if ( (short)parms[57] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[194]);
                }
                if ( (short)parms[58] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[195]);
                }
                if ( (short)parms[59] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[196]);
                }
                if ( (short)parms[60] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[197]);
                }
                if ( (short)parms[61] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[198]);
                }
                if ( (short)parms[62] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[199]);
                }
                if ( (short)parms[63] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[200]);
                }
                if ( (short)parms[64] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[201]);
                }
                if ( (short)parms[65] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[202]);
                }
                if ( (short)parms[66] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[203]);
                }
                if ( (short)parms[67] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[204]);
                }
                if ( (short)parms[68] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[205]);
                }
                if ( (short)parms[69] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[206]);
                }
                if ( (short)parms[70] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[207]);
                }
                if ( (short)parms[71] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[208]);
                }
                if ( (short)parms[72] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[209]);
                }
                if ( (short)parms[73] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[210]);
                }
                if ( (short)parms[74] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[211]);
                }
                if ( (short)parms[75] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[212]);
                }
                if ( (short)parms[76] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[213]);
                }
                if ( (short)parms[77] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[214]);
                }
                if ( (short)parms[78] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[215]);
                }
                if ( (short)parms[79] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[216]);
                }
                if ( (short)parms[80] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[217]);
                }
                if ( (short)parms[81] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[218]);
                }
                if ( (short)parms[82] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[219]);
                }
                if ( (short)parms[83] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[220]);
                }
                if ( (short)parms[84] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[221]);
                }
                if ( (short)parms[85] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[222]);
                }
                if ( (short)parms[86] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[223]);
                }
                if ( (short)parms[87] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[224]);
                }
                if ( (short)parms[88] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[225]);
                }
                if ( (short)parms[89] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[226]);
                }
                if ( (short)parms[90] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[227]);
                }
                if ( (short)parms[91] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[228]);
                }
                if ( (short)parms[92] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[229]);
                }
                if ( (short)parms[93] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[230]);
                }
                if ( (short)parms[94] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[231]);
                }
                if ( (short)parms[95] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[232]);
                }
                if ( (short)parms[96] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[233]);
                }
                if ( (short)parms[97] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[234]);
                }
                if ( (short)parms[98] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[235]);
                }
                if ( (short)parms[99] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[236]);
                }
                if ( (short)parms[100] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[237]);
                }
                if ( (short)parms[101] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[238]);
                }
                if ( (short)parms[102] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[239]);
                }
                if ( (short)parms[103] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[240]);
                }
                if ( (short)parms[104] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[241]);
                }
                if ( (short)parms[105] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[242]);
                }
                if ( (short)parms[106] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[243]);
                }
                if ( (short)parms[107] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[244]);
                }
                if ( (short)parms[108] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[245]);
                }
                if ( (short)parms[109] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[246]);
                }
                if ( (short)parms[110] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[247]);
                }
                if ( (short)parms[111] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[248]);
                }
                if ( (short)parms[112] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[249]);
                }
                if ( (short)parms[113] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[250]);
                }
                if ( (short)parms[114] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[251]);
                }
                if ( (short)parms[115] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[252]);
                }
                if ( (short)parms[116] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[253]);
                }
                if ( (short)parms[117] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[254]);
                }
                if ( (short)parms[118] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[255]);
                }
                if ( (short)parms[119] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[256]);
                }
                if ( (short)parms[120] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[257]);
                }
                if ( (short)parms[121] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[258]);
                }
                if ( (short)parms[122] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[259]);
                }
                if ( (short)parms[123] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[260]);
                }
                if ( (short)parms[124] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[261]);
                }
                if ( (short)parms[125] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[262]);
                }
                if ( (short)parms[126] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[263]);
                }
                if ( (short)parms[127] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[264]);
                }
                if ( (short)parms[128] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[265]);
                }
                if ( (short)parms[129] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[266]);
                }
                if ( (short)parms[130] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[267]);
                }
                if ( (short)parms[131] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[268]);
                }
                if ( (short)parms[132] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[269]);
                }
                if ( (short)parms[133] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[270]);
                }
                if ( (short)parms[134] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[271]);
                }
                if ( (short)parms[135] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[272]);
                }
                if ( (short)parms[136] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[273]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwp_caixadmnfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwp_caixadmnfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwp_caixadmnfilterdata") )
          {
             return  ;
          }
          getwp_caixadmnfilterdata worker = new getwp_caixadmnfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
