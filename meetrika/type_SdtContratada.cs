/*
               File: type_SdtContratada
        Description: Contratada
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:16:14.36
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "Contratada" )]
   [XmlType(TypeName =  "Contratada" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtContratada : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContratada( )
      {
         /* Constructor for serialization */
         gxTv_SdtContratada_Contratada_areatrabalhodes = "";
         gxTv_SdtContratada_Contratada_areatrbclcpfnl = "";
         gxTv_SdtContratada_Contratada_pessoanom = "";
         gxTv_SdtContratada_Contratada_pessoacnpj = "";
         gxTv_SdtContratada_Pessoa_ie = "";
         gxTv_SdtContratada_Pessoa_endereco = "";
         gxTv_SdtContratada_Pessoa_cep = "";
         gxTv_SdtContratada_Pessoa_telefone = "";
         gxTv_SdtContratada_Pessoa_fax = "";
         gxTv_SdtContratada_Contratada_sigla = "";
         gxTv_SdtContratada_Contratada_tipofabrica = "";
         gxTv_SdtContratada_Contratada_banconome = "";
         gxTv_SdtContratada_Contratada_banconro = "";
         gxTv_SdtContratada_Contratada_agencianome = "";
         gxTv_SdtContratada_Contratada_agencianro = "";
         gxTv_SdtContratada_Contratada_contacorrente = "";
         gxTv_SdtContratada_Contratada_uf = "";
         gxTv_SdtContratada_Contratada_logoarquivo = "";
         gxTv_SdtContratada_Contratada_logonomearq = "";
         gxTv_SdtContratada_Contratada_logotipoarq = "";
         gxTv_SdtContratada_Contratada_logo = "";
         gxTv_SdtContratada_Contratada_logo_gxi = "";
         gxTv_SdtContratada_Mode = "";
         gxTv_SdtContratada_Pessoa_ie_Z = "";
         gxTv_SdtContratada_Pessoa_endereco_Z = "";
         gxTv_SdtContratada_Pessoa_cep_Z = "";
         gxTv_SdtContratada_Pessoa_telefone_Z = "";
         gxTv_SdtContratada_Pessoa_fax_Z = "";
         gxTv_SdtContratada_Contratada_areatrabalhodes_Z = "";
         gxTv_SdtContratada_Contratada_areatrbclcpfnl_Z = "";
         gxTv_SdtContratada_Contratada_pessoanom_Z = "";
         gxTv_SdtContratada_Contratada_pessoacnpj_Z = "";
         gxTv_SdtContratada_Contratada_sigla_Z = "";
         gxTv_SdtContratada_Contratada_tipofabrica_Z = "";
         gxTv_SdtContratada_Contratada_banconome_Z = "";
         gxTv_SdtContratada_Contratada_banconro_Z = "";
         gxTv_SdtContratada_Contratada_agencianome_Z = "";
         gxTv_SdtContratada_Contratada_agencianro_Z = "";
         gxTv_SdtContratada_Contratada_contacorrente_Z = "";
         gxTv_SdtContratada_Contratada_uf_Z = "";
         gxTv_SdtContratada_Contratada_logonomearq_Z = "";
         gxTv_SdtContratada_Contratada_logotipoarq_Z = "";
         gxTv_SdtContratada_Contratada_logo_gxi_Z = "";
      }

      public SdtContratada( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV39Contratada_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV39Contratada_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"Contratada_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "Contratada");
         metadata.Set("BT", "Contratada");
         metadata.Set("PK", "[ \"Contratada_Codigo\" ]");
         metadata.Set("PKAssigned", "[ \"Contratada_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"AreaTrabalho_Codigo\" ],\"FKMap\":[ \"Contratada_AreaTrabalhoCod-AreaTrabalho_Codigo\" ] },{ \"FK\":[ \"Municipio_Codigo\" ],\"FKMap\":[ \"Contratada_MunicipioCod-Municipio_Codigo\" ] },{ \"FK\":[ \"Pessoa_Codigo\" ],\"FKMap\":[ \"Contratada_PessoaCod-Pessoa_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Contratada_logo_gxi" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Pessoa_ie_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Pessoa_endereco_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Pessoa_cep_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Pessoa_telefone_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Pessoa_fax_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_areatrabalhocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_areatrabalhodes_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_areatrbclcpfnl_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_areatrbsrvpdr_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_pessoacod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_pessoanom_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_pessoacnpj_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_sigla_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_tipofabrica_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_banconome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_banconro_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_agencianome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_agencianro_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_contacorrente_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_municipiocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_uf_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_os_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_ss_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_lote_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_logonomearq_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_logotipoarq_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_usaosistema_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_ospreferencial_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_cntpadrao_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_ativo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_logo_gxi_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_areatrabalhodes_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_areatrbclcpfnl_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_areatrbsrvpdr_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_pessoanom_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_pessoacnpj_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Pessoa_ie_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Pessoa_endereco_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Pessoa_cep_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Pessoa_telefone_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Pessoa_fax_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_banconome_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_banconro_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_agencianome_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_agencianro_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_contacorrente_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_municipiocod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_uf_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_os_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_ss_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_lote_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_logoarquivo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_logonomearq_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_logotipoarq_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_usaosistema_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_ospreferencial_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_cntpadrao_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_logo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_logo_gxi_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtContratada deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtContratada)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtContratada obj ;
         obj = this;
         obj.gxTpr_Contratada_codigo = deserialized.gxTpr_Contratada_codigo;
         obj.gxTpr_Contratada_areatrabalhocod = deserialized.gxTpr_Contratada_areatrabalhocod;
         obj.gxTpr_Contratada_areatrabalhodes = deserialized.gxTpr_Contratada_areatrabalhodes;
         obj.gxTpr_Contratada_areatrbclcpfnl = deserialized.gxTpr_Contratada_areatrbclcpfnl;
         obj.gxTpr_Contratada_areatrbsrvpdr = deserialized.gxTpr_Contratada_areatrbsrvpdr;
         obj.gxTpr_Contratada_pessoacod = deserialized.gxTpr_Contratada_pessoacod;
         obj.gxTpr_Contratada_pessoanom = deserialized.gxTpr_Contratada_pessoanom;
         obj.gxTpr_Contratada_pessoacnpj = deserialized.gxTpr_Contratada_pessoacnpj;
         obj.gxTpr_Pessoa_ie = deserialized.gxTpr_Pessoa_ie;
         obj.gxTpr_Pessoa_endereco = deserialized.gxTpr_Pessoa_endereco;
         obj.gxTpr_Pessoa_cep = deserialized.gxTpr_Pessoa_cep;
         obj.gxTpr_Pessoa_telefone = deserialized.gxTpr_Pessoa_telefone;
         obj.gxTpr_Pessoa_fax = deserialized.gxTpr_Pessoa_fax;
         obj.gxTpr_Contratada_sigla = deserialized.gxTpr_Contratada_sigla;
         obj.gxTpr_Contratada_tipofabrica = deserialized.gxTpr_Contratada_tipofabrica;
         obj.gxTpr_Contratada_banconome = deserialized.gxTpr_Contratada_banconome;
         obj.gxTpr_Contratada_banconro = deserialized.gxTpr_Contratada_banconro;
         obj.gxTpr_Contratada_agencianome = deserialized.gxTpr_Contratada_agencianome;
         obj.gxTpr_Contratada_agencianro = deserialized.gxTpr_Contratada_agencianro;
         obj.gxTpr_Contratada_contacorrente = deserialized.gxTpr_Contratada_contacorrente;
         obj.gxTpr_Contratada_municipiocod = deserialized.gxTpr_Contratada_municipiocod;
         obj.gxTpr_Contratada_uf = deserialized.gxTpr_Contratada_uf;
         obj.gxTpr_Contratada_os = deserialized.gxTpr_Contratada_os;
         obj.gxTpr_Contratada_ss = deserialized.gxTpr_Contratada_ss;
         obj.gxTpr_Contratada_lote = deserialized.gxTpr_Contratada_lote;
         obj.gxTpr_Contratada_logoarquivo = deserialized.gxTpr_Contratada_logoarquivo;
         obj.gxTpr_Contratada_logonomearq = deserialized.gxTpr_Contratada_logonomearq;
         obj.gxTpr_Contratada_logotipoarq = deserialized.gxTpr_Contratada_logotipoarq;
         obj.gxTpr_Contratada_usaosistema = deserialized.gxTpr_Contratada_usaosistema;
         obj.gxTpr_Contratada_ospreferencial = deserialized.gxTpr_Contratada_ospreferencial;
         obj.gxTpr_Contratada_cntpadrao = deserialized.gxTpr_Contratada_cntpadrao;
         obj.gxTpr_Contratada_ativo = deserialized.gxTpr_Contratada_ativo;
         obj.gxTpr_Contratada_logo = deserialized.gxTpr_Contratada_logo;
         obj.gxTpr_Contratada_logo_gxi = deserialized.gxTpr_Contratada_logo_gxi;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Pessoa_ie_Z = deserialized.gxTpr_Pessoa_ie_Z;
         obj.gxTpr_Pessoa_endereco_Z = deserialized.gxTpr_Pessoa_endereco_Z;
         obj.gxTpr_Pessoa_cep_Z = deserialized.gxTpr_Pessoa_cep_Z;
         obj.gxTpr_Pessoa_telefone_Z = deserialized.gxTpr_Pessoa_telefone_Z;
         obj.gxTpr_Pessoa_fax_Z = deserialized.gxTpr_Pessoa_fax_Z;
         obj.gxTpr_Contratada_codigo_Z = deserialized.gxTpr_Contratada_codigo_Z;
         obj.gxTpr_Contratada_areatrabalhocod_Z = deserialized.gxTpr_Contratada_areatrabalhocod_Z;
         obj.gxTpr_Contratada_areatrabalhodes_Z = deserialized.gxTpr_Contratada_areatrabalhodes_Z;
         obj.gxTpr_Contratada_areatrbclcpfnl_Z = deserialized.gxTpr_Contratada_areatrbclcpfnl_Z;
         obj.gxTpr_Contratada_areatrbsrvpdr_Z = deserialized.gxTpr_Contratada_areatrbsrvpdr_Z;
         obj.gxTpr_Contratada_pessoacod_Z = deserialized.gxTpr_Contratada_pessoacod_Z;
         obj.gxTpr_Contratada_pessoanom_Z = deserialized.gxTpr_Contratada_pessoanom_Z;
         obj.gxTpr_Contratada_pessoacnpj_Z = deserialized.gxTpr_Contratada_pessoacnpj_Z;
         obj.gxTpr_Contratada_sigla_Z = deserialized.gxTpr_Contratada_sigla_Z;
         obj.gxTpr_Contratada_tipofabrica_Z = deserialized.gxTpr_Contratada_tipofabrica_Z;
         obj.gxTpr_Contratada_banconome_Z = deserialized.gxTpr_Contratada_banconome_Z;
         obj.gxTpr_Contratada_banconro_Z = deserialized.gxTpr_Contratada_banconro_Z;
         obj.gxTpr_Contratada_agencianome_Z = deserialized.gxTpr_Contratada_agencianome_Z;
         obj.gxTpr_Contratada_agencianro_Z = deserialized.gxTpr_Contratada_agencianro_Z;
         obj.gxTpr_Contratada_contacorrente_Z = deserialized.gxTpr_Contratada_contacorrente_Z;
         obj.gxTpr_Contratada_municipiocod_Z = deserialized.gxTpr_Contratada_municipiocod_Z;
         obj.gxTpr_Contratada_uf_Z = deserialized.gxTpr_Contratada_uf_Z;
         obj.gxTpr_Contratada_os_Z = deserialized.gxTpr_Contratada_os_Z;
         obj.gxTpr_Contratada_ss_Z = deserialized.gxTpr_Contratada_ss_Z;
         obj.gxTpr_Contratada_lote_Z = deserialized.gxTpr_Contratada_lote_Z;
         obj.gxTpr_Contratada_logonomearq_Z = deserialized.gxTpr_Contratada_logonomearq_Z;
         obj.gxTpr_Contratada_logotipoarq_Z = deserialized.gxTpr_Contratada_logotipoarq_Z;
         obj.gxTpr_Contratada_usaosistema_Z = deserialized.gxTpr_Contratada_usaosistema_Z;
         obj.gxTpr_Contratada_ospreferencial_Z = deserialized.gxTpr_Contratada_ospreferencial_Z;
         obj.gxTpr_Contratada_cntpadrao_Z = deserialized.gxTpr_Contratada_cntpadrao_Z;
         obj.gxTpr_Contratada_ativo_Z = deserialized.gxTpr_Contratada_ativo_Z;
         obj.gxTpr_Contratada_logo_gxi_Z = deserialized.gxTpr_Contratada_logo_gxi_Z;
         obj.gxTpr_Contratada_areatrabalhodes_N = deserialized.gxTpr_Contratada_areatrabalhodes_N;
         obj.gxTpr_Contratada_areatrbclcpfnl_N = deserialized.gxTpr_Contratada_areatrbclcpfnl_N;
         obj.gxTpr_Contratada_areatrbsrvpdr_N = deserialized.gxTpr_Contratada_areatrbsrvpdr_N;
         obj.gxTpr_Contratada_pessoanom_N = deserialized.gxTpr_Contratada_pessoanom_N;
         obj.gxTpr_Contratada_pessoacnpj_N = deserialized.gxTpr_Contratada_pessoacnpj_N;
         obj.gxTpr_Pessoa_ie_N = deserialized.gxTpr_Pessoa_ie_N;
         obj.gxTpr_Pessoa_endereco_N = deserialized.gxTpr_Pessoa_endereco_N;
         obj.gxTpr_Pessoa_cep_N = deserialized.gxTpr_Pessoa_cep_N;
         obj.gxTpr_Pessoa_telefone_N = deserialized.gxTpr_Pessoa_telefone_N;
         obj.gxTpr_Pessoa_fax_N = deserialized.gxTpr_Pessoa_fax_N;
         obj.gxTpr_Contratada_banconome_N = deserialized.gxTpr_Contratada_banconome_N;
         obj.gxTpr_Contratada_banconro_N = deserialized.gxTpr_Contratada_banconro_N;
         obj.gxTpr_Contratada_agencianome_N = deserialized.gxTpr_Contratada_agencianome_N;
         obj.gxTpr_Contratada_agencianro_N = deserialized.gxTpr_Contratada_agencianro_N;
         obj.gxTpr_Contratada_contacorrente_N = deserialized.gxTpr_Contratada_contacorrente_N;
         obj.gxTpr_Contratada_municipiocod_N = deserialized.gxTpr_Contratada_municipiocod_N;
         obj.gxTpr_Contratada_uf_N = deserialized.gxTpr_Contratada_uf_N;
         obj.gxTpr_Contratada_os_N = deserialized.gxTpr_Contratada_os_N;
         obj.gxTpr_Contratada_ss_N = deserialized.gxTpr_Contratada_ss_N;
         obj.gxTpr_Contratada_lote_N = deserialized.gxTpr_Contratada_lote_N;
         obj.gxTpr_Contratada_logoarquivo_N = deserialized.gxTpr_Contratada_logoarquivo_N;
         obj.gxTpr_Contratada_logonomearq_N = deserialized.gxTpr_Contratada_logonomearq_N;
         obj.gxTpr_Contratada_logotipoarq_N = deserialized.gxTpr_Contratada_logotipoarq_N;
         obj.gxTpr_Contratada_usaosistema_N = deserialized.gxTpr_Contratada_usaosistema_N;
         obj.gxTpr_Contratada_ospreferencial_N = deserialized.gxTpr_Contratada_ospreferencial_N;
         obj.gxTpr_Contratada_cntpadrao_N = deserialized.gxTpr_Contratada_cntpadrao_N;
         obj.gxTpr_Contratada_logo_N = deserialized.gxTpr_Contratada_logo_N;
         obj.gxTpr_Contratada_logo_gxi_N = deserialized.gxTpr_Contratada_logo_gxi_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_Codigo") )
               {
                  gxTv_SdtContratada_Contratada_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_AreaTrabalhoCod") )
               {
                  gxTv_SdtContratada_Contratada_areatrabalhocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_AreaTrabalhoDes") )
               {
                  gxTv_SdtContratada_Contratada_areatrabalhodes = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_AreaTrbClcPFnl") )
               {
                  gxTv_SdtContratada_Contratada_areatrbclcpfnl = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_AreaTrbSrvPdr") )
               {
                  gxTv_SdtContratada_Contratada_areatrbsrvpdr = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_PessoaCod") )
               {
                  gxTv_SdtContratada_Contratada_pessoacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_PessoaNom") )
               {
                  gxTv_SdtContratada_Contratada_pessoanom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_PessoaCNPJ") )
               {
                  gxTv_SdtContratada_Contratada_pessoacnpj = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_IE") )
               {
                  gxTv_SdtContratada_Pessoa_ie = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_Endereco") )
               {
                  gxTv_SdtContratada_Pessoa_endereco = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_CEP") )
               {
                  gxTv_SdtContratada_Pessoa_cep = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_Telefone") )
               {
                  gxTv_SdtContratada_Pessoa_telefone = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_Fax") )
               {
                  gxTv_SdtContratada_Pessoa_fax = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_Sigla") )
               {
                  gxTv_SdtContratada_Contratada_sigla = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_TipoFabrica") )
               {
                  gxTv_SdtContratada_Contratada_tipofabrica = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_BancoNome") )
               {
                  gxTv_SdtContratada_Contratada_banconome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_BancoNro") )
               {
                  gxTv_SdtContratada_Contratada_banconro = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_AgenciaNome") )
               {
                  gxTv_SdtContratada_Contratada_agencianome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_AgenciaNro") )
               {
                  gxTv_SdtContratada_Contratada_agencianro = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_ContaCorrente") )
               {
                  gxTv_SdtContratada_Contratada_contacorrente = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_MunicipioCod") )
               {
                  gxTv_SdtContratada_Contratada_municipiocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_UF") )
               {
                  gxTv_SdtContratada_Contratada_uf = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_OS") )
               {
                  gxTv_SdtContratada_Contratada_os = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_SS") )
               {
                  gxTv_SdtContratada_Contratada_ss = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_Lote") )
               {
                  gxTv_SdtContratada_Contratada_lote = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_LogoArquivo") )
               {
                  gxTv_SdtContratada_Contratada_logoarquivo=context.FileFromBase64( oReader.Value) ;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_LogoNomeArq") )
               {
                  gxTv_SdtContratada_Contratada_logonomearq = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_LogoTipoArq") )
               {
                  gxTv_SdtContratada_Contratada_logotipoarq = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_UsaOSistema") )
               {
                  gxTv_SdtContratada_Contratada_usaosistema = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_OSPreferencial") )
               {
                  gxTv_SdtContratada_Contratada_ospreferencial = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_CntPadrao") )
               {
                  gxTv_SdtContratada_Contratada_cntpadrao = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_Ativo") )
               {
                  gxTv_SdtContratada_Contratada_ativo = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_Logo") )
               {
                  gxTv_SdtContratada_Contratada_logo = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_Logo_GXI") )
               {
                  gxTv_SdtContratada_Contratada_logo_gxi = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtContratada_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtContratada_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_IE_Z") )
               {
                  gxTv_SdtContratada_Pessoa_ie_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_Endereco_Z") )
               {
                  gxTv_SdtContratada_Pessoa_endereco_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_CEP_Z") )
               {
                  gxTv_SdtContratada_Pessoa_cep_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_Telefone_Z") )
               {
                  gxTv_SdtContratada_Pessoa_telefone_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_Fax_Z") )
               {
                  gxTv_SdtContratada_Pessoa_fax_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_Codigo_Z") )
               {
                  gxTv_SdtContratada_Contratada_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_AreaTrabalhoCod_Z") )
               {
                  gxTv_SdtContratada_Contratada_areatrabalhocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_AreaTrabalhoDes_Z") )
               {
                  gxTv_SdtContratada_Contratada_areatrabalhodes_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_AreaTrbClcPFnl_Z") )
               {
                  gxTv_SdtContratada_Contratada_areatrbclcpfnl_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_AreaTrbSrvPdr_Z") )
               {
                  gxTv_SdtContratada_Contratada_areatrbsrvpdr_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_PessoaCod_Z") )
               {
                  gxTv_SdtContratada_Contratada_pessoacod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_PessoaNom_Z") )
               {
                  gxTv_SdtContratada_Contratada_pessoanom_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_PessoaCNPJ_Z") )
               {
                  gxTv_SdtContratada_Contratada_pessoacnpj_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_Sigla_Z") )
               {
                  gxTv_SdtContratada_Contratada_sigla_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_TipoFabrica_Z") )
               {
                  gxTv_SdtContratada_Contratada_tipofabrica_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_BancoNome_Z") )
               {
                  gxTv_SdtContratada_Contratada_banconome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_BancoNro_Z") )
               {
                  gxTv_SdtContratada_Contratada_banconro_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_AgenciaNome_Z") )
               {
                  gxTv_SdtContratada_Contratada_agencianome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_AgenciaNro_Z") )
               {
                  gxTv_SdtContratada_Contratada_agencianro_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_ContaCorrente_Z") )
               {
                  gxTv_SdtContratada_Contratada_contacorrente_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_MunicipioCod_Z") )
               {
                  gxTv_SdtContratada_Contratada_municipiocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_UF_Z") )
               {
                  gxTv_SdtContratada_Contratada_uf_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_OS_Z") )
               {
                  gxTv_SdtContratada_Contratada_os_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_SS_Z") )
               {
                  gxTv_SdtContratada_Contratada_ss_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_Lote_Z") )
               {
                  gxTv_SdtContratada_Contratada_lote_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_LogoNomeArq_Z") )
               {
                  gxTv_SdtContratada_Contratada_logonomearq_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_LogoTipoArq_Z") )
               {
                  gxTv_SdtContratada_Contratada_logotipoarq_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_UsaOSistema_Z") )
               {
                  gxTv_SdtContratada_Contratada_usaosistema_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_OSPreferencial_Z") )
               {
                  gxTv_SdtContratada_Contratada_ospreferencial_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_CntPadrao_Z") )
               {
                  gxTv_SdtContratada_Contratada_cntpadrao_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_Ativo_Z") )
               {
                  gxTv_SdtContratada_Contratada_ativo_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_Logo_GXI_Z") )
               {
                  gxTv_SdtContratada_Contratada_logo_gxi_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_AreaTrabalhoDes_N") )
               {
                  gxTv_SdtContratada_Contratada_areatrabalhodes_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_AreaTrbClcPFnl_N") )
               {
                  gxTv_SdtContratada_Contratada_areatrbclcpfnl_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_AreaTrbSrvPdr_N") )
               {
                  gxTv_SdtContratada_Contratada_areatrbsrvpdr_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_PessoaNom_N") )
               {
                  gxTv_SdtContratada_Contratada_pessoanom_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_PessoaCNPJ_N") )
               {
                  gxTv_SdtContratada_Contratada_pessoacnpj_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_IE_N") )
               {
                  gxTv_SdtContratada_Pessoa_ie_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_Endereco_N") )
               {
                  gxTv_SdtContratada_Pessoa_endereco_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_CEP_N") )
               {
                  gxTv_SdtContratada_Pessoa_cep_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_Telefone_N") )
               {
                  gxTv_SdtContratada_Pessoa_telefone_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_Fax_N") )
               {
                  gxTv_SdtContratada_Pessoa_fax_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_BancoNome_N") )
               {
                  gxTv_SdtContratada_Contratada_banconome_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_BancoNro_N") )
               {
                  gxTv_SdtContratada_Contratada_banconro_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_AgenciaNome_N") )
               {
                  gxTv_SdtContratada_Contratada_agencianome_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_AgenciaNro_N") )
               {
                  gxTv_SdtContratada_Contratada_agencianro_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_ContaCorrente_N") )
               {
                  gxTv_SdtContratada_Contratada_contacorrente_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_MunicipioCod_N") )
               {
                  gxTv_SdtContratada_Contratada_municipiocod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_UF_N") )
               {
                  gxTv_SdtContratada_Contratada_uf_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_OS_N") )
               {
                  gxTv_SdtContratada_Contratada_os_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_SS_N") )
               {
                  gxTv_SdtContratada_Contratada_ss_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_Lote_N") )
               {
                  gxTv_SdtContratada_Contratada_lote_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_LogoArquivo_N") )
               {
                  gxTv_SdtContratada_Contratada_logoarquivo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_LogoNomeArq_N") )
               {
                  gxTv_SdtContratada_Contratada_logonomearq_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_LogoTipoArq_N") )
               {
                  gxTv_SdtContratada_Contratada_logotipoarq_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_UsaOSistema_N") )
               {
                  gxTv_SdtContratada_Contratada_usaosistema_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_OSPreferencial_N") )
               {
                  gxTv_SdtContratada_Contratada_ospreferencial_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_CntPadrao_N") )
               {
                  gxTv_SdtContratada_Contratada_cntpadrao_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_Logo_N") )
               {
                  gxTv_SdtContratada_Contratada_logo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_Logo_GXI_N") )
               {
                  gxTv_SdtContratada_Contratada_logo_gxi_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "Contratada";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Contratada_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_AreaTrabalhoCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_areatrabalhocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_AreaTrabalhoDes", StringUtil.RTrim( gxTv_SdtContratada_Contratada_areatrabalhodes));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_AreaTrbClcPFnl", StringUtil.RTrim( gxTv_SdtContratada_Contratada_areatrbclcpfnl));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_AreaTrbSrvPdr", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_areatrbsrvpdr), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_PessoaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_pessoacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_PessoaNom", StringUtil.RTrim( gxTv_SdtContratada_Contratada_pessoanom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_PessoaCNPJ", StringUtil.RTrim( gxTv_SdtContratada_Contratada_pessoacnpj));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Pessoa_IE", StringUtil.RTrim( gxTv_SdtContratada_Pessoa_ie));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Pessoa_Endereco", StringUtil.RTrim( gxTv_SdtContratada_Pessoa_endereco));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Pessoa_CEP", StringUtil.RTrim( gxTv_SdtContratada_Pessoa_cep));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Pessoa_Telefone", StringUtil.RTrim( gxTv_SdtContratada_Pessoa_telefone));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Pessoa_Fax", StringUtil.RTrim( gxTv_SdtContratada_Pessoa_fax));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_Sigla", StringUtil.RTrim( gxTv_SdtContratada_Contratada_sigla));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_TipoFabrica", StringUtil.RTrim( gxTv_SdtContratada_Contratada_tipofabrica));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_BancoNome", StringUtil.RTrim( gxTv_SdtContratada_Contratada_banconome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_BancoNro", StringUtil.RTrim( gxTv_SdtContratada_Contratada_banconro));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_AgenciaNome", StringUtil.RTrim( gxTv_SdtContratada_Contratada_agencianome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_AgenciaNro", StringUtil.RTrim( gxTv_SdtContratada_Contratada_agencianro));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_ContaCorrente", StringUtil.RTrim( gxTv_SdtContratada_Contratada_contacorrente));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_MunicipioCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_municipiocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_UF", StringUtil.RTrim( gxTv_SdtContratada_Contratada_uf));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_OS", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_os), 8, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_SS", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_ss), 8, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_Lote", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_lote), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_LogoArquivo", context.FileToBase64( gxTv_SdtContratada_Contratada_logoarquivo));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_LogoNomeArq", StringUtil.RTrim( gxTv_SdtContratada_Contratada_logonomearq));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_LogoTipoArq", StringUtil.RTrim( gxTv_SdtContratada_Contratada_logotipoarq));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_UsaOSistema", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratada_Contratada_usaosistema)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_OSPreferencial", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratada_Contratada_ospreferencial)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_CntPadrao", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_cntpadrao), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_Ativo", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratada_Contratada_ativo)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_Logo", StringUtil.RTrim( gxTv_SdtContratada_Contratada_logo));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Contratada_Logo_GXI", StringUtil.RTrim( gxTv_SdtContratada_Contratada_logo_gxi));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtContratada_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Pessoa_IE_Z", StringUtil.RTrim( gxTv_SdtContratada_Pessoa_ie_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Pessoa_Endereco_Z", StringUtil.RTrim( gxTv_SdtContratada_Pessoa_endereco_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Pessoa_CEP_Z", StringUtil.RTrim( gxTv_SdtContratada_Pessoa_cep_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Pessoa_Telefone_Z", StringUtil.RTrim( gxTv_SdtContratada_Pessoa_telefone_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Pessoa_Fax_Z", StringUtil.RTrim( gxTv_SdtContratada_Pessoa_fax_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_AreaTrabalhoCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_areatrabalhocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_AreaTrabalhoDes_Z", StringUtil.RTrim( gxTv_SdtContratada_Contratada_areatrabalhodes_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_AreaTrbClcPFnl_Z", StringUtil.RTrim( gxTv_SdtContratada_Contratada_areatrbclcpfnl_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_AreaTrbSrvPdr_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_areatrbsrvpdr_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_PessoaCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_pessoacod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_PessoaNom_Z", StringUtil.RTrim( gxTv_SdtContratada_Contratada_pessoanom_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_PessoaCNPJ_Z", StringUtil.RTrim( gxTv_SdtContratada_Contratada_pessoacnpj_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_Sigla_Z", StringUtil.RTrim( gxTv_SdtContratada_Contratada_sigla_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_TipoFabrica_Z", StringUtil.RTrim( gxTv_SdtContratada_Contratada_tipofabrica_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_BancoNome_Z", StringUtil.RTrim( gxTv_SdtContratada_Contratada_banconome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_BancoNro_Z", StringUtil.RTrim( gxTv_SdtContratada_Contratada_banconro_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_AgenciaNome_Z", StringUtil.RTrim( gxTv_SdtContratada_Contratada_agencianome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_AgenciaNro_Z", StringUtil.RTrim( gxTv_SdtContratada_Contratada_agencianro_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_ContaCorrente_Z", StringUtil.RTrim( gxTv_SdtContratada_Contratada_contacorrente_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_MunicipioCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_municipiocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_UF_Z", StringUtil.RTrim( gxTv_SdtContratada_Contratada_uf_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_OS_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_os_Z), 8, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_SS_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_ss_Z), 8, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_Lote_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_lote_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_LogoNomeArq_Z", StringUtil.RTrim( gxTv_SdtContratada_Contratada_logonomearq_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_LogoTipoArq_Z", StringUtil.RTrim( gxTv_SdtContratada_Contratada_logotipoarq_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_UsaOSistema_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratada_Contratada_usaosistema_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_OSPreferencial_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratada_Contratada_ospreferencial_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_CntPadrao_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_cntpadrao_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_Ativo_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratada_Contratada_ativo_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_Logo_GXI_Z", StringUtil.RTrim( gxTv_SdtContratada_Contratada_logo_gxi_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_AreaTrabalhoDes_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_areatrabalhodes_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_AreaTrbClcPFnl_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_areatrbclcpfnl_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_AreaTrbSrvPdr_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_areatrbsrvpdr_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_PessoaNom_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_pessoanom_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_PessoaCNPJ_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_pessoacnpj_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Pessoa_IE_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Pessoa_ie_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Pessoa_Endereco_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Pessoa_endereco_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Pessoa_CEP_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Pessoa_cep_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Pessoa_Telefone_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Pessoa_telefone_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Pessoa_Fax_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Pessoa_fax_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_BancoNome_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_banconome_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_BancoNro_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_banconro_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_AgenciaNome_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_agencianome_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_AgenciaNro_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_agencianro_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_ContaCorrente_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_contacorrente_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_MunicipioCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_municipiocod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_UF_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_uf_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_OS_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_os_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_SS_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_ss_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_Lote_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_lote_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_LogoArquivo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_logoarquivo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_LogoNomeArq_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_logonomearq_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_LogoTipoArq_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_logotipoarq_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_UsaOSistema_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_usaosistema_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_OSPreferencial_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_ospreferencial_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_CntPadrao_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_cntpadrao_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_Logo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_logo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_Logo_GXI_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratada_Contratada_logo_gxi_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Contratada_Codigo", gxTv_SdtContratada_Contratada_codigo, false);
         AddObjectProperty("Contratada_AreaTrabalhoCod", gxTv_SdtContratada_Contratada_areatrabalhocod, false);
         AddObjectProperty("Contratada_AreaTrabalhoDes", gxTv_SdtContratada_Contratada_areatrabalhodes, false);
         AddObjectProperty("Contratada_AreaTrbClcPFnl", gxTv_SdtContratada_Contratada_areatrbclcpfnl, false);
         AddObjectProperty("Contratada_AreaTrbSrvPdr", gxTv_SdtContratada_Contratada_areatrbsrvpdr, false);
         AddObjectProperty("Contratada_PessoaCod", gxTv_SdtContratada_Contratada_pessoacod, false);
         AddObjectProperty("Contratada_PessoaNom", gxTv_SdtContratada_Contratada_pessoanom, false);
         AddObjectProperty("Contratada_PessoaCNPJ", gxTv_SdtContratada_Contratada_pessoacnpj, false);
         AddObjectProperty("Pessoa_IE", gxTv_SdtContratada_Pessoa_ie, false);
         AddObjectProperty("Pessoa_Endereco", gxTv_SdtContratada_Pessoa_endereco, false);
         AddObjectProperty("Pessoa_CEP", gxTv_SdtContratada_Pessoa_cep, false);
         AddObjectProperty("Pessoa_Telefone", gxTv_SdtContratada_Pessoa_telefone, false);
         AddObjectProperty("Pessoa_Fax", gxTv_SdtContratada_Pessoa_fax, false);
         AddObjectProperty("Contratada_Sigla", gxTv_SdtContratada_Contratada_sigla, false);
         AddObjectProperty("Contratada_TipoFabrica", gxTv_SdtContratada_Contratada_tipofabrica, false);
         AddObjectProperty("Contratada_BancoNome", gxTv_SdtContratada_Contratada_banconome, false);
         AddObjectProperty("Contratada_BancoNro", gxTv_SdtContratada_Contratada_banconro, false);
         AddObjectProperty("Contratada_AgenciaNome", gxTv_SdtContratada_Contratada_agencianome, false);
         AddObjectProperty("Contratada_AgenciaNro", gxTv_SdtContratada_Contratada_agencianro, false);
         AddObjectProperty("Contratada_ContaCorrente", gxTv_SdtContratada_Contratada_contacorrente, false);
         AddObjectProperty("Contratada_MunicipioCod", gxTv_SdtContratada_Contratada_municipiocod, false);
         AddObjectProperty("Contratada_UF", gxTv_SdtContratada_Contratada_uf, false);
         AddObjectProperty("Contratada_OS", gxTv_SdtContratada_Contratada_os, false);
         AddObjectProperty("Contratada_SS", gxTv_SdtContratada_Contratada_ss, false);
         AddObjectProperty("Contratada_Lote", gxTv_SdtContratada_Contratada_lote, false);
         AddObjectProperty("Contratada_LogoArquivo", gxTv_SdtContratada_Contratada_logoarquivo, false);
         AddObjectProperty("Contratada_LogoNomeArq", gxTv_SdtContratada_Contratada_logonomearq, false);
         AddObjectProperty("Contratada_LogoTipoArq", gxTv_SdtContratada_Contratada_logotipoarq, false);
         AddObjectProperty("Contratada_UsaOSistema", gxTv_SdtContratada_Contratada_usaosistema, false);
         AddObjectProperty("Contratada_OSPreferencial", gxTv_SdtContratada_Contratada_ospreferencial, false);
         AddObjectProperty("Contratada_CntPadrao", gxTv_SdtContratada_Contratada_cntpadrao, false);
         AddObjectProperty("Contratada_Ativo", gxTv_SdtContratada_Contratada_ativo, false);
         AddObjectProperty("Contratada_Logo", gxTv_SdtContratada_Contratada_logo, false);
         if ( includeState )
         {
            AddObjectProperty("Contratada_Logo_GXI", gxTv_SdtContratada_Contratada_logo_gxi, false);
            AddObjectProperty("Mode", gxTv_SdtContratada_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtContratada_Initialized, false);
            AddObjectProperty("Pessoa_IE_Z", gxTv_SdtContratada_Pessoa_ie_Z, false);
            AddObjectProperty("Pessoa_Endereco_Z", gxTv_SdtContratada_Pessoa_endereco_Z, false);
            AddObjectProperty("Pessoa_CEP_Z", gxTv_SdtContratada_Pessoa_cep_Z, false);
            AddObjectProperty("Pessoa_Telefone_Z", gxTv_SdtContratada_Pessoa_telefone_Z, false);
            AddObjectProperty("Pessoa_Fax_Z", gxTv_SdtContratada_Pessoa_fax_Z, false);
            AddObjectProperty("Contratada_Codigo_Z", gxTv_SdtContratada_Contratada_codigo_Z, false);
            AddObjectProperty("Contratada_AreaTrabalhoCod_Z", gxTv_SdtContratada_Contratada_areatrabalhocod_Z, false);
            AddObjectProperty("Contratada_AreaTrabalhoDes_Z", gxTv_SdtContratada_Contratada_areatrabalhodes_Z, false);
            AddObjectProperty("Contratada_AreaTrbClcPFnl_Z", gxTv_SdtContratada_Contratada_areatrbclcpfnl_Z, false);
            AddObjectProperty("Contratada_AreaTrbSrvPdr_Z", gxTv_SdtContratada_Contratada_areatrbsrvpdr_Z, false);
            AddObjectProperty("Contratada_PessoaCod_Z", gxTv_SdtContratada_Contratada_pessoacod_Z, false);
            AddObjectProperty("Contratada_PessoaNom_Z", gxTv_SdtContratada_Contratada_pessoanom_Z, false);
            AddObjectProperty("Contratada_PessoaCNPJ_Z", gxTv_SdtContratada_Contratada_pessoacnpj_Z, false);
            AddObjectProperty("Contratada_Sigla_Z", gxTv_SdtContratada_Contratada_sigla_Z, false);
            AddObjectProperty("Contratada_TipoFabrica_Z", gxTv_SdtContratada_Contratada_tipofabrica_Z, false);
            AddObjectProperty("Contratada_BancoNome_Z", gxTv_SdtContratada_Contratada_banconome_Z, false);
            AddObjectProperty("Contratada_BancoNro_Z", gxTv_SdtContratada_Contratada_banconro_Z, false);
            AddObjectProperty("Contratada_AgenciaNome_Z", gxTv_SdtContratada_Contratada_agencianome_Z, false);
            AddObjectProperty("Contratada_AgenciaNro_Z", gxTv_SdtContratada_Contratada_agencianro_Z, false);
            AddObjectProperty("Contratada_ContaCorrente_Z", gxTv_SdtContratada_Contratada_contacorrente_Z, false);
            AddObjectProperty("Contratada_MunicipioCod_Z", gxTv_SdtContratada_Contratada_municipiocod_Z, false);
            AddObjectProperty("Contratada_UF_Z", gxTv_SdtContratada_Contratada_uf_Z, false);
            AddObjectProperty("Contratada_OS_Z", gxTv_SdtContratada_Contratada_os_Z, false);
            AddObjectProperty("Contratada_SS_Z", gxTv_SdtContratada_Contratada_ss_Z, false);
            AddObjectProperty("Contratada_Lote_Z", gxTv_SdtContratada_Contratada_lote_Z, false);
            AddObjectProperty("Contratada_LogoNomeArq_Z", gxTv_SdtContratada_Contratada_logonomearq_Z, false);
            AddObjectProperty("Contratada_LogoTipoArq_Z", gxTv_SdtContratada_Contratada_logotipoarq_Z, false);
            AddObjectProperty("Contratada_UsaOSistema_Z", gxTv_SdtContratada_Contratada_usaosistema_Z, false);
            AddObjectProperty("Contratada_OSPreferencial_Z", gxTv_SdtContratada_Contratada_ospreferencial_Z, false);
            AddObjectProperty("Contratada_CntPadrao_Z", gxTv_SdtContratada_Contratada_cntpadrao_Z, false);
            AddObjectProperty("Contratada_Ativo_Z", gxTv_SdtContratada_Contratada_ativo_Z, false);
            AddObjectProperty("Contratada_Logo_GXI_Z", gxTv_SdtContratada_Contratada_logo_gxi_Z, false);
            AddObjectProperty("Contratada_AreaTrabalhoDes_N", gxTv_SdtContratada_Contratada_areatrabalhodes_N, false);
            AddObjectProperty("Contratada_AreaTrbClcPFnl_N", gxTv_SdtContratada_Contratada_areatrbclcpfnl_N, false);
            AddObjectProperty("Contratada_AreaTrbSrvPdr_N", gxTv_SdtContratada_Contratada_areatrbsrvpdr_N, false);
            AddObjectProperty("Contratada_PessoaNom_N", gxTv_SdtContratada_Contratada_pessoanom_N, false);
            AddObjectProperty("Contratada_PessoaCNPJ_N", gxTv_SdtContratada_Contratada_pessoacnpj_N, false);
            AddObjectProperty("Pessoa_IE_N", gxTv_SdtContratada_Pessoa_ie_N, false);
            AddObjectProperty("Pessoa_Endereco_N", gxTv_SdtContratada_Pessoa_endereco_N, false);
            AddObjectProperty("Pessoa_CEP_N", gxTv_SdtContratada_Pessoa_cep_N, false);
            AddObjectProperty("Pessoa_Telefone_N", gxTv_SdtContratada_Pessoa_telefone_N, false);
            AddObjectProperty("Pessoa_Fax_N", gxTv_SdtContratada_Pessoa_fax_N, false);
            AddObjectProperty("Contratada_BancoNome_N", gxTv_SdtContratada_Contratada_banconome_N, false);
            AddObjectProperty("Contratada_BancoNro_N", gxTv_SdtContratada_Contratada_banconro_N, false);
            AddObjectProperty("Contratada_AgenciaNome_N", gxTv_SdtContratada_Contratada_agencianome_N, false);
            AddObjectProperty("Contratada_AgenciaNro_N", gxTv_SdtContratada_Contratada_agencianro_N, false);
            AddObjectProperty("Contratada_ContaCorrente_N", gxTv_SdtContratada_Contratada_contacorrente_N, false);
            AddObjectProperty("Contratada_MunicipioCod_N", gxTv_SdtContratada_Contratada_municipiocod_N, false);
            AddObjectProperty("Contratada_UF_N", gxTv_SdtContratada_Contratada_uf_N, false);
            AddObjectProperty("Contratada_OS_N", gxTv_SdtContratada_Contratada_os_N, false);
            AddObjectProperty("Contratada_SS_N", gxTv_SdtContratada_Contratada_ss_N, false);
            AddObjectProperty("Contratada_Lote_N", gxTv_SdtContratada_Contratada_lote_N, false);
            AddObjectProperty("Contratada_LogoArquivo_N", gxTv_SdtContratada_Contratada_logoarquivo_N, false);
            AddObjectProperty("Contratada_LogoNomeArq_N", gxTv_SdtContratada_Contratada_logonomearq_N, false);
            AddObjectProperty("Contratada_LogoTipoArq_N", gxTv_SdtContratada_Contratada_logotipoarq_N, false);
            AddObjectProperty("Contratada_UsaOSistema_N", gxTv_SdtContratada_Contratada_usaosistema_N, false);
            AddObjectProperty("Contratada_OSPreferencial_N", gxTv_SdtContratada_Contratada_ospreferencial_N, false);
            AddObjectProperty("Contratada_CntPadrao_N", gxTv_SdtContratada_Contratada_cntpadrao_N, false);
            AddObjectProperty("Contratada_Logo_N", gxTv_SdtContratada_Contratada_logo_N, false);
            AddObjectProperty("Contratada_Logo_GXI_N", gxTv_SdtContratada_Contratada_logo_gxi_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Contratada_Codigo" )]
      [  XmlElement( ElementName = "Contratada_Codigo"   )]
      public int gxTpr_Contratada_codigo
      {
         get {
            return gxTv_SdtContratada_Contratada_codigo ;
         }

         set {
            if ( gxTv_SdtContratada_Contratada_codigo != value )
            {
               gxTv_SdtContratada_Mode = "INS";
               this.gxTv_SdtContratada_Pessoa_ie_Z_SetNull( );
               this.gxTv_SdtContratada_Pessoa_endereco_Z_SetNull( );
               this.gxTv_SdtContratada_Pessoa_cep_Z_SetNull( );
               this.gxTv_SdtContratada_Pessoa_telefone_Z_SetNull( );
               this.gxTv_SdtContratada_Pessoa_fax_Z_SetNull( );
               this.gxTv_SdtContratada_Contratada_codigo_Z_SetNull( );
               this.gxTv_SdtContratada_Contratada_areatrabalhocod_Z_SetNull( );
               this.gxTv_SdtContratada_Contratada_areatrabalhodes_Z_SetNull( );
               this.gxTv_SdtContratada_Contratada_areatrbclcpfnl_Z_SetNull( );
               this.gxTv_SdtContratada_Contratada_areatrbsrvpdr_Z_SetNull( );
               this.gxTv_SdtContratada_Contratada_pessoacod_Z_SetNull( );
               this.gxTv_SdtContratada_Contratada_pessoanom_Z_SetNull( );
               this.gxTv_SdtContratada_Contratada_pessoacnpj_Z_SetNull( );
               this.gxTv_SdtContratada_Pessoa_ie_Z_SetNull( );
               this.gxTv_SdtContratada_Pessoa_endereco_Z_SetNull( );
               this.gxTv_SdtContratada_Pessoa_cep_Z_SetNull( );
               this.gxTv_SdtContratada_Pessoa_telefone_Z_SetNull( );
               this.gxTv_SdtContratada_Pessoa_fax_Z_SetNull( );
               this.gxTv_SdtContratada_Contratada_sigla_Z_SetNull( );
               this.gxTv_SdtContratada_Contratada_tipofabrica_Z_SetNull( );
               this.gxTv_SdtContratada_Contratada_banconome_Z_SetNull( );
               this.gxTv_SdtContratada_Contratada_banconro_Z_SetNull( );
               this.gxTv_SdtContratada_Contratada_agencianome_Z_SetNull( );
               this.gxTv_SdtContratada_Contratada_agencianro_Z_SetNull( );
               this.gxTv_SdtContratada_Contratada_contacorrente_Z_SetNull( );
               this.gxTv_SdtContratada_Contratada_municipiocod_Z_SetNull( );
               this.gxTv_SdtContratada_Contratada_uf_Z_SetNull( );
               this.gxTv_SdtContratada_Contratada_os_Z_SetNull( );
               this.gxTv_SdtContratada_Contratada_ss_Z_SetNull( );
               this.gxTv_SdtContratada_Contratada_lote_Z_SetNull( );
               this.gxTv_SdtContratada_Contratada_logonomearq_Z_SetNull( );
               this.gxTv_SdtContratada_Contratada_logotipoarq_Z_SetNull( );
               this.gxTv_SdtContratada_Contratada_usaosistema_Z_SetNull( );
               this.gxTv_SdtContratada_Contratada_ospreferencial_Z_SetNull( );
               this.gxTv_SdtContratada_Contratada_cntpadrao_Z_SetNull( );
               this.gxTv_SdtContratada_Contratada_ativo_Z_SetNull( );
               this.gxTv_SdtContratada_Contratada_logo_gxi_Z_SetNull( );
            }
            gxTv_SdtContratada_Contratada_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Contratada_AreaTrabalhoCod" )]
      [  XmlElement( ElementName = "Contratada_AreaTrabalhoCod"   )]
      public int gxTpr_Contratada_areatrabalhocod
      {
         get {
            return gxTv_SdtContratada_Contratada_areatrabalhocod ;
         }

         set {
            gxTv_SdtContratada_Contratada_areatrabalhocod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Contratada_AreaTrabalhoDes" )]
      [  XmlElement( ElementName = "Contratada_AreaTrabalhoDes"   )]
      public String gxTpr_Contratada_areatrabalhodes
      {
         get {
            return gxTv_SdtContratada_Contratada_areatrabalhodes ;
         }

         set {
            gxTv_SdtContratada_Contratada_areatrabalhodes_N = 0;
            gxTv_SdtContratada_Contratada_areatrabalhodes = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_areatrabalhodes_SetNull( )
      {
         gxTv_SdtContratada_Contratada_areatrabalhodes_N = 1;
         gxTv_SdtContratada_Contratada_areatrabalhodes = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_areatrabalhodes_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_AreaTrbClcPFnl" )]
      [  XmlElement( ElementName = "Contratada_AreaTrbClcPFnl"   )]
      public String gxTpr_Contratada_areatrbclcpfnl
      {
         get {
            return gxTv_SdtContratada_Contratada_areatrbclcpfnl ;
         }

         set {
            gxTv_SdtContratada_Contratada_areatrbclcpfnl_N = 0;
            gxTv_SdtContratada_Contratada_areatrbclcpfnl = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_areatrbclcpfnl_SetNull( )
      {
         gxTv_SdtContratada_Contratada_areatrbclcpfnl_N = 1;
         gxTv_SdtContratada_Contratada_areatrbclcpfnl = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_areatrbclcpfnl_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_AreaTrbSrvPdr" )]
      [  XmlElement( ElementName = "Contratada_AreaTrbSrvPdr"   )]
      public int gxTpr_Contratada_areatrbsrvpdr
      {
         get {
            return gxTv_SdtContratada_Contratada_areatrbsrvpdr ;
         }

         set {
            gxTv_SdtContratada_Contratada_areatrbsrvpdr_N = 0;
            gxTv_SdtContratada_Contratada_areatrbsrvpdr = (int)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_areatrbsrvpdr_SetNull( )
      {
         gxTv_SdtContratada_Contratada_areatrbsrvpdr_N = 1;
         gxTv_SdtContratada_Contratada_areatrbsrvpdr = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_areatrbsrvpdr_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_PessoaCod" )]
      [  XmlElement( ElementName = "Contratada_PessoaCod"   )]
      public int gxTpr_Contratada_pessoacod
      {
         get {
            return gxTv_SdtContratada_Contratada_pessoacod ;
         }

         set {
            gxTv_SdtContratada_Contratada_pessoacod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Contratada_PessoaNom" )]
      [  XmlElement( ElementName = "Contratada_PessoaNom"   )]
      public String gxTpr_Contratada_pessoanom
      {
         get {
            return gxTv_SdtContratada_Contratada_pessoanom ;
         }

         set {
            gxTv_SdtContratada_Contratada_pessoanom_N = 0;
            gxTv_SdtContratada_Contratada_pessoanom = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_pessoanom_SetNull( )
      {
         gxTv_SdtContratada_Contratada_pessoanom_N = 1;
         gxTv_SdtContratada_Contratada_pessoanom = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_pessoanom_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_PessoaCNPJ" )]
      [  XmlElement( ElementName = "Contratada_PessoaCNPJ"   )]
      public String gxTpr_Contratada_pessoacnpj
      {
         get {
            return gxTv_SdtContratada_Contratada_pessoacnpj ;
         }

         set {
            gxTv_SdtContratada_Contratada_pessoacnpj_N = 0;
            gxTv_SdtContratada_Contratada_pessoacnpj = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_pessoacnpj_SetNull( )
      {
         gxTv_SdtContratada_Contratada_pessoacnpj_N = 1;
         gxTv_SdtContratada_Contratada_pessoacnpj = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_pessoacnpj_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_IE" )]
      [  XmlElement( ElementName = "Pessoa_IE"   )]
      public String gxTpr_Pessoa_ie
      {
         get {
            return gxTv_SdtContratada_Pessoa_ie ;
         }

         set {
            gxTv_SdtContratada_Pessoa_ie_N = 0;
            gxTv_SdtContratada_Pessoa_ie = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Pessoa_ie_SetNull( )
      {
         gxTv_SdtContratada_Pessoa_ie_N = 1;
         gxTv_SdtContratada_Pessoa_ie = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Pessoa_ie_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_Endereco" )]
      [  XmlElement( ElementName = "Pessoa_Endereco"   )]
      public String gxTpr_Pessoa_endereco
      {
         get {
            return gxTv_SdtContratada_Pessoa_endereco ;
         }

         set {
            gxTv_SdtContratada_Pessoa_endereco_N = 0;
            gxTv_SdtContratada_Pessoa_endereco = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Pessoa_endereco_SetNull( )
      {
         gxTv_SdtContratada_Pessoa_endereco_N = 1;
         gxTv_SdtContratada_Pessoa_endereco = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Pessoa_endereco_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_CEP" )]
      [  XmlElement( ElementName = "Pessoa_CEP"   )]
      public String gxTpr_Pessoa_cep
      {
         get {
            return gxTv_SdtContratada_Pessoa_cep ;
         }

         set {
            gxTv_SdtContratada_Pessoa_cep_N = 0;
            gxTv_SdtContratada_Pessoa_cep = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Pessoa_cep_SetNull( )
      {
         gxTv_SdtContratada_Pessoa_cep_N = 1;
         gxTv_SdtContratada_Pessoa_cep = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Pessoa_cep_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_Telefone" )]
      [  XmlElement( ElementName = "Pessoa_Telefone"   )]
      public String gxTpr_Pessoa_telefone
      {
         get {
            return gxTv_SdtContratada_Pessoa_telefone ;
         }

         set {
            gxTv_SdtContratada_Pessoa_telefone_N = 0;
            gxTv_SdtContratada_Pessoa_telefone = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Pessoa_telefone_SetNull( )
      {
         gxTv_SdtContratada_Pessoa_telefone_N = 1;
         gxTv_SdtContratada_Pessoa_telefone = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Pessoa_telefone_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_Fax" )]
      [  XmlElement( ElementName = "Pessoa_Fax"   )]
      public String gxTpr_Pessoa_fax
      {
         get {
            return gxTv_SdtContratada_Pessoa_fax ;
         }

         set {
            gxTv_SdtContratada_Pessoa_fax_N = 0;
            gxTv_SdtContratada_Pessoa_fax = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Pessoa_fax_SetNull( )
      {
         gxTv_SdtContratada_Pessoa_fax_N = 1;
         gxTv_SdtContratada_Pessoa_fax = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Pessoa_fax_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_Sigla" )]
      [  XmlElement( ElementName = "Contratada_Sigla"   )]
      public String gxTpr_Contratada_sigla
      {
         get {
            return gxTv_SdtContratada_Contratada_sigla ;
         }

         set {
            gxTv_SdtContratada_Contratada_sigla = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Contratada_TipoFabrica" )]
      [  XmlElement( ElementName = "Contratada_TipoFabrica"   )]
      public String gxTpr_Contratada_tipofabrica
      {
         get {
            return gxTv_SdtContratada_Contratada_tipofabrica ;
         }

         set {
            gxTv_SdtContratada_Contratada_tipofabrica = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Contratada_BancoNome" )]
      [  XmlElement( ElementName = "Contratada_BancoNome"   )]
      public String gxTpr_Contratada_banconome
      {
         get {
            return gxTv_SdtContratada_Contratada_banconome ;
         }

         set {
            gxTv_SdtContratada_Contratada_banconome_N = 0;
            gxTv_SdtContratada_Contratada_banconome = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_banconome_SetNull( )
      {
         gxTv_SdtContratada_Contratada_banconome_N = 1;
         gxTv_SdtContratada_Contratada_banconome = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_banconome_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_BancoNro" )]
      [  XmlElement( ElementName = "Contratada_BancoNro"   )]
      public String gxTpr_Contratada_banconro
      {
         get {
            return gxTv_SdtContratada_Contratada_banconro ;
         }

         set {
            gxTv_SdtContratada_Contratada_banconro_N = 0;
            gxTv_SdtContratada_Contratada_banconro = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_banconro_SetNull( )
      {
         gxTv_SdtContratada_Contratada_banconro_N = 1;
         gxTv_SdtContratada_Contratada_banconro = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_banconro_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_AgenciaNome" )]
      [  XmlElement( ElementName = "Contratada_AgenciaNome"   )]
      public String gxTpr_Contratada_agencianome
      {
         get {
            return gxTv_SdtContratada_Contratada_agencianome ;
         }

         set {
            gxTv_SdtContratada_Contratada_agencianome_N = 0;
            gxTv_SdtContratada_Contratada_agencianome = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_agencianome_SetNull( )
      {
         gxTv_SdtContratada_Contratada_agencianome_N = 1;
         gxTv_SdtContratada_Contratada_agencianome = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_agencianome_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_AgenciaNro" )]
      [  XmlElement( ElementName = "Contratada_AgenciaNro"   )]
      public String gxTpr_Contratada_agencianro
      {
         get {
            return gxTv_SdtContratada_Contratada_agencianro ;
         }

         set {
            gxTv_SdtContratada_Contratada_agencianro_N = 0;
            gxTv_SdtContratada_Contratada_agencianro = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_agencianro_SetNull( )
      {
         gxTv_SdtContratada_Contratada_agencianro_N = 1;
         gxTv_SdtContratada_Contratada_agencianro = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_agencianro_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_ContaCorrente" )]
      [  XmlElement( ElementName = "Contratada_ContaCorrente"   )]
      public String gxTpr_Contratada_contacorrente
      {
         get {
            return gxTv_SdtContratada_Contratada_contacorrente ;
         }

         set {
            gxTv_SdtContratada_Contratada_contacorrente_N = 0;
            gxTv_SdtContratada_Contratada_contacorrente = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_contacorrente_SetNull( )
      {
         gxTv_SdtContratada_Contratada_contacorrente_N = 1;
         gxTv_SdtContratada_Contratada_contacorrente = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_contacorrente_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_MunicipioCod" )]
      [  XmlElement( ElementName = "Contratada_MunicipioCod"   )]
      public int gxTpr_Contratada_municipiocod
      {
         get {
            return gxTv_SdtContratada_Contratada_municipiocod ;
         }

         set {
            gxTv_SdtContratada_Contratada_municipiocod_N = 0;
            gxTv_SdtContratada_Contratada_municipiocod = (int)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_municipiocod_SetNull( )
      {
         gxTv_SdtContratada_Contratada_municipiocod_N = 1;
         gxTv_SdtContratada_Contratada_municipiocod = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_municipiocod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_UF" )]
      [  XmlElement( ElementName = "Contratada_UF"   )]
      public String gxTpr_Contratada_uf
      {
         get {
            return gxTv_SdtContratada_Contratada_uf ;
         }

         set {
            gxTv_SdtContratada_Contratada_uf_N = 0;
            gxTv_SdtContratada_Contratada_uf = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_uf_SetNull( )
      {
         gxTv_SdtContratada_Contratada_uf_N = 1;
         gxTv_SdtContratada_Contratada_uf = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_uf_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_OS" )]
      [  XmlElement( ElementName = "Contratada_OS"   )]
      public int gxTpr_Contratada_os
      {
         get {
            return gxTv_SdtContratada_Contratada_os ;
         }

         set {
            gxTv_SdtContratada_Contratada_os_N = 0;
            gxTv_SdtContratada_Contratada_os = (int)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_os_SetNull( )
      {
         gxTv_SdtContratada_Contratada_os_N = 1;
         gxTv_SdtContratada_Contratada_os = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_os_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_SS" )]
      [  XmlElement( ElementName = "Contratada_SS"   )]
      public int gxTpr_Contratada_ss
      {
         get {
            return gxTv_SdtContratada_Contratada_ss ;
         }

         set {
            gxTv_SdtContratada_Contratada_ss_N = 0;
            gxTv_SdtContratada_Contratada_ss = (int)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_ss_SetNull( )
      {
         gxTv_SdtContratada_Contratada_ss_N = 1;
         gxTv_SdtContratada_Contratada_ss = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_ss_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_Lote" )]
      [  XmlElement( ElementName = "Contratada_Lote"   )]
      public short gxTpr_Contratada_lote
      {
         get {
            return gxTv_SdtContratada_Contratada_lote ;
         }

         set {
            gxTv_SdtContratada_Contratada_lote_N = 0;
            gxTv_SdtContratada_Contratada_lote = (short)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_lote_SetNull( )
      {
         gxTv_SdtContratada_Contratada_lote_N = 1;
         gxTv_SdtContratada_Contratada_lote = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_lote_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_LogoArquivo" )]
      [  XmlElement( ElementName = "Contratada_LogoArquivo"   )]
      [GxUpload()]
      public byte[] gxTpr_Contratada_logoarquivo_Blob
      {
         get {
            IGxContext context = this.context == null ? new GxContext() : this.context;
            return context.FileToByteArray( gxTv_SdtContratada_Contratada_logoarquivo) ;
         }

         set {
            gxTv_SdtContratada_Contratada_logoarquivo_N = 0;
            IGxContext context = this.context == null ? new GxContext() : this.context;
            gxTv_SdtContratada_Contratada_logoarquivo=context.FileFromByteArray( value) ;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      [GxUpload()]
      public String gxTpr_Contratada_logoarquivo
      {
         get {
            return gxTv_SdtContratada_Contratada_logoarquivo ;
         }

         set {
            gxTv_SdtContratada_Contratada_logoarquivo_N = 0;
            gxTv_SdtContratada_Contratada_logoarquivo = value;
         }

      }

      public void gxTv_SdtContratada_Contratada_logoarquivo_SetBlob( String blob ,
                                                                     String fileName ,
                                                                     String fileType )
      {
         gxTv_SdtContratada_Contratada_logoarquivo = blob;
         gxTv_SdtContratada_Contratada_logonomearq = fileName;
         gxTv_SdtContratada_Contratada_logotipoarq = fileType;
         return  ;
      }

      public void gxTv_SdtContratada_Contratada_logoarquivo_SetNull( )
      {
         gxTv_SdtContratada_Contratada_logoarquivo_N = 1;
         gxTv_SdtContratada_Contratada_logoarquivo = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_logoarquivo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_LogoNomeArq" )]
      [  XmlElement( ElementName = "Contratada_LogoNomeArq"   )]
      public String gxTpr_Contratada_logonomearq
      {
         get {
            return gxTv_SdtContratada_Contratada_logonomearq ;
         }

         set {
            gxTv_SdtContratada_Contratada_logonomearq_N = 0;
            gxTv_SdtContratada_Contratada_logonomearq = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_logonomearq_SetNull( )
      {
         gxTv_SdtContratada_Contratada_logonomearq_N = 1;
         gxTv_SdtContratada_Contratada_logonomearq = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_logonomearq_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_LogoTipoArq" )]
      [  XmlElement( ElementName = "Contratada_LogoTipoArq"   )]
      public String gxTpr_Contratada_logotipoarq
      {
         get {
            return gxTv_SdtContratada_Contratada_logotipoarq ;
         }

         set {
            gxTv_SdtContratada_Contratada_logotipoarq_N = 0;
            gxTv_SdtContratada_Contratada_logotipoarq = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_logotipoarq_SetNull( )
      {
         gxTv_SdtContratada_Contratada_logotipoarq_N = 1;
         gxTv_SdtContratada_Contratada_logotipoarq = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_logotipoarq_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_UsaOSistema" )]
      [  XmlElement( ElementName = "Contratada_UsaOSistema"   )]
      public bool gxTpr_Contratada_usaosistema
      {
         get {
            return gxTv_SdtContratada_Contratada_usaosistema ;
         }

         set {
            gxTv_SdtContratada_Contratada_usaosistema_N = 0;
            gxTv_SdtContratada_Contratada_usaosistema = value;
         }

      }

      public void gxTv_SdtContratada_Contratada_usaosistema_SetNull( )
      {
         gxTv_SdtContratada_Contratada_usaosistema_N = 1;
         gxTv_SdtContratada_Contratada_usaosistema = false;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_usaosistema_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_OSPreferencial" )]
      [  XmlElement( ElementName = "Contratada_OSPreferencial"   )]
      public bool gxTpr_Contratada_ospreferencial
      {
         get {
            return gxTv_SdtContratada_Contratada_ospreferencial ;
         }

         set {
            gxTv_SdtContratada_Contratada_ospreferencial_N = 0;
            gxTv_SdtContratada_Contratada_ospreferencial = value;
         }

      }

      public void gxTv_SdtContratada_Contratada_ospreferencial_SetNull( )
      {
         gxTv_SdtContratada_Contratada_ospreferencial_N = 1;
         gxTv_SdtContratada_Contratada_ospreferencial = false;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_ospreferencial_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_CntPadrao" )]
      [  XmlElement( ElementName = "Contratada_CntPadrao"   )]
      public int gxTpr_Contratada_cntpadrao
      {
         get {
            return gxTv_SdtContratada_Contratada_cntpadrao ;
         }

         set {
            gxTv_SdtContratada_Contratada_cntpadrao_N = 0;
            gxTv_SdtContratada_Contratada_cntpadrao = (int)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_cntpadrao_SetNull( )
      {
         gxTv_SdtContratada_Contratada_cntpadrao_N = 1;
         gxTv_SdtContratada_Contratada_cntpadrao = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_cntpadrao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_Ativo" )]
      [  XmlElement( ElementName = "Contratada_Ativo"   )]
      public bool gxTpr_Contratada_ativo
      {
         get {
            return gxTv_SdtContratada_Contratada_ativo ;
         }

         set {
            gxTv_SdtContratada_Contratada_ativo = value;
         }

      }

      [  SoapElement( ElementName = "Contratada_Logo" )]
      [  XmlElement( ElementName = "Contratada_Logo"   )]
      [GxUpload()]
      public String gxTpr_Contratada_logo
      {
         get {
            return gxTv_SdtContratada_Contratada_logo ;
         }

         set {
            gxTv_SdtContratada_Contratada_logo_N = 0;
            gxTv_SdtContratada_Contratada_logo = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_logo_SetNull( )
      {
         gxTv_SdtContratada_Contratada_logo_N = 1;
         gxTv_SdtContratada_Contratada_logo = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_logo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_Logo_GXI" )]
      [  XmlElement( ElementName = "Contratada_Logo_GXI"   )]
      public String gxTpr_Contratada_logo_gxi
      {
         get {
            return gxTv_SdtContratada_Contratada_logo_gxi ;
         }

         set {
            gxTv_SdtContratada_Contratada_logo_gxi_N = 0;
            gxTv_SdtContratada_Contratada_logo_gxi = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_logo_gxi_SetNull( )
      {
         gxTv_SdtContratada_Contratada_logo_gxi_N = 1;
         gxTv_SdtContratada_Contratada_logo_gxi = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_logo_gxi_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtContratada_Mode ;
         }

         set {
            gxTv_SdtContratada_Mode = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Mode_SetNull( )
      {
         gxTv_SdtContratada_Mode = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtContratada_Initialized ;
         }

         set {
            gxTv_SdtContratada_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtContratada_Initialized_SetNull( )
      {
         gxTv_SdtContratada_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_IE_Z" )]
      [  XmlElement( ElementName = "Pessoa_IE_Z"   )]
      public String gxTpr_Pessoa_ie_Z
      {
         get {
            return gxTv_SdtContratada_Pessoa_ie_Z ;
         }

         set {
            gxTv_SdtContratada_Pessoa_ie_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Pessoa_ie_Z_SetNull( )
      {
         gxTv_SdtContratada_Pessoa_ie_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Pessoa_ie_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_Endereco_Z" )]
      [  XmlElement( ElementName = "Pessoa_Endereco_Z"   )]
      public String gxTpr_Pessoa_endereco_Z
      {
         get {
            return gxTv_SdtContratada_Pessoa_endereco_Z ;
         }

         set {
            gxTv_SdtContratada_Pessoa_endereco_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Pessoa_endereco_Z_SetNull( )
      {
         gxTv_SdtContratada_Pessoa_endereco_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Pessoa_endereco_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_CEP_Z" )]
      [  XmlElement( ElementName = "Pessoa_CEP_Z"   )]
      public String gxTpr_Pessoa_cep_Z
      {
         get {
            return gxTv_SdtContratada_Pessoa_cep_Z ;
         }

         set {
            gxTv_SdtContratada_Pessoa_cep_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Pessoa_cep_Z_SetNull( )
      {
         gxTv_SdtContratada_Pessoa_cep_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Pessoa_cep_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_Telefone_Z" )]
      [  XmlElement( ElementName = "Pessoa_Telefone_Z"   )]
      public String gxTpr_Pessoa_telefone_Z
      {
         get {
            return gxTv_SdtContratada_Pessoa_telefone_Z ;
         }

         set {
            gxTv_SdtContratada_Pessoa_telefone_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Pessoa_telefone_Z_SetNull( )
      {
         gxTv_SdtContratada_Pessoa_telefone_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Pessoa_telefone_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_Fax_Z" )]
      [  XmlElement( ElementName = "Pessoa_Fax_Z"   )]
      public String gxTpr_Pessoa_fax_Z
      {
         get {
            return gxTv_SdtContratada_Pessoa_fax_Z ;
         }

         set {
            gxTv_SdtContratada_Pessoa_fax_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Pessoa_fax_Z_SetNull( )
      {
         gxTv_SdtContratada_Pessoa_fax_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Pessoa_fax_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_Codigo_Z" )]
      [  XmlElement( ElementName = "Contratada_Codigo_Z"   )]
      public int gxTpr_Contratada_codigo_Z
      {
         get {
            return gxTv_SdtContratada_Contratada_codigo_Z ;
         }

         set {
            gxTv_SdtContratada_Contratada_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_codigo_Z_SetNull( )
      {
         gxTv_SdtContratada_Contratada_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_AreaTrabalhoCod_Z" )]
      [  XmlElement( ElementName = "Contratada_AreaTrabalhoCod_Z"   )]
      public int gxTpr_Contratada_areatrabalhocod_Z
      {
         get {
            return gxTv_SdtContratada_Contratada_areatrabalhocod_Z ;
         }

         set {
            gxTv_SdtContratada_Contratada_areatrabalhocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_areatrabalhocod_Z_SetNull( )
      {
         gxTv_SdtContratada_Contratada_areatrabalhocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_areatrabalhocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_AreaTrabalhoDes_Z" )]
      [  XmlElement( ElementName = "Contratada_AreaTrabalhoDes_Z"   )]
      public String gxTpr_Contratada_areatrabalhodes_Z
      {
         get {
            return gxTv_SdtContratada_Contratada_areatrabalhodes_Z ;
         }

         set {
            gxTv_SdtContratada_Contratada_areatrabalhodes_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_areatrabalhodes_Z_SetNull( )
      {
         gxTv_SdtContratada_Contratada_areatrabalhodes_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_areatrabalhodes_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_AreaTrbClcPFnl_Z" )]
      [  XmlElement( ElementName = "Contratada_AreaTrbClcPFnl_Z"   )]
      public String gxTpr_Contratada_areatrbclcpfnl_Z
      {
         get {
            return gxTv_SdtContratada_Contratada_areatrbclcpfnl_Z ;
         }

         set {
            gxTv_SdtContratada_Contratada_areatrbclcpfnl_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_areatrbclcpfnl_Z_SetNull( )
      {
         gxTv_SdtContratada_Contratada_areatrbclcpfnl_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_areatrbclcpfnl_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_AreaTrbSrvPdr_Z" )]
      [  XmlElement( ElementName = "Contratada_AreaTrbSrvPdr_Z"   )]
      public int gxTpr_Contratada_areatrbsrvpdr_Z
      {
         get {
            return gxTv_SdtContratada_Contratada_areatrbsrvpdr_Z ;
         }

         set {
            gxTv_SdtContratada_Contratada_areatrbsrvpdr_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_areatrbsrvpdr_Z_SetNull( )
      {
         gxTv_SdtContratada_Contratada_areatrbsrvpdr_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_areatrbsrvpdr_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_PessoaCod_Z" )]
      [  XmlElement( ElementName = "Contratada_PessoaCod_Z"   )]
      public int gxTpr_Contratada_pessoacod_Z
      {
         get {
            return gxTv_SdtContratada_Contratada_pessoacod_Z ;
         }

         set {
            gxTv_SdtContratada_Contratada_pessoacod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_pessoacod_Z_SetNull( )
      {
         gxTv_SdtContratada_Contratada_pessoacod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_pessoacod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_PessoaNom_Z" )]
      [  XmlElement( ElementName = "Contratada_PessoaNom_Z"   )]
      public String gxTpr_Contratada_pessoanom_Z
      {
         get {
            return gxTv_SdtContratada_Contratada_pessoanom_Z ;
         }

         set {
            gxTv_SdtContratada_Contratada_pessoanom_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_pessoanom_Z_SetNull( )
      {
         gxTv_SdtContratada_Contratada_pessoanom_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_pessoanom_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_PessoaCNPJ_Z" )]
      [  XmlElement( ElementName = "Contratada_PessoaCNPJ_Z"   )]
      public String gxTpr_Contratada_pessoacnpj_Z
      {
         get {
            return gxTv_SdtContratada_Contratada_pessoacnpj_Z ;
         }

         set {
            gxTv_SdtContratada_Contratada_pessoacnpj_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_pessoacnpj_Z_SetNull( )
      {
         gxTv_SdtContratada_Contratada_pessoacnpj_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_pessoacnpj_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_Sigla_Z" )]
      [  XmlElement( ElementName = "Contratada_Sigla_Z"   )]
      public String gxTpr_Contratada_sigla_Z
      {
         get {
            return gxTv_SdtContratada_Contratada_sigla_Z ;
         }

         set {
            gxTv_SdtContratada_Contratada_sigla_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_sigla_Z_SetNull( )
      {
         gxTv_SdtContratada_Contratada_sigla_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_sigla_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_TipoFabrica_Z" )]
      [  XmlElement( ElementName = "Contratada_TipoFabrica_Z"   )]
      public String gxTpr_Contratada_tipofabrica_Z
      {
         get {
            return gxTv_SdtContratada_Contratada_tipofabrica_Z ;
         }

         set {
            gxTv_SdtContratada_Contratada_tipofabrica_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_tipofabrica_Z_SetNull( )
      {
         gxTv_SdtContratada_Contratada_tipofabrica_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_tipofabrica_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_BancoNome_Z" )]
      [  XmlElement( ElementName = "Contratada_BancoNome_Z"   )]
      public String gxTpr_Contratada_banconome_Z
      {
         get {
            return gxTv_SdtContratada_Contratada_banconome_Z ;
         }

         set {
            gxTv_SdtContratada_Contratada_banconome_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_banconome_Z_SetNull( )
      {
         gxTv_SdtContratada_Contratada_banconome_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_banconome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_BancoNro_Z" )]
      [  XmlElement( ElementName = "Contratada_BancoNro_Z"   )]
      public String gxTpr_Contratada_banconro_Z
      {
         get {
            return gxTv_SdtContratada_Contratada_banconro_Z ;
         }

         set {
            gxTv_SdtContratada_Contratada_banconro_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_banconro_Z_SetNull( )
      {
         gxTv_SdtContratada_Contratada_banconro_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_banconro_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_AgenciaNome_Z" )]
      [  XmlElement( ElementName = "Contratada_AgenciaNome_Z"   )]
      public String gxTpr_Contratada_agencianome_Z
      {
         get {
            return gxTv_SdtContratada_Contratada_agencianome_Z ;
         }

         set {
            gxTv_SdtContratada_Contratada_agencianome_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_agencianome_Z_SetNull( )
      {
         gxTv_SdtContratada_Contratada_agencianome_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_agencianome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_AgenciaNro_Z" )]
      [  XmlElement( ElementName = "Contratada_AgenciaNro_Z"   )]
      public String gxTpr_Contratada_agencianro_Z
      {
         get {
            return gxTv_SdtContratada_Contratada_agencianro_Z ;
         }

         set {
            gxTv_SdtContratada_Contratada_agencianro_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_agencianro_Z_SetNull( )
      {
         gxTv_SdtContratada_Contratada_agencianro_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_agencianro_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_ContaCorrente_Z" )]
      [  XmlElement( ElementName = "Contratada_ContaCorrente_Z"   )]
      public String gxTpr_Contratada_contacorrente_Z
      {
         get {
            return gxTv_SdtContratada_Contratada_contacorrente_Z ;
         }

         set {
            gxTv_SdtContratada_Contratada_contacorrente_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_contacorrente_Z_SetNull( )
      {
         gxTv_SdtContratada_Contratada_contacorrente_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_contacorrente_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_MunicipioCod_Z" )]
      [  XmlElement( ElementName = "Contratada_MunicipioCod_Z"   )]
      public int gxTpr_Contratada_municipiocod_Z
      {
         get {
            return gxTv_SdtContratada_Contratada_municipiocod_Z ;
         }

         set {
            gxTv_SdtContratada_Contratada_municipiocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_municipiocod_Z_SetNull( )
      {
         gxTv_SdtContratada_Contratada_municipiocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_municipiocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_UF_Z" )]
      [  XmlElement( ElementName = "Contratada_UF_Z"   )]
      public String gxTpr_Contratada_uf_Z
      {
         get {
            return gxTv_SdtContratada_Contratada_uf_Z ;
         }

         set {
            gxTv_SdtContratada_Contratada_uf_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_uf_Z_SetNull( )
      {
         gxTv_SdtContratada_Contratada_uf_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_uf_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_OS_Z" )]
      [  XmlElement( ElementName = "Contratada_OS_Z"   )]
      public int gxTpr_Contratada_os_Z
      {
         get {
            return gxTv_SdtContratada_Contratada_os_Z ;
         }

         set {
            gxTv_SdtContratada_Contratada_os_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_os_Z_SetNull( )
      {
         gxTv_SdtContratada_Contratada_os_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_os_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_SS_Z" )]
      [  XmlElement( ElementName = "Contratada_SS_Z"   )]
      public int gxTpr_Contratada_ss_Z
      {
         get {
            return gxTv_SdtContratada_Contratada_ss_Z ;
         }

         set {
            gxTv_SdtContratada_Contratada_ss_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_ss_Z_SetNull( )
      {
         gxTv_SdtContratada_Contratada_ss_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_ss_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_Lote_Z" )]
      [  XmlElement( ElementName = "Contratada_Lote_Z"   )]
      public short gxTpr_Contratada_lote_Z
      {
         get {
            return gxTv_SdtContratada_Contratada_lote_Z ;
         }

         set {
            gxTv_SdtContratada_Contratada_lote_Z = (short)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_lote_Z_SetNull( )
      {
         gxTv_SdtContratada_Contratada_lote_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_lote_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_LogoNomeArq_Z" )]
      [  XmlElement( ElementName = "Contratada_LogoNomeArq_Z"   )]
      public String gxTpr_Contratada_logonomearq_Z
      {
         get {
            return gxTv_SdtContratada_Contratada_logonomearq_Z ;
         }

         set {
            gxTv_SdtContratada_Contratada_logonomearq_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_logonomearq_Z_SetNull( )
      {
         gxTv_SdtContratada_Contratada_logonomearq_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_logonomearq_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_LogoTipoArq_Z" )]
      [  XmlElement( ElementName = "Contratada_LogoTipoArq_Z"   )]
      public String gxTpr_Contratada_logotipoarq_Z
      {
         get {
            return gxTv_SdtContratada_Contratada_logotipoarq_Z ;
         }

         set {
            gxTv_SdtContratada_Contratada_logotipoarq_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_logotipoarq_Z_SetNull( )
      {
         gxTv_SdtContratada_Contratada_logotipoarq_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_logotipoarq_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_UsaOSistema_Z" )]
      [  XmlElement( ElementName = "Contratada_UsaOSistema_Z"   )]
      public bool gxTpr_Contratada_usaosistema_Z
      {
         get {
            return gxTv_SdtContratada_Contratada_usaosistema_Z ;
         }

         set {
            gxTv_SdtContratada_Contratada_usaosistema_Z = value;
         }

      }

      public void gxTv_SdtContratada_Contratada_usaosistema_Z_SetNull( )
      {
         gxTv_SdtContratada_Contratada_usaosistema_Z = false;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_usaosistema_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_OSPreferencial_Z" )]
      [  XmlElement( ElementName = "Contratada_OSPreferencial_Z"   )]
      public bool gxTpr_Contratada_ospreferencial_Z
      {
         get {
            return gxTv_SdtContratada_Contratada_ospreferencial_Z ;
         }

         set {
            gxTv_SdtContratada_Contratada_ospreferencial_Z = value;
         }

      }

      public void gxTv_SdtContratada_Contratada_ospreferencial_Z_SetNull( )
      {
         gxTv_SdtContratada_Contratada_ospreferencial_Z = false;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_ospreferencial_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_CntPadrao_Z" )]
      [  XmlElement( ElementName = "Contratada_CntPadrao_Z"   )]
      public int gxTpr_Contratada_cntpadrao_Z
      {
         get {
            return gxTv_SdtContratada_Contratada_cntpadrao_Z ;
         }

         set {
            gxTv_SdtContratada_Contratada_cntpadrao_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_cntpadrao_Z_SetNull( )
      {
         gxTv_SdtContratada_Contratada_cntpadrao_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_cntpadrao_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_Ativo_Z" )]
      [  XmlElement( ElementName = "Contratada_Ativo_Z"   )]
      public bool gxTpr_Contratada_ativo_Z
      {
         get {
            return gxTv_SdtContratada_Contratada_ativo_Z ;
         }

         set {
            gxTv_SdtContratada_Contratada_ativo_Z = value;
         }

      }

      public void gxTv_SdtContratada_Contratada_ativo_Z_SetNull( )
      {
         gxTv_SdtContratada_Contratada_ativo_Z = false;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_ativo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_Logo_GXI_Z" )]
      [  XmlElement( ElementName = "Contratada_Logo_GXI_Z"   )]
      public String gxTpr_Contratada_logo_gxi_Z
      {
         get {
            return gxTv_SdtContratada_Contratada_logo_gxi_Z ;
         }

         set {
            gxTv_SdtContratada_Contratada_logo_gxi_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_logo_gxi_Z_SetNull( )
      {
         gxTv_SdtContratada_Contratada_logo_gxi_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_logo_gxi_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_AreaTrabalhoDes_N" )]
      [  XmlElement( ElementName = "Contratada_AreaTrabalhoDes_N"   )]
      public short gxTpr_Contratada_areatrabalhodes_N
      {
         get {
            return gxTv_SdtContratada_Contratada_areatrabalhodes_N ;
         }

         set {
            gxTv_SdtContratada_Contratada_areatrabalhodes_N = (short)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_areatrabalhodes_N_SetNull( )
      {
         gxTv_SdtContratada_Contratada_areatrabalhodes_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_areatrabalhodes_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_AreaTrbClcPFnl_N" )]
      [  XmlElement( ElementName = "Contratada_AreaTrbClcPFnl_N"   )]
      public short gxTpr_Contratada_areatrbclcpfnl_N
      {
         get {
            return gxTv_SdtContratada_Contratada_areatrbclcpfnl_N ;
         }

         set {
            gxTv_SdtContratada_Contratada_areatrbclcpfnl_N = (short)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_areatrbclcpfnl_N_SetNull( )
      {
         gxTv_SdtContratada_Contratada_areatrbclcpfnl_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_areatrbclcpfnl_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_AreaTrbSrvPdr_N" )]
      [  XmlElement( ElementName = "Contratada_AreaTrbSrvPdr_N"   )]
      public short gxTpr_Contratada_areatrbsrvpdr_N
      {
         get {
            return gxTv_SdtContratada_Contratada_areatrbsrvpdr_N ;
         }

         set {
            gxTv_SdtContratada_Contratada_areatrbsrvpdr_N = (short)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_areatrbsrvpdr_N_SetNull( )
      {
         gxTv_SdtContratada_Contratada_areatrbsrvpdr_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_areatrbsrvpdr_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_PessoaNom_N" )]
      [  XmlElement( ElementName = "Contratada_PessoaNom_N"   )]
      public short gxTpr_Contratada_pessoanom_N
      {
         get {
            return gxTv_SdtContratada_Contratada_pessoanom_N ;
         }

         set {
            gxTv_SdtContratada_Contratada_pessoanom_N = (short)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_pessoanom_N_SetNull( )
      {
         gxTv_SdtContratada_Contratada_pessoanom_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_pessoanom_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_PessoaCNPJ_N" )]
      [  XmlElement( ElementName = "Contratada_PessoaCNPJ_N"   )]
      public short gxTpr_Contratada_pessoacnpj_N
      {
         get {
            return gxTv_SdtContratada_Contratada_pessoacnpj_N ;
         }

         set {
            gxTv_SdtContratada_Contratada_pessoacnpj_N = (short)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_pessoacnpj_N_SetNull( )
      {
         gxTv_SdtContratada_Contratada_pessoacnpj_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_pessoacnpj_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_IE_N" )]
      [  XmlElement( ElementName = "Pessoa_IE_N"   )]
      public short gxTpr_Pessoa_ie_N
      {
         get {
            return gxTv_SdtContratada_Pessoa_ie_N ;
         }

         set {
            gxTv_SdtContratada_Pessoa_ie_N = (short)(value);
         }

      }

      public void gxTv_SdtContratada_Pessoa_ie_N_SetNull( )
      {
         gxTv_SdtContratada_Pessoa_ie_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Pessoa_ie_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_Endereco_N" )]
      [  XmlElement( ElementName = "Pessoa_Endereco_N"   )]
      public short gxTpr_Pessoa_endereco_N
      {
         get {
            return gxTv_SdtContratada_Pessoa_endereco_N ;
         }

         set {
            gxTv_SdtContratada_Pessoa_endereco_N = (short)(value);
         }

      }

      public void gxTv_SdtContratada_Pessoa_endereco_N_SetNull( )
      {
         gxTv_SdtContratada_Pessoa_endereco_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Pessoa_endereco_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_CEP_N" )]
      [  XmlElement( ElementName = "Pessoa_CEP_N"   )]
      public short gxTpr_Pessoa_cep_N
      {
         get {
            return gxTv_SdtContratada_Pessoa_cep_N ;
         }

         set {
            gxTv_SdtContratada_Pessoa_cep_N = (short)(value);
         }

      }

      public void gxTv_SdtContratada_Pessoa_cep_N_SetNull( )
      {
         gxTv_SdtContratada_Pessoa_cep_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Pessoa_cep_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_Telefone_N" )]
      [  XmlElement( ElementName = "Pessoa_Telefone_N"   )]
      public short gxTpr_Pessoa_telefone_N
      {
         get {
            return gxTv_SdtContratada_Pessoa_telefone_N ;
         }

         set {
            gxTv_SdtContratada_Pessoa_telefone_N = (short)(value);
         }

      }

      public void gxTv_SdtContratada_Pessoa_telefone_N_SetNull( )
      {
         gxTv_SdtContratada_Pessoa_telefone_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Pessoa_telefone_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_Fax_N" )]
      [  XmlElement( ElementName = "Pessoa_Fax_N"   )]
      public short gxTpr_Pessoa_fax_N
      {
         get {
            return gxTv_SdtContratada_Pessoa_fax_N ;
         }

         set {
            gxTv_SdtContratada_Pessoa_fax_N = (short)(value);
         }

      }

      public void gxTv_SdtContratada_Pessoa_fax_N_SetNull( )
      {
         gxTv_SdtContratada_Pessoa_fax_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Pessoa_fax_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_BancoNome_N" )]
      [  XmlElement( ElementName = "Contratada_BancoNome_N"   )]
      public short gxTpr_Contratada_banconome_N
      {
         get {
            return gxTv_SdtContratada_Contratada_banconome_N ;
         }

         set {
            gxTv_SdtContratada_Contratada_banconome_N = (short)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_banconome_N_SetNull( )
      {
         gxTv_SdtContratada_Contratada_banconome_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_banconome_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_BancoNro_N" )]
      [  XmlElement( ElementName = "Contratada_BancoNro_N"   )]
      public short gxTpr_Contratada_banconro_N
      {
         get {
            return gxTv_SdtContratada_Contratada_banconro_N ;
         }

         set {
            gxTv_SdtContratada_Contratada_banconro_N = (short)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_banconro_N_SetNull( )
      {
         gxTv_SdtContratada_Contratada_banconro_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_banconro_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_AgenciaNome_N" )]
      [  XmlElement( ElementName = "Contratada_AgenciaNome_N"   )]
      public short gxTpr_Contratada_agencianome_N
      {
         get {
            return gxTv_SdtContratada_Contratada_agencianome_N ;
         }

         set {
            gxTv_SdtContratada_Contratada_agencianome_N = (short)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_agencianome_N_SetNull( )
      {
         gxTv_SdtContratada_Contratada_agencianome_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_agencianome_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_AgenciaNro_N" )]
      [  XmlElement( ElementName = "Contratada_AgenciaNro_N"   )]
      public short gxTpr_Contratada_agencianro_N
      {
         get {
            return gxTv_SdtContratada_Contratada_agencianro_N ;
         }

         set {
            gxTv_SdtContratada_Contratada_agencianro_N = (short)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_agencianro_N_SetNull( )
      {
         gxTv_SdtContratada_Contratada_agencianro_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_agencianro_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_ContaCorrente_N" )]
      [  XmlElement( ElementName = "Contratada_ContaCorrente_N"   )]
      public short gxTpr_Contratada_contacorrente_N
      {
         get {
            return gxTv_SdtContratada_Contratada_contacorrente_N ;
         }

         set {
            gxTv_SdtContratada_Contratada_contacorrente_N = (short)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_contacorrente_N_SetNull( )
      {
         gxTv_SdtContratada_Contratada_contacorrente_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_contacorrente_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_MunicipioCod_N" )]
      [  XmlElement( ElementName = "Contratada_MunicipioCod_N"   )]
      public short gxTpr_Contratada_municipiocod_N
      {
         get {
            return gxTv_SdtContratada_Contratada_municipiocod_N ;
         }

         set {
            gxTv_SdtContratada_Contratada_municipiocod_N = (short)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_municipiocod_N_SetNull( )
      {
         gxTv_SdtContratada_Contratada_municipiocod_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_municipiocod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_UF_N" )]
      [  XmlElement( ElementName = "Contratada_UF_N"   )]
      public short gxTpr_Contratada_uf_N
      {
         get {
            return gxTv_SdtContratada_Contratada_uf_N ;
         }

         set {
            gxTv_SdtContratada_Contratada_uf_N = (short)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_uf_N_SetNull( )
      {
         gxTv_SdtContratada_Contratada_uf_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_uf_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_OS_N" )]
      [  XmlElement( ElementName = "Contratada_OS_N"   )]
      public short gxTpr_Contratada_os_N
      {
         get {
            return gxTv_SdtContratada_Contratada_os_N ;
         }

         set {
            gxTv_SdtContratada_Contratada_os_N = (short)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_os_N_SetNull( )
      {
         gxTv_SdtContratada_Contratada_os_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_os_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_SS_N" )]
      [  XmlElement( ElementName = "Contratada_SS_N"   )]
      public short gxTpr_Contratada_ss_N
      {
         get {
            return gxTv_SdtContratada_Contratada_ss_N ;
         }

         set {
            gxTv_SdtContratada_Contratada_ss_N = (short)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_ss_N_SetNull( )
      {
         gxTv_SdtContratada_Contratada_ss_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_ss_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_Lote_N" )]
      [  XmlElement( ElementName = "Contratada_Lote_N"   )]
      public short gxTpr_Contratada_lote_N
      {
         get {
            return gxTv_SdtContratada_Contratada_lote_N ;
         }

         set {
            gxTv_SdtContratada_Contratada_lote_N = (short)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_lote_N_SetNull( )
      {
         gxTv_SdtContratada_Contratada_lote_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_lote_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_LogoArquivo_N" )]
      [  XmlElement( ElementName = "Contratada_LogoArquivo_N"   )]
      public short gxTpr_Contratada_logoarquivo_N
      {
         get {
            return gxTv_SdtContratada_Contratada_logoarquivo_N ;
         }

         set {
            gxTv_SdtContratada_Contratada_logoarquivo_N = (short)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_logoarquivo_N_SetNull( )
      {
         gxTv_SdtContratada_Contratada_logoarquivo_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_logoarquivo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_LogoNomeArq_N" )]
      [  XmlElement( ElementName = "Contratada_LogoNomeArq_N"   )]
      public short gxTpr_Contratada_logonomearq_N
      {
         get {
            return gxTv_SdtContratada_Contratada_logonomearq_N ;
         }

         set {
            gxTv_SdtContratada_Contratada_logonomearq_N = (short)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_logonomearq_N_SetNull( )
      {
         gxTv_SdtContratada_Contratada_logonomearq_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_logonomearq_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_LogoTipoArq_N" )]
      [  XmlElement( ElementName = "Contratada_LogoTipoArq_N"   )]
      public short gxTpr_Contratada_logotipoarq_N
      {
         get {
            return gxTv_SdtContratada_Contratada_logotipoarq_N ;
         }

         set {
            gxTv_SdtContratada_Contratada_logotipoarq_N = (short)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_logotipoarq_N_SetNull( )
      {
         gxTv_SdtContratada_Contratada_logotipoarq_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_logotipoarq_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_UsaOSistema_N" )]
      [  XmlElement( ElementName = "Contratada_UsaOSistema_N"   )]
      public short gxTpr_Contratada_usaosistema_N
      {
         get {
            return gxTv_SdtContratada_Contratada_usaosistema_N ;
         }

         set {
            gxTv_SdtContratada_Contratada_usaosistema_N = (short)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_usaosistema_N_SetNull( )
      {
         gxTv_SdtContratada_Contratada_usaosistema_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_usaosistema_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_OSPreferencial_N" )]
      [  XmlElement( ElementName = "Contratada_OSPreferencial_N"   )]
      public short gxTpr_Contratada_ospreferencial_N
      {
         get {
            return gxTv_SdtContratada_Contratada_ospreferencial_N ;
         }

         set {
            gxTv_SdtContratada_Contratada_ospreferencial_N = (short)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_ospreferencial_N_SetNull( )
      {
         gxTv_SdtContratada_Contratada_ospreferencial_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_ospreferencial_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_CntPadrao_N" )]
      [  XmlElement( ElementName = "Contratada_CntPadrao_N"   )]
      public short gxTpr_Contratada_cntpadrao_N
      {
         get {
            return gxTv_SdtContratada_Contratada_cntpadrao_N ;
         }

         set {
            gxTv_SdtContratada_Contratada_cntpadrao_N = (short)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_cntpadrao_N_SetNull( )
      {
         gxTv_SdtContratada_Contratada_cntpadrao_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_cntpadrao_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_Logo_N" )]
      [  XmlElement( ElementName = "Contratada_Logo_N"   )]
      public short gxTpr_Contratada_logo_N
      {
         get {
            return gxTv_SdtContratada_Contratada_logo_N ;
         }

         set {
            gxTv_SdtContratada_Contratada_logo_N = (short)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_logo_N_SetNull( )
      {
         gxTv_SdtContratada_Contratada_logo_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_logo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_Logo_GXI_N" )]
      [  XmlElement( ElementName = "Contratada_Logo_GXI_N"   )]
      public short gxTpr_Contratada_logo_gxi_N
      {
         get {
            return gxTv_SdtContratada_Contratada_logo_gxi_N ;
         }

         set {
            gxTv_SdtContratada_Contratada_logo_gxi_N = (short)(value);
         }

      }

      public void gxTv_SdtContratada_Contratada_logo_gxi_N_SetNull( )
      {
         gxTv_SdtContratada_Contratada_logo_gxi_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratada_Contratada_logo_gxi_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtContratada_Contratada_areatrabalhodes = "";
         gxTv_SdtContratada_Contratada_areatrbclcpfnl = "MB";
         gxTv_SdtContratada_Contratada_pessoanom = "";
         gxTv_SdtContratada_Contratada_pessoacnpj = "";
         gxTv_SdtContratada_Pessoa_ie = "";
         gxTv_SdtContratada_Pessoa_endereco = "";
         gxTv_SdtContratada_Pessoa_cep = "";
         gxTv_SdtContratada_Pessoa_telefone = "";
         gxTv_SdtContratada_Pessoa_fax = "";
         gxTv_SdtContratada_Contratada_sigla = "";
         gxTv_SdtContratada_Contratada_tipofabrica = "S";
         gxTv_SdtContratada_Contratada_banconome = "";
         gxTv_SdtContratada_Contratada_banconro = "";
         gxTv_SdtContratada_Contratada_agencianome = "";
         gxTv_SdtContratada_Contratada_agencianro = "";
         gxTv_SdtContratada_Contratada_contacorrente = "";
         gxTv_SdtContratada_Contratada_uf = "";
         gxTv_SdtContratada_Contratada_logoarquivo = "";
         gxTv_SdtContratada_Contratada_logonomearq = "";
         gxTv_SdtContratada_Contratada_logotipoarq = "";
         gxTv_SdtContratada_Contratada_logo = "";
         gxTv_SdtContratada_Contratada_logo_gxi = "";
         gxTv_SdtContratada_Mode = "";
         gxTv_SdtContratada_Pessoa_ie_Z = "";
         gxTv_SdtContratada_Pessoa_endereco_Z = "";
         gxTv_SdtContratada_Pessoa_cep_Z = "";
         gxTv_SdtContratada_Pessoa_telefone_Z = "";
         gxTv_SdtContratada_Pessoa_fax_Z = "";
         gxTv_SdtContratada_Contratada_areatrabalhodes_Z = "";
         gxTv_SdtContratada_Contratada_areatrbclcpfnl_Z = "";
         gxTv_SdtContratada_Contratada_pessoanom_Z = "";
         gxTv_SdtContratada_Contratada_pessoacnpj_Z = "";
         gxTv_SdtContratada_Contratada_sigla_Z = "";
         gxTv_SdtContratada_Contratada_tipofabrica_Z = "";
         gxTv_SdtContratada_Contratada_banconome_Z = "";
         gxTv_SdtContratada_Contratada_banconro_Z = "";
         gxTv_SdtContratada_Contratada_agencianome_Z = "";
         gxTv_SdtContratada_Contratada_agencianro_Z = "";
         gxTv_SdtContratada_Contratada_contacorrente_Z = "";
         gxTv_SdtContratada_Contratada_uf_Z = "";
         gxTv_SdtContratada_Contratada_logonomearq_Z = "";
         gxTv_SdtContratada_Contratada_logotipoarq_Z = "";
         gxTv_SdtContratada_Contratada_logo_gxi_Z = "";
         gxTv_SdtContratada_Contratada_ativo = true;
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "contratada", "GeneXus.Programs.contratada_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtContratada_Contratada_lote ;
      private short gxTv_SdtContratada_Initialized ;
      private short gxTv_SdtContratada_Contratada_lote_Z ;
      private short gxTv_SdtContratada_Contratada_areatrabalhodes_N ;
      private short gxTv_SdtContratada_Contratada_areatrbclcpfnl_N ;
      private short gxTv_SdtContratada_Contratada_areatrbsrvpdr_N ;
      private short gxTv_SdtContratada_Contratada_pessoanom_N ;
      private short gxTv_SdtContratada_Contratada_pessoacnpj_N ;
      private short gxTv_SdtContratada_Pessoa_ie_N ;
      private short gxTv_SdtContratada_Pessoa_endereco_N ;
      private short gxTv_SdtContratada_Pessoa_cep_N ;
      private short gxTv_SdtContratada_Pessoa_telefone_N ;
      private short gxTv_SdtContratada_Pessoa_fax_N ;
      private short gxTv_SdtContratada_Contratada_banconome_N ;
      private short gxTv_SdtContratada_Contratada_banconro_N ;
      private short gxTv_SdtContratada_Contratada_agencianome_N ;
      private short gxTv_SdtContratada_Contratada_agencianro_N ;
      private short gxTv_SdtContratada_Contratada_contacorrente_N ;
      private short gxTv_SdtContratada_Contratada_municipiocod_N ;
      private short gxTv_SdtContratada_Contratada_uf_N ;
      private short gxTv_SdtContratada_Contratada_os_N ;
      private short gxTv_SdtContratada_Contratada_ss_N ;
      private short gxTv_SdtContratada_Contratada_lote_N ;
      private short gxTv_SdtContratada_Contratada_logoarquivo_N ;
      private short gxTv_SdtContratada_Contratada_logonomearq_N ;
      private short gxTv_SdtContratada_Contratada_logotipoarq_N ;
      private short gxTv_SdtContratada_Contratada_usaosistema_N ;
      private short gxTv_SdtContratada_Contratada_ospreferencial_N ;
      private short gxTv_SdtContratada_Contratada_cntpadrao_N ;
      private short gxTv_SdtContratada_Contratada_logo_N ;
      private short gxTv_SdtContratada_Contratada_logo_gxi_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtContratada_Contratada_codigo ;
      private int gxTv_SdtContratada_Contratada_areatrabalhocod ;
      private int gxTv_SdtContratada_Contratada_areatrbsrvpdr ;
      private int gxTv_SdtContratada_Contratada_pessoacod ;
      private int gxTv_SdtContratada_Contratada_municipiocod ;
      private int gxTv_SdtContratada_Contratada_os ;
      private int gxTv_SdtContratada_Contratada_ss ;
      private int gxTv_SdtContratada_Contratada_cntpadrao ;
      private int gxTv_SdtContratada_Contratada_codigo_Z ;
      private int gxTv_SdtContratada_Contratada_areatrabalhocod_Z ;
      private int gxTv_SdtContratada_Contratada_areatrbsrvpdr_Z ;
      private int gxTv_SdtContratada_Contratada_pessoacod_Z ;
      private int gxTv_SdtContratada_Contratada_municipiocod_Z ;
      private int gxTv_SdtContratada_Contratada_os_Z ;
      private int gxTv_SdtContratada_Contratada_ss_Z ;
      private int gxTv_SdtContratada_Contratada_cntpadrao_Z ;
      private String gxTv_SdtContratada_Contratada_areatrbclcpfnl ;
      private String gxTv_SdtContratada_Contratada_pessoanom ;
      private String gxTv_SdtContratada_Pessoa_ie ;
      private String gxTv_SdtContratada_Pessoa_cep ;
      private String gxTv_SdtContratada_Pessoa_telefone ;
      private String gxTv_SdtContratada_Pessoa_fax ;
      private String gxTv_SdtContratada_Contratada_sigla ;
      private String gxTv_SdtContratada_Contratada_tipofabrica ;
      private String gxTv_SdtContratada_Contratada_banconome ;
      private String gxTv_SdtContratada_Contratada_banconro ;
      private String gxTv_SdtContratada_Contratada_agencianome ;
      private String gxTv_SdtContratada_Contratada_agencianro ;
      private String gxTv_SdtContratada_Contratada_contacorrente ;
      private String gxTv_SdtContratada_Contratada_uf ;
      private String gxTv_SdtContratada_Contratada_logonomearq ;
      private String gxTv_SdtContratada_Contratada_logotipoarq ;
      private String gxTv_SdtContratada_Mode ;
      private String gxTv_SdtContratada_Pessoa_ie_Z ;
      private String gxTv_SdtContratada_Pessoa_cep_Z ;
      private String gxTv_SdtContratada_Pessoa_telefone_Z ;
      private String gxTv_SdtContratada_Pessoa_fax_Z ;
      private String gxTv_SdtContratada_Contratada_areatrbclcpfnl_Z ;
      private String gxTv_SdtContratada_Contratada_pessoanom_Z ;
      private String gxTv_SdtContratada_Contratada_sigla_Z ;
      private String gxTv_SdtContratada_Contratada_tipofabrica_Z ;
      private String gxTv_SdtContratada_Contratada_banconome_Z ;
      private String gxTv_SdtContratada_Contratada_banconro_Z ;
      private String gxTv_SdtContratada_Contratada_agencianome_Z ;
      private String gxTv_SdtContratada_Contratada_agencianro_Z ;
      private String gxTv_SdtContratada_Contratada_contacorrente_Z ;
      private String gxTv_SdtContratada_Contratada_uf_Z ;
      private String gxTv_SdtContratada_Contratada_logonomearq_Z ;
      private String gxTv_SdtContratada_Contratada_logotipoarq_Z ;
      private String sTagName ;
      private bool gxTv_SdtContratada_Contratada_usaosistema ;
      private bool gxTv_SdtContratada_Contratada_ospreferencial ;
      private bool gxTv_SdtContratada_Contratada_ativo ;
      private bool gxTv_SdtContratada_Contratada_usaosistema_Z ;
      private bool gxTv_SdtContratada_Contratada_ospreferencial_Z ;
      private bool gxTv_SdtContratada_Contratada_ativo_Z ;
      private String gxTv_SdtContratada_Contratada_areatrabalhodes ;
      private String gxTv_SdtContratada_Contratada_pessoacnpj ;
      private String gxTv_SdtContratada_Pessoa_endereco ;
      private String gxTv_SdtContratada_Contratada_logo_gxi ;
      private String gxTv_SdtContratada_Pessoa_endereco_Z ;
      private String gxTv_SdtContratada_Contratada_areatrabalhodes_Z ;
      private String gxTv_SdtContratada_Contratada_pessoacnpj_Z ;
      private String gxTv_SdtContratada_Contratada_logo_gxi_Z ;
      private String gxTv_SdtContratada_Contratada_logo ;
      private String gxTv_SdtContratada_Contratada_logoarquivo ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"Contratada", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtContratada_RESTInterface : GxGenericCollectionItem<SdtContratada>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContratada_RESTInterface( ) : base()
      {
      }

      public SdtContratada_RESTInterface( SdtContratada psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Contratada_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratada_codigo
      {
         get {
            return sdt.gxTpr_Contratada_codigo ;
         }

         set {
            sdt.gxTpr_Contratada_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contratada_AreaTrabalhoCod" , Order = 1 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratada_areatrabalhocod
      {
         get {
            return sdt.gxTpr_Contratada_areatrabalhocod ;
         }

         set {
            sdt.gxTpr_Contratada_areatrabalhocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contratada_AreaTrabalhoDes" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Contratada_areatrabalhodes
      {
         get {
            return sdt.gxTpr_Contratada_areatrabalhodes ;
         }

         set {
            sdt.gxTpr_Contratada_areatrabalhodes = (String)(value);
         }

      }

      [DataMember( Name = "Contratada_AreaTrbClcPFnl" , Order = 3 )]
      [GxSeudo()]
      public String gxTpr_Contratada_areatrbclcpfnl
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratada_areatrbclcpfnl) ;
         }

         set {
            sdt.gxTpr_Contratada_areatrbclcpfnl = (String)(value);
         }

      }

      [DataMember( Name = "Contratada_AreaTrbSrvPdr" , Order = 4 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratada_areatrbsrvpdr
      {
         get {
            return sdt.gxTpr_Contratada_areatrbsrvpdr ;
         }

         set {
            sdt.gxTpr_Contratada_areatrbsrvpdr = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contratada_PessoaCod" , Order = 5 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratada_pessoacod
      {
         get {
            return sdt.gxTpr_Contratada_pessoacod ;
         }

         set {
            sdt.gxTpr_Contratada_pessoacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contratada_PessoaNom" , Order = 6 )]
      [GxSeudo()]
      public String gxTpr_Contratada_pessoanom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratada_pessoanom) ;
         }

         set {
            sdt.gxTpr_Contratada_pessoanom = (String)(value);
         }

      }

      [DataMember( Name = "Contratada_PessoaCNPJ" , Order = 7 )]
      [GxSeudo()]
      public String gxTpr_Contratada_pessoacnpj
      {
         get {
            return sdt.gxTpr_Contratada_pessoacnpj ;
         }

         set {
            sdt.gxTpr_Contratada_pessoacnpj = (String)(value);
         }

      }

      [DataMember( Name = "Pessoa_IE" , Order = 8 )]
      [GxSeudo()]
      public String gxTpr_Pessoa_ie
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Pessoa_ie) ;
         }

         set {
            sdt.gxTpr_Pessoa_ie = (String)(value);
         }

      }

      [DataMember( Name = "Pessoa_Endereco" , Order = 9 )]
      [GxSeudo()]
      public String gxTpr_Pessoa_endereco
      {
         get {
            return sdt.gxTpr_Pessoa_endereco ;
         }

         set {
            sdt.gxTpr_Pessoa_endereco = (String)(value);
         }

      }

      [DataMember( Name = "Pessoa_CEP" , Order = 10 )]
      [GxSeudo()]
      public String gxTpr_Pessoa_cep
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Pessoa_cep) ;
         }

         set {
            sdt.gxTpr_Pessoa_cep = (String)(value);
         }

      }

      [DataMember( Name = "Pessoa_Telefone" , Order = 11 )]
      [GxSeudo()]
      public String gxTpr_Pessoa_telefone
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Pessoa_telefone) ;
         }

         set {
            sdt.gxTpr_Pessoa_telefone = (String)(value);
         }

      }

      [DataMember( Name = "Pessoa_Fax" , Order = 12 )]
      [GxSeudo()]
      public String gxTpr_Pessoa_fax
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Pessoa_fax) ;
         }

         set {
            sdt.gxTpr_Pessoa_fax = (String)(value);
         }

      }

      [DataMember( Name = "Contratada_Sigla" , Order = 13 )]
      [GxSeudo()]
      public String gxTpr_Contratada_sigla
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratada_sigla) ;
         }

         set {
            sdt.gxTpr_Contratada_sigla = (String)(value);
         }

      }

      [DataMember( Name = "Contratada_TipoFabrica" , Order = 14 )]
      [GxSeudo()]
      public String gxTpr_Contratada_tipofabrica
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratada_tipofabrica) ;
         }

         set {
            sdt.gxTpr_Contratada_tipofabrica = (String)(value);
         }

      }

      [DataMember( Name = "Contratada_BancoNome" , Order = 15 )]
      [GxSeudo()]
      public String gxTpr_Contratada_banconome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratada_banconome) ;
         }

         set {
            sdt.gxTpr_Contratada_banconome = (String)(value);
         }

      }

      [DataMember( Name = "Contratada_BancoNro" , Order = 16 )]
      [GxSeudo()]
      public String gxTpr_Contratada_banconro
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratada_banconro) ;
         }

         set {
            sdt.gxTpr_Contratada_banconro = (String)(value);
         }

      }

      [DataMember( Name = "Contratada_AgenciaNome" , Order = 17 )]
      [GxSeudo()]
      public String gxTpr_Contratada_agencianome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratada_agencianome) ;
         }

         set {
            sdt.gxTpr_Contratada_agencianome = (String)(value);
         }

      }

      [DataMember( Name = "Contratada_AgenciaNro" , Order = 18 )]
      [GxSeudo()]
      public String gxTpr_Contratada_agencianro
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratada_agencianro) ;
         }

         set {
            sdt.gxTpr_Contratada_agencianro = (String)(value);
         }

      }

      [DataMember( Name = "Contratada_ContaCorrente" , Order = 19 )]
      [GxSeudo()]
      public String gxTpr_Contratada_contacorrente
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratada_contacorrente) ;
         }

         set {
            sdt.gxTpr_Contratada_contacorrente = (String)(value);
         }

      }

      [DataMember( Name = "Contratada_MunicipioCod" , Order = 20 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratada_municipiocod
      {
         get {
            return sdt.gxTpr_Contratada_municipiocod ;
         }

         set {
            sdt.gxTpr_Contratada_municipiocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contratada_UF" , Order = 21 )]
      [GxSeudo()]
      public String gxTpr_Contratada_uf
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratada_uf) ;
         }

         set {
            sdt.gxTpr_Contratada_uf = (String)(value);
         }

      }

      [DataMember( Name = "Contratada_OS" , Order = 22 )]
      [GxSeudo()]
      public String gxTpr_Contratada_os
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Contratada_os), 8, 0)) ;
         }

         set {
            sdt.gxTpr_Contratada_os = (int)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "Contratada_SS" , Order = 23 )]
      [GxSeudo()]
      public String gxTpr_Contratada_ss
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Contratada_ss), 8, 0)) ;
         }

         set {
            sdt.gxTpr_Contratada_ss = (int)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "Contratada_Lote" , Order = 24 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contratada_lote
      {
         get {
            return sdt.gxTpr_Contratada_lote ;
         }

         set {
            sdt.gxTpr_Contratada_lote = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contratada_LogoArquivo" , Order = 25 )]
      [GxUpload()]
      public String gxTpr_Contratada_logoarquivo
      {
         get {
            return PathUtil.RelativePath( sdt.gxTpr_Contratada_logoarquivo) ;
         }

         set {
            sdt.gxTpr_Contratada_logoarquivo = value;
         }

      }

      [DataMember( Name = "Contratada_LogoNomeArq" , Order = 26 )]
      public String gxTpr_Contratada_logonomearq
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratada_logonomearq) ;
         }

         set {
            sdt.gxTpr_Contratada_logonomearq = (String)(value);
         }

      }

      [DataMember( Name = "Contratada_LogoTipoArq" , Order = 27 )]
      public String gxTpr_Contratada_logotipoarq
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratada_logotipoarq) ;
         }

         set {
            sdt.gxTpr_Contratada_logotipoarq = (String)(value);
         }

      }

      [DataMember( Name = "Contratada_UsaOSistema" , Order = 28 )]
      [GxSeudo()]
      public bool gxTpr_Contratada_usaosistema
      {
         get {
            return sdt.gxTpr_Contratada_usaosistema ;
         }

         set {
            sdt.gxTpr_Contratada_usaosistema = value;
         }

      }

      [DataMember( Name = "Contratada_OSPreferencial" , Order = 29 )]
      [GxSeudo()]
      public bool gxTpr_Contratada_ospreferencial
      {
         get {
            return sdt.gxTpr_Contratada_ospreferencial ;
         }

         set {
            sdt.gxTpr_Contratada_ospreferencial = value;
         }

      }

      [DataMember( Name = "Contratada_CntPadrao" , Order = 30 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratada_cntpadrao
      {
         get {
            return sdt.gxTpr_Contratada_cntpadrao ;
         }

         set {
            sdt.gxTpr_Contratada_cntpadrao = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contratada_Ativo" , Order = 31 )]
      [GxSeudo()]
      public bool gxTpr_Contratada_ativo
      {
         get {
            return sdt.gxTpr_Contratada_ativo ;
         }

         set {
            sdt.gxTpr_Contratada_ativo = value;
         }

      }

      [DataMember( Name = "Contratada_Logo" , Order = 32 )]
      [GxUpload()]
      public String gxTpr_Contratada_logo
      {
         get {
            return (!String.IsNullOrEmpty(StringUtil.RTrim( sdt.gxTpr_Contratada_logo)) ? PathUtil.RelativePath( sdt.gxTpr_Contratada_logo) : StringUtil.RTrim( sdt.gxTpr_Contratada_logo_gxi)) ;
         }

         set {
            sdt.gxTpr_Contratada_logo = (String)(value);
         }

      }

      public SdtContratada sdt
      {
         get {
            return (SdtContratada)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtContratada() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 96 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
