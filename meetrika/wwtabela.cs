/*
               File: WWTabela
        Description:  Tabela
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:34:0.17
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwtabela : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwtabela( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwtabela( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         dynavSistema_areatrabalhocod = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         dynavTabela_modulocod1 = new GXCombobox();
         chkTabela_Ativo = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vSISTEMA_AREATRABALHOCOD") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvSISTEMA_AREATRABALHOCOD462( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vTABELA_MODULOCOD1") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvTABELA_MODULOCOD1462( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_62 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_62_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_62_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV61Sistema_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61Sistema_AreaTrabalhoCod), 6, 0)));
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17Tabela_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Tabela_Nome1", AV17Tabela_Nome1);
               AV96Tabela_ModuloDes1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV96Tabela_ModuloDes1", AV96Tabela_ModuloDes1);
               AV62Tabela_ModuloCod1 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62Tabela_ModuloCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62Tabela_ModuloCod1), 6, 0)));
               AV70TFTabela_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFTabela_Nome", AV70TFTabela_Nome);
               AV71TFTabela_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFTabela_Nome_Sel", AV71TFTabela_Nome_Sel);
               AV74TFTabela_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFTabela_Descricao", AV74TFTabela_Descricao);
               AV75TFTabela_Descricao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFTabela_Descricao_Sel", AV75TFTabela_Descricao_Sel);
               AV78TFTabela_SistemaDes = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFTabela_SistemaDes", AV78TFTabela_SistemaDes);
               AV79TFTabela_SistemaDes_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFTabela_SistemaDes_Sel", AV79TFTabela_SistemaDes_Sel);
               AV82TFTabela_ModuloDes = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFTabela_ModuloDes", AV82TFTabela_ModuloDes);
               AV83TFTabela_ModuloDes_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFTabela_ModuloDes_Sel", AV83TFTabela_ModuloDes_Sel);
               AV86TFTabela_PaiNom = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86TFTabela_PaiNom", AV86TFTabela_PaiNom);
               AV87TFTabela_PaiNom_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87TFTabela_PaiNom_Sel", AV87TFTabela_PaiNom_Sel);
               AV104TFTabela_MelhoraCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV104TFTabela_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV104TFTabela_MelhoraCod), 6, 0)));
               AV105TFTabela_MelhoraCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105TFTabela_MelhoraCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105TFTabela_MelhoraCod_To), 6, 0)));
               AV90TFTabela_Ativo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90TFTabela_Ativo_Sel", StringUtil.Str( (decimal)(AV90TFTabela_Ativo_Sel), 1, 0));
               AV97ManageFiltersExecutionStep = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV97ManageFiltersExecutionStep), 1, 0));
               AV72ddo_Tabela_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_Tabela_NomeTitleControlIdToReplace", AV72ddo_Tabela_NomeTitleControlIdToReplace);
               AV76ddo_Tabela_DescricaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76ddo_Tabela_DescricaoTitleControlIdToReplace", AV76ddo_Tabela_DescricaoTitleControlIdToReplace);
               AV80ddo_Tabela_SistemaDesTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80ddo_Tabela_SistemaDesTitleControlIdToReplace", AV80ddo_Tabela_SistemaDesTitleControlIdToReplace);
               AV84ddo_Tabela_ModuloDesTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84ddo_Tabela_ModuloDesTitleControlIdToReplace", AV84ddo_Tabela_ModuloDesTitleControlIdToReplace);
               AV88ddo_Tabela_PaiNomTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88ddo_Tabela_PaiNomTitleControlIdToReplace", AV88ddo_Tabela_PaiNomTitleControlIdToReplace);
               AV106ddo_Tabela_MelhoraCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV106ddo_Tabela_MelhoraCodTitleControlIdToReplace", AV106ddo_Tabela_MelhoraCodTitleControlIdToReplace);
               AV91ddo_Tabela_AtivoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91ddo_Tabela_AtivoTitleControlIdToReplace", AV91ddo_Tabela_AtivoTitleControlIdToReplace);
               AV131Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               A172Tabela_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A190Tabela_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV65Tabela_ModuloCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A188Tabela_ModuloCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n188Tabela_ModuloCod = false;
               A181Tabela_PaiCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n181Tabela_PaiCod = false;
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV61Sistema_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Tabela_Nome1, AV96Tabela_ModuloDes1, AV62Tabela_ModuloCod1, AV70TFTabela_Nome, AV71TFTabela_Nome_Sel, AV74TFTabela_Descricao, AV75TFTabela_Descricao_Sel, AV78TFTabela_SistemaDes, AV79TFTabela_SistemaDes_Sel, AV82TFTabela_ModuloDes, AV83TFTabela_ModuloDes_Sel, AV86TFTabela_PaiNom, AV87TFTabela_PaiNom_Sel, AV104TFTabela_MelhoraCod, AV105TFTabela_MelhoraCod_To, AV90TFTabela_Ativo_Sel, AV97ManageFiltersExecutionStep, AV72ddo_Tabela_NomeTitleControlIdToReplace, AV76ddo_Tabela_DescricaoTitleControlIdToReplace, AV80ddo_Tabela_SistemaDesTitleControlIdToReplace, AV84ddo_Tabela_ModuloDesTitleControlIdToReplace, AV88ddo_Tabela_PaiNomTitleControlIdToReplace, AV106ddo_Tabela_MelhoraCodTitleControlIdToReplace, AV91ddo_Tabela_AtivoTitleControlIdToReplace, AV131Pgmname, AV10GridState, AV6WWPContext, A172Tabela_Codigo, A190Tabela_SistemaCod, AV65Tabela_ModuloCod, A188Tabela_ModuloCod, A181Tabela_PaiCod) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA462( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START462( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311734066");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwtabela.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vSISTEMA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV61Sistema_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTABELA_NOME1", StringUtil.RTrim( AV17Tabela_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vTABELA_MODULODES1", StringUtil.RTrim( AV96Tabela_ModuloDes1));
         GxWebStd.gx_hidden_field( context, "GXH_vTABELA_MODULOCOD1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV62Tabela_ModuloCod1), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFTABELA_NOME", StringUtil.RTrim( AV70TFTabela_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFTABELA_NOME_SEL", StringUtil.RTrim( AV71TFTabela_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFTABELA_DESCRICAO", AV74TFTabela_Descricao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFTABELA_DESCRICAO_SEL", AV75TFTabela_Descricao_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFTABELA_SISTEMADES", AV78TFTabela_SistemaDes);
         GxWebStd.gx_hidden_field( context, "GXH_vTFTABELA_SISTEMADES_SEL", AV79TFTabela_SistemaDes_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFTABELA_MODULODES", StringUtil.RTrim( AV82TFTabela_ModuloDes));
         GxWebStd.gx_hidden_field( context, "GXH_vTFTABELA_MODULODES_SEL", StringUtil.RTrim( AV83TFTabela_ModuloDes_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFTABELA_PAINOM", StringUtil.RTrim( AV86TFTabela_PaiNom));
         GxWebStd.gx_hidden_field( context, "GXH_vTFTABELA_PAINOM_SEL", StringUtil.RTrim( AV87TFTabela_PaiNom_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFTABELA_MELHORACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV104TFTabela_MelhoraCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFTABELA_MELHORACOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV105TFTabela_MelhoraCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFTABELA_ATIVO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV90TFTabela_Ativo_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_62", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_62), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMANAGEFILTERSDATA", AV101ManageFiltersData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMANAGEFILTERSDATA", AV101ManageFiltersData);
         }
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV94GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV95GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV92DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV92DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTABELA_NOMETITLEFILTERDATA", AV69Tabela_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTABELA_NOMETITLEFILTERDATA", AV69Tabela_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTABELA_DESCRICAOTITLEFILTERDATA", AV73Tabela_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTABELA_DESCRICAOTITLEFILTERDATA", AV73Tabela_DescricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTABELA_SISTEMADESTITLEFILTERDATA", AV77Tabela_SistemaDesTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTABELA_SISTEMADESTITLEFILTERDATA", AV77Tabela_SistemaDesTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTABELA_MODULODESTITLEFILTERDATA", AV81Tabela_ModuloDesTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTABELA_MODULODESTITLEFILTERDATA", AV81Tabela_ModuloDesTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTABELA_PAINOMTITLEFILTERDATA", AV85Tabela_PaiNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTABELA_PAINOMTITLEFILTERDATA", AV85Tabela_PaiNomTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTABELA_MELHORACODTITLEFILTERDATA", AV103Tabela_MelhoraCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTABELA_MELHORACODTITLEFILTERDATA", AV103Tabela_MelhoraCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTABELA_ATIVOTITLEFILTERDATA", AV89Tabela_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTABELA_ATIVOTITLEFILTERDATA", AV89Tabela_AtivoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV131Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vTABELA_MODULOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV65Tabela_ModuloCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Icon", StringUtil.RTrim( Ddo_managefilters_Icon));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Caption", StringUtil.RTrim( Ddo_managefilters_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Tooltip", StringUtil.RTrim( Ddo_managefilters_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Cls", StringUtil.RTrim( Ddo_managefilters_Cls));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_NOME_Caption", StringUtil.RTrim( Ddo_tabela_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_NOME_Tooltip", StringUtil.RTrim( Ddo_tabela_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_NOME_Cls", StringUtil.RTrim( Ddo_tabela_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_tabela_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_tabela_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_tabela_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_tabela_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_tabela_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_tabela_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_NOME_Sortedstatus", StringUtil.RTrim( Ddo_tabela_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_NOME_Includefilter", StringUtil.BoolToStr( Ddo_tabela_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_NOME_Filtertype", StringUtil.RTrim( Ddo_tabela_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_tabela_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_tabela_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_NOME_Datalisttype", StringUtil.RTrim( Ddo_tabela_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_NOME_Datalistproc", StringUtil.RTrim( Ddo_tabela_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_tabela_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_NOME_Sortasc", StringUtil.RTrim( Ddo_tabela_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_NOME_Sortdsc", StringUtil.RTrim( Ddo_tabela_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_NOME_Loadingdata", StringUtil.RTrim( Ddo_tabela_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_NOME_Cleanfilter", StringUtil.RTrim( Ddo_tabela_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_NOME_Noresultsfound", StringUtil.RTrim( Ddo_tabela_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_tabela_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_DESCRICAO_Caption", StringUtil.RTrim( Ddo_tabela_descricao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_tabela_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_DESCRICAO_Cls", StringUtil.RTrim( Ddo_tabela_descricao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_tabela_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_tabela_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_tabela_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_tabela_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_tabela_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_tabela_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_tabela_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_tabela_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_tabela_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_tabela_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_tabela_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_tabela_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_tabela_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_tabela_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_tabela_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_tabela_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_tabela_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_tabela_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_tabela_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_tabela_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_SISTEMADES_Caption", StringUtil.RTrim( Ddo_tabela_sistemades_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_SISTEMADES_Tooltip", StringUtil.RTrim( Ddo_tabela_sistemades_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_SISTEMADES_Cls", StringUtil.RTrim( Ddo_tabela_sistemades_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_SISTEMADES_Filteredtext_set", StringUtil.RTrim( Ddo_tabela_sistemades_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_SISTEMADES_Selectedvalue_set", StringUtil.RTrim( Ddo_tabela_sistemades_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_SISTEMADES_Dropdownoptionstype", StringUtil.RTrim( Ddo_tabela_sistemades_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_SISTEMADES_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_tabela_sistemades_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_SISTEMADES_Includesortasc", StringUtil.BoolToStr( Ddo_tabela_sistemades_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_SISTEMADES_Includesortdsc", StringUtil.BoolToStr( Ddo_tabela_sistemades_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_SISTEMADES_Sortedstatus", StringUtil.RTrim( Ddo_tabela_sistemades_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_SISTEMADES_Includefilter", StringUtil.BoolToStr( Ddo_tabela_sistemades_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_SISTEMADES_Filtertype", StringUtil.RTrim( Ddo_tabela_sistemades_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_SISTEMADES_Filterisrange", StringUtil.BoolToStr( Ddo_tabela_sistemades_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_SISTEMADES_Includedatalist", StringUtil.BoolToStr( Ddo_tabela_sistemades_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_SISTEMADES_Datalisttype", StringUtil.RTrim( Ddo_tabela_sistemades_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_SISTEMADES_Datalistproc", StringUtil.RTrim( Ddo_tabela_sistemades_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_SISTEMADES_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_tabela_sistemades_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_SISTEMADES_Sortasc", StringUtil.RTrim( Ddo_tabela_sistemades_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_SISTEMADES_Sortdsc", StringUtil.RTrim( Ddo_tabela_sistemades_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_SISTEMADES_Loadingdata", StringUtil.RTrim( Ddo_tabela_sistemades_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_SISTEMADES_Cleanfilter", StringUtil.RTrim( Ddo_tabela_sistemades_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_SISTEMADES_Noresultsfound", StringUtil.RTrim( Ddo_tabela_sistemades_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_SISTEMADES_Searchbuttontext", StringUtil.RTrim( Ddo_tabela_sistemades_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MODULODES_Caption", StringUtil.RTrim( Ddo_tabela_modulodes_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MODULODES_Tooltip", StringUtil.RTrim( Ddo_tabela_modulodes_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MODULODES_Cls", StringUtil.RTrim( Ddo_tabela_modulodes_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MODULODES_Filteredtext_set", StringUtil.RTrim( Ddo_tabela_modulodes_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MODULODES_Selectedvalue_set", StringUtil.RTrim( Ddo_tabela_modulodes_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MODULODES_Dropdownoptionstype", StringUtil.RTrim( Ddo_tabela_modulodes_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MODULODES_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_tabela_modulodes_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MODULODES_Includesortasc", StringUtil.BoolToStr( Ddo_tabela_modulodes_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MODULODES_Includesortdsc", StringUtil.BoolToStr( Ddo_tabela_modulodes_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MODULODES_Sortedstatus", StringUtil.RTrim( Ddo_tabela_modulodes_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MODULODES_Includefilter", StringUtil.BoolToStr( Ddo_tabela_modulodes_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MODULODES_Filtertype", StringUtil.RTrim( Ddo_tabela_modulodes_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MODULODES_Filterisrange", StringUtil.BoolToStr( Ddo_tabela_modulodes_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MODULODES_Includedatalist", StringUtil.BoolToStr( Ddo_tabela_modulodes_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MODULODES_Datalisttype", StringUtil.RTrim( Ddo_tabela_modulodes_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MODULODES_Datalistproc", StringUtil.RTrim( Ddo_tabela_modulodes_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MODULODES_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_tabela_modulodes_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MODULODES_Sortasc", StringUtil.RTrim( Ddo_tabela_modulodes_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MODULODES_Sortdsc", StringUtil.RTrim( Ddo_tabela_modulodes_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MODULODES_Loadingdata", StringUtil.RTrim( Ddo_tabela_modulodes_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MODULODES_Cleanfilter", StringUtil.RTrim( Ddo_tabela_modulodes_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MODULODES_Noresultsfound", StringUtil.RTrim( Ddo_tabela_modulodes_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MODULODES_Searchbuttontext", StringUtil.RTrim( Ddo_tabela_modulodes_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_PAINOM_Caption", StringUtil.RTrim( Ddo_tabela_painom_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_PAINOM_Tooltip", StringUtil.RTrim( Ddo_tabela_painom_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_PAINOM_Cls", StringUtil.RTrim( Ddo_tabela_painom_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_PAINOM_Filteredtext_set", StringUtil.RTrim( Ddo_tabela_painom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_PAINOM_Selectedvalue_set", StringUtil.RTrim( Ddo_tabela_painom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_PAINOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_tabela_painom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_PAINOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_tabela_painom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_PAINOM_Includesortasc", StringUtil.BoolToStr( Ddo_tabela_painom_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_PAINOM_Includesortdsc", StringUtil.BoolToStr( Ddo_tabela_painom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_PAINOM_Sortedstatus", StringUtil.RTrim( Ddo_tabela_painom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_PAINOM_Includefilter", StringUtil.BoolToStr( Ddo_tabela_painom_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_PAINOM_Filtertype", StringUtil.RTrim( Ddo_tabela_painom_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_PAINOM_Filterisrange", StringUtil.BoolToStr( Ddo_tabela_painom_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_PAINOM_Includedatalist", StringUtil.BoolToStr( Ddo_tabela_painom_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_PAINOM_Datalisttype", StringUtil.RTrim( Ddo_tabela_painom_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_PAINOM_Datalistproc", StringUtil.RTrim( Ddo_tabela_painom_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_PAINOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_tabela_painom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_PAINOM_Sortasc", StringUtil.RTrim( Ddo_tabela_painom_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_PAINOM_Sortdsc", StringUtil.RTrim( Ddo_tabela_painom_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_PAINOM_Loadingdata", StringUtil.RTrim( Ddo_tabela_painom_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_PAINOM_Cleanfilter", StringUtil.RTrim( Ddo_tabela_painom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_PAINOM_Noresultsfound", StringUtil.RTrim( Ddo_tabela_painom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_PAINOM_Searchbuttontext", StringUtil.RTrim( Ddo_tabela_painom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MELHORACOD_Caption", StringUtil.RTrim( Ddo_tabela_melhoracod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MELHORACOD_Tooltip", StringUtil.RTrim( Ddo_tabela_melhoracod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MELHORACOD_Cls", StringUtil.RTrim( Ddo_tabela_melhoracod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MELHORACOD_Filteredtext_set", StringUtil.RTrim( Ddo_tabela_melhoracod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MELHORACOD_Filteredtextto_set", StringUtil.RTrim( Ddo_tabela_melhoracod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MELHORACOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_tabela_melhoracod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MELHORACOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_tabela_melhoracod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MELHORACOD_Includesortasc", StringUtil.BoolToStr( Ddo_tabela_melhoracod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MELHORACOD_Includesortdsc", StringUtil.BoolToStr( Ddo_tabela_melhoracod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MELHORACOD_Sortedstatus", StringUtil.RTrim( Ddo_tabela_melhoracod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MELHORACOD_Includefilter", StringUtil.BoolToStr( Ddo_tabela_melhoracod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MELHORACOD_Filtertype", StringUtil.RTrim( Ddo_tabela_melhoracod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MELHORACOD_Filterisrange", StringUtil.BoolToStr( Ddo_tabela_melhoracod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MELHORACOD_Includedatalist", StringUtil.BoolToStr( Ddo_tabela_melhoracod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MELHORACOD_Sortasc", StringUtil.RTrim( Ddo_tabela_melhoracod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MELHORACOD_Sortdsc", StringUtil.RTrim( Ddo_tabela_melhoracod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MELHORACOD_Cleanfilter", StringUtil.RTrim( Ddo_tabela_melhoracod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MELHORACOD_Rangefilterfrom", StringUtil.RTrim( Ddo_tabela_melhoracod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MELHORACOD_Rangefilterto", StringUtil.RTrim( Ddo_tabela_melhoracod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MELHORACOD_Searchbuttontext", StringUtil.RTrim( Ddo_tabela_melhoracod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_ATIVO_Caption", StringUtil.RTrim( Ddo_tabela_ativo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_ATIVO_Tooltip", StringUtil.RTrim( Ddo_tabela_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_ATIVO_Cls", StringUtil.RTrim( Ddo_tabela_ativo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_tabela_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_tabela_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_tabela_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_tabela_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_tabela_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_tabela_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_tabela_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_tabela_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_tabela_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_tabela_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_ATIVO_Sortasc", StringUtil.RTrim( Ddo_tabela_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_tabela_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_tabela_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_tabela_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_NOME_Activeeventkey", StringUtil.RTrim( Ddo_tabela_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_tabela_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_tabela_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_tabela_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_tabela_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_tabela_descricao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_SISTEMADES_Activeeventkey", StringUtil.RTrim( Ddo_tabela_sistemades_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_SISTEMADES_Filteredtext_get", StringUtil.RTrim( Ddo_tabela_sistemades_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_SISTEMADES_Selectedvalue_get", StringUtil.RTrim( Ddo_tabela_sistemades_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MODULODES_Activeeventkey", StringUtil.RTrim( Ddo_tabela_modulodes_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MODULODES_Filteredtext_get", StringUtil.RTrim( Ddo_tabela_modulodes_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MODULODES_Selectedvalue_get", StringUtil.RTrim( Ddo_tabela_modulodes_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_PAINOM_Activeeventkey", StringUtil.RTrim( Ddo_tabela_painom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_PAINOM_Filteredtext_get", StringUtil.RTrim( Ddo_tabela_painom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_PAINOM_Selectedvalue_get", StringUtil.RTrim( Ddo_tabela_painom_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MELHORACOD_Activeeventkey", StringUtil.RTrim( Ddo_tabela_melhoracod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MELHORACOD_Filteredtext_get", StringUtil.RTrim( Ddo_tabela_melhoracod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_MELHORACOD_Filteredtextto_get", StringUtil.RTrim( Ddo_tabela_melhoracod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_tabela_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_TABELA_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_tabela_ativo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Activeeventkey", StringUtil.RTrim( Ddo_managefilters_Activeeventkey));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE462( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT462( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwtabela.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWTabela" ;
      }

      public override String GetPgmdesc( )
      {
         return " Tabela" ;
      }

      protected void WB460( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_462( true) ;
         }
         else
         {
            wb_table1_2_462( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_462e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'" + sGXsfl_62_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavManagefiltersexecutionstep_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV97ManageFiltersExecutionStep), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV97ManageFiltersExecutionStep), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,81);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavManagefiltersexecutionstep_Jsonclick, 0, "Attribute", "", "", "", edtavManagefiltersexecutionstep_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWTabela.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'" + sGXsfl_62_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftabela_nome_Internalname, StringUtil.RTrim( AV70TFTabela_Nome), StringUtil.RTrim( context.localUtil.Format( AV70TFTabela_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,82);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftabela_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTftabela_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWTabela.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'" + sGXsfl_62_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftabela_nome_sel_Internalname, StringUtil.RTrim( AV71TFTabela_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV71TFTabela_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,83);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftabela_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTftabela_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWTabela.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'" + sGXsfl_62_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTftabela_descricao_Internalname, AV74TFTabela_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,84);\"", 0, edtavTftabela_descricao_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWTabela.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'',false,'" + sGXsfl_62_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTftabela_descricao_sel_Internalname, AV75TFTabela_Descricao_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,85);\"", 0, edtavTftabela_descricao_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWTabela.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'',false,'" + sGXsfl_62_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftabela_sistemades_Internalname, AV78TFTabela_SistemaDes, StringUtil.RTrim( context.localUtil.Format( AV78TFTabela_SistemaDes, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,86);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftabela_sistemades_Jsonclick, 0, "Attribute", "", "", "", edtavTftabela_sistemades_Visible, 1, 0, "text", "", 300, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWTabela.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'',false,'" + sGXsfl_62_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftabela_sistemades_sel_Internalname, AV79TFTabela_SistemaDes_Sel, StringUtil.RTrim( context.localUtil.Format( AV79TFTabela_SistemaDes_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,87);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftabela_sistemades_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTftabela_sistemades_sel_Visible, 1, 0, "text", "", 300, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWTabela.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'',false,'" + sGXsfl_62_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftabela_modulodes_Internalname, StringUtil.RTrim( AV82TFTabela_ModuloDes), StringUtil.RTrim( context.localUtil.Format( AV82TFTabela_ModuloDes, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,88);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftabela_modulodes_Jsonclick, 0, "Attribute", "", "", "", edtavTftabela_modulodes_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWTabela.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'" + sGXsfl_62_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftabela_modulodes_sel_Internalname, StringUtil.RTrim( AV83TFTabela_ModuloDes_Sel), StringUtil.RTrim( context.localUtil.Format( AV83TFTabela_ModuloDes_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,89);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftabela_modulodes_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTftabela_modulodes_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWTabela.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'',false,'" + sGXsfl_62_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftabela_painom_Internalname, StringUtil.RTrim( AV86TFTabela_PaiNom), StringUtil.RTrim( context.localUtil.Format( AV86TFTabela_PaiNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,90);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftabela_painom_Jsonclick, 0, "Attribute", "", "", "", edtavTftabela_painom_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWTabela.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'" + sGXsfl_62_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftabela_painom_sel_Internalname, StringUtil.RTrim( AV87TFTabela_PaiNom_Sel), StringUtil.RTrim( context.localUtil.Format( AV87TFTabela_PaiNom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,91);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftabela_painom_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTftabela_painom_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWTabela.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'" + sGXsfl_62_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftabela_melhoracod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV104TFTabela_MelhoraCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV104TFTabela_MelhoraCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,92);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftabela_melhoracod_Jsonclick, 0, "Attribute", "", "", "", edtavTftabela_melhoracod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWTabela.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'',false,'" + sGXsfl_62_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftabela_melhoracod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV105TFTabela_MelhoraCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV105TFTabela_MelhoraCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,93);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftabela_melhoracod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTftabela_melhoracod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWTabela.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'" + sGXsfl_62_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftabela_ativo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV90TFTabela_Ativo_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV90TFTabela_Ativo_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,94);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftabela_ativo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTftabela_ativo_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWTabela.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_TABELA_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_62_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_tabela_nometitlecontrolidtoreplace_Internalname, AV72ddo_Tabela_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,96);\"", 0, edtavDdo_tabela_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWTabela.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_TABELA_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'" + sGXsfl_62_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_tabela_descricaotitlecontrolidtoreplace_Internalname, AV76ddo_Tabela_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,98);\"", 0, edtavDdo_tabela_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWTabela.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_TABELA_SISTEMADESContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_62_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_tabela_sistemadestitlecontrolidtoreplace_Internalname, AV80ddo_Tabela_SistemaDesTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,100);\"", 0, edtavDdo_tabela_sistemadestitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWTabela.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_TABELA_MODULODESContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_62_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_tabela_modulodestitlecontrolidtoreplace_Internalname, AV84ddo_Tabela_ModuloDesTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,102);\"", 0, edtavDdo_tabela_modulodestitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWTabela.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_TABELA_PAINOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_62_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_tabela_painomtitlecontrolidtoreplace_Internalname, AV88ddo_Tabela_PaiNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,104);\"", 0, edtavDdo_tabela_painomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWTabela.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_TABELA_MELHORACODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_62_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_tabela_melhoracodtitlecontrolidtoreplace_Internalname, AV106ddo_Tabela_MelhoraCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,106);\"", 0, edtavDdo_tabela_melhoracodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWTabela.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_TABELA_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_62_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_tabela_ativotitlecontrolidtoreplace_Internalname, AV91ddo_Tabela_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,108);\"", 0, edtavDdo_tabela_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWTabela.htm");
         }
         wbLoad = true;
      }

      protected void START462( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Tabela", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP460( ) ;
      }

      protected void WS462( )
      {
         START462( ) ;
         EVT462( ) ;
      }

      protected void EVT462( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_MANAGEFILTERS.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11462 */
                              E11462 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12462 */
                              E12462 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_TABELA_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13462 */
                              E13462 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_TABELA_DESCRICAO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14462 */
                              E14462 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_TABELA_SISTEMADES.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15462 */
                              E15462 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_TABELA_MODULODES.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16462 */
                              E16462 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_TABELA_PAINOM.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17462 */
                              E17462 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_TABELA_MELHORACOD.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18462 */
                              E18462 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_TABELA_ATIVO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19462 */
                              E19462 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20462 */
                              E20462 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21462 */
                              E21462 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22462 */
                              E22462 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_62_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_62_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_62_idx), 4, 0)), 4, "0");
                              SubsflControlProps_622( ) ;
                              AV37Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV37Update)) ? AV128Update_GXI : context.convertURL( context.PathToRelativeUrl( AV37Update))));
                              AV38Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV38Delete)) ? AV129Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV38Delete))));
                              AV66Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV66Display)) ? AV130Display_GXI : context.convertURL( context.PathToRelativeUrl( AV66Display))));
                              A172Tabela_Codigo = (int)(context.localUtil.CToN( cgiGet( edtTabela_Codigo_Internalname), ",", "."));
                              A173Tabela_Nome = StringUtil.Upper( cgiGet( edtTabela_Nome_Internalname));
                              A175Tabela_Descricao = cgiGet( edtTabela_Descricao_Internalname);
                              n175Tabela_Descricao = false;
                              A190Tabela_SistemaCod = (int)(context.localUtil.CToN( cgiGet( edtTabela_SistemaCod_Internalname), ",", "."));
                              A191Tabela_SistemaDes = StringUtil.Upper( cgiGet( edtTabela_SistemaDes_Internalname));
                              n191Tabela_SistemaDes = false;
                              A188Tabela_ModuloCod = (int)(context.localUtil.CToN( cgiGet( edtTabela_ModuloCod_Internalname), ",", "."));
                              n188Tabela_ModuloCod = false;
                              A189Tabela_ModuloDes = StringUtil.Upper( cgiGet( edtTabela_ModuloDes_Internalname));
                              n189Tabela_ModuloDes = false;
                              A181Tabela_PaiCod = (int)(context.localUtil.CToN( cgiGet( edtTabela_PaiCod_Internalname), ",", "."));
                              n181Tabela_PaiCod = false;
                              A182Tabela_PaiNom = StringUtil.Upper( cgiGet( edtTabela_PaiNom_Internalname));
                              n182Tabela_PaiNom = false;
                              A746Tabela_MelhoraCod = (int)(context.localUtil.CToN( cgiGet( edtTabela_MelhoraCod_Internalname), ",", "."));
                              n746Tabela_MelhoraCod = false;
                              A174Tabela_Ativo = StringUtil.StrToBool( cgiGet( chkTabela_Ativo_Internalname));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E23462 */
                                    E23462 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E24462 */
                                    E24462 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E25462 */
                                    E25462 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Sistema_areatrabalhocod Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vSISTEMA_AREATRABALHOCOD"), ",", ".") != Convert.ToDecimal( AV61Sistema_AreaTrabalhoCod )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tabela_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTABELA_NOME1"), AV17Tabela_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tabela_modulodes1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTABELA_MODULODES1"), AV96Tabela_ModuloDes1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tabela_modulocod1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTABELA_MODULOCOD1"), ",", ".") != Convert.ToDecimal( AV62Tabela_ModuloCod1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tftabela_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTABELA_NOME"), AV70TFTabela_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tftabela_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTABELA_NOME_SEL"), AV71TFTabela_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tftabela_descricao Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTABELA_DESCRICAO"), AV74TFTabela_Descricao) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tftabela_descricao_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTABELA_DESCRICAO_SEL"), AV75TFTabela_Descricao_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tftabela_sistemades Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTABELA_SISTEMADES"), AV78TFTabela_SistemaDes) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tftabela_sistemades_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTABELA_SISTEMADES_SEL"), AV79TFTabela_SistemaDes_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tftabela_modulodes Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTABELA_MODULODES"), AV82TFTabela_ModuloDes) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tftabela_modulodes_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTABELA_MODULODES_SEL"), AV83TFTabela_ModuloDes_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tftabela_painom Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTABELA_PAINOM"), AV86TFTabela_PaiNom) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tftabela_painom_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTABELA_PAINOM_SEL"), AV87TFTabela_PaiNom_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tftabela_melhoracod Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFTABELA_MELHORACOD"), ",", ".") != Convert.ToDecimal( AV104TFTabela_MelhoraCod )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tftabela_melhoracod_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFTABELA_MELHORACOD_TO"), ",", ".") != Convert.ToDecimal( AV105TFTabela_MelhoraCod_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tftabela_ativo_sel Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFTABELA_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV90TFTabela_Ativo_Sel )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE462( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA462( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            dynavSistema_areatrabalhocod.Name = "vSISTEMA_AREATRABALHOCOD";
            dynavSistema_areatrabalhocod.WebTags = "";
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("TABELA_NOME", "Nome", 0);
            cmbavDynamicfiltersselector1.addItem("TABELA_MODULODES", "Des", 0);
            cmbavDynamicfiltersselector1.addItem("TABELA_MODULOCOD", "M�dulo", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            cmbavDynamicfiltersoperator1.addItem("2", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("3", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            dynavTabela_modulocod1.Name = "vTABELA_MODULOCOD1";
            dynavTabela_modulocod1.WebTags = "";
            GXCCtl = "TABELA_ATIVO_" + sGXsfl_62_idx;
            chkTabela_Ativo.Name = GXCCtl;
            chkTabela_Ativo.WebTags = "";
            chkTabela_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkTabela_Ativo_Internalname, "TitleCaption", chkTabela_Ativo.Caption);
            chkTabela_Ativo.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvSISTEMA_AREATRABALHOCOD462( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvSISTEMA_AREATRABALHOCOD_data462( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvSISTEMA_AREATRABALHOCOD_html462( )
      {
         int gxdynajaxvalue ;
         GXDLVvSISTEMA_AREATRABALHOCOD_data462( ) ;
         gxdynajaxindex = 1;
         dynavSistema_areatrabalhocod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavSistema_areatrabalhocod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavSistema_areatrabalhocod.ItemCount > 0 )
         {
            AV61Sistema_AreaTrabalhoCod = (int)(NumberUtil.Val( dynavSistema_areatrabalhocod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV61Sistema_AreaTrabalhoCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61Sistema_AreaTrabalhoCod), 6, 0)));
         }
      }

      protected void GXDLVvSISTEMA_AREATRABALHOCOD_data462( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor H00462 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00462_A5AreaTrabalho_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00462_A6AreaTrabalho_Descricao[0]);
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXDLVvTABELA_MODULOCOD1462( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvTABELA_MODULOCOD1_data462( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvTABELA_MODULOCOD1_html462( )
      {
         int gxdynajaxvalue ;
         GXDLVvTABELA_MODULOCOD1_data462( ) ;
         gxdynajaxindex = 1;
         dynavTabela_modulocod1.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavTabela_modulocod1.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavTabela_modulocod1.ItemCount > 0 )
         {
            AV62Tabela_ModuloCod1 = (int)(NumberUtil.Val( dynavTabela_modulocod1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV62Tabela_ModuloCod1), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62Tabela_ModuloCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62Tabela_ModuloCod1), 6, 0)));
         }
      }

      protected void GXDLVvTABELA_MODULOCOD1_data462( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00463 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00463_A146Modulo_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00463_A143Modulo_Nome[0]));
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_622( ) ;
         while ( nGXsfl_62_idx <= nRC_GXsfl_62 )
         {
            sendrow_622( ) ;
            nGXsfl_62_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_62_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_62_idx+1));
            sGXsfl_62_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_62_idx), 4, 0)), 4, "0");
            SubsflControlProps_622( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       int AV61Sistema_AreaTrabalhoCod ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV17Tabela_Nome1 ,
                                       String AV96Tabela_ModuloDes1 ,
                                       int AV62Tabela_ModuloCod1 ,
                                       String AV70TFTabela_Nome ,
                                       String AV71TFTabela_Nome_Sel ,
                                       String AV74TFTabela_Descricao ,
                                       String AV75TFTabela_Descricao_Sel ,
                                       String AV78TFTabela_SistemaDes ,
                                       String AV79TFTabela_SistemaDes_Sel ,
                                       String AV82TFTabela_ModuloDes ,
                                       String AV83TFTabela_ModuloDes_Sel ,
                                       String AV86TFTabela_PaiNom ,
                                       String AV87TFTabela_PaiNom_Sel ,
                                       int AV104TFTabela_MelhoraCod ,
                                       int AV105TFTabela_MelhoraCod_To ,
                                       short AV90TFTabela_Ativo_Sel ,
                                       short AV97ManageFiltersExecutionStep ,
                                       String AV72ddo_Tabela_NomeTitleControlIdToReplace ,
                                       String AV76ddo_Tabela_DescricaoTitleControlIdToReplace ,
                                       String AV80ddo_Tabela_SistemaDesTitleControlIdToReplace ,
                                       String AV84ddo_Tabela_ModuloDesTitleControlIdToReplace ,
                                       String AV88ddo_Tabela_PaiNomTitleControlIdToReplace ,
                                       String AV106ddo_Tabela_MelhoraCodTitleControlIdToReplace ,
                                       String AV91ddo_Tabela_AtivoTitleControlIdToReplace ,
                                       String AV131Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       int A172Tabela_Codigo ,
                                       int A190Tabela_SistemaCod ,
                                       int AV65Tabela_ModuloCod ,
                                       int A188Tabela_ModuloCod ,
                                       int A181Tabela_PaiCod )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF462( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_TABELA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A172Tabela_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "TABELA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A172Tabela_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_TABELA_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A173Tabela_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "TABELA_NOME", StringUtil.RTrim( A173Tabela_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_TABELA_DESCRICAO", GetSecureSignedToken( "", A175Tabela_Descricao));
         GxWebStd.gx_hidden_field( context, "TABELA_DESCRICAO", A175Tabela_Descricao);
         GxWebStd.gx_hidden_field( context, "gxhash_TABELA_SISTEMACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A190Tabela_SistemaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "TABELA_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A190Tabela_SistemaCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_TABELA_MODULOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A188Tabela_ModuloCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "TABELA_MODULOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A188Tabela_ModuloCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_TABELA_PAICOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A181Tabela_PaiCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "TABELA_PAICOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A181Tabela_PaiCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_TABELA_MELHORACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A746Tabela_MelhoraCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "TABELA_MELHORACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A746Tabela_MelhoraCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_TABELA_ATIVO", GetSecureSignedToken( "", A174Tabela_Ativo));
         GxWebStd.gx_hidden_field( context, "TABELA_ATIVO", StringUtil.BoolToStr( A174Tabela_Ativo));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( dynavSistema_areatrabalhocod.ItemCount > 0 )
         {
            AV61Sistema_AreaTrabalhoCod = (int)(NumberUtil.Val( dynavSistema_areatrabalhocod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV61Sistema_AreaTrabalhoCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61Sistema_AreaTrabalhoCod), 6, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( dynavTabela_modulocod1.ItemCount > 0 )
         {
            AV62Tabela_ModuloCod1 = (int)(NumberUtil.Val( dynavTabela_modulocod1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV62Tabela_ModuloCod1), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62Tabela_ModuloCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62Tabela_ModuloCod1), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF462( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV131Pgmname = "WWTabela";
         context.Gx_err = 0;
      }

      protected void RF462( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 62;
         /* Execute user event: E24462 */
         E24462 ();
         nGXsfl_62_idx = 1;
         sGXsfl_62_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_62_idx), 4, 0)), 4, "0");
         SubsflControlProps_622( ) ;
         nGXsfl_62_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_622( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(2, new Object[]{ new Object[]{
                                                 AV109WWTabelaDS_1_Sistema_areatrabalhocod ,
                                                 AV110WWTabelaDS_2_Dynamicfiltersselector1 ,
                                                 AV111WWTabelaDS_3_Dynamicfiltersoperator1 ,
                                                 AV112WWTabelaDS_4_Tabela_nome1 ,
                                                 AV113WWTabelaDS_5_Tabela_modulodes1 ,
                                                 AV114WWTabelaDS_6_Tabela_modulocod1 ,
                                                 AV116WWTabelaDS_8_Tftabela_nome_sel ,
                                                 AV115WWTabelaDS_7_Tftabela_nome ,
                                                 AV118WWTabelaDS_10_Tftabela_descricao_sel ,
                                                 AV117WWTabelaDS_9_Tftabela_descricao ,
                                                 AV120WWTabelaDS_12_Tftabela_sistemades_sel ,
                                                 AV119WWTabelaDS_11_Tftabela_sistemades ,
                                                 AV122WWTabelaDS_14_Tftabela_modulodes_sel ,
                                                 AV121WWTabelaDS_13_Tftabela_modulodes ,
                                                 AV124WWTabelaDS_16_Tftabela_painom_sel ,
                                                 AV123WWTabelaDS_15_Tftabela_painom ,
                                                 AV125WWTabelaDS_17_Tftabela_melhoracod ,
                                                 AV126WWTabelaDS_18_Tftabela_melhoracod_to ,
                                                 AV127WWTabelaDS_19_Tftabela_ativo_sel ,
                                                 A135Sistema_AreaTrabalhoCod ,
                                                 A173Tabela_Nome ,
                                                 A189Tabela_ModuloDes ,
                                                 A188Tabela_ModuloCod ,
                                                 A175Tabela_Descricao ,
                                                 A191Tabela_SistemaDes ,
                                                 A182Tabela_PaiNom ,
                                                 A746Tabela_MelhoraCod ,
                                                 A174Tabela_Ativo ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.INT,
                                                 TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV112WWTabelaDS_4_Tabela_nome1 = StringUtil.PadR( StringUtil.RTrim( AV112WWTabelaDS_4_Tabela_nome1), 50, "%");
            lV112WWTabelaDS_4_Tabela_nome1 = StringUtil.PadR( StringUtil.RTrim( AV112WWTabelaDS_4_Tabela_nome1), 50, "%");
            lV113WWTabelaDS_5_Tabela_modulodes1 = StringUtil.PadR( StringUtil.RTrim( AV113WWTabelaDS_5_Tabela_modulodes1), 50, "%");
            lV113WWTabelaDS_5_Tabela_modulodes1 = StringUtil.PadR( StringUtil.RTrim( AV113WWTabelaDS_5_Tabela_modulodes1), 50, "%");
            lV115WWTabelaDS_7_Tftabela_nome = StringUtil.PadR( StringUtil.RTrim( AV115WWTabelaDS_7_Tftabela_nome), 50, "%");
            lV117WWTabelaDS_9_Tftabela_descricao = StringUtil.Concat( StringUtil.RTrim( AV117WWTabelaDS_9_Tftabela_descricao), "%", "");
            lV119WWTabelaDS_11_Tftabela_sistemades = StringUtil.Concat( StringUtil.RTrim( AV119WWTabelaDS_11_Tftabela_sistemades), "%", "");
            lV121WWTabelaDS_13_Tftabela_modulodes = StringUtil.PadR( StringUtil.RTrim( AV121WWTabelaDS_13_Tftabela_modulodes), 50, "%");
            lV123WWTabelaDS_15_Tftabela_painom = StringUtil.PadR( StringUtil.RTrim( AV123WWTabelaDS_15_Tftabela_painom), 50, "%");
            /* Using cursor H00464 */
            pr_default.execute(2, new Object[] {AV109WWTabelaDS_1_Sistema_areatrabalhocod, lV112WWTabelaDS_4_Tabela_nome1, lV112WWTabelaDS_4_Tabela_nome1, lV113WWTabelaDS_5_Tabela_modulodes1, lV113WWTabelaDS_5_Tabela_modulodes1, AV114WWTabelaDS_6_Tabela_modulocod1, lV115WWTabelaDS_7_Tftabela_nome, AV116WWTabelaDS_8_Tftabela_nome_sel, lV117WWTabelaDS_9_Tftabela_descricao, AV118WWTabelaDS_10_Tftabela_descricao_sel, lV119WWTabelaDS_11_Tftabela_sistemades, AV120WWTabelaDS_12_Tftabela_sistemades_sel, lV121WWTabelaDS_13_Tftabela_modulodes, AV122WWTabelaDS_14_Tftabela_modulodes_sel, lV123WWTabelaDS_15_Tftabela_painom, AV124WWTabelaDS_16_Tftabela_painom_sel, AV125WWTabelaDS_17_Tftabela_melhoracod, AV126WWTabelaDS_18_Tftabela_melhoracod_to, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_62_idx = 1;
            while ( ( (pr_default.getStatus(2) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A135Sistema_AreaTrabalhoCod = H00464_A135Sistema_AreaTrabalhoCod[0];
               n135Sistema_AreaTrabalhoCod = H00464_n135Sistema_AreaTrabalhoCod[0];
               A174Tabela_Ativo = H00464_A174Tabela_Ativo[0];
               A746Tabela_MelhoraCod = H00464_A746Tabela_MelhoraCod[0];
               n746Tabela_MelhoraCod = H00464_n746Tabela_MelhoraCod[0];
               A182Tabela_PaiNom = H00464_A182Tabela_PaiNom[0];
               n182Tabela_PaiNom = H00464_n182Tabela_PaiNom[0];
               A181Tabela_PaiCod = H00464_A181Tabela_PaiCod[0];
               n181Tabela_PaiCod = H00464_n181Tabela_PaiCod[0];
               A189Tabela_ModuloDes = H00464_A189Tabela_ModuloDes[0];
               n189Tabela_ModuloDes = H00464_n189Tabela_ModuloDes[0];
               A188Tabela_ModuloCod = H00464_A188Tabela_ModuloCod[0];
               n188Tabela_ModuloCod = H00464_n188Tabela_ModuloCod[0];
               A191Tabela_SistemaDes = H00464_A191Tabela_SistemaDes[0];
               n191Tabela_SistemaDes = H00464_n191Tabela_SistemaDes[0];
               A190Tabela_SistemaCod = H00464_A190Tabela_SistemaCod[0];
               A175Tabela_Descricao = H00464_A175Tabela_Descricao[0];
               n175Tabela_Descricao = H00464_n175Tabela_Descricao[0];
               A173Tabela_Nome = H00464_A173Tabela_Nome[0];
               A172Tabela_Codigo = H00464_A172Tabela_Codigo[0];
               A182Tabela_PaiNom = H00464_A182Tabela_PaiNom[0];
               n182Tabela_PaiNom = H00464_n182Tabela_PaiNom[0];
               A189Tabela_ModuloDes = H00464_A189Tabela_ModuloDes[0];
               n189Tabela_ModuloDes = H00464_n189Tabela_ModuloDes[0];
               A135Sistema_AreaTrabalhoCod = H00464_A135Sistema_AreaTrabalhoCod[0];
               n135Sistema_AreaTrabalhoCod = H00464_n135Sistema_AreaTrabalhoCod[0];
               A191Tabela_SistemaDes = H00464_A191Tabela_SistemaDes[0];
               n191Tabela_SistemaDes = H00464_n191Tabela_SistemaDes[0];
               /* Execute user event: E25462 */
               E25462 ();
               pr_default.readNext(2);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(2) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(2);
            wbEnd = 62;
            WB460( ) ;
         }
         nGXsfl_62_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV109WWTabelaDS_1_Sistema_areatrabalhocod = AV61Sistema_AreaTrabalhoCod;
         AV110WWTabelaDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV111WWTabelaDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV112WWTabelaDS_4_Tabela_nome1 = AV17Tabela_Nome1;
         AV113WWTabelaDS_5_Tabela_modulodes1 = AV96Tabela_ModuloDes1;
         AV114WWTabelaDS_6_Tabela_modulocod1 = AV62Tabela_ModuloCod1;
         AV115WWTabelaDS_7_Tftabela_nome = AV70TFTabela_Nome;
         AV116WWTabelaDS_8_Tftabela_nome_sel = AV71TFTabela_Nome_Sel;
         AV117WWTabelaDS_9_Tftabela_descricao = AV74TFTabela_Descricao;
         AV118WWTabelaDS_10_Tftabela_descricao_sel = AV75TFTabela_Descricao_Sel;
         AV119WWTabelaDS_11_Tftabela_sistemades = AV78TFTabela_SistemaDes;
         AV120WWTabelaDS_12_Tftabela_sistemades_sel = AV79TFTabela_SistemaDes_Sel;
         AV121WWTabelaDS_13_Tftabela_modulodes = AV82TFTabela_ModuloDes;
         AV122WWTabelaDS_14_Tftabela_modulodes_sel = AV83TFTabela_ModuloDes_Sel;
         AV123WWTabelaDS_15_Tftabela_painom = AV86TFTabela_PaiNom;
         AV124WWTabelaDS_16_Tftabela_painom_sel = AV87TFTabela_PaiNom_Sel;
         AV125WWTabelaDS_17_Tftabela_melhoracod = AV104TFTabela_MelhoraCod;
         AV126WWTabelaDS_18_Tftabela_melhoracod_to = AV105TFTabela_MelhoraCod_To;
         AV127WWTabelaDS_19_Tftabela_ativo_sel = AV90TFTabela_Ativo_Sel;
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              AV109WWTabelaDS_1_Sistema_areatrabalhocod ,
                                              AV110WWTabelaDS_2_Dynamicfiltersselector1 ,
                                              AV111WWTabelaDS_3_Dynamicfiltersoperator1 ,
                                              AV112WWTabelaDS_4_Tabela_nome1 ,
                                              AV113WWTabelaDS_5_Tabela_modulodes1 ,
                                              AV114WWTabelaDS_6_Tabela_modulocod1 ,
                                              AV116WWTabelaDS_8_Tftabela_nome_sel ,
                                              AV115WWTabelaDS_7_Tftabela_nome ,
                                              AV118WWTabelaDS_10_Tftabela_descricao_sel ,
                                              AV117WWTabelaDS_9_Tftabela_descricao ,
                                              AV120WWTabelaDS_12_Tftabela_sistemades_sel ,
                                              AV119WWTabelaDS_11_Tftabela_sistemades ,
                                              AV122WWTabelaDS_14_Tftabela_modulodes_sel ,
                                              AV121WWTabelaDS_13_Tftabela_modulodes ,
                                              AV124WWTabelaDS_16_Tftabela_painom_sel ,
                                              AV123WWTabelaDS_15_Tftabela_painom ,
                                              AV125WWTabelaDS_17_Tftabela_melhoracod ,
                                              AV126WWTabelaDS_18_Tftabela_melhoracod_to ,
                                              AV127WWTabelaDS_19_Tftabela_ativo_sel ,
                                              A135Sistema_AreaTrabalhoCod ,
                                              A173Tabela_Nome ,
                                              A189Tabela_ModuloDes ,
                                              A188Tabela_ModuloCod ,
                                              A175Tabela_Descricao ,
                                              A191Tabela_SistemaDes ,
                                              A182Tabela_PaiNom ,
                                              A746Tabela_MelhoraCod ,
                                              A174Tabela_Ativo ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV112WWTabelaDS_4_Tabela_nome1 = StringUtil.PadR( StringUtil.RTrim( AV112WWTabelaDS_4_Tabela_nome1), 50, "%");
         lV112WWTabelaDS_4_Tabela_nome1 = StringUtil.PadR( StringUtil.RTrim( AV112WWTabelaDS_4_Tabela_nome1), 50, "%");
         lV113WWTabelaDS_5_Tabela_modulodes1 = StringUtil.PadR( StringUtil.RTrim( AV113WWTabelaDS_5_Tabela_modulodes1), 50, "%");
         lV113WWTabelaDS_5_Tabela_modulodes1 = StringUtil.PadR( StringUtil.RTrim( AV113WWTabelaDS_5_Tabela_modulodes1), 50, "%");
         lV115WWTabelaDS_7_Tftabela_nome = StringUtil.PadR( StringUtil.RTrim( AV115WWTabelaDS_7_Tftabela_nome), 50, "%");
         lV117WWTabelaDS_9_Tftabela_descricao = StringUtil.Concat( StringUtil.RTrim( AV117WWTabelaDS_9_Tftabela_descricao), "%", "");
         lV119WWTabelaDS_11_Tftabela_sistemades = StringUtil.Concat( StringUtil.RTrim( AV119WWTabelaDS_11_Tftabela_sistemades), "%", "");
         lV121WWTabelaDS_13_Tftabela_modulodes = StringUtil.PadR( StringUtil.RTrim( AV121WWTabelaDS_13_Tftabela_modulodes), 50, "%");
         lV123WWTabelaDS_15_Tftabela_painom = StringUtil.PadR( StringUtil.RTrim( AV123WWTabelaDS_15_Tftabela_painom), 50, "%");
         /* Using cursor H00465 */
         pr_default.execute(3, new Object[] {AV109WWTabelaDS_1_Sistema_areatrabalhocod, lV112WWTabelaDS_4_Tabela_nome1, lV112WWTabelaDS_4_Tabela_nome1, lV113WWTabelaDS_5_Tabela_modulodes1, lV113WWTabelaDS_5_Tabela_modulodes1, AV114WWTabelaDS_6_Tabela_modulocod1, lV115WWTabelaDS_7_Tftabela_nome, AV116WWTabelaDS_8_Tftabela_nome_sel, lV117WWTabelaDS_9_Tftabela_descricao, AV118WWTabelaDS_10_Tftabela_descricao_sel, lV119WWTabelaDS_11_Tftabela_sistemades, AV120WWTabelaDS_12_Tftabela_sistemades_sel, lV121WWTabelaDS_13_Tftabela_modulodes, AV122WWTabelaDS_14_Tftabela_modulodes_sel, lV123WWTabelaDS_15_Tftabela_painom, AV124WWTabelaDS_16_Tftabela_painom_sel, AV125WWTabelaDS_17_Tftabela_melhoracod, AV126WWTabelaDS_18_Tftabela_melhoracod_to});
         GRID_nRecordCount = H00465_AGRID_nRecordCount[0];
         pr_default.close(3);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV109WWTabelaDS_1_Sistema_areatrabalhocod = AV61Sistema_AreaTrabalhoCod;
         AV110WWTabelaDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV111WWTabelaDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV112WWTabelaDS_4_Tabela_nome1 = AV17Tabela_Nome1;
         AV113WWTabelaDS_5_Tabela_modulodes1 = AV96Tabela_ModuloDes1;
         AV114WWTabelaDS_6_Tabela_modulocod1 = AV62Tabela_ModuloCod1;
         AV115WWTabelaDS_7_Tftabela_nome = AV70TFTabela_Nome;
         AV116WWTabelaDS_8_Tftabela_nome_sel = AV71TFTabela_Nome_Sel;
         AV117WWTabelaDS_9_Tftabela_descricao = AV74TFTabela_Descricao;
         AV118WWTabelaDS_10_Tftabela_descricao_sel = AV75TFTabela_Descricao_Sel;
         AV119WWTabelaDS_11_Tftabela_sistemades = AV78TFTabela_SistemaDes;
         AV120WWTabelaDS_12_Tftabela_sistemades_sel = AV79TFTabela_SistemaDes_Sel;
         AV121WWTabelaDS_13_Tftabela_modulodes = AV82TFTabela_ModuloDes;
         AV122WWTabelaDS_14_Tftabela_modulodes_sel = AV83TFTabela_ModuloDes_Sel;
         AV123WWTabelaDS_15_Tftabela_painom = AV86TFTabela_PaiNom;
         AV124WWTabelaDS_16_Tftabela_painom_sel = AV87TFTabela_PaiNom_Sel;
         AV125WWTabelaDS_17_Tftabela_melhoracod = AV104TFTabela_MelhoraCod;
         AV126WWTabelaDS_18_Tftabela_melhoracod_to = AV105TFTabela_MelhoraCod_To;
         AV127WWTabelaDS_19_Tftabela_ativo_sel = AV90TFTabela_Ativo_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV61Sistema_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Tabela_Nome1, AV96Tabela_ModuloDes1, AV62Tabela_ModuloCod1, AV70TFTabela_Nome, AV71TFTabela_Nome_Sel, AV74TFTabela_Descricao, AV75TFTabela_Descricao_Sel, AV78TFTabela_SistemaDes, AV79TFTabela_SistemaDes_Sel, AV82TFTabela_ModuloDes, AV83TFTabela_ModuloDes_Sel, AV86TFTabela_PaiNom, AV87TFTabela_PaiNom_Sel, AV104TFTabela_MelhoraCod, AV105TFTabela_MelhoraCod_To, AV90TFTabela_Ativo_Sel, AV97ManageFiltersExecutionStep, AV72ddo_Tabela_NomeTitleControlIdToReplace, AV76ddo_Tabela_DescricaoTitleControlIdToReplace, AV80ddo_Tabela_SistemaDesTitleControlIdToReplace, AV84ddo_Tabela_ModuloDesTitleControlIdToReplace, AV88ddo_Tabela_PaiNomTitleControlIdToReplace, AV106ddo_Tabela_MelhoraCodTitleControlIdToReplace, AV91ddo_Tabela_AtivoTitleControlIdToReplace, AV131Pgmname, AV10GridState, AV6WWPContext, A172Tabela_Codigo, A190Tabela_SistemaCod, AV65Tabela_ModuloCod, A188Tabela_ModuloCod, A181Tabela_PaiCod) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV109WWTabelaDS_1_Sistema_areatrabalhocod = AV61Sistema_AreaTrabalhoCod;
         AV110WWTabelaDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV111WWTabelaDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV112WWTabelaDS_4_Tabela_nome1 = AV17Tabela_Nome1;
         AV113WWTabelaDS_5_Tabela_modulodes1 = AV96Tabela_ModuloDes1;
         AV114WWTabelaDS_6_Tabela_modulocod1 = AV62Tabela_ModuloCod1;
         AV115WWTabelaDS_7_Tftabela_nome = AV70TFTabela_Nome;
         AV116WWTabelaDS_8_Tftabela_nome_sel = AV71TFTabela_Nome_Sel;
         AV117WWTabelaDS_9_Tftabela_descricao = AV74TFTabela_Descricao;
         AV118WWTabelaDS_10_Tftabela_descricao_sel = AV75TFTabela_Descricao_Sel;
         AV119WWTabelaDS_11_Tftabela_sistemades = AV78TFTabela_SistemaDes;
         AV120WWTabelaDS_12_Tftabela_sistemades_sel = AV79TFTabela_SistemaDes_Sel;
         AV121WWTabelaDS_13_Tftabela_modulodes = AV82TFTabela_ModuloDes;
         AV122WWTabelaDS_14_Tftabela_modulodes_sel = AV83TFTabela_ModuloDes_Sel;
         AV123WWTabelaDS_15_Tftabela_painom = AV86TFTabela_PaiNom;
         AV124WWTabelaDS_16_Tftabela_painom_sel = AV87TFTabela_PaiNom_Sel;
         AV125WWTabelaDS_17_Tftabela_melhoracod = AV104TFTabela_MelhoraCod;
         AV126WWTabelaDS_18_Tftabela_melhoracod_to = AV105TFTabela_MelhoraCod_To;
         AV127WWTabelaDS_19_Tftabela_ativo_sel = AV90TFTabela_Ativo_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV61Sistema_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Tabela_Nome1, AV96Tabela_ModuloDes1, AV62Tabela_ModuloCod1, AV70TFTabela_Nome, AV71TFTabela_Nome_Sel, AV74TFTabela_Descricao, AV75TFTabela_Descricao_Sel, AV78TFTabela_SistemaDes, AV79TFTabela_SistemaDes_Sel, AV82TFTabela_ModuloDes, AV83TFTabela_ModuloDes_Sel, AV86TFTabela_PaiNom, AV87TFTabela_PaiNom_Sel, AV104TFTabela_MelhoraCod, AV105TFTabela_MelhoraCod_To, AV90TFTabela_Ativo_Sel, AV97ManageFiltersExecutionStep, AV72ddo_Tabela_NomeTitleControlIdToReplace, AV76ddo_Tabela_DescricaoTitleControlIdToReplace, AV80ddo_Tabela_SistemaDesTitleControlIdToReplace, AV84ddo_Tabela_ModuloDesTitleControlIdToReplace, AV88ddo_Tabela_PaiNomTitleControlIdToReplace, AV106ddo_Tabela_MelhoraCodTitleControlIdToReplace, AV91ddo_Tabela_AtivoTitleControlIdToReplace, AV131Pgmname, AV10GridState, AV6WWPContext, A172Tabela_Codigo, A190Tabela_SistemaCod, AV65Tabela_ModuloCod, A188Tabela_ModuloCod, A181Tabela_PaiCod) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV109WWTabelaDS_1_Sistema_areatrabalhocod = AV61Sistema_AreaTrabalhoCod;
         AV110WWTabelaDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV111WWTabelaDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV112WWTabelaDS_4_Tabela_nome1 = AV17Tabela_Nome1;
         AV113WWTabelaDS_5_Tabela_modulodes1 = AV96Tabela_ModuloDes1;
         AV114WWTabelaDS_6_Tabela_modulocod1 = AV62Tabela_ModuloCod1;
         AV115WWTabelaDS_7_Tftabela_nome = AV70TFTabela_Nome;
         AV116WWTabelaDS_8_Tftabela_nome_sel = AV71TFTabela_Nome_Sel;
         AV117WWTabelaDS_9_Tftabela_descricao = AV74TFTabela_Descricao;
         AV118WWTabelaDS_10_Tftabela_descricao_sel = AV75TFTabela_Descricao_Sel;
         AV119WWTabelaDS_11_Tftabela_sistemades = AV78TFTabela_SistemaDes;
         AV120WWTabelaDS_12_Tftabela_sistemades_sel = AV79TFTabela_SistemaDes_Sel;
         AV121WWTabelaDS_13_Tftabela_modulodes = AV82TFTabela_ModuloDes;
         AV122WWTabelaDS_14_Tftabela_modulodes_sel = AV83TFTabela_ModuloDes_Sel;
         AV123WWTabelaDS_15_Tftabela_painom = AV86TFTabela_PaiNom;
         AV124WWTabelaDS_16_Tftabela_painom_sel = AV87TFTabela_PaiNom_Sel;
         AV125WWTabelaDS_17_Tftabela_melhoracod = AV104TFTabela_MelhoraCod;
         AV126WWTabelaDS_18_Tftabela_melhoracod_to = AV105TFTabela_MelhoraCod_To;
         AV127WWTabelaDS_19_Tftabela_ativo_sel = AV90TFTabela_Ativo_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV61Sistema_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Tabela_Nome1, AV96Tabela_ModuloDes1, AV62Tabela_ModuloCod1, AV70TFTabela_Nome, AV71TFTabela_Nome_Sel, AV74TFTabela_Descricao, AV75TFTabela_Descricao_Sel, AV78TFTabela_SistemaDes, AV79TFTabela_SistemaDes_Sel, AV82TFTabela_ModuloDes, AV83TFTabela_ModuloDes_Sel, AV86TFTabela_PaiNom, AV87TFTabela_PaiNom_Sel, AV104TFTabela_MelhoraCod, AV105TFTabela_MelhoraCod_To, AV90TFTabela_Ativo_Sel, AV97ManageFiltersExecutionStep, AV72ddo_Tabela_NomeTitleControlIdToReplace, AV76ddo_Tabela_DescricaoTitleControlIdToReplace, AV80ddo_Tabela_SistemaDesTitleControlIdToReplace, AV84ddo_Tabela_ModuloDesTitleControlIdToReplace, AV88ddo_Tabela_PaiNomTitleControlIdToReplace, AV106ddo_Tabela_MelhoraCodTitleControlIdToReplace, AV91ddo_Tabela_AtivoTitleControlIdToReplace, AV131Pgmname, AV10GridState, AV6WWPContext, A172Tabela_Codigo, A190Tabela_SistemaCod, AV65Tabela_ModuloCod, A188Tabela_ModuloCod, A181Tabela_PaiCod) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV109WWTabelaDS_1_Sistema_areatrabalhocod = AV61Sistema_AreaTrabalhoCod;
         AV110WWTabelaDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV111WWTabelaDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV112WWTabelaDS_4_Tabela_nome1 = AV17Tabela_Nome1;
         AV113WWTabelaDS_5_Tabela_modulodes1 = AV96Tabela_ModuloDes1;
         AV114WWTabelaDS_6_Tabela_modulocod1 = AV62Tabela_ModuloCod1;
         AV115WWTabelaDS_7_Tftabela_nome = AV70TFTabela_Nome;
         AV116WWTabelaDS_8_Tftabela_nome_sel = AV71TFTabela_Nome_Sel;
         AV117WWTabelaDS_9_Tftabela_descricao = AV74TFTabela_Descricao;
         AV118WWTabelaDS_10_Tftabela_descricao_sel = AV75TFTabela_Descricao_Sel;
         AV119WWTabelaDS_11_Tftabela_sistemades = AV78TFTabela_SistemaDes;
         AV120WWTabelaDS_12_Tftabela_sistemades_sel = AV79TFTabela_SistemaDes_Sel;
         AV121WWTabelaDS_13_Tftabela_modulodes = AV82TFTabela_ModuloDes;
         AV122WWTabelaDS_14_Tftabela_modulodes_sel = AV83TFTabela_ModuloDes_Sel;
         AV123WWTabelaDS_15_Tftabela_painom = AV86TFTabela_PaiNom;
         AV124WWTabelaDS_16_Tftabela_painom_sel = AV87TFTabela_PaiNom_Sel;
         AV125WWTabelaDS_17_Tftabela_melhoracod = AV104TFTabela_MelhoraCod;
         AV126WWTabelaDS_18_Tftabela_melhoracod_to = AV105TFTabela_MelhoraCod_To;
         AV127WWTabelaDS_19_Tftabela_ativo_sel = AV90TFTabela_Ativo_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV61Sistema_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Tabela_Nome1, AV96Tabela_ModuloDes1, AV62Tabela_ModuloCod1, AV70TFTabela_Nome, AV71TFTabela_Nome_Sel, AV74TFTabela_Descricao, AV75TFTabela_Descricao_Sel, AV78TFTabela_SistemaDes, AV79TFTabela_SistemaDes_Sel, AV82TFTabela_ModuloDes, AV83TFTabela_ModuloDes_Sel, AV86TFTabela_PaiNom, AV87TFTabela_PaiNom_Sel, AV104TFTabela_MelhoraCod, AV105TFTabela_MelhoraCod_To, AV90TFTabela_Ativo_Sel, AV97ManageFiltersExecutionStep, AV72ddo_Tabela_NomeTitleControlIdToReplace, AV76ddo_Tabela_DescricaoTitleControlIdToReplace, AV80ddo_Tabela_SistemaDesTitleControlIdToReplace, AV84ddo_Tabela_ModuloDesTitleControlIdToReplace, AV88ddo_Tabela_PaiNomTitleControlIdToReplace, AV106ddo_Tabela_MelhoraCodTitleControlIdToReplace, AV91ddo_Tabela_AtivoTitleControlIdToReplace, AV131Pgmname, AV10GridState, AV6WWPContext, A172Tabela_Codigo, A190Tabela_SistemaCod, AV65Tabela_ModuloCod, A188Tabela_ModuloCod, A181Tabela_PaiCod) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV109WWTabelaDS_1_Sistema_areatrabalhocod = AV61Sistema_AreaTrabalhoCod;
         AV110WWTabelaDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV111WWTabelaDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV112WWTabelaDS_4_Tabela_nome1 = AV17Tabela_Nome1;
         AV113WWTabelaDS_5_Tabela_modulodes1 = AV96Tabela_ModuloDes1;
         AV114WWTabelaDS_6_Tabela_modulocod1 = AV62Tabela_ModuloCod1;
         AV115WWTabelaDS_7_Tftabela_nome = AV70TFTabela_Nome;
         AV116WWTabelaDS_8_Tftabela_nome_sel = AV71TFTabela_Nome_Sel;
         AV117WWTabelaDS_9_Tftabela_descricao = AV74TFTabela_Descricao;
         AV118WWTabelaDS_10_Tftabela_descricao_sel = AV75TFTabela_Descricao_Sel;
         AV119WWTabelaDS_11_Tftabela_sistemades = AV78TFTabela_SistemaDes;
         AV120WWTabelaDS_12_Tftabela_sistemades_sel = AV79TFTabela_SistemaDes_Sel;
         AV121WWTabelaDS_13_Tftabela_modulodes = AV82TFTabela_ModuloDes;
         AV122WWTabelaDS_14_Tftabela_modulodes_sel = AV83TFTabela_ModuloDes_Sel;
         AV123WWTabelaDS_15_Tftabela_painom = AV86TFTabela_PaiNom;
         AV124WWTabelaDS_16_Tftabela_painom_sel = AV87TFTabela_PaiNom_Sel;
         AV125WWTabelaDS_17_Tftabela_melhoracod = AV104TFTabela_MelhoraCod;
         AV126WWTabelaDS_18_Tftabela_melhoracod_to = AV105TFTabela_MelhoraCod_To;
         AV127WWTabelaDS_19_Tftabela_ativo_sel = AV90TFTabela_Ativo_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV61Sistema_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Tabela_Nome1, AV96Tabela_ModuloDes1, AV62Tabela_ModuloCod1, AV70TFTabela_Nome, AV71TFTabela_Nome_Sel, AV74TFTabela_Descricao, AV75TFTabela_Descricao_Sel, AV78TFTabela_SistemaDes, AV79TFTabela_SistemaDes_Sel, AV82TFTabela_ModuloDes, AV83TFTabela_ModuloDes_Sel, AV86TFTabela_PaiNom, AV87TFTabela_PaiNom_Sel, AV104TFTabela_MelhoraCod, AV105TFTabela_MelhoraCod_To, AV90TFTabela_Ativo_Sel, AV97ManageFiltersExecutionStep, AV72ddo_Tabela_NomeTitleControlIdToReplace, AV76ddo_Tabela_DescricaoTitleControlIdToReplace, AV80ddo_Tabela_SistemaDesTitleControlIdToReplace, AV84ddo_Tabela_ModuloDesTitleControlIdToReplace, AV88ddo_Tabela_PaiNomTitleControlIdToReplace, AV106ddo_Tabela_MelhoraCodTitleControlIdToReplace, AV91ddo_Tabela_AtivoTitleControlIdToReplace, AV131Pgmname, AV10GridState, AV6WWPContext, A172Tabela_Codigo, A190Tabela_SistemaCod, AV65Tabela_ModuloCod, A188Tabela_ModuloCod, A181Tabela_PaiCod) ;
         }
         return (int)(0) ;
      }

      protected void STRUP460( )
      {
         /* Before Start, stand alone formulas. */
         AV131Pgmname = "WWTabela";
         context.Gx_err = 0;
         GXVvSISTEMA_AREATRABALHOCOD_html462( ) ;
         GXVvTABELA_MODULOCOD1_html462( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E23462 */
         E23462 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vMANAGEFILTERSDATA"), AV101ManageFiltersData);
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV92DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vTABELA_NOMETITLEFILTERDATA"), AV69Tabela_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vTABELA_DESCRICAOTITLEFILTERDATA"), AV73Tabela_DescricaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vTABELA_SISTEMADESTITLEFILTERDATA"), AV77Tabela_SistemaDesTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vTABELA_MODULODESTITLEFILTERDATA"), AV81Tabela_ModuloDesTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vTABELA_PAINOMTITLEFILTERDATA"), AV85Tabela_PaiNomTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vTABELA_MELHORACODTITLEFILTERDATA"), AV103Tabela_MelhoraCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vTABELA_ATIVOTITLEFILTERDATA"), AV89Tabela_AtivoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            dynavSistema_areatrabalhocod.Name = dynavSistema_areatrabalhocod_Internalname;
            dynavSistema_areatrabalhocod.CurrentValue = cgiGet( dynavSistema_areatrabalhocod_Internalname);
            AV61Sistema_AreaTrabalhoCod = (int)(NumberUtil.Val( cgiGet( dynavSistema_areatrabalhocod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61Sistema_AreaTrabalhoCod), 6, 0)));
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV17Tabela_Nome1 = StringUtil.Upper( cgiGet( edtavTabela_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Tabela_Nome1", AV17Tabela_Nome1);
            AV96Tabela_ModuloDes1 = StringUtil.Upper( cgiGet( edtavTabela_modulodes1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV96Tabela_ModuloDes1", AV96Tabela_ModuloDes1);
            dynavTabela_modulocod1.Name = dynavTabela_modulocod1_Internalname;
            dynavTabela_modulocod1.CurrentValue = cgiGet( dynavTabela_modulocod1_Internalname);
            AV62Tabela_ModuloCod1 = (int)(NumberUtil.Val( cgiGet( dynavTabela_modulocod1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62Tabela_ModuloCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62Tabela_ModuloCod1), 6, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vMANAGEFILTERSEXECUTIONSTEP");
               GX_FocusControl = edtavManagefiltersexecutionstep_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV97ManageFiltersExecutionStep = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV97ManageFiltersExecutionStep), 1, 0));
            }
            else
            {
               AV97ManageFiltersExecutionStep = (short)(context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV97ManageFiltersExecutionStep), 1, 0));
            }
            AV70TFTabela_Nome = StringUtil.Upper( cgiGet( edtavTftabela_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFTabela_Nome", AV70TFTabela_Nome);
            AV71TFTabela_Nome_Sel = StringUtil.Upper( cgiGet( edtavTftabela_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFTabela_Nome_Sel", AV71TFTabela_Nome_Sel);
            AV74TFTabela_Descricao = cgiGet( edtavTftabela_descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFTabela_Descricao", AV74TFTabela_Descricao);
            AV75TFTabela_Descricao_Sel = cgiGet( edtavTftabela_descricao_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFTabela_Descricao_Sel", AV75TFTabela_Descricao_Sel);
            AV78TFTabela_SistemaDes = StringUtil.Upper( cgiGet( edtavTftabela_sistemades_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFTabela_SistemaDes", AV78TFTabela_SistemaDes);
            AV79TFTabela_SistemaDes_Sel = StringUtil.Upper( cgiGet( edtavTftabela_sistemades_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFTabela_SistemaDes_Sel", AV79TFTabela_SistemaDes_Sel);
            AV82TFTabela_ModuloDes = StringUtil.Upper( cgiGet( edtavTftabela_modulodes_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFTabela_ModuloDes", AV82TFTabela_ModuloDes);
            AV83TFTabela_ModuloDes_Sel = StringUtil.Upper( cgiGet( edtavTftabela_modulodes_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFTabela_ModuloDes_Sel", AV83TFTabela_ModuloDes_Sel);
            AV86TFTabela_PaiNom = StringUtil.Upper( cgiGet( edtavTftabela_painom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86TFTabela_PaiNom", AV86TFTabela_PaiNom);
            AV87TFTabela_PaiNom_Sel = StringUtil.Upper( cgiGet( edtavTftabela_painom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87TFTabela_PaiNom_Sel", AV87TFTabela_PaiNom_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTftabela_melhoracod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTftabela_melhoracod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFTABELA_MELHORACOD");
               GX_FocusControl = edtavTftabela_melhoracod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV104TFTabela_MelhoraCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV104TFTabela_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV104TFTabela_MelhoraCod), 6, 0)));
            }
            else
            {
               AV104TFTabela_MelhoraCod = (int)(context.localUtil.CToN( cgiGet( edtavTftabela_melhoracod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV104TFTabela_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV104TFTabela_MelhoraCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTftabela_melhoracod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTftabela_melhoracod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFTABELA_MELHORACOD_TO");
               GX_FocusControl = edtavTftabela_melhoracod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV105TFTabela_MelhoraCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105TFTabela_MelhoraCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105TFTabela_MelhoraCod_To), 6, 0)));
            }
            else
            {
               AV105TFTabela_MelhoraCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTftabela_melhoracod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105TFTabela_MelhoraCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105TFTabela_MelhoraCod_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTftabela_ativo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTftabela_ativo_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFTABELA_ATIVO_SEL");
               GX_FocusControl = edtavTftabela_ativo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV90TFTabela_Ativo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90TFTabela_Ativo_Sel", StringUtil.Str( (decimal)(AV90TFTabela_Ativo_Sel), 1, 0));
            }
            else
            {
               AV90TFTabela_Ativo_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTftabela_ativo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90TFTabela_Ativo_Sel", StringUtil.Str( (decimal)(AV90TFTabela_Ativo_Sel), 1, 0));
            }
            AV72ddo_Tabela_NomeTitleControlIdToReplace = cgiGet( edtavDdo_tabela_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_Tabela_NomeTitleControlIdToReplace", AV72ddo_Tabela_NomeTitleControlIdToReplace);
            AV76ddo_Tabela_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_tabela_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76ddo_Tabela_DescricaoTitleControlIdToReplace", AV76ddo_Tabela_DescricaoTitleControlIdToReplace);
            AV80ddo_Tabela_SistemaDesTitleControlIdToReplace = cgiGet( edtavDdo_tabela_sistemadestitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80ddo_Tabela_SistemaDesTitleControlIdToReplace", AV80ddo_Tabela_SistemaDesTitleControlIdToReplace);
            AV84ddo_Tabela_ModuloDesTitleControlIdToReplace = cgiGet( edtavDdo_tabela_modulodestitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84ddo_Tabela_ModuloDesTitleControlIdToReplace", AV84ddo_Tabela_ModuloDesTitleControlIdToReplace);
            AV88ddo_Tabela_PaiNomTitleControlIdToReplace = cgiGet( edtavDdo_tabela_painomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88ddo_Tabela_PaiNomTitleControlIdToReplace", AV88ddo_Tabela_PaiNomTitleControlIdToReplace);
            AV106ddo_Tabela_MelhoraCodTitleControlIdToReplace = cgiGet( edtavDdo_tabela_melhoracodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV106ddo_Tabela_MelhoraCodTitleControlIdToReplace", AV106ddo_Tabela_MelhoraCodTitleControlIdToReplace);
            AV91ddo_Tabela_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_tabela_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91ddo_Tabela_AtivoTitleControlIdToReplace", AV91ddo_Tabela_AtivoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_62 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_62"), ",", "."));
            AV94GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV95GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Ddo_managefilters_Icon = cgiGet( "DDO_MANAGEFILTERS_Icon");
            Ddo_managefilters_Caption = cgiGet( "DDO_MANAGEFILTERS_Caption");
            Ddo_managefilters_Tooltip = cgiGet( "DDO_MANAGEFILTERS_Tooltip");
            Ddo_managefilters_Cls = cgiGet( "DDO_MANAGEFILTERS_Cls");
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_tabela_nome_Caption = cgiGet( "DDO_TABELA_NOME_Caption");
            Ddo_tabela_nome_Tooltip = cgiGet( "DDO_TABELA_NOME_Tooltip");
            Ddo_tabela_nome_Cls = cgiGet( "DDO_TABELA_NOME_Cls");
            Ddo_tabela_nome_Filteredtext_set = cgiGet( "DDO_TABELA_NOME_Filteredtext_set");
            Ddo_tabela_nome_Selectedvalue_set = cgiGet( "DDO_TABELA_NOME_Selectedvalue_set");
            Ddo_tabela_nome_Dropdownoptionstype = cgiGet( "DDO_TABELA_NOME_Dropdownoptionstype");
            Ddo_tabela_nome_Titlecontrolidtoreplace = cgiGet( "DDO_TABELA_NOME_Titlecontrolidtoreplace");
            Ddo_tabela_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_TABELA_NOME_Includesortasc"));
            Ddo_tabela_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_TABELA_NOME_Includesortdsc"));
            Ddo_tabela_nome_Sortedstatus = cgiGet( "DDO_TABELA_NOME_Sortedstatus");
            Ddo_tabela_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_TABELA_NOME_Includefilter"));
            Ddo_tabela_nome_Filtertype = cgiGet( "DDO_TABELA_NOME_Filtertype");
            Ddo_tabela_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_TABELA_NOME_Filterisrange"));
            Ddo_tabela_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_TABELA_NOME_Includedatalist"));
            Ddo_tabela_nome_Datalisttype = cgiGet( "DDO_TABELA_NOME_Datalisttype");
            Ddo_tabela_nome_Datalistproc = cgiGet( "DDO_TABELA_NOME_Datalistproc");
            Ddo_tabela_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_TABELA_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_tabela_nome_Sortasc = cgiGet( "DDO_TABELA_NOME_Sortasc");
            Ddo_tabela_nome_Sortdsc = cgiGet( "DDO_TABELA_NOME_Sortdsc");
            Ddo_tabela_nome_Loadingdata = cgiGet( "DDO_TABELA_NOME_Loadingdata");
            Ddo_tabela_nome_Cleanfilter = cgiGet( "DDO_TABELA_NOME_Cleanfilter");
            Ddo_tabela_nome_Noresultsfound = cgiGet( "DDO_TABELA_NOME_Noresultsfound");
            Ddo_tabela_nome_Searchbuttontext = cgiGet( "DDO_TABELA_NOME_Searchbuttontext");
            Ddo_tabela_descricao_Caption = cgiGet( "DDO_TABELA_DESCRICAO_Caption");
            Ddo_tabela_descricao_Tooltip = cgiGet( "DDO_TABELA_DESCRICAO_Tooltip");
            Ddo_tabela_descricao_Cls = cgiGet( "DDO_TABELA_DESCRICAO_Cls");
            Ddo_tabela_descricao_Filteredtext_set = cgiGet( "DDO_TABELA_DESCRICAO_Filteredtext_set");
            Ddo_tabela_descricao_Selectedvalue_set = cgiGet( "DDO_TABELA_DESCRICAO_Selectedvalue_set");
            Ddo_tabela_descricao_Dropdownoptionstype = cgiGet( "DDO_TABELA_DESCRICAO_Dropdownoptionstype");
            Ddo_tabela_descricao_Titlecontrolidtoreplace = cgiGet( "DDO_TABELA_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_tabela_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_TABELA_DESCRICAO_Includesortasc"));
            Ddo_tabela_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_TABELA_DESCRICAO_Includesortdsc"));
            Ddo_tabela_descricao_Sortedstatus = cgiGet( "DDO_TABELA_DESCRICAO_Sortedstatus");
            Ddo_tabela_descricao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_TABELA_DESCRICAO_Includefilter"));
            Ddo_tabela_descricao_Filtertype = cgiGet( "DDO_TABELA_DESCRICAO_Filtertype");
            Ddo_tabela_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_TABELA_DESCRICAO_Filterisrange"));
            Ddo_tabela_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_TABELA_DESCRICAO_Includedatalist"));
            Ddo_tabela_descricao_Datalisttype = cgiGet( "DDO_TABELA_DESCRICAO_Datalisttype");
            Ddo_tabela_descricao_Datalistproc = cgiGet( "DDO_TABELA_DESCRICAO_Datalistproc");
            Ddo_tabela_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_TABELA_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_tabela_descricao_Sortasc = cgiGet( "DDO_TABELA_DESCRICAO_Sortasc");
            Ddo_tabela_descricao_Sortdsc = cgiGet( "DDO_TABELA_DESCRICAO_Sortdsc");
            Ddo_tabela_descricao_Loadingdata = cgiGet( "DDO_TABELA_DESCRICAO_Loadingdata");
            Ddo_tabela_descricao_Cleanfilter = cgiGet( "DDO_TABELA_DESCRICAO_Cleanfilter");
            Ddo_tabela_descricao_Noresultsfound = cgiGet( "DDO_TABELA_DESCRICAO_Noresultsfound");
            Ddo_tabela_descricao_Searchbuttontext = cgiGet( "DDO_TABELA_DESCRICAO_Searchbuttontext");
            Ddo_tabela_sistemades_Caption = cgiGet( "DDO_TABELA_SISTEMADES_Caption");
            Ddo_tabela_sistemades_Tooltip = cgiGet( "DDO_TABELA_SISTEMADES_Tooltip");
            Ddo_tabela_sistemades_Cls = cgiGet( "DDO_TABELA_SISTEMADES_Cls");
            Ddo_tabela_sistemades_Filteredtext_set = cgiGet( "DDO_TABELA_SISTEMADES_Filteredtext_set");
            Ddo_tabela_sistemades_Selectedvalue_set = cgiGet( "DDO_TABELA_SISTEMADES_Selectedvalue_set");
            Ddo_tabela_sistemades_Dropdownoptionstype = cgiGet( "DDO_TABELA_SISTEMADES_Dropdownoptionstype");
            Ddo_tabela_sistemades_Titlecontrolidtoreplace = cgiGet( "DDO_TABELA_SISTEMADES_Titlecontrolidtoreplace");
            Ddo_tabela_sistemades_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_TABELA_SISTEMADES_Includesortasc"));
            Ddo_tabela_sistemades_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_TABELA_SISTEMADES_Includesortdsc"));
            Ddo_tabela_sistemades_Sortedstatus = cgiGet( "DDO_TABELA_SISTEMADES_Sortedstatus");
            Ddo_tabela_sistemades_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_TABELA_SISTEMADES_Includefilter"));
            Ddo_tabela_sistemades_Filtertype = cgiGet( "DDO_TABELA_SISTEMADES_Filtertype");
            Ddo_tabela_sistemades_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_TABELA_SISTEMADES_Filterisrange"));
            Ddo_tabela_sistemades_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_TABELA_SISTEMADES_Includedatalist"));
            Ddo_tabela_sistemades_Datalisttype = cgiGet( "DDO_TABELA_SISTEMADES_Datalisttype");
            Ddo_tabela_sistemades_Datalistproc = cgiGet( "DDO_TABELA_SISTEMADES_Datalistproc");
            Ddo_tabela_sistemades_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_TABELA_SISTEMADES_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_tabela_sistemades_Sortasc = cgiGet( "DDO_TABELA_SISTEMADES_Sortasc");
            Ddo_tabela_sistemades_Sortdsc = cgiGet( "DDO_TABELA_SISTEMADES_Sortdsc");
            Ddo_tabela_sistemades_Loadingdata = cgiGet( "DDO_TABELA_SISTEMADES_Loadingdata");
            Ddo_tabela_sistemades_Cleanfilter = cgiGet( "DDO_TABELA_SISTEMADES_Cleanfilter");
            Ddo_tabela_sistemades_Noresultsfound = cgiGet( "DDO_TABELA_SISTEMADES_Noresultsfound");
            Ddo_tabela_sistemades_Searchbuttontext = cgiGet( "DDO_TABELA_SISTEMADES_Searchbuttontext");
            Ddo_tabela_modulodes_Caption = cgiGet( "DDO_TABELA_MODULODES_Caption");
            Ddo_tabela_modulodes_Tooltip = cgiGet( "DDO_TABELA_MODULODES_Tooltip");
            Ddo_tabela_modulodes_Cls = cgiGet( "DDO_TABELA_MODULODES_Cls");
            Ddo_tabela_modulodes_Filteredtext_set = cgiGet( "DDO_TABELA_MODULODES_Filteredtext_set");
            Ddo_tabela_modulodes_Selectedvalue_set = cgiGet( "DDO_TABELA_MODULODES_Selectedvalue_set");
            Ddo_tabela_modulodes_Dropdownoptionstype = cgiGet( "DDO_TABELA_MODULODES_Dropdownoptionstype");
            Ddo_tabela_modulodes_Titlecontrolidtoreplace = cgiGet( "DDO_TABELA_MODULODES_Titlecontrolidtoreplace");
            Ddo_tabela_modulodes_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_TABELA_MODULODES_Includesortasc"));
            Ddo_tabela_modulodes_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_TABELA_MODULODES_Includesortdsc"));
            Ddo_tabela_modulodes_Sortedstatus = cgiGet( "DDO_TABELA_MODULODES_Sortedstatus");
            Ddo_tabela_modulodes_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_TABELA_MODULODES_Includefilter"));
            Ddo_tabela_modulodes_Filtertype = cgiGet( "DDO_TABELA_MODULODES_Filtertype");
            Ddo_tabela_modulodes_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_TABELA_MODULODES_Filterisrange"));
            Ddo_tabela_modulodes_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_TABELA_MODULODES_Includedatalist"));
            Ddo_tabela_modulodes_Datalisttype = cgiGet( "DDO_TABELA_MODULODES_Datalisttype");
            Ddo_tabela_modulodes_Datalistproc = cgiGet( "DDO_TABELA_MODULODES_Datalistproc");
            Ddo_tabela_modulodes_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_TABELA_MODULODES_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_tabela_modulodes_Sortasc = cgiGet( "DDO_TABELA_MODULODES_Sortasc");
            Ddo_tabela_modulodes_Sortdsc = cgiGet( "DDO_TABELA_MODULODES_Sortdsc");
            Ddo_tabela_modulodes_Loadingdata = cgiGet( "DDO_TABELA_MODULODES_Loadingdata");
            Ddo_tabela_modulodes_Cleanfilter = cgiGet( "DDO_TABELA_MODULODES_Cleanfilter");
            Ddo_tabela_modulodes_Noresultsfound = cgiGet( "DDO_TABELA_MODULODES_Noresultsfound");
            Ddo_tabela_modulodes_Searchbuttontext = cgiGet( "DDO_TABELA_MODULODES_Searchbuttontext");
            Ddo_tabela_painom_Caption = cgiGet( "DDO_TABELA_PAINOM_Caption");
            Ddo_tabela_painom_Tooltip = cgiGet( "DDO_TABELA_PAINOM_Tooltip");
            Ddo_tabela_painom_Cls = cgiGet( "DDO_TABELA_PAINOM_Cls");
            Ddo_tabela_painom_Filteredtext_set = cgiGet( "DDO_TABELA_PAINOM_Filteredtext_set");
            Ddo_tabela_painom_Selectedvalue_set = cgiGet( "DDO_TABELA_PAINOM_Selectedvalue_set");
            Ddo_tabela_painom_Dropdownoptionstype = cgiGet( "DDO_TABELA_PAINOM_Dropdownoptionstype");
            Ddo_tabela_painom_Titlecontrolidtoreplace = cgiGet( "DDO_TABELA_PAINOM_Titlecontrolidtoreplace");
            Ddo_tabela_painom_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_TABELA_PAINOM_Includesortasc"));
            Ddo_tabela_painom_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_TABELA_PAINOM_Includesortdsc"));
            Ddo_tabela_painom_Sortedstatus = cgiGet( "DDO_TABELA_PAINOM_Sortedstatus");
            Ddo_tabela_painom_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_TABELA_PAINOM_Includefilter"));
            Ddo_tabela_painom_Filtertype = cgiGet( "DDO_TABELA_PAINOM_Filtertype");
            Ddo_tabela_painom_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_TABELA_PAINOM_Filterisrange"));
            Ddo_tabela_painom_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_TABELA_PAINOM_Includedatalist"));
            Ddo_tabela_painom_Datalisttype = cgiGet( "DDO_TABELA_PAINOM_Datalisttype");
            Ddo_tabela_painom_Datalistproc = cgiGet( "DDO_TABELA_PAINOM_Datalistproc");
            Ddo_tabela_painom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_TABELA_PAINOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_tabela_painom_Sortasc = cgiGet( "DDO_TABELA_PAINOM_Sortasc");
            Ddo_tabela_painom_Sortdsc = cgiGet( "DDO_TABELA_PAINOM_Sortdsc");
            Ddo_tabela_painom_Loadingdata = cgiGet( "DDO_TABELA_PAINOM_Loadingdata");
            Ddo_tabela_painom_Cleanfilter = cgiGet( "DDO_TABELA_PAINOM_Cleanfilter");
            Ddo_tabela_painom_Noresultsfound = cgiGet( "DDO_TABELA_PAINOM_Noresultsfound");
            Ddo_tabela_painom_Searchbuttontext = cgiGet( "DDO_TABELA_PAINOM_Searchbuttontext");
            Ddo_tabela_melhoracod_Caption = cgiGet( "DDO_TABELA_MELHORACOD_Caption");
            Ddo_tabela_melhoracod_Tooltip = cgiGet( "DDO_TABELA_MELHORACOD_Tooltip");
            Ddo_tabela_melhoracod_Cls = cgiGet( "DDO_TABELA_MELHORACOD_Cls");
            Ddo_tabela_melhoracod_Filteredtext_set = cgiGet( "DDO_TABELA_MELHORACOD_Filteredtext_set");
            Ddo_tabela_melhoracod_Filteredtextto_set = cgiGet( "DDO_TABELA_MELHORACOD_Filteredtextto_set");
            Ddo_tabela_melhoracod_Dropdownoptionstype = cgiGet( "DDO_TABELA_MELHORACOD_Dropdownoptionstype");
            Ddo_tabela_melhoracod_Titlecontrolidtoreplace = cgiGet( "DDO_TABELA_MELHORACOD_Titlecontrolidtoreplace");
            Ddo_tabela_melhoracod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_TABELA_MELHORACOD_Includesortasc"));
            Ddo_tabela_melhoracod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_TABELA_MELHORACOD_Includesortdsc"));
            Ddo_tabela_melhoracod_Sortedstatus = cgiGet( "DDO_TABELA_MELHORACOD_Sortedstatus");
            Ddo_tabela_melhoracod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_TABELA_MELHORACOD_Includefilter"));
            Ddo_tabela_melhoracod_Filtertype = cgiGet( "DDO_TABELA_MELHORACOD_Filtertype");
            Ddo_tabela_melhoracod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_TABELA_MELHORACOD_Filterisrange"));
            Ddo_tabela_melhoracod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_TABELA_MELHORACOD_Includedatalist"));
            Ddo_tabela_melhoracod_Sortasc = cgiGet( "DDO_TABELA_MELHORACOD_Sortasc");
            Ddo_tabela_melhoracod_Sortdsc = cgiGet( "DDO_TABELA_MELHORACOD_Sortdsc");
            Ddo_tabela_melhoracod_Cleanfilter = cgiGet( "DDO_TABELA_MELHORACOD_Cleanfilter");
            Ddo_tabela_melhoracod_Rangefilterfrom = cgiGet( "DDO_TABELA_MELHORACOD_Rangefilterfrom");
            Ddo_tabela_melhoracod_Rangefilterto = cgiGet( "DDO_TABELA_MELHORACOD_Rangefilterto");
            Ddo_tabela_melhoracod_Searchbuttontext = cgiGet( "DDO_TABELA_MELHORACOD_Searchbuttontext");
            Ddo_tabela_ativo_Caption = cgiGet( "DDO_TABELA_ATIVO_Caption");
            Ddo_tabela_ativo_Tooltip = cgiGet( "DDO_TABELA_ATIVO_Tooltip");
            Ddo_tabela_ativo_Cls = cgiGet( "DDO_TABELA_ATIVO_Cls");
            Ddo_tabela_ativo_Selectedvalue_set = cgiGet( "DDO_TABELA_ATIVO_Selectedvalue_set");
            Ddo_tabela_ativo_Dropdownoptionstype = cgiGet( "DDO_TABELA_ATIVO_Dropdownoptionstype");
            Ddo_tabela_ativo_Titlecontrolidtoreplace = cgiGet( "DDO_TABELA_ATIVO_Titlecontrolidtoreplace");
            Ddo_tabela_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_TABELA_ATIVO_Includesortasc"));
            Ddo_tabela_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_TABELA_ATIVO_Includesortdsc"));
            Ddo_tabela_ativo_Sortedstatus = cgiGet( "DDO_TABELA_ATIVO_Sortedstatus");
            Ddo_tabela_ativo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_TABELA_ATIVO_Includefilter"));
            Ddo_tabela_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_TABELA_ATIVO_Includedatalist"));
            Ddo_tabela_ativo_Datalisttype = cgiGet( "DDO_TABELA_ATIVO_Datalisttype");
            Ddo_tabela_ativo_Datalistfixedvalues = cgiGet( "DDO_TABELA_ATIVO_Datalistfixedvalues");
            Ddo_tabela_ativo_Sortasc = cgiGet( "DDO_TABELA_ATIVO_Sortasc");
            Ddo_tabela_ativo_Sortdsc = cgiGet( "DDO_TABELA_ATIVO_Sortdsc");
            Ddo_tabela_ativo_Cleanfilter = cgiGet( "DDO_TABELA_ATIVO_Cleanfilter");
            Ddo_tabela_ativo_Searchbuttontext = cgiGet( "DDO_TABELA_ATIVO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_tabela_nome_Activeeventkey = cgiGet( "DDO_TABELA_NOME_Activeeventkey");
            Ddo_tabela_nome_Filteredtext_get = cgiGet( "DDO_TABELA_NOME_Filteredtext_get");
            Ddo_tabela_nome_Selectedvalue_get = cgiGet( "DDO_TABELA_NOME_Selectedvalue_get");
            Ddo_tabela_descricao_Activeeventkey = cgiGet( "DDO_TABELA_DESCRICAO_Activeeventkey");
            Ddo_tabela_descricao_Filteredtext_get = cgiGet( "DDO_TABELA_DESCRICAO_Filteredtext_get");
            Ddo_tabela_descricao_Selectedvalue_get = cgiGet( "DDO_TABELA_DESCRICAO_Selectedvalue_get");
            Ddo_tabela_sistemades_Activeeventkey = cgiGet( "DDO_TABELA_SISTEMADES_Activeeventkey");
            Ddo_tabela_sistemades_Filteredtext_get = cgiGet( "DDO_TABELA_SISTEMADES_Filteredtext_get");
            Ddo_tabela_sistemades_Selectedvalue_get = cgiGet( "DDO_TABELA_SISTEMADES_Selectedvalue_get");
            Ddo_tabela_modulodes_Activeeventkey = cgiGet( "DDO_TABELA_MODULODES_Activeeventkey");
            Ddo_tabela_modulodes_Filteredtext_get = cgiGet( "DDO_TABELA_MODULODES_Filteredtext_get");
            Ddo_tabela_modulodes_Selectedvalue_get = cgiGet( "DDO_TABELA_MODULODES_Selectedvalue_get");
            Ddo_tabela_painom_Activeeventkey = cgiGet( "DDO_TABELA_PAINOM_Activeeventkey");
            Ddo_tabela_painom_Filteredtext_get = cgiGet( "DDO_TABELA_PAINOM_Filteredtext_get");
            Ddo_tabela_painom_Selectedvalue_get = cgiGet( "DDO_TABELA_PAINOM_Selectedvalue_get");
            Ddo_tabela_melhoracod_Activeeventkey = cgiGet( "DDO_TABELA_MELHORACOD_Activeeventkey");
            Ddo_tabela_melhoracod_Filteredtext_get = cgiGet( "DDO_TABELA_MELHORACOD_Filteredtext_get");
            Ddo_tabela_melhoracod_Filteredtextto_get = cgiGet( "DDO_TABELA_MELHORACOD_Filteredtextto_get");
            Ddo_tabela_ativo_Activeeventkey = cgiGet( "DDO_TABELA_ATIVO_Activeeventkey");
            Ddo_tabela_ativo_Selectedvalue_get = cgiGet( "DDO_TABELA_ATIVO_Selectedvalue_get");
            Ddo_managefilters_Activeeventkey = cgiGet( "DDO_MANAGEFILTERS_Activeeventkey");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vSISTEMA_AREATRABALHOCOD"), ",", ".") != Convert.ToDecimal( AV61Sistema_AreaTrabalhoCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTABELA_NOME1"), AV17Tabela_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTABELA_MODULODES1"), AV96Tabela_ModuloDes1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTABELA_MODULOCOD1"), ",", ".") != Convert.ToDecimal( AV62Tabela_ModuloCod1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTABELA_NOME"), AV70TFTabela_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTABELA_NOME_SEL"), AV71TFTabela_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTABELA_DESCRICAO"), AV74TFTabela_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTABELA_DESCRICAO_SEL"), AV75TFTabela_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTABELA_SISTEMADES"), AV78TFTabela_SistemaDes) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTABELA_SISTEMADES_SEL"), AV79TFTabela_SistemaDes_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTABELA_MODULODES"), AV82TFTabela_ModuloDes) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTABELA_MODULODES_SEL"), AV83TFTabela_ModuloDes_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTABELA_PAINOM"), AV86TFTabela_PaiNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTABELA_PAINOM_SEL"), AV87TFTabela_PaiNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFTABELA_MELHORACOD"), ",", ".") != Convert.ToDecimal( AV104TFTabela_MelhoraCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFTABELA_MELHORACOD_TO"), ",", ".") != Convert.ToDecimal( AV105TFTabela_MelhoraCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFTABELA_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV90TFTabela_Ativo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E23462 */
         E23462 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E23462( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         if ( StringUtil.StrCmp(AV7HTTPRequest.Method, "GET") == 0 )
         {
            /* Execute user subroutine: 'LOADSAVEDFILTERS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            edtavManagefiltersexecutionstep_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavManagefiltersexecutionstep_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavManagefiltersexecutionstep_Visible), 5, 0)));
         }
         AV15DynamicFiltersSelector1 = "TABELA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavTftabela_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftabela_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftabela_nome_Visible), 5, 0)));
         edtavTftabela_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftabela_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftabela_nome_sel_Visible), 5, 0)));
         edtavTftabela_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftabela_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftabela_descricao_Visible), 5, 0)));
         edtavTftabela_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftabela_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftabela_descricao_sel_Visible), 5, 0)));
         edtavTftabela_sistemades_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftabela_sistemades_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftabela_sistemades_Visible), 5, 0)));
         edtavTftabela_sistemades_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftabela_sistemades_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftabela_sistemades_sel_Visible), 5, 0)));
         edtavTftabela_modulodes_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftabela_modulodes_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftabela_modulodes_Visible), 5, 0)));
         edtavTftabela_modulodes_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftabela_modulodes_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftabela_modulodes_sel_Visible), 5, 0)));
         edtavTftabela_painom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftabela_painom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftabela_painom_Visible), 5, 0)));
         edtavTftabela_painom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftabela_painom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftabela_painom_sel_Visible), 5, 0)));
         edtavTftabela_melhoracod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftabela_melhoracod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftabela_melhoracod_Visible), 5, 0)));
         edtavTftabela_melhoracod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftabela_melhoracod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftabela_melhoracod_to_Visible), 5, 0)));
         edtavTftabela_ativo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftabela_ativo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftabela_ativo_sel_Visible), 5, 0)));
         Ddo_tabela_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Tabela_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_nome_Internalname, "TitleControlIdToReplace", Ddo_tabela_nome_Titlecontrolidtoreplace);
         AV72ddo_Tabela_NomeTitleControlIdToReplace = Ddo_tabela_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_Tabela_NomeTitleControlIdToReplace", AV72ddo_Tabela_NomeTitleControlIdToReplace);
         edtavDdo_tabela_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_tabela_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_tabela_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_tabela_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_Tabela_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_descricao_Internalname, "TitleControlIdToReplace", Ddo_tabela_descricao_Titlecontrolidtoreplace);
         AV76ddo_Tabela_DescricaoTitleControlIdToReplace = Ddo_tabela_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76ddo_Tabela_DescricaoTitleControlIdToReplace", AV76ddo_Tabela_DescricaoTitleControlIdToReplace);
         edtavDdo_tabela_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_tabela_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_tabela_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_tabela_sistemades_Titlecontrolidtoreplace = subGrid_Internalname+"_Tabela_SistemaDes";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_sistemades_Internalname, "TitleControlIdToReplace", Ddo_tabela_sistemades_Titlecontrolidtoreplace);
         AV80ddo_Tabela_SistemaDesTitleControlIdToReplace = Ddo_tabela_sistemades_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80ddo_Tabela_SistemaDesTitleControlIdToReplace", AV80ddo_Tabela_SistemaDesTitleControlIdToReplace);
         edtavDdo_tabela_sistemadestitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_tabela_sistemadestitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_tabela_sistemadestitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_tabela_modulodes_Titlecontrolidtoreplace = subGrid_Internalname+"_Tabela_ModuloDes";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_modulodes_Internalname, "TitleControlIdToReplace", Ddo_tabela_modulodes_Titlecontrolidtoreplace);
         AV84ddo_Tabela_ModuloDesTitleControlIdToReplace = Ddo_tabela_modulodes_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84ddo_Tabela_ModuloDesTitleControlIdToReplace", AV84ddo_Tabela_ModuloDesTitleControlIdToReplace);
         edtavDdo_tabela_modulodestitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_tabela_modulodestitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_tabela_modulodestitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_tabela_painom_Titlecontrolidtoreplace = subGrid_Internalname+"_Tabela_PaiNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_painom_Internalname, "TitleControlIdToReplace", Ddo_tabela_painom_Titlecontrolidtoreplace);
         AV88ddo_Tabela_PaiNomTitleControlIdToReplace = Ddo_tabela_painom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88ddo_Tabela_PaiNomTitleControlIdToReplace", AV88ddo_Tabela_PaiNomTitleControlIdToReplace);
         edtavDdo_tabela_painomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_tabela_painomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_tabela_painomtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_tabela_melhoracod_Titlecontrolidtoreplace = subGrid_Internalname+"_Tabela_MelhoraCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_melhoracod_Internalname, "TitleControlIdToReplace", Ddo_tabela_melhoracod_Titlecontrolidtoreplace);
         AV106ddo_Tabela_MelhoraCodTitleControlIdToReplace = Ddo_tabela_melhoracod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV106ddo_Tabela_MelhoraCodTitleControlIdToReplace", AV106ddo_Tabela_MelhoraCodTitleControlIdToReplace);
         edtavDdo_tabela_melhoracodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_tabela_melhoracodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_tabela_melhoracodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_tabela_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_Tabela_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_ativo_Internalname, "TitleControlIdToReplace", Ddo_tabela_ativo_Titlecontrolidtoreplace);
         AV91ddo_Tabela_AtivoTitleControlIdToReplace = Ddo_tabela_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91ddo_Tabela_AtivoTitleControlIdToReplace", AV91ddo_Tabela_AtivoTitleControlIdToReplace);
         edtavDdo_tabela_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_tabela_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_tabela_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Tabela";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Nome", 0);
         cmbavOrderedby.addItem("2", "Descri��o", 0);
         cmbavOrderedby.addItem("3", "Sistema", 0);
         cmbavOrderedby.addItem("4", "M�dulo", 0);
         cmbavOrderedby.addItem("5", "Tabela Pai", 0);
         cmbavOrderedby.addItem("6", "a melhorar", 0);
         cmbavOrderedby.addItem("7", "Ativo", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         Ddo_managefilters_Icon = context.convertURL( (String)(context.GetImagePath( "5efb9e2b-46db-43f4-8f74-dd3f5818d30e", "", context.GetTheme( ))));
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_managefilters_Internalname, "Icon", Ddo_managefilters_Icon);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV92DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV92DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E24462( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV69Tabela_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV73Tabela_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV77Tabela_SistemaDesTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV81Tabela_ModuloDesTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV85Tabela_PaiNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV103Tabela_MelhoraCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV89Tabela_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         if ( AV97ManageFiltersExecutionStep == 1 )
         {
            AV97ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV97ManageFiltersExecutionStep), 1, 0));
         }
         else if ( AV97ManageFiltersExecutionStep == 2 )
         {
            AV97ManageFiltersExecutionStep = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV97ManageFiltersExecutionStep), 1, 0));
            /* Execute user subroutine: 'LOADSAVEDFILTERS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         cmbavDynamicfiltersoperator1.removeAllItems();
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TABELA_NOME") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            cmbavDynamicfiltersoperator1.addItem("2", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("3", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TABELA_MODULODES") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtTabela_Nome_Titleformat = 2;
         edtTabela_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV72ddo_Tabela_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTabela_Nome_Internalname, "Title", edtTabela_Nome_Title);
         edtTabela_Descricao_Titleformat = 2;
         edtTabela_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Descri��o", AV76ddo_Tabela_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTabela_Descricao_Internalname, "Title", edtTabela_Descricao_Title);
         edtTabela_SistemaDes_Titleformat = 2;
         edtTabela_SistemaDes_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Sistema", AV80ddo_Tabela_SistemaDesTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTabela_SistemaDes_Internalname, "Title", edtTabela_SistemaDes_Title);
         edtTabela_ModuloDes_Titleformat = 2;
         edtTabela_ModuloDes_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "M�dulo", AV84ddo_Tabela_ModuloDesTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTabela_ModuloDes_Internalname, "Title", edtTabela_ModuloDes_Title);
         edtTabela_PaiNom_Titleformat = 2;
         edtTabela_PaiNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tabela Pai", AV88ddo_Tabela_PaiNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTabela_PaiNom_Internalname, "Title", edtTabela_PaiNom_Title);
         edtTabela_MelhoraCod_Titleformat = 2;
         edtTabela_MelhoraCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "a melhorar", AV106ddo_Tabela_MelhoraCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTabela_MelhoraCod_Internalname, "Title", edtTabela_MelhoraCod_Title);
         chkTabela_Ativo_Titleformat = 2;
         chkTabela_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ativo", AV91ddo_Tabela_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkTabela_Ativo_Internalname, "Title", chkTabela_Ativo.Title.Text);
         AV94GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV94GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV94GridCurrentPage), 10, 0)));
         AV95GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV95GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV95GridPageCount), 10, 0)));
         AV61Sistema_AreaTrabalhoCod = AV6WWPContext.gxTpr_Areatrabalho_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61Sistema_AreaTrabalhoCod), 6, 0)));
         AV109WWTabelaDS_1_Sistema_areatrabalhocod = AV61Sistema_AreaTrabalhoCod;
         AV110WWTabelaDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV111WWTabelaDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV112WWTabelaDS_4_Tabela_nome1 = AV17Tabela_Nome1;
         AV113WWTabelaDS_5_Tabela_modulodes1 = AV96Tabela_ModuloDes1;
         AV114WWTabelaDS_6_Tabela_modulocod1 = AV62Tabela_ModuloCod1;
         AV115WWTabelaDS_7_Tftabela_nome = AV70TFTabela_Nome;
         AV116WWTabelaDS_8_Tftabela_nome_sel = AV71TFTabela_Nome_Sel;
         AV117WWTabelaDS_9_Tftabela_descricao = AV74TFTabela_Descricao;
         AV118WWTabelaDS_10_Tftabela_descricao_sel = AV75TFTabela_Descricao_Sel;
         AV119WWTabelaDS_11_Tftabela_sistemades = AV78TFTabela_SistemaDes;
         AV120WWTabelaDS_12_Tftabela_sistemades_sel = AV79TFTabela_SistemaDes_Sel;
         AV121WWTabelaDS_13_Tftabela_modulodes = AV82TFTabela_ModuloDes;
         AV122WWTabelaDS_14_Tftabela_modulodes_sel = AV83TFTabela_ModuloDes_Sel;
         AV123WWTabelaDS_15_Tftabela_painom = AV86TFTabela_PaiNom;
         AV124WWTabelaDS_16_Tftabela_painom_sel = AV87TFTabela_PaiNom_Sel;
         AV125WWTabelaDS_17_Tftabela_melhoracod = AV104TFTabela_MelhoraCod;
         AV126WWTabelaDS_18_Tftabela_melhoracod_to = AV105TFTabela_MelhoraCod_To;
         AV127WWTabelaDS_19_Tftabela_ativo_sel = AV90TFTabela_Ativo_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV69Tabela_NomeTitleFilterData", AV69Tabela_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV73Tabela_DescricaoTitleFilterData", AV73Tabela_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV77Tabela_SistemaDesTitleFilterData", AV77Tabela_SistemaDesTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV81Tabela_ModuloDesTitleFilterData", AV81Tabela_ModuloDesTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV85Tabela_PaiNomTitleFilterData", AV85Tabela_PaiNomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV103Tabela_MelhoraCodTitleFilterData", AV103Tabela_MelhoraCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV89Tabela_AtivoTitleFilterData", AV89Tabela_AtivoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         dynavSistema_areatrabalhocod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV61Sistema_AreaTrabalhoCod), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSistema_areatrabalhocod_Internalname, "Values", dynavSistema_areatrabalhocod.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV101ManageFiltersData", AV101ManageFiltersData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E12462( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV93PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV93PageToGo) ;
         }
      }

      protected void E13462( )
      {
         /* Ddo_tabela_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_tabela_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tabela_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_nome_Internalname, "SortedStatus", Ddo_tabela_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tabela_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tabela_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_nome_Internalname, "SortedStatus", Ddo_tabela_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tabela_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV70TFTabela_Nome = Ddo_tabela_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFTabela_Nome", AV70TFTabela_Nome);
            AV71TFTabela_Nome_Sel = Ddo_tabela_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFTabela_Nome_Sel", AV71TFTabela_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14462( )
      {
         /* Ddo_tabela_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_tabela_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tabela_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_descricao_Internalname, "SortedStatus", Ddo_tabela_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tabela_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tabela_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_descricao_Internalname, "SortedStatus", Ddo_tabela_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tabela_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV74TFTabela_Descricao = Ddo_tabela_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFTabela_Descricao", AV74TFTabela_Descricao);
            AV75TFTabela_Descricao_Sel = Ddo_tabela_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFTabela_Descricao_Sel", AV75TFTabela_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15462( )
      {
         /* Ddo_tabela_sistemades_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_tabela_sistemades_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tabela_sistemades_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_sistemades_Internalname, "SortedStatus", Ddo_tabela_sistemades_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tabela_sistemades_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tabela_sistemades_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_sistemades_Internalname, "SortedStatus", Ddo_tabela_sistemades_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tabela_sistemades_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV78TFTabela_SistemaDes = Ddo_tabela_sistemades_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFTabela_SistemaDes", AV78TFTabela_SistemaDes);
            AV79TFTabela_SistemaDes_Sel = Ddo_tabela_sistemades_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFTabela_SistemaDes_Sel", AV79TFTabela_SistemaDes_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E16462( )
      {
         /* Ddo_tabela_modulodes_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_tabela_modulodes_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tabela_modulodes_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_modulodes_Internalname, "SortedStatus", Ddo_tabela_modulodes_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tabela_modulodes_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tabela_modulodes_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_modulodes_Internalname, "SortedStatus", Ddo_tabela_modulodes_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tabela_modulodes_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV82TFTabela_ModuloDes = Ddo_tabela_modulodes_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFTabela_ModuloDes", AV82TFTabela_ModuloDes);
            AV83TFTabela_ModuloDes_Sel = Ddo_tabela_modulodes_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFTabela_ModuloDes_Sel", AV83TFTabela_ModuloDes_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E17462( )
      {
         /* Ddo_tabela_painom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_tabela_painom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tabela_painom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_painom_Internalname, "SortedStatus", Ddo_tabela_painom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tabela_painom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tabela_painom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_painom_Internalname, "SortedStatus", Ddo_tabela_painom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tabela_painom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV86TFTabela_PaiNom = Ddo_tabela_painom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86TFTabela_PaiNom", AV86TFTabela_PaiNom);
            AV87TFTabela_PaiNom_Sel = Ddo_tabela_painom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87TFTabela_PaiNom_Sel", AV87TFTabela_PaiNom_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E18462( )
      {
         /* Ddo_tabela_melhoracod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_tabela_melhoracod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tabela_melhoracod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_melhoracod_Internalname, "SortedStatus", Ddo_tabela_melhoracod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tabela_melhoracod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tabela_melhoracod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_melhoracod_Internalname, "SortedStatus", Ddo_tabela_melhoracod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tabela_melhoracod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV104TFTabela_MelhoraCod = (int)(NumberUtil.Val( Ddo_tabela_melhoracod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV104TFTabela_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV104TFTabela_MelhoraCod), 6, 0)));
            AV105TFTabela_MelhoraCod_To = (int)(NumberUtil.Val( Ddo_tabela_melhoracod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105TFTabela_MelhoraCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105TFTabela_MelhoraCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E19462( )
      {
         /* Ddo_tabela_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_tabela_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tabela_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_ativo_Internalname, "SortedStatus", Ddo_tabela_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tabela_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tabela_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_ativo_Internalname, "SortedStatus", Ddo_tabela_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tabela_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV90TFTabela_Ativo_Sel = (short)(NumberUtil.Val( Ddo_tabela_ativo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90TFTabela_Ativo_Sel", StringUtil.Str( (decimal)(AV90TFTabela_Ativo_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E25462( )
      {
         /* Grid_Load Routine */
         edtavUpdate_Tooltiptext = "Modifica";
         if ( AV6WWPContext.gxTpr_Update )
         {
            edtavUpdate_Link = formatLink("tabela.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A172Tabela_Codigo) + "," + UrlEncode("" +A190Tabela_SistemaCod) + "," + UrlEncode("" +AV65Tabela_ModuloCod);
            AV37Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV37Update);
            AV128Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavUpdate_Enabled = 1;
         }
         else
         {
            edtavUpdate_Link = "";
            AV37Update = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV37Update);
            AV128Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavUpdate_Enabled = 0;
         }
         edtavDelete_Tooltiptext = "Eliminar";
         if ( AV6WWPContext.gxTpr_Delete )
         {
            edtavDelete_Link = formatLink("tabela.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A172Tabela_Codigo) + "," + UrlEncode("" +A190Tabela_SistemaCod) + "," + UrlEncode("" +AV65Tabela_ModuloCod);
            AV38Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV38Delete);
            AV129Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavDelete_Enabled = 1;
         }
         else
         {
            edtavDelete_Link = "";
            AV38Delete = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV38Delete);
            AV129Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
            edtavDelete_Enabled = 0;
         }
         edtavDisplay_Tooltiptext = "Mostrar";
         if ( AV6WWPContext.gxTpr_Display )
         {
            edtavDisplay_Link = formatLink("viewtabela.aspx") + "?" + UrlEncode("" +A172Tabela_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
            AV66Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV66Display);
            AV130Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
            edtavDisplay_Enabled = 1;
         }
         else
         {
            edtavDisplay_Link = "";
            AV66Display = context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV66Display);
            AV130Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( )));
            edtavDisplay_Enabled = 0;
         }
         edtTabela_Nome_Link = formatLink("viewtabela.aspx") + "?" + UrlEncode("" +A172Tabela_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtTabela_ModuloDes_Link = formatLink("viewmodulo.aspx") + "?" + UrlEncode("" +A188Tabela_ModuloCod) + "," + UrlEncode(StringUtil.RTrim(""));
         edtTabela_PaiNom_Link = formatLink("viewtabela.aspx") + "?" + UrlEncode("" +A181Tabela_PaiCod) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 62;
         }
         sendrow_622( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_62_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(62, GridRow);
         }
      }

      protected void E20462( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E22462( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E11462( )
      {
         /* Ddo_managefilters_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Clean#>") == 0 )
         {
            /* Execute user subroutine: 'CLEANFILTERS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Save#>") == 0 )
         {
            /* Execute user subroutine: 'SAVEGRIDSTATE' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            context.PopUp(formatLink("wwpbaseobjects.savefilteras.aspx") + "?" + UrlEncode(StringUtil.RTrim("WWTabelaFilters")) + "," + UrlEncode(StringUtil.RTrim(AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika"))), new Object[] {});
            AV97ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV97ManageFiltersExecutionStep), 1, 0));
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Manage#>") == 0 )
         {
            context.PopUp(formatLink("wwpbaseobjects.managefilters.aspx") + "?" + UrlEncode(StringUtil.RTrim("WWTabelaFilters")), new Object[] {});
            AV97ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV97ManageFiltersExecutionStep), 1, 0));
            context.DoAjaxRefresh();
         }
         else
         {
            GXt_char2 = AV98ManageFiltersXml;
            new wwpbaseobjects.getfilterbyname(context ).execute(  "WWTabelaFilters",  Ddo_managefilters_Activeeventkey, out  GXt_char2) ;
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_managefilters_Internalname, "ActiveEventKey", Ddo_managefilters_Activeeventkey);
            AV98ManageFiltersXml = GXt_char2;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV98ManageFiltersXml)) )
            {
               GX_msglist.addItem("O filtro selecionado n�o existe mais.");
            }
            else
            {
               /* Execute user subroutine: 'CLEANFILTERS' */
               S182 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               new wwpbaseobjects.savegridstate(context ).execute(  AV131Pgmname+"GridState",  AV98ManageFiltersXml) ;
               AV10GridState.FromXml(AV98ManageFiltersXml, "");
               AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
               S152 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
               S192 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
               S202 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               subgrid_firstpage( ) ;
               context.DoAjaxRefresh();
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         dynavSistema_areatrabalhocod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV61Sistema_AreaTrabalhoCod), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSistema_areatrabalhocod_Internalname, "Values", dynavSistema_areatrabalhocod.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         dynavTabela_modulocod1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV62Tabela_ModuloCod1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTabela_modulocod1_Internalname, "Values", dynavTabela_modulocod1.ToJavascriptSource());
      }

      protected void E21462( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         dynavSistema_areatrabalhocod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV61Sistema_AreaTrabalhoCod), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSistema_areatrabalhocod_Internalname, "Values", dynavSistema_areatrabalhocod.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         dynavTabela_modulocod1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV62Tabela_ModuloCod1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTabela_modulocod1_Internalname, "Values", dynavTabela_modulocod1.ToJavascriptSource());
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_tabela_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_nome_Internalname, "SortedStatus", Ddo_tabela_nome_Sortedstatus);
         Ddo_tabela_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_descricao_Internalname, "SortedStatus", Ddo_tabela_descricao_Sortedstatus);
         Ddo_tabela_sistemades_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_sistemades_Internalname, "SortedStatus", Ddo_tabela_sistemades_Sortedstatus);
         Ddo_tabela_modulodes_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_modulodes_Internalname, "SortedStatus", Ddo_tabela_modulodes_Sortedstatus);
         Ddo_tabela_painom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_painom_Internalname, "SortedStatus", Ddo_tabela_painom_Sortedstatus);
         Ddo_tabela_melhoracod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_melhoracod_Internalname, "SortedStatus", Ddo_tabela_melhoracod_Sortedstatus);
         Ddo_tabela_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_ativo_Internalname, "SortedStatus", Ddo_tabela_ativo_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_tabela_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_nome_Internalname, "SortedStatus", Ddo_tabela_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_tabela_descricao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_descricao_Internalname, "SortedStatus", Ddo_tabela_descricao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_tabela_sistemades_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_sistemades_Internalname, "SortedStatus", Ddo_tabela_sistemades_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_tabela_modulodes_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_modulodes_Internalname, "SortedStatus", Ddo_tabela_modulodes_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_tabela_painom_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_painom_Internalname, "SortedStatus", Ddo_tabela_painom_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_tabela_melhoracod_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_melhoracod_Internalname, "SortedStatus", Ddo_tabela_melhoracod_Sortedstatus);
         }
         else if ( AV13OrderedBy == 7 )
         {
            Ddo_tabela_ativo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_ativo_Internalname, "SortedStatus", Ddo_tabela_ativo_Sortedstatus);
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavTabela_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTabela_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_nome1_Visible), 5, 0)));
         edtavTabela_modulodes1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTabela_modulodes1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_modulodes1_Visible), 5, 0)));
         dynavTabela_modulocod1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTabela_modulocod1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavTabela_modulocod1.Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TABELA_NOME") == 0 )
         {
            edtavTabela_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTabela_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TABELA_MODULODES") == 0 )
         {
            edtavTabela_modulodes1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTabela_modulodes1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_modulodes1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TABELA_MODULOCOD") == 0 )
         {
            dynavTabela_modulocod1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTabela_modulocod1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavTabela_modulocod1.Visible), 5, 0)));
         }
      }

      protected void S112( )
      {
         /* 'LOADSAVEDFILTERS' Routine */
         AV101ManageFiltersData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV102ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV102ManageFiltersDataItem.gxTpr_Title = "Limpar filtros";
         AV102ManageFiltersDataItem.gxTpr_Eventkey = "<#Clean#>";
         AV102ManageFiltersDataItem.gxTpr_Isdivider = false;
         AV102ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "63d2ae92-4e43-4a70-af61-0943e39ea422", "", context.GetTheme( ))));
         AV101ManageFiltersData.Add(AV102ManageFiltersDataItem, 0);
         AV102ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV102ManageFiltersDataItem.gxTpr_Title = "Salvar filtro como...";
         AV102ManageFiltersDataItem.gxTpr_Eventkey = "<#Save#>";
         AV102ManageFiltersDataItem.gxTpr_Isdivider = false;
         AV102ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "6eee63e8-73c7-4738-beee-f98e3a8d2841", "", context.GetTheme( ))));
         AV101ManageFiltersData.Add(AV102ManageFiltersDataItem, 0);
         AV102ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV102ManageFiltersDataItem.gxTpr_Isdivider = true;
         AV101ManageFiltersData.Add(AV102ManageFiltersDataItem, 0);
         AV99ManageFiltersItems.FromXml(new wwpbaseobjects.loadmanagefiltersstate(context).executeUdp(  "WWTabelaFilters"), "");
         AV132GXV1 = 1;
         while ( AV132GXV1 <= AV99ManageFiltersItems.Count )
         {
            AV100ManageFiltersItem = ((wwpbaseobjects.SdtGridStateCollection_Item)AV99ManageFiltersItems.Item(AV132GXV1));
            AV102ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV102ManageFiltersDataItem.gxTpr_Title = AV100ManageFiltersItem.gxTpr_Title;
            AV102ManageFiltersDataItem.gxTpr_Eventkey = AV100ManageFiltersItem.gxTpr_Title;
            AV102ManageFiltersDataItem.gxTpr_Isdivider = false;
            AV101ManageFiltersData.Add(AV102ManageFiltersDataItem, 0);
            if ( AV101ManageFiltersData.Count == 13 )
            {
               if (true) break;
            }
            AV132GXV1 = (int)(AV132GXV1+1);
         }
         if ( AV101ManageFiltersData.Count > 3 )
         {
            AV102ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV102ManageFiltersDataItem.gxTpr_Isdivider = true;
            AV101ManageFiltersData.Add(AV102ManageFiltersDataItem, 0);
            AV102ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV102ManageFiltersDataItem.gxTpr_Title = "Gerenciar filtros";
            AV102ManageFiltersDataItem.gxTpr_Eventkey = "<#Manage#>";
            AV102ManageFiltersDataItem.gxTpr_Isdivider = false;
            AV102ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "653f6166-5d82-407a-af84-19e0dde65efd", "", context.GetTheme( ))));
            AV102ManageFiltersDataItem.gxTpr_Jsonclickevent = "";
            AV101ManageFiltersData.Add(AV102ManageFiltersDataItem, 0);
         }
      }

      protected void S182( )
      {
         /* 'CLEANFILTERS' Routine */
         AV61Sistema_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61Sistema_AreaTrabalhoCod), 6, 0)));
         AV70TFTabela_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFTabela_Nome", AV70TFTabela_Nome);
         Ddo_tabela_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_nome_Internalname, "FilteredText_set", Ddo_tabela_nome_Filteredtext_set);
         AV71TFTabela_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFTabela_Nome_Sel", AV71TFTabela_Nome_Sel);
         Ddo_tabela_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_nome_Internalname, "SelectedValue_set", Ddo_tabela_nome_Selectedvalue_set);
         AV74TFTabela_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFTabela_Descricao", AV74TFTabela_Descricao);
         Ddo_tabela_descricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_descricao_Internalname, "FilteredText_set", Ddo_tabela_descricao_Filteredtext_set);
         AV75TFTabela_Descricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFTabela_Descricao_Sel", AV75TFTabela_Descricao_Sel);
         Ddo_tabela_descricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_descricao_Internalname, "SelectedValue_set", Ddo_tabela_descricao_Selectedvalue_set);
         AV78TFTabela_SistemaDes = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFTabela_SistemaDes", AV78TFTabela_SistemaDes);
         Ddo_tabela_sistemades_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_sistemades_Internalname, "FilteredText_set", Ddo_tabela_sistemades_Filteredtext_set);
         AV79TFTabela_SistemaDes_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFTabela_SistemaDes_Sel", AV79TFTabela_SistemaDes_Sel);
         Ddo_tabela_sistemades_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_sistemades_Internalname, "SelectedValue_set", Ddo_tabela_sistemades_Selectedvalue_set);
         AV82TFTabela_ModuloDes = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFTabela_ModuloDes", AV82TFTabela_ModuloDes);
         Ddo_tabela_modulodes_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_modulodes_Internalname, "FilteredText_set", Ddo_tabela_modulodes_Filteredtext_set);
         AV83TFTabela_ModuloDes_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFTabela_ModuloDes_Sel", AV83TFTabela_ModuloDes_Sel);
         Ddo_tabela_modulodes_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_modulodes_Internalname, "SelectedValue_set", Ddo_tabela_modulodes_Selectedvalue_set);
         AV86TFTabela_PaiNom = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86TFTabela_PaiNom", AV86TFTabela_PaiNom);
         Ddo_tabela_painom_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_painom_Internalname, "FilteredText_set", Ddo_tabela_painom_Filteredtext_set);
         AV87TFTabela_PaiNom_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87TFTabela_PaiNom_Sel", AV87TFTabela_PaiNom_Sel);
         Ddo_tabela_painom_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_painom_Internalname, "SelectedValue_set", Ddo_tabela_painom_Selectedvalue_set);
         AV104TFTabela_MelhoraCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV104TFTabela_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV104TFTabela_MelhoraCod), 6, 0)));
         Ddo_tabela_melhoracod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_melhoracod_Internalname, "FilteredText_set", Ddo_tabela_melhoracod_Filteredtext_set);
         AV105TFTabela_MelhoraCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105TFTabela_MelhoraCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105TFTabela_MelhoraCod_To), 6, 0)));
         Ddo_tabela_melhoracod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_melhoracod_Internalname, "FilteredTextTo_set", Ddo_tabela_melhoracod_Filteredtextto_set);
         AV90TFTabela_Ativo_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90TFTabela_Ativo_Sel", StringUtil.Str( (decimal)(AV90TFTabela_Ativo_Sel), 1, 0));
         Ddo_tabela_ativo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_ativo_Internalname, "SelectedValue_set", Ddo_tabela_ativo_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "TABELA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV17Tabela_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Tabela_Nome1", AV17Tabela_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV39Session.Get(AV131Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV131Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV39Session.Get(AV131Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S192( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV133GXV2 = 1;
         while ( AV133GXV2 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV133GXV2));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "SISTEMA_AREATRABALHOCOD") == 0 )
            {
               AV61Sistema_AreaTrabalhoCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61Sistema_AreaTrabalhoCod), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFTABELA_NOME") == 0 )
            {
               AV70TFTabela_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFTabela_Nome", AV70TFTabela_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70TFTabela_Nome)) )
               {
                  Ddo_tabela_nome_Filteredtext_set = AV70TFTabela_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_nome_Internalname, "FilteredText_set", Ddo_tabela_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFTABELA_NOME_SEL") == 0 )
            {
               AV71TFTabela_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFTabela_Nome_Sel", AV71TFTabela_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71TFTabela_Nome_Sel)) )
               {
                  Ddo_tabela_nome_Selectedvalue_set = AV71TFTabela_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_nome_Internalname, "SelectedValue_set", Ddo_tabela_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFTABELA_DESCRICAO") == 0 )
            {
               AV74TFTabela_Descricao = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFTabela_Descricao", AV74TFTabela_Descricao);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74TFTabela_Descricao)) )
               {
                  Ddo_tabela_descricao_Filteredtext_set = AV74TFTabela_Descricao;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_descricao_Internalname, "FilteredText_set", Ddo_tabela_descricao_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFTABELA_DESCRICAO_SEL") == 0 )
            {
               AV75TFTabela_Descricao_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFTabela_Descricao_Sel", AV75TFTabela_Descricao_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75TFTabela_Descricao_Sel)) )
               {
                  Ddo_tabela_descricao_Selectedvalue_set = AV75TFTabela_Descricao_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_descricao_Internalname, "SelectedValue_set", Ddo_tabela_descricao_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFTABELA_SISTEMADES") == 0 )
            {
               AV78TFTabela_SistemaDes = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFTabela_SistemaDes", AV78TFTabela_SistemaDes);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78TFTabela_SistemaDes)) )
               {
                  Ddo_tabela_sistemades_Filteredtext_set = AV78TFTabela_SistemaDes;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_sistemades_Internalname, "FilteredText_set", Ddo_tabela_sistemades_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFTABELA_SISTEMADES_SEL") == 0 )
            {
               AV79TFTabela_SistemaDes_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFTabela_SistemaDes_Sel", AV79TFTabela_SistemaDes_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79TFTabela_SistemaDes_Sel)) )
               {
                  Ddo_tabela_sistemades_Selectedvalue_set = AV79TFTabela_SistemaDes_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_sistemades_Internalname, "SelectedValue_set", Ddo_tabela_sistemades_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFTABELA_MODULODES") == 0 )
            {
               AV82TFTabela_ModuloDes = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFTabela_ModuloDes", AV82TFTabela_ModuloDes);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82TFTabela_ModuloDes)) )
               {
                  Ddo_tabela_modulodes_Filteredtext_set = AV82TFTabela_ModuloDes;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_modulodes_Internalname, "FilteredText_set", Ddo_tabela_modulodes_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFTABELA_MODULODES_SEL") == 0 )
            {
               AV83TFTabela_ModuloDes_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFTabela_ModuloDes_Sel", AV83TFTabela_ModuloDes_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83TFTabela_ModuloDes_Sel)) )
               {
                  Ddo_tabela_modulodes_Selectedvalue_set = AV83TFTabela_ModuloDes_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_modulodes_Internalname, "SelectedValue_set", Ddo_tabela_modulodes_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFTABELA_PAINOM") == 0 )
            {
               AV86TFTabela_PaiNom = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86TFTabela_PaiNom", AV86TFTabela_PaiNom);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86TFTabela_PaiNom)) )
               {
                  Ddo_tabela_painom_Filteredtext_set = AV86TFTabela_PaiNom;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_painom_Internalname, "FilteredText_set", Ddo_tabela_painom_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFTABELA_PAINOM_SEL") == 0 )
            {
               AV87TFTabela_PaiNom_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87TFTabela_PaiNom_Sel", AV87TFTabela_PaiNom_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87TFTabela_PaiNom_Sel)) )
               {
                  Ddo_tabela_painom_Selectedvalue_set = AV87TFTabela_PaiNom_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_painom_Internalname, "SelectedValue_set", Ddo_tabela_painom_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFTABELA_MELHORACOD") == 0 )
            {
               AV104TFTabela_MelhoraCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV104TFTabela_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV104TFTabela_MelhoraCod), 6, 0)));
               AV105TFTabela_MelhoraCod_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105TFTabela_MelhoraCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105TFTabela_MelhoraCod_To), 6, 0)));
               if ( ! (0==AV104TFTabela_MelhoraCod) )
               {
                  Ddo_tabela_melhoracod_Filteredtext_set = StringUtil.Str( (decimal)(AV104TFTabela_MelhoraCod), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_melhoracod_Internalname, "FilteredText_set", Ddo_tabela_melhoracod_Filteredtext_set);
               }
               if ( ! (0==AV105TFTabela_MelhoraCod_To) )
               {
                  Ddo_tabela_melhoracod_Filteredtextto_set = StringUtil.Str( (decimal)(AV105TFTabela_MelhoraCod_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_melhoracod_Internalname, "FilteredTextTo_set", Ddo_tabela_melhoracod_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFTABELA_ATIVO_SEL") == 0 )
            {
               AV90TFTabela_Ativo_Sel = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90TFTabela_Ativo_Sel", StringUtil.Str( (decimal)(AV90TFTabela_Ativo_Sel), 1, 0));
               if ( ! (0==AV90TFTabela_Ativo_Sel) )
               {
                  Ddo_tabela_ativo_Selectedvalue_set = StringUtil.Str( (decimal)(AV90TFTabela_Ativo_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tabela_ativo_Internalname, "SelectedValue_set", Ddo_tabela_ativo_Selectedvalue_set);
               }
            }
            AV133GXV2 = (int)(AV133GXV2+1);
         }
      }

      protected void S202( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TABELA_NOME") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17Tabela_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Tabela_Nome1", AV17Tabela_Nome1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TABELA_MODULODES") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV96Tabela_ModuloDes1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV96Tabela_ModuloDes1", AV96Tabela_ModuloDes1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TABELA_MODULOCOD") == 0 )
            {
               AV62Tabela_ModuloCod1 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62Tabela_ModuloCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62Tabela_ModuloCod1), 6, 0)));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV39Session.Get(AV131Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! (0==AV61Sistema_AreaTrabalhoCod) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "SISTEMA_AREATRABALHOCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV61Sistema_AreaTrabalhoCod), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70TFTabela_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTABELA_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV70TFTabela_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71TFTabela_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTABELA_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV71TFTabela_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74TFTabela_Descricao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTABELA_DESCRICAO";
            AV11GridStateFilterValue.gxTpr_Value = AV74TFTabela_Descricao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75TFTabela_Descricao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTABELA_DESCRICAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV75TFTabela_Descricao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78TFTabela_SistemaDes)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTABELA_SISTEMADES";
            AV11GridStateFilterValue.gxTpr_Value = AV78TFTabela_SistemaDes;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79TFTabela_SistemaDes_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTABELA_SISTEMADES_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV79TFTabela_SistemaDes_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82TFTabela_ModuloDes)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTABELA_MODULODES";
            AV11GridStateFilterValue.gxTpr_Value = AV82TFTabela_ModuloDes;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83TFTabela_ModuloDes_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTABELA_MODULODES_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV83TFTabela_ModuloDes_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86TFTabela_PaiNom)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTABELA_PAINOM";
            AV11GridStateFilterValue.gxTpr_Value = AV86TFTabela_PaiNom;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87TFTabela_PaiNom_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTABELA_PAINOM_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV87TFTabela_PaiNom_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV104TFTabela_MelhoraCod) && (0==AV105TFTabela_MelhoraCod_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTABELA_MELHORACOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV104TFTabela_MelhoraCod), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV105TFTabela_MelhoraCod_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV90TFTabela_Ativo_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTABELA_ATIVO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV90TFTabela_Ativo_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV131Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S222( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TABELA_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Tabela_Nome1)) )
         {
            AV12GridStateDynamicFilter.gxTpr_Value = AV17Tabela_Nome1;
            AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
         }
         else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TABELA_MODULODES") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV96Tabela_ModuloDes1)) )
         {
            AV12GridStateDynamicFilter.gxTpr_Value = AV96Tabela_ModuloDes1;
            AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
         }
         else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TABELA_MODULOCOD") == 0 ) && ! (0==AV62Tabela_ModuloCod1) )
         {
            AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV62Tabela_ModuloCod1), 6, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
         {
            AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
         }
      }

      protected void S132( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV131Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Tabela";
         AV39Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void S212( )
      {
         /* 'RESETDYNFILTERS' Routine */
      }

      protected void wb_table1_2_462( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_462( true) ;
         }
         else
         {
            wb_table2_8_462( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_462e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_56_462( true) ;
         }
         else
         {
            wb_table3_56_462( false) ;
         }
         return  ;
      }

      protected void wb_table3_56_462e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_462e( true) ;
         }
         else
         {
            wb_table1_2_462e( false) ;
         }
      }

      protected void wb_table3_56_462( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "TableGridHeader", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_59_462( true) ;
         }
         else
         {
            wb_table4_59_462( false) ;
         }
         return  ;
      }

      protected void wb_table4_59_462e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_56_462e( true) ;
         }
         else
         {
            wb_table3_56_462e( false) ;
         }
      }

      protected void wb_table4_59_462( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"62\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtTabela_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtTabela_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtTabela_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtTabela_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtTabela_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtTabela_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Sistema") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtTabela_SistemaDes_Titleformat == 0 )
               {
                  context.SendWebValue( edtTabela_SistemaDes_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtTabela_SistemaDes_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�d. M�dulo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtTabela_ModuloDes_Titleformat == 0 )
               {
                  context.SendWebValue( edtTabela_ModuloDes_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtTabela_ModuloDes_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Tabela") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtTabela_PaiNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtTabela_PaiNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtTabela_PaiNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtTabela_MelhoraCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtTabela_MelhoraCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtTabela_MelhoraCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkTabela_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkTabela_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkTabela_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV37Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV38Delete));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV66Display));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A172Tabela_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A173Tabela_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtTabela_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTabela_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtTabela_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A175Tabela_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtTabela_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTabela_Descricao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A190Tabela_SistemaCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A191Tabela_SistemaDes);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtTabela_SistemaDes_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTabela_SistemaDes_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A188Tabela_ModuloCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A189Tabela_ModuloDes));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtTabela_ModuloDes_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTabela_ModuloDes_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtTabela_ModuloDes_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A181Tabela_PaiCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A182Tabela_PaiNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtTabela_PaiNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTabela_PaiNom_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtTabela_PaiNom_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A746Tabela_MelhoraCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtTabela_MelhoraCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTabela_MelhoraCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A174Tabela_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkTabela_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkTabela_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 62 )
         {
            wbEnd = 0;
            nRC_GXsfl_62 = (short)(nGXsfl_62_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_59_462e( true) ;
         }
         else
         {
            wb_table4_59_462e( false) ;
         }
      }

      protected void wb_table2_8_462( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table5_11_462( true) ;
         }
         else
         {
            wb_table5_11_462( false) ;
         }
         return  ;
      }

      protected void wb_table5_11_462e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_21_462( true) ;
         }
         else
         {
            wb_table6_21_462( false) ;
         }
         return  ;
      }

      protected void wb_table6_21_462e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_462e( true) ;
         }
         else
         {
            wb_table2_8_462e( false) ;
         }
      }

      protected void wb_table6_21_462( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "TableFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_MANAGEFILTERSContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWTabela.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextsistema_areatrabalhocod_Internalname, "�rea de Trabalho", "", "", lblFiltertextsistema_areatrabalhocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WWTabela.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'" + sGXsfl_62_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavSistema_areatrabalhocod, dynavSistema_areatrabalhocod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV61Sistema_AreaTrabalhoCod), 6, 0)), 1, dynavSistema_areatrabalhocod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,30);\"", "", true, "HLP_WWTabela.htm");
            dynavSistema_areatrabalhocod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV61Sistema_AreaTrabalhoCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSistema_areatrabalhocod_Internalname, "Values", (String)(dynavSistema_areatrabalhocod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_33_462( true) ;
         }
         else
         {
            wb_table7_33_462( false) ;
         }
         return  ;
      }

      protected void wb_table7_33_462e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_21_462e( true) ;
         }
         else
         {
            wb_table6_21_462e( false) ;
         }
      }

      protected void wb_table7_33_462( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWTabela.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'" + sGXsfl_62_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", "", true, "HLP_WWTabela.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWTabela.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_42_462( true) ;
         }
         else
         {
            wb_table8_42_462( false) ;
         }
         return  ;
      }

      protected void wb_table8_42_462e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_33_462e( true) ;
         }
         else
         {
            wb_table7_33_462e( false) ;
         }
      }

      protected void wb_table8_42_462( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'" + sGXsfl_62_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,45);\"", "", true, "HLP_WWTabela.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'" + sGXsfl_62_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTabela_nome1_Internalname, StringUtil.RTrim( AV17Tabela_Nome1), StringUtil.RTrim( context.localUtil.Format( AV17Tabela_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,47);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTabela_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavTabela_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWTabela.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'" + sGXsfl_62_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTabela_modulodes1_Internalname, StringUtil.RTrim( AV96Tabela_ModuloDes1), StringUtil.RTrim( context.localUtil.Format( AV96Tabela_ModuloDes1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,48);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTabela_modulodes1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavTabela_modulodes1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWTabela.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_62_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavTabela_modulocod1, dynavTabela_modulocod1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV62Tabela_ModuloCod1), 6, 0)), 1, dynavTabela_modulocod1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavTabela_modulocod1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "", true, "HLP_WWTabela.htm");
            dynavTabela_modulocod1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV62Tabela_ModuloCod1), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTabela_modulocod1_Internalname, "Values", (String)(dynavTabela_modulocod1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_42_462e( true) ;
         }
         else
         {
            wb_table8_42_462e( false) ;
         }
      }

      protected void wb_table5_11_462( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTabelatitle_Internalname, "Tabelas", "", "", lblTabelatitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWTabela.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWTabela.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'" + sGXsfl_62_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_WWTabela.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'" + sGXsfl_62_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWTabela.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_11_462e( true) ;
         }
         else
         {
            wb_table5_11_462e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA462( ) ;
         WS462( ) ;
         WE462( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117341178");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwtabela.js", "?20203117341178");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_622( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_62_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_62_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_62_idx;
         edtTabela_Codigo_Internalname = "TABELA_CODIGO_"+sGXsfl_62_idx;
         edtTabela_Nome_Internalname = "TABELA_NOME_"+sGXsfl_62_idx;
         edtTabela_Descricao_Internalname = "TABELA_DESCRICAO_"+sGXsfl_62_idx;
         edtTabela_SistemaCod_Internalname = "TABELA_SISTEMACOD_"+sGXsfl_62_idx;
         edtTabela_SistemaDes_Internalname = "TABELA_SISTEMADES_"+sGXsfl_62_idx;
         edtTabela_ModuloCod_Internalname = "TABELA_MODULOCOD_"+sGXsfl_62_idx;
         edtTabela_ModuloDes_Internalname = "TABELA_MODULODES_"+sGXsfl_62_idx;
         edtTabela_PaiCod_Internalname = "TABELA_PAICOD_"+sGXsfl_62_idx;
         edtTabela_PaiNom_Internalname = "TABELA_PAINOM_"+sGXsfl_62_idx;
         edtTabela_MelhoraCod_Internalname = "TABELA_MELHORACOD_"+sGXsfl_62_idx;
         chkTabela_Ativo_Internalname = "TABELA_ATIVO_"+sGXsfl_62_idx;
      }

      protected void SubsflControlProps_fel_622( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_62_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_62_fel_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_62_fel_idx;
         edtTabela_Codigo_Internalname = "TABELA_CODIGO_"+sGXsfl_62_fel_idx;
         edtTabela_Nome_Internalname = "TABELA_NOME_"+sGXsfl_62_fel_idx;
         edtTabela_Descricao_Internalname = "TABELA_DESCRICAO_"+sGXsfl_62_fel_idx;
         edtTabela_SistemaCod_Internalname = "TABELA_SISTEMACOD_"+sGXsfl_62_fel_idx;
         edtTabela_SistemaDes_Internalname = "TABELA_SISTEMADES_"+sGXsfl_62_fel_idx;
         edtTabela_ModuloCod_Internalname = "TABELA_MODULOCOD_"+sGXsfl_62_fel_idx;
         edtTabela_ModuloDes_Internalname = "TABELA_MODULODES_"+sGXsfl_62_fel_idx;
         edtTabela_PaiCod_Internalname = "TABELA_PAICOD_"+sGXsfl_62_fel_idx;
         edtTabela_PaiNom_Internalname = "TABELA_PAINOM_"+sGXsfl_62_fel_idx;
         edtTabela_MelhoraCod_Internalname = "TABELA_MELHORACOD_"+sGXsfl_62_fel_idx;
         chkTabela_Ativo_Internalname = "TABELA_ATIVO_"+sGXsfl_62_fel_idx;
      }

      protected void sendrow_622( )
      {
         SubsflControlProps_622( ) ;
         WB460( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_62_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_62_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_62_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV37Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV37Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV128Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV37Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV37Update)) ? AV128Update_GXI : context.PathToRelativeUrl( AV37Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV37Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV38Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV38Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV129Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV38Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV38Delete)) ? AV129Delete_GXI : context.PathToRelativeUrl( AV38Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDelete_Enabled,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV38Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV66Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV66Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV130Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV66Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV66Display)) ? AV130Display_GXI : context.PathToRelativeUrl( AV66Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDisplay_Enabled,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV66Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTabela_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A172Tabela_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A172Tabela_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTabela_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)62,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTabela_Nome_Internalname,StringUtil.RTrim( A173Tabela_Nome),StringUtil.RTrim( context.localUtil.Format( A173Tabela_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtTabela_Nome_Link,(String)"",(String)"",(String)"",(String)edtTabela_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)62,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTabela_Descricao_Internalname,(String)A175Tabela_Descricao,(String)A175Tabela_Descricao,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTabela_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)62,(short)1,(short)0,(short)-1,(bool)true,(String)"DescricaoLonga",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTabela_SistemaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A190Tabela_SistemaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A190Tabela_SistemaCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTabela_SistemaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)62,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTabela_SistemaDes_Internalname,(String)A191Tabela_SistemaDes,StringUtil.RTrim( context.localUtil.Format( A191Tabela_SistemaDes, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTabela_SistemaDes_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)62,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTabela_ModuloCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A188Tabela_ModuloCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A188Tabela_ModuloCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTabela_ModuloCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)62,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTabela_ModuloDes_Internalname,StringUtil.RTrim( A189Tabela_ModuloDes),StringUtil.RTrim( context.localUtil.Format( A189Tabela_ModuloDes, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtTabela_ModuloDes_Link,(String)"",(String)"",(String)"",(String)edtTabela_ModuloDes_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)62,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTabela_PaiCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A181Tabela_PaiCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A181Tabela_PaiCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTabela_PaiCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)62,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTabela_PaiNom_Internalname,StringUtil.RTrim( A182Tabela_PaiNom),StringUtil.RTrim( context.localUtil.Format( A182Tabela_PaiNom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtTabela_PaiNom_Link,(String)"",(String)"",(String)"",(String)edtTabela_PaiNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)62,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTabela_MelhoraCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A746Tabela_MelhoraCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A746Tabela_MelhoraCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTabela_MelhoraCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)62,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkTabela_Ativo_Internalname,StringUtil.BoolToStr( A174Tabela_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            GxWebStd.gx_hidden_field( context, "gxhash_TABELA_CODIGO"+"_"+sGXsfl_62_idx, GetSecureSignedToken( sGXsfl_62_idx, context.localUtil.Format( (decimal)(A172Tabela_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_TABELA_NOME"+"_"+sGXsfl_62_idx, GetSecureSignedToken( sGXsfl_62_idx, StringUtil.RTrim( context.localUtil.Format( A173Tabela_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_TABELA_DESCRICAO"+"_"+sGXsfl_62_idx, GetSecureSignedToken( sGXsfl_62_idx, A175Tabela_Descricao));
            GxWebStd.gx_hidden_field( context, "gxhash_TABELA_SISTEMACOD"+"_"+sGXsfl_62_idx, GetSecureSignedToken( sGXsfl_62_idx, context.localUtil.Format( (decimal)(A190Tabela_SistemaCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_TABELA_MODULOCOD"+"_"+sGXsfl_62_idx, GetSecureSignedToken( sGXsfl_62_idx, context.localUtil.Format( (decimal)(A188Tabela_ModuloCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_TABELA_PAICOD"+"_"+sGXsfl_62_idx, GetSecureSignedToken( sGXsfl_62_idx, context.localUtil.Format( (decimal)(A181Tabela_PaiCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_TABELA_MELHORACOD"+"_"+sGXsfl_62_idx, GetSecureSignedToken( sGXsfl_62_idx, context.localUtil.Format( (decimal)(A746Tabela_MelhoraCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_TABELA_ATIVO"+"_"+sGXsfl_62_idx, GetSecureSignedToken( sGXsfl_62_idx, A174Tabela_Ativo));
            GridContainer.AddRow(GridRow);
            nGXsfl_62_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_62_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_62_idx+1));
            sGXsfl_62_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_62_idx), 4, 0)), 4, "0");
            SubsflControlProps_622( ) ;
         }
         /* End function sendrow_622 */
      }

      protected void init_default_properties( )
      {
         lblTabelatitle_Internalname = "TABELATITLE";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         tblTableactions_Internalname = "TABLEACTIONS";
         Ddo_managefilters_Internalname = "DDO_MANAGEFILTERS";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblFiltertextsistema_areatrabalhocod_Internalname = "FILTERTEXTSISTEMA_AREATRABALHOCOD";
         dynavSistema_areatrabalhocod_Internalname = "vSISTEMA_AREATRABALHOCOD";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavTabela_nome1_Internalname = "vTABELA_NOME1";
         edtavTabela_modulodes1_Internalname = "vTABELA_MODULODES1";
         dynavTabela_modulocod1_Internalname = "vTABELA_MODULOCOD1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtavDisplay_Internalname = "vDISPLAY";
         edtTabela_Codigo_Internalname = "TABELA_CODIGO";
         edtTabela_Nome_Internalname = "TABELA_NOME";
         edtTabela_Descricao_Internalname = "TABELA_DESCRICAO";
         edtTabela_SistemaCod_Internalname = "TABELA_SISTEMACOD";
         edtTabela_SistemaDes_Internalname = "TABELA_SISTEMADES";
         edtTabela_ModuloCod_Internalname = "TABELA_MODULOCOD";
         edtTabela_ModuloDes_Internalname = "TABELA_MODULODES";
         edtTabela_PaiCod_Internalname = "TABELA_PAICOD";
         edtTabela_PaiNom_Internalname = "TABELA_PAINOM";
         edtTabela_MelhoraCod_Internalname = "TABELA_MELHORACOD";
         chkTabela_Ativo_Internalname = "TABELA_ATIVO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         edtavManagefiltersexecutionstep_Internalname = "vMANAGEFILTERSEXECUTIONSTEP";
         edtavTftabela_nome_Internalname = "vTFTABELA_NOME";
         edtavTftabela_nome_sel_Internalname = "vTFTABELA_NOME_SEL";
         edtavTftabela_descricao_Internalname = "vTFTABELA_DESCRICAO";
         edtavTftabela_descricao_sel_Internalname = "vTFTABELA_DESCRICAO_SEL";
         edtavTftabela_sistemades_Internalname = "vTFTABELA_SISTEMADES";
         edtavTftabela_sistemades_sel_Internalname = "vTFTABELA_SISTEMADES_SEL";
         edtavTftabela_modulodes_Internalname = "vTFTABELA_MODULODES";
         edtavTftabela_modulodes_sel_Internalname = "vTFTABELA_MODULODES_SEL";
         edtavTftabela_painom_Internalname = "vTFTABELA_PAINOM";
         edtavTftabela_painom_sel_Internalname = "vTFTABELA_PAINOM_SEL";
         edtavTftabela_melhoracod_Internalname = "vTFTABELA_MELHORACOD";
         edtavTftabela_melhoracod_to_Internalname = "vTFTABELA_MELHORACOD_TO";
         edtavTftabela_ativo_sel_Internalname = "vTFTABELA_ATIVO_SEL";
         Ddo_tabela_nome_Internalname = "DDO_TABELA_NOME";
         edtavDdo_tabela_nometitlecontrolidtoreplace_Internalname = "vDDO_TABELA_NOMETITLECONTROLIDTOREPLACE";
         Ddo_tabela_descricao_Internalname = "DDO_TABELA_DESCRICAO";
         edtavDdo_tabela_descricaotitlecontrolidtoreplace_Internalname = "vDDO_TABELA_DESCRICAOTITLECONTROLIDTOREPLACE";
         Ddo_tabela_sistemades_Internalname = "DDO_TABELA_SISTEMADES";
         edtavDdo_tabela_sistemadestitlecontrolidtoreplace_Internalname = "vDDO_TABELA_SISTEMADESTITLECONTROLIDTOREPLACE";
         Ddo_tabela_modulodes_Internalname = "DDO_TABELA_MODULODES";
         edtavDdo_tabela_modulodestitlecontrolidtoreplace_Internalname = "vDDO_TABELA_MODULODESTITLECONTROLIDTOREPLACE";
         Ddo_tabela_painom_Internalname = "DDO_TABELA_PAINOM";
         edtavDdo_tabela_painomtitlecontrolidtoreplace_Internalname = "vDDO_TABELA_PAINOMTITLECONTROLIDTOREPLACE";
         Ddo_tabela_melhoracod_Internalname = "DDO_TABELA_MELHORACOD";
         edtavDdo_tabela_melhoracodtitlecontrolidtoreplace_Internalname = "vDDO_TABELA_MELHORACODTITLECONTROLIDTOREPLACE";
         Ddo_tabela_ativo_Internalname = "DDO_TABELA_ATIVO";
         edtavDdo_tabela_ativotitlecontrolidtoreplace_Internalname = "vDDO_TABELA_ATIVOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtTabela_MelhoraCod_Jsonclick = "";
         edtTabela_PaiNom_Jsonclick = "";
         edtTabela_PaiCod_Jsonclick = "";
         edtTabela_ModuloDes_Jsonclick = "";
         edtTabela_ModuloCod_Jsonclick = "";
         edtTabela_SistemaDes_Jsonclick = "";
         edtTabela_SistemaCod_Jsonclick = "";
         edtTabela_Descricao_Jsonclick = "";
         edtTabela_Nome_Jsonclick = "";
         edtTabela_Codigo_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         dynavTabela_modulocod1_Jsonclick = "";
         edtavTabela_modulodes1_Jsonclick = "";
         edtavTabela_nome1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         dynavSistema_areatrabalhocod_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtTabela_PaiNom_Link = "";
         edtTabela_ModuloDes_Link = "";
         edtTabela_Nome_Link = "";
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDisplay_Enabled = 1;
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavDelete_Enabled = 1;
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavUpdate_Enabled = 1;
         chkTabela_Ativo_Titleformat = 0;
         edtTabela_MelhoraCod_Titleformat = 0;
         edtTabela_PaiNom_Titleformat = 0;
         edtTabela_ModuloDes_Titleformat = 0;
         edtTabela_SistemaDes_Titleformat = 0;
         edtTabela_Descricao_Titleformat = 0;
         edtTabela_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator1.Visible = 1;
         dynavTabela_modulocod1.Visible = 1;
         edtavTabela_modulodes1_Visible = 1;
         edtavTabela_nome1_Visible = 1;
         chkTabela_Ativo.Title.Text = "Ativo";
         edtTabela_MelhoraCod_Title = "a melhorar";
         edtTabela_PaiNom_Title = "Tabela Pai";
         edtTabela_ModuloDes_Title = "M�dulo";
         edtTabela_SistemaDes_Title = "Sistema";
         edtTabela_Descricao_Title = "Descri��o";
         edtTabela_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkTabela_Ativo.Caption = "";
         edtavDdo_tabela_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_tabela_melhoracodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_tabela_painomtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_tabela_modulodestitlecontrolidtoreplace_Visible = 1;
         edtavDdo_tabela_sistemadestitlecontrolidtoreplace_Visible = 1;
         edtavDdo_tabela_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_tabela_nometitlecontrolidtoreplace_Visible = 1;
         edtavTftabela_ativo_sel_Jsonclick = "";
         edtavTftabela_ativo_sel_Visible = 1;
         edtavTftabela_melhoracod_to_Jsonclick = "";
         edtavTftabela_melhoracod_to_Visible = 1;
         edtavTftabela_melhoracod_Jsonclick = "";
         edtavTftabela_melhoracod_Visible = 1;
         edtavTftabela_painom_sel_Jsonclick = "";
         edtavTftabela_painom_sel_Visible = 1;
         edtavTftabela_painom_Jsonclick = "";
         edtavTftabela_painom_Visible = 1;
         edtavTftabela_modulodes_sel_Jsonclick = "";
         edtavTftabela_modulodes_sel_Visible = 1;
         edtavTftabela_modulodes_Jsonclick = "";
         edtavTftabela_modulodes_Visible = 1;
         edtavTftabela_sistemades_sel_Jsonclick = "";
         edtavTftabela_sistemades_sel_Visible = 1;
         edtavTftabela_sistemades_Jsonclick = "";
         edtavTftabela_sistemades_Visible = 1;
         edtavTftabela_descricao_sel_Visible = 1;
         edtavTftabela_descricao_Visible = 1;
         edtavTftabela_nome_sel_Jsonclick = "";
         edtavTftabela_nome_sel_Visible = 1;
         edtavTftabela_nome_Jsonclick = "";
         edtavTftabela_nome_Visible = 1;
         edtavManagefiltersexecutionstep_Jsonclick = "";
         edtavManagefiltersexecutionstep_Visible = 1;
         Ddo_tabela_ativo_Searchbuttontext = "Pesquisar";
         Ddo_tabela_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_tabela_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_tabela_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_tabela_ativo_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_tabela_ativo_Datalisttype = "FixedValues";
         Ddo_tabela_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_tabela_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_tabela_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_tabela_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_tabela_ativo_Titlecontrolidtoreplace = "";
         Ddo_tabela_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_tabela_ativo_Cls = "ColumnSettings";
         Ddo_tabela_ativo_Tooltip = "Op��es";
         Ddo_tabela_ativo_Caption = "";
         Ddo_tabela_melhoracod_Searchbuttontext = "Pesquisar";
         Ddo_tabela_melhoracod_Rangefilterto = "At�";
         Ddo_tabela_melhoracod_Rangefilterfrom = "Desde";
         Ddo_tabela_melhoracod_Cleanfilter = "Limpar pesquisa";
         Ddo_tabela_melhoracod_Sortdsc = "Ordenar de Z � A";
         Ddo_tabela_melhoracod_Sortasc = "Ordenar de A � Z";
         Ddo_tabela_melhoracod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_tabela_melhoracod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_tabela_melhoracod_Filtertype = "Numeric";
         Ddo_tabela_melhoracod_Includefilter = Convert.ToBoolean( -1);
         Ddo_tabela_melhoracod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_tabela_melhoracod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_tabela_melhoracod_Titlecontrolidtoreplace = "";
         Ddo_tabela_melhoracod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_tabela_melhoracod_Cls = "ColumnSettings";
         Ddo_tabela_melhoracod_Tooltip = "Op��es";
         Ddo_tabela_melhoracod_Caption = "";
         Ddo_tabela_painom_Searchbuttontext = "Pesquisar";
         Ddo_tabela_painom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_tabela_painom_Cleanfilter = "Limpar pesquisa";
         Ddo_tabela_painom_Loadingdata = "Carregando dados...";
         Ddo_tabela_painom_Sortdsc = "Ordenar de Z � A";
         Ddo_tabela_painom_Sortasc = "Ordenar de A � Z";
         Ddo_tabela_painom_Datalistupdateminimumcharacters = 0;
         Ddo_tabela_painom_Datalistproc = "GetWWTabelaFilterData";
         Ddo_tabela_painom_Datalisttype = "Dynamic";
         Ddo_tabela_painom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_tabela_painom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_tabela_painom_Filtertype = "Character";
         Ddo_tabela_painom_Includefilter = Convert.ToBoolean( -1);
         Ddo_tabela_painom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_tabela_painom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_tabela_painom_Titlecontrolidtoreplace = "";
         Ddo_tabela_painom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_tabela_painom_Cls = "ColumnSettings";
         Ddo_tabela_painom_Tooltip = "Op��es";
         Ddo_tabela_painom_Caption = "";
         Ddo_tabela_modulodes_Searchbuttontext = "Pesquisar";
         Ddo_tabela_modulodes_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_tabela_modulodes_Cleanfilter = "Limpar pesquisa";
         Ddo_tabela_modulodes_Loadingdata = "Carregando dados...";
         Ddo_tabela_modulodes_Sortdsc = "Ordenar de Z � A";
         Ddo_tabela_modulodes_Sortasc = "Ordenar de A � Z";
         Ddo_tabela_modulodes_Datalistupdateminimumcharacters = 0;
         Ddo_tabela_modulodes_Datalistproc = "GetWWTabelaFilterData";
         Ddo_tabela_modulodes_Datalisttype = "Dynamic";
         Ddo_tabela_modulodes_Includedatalist = Convert.ToBoolean( -1);
         Ddo_tabela_modulodes_Filterisrange = Convert.ToBoolean( 0);
         Ddo_tabela_modulodes_Filtertype = "Character";
         Ddo_tabela_modulodes_Includefilter = Convert.ToBoolean( -1);
         Ddo_tabela_modulodes_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_tabela_modulodes_Includesortasc = Convert.ToBoolean( -1);
         Ddo_tabela_modulodes_Titlecontrolidtoreplace = "";
         Ddo_tabela_modulodes_Dropdownoptionstype = "GridTitleSettings";
         Ddo_tabela_modulodes_Cls = "ColumnSettings";
         Ddo_tabela_modulodes_Tooltip = "Op��es";
         Ddo_tabela_modulodes_Caption = "";
         Ddo_tabela_sistemades_Searchbuttontext = "Pesquisar";
         Ddo_tabela_sistemades_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_tabela_sistemades_Cleanfilter = "Limpar pesquisa";
         Ddo_tabela_sistemades_Loadingdata = "Carregando dados...";
         Ddo_tabela_sistemades_Sortdsc = "Ordenar de Z � A";
         Ddo_tabela_sistemades_Sortasc = "Ordenar de A � Z";
         Ddo_tabela_sistemades_Datalistupdateminimumcharacters = 0;
         Ddo_tabela_sistemades_Datalistproc = "GetWWTabelaFilterData";
         Ddo_tabela_sistemades_Datalisttype = "Dynamic";
         Ddo_tabela_sistemades_Includedatalist = Convert.ToBoolean( -1);
         Ddo_tabela_sistemades_Filterisrange = Convert.ToBoolean( 0);
         Ddo_tabela_sistemades_Filtertype = "Character";
         Ddo_tabela_sistemades_Includefilter = Convert.ToBoolean( -1);
         Ddo_tabela_sistemades_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_tabela_sistemades_Includesortasc = Convert.ToBoolean( -1);
         Ddo_tabela_sistemades_Titlecontrolidtoreplace = "";
         Ddo_tabela_sistemades_Dropdownoptionstype = "GridTitleSettings";
         Ddo_tabela_sistemades_Cls = "ColumnSettings";
         Ddo_tabela_sistemades_Tooltip = "Op��es";
         Ddo_tabela_sistemades_Caption = "";
         Ddo_tabela_descricao_Searchbuttontext = "Pesquisar";
         Ddo_tabela_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_tabela_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_tabela_descricao_Loadingdata = "Carregando dados...";
         Ddo_tabela_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_tabela_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_tabela_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_tabela_descricao_Datalistproc = "GetWWTabelaFilterData";
         Ddo_tabela_descricao_Datalisttype = "Dynamic";
         Ddo_tabela_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_tabela_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_tabela_descricao_Filtertype = "Character";
         Ddo_tabela_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_tabela_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_tabela_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_tabela_descricao_Titlecontrolidtoreplace = "";
         Ddo_tabela_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_tabela_descricao_Cls = "ColumnSettings";
         Ddo_tabela_descricao_Tooltip = "Op��es";
         Ddo_tabela_descricao_Caption = "";
         Ddo_tabela_nome_Searchbuttontext = "Pesquisar";
         Ddo_tabela_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_tabela_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_tabela_nome_Loadingdata = "Carregando dados...";
         Ddo_tabela_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_tabela_nome_Sortasc = "Ordenar de A � Z";
         Ddo_tabela_nome_Datalistupdateminimumcharacters = 0;
         Ddo_tabela_nome_Datalistproc = "GetWWTabelaFilterData";
         Ddo_tabela_nome_Datalisttype = "Dynamic";
         Ddo_tabela_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_tabela_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_tabela_nome_Filtertype = "Character";
         Ddo_tabela_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_tabela_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_tabela_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_tabela_nome_Titlecontrolidtoreplace = "";
         Ddo_tabela_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_tabela_nome_Cls = "ColumnSettings";
         Ddo_tabela_nome_Tooltip = "Op��es";
         Ddo_tabela_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Ddo_managefilters_Cls = "ManageFilters";
         Ddo_managefilters_Tooltip = "Gerenciar filtros";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Tabela";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV65Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0},{av:'A188Tabela_ModuloCod',fld:'TABELA_MODULOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A181Tabela_PaiCod',fld:'TABELA_PAICOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV97ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV72ddo_Tabela_NomeTitleControlIdToReplace',fld:'vDDO_TABELA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_Tabela_DescricaoTitleControlIdToReplace',fld:'vDDO_TABELA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_Tabela_SistemaDesTitleControlIdToReplace',fld:'vDDO_TABELA_SISTEMADESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Tabela_ModuloDesTitleControlIdToReplace',fld:'vDDO_TABELA_MODULODESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_Tabela_PaiNomTitleControlIdToReplace',fld:'vDDO_TABELA_PAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_Tabela_MelhoraCodTitleControlIdToReplace',fld:'vDDO_TABELA_MELHORACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_Tabela_AtivoTitleControlIdToReplace',fld:'vDDO_TABELA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Tabela_Nome1',fld:'vTABELA_NOME1',pic:'@!',nv:''},{av:'AV96Tabela_ModuloDes1',fld:'vTABELA_MODULODES1',pic:'@!',nv:''},{av:'AV62Tabela_ModuloCod1',fld:'vTABELA_MODULOCOD1',pic:'ZZZZZ9',nv:0},{av:'AV70TFTabela_Nome',fld:'vTFTABELA_NOME',pic:'@!',nv:''},{av:'AV71TFTabela_Nome_Sel',fld:'vTFTABELA_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFTabela_Descricao',fld:'vTFTABELA_DESCRICAO',pic:'',nv:''},{av:'AV75TFTabela_Descricao_Sel',fld:'vTFTABELA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV78TFTabela_SistemaDes',fld:'vTFTABELA_SISTEMADES',pic:'@!',nv:''},{av:'AV79TFTabela_SistemaDes_Sel',fld:'vTFTABELA_SISTEMADES_SEL',pic:'@!',nv:''},{av:'AV82TFTabela_ModuloDes',fld:'vTFTABELA_MODULODES',pic:'@!',nv:''},{av:'AV83TFTabela_ModuloDes_Sel',fld:'vTFTABELA_MODULODES_SEL',pic:'@!',nv:''},{av:'AV86TFTabela_PaiNom',fld:'vTFTABELA_PAINOM',pic:'@!',nv:''},{av:'AV87TFTabela_PaiNom_Sel',fld:'vTFTABELA_PAINOM_SEL',pic:'@!',nv:''},{av:'AV104TFTabela_MelhoraCod',fld:'vTFTABELA_MELHORACOD',pic:'ZZZZZ9',nv:0},{av:'AV105TFTabela_MelhoraCod_To',fld:'vTFTABELA_MELHORACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV90TFTabela_Ativo_Sel',fld:'vTFTABELA_ATIVO_SEL',pic:'9',nv:0},{av:'AV131Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}],oparms:[{av:'AV69Tabela_NomeTitleFilterData',fld:'vTABELA_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV73Tabela_DescricaoTitleFilterData',fld:'vTABELA_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV77Tabela_SistemaDesTitleFilterData',fld:'vTABELA_SISTEMADESTITLEFILTERDATA',pic:'',nv:null},{av:'AV81Tabela_ModuloDesTitleFilterData',fld:'vTABELA_MODULODESTITLEFILTERDATA',pic:'',nv:null},{av:'AV85Tabela_PaiNomTitleFilterData',fld:'vTABELA_PAINOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV103Tabela_MelhoraCodTitleFilterData',fld:'vTABELA_MELHORACODTITLEFILTERDATA',pic:'',nv:null},{av:'AV89Tabela_AtivoTitleFilterData',fld:'vTABELA_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV97ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'edtTabela_Nome_Titleformat',ctrl:'TABELA_NOME',prop:'Titleformat'},{av:'edtTabela_Nome_Title',ctrl:'TABELA_NOME',prop:'Title'},{av:'edtTabela_Descricao_Titleformat',ctrl:'TABELA_DESCRICAO',prop:'Titleformat'},{av:'edtTabela_Descricao_Title',ctrl:'TABELA_DESCRICAO',prop:'Title'},{av:'edtTabela_SistemaDes_Titleformat',ctrl:'TABELA_SISTEMADES',prop:'Titleformat'},{av:'edtTabela_SistemaDes_Title',ctrl:'TABELA_SISTEMADES',prop:'Title'},{av:'edtTabela_ModuloDes_Titleformat',ctrl:'TABELA_MODULODES',prop:'Titleformat'},{av:'edtTabela_ModuloDes_Title',ctrl:'TABELA_MODULODES',prop:'Title'},{av:'edtTabela_PaiNom_Titleformat',ctrl:'TABELA_PAINOM',prop:'Titleformat'},{av:'edtTabela_PaiNom_Title',ctrl:'TABELA_PAINOM',prop:'Title'},{av:'edtTabela_MelhoraCod_Titleformat',ctrl:'TABELA_MELHORACOD',prop:'Titleformat'},{av:'edtTabela_MelhoraCod_Title',ctrl:'TABELA_MELHORACOD',prop:'Title'},{av:'chkTabela_Ativo_Titleformat',ctrl:'TABELA_ATIVO',prop:'Titleformat'},{av:'chkTabela_Ativo.Title.Text',ctrl:'TABELA_ATIVO',prop:'Title'},{av:'AV94GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV95GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV61Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV101ManageFiltersData',fld:'vMANAGEFILTERSDATA',pic:'',nv:null},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E12462',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV61Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Tabela_Nome1',fld:'vTABELA_NOME1',pic:'@!',nv:''},{av:'AV96Tabela_ModuloDes1',fld:'vTABELA_MODULODES1',pic:'@!',nv:''},{av:'AV62Tabela_ModuloCod1',fld:'vTABELA_MODULOCOD1',pic:'ZZZZZ9',nv:0},{av:'AV70TFTabela_Nome',fld:'vTFTABELA_NOME',pic:'@!',nv:''},{av:'AV71TFTabela_Nome_Sel',fld:'vTFTABELA_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFTabela_Descricao',fld:'vTFTABELA_DESCRICAO',pic:'',nv:''},{av:'AV75TFTabela_Descricao_Sel',fld:'vTFTABELA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV78TFTabela_SistemaDes',fld:'vTFTABELA_SISTEMADES',pic:'@!',nv:''},{av:'AV79TFTabela_SistemaDes_Sel',fld:'vTFTABELA_SISTEMADES_SEL',pic:'@!',nv:''},{av:'AV82TFTabela_ModuloDes',fld:'vTFTABELA_MODULODES',pic:'@!',nv:''},{av:'AV83TFTabela_ModuloDes_Sel',fld:'vTFTABELA_MODULODES_SEL',pic:'@!',nv:''},{av:'AV86TFTabela_PaiNom',fld:'vTFTABELA_PAINOM',pic:'@!',nv:''},{av:'AV87TFTabela_PaiNom_Sel',fld:'vTFTABELA_PAINOM_SEL',pic:'@!',nv:''},{av:'AV104TFTabela_MelhoraCod',fld:'vTFTABELA_MELHORACOD',pic:'ZZZZZ9',nv:0},{av:'AV105TFTabela_MelhoraCod_To',fld:'vTFTABELA_MELHORACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV90TFTabela_Ativo_Sel',fld:'vTFTABELA_ATIVO_SEL',pic:'9',nv:0},{av:'AV97ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV72ddo_Tabela_NomeTitleControlIdToReplace',fld:'vDDO_TABELA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_Tabela_DescricaoTitleControlIdToReplace',fld:'vDDO_TABELA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_Tabela_SistemaDesTitleControlIdToReplace',fld:'vDDO_TABELA_SISTEMADESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Tabela_ModuloDesTitleControlIdToReplace',fld:'vDDO_TABELA_MODULODESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_Tabela_PaiNomTitleControlIdToReplace',fld:'vDDO_TABELA_PAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_Tabela_MelhoraCodTitleControlIdToReplace',fld:'vDDO_TABELA_MELHORACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_Tabela_AtivoTitleControlIdToReplace',fld:'vDDO_TABELA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV131Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV65Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0},{av:'A188Tabela_ModuloCod',fld:'TABELA_MODULOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A181Tabela_PaiCod',fld:'TABELA_PAICOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_TABELA_NOME.ONOPTIONCLICKED","{handler:'E13462',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV61Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Tabela_Nome1',fld:'vTABELA_NOME1',pic:'@!',nv:''},{av:'AV96Tabela_ModuloDes1',fld:'vTABELA_MODULODES1',pic:'@!',nv:''},{av:'AV62Tabela_ModuloCod1',fld:'vTABELA_MODULOCOD1',pic:'ZZZZZ9',nv:0},{av:'AV70TFTabela_Nome',fld:'vTFTABELA_NOME',pic:'@!',nv:''},{av:'AV71TFTabela_Nome_Sel',fld:'vTFTABELA_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFTabela_Descricao',fld:'vTFTABELA_DESCRICAO',pic:'',nv:''},{av:'AV75TFTabela_Descricao_Sel',fld:'vTFTABELA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV78TFTabela_SistemaDes',fld:'vTFTABELA_SISTEMADES',pic:'@!',nv:''},{av:'AV79TFTabela_SistemaDes_Sel',fld:'vTFTABELA_SISTEMADES_SEL',pic:'@!',nv:''},{av:'AV82TFTabela_ModuloDes',fld:'vTFTABELA_MODULODES',pic:'@!',nv:''},{av:'AV83TFTabela_ModuloDes_Sel',fld:'vTFTABELA_MODULODES_SEL',pic:'@!',nv:''},{av:'AV86TFTabela_PaiNom',fld:'vTFTABELA_PAINOM',pic:'@!',nv:''},{av:'AV87TFTabela_PaiNom_Sel',fld:'vTFTABELA_PAINOM_SEL',pic:'@!',nv:''},{av:'AV104TFTabela_MelhoraCod',fld:'vTFTABELA_MELHORACOD',pic:'ZZZZZ9',nv:0},{av:'AV105TFTabela_MelhoraCod_To',fld:'vTFTABELA_MELHORACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV90TFTabela_Ativo_Sel',fld:'vTFTABELA_ATIVO_SEL',pic:'9',nv:0},{av:'AV97ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV72ddo_Tabela_NomeTitleControlIdToReplace',fld:'vDDO_TABELA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_Tabela_DescricaoTitleControlIdToReplace',fld:'vDDO_TABELA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_Tabela_SistemaDesTitleControlIdToReplace',fld:'vDDO_TABELA_SISTEMADESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Tabela_ModuloDesTitleControlIdToReplace',fld:'vDDO_TABELA_MODULODESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_Tabela_PaiNomTitleControlIdToReplace',fld:'vDDO_TABELA_PAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_Tabela_MelhoraCodTitleControlIdToReplace',fld:'vDDO_TABELA_MELHORACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_Tabela_AtivoTitleControlIdToReplace',fld:'vDDO_TABELA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV131Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV65Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0},{av:'A188Tabela_ModuloCod',fld:'TABELA_MODULOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A181Tabela_PaiCod',fld:'TABELA_PAICOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_tabela_nome_Activeeventkey',ctrl:'DDO_TABELA_NOME',prop:'ActiveEventKey'},{av:'Ddo_tabela_nome_Filteredtext_get',ctrl:'DDO_TABELA_NOME',prop:'FilteredText_get'},{av:'Ddo_tabela_nome_Selectedvalue_get',ctrl:'DDO_TABELA_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_tabela_nome_Sortedstatus',ctrl:'DDO_TABELA_NOME',prop:'SortedStatus'},{av:'AV70TFTabela_Nome',fld:'vTFTABELA_NOME',pic:'@!',nv:''},{av:'AV71TFTabela_Nome_Sel',fld:'vTFTABELA_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_tabela_descricao_Sortedstatus',ctrl:'DDO_TABELA_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_tabela_sistemades_Sortedstatus',ctrl:'DDO_TABELA_SISTEMADES',prop:'SortedStatus'},{av:'Ddo_tabela_modulodes_Sortedstatus',ctrl:'DDO_TABELA_MODULODES',prop:'SortedStatus'},{av:'Ddo_tabela_painom_Sortedstatus',ctrl:'DDO_TABELA_PAINOM',prop:'SortedStatus'},{av:'Ddo_tabela_melhoracod_Sortedstatus',ctrl:'DDO_TABELA_MELHORACOD',prop:'SortedStatus'},{av:'Ddo_tabela_ativo_Sortedstatus',ctrl:'DDO_TABELA_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_TABELA_DESCRICAO.ONOPTIONCLICKED","{handler:'E14462',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV61Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Tabela_Nome1',fld:'vTABELA_NOME1',pic:'@!',nv:''},{av:'AV96Tabela_ModuloDes1',fld:'vTABELA_MODULODES1',pic:'@!',nv:''},{av:'AV62Tabela_ModuloCod1',fld:'vTABELA_MODULOCOD1',pic:'ZZZZZ9',nv:0},{av:'AV70TFTabela_Nome',fld:'vTFTABELA_NOME',pic:'@!',nv:''},{av:'AV71TFTabela_Nome_Sel',fld:'vTFTABELA_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFTabela_Descricao',fld:'vTFTABELA_DESCRICAO',pic:'',nv:''},{av:'AV75TFTabela_Descricao_Sel',fld:'vTFTABELA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV78TFTabela_SistemaDes',fld:'vTFTABELA_SISTEMADES',pic:'@!',nv:''},{av:'AV79TFTabela_SistemaDes_Sel',fld:'vTFTABELA_SISTEMADES_SEL',pic:'@!',nv:''},{av:'AV82TFTabela_ModuloDes',fld:'vTFTABELA_MODULODES',pic:'@!',nv:''},{av:'AV83TFTabela_ModuloDes_Sel',fld:'vTFTABELA_MODULODES_SEL',pic:'@!',nv:''},{av:'AV86TFTabela_PaiNom',fld:'vTFTABELA_PAINOM',pic:'@!',nv:''},{av:'AV87TFTabela_PaiNom_Sel',fld:'vTFTABELA_PAINOM_SEL',pic:'@!',nv:''},{av:'AV104TFTabela_MelhoraCod',fld:'vTFTABELA_MELHORACOD',pic:'ZZZZZ9',nv:0},{av:'AV105TFTabela_MelhoraCod_To',fld:'vTFTABELA_MELHORACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV90TFTabela_Ativo_Sel',fld:'vTFTABELA_ATIVO_SEL',pic:'9',nv:0},{av:'AV97ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV72ddo_Tabela_NomeTitleControlIdToReplace',fld:'vDDO_TABELA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_Tabela_DescricaoTitleControlIdToReplace',fld:'vDDO_TABELA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_Tabela_SistemaDesTitleControlIdToReplace',fld:'vDDO_TABELA_SISTEMADESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Tabela_ModuloDesTitleControlIdToReplace',fld:'vDDO_TABELA_MODULODESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_Tabela_PaiNomTitleControlIdToReplace',fld:'vDDO_TABELA_PAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_Tabela_MelhoraCodTitleControlIdToReplace',fld:'vDDO_TABELA_MELHORACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_Tabela_AtivoTitleControlIdToReplace',fld:'vDDO_TABELA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV131Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV65Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0},{av:'A188Tabela_ModuloCod',fld:'TABELA_MODULOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A181Tabela_PaiCod',fld:'TABELA_PAICOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_tabela_descricao_Activeeventkey',ctrl:'DDO_TABELA_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_tabela_descricao_Filteredtext_get',ctrl:'DDO_TABELA_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_tabela_descricao_Selectedvalue_get',ctrl:'DDO_TABELA_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_tabela_descricao_Sortedstatus',ctrl:'DDO_TABELA_DESCRICAO',prop:'SortedStatus'},{av:'AV74TFTabela_Descricao',fld:'vTFTABELA_DESCRICAO',pic:'',nv:''},{av:'AV75TFTabela_Descricao_Sel',fld:'vTFTABELA_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_tabela_nome_Sortedstatus',ctrl:'DDO_TABELA_NOME',prop:'SortedStatus'},{av:'Ddo_tabela_sistemades_Sortedstatus',ctrl:'DDO_TABELA_SISTEMADES',prop:'SortedStatus'},{av:'Ddo_tabela_modulodes_Sortedstatus',ctrl:'DDO_TABELA_MODULODES',prop:'SortedStatus'},{av:'Ddo_tabela_painom_Sortedstatus',ctrl:'DDO_TABELA_PAINOM',prop:'SortedStatus'},{av:'Ddo_tabela_melhoracod_Sortedstatus',ctrl:'DDO_TABELA_MELHORACOD',prop:'SortedStatus'},{av:'Ddo_tabela_ativo_Sortedstatus',ctrl:'DDO_TABELA_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_TABELA_SISTEMADES.ONOPTIONCLICKED","{handler:'E15462',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV61Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Tabela_Nome1',fld:'vTABELA_NOME1',pic:'@!',nv:''},{av:'AV96Tabela_ModuloDes1',fld:'vTABELA_MODULODES1',pic:'@!',nv:''},{av:'AV62Tabela_ModuloCod1',fld:'vTABELA_MODULOCOD1',pic:'ZZZZZ9',nv:0},{av:'AV70TFTabela_Nome',fld:'vTFTABELA_NOME',pic:'@!',nv:''},{av:'AV71TFTabela_Nome_Sel',fld:'vTFTABELA_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFTabela_Descricao',fld:'vTFTABELA_DESCRICAO',pic:'',nv:''},{av:'AV75TFTabela_Descricao_Sel',fld:'vTFTABELA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV78TFTabela_SistemaDes',fld:'vTFTABELA_SISTEMADES',pic:'@!',nv:''},{av:'AV79TFTabela_SistemaDes_Sel',fld:'vTFTABELA_SISTEMADES_SEL',pic:'@!',nv:''},{av:'AV82TFTabela_ModuloDes',fld:'vTFTABELA_MODULODES',pic:'@!',nv:''},{av:'AV83TFTabela_ModuloDes_Sel',fld:'vTFTABELA_MODULODES_SEL',pic:'@!',nv:''},{av:'AV86TFTabela_PaiNom',fld:'vTFTABELA_PAINOM',pic:'@!',nv:''},{av:'AV87TFTabela_PaiNom_Sel',fld:'vTFTABELA_PAINOM_SEL',pic:'@!',nv:''},{av:'AV104TFTabela_MelhoraCod',fld:'vTFTABELA_MELHORACOD',pic:'ZZZZZ9',nv:0},{av:'AV105TFTabela_MelhoraCod_To',fld:'vTFTABELA_MELHORACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV90TFTabela_Ativo_Sel',fld:'vTFTABELA_ATIVO_SEL',pic:'9',nv:0},{av:'AV97ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV72ddo_Tabela_NomeTitleControlIdToReplace',fld:'vDDO_TABELA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_Tabela_DescricaoTitleControlIdToReplace',fld:'vDDO_TABELA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_Tabela_SistemaDesTitleControlIdToReplace',fld:'vDDO_TABELA_SISTEMADESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Tabela_ModuloDesTitleControlIdToReplace',fld:'vDDO_TABELA_MODULODESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_Tabela_PaiNomTitleControlIdToReplace',fld:'vDDO_TABELA_PAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_Tabela_MelhoraCodTitleControlIdToReplace',fld:'vDDO_TABELA_MELHORACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_Tabela_AtivoTitleControlIdToReplace',fld:'vDDO_TABELA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV131Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV65Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0},{av:'A188Tabela_ModuloCod',fld:'TABELA_MODULOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A181Tabela_PaiCod',fld:'TABELA_PAICOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_tabela_sistemades_Activeeventkey',ctrl:'DDO_TABELA_SISTEMADES',prop:'ActiveEventKey'},{av:'Ddo_tabela_sistemades_Filteredtext_get',ctrl:'DDO_TABELA_SISTEMADES',prop:'FilteredText_get'},{av:'Ddo_tabela_sistemades_Selectedvalue_get',ctrl:'DDO_TABELA_SISTEMADES',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_tabela_sistemades_Sortedstatus',ctrl:'DDO_TABELA_SISTEMADES',prop:'SortedStatus'},{av:'AV78TFTabela_SistemaDes',fld:'vTFTABELA_SISTEMADES',pic:'@!',nv:''},{av:'AV79TFTabela_SistemaDes_Sel',fld:'vTFTABELA_SISTEMADES_SEL',pic:'@!',nv:''},{av:'Ddo_tabela_nome_Sortedstatus',ctrl:'DDO_TABELA_NOME',prop:'SortedStatus'},{av:'Ddo_tabela_descricao_Sortedstatus',ctrl:'DDO_TABELA_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_tabela_modulodes_Sortedstatus',ctrl:'DDO_TABELA_MODULODES',prop:'SortedStatus'},{av:'Ddo_tabela_painom_Sortedstatus',ctrl:'DDO_TABELA_PAINOM',prop:'SortedStatus'},{av:'Ddo_tabela_melhoracod_Sortedstatus',ctrl:'DDO_TABELA_MELHORACOD',prop:'SortedStatus'},{av:'Ddo_tabela_ativo_Sortedstatus',ctrl:'DDO_TABELA_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_TABELA_MODULODES.ONOPTIONCLICKED","{handler:'E16462',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV61Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Tabela_Nome1',fld:'vTABELA_NOME1',pic:'@!',nv:''},{av:'AV96Tabela_ModuloDes1',fld:'vTABELA_MODULODES1',pic:'@!',nv:''},{av:'AV62Tabela_ModuloCod1',fld:'vTABELA_MODULOCOD1',pic:'ZZZZZ9',nv:0},{av:'AV70TFTabela_Nome',fld:'vTFTABELA_NOME',pic:'@!',nv:''},{av:'AV71TFTabela_Nome_Sel',fld:'vTFTABELA_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFTabela_Descricao',fld:'vTFTABELA_DESCRICAO',pic:'',nv:''},{av:'AV75TFTabela_Descricao_Sel',fld:'vTFTABELA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV78TFTabela_SistemaDes',fld:'vTFTABELA_SISTEMADES',pic:'@!',nv:''},{av:'AV79TFTabela_SistemaDes_Sel',fld:'vTFTABELA_SISTEMADES_SEL',pic:'@!',nv:''},{av:'AV82TFTabela_ModuloDes',fld:'vTFTABELA_MODULODES',pic:'@!',nv:''},{av:'AV83TFTabela_ModuloDes_Sel',fld:'vTFTABELA_MODULODES_SEL',pic:'@!',nv:''},{av:'AV86TFTabela_PaiNom',fld:'vTFTABELA_PAINOM',pic:'@!',nv:''},{av:'AV87TFTabela_PaiNom_Sel',fld:'vTFTABELA_PAINOM_SEL',pic:'@!',nv:''},{av:'AV104TFTabela_MelhoraCod',fld:'vTFTABELA_MELHORACOD',pic:'ZZZZZ9',nv:0},{av:'AV105TFTabela_MelhoraCod_To',fld:'vTFTABELA_MELHORACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV90TFTabela_Ativo_Sel',fld:'vTFTABELA_ATIVO_SEL',pic:'9',nv:0},{av:'AV97ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV72ddo_Tabela_NomeTitleControlIdToReplace',fld:'vDDO_TABELA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_Tabela_DescricaoTitleControlIdToReplace',fld:'vDDO_TABELA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_Tabela_SistemaDesTitleControlIdToReplace',fld:'vDDO_TABELA_SISTEMADESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Tabela_ModuloDesTitleControlIdToReplace',fld:'vDDO_TABELA_MODULODESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_Tabela_PaiNomTitleControlIdToReplace',fld:'vDDO_TABELA_PAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_Tabela_MelhoraCodTitleControlIdToReplace',fld:'vDDO_TABELA_MELHORACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_Tabela_AtivoTitleControlIdToReplace',fld:'vDDO_TABELA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV131Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV65Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0},{av:'A188Tabela_ModuloCod',fld:'TABELA_MODULOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A181Tabela_PaiCod',fld:'TABELA_PAICOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_tabela_modulodes_Activeeventkey',ctrl:'DDO_TABELA_MODULODES',prop:'ActiveEventKey'},{av:'Ddo_tabela_modulodes_Filteredtext_get',ctrl:'DDO_TABELA_MODULODES',prop:'FilteredText_get'},{av:'Ddo_tabela_modulodes_Selectedvalue_get',ctrl:'DDO_TABELA_MODULODES',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_tabela_modulodes_Sortedstatus',ctrl:'DDO_TABELA_MODULODES',prop:'SortedStatus'},{av:'AV82TFTabela_ModuloDes',fld:'vTFTABELA_MODULODES',pic:'@!',nv:''},{av:'AV83TFTabela_ModuloDes_Sel',fld:'vTFTABELA_MODULODES_SEL',pic:'@!',nv:''},{av:'Ddo_tabela_nome_Sortedstatus',ctrl:'DDO_TABELA_NOME',prop:'SortedStatus'},{av:'Ddo_tabela_descricao_Sortedstatus',ctrl:'DDO_TABELA_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_tabela_sistemades_Sortedstatus',ctrl:'DDO_TABELA_SISTEMADES',prop:'SortedStatus'},{av:'Ddo_tabela_painom_Sortedstatus',ctrl:'DDO_TABELA_PAINOM',prop:'SortedStatus'},{av:'Ddo_tabela_melhoracod_Sortedstatus',ctrl:'DDO_TABELA_MELHORACOD',prop:'SortedStatus'},{av:'Ddo_tabela_ativo_Sortedstatus',ctrl:'DDO_TABELA_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_TABELA_PAINOM.ONOPTIONCLICKED","{handler:'E17462',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV61Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Tabela_Nome1',fld:'vTABELA_NOME1',pic:'@!',nv:''},{av:'AV96Tabela_ModuloDes1',fld:'vTABELA_MODULODES1',pic:'@!',nv:''},{av:'AV62Tabela_ModuloCod1',fld:'vTABELA_MODULOCOD1',pic:'ZZZZZ9',nv:0},{av:'AV70TFTabela_Nome',fld:'vTFTABELA_NOME',pic:'@!',nv:''},{av:'AV71TFTabela_Nome_Sel',fld:'vTFTABELA_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFTabela_Descricao',fld:'vTFTABELA_DESCRICAO',pic:'',nv:''},{av:'AV75TFTabela_Descricao_Sel',fld:'vTFTABELA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV78TFTabela_SistemaDes',fld:'vTFTABELA_SISTEMADES',pic:'@!',nv:''},{av:'AV79TFTabela_SistemaDes_Sel',fld:'vTFTABELA_SISTEMADES_SEL',pic:'@!',nv:''},{av:'AV82TFTabela_ModuloDes',fld:'vTFTABELA_MODULODES',pic:'@!',nv:''},{av:'AV83TFTabela_ModuloDes_Sel',fld:'vTFTABELA_MODULODES_SEL',pic:'@!',nv:''},{av:'AV86TFTabela_PaiNom',fld:'vTFTABELA_PAINOM',pic:'@!',nv:''},{av:'AV87TFTabela_PaiNom_Sel',fld:'vTFTABELA_PAINOM_SEL',pic:'@!',nv:''},{av:'AV104TFTabela_MelhoraCod',fld:'vTFTABELA_MELHORACOD',pic:'ZZZZZ9',nv:0},{av:'AV105TFTabela_MelhoraCod_To',fld:'vTFTABELA_MELHORACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV90TFTabela_Ativo_Sel',fld:'vTFTABELA_ATIVO_SEL',pic:'9',nv:0},{av:'AV97ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV72ddo_Tabela_NomeTitleControlIdToReplace',fld:'vDDO_TABELA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_Tabela_DescricaoTitleControlIdToReplace',fld:'vDDO_TABELA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_Tabela_SistemaDesTitleControlIdToReplace',fld:'vDDO_TABELA_SISTEMADESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Tabela_ModuloDesTitleControlIdToReplace',fld:'vDDO_TABELA_MODULODESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_Tabela_PaiNomTitleControlIdToReplace',fld:'vDDO_TABELA_PAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_Tabela_MelhoraCodTitleControlIdToReplace',fld:'vDDO_TABELA_MELHORACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_Tabela_AtivoTitleControlIdToReplace',fld:'vDDO_TABELA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV131Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV65Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0},{av:'A188Tabela_ModuloCod',fld:'TABELA_MODULOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A181Tabela_PaiCod',fld:'TABELA_PAICOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_tabela_painom_Activeeventkey',ctrl:'DDO_TABELA_PAINOM',prop:'ActiveEventKey'},{av:'Ddo_tabela_painom_Filteredtext_get',ctrl:'DDO_TABELA_PAINOM',prop:'FilteredText_get'},{av:'Ddo_tabela_painom_Selectedvalue_get',ctrl:'DDO_TABELA_PAINOM',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_tabela_painom_Sortedstatus',ctrl:'DDO_TABELA_PAINOM',prop:'SortedStatus'},{av:'AV86TFTabela_PaiNom',fld:'vTFTABELA_PAINOM',pic:'@!',nv:''},{av:'AV87TFTabela_PaiNom_Sel',fld:'vTFTABELA_PAINOM_SEL',pic:'@!',nv:''},{av:'Ddo_tabela_nome_Sortedstatus',ctrl:'DDO_TABELA_NOME',prop:'SortedStatus'},{av:'Ddo_tabela_descricao_Sortedstatus',ctrl:'DDO_TABELA_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_tabela_sistemades_Sortedstatus',ctrl:'DDO_TABELA_SISTEMADES',prop:'SortedStatus'},{av:'Ddo_tabela_modulodes_Sortedstatus',ctrl:'DDO_TABELA_MODULODES',prop:'SortedStatus'},{av:'Ddo_tabela_melhoracod_Sortedstatus',ctrl:'DDO_TABELA_MELHORACOD',prop:'SortedStatus'},{av:'Ddo_tabela_ativo_Sortedstatus',ctrl:'DDO_TABELA_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_TABELA_MELHORACOD.ONOPTIONCLICKED","{handler:'E18462',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV61Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Tabela_Nome1',fld:'vTABELA_NOME1',pic:'@!',nv:''},{av:'AV96Tabela_ModuloDes1',fld:'vTABELA_MODULODES1',pic:'@!',nv:''},{av:'AV62Tabela_ModuloCod1',fld:'vTABELA_MODULOCOD1',pic:'ZZZZZ9',nv:0},{av:'AV70TFTabela_Nome',fld:'vTFTABELA_NOME',pic:'@!',nv:''},{av:'AV71TFTabela_Nome_Sel',fld:'vTFTABELA_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFTabela_Descricao',fld:'vTFTABELA_DESCRICAO',pic:'',nv:''},{av:'AV75TFTabela_Descricao_Sel',fld:'vTFTABELA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV78TFTabela_SistemaDes',fld:'vTFTABELA_SISTEMADES',pic:'@!',nv:''},{av:'AV79TFTabela_SistemaDes_Sel',fld:'vTFTABELA_SISTEMADES_SEL',pic:'@!',nv:''},{av:'AV82TFTabela_ModuloDes',fld:'vTFTABELA_MODULODES',pic:'@!',nv:''},{av:'AV83TFTabela_ModuloDes_Sel',fld:'vTFTABELA_MODULODES_SEL',pic:'@!',nv:''},{av:'AV86TFTabela_PaiNom',fld:'vTFTABELA_PAINOM',pic:'@!',nv:''},{av:'AV87TFTabela_PaiNom_Sel',fld:'vTFTABELA_PAINOM_SEL',pic:'@!',nv:''},{av:'AV104TFTabela_MelhoraCod',fld:'vTFTABELA_MELHORACOD',pic:'ZZZZZ9',nv:0},{av:'AV105TFTabela_MelhoraCod_To',fld:'vTFTABELA_MELHORACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV90TFTabela_Ativo_Sel',fld:'vTFTABELA_ATIVO_SEL',pic:'9',nv:0},{av:'AV97ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV72ddo_Tabela_NomeTitleControlIdToReplace',fld:'vDDO_TABELA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_Tabela_DescricaoTitleControlIdToReplace',fld:'vDDO_TABELA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_Tabela_SistemaDesTitleControlIdToReplace',fld:'vDDO_TABELA_SISTEMADESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Tabela_ModuloDesTitleControlIdToReplace',fld:'vDDO_TABELA_MODULODESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_Tabela_PaiNomTitleControlIdToReplace',fld:'vDDO_TABELA_PAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_Tabela_MelhoraCodTitleControlIdToReplace',fld:'vDDO_TABELA_MELHORACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_Tabela_AtivoTitleControlIdToReplace',fld:'vDDO_TABELA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV131Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV65Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0},{av:'A188Tabela_ModuloCod',fld:'TABELA_MODULOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A181Tabela_PaiCod',fld:'TABELA_PAICOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_tabela_melhoracod_Activeeventkey',ctrl:'DDO_TABELA_MELHORACOD',prop:'ActiveEventKey'},{av:'Ddo_tabela_melhoracod_Filteredtext_get',ctrl:'DDO_TABELA_MELHORACOD',prop:'FilteredText_get'},{av:'Ddo_tabela_melhoracod_Filteredtextto_get',ctrl:'DDO_TABELA_MELHORACOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_tabela_melhoracod_Sortedstatus',ctrl:'DDO_TABELA_MELHORACOD',prop:'SortedStatus'},{av:'AV104TFTabela_MelhoraCod',fld:'vTFTABELA_MELHORACOD',pic:'ZZZZZ9',nv:0},{av:'AV105TFTabela_MelhoraCod_To',fld:'vTFTABELA_MELHORACOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_tabela_nome_Sortedstatus',ctrl:'DDO_TABELA_NOME',prop:'SortedStatus'},{av:'Ddo_tabela_descricao_Sortedstatus',ctrl:'DDO_TABELA_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_tabela_sistemades_Sortedstatus',ctrl:'DDO_TABELA_SISTEMADES',prop:'SortedStatus'},{av:'Ddo_tabela_modulodes_Sortedstatus',ctrl:'DDO_TABELA_MODULODES',prop:'SortedStatus'},{av:'Ddo_tabela_painom_Sortedstatus',ctrl:'DDO_TABELA_PAINOM',prop:'SortedStatus'},{av:'Ddo_tabela_ativo_Sortedstatus',ctrl:'DDO_TABELA_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_TABELA_ATIVO.ONOPTIONCLICKED","{handler:'E19462',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV61Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Tabela_Nome1',fld:'vTABELA_NOME1',pic:'@!',nv:''},{av:'AV96Tabela_ModuloDes1',fld:'vTABELA_MODULODES1',pic:'@!',nv:''},{av:'AV62Tabela_ModuloCod1',fld:'vTABELA_MODULOCOD1',pic:'ZZZZZ9',nv:0},{av:'AV70TFTabela_Nome',fld:'vTFTABELA_NOME',pic:'@!',nv:''},{av:'AV71TFTabela_Nome_Sel',fld:'vTFTABELA_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFTabela_Descricao',fld:'vTFTABELA_DESCRICAO',pic:'',nv:''},{av:'AV75TFTabela_Descricao_Sel',fld:'vTFTABELA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV78TFTabela_SistemaDes',fld:'vTFTABELA_SISTEMADES',pic:'@!',nv:''},{av:'AV79TFTabela_SistemaDes_Sel',fld:'vTFTABELA_SISTEMADES_SEL',pic:'@!',nv:''},{av:'AV82TFTabela_ModuloDes',fld:'vTFTABELA_MODULODES',pic:'@!',nv:''},{av:'AV83TFTabela_ModuloDes_Sel',fld:'vTFTABELA_MODULODES_SEL',pic:'@!',nv:''},{av:'AV86TFTabela_PaiNom',fld:'vTFTABELA_PAINOM',pic:'@!',nv:''},{av:'AV87TFTabela_PaiNom_Sel',fld:'vTFTABELA_PAINOM_SEL',pic:'@!',nv:''},{av:'AV104TFTabela_MelhoraCod',fld:'vTFTABELA_MELHORACOD',pic:'ZZZZZ9',nv:0},{av:'AV105TFTabela_MelhoraCod_To',fld:'vTFTABELA_MELHORACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV90TFTabela_Ativo_Sel',fld:'vTFTABELA_ATIVO_SEL',pic:'9',nv:0},{av:'AV97ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV72ddo_Tabela_NomeTitleControlIdToReplace',fld:'vDDO_TABELA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_Tabela_DescricaoTitleControlIdToReplace',fld:'vDDO_TABELA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_Tabela_SistemaDesTitleControlIdToReplace',fld:'vDDO_TABELA_SISTEMADESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Tabela_ModuloDesTitleControlIdToReplace',fld:'vDDO_TABELA_MODULODESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_Tabela_PaiNomTitleControlIdToReplace',fld:'vDDO_TABELA_PAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_Tabela_MelhoraCodTitleControlIdToReplace',fld:'vDDO_TABELA_MELHORACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_Tabela_AtivoTitleControlIdToReplace',fld:'vDDO_TABELA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV131Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV65Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0},{av:'A188Tabela_ModuloCod',fld:'TABELA_MODULOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A181Tabela_PaiCod',fld:'TABELA_PAICOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_tabela_ativo_Activeeventkey',ctrl:'DDO_TABELA_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_tabela_ativo_Selectedvalue_get',ctrl:'DDO_TABELA_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_tabela_ativo_Sortedstatus',ctrl:'DDO_TABELA_ATIVO',prop:'SortedStatus'},{av:'AV90TFTabela_Ativo_Sel',fld:'vTFTABELA_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_tabela_nome_Sortedstatus',ctrl:'DDO_TABELA_NOME',prop:'SortedStatus'},{av:'Ddo_tabela_descricao_Sortedstatus',ctrl:'DDO_TABELA_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_tabela_sistemades_Sortedstatus',ctrl:'DDO_TABELA_SISTEMADES',prop:'SortedStatus'},{av:'Ddo_tabela_modulodes_Sortedstatus',ctrl:'DDO_TABELA_MODULODES',prop:'SortedStatus'},{av:'Ddo_tabela_painom_Sortedstatus',ctrl:'DDO_TABELA_PAINOM',prop:'SortedStatus'},{av:'Ddo_tabela_melhoracod_Sortedstatus',ctrl:'DDO_TABELA_MELHORACOD',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E25462',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV65Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0},{av:'A188Tabela_ModuloCod',fld:'TABELA_MODULOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A181Tabela_PaiCod',fld:'TABELA_PAICOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV37Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV38Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'AV66Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Enabled',ctrl:'vDISPLAY',prop:'Enabled'},{av:'edtTabela_Nome_Link',ctrl:'TABELA_NOME',prop:'Link'},{av:'edtTabela_ModuloDes_Link',ctrl:'TABELA_MODULODES',prop:'Link'},{av:'edtTabela_PaiNom_Link',ctrl:'TABELA_PAINOM',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E20462',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV61Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Tabela_Nome1',fld:'vTABELA_NOME1',pic:'@!',nv:''},{av:'AV96Tabela_ModuloDes1',fld:'vTABELA_MODULODES1',pic:'@!',nv:''},{av:'AV62Tabela_ModuloCod1',fld:'vTABELA_MODULOCOD1',pic:'ZZZZZ9',nv:0},{av:'AV70TFTabela_Nome',fld:'vTFTABELA_NOME',pic:'@!',nv:''},{av:'AV71TFTabela_Nome_Sel',fld:'vTFTABELA_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFTabela_Descricao',fld:'vTFTABELA_DESCRICAO',pic:'',nv:''},{av:'AV75TFTabela_Descricao_Sel',fld:'vTFTABELA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV78TFTabela_SistemaDes',fld:'vTFTABELA_SISTEMADES',pic:'@!',nv:''},{av:'AV79TFTabela_SistemaDes_Sel',fld:'vTFTABELA_SISTEMADES_SEL',pic:'@!',nv:''},{av:'AV82TFTabela_ModuloDes',fld:'vTFTABELA_MODULODES',pic:'@!',nv:''},{av:'AV83TFTabela_ModuloDes_Sel',fld:'vTFTABELA_MODULODES_SEL',pic:'@!',nv:''},{av:'AV86TFTabela_PaiNom',fld:'vTFTABELA_PAINOM',pic:'@!',nv:''},{av:'AV87TFTabela_PaiNom_Sel',fld:'vTFTABELA_PAINOM_SEL',pic:'@!',nv:''},{av:'AV104TFTabela_MelhoraCod',fld:'vTFTABELA_MELHORACOD',pic:'ZZZZZ9',nv:0},{av:'AV105TFTabela_MelhoraCod_To',fld:'vTFTABELA_MELHORACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV90TFTabela_Ativo_Sel',fld:'vTFTABELA_ATIVO_SEL',pic:'9',nv:0},{av:'AV97ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV72ddo_Tabela_NomeTitleControlIdToReplace',fld:'vDDO_TABELA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_Tabela_DescricaoTitleControlIdToReplace',fld:'vDDO_TABELA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_Tabela_SistemaDesTitleControlIdToReplace',fld:'vDDO_TABELA_SISTEMADESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Tabela_ModuloDesTitleControlIdToReplace',fld:'vDDO_TABELA_MODULODESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_Tabela_PaiNomTitleControlIdToReplace',fld:'vDDO_TABELA_PAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_Tabela_MelhoraCodTitleControlIdToReplace',fld:'vDDO_TABELA_MELHORACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_Tabela_AtivoTitleControlIdToReplace',fld:'vDDO_TABELA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV131Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV65Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0},{av:'A188Tabela_ModuloCod',fld:'TABELA_MODULOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A181Tabela_PaiCod',fld:'TABELA_PAICOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E22462',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'edtavTabela_nome1_Visible',ctrl:'vTABELA_NOME1',prop:'Visible'},{av:'edtavTabela_modulodes1_Visible',ctrl:'vTABELA_MODULODES1',prop:'Visible'},{av:'dynavTabela_modulocod1'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("DDO_MANAGEFILTERS.ONOPTIONCLICKED","{handler:'E11462',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV61Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Tabela_Nome1',fld:'vTABELA_NOME1',pic:'@!',nv:''},{av:'AV96Tabela_ModuloDes1',fld:'vTABELA_MODULODES1',pic:'@!',nv:''},{av:'AV62Tabela_ModuloCod1',fld:'vTABELA_MODULOCOD1',pic:'ZZZZZ9',nv:0},{av:'AV70TFTabela_Nome',fld:'vTFTABELA_NOME',pic:'@!',nv:''},{av:'AV71TFTabela_Nome_Sel',fld:'vTFTABELA_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFTabela_Descricao',fld:'vTFTABELA_DESCRICAO',pic:'',nv:''},{av:'AV75TFTabela_Descricao_Sel',fld:'vTFTABELA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV78TFTabela_SistemaDes',fld:'vTFTABELA_SISTEMADES',pic:'@!',nv:''},{av:'AV79TFTabela_SistemaDes_Sel',fld:'vTFTABELA_SISTEMADES_SEL',pic:'@!',nv:''},{av:'AV82TFTabela_ModuloDes',fld:'vTFTABELA_MODULODES',pic:'@!',nv:''},{av:'AV83TFTabela_ModuloDes_Sel',fld:'vTFTABELA_MODULODES_SEL',pic:'@!',nv:''},{av:'AV86TFTabela_PaiNom',fld:'vTFTABELA_PAINOM',pic:'@!',nv:''},{av:'AV87TFTabela_PaiNom_Sel',fld:'vTFTABELA_PAINOM_SEL',pic:'@!',nv:''},{av:'AV104TFTabela_MelhoraCod',fld:'vTFTABELA_MELHORACOD',pic:'ZZZZZ9',nv:0},{av:'AV105TFTabela_MelhoraCod_To',fld:'vTFTABELA_MELHORACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV90TFTabela_Ativo_Sel',fld:'vTFTABELA_ATIVO_SEL',pic:'9',nv:0},{av:'AV97ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV72ddo_Tabela_NomeTitleControlIdToReplace',fld:'vDDO_TABELA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_Tabela_DescricaoTitleControlIdToReplace',fld:'vDDO_TABELA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_Tabela_SistemaDesTitleControlIdToReplace',fld:'vDDO_TABELA_SISTEMADESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Tabela_ModuloDesTitleControlIdToReplace',fld:'vDDO_TABELA_MODULODESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_Tabela_PaiNomTitleControlIdToReplace',fld:'vDDO_TABELA_PAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_Tabela_MelhoraCodTitleControlIdToReplace',fld:'vDDO_TABELA_MELHORACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_Tabela_AtivoTitleControlIdToReplace',fld:'vDDO_TABELA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV131Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV65Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0},{av:'A188Tabela_ModuloCod',fld:'TABELA_MODULOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A181Tabela_PaiCod',fld:'TABELA_PAICOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_managefilters_Activeeventkey',ctrl:'DDO_MANAGEFILTERS',prop:'ActiveEventKey'}],oparms:[{av:'AV97ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV61Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV70TFTabela_Nome',fld:'vTFTABELA_NOME',pic:'@!',nv:''},{av:'Ddo_tabela_nome_Filteredtext_set',ctrl:'DDO_TABELA_NOME',prop:'FilteredText_set'},{av:'AV71TFTabela_Nome_Sel',fld:'vTFTABELA_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_tabela_nome_Selectedvalue_set',ctrl:'DDO_TABELA_NOME',prop:'SelectedValue_set'},{av:'AV74TFTabela_Descricao',fld:'vTFTABELA_DESCRICAO',pic:'',nv:''},{av:'Ddo_tabela_descricao_Filteredtext_set',ctrl:'DDO_TABELA_DESCRICAO',prop:'FilteredText_set'},{av:'AV75TFTabela_Descricao_Sel',fld:'vTFTABELA_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_tabela_descricao_Selectedvalue_set',ctrl:'DDO_TABELA_DESCRICAO',prop:'SelectedValue_set'},{av:'AV78TFTabela_SistemaDes',fld:'vTFTABELA_SISTEMADES',pic:'@!',nv:''},{av:'Ddo_tabela_sistemades_Filteredtext_set',ctrl:'DDO_TABELA_SISTEMADES',prop:'FilteredText_set'},{av:'AV79TFTabela_SistemaDes_Sel',fld:'vTFTABELA_SISTEMADES_SEL',pic:'@!',nv:''},{av:'Ddo_tabela_sistemades_Selectedvalue_set',ctrl:'DDO_TABELA_SISTEMADES',prop:'SelectedValue_set'},{av:'AV82TFTabela_ModuloDes',fld:'vTFTABELA_MODULODES',pic:'@!',nv:''},{av:'Ddo_tabela_modulodes_Filteredtext_set',ctrl:'DDO_TABELA_MODULODES',prop:'FilteredText_set'},{av:'AV83TFTabela_ModuloDes_Sel',fld:'vTFTABELA_MODULODES_SEL',pic:'@!',nv:''},{av:'Ddo_tabela_modulodes_Selectedvalue_set',ctrl:'DDO_TABELA_MODULODES',prop:'SelectedValue_set'},{av:'AV86TFTabela_PaiNom',fld:'vTFTABELA_PAINOM',pic:'@!',nv:''},{av:'Ddo_tabela_painom_Filteredtext_set',ctrl:'DDO_TABELA_PAINOM',prop:'FilteredText_set'},{av:'AV87TFTabela_PaiNom_Sel',fld:'vTFTABELA_PAINOM_SEL',pic:'@!',nv:''},{av:'Ddo_tabela_painom_Selectedvalue_set',ctrl:'DDO_TABELA_PAINOM',prop:'SelectedValue_set'},{av:'AV104TFTabela_MelhoraCod',fld:'vTFTABELA_MELHORACOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_tabela_melhoracod_Filteredtext_set',ctrl:'DDO_TABELA_MELHORACOD',prop:'FilteredText_set'},{av:'AV105TFTabela_MelhoraCod_To',fld:'vTFTABELA_MELHORACOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_tabela_melhoracod_Filteredtextto_set',ctrl:'DDO_TABELA_MELHORACOD',prop:'FilteredTextTo_set'},{av:'AV90TFTabela_Ativo_Sel',fld:'vTFTABELA_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_tabela_ativo_Selectedvalue_set',ctrl:'DDO_TABELA_ATIVO',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Tabela_Nome1',fld:'vTABELA_NOME1',pic:'@!',nv:''},{av:'Ddo_tabela_ativo_Sortedstatus',ctrl:'DDO_TABELA_ATIVO',prop:'SortedStatus'},{av:'Ddo_tabela_melhoracod_Sortedstatus',ctrl:'DDO_TABELA_MELHORACOD',prop:'SortedStatus'},{av:'Ddo_tabela_painom_Sortedstatus',ctrl:'DDO_TABELA_PAINOM',prop:'SortedStatus'},{av:'Ddo_tabela_modulodes_Sortedstatus',ctrl:'DDO_TABELA_MODULODES',prop:'SortedStatus'},{av:'Ddo_tabela_sistemades_Sortedstatus',ctrl:'DDO_TABELA_SISTEMADES',prop:'SortedStatus'},{av:'Ddo_tabela_descricao_Sortedstatus',ctrl:'DDO_TABELA_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_tabela_nome_Sortedstatus',ctrl:'DDO_TABELA_NOME',prop:'SortedStatus'},{av:'AV96Tabela_ModuloDes1',fld:'vTABELA_MODULODES1',pic:'@!',nv:''},{av:'AV62Tabela_ModuloCod1',fld:'vTABELA_MODULOCOD1',pic:'ZZZZZ9',nv:0},{av:'edtavTabela_nome1_Visible',ctrl:'vTABELA_NOME1',prop:'Visible'},{av:'edtavTabela_modulodes1_Visible',ctrl:'vTABELA_MODULODES1',prop:'Visible'},{av:'dynavTabela_modulocod1'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E21462',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV61Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Tabela_Nome1',fld:'vTABELA_NOME1',pic:'@!',nv:''},{av:'AV96Tabela_ModuloDes1',fld:'vTABELA_MODULODES1',pic:'@!',nv:''},{av:'AV62Tabela_ModuloCod1',fld:'vTABELA_MODULOCOD1',pic:'ZZZZZ9',nv:0},{av:'AV70TFTabela_Nome',fld:'vTFTABELA_NOME',pic:'@!',nv:''},{av:'AV71TFTabela_Nome_Sel',fld:'vTFTABELA_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFTabela_Descricao',fld:'vTFTABELA_DESCRICAO',pic:'',nv:''},{av:'AV75TFTabela_Descricao_Sel',fld:'vTFTABELA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV78TFTabela_SistemaDes',fld:'vTFTABELA_SISTEMADES',pic:'@!',nv:''},{av:'AV79TFTabela_SistemaDes_Sel',fld:'vTFTABELA_SISTEMADES_SEL',pic:'@!',nv:''},{av:'AV82TFTabela_ModuloDes',fld:'vTFTABELA_MODULODES',pic:'@!',nv:''},{av:'AV83TFTabela_ModuloDes_Sel',fld:'vTFTABELA_MODULODES_SEL',pic:'@!',nv:''},{av:'AV86TFTabela_PaiNom',fld:'vTFTABELA_PAINOM',pic:'@!',nv:''},{av:'AV87TFTabela_PaiNom_Sel',fld:'vTFTABELA_PAINOM_SEL',pic:'@!',nv:''},{av:'AV104TFTabela_MelhoraCod',fld:'vTFTABELA_MELHORACOD',pic:'ZZZZZ9',nv:0},{av:'AV105TFTabela_MelhoraCod_To',fld:'vTFTABELA_MELHORACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV90TFTabela_Ativo_Sel',fld:'vTFTABELA_ATIVO_SEL',pic:'9',nv:0},{av:'AV97ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV72ddo_Tabela_NomeTitleControlIdToReplace',fld:'vDDO_TABELA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_Tabela_DescricaoTitleControlIdToReplace',fld:'vDDO_TABELA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_Tabela_SistemaDesTitleControlIdToReplace',fld:'vDDO_TABELA_SISTEMADESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Tabela_ModuloDesTitleControlIdToReplace',fld:'vDDO_TABELA_MODULODESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_Tabela_PaiNomTitleControlIdToReplace',fld:'vDDO_TABELA_PAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_Tabela_MelhoraCodTitleControlIdToReplace',fld:'vDDO_TABELA_MELHORACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_Tabela_AtivoTitleControlIdToReplace',fld:'vDDO_TABELA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV131Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV65Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0},{av:'A188Tabela_ModuloCod',fld:'TABELA_MODULOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A181Tabela_PaiCod',fld:'TABELA_PAICOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV61Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV70TFTabela_Nome',fld:'vTFTABELA_NOME',pic:'@!',nv:''},{av:'Ddo_tabela_nome_Filteredtext_set',ctrl:'DDO_TABELA_NOME',prop:'FilteredText_set'},{av:'AV71TFTabela_Nome_Sel',fld:'vTFTABELA_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_tabela_nome_Selectedvalue_set',ctrl:'DDO_TABELA_NOME',prop:'SelectedValue_set'},{av:'AV74TFTabela_Descricao',fld:'vTFTABELA_DESCRICAO',pic:'',nv:''},{av:'Ddo_tabela_descricao_Filteredtext_set',ctrl:'DDO_TABELA_DESCRICAO',prop:'FilteredText_set'},{av:'AV75TFTabela_Descricao_Sel',fld:'vTFTABELA_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_tabela_descricao_Selectedvalue_set',ctrl:'DDO_TABELA_DESCRICAO',prop:'SelectedValue_set'},{av:'AV78TFTabela_SistemaDes',fld:'vTFTABELA_SISTEMADES',pic:'@!',nv:''},{av:'Ddo_tabela_sistemades_Filteredtext_set',ctrl:'DDO_TABELA_SISTEMADES',prop:'FilteredText_set'},{av:'AV79TFTabela_SistemaDes_Sel',fld:'vTFTABELA_SISTEMADES_SEL',pic:'@!',nv:''},{av:'Ddo_tabela_sistemades_Selectedvalue_set',ctrl:'DDO_TABELA_SISTEMADES',prop:'SelectedValue_set'},{av:'AV82TFTabela_ModuloDes',fld:'vTFTABELA_MODULODES',pic:'@!',nv:''},{av:'Ddo_tabela_modulodes_Filteredtext_set',ctrl:'DDO_TABELA_MODULODES',prop:'FilteredText_set'},{av:'AV83TFTabela_ModuloDes_Sel',fld:'vTFTABELA_MODULODES_SEL',pic:'@!',nv:''},{av:'Ddo_tabela_modulodes_Selectedvalue_set',ctrl:'DDO_TABELA_MODULODES',prop:'SelectedValue_set'},{av:'AV86TFTabela_PaiNom',fld:'vTFTABELA_PAINOM',pic:'@!',nv:''},{av:'Ddo_tabela_painom_Filteredtext_set',ctrl:'DDO_TABELA_PAINOM',prop:'FilteredText_set'},{av:'AV87TFTabela_PaiNom_Sel',fld:'vTFTABELA_PAINOM_SEL',pic:'@!',nv:''},{av:'Ddo_tabela_painom_Selectedvalue_set',ctrl:'DDO_TABELA_PAINOM',prop:'SelectedValue_set'},{av:'AV104TFTabela_MelhoraCod',fld:'vTFTABELA_MELHORACOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_tabela_melhoracod_Filteredtext_set',ctrl:'DDO_TABELA_MELHORACOD',prop:'FilteredText_set'},{av:'AV105TFTabela_MelhoraCod_To',fld:'vTFTABELA_MELHORACOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_tabela_melhoracod_Filteredtextto_set',ctrl:'DDO_TABELA_MELHORACOD',prop:'FilteredTextTo_set'},{av:'AV90TFTabela_Ativo_Sel',fld:'vTFTABELA_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_tabela_ativo_Selectedvalue_set',ctrl:'DDO_TABELA_ATIVO',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Tabela_Nome1',fld:'vTABELA_NOME1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavTabela_nome1_Visible',ctrl:'vTABELA_NOME1',prop:'Visible'},{av:'edtavTabela_modulodes1_Visible',ctrl:'vTABELA_MODULODES1',prop:'Visible'},{av:'dynavTabela_modulocod1'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV96Tabela_ModuloDes1',fld:'vTABELA_MODULODES1',pic:'@!',nv:''},{av:'AV62Tabela_ModuloCod1',fld:'vTABELA_MODULOCOD1',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_tabela_nome_Activeeventkey = "";
         Ddo_tabela_nome_Filteredtext_get = "";
         Ddo_tabela_nome_Selectedvalue_get = "";
         Ddo_tabela_descricao_Activeeventkey = "";
         Ddo_tabela_descricao_Filteredtext_get = "";
         Ddo_tabela_descricao_Selectedvalue_get = "";
         Ddo_tabela_sistemades_Activeeventkey = "";
         Ddo_tabela_sistemades_Filteredtext_get = "";
         Ddo_tabela_sistemades_Selectedvalue_get = "";
         Ddo_tabela_modulodes_Activeeventkey = "";
         Ddo_tabela_modulodes_Filteredtext_get = "";
         Ddo_tabela_modulodes_Selectedvalue_get = "";
         Ddo_tabela_painom_Activeeventkey = "";
         Ddo_tabela_painom_Filteredtext_get = "";
         Ddo_tabela_painom_Selectedvalue_get = "";
         Ddo_tabela_melhoracod_Activeeventkey = "";
         Ddo_tabela_melhoracod_Filteredtext_get = "";
         Ddo_tabela_melhoracod_Filteredtextto_get = "";
         Ddo_tabela_ativo_Activeeventkey = "";
         Ddo_tabela_ativo_Selectedvalue_get = "";
         Ddo_managefilters_Activeeventkey = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17Tabela_Nome1 = "";
         AV96Tabela_ModuloDes1 = "";
         AV70TFTabela_Nome = "";
         AV71TFTabela_Nome_Sel = "";
         AV74TFTabela_Descricao = "";
         AV75TFTabela_Descricao_Sel = "";
         AV78TFTabela_SistemaDes = "";
         AV79TFTabela_SistemaDes_Sel = "";
         AV82TFTabela_ModuloDes = "";
         AV83TFTabela_ModuloDes_Sel = "";
         AV86TFTabela_PaiNom = "";
         AV87TFTabela_PaiNom_Sel = "";
         AV72ddo_Tabela_NomeTitleControlIdToReplace = "";
         AV76ddo_Tabela_DescricaoTitleControlIdToReplace = "";
         AV80ddo_Tabela_SistemaDesTitleControlIdToReplace = "";
         AV84ddo_Tabela_ModuloDesTitleControlIdToReplace = "";
         AV88ddo_Tabela_PaiNomTitleControlIdToReplace = "";
         AV106ddo_Tabela_MelhoraCodTitleControlIdToReplace = "";
         AV91ddo_Tabela_AtivoTitleControlIdToReplace = "";
         AV131Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV101ManageFiltersData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV92DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV69Tabela_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV73Tabela_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV77Tabela_SistemaDesTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV81Tabela_ModuloDesTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV85Tabela_PaiNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV103Tabela_MelhoraCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV89Tabela_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_managefilters_Icon = "";
         Ddo_managefilters_Caption = "";
         Ddo_tabela_nome_Filteredtext_set = "";
         Ddo_tabela_nome_Selectedvalue_set = "";
         Ddo_tabela_nome_Sortedstatus = "";
         Ddo_tabela_descricao_Filteredtext_set = "";
         Ddo_tabela_descricao_Selectedvalue_set = "";
         Ddo_tabela_descricao_Sortedstatus = "";
         Ddo_tabela_sistemades_Filteredtext_set = "";
         Ddo_tabela_sistemades_Selectedvalue_set = "";
         Ddo_tabela_sistemades_Sortedstatus = "";
         Ddo_tabela_modulodes_Filteredtext_set = "";
         Ddo_tabela_modulodes_Selectedvalue_set = "";
         Ddo_tabela_modulodes_Sortedstatus = "";
         Ddo_tabela_painom_Filteredtext_set = "";
         Ddo_tabela_painom_Selectedvalue_set = "";
         Ddo_tabela_painom_Sortedstatus = "";
         Ddo_tabela_melhoracod_Filteredtext_set = "";
         Ddo_tabela_melhoracod_Filteredtextto_set = "";
         Ddo_tabela_melhoracod_Sortedstatus = "";
         Ddo_tabela_ativo_Selectedvalue_set = "";
         Ddo_tabela_ativo_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV37Update = "";
         AV128Update_GXI = "";
         AV38Delete = "";
         AV129Delete_GXI = "";
         AV66Display = "";
         AV130Display_GXI = "";
         A173Tabela_Nome = "";
         A175Tabela_Descricao = "";
         A191Tabela_SistemaDes = "";
         A189Tabela_ModuloDes = "";
         A182Tabela_PaiNom = "";
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00462_A5AreaTrabalho_Codigo = new int[1] ;
         H00462_A6AreaTrabalho_Descricao = new String[] {""} ;
         H00462_A72AreaTrabalho_Ativo = new bool[] {false} ;
         H00463_A146Modulo_Codigo = new int[1] ;
         H00463_A143Modulo_Nome = new String[] {""} ;
         GridContainer = new GXWebGrid( context);
         lV112WWTabelaDS_4_Tabela_nome1 = "";
         lV113WWTabelaDS_5_Tabela_modulodes1 = "";
         lV115WWTabelaDS_7_Tftabela_nome = "";
         lV117WWTabelaDS_9_Tftabela_descricao = "";
         lV119WWTabelaDS_11_Tftabela_sistemades = "";
         lV121WWTabelaDS_13_Tftabela_modulodes = "";
         lV123WWTabelaDS_15_Tftabela_painom = "";
         AV110WWTabelaDS_2_Dynamicfiltersselector1 = "";
         AV112WWTabelaDS_4_Tabela_nome1 = "";
         AV113WWTabelaDS_5_Tabela_modulodes1 = "";
         AV116WWTabelaDS_8_Tftabela_nome_sel = "";
         AV115WWTabelaDS_7_Tftabela_nome = "";
         AV118WWTabelaDS_10_Tftabela_descricao_sel = "";
         AV117WWTabelaDS_9_Tftabela_descricao = "";
         AV120WWTabelaDS_12_Tftabela_sistemades_sel = "";
         AV119WWTabelaDS_11_Tftabela_sistemades = "";
         AV122WWTabelaDS_14_Tftabela_modulodes_sel = "";
         AV121WWTabelaDS_13_Tftabela_modulodes = "";
         AV124WWTabelaDS_16_Tftabela_painom_sel = "";
         AV123WWTabelaDS_15_Tftabela_painom = "";
         H00464_A135Sistema_AreaTrabalhoCod = new int[1] ;
         H00464_n135Sistema_AreaTrabalhoCod = new bool[] {false} ;
         H00464_A174Tabela_Ativo = new bool[] {false} ;
         H00464_A746Tabela_MelhoraCod = new int[1] ;
         H00464_n746Tabela_MelhoraCod = new bool[] {false} ;
         H00464_A182Tabela_PaiNom = new String[] {""} ;
         H00464_n182Tabela_PaiNom = new bool[] {false} ;
         H00464_A181Tabela_PaiCod = new int[1] ;
         H00464_n181Tabela_PaiCod = new bool[] {false} ;
         H00464_A189Tabela_ModuloDes = new String[] {""} ;
         H00464_n189Tabela_ModuloDes = new bool[] {false} ;
         H00464_A188Tabela_ModuloCod = new int[1] ;
         H00464_n188Tabela_ModuloCod = new bool[] {false} ;
         H00464_A191Tabela_SistemaDes = new String[] {""} ;
         H00464_n191Tabela_SistemaDes = new bool[] {false} ;
         H00464_A190Tabela_SistemaCod = new int[1] ;
         H00464_A175Tabela_Descricao = new String[] {""} ;
         H00464_n175Tabela_Descricao = new bool[] {false} ;
         H00464_A173Tabela_Nome = new String[] {""} ;
         H00464_A172Tabela_Codigo = new int[1] ;
         H00465_AGRID_nRecordCount = new long[1] ;
         AV7HTTPRequest = new GxHttpRequest( context);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         GridRow = new GXWebRow();
         AV98ManageFiltersXml = "";
         GXt_char2 = "";
         AV102ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV99ManageFiltersItems = new GxObjectCollection( context, "GridStateCollection.Item", "", "wwpbaseobjects.SdtGridStateCollection_Item", "GeneXus.Programs");
         AV100ManageFiltersItem = new wwpbaseobjects.SdtGridStateCollection_Item(context);
         AV39Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         imgCleanfilters_Jsonclick = "";
         lblFiltertextsistema_areatrabalhocod_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblTabelatitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwtabela__default(),
            new Object[][] {
                new Object[] {
               H00462_A5AreaTrabalho_Codigo, H00462_A6AreaTrabalho_Descricao, H00462_A72AreaTrabalho_Ativo
               }
               , new Object[] {
               H00463_A146Modulo_Codigo, H00463_A143Modulo_Nome
               }
               , new Object[] {
               H00464_A135Sistema_AreaTrabalhoCod, H00464_n135Sistema_AreaTrabalhoCod, H00464_A174Tabela_Ativo, H00464_A746Tabela_MelhoraCod, H00464_n746Tabela_MelhoraCod, H00464_A182Tabela_PaiNom, H00464_n182Tabela_PaiNom, H00464_A181Tabela_PaiCod, H00464_n181Tabela_PaiCod, H00464_A189Tabela_ModuloDes,
               H00464_n189Tabela_ModuloDes, H00464_A188Tabela_ModuloCod, H00464_n188Tabela_ModuloCod, H00464_A191Tabela_SistemaDes, H00464_n191Tabela_SistemaDes, H00464_A190Tabela_SistemaCod, H00464_A175Tabela_Descricao, H00464_n175Tabela_Descricao, H00464_A173Tabela_Nome, H00464_A172Tabela_Codigo
               }
               , new Object[] {
               H00465_AGRID_nRecordCount
               }
            }
         );
         AV131Pgmname = "WWTabela";
         /* GeneXus formulas. */
         AV131Pgmname = "WWTabela";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_62 ;
      private short nGXsfl_62_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV90TFTabela_Ativo_Sel ;
      private short AV97ManageFiltersExecutionStep ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_62_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV111WWTabelaDS_3_Dynamicfiltersoperator1 ;
      private short AV127WWTabelaDS_19_Tftabela_ativo_sel ;
      private short edtTabela_Nome_Titleformat ;
      private short edtTabela_Descricao_Titleformat ;
      private short edtTabela_SistemaDes_Titleformat ;
      private short edtTabela_ModuloDes_Titleformat ;
      private short edtTabela_PaiNom_Titleformat ;
      private short edtTabela_MelhoraCod_Titleformat ;
      private short chkTabela_Ativo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV61Sistema_AreaTrabalhoCod ;
      private int AV62Tabela_ModuloCod1 ;
      private int AV104TFTabela_MelhoraCod ;
      private int AV105TFTabela_MelhoraCod_To ;
      private int A172Tabela_Codigo ;
      private int A190Tabela_SistemaCod ;
      private int AV65Tabela_ModuloCod ;
      private int A188Tabela_ModuloCod ;
      private int A181Tabela_PaiCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_tabela_nome_Datalistupdateminimumcharacters ;
      private int Ddo_tabela_descricao_Datalistupdateminimumcharacters ;
      private int Ddo_tabela_sistemades_Datalistupdateminimumcharacters ;
      private int Ddo_tabela_modulodes_Datalistupdateminimumcharacters ;
      private int Ddo_tabela_painom_Datalistupdateminimumcharacters ;
      private int edtavManagefiltersexecutionstep_Visible ;
      private int edtavTftabela_nome_Visible ;
      private int edtavTftabela_nome_sel_Visible ;
      private int edtavTftabela_descricao_Visible ;
      private int edtavTftabela_descricao_sel_Visible ;
      private int edtavTftabela_sistemades_Visible ;
      private int edtavTftabela_sistemades_sel_Visible ;
      private int edtavTftabela_modulodes_Visible ;
      private int edtavTftabela_modulodes_sel_Visible ;
      private int edtavTftabela_painom_Visible ;
      private int edtavTftabela_painom_sel_Visible ;
      private int edtavTftabela_melhoracod_Visible ;
      private int edtavTftabela_melhoracod_to_Visible ;
      private int edtavTftabela_ativo_sel_Visible ;
      private int edtavDdo_tabela_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_tabela_descricaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_tabela_sistemadestitlecontrolidtoreplace_Visible ;
      private int edtavDdo_tabela_modulodestitlecontrolidtoreplace_Visible ;
      private int edtavDdo_tabela_painomtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_tabela_melhoracodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_tabela_ativotitlecontrolidtoreplace_Visible ;
      private int A746Tabela_MelhoraCod ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV109WWTabelaDS_1_Sistema_areatrabalhocod ;
      private int AV114WWTabelaDS_6_Tabela_modulocod1 ;
      private int AV125WWTabelaDS_17_Tftabela_melhoracod ;
      private int AV126WWTabelaDS_18_Tftabela_melhoracod_to ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int edtavOrdereddsc_Visible ;
      private int AV93PageToGo ;
      private int edtavUpdate_Enabled ;
      private int edtavDelete_Enabled ;
      private int edtavDisplay_Enabled ;
      private int edtavTabela_nome1_Visible ;
      private int edtavTabela_modulodes1_Visible ;
      private int AV132GXV1 ;
      private int AV133GXV2 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV94GridCurrentPage ;
      private long AV95GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_tabela_nome_Activeeventkey ;
      private String Ddo_tabela_nome_Filteredtext_get ;
      private String Ddo_tabela_nome_Selectedvalue_get ;
      private String Ddo_tabela_descricao_Activeeventkey ;
      private String Ddo_tabela_descricao_Filteredtext_get ;
      private String Ddo_tabela_descricao_Selectedvalue_get ;
      private String Ddo_tabela_sistemades_Activeeventkey ;
      private String Ddo_tabela_sistemades_Filteredtext_get ;
      private String Ddo_tabela_sistemades_Selectedvalue_get ;
      private String Ddo_tabela_modulodes_Activeeventkey ;
      private String Ddo_tabela_modulodes_Filteredtext_get ;
      private String Ddo_tabela_modulodes_Selectedvalue_get ;
      private String Ddo_tabela_painom_Activeeventkey ;
      private String Ddo_tabela_painom_Filteredtext_get ;
      private String Ddo_tabela_painom_Selectedvalue_get ;
      private String Ddo_tabela_melhoracod_Activeeventkey ;
      private String Ddo_tabela_melhoracod_Filteredtext_get ;
      private String Ddo_tabela_melhoracod_Filteredtextto_get ;
      private String Ddo_tabela_ativo_Activeeventkey ;
      private String Ddo_tabela_ativo_Selectedvalue_get ;
      private String Ddo_managefilters_Activeeventkey ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_62_idx="0001" ;
      private String AV17Tabela_Nome1 ;
      private String AV96Tabela_ModuloDes1 ;
      private String AV70TFTabela_Nome ;
      private String AV71TFTabela_Nome_Sel ;
      private String AV82TFTabela_ModuloDes ;
      private String AV83TFTabela_ModuloDes_Sel ;
      private String AV86TFTabela_PaiNom ;
      private String AV87TFTabela_PaiNom_Sel ;
      private String AV131Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Ddo_managefilters_Icon ;
      private String Ddo_managefilters_Caption ;
      private String Ddo_managefilters_Tooltip ;
      private String Ddo_managefilters_Cls ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_tabela_nome_Caption ;
      private String Ddo_tabela_nome_Tooltip ;
      private String Ddo_tabela_nome_Cls ;
      private String Ddo_tabela_nome_Filteredtext_set ;
      private String Ddo_tabela_nome_Selectedvalue_set ;
      private String Ddo_tabela_nome_Dropdownoptionstype ;
      private String Ddo_tabela_nome_Titlecontrolidtoreplace ;
      private String Ddo_tabela_nome_Sortedstatus ;
      private String Ddo_tabela_nome_Filtertype ;
      private String Ddo_tabela_nome_Datalisttype ;
      private String Ddo_tabela_nome_Datalistproc ;
      private String Ddo_tabela_nome_Sortasc ;
      private String Ddo_tabela_nome_Sortdsc ;
      private String Ddo_tabela_nome_Loadingdata ;
      private String Ddo_tabela_nome_Cleanfilter ;
      private String Ddo_tabela_nome_Noresultsfound ;
      private String Ddo_tabela_nome_Searchbuttontext ;
      private String Ddo_tabela_descricao_Caption ;
      private String Ddo_tabela_descricao_Tooltip ;
      private String Ddo_tabela_descricao_Cls ;
      private String Ddo_tabela_descricao_Filteredtext_set ;
      private String Ddo_tabela_descricao_Selectedvalue_set ;
      private String Ddo_tabela_descricao_Dropdownoptionstype ;
      private String Ddo_tabela_descricao_Titlecontrolidtoreplace ;
      private String Ddo_tabela_descricao_Sortedstatus ;
      private String Ddo_tabela_descricao_Filtertype ;
      private String Ddo_tabela_descricao_Datalisttype ;
      private String Ddo_tabela_descricao_Datalistproc ;
      private String Ddo_tabela_descricao_Sortasc ;
      private String Ddo_tabela_descricao_Sortdsc ;
      private String Ddo_tabela_descricao_Loadingdata ;
      private String Ddo_tabela_descricao_Cleanfilter ;
      private String Ddo_tabela_descricao_Noresultsfound ;
      private String Ddo_tabela_descricao_Searchbuttontext ;
      private String Ddo_tabela_sistemades_Caption ;
      private String Ddo_tabela_sistemades_Tooltip ;
      private String Ddo_tabela_sistemades_Cls ;
      private String Ddo_tabela_sistemades_Filteredtext_set ;
      private String Ddo_tabela_sistemades_Selectedvalue_set ;
      private String Ddo_tabela_sistemades_Dropdownoptionstype ;
      private String Ddo_tabela_sistemades_Titlecontrolidtoreplace ;
      private String Ddo_tabela_sistemades_Sortedstatus ;
      private String Ddo_tabela_sistemades_Filtertype ;
      private String Ddo_tabela_sistemades_Datalisttype ;
      private String Ddo_tabela_sistemades_Datalistproc ;
      private String Ddo_tabela_sistemades_Sortasc ;
      private String Ddo_tabela_sistemades_Sortdsc ;
      private String Ddo_tabela_sistemades_Loadingdata ;
      private String Ddo_tabela_sistemades_Cleanfilter ;
      private String Ddo_tabela_sistemades_Noresultsfound ;
      private String Ddo_tabela_sistemades_Searchbuttontext ;
      private String Ddo_tabela_modulodes_Caption ;
      private String Ddo_tabela_modulodes_Tooltip ;
      private String Ddo_tabela_modulodes_Cls ;
      private String Ddo_tabela_modulodes_Filteredtext_set ;
      private String Ddo_tabela_modulodes_Selectedvalue_set ;
      private String Ddo_tabela_modulodes_Dropdownoptionstype ;
      private String Ddo_tabela_modulodes_Titlecontrolidtoreplace ;
      private String Ddo_tabela_modulodes_Sortedstatus ;
      private String Ddo_tabela_modulodes_Filtertype ;
      private String Ddo_tabela_modulodes_Datalisttype ;
      private String Ddo_tabela_modulodes_Datalistproc ;
      private String Ddo_tabela_modulodes_Sortasc ;
      private String Ddo_tabela_modulodes_Sortdsc ;
      private String Ddo_tabela_modulodes_Loadingdata ;
      private String Ddo_tabela_modulodes_Cleanfilter ;
      private String Ddo_tabela_modulodes_Noresultsfound ;
      private String Ddo_tabela_modulodes_Searchbuttontext ;
      private String Ddo_tabela_painom_Caption ;
      private String Ddo_tabela_painom_Tooltip ;
      private String Ddo_tabela_painom_Cls ;
      private String Ddo_tabela_painom_Filteredtext_set ;
      private String Ddo_tabela_painom_Selectedvalue_set ;
      private String Ddo_tabela_painom_Dropdownoptionstype ;
      private String Ddo_tabela_painom_Titlecontrolidtoreplace ;
      private String Ddo_tabela_painom_Sortedstatus ;
      private String Ddo_tabela_painom_Filtertype ;
      private String Ddo_tabela_painom_Datalisttype ;
      private String Ddo_tabela_painom_Datalistproc ;
      private String Ddo_tabela_painom_Sortasc ;
      private String Ddo_tabela_painom_Sortdsc ;
      private String Ddo_tabela_painom_Loadingdata ;
      private String Ddo_tabela_painom_Cleanfilter ;
      private String Ddo_tabela_painom_Noresultsfound ;
      private String Ddo_tabela_painom_Searchbuttontext ;
      private String Ddo_tabela_melhoracod_Caption ;
      private String Ddo_tabela_melhoracod_Tooltip ;
      private String Ddo_tabela_melhoracod_Cls ;
      private String Ddo_tabela_melhoracod_Filteredtext_set ;
      private String Ddo_tabela_melhoracod_Filteredtextto_set ;
      private String Ddo_tabela_melhoracod_Dropdownoptionstype ;
      private String Ddo_tabela_melhoracod_Titlecontrolidtoreplace ;
      private String Ddo_tabela_melhoracod_Sortedstatus ;
      private String Ddo_tabela_melhoracod_Filtertype ;
      private String Ddo_tabela_melhoracod_Sortasc ;
      private String Ddo_tabela_melhoracod_Sortdsc ;
      private String Ddo_tabela_melhoracod_Cleanfilter ;
      private String Ddo_tabela_melhoracod_Rangefilterfrom ;
      private String Ddo_tabela_melhoracod_Rangefilterto ;
      private String Ddo_tabela_melhoracod_Searchbuttontext ;
      private String Ddo_tabela_ativo_Caption ;
      private String Ddo_tabela_ativo_Tooltip ;
      private String Ddo_tabela_ativo_Cls ;
      private String Ddo_tabela_ativo_Selectedvalue_set ;
      private String Ddo_tabela_ativo_Dropdownoptionstype ;
      private String Ddo_tabela_ativo_Titlecontrolidtoreplace ;
      private String Ddo_tabela_ativo_Sortedstatus ;
      private String Ddo_tabela_ativo_Datalisttype ;
      private String Ddo_tabela_ativo_Datalistfixedvalues ;
      private String Ddo_tabela_ativo_Sortasc ;
      private String Ddo_tabela_ativo_Sortdsc ;
      private String Ddo_tabela_ativo_Cleanfilter ;
      private String Ddo_tabela_ativo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String edtavManagefiltersexecutionstep_Internalname ;
      private String edtavManagefiltersexecutionstep_Jsonclick ;
      private String edtavTftabela_nome_Internalname ;
      private String edtavTftabela_nome_Jsonclick ;
      private String edtavTftabela_nome_sel_Internalname ;
      private String edtavTftabela_nome_sel_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavTftabela_descricao_Internalname ;
      private String edtavTftabela_descricao_sel_Internalname ;
      private String edtavTftabela_sistemades_Internalname ;
      private String edtavTftabela_sistemades_Jsonclick ;
      private String edtavTftabela_sistemades_sel_Internalname ;
      private String edtavTftabela_sistemades_sel_Jsonclick ;
      private String edtavTftabela_modulodes_Internalname ;
      private String edtavTftabela_modulodes_Jsonclick ;
      private String edtavTftabela_modulodes_sel_Internalname ;
      private String edtavTftabela_modulodes_sel_Jsonclick ;
      private String edtavTftabela_painom_Internalname ;
      private String edtavTftabela_painom_Jsonclick ;
      private String edtavTftabela_painom_sel_Internalname ;
      private String edtavTftabela_painom_sel_Jsonclick ;
      private String edtavTftabela_melhoracod_Internalname ;
      private String edtavTftabela_melhoracod_Jsonclick ;
      private String edtavTftabela_melhoracod_to_Internalname ;
      private String edtavTftabela_melhoracod_to_Jsonclick ;
      private String edtavTftabela_ativo_sel_Internalname ;
      private String edtavTftabela_ativo_sel_Jsonclick ;
      private String edtavDdo_tabela_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_tabela_descricaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_tabela_sistemadestitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_tabela_modulodestitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_tabela_painomtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_tabela_melhoracodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_tabela_ativotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtTabela_Codigo_Internalname ;
      private String A173Tabela_Nome ;
      private String edtTabela_Nome_Internalname ;
      private String edtTabela_Descricao_Internalname ;
      private String edtTabela_SistemaCod_Internalname ;
      private String edtTabela_SistemaDes_Internalname ;
      private String edtTabela_ModuloCod_Internalname ;
      private String A189Tabela_ModuloDes ;
      private String edtTabela_ModuloDes_Internalname ;
      private String edtTabela_PaiCod_Internalname ;
      private String A182Tabela_PaiNom ;
      private String edtTabela_PaiNom_Internalname ;
      private String edtTabela_MelhoraCod_Internalname ;
      private String chkTabela_Ativo_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String lV112WWTabelaDS_4_Tabela_nome1 ;
      private String lV113WWTabelaDS_5_Tabela_modulodes1 ;
      private String lV115WWTabelaDS_7_Tftabela_nome ;
      private String lV121WWTabelaDS_13_Tftabela_modulodes ;
      private String lV123WWTabelaDS_15_Tftabela_painom ;
      private String AV112WWTabelaDS_4_Tabela_nome1 ;
      private String AV113WWTabelaDS_5_Tabela_modulodes1 ;
      private String AV116WWTabelaDS_8_Tftabela_nome_sel ;
      private String AV115WWTabelaDS_7_Tftabela_nome ;
      private String AV122WWTabelaDS_14_Tftabela_modulodes_sel ;
      private String AV121WWTabelaDS_13_Tftabela_modulodes ;
      private String AV124WWTabelaDS_16_Tftabela_painom_sel ;
      private String AV123WWTabelaDS_15_Tftabela_painom ;
      private String edtavOrdereddsc_Internalname ;
      private String dynavSistema_areatrabalhocod_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavTabela_nome1_Internalname ;
      private String edtavTabela_modulodes1_Internalname ;
      private String dynavTabela_modulocod1_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_tabela_nome_Internalname ;
      private String Ddo_tabela_descricao_Internalname ;
      private String Ddo_tabela_sistemades_Internalname ;
      private String Ddo_tabela_modulodes_Internalname ;
      private String Ddo_tabela_painom_Internalname ;
      private String Ddo_tabela_melhoracod_Internalname ;
      private String Ddo_tabela_ativo_Internalname ;
      private String Ddo_managefilters_Internalname ;
      private String edtTabela_Nome_Title ;
      private String edtTabela_Descricao_Title ;
      private String edtTabela_SistemaDes_Title ;
      private String edtTabela_ModuloDes_Title ;
      private String edtTabela_PaiNom_Title ;
      private String edtTabela_MelhoraCod_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String edtTabela_Nome_Link ;
      private String edtTabela_ModuloDes_Link ;
      private String edtTabela_PaiNom_Link ;
      private String GXt_char2 ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String tblTablefilters_Internalname ;
      private String imgCleanfilters_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String lblFiltertextsistema_areatrabalhocod_Internalname ;
      private String lblFiltertextsistema_areatrabalhocod_Jsonclick ;
      private String dynavSistema_areatrabalhocod_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavTabela_nome1_Jsonclick ;
      private String edtavTabela_modulodes1_Jsonclick ;
      private String dynavTabela_modulocod1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String lblTabelatitle_Internalname ;
      private String lblTabelatitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String sGXsfl_62_fel_idx="0001" ;
      private String ROClassString ;
      private String edtTabela_Codigo_Jsonclick ;
      private String edtTabela_Nome_Jsonclick ;
      private String edtTabela_Descricao_Jsonclick ;
      private String edtTabela_SistemaCod_Jsonclick ;
      private String edtTabela_SistemaDes_Jsonclick ;
      private String edtTabela_ModuloCod_Jsonclick ;
      private String edtTabela_ModuloDes_Jsonclick ;
      private String edtTabela_PaiCod_Jsonclick ;
      private String edtTabela_PaiNom_Jsonclick ;
      private String edtTabela_MelhoraCod_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool n188Tabela_ModuloCod ;
      private bool n181Tabela_PaiCod ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_tabela_nome_Includesortasc ;
      private bool Ddo_tabela_nome_Includesortdsc ;
      private bool Ddo_tabela_nome_Includefilter ;
      private bool Ddo_tabela_nome_Filterisrange ;
      private bool Ddo_tabela_nome_Includedatalist ;
      private bool Ddo_tabela_descricao_Includesortasc ;
      private bool Ddo_tabela_descricao_Includesortdsc ;
      private bool Ddo_tabela_descricao_Includefilter ;
      private bool Ddo_tabela_descricao_Filterisrange ;
      private bool Ddo_tabela_descricao_Includedatalist ;
      private bool Ddo_tabela_sistemades_Includesortasc ;
      private bool Ddo_tabela_sistemades_Includesortdsc ;
      private bool Ddo_tabela_sistemades_Includefilter ;
      private bool Ddo_tabela_sistemades_Filterisrange ;
      private bool Ddo_tabela_sistemades_Includedatalist ;
      private bool Ddo_tabela_modulodes_Includesortasc ;
      private bool Ddo_tabela_modulodes_Includesortdsc ;
      private bool Ddo_tabela_modulodes_Includefilter ;
      private bool Ddo_tabela_modulodes_Filterisrange ;
      private bool Ddo_tabela_modulodes_Includedatalist ;
      private bool Ddo_tabela_painom_Includesortasc ;
      private bool Ddo_tabela_painom_Includesortdsc ;
      private bool Ddo_tabela_painom_Includefilter ;
      private bool Ddo_tabela_painom_Filterisrange ;
      private bool Ddo_tabela_painom_Includedatalist ;
      private bool Ddo_tabela_melhoracod_Includesortasc ;
      private bool Ddo_tabela_melhoracod_Includesortdsc ;
      private bool Ddo_tabela_melhoracod_Includefilter ;
      private bool Ddo_tabela_melhoracod_Filterisrange ;
      private bool Ddo_tabela_melhoracod_Includedatalist ;
      private bool Ddo_tabela_ativo_Includesortasc ;
      private bool Ddo_tabela_ativo_Includesortdsc ;
      private bool Ddo_tabela_ativo_Includefilter ;
      private bool Ddo_tabela_ativo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n175Tabela_Descricao ;
      private bool n191Tabela_SistemaDes ;
      private bool n189Tabela_ModuloDes ;
      private bool n182Tabela_PaiNom ;
      private bool n746Tabela_MelhoraCod ;
      private bool A174Tabela_Ativo ;
      private bool n135Sistema_AreaTrabalhoCod ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV37Update_IsBlob ;
      private bool AV38Delete_IsBlob ;
      private bool AV66Display_IsBlob ;
      private String A175Tabela_Descricao ;
      private String AV98ManageFiltersXml ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV74TFTabela_Descricao ;
      private String AV75TFTabela_Descricao_Sel ;
      private String AV78TFTabela_SistemaDes ;
      private String AV79TFTabela_SistemaDes_Sel ;
      private String AV72ddo_Tabela_NomeTitleControlIdToReplace ;
      private String AV76ddo_Tabela_DescricaoTitleControlIdToReplace ;
      private String AV80ddo_Tabela_SistemaDesTitleControlIdToReplace ;
      private String AV84ddo_Tabela_ModuloDesTitleControlIdToReplace ;
      private String AV88ddo_Tabela_PaiNomTitleControlIdToReplace ;
      private String AV106ddo_Tabela_MelhoraCodTitleControlIdToReplace ;
      private String AV91ddo_Tabela_AtivoTitleControlIdToReplace ;
      private String AV128Update_GXI ;
      private String AV129Delete_GXI ;
      private String AV130Display_GXI ;
      private String A191Tabela_SistemaDes ;
      private String lV117WWTabelaDS_9_Tftabela_descricao ;
      private String lV119WWTabelaDS_11_Tftabela_sistemades ;
      private String AV110WWTabelaDS_2_Dynamicfiltersselector1 ;
      private String AV118WWTabelaDS_10_Tftabela_descricao_sel ;
      private String AV117WWTabelaDS_9_Tftabela_descricao ;
      private String AV120WWTabelaDS_12_Tftabela_sistemades_sel ;
      private String AV119WWTabelaDS_11_Tftabela_sistemades ;
      private String AV37Update ;
      private String AV38Delete ;
      private String AV66Display ;
      private IGxSession AV39Session ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox dynavSistema_areatrabalhocod ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox dynavTabela_modulocod1 ;
      private GXCheckbox chkTabela_Ativo ;
      private IDataStoreProvider pr_default ;
      private int[] H00462_A5AreaTrabalho_Codigo ;
      private String[] H00462_A6AreaTrabalho_Descricao ;
      private bool[] H00462_A72AreaTrabalho_Ativo ;
      private int[] H00463_A146Modulo_Codigo ;
      private String[] H00463_A143Modulo_Nome ;
      private int[] H00464_A135Sistema_AreaTrabalhoCod ;
      private bool[] H00464_n135Sistema_AreaTrabalhoCod ;
      private bool[] H00464_A174Tabela_Ativo ;
      private int[] H00464_A746Tabela_MelhoraCod ;
      private bool[] H00464_n746Tabela_MelhoraCod ;
      private String[] H00464_A182Tabela_PaiNom ;
      private bool[] H00464_n182Tabela_PaiNom ;
      private int[] H00464_A181Tabela_PaiCod ;
      private bool[] H00464_n181Tabela_PaiCod ;
      private String[] H00464_A189Tabela_ModuloDes ;
      private bool[] H00464_n189Tabela_ModuloDes ;
      private int[] H00464_A188Tabela_ModuloCod ;
      private bool[] H00464_n188Tabela_ModuloCod ;
      private String[] H00464_A191Tabela_SistemaDes ;
      private bool[] H00464_n191Tabela_SistemaDes ;
      private int[] H00464_A190Tabela_SistemaCod ;
      private String[] H00464_A175Tabela_Descricao ;
      private bool[] H00464_n175Tabela_Descricao ;
      private String[] H00464_A173Tabela_Nome ;
      private int[] H00464_A172Tabela_Codigo ;
      private long[] H00465_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV101ManageFiltersData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV69Tabela_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV73Tabela_DescricaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV77Tabela_SistemaDesTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV81Tabela_ModuloDesTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV85Tabela_PaiNomTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV103Tabela_MelhoraCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV89Tabela_AtivoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtGridStateCollection_Item ))]
      private IGxCollection AV99ManageFiltersItems ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item AV102ManageFiltersDataItem ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV92DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
      private wwpbaseobjects.SdtGridStateCollection_Item AV100ManageFiltersItem ;
   }

   public class wwtabela__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00464( IGxContext context ,
                                             int AV109WWTabelaDS_1_Sistema_areatrabalhocod ,
                                             String AV110WWTabelaDS_2_Dynamicfiltersselector1 ,
                                             short AV111WWTabelaDS_3_Dynamicfiltersoperator1 ,
                                             String AV112WWTabelaDS_4_Tabela_nome1 ,
                                             String AV113WWTabelaDS_5_Tabela_modulodes1 ,
                                             int AV114WWTabelaDS_6_Tabela_modulocod1 ,
                                             String AV116WWTabelaDS_8_Tftabela_nome_sel ,
                                             String AV115WWTabelaDS_7_Tftabela_nome ,
                                             String AV118WWTabelaDS_10_Tftabela_descricao_sel ,
                                             String AV117WWTabelaDS_9_Tftabela_descricao ,
                                             String AV120WWTabelaDS_12_Tftabela_sistemades_sel ,
                                             String AV119WWTabelaDS_11_Tftabela_sistemades ,
                                             String AV122WWTabelaDS_14_Tftabela_modulodes_sel ,
                                             String AV121WWTabelaDS_13_Tftabela_modulodes ,
                                             String AV124WWTabelaDS_16_Tftabela_painom_sel ,
                                             String AV123WWTabelaDS_15_Tftabela_painom ,
                                             int AV125WWTabelaDS_17_Tftabela_melhoracod ,
                                             int AV126WWTabelaDS_18_Tftabela_melhoracod_to ,
                                             short AV127WWTabelaDS_19_Tftabela_ativo_sel ,
                                             int A135Sistema_AreaTrabalhoCod ,
                                             String A173Tabela_Nome ,
                                             String A189Tabela_ModuloDes ,
                                             int A188Tabela_ModuloCod ,
                                             String A175Tabela_Descricao ,
                                             String A191Tabela_SistemaDes ,
                                             String A182Tabela_PaiNom ,
                                             int A746Tabela_MelhoraCod ,
                                             bool A174Tabela_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [23] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T4.[Sistema_AreaTrabalhoCod], T1.[Tabela_Ativo], T1.[Tabela_MelhoraCod], T2.[Tabela_Nome] AS Tabela_PaiNom, T1.[Tabela_PaiCod] AS Tabela_PaiCod, T3.[Modulo_Nome] AS Tabela_ModuloDes, T1.[Tabela_ModuloCod] AS Tabela_ModuloCod, T4.[Sistema_Nome] AS Tabela_SistemaDes, T1.[Tabela_SistemaCod] AS Tabela_SistemaCod, T1.[Tabela_Descricao], T1.[Tabela_Nome], T1.[Tabela_Codigo]";
         sFromString = " FROM ((([Tabela] T1 WITH (NOLOCK) LEFT JOIN [Tabela] T2 WITH (NOLOCK) ON T2.[Tabela_Codigo] = T1.[Tabela_PaiCod]) LEFT JOIN [Modulo] T3 WITH (NOLOCK) ON T3.[Modulo_Codigo] = T1.[Tabela_ModuloCod]) INNER JOIN [Sistema] T4 WITH (NOLOCK) ON T4.[Sistema_Codigo] = T1.[Tabela_SistemaCod])";
         sOrderString = "";
         if ( ! (0==AV109WWTabelaDS_1_Sistema_areatrabalhocod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Sistema_AreaTrabalhoCod] = @AV109WWTabelaDS_1_Sistema_areatrabalhocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Sistema_AreaTrabalhoCod] = @AV109WWTabelaDS_1_Sistema_areatrabalhocod)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV110WWTabelaDS_2_Dynamicfiltersselector1, "TABELA_NOME") == 0 ) && ( ( AV111WWTabelaDS_3_Dynamicfiltersoperator1 == 0 ) || ( AV111WWTabelaDS_3_Dynamicfiltersoperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV112WWTabelaDS_4_Tabela_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Nome] like @lV112WWTabelaDS_4_Tabela_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Nome] like @lV112WWTabelaDS_4_Tabela_nome1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV110WWTabelaDS_2_Dynamicfiltersselector1, "TABELA_NOME") == 0 ) && ( ( AV111WWTabelaDS_3_Dynamicfiltersoperator1 == 1 ) || ( AV111WWTabelaDS_3_Dynamicfiltersoperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV112WWTabelaDS_4_Tabela_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Nome] like '%' + @lV112WWTabelaDS_4_Tabela_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Nome] like '%' + @lV112WWTabelaDS_4_Tabela_nome1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV110WWTabelaDS_2_Dynamicfiltersselector1, "TABELA_MODULODES") == 0 ) && ( AV111WWTabelaDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113WWTabelaDS_5_Tabela_modulodes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Modulo_Nome] like @lV113WWTabelaDS_5_Tabela_modulodes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Modulo_Nome] like @lV113WWTabelaDS_5_Tabela_modulodes1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV110WWTabelaDS_2_Dynamicfiltersselector1, "TABELA_MODULODES") == 0 ) && ( AV111WWTabelaDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113WWTabelaDS_5_Tabela_modulodes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Modulo_Nome] like '%' + @lV113WWTabelaDS_5_Tabela_modulodes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Modulo_Nome] like '%' + @lV113WWTabelaDS_5_Tabela_modulodes1)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV110WWTabelaDS_2_Dynamicfiltersselector1, "TABELA_MODULOCOD") == 0 ) && ( ! (0==AV114WWTabelaDS_6_Tabela_modulocod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_ModuloCod] = @AV114WWTabelaDS_6_Tabela_modulocod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_ModuloCod] = @AV114WWTabelaDS_6_Tabela_modulocod1)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV116WWTabelaDS_8_Tftabela_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV115WWTabelaDS_7_Tftabela_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Nome] like @lV115WWTabelaDS_7_Tftabela_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Nome] like @lV115WWTabelaDS_7_Tftabela_nome)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV116WWTabelaDS_8_Tftabela_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Nome] = @AV116WWTabelaDS_8_Tftabela_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Nome] = @AV116WWTabelaDS_8_Tftabela_nome_sel)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV118WWTabelaDS_10_Tftabela_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117WWTabelaDS_9_Tftabela_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Descricao] like @lV117WWTabelaDS_9_Tftabela_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Descricao] like @lV117WWTabelaDS_9_Tftabela_descricao)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV118WWTabelaDS_10_Tftabela_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Descricao] = @AV118WWTabelaDS_10_Tftabela_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Descricao] = @AV118WWTabelaDS_10_Tftabela_descricao_sel)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV120WWTabelaDS_12_Tftabela_sistemades_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV119WWTabelaDS_11_Tftabela_sistemades)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Sistema_Nome] like @lV119WWTabelaDS_11_Tftabela_sistemades)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Sistema_Nome] like @lV119WWTabelaDS_11_Tftabela_sistemades)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV120WWTabelaDS_12_Tftabela_sistemades_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Sistema_Nome] = @AV120WWTabelaDS_12_Tftabela_sistemades_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Sistema_Nome] = @AV120WWTabelaDS_12_Tftabela_sistemades_sel)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV122WWTabelaDS_14_Tftabela_modulodes_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV121WWTabelaDS_13_Tftabela_modulodes)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Modulo_Nome] like @lV121WWTabelaDS_13_Tftabela_modulodes)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Modulo_Nome] like @lV121WWTabelaDS_13_Tftabela_modulodes)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV122WWTabelaDS_14_Tftabela_modulodes_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Modulo_Nome] = @AV122WWTabelaDS_14_Tftabela_modulodes_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Modulo_Nome] = @AV122WWTabelaDS_14_Tftabela_modulodes_sel)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV124WWTabelaDS_16_Tftabela_painom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV123WWTabelaDS_15_Tftabela_painom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like @lV123WWTabelaDS_15_Tftabela_painom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like @lV123WWTabelaDS_15_Tftabela_painom)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV124WWTabelaDS_16_Tftabela_painom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] = @AV124WWTabelaDS_16_Tftabela_painom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] = @AV124WWTabelaDS_16_Tftabela_painom_sel)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( ! (0==AV125WWTabelaDS_17_Tftabela_melhoracod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_MelhoraCod] >= @AV125WWTabelaDS_17_Tftabela_melhoracod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_MelhoraCod] >= @AV125WWTabelaDS_17_Tftabela_melhoracod)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( ! (0==AV126WWTabelaDS_18_Tftabela_melhoracod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_MelhoraCod] <= @AV126WWTabelaDS_18_Tftabela_melhoracod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_MelhoraCod] <= @AV126WWTabelaDS_18_Tftabela_melhoracod_to)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( AV127WWTabelaDS_19_Tftabela_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Ativo] = 1)";
            }
         }
         if ( AV127WWTabelaDS_19_Tftabela_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tabela_Nome]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tabela_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tabela_Descricao]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tabela_Descricao] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Sistema_Nome]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Sistema_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Modulo_Nome]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Modulo_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Tabela_Nome]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Tabela_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tabela_MelhoraCod]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tabela_MelhoraCod] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tabela_Ativo]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tabela_Ativo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tabela_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_H00465( IGxContext context ,
                                             int AV109WWTabelaDS_1_Sistema_areatrabalhocod ,
                                             String AV110WWTabelaDS_2_Dynamicfiltersselector1 ,
                                             short AV111WWTabelaDS_3_Dynamicfiltersoperator1 ,
                                             String AV112WWTabelaDS_4_Tabela_nome1 ,
                                             String AV113WWTabelaDS_5_Tabela_modulodes1 ,
                                             int AV114WWTabelaDS_6_Tabela_modulocod1 ,
                                             String AV116WWTabelaDS_8_Tftabela_nome_sel ,
                                             String AV115WWTabelaDS_7_Tftabela_nome ,
                                             String AV118WWTabelaDS_10_Tftabela_descricao_sel ,
                                             String AV117WWTabelaDS_9_Tftabela_descricao ,
                                             String AV120WWTabelaDS_12_Tftabela_sistemades_sel ,
                                             String AV119WWTabelaDS_11_Tftabela_sistemades ,
                                             String AV122WWTabelaDS_14_Tftabela_modulodes_sel ,
                                             String AV121WWTabelaDS_13_Tftabela_modulodes ,
                                             String AV124WWTabelaDS_16_Tftabela_painom_sel ,
                                             String AV123WWTabelaDS_15_Tftabela_painom ,
                                             int AV125WWTabelaDS_17_Tftabela_melhoracod ,
                                             int AV126WWTabelaDS_18_Tftabela_melhoracod_to ,
                                             short AV127WWTabelaDS_19_Tftabela_ativo_sel ,
                                             int A135Sistema_AreaTrabalhoCod ,
                                             String A173Tabela_Nome ,
                                             String A189Tabela_ModuloDes ,
                                             int A188Tabela_ModuloCod ,
                                             String A175Tabela_Descricao ,
                                             String A191Tabela_SistemaDes ,
                                             String A182Tabela_PaiNom ,
                                             int A746Tabela_MelhoraCod ,
                                             bool A174Tabela_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [18] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ((([Tabela] T1 WITH (NOLOCK) LEFT JOIN [Tabela] T4 WITH (NOLOCK) ON T4.[Tabela_Codigo] = T1.[Tabela_PaiCod]) LEFT JOIN [Modulo] T2 WITH (NOLOCK) ON T2.[Modulo_Codigo] = T1.[Tabela_ModuloCod]) INNER JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T1.[Tabela_SistemaCod])";
         if ( ! (0==AV109WWTabelaDS_1_Sistema_areatrabalhocod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Sistema_AreaTrabalhoCod] = @AV109WWTabelaDS_1_Sistema_areatrabalhocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Sistema_AreaTrabalhoCod] = @AV109WWTabelaDS_1_Sistema_areatrabalhocod)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV110WWTabelaDS_2_Dynamicfiltersselector1, "TABELA_NOME") == 0 ) && ( ( AV111WWTabelaDS_3_Dynamicfiltersoperator1 == 0 ) || ( AV111WWTabelaDS_3_Dynamicfiltersoperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV112WWTabelaDS_4_Tabela_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Nome] like @lV112WWTabelaDS_4_Tabela_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Nome] like @lV112WWTabelaDS_4_Tabela_nome1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV110WWTabelaDS_2_Dynamicfiltersselector1, "TABELA_NOME") == 0 ) && ( ( AV111WWTabelaDS_3_Dynamicfiltersoperator1 == 1 ) || ( AV111WWTabelaDS_3_Dynamicfiltersoperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV112WWTabelaDS_4_Tabela_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Nome] like '%' + @lV112WWTabelaDS_4_Tabela_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Nome] like '%' + @lV112WWTabelaDS_4_Tabela_nome1)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV110WWTabelaDS_2_Dynamicfiltersselector1, "TABELA_MODULODES") == 0 ) && ( AV111WWTabelaDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113WWTabelaDS_5_Tabela_modulodes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Modulo_Nome] like @lV113WWTabelaDS_5_Tabela_modulodes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Modulo_Nome] like @lV113WWTabelaDS_5_Tabela_modulodes1)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV110WWTabelaDS_2_Dynamicfiltersselector1, "TABELA_MODULODES") == 0 ) && ( AV111WWTabelaDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113WWTabelaDS_5_Tabela_modulodes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Modulo_Nome] like '%' + @lV113WWTabelaDS_5_Tabela_modulodes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Modulo_Nome] like '%' + @lV113WWTabelaDS_5_Tabela_modulodes1)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV110WWTabelaDS_2_Dynamicfiltersselector1, "TABELA_MODULOCOD") == 0 ) && ( ! (0==AV114WWTabelaDS_6_Tabela_modulocod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_ModuloCod] = @AV114WWTabelaDS_6_Tabela_modulocod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_ModuloCod] = @AV114WWTabelaDS_6_Tabela_modulocod1)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV116WWTabelaDS_8_Tftabela_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV115WWTabelaDS_7_Tftabela_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Nome] like @lV115WWTabelaDS_7_Tftabela_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Nome] like @lV115WWTabelaDS_7_Tftabela_nome)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV116WWTabelaDS_8_Tftabela_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Nome] = @AV116WWTabelaDS_8_Tftabela_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Nome] = @AV116WWTabelaDS_8_Tftabela_nome_sel)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV118WWTabelaDS_10_Tftabela_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117WWTabelaDS_9_Tftabela_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Descricao] like @lV117WWTabelaDS_9_Tftabela_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Descricao] like @lV117WWTabelaDS_9_Tftabela_descricao)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV118WWTabelaDS_10_Tftabela_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Descricao] = @AV118WWTabelaDS_10_Tftabela_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Descricao] = @AV118WWTabelaDS_10_Tftabela_descricao_sel)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV120WWTabelaDS_12_Tftabela_sistemades_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV119WWTabelaDS_11_Tftabela_sistemades)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Sistema_Nome] like @lV119WWTabelaDS_11_Tftabela_sistemades)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Sistema_Nome] like @lV119WWTabelaDS_11_Tftabela_sistemades)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV120WWTabelaDS_12_Tftabela_sistemades_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Sistema_Nome] = @AV120WWTabelaDS_12_Tftabela_sistemades_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Sistema_Nome] = @AV120WWTabelaDS_12_Tftabela_sistemades_sel)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV122WWTabelaDS_14_Tftabela_modulodes_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV121WWTabelaDS_13_Tftabela_modulodes)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Modulo_Nome] like @lV121WWTabelaDS_13_Tftabela_modulodes)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Modulo_Nome] like @lV121WWTabelaDS_13_Tftabela_modulodes)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV122WWTabelaDS_14_Tftabela_modulodes_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Modulo_Nome] = @AV122WWTabelaDS_14_Tftabela_modulodes_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Modulo_Nome] = @AV122WWTabelaDS_14_Tftabela_modulodes_sel)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV124WWTabelaDS_16_Tftabela_painom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV123WWTabelaDS_15_Tftabela_painom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Tabela_Nome] like @lV123WWTabelaDS_15_Tftabela_painom)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Tabela_Nome] like @lV123WWTabelaDS_15_Tftabela_painom)";
            }
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV124WWTabelaDS_16_Tftabela_painom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Tabela_Nome] = @AV124WWTabelaDS_16_Tftabela_painom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Tabela_Nome] = @AV124WWTabelaDS_16_Tftabela_painom_sel)";
            }
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( ! (0==AV125WWTabelaDS_17_Tftabela_melhoracod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_MelhoraCod] >= @AV125WWTabelaDS_17_Tftabela_melhoracod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_MelhoraCod] >= @AV125WWTabelaDS_17_Tftabela_melhoracod)";
            }
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( ! (0==AV126WWTabelaDS_18_Tftabela_melhoracod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_MelhoraCod] <= @AV126WWTabelaDS_18_Tftabela_melhoracod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_MelhoraCod] <= @AV126WWTabelaDS_18_Tftabela_melhoracod_to)";
            }
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( AV127WWTabelaDS_19_Tftabela_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Ativo] = 1)";
            }
         }
         if ( AV127WWTabelaDS_19_Tftabela_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Tabela_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Tabela_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 2 :
                     return conditional_H00464(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (short)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (int)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (short)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (int)dynConstraints[26] , (bool)dynConstraints[27] , (short)dynConstraints[28] , (bool)dynConstraints[29] );
               case 3 :
                     return conditional_H00465(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (short)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (int)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (short)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (int)dynConstraints[26] , (bool)dynConstraints[27] , (short)dynConstraints[28] , (bool)dynConstraints[29] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00462 ;
          prmH00462 = new Object[] {
          } ;
          Object[] prmH00463 ;
          prmH00463 = new Object[] {
          } ;
          Object[] prmH00464 ;
          prmH00464 = new Object[] {
          new Object[] {"@AV109WWTabelaDS_1_Sistema_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV112WWTabelaDS_4_Tabela_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV112WWTabelaDS_4_Tabela_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV113WWTabelaDS_5_Tabela_modulodes1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV113WWTabelaDS_5_Tabela_modulodes1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV114WWTabelaDS_6_Tabela_modulocod1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV115WWTabelaDS_7_Tftabela_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV116WWTabelaDS_8_Tftabela_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV117WWTabelaDS_9_Tftabela_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV118WWTabelaDS_10_Tftabela_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV119WWTabelaDS_11_Tftabela_sistemades",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV120WWTabelaDS_12_Tftabela_sistemades_sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV121WWTabelaDS_13_Tftabela_modulodes",SqlDbType.Char,50,0} ,
          new Object[] {"@AV122WWTabelaDS_14_Tftabela_modulodes_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV123WWTabelaDS_15_Tftabela_painom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV124WWTabelaDS_16_Tftabela_painom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV125WWTabelaDS_17_Tftabela_melhoracod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV126WWTabelaDS_18_Tftabela_melhoracod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00465 ;
          prmH00465 = new Object[] {
          new Object[] {"@AV109WWTabelaDS_1_Sistema_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV112WWTabelaDS_4_Tabela_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV112WWTabelaDS_4_Tabela_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV113WWTabelaDS_5_Tabela_modulodes1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV113WWTabelaDS_5_Tabela_modulodes1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV114WWTabelaDS_6_Tabela_modulocod1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV115WWTabelaDS_7_Tftabela_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV116WWTabelaDS_8_Tftabela_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV117WWTabelaDS_9_Tftabela_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV118WWTabelaDS_10_Tftabela_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV119WWTabelaDS_11_Tftabela_sistemades",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV120WWTabelaDS_12_Tftabela_sistemades_sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV121WWTabelaDS_13_Tftabela_modulodes",SqlDbType.Char,50,0} ,
          new Object[] {"@AV122WWTabelaDS_14_Tftabela_modulodes_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV123WWTabelaDS_15_Tftabela_painom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV124WWTabelaDS_16_Tftabela_painom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV125WWTabelaDS_17_Tftabela_melhoracod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV126WWTabelaDS_18_Tftabela_melhoracod_to",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00462", "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao], [AreaTrabalho_Ativo] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Ativo] = 1 ORDER BY [AreaTrabalho_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00462,0,0,true,false )
             ,new CursorDef("H00463", "SELECT [Modulo_Codigo], [Modulo_Nome] FROM [Modulo] WITH (NOLOCK) ORDER BY [Modulo_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00463,0,0,true,false )
             ,new CursorDef("H00464", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00464,11,0,true,false )
             ,new CursorDef("H00465", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00465,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 50) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                ((String[]) buf[16])[0] = rslt.getLongVarchar(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getString(11, 50) ;
                ((int[]) buf[19])[0] = rslt.getInt(12) ;
                return;
             case 3 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                return;
       }
    }

 }

}
