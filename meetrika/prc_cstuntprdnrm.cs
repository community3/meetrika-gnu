/*
               File: PRC_CstUntPrdNrm
        Description: Custo unit�rio de produ��o normal
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:5.87
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_cstuntprdnrm : GXProcedure
   {
      public prc_cstuntprdnrm( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_cstuntprdnrm( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContratoServicos_Codigo ,
                           ref int aP1_ContratadaUsuario_UsuarioCod ,
                           ref decimal aP2_CstUntNrm ,
                           out String aP3_CalculoPFinal )
      {
         this.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         this.A69ContratadaUsuario_UsuarioCod = aP1_ContratadaUsuario_UsuarioCod;
         this.AV8CstUntNrm = aP2_CstUntNrm;
         this.AV9CalculoPFinal = "" ;
         initialize();
         executePrivate();
         aP0_ContratoServicos_Codigo=this.A160ContratoServicos_Codigo;
         aP1_ContratadaUsuario_UsuarioCod=this.A69ContratadaUsuario_UsuarioCod;
         aP2_CstUntNrm=this.AV8CstUntNrm;
         aP3_CalculoPFinal=this.AV9CalculoPFinal;
      }

      public String executeUdp( ref int aP0_ContratoServicos_Codigo ,
                                ref int aP1_ContratadaUsuario_UsuarioCod ,
                                ref decimal aP2_CstUntNrm )
      {
         this.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         this.A69ContratadaUsuario_UsuarioCod = aP1_ContratadaUsuario_UsuarioCod;
         this.AV8CstUntNrm = aP2_CstUntNrm;
         this.AV9CalculoPFinal = "" ;
         initialize();
         executePrivate();
         aP0_ContratoServicos_Codigo=this.A160ContratoServicos_Codigo;
         aP1_ContratadaUsuario_UsuarioCod=this.A69ContratadaUsuario_UsuarioCod;
         aP2_CstUntNrm=this.AV8CstUntNrm;
         aP3_CalculoPFinal=this.AV9CalculoPFinal;
         return AV9CalculoPFinal ;
      }

      public void executeSubmit( ref int aP0_ContratoServicos_Codigo ,
                                 ref int aP1_ContratadaUsuario_UsuarioCod ,
                                 ref decimal aP2_CstUntNrm ,
                                 out String aP3_CalculoPFinal )
      {
         prc_cstuntprdnrm objprc_cstuntprdnrm;
         objprc_cstuntprdnrm = new prc_cstuntprdnrm();
         objprc_cstuntprdnrm.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         objprc_cstuntprdnrm.A69ContratadaUsuario_UsuarioCod = aP1_ContratadaUsuario_UsuarioCod;
         objprc_cstuntprdnrm.AV8CstUntNrm = aP2_CstUntNrm;
         objprc_cstuntprdnrm.AV9CalculoPFinal = "" ;
         objprc_cstuntprdnrm.context.SetSubmitInitialConfig(context);
         objprc_cstuntprdnrm.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_cstuntprdnrm);
         aP0_ContratoServicos_Codigo=this.A160ContratoServicos_Codigo;
         aP1_ContratadaUsuario_UsuarioCod=this.A69ContratadaUsuario_UsuarioCod;
         aP2_CstUntNrm=this.AV8CstUntNrm;
         aP3_CalculoPFinal=this.AV9CalculoPFinal;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_cstuntprdnrm)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00972 */
         pr_default.execute(0, new Object[] {A160ContratoServicos_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A74Contrato_Codigo = P00972_A74Contrato_Codigo[0];
            A75Contrato_AreaTrabalhoCod = P00972_A75Contrato_AreaTrabalhoCod[0];
            A642AreaTrabalho_CalculoPFinal = P00972_A642AreaTrabalho_CalculoPFinal[0];
            n642AreaTrabalho_CalculoPFinal = P00972_n642AreaTrabalho_CalculoPFinal[0];
            A39Contratada_Codigo = P00972_A39Contratada_Codigo[0];
            A75Contrato_AreaTrabalhoCod = P00972_A75Contrato_AreaTrabalhoCod[0];
            A39Contratada_Codigo = P00972_A39Contratada_Codigo[0];
            A642AreaTrabalho_CalculoPFinal = P00972_A642AreaTrabalho_CalculoPFinal[0];
            n642AreaTrabalho_CalculoPFinal = P00972_n642AreaTrabalho_CalculoPFinal[0];
            AV12Contratada_Codigo = A39Contratada_Codigo;
            AV16GXLvl4 = 0;
            /* Using cursor P00973 */
            pr_default.execute(1, new Object[] {A69ContratadaUsuario_UsuarioCod, A160ContratoServicos_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A1471ContratoServicosCusto_CntSrvCod = P00973_A1471ContratoServicosCusto_CntSrvCod[0];
               A1472ContratoServicosCusto_UsuarioCod = P00973_A1472ContratoServicosCusto_UsuarioCod[0];
               A1474ContratoServicosCusto_CstUntPrdNrm = P00973_A1474ContratoServicosCusto_CstUntPrdNrm[0];
               A1473ContratoServicosCusto_Codigo = P00973_A1473ContratoServicosCusto_Codigo[0];
               AV16GXLvl4 = 1;
               AV8CstUntNrm = A1474ContratoServicosCusto_CstUntPrdNrm;
               AV9CalculoPFinal = A642AreaTrabalho_CalculoPFinal;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(1);
            }
            pr_default.close(1);
            if ( AV16GXLvl4 == 0 )
            {
               /* Using cursor P00974 */
               pr_default.execute(2, new Object[] {AV12Contratada_Codigo, A69ContratadaUsuario_UsuarioCod});
               while ( (pr_default.getStatus(2) != 101) )
               {
                  A1228ContratadaUsuario_AreaTrabalhoCod = P00974_A1228ContratadaUsuario_AreaTrabalhoCod[0];
                  n1228ContratadaUsuario_AreaTrabalhoCod = P00974_n1228ContratadaUsuario_AreaTrabalhoCod[0];
                  A642AreaTrabalho_CalculoPFinal = P00974_A642AreaTrabalho_CalculoPFinal[0];
                  n642AreaTrabalho_CalculoPFinal = P00974_n642AreaTrabalho_CalculoPFinal[0];
                  A66ContratadaUsuario_ContratadaCod = P00974_A66ContratadaUsuario_ContratadaCod[0];
                  A576ContratadaUsuario_CstUntPrdNrm = P00974_A576ContratadaUsuario_CstUntPrdNrm[0];
                  n576ContratadaUsuario_CstUntPrdNrm = P00974_n576ContratadaUsuario_CstUntPrdNrm[0];
                  A1228ContratadaUsuario_AreaTrabalhoCod = P00974_A1228ContratadaUsuario_AreaTrabalhoCod[0];
                  n1228ContratadaUsuario_AreaTrabalhoCod = P00974_n1228ContratadaUsuario_AreaTrabalhoCod[0];
                  A642AreaTrabalho_CalculoPFinal = P00974_A642AreaTrabalho_CalculoPFinal[0];
                  n642AreaTrabalho_CalculoPFinal = P00974_n642AreaTrabalho_CalculoPFinal[0];
                  AV8CstUntNrm = A576ContratadaUsuario_CstUntPrdNrm;
                  /* Using cursor P00975 */
                  pr_default.execute(3, new Object[] {n1228ContratadaUsuario_AreaTrabalhoCod, A1228ContratadaUsuario_AreaTrabalhoCod});
                  while ( (pr_default.getStatus(3) != 101) )
                  {
                     A5AreaTrabalho_Codigo = P00975_A5AreaTrabalho_Codigo[0];
                     AV9CalculoPFinal = A642AreaTrabalho_CalculoPFinal;
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                     /* Exiting from a For First loop. */
                     if (true) break;
                  }
                  pr_default.close(3);
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(2);
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00972_A74Contrato_Codigo = new int[1] ;
         P00972_A75Contrato_AreaTrabalhoCod = new int[1] ;
         P00972_A160ContratoServicos_Codigo = new int[1] ;
         P00972_A642AreaTrabalho_CalculoPFinal = new String[] {""} ;
         P00972_n642AreaTrabalho_CalculoPFinal = new bool[] {false} ;
         P00972_A39Contratada_Codigo = new int[1] ;
         A642AreaTrabalho_CalculoPFinal = "";
         P00973_A1471ContratoServicosCusto_CntSrvCod = new int[1] ;
         P00973_A1472ContratoServicosCusto_UsuarioCod = new int[1] ;
         P00973_A1474ContratoServicosCusto_CstUntPrdNrm = new decimal[1] ;
         P00973_A1473ContratoServicosCusto_Codigo = new int[1] ;
         P00974_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P00974_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         P00974_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         P00974_A642AreaTrabalho_CalculoPFinal = new String[] {""} ;
         P00974_n642AreaTrabalho_CalculoPFinal = new bool[] {false} ;
         P00974_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P00974_A576ContratadaUsuario_CstUntPrdNrm = new decimal[1] ;
         P00974_n576ContratadaUsuario_CstUntPrdNrm = new bool[] {false} ;
         P00975_A5AreaTrabalho_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_cstuntprdnrm__default(),
            new Object[][] {
                new Object[] {
               P00972_A74Contrato_Codigo, P00972_A75Contrato_AreaTrabalhoCod, P00972_A160ContratoServicos_Codigo, P00972_A642AreaTrabalho_CalculoPFinal, P00972_n642AreaTrabalho_CalculoPFinal, P00972_A39Contratada_Codigo
               }
               , new Object[] {
               P00973_A1471ContratoServicosCusto_CntSrvCod, P00973_A1472ContratoServicosCusto_UsuarioCod, P00973_A1474ContratoServicosCusto_CstUntPrdNrm, P00973_A1473ContratoServicosCusto_Codigo
               }
               , new Object[] {
               P00974_A69ContratadaUsuario_UsuarioCod, P00974_A1228ContratadaUsuario_AreaTrabalhoCod, P00974_n1228ContratadaUsuario_AreaTrabalhoCod, P00974_A642AreaTrabalho_CalculoPFinal, P00974_n642AreaTrabalho_CalculoPFinal, P00974_A66ContratadaUsuario_ContratadaCod, P00974_A576ContratadaUsuario_CstUntPrdNrm, P00974_n576ContratadaUsuario_CstUntPrdNrm
               }
               , new Object[] {
               P00975_A5AreaTrabalho_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV16GXLvl4 ;
      private int A160ContratoServicos_Codigo ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A74Contrato_Codigo ;
      private int A75Contrato_AreaTrabalhoCod ;
      private int A39Contratada_Codigo ;
      private int AV12Contratada_Codigo ;
      private int A1471ContratoServicosCusto_CntSrvCod ;
      private int A1472ContratoServicosCusto_UsuarioCod ;
      private int A1473ContratoServicosCusto_Codigo ;
      private int A1228ContratadaUsuario_AreaTrabalhoCod ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A5AreaTrabalho_Codigo ;
      private decimal AV8CstUntNrm ;
      private decimal A1474ContratoServicosCusto_CstUntPrdNrm ;
      private decimal A576ContratadaUsuario_CstUntPrdNrm ;
      private String AV9CalculoPFinal ;
      private String scmdbuf ;
      private String A642AreaTrabalho_CalculoPFinal ;
      private bool n642AreaTrabalho_CalculoPFinal ;
      private bool n1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool n576ContratadaUsuario_CstUntPrdNrm ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContratoServicos_Codigo ;
      private int aP1_ContratadaUsuario_UsuarioCod ;
      private decimal aP2_CstUntNrm ;
      private IDataStoreProvider pr_default ;
      private int[] P00972_A74Contrato_Codigo ;
      private int[] P00972_A75Contrato_AreaTrabalhoCod ;
      private int[] P00972_A160ContratoServicos_Codigo ;
      private String[] P00972_A642AreaTrabalho_CalculoPFinal ;
      private bool[] P00972_n642AreaTrabalho_CalculoPFinal ;
      private int[] P00972_A39Contratada_Codigo ;
      private int[] P00973_A1471ContratoServicosCusto_CntSrvCod ;
      private int[] P00973_A1472ContratoServicosCusto_UsuarioCod ;
      private decimal[] P00973_A1474ContratoServicosCusto_CstUntPrdNrm ;
      private int[] P00973_A1473ContratoServicosCusto_Codigo ;
      private int[] P00974_A69ContratadaUsuario_UsuarioCod ;
      private int[] P00974_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] P00974_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private String[] P00974_A642AreaTrabalho_CalculoPFinal ;
      private bool[] P00974_n642AreaTrabalho_CalculoPFinal ;
      private int[] P00974_A66ContratadaUsuario_ContratadaCod ;
      private decimal[] P00974_A576ContratadaUsuario_CstUntPrdNrm ;
      private bool[] P00974_n576ContratadaUsuario_CstUntPrdNrm ;
      private int[] P00975_A5AreaTrabalho_Codigo ;
      private String aP3_CalculoPFinal ;
   }

   public class prc_cstuntprdnrm__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00972 ;
          prmP00972 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00973 ;
          prmP00973 = new Object[] {
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00974 ;
          prmP00974 = new Object[] {
          new Object[] {"@AV12Contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00975 ;
          prmP00975 = new Object[] {
          new Object[] {"@ContratadaUsuario_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00972", "SELECT T1.[Contrato_Codigo], T2.[Contrato_AreaTrabalhoCod] AS Contrato_AreaTrabalhoCod, T1.[ContratoServicos_Codigo], T3.[AreaTrabalho_CalculoPFinal], T2.[Contratada_Codigo] FROM (([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [AreaTrabalho] T3 WITH (NOLOCK) ON T3.[AreaTrabalho_Codigo] = T2.[Contrato_AreaTrabalhoCod]) WHERE T1.[ContratoServicos_Codigo] = @ContratoServicos_Codigo ORDER BY T1.[ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00972,1,0,true,true )
             ,new CursorDef("P00973", "SELECT TOP 1 [ContratoServicosCusto_CntSrvCod], [ContratoServicosCusto_UsuarioCod], [ContratoServicosCusto_CstUntPrdNrm], [ContratoServicosCusto_Codigo] FROM [ContratoServicosCusto] WITH (NOLOCK) WHERE ([ContratoServicosCusto_UsuarioCod] = @ContratadaUsuario_UsuarioCod) AND ([ContratoServicosCusto_CntSrvCod] = @ContratoServicos_Codigo) AND ([ContratoServicosCusto_CstUntPrdNrm] > 0) ORDER BY [ContratoServicosCusto_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00973,1,0,false,true )
             ,new CursorDef("P00974", "SELECT TOP 1 T1.[ContratadaUsuario_UsuarioCod], T2.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod, T3.[AreaTrabalho_CalculoPFinal], T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T1.[ContratadaUsuario_CstUntPrdNrm] FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) LEFT JOIN [AreaTrabalho] T3 WITH (NOLOCK) ON T3.[AreaTrabalho_Codigo] = T2.[Contratada_AreaTrabalhoCod]) WHERE T1.[ContratadaUsuario_ContratadaCod] = @AV12Contratada_Codigo and T1.[ContratadaUsuario_UsuarioCod] = @ContratadaUsuario_UsuarioCod ORDER BY T1.[ContratadaUsuario_ContratadaCod], T1.[ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00974,1,0,true,true )
             ,new CursorDef("P00975", "SELECT TOP 1 [AreaTrabalho_Codigo] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @ContratadaUsuario_AreaTrabalhoCod ORDER BY [AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00975,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 2) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 2) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
