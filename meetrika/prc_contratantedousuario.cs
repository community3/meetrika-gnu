/*
               File: PRC_ContratanteDoUsuario
        Description: Contratante Do Usuario
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:54.47
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_contratantedousuario : GXProcedure
   {
      public prc_contratantedousuario( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_contratantedousuario( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_Codigo ,
                           ref int aP1_ContratanteUsuario_UsuarioCod ,
                           out int aP2_Contratante_Codigo )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.A60ContratanteUsuario_UsuarioCod = aP1_ContratanteUsuario_UsuarioCod;
         this.AV9Contratante_Codigo = 0 ;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP1_ContratanteUsuario_UsuarioCod=this.A60ContratanteUsuario_UsuarioCod;
         aP2_Contratante_Codigo=this.AV9Contratante_Codigo;
      }

      public int executeUdp( ref int aP0_ContagemResultado_Codigo ,
                             ref int aP1_ContratanteUsuario_UsuarioCod )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.A60ContratanteUsuario_UsuarioCod = aP1_ContratanteUsuario_UsuarioCod;
         this.AV9Contratante_Codigo = 0 ;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP1_ContratanteUsuario_UsuarioCod=this.A60ContratanteUsuario_UsuarioCod;
         aP2_Contratante_Codigo=this.AV9Contratante_Codigo;
         return AV9Contratante_Codigo ;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_Codigo ,
                                 ref int aP1_ContratanteUsuario_UsuarioCod ,
                                 out int aP2_Contratante_Codigo )
      {
         prc_contratantedousuario objprc_contratantedousuario;
         objprc_contratantedousuario = new prc_contratantedousuario();
         objprc_contratantedousuario.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_contratantedousuario.A60ContratanteUsuario_UsuarioCod = aP1_ContratanteUsuario_UsuarioCod;
         objprc_contratantedousuario.AV9Contratante_Codigo = 0 ;
         objprc_contratantedousuario.context.SetSubmitInitialConfig(context);
         objprc_contratantedousuario.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_contratantedousuario);
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP1_ContratanteUsuario_UsuarioCod=this.A60ContratanteUsuario_UsuarioCod;
         aP2_Contratante_Codigo=this.AV9Contratante_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_contratantedousuario)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00892 */
         pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A490ContagemResultado_ContratadaCod = P00892_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00892_n490ContagemResultado_ContratadaCod[0];
            A52Contratada_AreaTrabalhoCod = P00892_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00892_n52Contratada_AreaTrabalhoCod[0];
            A489ContagemResultado_SistemaCod = P00892_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00892_n489ContagemResultado_SistemaCod[0];
            A496Contagemresultado_SistemaAreaCod = P00892_A496Contagemresultado_SistemaAreaCod[0];
            n496Contagemresultado_SistemaAreaCod = P00892_n496Contagemresultado_SistemaAreaCod[0];
            A52Contratada_AreaTrabalhoCod = P00892_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00892_n52Contratada_AreaTrabalhoCod[0];
            A496Contagemresultado_SistemaAreaCod = P00892_A496Contagemresultado_SistemaAreaCod[0];
            n496Contagemresultado_SistemaAreaCod = P00892_n496Contagemresultado_SistemaAreaCod[0];
            if ( A490ContagemResultado_ContratadaCod > 0 )
            {
               AV10AreaTrabalho_Codigo = A52Contratada_AreaTrabalhoCod;
            }
            else if ( A489ContagemResultado_SistemaCod > 0 )
            {
               AV10AreaTrabalho_Codigo = A496Contagemresultado_SistemaAreaCod;
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         /* Using cursor P00894 */
         pr_default.execute(1, new Object[] {A60ContratanteUsuario_UsuarioCod, AV10AreaTrabalho_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A63ContratanteUsuario_ContratanteCod = P00894_A63ContratanteUsuario_ContratanteCod[0];
            A1020ContratanteUsuario_AreaTrabalhoCod = P00894_A1020ContratanteUsuario_AreaTrabalhoCod[0];
            n1020ContratanteUsuario_AreaTrabalhoCod = P00894_n1020ContratanteUsuario_AreaTrabalhoCod[0];
            A1020ContratanteUsuario_AreaTrabalhoCod = P00894_A1020ContratanteUsuario_AreaTrabalhoCod[0];
            n1020ContratanteUsuario_AreaTrabalhoCod = P00894_n1020ContratanteUsuario_AreaTrabalhoCod[0];
            OV9Contratante_Codigo = AV9Contratante_Codigo;
            AV9Contratante_Codigo = A63ContratanteUsuario_ContratanteCod;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(1);
         }
         pr_default.close(1);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00892_A456ContagemResultado_Codigo = new int[1] ;
         P00892_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00892_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00892_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00892_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00892_A489ContagemResultado_SistemaCod = new int[1] ;
         P00892_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00892_A496Contagemresultado_SistemaAreaCod = new int[1] ;
         P00892_n496Contagemresultado_SistemaAreaCod = new bool[] {false} ;
         P00894_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         P00894_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         P00894_A1020ContratanteUsuario_AreaTrabalhoCod = new int[1] ;
         P00894_n1020ContratanteUsuario_AreaTrabalhoCod = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_contratantedousuario__default(),
            new Object[][] {
                new Object[] {
               P00892_A456ContagemResultado_Codigo, P00892_A490ContagemResultado_ContratadaCod, P00892_n490ContagemResultado_ContratadaCod, P00892_A52Contratada_AreaTrabalhoCod, P00892_n52Contratada_AreaTrabalhoCod, P00892_A489ContagemResultado_SistemaCod, P00892_n489ContagemResultado_SistemaCod, P00892_A496Contagemresultado_SistemaAreaCod, P00892_n496Contagemresultado_SistemaAreaCod
               }
               , new Object[] {
               P00894_A60ContratanteUsuario_UsuarioCod, P00894_A63ContratanteUsuario_ContratanteCod, P00894_A1020ContratanteUsuario_AreaTrabalhoCod, P00894_n1020ContratanteUsuario_AreaTrabalhoCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A456ContagemResultado_Codigo ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int AV9Contratante_Codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A489ContagemResultado_SistemaCod ;
      private int A496Contagemresultado_SistemaAreaCod ;
      private int AV10AreaTrabalho_Codigo ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A1020ContratanteUsuario_AreaTrabalhoCod ;
      private int OV9Contratante_Codigo ;
      private String scmdbuf ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n496Contagemresultado_SistemaAreaCod ;
      private bool n1020ContratanteUsuario_AreaTrabalhoCod ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_Codigo ;
      private int aP1_ContratanteUsuario_UsuarioCod ;
      private IDataStoreProvider pr_default ;
      private int[] P00892_A456ContagemResultado_Codigo ;
      private int[] P00892_A490ContagemResultado_ContratadaCod ;
      private bool[] P00892_n490ContagemResultado_ContratadaCod ;
      private int[] P00892_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00892_n52Contratada_AreaTrabalhoCod ;
      private int[] P00892_A489ContagemResultado_SistemaCod ;
      private bool[] P00892_n489ContagemResultado_SistemaCod ;
      private int[] P00892_A496Contagemresultado_SistemaAreaCod ;
      private bool[] P00892_n496Contagemresultado_SistemaAreaCod ;
      private int[] P00894_A60ContratanteUsuario_UsuarioCod ;
      private int[] P00894_A63ContratanteUsuario_ContratanteCod ;
      private int[] P00894_A1020ContratanteUsuario_AreaTrabalhoCod ;
      private bool[] P00894_n1020ContratanteUsuario_AreaTrabalhoCod ;
      private int aP2_Contratante_Codigo ;
   }

   public class prc_contratantedousuario__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00892 ;
          prmP00892 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00894 ;
          prmP00894 = new Object[] {
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV10AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00892", "SELECT TOP 1 T1.[ContagemResultado_Codigo], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T2.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T3.[Sistema_AreaTrabalhoCod] AS Contagemresultado_SistemaAreaCod FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) WHERE T1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00892,1,0,false,true )
             ,new CursorDef("P00894", "SELECT TOP 1 T1.[ContratanteUsuario_UsuarioCod], T1.[ContratanteUsuario_ContratanteCod], COALESCE( T2.[ContratanteUsuario_AreaTrabalhoCod], 0) AS ContratanteUsuario_AreaTrabalhoCod FROM ([ContratanteUsuario] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN(T3.[AreaTrabalho_Codigo]) AS ContratanteUsuario_AreaTrabalhoCod, T4.[ContratanteUsuario_ContratanteCod], T4.[ContratanteUsuario_UsuarioCod] FROM [AreaTrabalho] T3 WITH (NOLOCK),  [ContratanteUsuario] T4 WITH (NOLOCK) WHERE T3.[Contratante_Codigo] = T4.[ContratanteUsuario_ContratanteCod] GROUP BY T4.[ContratanteUsuario_ContratanteCod], T4.[ContratanteUsuario_UsuarioCod] ) T2 ON T2.[ContratanteUsuario_ContratanteCod] = T1.[ContratanteUsuario_ContratanteCod] AND T2.[ContratanteUsuario_UsuarioCod] = T1.[ContratanteUsuario_UsuarioCod]) WHERE (T1.[ContratanteUsuario_UsuarioCod] = @ContratanteUsuario_UsuarioCod) AND (COALESCE( T2.[ContratanteUsuario_AreaTrabalhoCod], 0) = @AV10AreaTrabalho_Codigo) ORDER BY T1.[ContratanteUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00894,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
