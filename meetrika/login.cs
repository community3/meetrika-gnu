/*
               File: Login
        Description: .: Login :. Meetrika
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 21:58:15.80
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class login : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public login( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public login( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         chkavRememberme = new GXCheckbox();
         chkavKeepmeloggedin = new GXCheckbox();
         chkavUsuario_inativo = new GXCheckbox();
         chkavUsuario_deferias = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PA2X2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               Gx_date = DateTimeUtil.Today( context);
               context.Gx_err = 0;
               WS2X2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE2X2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( ".: Login :. Meetrika") ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202032621581767");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText(  "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"\" novalidate action=\""+formatLink("login.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "USUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "USUARIO_EHAUDITORFM", A290Usuario_EhAuditorFM);
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Title", StringUtil.RTrim( Confirmpanel_Title));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Confirmationtext", StringUtil.RTrim( Confirmpanel_Confirmationtext));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Yesbuttoncaption", StringUtil.RTrim( Confirmpanel_Yesbuttoncaption));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Nobuttoncaption", StringUtil.RTrim( Confirmpanel_Nobuttoncaption));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Yesbuttonposition", StringUtil.RTrim( Confirmpanel_Yesbuttonposition));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL2_Title", StringUtil.RTrim( Confirmpanel2_Title));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL2_Confirmationtext", StringUtil.RTrim( Confirmpanel2_Confirmationtext));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL2_Yesbuttoncaption", StringUtil.RTrim( Confirmpanel2_Yesbuttoncaption));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL2_Nobuttoncaption", StringUtil.RTrim( Confirmpanel2_Nobuttoncaption));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL2_Cancelbuttoncaption", StringUtil.RTrim( Confirmpanel2_Cancelbuttoncaption));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL2_Yesbuttonposition", StringUtil.RTrim( Confirmpanel2_Yesbuttonposition));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL2_Confirmtype", StringUtil.RTrim( Confirmpanel2_Confirmtype));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Result", StringUtil.RTrim( Confirmpanel_Result));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm2X2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         context.WriteHtmlTextNl( "</form>") ;
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
      }

      public override String GetPgmname( )
      {
         return "Login" ;
      }

      public override String GetPgmdesc( )
      {
         return ".: Login :. Meetrika" ;
      }

      protected void WB2X0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            wb_table1_2_2X2( true) ;
         }
         else
         {
            wb_table1_2_2X2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_2X2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Div Control */
            GxWebStd.gx_div_start( context, divContainer_Internalname, 1, 0, "px", 0, "px", "container-fluid", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divCollg12_Internalname, 1, 0, "px", 0, "px", "col-lg-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divPanellogin_Internalname, 1, 0, "px", 0, "px", "panel-login", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divLogo1_Internalname, 1, 0, "px", 0, "px", "logo", "left", "top", "", "", "div");
            GxWebStd.gx_div_end( context, "left", "top");
            /* Div Control */
            GxWebStd.gx_div_start( context, divFormgroup_Internalname, 1, 0, "px", 0, "px", "form-group", "left", "top", "", "", "div");
            GxWebStd.gx_div_end( context, "left", "top");
            /* Div Control */
            GxWebStd.gx_div_start( context, divCheckbox_Internalname, 1, 0, "px", 0, "px", "", "left", "top", "", "", "div");
            wb_table2_67_2X2( true) ;
         }
         else
         {
            wb_table2_67_2X2( false) ;
         }
         return  ;
      }

      protected void wb_table2_67_2X2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"CONFIRMPANELContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"CONFIRMPANELContainer"+"Body"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "</div>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"CONFIRMPANEL2Container"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"CONFIRMPANEL2Container"+"Body"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "</div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavUsuario_inativo_Internalname, StringUtil.BoolToStr( AV50Usuario_Inativo), "", "", chkavUsuario_inativo.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(90, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,90);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavUsuario_deferias_Internalname, StringUtil.BoolToStr( AV48Usuario_DeFerias), "", "", chkavUsuario_deferias.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(91, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,91);\"");
         }
         wbLoad = true;
      }

      protected void START2X2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", ".: Login :. Meetrika", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP2X0( ) ;
      }

      protected void WS2X2( )
      {
         START2X2( ) ;
         EVT2X2( ) ;
      }

      protected void EVT2X2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "CONFIRMPANEL.CLOSE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E112X2 */
                           E112X2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "CONFIRMPANEL2.CLOSE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E122X2 */
                           E122X2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E132X2 */
                           E132X2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E142X2 */
                           E142X2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( ! wbErr )
                           {
                              Rfr0gs = false;
                              if ( ! Rfr0gs )
                              {
                                 /* Execute user event: E152X2 */
                                 E152X2 ();
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'FORGOTPASSWORD'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E162X2 */
                           E162X2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VBUTTONFACEBOOK.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E172X2 */
                           E172X2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VBUTTONGOOGLE.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E182X2 */
                           E182X2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VBUTTONTWITTER.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E192X2 */
                           E192X2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VBUTTONGAMREMOTE.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E202X2 */
                           E202X2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E212X2 */
                           E212X2 ();
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE2X2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm2X2( ) ;
            }
         }
      }

      protected void PA2X2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            chkavRememberme.Name = "vREMEMBERME";
            chkavRememberme.WebTags = "";
            chkavRememberme.Caption = "Lembrar de Mim";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavRememberme_Internalname, "TitleCaption", chkavRememberme.Caption);
            chkavRememberme.CheckedValue = "false";
            chkavKeepmeloggedin.Name = "vKEEPMELOGGEDIN";
            chkavKeepmeloggedin.WebTags = "";
            chkavKeepmeloggedin.Caption = "Mantenha-me logado";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavKeepmeloggedin_Internalname, "TitleCaption", chkavKeepmeloggedin.Caption);
            chkavKeepmeloggedin.CheckedValue = "false";
            chkavUsuario_inativo.Name = "vUSUARIO_INATIVO";
            chkavUsuario_inativo.WebTags = "";
            chkavUsuario_inativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavUsuario_inativo_Internalname, "TitleCaption", chkavUsuario_inativo.Caption);
            chkavUsuario_inativo.CheckedValue = "false";
            chkavUsuario_deferias.Name = "vUSUARIO_DEFERIAS";
            chkavUsuario_deferias.WebTags = "";
            chkavUsuario_deferias.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavUsuario_deferias_Internalname, "TitleCaption", chkavUsuario_deferias.Caption);
            chkavUsuario_deferias.CheckedValue = "false";
            if ( toggleJsOutput )
            {
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = chkavRememberme_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF2X2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      protected void RF2X2( )
      {
         /* Execute user event: E142X2 */
         E142X2 ();
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E212X2 */
            E212X2 ();
            WB2X0( ) ;
         }
      }

      protected void STRUP2X0( )
      {
         /* Before Start, stand alone formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E132X2 */
         E132X2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV10ButtonFacebook = cgiGet( imgavButtonfacebook_Internalname);
            AV12ButtonGoogle = cgiGet( imgavButtongoogle_Internalname);
            AV13ButtonTwitter = cgiGet( imgavButtontwitter_Internalname);
            AV11ButtonGAMRemote = cgiGet( imgavButtongamremote_Internalname);
            AV28RememberMe = StringUtil.StrToBool( cgiGet( chkavRememberme_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28RememberMe", AV28RememberMe);
            AV33UserName = cgiGet( edtavUsername_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33UserName", AV33UserName);
            AV34UserPassword = cgiGet( edtavUserpassword_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34UserPassword", AV34UserPassword);
            AV23KeepMeLoggedIn = StringUtil.StrToBool( cgiGet( chkavKeepmeloggedin_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23KeepMeLoggedIn", AV23KeepMeLoggedIn);
            AV50Usuario_Inativo = StringUtil.StrToBool( cgiGet( chkavUsuario_inativo_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50Usuario_Inativo", AV50Usuario_Inativo);
            AV48Usuario_DeFerias = StringUtil.StrToBool( cgiGet( chkavUsuario_deferias_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48Usuario_DeFerias", AV48Usuario_DeFerias);
            /* Read saved values. */
            Confirmpanel_Title = cgiGet( "CONFIRMPANEL_Title");
            Confirmpanel_Confirmationtext = cgiGet( "CONFIRMPANEL_Confirmationtext");
            Confirmpanel_Yesbuttoncaption = cgiGet( "CONFIRMPANEL_Yesbuttoncaption");
            Confirmpanel_Nobuttoncaption = cgiGet( "CONFIRMPANEL_Nobuttoncaption");
            Confirmpanel_Yesbuttonposition = cgiGet( "CONFIRMPANEL_Yesbuttonposition");
            Confirmpanel2_Title = cgiGet( "CONFIRMPANEL2_Title");
            Confirmpanel2_Confirmationtext = cgiGet( "CONFIRMPANEL2_Confirmationtext");
            Confirmpanel2_Yesbuttoncaption = cgiGet( "CONFIRMPANEL2_Yesbuttoncaption");
            Confirmpanel2_Nobuttoncaption = cgiGet( "CONFIRMPANEL2_Nobuttoncaption");
            Confirmpanel2_Cancelbuttoncaption = cgiGet( "CONFIRMPANEL2_Cancelbuttoncaption");
            Confirmpanel2_Yesbuttonposition = cgiGet( "CONFIRMPANEL2_Yesbuttonposition");
            Confirmpanel2_Confirmtype = cgiGet( "CONFIRMPANEL2_Confirmtype");
            Confirmpanel_Result = cgiGet( "CONFIRMPANEL_Result");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E132X2 */
         E132X2 ();
         if (returnInSub) return;
      }

      protected void E132X2( )
      {
         /* Start Routine */
         Form.Meta.addItem("Versao", "1.2 - Data: 26/03/2020 00:01", 0) ;
         Form.Headerrawhtml = Form.Headerrawhtml+StringUtil.Format( "<link rel=\"SHORTCUT ICON\" href=\"%1\" /> ", context.convertURL( (String)(context.GetImagePath( "f4e7b3e0-b872-40d0-97e9-87e3199a4868", "", context.GetTheme( )))), "", "", "", "", "", "", "", "");
         Form.Headerrawhtml = Form.Headerrawhtml+"<link rel=\"stylesheet\" type=\"text/css\" href=\"extsrc/login/css/bootstrap.min.css\">";
         Form.Headerrawhtml = Form.Headerrawhtml+"<link rel=\"stylesheet\" type=\"text/css\" href=\"extsrc/login/css/estilos-login.css\">";
         Form.Headerrawhtml = Form.Headerrawhtml+"<script src=\"extsrc/login/js/jquery.min.js\"></script>";
         Form.Headerrawhtml = Form.Headerrawhtml+"<script src=\"extsrc/login/js/popper.min.js\"></script>";
         Form.Headerrawhtml = Form.Headerrawhtml+"<script src=\"extsrc/login/js/bootstrap.min.js\"></script>";
         AV15ConnectionInfoCollection = new SdtGAM(context).getconnections();
         if ( ( AV15ConnectionInfoCollection.Count > 0 ) && (0==new SdtGAMRepository(context).getid()) )
         {
            AV21isOK = new SdtGAM(context).setconnection(((SdtGAMConnectionInfo)AV15ConnectionInfoCollection.Item(1)).gxTpr_Name, out  AV18Errors);
         }
         imgavButtonfacebook_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavButtonfacebook_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavButtonfacebook_Visible), 5, 0)));
         imgavButtongoogle_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavButtongoogle_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavButtongoogle_Visible), 5, 0)));
         imgavButtontwitter_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavButtontwitter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavButtontwitter_Visible), 5, 0)));
         imgavButtongamremote_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavButtongamremote_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavButtongamremote_Visible), 5, 0)));
         if ( new SdtGAMRepository(context).canauthenticatewith("Facebook") )
         {
            imgavButtonfacebook_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavButtonfacebook_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavButtonfacebook_Visible), 5, 0)));
            AV10ButtonFacebook = context.GetImagePath( "1965aed6-daad-4687-8b71-4797860e630c", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavButtonfacebook_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV10ButtonFacebook)) ? AV54Buttonfacebook_GXI : context.convertURL( context.PathToRelativeUrl( AV10ButtonFacebook))));
            AV54Buttonfacebook_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "1965aed6-daad-4687-8b71-4797860e630c", "", context.GetTheme( )));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavButtonfacebook_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV10ButtonFacebook)) ? AV54Buttonfacebook_GXI : context.convertURL( context.PathToRelativeUrl( AV10ButtonFacebook))));
         }
         if ( new SdtGAMRepository(context).canauthenticatewith("Google") )
         {
            imgavButtongoogle_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavButtongoogle_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavButtongoogle_Visible), 5, 0)));
            AV12ButtonGoogle = context.GetImagePath( "57720a5a-eb93-4911-895a-35207ed2dd03", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavButtongoogle_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV12ButtonGoogle)) ? AV55Buttongoogle_GXI : context.convertURL( context.PathToRelativeUrl( AV12ButtonGoogle))));
            AV55Buttongoogle_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "57720a5a-eb93-4911-895a-35207ed2dd03", "", context.GetTheme( )));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavButtongoogle_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV12ButtonGoogle)) ? AV55Buttongoogle_GXI : context.convertURL( context.PathToRelativeUrl( AV12ButtonGoogle))));
         }
         if ( new SdtGAMRepository(context).canauthenticatewith("Twitter") )
         {
            imgavButtontwitter_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavButtontwitter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavButtontwitter_Visible), 5, 0)));
            AV13ButtonTwitter = context.GetImagePath( "5b513ef3-c434-4c8c-aacb-feefd834b2ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavButtontwitter_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV13ButtonTwitter)) ? AV56Buttontwitter_GXI : context.convertURL( context.PathToRelativeUrl( AV13ButtonTwitter))));
            AV56Buttontwitter_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "5b513ef3-c434-4c8c-aacb-feefd834b2ee", "", context.GetTheme( )));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavButtontwitter_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV13ButtonTwitter)) ? AV56Buttontwitter_GXI : context.convertURL( context.PathToRelativeUrl( AV13ButtonTwitter))));
         }
         if ( new SdtGAMRepository(context).canauthenticatewith("GAMRemote") )
         {
            imgavButtongamremote_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavButtongamremote_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavButtongamremote_Visible), 5, 0)));
            AV11ButtonGAMRemote = context.GetImagePath( "6cdd3e18-cc5b-44e0-bd22-3efaf48a6c40", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavButtongamremote_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV11ButtonGAMRemote)) ? AV57Buttongamremote_GXI : context.convertURL( context.PathToRelativeUrl( AV11ButtonGAMRemote))));
            AV57Buttongamremote_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "6cdd3e18-cc5b-44e0-bd22-3efaf48a6c40", "", context.GetTheme( )));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavButtongamremote_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV11ButtonGAMRemote)) ? AV57Buttongamremote_GXI : context.convertURL( context.PathToRelativeUrl( AV11ButtonGAMRemote))));
         }
         if ( 1 == 2 )
         {
            new listwwpprograms(context ).execute( out  AV40Pgmnames) ;
            context.wjLoc = formatLink("wp_lote.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         tblTblmain_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblmain_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblmain_Visible), 5, 0)));
      }

      protected void E142X2( )
      {
         /* Refresh Routine */
         chkavUsuario_deferias.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavUsuario_deferias_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavUsuario_deferias.Visible), 5, 0)));
         chkavUsuario_inativo.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavUsuario_inativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavUsuario_inativo.Visible), 5, 0)));
         AV34UserPassword = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34UserPassword", AV34UserPassword);
         AV22isRedirect = false;
         AV19ErrorsLogin = new SdtGAMRepository(context).getlasterrors();
         if ( AV19ErrorsLogin.Count > 0 )
         {
            if ( ((SdtGAMError)AV19ErrorsLogin.Item(1)).gxTpr_Code == 161 )
            {
               context.wjLoc = formatLink("gamexampleupdateregisteruser.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV6ApplicationClientId));
               context.wjLocDisableFrm = 1;
               AV22isRedirect = true;
            }
            else
            {
               AV34UserPassword = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34UserPassword", AV34UserPassword);
               AV18Errors = AV19ErrorsLogin;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33UserName)) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV34UserPassword)) )
               {
                  /* Execute user subroutine: 'DISPLAYMESSAGES' */
                  S112 ();
                  if (returnInSub) return;
               }
            }
         }
         if ( ! AV22isRedirect )
         {
            AV31SessionValid = new SdtGAMSession(context).isvalid(out  AV30Session, out  AV18Errors);
            if ( AV31SessionValid && ! AV30Session.gxTpr_Isanonymous )
            {
               AV32URL = new SdtGAMRepository(context).getlasterrorsurl();
               if ( ! (0==AV36wwpContext.gxTpr_Userid) )
               {
                  if ( String.IsNullOrEmpty(StringUtil.RTrim( AV32URL)) )
                  {
                  }
                  else
                  {
                     context.wjLoc = formatLink(AV32URL) ;
                     context.wjLocDisableFrm = 0;
                  }
               }
            }
            else
            {
               AV9AuthenticationTypes = new SdtGAMRepository(context).getenabledauthenticationtypes(AV24Language, out  AV18Errors);
               AV58GXV1 = 1;
               while ( AV58GXV1 <= AV9AuthenticationTypes.Count )
               {
                  AV8AuthenticationType = ((SdtGAMAuthenticationTypeSimple)AV9AuthenticationTypes.Item(AV58GXV1));
                  if ( AV8AuthenticationType.gxTpr_Needusername )
                  {
                  }
                  AV58GXV1 = (int)(AV58GXV1+1);
               }
               AV21isOK = new SdtGAMRepository(context).getrememberlogin(out  AV27LogOnTo, out  AV33UserName, out  AV35UserRememberMe, out  AV18Errors);
               if ( AV35UserRememberMe == 2 )
               {
                  AV28RememberMe = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28RememberMe", AV28RememberMe);
               }
               AV29Repository = new SdtGAMRepository(context).get();
               chkavRememberme.Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavRememberme_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavRememberme.Visible), 5, 0)));
               chkavKeepmeloggedin.Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavKeepmeloggedin_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavKeepmeloggedin.Visible), 5, 0)));
               if ( StringUtil.StrCmp(AV29Repository.gxTpr_Userremembermetype, "Login") == 0 )
               {
                  chkavRememberme.Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavRememberme_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavRememberme.Visible), 5, 0)));
               }
               else if ( StringUtil.StrCmp(AV29Repository.gxTpr_Userremembermetype, "Auth") == 0 )
               {
                  chkavKeepmeloggedin.Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavKeepmeloggedin_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavKeepmeloggedin.Visible), 5, 0)));
               }
               else if ( StringUtil.StrCmp(AV29Repository.gxTpr_Userremembermetype, "Both") == 0 )
               {
                  chkavRememberme.Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavRememberme_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavRememberme.Visible), 5, 0)));
                  chkavKeepmeloggedin.Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavKeepmeloggedin_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavKeepmeloggedin.Visible), 5, 0)));
               }
            }
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E152X2 */
         E152X2 ();
         if (returnInSub) return;
      }

      protected void E152X2( )
      {
         /* Enter Routine */
         if ( AV23KeepMeLoggedIn )
         {
            AV5AdditionalParameter.gxTpr_Rememberusertype = (short)((AV23KeepMeLoggedIn ? 3 : 1));
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AdditionalParameter", AV5AdditionalParameter);
         }
         else
         {
            if ( AV28RememberMe )
            {
               AV5AdditionalParameter.gxTpr_Rememberusertype = (short)((AV28RememberMe ? 2 : 1));
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AdditionalParameter", AV5AdditionalParameter);
            }
            else
            {
               AV5AdditionalParameter.gxTpr_Rememberusertype = 1;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AdditionalParameter", AV5AdditionalParameter);
            }
         }
         AV5AdditionalParameter.gxTpr_Authenticationtypename = AV27LogOnTo;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AdditionalParameter", AV5AdditionalParameter);
         AV25LoginOK = new SdtGAMRepository(context).login(AV33UserName, AV34UserPassword, AV5AdditionalParameter, out  AV18Errors);
         if ( AV25LoginOK )
         {
            AV7ApplicationData = new SdtGAMSession(context).getapplicationdata();
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV7ApplicationData)) )
            {
               AV20GAMExampleSDTApplicationData.FromJSonString(AV7ApplicationData);
            }
            AV38GamUser = new SdtGAMUser(context).get();
            /* Using cursor H002X2 */
            pr_default.execute(0, new Object[] {AV38GamUser.gxTpr_Guid});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A341Usuario_UserGamGuid = H002X2_A341Usuario_UserGamGuid[0];
               A289Usuario_EhContador = H002X2_A289Usuario_EhContador[0];
               A291Usuario_EhContratada = H002X2_A291Usuario_EhContratada[0];
               A292Usuario_EhContratante = H002X2_A292Usuario_EhContratante[0];
               A293Usuario_EhFinanceiro = H002X2_A293Usuario_EhFinanceiro[0];
               A1647Usuario_Email = H002X2_A1647Usuario_Email[0];
               n1647Usuario_Email = H002X2_n1647Usuario_Email[0];
               A1908Usuario_DeFerias = H002X2_A1908Usuario_DeFerias[0];
               n1908Usuario_DeFerias = H002X2_n1908Usuario_DeFerias[0];
               A2016Usuario_UltimaArea = H002X2_A2016Usuario_UltimaArea[0];
               n2016Usuario_UltimaArea = H002X2_n2016Usuario_UltimaArea[0];
               A1Usuario_Codigo = H002X2_A1Usuario_Codigo[0];
               GXt_boolean1 = A290Usuario_EhAuditorFM;
               new prc_usuarioehauditorfm(context ).execute(  A1Usuario_Codigo, out  GXt_boolean1) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
               A290Usuario_EhAuditorFM = GXt_boolean1;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A290Usuario_EhAuditorFM", A290Usuario_EhAuditorFM);
               AV36wwpContext.gxTpr_Userid = (short)(A1Usuario_Codigo);
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36wwpContext", AV36wwpContext);
               AV36wwpContext.gxTpr_Userehauditorfm = A290Usuario_EhAuditorFM;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36wwpContext", AV36wwpContext);
               AV36wwpContext.gxTpr_Userehcontador = A289Usuario_EhContador;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36wwpContext", AV36wwpContext);
               AV36wwpContext.gxTpr_Userehcontratada = A291Usuario_EhContratada;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36wwpContext", AV36wwpContext);
               AV36wwpContext.gxTpr_Userehcontratante = A292Usuario_EhContratante;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36wwpContext", AV36wwpContext);
               AV36wwpContext.gxTpr_Userehfinanceiro = A293Usuario_EhFinanceiro;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36wwpContext", AV36wwpContext);
               AV36wwpContext.gxTpr_Usuario_email = A1647Usuario_Email;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36wwpContext", AV36wwpContext);
               AV48Usuario_DeFerias = A1908Usuario_DeFerias;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48Usuario_DeFerias", AV48Usuario_DeFerias);
               AV43LastUserAreaTrabalho = A2016Usuario_UltimaArea;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43LastUserAreaTrabalho", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43LastUserAreaTrabalho), 6, 0)));
               /* Using cursor H002X3 */
               pr_default.execute(1, new Object[] {A1Usuario_Codigo});
               while ( (pr_default.getStatus(1) != 101) )
               {
                  A69ContratadaUsuario_UsuarioCod = H002X3_A69ContratadaUsuario_UsuarioCod[0];
                  A43Contratada_Ativo = H002X3_A43Contratada_Ativo[0];
                  n43Contratada_Ativo = H002X3_n43Contratada_Ativo[0];
                  A1394ContratadaUsuario_UsuarioAtivo = H002X3_A1394ContratadaUsuario_UsuarioAtivo[0];
                  n1394ContratadaUsuario_UsuarioAtivo = H002X3_n1394ContratadaUsuario_UsuarioAtivo[0];
                  A66ContratadaUsuario_ContratadaCod = H002X3_A66ContratadaUsuario_ContratadaCod[0];
                  A67ContratadaUsuario_ContratadaPessoaCod = H002X3_A67ContratadaUsuario_ContratadaPessoaCod[0];
                  n67ContratadaUsuario_ContratadaPessoaCod = H002X3_n67ContratadaUsuario_ContratadaPessoaCod[0];
                  A68ContratadaUsuario_ContratadaPessoaNom = H002X3_A68ContratadaUsuario_ContratadaPessoaNom[0];
                  n68ContratadaUsuario_ContratadaPessoaNom = H002X3_n68ContratadaUsuario_ContratadaPessoaNom[0];
                  A1394ContratadaUsuario_UsuarioAtivo = H002X3_A1394ContratadaUsuario_UsuarioAtivo[0];
                  n1394ContratadaUsuario_UsuarioAtivo = H002X3_n1394ContratadaUsuario_UsuarioAtivo[0];
                  A43Contratada_Ativo = H002X3_A43Contratada_Ativo[0];
                  n43Contratada_Ativo = H002X3_n43Contratada_Ativo[0];
                  A67ContratadaUsuario_ContratadaPessoaCod = H002X3_A67ContratadaUsuario_ContratadaPessoaCod[0];
                  n67ContratadaUsuario_ContratadaPessoaCod = H002X3_n67ContratadaUsuario_ContratadaPessoaCod[0];
                  A68ContratadaUsuario_ContratadaPessoaNom = H002X3_A68ContratadaUsuario_ContratadaPessoaNom[0];
                  n68ContratadaUsuario_ContratadaPessoaNom = H002X3_n68ContratadaUsuario_ContratadaPessoaNom[0];
                  AV36wwpContext.gxTpr_Contratada_codigo = A66ContratadaUsuario_ContratadaCod;
                  context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36wwpContext", AV36wwpContext);
                  AV36wwpContext.gxTpr_Contratada_pessoacod = A67ContratadaUsuario_ContratadaPessoaCod;
                  context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36wwpContext", AV36wwpContext);
                  AV36wwpContext.gxTpr_Contratada_pessoanom = A68ContratadaUsuario_ContratadaPessoaNom;
                  context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36wwpContext", AV36wwpContext);
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  pr_default.readNext(1);
               }
               pr_default.close(1);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(0);
            }
            pr_default.close(0);
            AV36wwpContext.gxTpr_Userehadministradorgam = new SdtGAMRepository(context).checkpermission("is_gam_administrator");
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36wwpContext", AV36wwpContext);
            GXt_boolean1 = false;
            new prc_usuarioehlicensiado(context ).execute(  AV36wwpContext.gxTpr_Userid, out  GXt_boolean1) ;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36wwpContext", AV36wwpContext);
            AV36wwpContext.gxTpr_Userehlicenciado = GXt_boolean1;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36wwpContext", AV36wwpContext);
            AV36wwpContext.gxTpr_Username = StringUtil.Trim( AV38GamUser.gxTpr_Firstname)+" "+StringUtil.Trim( AV38GamUser.gxTpr_Lastname);
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36wwpContext", AV36wwpContext);
            if ( AV43LastUserAreaTrabalho > 0 )
            {
               AV61GXLvl181 = 0;
               /* Using cursor H002X4 */
               pr_default.execute(2, new Object[] {AV43LastUserAreaTrabalho});
               while ( (pr_default.getStatus(2) != 101) )
               {
                  A335Contratante_PessoaCod = H002X4_A335Contratante_PessoaCod[0];
                  A30Contratante_Ativo = H002X4_A30Contratante_Ativo[0];
                  n30Contratante_Ativo = H002X4_n30Contratante_Ativo[0];
                  A72AreaTrabalho_Ativo = H002X4_A72AreaTrabalho_Ativo[0];
                  A5AreaTrabalho_Codigo = H002X4_A5AreaTrabalho_Codigo[0];
                  A29Contratante_Codigo = H002X4_A29Contratante_Codigo[0];
                  n29Contratante_Codigo = H002X4_n29Contratante_Codigo[0];
                  A10Contratante_NomeFantasia = H002X4_A10Contratante_NomeFantasia[0];
                  A9Contratante_RazaoSocial = H002X4_A9Contratante_RazaoSocial[0];
                  n9Contratante_RazaoSocial = H002X4_n9Contratante_RazaoSocial[0];
                  A12Contratante_CNPJ = H002X4_A12Contratante_CNPJ[0];
                  n12Contratante_CNPJ = H002X4_n12Contratante_CNPJ[0];
                  A1216AreaTrabalho_OrganizacaoCod = H002X4_A1216AreaTrabalho_OrganizacaoCod[0];
                  n1216AreaTrabalho_OrganizacaoCod = H002X4_n1216AreaTrabalho_OrganizacaoCod[0];
                  A6AreaTrabalho_Descricao = H002X4_A6AreaTrabalho_Descricao[0];
                  A830AreaTrabalho_ServicoPadrao = H002X4_A830AreaTrabalho_ServicoPadrao[0];
                  n830AreaTrabalho_ServicoPadrao = H002X4_n830AreaTrabalho_ServicoPadrao[0];
                  A642AreaTrabalho_CalculoPFinal = H002X4_A642AreaTrabalho_CalculoPFinal[0];
                  A335Contratante_PessoaCod = H002X4_A335Contratante_PessoaCod[0];
                  A30Contratante_Ativo = H002X4_A30Contratante_Ativo[0];
                  n30Contratante_Ativo = H002X4_n30Contratante_Ativo[0];
                  A10Contratante_NomeFantasia = H002X4_A10Contratante_NomeFantasia[0];
                  A9Contratante_RazaoSocial = H002X4_A9Contratante_RazaoSocial[0];
                  n9Contratante_RazaoSocial = H002X4_n9Contratante_RazaoSocial[0];
                  A12Contratante_CNPJ = H002X4_A12Contratante_CNPJ[0];
                  n12Contratante_CNPJ = H002X4_n12Contratante_CNPJ[0];
                  AV61GXLvl181 = 1;
                  AV36wwpContext.gxTpr_Contratante_codigo = A29Contratante_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36wwpContext", AV36wwpContext);
                  AV36wwpContext.gxTpr_Contratante_nomefantasia = A10Contratante_NomeFantasia;
                  context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36wwpContext", AV36wwpContext);
                  AV36wwpContext.gxTpr_Contratante_razaosocial = A9Contratante_RazaoSocial;
                  context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36wwpContext", AV36wwpContext);
                  AV36wwpContext.gxTpr_Contratante_cnpj = A12Contratante_CNPJ;
                  context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36wwpContext", AV36wwpContext);
                  AV36wwpContext.gxTpr_Organizacao_codigo = A1216AreaTrabalho_OrganizacaoCod;
                  context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36wwpContext", AV36wwpContext);
                  AV36wwpContext.gxTpr_Areatrabalho_codigo = A5AreaTrabalho_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36wwpContext", AV36wwpContext);
                  AV36wwpContext.gxTpr_Areatrabalho_descricao = A6AreaTrabalho_Descricao;
                  context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36wwpContext", AV36wwpContext);
                  AV36wwpContext.gxTpr_Servicopadrao = (short)(A830AreaTrabalho_ServicoPadrao);
                  context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36wwpContext", AV36wwpContext);
                  AV36wwpContext.gxTpr_Calculopfinal = A642AreaTrabalho_CalculoPFinal;
                  context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36wwpContext", AV36wwpContext);
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(2);
               if ( AV61GXLvl181 == 0 )
               {
                  /* Execute user subroutine: 'USUARIOSEMAREA' */
                  S122 ();
                  if (returnInSub) return;
               }
            }
            else
            {
               /* Execute user subroutine: 'USUARIOSEMAREA' */
               S122 ();
               if (returnInSub) return;
            }
            /* Using cursor H002X5 */
            pr_default.execute(3);
            while ( (pr_default.getStatus(3) != 101) )
            {
               A334ParametrosSistema_AppID = H002X5_A334ParametrosSistema_AppID[0];
               A331ParametrosSistema_NomeSistema = H002X5_A331ParametrosSistema_NomeSistema[0];
               A1021ParametrosSistema_PathCrtf = H002X5_A1021ParametrosSistema_PathCrtf[0];
               n1021ParametrosSistema_PathCrtf = H002X5_n1021ParametrosSistema_PathCrtf[0];
               AV36wwpContext.gxTpr_Parametrossistema_appid = A334ParametrosSistema_AppID;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36wwpContext", AV36wwpContext);
               AV36wwpContext.gxTpr_Parametrossistema_nomesistema = A331ParametrosSistema_NomeSistema;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36wwpContext", AV36wwpContext);
               AV36wwpContext.gxTpr_Pathcertificacao = A1021ParametrosSistema_PathCrtf;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36wwpContext", AV36wwpContext);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(3);
            }
            pr_default.close(3);
            new wwpbaseobjects.setwwpcontext(context ).execute(  AV36wwpContext) ;
            if ( AV36wwpContext.gxTpr_Userehcontratada || AV36wwpContext.gxTpr_Userehcontratante )
            {
               AV63GXLvl211 = 0;
               /* Using cursor H002X6 */
               pr_default.execute(4, new Object[] {AV36wwpContext.gxTpr_Userid});
               while ( (pr_default.getStatus(4) != 101) )
               {
                  A63ContratanteUsuario_ContratanteCod = H002X6_A63ContratanteUsuario_ContratanteCod[0];
                  A30Contratante_Ativo = H002X6_A30Contratante_Ativo[0];
                  n30Contratante_Ativo = H002X6_n30Contratante_Ativo[0];
                  A54Usuario_Ativo = H002X6_A54Usuario_Ativo[0];
                  n54Usuario_Ativo = H002X6_n54Usuario_Ativo[0];
                  A60ContratanteUsuario_UsuarioCod = H002X6_A60ContratanteUsuario_UsuarioCod[0];
                  A30Contratante_Ativo = H002X6_A30Contratante_Ativo[0];
                  n30Contratante_Ativo = H002X6_n30Contratante_Ativo[0];
                  A54Usuario_Ativo = H002X6_A54Usuario_Ativo[0];
                  n54Usuario_Ativo = H002X6_n54Usuario_Ativo[0];
                  AV63GXLvl211 = 1;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  pr_default.readNext(4);
               }
               pr_default.close(4);
               if ( AV63GXLvl211 == 0 )
               {
                  AV64GXLvl217 = 0;
                  /* Using cursor H002X7 */
                  pr_default.execute(5, new Object[] {AV36wwpContext.gxTpr_Userid});
                  while ( (pr_default.getStatus(5) != 101) )
                  {
                     A66ContratadaUsuario_ContratadaCod = H002X7_A66ContratadaUsuario_ContratadaCod[0];
                     A43Contratada_Ativo = H002X7_A43Contratada_Ativo[0];
                     n43Contratada_Ativo = H002X7_n43Contratada_Ativo[0];
                     A1394ContratadaUsuario_UsuarioAtivo = H002X7_A1394ContratadaUsuario_UsuarioAtivo[0];
                     n1394ContratadaUsuario_UsuarioAtivo = H002X7_n1394ContratadaUsuario_UsuarioAtivo[0];
                     A69ContratadaUsuario_UsuarioCod = H002X7_A69ContratadaUsuario_UsuarioCod[0];
                     A43Contratada_Ativo = H002X7_A43Contratada_Ativo[0];
                     n43Contratada_Ativo = H002X7_n43Contratada_Ativo[0];
                     A1394ContratadaUsuario_UsuarioAtivo = H002X7_A1394ContratadaUsuario_UsuarioAtivo[0];
                     n1394ContratadaUsuario_UsuarioAtivo = H002X7_n1394ContratadaUsuario_UsuarioAtivo[0];
                     AV64GXLvl217 = 1;
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                     pr_default.readNext(5);
                  }
                  pr_default.close(5);
                  if ( AV64GXLvl217 == 0 )
                  {
                     AV50Usuario_Inativo = true;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50Usuario_Inativo", AV50Usuario_Inativo);
                  }
               }
            }
            if ( AV50Usuario_Inativo )
            {
               this.executeUsercontrolMethod("", false, "CONFIRMPANEL2Container", "Confirm", "", new Object[] {});
            }
            else if ( AV48Usuario_DeFerias )
            {
               this.executeUsercontrolMethod("", false, "CONFIRMPANELContainer", "Confirm", "", new Object[] {});
            }
            else if ( AV51Usuario_SemArea )
            {
               Confirmpanel2_Confirmationtext = "O seu usu�rio n�o pertence a Contratadas ou �reas ATIVAS";
               context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Confirmpanel2_Internalname, "ConfirmationText", Confirmpanel2_Confirmationtext);
               this.executeUsercontrolMethod("", false, "CONFIRMPANEL2Container", "Confirm", "", new Object[] {});
            }
            else
            {
               /* Execute user subroutine: 'GOURL' */
               S132 ();
               if (returnInSub) return;
            }
         }
         else
         {
            if ( AV18Errors.Count > 0 )
            {
               if ( ( ((SdtGAMError)AV18Errors.Item(1)).gxTpr_Code == 24 ) || ( ((SdtGAMError)AV18Errors.Item(1)).gxTpr_Code == 23 ) )
               {
                  context.wjLoc = formatLink("gamexamplechangepassword.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV27LogOnTo)) + "," + UrlEncode(StringUtil.RTrim(AV33UserName)) + "," + UrlEncode("" +AV35UserRememberMe) + "," + UrlEncode(StringUtil.RTrim(AV6ApplicationClientId));
                  context.wjLocDisableFrm = 1;
               }
               else if ( ((SdtGAMError)AV18Errors.Item(1)).gxTpr_Code == 161 )
               {
                  context.wjLoc = formatLink("gamexampleupdateregisteruser.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV6ApplicationClientId));
                  context.wjLocDisableFrm = 1;
               }
               else
               {
                  GX_FocusControl = edtavUsername_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  context.DoAjaxSetFocus(GX_FocusControl);
                  AV34UserPassword = "";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34UserPassword", AV34UserPassword);
                  /* Execute user subroutine: 'DISPLAYMESSAGES' */
                  S112 ();
                  if (returnInSub) return;
               }
            }
         }
      }

      protected void E112X2( )
      {
         /* Confirmpanel_Close Routine */
         if ( StringUtil.StrCmp(Confirmpanel_Result, "Yes") == 0 )
         {
            AV49Usuario.Load(AV36wwpContext.gxTpr_Userid);
            AV49Usuario.gxTpr_Usuario_deferias = false;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV49Usuario", AV49Usuario);
            AV49Usuario.Save();
            if ( AV49Usuario.Success() )
            {
               context.CommitDataStores( "Login");
            }
            else
            {
               context.RollbackDataStores( "Login");
            }
         }
         /* Execute user subroutine: 'GOURL' */
         S132 ();
         if (returnInSub) return;
      }

      protected void E122X2( )
      {
         /* Confirmpanel2_Close Routine */
         context.wjLoc = formatLink("login.aspx") ;
         context.wjLocDisableFrm = 1;
      }

      protected void S142( )
      {
         /* 'TELAINICIAL' Routine */
         if ( AV36wwpContext.gxTpr_Userehcontratante )
         {
            context.wjLoc = formatLink("wp_monitordmn.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         else
         {
            AV44Usuario_EhGestor = false;
            /* Using cursor H002X8 */
            pr_default.execute(6, new Object[] {AV36wwpContext.gxTpr_Userid});
            while ( (pr_default.getStatus(6) != 101) )
            {
               A1079ContratoGestor_UsuarioCod = H002X8_A1079ContratoGestor_UsuarioCod[0];
               AV44Usuario_EhGestor = true;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(6);
            }
            pr_default.close(6);
            if ( AV44Usuario_EhGestor )
            {
               context.wjLoc = formatLink("wp_gestao.aspx") ;
               context.wjLocDisableFrm = 1;
            }
            else
            {
               context.wjLoc = formatLink("wp_monitordmn.aspx") ;
               context.wjLocDisableFrm = 1;
            }
         }
      }

      protected void S112( )
      {
         /* 'DISPLAYMESSAGES' Routine */
         AV66GXV2 = 1;
         while ( AV66GXV2 <= AV18Errors.Count )
         {
            AV17Error = ((SdtGAMError)AV18Errors.Item(AV66GXV2));
            if ( AV17Error.gxTpr_Code != 13 )
            {
               GX_msglist.addItem(StringUtil.Format( "%1 (GAM%2)", AV17Error.gxTpr_Message, StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Error.gxTpr_Code), 12, 0)), "", "", "", "", "", "", ""));
            }
            AV66GXV2 = (int)(AV66GXV2+1);
         }
      }

      protected void E162X2( )
      {
         /* 'ForgotPassword' Routine */
         context.wjLoc = formatLink("gamexamplerecoverpasswordstep1.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV6ApplicationClientId));
         context.wjLocDisableFrm = 1;
      }

      protected void E172X2( )
      {
         /* Buttonfacebook_Click Routine */
         new SdtGAMRepository(context).loginfacebook() ;
      }

      protected void E182X2( )
      {
         /* Buttongoogle_Click Routine */
         new SdtGAMRepository(context).logingoogle() ;
      }

      protected void E192X2( )
      {
         /* Buttontwitter_Click Routine */
         new SdtGAMRepository(context).logintwitter() ;
      }

      protected void E202X2( )
      {
         /* Buttongamremote_Click Routine */
         new SdtGAMRepository(context).logingamremote() ;
         context.wjLoc = formatLink("wp_sql.aspx") ;
         context.wjLocDisableFrm = 1;
         context.wjLoc = formatLink("wp_consultacontadores.aspx") ;
         context.wjLocDisableFrm = 1;
         context.wjLoc = formatLink("wp_consultadiferencapf.aspx") ;
         context.wjLocDisableFrm = 1;
         context.wjLoc = formatLink("wp_consultasistemas.aspx") ;
         context.wjLocDisableFrm = 1;
         context.wjLoc = formatLink("wp_os.aspx") ;
         context.wjLocDisableFrm = 1;
         context.wjLoc = formatLink("wp_mensuracao.aspx") ;
         context.wjLocDisableFrm = 1;
         context.wjLoc = formatLink("wp_configuracaoplanilha.aspx") ;
         context.wjLocDisableFrm = 1;
         context.wjLoc = formatLink("wp_consultacontagens.aspx") ;
         context.wjLocDisableFrm = 1;
         context.wjLoc = formatLink("variaveiscocomo.aspx") + "?" + UrlEncode(StringUtil.RTrim("")) + "," + UrlEncode("" +0) + "," + UrlEncode(StringUtil.RTrim("")) + "," + UrlEncode(StringUtil.RTrim("")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
         context.wjLoc = formatLink("wp_funcoestrn.aspx") + "?" + UrlEncode("" +0) + "," + UrlEncode(StringUtil.RTrim(""));
         context.wjLocDisableFrm = 1;
         context.wjLoc = formatLink("wp_relatoriocontagens.aspx") ;
         context.wjLocDisableFrm = 1;
         context.wjLoc = formatLink("wp_graficocontagens.aspx") ;
         context.wjLocDisableFrm = 1;
         context.wjLoc = formatLink("wp_importarpffs.aspx") ;
         context.wjLocDisableFrm = 1;
         context.wjLoc = formatLink("about.aspx") ;
         context.wjLocDisableFrm = 1;
         context.wjLoc = formatLink("wp_monitordmn.aspx") ;
         context.wjLocDisableFrm = 1;
         context.wjLoc = formatLink("wp_importabaseline.aspx") ;
         context.wjLocDisableFrm = 1;
         context.wjLoc = formatLink("wp_renumeraosfm.aspx") ;
         context.wjLocDisableFrm = 1;
         context.wjLoc = formatLink("wp_resumodmn.aspx") ;
         context.wjLocDisableFrm = 1;
         context.wjLoc = formatLink("wp_sntdmn.aspx") ;
         context.wjLocDisableFrm = 1;
         context.wjLoc = formatLink("extrawwcontagemresultadogerencial.aspx") ;
         context.wjLocDisableFrm = 1;
         context.wjLoc = formatLink("extrawwcontagemresultadopgtofnc.aspx") ;
         context.wjLocDisableFrm = 1;
         new rel_contagemgerencialarqpdf(context ).execute(  0,  0,  "",  "",  "",  "",  "",  Gx_date,  Gx_date) ;
         context.wjLoc = formatLink("wp_resumoafaturar.aspx") ;
         context.wjLocDisableFrm = 1;
         context.wjLoc = formatLink("wp_wdsldeos.aspx") ;
         context.wjLocDisableFrm = 1;
         context.wjLoc = formatLink("wp_redmine144.aspx") ;
         context.wjLocDisableFrm = 1;
         context.wjLoc = formatLink("wp_homologaros.aspx") + "?" + UrlEncode("" +0) + "," + UrlEncode(StringUtil.RTrim("")) + "," + UrlEncode("" +0) + "," + UrlEncode(StringUtil.BoolToStr(false));
         context.wjLocDisableFrm = 1;
         context.wjLoc = formatLink("wp_rdmcustomfields.aspx") ;
         context.wjLocDisableFrm = 1;
         context.wjLoc = formatLink("wp_renumerass.aspx") ;
         context.wjLocDisableFrm = 1;
         context.wjLoc = formatLink("wp_gestao.aspx") ;
         context.wjLocDisableFrm = 1;
         context.wjLoc = formatLink("wp_novoprojeto.aspx") + "?" + UrlEncode("" +0) + "," + UrlEncode(StringUtil.RTrim(""));
         context.wjLocDisableFrm = 1;
         context.wjLoc = formatLink("wp_ss.aspx") ;
         context.wjLocDisableFrm = 1;
         context.wjLoc = formatLink("wp_helpsistema.aspx") + "?" + UrlEncode(StringUtil.RTrim(""));
         context.wjLocDisableFrm = 1;
         context.wjLoc = formatLink("wp_acompanhamentofinanceiro.aspx") ;
         context.wjLocDisableFrm = 1;
         context.wjLoc = formatLink("wp_updossistema.aspx") ;
         context.wjLocDisableFrm = 1;
         context.wjLoc = formatLink("wp_monitoramento.aspx") ;
         context.wjLocDisableFrm = 1;
         context.wjLoc = formatLink("wp_migrarrequisitos.aspx") ;
         context.wjLocDisableFrm = 1;
         context.wjLoc = formatLink("aajustardatasentrega.aspx") ;
         context.wjLocDisableFrm = 0;
         context.wjLoc = formatLink("webpanel2.aspx") ;
         context.wjLocDisableFrm = 1;
         context.wjLoc = formatLink("aresponsavel_divergencias.aspx") ;
         context.wjLocDisableFrm = 0;
         context.wjLoc = formatLink("acriarcicloscrr.aspx") ;
         context.wjLocDisableFrm = 0;
      }

      protected void S152( )
      {
         /* 'ALERTAS' Routine */
         /* Using cursor H002X9 */
         pr_default.execute(7);
         while ( (pr_default.getStatus(7) != 101) )
         {
            A1882Alerta_ToAreaTrabalho = H002X9_A1882Alerta_ToAreaTrabalho[0];
            n1882Alerta_ToAreaTrabalho = H002X9_n1882Alerta_ToAreaTrabalho[0];
            A1881Alerta_Codigo = H002X9_A1881Alerta_Codigo[0];
            A1886Alerta_Fim = H002X9_A1886Alerta_Fim[0];
            n1886Alerta_Fim = H002X9_n1886Alerta_Fim[0];
            A1885Alerta_Inicio = H002X9_A1885Alerta_Inicio[0];
            A1883Alerta_ToUser = H002X9_A1883Alerta_ToUser[0];
            n1883Alerta_ToUser = H002X9_n1883Alerta_ToUser[0];
            A1884Alerta_ToGroup = H002X9_A1884Alerta_ToGroup[0];
            n1884Alerta_ToGroup = H002X9_n1884Alerta_ToGroup[0];
            if ( A1885Alerta_Inicio <= DateTimeUtil.ServerDate( context, "DEFAULT") )
            {
               if ( A1886Alerta_Fim >= DateTimeUtil.ServerDate( context, "DEFAULT") )
               {
                  if ( A1883Alerta_ToUser == AV36wwpContext.gxTpr_Userid )
                  {
                     AV46Alertas.Add(A1881Alerta_Codigo, 0);
                  }
                  else if ( H002X9_n1883Alerta_ToUser[0] && ( A1882Alerta_ToAreaTrabalho == AV36wwpContext.gxTpr_Areatrabalho_codigo ) )
                  {
                     AV46Alertas.Add(A1881Alerta_Codigo, 0);
                  }
                  else if ( A1884Alerta_ToGroup == 1 )
                  {
                     AV46Alertas.Add(A1881Alerta_Codigo, 0);
                  }
                  else if ( ( A1884Alerta_ToGroup == 3 ) && AV36wwpContext.gxTpr_Userehcontratada )
                  {
                     AV46Alertas.Add(A1881Alerta_Codigo, 0);
                  }
                  else if ( ( A1884Alerta_ToGroup == 2 ) && AV36wwpContext.gxTpr_Userehcontratante )
                  {
                     AV46Alertas.Add(A1881Alerta_Codigo, 0);
                  }
                  else if ( A1884Alerta_ToGroup == 4 )
                  {
                     /* Using cursor H002X10 */
                     pr_default.execute(8, new Object[] {AV36wwpContext.gxTpr_Userid});
                     while ( (pr_default.getStatus(8) != 101) )
                     {
                        A1079ContratoGestor_UsuarioCod = H002X10_A1079ContratoGestor_UsuarioCod[0];
                        AV46Alertas.Add(A1881Alerta_Codigo, 0);
                        /* Exit For each command. Update data (if necessary), close cursors & exit. */
                        if (true) break;
                        pr_default.readNext(8);
                     }
                     pr_default.close(8);
                  }
                  else if ( ( A1884Alerta_ToGroup == 6 ) && AV36wwpContext.gxTpr_Userehcontratada )
                  {
                     pr_default.dynParam(9, new Object[]{ new Object[]{
                                                          A1882Alerta_ToAreaTrabalho ,
                                                          A1446ContratoGestor_ContratadaAreaCod ,
                                                          A40Contratada_PessoaCod ,
                                                          AV36wwpContext.gxTpr_Contratada_pessoacod ,
                                                          AV36wwpContext.gxTpr_Userid ,
                                                          A1079ContratoGestor_UsuarioCod },
                                                          new int[] {
                                                          TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.INT
                                                          }
                     });
                     /* Using cursor H002X11 */
                     pr_default.execute(9, new Object[] {AV36wwpContext.gxTpr_Userid, AV36wwpContext.gxTpr_Contratada_pessoacod});
                     while ( (pr_default.getStatus(9) != 101) )
                     {
                        A1078ContratoGestor_ContratoCod = H002X11_A1078ContratoGestor_ContratoCod[0];
                        A1136ContratoGestor_ContratadaCod = H002X11_A1136ContratoGestor_ContratadaCod[0];
                        n1136ContratoGestor_ContratadaCod = H002X11_n1136ContratoGestor_ContratadaCod[0];
                        A1079ContratoGestor_UsuarioCod = H002X11_A1079ContratoGestor_UsuarioCod[0];
                        A40Contratada_PessoaCod = H002X11_A40Contratada_PessoaCod[0];
                        n40Contratada_PessoaCod = H002X11_n40Contratada_PessoaCod[0];
                        A1446ContratoGestor_ContratadaAreaCod = H002X11_A1446ContratoGestor_ContratadaAreaCod[0];
                        n1446ContratoGestor_ContratadaAreaCod = H002X11_n1446ContratoGestor_ContratadaAreaCod[0];
                        A1136ContratoGestor_ContratadaCod = H002X11_A1136ContratoGestor_ContratadaCod[0];
                        n1136ContratoGestor_ContratadaCod = H002X11_n1136ContratoGestor_ContratadaCod[0];
                        A1446ContratoGestor_ContratadaAreaCod = H002X11_A1446ContratoGestor_ContratadaAreaCod[0];
                        n1446ContratoGestor_ContratadaAreaCod = H002X11_n1446ContratoGestor_ContratadaAreaCod[0];
                        A40Contratada_PessoaCod = H002X11_A40Contratada_PessoaCod[0];
                        n40Contratada_PessoaCod = H002X11_n40Contratada_PessoaCod[0];
                        AV46Alertas.Add(A1881Alerta_Codigo, 0);
                        /* Exit For each command. Update data (if necessary), close cursors & exit. */
                        if (true) break;
                        pr_default.readNext(9);
                     }
                     pr_default.close(9);
                  }
                  else if ( ( A1884Alerta_ToGroup == 5 ) && AV36wwpContext.gxTpr_Userehcontratante )
                  {
                     pr_default.dynParam(10, new Object[]{ new Object[]{
                                                          A1882Alerta_ToAreaTrabalho ,
                                                          A1446ContratoGestor_ContratadaAreaCod ,
                                                          AV36wwpContext.gxTpr_Userid ,
                                                          A1079ContratoGestor_UsuarioCod },
                                                          new int[] {
                                                          TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.INT
                                                          }
                     });
                     /* Using cursor H002X12 */
                     pr_default.execute(10, new Object[] {AV36wwpContext.gxTpr_Userid});
                     while ( (pr_default.getStatus(10) != 101) )
                     {
                        A1078ContratoGestor_ContratoCod = H002X12_A1078ContratoGestor_ContratoCod[0];
                        A1079ContratoGestor_UsuarioCod = H002X12_A1079ContratoGestor_UsuarioCod[0];
                        A1446ContratoGestor_ContratadaAreaCod = H002X12_A1446ContratoGestor_ContratadaAreaCod[0];
                        n1446ContratoGestor_ContratadaAreaCod = H002X12_n1446ContratoGestor_ContratadaAreaCod[0];
                        A1446ContratoGestor_ContratadaAreaCod = H002X12_A1446ContratoGestor_ContratadaAreaCod[0];
                        n1446ContratoGestor_ContratadaAreaCod = H002X12_n1446ContratoGestor_ContratadaAreaCod[0];
                        AV46Alertas.Add(A1881Alerta_Codigo, 0);
                        /* Exit For each command. Update data (if necessary), close cursors & exit. */
                        if (true) break;
                        pr_default.readNext(10);
                     }
                     pr_default.close(10);
                  }
               }
            }
            pr_default.readNext(7);
         }
         pr_default.close(7);
         AV72GXV3 = 1;
         while ( AV72GXV3 <= AV46Alertas.Count )
         {
            AV47Alerta_Codigo = (int)(AV46Alertas.GetNumeric(AV72GXV3));
            context.PopUp(formatLink("wp_alerta.aspx") + "?" + UrlEncode("" +AV47Alerta_Codigo) + "," + UrlEncode("" +AV36wwpContext.gxTpr_Userid), new Object[] {"AV47Alerta_Codigo",});
            AV72GXV3 = (int)(AV72GXV3+1);
         }
      }

      protected void S132( )
      {
         /* 'GOURL' Routine */
         AV32URL = new SdtGAMRepository(context).getlasterrorsurl();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV32URL)) )
         {
            /* Execute user subroutine: 'ALERTAS' */
            S152 ();
            if (returnInSub) return;
            if ( new prc_usuarioehlicensiado(context).executeUdp(  AV36wwpContext.gxTpr_Userid) )
            {
               context.wjLoc = formatLink("wwareatrabalho.aspx") ;
               context.wjLocDisableFrm = 1;
            }
            else
            {
               /* Execute user subroutine: 'TELAINICIAL' */
               S142 ();
               if (returnInSub) return;
            }
         }
         else
         {
            if ( ! (0==AV36wwpContext.gxTpr_Userid) )
            {
               context.wjLoc = formatLink(AV32URL) ;
               context.wjLocDisableFrm = 0;
            }
         }
      }

      protected void S122( )
      {
         /* 'USUARIOSEMAREA' Routine */
         if ( ! new SdtGAMRepository(context).checkpermission("is_gam_administrator") )
         {
            AV73GXLvl438 = 0;
            /* Using cursor H002X13 */
            pr_default.execute(11, new Object[] {AV36wwpContext.gxTpr_Userid});
            while ( (pr_default.getStatus(11) != 101) )
            {
               A66ContratadaUsuario_ContratadaCod = H002X13_A66ContratadaUsuario_ContratadaCod[0];
               A43Contratada_Ativo = H002X13_A43Contratada_Ativo[0];
               n43Contratada_Ativo = H002X13_n43Contratada_Ativo[0];
               A1394ContratadaUsuario_UsuarioAtivo = H002X13_A1394ContratadaUsuario_UsuarioAtivo[0];
               n1394ContratadaUsuario_UsuarioAtivo = H002X13_n1394ContratadaUsuario_UsuarioAtivo[0];
               A69ContratadaUsuario_UsuarioCod = H002X13_A69ContratadaUsuario_UsuarioCod[0];
               A43Contratada_Ativo = H002X13_A43Contratada_Ativo[0];
               n43Contratada_Ativo = H002X13_n43Contratada_Ativo[0];
               A1394ContratadaUsuario_UsuarioAtivo = H002X13_A1394ContratadaUsuario_UsuarioAtivo[0];
               n1394ContratadaUsuario_UsuarioAtivo = H002X13_n1394ContratadaUsuario_UsuarioAtivo[0];
               AV73GXLvl438 = 1;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(11);
            }
            pr_default.close(11);
            if ( AV73GXLvl438 == 0 )
            {
               AV74GXLvl444 = 0;
               /* Using cursor H002X14 */
               pr_default.execute(12, new Object[] {AV36wwpContext.gxTpr_Userid});
               while ( (pr_default.getStatus(12) != 101) )
               {
                  A63ContratanteUsuario_ContratanteCod = H002X14_A63ContratanteUsuario_ContratanteCod[0];
                  A54Usuario_Ativo = H002X14_A54Usuario_Ativo[0];
                  n54Usuario_Ativo = H002X14_n54Usuario_Ativo[0];
                  A60ContratanteUsuario_UsuarioCod = H002X14_A60ContratanteUsuario_UsuarioCod[0];
                  A54Usuario_Ativo = H002X14_A54Usuario_Ativo[0];
                  n54Usuario_Ativo = H002X14_n54Usuario_Ativo[0];
                  AV74GXLvl444 = 1;
                  AV75GXLvl447 = 0;
                  /* Using cursor H002X15 */
                  pr_default.execute(13, new Object[] {A63ContratanteUsuario_ContratanteCod});
                  while ( (pr_default.getStatus(13) != 101) )
                  {
                     A29Contratante_Codigo = H002X15_A29Contratante_Codigo[0];
                     n29Contratante_Codigo = H002X15_n29Contratante_Codigo[0];
                     AV75GXLvl447 = 1;
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                     pr_default.readNext(13);
                  }
                  pr_default.close(13);
                  if ( AV75GXLvl447 == 0 )
                  {
                     AV51Usuario_SemArea = true;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51Usuario_SemArea", AV51Usuario_SemArea);
                  }
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  pr_default.readNext(12);
               }
               pr_default.close(12);
               if ( AV74GXLvl444 == 0 )
               {
                  AV51Usuario_SemArea = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51Usuario_SemArea", AV51Usuario_SemArea);
               }
            }
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E212X2( )
      {
         /* Load Routine */
      }

      protected void wb_table2_67_2X2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + "BACKGROUND-IMAGE: url(" + context.convertURL( "f8ced90a-a0e4-4bc3-9a3d-0e3a94f040d5") + ");";
            sStyleString = sStyleString + "background-color: " + "Transparent;";
            sStyleString = sStyleString + " height: " + StringUtil.LTrim( StringUtil.Str( (decimal)(170), 10, 0)) + "px" + ";";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(350), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTable3_Internalname, tblTable3_Internalname, "", "Table", 0, "center", "", 1, 20, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_70_2X2( true) ;
         }
         else
         {
            wb_table3_70_2X2( false) ;
         }
         return  ;
      }

      protected void wb_table3_70_2X2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table4_76_2X2( true) ;
         }
         else
         {
            wb_table4_76_2X2( false) ;
         }
         return  ;
      }

      protected void wb_table4_76_2X2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table5_80_2X2( true) ;
         }
         else
         {
            wb_table5_80_2X2( false) ;
         }
         return  ;
      }

      protected void wb_table5_80_2X2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_67_2X2e( true) ;
         }
         else
         {
            wb_table2_67_2X2e( false) ;
         }
      }

      protected void wb_table5_80_2X2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + "background-color: " + "Transparent;";
            GxWebStd.gx_table_start( context, tblTable6_Internalname, tblTable6_Internalname, "", "Table", 0, "center", "", 1, 20, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'',0)\"";
            ClassString = "";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavKeepmeloggedin_Internalname, StringUtil.BoolToStr( AV23KeepMeLoggedIn), "", "", chkavKeepmeloggedin.Visible, 1, "true", "Mantenha-me logado", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(83, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,83);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbrememberme2_Internalname, "Esqueceu sua senha?", "", "", lblTbrememberme2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'FORGOTPASSWORD\\'."+"'", "", "", 5, "", 1, 1, 2, "HLP_Login.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_80_2X2e( true) ;
         }
         else
         {
            wb_table5_80_2X2e( false) ;
         }
      }

      protected void wb_table4_76_2X2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + "background-color: " + "Transparent;";
            GxWebStd.gx_table_start( context, tblTable4_Internalname, tblTable4_Internalname, "", "Table", 0, "center", "", 1, 10, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton1_Internalname, "", "Entrar", bttButton1_Jsonclick, 5, "Entrar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Login.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_76_2X2e( true) ;
         }
         else
         {
            wb_table4_76_2X2e( false) ;
         }
      }

      protected void wb_table3_70_2X2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable5_Internalname, tblTable5_Internalname, "", "Table", 0, "center", "", 1, 20, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsername_Internalname, AV33UserName, StringUtil.RTrim( context.localUtil.Format( AV33UserName, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,73);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "Login...", edtavUsername_Jsonclick, 0, "form-control", "", "", "", 1, 1, 0, "text", "", 0, "px", 1, "row", 100, 0, 0, 0, 1, 0, 0, true, "GAMUserIdentification", "left", true, "HLP_Login.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUserpassword_Internalname, StringUtil.RTrim( AV34UserPassword), StringUtil.RTrim( context.localUtil.Format( AV34UserPassword, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,75);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "Senha...", edtavUserpassword_Jsonclick, 0, "form-control", "", "", "", 1, 1, 0, "text", "", 0, "px", 1, "row", 50, -1, 0, 0, 1, 0, 0, true, "GAMPassword", "left", true, "HLP_Login.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_70_2X2e( true) ;
         }
         else
         {
            wb_table3_70_2X2e( false) ;
         }
      }

      protected void wb_table1_2_2X2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblmain_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTblmain_Internalname, tblTblmain_Internalname, "", "Table", 0, "center", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='Table'>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            wb_table6_6_2X2( true) ;
         }
         else
         {
            wb_table6_6_2X2( false) ;
         }
         return  ;
      }

      protected void wb_table6_6_2X2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_2X2e( true) ;
         }
         else
         {
            wb_table1_2_2X2e( false) ;
         }
      }

      protected void wb_table6_6_2X2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + "BACKGROUND-IMAGE: url(" + context.convertURL( "f8ced90a-a0e4-4bc3-9a3d-0e3a94f040d5") + ");";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(325), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTbllogin_Internalname, tblTbllogin_Internalname, "", "TableLogin", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='HeaderLogin'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbtitle_Internalname, "Meetrika - V.0.030", "", "", lblTbtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "color:#FFFFFF;", "Title", 0, "", 1, 1, 0, "HLP_Login.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td data-align=\"center\" background=\""+context.convertURL( context.GetImagePath( "f8ced90a-a0e4-4bc3-9a3d-0e3a94f040d5", "", context.GetTheme( )))+"\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;height:262px")+"\" class='Table'>") ;
            wb_table7_12_2X2( true) ;
         }
         else
         {
            wb_table7_12_2X2( false) ;
         }
         return  ;
      }

      protected void wb_table7_12_2X2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_6_2X2e( true) ;
         }
         else
         {
            wb_table6_6_2X2e( false) ;
         }
      }

      protected void wb_table7_12_2X2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(85), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblTblfields_Internalname, tblTblfields_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:9px")+"\" class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;vertical-align:top")+"\" class='Table'>") ;
            wb_table8_17_2X2( true) ;
         }
         else
         {
            wb_table8_17_2X2( false) ;
         }
         return  ;
      }

      protected void wb_table8_17_2X2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:bottom;height:25px")+"\" class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbusername_Internalname, "Email ou Login", "", "", lblTbusername_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_Login.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:bottom;height:25px")+"\" class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbpassword_Internalname, "Senha", "", "", lblTbpassword_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_Login.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:25px")+"\" class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;height:25px")+"\" class='Table'>") ;
            wb_table9_46_2X2( true) ;
         }
         else
         {
            wb_table9_46_2X2( false) ;
         }
         return  ;
      }

      protected void wb_table9_46_2X2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "background-color:#008000;";
            GxWebStd.gx_button_ctrl( context, bttLogin_Internalname, "", "Login", bttLogin_Jsonclick, 5, "Login", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Login.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;height:20px")+"\" class='Table'>") ;
            wb_table10_57_2X2( true) ;
         }
         else
         {
            wb_table10_57_2X2( false) ;
         }
         return  ;
      }

      protected void wb_table10_57_2X2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_12_2X2e( true) ;
         }
         else
         {
            wb_table7_12_2X2e( false) ;
         }
      }

      protected void wb_table10_57_2X2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "left", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_57_2X2e( true) ;
         }
         else
         {
            wb_table10_57_2X2e( false) ;
         }
      }

      protected void wb_table9_46_2X2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblTblrem_Internalname, tblTblrem_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:9px")+"\" class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'',0)\"";
            ClassString = "AttSemBorda";
            StyleString = "font-family:'Verdana'; font-size:7.0pt; font-weight:normal; font-style:normal;";
            GxWebStd.gx_checkbox_ctrl( context, chkavRememberme_Internalname, StringUtil.BoolToStr( AV28RememberMe), "", "", chkavRememberme.Visible, 1, "true", "Lembrar de Mim", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(51, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,51);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_46_2X2e( true) ;
         }
         else
         {
            wb_table9_46_2X2e( false) ;
         }
      }

      protected void wb_table8_17_2X2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTblextauth_Internalname, tblTblextauth_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:37px")+"\" class='Table'>") ;
            /* Active Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavButtonfacebook_Internalname, AV10ButtonFacebook);
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            AV10ButtonFacebook_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV10ButtonFacebook))&&String.IsNullOrEmpty(StringUtil.RTrim( AV54Buttonfacebook_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV10ButtonFacebook)));
            GxWebStd.gx_bitmap( context, imgavButtonfacebook_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV10ButtonFacebook)) ? AV54Buttonfacebook_GXI : context.PathToRelativeUrl( AV10ButtonFacebook)), "", "", "", context.GetTheme( ), imgavButtonfacebook_Visible, 1, "", "Sign in with Facebook", 0, -1, 0, "", 0, "", 0, 0, 5, imgavButtonfacebook_Jsonclick, "'"+""+"'"+",false,"+"'"+"EVBUTTONFACEBOOK.CLICK."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, AV10ButtonFacebook_IsBlob, false, "HLP_Login.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:9px")+"\" class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:65px")+"\" class='Table'>") ;
            /* Active Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavButtongoogle_Internalname, AV12ButtonGoogle);
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            AV12ButtonGoogle_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV12ButtonGoogle))&&String.IsNullOrEmpty(StringUtil.RTrim( AV55Buttongoogle_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV12ButtonGoogle)));
            GxWebStd.gx_bitmap( context, imgavButtongoogle_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV12ButtonGoogle)) ? AV55Buttongoogle_GXI : context.PathToRelativeUrl( AV12ButtonGoogle)), "", "", "", context.GetTheme( ), imgavButtongoogle_Visible, 1, "", "Sign in with Google", 0, -1, 0, "", 0, "", 0, 0, 5, imgavButtongoogle_Jsonclick, "'"+""+"'"+",false,"+"'"+"EVBUTTONGOOGLE.CLICK."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, AV12ButtonGoogle_IsBlob, false, "HLP_Login.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:9px")+"\" class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Active Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavButtontwitter_Internalname, AV13ButtonTwitter);
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            AV13ButtonTwitter_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV13ButtonTwitter))&&String.IsNullOrEmpty(StringUtil.RTrim( AV56Buttontwitter_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV13ButtonTwitter)));
            GxWebStd.gx_bitmap( context, imgavButtontwitter_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV13ButtonTwitter)) ? AV56Buttontwitter_GXI : context.PathToRelativeUrl( AV13ButtonTwitter)), "", "", "", context.GetTheme( ), imgavButtontwitter_Visible, 1, "", "Sign in with Twitter", 0, -1, 0, "", 0, "", 0, 0, 5, imgavButtontwitter_Jsonclick, "'"+""+"'"+",false,"+"'"+"EVBUTTONTWITTER.CLICK."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, AV13ButtonTwitter_IsBlob, false, "HLP_Login.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:9px")+"\" class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Active Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavButtongamremote_Internalname, AV11ButtonGAMRemote);
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            AV11ButtonGAMRemote_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV11ButtonGAMRemote))&&String.IsNullOrEmpty(StringUtil.RTrim( AV57Buttongamremote_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV11ButtonGAMRemote)));
            GxWebStd.gx_bitmap( context, imgavButtongamremote_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV11ButtonGAMRemote)) ? AV57Buttongamremote_GXI : context.PathToRelativeUrl( AV11ButtonGAMRemote)), "", "", "", context.GetTheme( ), imgavButtongamremote_Visible, 1, "", "", 0, -1, 0, "", 0, "", 0, 0, 5, imgavButtongamremote_Jsonclick, "'"+""+"'"+",false,"+"'"+"EVBUTTONGAMREMOTE.CLICK."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, AV11ButtonGAMRemote_IsBlob, false, "HLP_Login.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_17_2X2e( true) ;
         }
         else
         {
            wb_table8_17_2X2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA2X2( ) ;
         WS2X2( ) ;
         WE2X2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202032621583227");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("login.js", "?202032621583229");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTbtitle_Internalname = "TBTITLE";
         imgavButtonfacebook_Internalname = "vBUTTONFACEBOOK";
         imgavButtongoogle_Internalname = "vBUTTONGOOGLE";
         imgavButtontwitter_Internalname = "vBUTTONTWITTER";
         imgavButtongamremote_Internalname = "vBUTTONGAMREMOTE";
         tblTblextauth_Internalname = "TBLEXTAUTH";
         lblTbusername_Internalname = "TBUSERNAME";
         lblTbpassword_Internalname = "TBPASSWORD";
         chkavRememberme_Internalname = "vREMEMBERME";
         tblTblrem_Internalname = "TBLREM";
         bttLogin_Internalname = "LOGIN";
         tblTable1_Internalname = "TABLE1";
         tblTblfields_Internalname = "TBLFIELDS";
         tblTbllogin_Internalname = "TBLLOGIN";
         tblTblmain_Internalname = "TBLMAIN";
         divLogo1_Internalname = "LOGO1";
         divFormgroup_Internalname = "FORMGROUP";
         edtavUsername_Internalname = "vUSERNAME";
         edtavUserpassword_Internalname = "vUSERPASSWORD";
         tblTable5_Internalname = "TABLE5";
         bttButton1_Internalname = "BUTTON1";
         tblTable4_Internalname = "TABLE4";
         chkavKeepmeloggedin_Internalname = "vKEEPMELOGGEDIN";
         lblTbrememberme2_Internalname = "TBREMEMBERME2";
         tblTable6_Internalname = "TABLE6";
         tblTable3_Internalname = "TABLE3";
         divCheckbox_Internalname = "CHECKBOX";
         divPanellogin_Internalname = "PANELLOGIN";
         divCollg12_Internalname = "COLLG12";
         divContainer_Internalname = "CONTAINER";
         Confirmpanel_Internalname = "CONFIRMPANEL";
         Confirmpanel2_Internalname = "CONFIRMPANEL2";
         chkavUsuario_inativo_Internalname = "vUSUARIO_INATIVO";
         chkavUsuario_deferias_Internalname = "vUSUARIO_DEFERIAS";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         init_default_properties( ) ;
         imgavButtongamremote_Jsonclick = "";
         imgavButtontwitter_Jsonclick = "";
         imgavButtongoogle_Jsonclick = "";
         imgavButtonfacebook_Jsonclick = "";
         edtavUserpassword_Jsonclick = "";
         edtavUsername_Jsonclick = "";
         chkavKeepmeloggedin.BackColor = (int)(0xFFFFFF);
         chkavKeepmeloggedin.Visible = 1;
         chkavRememberme.Visible = 1;
         tblTblmain_Visible = 1;
         imgavButtongamremote_Visible = 1;
         imgavButtontwitter_Visible = 1;
         imgavButtongoogle_Visible = 1;
         imgavButtonfacebook_Visible = 1;
         chkavUsuario_deferias.Caption = "";
         chkavUsuario_inativo.Caption = "";
         chkavKeepmeloggedin.Caption = "";
         chkavRememberme.Caption = "";
         chkavUsuario_deferias.Visible = 1;
         chkavUsuario_inativo.Visible = 1;
         Confirmpanel2_Confirmtype = "0";
         Confirmpanel2_Yesbuttonposition = "left";
         Confirmpanel2_Cancelbuttoncaption = "Fechar";
         Confirmpanel2_Nobuttoncaption = "N�o";
         Confirmpanel2_Yesbuttoncaption = "Entendi";
         Confirmpanel2_Confirmationtext = "O seu usu�rio encontra-se atualmente INATIVO!";
         Confirmpanel2_Title = "Aten��o";
         Confirmpanel_Yesbuttonposition = "left";
         Confirmpanel_Nobuttoncaption = "Ainda n�o";
         Confirmpanel_Yesbuttoncaption = "Sim";
         Confirmpanel_Confirmationtext = "Confirma o seu retorno �s atividades?";
         Confirmpanel_Title = "Afastado";
         Form.Headerrawhtml = "";
         context.GX_msglist.DisplayMode = 1;
      }

      protected override bool IsSpaSupported( )
      {
         return false ;
      }

      public override void InitializeDynEvents( )
      {
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         Gx_date = DateTime.MinValue;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         Confirmpanel_Result = "";
         GXKey = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV48Usuario_DeFerias = false;
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV10ButtonFacebook = "";
         AV12ButtonGoogle = "";
         AV13ButtonTwitter = "";
         AV11ButtonGAMRemote = "";
         AV33UserName = "";
         AV34UserPassword = "";
         AV15ConnectionInfoCollection = new GxExternalCollection( context, "SdtGAMConnectionInfo", "GeneXus.Programs");
         AV18Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         AV54Buttonfacebook_GXI = "";
         AV55Buttongoogle_GXI = "";
         AV56Buttontwitter_GXI = "";
         AV57Buttongamremote_GXI = "";
         AV40Pgmnames = new GxObjectCollection( context, "ProgramNames.ProgramName", "GxEv3Up14_Meetrika", "wwpbaseobjects.SdtProgramNames_ProgramName", "GeneXus.Programs");
         AV19ErrorsLogin = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         AV6ApplicationClientId = "";
         AV30Session = new SdtGAMSession(context);
         AV32URL = "";
         AV36wwpContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9AuthenticationTypes = new GxExternalCollection( context, "SdtGAMAuthenticationTypeSimple", "GeneXus.Programs");
         AV24Language = "";
         AV8AuthenticationType = new SdtGAMAuthenticationTypeSimple(context);
         AV27LogOnTo = "";
         AV29Repository = new SdtGAMRepository(context);
         AV5AdditionalParameter = new SdtGAMLoginAdditionalParameters(context);
         AV7ApplicationData = "";
         AV20GAMExampleSDTApplicationData = new SdtGAMExampleSDTApplicationData(context);
         AV38GamUser = new SdtGAMUser(context);
         scmdbuf = "";
         H002X2_A341Usuario_UserGamGuid = new String[] {""} ;
         H002X2_A289Usuario_EhContador = new bool[] {false} ;
         H002X2_A291Usuario_EhContratada = new bool[] {false} ;
         H002X2_A292Usuario_EhContratante = new bool[] {false} ;
         H002X2_A293Usuario_EhFinanceiro = new bool[] {false} ;
         H002X2_A1647Usuario_Email = new String[] {""} ;
         H002X2_n1647Usuario_Email = new bool[] {false} ;
         H002X2_A1908Usuario_DeFerias = new bool[] {false} ;
         H002X2_n1908Usuario_DeFerias = new bool[] {false} ;
         H002X2_A2016Usuario_UltimaArea = new int[1] ;
         H002X2_n2016Usuario_UltimaArea = new bool[] {false} ;
         H002X2_A1Usuario_Codigo = new int[1] ;
         A341Usuario_UserGamGuid = "";
         A1647Usuario_Email = "";
         H002X3_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H002X3_A43Contratada_Ativo = new bool[] {false} ;
         H002X3_n43Contratada_Ativo = new bool[] {false} ;
         H002X3_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         H002X3_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         H002X3_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H002X3_A67ContratadaUsuario_ContratadaPessoaCod = new int[1] ;
         H002X3_n67ContratadaUsuario_ContratadaPessoaCod = new bool[] {false} ;
         H002X3_A68ContratadaUsuario_ContratadaPessoaNom = new String[] {""} ;
         H002X3_n68ContratadaUsuario_ContratadaPessoaNom = new bool[] {false} ;
         A68ContratadaUsuario_ContratadaPessoaNom = "";
         H002X4_A335Contratante_PessoaCod = new int[1] ;
         H002X4_A30Contratante_Ativo = new bool[] {false} ;
         H002X4_n30Contratante_Ativo = new bool[] {false} ;
         H002X4_A72AreaTrabalho_Ativo = new bool[] {false} ;
         H002X4_A5AreaTrabalho_Codigo = new int[1] ;
         H002X4_A29Contratante_Codigo = new int[1] ;
         H002X4_n29Contratante_Codigo = new bool[] {false} ;
         H002X4_A10Contratante_NomeFantasia = new String[] {""} ;
         H002X4_A9Contratante_RazaoSocial = new String[] {""} ;
         H002X4_n9Contratante_RazaoSocial = new bool[] {false} ;
         H002X4_A12Contratante_CNPJ = new String[] {""} ;
         H002X4_n12Contratante_CNPJ = new bool[] {false} ;
         H002X4_A1216AreaTrabalho_OrganizacaoCod = new int[1] ;
         H002X4_n1216AreaTrabalho_OrganizacaoCod = new bool[] {false} ;
         H002X4_A6AreaTrabalho_Descricao = new String[] {""} ;
         H002X4_A830AreaTrabalho_ServicoPadrao = new int[1] ;
         H002X4_n830AreaTrabalho_ServicoPadrao = new bool[] {false} ;
         H002X4_A642AreaTrabalho_CalculoPFinal = new String[] {""} ;
         A10Contratante_NomeFantasia = "";
         A9Contratante_RazaoSocial = "";
         A12Contratante_CNPJ = "";
         A6AreaTrabalho_Descricao = "";
         A642AreaTrabalho_CalculoPFinal = "";
         H002X5_A330ParametrosSistema_Codigo = new int[1] ;
         H002X5_A334ParametrosSistema_AppID = new long[1] ;
         H002X5_A331ParametrosSistema_NomeSistema = new String[] {""} ;
         H002X5_A1021ParametrosSistema_PathCrtf = new String[] {""} ;
         H002X5_n1021ParametrosSistema_PathCrtf = new bool[] {false} ;
         A331ParametrosSistema_NomeSistema = "";
         A1021ParametrosSistema_PathCrtf = "";
         H002X6_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         H002X6_A30Contratante_Ativo = new bool[] {false} ;
         H002X6_n30Contratante_Ativo = new bool[] {false} ;
         H002X6_A54Usuario_Ativo = new bool[] {false} ;
         H002X6_n54Usuario_Ativo = new bool[] {false} ;
         H002X6_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         H002X7_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H002X7_A43Contratada_Ativo = new bool[] {false} ;
         H002X7_n43Contratada_Ativo = new bool[] {false} ;
         H002X7_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         H002X7_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         H002X7_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         AV49Usuario = new SdtUsuario(context);
         AV44Usuario_EhGestor = false;
         H002X8_A1078ContratoGestor_ContratoCod = new int[1] ;
         H002X8_A1079ContratoGestor_UsuarioCod = new int[1] ;
         AV17Error = new SdtGAMError(context);
         H002X9_A1882Alerta_ToAreaTrabalho = new int[1] ;
         H002X9_n1882Alerta_ToAreaTrabalho = new bool[] {false} ;
         H002X9_A1881Alerta_Codigo = new int[1] ;
         H002X9_A1886Alerta_Fim = new DateTime[] {DateTime.MinValue} ;
         H002X9_n1886Alerta_Fim = new bool[] {false} ;
         H002X9_A1885Alerta_Inicio = new DateTime[] {DateTime.MinValue} ;
         H002X9_A1883Alerta_ToUser = new int[1] ;
         H002X9_n1883Alerta_ToUser = new bool[] {false} ;
         H002X9_A1884Alerta_ToGroup = new short[1] ;
         H002X9_n1884Alerta_ToGroup = new bool[] {false} ;
         A1886Alerta_Fim = DateTime.MinValue;
         A1885Alerta_Inicio = DateTime.MinValue;
         AV46Alertas = new GxSimpleCollection();
         H002X10_A1078ContratoGestor_ContratoCod = new int[1] ;
         H002X10_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H002X11_A1078ContratoGestor_ContratoCod = new int[1] ;
         H002X11_A1136ContratoGestor_ContratadaCod = new int[1] ;
         H002X11_n1136ContratoGestor_ContratadaCod = new bool[] {false} ;
         H002X11_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H002X11_A40Contratada_PessoaCod = new int[1] ;
         H002X11_n40Contratada_PessoaCod = new bool[] {false} ;
         H002X11_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         H002X11_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         H002X12_A1078ContratoGestor_ContratoCod = new int[1] ;
         H002X12_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H002X12_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         H002X12_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         H002X13_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H002X13_A43Contratada_Ativo = new bool[] {false} ;
         H002X13_n43Contratada_Ativo = new bool[] {false} ;
         H002X13_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         H002X13_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         H002X13_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H002X14_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         H002X14_A54Usuario_Ativo = new bool[] {false} ;
         H002X14_n54Usuario_Ativo = new bool[] {false} ;
         H002X14_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         H002X15_A5AreaTrabalho_Codigo = new int[1] ;
         H002X15_A29Contratante_Codigo = new int[1] ;
         H002X15_n29Contratante_Codigo = new bool[] {false} ;
         sStyleString = "";
         lblTbrememberme2_Jsonclick = "";
         bttButton1_Jsonclick = "";
         lblTbtitle_Jsonclick = "";
         lblTbusername_Jsonclick = "";
         lblTbpassword_Jsonclick = "";
         bttLogin_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.login__default(),
            new Object[][] {
                new Object[] {
               H002X2_A341Usuario_UserGamGuid, H002X2_A289Usuario_EhContador, H002X2_A291Usuario_EhContratada, H002X2_A292Usuario_EhContratante, H002X2_A293Usuario_EhFinanceiro, H002X2_A1647Usuario_Email, H002X2_n1647Usuario_Email, H002X2_A1908Usuario_DeFerias, H002X2_n1908Usuario_DeFerias, H002X2_A2016Usuario_UltimaArea,
               H002X2_n2016Usuario_UltimaArea, H002X2_A1Usuario_Codigo
               }
               , new Object[] {
               H002X3_A69ContratadaUsuario_UsuarioCod, H002X3_A43Contratada_Ativo, H002X3_n43Contratada_Ativo, H002X3_A1394ContratadaUsuario_UsuarioAtivo, H002X3_n1394ContratadaUsuario_UsuarioAtivo, H002X3_A66ContratadaUsuario_ContratadaCod, H002X3_A67ContratadaUsuario_ContratadaPessoaCod, H002X3_n67ContratadaUsuario_ContratadaPessoaCod, H002X3_A68ContratadaUsuario_ContratadaPessoaNom, H002X3_n68ContratadaUsuario_ContratadaPessoaNom
               }
               , new Object[] {
               H002X4_A335Contratante_PessoaCod, H002X4_A30Contratante_Ativo, H002X4_A72AreaTrabalho_Ativo, H002X4_A5AreaTrabalho_Codigo, H002X4_A29Contratante_Codigo, H002X4_n29Contratante_Codigo, H002X4_A10Contratante_NomeFantasia, H002X4_A9Contratante_RazaoSocial, H002X4_n9Contratante_RazaoSocial, H002X4_A12Contratante_CNPJ,
               H002X4_n12Contratante_CNPJ, H002X4_A1216AreaTrabalho_OrganizacaoCod, H002X4_n1216AreaTrabalho_OrganizacaoCod, H002X4_A6AreaTrabalho_Descricao, H002X4_A830AreaTrabalho_ServicoPadrao, H002X4_n830AreaTrabalho_ServicoPadrao, H002X4_A642AreaTrabalho_CalculoPFinal
               }
               , new Object[] {
               H002X5_A330ParametrosSistema_Codigo, H002X5_A334ParametrosSistema_AppID, H002X5_A331ParametrosSistema_NomeSistema, H002X5_A1021ParametrosSistema_PathCrtf, H002X5_n1021ParametrosSistema_PathCrtf
               }
               , new Object[] {
               H002X6_A63ContratanteUsuario_ContratanteCod, H002X6_A30Contratante_Ativo, H002X6_n30Contratante_Ativo, H002X6_A54Usuario_Ativo, H002X6_n54Usuario_Ativo, H002X6_A60ContratanteUsuario_UsuarioCod
               }
               , new Object[] {
               H002X7_A66ContratadaUsuario_ContratadaCod, H002X7_A43Contratada_Ativo, H002X7_n43Contratada_Ativo, H002X7_A1394ContratadaUsuario_UsuarioAtivo, H002X7_n1394ContratadaUsuario_UsuarioAtivo, H002X7_A69ContratadaUsuario_UsuarioCod
               }
               , new Object[] {
               H002X8_A1078ContratoGestor_ContratoCod, H002X8_A1079ContratoGestor_UsuarioCod
               }
               , new Object[] {
               H002X9_A1882Alerta_ToAreaTrabalho, H002X9_n1882Alerta_ToAreaTrabalho, H002X9_A1881Alerta_Codigo, H002X9_A1886Alerta_Fim, H002X9_n1886Alerta_Fim, H002X9_A1885Alerta_Inicio, H002X9_A1883Alerta_ToUser, H002X9_n1883Alerta_ToUser, H002X9_A1884Alerta_ToGroup, H002X9_n1884Alerta_ToGroup
               }
               , new Object[] {
               H002X10_A1078ContratoGestor_ContratoCod, H002X10_A1079ContratoGestor_UsuarioCod
               }
               , new Object[] {
               H002X11_A1078ContratoGestor_ContratoCod, H002X11_A1136ContratoGestor_ContratadaCod, H002X11_n1136ContratoGestor_ContratadaCod, H002X11_A1079ContratoGestor_UsuarioCod, H002X11_A40Contratada_PessoaCod, H002X11_n40Contratada_PessoaCod, H002X11_A1446ContratoGestor_ContratadaAreaCod, H002X11_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               H002X12_A1078ContratoGestor_ContratoCod, H002X12_A1079ContratoGestor_UsuarioCod, H002X12_A1446ContratoGestor_ContratadaAreaCod, H002X12_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               H002X13_A66ContratadaUsuario_ContratadaCod, H002X13_A43Contratada_Ativo, H002X13_n43Contratada_Ativo, H002X13_A1394ContratadaUsuario_UsuarioAtivo, H002X13_n1394ContratadaUsuario_UsuarioAtivo, H002X13_A69ContratadaUsuario_UsuarioCod
               }
               , new Object[] {
               H002X14_A63ContratanteUsuario_ContratanteCod, H002X14_A54Usuario_Ativo, H002X14_n54Usuario_Ativo, H002X14_A60ContratanteUsuario_UsuarioCod
               }
               , new Object[] {
               H002X15_A5AreaTrabalho_Codigo, H002X15_A29Contratante_Codigo, H002X15_n29Contratante_Codigo
               }
            }
         );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      private short nRcdExists_8 ;
      private short nIsMod_8 ;
      private short nRcdExists_7 ;
      private short nIsMod_7 ;
      private short nRcdExists_6 ;
      private short nIsMod_6 ;
      private short nRcdExists_5 ;
      private short nIsMod_5 ;
      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nRcdExists_4 ;
      private short nIsMod_4 ;
      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV35UserRememberMe ;
      private short AV61GXLvl181 ;
      private short AV63GXLvl211 ;
      private short AV64GXLvl217 ;
      private short A1884Alerta_ToGroup ;
      private short AV36wwpContext_gxTpr_Userid ;
      private short AV73GXLvl438 ;
      private short AV74GXLvl444 ;
      private short AV75GXLvl447 ;
      private short nGXWrapped ;
      private int A1Usuario_Codigo ;
      private int imgavButtonfacebook_Visible ;
      private int imgavButtongoogle_Visible ;
      private int imgavButtontwitter_Visible ;
      private int imgavButtongamremote_Visible ;
      private int tblTblmain_Visible ;
      private int AV58GXV1 ;
      private int A2016Usuario_UltimaArea ;
      private int AV43LastUserAreaTrabalho ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A67ContratadaUsuario_ContratadaPessoaCod ;
      private int A335Contratante_PessoaCod ;
      private int A5AreaTrabalho_Codigo ;
      private int A29Contratante_Codigo ;
      private int A1216AreaTrabalho_OrganizacaoCod ;
      private int A830AreaTrabalho_ServicoPadrao ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int AV66GXV2 ;
      private int A1882Alerta_ToAreaTrabalho ;
      private int A1881Alerta_Codigo ;
      private int A1883Alerta_ToUser ;
      private int AV36wwpContext_gxTpr_Contratada_pessoacod ;
      private int A1446ContratoGestor_ContratadaAreaCod ;
      private int A40Contratada_PessoaCod ;
      private int A1078ContratoGestor_ContratoCod ;
      private int A1136ContratoGestor_ContratadaCod ;
      private int AV72GXV3 ;
      private int AV47Alerta_Codigo ;
      private int idxLst ;
      private long A334ParametrosSistema_AppID ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Confirmpanel_Title ;
      private String Confirmpanel_Confirmationtext ;
      private String Confirmpanel_Yesbuttoncaption ;
      private String Confirmpanel_Nobuttoncaption ;
      private String Confirmpanel_Yesbuttonposition ;
      private String Confirmpanel2_Title ;
      private String Confirmpanel2_Confirmationtext ;
      private String Confirmpanel2_Yesbuttoncaption ;
      private String Confirmpanel2_Nobuttoncaption ;
      private String Confirmpanel2_Cancelbuttoncaption ;
      private String Confirmpanel2_Yesbuttonposition ;
      private String Confirmpanel2_Confirmtype ;
      private String Confirmpanel_Result ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String divContainer_Internalname ;
      private String divCollg12_Internalname ;
      private String divPanellogin_Internalname ;
      private String divLogo1_Internalname ;
      private String divFormgroup_Internalname ;
      private String divCheckbox_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavUsuario_inativo_Internalname ;
      private String chkavUsuario_deferias_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String chkavRememberme_Internalname ;
      private String chkavKeepmeloggedin_Internalname ;
      private String imgavButtonfacebook_Internalname ;
      private String imgavButtongoogle_Internalname ;
      private String imgavButtontwitter_Internalname ;
      private String imgavButtongamremote_Internalname ;
      private String edtavUsername_Internalname ;
      private String AV34UserPassword ;
      private String edtavUserpassword_Internalname ;
      private String tblTblmain_Internalname ;
      private String AV6ApplicationClientId ;
      private String AV24Language ;
      private String AV27LogOnTo ;
      private String scmdbuf ;
      private String A341Usuario_UserGamGuid ;
      private String A68ContratadaUsuario_ContratadaPessoaNom ;
      private String A10Contratante_NomeFantasia ;
      private String A9Contratante_RazaoSocial ;
      private String A642AreaTrabalho_CalculoPFinal ;
      private String A331ParametrosSistema_NomeSistema ;
      private String Confirmpanel2_Internalname ;
      private String sStyleString ;
      private String tblTable3_Internalname ;
      private String tblTable6_Internalname ;
      private String lblTbrememberme2_Internalname ;
      private String lblTbrememberme2_Jsonclick ;
      private String tblTable4_Internalname ;
      private String bttButton1_Internalname ;
      private String bttButton1_Jsonclick ;
      private String tblTable5_Internalname ;
      private String edtavUsername_Jsonclick ;
      private String edtavUserpassword_Jsonclick ;
      private String tblTbllogin_Internalname ;
      private String lblTbtitle_Internalname ;
      private String lblTbtitle_Jsonclick ;
      private String tblTblfields_Internalname ;
      private String lblTbusername_Internalname ;
      private String lblTbusername_Jsonclick ;
      private String lblTbpassword_Internalname ;
      private String lblTbpassword_Jsonclick ;
      private String bttLogin_Internalname ;
      private String bttLogin_Jsonclick ;
      private String tblTable1_Internalname ;
      private String tblTblrem_Internalname ;
      private String tblTblextauth_Internalname ;
      private String imgavButtonfacebook_Jsonclick ;
      private String imgavButtongoogle_Jsonclick ;
      private String imgavButtontwitter_Jsonclick ;
      private String imgavButtongamremote_Jsonclick ;
      private String Confirmpanel_Internalname ;
      private DateTime Gx_date ;
      private DateTime A1886Alerta_Fim ;
      private DateTime A1885Alerta_Inicio ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool A290Usuario_EhAuditorFM ;
      private bool wbLoad ;
      private bool AV50Usuario_Inativo ;
      private bool AV48Usuario_DeFerias ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV28RememberMe ;
      private bool AV23KeepMeLoggedIn ;
      private bool returnInSub ;
      private bool AV21isOK ;
      private bool AV22isRedirect ;
      private bool AV31SessionValid ;
      private bool AV25LoginOK ;
      private bool A289Usuario_EhContador ;
      private bool A291Usuario_EhContratada ;
      private bool A292Usuario_EhContratante ;
      private bool A293Usuario_EhFinanceiro ;
      private bool n1647Usuario_Email ;
      private bool A1908Usuario_DeFerias ;
      private bool n1908Usuario_DeFerias ;
      private bool n2016Usuario_UltimaArea ;
      private bool A43Contratada_Ativo ;
      private bool n43Contratada_Ativo ;
      private bool A1394ContratadaUsuario_UsuarioAtivo ;
      private bool n1394ContratadaUsuario_UsuarioAtivo ;
      private bool n67ContratadaUsuario_ContratadaPessoaCod ;
      private bool n68ContratadaUsuario_ContratadaPessoaNom ;
      private bool GXt_boolean1 ;
      private bool A30Contratante_Ativo ;
      private bool n30Contratante_Ativo ;
      private bool A72AreaTrabalho_Ativo ;
      private bool n29Contratante_Codigo ;
      private bool n9Contratante_RazaoSocial ;
      private bool n12Contratante_CNPJ ;
      private bool n1216AreaTrabalho_OrganizacaoCod ;
      private bool n830AreaTrabalho_ServicoPadrao ;
      private bool n1021ParametrosSistema_PathCrtf ;
      private bool A54Usuario_Ativo ;
      private bool n54Usuario_Ativo ;
      private bool AV51Usuario_SemArea ;
      private bool AV44Usuario_EhGestor ;
      private bool n1882Alerta_ToAreaTrabalho ;
      private bool n1886Alerta_Fim ;
      private bool n1883Alerta_ToUser ;
      private bool n1884Alerta_ToGroup ;
      private bool n1136ContratoGestor_ContratadaCod ;
      private bool n40Contratada_PessoaCod ;
      private bool n1446ContratoGestor_ContratadaAreaCod ;
      private bool AV10ButtonFacebook_IsBlob ;
      private bool AV12ButtonGoogle_IsBlob ;
      private bool AV13ButtonTwitter_IsBlob ;
      private bool AV11ButtonGAMRemote_IsBlob ;
      private String AV7ApplicationData ;
      private String AV33UserName ;
      private String AV54Buttonfacebook_GXI ;
      private String AV55Buttongoogle_GXI ;
      private String AV56Buttontwitter_GXI ;
      private String AV57Buttongamremote_GXI ;
      private String AV32URL ;
      private String A1647Usuario_Email ;
      private String A12Contratante_CNPJ ;
      private String A6AreaTrabalho_Descricao ;
      private String A1021ParametrosSistema_PathCrtf ;
      private String AV10ButtonFacebook ;
      private String AV12ButtonGoogle ;
      private String AV13ButtonTwitter ;
      private String AV11ButtonGAMRemote ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCheckbox chkavRememberme ;
      private GXCheckbox chkavKeepmeloggedin ;
      private GXCheckbox chkavUsuario_inativo ;
      private GXCheckbox chkavUsuario_deferias ;
      private IDataStoreProvider pr_default ;
      private String[] H002X2_A341Usuario_UserGamGuid ;
      private bool[] H002X2_A289Usuario_EhContador ;
      private bool[] H002X2_A291Usuario_EhContratada ;
      private bool[] H002X2_A292Usuario_EhContratante ;
      private bool[] H002X2_A293Usuario_EhFinanceiro ;
      private String[] H002X2_A1647Usuario_Email ;
      private bool[] H002X2_n1647Usuario_Email ;
      private bool[] H002X2_A1908Usuario_DeFerias ;
      private bool[] H002X2_n1908Usuario_DeFerias ;
      private int[] H002X2_A2016Usuario_UltimaArea ;
      private bool[] H002X2_n2016Usuario_UltimaArea ;
      private int[] H002X2_A1Usuario_Codigo ;
      private int[] H002X3_A69ContratadaUsuario_UsuarioCod ;
      private bool[] H002X3_A43Contratada_Ativo ;
      private bool[] H002X3_n43Contratada_Ativo ;
      private bool[] H002X3_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] H002X3_n1394ContratadaUsuario_UsuarioAtivo ;
      private int[] H002X3_A66ContratadaUsuario_ContratadaCod ;
      private int[] H002X3_A67ContratadaUsuario_ContratadaPessoaCod ;
      private bool[] H002X3_n67ContratadaUsuario_ContratadaPessoaCod ;
      private String[] H002X3_A68ContratadaUsuario_ContratadaPessoaNom ;
      private bool[] H002X3_n68ContratadaUsuario_ContratadaPessoaNom ;
      private int[] H002X4_A335Contratante_PessoaCod ;
      private bool[] H002X4_A30Contratante_Ativo ;
      private bool[] H002X4_n30Contratante_Ativo ;
      private bool[] H002X4_A72AreaTrabalho_Ativo ;
      private int[] H002X4_A5AreaTrabalho_Codigo ;
      private int[] H002X4_A29Contratante_Codigo ;
      private bool[] H002X4_n29Contratante_Codigo ;
      private String[] H002X4_A10Contratante_NomeFantasia ;
      private String[] H002X4_A9Contratante_RazaoSocial ;
      private bool[] H002X4_n9Contratante_RazaoSocial ;
      private String[] H002X4_A12Contratante_CNPJ ;
      private bool[] H002X4_n12Contratante_CNPJ ;
      private int[] H002X4_A1216AreaTrabalho_OrganizacaoCod ;
      private bool[] H002X4_n1216AreaTrabalho_OrganizacaoCod ;
      private String[] H002X4_A6AreaTrabalho_Descricao ;
      private int[] H002X4_A830AreaTrabalho_ServicoPadrao ;
      private bool[] H002X4_n830AreaTrabalho_ServicoPadrao ;
      private String[] H002X4_A642AreaTrabalho_CalculoPFinal ;
      private int[] H002X5_A330ParametrosSistema_Codigo ;
      private long[] H002X5_A334ParametrosSistema_AppID ;
      private String[] H002X5_A331ParametrosSistema_NomeSistema ;
      private String[] H002X5_A1021ParametrosSistema_PathCrtf ;
      private bool[] H002X5_n1021ParametrosSistema_PathCrtf ;
      private int[] H002X6_A63ContratanteUsuario_ContratanteCod ;
      private bool[] H002X6_A30Contratante_Ativo ;
      private bool[] H002X6_n30Contratante_Ativo ;
      private bool[] H002X6_A54Usuario_Ativo ;
      private bool[] H002X6_n54Usuario_Ativo ;
      private int[] H002X6_A60ContratanteUsuario_UsuarioCod ;
      private int[] H002X7_A66ContratadaUsuario_ContratadaCod ;
      private bool[] H002X7_A43Contratada_Ativo ;
      private bool[] H002X7_n43Contratada_Ativo ;
      private bool[] H002X7_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] H002X7_n1394ContratadaUsuario_UsuarioAtivo ;
      private int[] H002X7_A69ContratadaUsuario_UsuarioCod ;
      private int[] H002X8_A1078ContratoGestor_ContratoCod ;
      private int[] H002X8_A1079ContratoGestor_UsuarioCod ;
      private int[] H002X9_A1882Alerta_ToAreaTrabalho ;
      private bool[] H002X9_n1882Alerta_ToAreaTrabalho ;
      private int[] H002X9_A1881Alerta_Codigo ;
      private DateTime[] H002X9_A1886Alerta_Fim ;
      private bool[] H002X9_n1886Alerta_Fim ;
      private DateTime[] H002X9_A1885Alerta_Inicio ;
      private int[] H002X9_A1883Alerta_ToUser ;
      private bool[] H002X9_n1883Alerta_ToUser ;
      private short[] H002X9_A1884Alerta_ToGroup ;
      private bool[] H002X9_n1884Alerta_ToGroup ;
      private int[] H002X10_A1078ContratoGestor_ContratoCod ;
      private int[] H002X10_A1079ContratoGestor_UsuarioCod ;
      private int[] H002X11_A1078ContratoGestor_ContratoCod ;
      private int[] H002X11_A1136ContratoGestor_ContratadaCod ;
      private bool[] H002X11_n1136ContratoGestor_ContratadaCod ;
      private int[] H002X11_A1079ContratoGestor_UsuarioCod ;
      private int[] H002X11_A40Contratada_PessoaCod ;
      private bool[] H002X11_n40Contratada_PessoaCod ;
      private int[] H002X11_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] H002X11_n1446ContratoGestor_ContratadaAreaCod ;
      private int[] H002X12_A1078ContratoGestor_ContratoCod ;
      private int[] H002X12_A1079ContratoGestor_UsuarioCod ;
      private int[] H002X12_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] H002X12_n1446ContratoGestor_ContratadaAreaCod ;
      private int[] H002X13_A66ContratadaUsuario_ContratadaCod ;
      private bool[] H002X13_A43Contratada_Ativo ;
      private bool[] H002X13_n43Contratada_Ativo ;
      private bool[] H002X13_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] H002X13_n1394ContratadaUsuario_UsuarioAtivo ;
      private int[] H002X13_A69ContratadaUsuario_UsuarioCod ;
      private int[] H002X14_A63ContratanteUsuario_ContratanteCod ;
      private bool[] H002X14_A54Usuario_Ativo ;
      private bool[] H002X14_n54Usuario_Ativo ;
      private int[] H002X14_A60ContratanteUsuario_UsuarioCod ;
      private int[] H002X15_A5AreaTrabalho_Codigo ;
      private int[] H002X15_A29Contratante_Codigo ;
      private bool[] H002X15_n29Contratante_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV46Alertas ;
      [ObjectCollection(ItemType=typeof( SdtGAMAuthenticationTypeSimple ))]
      private IGxCollection AV9AuthenticationTypes ;
      [ObjectCollection(ItemType=typeof( SdtGAMConnectionInfo ))]
      private IGxCollection AV15ConnectionInfoCollection ;
      [ObjectCollection(ItemType=typeof( SdtGAMError ))]
      private IGxCollection AV18Errors ;
      [ObjectCollection(ItemType=typeof( SdtGAMError ))]
      private IGxCollection AV19ErrorsLogin ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtProgramNames_ProgramName ))]
      private IGxCollection AV40Pgmnames ;
      private SdtGAMLoginAdditionalParameters AV5AdditionalParameter ;
      private SdtGAMAuthenticationTypeSimple AV8AuthenticationType ;
      private SdtGAMError AV17Error ;
      private SdtGAMExampleSDTApplicationData AV20GAMExampleSDTApplicationData ;
      private SdtGAMUser AV38GamUser ;
      private SdtGAMRepository AV29Repository ;
      private SdtGAMSession AV30Session ;
      private wwpbaseobjects.SdtWWPContext AV36wwpContext ;
      private SdtUsuario AV49Usuario ;
   }

   public class login__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H002X11( IGxContext context ,
                                              int A1882Alerta_ToAreaTrabalho ,
                                              int A1446ContratoGestor_ContratadaAreaCod ,
                                              int A40Contratada_PessoaCod ,
                                              int AV36wwpContext_gxTpr_Contratada_pessoacod ,
                                              short AV36wwpContext_gxTpr_Userid ,
                                              int A1079ContratoGestor_UsuarioCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [2] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT TOP 1 T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T2.[Contratada_Codigo] AS ContratoGestor_ContratadaCod, T1.[ContratoGestor_UsuarioCod], T3.[Contratada_PessoaCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM (([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratoGestor_UsuarioCod] = @AV36wwpContext__Userid)";
         scmdbuf = scmdbuf + " and (T3.[Contratada_PessoaCod] = @AV36wwpC_1Contratada_pessoaco)";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoGestor_UsuarioCod]";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H002X12( IGxContext context ,
                                              int A1882Alerta_ToAreaTrabalho ,
                                              int A1446ContratoGestor_ContratadaAreaCod ,
                                              short AV36wwpContext_gxTpr_Userid ,
                                              int A1079ContratoGestor_UsuarioCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [1] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT TOP 1 T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratoGestor_UsuarioCod] = @AV36wwpContext__Userid)";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoGestor_UsuarioCod]";
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 9 :
                     return conditional_H002X11(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (short)dynConstraints[4] , (int)dynConstraints[5] );
               case 10 :
                     return conditional_H002X12(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (short)dynConstraints[2] , (int)dynConstraints[3] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH002X2 ;
          prmH002X2 = new Object[] {
          new Object[] {"@AV38GamUser__Guid",SqlDbType.Char,40,0}
          } ;
          Object[] prmH002X3 ;
          prmH002X3 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH002X4 ;
          prmH002X4 = new Object[] {
          new Object[] {"@AV43LastUserAreaTrabalho",SqlDbType.Int,6,0}
          } ;
          Object[] prmH002X5 ;
          prmH002X5 = new Object[] {
          } ;
          Object[] prmH002X6 ;
          prmH002X6 = new Object[] {
          new Object[] {"@AV36wwpContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH002X7 ;
          prmH002X7 = new Object[] {
          new Object[] {"@AV36wwpContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH002X8 ;
          prmH002X8 = new Object[] {
          new Object[] {"@AV36wwpContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH002X9 ;
          prmH002X9 = new Object[] {
          } ;
          Object[] prmH002X10 ;
          prmH002X10 = new Object[] {
          new Object[] {"@AV36wwpContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH002X13 ;
          prmH002X13 = new Object[] {
          new Object[] {"@AV36wwpContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH002X14 ;
          prmH002X14 = new Object[] {
          new Object[] {"@AV36wwpContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH002X15 ;
          prmH002X15 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH002X11 ;
          prmH002X11 = new Object[] {
          new Object[] {"@AV36wwpContext__Userid",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV36wwpC_1Contratada_pessoaco",SqlDbType.Int,6,0}
          } ;
          Object[] prmH002X12 ;
          prmH002X12 = new Object[] {
          new Object[] {"@AV36wwpContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H002X2", "SELECT TOP 1 [Usuario_UserGamGuid], [Usuario_EhContador], [Usuario_EhContratada], [Usuario_EhContratante], [Usuario_EhFinanceiro], [Usuario_Email], [Usuario_DeFerias], [Usuario_UltimaArea], [Usuario_Codigo] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_UserGamGuid] = @AV38GamUser__Guid ORDER BY [Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH002X2,1,0,true,true )
             ,new CursorDef("H002X3", "SELECT TOP 1 T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T3.[Contratada_Ativo], T2.[Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo, T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T3.[Contratada_PessoaCod] AS ContratadaUsuario_ContratadaPessoaCod, T4.[Pessoa_Nome] AS ContratadaUsuario_ContratadaPessoaNom FROM ((([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod]) WHERE (T1.[ContratadaUsuario_UsuarioCod] = @Usuario_Codigo) AND (T2.[Usuario_Ativo] = 1) AND (T3.[Contratada_Ativo] = 1) ORDER BY T1.[ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH002X3,1,0,false,true )
             ,new CursorDef("H002X4", "SELECT T2.[Contratante_PessoaCod] AS Contratante_PessoaCod, T2.[Contratante_Ativo], T1.[AreaTrabalho_Ativo], T1.[AreaTrabalho_Codigo], T1.[Contratante_Codigo], T2.[Contratante_NomeFantasia], T3.[Pessoa_Nome] AS Contratante_RazaoSocial, T3.[Pessoa_Docto] AS Contratante_CNPJ, T1.[AreaTrabalho_OrganizacaoCod], T1.[AreaTrabalho_Descricao], T1.[AreaTrabalho_ServicoPadrao], T1.[AreaTrabalho_CalculoPFinal] FROM (([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratante_PessoaCod]) WHERE (T1.[AreaTrabalho_Codigo] = @AV43LastUserAreaTrabalho) AND (T1.[AreaTrabalho_Ativo] = 1) AND (T2.[Contratante_Ativo] = 1) ORDER BY T1.[AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH002X4,1,0,false,true )
             ,new CursorDef("H002X5", "SELECT TOP 1 [ParametrosSistema_Codigo], [ParametrosSistema_AppID], [ParametrosSistema_NomeSistema], [ParametrosSistema_PathCrtf] FROM [ParametrosSistema] WITH (NOLOCK) ORDER BY [ParametrosSistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH002X5,1,0,false,true )
             ,new CursorDef("H002X6", "SELECT TOP 1 T1.[ContratanteUsuario_ContratanteCod] AS ContratanteUsuario_ContratanteCod, T2.[Contratante_Ativo], T3.[Usuario_Ativo], T1.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod FROM (([ContratanteUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[ContratanteUsuario_ContratanteCod]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[ContratanteUsuario_UsuarioCod]) WHERE (T1.[ContratanteUsuario_UsuarioCod] = @AV36wwpContext__Userid) AND (T3.[Usuario_Ativo] = 1) AND (T2.[Contratante_Ativo] = 1) ORDER BY T1.[ContratanteUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH002X6,1,0,false,true )
             ,new CursorDef("H002X7", "SELECT TOP 1 T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T2.[Contratada_Ativo], T3.[Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) WHERE (T1.[ContratadaUsuario_UsuarioCod] = @AV36wwpContext__Userid) AND (T3.[Usuario_Ativo] = 1) AND (T2.[Contratada_Ativo] = 1) ORDER BY T1.[ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH002X7,1,0,false,true )
             ,new CursorDef("H002X8", "SELECT TOP 1 [ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, [ContratoGestor_UsuarioCod] FROM [ContratoGestor] WITH (NOLOCK) WHERE [ContratoGestor_UsuarioCod] = @AV36wwpContext__Userid ORDER BY [ContratoGestor_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH002X8,1,0,false,true )
             ,new CursorDef("H002X9", "SELECT [Alerta_ToAreaTrabalho], [Alerta_Codigo], [Alerta_Fim], [Alerta_Inicio], [Alerta_ToUser], [Alerta_ToGroup] FROM [Alerta] WITH (NOLOCK) ORDER BY [Alerta_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH002X9,100,0,true,false )
             ,new CursorDef("H002X10", "SELECT TOP 1 [ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, [ContratoGestor_UsuarioCod] FROM [ContratoGestor] WITH (NOLOCK) WHERE [ContratoGestor_UsuarioCod] = @AV36wwpContext__Userid ORDER BY [ContratoGestor_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH002X10,1,0,false,true )
             ,new CursorDef("H002X11", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH002X11,1,0,false,true )
             ,new CursorDef("H002X12", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH002X12,1,0,false,true )
             ,new CursorDef("H002X13", "SELECT TOP 1 T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T2.[Contratada_Ativo], T3.[Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) WHERE (T1.[ContratadaUsuario_UsuarioCod] = @AV36wwpContext__Userid) AND (T3.[Usuario_Ativo] = 1) AND (T2.[Contratada_Ativo] = 1) ORDER BY T1.[ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH002X13,1,0,false,true )
             ,new CursorDef("H002X14", "SELECT TOP 1 T1.[ContratanteUsuario_ContratanteCod] AS ContratanteUsuario_ContratanteCod, T2.[Usuario_Ativo], T1.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod FROM ([ContratanteUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratanteUsuario_UsuarioCod]) WHERE (T1.[ContratanteUsuario_UsuarioCod] = @AV36wwpContext__Userid) AND (T2.[Usuario_Ativo] = 1) ORDER BY T1.[ContratanteUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH002X14,1,0,true,true )
             ,new CursorDef("H002X15", "SELECT TOP 1 [AreaTrabalho_Codigo], [Contratante_Codigo] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [Contratante_Codigo] = @ContratanteUsuario_ContratanteCod ORDER BY [Contratante_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH002X15,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 40) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((bool[]) buf[4])[0] = rslt.getBool(5) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((bool[]) buf[7])[0] = rslt.getBool(7) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((int[]) buf[9])[0] = rslt.getInt(8) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((int[]) buf[11])[0] = rslt.getInt(9) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((String[]) buf[6])[0] = rslt.getString(6, 100) ;
                ((String[]) buf[7])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((String[]) buf[9])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((int[]) buf[11])[0] = rslt.getInt(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getVarchar(10) ;
                ((int[]) buf[14])[0] = rslt.getInt(11) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(11);
                ((String[]) buf[16])[0] = rslt.getString(12, 2) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((long[]) buf[1])[0] = rslt.getLong(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((short[]) buf[8])[0] = rslt.getShort(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 9 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                return;
             case 10 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[1]);
                }
                return;
             case 11 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
