/*
               File: SistemaBaseline
        Description: Sistema Baseline
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 9:12:22.62
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class sistemabaseline : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public sistemabaseline( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public sistemabaseline( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Sistema_Codigo )
      {
         this.AV7Sistema_Codigo = aP0_Sistema_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Sistema_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7Sistema_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_37 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_37_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_37_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV15OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
                  AV13OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedDsc", AV13OrderedDsc);
                  AV18Baseline_DataHomologacao = context.localUtil.ParseDTimeParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Baseline_DataHomologacao", context.localUtil.TToC( AV18Baseline_DataHomologacao, 8, 5, 0, 3, "/", ":", " "));
                  AV19Baseline_DataHomologacao_To = context.localUtil.ParseDTimeParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Baseline_DataHomologacao_To", context.localUtil.TToC( AV19Baseline_DataHomologacao_To, 8, 5, 0, 3, "/", ":", " "));
                  AV23TFBaseline_DataHomologacao = context.localUtil.ParseDTimeParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFBaseline_DataHomologacao", context.localUtil.TToC( AV23TFBaseline_DataHomologacao, 8, 5, 0, 3, "/", ":", " "));
                  AV24TFBaseline_DataHomologacao_To = context.localUtil.ParseDTimeParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFBaseline_DataHomologacao_To", context.localUtil.TToC( AV24TFBaseline_DataHomologacao_To, 8, 5, 0, 3, "/", ":", " "));
                  AV29TFBaseline_PFBAntes = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFBaseline_PFBAntes", StringUtil.LTrim( StringUtil.Str( AV29TFBaseline_PFBAntes, 14, 5)));
                  AV30TFBaseline_PFBAntes_To = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFBaseline_PFBAntes_To", StringUtil.LTrim( StringUtil.Str( AV30TFBaseline_PFBAntes_To, 14, 5)));
                  AV33TFBaseline_PFBDepois = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFBaseline_PFBDepois", StringUtil.LTrim( StringUtil.Str( AV33TFBaseline_PFBDepois, 14, 5)));
                  AV34TFBaseline_PFBDepois_To = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFBaseline_PFBDepois_To", StringUtil.LTrim( StringUtil.Str( AV34TFBaseline_PFBDepois_To, 14, 5)));
                  AV7Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Sistema_Codigo), 6, 0)));
                  AV27ddo_Baseline_DataHomologacaoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27ddo_Baseline_DataHomologacaoTitleControlIdToReplace", AV27ddo_Baseline_DataHomologacaoTitleControlIdToReplace);
                  AV31ddo_Baseline_PFBAntesTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ddo_Baseline_PFBAntesTitleControlIdToReplace", AV31ddo_Baseline_PFBAntesTitleControlIdToReplace);
                  AV35ddo_Baseline_PFBDepoisTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35ddo_Baseline_PFBDepoisTitleControlIdToReplace", AV35ddo_Baseline_PFBDepoisTitleControlIdToReplace);
                  AV42Pgmname = GetNextPar( );
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV15OrderedBy, AV13OrderedDsc, AV18Baseline_DataHomologacao, AV19Baseline_DataHomologacao_To, AV23TFBaseline_DataHomologacao, AV24TFBaseline_DataHomologacao_To, AV29TFBaseline_PFBAntes, AV30TFBaseline_PFBAntes_To, AV33TFBaseline_PFBDepois, AV34TFBaseline_PFBDepois_To, AV7Sistema_Codigo, AV27ddo_Baseline_DataHomologacaoTitleControlIdToReplace, AV31ddo_Baseline_PFBAntesTitleControlIdToReplace, AV35ddo_Baseline_PFBDepoisTitleControlIdToReplace, AV42Pgmname, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAED2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV42Pgmname = "SistemaBaseline";
               context.Gx_err = 0;
               WSED2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Sistema Baseline") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203269122279");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("sistemabaseline.aspx") + "?" + UrlEncode("" +AV7Sistema_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV13OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vBASELINE_DATAHOMOLOGACAO", context.localUtil.TToC( AV18Baseline_DataHomologacao, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vBASELINE_DATAHOMOLOGACAO_TO", context.localUtil.TToC( AV19Baseline_DataHomologacao_To, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFBASELINE_DATAHOMOLOGACAO", context.localUtil.TToC( AV23TFBaseline_DataHomologacao, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFBASELINE_DATAHOMOLOGACAO_TO", context.localUtil.TToC( AV24TFBaseline_DataHomologacao_To, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFBASELINE_PFBANTES", StringUtil.LTrim( StringUtil.NToC( AV29TFBaseline_PFBAntes, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFBASELINE_PFBANTES_TO", StringUtil.LTrim( StringUtil.NToC( AV30TFBaseline_PFBAntes_To, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFBASELINE_PFBDEPOIS", StringUtil.LTrim( StringUtil.NToC( AV33TFBaseline_PFBDepois, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFBASELINE_PFBDEPOIS_TO", StringUtil.LTrim( StringUtil.NToC( AV34TFBaseline_PFBDepois_To, 14, 5, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_37", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_37), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV36DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV36DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vBASELINE_DATAHOMOLOGACAOTITLEFILTERDATA", AV22Baseline_DataHomologacaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vBASELINE_DATAHOMOLOGACAOTITLEFILTERDATA", AV22Baseline_DataHomologacaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vBASELINE_PFBANTESTITLEFILTERDATA", AV28Baseline_PFBAntesTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vBASELINE_PFBANTESTITLEFILTERDATA", AV28Baseline_PFBAntesTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vBASELINE_PFBDEPOISTITLEFILTERDATA", AV32Baseline_PFBDepoisTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vBASELINE_PFBDEPOISTITLEFILTERDATA", AV32Baseline_PFBDepoisTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7Sistema_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vSISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV42Pgmname));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Caption", StringUtil.RTrim( Ddo_baseline_datahomologacao_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Tooltip", StringUtil.RTrim( Ddo_baseline_datahomologacao_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Cls", StringUtil.RTrim( Ddo_baseline_datahomologacao_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Filteredtext_set", StringUtil.RTrim( Ddo_baseline_datahomologacao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Filteredtextto_set", StringUtil.RTrim( Ddo_baseline_datahomologacao_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_baseline_datahomologacao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_baseline_datahomologacao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Includesortasc", StringUtil.BoolToStr( Ddo_baseline_datahomologacao_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Includesortdsc", StringUtil.BoolToStr( Ddo_baseline_datahomologacao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Sortedstatus", StringUtil.RTrim( Ddo_baseline_datahomologacao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Includefilter", StringUtil.BoolToStr( Ddo_baseline_datahomologacao_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Filtertype", StringUtil.RTrim( Ddo_baseline_datahomologacao_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Filterisrange", StringUtil.BoolToStr( Ddo_baseline_datahomologacao_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Includedatalist", StringUtil.BoolToStr( Ddo_baseline_datahomologacao_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Sortasc", StringUtil.RTrim( Ddo_baseline_datahomologacao_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Sortdsc", StringUtil.RTrim( Ddo_baseline_datahomologacao_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Cleanfilter", StringUtil.RTrim( Ddo_baseline_datahomologacao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Rangefilterfrom", StringUtil.RTrim( Ddo_baseline_datahomologacao_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Rangefilterto", StringUtil.RTrim( Ddo_baseline_datahomologacao_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Searchbuttontext", StringUtil.RTrim( Ddo_baseline_datahomologacao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBANTES_Caption", StringUtil.RTrim( Ddo_baseline_pfbantes_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBANTES_Tooltip", StringUtil.RTrim( Ddo_baseline_pfbantes_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBANTES_Cls", StringUtil.RTrim( Ddo_baseline_pfbantes_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBANTES_Filteredtext_set", StringUtil.RTrim( Ddo_baseline_pfbantes_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBANTES_Filteredtextto_set", StringUtil.RTrim( Ddo_baseline_pfbantes_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBANTES_Dropdownoptionstype", StringUtil.RTrim( Ddo_baseline_pfbantes_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBANTES_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_baseline_pfbantes_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBANTES_Includesortasc", StringUtil.BoolToStr( Ddo_baseline_pfbantes_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBANTES_Includesortdsc", StringUtil.BoolToStr( Ddo_baseline_pfbantes_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBANTES_Sortedstatus", StringUtil.RTrim( Ddo_baseline_pfbantes_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBANTES_Includefilter", StringUtil.BoolToStr( Ddo_baseline_pfbantes_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBANTES_Filtertype", StringUtil.RTrim( Ddo_baseline_pfbantes_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBANTES_Filterisrange", StringUtil.BoolToStr( Ddo_baseline_pfbantes_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBANTES_Includedatalist", StringUtil.BoolToStr( Ddo_baseline_pfbantes_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBANTES_Sortasc", StringUtil.RTrim( Ddo_baseline_pfbantes_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBANTES_Sortdsc", StringUtil.RTrim( Ddo_baseline_pfbantes_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBANTES_Cleanfilter", StringUtil.RTrim( Ddo_baseline_pfbantes_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBANTES_Rangefilterfrom", StringUtil.RTrim( Ddo_baseline_pfbantes_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBANTES_Rangefilterto", StringUtil.RTrim( Ddo_baseline_pfbantes_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBANTES_Searchbuttontext", StringUtil.RTrim( Ddo_baseline_pfbantes_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBDEPOIS_Caption", StringUtil.RTrim( Ddo_baseline_pfbdepois_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBDEPOIS_Tooltip", StringUtil.RTrim( Ddo_baseline_pfbdepois_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBDEPOIS_Cls", StringUtil.RTrim( Ddo_baseline_pfbdepois_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBDEPOIS_Filteredtext_set", StringUtil.RTrim( Ddo_baseline_pfbdepois_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBDEPOIS_Filteredtextto_set", StringUtil.RTrim( Ddo_baseline_pfbdepois_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBDEPOIS_Dropdownoptionstype", StringUtil.RTrim( Ddo_baseline_pfbdepois_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBDEPOIS_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_baseline_pfbdepois_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBDEPOIS_Includesortasc", StringUtil.BoolToStr( Ddo_baseline_pfbdepois_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBDEPOIS_Includesortdsc", StringUtil.BoolToStr( Ddo_baseline_pfbdepois_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBDEPOIS_Sortedstatus", StringUtil.RTrim( Ddo_baseline_pfbdepois_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBDEPOIS_Includefilter", StringUtil.BoolToStr( Ddo_baseline_pfbdepois_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBDEPOIS_Filtertype", StringUtil.RTrim( Ddo_baseline_pfbdepois_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBDEPOIS_Filterisrange", StringUtil.BoolToStr( Ddo_baseline_pfbdepois_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBDEPOIS_Includedatalist", StringUtil.BoolToStr( Ddo_baseline_pfbdepois_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBDEPOIS_Sortasc", StringUtil.RTrim( Ddo_baseline_pfbdepois_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBDEPOIS_Sortdsc", StringUtil.RTrim( Ddo_baseline_pfbdepois_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBDEPOIS_Cleanfilter", StringUtil.RTrim( Ddo_baseline_pfbdepois_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBDEPOIS_Rangefilterfrom", StringUtil.RTrim( Ddo_baseline_pfbdepois_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBDEPOIS_Rangefilterto", StringUtil.RTrim( Ddo_baseline_pfbdepois_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBDEPOIS_Searchbuttontext", StringUtil.RTrim( Ddo_baseline_pfbdepois_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Activeeventkey", StringUtil.RTrim( Ddo_baseline_datahomologacao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Filteredtext_get", StringUtil.RTrim( Ddo_baseline_datahomologacao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Filteredtextto_get", StringUtil.RTrim( Ddo_baseline_datahomologacao_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBANTES_Activeeventkey", StringUtil.RTrim( Ddo_baseline_pfbantes_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBANTES_Filteredtext_get", StringUtil.RTrim( Ddo_baseline_pfbantes_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBANTES_Filteredtextto_get", StringUtil.RTrim( Ddo_baseline_pfbantes_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBDEPOIS_Activeeventkey", StringUtil.RTrim( Ddo_baseline_pfbdepois_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBDEPOIS_Filteredtext_get", StringUtil.RTrim( Ddo_baseline_pfbdepois_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_BASELINE_PFBDEPOIS_Filteredtextto_get", StringUtil.RTrim( Ddo_baseline_pfbdepois_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormED2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("sistemabaseline.js", "?20203269122381");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "SistemaBaseline" ;
      }

      public override String GetPgmdesc( )
      {
         return "Sistema Baseline" ;
      }

      protected void WBED0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "sistemabaseline.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_ED2( true) ;
         }
         else
         {
            wb_table1_2_ED2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_ED2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSistema_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSistema_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtSistema_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_SistemaBaseline.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'" + sPrefix + "',false,'" + sGXsfl_37_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfbaseline_datahomologacao_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfbaseline_datahomologacao_Internalname, context.localUtil.TToC( AV23TFBaseline_DataHomologacao, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV23TFBaseline_DataHomologacao, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,47);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfbaseline_datahomologacao_Jsonclick, 0, "Attribute", "", "", "", edtavTfbaseline_datahomologacao_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaBaseline.htm");
            GxWebStd.gx_bitmap( context, edtavTfbaseline_datahomologacao_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfbaseline_datahomologacao_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_SistemaBaseline.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'" + sPrefix + "',false,'" + sGXsfl_37_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfbaseline_datahomologacao_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfbaseline_datahomologacao_to_Internalname, context.localUtil.TToC( AV24TFBaseline_DataHomologacao_To, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV24TFBaseline_DataHomologacao_To, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,48);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfbaseline_datahomologacao_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfbaseline_datahomologacao_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaBaseline.htm");
            GxWebStd.gx_bitmap( context, edtavTfbaseline_datahomologacao_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfbaseline_datahomologacao_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_SistemaBaseline.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_baseline_datahomologacaoauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'" + sPrefix + "',false,'" + sGXsfl_37_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_baseline_datahomologacaoauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_baseline_datahomologacaoauxdate_Internalname, context.localUtil.Format(AV25DDO_Baseline_DataHomologacaoAuxDate, "99/99/99"), context.localUtil.Format( AV25DDO_Baseline_DataHomologacaoAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,50);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_baseline_datahomologacaoauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaBaseline.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_baseline_datahomologacaoauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_SistemaBaseline.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'" + sPrefix + "',false,'" + sGXsfl_37_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_baseline_datahomologacaoauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_baseline_datahomologacaoauxdateto_Internalname, context.localUtil.Format(AV26DDO_Baseline_DataHomologacaoAuxDateTo, "99/99/99"), context.localUtil.Format( AV26DDO_Baseline_DataHomologacaoAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,51);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_baseline_datahomologacaoauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaBaseline.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_baseline_datahomologacaoauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_SistemaBaseline.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'" + sPrefix + "',false,'" + sGXsfl_37_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfbaseline_pfbantes_Internalname, StringUtil.LTrim( StringUtil.NToC( AV29TFBaseline_PFBAntes, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV29TFBaseline_PFBAntes, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,52);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfbaseline_pfbantes_Jsonclick, 0, "Attribute", "", "", "", edtavTfbaseline_pfbantes_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaBaseline.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'" + sPrefix + "',false,'" + sGXsfl_37_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfbaseline_pfbantes_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV30TFBaseline_PFBAntes_To, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV30TFBaseline_PFBAntes_To, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,53);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfbaseline_pfbantes_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfbaseline_pfbantes_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaBaseline.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'" + sPrefix + "',false,'" + sGXsfl_37_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfbaseline_pfbdepois_Internalname, StringUtil.LTrim( StringUtil.NToC( AV33TFBaseline_PFBDepois, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV33TFBaseline_PFBDepois, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,54);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfbaseline_pfbdepois_Jsonclick, 0, "Attribute", "", "", "", edtavTfbaseline_pfbdepois_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaBaseline.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'" + sPrefix + "',false,'" + sGXsfl_37_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfbaseline_pfbdepois_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV34TFBaseline_PFBDepois_To, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV34TFBaseline_PFBDepois_To, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,55);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfbaseline_pfbdepois_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfbaseline_pfbdepois_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaBaseline.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_BASELINE_DATAHOMOLOGACAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'" + sPrefix + "',false,'" + sGXsfl_37_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_baseline_datahomologacaotitlecontrolidtoreplace_Internalname, AV27ddo_Baseline_DataHomologacaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,57);\"", 0, edtavDdo_baseline_datahomologacaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_SistemaBaseline.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_BASELINE_PFBANTESContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'" + sPrefix + "',false,'" + sGXsfl_37_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_baseline_pfbantestitlecontrolidtoreplace_Internalname, AV31ddo_Baseline_PFBAntesTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", 0, edtavDdo_baseline_pfbantestitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_SistemaBaseline.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_BASELINE_PFBDEPOISContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'" + sPrefix + "',false,'" + sGXsfl_37_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_baseline_pfbdepoistitlecontrolidtoreplace_Internalname, AV35ddo_Baseline_PFBDepoisTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,61);\"", 0, edtavDdo_baseline_pfbdepoistitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_SistemaBaseline.htm");
         }
         wbLoad = true;
      }

      protected void STARTED2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Sistema Baseline", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPED0( ) ;
            }
         }
      }

      protected void WSED2( )
      {
         STARTED2( ) ;
         EVTED2( ) ;
      }

      protected void EVTED2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPED0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPED0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11ED2 */
                                    E11ED2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_BASELINE_DATAHOMOLOGACAO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPED0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12ED2 */
                                    E12ED2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_BASELINE_PFBANTES.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPED0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13ED2 */
                                    E13ED2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_BASELINE_PFBDEPOIS.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPED0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14ED2 */
                                    E14ED2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPED0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15ED2 */
                                    E15ED2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPED0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E16ED2 */
                                    E16ED2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPED0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = cmbavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPED0( ) ;
                              }
                              nGXsfl_37_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_37_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_37_idx), 4, 0)), 4, "0");
                              SubsflControlProps_372( ) ;
                              A722Baseline_Codigo = (int)(context.localUtil.CToN( cgiGet( edtBaseline_Codigo_Internalname), ",", "."));
                              A1164Baseline_DataHomologacao = context.localUtil.CToT( cgiGet( edtBaseline_DataHomologacao_Internalname), 0);
                              A726Baseline_PFBAntes = context.localUtil.CToN( cgiGet( edtBaseline_PFBAntes_Internalname), ",", ".");
                              n726Baseline_PFBAntes = false;
                              A727Baseline_PFBDepois = context.localUtil.CToN( cgiGet( edtBaseline_PFBDepois_Internalname), ",", ".");
                              n727Baseline_PFBDepois = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E17ED2 */
                                          E17ED2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E18ED2 */
                                          E18ED2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E19ED2 */
                                          E19ED2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV15OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV13OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Baseline_datahomologacao Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vBASELINE_DATAHOMOLOGACAO"), 0) != AV18Baseline_DataHomologacao )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Baseline_datahomologacao_to Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vBASELINE_DATAHOMOLOGACAO_TO"), 0) != AV19Baseline_DataHomologacao_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfbaseline_datahomologacao Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFBASELINE_DATAHOMOLOGACAO"), 0) != AV23TFBaseline_DataHomologacao )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfbaseline_datahomologacao_to Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFBASELINE_DATAHOMOLOGACAO_TO"), 0) != AV24TFBaseline_DataHomologacao_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfbaseline_pfbantes Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFBASELINE_PFBANTES"), ",", ".") != AV29TFBaseline_PFBAntes )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfbaseline_pfbantes_to Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFBASELINE_PFBANTES_TO"), ",", ".") != AV30TFBaseline_PFBAntes_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfbaseline_pfbdepois Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFBASELINE_PFBDEPOIS"), ",", ".") != AV33TFBaseline_PFBDepois )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfbaseline_pfbdepois_to Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFBASELINE_PFBDEPOIS_TO"), ",", ".") != AV34TFBaseline_PFBDepois_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPED0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEED2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormED2( ) ;
            }
         }
      }

      protected void PAED2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV15OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_372( ) ;
         while ( nGXsfl_37_idx <= nRC_GXsfl_37 )
         {
            sendrow_372( ) ;
            nGXsfl_37_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_37_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_37_idx+1));
            sGXsfl_37_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_37_idx), 4, 0)), 4, "0");
            SubsflControlProps_372( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV15OrderedBy ,
                                       bool AV13OrderedDsc ,
                                       DateTime AV18Baseline_DataHomologacao ,
                                       DateTime AV19Baseline_DataHomologacao_To ,
                                       DateTime AV23TFBaseline_DataHomologacao ,
                                       DateTime AV24TFBaseline_DataHomologacao_To ,
                                       decimal AV29TFBaseline_PFBAntes ,
                                       decimal AV30TFBaseline_PFBAntes_To ,
                                       decimal AV33TFBaseline_PFBDepois ,
                                       decimal AV34TFBaseline_PFBDepois_To ,
                                       int AV7Sistema_Codigo ,
                                       String AV27ddo_Baseline_DataHomologacaoTitleControlIdToReplace ,
                                       String AV31ddo_Baseline_PFBAntesTitleControlIdToReplace ,
                                       String AV35ddo_Baseline_PFBDepoisTitleControlIdToReplace ,
                                       String AV42Pgmname ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFED2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_BASELINE_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A722Baseline_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"BASELINE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A722Baseline_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_BASELINE_DATAHOMOLOGACAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1164Baseline_DataHomologacao, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"BASELINE_DATAHOMOLOGACAO", context.localUtil.TToC( A1164Baseline_DataHomologacao, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_BASELINE_PFBANTES", GetSecureSignedToken( sPrefix, context.localUtil.Format( A726Baseline_PFBAntes, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"BASELINE_PFBANTES", StringUtil.LTrim( StringUtil.NToC( A726Baseline_PFBAntes, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_BASELINE_PFBDEPOIS", GetSecureSignedToken( sPrefix, context.localUtil.Format( A727Baseline_PFBDepois, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"BASELINE_PFBDEPOIS", StringUtil.LTrim( StringUtil.NToC( A727Baseline_PFBDepois, 14, 5, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV15OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFED2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV42Pgmname = "SistemaBaseline";
         context.Gx_err = 0;
      }

      protected void RFED2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 37;
         /* Execute user event: E18ED2 */
         E18ED2 ();
         nGXsfl_37_idx = 1;
         sGXsfl_37_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_37_idx), 4, 0)), 4, "0");
         SubsflControlProps_372( ) ;
         nGXsfl_37_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_372( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV18Baseline_DataHomologacao ,
                                                 AV19Baseline_DataHomologacao_To ,
                                                 AV23TFBaseline_DataHomologacao ,
                                                 AV24TFBaseline_DataHomologacao_To ,
                                                 AV29TFBaseline_PFBAntes ,
                                                 AV30TFBaseline_PFBAntes_To ,
                                                 AV33TFBaseline_PFBDepois ,
                                                 AV34TFBaseline_PFBDepois_To ,
                                                 A1164Baseline_DataHomologacao ,
                                                 A726Baseline_PFBAntes ,
                                                 A727Baseline_PFBDepois ,
                                                 AV15OrderedBy ,
                                                 AV13OrderedDsc ,
                                                 A127Sistema_Codigo ,
                                                 AV7Sistema_Codigo },
                                                 new int[] {
                                                 TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.DECIMAL,
                                                 TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                                 }
            });
            /* Using cursor H00ED2 */
            pr_default.execute(0, new Object[] {AV7Sistema_Codigo, AV18Baseline_DataHomologacao, AV19Baseline_DataHomologacao_To, AV23TFBaseline_DataHomologacao, AV24TFBaseline_DataHomologacao_To, AV29TFBaseline_PFBAntes, AV30TFBaseline_PFBAntes_To, AV33TFBaseline_PFBDepois, AV34TFBaseline_PFBDepois_To, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_37_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A735Baseline_ProjetoMelCod = H00ED2_A735Baseline_ProjetoMelCod[0];
               n735Baseline_ProjetoMelCod = H00ED2_n735Baseline_ProjetoMelCod[0];
               A698ProjetoMelhoria_FnAPFCod = H00ED2_A698ProjetoMelhoria_FnAPFCod[0];
               n698ProjetoMelhoria_FnAPFCod = H00ED2_n698ProjetoMelhoria_FnAPFCod[0];
               A359FuncaoAPF_ModuloCod = H00ED2_A359FuncaoAPF_ModuloCod[0];
               n359FuncaoAPF_ModuloCod = H00ED2_n359FuncaoAPF_ModuloCod[0];
               A127Sistema_Codigo = H00ED2_A127Sistema_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
               n127Sistema_Codigo = H00ED2_n127Sistema_Codigo[0];
               A727Baseline_PFBDepois = H00ED2_A727Baseline_PFBDepois[0];
               n727Baseline_PFBDepois = H00ED2_n727Baseline_PFBDepois[0];
               A726Baseline_PFBAntes = H00ED2_A726Baseline_PFBAntes[0];
               n726Baseline_PFBAntes = H00ED2_n726Baseline_PFBAntes[0];
               A1164Baseline_DataHomologacao = H00ED2_A1164Baseline_DataHomologacao[0];
               A722Baseline_Codigo = H00ED2_A722Baseline_Codigo[0];
               A698ProjetoMelhoria_FnAPFCod = H00ED2_A698ProjetoMelhoria_FnAPFCod[0];
               n698ProjetoMelhoria_FnAPFCod = H00ED2_n698ProjetoMelhoria_FnAPFCod[0];
               A359FuncaoAPF_ModuloCod = H00ED2_A359FuncaoAPF_ModuloCod[0];
               n359FuncaoAPF_ModuloCod = H00ED2_n359FuncaoAPF_ModuloCod[0];
               A127Sistema_Codigo = H00ED2_A127Sistema_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
               n127Sistema_Codigo = H00ED2_n127Sistema_Codigo[0];
               /* Execute user event: E19ED2 */
               E19ED2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 37;
            WBED0( ) ;
         }
         nGXsfl_37_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV18Baseline_DataHomologacao ,
                                              AV19Baseline_DataHomologacao_To ,
                                              AV23TFBaseline_DataHomologacao ,
                                              AV24TFBaseline_DataHomologacao_To ,
                                              AV29TFBaseline_PFBAntes ,
                                              AV30TFBaseline_PFBAntes_To ,
                                              AV33TFBaseline_PFBDepois ,
                                              AV34TFBaseline_PFBDepois_To ,
                                              A1164Baseline_DataHomologacao ,
                                              A726Baseline_PFBAntes ,
                                              A727Baseline_PFBDepois ,
                                              AV15OrderedBy ,
                                              AV13OrderedDsc ,
                                              A127Sistema_Codigo ,
                                              AV7Sistema_Codigo },
                                              new int[] {
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.DECIMAL,
                                              TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         /* Using cursor H00ED3 */
         pr_default.execute(1, new Object[] {AV7Sistema_Codigo, AV18Baseline_DataHomologacao, AV19Baseline_DataHomologacao_To, AV23TFBaseline_DataHomologacao, AV24TFBaseline_DataHomologacao_To, AV29TFBaseline_PFBAntes, AV30TFBaseline_PFBAntes_To, AV33TFBaseline_PFBDepois, AV34TFBaseline_PFBDepois_To});
         GRID_nRecordCount = H00ED3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15OrderedBy, AV13OrderedDsc, AV18Baseline_DataHomologacao, AV19Baseline_DataHomologacao_To, AV23TFBaseline_DataHomologacao, AV24TFBaseline_DataHomologacao_To, AV29TFBaseline_PFBAntes, AV30TFBaseline_PFBAntes_To, AV33TFBaseline_PFBDepois, AV34TFBaseline_PFBDepois_To, AV7Sistema_Codigo, AV27ddo_Baseline_DataHomologacaoTitleControlIdToReplace, AV31ddo_Baseline_PFBAntesTitleControlIdToReplace, AV35ddo_Baseline_PFBDepoisTitleControlIdToReplace, AV42Pgmname, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15OrderedBy, AV13OrderedDsc, AV18Baseline_DataHomologacao, AV19Baseline_DataHomologacao_To, AV23TFBaseline_DataHomologacao, AV24TFBaseline_DataHomologacao_To, AV29TFBaseline_PFBAntes, AV30TFBaseline_PFBAntes_To, AV33TFBaseline_PFBDepois, AV34TFBaseline_PFBDepois_To, AV7Sistema_Codigo, AV27ddo_Baseline_DataHomologacaoTitleControlIdToReplace, AV31ddo_Baseline_PFBAntesTitleControlIdToReplace, AV35ddo_Baseline_PFBDepoisTitleControlIdToReplace, AV42Pgmname, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15OrderedBy, AV13OrderedDsc, AV18Baseline_DataHomologacao, AV19Baseline_DataHomologacao_To, AV23TFBaseline_DataHomologacao, AV24TFBaseline_DataHomologacao_To, AV29TFBaseline_PFBAntes, AV30TFBaseline_PFBAntes_To, AV33TFBaseline_PFBDepois, AV34TFBaseline_PFBDepois_To, AV7Sistema_Codigo, AV27ddo_Baseline_DataHomologacaoTitleControlIdToReplace, AV31ddo_Baseline_PFBAntesTitleControlIdToReplace, AV35ddo_Baseline_PFBDepoisTitleControlIdToReplace, AV42Pgmname, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15OrderedBy, AV13OrderedDsc, AV18Baseline_DataHomologacao, AV19Baseline_DataHomologacao_To, AV23TFBaseline_DataHomologacao, AV24TFBaseline_DataHomologacao_To, AV29TFBaseline_PFBAntes, AV30TFBaseline_PFBAntes_To, AV33TFBaseline_PFBDepois, AV34TFBaseline_PFBDepois_To, AV7Sistema_Codigo, AV27ddo_Baseline_DataHomologacaoTitleControlIdToReplace, AV31ddo_Baseline_PFBAntesTitleControlIdToReplace, AV35ddo_Baseline_PFBDepoisTitleControlIdToReplace, AV42Pgmname, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15OrderedBy, AV13OrderedDsc, AV18Baseline_DataHomologacao, AV19Baseline_DataHomologacao_To, AV23TFBaseline_DataHomologacao, AV24TFBaseline_DataHomologacao_To, AV29TFBaseline_PFBAntes, AV30TFBaseline_PFBAntes_To, AV33TFBaseline_PFBDepois, AV34TFBaseline_PFBDepois_To, AV7Sistema_Codigo, AV27ddo_Baseline_DataHomologacaoTitleControlIdToReplace, AV31ddo_Baseline_PFBAntesTitleControlIdToReplace, AV35ddo_Baseline_PFBDepoisTitleControlIdToReplace, AV42Pgmname, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPED0( )
      {
         /* Before Start, stand alone formulas. */
         AV42Pgmname = "SistemaBaseline";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E17ED2 */
         E17ED2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV36DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vBASELINE_DATAHOMOLOGACAOTITLEFILTERDATA"), AV22Baseline_DataHomologacaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vBASELINE_PFBANTESTITLEFILTERDATA"), AV28Baseline_PFBAntesTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vBASELINE_PFBDEPOISTITLEFILTERDATA"), AV32Baseline_PFBDepoisTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV15OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV13OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedDsc", AV13OrderedDsc);
            if ( context.localUtil.VCDateTime( cgiGet( edtavBaseline_datahomologacao_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Baseline_Data Homologacao"}), 1, "vBASELINE_DATAHOMOLOGACAO");
               GX_FocusControl = edtavBaseline_datahomologacao_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV18Baseline_DataHomologacao = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Baseline_DataHomologacao", context.localUtil.TToC( AV18Baseline_DataHomologacao, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV18Baseline_DataHomologacao = context.localUtil.CToT( cgiGet( edtavBaseline_datahomologacao_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Baseline_DataHomologacao", context.localUtil.TToC( AV18Baseline_DataHomologacao, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavBaseline_datahomologacao_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Baseline_Data Homologacao_To"}), 1, "vBASELINE_DATAHOMOLOGACAO_TO");
               GX_FocusControl = edtavBaseline_datahomologacao_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV19Baseline_DataHomologacao_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Baseline_DataHomologacao_To", context.localUtil.TToC( AV19Baseline_DataHomologacao_To, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV19Baseline_DataHomologacao_To = context.localUtil.CToT( cgiGet( edtavBaseline_datahomologacao_to_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Baseline_DataHomologacao_To", context.localUtil.TToC( AV19Baseline_DataHomologacao_To, 8, 5, 0, 3, "/", ":", " "));
            }
            A127Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( edtSistema_Codigo_Internalname), ",", "."));
            n127Sistema_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfbaseline_datahomologacao_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFBaseline_Data Homologacao"}), 1, "vTFBASELINE_DATAHOMOLOGACAO");
               GX_FocusControl = edtavTfbaseline_datahomologacao_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV23TFBaseline_DataHomologacao = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFBaseline_DataHomologacao", context.localUtil.TToC( AV23TFBaseline_DataHomologacao, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV23TFBaseline_DataHomologacao = context.localUtil.CToT( cgiGet( edtavTfbaseline_datahomologacao_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFBaseline_DataHomologacao", context.localUtil.TToC( AV23TFBaseline_DataHomologacao, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfbaseline_datahomologacao_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFBaseline_Data Homologacao_To"}), 1, "vTFBASELINE_DATAHOMOLOGACAO_TO");
               GX_FocusControl = edtavTfbaseline_datahomologacao_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV24TFBaseline_DataHomologacao_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFBaseline_DataHomologacao_To", context.localUtil.TToC( AV24TFBaseline_DataHomologacao_To, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV24TFBaseline_DataHomologacao_To = context.localUtil.CToT( cgiGet( edtavTfbaseline_datahomologacao_to_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFBaseline_DataHomologacao_To", context.localUtil.TToC( AV24TFBaseline_DataHomologacao_To, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_baseline_datahomologacaoauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Baseline_Data Homologacao Aux Date"}), 1, "vDDO_BASELINE_DATAHOMOLOGACAOAUXDATE");
               GX_FocusControl = edtavDdo_baseline_datahomologacaoauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV25DDO_Baseline_DataHomologacaoAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DDO_Baseline_DataHomologacaoAuxDate", context.localUtil.Format(AV25DDO_Baseline_DataHomologacaoAuxDate, "99/99/99"));
            }
            else
            {
               AV25DDO_Baseline_DataHomologacaoAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_baseline_datahomologacaoauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DDO_Baseline_DataHomologacaoAuxDate", context.localUtil.Format(AV25DDO_Baseline_DataHomologacaoAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_baseline_datahomologacaoauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Baseline_Data Homologacao Aux Date To"}), 1, "vDDO_BASELINE_DATAHOMOLOGACAOAUXDATETO");
               GX_FocusControl = edtavDdo_baseline_datahomologacaoauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV26DDO_Baseline_DataHomologacaoAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DDO_Baseline_DataHomologacaoAuxDateTo", context.localUtil.Format(AV26DDO_Baseline_DataHomologacaoAuxDateTo, "99/99/99"));
            }
            else
            {
               AV26DDO_Baseline_DataHomologacaoAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_baseline_datahomologacaoauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DDO_Baseline_DataHomologacaoAuxDateTo", context.localUtil.Format(AV26DDO_Baseline_DataHomologacaoAuxDateTo, "99/99/99"));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfbaseline_pfbantes_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfbaseline_pfbantes_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFBASELINE_PFBANTES");
               GX_FocusControl = edtavTfbaseline_pfbantes_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV29TFBaseline_PFBAntes = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFBaseline_PFBAntes", StringUtil.LTrim( StringUtil.Str( AV29TFBaseline_PFBAntes, 14, 5)));
            }
            else
            {
               AV29TFBaseline_PFBAntes = context.localUtil.CToN( cgiGet( edtavTfbaseline_pfbantes_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFBaseline_PFBAntes", StringUtil.LTrim( StringUtil.Str( AV29TFBaseline_PFBAntes, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfbaseline_pfbantes_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfbaseline_pfbantes_to_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFBASELINE_PFBANTES_TO");
               GX_FocusControl = edtavTfbaseline_pfbantes_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV30TFBaseline_PFBAntes_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFBaseline_PFBAntes_To", StringUtil.LTrim( StringUtil.Str( AV30TFBaseline_PFBAntes_To, 14, 5)));
            }
            else
            {
               AV30TFBaseline_PFBAntes_To = context.localUtil.CToN( cgiGet( edtavTfbaseline_pfbantes_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFBaseline_PFBAntes_To", StringUtil.LTrim( StringUtil.Str( AV30TFBaseline_PFBAntes_To, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfbaseline_pfbdepois_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfbaseline_pfbdepois_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFBASELINE_PFBDEPOIS");
               GX_FocusControl = edtavTfbaseline_pfbdepois_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV33TFBaseline_PFBDepois = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFBaseline_PFBDepois", StringUtil.LTrim( StringUtil.Str( AV33TFBaseline_PFBDepois, 14, 5)));
            }
            else
            {
               AV33TFBaseline_PFBDepois = context.localUtil.CToN( cgiGet( edtavTfbaseline_pfbdepois_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFBaseline_PFBDepois", StringUtil.LTrim( StringUtil.Str( AV33TFBaseline_PFBDepois, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfbaseline_pfbdepois_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfbaseline_pfbdepois_to_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFBASELINE_PFBDEPOIS_TO");
               GX_FocusControl = edtavTfbaseline_pfbdepois_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV34TFBaseline_PFBDepois_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFBaseline_PFBDepois_To", StringUtil.LTrim( StringUtil.Str( AV34TFBaseline_PFBDepois_To, 14, 5)));
            }
            else
            {
               AV34TFBaseline_PFBDepois_To = context.localUtil.CToN( cgiGet( edtavTfbaseline_pfbdepois_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFBaseline_PFBDepois_To", StringUtil.LTrim( StringUtil.Str( AV34TFBaseline_PFBDepois_To, 14, 5)));
            }
            AV27ddo_Baseline_DataHomologacaoTitleControlIdToReplace = cgiGet( edtavDdo_baseline_datahomologacaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27ddo_Baseline_DataHomologacaoTitleControlIdToReplace", AV27ddo_Baseline_DataHomologacaoTitleControlIdToReplace);
            AV31ddo_Baseline_PFBAntesTitleControlIdToReplace = cgiGet( edtavDdo_baseline_pfbantestitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ddo_Baseline_PFBAntesTitleControlIdToReplace", AV31ddo_Baseline_PFBAntesTitleControlIdToReplace);
            AV35ddo_Baseline_PFBDepoisTitleControlIdToReplace = cgiGet( edtavDdo_baseline_pfbdepoistitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35ddo_Baseline_PFBDepoisTitleControlIdToReplace", AV35ddo_Baseline_PFBDepoisTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_37 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_37"), ",", "."));
            AV38GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV39GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Sistema_Codigo"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_baseline_datahomologacao_Caption = cgiGet( sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Caption");
            Ddo_baseline_datahomologacao_Tooltip = cgiGet( sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Tooltip");
            Ddo_baseline_datahomologacao_Cls = cgiGet( sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Cls");
            Ddo_baseline_datahomologacao_Filteredtext_set = cgiGet( sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Filteredtext_set");
            Ddo_baseline_datahomologacao_Filteredtextto_set = cgiGet( sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Filteredtextto_set");
            Ddo_baseline_datahomologacao_Dropdownoptionstype = cgiGet( sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Dropdownoptionstype");
            Ddo_baseline_datahomologacao_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Titlecontrolidtoreplace");
            Ddo_baseline_datahomologacao_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Includesortasc"));
            Ddo_baseline_datahomologacao_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Includesortdsc"));
            Ddo_baseline_datahomologacao_Sortedstatus = cgiGet( sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Sortedstatus");
            Ddo_baseline_datahomologacao_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Includefilter"));
            Ddo_baseline_datahomologacao_Filtertype = cgiGet( sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Filtertype");
            Ddo_baseline_datahomologacao_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Filterisrange"));
            Ddo_baseline_datahomologacao_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Includedatalist"));
            Ddo_baseline_datahomologacao_Sortasc = cgiGet( sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Sortasc");
            Ddo_baseline_datahomologacao_Sortdsc = cgiGet( sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Sortdsc");
            Ddo_baseline_datahomologacao_Cleanfilter = cgiGet( sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Cleanfilter");
            Ddo_baseline_datahomologacao_Rangefilterfrom = cgiGet( sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Rangefilterfrom");
            Ddo_baseline_datahomologacao_Rangefilterto = cgiGet( sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Rangefilterto");
            Ddo_baseline_datahomologacao_Searchbuttontext = cgiGet( sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Searchbuttontext");
            Ddo_baseline_pfbantes_Caption = cgiGet( sPrefix+"DDO_BASELINE_PFBANTES_Caption");
            Ddo_baseline_pfbantes_Tooltip = cgiGet( sPrefix+"DDO_BASELINE_PFBANTES_Tooltip");
            Ddo_baseline_pfbantes_Cls = cgiGet( sPrefix+"DDO_BASELINE_PFBANTES_Cls");
            Ddo_baseline_pfbantes_Filteredtext_set = cgiGet( sPrefix+"DDO_BASELINE_PFBANTES_Filteredtext_set");
            Ddo_baseline_pfbantes_Filteredtextto_set = cgiGet( sPrefix+"DDO_BASELINE_PFBANTES_Filteredtextto_set");
            Ddo_baseline_pfbantes_Dropdownoptionstype = cgiGet( sPrefix+"DDO_BASELINE_PFBANTES_Dropdownoptionstype");
            Ddo_baseline_pfbantes_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_BASELINE_PFBANTES_Titlecontrolidtoreplace");
            Ddo_baseline_pfbantes_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_BASELINE_PFBANTES_Includesortasc"));
            Ddo_baseline_pfbantes_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_BASELINE_PFBANTES_Includesortdsc"));
            Ddo_baseline_pfbantes_Sortedstatus = cgiGet( sPrefix+"DDO_BASELINE_PFBANTES_Sortedstatus");
            Ddo_baseline_pfbantes_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_BASELINE_PFBANTES_Includefilter"));
            Ddo_baseline_pfbantes_Filtertype = cgiGet( sPrefix+"DDO_BASELINE_PFBANTES_Filtertype");
            Ddo_baseline_pfbantes_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_BASELINE_PFBANTES_Filterisrange"));
            Ddo_baseline_pfbantes_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_BASELINE_PFBANTES_Includedatalist"));
            Ddo_baseline_pfbantes_Sortasc = cgiGet( sPrefix+"DDO_BASELINE_PFBANTES_Sortasc");
            Ddo_baseline_pfbantes_Sortdsc = cgiGet( sPrefix+"DDO_BASELINE_PFBANTES_Sortdsc");
            Ddo_baseline_pfbantes_Cleanfilter = cgiGet( sPrefix+"DDO_BASELINE_PFBANTES_Cleanfilter");
            Ddo_baseline_pfbantes_Rangefilterfrom = cgiGet( sPrefix+"DDO_BASELINE_PFBANTES_Rangefilterfrom");
            Ddo_baseline_pfbantes_Rangefilterto = cgiGet( sPrefix+"DDO_BASELINE_PFBANTES_Rangefilterto");
            Ddo_baseline_pfbantes_Searchbuttontext = cgiGet( sPrefix+"DDO_BASELINE_PFBANTES_Searchbuttontext");
            Ddo_baseline_pfbdepois_Caption = cgiGet( sPrefix+"DDO_BASELINE_PFBDEPOIS_Caption");
            Ddo_baseline_pfbdepois_Tooltip = cgiGet( sPrefix+"DDO_BASELINE_PFBDEPOIS_Tooltip");
            Ddo_baseline_pfbdepois_Cls = cgiGet( sPrefix+"DDO_BASELINE_PFBDEPOIS_Cls");
            Ddo_baseline_pfbdepois_Filteredtext_set = cgiGet( sPrefix+"DDO_BASELINE_PFBDEPOIS_Filteredtext_set");
            Ddo_baseline_pfbdepois_Filteredtextto_set = cgiGet( sPrefix+"DDO_BASELINE_PFBDEPOIS_Filteredtextto_set");
            Ddo_baseline_pfbdepois_Dropdownoptionstype = cgiGet( sPrefix+"DDO_BASELINE_PFBDEPOIS_Dropdownoptionstype");
            Ddo_baseline_pfbdepois_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_BASELINE_PFBDEPOIS_Titlecontrolidtoreplace");
            Ddo_baseline_pfbdepois_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_BASELINE_PFBDEPOIS_Includesortasc"));
            Ddo_baseline_pfbdepois_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_BASELINE_PFBDEPOIS_Includesortdsc"));
            Ddo_baseline_pfbdepois_Sortedstatus = cgiGet( sPrefix+"DDO_BASELINE_PFBDEPOIS_Sortedstatus");
            Ddo_baseline_pfbdepois_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_BASELINE_PFBDEPOIS_Includefilter"));
            Ddo_baseline_pfbdepois_Filtertype = cgiGet( sPrefix+"DDO_BASELINE_PFBDEPOIS_Filtertype");
            Ddo_baseline_pfbdepois_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_BASELINE_PFBDEPOIS_Filterisrange"));
            Ddo_baseline_pfbdepois_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_BASELINE_PFBDEPOIS_Includedatalist"));
            Ddo_baseline_pfbdepois_Sortasc = cgiGet( sPrefix+"DDO_BASELINE_PFBDEPOIS_Sortasc");
            Ddo_baseline_pfbdepois_Sortdsc = cgiGet( sPrefix+"DDO_BASELINE_PFBDEPOIS_Sortdsc");
            Ddo_baseline_pfbdepois_Cleanfilter = cgiGet( sPrefix+"DDO_BASELINE_PFBDEPOIS_Cleanfilter");
            Ddo_baseline_pfbdepois_Rangefilterfrom = cgiGet( sPrefix+"DDO_BASELINE_PFBDEPOIS_Rangefilterfrom");
            Ddo_baseline_pfbdepois_Rangefilterto = cgiGet( sPrefix+"DDO_BASELINE_PFBDEPOIS_Rangefilterto");
            Ddo_baseline_pfbdepois_Searchbuttontext = cgiGet( sPrefix+"DDO_BASELINE_PFBDEPOIS_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_baseline_datahomologacao_Activeeventkey = cgiGet( sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Activeeventkey");
            Ddo_baseline_datahomologacao_Filteredtext_get = cgiGet( sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Filteredtext_get");
            Ddo_baseline_datahomologacao_Filteredtextto_get = cgiGet( sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO_Filteredtextto_get");
            Ddo_baseline_pfbantes_Activeeventkey = cgiGet( sPrefix+"DDO_BASELINE_PFBANTES_Activeeventkey");
            Ddo_baseline_pfbantes_Filteredtext_get = cgiGet( sPrefix+"DDO_BASELINE_PFBANTES_Filteredtext_get");
            Ddo_baseline_pfbantes_Filteredtextto_get = cgiGet( sPrefix+"DDO_BASELINE_PFBANTES_Filteredtextto_get");
            Ddo_baseline_pfbdepois_Activeeventkey = cgiGet( sPrefix+"DDO_BASELINE_PFBDEPOIS_Activeeventkey");
            Ddo_baseline_pfbdepois_Filteredtext_get = cgiGet( sPrefix+"DDO_BASELINE_PFBDEPOIS_Filteredtext_get");
            Ddo_baseline_pfbdepois_Filteredtextto_get = cgiGet( sPrefix+"DDO_BASELINE_PFBDEPOIS_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV15OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV13OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vBASELINE_DATAHOMOLOGACAO"), 0) != AV18Baseline_DataHomologacao )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vBASELINE_DATAHOMOLOGACAO_TO"), 0) != AV19Baseline_DataHomologacao_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFBASELINE_DATAHOMOLOGACAO"), 0) != AV23TFBaseline_DataHomologacao )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFBASELINE_DATAHOMOLOGACAO_TO"), 0) != AV24TFBaseline_DataHomologacao_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFBASELINE_PFBANTES"), ",", ".") != AV29TFBaseline_PFBAntes )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFBASELINE_PFBANTES_TO"), ",", ".") != AV30TFBaseline_PFBAntes_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFBASELINE_PFBDEPOIS"), ",", ".") != AV33TFBaseline_PFBDepois )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFBASELINE_PFBDEPOIS_TO"), ",", ".") != AV34TFBaseline_PFBDepois_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E17ED2 */
         E17ED2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E17ED2( )
      {
         /* Start Routine */
         subGrid_Rows = 50;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTfbaseline_datahomologacao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfbaseline_datahomologacao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfbaseline_datahomologacao_Visible), 5, 0)));
         edtavTfbaseline_datahomologacao_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfbaseline_datahomologacao_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfbaseline_datahomologacao_to_Visible), 5, 0)));
         edtavTfbaseline_pfbantes_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfbaseline_pfbantes_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfbaseline_pfbantes_Visible), 5, 0)));
         edtavTfbaseline_pfbantes_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfbaseline_pfbantes_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfbaseline_pfbantes_to_Visible), 5, 0)));
         edtavTfbaseline_pfbdepois_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfbaseline_pfbdepois_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfbaseline_pfbdepois_Visible), 5, 0)));
         edtavTfbaseline_pfbdepois_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfbaseline_pfbdepois_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfbaseline_pfbdepois_to_Visible), 5, 0)));
         Ddo_baseline_datahomologacao_Titlecontrolidtoreplace = subGrid_Internalname+"_Baseline_DataHomologacao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_baseline_datahomologacao_Internalname, "TitleControlIdToReplace", Ddo_baseline_datahomologacao_Titlecontrolidtoreplace);
         AV27ddo_Baseline_DataHomologacaoTitleControlIdToReplace = Ddo_baseline_datahomologacao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27ddo_Baseline_DataHomologacaoTitleControlIdToReplace", AV27ddo_Baseline_DataHomologacaoTitleControlIdToReplace);
         edtavDdo_baseline_datahomologacaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_baseline_datahomologacaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_baseline_datahomologacaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_baseline_pfbantes_Titlecontrolidtoreplace = subGrid_Internalname+"_Baseline_PFBAntes";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_baseline_pfbantes_Internalname, "TitleControlIdToReplace", Ddo_baseline_pfbantes_Titlecontrolidtoreplace);
         AV31ddo_Baseline_PFBAntesTitleControlIdToReplace = Ddo_baseline_pfbantes_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ddo_Baseline_PFBAntesTitleControlIdToReplace", AV31ddo_Baseline_PFBAntesTitleControlIdToReplace);
         edtavDdo_baseline_pfbantestitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_baseline_pfbantestitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_baseline_pfbantestitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_baseline_pfbdepois_Titlecontrolidtoreplace = subGrid_Internalname+"_Baseline_PFBDepois";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_baseline_pfbdepois_Internalname, "TitleControlIdToReplace", Ddo_baseline_pfbdepois_Titlecontrolidtoreplace);
         AV35ddo_Baseline_PFBDepoisTitleControlIdToReplace = Ddo_baseline_pfbdepois_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35ddo_Baseline_PFBDepoisTitleControlIdToReplace", AV35ddo_Baseline_PFBDepoisTitleControlIdToReplace);
         edtavDdo_baseline_pfbdepoistitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_baseline_pfbdepoistitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_baseline_pfbdepoistitlecontrolidtoreplace_Visible), 5, 0)));
         edtSistema_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtSistema_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistema_Codigo_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Cronol�gico", 0);
         cmbavOrderedby.addItem("2", "Homologa��o", 0);
         cmbavOrderedby.addItem("3", "PFB Antes", 0);
         cmbavOrderedby.addItem("4", "PFB Depois", 0);
         if ( AV15OrderedBy < 1 )
         {
            AV15OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV36DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV36DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E18ED2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV22Baseline_DataHomologacaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV28Baseline_PFBAntesTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV32Baseline_PFBDepoisTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtBaseline_DataHomologacao_Titleformat = 2;
         edtBaseline_DataHomologacao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Homologa��o", AV27ddo_Baseline_DataHomologacaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtBaseline_DataHomologacao_Internalname, "Title", edtBaseline_DataHomologacao_Title);
         edtBaseline_PFBAntes_Titleformat = 2;
         edtBaseline_PFBAntes_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "PFB Antes", AV31ddo_Baseline_PFBAntesTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtBaseline_PFBAntes_Internalname, "Title", edtBaseline_PFBAntes_Title);
         edtBaseline_PFBDepois_Titleformat = 2;
         edtBaseline_PFBDepois_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "PFB Depois", AV35ddo_Baseline_PFBDepoisTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtBaseline_PFBDepois_Internalname, "Title", edtBaseline_PFBDepois_Title);
         AV38GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38GridCurrentPage), 10, 0)));
         AV39GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV22Baseline_DataHomologacaoTitleFilterData", AV22Baseline_DataHomologacaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV28Baseline_PFBAntesTitleFilterData", AV28Baseline_PFBAntesTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV32Baseline_PFBDepoisTitleFilterData", AV32Baseline_PFBDepoisTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
      }

      protected void E11ED2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV37PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV37PageToGo) ;
         }
      }

      protected void E12ED2( )
      {
         /* Ddo_baseline_datahomologacao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_baseline_datahomologacao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV15OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV13OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_baseline_datahomologacao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_baseline_datahomologacao_Internalname, "SortedStatus", Ddo_baseline_datahomologacao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_baseline_datahomologacao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV15OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV13OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_baseline_datahomologacao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_baseline_datahomologacao_Internalname, "SortedStatus", Ddo_baseline_datahomologacao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_baseline_datahomologacao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV23TFBaseline_DataHomologacao = context.localUtil.CToT( Ddo_baseline_datahomologacao_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFBaseline_DataHomologacao", context.localUtil.TToC( AV23TFBaseline_DataHomologacao, 8, 5, 0, 3, "/", ":", " "));
            AV24TFBaseline_DataHomologacao_To = context.localUtil.CToT( Ddo_baseline_datahomologacao_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFBaseline_DataHomologacao_To", context.localUtil.TToC( AV24TFBaseline_DataHomologacao_To, 8, 5, 0, 3, "/", ":", " "));
            if ( ! (DateTime.MinValue==AV24TFBaseline_DataHomologacao_To) )
            {
               AV24TFBaseline_DataHomologacao_To = context.localUtil.YMDHMSToT( (short)(DateTimeUtil.Year( AV24TFBaseline_DataHomologacao_To)), (short)(DateTimeUtil.Month( AV24TFBaseline_DataHomologacao_To)), (short)(DateTimeUtil.Day( AV24TFBaseline_DataHomologacao_To)), 23, 59, 59);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFBaseline_DataHomologacao_To", context.localUtil.TToC( AV24TFBaseline_DataHomologacao_To, 8, 5, 0, 3, "/", ":", " "));
            }
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13ED2( )
      {
         /* Ddo_baseline_pfbantes_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_baseline_pfbantes_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV15OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV13OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_baseline_pfbantes_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_baseline_pfbantes_Internalname, "SortedStatus", Ddo_baseline_pfbantes_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_baseline_pfbantes_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV15OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV13OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_baseline_pfbantes_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_baseline_pfbantes_Internalname, "SortedStatus", Ddo_baseline_pfbantes_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_baseline_pfbantes_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV29TFBaseline_PFBAntes = NumberUtil.Val( Ddo_baseline_pfbantes_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFBaseline_PFBAntes", StringUtil.LTrim( StringUtil.Str( AV29TFBaseline_PFBAntes, 14, 5)));
            AV30TFBaseline_PFBAntes_To = NumberUtil.Val( Ddo_baseline_pfbantes_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFBaseline_PFBAntes_To", StringUtil.LTrim( StringUtil.Str( AV30TFBaseline_PFBAntes_To, 14, 5)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14ED2( )
      {
         /* Ddo_baseline_pfbdepois_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_baseline_pfbdepois_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV15OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV13OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_baseline_pfbdepois_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_baseline_pfbdepois_Internalname, "SortedStatus", Ddo_baseline_pfbdepois_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_baseline_pfbdepois_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV15OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV13OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_baseline_pfbdepois_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_baseline_pfbdepois_Internalname, "SortedStatus", Ddo_baseline_pfbdepois_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_baseline_pfbdepois_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV33TFBaseline_PFBDepois = NumberUtil.Val( Ddo_baseline_pfbdepois_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFBaseline_PFBDepois", StringUtil.LTrim( StringUtil.Str( AV33TFBaseline_PFBDepois, 14, 5)));
            AV34TFBaseline_PFBDepois_To = NumberUtil.Val( Ddo_baseline_pfbdepois_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFBaseline_PFBDepois_To", StringUtil.LTrim( StringUtil.Str( AV34TFBaseline_PFBDepois_To, 14, 5)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E19ED2( )
      {
         /* Grid_Load Routine */
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 37;
         }
         sendrow_372( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_37_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(37, GridRow);
         }
      }

      protected void E15ED2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E16ED2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void S152( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_baseline_datahomologacao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_baseline_datahomologacao_Internalname, "SortedStatus", Ddo_baseline_datahomologacao_Sortedstatus);
         Ddo_baseline_pfbantes_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_baseline_pfbantes_Internalname, "SortedStatus", Ddo_baseline_pfbantes_Sortedstatus);
         Ddo_baseline_pfbdepois_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_baseline_pfbdepois_Internalname, "SortedStatus", Ddo_baseline_pfbdepois_Sortedstatus);
      }

      protected void S132( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV15OrderedBy == 2 )
         {
            Ddo_baseline_datahomologacao_Sortedstatus = (AV13OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_baseline_datahomologacao_Internalname, "SortedStatus", Ddo_baseline_datahomologacao_Sortedstatus);
         }
         else if ( AV15OrderedBy == 3 )
         {
            Ddo_baseline_pfbantes_Sortedstatus = (AV13OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_baseline_pfbantes_Internalname, "SortedStatus", Ddo_baseline_pfbantes_Sortedstatus);
         }
         else if ( AV15OrderedBy == 4 )
         {
            Ddo_baseline_pfbdepois_Sortedstatus = (AV13OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_baseline_pfbdepois_Internalname, "SortedStatus", Ddo_baseline_pfbdepois_Sortedstatus);
         }
      }

      protected void S162( )
      {
         /* 'CLEANFILTERS' Routine */
         AV18Baseline_DataHomologacao = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Baseline_DataHomologacao", context.localUtil.TToC( AV18Baseline_DataHomologacao, 8, 5, 0, 3, "/", ":", " "));
         AV19Baseline_DataHomologacao_To = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Baseline_DataHomologacao_To", context.localUtil.TToC( AV19Baseline_DataHomologacao_To, 8, 5, 0, 3, "/", ":", " "));
         AV23TFBaseline_DataHomologacao = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFBaseline_DataHomologacao", context.localUtil.TToC( AV23TFBaseline_DataHomologacao, 8, 5, 0, 3, "/", ":", " "));
         Ddo_baseline_datahomologacao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_baseline_datahomologacao_Internalname, "FilteredText_set", Ddo_baseline_datahomologacao_Filteredtext_set);
         AV24TFBaseline_DataHomologacao_To = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFBaseline_DataHomologacao_To", context.localUtil.TToC( AV24TFBaseline_DataHomologacao_To, 8, 5, 0, 3, "/", ":", " "));
         Ddo_baseline_datahomologacao_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_baseline_datahomologacao_Internalname, "FilteredTextTo_set", Ddo_baseline_datahomologacao_Filteredtextto_set);
         AV29TFBaseline_PFBAntes = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFBaseline_PFBAntes", StringUtil.LTrim( StringUtil.Str( AV29TFBaseline_PFBAntes, 14, 5)));
         Ddo_baseline_pfbantes_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_baseline_pfbantes_Internalname, "FilteredText_set", Ddo_baseline_pfbantes_Filteredtext_set);
         AV30TFBaseline_PFBAntes_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFBaseline_PFBAntes_To", StringUtil.LTrim( StringUtil.Str( AV30TFBaseline_PFBAntes_To, 14, 5)));
         Ddo_baseline_pfbantes_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_baseline_pfbantes_Internalname, "FilteredTextTo_set", Ddo_baseline_pfbantes_Filteredtextto_set);
         AV33TFBaseline_PFBDepois = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFBaseline_PFBDepois", StringUtil.LTrim( StringUtil.Str( AV33TFBaseline_PFBDepois, 14, 5)));
         Ddo_baseline_pfbdepois_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_baseline_pfbdepois_Internalname, "FilteredText_set", Ddo_baseline_pfbdepois_Filteredtext_set);
         AV34TFBaseline_PFBDepois_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFBaseline_PFBDepois_To", StringUtil.LTrim( StringUtil.Str( AV34TFBaseline_PFBDepois_To, 14, 5)));
         Ddo_baseline_pfbdepois_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_baseline_pfbdepois_Internalname, "FilteredTextTo_set", Ddo_baseline_pfbdepois_Filteredtextto_set);
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV14Session.Get(AV42Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV42Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV14Session.Get(AV42Pgmname+"GridState"), "");
         }
         AV15OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
         AV13OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedDsc", AV13OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S172( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV43GXV1 = 1;
         while ( AV43GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV43GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "BASELINE_DATAHOMOLOGACAO") == 0 )
            {
               AV18Baseline_DataHomologacao = context.localUtil.CToT( AV12GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Baseline_DataHomologacao", context.localUtil.TToC( AV18Baseline_DataHomologacao, 8, 5, 0, 3, "/", ":", " "));
               AV19Baseline_DataHomologacao_To = context.localUtil.CToT( AV12GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Baseline_DataHomologacao_To", context.localUtil.TToC( AV19Baseline_DataHomologacao_To, 8, 5, 0, 3, "/", ":", " "));
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFBASELINE_DATAHOMOLOGACAO") == 0 )
            {
               AV23TFBaseline_DataHomologacao = context.localUtil.CToT( AV12GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFBaseline_DataHomologacao", context.localUtil.TToC( AV23TFBaseline_DataHomologacao, 8, 5, 0, 3, "/", ":", " "));
               AV24TFBaseline_DataHomologacao_To = context.localUtil.CToT( AV12GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFBaseline_DataHomologacao_To", context.localUtil.TToC( AV24TFBaseline_DataHomologacao_To, 8, 5, 0, 3, "/", ":", " "));
               if ( ! (DateTime.MinValue==AV23TFBaseline_DataHomologacao) )
               {
                  AV25DDO_Baseline_DataHomologacaoAuxDate = DateTimeUtil.ResetTime(AV23TFBaseline_DataHomologacao);
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DDO_Baseline_DataHomologacaoAuxDate", context.localUtil.Format(AV25DDO_Baseline_DataHomologacaoAuxDate, "99/99/99"));
                  Ddo_baseline_datahomologacao_Filteredtext_set = context.localUtil.DToC( AV25DDO_Baseline_DataHomologacaoAuxDate, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_baseline_datahomologacao_Internalname, "FilteredText_set", Ddo_baseline_datahomologacao_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV24TFBaseline_DataHomologacao_To) )
               {
                  AV26DDO_Baseline_DataHomologacaoAuxDateTo = DateTimeUtil.ResetTime(AV24TFBaseline_DataHomologacao_To);
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DDO_Baseline_DataHomologacaoAuxDateTo", context.localUtil.Format(AV26DDO_Baseline_DataHomologacaoAuxDateTo, "99/99/99"));
                  Ddo_baseline_datahomologacao_Filteredtextto_set = context.localUtil.DToC( AV26DDO_Baseline_DataHomologacaoAuxDateTo, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_baseline_datahomologacao_Internalname, "FilteredTextTo_set", Ddo_baseline_datahomologacao_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFBASELINE_PFBANTES") == 0 )
            {
               AV29TFBaseline_PFBAntes = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFBaseline_PFBAntes", StringUtil.LTrim( StringUtil.Str( AV29TFBaseline_PFBAntes, 14, 5)));
               AV30TFBaseline_PFBAntes_To = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFBaseline_PFBAntes_To", StringUtil.LTrim( StringUtil.Str( AV30TFBaseline_PFBAntes_To, 14, 5)));
               if ( ! (Convert.ToDecimal(0)==AV29TFBaseline_PFBAntes) )
               {
                  Ddo_baseline_pfbantes_Filteredtext_set = StringUtil.Str( AV29TFBaseline_PFBAntes, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_baseline_pfbantes_Internalname, "FilteredText_set", Ddo_baseline_pfbantes_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV30TFBaseline_PFBAntes_To) )
               {
                  Ddo_baseline_pfbantes_Filteredtextto_set = StringUtil.Str( AV30TFBaseline_PFBAntes_To, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_baseline_pfbantes_Internalname, "FilteredTextTo_set", Ddo_baseline_pfbantes_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFBASELINE_PFBDEPOIS") == 0 )
            {
               AV33TFBaseline_PFBDepois = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFBaseline_PFBDepois", StringUtil.LTrim( StringUtil.Str( AV33TFBaseline_PFBDepois, 14, 5)));
               AV34TFBaseline_PFBDepois_To = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFBaseline_PFBDepois_To", StringUtil.LTrim( StringUtil.Str( AV34TFBaseline_PFBDepois_To, 14, 5)));
               if ( ! (Convert.ToDecimal(0)==AV33TFBaseline_PFBDepois) )
               {
                  Ddo_baseline_pfbdepois_Filteredtext_set = StringUtil.Str( AV33TFBaseline_PFBDepois, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_baseline_pfbdepois_Internalname, "FilteredText_set", Ddo_baseline_pfbdepois_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV34TFBaseline_PFBDepois_To) )
               {
                  Ddo_baseline_pfbdepois_Filteredtextto_set = StringUtil.Str( AV34TFBaseline_PFBDepois_To, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_baseline_pfbdepois_Internalname, "FilteredTextTo_set", Ddo_baseline_pfbdepois_Filteredtextto_set);
               }
            }
            AV43GXV1 = (int)(AV43GXV1+1);
         }
      }

      protected void S142( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV14Session.Get(AV42Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV15OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV13OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (DateTime.MinValue==AV18Baseline_DataHomologacao) && (DateTime.MinValue==AV19Baseline_DataHomologacao_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "BASELINE_DATAHOMOLOGACAO";
            AV12GridStateFilterValue.gxTpr_Value = context.localUtil.TToC( AV18Baseline_DataHomologacao, 8, 5, 0, 3, "/", ":", " ");
            AV12GridStateFilterValue.gxTpr_Valueto = context.localUtil.TToC( AV19Baseline_DataHomologacao_To, 8, 5, 0, 3, "/", ":", " ");
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV23TFBaseline_DataHomologacao) && (DateTime.MinValue==AV24TFBaseline_DataHomologacao_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFBASELINE_DATAHOMOLOGACAO";
            AV12GridStateFilterValue.gxTpr_Value = context.localUtil.TToC( AV23TFBaseline_DataHomologacao, 8, 5, 0, 3, "/", ":", " ");
            AV12GridStateFilterValue.gxTpr_Valueto = context.localUtil.TToC( AV24TFBaseline_DataHomologacao_To, 8, 5, 0, 3, "/", ":", " ");
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV29TFBaseline_PFBAntes) && (Convert.ToDecimal(0)==AV30TFBaseline_PFBAntes_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFBASELINE_PFBANTES";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV29TFBaseline_PFBAntes, 14, 5);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV30TFBaseline_PFBAntes_To, 14, 5);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV33TFBaseline_PFBDepois) && (Convert.ToDecimal(0)==AV34TFBaseline_PFBDepois_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFBASELINE_PFBDEPOIS";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV33TFBaseline_PFBDepois, 14, 5);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV34TFBaseline_PFBDepois_To, 14, 5);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV42Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV42Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "FuncaoAPF";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "Sistema_Codigo";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Sistema_Codigo), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV14Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_ED2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table2_5_ED2( true) ;
         }
         else
         {
            wb_table2_5_ED2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_ED2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_34_ED2( true) ;
         }
         else
         {
            wb_table3_34_ED2( false) ;
         }
         return  ;
      }

      protected void wb_table3_34_ED2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_ED2e( true) ;
         }
         else
         {
            wb_table1_2_ED2e( false) ;
         }
      }

      protected void wb_table3_34_ED2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"37\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtBaseline_DataHomologacao_Titleformat == 0 )
               {
                  context.SendWebValue( edtBaseline_DataHomologacao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtBaseline_DataHomologacao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtBaseline_PFBAntes_Titleformat == 0 )
               {
                  context.SendWebValue( edtBaseline_PFBAntes_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtBaseline_PFBAntes_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtBaseline_PFBDepois_Titleformat == 0 )
               {
                  context.SendWebValue( edtBaseline_PFBDepois_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtBaseline_PFBDepois_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A722Baseline_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A1164Baseline_DataHomologacao, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtBaseline_DataHomologacao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtBaseline_DataHomologacao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A726Baseline_PFBAntes, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtBaseline_PFBAntes_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtBaseline_PFBAntes_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A727Baseline_PFBDepois, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtBaseline_PFBDepois_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtBaseline_PFBDepois_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 37 )
         {
            wbEnd = 0;
            nRC_GXsfl_37 = (short)(nGXsfl_37_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_34_ED2e( true) ;
         }
         else
         {
            wb_table3_34_ED2e( false) ;
         }
      }

      protected void wb_table2_5_ED2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_8_ED2( true) ;
         }
         else
         {
            wb_table4_8_ED2( false) ;
         }
         return  ;
      }

      protected void wb_table4_8_ED2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_SistemaBaseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'" + sPrefix + "',false,'" + sGXsfl_37_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,14);\"", "", true, "HLP_SistemaBaseline.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'" + sPrefix + "',false,'" + sGXsfl_37_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV13OrderedDsc), StringUtil.BoolToStr( AV13OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,15);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_SistemaBaseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_17_ED2( true) ;
         }
         else
         {
            wb_table5_17_ED2( false) ;
         }
         return  ;
      }

      protected void wb_table5_17_ED2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_ED2e( true) ;
         }
         else
         {
            wb_table2_5_ED2e( false) ;
         }
      }

      protected void wb_table5_17_ED2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SistemaBaseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextbaseline_datahomologacao_Internalname, "Data de Homologa��o", "", "", lblFiltertextbaseline_datahomologacao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_SistemaBaseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_24_ED2( true) ;
         }
         else
         {
            wb_table6_24_ED2( false) ;
         }
         return  ;
      }

      protected void wb_table6_24_ED2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_17_ED2e( true) ;
         }
         else
         {
            wb_table5_17_ED2e( false) ;
         }
      }

      protected void wb_table6_24_ED2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedbaseline_datahomologacao_Internalname, tblTablemergedbaseline_datahomologacao_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'" + sPrefix + "',false,'" + sGXsfl_37_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavBaseline_datahomologacao_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavBaseline_datahomologacao_Internalname, context.localUtil.TToC( AV18Baseline_DataHomologacao, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV18Baseline_DataHomologacao, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,27);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavBaseline_datahomologacao_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaBaseline.htm");
            GxWebStd.gx_bitmap( context, edtavBaseline_datahomologacao_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_SistemaBaseline.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblBaseline_datahomologacao_rangemiddletext_Internalname, "at�", "", "", lblBaseline_datahomologacao_rangemiddletext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_SistemaBaseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'" + sPrefix + "',false,'" + sGXsfl_37_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavBaseline_datahomologacao_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavBaseline_datahomologacao_to_Internalname, context.localUtil.TToC( AV19Baseline_DataHomologacao_To, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV19Baseline_DataHomologacao_To, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,31);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavBaseline_datahomologacao_to_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaBaseline.htm");
            GxWebStd.gx_bitmap( context, edtavBaseline_datahomologacao_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_SistemaBaseline.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_24_ED2e( true) ;
         }
         else
         {
            wb_table6_24_ED2e( false) ;
         }
      }

      protected void wb_table4_8_ED2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_8_ED2e( true) ;
         }
         else
         {
            wb_table4_8_ED2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7Sistema_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Sistema_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAED2( ) ;
         WSED2( ) ;
         WEED2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7Sistema_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAED2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "sistemabaseline");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAED2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7Sistema_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Sistema_Codigo), 6, 0)));
         }
         wcpOAV7Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Sistema_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7Sistema_Codigo != wcpOAV7Sistema_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV7Sistema_Codigo = AV7Sistema_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7Sistema_Codigo = cgiGet( sPrefix+"AV7Sistema_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV7Sistema_Codigo) > 0 )
         {
            AV7Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7Sistema_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Sistema_Codigo), 6, 0)));
         }
         else
         {
            AV7Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7Sistema_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAED2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSED2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSED2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7Sistema_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Sistema_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7Sistema_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7Sistema_Codigo_CTRL", StringUtil.RTrim( sCtrlAV7Sistema_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEED2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203269122694");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("sistemabaseline.js", "?20203269122695");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_372( )
      {
         edtBaseline_Codigo_Internalname = sPrefix+"BASELINE_CODIGO_"+sGXsfl_37_idx;
         edtBaseline_DataHomologacao_Internalname = sPrefix+"BASELINE_DATAHOMOLOGACAO_"+sGXsfl_37_idx;
         edtBaseline_PFBAntes_Internalname = sPrefix+"BASELINE_PFBANTES_"+sGXsfl_37_idx;
         edtBaseline_PFBDepois_Internalname = sPrefix+"BASELINE_PFBDEPOIS_"+sGXsfl_37_idx;
      }

      protected void SubsflControlProps_fel_372( )
      {
         edtBaseline_Codigo_Internalname = sPrefix+"BASELINE_CODIGO_"+sGXsfl_37_fel_idx;
         edtBaseline_DataHomologacao_Internalname = sPrefix+"BASELINE_DATAHOMOLOGACAO_"+sGXsfl_37_fel_idx;
         edtBaseline_PFBAntes_Internalname = sPrefix+"BASELINE_PFBANTES_"+sGXsfl_37_fel_idx;
         edtBaseline_PFBDepois_Internalname = sPrefix+"BASELINE_PFBDEPOIS_"+sGXsfl_37_fel_idx;
      }

      protected void sendrow_372( )
      {
         SubsflControlProps_372( ) ;
         WBED0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_37_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_37_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_37_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtBaseline_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A722Baseline_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A722Baseline_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtBaseline_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)37,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtBaseline_DataHomologacao_Internalname,context.localUtil.TToC( A1164Baseline_DataHomologacao, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A1164Baseline_DataHomologacao, "99/99/99 99:99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtBaseline_DataHomologacao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)37,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtBaseline_PFBAntes_Internalname,StringUtil.LTrim( StringUtil.NToC( A726Baseline_PFBAntes, 14, 5, ",", "")),context.localUtil.Format( A726Baseline_PFBAntes, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtBaseline_PFBAntes_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)37,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtBaseline_PFBDepois_Internalname,StringUtil.LTrim( StringUtil.NToC( A727Baseline_PFBDepois, 14, 5, ",", "")),context.localUtil.Format( A727Baseline_PFBDepois, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtBaseline_PFBDepois_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)37,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_BASELINE_CODIGO"+"_"+sGXsfl_37_idx, GetSecureSignedToken( sPrefix+sGXsfl_37_idx, context.localUtil.Format( (decimal)(A722Baseline_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_BASELINE_DATAHOMOLOGACAO"+"_"+sGXsfl_37_idx, GetSecureSignedToken( sPrefix+sGXsfl_37_idx, context.localUtil.Format( A1164Baseline_DataHomologacao, "99/99/99 99:99")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_BASELINE_PFBANTES"+"_"+sGXsfl_37_idx, GetSecureSignedToken( sPrefix+sGXsfl_37_idx, context.localUtil.Format( A726Baseline_PFBAntes, "ZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_BASELINE_PFBDEPOIS"+"_"+sGXsfl_37_idx, GetSecureSignedToken( sPrefix+sGXsfl_37_idx, context.localUtil.Format( A727Baseline_PFBDepois, "ZZ,ZZZ,ZZ9.999")));
            GridContainer.AddRow(GridRow);
            nGXsfl_37_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_37_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_37_idx+1));
            sGXsfl_37_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_37_idx), 4, 0)), 4, "0");
            SubsflControlProps_372( ) ;
         }
         /* End function sendrow_372 */
      }

      protected void init_default_properties( )
      {
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         lblOrderedtext_Internalname = sPrefix+"ORDEREDTEXT";
         cmbavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         imgCleanfilters_Internalname = sPrefix+"CLEANFILTERS";
         lblFiltertextbaseline_datahomologacao_Internalname = sPrefix+"FILTERTEXTBASELINE_DATAHOMOLOGACAO";
         edtavBaseline_datahomologacao_Internalname = sPrefix+"vBASELINE_DATAHOMOLOGACAO";
         lblBaseline_datahomologacao_rangemiddletext_Internalname = sPrefix+"BASELINE_DATAHOMOLOGACAO_RANGEMIDDLETEXT";
         edtavBaseline_datahomologacao_to_Internalname = sPrefix+"vBASELINE_DATAHOMOLOGACAO_TO";
         tblTablemergedbaseline_datahomologacao_Internalname = sPrefix+"TABLEMERGEDBASELINE_DATAHOMOLOGACAO";
         tblTablefilters_Internalname = sPrefix+"TABLEFILTERS";
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         edtBaseline_Codigo_Internalname = sPrefix+"BASELINE_CODIGO";
         edtBaseline_DataHomologacao_Internalname = sPrefix+"BASELINE_DATAHOMOLOGACAO";
         edtBaseline_PFBAntes_Internalname = sPrefix+"BASELINE_PFBANTES";
         edtBaseline_PFBDepois_Internalname = sPrefix+"BASELINE_PFBDEPOIS";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         edtSistema_Codigo_Internalname = sPrefix+"SISTEMA_CODIGO";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavTfbaseline_datahomologacao_Internalname = sPrefix+"vTFBASELINE_DATAHOMOLOGACAO";
         edtavTfbaseline_datahomologacao_to_Internalname = sPrefix+"vTFBASELINE_DATAHOMOLOGACAO_TO";
         edtavDdo_baseline_datahomologacaoauxdate_Internalname = sPrefix+"vDDO_BASELINE_DATAHOMOLOGACAOAUXDATE";
         edtavDdo_baseline_datahomologacaoauxdateto_Internalname = sPrefix+"vDDO_BASELINE_DATAHOMOLOGACAOAUXDATETO";
         divDdo_baseline_datahomologacaoauxdates_Internalname = sPrefix+"DDO_BASELINE_DATAHOMOLOGACAOAUXDATES";
         edtavTfbaseline_pfbantes_Internalname = sPrefix+"vTFBASELINE_PFBANTES";
         edtavTfbaseline_pfbantes_to_Internalname = sPrefix+"vTFBASELINE_PFBANTES_TO";
         edtavTfbaseline_pfbdepois_Internalname = sPrefix+"vTFBASELINE_PFBDEPOIS";
         edtavTfbaseline_pfbdepois_to_Internalname = sPrefix+"vTFBASELINE_PFBDEPOIS_TO";
         Ddo_baseline_datahomologacao_Internalname = sPrefix+"DDO_BASELINE_DATAHOMOLOGACAO";
         edtavDdo_baseline_datahomologacaotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_BASELINE_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE";
         Ddo_baseline_pfbantes_Internalname = sPrefix+"DDO_BASELINE_PFBANTES";
         edtavDdo_baseline_pfbantestitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_BASELINE_PFBANTESTITLECONTROLIDTOREPLACE";
         Ddo_baseline_pfbdepois_Internalname = sPrefix+"DDO_BASELINE_PFBDEPOIS";
         edtavDdo_baseline_pfbdepoistitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_BASELINE_PFBDEPOISTITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtBaseline_PFBDepois_Jsonclick = "";
         edtBaseline_PFBAntes_Jsonclick = "";
         edtBaseline_DataHomologacao_Jsonclick = "";
         edtBaseline_Codigo_Jsonclick = "";
         edtavBaseline_datahomologacao_to_Jsonclick = "";
         edtavBaseline_datahomologacao_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtBaseline_PFBDepois_Titleformat = 0;
         edtBaseline_PFBAntes_Titleformat = 0;
         edtBaseline_DataHomologacao_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtBaseline_PFBDepois_Title = "PFB Depois";
         edtBaseline_PFBAntes_Title = "PFB Antes";
         edtBaseline_DataHomologacao_Title = "Homologa��o";
         edtavOrdereddsc_Visible = 1;
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_baseline_pfbdepoistitlecontrolidtoreplace_Visible = 1;
         edtavDdo_baseline_pfbantestitlecontrolidtoreplace_Visible = 1;
         edtavDdo_baseline_datahomologacaotitlecontrolidtoreplace_Visible = 1;
         edtavTfbaseline_pfbdepois_to_Jsonclick = "";
         edtavTfbaseline_pfbdepois_to_Visible = 1;
         edtavTfbaseline_pfbdepois_Jsonclick = "";
         edtavTfbaseline_pfbdepois_Visible = 1;
         edtavTfbaseline_pfbantes_to_Jsonclick = "";
         edtavTfbaseline_pfbantes_to_Visible = 1;
         edtavTfbaseline_pfbantes_Jsonclick = "";
         edtavTfbaseline_pfbantes_Visible = 1;
         edtavDdo_baseline_datahomologacaoauxdateto_Jsonclick = "";
         edtavDdo_baseline_datahomologacaoauxdate_Jsonclick = "";
         edtavTfbaseline_datahomologacao_to_Jsonclick = "";
         edtavTfbaseline_datahomologacao_to_Visible = 1;
         edtavTfbaseline_datahomologacao_Jsonclick = "";
         edtavTfbaseline_datahomologacao_Visible = 1;
         edtSistema_Codigo_Jsonclick = "";
         edtSistema_Codigo_Visible = 1;
         Ddo_baseline_pfbdepois_Searchbuttontext = "Pesquisar";
         Ddo_baseline_pfbdepois_Rangefilterto = "At�";
         Ddo_baseline_pfbdepois_Rangefilterfrom = "Desde";
         Ddo_baseline_pfbdepois_Cleanfilter = "Limpar pesquisa";
         Ddo_baseline_pfbdepois_Sortdsc = "Ordenar de Z � A";
         Ddo_baseline_pfbdepois_Sortasc = "Ordenar de A � Z";
         Ddo_baseline_pfbdepois_Includedatalist = Convert.ToBoolean( 0);
         Ddo_baseline_pfbdepois_Filterisrange = Convert.ToBoolean( -1);
         Ddo_baseline_pfbdepois_Filtertype = "Numeric";
         Ddo_baseline_pfbdepois_Includefilter = Convert.ToBoolean( -1);
         Ddo_baseline_pfbdepois_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_baseline_pfbdepois_Includesortasc = Convert.ToBoolean( -1);
         Ddo_baseline_pfbdepois_Titlecontrolidtoreplace = "";
         Ddo_baseline_pfbdepois_Dropdownoptionstype = "GridTitleSettings";
         Ddo_baseline_pfbdepois_Cls = "ColumnSettings";
         Ddo_baseline_pfbdepois_Tooltip = "Op��es";
         Ddo_baseline_pfbdepois_Caption = "";
         Ddo_baseline_pfbantes_Searchbuttontext = "Pesquisar";
         Ddo_baseline_pfbantes_Rangefilterto = "At�";
         Ddo_baseline_pfbantes_Rangefilterfrom = "Desde";
         Ddo_baseline_pfbantes_Cleanfilter = "Limpar pesquisa";
         Ddo_baseline_pfbantes_Sortdsc = "Ordenar de Z � A";
         Ddo_baseline_pfbantes_Sortasc = "Ordenar de A � Z";
         Ddo_baseline_pfbantes_Includedatalist = Convert.ToBoolean( 0);
         Ddo_baseline_pfbantes_Filterisrange = Convert.ToBoolean( -1);
         Ddo_baseline_pfbantes_Filtertype = "Numeric";
         Ddo_baseline_pfbantes_Includefilter = Convert.ToBoolean( -1);
         Ddo_baseline_pfbantes_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_baseline_pfbantes_Includesortasc = Convert.ToBoolean( -1);
         Ddo_baseline_pfbantes_Titlecontrolidtoreplace = "";
         Ddo_baseline_pfbantes_Dropdownoptionstype = "GridTitleSettings";
         Ddo_baseline_pfbantes_Cls = "ColumnSettings";
         Ddo_baseline_pfbantes_Tooltip = "Op��es";
         Ddo_baseline_pfbantes_Caption = "";
         Ddo_baseline_datahomologacao_Searchbuttontext = "Pesquisar";
         Ddo_baseline_datahomologacao_Rangefilterto = "At�";
         Ddo_baseline_datahomologacao_Rangefilterfrom = "Desde";
         Ddo_baseline_datahomologacao_Cleanfilter = "Limpar pesquisa";
         Ddo_baseline_datahomologacao_Sortdsc = "Ordenar de Z � A";
         Ddo_baseline_datahomologacao_Sortasc = "Ordenar de A � Z";
         Ddo_baseline_datahomologacao_Includedatalist = Convert.ToBoolean( 0);
         Ddo_baseline_datahomologacao_Filterisrange = Convert.ToBoolean( -1);
         Ddo_baseline_datahomologacao_Filtertype = "Date";
         Ddo_baseline_datahomologacao_Includefilter = Convert.ToBoolean( -1);
         Ddo_baseline_datahomologacao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_baseline_datahomologacao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_baseline_datahomologacao_Titlecontrolidtoreplace = "";
         Ddo_baseline_datahomologacao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_baseline_datahomologacao_Cls = "ColumnSettings";
         Ddo_baseline_datahomologacao_Tooltip = "Op��es";
         Ddo_baseline_datahomologacao_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV7Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV27ddo_Baseline_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_BASELINE_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_Baseline_PFBAntesTitleControlIdToReplace',fld:'vDDO_BASELINE_PFBANTESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_Baseline_PFBDepoisTitleControlIdToReplace',fld:'vDDO_BASELINE_PFBDEPOISTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18Baseline_DataHomologacao',fld:'vBASELINE_DATAHOMOLOGACAO',pic:'99/99/99 99:99',nv:''},{av:'AV19Baseline_DataHomologacao_To',fld:'vBASELINE_DATAHOMOLOGACAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV23TFBaseline_DataHomologacao',fld:'vTFBASELINE_DATAHOMOLOGACAO',pic:'99/99/99 99:99',nv:''},{av:'AV24TFBaseline_DataHomologacao_To',fld:'vTFBASELINE_DATAHOMOLOGACAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV29TFBaseline_PFBAntes',fld:'vTFBASELINE_PFBANTES',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV30TFBaseline_PFBAntes_To',fld:'vTFBASELINE_PFBANTES_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV33TFBaseline_PFBDepois',fld:'vTFBASELINE_PFBDEPOIS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV34TFBaseline_PFBDepois_To',fld:'vTFBASELINE_PFBDEPOIS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}],oparms:[{av:'AV22Baseline_DataHomologacaoTitleFilterData',fld:'vBASELINE_DATAHOMOLOGACAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV28Baseline_PFBAntesTitleFilterData',fld:'vBASELINE_PFBANTESTITLEFILTERDATA',pic:'',nv:null},{av:'AV32Baseline_PFBDepoisTitleFilterData',fld:'vBASELINE_PFBDEPOISTITLEFILTERDATA',pic:'',nv:null},{av:'edtBaseline_DataHomologacao_Titleformat',ctrl:'BASELINE_DATAHOMOLOGACAO',prop:'Titleformat'},{av:'edtBaseline_DataHomologacao_Title',ctrl:'BASELINE_DATAHOMOLOGACAO',prop:'Title'},{av:'edtBaseline_PFBAntes_Titleformat',ctrl:'BASELINE_PFBANTES',prop:'Titleformat'},{av:'edtBaseline_PFBAntes_Title',ctrl:'BASELINE_PFBANTES',prop:'Title'},{av:'edtBaseline_PFBDepois_Titleformat',ctrl:'BASELINE_PFBDEPOIS',prop:'Titleformat'},{av:'edtBaseline_PFBDepois_Title',ctrl:'BASELINE_PFBDEPOIS',prop:'Title'},{av:'AV38GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV39GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11ED2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18Baseline_DataHomologacao',fld:'vBASELINE_DATAHOMOLOGACAO',pic:'99/99/99 99:99',nv:''},{av:'AV19Baseline_DataHomologacao_To',fld:'vBASELINE_DATAHOMOLOGACAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV23TFBaseline_DataHomologacao',fld:'vTFBASELINE_DATAHOMOLOGACAO',pic:'99/99/99 99:99',nv:''},{av:'AV24TFBaseline_DataHomologacao_To',fld:'vTFBASELINE_DATAHOMOLOGACAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV29TFBaseline_PFBAntes',fld:'vTFBASELINE_PFBANTES',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV30TFBaseline_PFBAntes_To',fld:'vTFBASELINE_PFBANTES_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV33TFBaseline_PFBDepois',fld:'vTFBASELINE_PFBDEPOIS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV34TFBaseline_PFBDepois_To',fld:'vTFBASELINE_PFBDEPOIS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV7Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV27ddo_Baseline_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_BASELINE_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_Baseline_PFBAntesTitleControlIdToReplace',fld:'vDDO_BASELINE_PFBANTESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_Baseline_PFBDepoisTitleControlIdToReplace',fld:'vDDO_BASELINE_PFBDEPOISTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_BASELINE_DATAHOMOLOGACAO.ONOPTIONCLICKED","{handler:'E12ED2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18Baseline_DataHomologacao',fld:'vBASELINE_DATAHOMOLOGACAO',pic:'99/99/99 99:99',nv:''},{av:'AV19Baseline_DataHomologacao_To',fld:'vBASELINE_DATAHOMOLOGACAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV23TFBaseline_DataHomologacao',fld:'vTFBASELINE_DATAHOMOLOGACAO',pic:'99/99/99 99:99',nv:''},{av:'AV24TFBaseline_DataHomologacao_To',fld:'vTFBASELINE_DATAHOMOLOGACAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV29TFBaseline_PFBAntes',fld:'vTFBASELINE_PFBANTES',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV30TFBaseline_PFBAntes_To',fld:'vTFBASELINE_PFBANTES_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV33TFBaseline_PFBDepois',fld:'vTFBASELINE_PFBDEPOIS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV34TFBaseline_PFBDepois_To',fld:'vTFBASELINE_PFBDEPOIS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV7Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV27ddo_Baseline_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_BASELINE_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_Baseline_PFBAntesTitleControlIdToReplace',fld:'vDDO_BASELINE_PFBANTESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_Baseline_PFBDepoisTitleControlIdToReplace',fld:'vDDO_BASELINE_PFBDEPOISTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_baseline_datahomologacao_Activeeventkey',ctrl:'DDO_BASELINE_DATAHOMOLOGACAO',prop:'ActiveEventKey'},{av:'Ddo_baseline_datahomologacao_Filteredtext_get',ctrl:'DDO_BASELINE_DATAHOMOLOGACAO',prop:'FilteredText_get'},{av:'Ddo_baseline_datahomologacao_Filteredtextto_get',ctrl:'DDO_BASELINE_DATAHOMOLOGACAO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_baseline_datahomologacao_Sortedstatus',ctrl:'DDO_BASELINE_DATAHOMOLOGACAO',prop:'SortedStatus'},{av:'AV23TFBaseline_DataHomologacao',fld:'vTFBASELINE_DATAHOMOLOGACAO',pic:'99/99/99 99:99',nv:''},{av:'AV24TFBaseline_DataHomologacao_To',fld:'vTFBASELINE_DATAHOMOLOGACAO_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_baseline_pfbantes_Sortedstatus',ctrl:'DDO_BASELINE_PFBANTES',prop:'SortedStatus'},{av:'Ddo_baseline_pfbdepois_Sortedstatus',ctrl:'DDO_BASELINE_PFBDEPOIS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_BASELINE_PFBANTES.ONOPTIONCLICKED","{handler:'E13ED2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18Baseline_DataHomologacao',fld:'vBASELINE_DATAHOMOLOGACAO',pic:'99/99/99 99:99',nv:''},{av:'AV19Baseline_DataHomologacao_To',fld:'vBASELINE_DATAHOMOLOGACAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV23TFBaseline_DataHomologacao',fld:'vTFBASELINE_DATAHOMOLOGACAO',pic:'99/99/99 99:99',nv:''},{av:'AV24TFBaseline_DataHomologacao_To',fld:'vTFBASELINE_DATAHOMOLOGACAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV29TFBaseline_PFBAntes',fld:'vTFBASELINE_PFBANTES',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV30TFBaseline_PFBAntes_To',fld:'vTFBASELINE_PFBANTES_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV33TFBaseline_PFBDepois',fld:'vTFBASELINE_PFBDEPOIS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV34TFBaseline_PFBDepois_To',fld:'vTFBASELINE_PFBDEPOIS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV7Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV27ddo_Baseline_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_BASELINE_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_Baseline_PFBAntesTitleControlIdToReplace',fld:'vDDO_BASELINE_PFBANTESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_Baseline_PFBDepoisTitleControlIdToReplace',fld:'vDDO_BASELINE_PFBDEPOISTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_baseline_pfbantes_Activeeventkey',ctrl:'DDO_BASELINE_PFBANTES',prop:'ActiveEventKey'},{av:'Ddo_baseline_pfbantes_Filteredtext_get',ctrl:'DDO_BASELINE_PFBANTES',prop:'FilteredText_get'},{av:'Ddo_baseline_pfbantes_Filteredtextto_get',ctrl:'DDO_BASELINE_PFBANTES',prop:'FilteredTextTo_get'}],oparms:[{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_baseline_pfbantes_Sortedstatus',ctrl:'DDO_BASELINE_PFBANTES',prop:'SortedStatus'},{av:'AV29TFBaseline_PFBAntes',fld:'vTFBASELINE_PFBANTES',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV30TFBaseline_PFBAntes_To',fld:'vTFBASELINE_PFBANTES_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_baseline_datahomologacao_Sortedstatus',ctrl:'DDO_BASELINE_DATAHOMOLOGACAO',prop:'SortedStatus'},{av:'Ddo_baseline_pfbdepois_Sortedstatus',ctrl:'DDO_BASELINE_PFBDEPOIS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_BASELINE_PFBDEPOIS.ONOPTIONCLICKED","{handler:'E14ED2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18Baseline_DataHomologacao',fld:'vBASELINE_DATAHOMOLOGACAO',pic:'99/99/99 99:99',nv:''},{av:'AV19Baseline_DataHomologacao_To',fld:'vBASELINE_DATAHOMOLOGACAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV23TFBaseline_DataHomologacao',fld:'vTFBASELINE_DATAHOMOLOGACAO',pic:'99/99/99 99:99',nv:''},{av:'AV24TFBaseline_DataHomologacao_To',fld:'vTFBASELINE_DATAHOMOLOGACAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV29TFBaseline_PFBAntes',fld:'vTFBASELINE_PFBANTES',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV30TFBaseline_PFBAntes_To',fld:'vTFBASELINE_PFBANTES_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV33TFBaseline_PFBDepois',fld:'vTFBASELINE_PFBDEPOIS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV34TFBaseline_PFBDepois_To',fld:'vTFBASELINE_PFBDEPOIS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV7Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV27ddo_Baseline_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_BASELINE_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_Baseline_PFBAntesTitleControlIdToReplace',fld:'vDDO_BASELINE_PFBANTESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_Baseline_PFBDepoisTitleControlIdToReplace',fld:'vDDO_BASELINE_PFBDEPOISTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_baseline_pfbdepois_Activeeventkey',ctrl:'DDO_BASELINE_PFBDEPOIS',prop:'ActiveEventKey'},{av:'Ddo_baseline_pfbdepois_Filteredtext_get',ctrl:'DDO_BASELINE_PFBDEPOIS',prop:'FilteredText_get'},{av:'Ddo_baseline_pfbdepois_Filteredtextto_get',ctrl:'DDO_BASELINE_PFBDEPOIS',prop:'FilteredTextTo_get'}],oparms:[{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_baseline_pfbdepois_Sortedstatus',ctrl:'DDO_BASELINE_PFBDEPOIS',prop:'SortedStatus'},{av:'AV33TFBaseline_PFBDepois',fld:'vTFBASELINE_PFBDEPOIS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV34TFBaseline_PFBDepois_To',fld:'vTFBASELINE_PFBDEPOIS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_baseline_datahomologacao_Sortedstatus',ctrl:'DDO_BASELINE_DATAHOMOLOGACAO',prop:'SortedStatus'},{av:'Ddo_baseline_pfbantes_Sortedstatus',ctrl:'DDO_BASELINE_PFBANTES',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E19ED2',iparms:[],oparms:[]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E15ED2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18Baseline_DataHomologacao',fld:'vBASELINE_DATAHOMOLOGACAO',pic:'99/99/99 99:99',nv:''},{av:'AV19Baseline_DataHomologacao_To',fld:'vBASELINE_DATAHOMOLOGACAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV23TFBaseline_DataHomologacao',fld:'vTFBASELINE_DATAHOMOLOGACAO',pic:'99/99/99 99:99',nv:''},{av:'AV24TFBaseline_DataHomologacao_To',fld:'vTFBASELINE_DATAHOMOLOGACAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV29TFBaseline_PFBAntes',fld:'vTFBASELINE_PFBANTES',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV30TFBaseline_PFBAntes_To',fld:'vTFBASELINE_PFBANTES_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV33TFBaseline_PFBDepois',fld:'vTFBASELINE_PFBDEPOIS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV34TFBaseline_PFBDepois_To',fld:'vTFBASELINE_PFBDEPOIS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV7Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV27ddo_Baseline_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_BASELINE_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_Baseline_PFBAntesTitleControlIdToReplace',fld:'vDDO_BASELINE_PFBANTESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_Baseline_PFBDepoisTitleControlIdToReplace',fld:'vDDO_BASELINE_PFBDEPOISTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E16ED2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18Baseline_DataHomologacao',fld:'vBASELINE_DATAHOMOLOGACAO',pic:'99/99/99 99:99',nv:''},{av:'AV19Baseline_DataHomologacao_To',fld:'vBASELINE_DATAHOMOLOGACAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV23TFBaseline_DataHomologacao',fld:'vTFBASELINE_DATAHOMOLOGACAO',pic:'99/99/99 99:99',nv:''},{av:'AV24TFBaseline_DataHomologacao_To',fld:'vTFBASELINE_DATAHOMOLOGACAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV29TFBaseline_PFBAntes',fld:'vTFBASELINE_PFBANTES',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV30TFBaseline_PFBAntes_To',fld:'vTFBASELINE_PFBANTES_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV33TFBaseline_PFBDepois',fld:'vTFBASELINE_PFBDEPOIS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV34TFBaseline_PFBDepois_To',fld:'vTFBASELINE_PFBDEPOIS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV7Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV27ddo_Baseline_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_BASELINE_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_Baseline_PFBAntesTitleControlIdToReplace',fld:'vDDO_BASELINE_PFBANTESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_Baseline_PFBDepoisTitleControlIdToReplace',fld:'vDDO_BASELINE_PFBDEPOISTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''}],oparms:[{av:'AV18Baseline_DataHomologacao',fld:'vBASELINE_DATAHOMOLOGACAO',pic:'99/99/99 99:99',nv:''},{av:'AV19Baseline_DataHomologacao_To',fld:'vBASELINE_DATAHOMOLOGACAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV23TFBaseline_DataHomologacao',fld:'vTFBASELINE_DATAHOMOLOGACAO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_baseline_datahomologacao_Filteredtext_set',ctrl:'DDO_BASELINE_DATAHOMOLOGACAO',prop:'FilteredText_set'},{av:'AV24TFBaseline_DataHomologacao_To',fld:'vTFBASELINE_DATAHOMOLOGACAO_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_baseline_datahomologacao_Filteredtextto_set',ctrl:'DDO_BASELINE_DATAHOMOLOGACAO',prop:'FilteredTextTo_set'},{av:'AV29TFBaseline_PFBAntes',fld:'vTFBASELINE_PFBANTES',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_baseline_pfbantes_Filteredtext_set',ctrl:'DDO_BASELINE_PFBANTES',prop:'FilteredText_set'},{av:'AV30TFBaseline_PFBAntes_To',fld:'vTFBASELINE_PFBANTES_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_baseline_pfbantes_Filteredtextto_set',ctrl:'DDO_BASELINE_PFBANTES',prop:'FilteredTextTo_set'},{av:'AV33TFBaseline_PFBDepois',fld:'vTFBASELINE_PFBDEPOIS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_baseline_pfbdepois_Filteredtext_set',ctrl:'DDO_BASELINE_PFBDEPOIS',prop:'FilteredText_set'},{av:'AV34TFBaseline_PFBDepois_To',fld:'vTFBASELINE_PFBDEPOIS_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_baseline_pfbdepois_Filteredtextto_set',ctrl:'DDO_BASELINE_PFBDEPOIS',prop:'FilteredTextTo_set'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_baseline_datahomologacao_Activeeventkey = "";
         Ddo_baseline_datahomologacao_Filteredtext_get = "";
         Ddo_baseline_datahomologacao_Filteredtextto_get = "";
         Ddo_baseline_pfbantes_Activeeventkey = "";
         Ddo_baseline_pfbantes_Filteredtext_get = "";
         Ddo_baseline_pfbantes_Filteredtextto_get = "";
         Ddo_baseline_pfbdepois_Activeeventkey = "";
         Ddo_baseline_pfbdepois_Filteredtext_get = "";
         Ddo_baseline_pfbdepois_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV18Baseline_DataHomologacao = (DateTime)(DateTime.MinValue);
         AV19Baseline_DataHomologacao_To = (DateTime)(DateTime.MinValue);
         AV23TFBaseline_DataHomologacao = (DateTime)(DateTime.MinValue);
         AV24TFBaseline_DataHomologacao_To = (DateTime)(DateTime.MinValue);
         AV27ddo_Baseline_DataHomologacaoTitleControlIdToReplace = "";
         AV31ddo_Baseline_PFBAntesTitleControlIdToReplace = "";
         AV35ddo_Baseline_PFBDepoisTitleControlIdToReplace = "";
         AV42Pgmname = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV36DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV22Baseline_DataHomologacaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV28Baseline_PFBAntesTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV32Baseline_PFBDepoisTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_baseline_datahomologacao_Filteredtext_set = "";
         Ddo_baseline_datahomologacao_Filteredtextto_set = "";
         Ddo_baseline_datahomologacao_Sortedstatus = "";
         Ddo_baseline_pfbantes_Filteredtext_set = "";
         Ddo_baseline_pfbantes_Filteredtextto_set = "";
         Ddo_baseline_pfbantes_Sortedstatus = "";
         Ddo_baseline_pfbdepois_Filteredtext_set = "";
         Ddo_baseline_pfbdepois_Filteredtextto_set = "";
         Ddo_baseline_pfbdepois_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         AV25DDO_Baseline_DataHomologacaoAuxDate = DateTime.MinValue;
         AV26DDO_Baseline_DataHomologacaoAuxDateTo = DateTime.MinValue;
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         A1164Baseline_DataHomologacao = (DateTime)(DateTime.MinValue);
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         H00ED2_A735Baseline_ProjetoMelCod = new int[1] ;
         H00ED2_n735Baseline_ProjetoMelCod = new bool[] {false} ;
         H00ED2_A698ProjetoMelhoria_FnAPFCod = new int[1] ;
         H00ED2_n698ProjetoMelhoria_FnAPFCod = new bool[] {false} ;
         H00ED2_A359FuncaoAPF_ModuloCod = new int[1] ;
         H00ED2_n359FuncaoAPF_ModuloCod = new bool[] {false} ;
         H00ED2_A127Sistema_Codigo = new int[1] ;
         H00ED2_n127Sistema_Codigo = new bool[] {false} ;
         H00ED2_A727Baseline_PFBDepois = new decimal[1] ;
         H00ED2_n727Baseline_PFBDepois = new bool[] {false} ;
         H00ED2_A726Baseline_PFBAntes = new decimal[1] ;
         H00ED2_n726Baseline_PFBAntes = new bool[] {false} ;
         H00ED2_A1164Baseline_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         H00ED2_A722Baseline_Codigo = new int[1] ;
         H00ED3_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GridRow = new GXWebRow();
         AV14Session = context.GetSession();
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         lblFiltertextbaseline_datahomologacao_Jsonclick = "";
         lblBaseline_datahomologacao_rangemiddletext_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7Sistema_Codigo = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.sistemabaseline__default(),
            new Object[][] {
                new Object[] {
               H00ED2_A735Baseline_ProjetoMelCod, H00ED2_n735Baseline_ProjetoMelCod, H00ED2_A698ProjetoMelhoria_FnAPFCod, H00ED2_n698ProjetoMelhoria_FnAPFCod, H00ED2_A359FuncaoAPF_ModuloCod, H00ED2_n359FuncaoAPF_ModuloCod, H00ED2_A127Sistema_Codigo, H00ED2_n127Sistema_Codigo, H00ED2_A727Baseline_PFBDepois, H00ED2_n727Baseline_PFBDepois,
               H00ED2_A726Baseline_PFBAntes, H00ED2_n726Baseline_PFBAntes, H00ED2_A1164Baseline_DataHomologacao, H00ED2_A722Baseline_Codigo
               }
               , new Object[] {
               H00ED3_AGRID_nRecordCount
               }
            }
         );
         AV42Pgmname = "SistemaBaseline";
         /* GeneXus formulas. */
         AV42Pgmname = "SistemaBaseline";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_37 ;
      private short nGXsfl_37_idx=1 ;
      private short AV15OrderedBy ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_37_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtBaseline_DataHomologacao_Titleformat ;
      private short edtBaseline_PFBAntes_Titleformat ;
      private short edtBaseline_PFBDepois_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7Sistema_Codigo ;
      private int wcpOAV7Sistema_Codigo ;
      private int subGrid_Rows ;
      private int Gridpaginationbar_Pagestoshow ;
      private int A127Sistema_Codigo ;
      private int edtSistema_Codigo_Visible ;
      private int edtavTfbaseline_datahomologacao_Visible ;
      private int edtavTfbaseline_datahomologacao_to_Visible ;
      private int edtavTfbaseline_pfbantes_Visible ;
      private int edtavTfbaseline_pfbantes_to_Visible ;
      private int edtavTfbaseline_pfbdepois_Visible ;
      private int edtavTfbaseline_pfbdepois_to_Visible ;
      private int edtavDdo_baseline_datahomologacaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_baseline_pfbantestitlecontrolidtoreplace_Visible ;
      private int edtavDdo_baseline_pfbdepoistitlecontrolidtoreplace_Visible ;
      private int A722Baseline_Codigo ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int A735Baseline_ProjetoMelCod ;
      private int A698ProjetoMelhoria_FnAPFCod ;
      private int A359FuncaoAPF_ModuloCod ;
      private int edtavOrdereddsc_Visible ;
      private int AV37PageToGo ;
      private int AV43GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV38GridCurrentPage ;
      private long AV39GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV29TFBaseline_PFBAntes ;
      private decimal AV30TFBaseline_PFBAntes_To ;
      private decimal AV33TFBaseline_PFBDepois ;
      private decimal AV34TFBaseline_PFBDepois_To ;
      private decimal A726Baseline_PFBAntes ;
      private decimal A727Baseline_PFBDepois ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_baseline_datahomologacao_Activeeventkey ;
      private String Ddo_baseline_datahomologacao_Filteredtext_get ;
      private String Ddo_baseline_datahomologacao_Filteredtextto_get ;
      private String Ddo_baseline_pfbantes_Activeeventkey ;
      private String Ddo_baseline_pfbantes_Filteredtext_get ;
      private String Ddo_baseline_pfbantes_Filteredtextto_get ;
      private String Ddo_baseline_pfbdepois_Activeeventkey ;
      private String Ddo_baseline_pfbdepois_Filteredtext_get ;
      private String Ddo_baseline_pfbdepois_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_37_idx="0001" ;
      private String AV42Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_baseline_datahomologacao_Caption ;
      private String Ddo_baseline_datahomologacao_Tooltip ;
      private String Ddo_baseline_datahomologacao_Cls ;
      private String Ddo_baseline_datahomologacao_Filteredtext_set ;
      private String Ddo_baseline_datahomologacao_Filteredtextto_set ;
      private String Ddo_baseline_datahomologacao_Dropdownoptionstype ;
      private String Ddo_baseline_datahomologacao_Titlecontrolidtoreplace ;
      private String Ddo_baseline_datahomologacao_Sortedstatus ;
      private String Ddo_baseline_datahomologacao_Filtertype ;
      private String Ddo_baseline_datahomologacao_Sortasc ;
      private String Ddo_baseline_datahomologacao_Sortdsc ;
      private String Ddo_baseline_datahomologacao_Cleanfilter ;
      private String Ddo_baseline_datahomologacao_Rangefilterfrom ;
      private String Ddo_baseline_datahomologacao_Rangefilterto ;
      private String Ddo_baseline_datahomologacao_Searchbuttontext ;
      private String Ddo_baseline_pfbantes_Caption ;
      private String Ddo_baseline_pfbantes_Tooltip ;
      private String Ddo_baseline_pfbantes_Cls ;
      private String Ddo_baseline_pfbantes_Filteredtext_set ;
      private String Ddo_baseline_pfbantes_Filteredtextto_set ;
      private String Ddo_baseline_pfbantes_Dropdownoptionstype ;
      private String Ddo_baseline_pfbantes_Titlecontrolidtoreplace ;
      private String Ddo_baseline_pfbantes_Sortedstatus ;
      private String Ddo_baseline_pfbantes_Filtertype ;
      private String Ddo_baseline_pfbantes_Sortasc ;
      private String Ddo_baseline_pfbantes_Sortdsc ;
      private String Ddo_baseline_pfbantes_Cleanfilter ;
      private String Ddo_baseline_pfbantes_Rangefilterfrom ;
      private String Ddo_baseline_pfbantes_Rangefilterto ;
      private String Ddo_baseline_pfbantes_Searchbuttontext ;
      private String Ddo_baseline_pfbdepois_Caption ;
      private String Ddo_baseline_pfbdepois_Tooltip ;
      private String Ddo_baseline_pfbdepois_Cls ;
      private String Ddo_baseline_pfbdepois_Filteredtext_set ;
      private String Ddo_baseline_pfbdepois_Filteredtextto_set ;
      private String Ddo_baseline_pfbdepois_Dropdownoptionstype ;
      private String Ddo_baseline_pfbdepois_Titlecontrolidtoreplace ;
      private String Ddo_baseline_pfbdepois_Sortedstatus ;
      private String Ddo_baseline_pfbdepois_Filtertype ;
      private String Ddo_baseline_pfbdepois_Sortasc ;
      private String Ddo_baseline_pfbdepois_Sortdsc ;
      private String Ddo_baseline_pfbdepois_Cleanfilter ;
      private String Ddo_baseline_pfbdepois_Rangefilterfrom ;
      private String Ddo_baseline_pfbdepois_Rangefilterto ;
      private String Ddo_baseline_pfbdepois_Searchbuttontext ;
      private String GX_FocusControl ;
      private String edtSistema_Codigo_Internalname ;
      private String edtSistema_Codigo_Jsonclick ;
      private String TempTags ;
      private String edtavTfbaseline_datahomologacao_Internalname ;
      private String edtavTfbaseline_datahomologacao_Jsonclick ;
      private String edtavTfbaseline_datahomologacao_to_Internalname ;
      private String edtavTfbaseline_datahomologacao_to_Jsonclick ;
      private String divDdo_baseline_datahomologacaoauxdates_Internalname ;
      private String edtavDdo_baseline_datahomologacaoauxdate_Internalname ;
      private String edtavDdo_baseline_datahomologacaoauxdate_Jsonclick ;
      private String edtavDdo_baseline_datahomologacaoauxdateto_Internalname ;
      private String edtavDdo_baseline_datahomologacaoauxdateto_Jsonclick ;
      private String edtavTfbaseline_pfbantes_Internalname ;
      private String edtavTfbaseline_pfbantes_Jsonclick ;
      private String edtavTfbaseline_pfbantes_to_Internalname ;
      private String edtavTfbaseline_pfbantes_to_Jsonclick ;
      private String edtavTfbaseline_pfbdepois_Internalname ;
      private String edtavTfbaseline_pfbdepois_Jsonclick ;
      private String edtavTfbaseline_pfbdepois_to_Internalname ;
      private String edtavTfbaseline_pfbdepois_to_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_baseline_datahomologacaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_baseline_pfbantestitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_baseline_pfbdepoistitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String cmbavOrderedby_Internalname ;
      private String edtBaseline_Codigo_Internalname ;
      private String edtBaseline_DataHomologacao_Internalname ;
      private String edtBaseline_PFBAntes_Internalname ;
      private String edtBaseline_PFBDepois_Internalname ;
      private String scmdbuf ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavBaseline_datahomologacao_Internalname ;
      private String edtavBaseline_datahomologacao_to_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_baseline_datahomologacao_Internalname ;
      private String Ddo_baseline_pfbantes_Internalname ;
      private String Ddo_baseline_pfbdepois_Internalname ;
      private String edtBaseline_DataHomologacao_Title ;
      private String edtBaseline_PFBAntes_Title ;
      private String edtBaseline_PFBDepois_Title ;
      private String sStyleString ;
      private String tblUnnamedtable1_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String imgCleanfilters_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String lblFiltertextbaseline_datahomologacao_Internalname ;
      private String lblFiltertextbaseline_datahomologacao_Jsonclick ;
      private String tblTablemergedbaseline_datahomologacao_Internalname ;
      private String edtavBaseline_datahomologacao_Jsonclick ;
      private String lblBaseline_datahomologacao_rangemiddletext_Internalname ;
      private String lblBaseline_datahomologacao_rangemiddletext_Jsonclick ;
      private String edtavBaseline_datahomologacao_to_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String sCtrlAV7Sistema_Codigo ;
      private String sGXsfl_37_fel_idx="0001" ;
      private String ROClassString ;
      private String edtBaseline_Codigo_Jsonclick ;
      private String edtBaseline_DataHomologacao_Jsonclick ;
      private String edtBaseline_PFBAntes_Jsonclick ;
      private String edtBaseline_PFBDepois_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV18Baseline_DataHomologacao ;
      private DateTime AV19Baseline_DataHomologacao_To ;
      private DateTime AV23TFBaseline_DataHomologacao ;
      private DateTime AV24TFBaseline_DataHomologacao_To ;
      private DateTime A1164Baseline_DataHomologacao ;
      private DateTime AV25DDO_Baseline_DataHomologacaoAuxDate ;
      private DateTime AV26DDO_Baseline_DataHomologacaoAuxDateTo ;
      private bool entryPointCalled ;
      private bool AV13OrderedDsc ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_baseline_datahomologacao_Includesortasc ;
      private bool Ddo_baseline_datahomologacao_Includesortdsc ;
      private bool Ddo_baseline_datahomologacao_Includefilter ;
      private bool Ddo_baseline_datahomologacao_Filterisrange ;
      private bool Ddo_baseline_datahomologacao_Includedatalist ;
      private bool Ddo_baseline_pfbantes_Includesortasc ;
      private bool Ddo_baseline_pfbantes_Includesortdsc ;
      private bool Ddo_baseline_pfbantes_Includefilter ;
      private bool Ddo_baseline_pfbantes_Filterisrange ;
      private bool Ddo_baseline_pfbantes_Includedatalist ;
      private bool Ddo_baseline_pfbdepois_Includesortasc ;
      private bool Ddo_baseline_pfbdepois_Includesortdsc ;
      private bool Ddo_baseline_pfbdepois_Includefilter ;
      private bool Ddo_baseline_pfbdepois_Filterisrange ;
      private bool Ddo_baseline_pfbdepois_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n726Baseline_PFBAntes ;
      private bool n727Baseline_PFBDepois ;
      private bool n735Baseline_ProjetoMelCod ;
      private bool n698ProjetoMelhoria_FnAPFCod ;
      private bool n359FuncaoAPF_ModuloCod ;
      private bool n127Sistema_Codigo ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private String AV27ddo_Baseline_DataHomologacaoTitleControlIdToReplace ;
      private String AV31ddo_Baseline_PFBAntesTitleControlIdToReplace ;
      private String AV35ddo_Baseline_PFBDepoisTitleControlIdToReplace ;
      private IGxSession AV14Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private IDataStoreProvider pr_default ;
      private int[] H00ED2_A735Baseline_ProjetoMelCod ;
      private bool[] H00ED2_n735Baseline_ProjetoMelCod ;
      private int[] H00ED2_A698ProjetoMelhoria_FnAPFCod ;
      private bool[] H00ED2_n698ProjetoMelhoria_FnAPFCod ;
      private int[] H00ED2_A359FuncaoAPF_ModuloCod ;
      private bool[] H00ED2_n359FuncaoAPF_ModuloCod ;
      private int[] H00ED2_A127Sistema_Codigo ;
      private bool[] H00ED2_n127Sistema_Codigo ;
      private decimal[] H00ED2_A727Baseline_PFBDepois ;
      private bool[] H00ED2_n727Baseline_PFBDepois ;
      private decimal[] H00ED2_A726Baseline_PFBAntes ;
      private bool[] H00ED2_n726Baseline_PFBAntes ;
      private DateTime[] H00ED2_A1164Baseline_DataHomologacao ;
      private int[] H00ED2_A722Baseline_Codigo ;
      private long[] H00ED3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV22Baseline_DataHomologacaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV28Baseline_PFBAntesTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV32Baseline_PFBDepoisTitleFilterData ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV36DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class sistemabaseline__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00ED2( IGxContext context ,
                                             DateTime AV18Baseline_DataHomologacao ,
                                             DateTime AV19Baseline_DataHomologacao_To ,
                                             DateTime AV23TFBaseline_DataHomologacao ,
                                             DateTime AV24TFBaseline_DataHomologacao_To ,
                                             decimal AV29TFBaseline_PFBAntes ,
                                             decimal AV30TFBaseline_PFBAntes_To ,
                                             decimal AV33TFBaseline_PFBDepois ,
                                             decimal AV34TFBaseline_PFBDepois_To ,
                                             DateTime A1164Baseline_DataHomologacao ,
                                             decimal A726Baseline_PFBAntes ,
                                             decimal A727Baseline_PFBDepois ,
                                             short AV15OrderedBy ,
                                             bool AV13OrderedDsc ,
                                             int A127Sistema_Codigo ,
                                             int AV7Sistema_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [14] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[Baseline_ProjetoMelCod] AS Baseline_ProjetoMelCod, T2.[ProjetoMelhoria_FnAPFCod] AS ProjetoMelhoria_FnAPFCod, T3.[FuncaoAPF_ModuloCod] AS FuncaoAPF_ModuloCod, T4.[Sistema_Codigo], T1.[Baseline_PFBDepois], T1.[Baseline_PFBAntes], T1.[Baseline_DataHomologacao], T1.[Baseline_Codigo]";
         sFromString = " FROM ((([Baseline] T1 WITH (NOLOCK) LEFT JOIN [ProjetoMelhoria] T2 WITH (NOLOCK) ON T2.[ProjetoMelhoria_Codigo] = T1.[Baseline_ProjetoMelCod]) LEFT JOIN [FuncoesAPF] T3 WITH (NOLOCK) ON T3.[FuncaoAPF_Codigo] = T2.[ProjetoMelhoria_FnAPFCod]) LEFT JOIN [Modulo] T4 WITH (NOLOCK) ON T4.[Modulo_Codigo] = T3.[FuncaoAPF_ModuloCod])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T4.[Sistema_Codigo] = @AV7Sistema_Codigo)";
         if ( ! (DateTime.MinValue==AV18Baseline_DataHomologacao) )
         {
            sWhereString = sWhereString + " and (T1.[Baseline_DataHomologacao] >= @AV18Baseline_DataHomologacao)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ! (DateTime.MinValue==AV19Baseline_DataHomologacao_To) )
         {
            sWhereString = sWhereString + " and (T1.[Baseline_DataHomologacao] <= @AV19Baseline_DataHomologacao_To)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! (DateTime.MinValue==AV23TFBaseline_DataHomologacao) )
         {
            sWhereString = sWhereString + " and (T1.[Baseline_DataHomologacao] >= @AV23TFBaseline_DataHomologacao)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! (DateTime.MinValue==AV24TFBaseline_DataHomologacao_To) )
         {
            sWhereString = sWhereString + " and (T1.[Baseline_DataHomologacao] <= @AV24TFBaseline_DataHomologacao_To)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV29TFBaseline_PFBAntes) )
         {
            sWhereString = sWhereString + " and (T1.[Baseline_PFBAntes] >= @AV29TFBaseline_PFBAntes)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV30TFBaseline_PFBAntes_To) )
         {
            sWhereString = sWhereString + " and (T1.[Baseline_PFBAntes] <= @AV30TFBaseline_PFBAntes_To)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV33TFBaseline_PFBDepois) )
         {
            sWhereString = sWhereString + " and (T1.[Baseline_PFBDepois] >= @AV33TFBaseline_PFBDepois)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV34TFBaseline_PFBDepois_To) )
         {
            sWhereString = sWhereString + " and (T1.[Baseline_PFBDepois] <= @AV34TFBaseline_PFBDepois_To)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( AV15OrderedBy == 1 )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Baseline_Codigo]";
         }
         else if ( ( AV15OrderedBy == 2 ) && ! AV13OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Sistema_Codigo], T1.[Baseline_DataHomologacao]";
         }
         else if ( ( AV15OrderedBy == 2 ) && ( AV13OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Sistema_Codigo] DESC, T1.[Baseline_DataHomologacao] DESC";
         }
         else if ( ( AV15OrderedBy == 3 ) && ! AV13OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Sistema_Codigo], T1.[Baseline_PFBAntes]";
         }
         else if ( ( AV15OrderedBy == 3 ) && ( AV13OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Sistema_Codigo] DESC, T1.[Baseline_PFBAntes] DESC";
         }
         else if ( ( AV15OrderedBy == 4 ) && ! AV13OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Sistema_Codigo], T1.[Baseline_PFBDepois]";
         }
         else if ( ( AV15OrderedBy == 4 ) && ( AV13OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Sistema_Codigo] DESC, T1.[Baseline_PFBDepois] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Baseline_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00ED3( IGxContext context ,
                                             DateTime AV18Baseline_DataHomologacao ,
                                             DateTime AV19Baseline_DataHomologacao_To ,
                                             DateTime AV23TFBaseline_DataHomologacao ,
                                             DateTime AV24TFBaseline_DataHomologacao_To ,
                                             decimal AV29TFBaseline_PFBAntes ,
                                             decimal AV30TFBaseline_PFBAntes_To ,
                                             decimal AV33TFBaseline_PFBDepois ,
                                             decimal AV34TFBaseline_PFBDepois_To ,
                                             DateTime A1164Baseline_DataHomologacao ,
                                             decimal A726Baseline_PFBAntes ,
                                             decimal A727Baseline_PFBDepois ,
                                             short AV15OrderedBy ,
                                             bool AV13OrderedDsc ,
                                             int A127Sistema_Codigo ,
                                             int AV7Sistema_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [9] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ((([Baseline] T1 WITH (NOLOCK) LEFT JOIN [ProjetoMelhoria] T2 WITH (NOLOCK) ON T2.[ProjetoMelhoria_Codigo] = T1.[Baseline_ProjetoMelCod]) LEFT JOIN [FuncoesAPF] T3 WITH (NOLOCK) ON T3.[FuncaoAPF_Codigo] = T2.[ProjetoMelhoria_FnAPFCod]) LEFT JOIN [Modulo] T4 WITH (NOLOCK) ON T4.[Modulo_Codigo] = T3.[FuncaoAPF_ModuloCod])";
         scmdbuf = scmdbuf + " WHERE (T4.[Sistema_Codigo] = @AV7Sistema_Codigo)";
         if ( ! (DateTime.MinValue==AV18Baseline_DataHomologacao) )
         {
            sWhereString = sWhereString + " and (T1.[Baseline_DataHomologacao] >= @AV18Baseline_DataHomologacao)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ! (DateTime.MinValue==AV19Baseline_DataHomologacao_To) )
         {
            sWhereString = sWhereString + " and (T1.[Baseline_DataHomologacao] <= @AV19Baseline_DataHomologacao_To)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ! (DateTime.MinValue==AV23TFBaseline_DataHomologacao) )
         {
            sWhereString = sWhereString + " and (T1.[Baseline_DataHomologacao] >= @AV23TFBaseline_DataHomologacao)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! (DateTime.MinValue==AV24TFBaseline_DataHomologacao_To) )
         {
            sWhereString = sWhereString + " and (T1.[Baseline_DataHomologacao] <= @AV24TFBaseline_DataHomologacao_To)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV29TFBaseline_PFBAntes) )
         {
            sWhereString = sWhereString + " and (T1.[Baseline_PFBAntes] >= @AV29TFBaseline_PFBAntes)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV30TFBaseline_PFBAntes_To) )
         {
            sWhereString = sWhereString + " and (T1.[Baseline_PFBAntes] <= @AV30TFBaseline_PFBAntes_To)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV33TFBaseline_PFBDepois) )
         {
            sWhereString = sWhereString + " and (T1.[Baseline_PFBDepois] >= @AV33TFBaseline_PFBDepois)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV34TFBaseline_PFBDepois_To) )
         {
            sWhereString = sWhereString + " and (T1.[Baseline_PFBDepois] <= @AV34TFBaseline_PFBDepois_To)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( AV15OrderedBy == 1 )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV15OrderedBy == 2 ) && ! AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV15OrderedBy == 2 ) && ( AV13OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV15OrderedBy == 3 ) && ! AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV15OrderedBy == 3 ) && ( AV13OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV15OrderedBy == 4 ) && ! AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV15OrderedBy == 4 ) && ( AV13OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00ED2(context, (DateTime)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (decimal)dynConstraints[4] , (decimal)dynConstraints[5] , (decimal)dynConstraints[6] , (decimal)dynConstraints[7] , (DateTime)dynConstraints[8] , (decimal)dynConstraints[9] , (decimal)dynConstraints[10] , (short)dynConstraints[11] , (bool)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] );
               case 1 :
                     return conditional_H00ED3(context, (DateTime)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (decimal)dynConstraints[4] , (decimal)dynConstraints[5] , (decimal)dynConstraints[6] , (decimal)dynConstraints[7] , (DateTime)dynConstraints[8] , (decimal)dynConstraints[9] , (decimal)dynConstraints[10] , (short)dynConstraints[11] , (bool)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00ED2 ;
          prmH00ED2 = new Object[] {
          new Object[] {"@AV7Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18Baseline_DataHomologacao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV19Baseline_DataHomologacao_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV23TFBaseline_DataHomologacao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV24TFBaseline_DataHomologacao_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV29TFBaseline_PFBAntes",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV30TFBaseline_PFBAntes_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV33TFBaseline_PFBDepois",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV34TFBaseline_PFBDepois_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00ED3 ;
          prmH00ED3 = new Object[] {
          new Object[] {"@AV7Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18Baseline_DataHomologacao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV19Baseline_DataHomologacao_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV23TFBaseline_DataHomologacao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV24TFBaseline_DataHomologacao_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV29TFBaseline_PFBAntes",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV30TFBaseline_PFBAntes_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV33TFBaseline_PFBDepois",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV34TFBaseline_PFBDepois_To",SqlDbType.Decimal,14,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00ED2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00ED2,11,0,true,false )
             ,new CursorDef("H00ED3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00ED3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[12])[0] = rslt.getGXDateTime(7) ;
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[15]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[16]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[17]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[18]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[19]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[20]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[21]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[22]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[10]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[11]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[12]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[13]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[14]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[15]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[16]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[17]);
                }
                return;
       }
    }

 }

}
