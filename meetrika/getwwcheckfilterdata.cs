/*
               File: GetWWCheckFilterData
        Description: Get WWCheck Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:12:13.19
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwcheckfilterdata : GXProcedure
   {
      public getwwcheckfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwcheckfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV19DDOName = aP0_DDOName;
         this.AV17SearchTxt = aP1_SearchTxt;
         this.AV18SearchTxtTo = aP2_SearchTxtTo;
         this.AV23OptionsJson = "" ;
         this.AV26OptionsDescJson = "" ;
         this.AV28OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV23OptionsJson;
         aP4_OptionsDescJson=this.AV26OptionsDescJson;
         aP5_OptionIndexesJson=this.AV28OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV19DDOName = aP0_DDOName;
         this.AV17SearchTxt = aP1_SearchTxt;
         this.AV18SearchTxtTo = aP2_SearchTxtTo;
         this.AV23OptionsJson = "" ;
         this.AV26OptionsDescJson = "" ;
         this.AV28OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV23OptionsJson;
         aP4_OptionsDescJson=this.AV26OptionsDescJson;
         aP5_OptionIndexesJson=this.AV28OptionIndexesJson;
         return AV28OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwcheckfilterdata objgetwwcheckfilterdata;
         objgetwwcheckfilterdata = new getwwcheckfilterdata();
         objgetwwcheckfilterdata.AV19DDOName = aP0_DDOName;
         objgetwwcheckfilterdata.AV17SearchTxt = aP1_SearchTxt;
         objgetwwcheckfilterdata.AV18SearchTxtTo = aP2_SearchTxtTo;
         objgetwwcheckfilterdata.AV23OptionsJson = "" ;
         objgetwwcheckfilterdata.AV26OptionsDescJson = "" ;
         objgetwwcheckfilterdata.AV28OptionIndexesJson = "" ;
         objgetwwcheckfilterdata.context.SetSubmitInitialConfig(context);
         objgetwwcheckfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwcheckfilterdata);
         aP3_OptionsJson=this.AV23OptionsJson;
         aP4_OptionsDescJson=this.AV26OptionsDescJson;
         aP5_OptionIndexesJson=this.AV28OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwcheckfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV22Options = (IGxCollection)(new GxSimpleCollection());
         AV25OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV27OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV19DDOName), "DDO_CHECK_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADCHECK_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV23OptionsJson = AV22Options.ToJSonString(false);
         AV26OptionsDescJson = AV25OptionsDesc.ToJSonString(false);
         AV28OptionIndexesJson = AV27OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV30Session.Get("WWCheckGridState"), "") == 0 )
         {
            AV32GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWCheckGridState"), "");
         }
         else
         {
            AV32GridState.FromXml(AV30Session.Get("WWCheckGridState"), "");
         }
         AV51GXV1 = 1;
         while ( AV51GXV1 <= AV32GridState.gxTpr_Filtervalues.Count )
         {
            AV33GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV32GridState.gxTpr_Filtervalues.Item(AV51GXV1));
            if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "CHECK_AREATRABALHOCOD") == 0 )
            {
               AV35Check_AreaTrabalhoCod = (int)(NumberUtil.Val( AV33GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFCHECK_NOME") == 0 )
            {
               AV10TFCheck_Nome = AV33GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFCHECK_NOME_SEL") == 0 )
            {
               AV11TFCheck_Nome_Sel = AV33GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFCHECK_MOMENTO_SEL") == 0 )
            {
               AV14TFCheck_Momento_SelsJson = AV33GridStateFilterValue.gxTpr_Value;
               AV15TFCheck_Momento_Sels.FromJSonString(AV14TFCheck_Momento_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFCHECK_QDONAOCMP_SEL") == 0 )
            {
               AV47TFCheck_QdoNaoCmp_SelsJson = AV33GridStateFilterValue.gxTpr_Value;
               AV48TFCheck_QdoNaoCmp_Sels.FromJSonString(AV47TFCheck_QdoNaoCmp_SelsJson);
            }
            AV51GXV1 = (int)(AV51GXV1+1);
         }
         if ( AV32GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV34GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV32GridState.gxTpr_Dynamicfilters.Item(1));
            AV36DynamicFiltersSelector1 = AV34GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "CHECK_NOME") == 0 )
            {
               AV37DynamicFiltersOperator1 = AV34GridStateDynamicFilter.gxTpr_Operator;
               AV38Check_Nome1 = AV34GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV32GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV39DynamicFiltersEnabled2 = true;
               AV34GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV32GridState.gxTpr_Dynamicfilters.Item(2));
               AV40DynamicFiltersSelector2 = AV34GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "CHECK_NOME") == 0 )
               {
                  AV41DynamicFiltersOperator2 = AV34GridStateDynamicFilter.gxTpr_Operator;
                  AV42Check_Nome2 = AV34GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV32GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV43DynamicFiltersEnabled3 = true;
                  AV34GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV32GridState.gxTpr_Dynamicfilters.Item(3));
                  AV44DynamicFiltersSelector3 = AV34GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV44DynamicFiltersSelector3, "CHECK_NOME") == 0 )
                  {
                     AV45DynamicFiltersOperator3 = AV34GridStateDynamicFilter.gxTpr_Operator;
                     AV46Check_Nome3 = AV34GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCHECK_NOMEOPTIONS' Routine */
         AV10TFCheck_Nome = AV17SearchTxt;
         AV11TFCheck_Nome_Sel = "";
         AV53WWCheckDS_1_Check_areatrabalhocod = AV35Check_AreaTrabalhoCod;
         AV54WWCheckDS_2_Dynamicfiltersselector1 = AV36DynamicFiltersSelector1;
         AV55WWCheckDS_3_Dynamicfiltersoperator1 = AV37DynamicFiltersOperator1;
         AV56WWCheckDS_4_Check_nome1 = AV38Check_Nome1;
         AV57WWCheckDS_5_Dynamicfiltersenabled2 = AV39DynamicFiltersEnabled2;
         AV58WWCheckDS_6_Dynamicfiltersselector2 = AV40DynamicFiltersSelector2;
         AV59WWCheckDS_7_Dynamicfiltersoperator2 = AV41DynamicFiltersOperator2;
         AV60WWCheckDS_8_Check_nome2 = AV42Check_Nome2;
         AV61WWCheckDS_9_Dynamicfiltersenabled3 = AV43DynamicFiltersEnabled3;
         AV62WWCheckDS_10_Dynamicfiltersselector3 = AV44DynamicFiltersSelector3;
         AV63WWCheckDS_11_Dynamicfiltersoperator3 = AV45DynamicFiltersOperator3;
         AV64WWCheckDS_12_Check_nome3 = AV46Check_Nome3;
         AV65WWCheckDS_13_Tfcheck_nome = AV10TFCheck_Nome;
         AV66WWCheckDS_14_Tfcheck_nome_sel = AV11TFCheck_Nome_Sel;
         AV67WWCheckDS_15_Tfcheck_momento_sels = AV15TFCheck_Momento_Sels;
         AV68WWCheckDS_16_Tfcheck_qdonaocmp_sels = AV48TFCheck_QdoNaoCmp_Sels;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A1843Check_Momento ,
                                              AV67WWCheckDS_15_Tfcheck_momento_sels ,
                                              A1856Check_QdoNaoCmp ,
                                              AV68WWCheckDS_16_Tfcheck_qdonaocmp_sels ,
                                              AV54WWCheckDS_2_Dynamicfiltersselector1 ,
                                              AV55WWCheckDS_3_Dynamicfiltersoperator1 ,
                                              AV56WWCheckDS_4_Check_nome1 ,
                                              AV57WWCheckDS_5_Dynamicfiltersenabled2 ,
                                              AV58WWCheckDS_6_Dynamicfiltersselector2 ,
                                              AV59WWCheckDS_7_Dynamicfiltersoperator2 ,
                                              AV60WWCheckDS_8_Check_nome2 ,
                                              AV61WWCheckDS_9_Dynamicfiltersenabled3 ,
                                              AV62WWCheckDS_10_Dynamicfiltersselector3 ,
                                              AV63WWCheckDS_11_Dynamicfiltersoperator3 ,
                                              AV64WWCheckDS_12_Check_nome3 ,
                                              AV66WWCheckDS_14_Tfcheck_nome_sel ,
                                              AV65WWCheckDS_13_Tfcheck_nome ,
                                              AV67WWCheckDS_15_Tfcheck_momento_sels.Count ,
                                              AV68WWCheckDS_16_Tfcheck_qdonaocmp_sels.Count ,
                                              A1841Check_Nome ,
                                              A1840Check_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.INT
                                              }
         });
         lV56WWCheckDS_4_Check_nome1 = StringUtil.PadR( StringUtil.RTrim( AV56WWCheckDS_4_Check_nome1), 50, "%");
         lV56WWCheckDS_4_Check_nome1 = StringUtil.PadR( StringUtil.RTrim( AV56WWCheckDS_4_Check_nome1), 50, "%");
         lV60WWCheckDS_8_Check_nome2 = StringUtil.PadR( StringUtil.RTrim( AV60WWCheckDS_8_Check_nome2), 50, "%");
         lV60WWCheckDS_8_Check_nome2 = StringUtil.PadR( StringUtil.RTrim( AV60WWCheckDS_8_Check_nome2), 50, "%");
         lV64WWCheckDS_12_Check_nome3 = StringUtil.PadR( StringUtil.RTrim( AV64WWCheckDS_12_Check_nome3), 50, "%");
         lV64WWCheckDS_12_Check_nome3 = StringUtil.PadR( StringUtil.RTrim( AV64WWCheckDS_12_Check_nome3), 50, "%");
         lV65WWCheckDS_13_Tfcheck_nome = StringUtil.PadR( StringUtil.RTrim( AV65WWCheckDS_13_Tfcheck_nome), 50, "%");
         /* Using cursor P00T02 */
         pr_default.execute(0, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, lV56WWCheckDS_4_Check_nome1, lV56WWCheckDS_4_Check_nome1, lV60WWCheckDS_8_Check_nome2, lV60WWCheckDS_8_Check_nome2, lV64WWCheckDS_12_Check_nome3, lV64WWCheckDS_12_Check_nome3, lV65WWCheckDS_13_Tfcheck_nome, AV66WWCheckDS_14_Tfcheck_nome_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKT02 = false;
            A1840Check_AreaTrabalhoCod = P00T02_A1840Check_AreaTrabalhoCod[0];
            A1841Check_Nome = P00T02_A1841Check_Nome[0];
            A1856Check_QdoNaoCmp = P00T02_A1856Check_QdoNaoCmp[0];
            n1856Check_QdoNaoCmp = P00T02_n1856Check_QdoNaoCmp[0];
            A1843Check_Momento = P00T02_A1843Check_Momento[0];
            A1839Check_Codigo = P00T02_A1839Check_Codigo[0];
            AV29count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00T02_A1841Check_Nome[0], A1841Check_Nome) == 0 ) )
            {
               BRKT02 = false;
               A1839Check_Codigo = P00T02_A1839Check_Codigo[0];
               AV29count = (long)(AV29count+1);
               BRKT02 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1841Check_Nome)) )
            {
               AV21Option = A1841Check_Nome;
               AV22Options.Add(AV21Option, 0);
               AV27OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV29count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV22Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKT02 )
            {
               BRKT02 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV22Options = new GxSimpleCollection();
         AV25OptionsDesc = new GxSimpleCollection();
         AV27OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV30Session = context.GetSession();
         AV32GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV33GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFCheck_Nome = "";
         AV11TFCheck_Nome_Sel = "";
         AV14TFCheck_Momento_SelsJson = "";
         AV15TFCheck_Momento_Sels = new GxSimpleCollection();
         AV47TFCheck_QdoNaoCmp_SelsJson = "";
         AV48TFCheck_QdoNaoCmp_Sels = new GxSimpleCollection();
         AV34GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV36DynamicFiltersSelector1 = "";
         AV38Check_Nome1 = "";
         AV40DynamicFiltersSelector2 = "";
         AV42Check_Nome2 = "";
         AV44DynamicFiltersSelector3 = "";
         AV46Check_Nome3 = "";
         AV54WWCheckDS_2_Dynamicfiltersselector1 = "";
         AV56WWCheckDS_4_Check_nome1 = "";
         AV58WWCheckDS_6_Dynamicfiltersselector2 = "";
         AV60WWCheckDS_8_Check_nome2 = "";
         AV62WWCheckDS_10_Dynamicfiltersselector3 = "";
         AV64WWCheckDS_12_Check_nome3 = "";
         AV65WWCheckDS_13_Tfcheck_nome = "";
         AV66WWCheckDS_14_Tfcheck_nome_sel = "";
         AV67WWCheckDS_15_Tfcheck_momento_sels = new GxSimpleCollection();
         AV68WWCheckDS_16_Tfcheck_qdonaocmp_sels = new GxSimpleCollection();
         scmdbuf = "";
         lV56WWCheckDS_4_Check_nome1 = "";
         lV60WWCheckDS_8_Check_nome2 = "";
         lV64WWCheckDS_12_Check_nome3 = "";
         lV65WWCheckDS_13_Tfcheck_nome = "";
         A1841Check_Nome = "";
         P00T02_A1840Check_AreaTrabalhoCod = new int[1] ;
         P00T02_A1841Check_Nome = new String[] {""} ;
         P00T02_A1856Check_QdoNaoCmp = new short[1] ;
         P00T02_n1856Check_QdoNaoCmp = new bool[] {false} ;
         P00T02_A1843Check_Momento = new short[1] ;
         P00T02_A1839Check_Codigo = new int[1] ;
         AV21Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwcheckfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00T02_A1840Check_AreaTrabalhoCod, P00T02_A1841Check_Nome, P00T02_A1856Check_QdoNaoCmp, P00T02_n1856Check_QdoNaoCmp, P00T02_A1843Check_Momento, P00T02_A1839Check_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV37DynamicFiltersOperator1 ;
      private short AV41DynamicFiltersOperator2 ;
      private short AV45DynamicFiltersOperator3 ;
      private short AV55WWCheckDS_3_Dynamicfiltersoperator1 ;
      private short AV59WWCheckDS_7_Dynamicfiltersoperator2 ;
      private short AV63WWCheckDS_11_Dynamicfiltersoperator3 ;
      private short A1843Check_Momento ;
      private short A1856Check_QdoNaoCmp ;
      private int AV51GXV1 ;
      private int AV35Check_AreaTrabalhoCod ;
      private int AV53WWCheckDS_1_Check_areatrabalhocod ;
      private int AV67WWCheckDS_15_Tfcheck_momento_sels_Count ;
      private int AV68WWCheckDS_16_Tfcheck_qdonaocmp_sels_Count ;
      private int AV9WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A1840Check_AreaTrabalhoCod ;
      private int A1839Check_Codigo ;
      private long AV29count ;
      private String AV10TFCheck_Nome ;
      private String AV11TFCheck_Nome_Sel ;
      private String AV38Check_Nome1 ;
      private String AV42Check_Nome2 ;
      private String AV46Check_Nome3 ;
      private String AV56WWCheckDS_4_Check_nome1 ;
      private String AV60WWCheckDS_8_Check_nome2 ;
      private String AV64WWCheckDS_12_Check_nome3 ;
      private String AV65WWCheckDS_13_Tfcheck_nome ;
      private String AV66WWCheckDS_14_Tfcheck_nome_sel ;
      private String scmdbuf ;
      private String lV56WWCheckDS_4_Check_nome1 ;
      private String lV60WWCheckDS_8_Check_nome2 ;
      private String lV64WWCheckDS_12_Check_nome3 ;
      private String lV65WWCheckDS_13_Tfcheck_nome ;
      private String A1841Check_Nome ;
      private bool returnInSub ;
      private bool AV39DynamicFiltersEnabled2 ;
      private bool AV43DynamicFiltersEnabled3 ;
      private bool AV57WWCheckDS_5_Dynamicfiltersenabled2 ;
      private bool AV61WWCheckDS_9_Dynamicfiltersenabled3 ;
      private bool BRKT02 ;
      private bool n1856Check_QdoNaoCmp ;
      private String AV28OptionIndexesJson ;
      private String AV23OptionsJson ;
      private String AV26OptionsDescJson ;
      private String AV14TFCheck_Momento_SelsJson ;
      private String AV47TFCheck_QdoNaoCmp_SelsJson ;
      private String AV19DDOName ;
      private String AV17SearchTxt ;
      private String AV18SearchTxtTo ;
      private String AV36DynamicFiltersSelector1 ;
      private String AV40DynamicFiltersSelector2 ;
      private String AV44DynamicFiltersSelector3 ;
      private String AV54WWCheckDS_2_Dynamicfiltersselector1 ;
      private String AV58WWCheckDS_6_Dynamicfiltersselector2 ;
      private String AV62WWCheckDS_10_Dynamicfiltersselector3 ;
      private String AV21Option ;
      private IGxSession AV30Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00T02_A1840Check_AreaTrabalhoCod ;
      private String[] P00T02_A1841Check_Nome ;
      private short[] P00T02_A1856Check_QdoNaoCmp ;
      private bool[] P00T02_n1856Check_QdoNaoCmp ;
      private short[] P00T02_A1843Check_Momento ;
      private int[] P00T02_A1839Check_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV15TFCheck_Momento_Sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV48TFCheck_QdoNaoCmp_Sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV67WWCheckDS_15_Tfcheck_momento_sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV68WWCheckDS_16_Tfcheck_qdonaocmp_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV25OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV27OptionIndexes ;
      private wwpbaseobjects.SdtWWPGridState AV32GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV33GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV34GridStateDynamicFilter ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
   }

   public class getwwcheckfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00T02( IGxContext context ,
                                             short A1843Check_Momento ,
                                             IGxCollection AV67WWCheckDS_15_Tfcheck_momento_sels ,
                                             short A1856Check_QdoNaoCmp ,
                                             IGxCollection AV68WWCheckDS_16_Tfcheck_qdonaocmp_sels ,
                                             String AV54WWCheckDS_2_Dynamicfiltersselector1 ,
                                             short AV55WWCheckDS_3_Dynamicfiltersoperator1 ,
                                             String AV56WWCheckDS_4_Check_nome1 ,
                                             bool AV57WWCheckDS_5_Dynamicfiltersenabled2 ,
                                             String AV58WWCheckDS_6_Dynamicfiltersselector2 ,
                                             short AV59WWCheckDS_7_Dynamicfiltersoperator2 ,
                                             String AV60WWCheckDS_8_Check_nome2 ,
                                             bool AV61WWCheckDS_9_Dynamicfiltersenabled3 ,
                                             String AV62WWCheckDS_10_Dynamicfiltersselector3 ,
                                             short AV63WWCheckDS_11_Dynamicfiltersoperator3 ,
                                             String AV64WWCheckDS_12_Check_nome3 ,
                                             String AV66WWCheckDS_14_Tfcheck_nome_sel ,
                                             String AV65WWCheckDS_13_Tfcheck_nome ,
                                             int AV67WWCheckDS_15_Tfcheck_momento_sels_Count ,
                                             int AV68WWCheckDS_16_Tfcheck_qdonaocmp_sels_Count ,
                                             String A1841Check_Nome ,
                                             int A1840Check_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [9] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [Check_AreaTrabalhoCod], [Check_Nome], [Check_QdoNaoCmp], [Check_Momento], [Check_Codigo] FROM [Check] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Check_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV54WWCheckDS_2_Dynamicfiltersselector1, "CHECK_NOME") == 0 ) && ( AV55WWCheckDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWCheckDS_4_Check_nome1)) ) )
         {
            sWhereString = sWhereString + " and ([Check_Nome] like @lV56WWCheckDS_4_Check_nome1)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV54WWCheckDS_2_Dynamicfiltersselector1, "CHECK_NOME") == 0 ) && ( AV55WWCheckDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWCheckDS_4_Check_nome1)) ) )
         {
            sWhereString = sWhereString + " and ([Check_Nome] like '%' + @lV56WWCheckDS_4_Check_nome1)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV57WWCheckDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV58WWCheckDS_6_Dynamicfiltersselector2, "CHECK_NOME") == 0 ) && ( AV59WWCheckDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWCheckDS_8_Check_nome2)) ) )
         {
            sWhereString = sWhereString + " and ([Check_Nome] like @lV60WWCheckDS_8_Check_nome2)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV57WWCheckDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV58WWCheckDS_6_Dynamicfiltersselector2, "CHECK_NOME") == 0 ) && ( AV59WWCheckDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWCheckDS_8_Check_nome2)) ) )
         {
            sWhereString = sWhereString + " and ([Check_Nome] like '%' + @lV60WWCheckDS_8_Check_nome2)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV61WWCheckDS_9_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV62WWCheckDS_10_Dynamicfiltersselector3, "CHECK_NOME") == 0 ) && ( AV63WWCheckDS_11_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWCheckDS_12_Check_nome3)) ) )
         {
            sWhereString = sWhereString + " and ([Check_Nome] like @lV64WWCheckDS_12_Check_nome3)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV61WWCheckDS_9_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV62WWCheckDS_10_Dynamicfiltersselector3, "CHECK_NOME") == 0 ) && ( AV63WWCheckDS_11_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWCheckDS_12_Check_nome3)) ) )
         {
            sWhereString = sWhereString + " and ([Check_Nome] like '%' + @lV64WWCheckDS_12_Check_nome3)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV66WWCheckDS_14_Tfcheck_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWCheckDS_13_Tfcheck_nome)) ) )
         {
            sWhereString = sWhereString + " and ([Check_Nome] like @lV65WWCheckDS_13_Tfcheck_nome)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWCheckDS_14_Tfcheck_nome_sel)) )
         {
            sWhereString = sWhereString + " and ([Check_Nome] = @AV66WWCheckDS_14_Tfcheck_nome_sel)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV67WWCheckDS_15_Tfcheck_momento_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV67WWCheckDS_15_Tfcheck_momento_sels, "[Check_Momento] IN (", ")") + ")";
         }
         if ( AV68WWCheckDS_16_Tfcheck_qdonaocmp_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV68WWCheckDS_16_Tfcheck_qdonaocmp_sels, "[Check_QdoNaoCmp] IN (", ")") + ")";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [Check_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00T02(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (short)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00T02 ;
          prmP00T02 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV56WWCheckDS_4_Check_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV56WWCheckDS_4_Check_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60WWCheckDS_8_Check_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60WWCheckDS_8_Check_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV64WWCheckDS_12_Check_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV64WWCheckDS_12_Check_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV65WWCheckDS_13_Tfcheck_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV66WWCheckDS_14_Tfcheck_nome_sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00T02", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00T02,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwcheckfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwcheckfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwcheckfilterdata") )
          {
             return  ;
          }
          getwwcheckfilterdata worker = new getwwcheckfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
