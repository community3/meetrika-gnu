/*
               File: SolicitacaoMudancaSolicitacaoMudancaItemWC
        Description: Solicitacao Mudanca Solicitacao Mudanca Item WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:26:10.57
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class solicitacaomudancasolicitacaomudancaitemwc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public solicitacaomudancasolicitacaomudancaitemwc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public solicitacaomudancasolicitacaomudancaitemwc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_SolicitacaoMudanca_Codigo )
      {
         this.AV7SolicitacaoMudanca_Codigo = aP0_SolicitacaoMudanca_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7SolicitacaoMudanca_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7SolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7SolicitacaoMudanca_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7SolicitacaoMudanca_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_25 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_25_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_25_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
                  AV21TFSolicitacaoMudancaItem_FuncaoAPF = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFSolicitacaoMudancaItem_FuncaoAPF", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21TFSolicitacaoMudancaItem_FuncaoAPF), 6, 0)));
                  AV22TFSolicitacaoMudancaItem_FuncaoAPF_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFSolicitacaoMudancaItem_FuncaoAPF_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22TFSolicitacaoMudancaItem_FuncaoAPF_To), 6, 0)));
                  AV7SolicitacaoMudanca_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7SolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7SolicitacaoMudanca_Codigo), 6, 0)));
                  AV23ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace", AV23ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace);
                  AV32Pgmname = GetNextPar( );
                  A996SolicitacaoMudanca_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A996SolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A996SolicitacaoMudanca_Codigo), 6, 0)));
                  A995SolicitacaoMudancaItem_FuncaoAPF = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV14OrderedDsc, AV21TFSolicitacaoMudancaItem_FuncaoAPF, AV22TFSolicitacaoMudancaItem_FuncaoAPF_To, AV7SolicitacaoMudanca_Codigo, AV23ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace, AV32Pgmname, A996SolicitacaoMudanca_Codigo, A995SolicitacaoMudancaItem_FuncaoAPF, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  forbiddenHiddens = sPrefix + "hsh" + "SolicitacaoMudancaSolicitacaoMudancaItemWC";
                  forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A996SolicitacaoMudanca_Codigo), "ZZZZZ9");
                  GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
                  GXUtil.WriteLog("solicitacaomudancasolicitacaomudancaitemwc:[SendSecurityCheck value for]"+"SolicitacaoMudanca_Codigo:"+context.localUtil.Format( (decimal)(A996SolicitacaoMudanca_Codigo), "ZZZZZ9"));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAH12( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV32Pgmname = "SolicitacaoMudancaSolicitacaoMudancaItemWC";
               context.Gx_err = 0;
               WSH12( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Solicitacao Mudanca Solicitacao Mudanca Item WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117261065");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("solicitacaomudancasolicitacaomudancaitemwc.aspx") + "?" + UrlEncode("" +AV7SolicitacaoMudanca_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21TFSolicitacaoMudancaItem_FuncaoAPF), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22TFSolicitacaoMudancaItem_FuncaoAPF_To), 6, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_25", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_25), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV26GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV27GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV24DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV24DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vSOLICITACAOMUDANCAITEM_FUNCAOAPFTITLEFILTERDATA", AV20SolicitacaoMudancaItem_FuncaoAPFTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vSOLICITACAOMUDANCAITEM_FUNCAOAPFTITLEFILTERDATA", AV20SolicitacaoMudancaItem_FuncaoAPFTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7SolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7SolicitacaoMudanca_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vSOLICITACAOMUDANCA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7SolicitacaoMudanca_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV32Pgmname));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Caption", StringUtil.RTrim( Ddo_solicitacaomudancaitem_funcaoapf_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Tooltip", StringUtil.RTrim( Ddo_solicitacaomudancaitem_funcaoapf_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Cls", StringUtil.RTrim( Ddo_solicitacaomudancaitem_funcaoapf_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Filteredtext_set", StringUtil.RTrim( Ddo_solicitacaomudancaitem_funcaoapf_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Filteredtextto_set", StringUtil.RTrim( Ddo_solicitacaomudancaitem_funcaoapf_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Dropdownoptionstype", StringUtil.RTrim( Ddo_solicitacaomudancaitem_funcaoapf_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_solicitacaomudancaitem_funcaoapf_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Includesortasc", StringUtil.BoolToStr( Ddo_solicitacaomudancaitem_funcaoapf_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Includesortdsc", StringUtil.BoolToStr( Ddo_solicitacaomudancaitem_funcaoapf_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Sortedstatus", StringUtil.RTrim( Ddo_solicitacaomudancaitem_funcaoapf_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Includefilter", StringUtil.BoolToStr( Ddo_solicitacaomudancaitem_funcaoapf_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Filtertype", StringUtil.RTrim( Ddo_solicitacaomudancaitem_funcaoapf_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Filterisrange", StringUtil.BoolToStr( Ddo_solicitacaomudancaitem_funcaoapf_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Includedatalist", StringUtil.BoolToStr( Ddo_solicitacaomudancaitem_funcaoapf_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Sortasc", StringUtil.RTrim( Ddo_solicitacaomudancaitem_funcaoapf_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Sortdsc", StringUtil.RTrim( Ddo_solicitacaomudancaitem_funcaoapf_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Cleanfilter", StringUtil.RTrim( Ddo_solicitacaomudancaitem_funcaoapf_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Rangefilterfrom", StringUtil.RTrim( Ddo_solicitacaomudancaitem_funcaoapf_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Rangefilterto", StringUtil.RTrim( Ddo_solicitacaomudancaitem_funcaoapf_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Searchbuttontext", StringUtil.RTrim( Ddo_solicitacaomudancaitem_funcaoapf_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Activeeventkey", StringUtil.RTrim( Ddo_solicitacaomudancaitem_funcaoapf_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Filteredtext_get", StringUtil.RTrim( Ddo_solicitacaomudancaitem_funcaoapf_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Filteredtextto_get", StringUtil.RTrim( Ddo_solicitacaomudancaitem_funcaoapf_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = sPrefix + "hsh" + "SolicitacaoMudancaSolicitacaoMudancaItemWC";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A996SolicitacaoMudanca_Codigo), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("solicitacaomudancasolicitacaomudancaitemwc:[SendSecurityCheck value for]"+"SolicitacaoMudanca_Codigo:"+context.localUtil.Format( (decimal)(A996SolicitacaoMudanca_Codigo), "ZZZZZ9"));
      }

      protected void RenderHtmlCloseFormH12( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("solicitacaomudancasolicitacaomudancaitemwc.js", "?2020311726114");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "SolicitacaoMudancaSolicitacaoMudancaItemWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Solicitacao Mudanca Solicitacao Mudanca Item WC" ;
      }

      protected void WBH10( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "solicitacaomudancasolicitacaomudancaitemwc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
               context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            wb_table1_2_H12( true) ;
         }
         else
         {
            wb_table1_2_H12( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_H12e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSolicitacaoMudanca_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A996SolicitacaoMudanca_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A996SolicitacaoMudanca_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSolicitacaoMudanca_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtSolicitacaoMudanca_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_SolicitacaoMudancaSolicitacaoMudancaItemWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'" + sPrefix + "',false,'" + sGXsfl_25_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,35);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_SolicitacaoMudancaSolicitacaoMudancaItemWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'" + sPrefix + "',false,'" + sGXsfl_25_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsolicitacaomudancaitem_funcaoapf_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21TFSolicitacaoMudancaItem_FuncaoAPF), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV21TFSolicitacaoMudancaItem_FuncaoAPF), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,36);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsolicitacaomudancaitem_funcaoapf_Jsonclick, 0, "Attribute", "", "", "", edtavTfsolicitacaomudancaitem_funcaoapf_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_SolicitacaoMudancaSolicitacaoMudancaItemWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'" + sPrefix + "',false,'" + sGXsfl_25_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsolicitacaomudancaitem_funcaoapf_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22TFSolicitacaoMudancaItem_FuncaoAPF_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV22TFSolicitacaoMudancaItem_FuncaoAPF_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,37);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsolicitacaomudancaitem_funcaoapf_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfsolicitacaomudancaitem_funcaoapf_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_SolicitacaoMudancaSolicitacaoMudancaItemWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPFContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'" + sPrefix + "',false,'" + sGXsfl_25_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_solicitacaomudancaitem_funcaoapftitlecontrolidtoreplace_Internalname, AV23ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,39);\"", 0, edtavDdo_solicitacaomudancaitem_funcaoapftitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_SolicitacaoMudancaSolicitacaoMudancaItemWC.htm");
         }
         wbLoad = true;
      }

      protected void STARTH12( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Solicitacao Mudanca Solicitacao Mudanca Item WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPH10( ) ;
            }
         }
      }

      protected void WSH12( )
      {
         STARTH12( ) ;
         EVTH12( ) ;
      }

      protected void EVTH12( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPH10( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPH10( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11H12 */
                                    E11H12 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPH10( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12H12 */
                                    E12H12 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPH10( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13H12 */
                                    E13H12 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPH10( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14H12 */
                                    E14H12 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPH10( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavOrdereddsc_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPH10( ) ;
                              }
                              nGXsfl_25_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_25_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_25_idx), 4, 0)), 4, "0");
                              SubsflControlProps_252( ) ;
                              GXCCtl = "SOLICITACAOMUDANCAITEM_DESCRICAO_Enabled_" + sGXsfl_25_idx;
                              Solicitacaomudancaitem_descricao_Enabled = StringUtil.StrToBool( cgiGet( sPrefix+GXCCtl));
                              context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Solicitacaomudancaitem_descricao_Internalname, "Enabled", StringUtil.BoolToStr( Solicitacaomudancaitem_descricao_Enabled));
                              AV15Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV15Update)) ? AV30Update_GXI : context.convertURL( context.PathToRelativeUrl( AV15Update))));
                              AV16Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV16Delete)) ? AV31Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV16Delete))));
                              A995SolicitacaoMudancaItem_FuncaoAPF = (int)(context.localUtil.CToN( cgiGet( edtSolicitacaoMudancaItem_FuncaoAPF_Internalname), ",", "."));
                              GXCCtl = "SOLICITACAOMUDANCAITEM_DESCRICAO_" + sGXsfl_25_idx;
                              A998SolicitacaoMudancaItem_Descricao = cgiGet( sPrefix+GXCCtl);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrdereddsc_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E15H12 */
                                          E15H12 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrdereddsc_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E16H12 */
                                          E16H12 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrdereddsc_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E17H12 */
                                          E17H12 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfsolicitacaomudancaitem_funcaoapf Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF"), ",", ".") != Convert.ToDecimal( AV21TFSolicitacaoMudancaItem_FuncaoAPF )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfsolicitacaomudancaitem_funcaoapf_to Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF_TO"), ",", ".") != Convert.ToDecimal( AV22TFSolicitacaoMudancaItem_FuncaoAPF_To )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrdereddsc_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPH10( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrdereddsc_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEH12( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormH12( ) ;
            }
         }
      }

      protected void PAH12( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavOrdereddsc_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_252( ) ;
         while ( nGXsfl_25_idx <= nRC_GXsfl_25 )
         {
            sendrow_252( ) ;
            nGXsfl_25_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_25_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_25_idx+1));
            sGXsfl_25_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_25_idx), 4, 0)), 4, "0");
            SubsflControlProps_252( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       bool AV14OrderedDsc ,
                                       int AV21TFSolicitacaoMudancaItem_FuncaoAPF ,
                                       int AV22TFSolicitacaoMudancaItem_FuncaoAPF_To ,
                                       int AV7SolicitacaoMudanca_Codigo ,
                                       String AV23ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace ,
                                       String AV32Pgmname ,
                                       int A996SolicitacaoMudanca_Codigo ,
                                       int A995SolicitacaoMudancaItem_FuncaoAPF ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFH12( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SOLICITACAOMUDANCAITEM_FUNCAOAPF", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A995SolicitacaoMudancaItem_FuncaoAPF), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SOLICITACAOMUDANCAITEM_FUNCAOAPF", StringUtil.LTrim( StringUtil.NToC( (decimal)(A995SolicitacaoMudancaItem_FuncaoAPF), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFH12( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV32Pgmname = "SolicitacaoMudancaSolicitacaoMudancaItemWC";
         context.Gx_err = 0;
      }

      protected void RFH12( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 25;
         /* Execute user event: E16H12 */
         E16H12 ();
         nGXsfl_25_idx = 1;
         sGXsfl_25_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_25_idx), 4, 0)), 4, "0");
         SubsflControlProps_252( ) ;
         nGXsfl_25_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_252( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV21TFSolicitacaoMudancaItem_FuncaoAPF ,
                                                 AV22TFSolicitacaoMudancaItem_FuncaoAPF_To ,
                                                 A995SolicitacaoMudancaItem_FuncaoAPF ,
                                                 AV14OrderedDsc ,
                                                 A996SolicitacaoMudanca_Codigo ,
                                                 AV7SolicitacaoMudanca_Codigo },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            /* Using cursor H00H12 */
            pr_default.execute(0, new Object[] {AV7SolicitacaoMudanca_Codigo, AV21TFSolicitacaoMudancaItem_FuncaoAPF, AV22TFSolicitacaoMudancaItem_FuncaoAPF_To, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_25_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A998SolicitacaoMudancaItem_Descricao = H00H12_A998SolicitacaoMudancaItem_Descricao[0];
               A996SolicitacaoMudanca_Codigo = H00H12_A996SolicitacaoMudanca_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A996SolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A996SolicitacaoMudanca_Codigo), 6, 0)));
               A995SolicitacaoMudancaItem_FuncaoAPF = H00H12_A995SolicitacaoMudancaItem_FuncaoAPF[0];
               /* Execute user event: E17H12 */
               E17H12 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 25;
            WBH10( ) ;
         }
         nGXsfl_25_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV21TFSolicitacaoMudancaItem_FuncaoAPF ,
                                              AV22TFSolicitacaoMudancaItem_FuncaoAPF_To ,
                                              A995SolicitacaoMudancaItem_FuncaoAPF ,
                                              AV14OrderedDsc ,
                                              A996SolicitacaoMudanca_Codigo ,
                                              AV7SolicitacaoMudanca_Codigo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         /* Using cursor H00H13 */
         pr_default.execute(1, new Object[] {AV7SolicitacaoMudanca_Codigo, AV21TFSolicitacaoMudancaItem_FuncaoAPF, AV22TFSolicitacaoMudancaItem_FuncaoAPF_To});
         GRID_nRecordCount = H00H13_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedDsc, AV21TFSolicitacaoMudancaItem_FuncaoAPF, AV22TFSolicitacaoMudancaItem_FuncaoAPF_To, AV7SolicitacaoMudanca_Codigo, AV23ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace, AV32Pgmname, A996SolicitacaoMudanca_Codigo, A995SolicitacaoMudancaItem_FuncaoAPF, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedDsc, AV21TFSolicitacaoMudancaItem_FuncaoAPF, AV22TFSolicitacaoMudancaItem_FuncaoAPF_To, AV7SolicitacaoMudanca_Codigo, AV23ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace, AV32Pgmname, A996SolicitacaoMudanca_Codigo, A995SolicitacaoMudancaItem_FuncaoAPF, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedDsc, AV21TFSolicitacaoMudancaItem_FuncaoAPF, AV22TFSolicitacaoMudancaItem_FuncaoAPF_To, AV7SolicitacaoMudanca_Codigo, AV23ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace, AV32Pgmname, A996SolicitacaoMudanca_Codigo, A995SolicitacaoMudancaItem_FuncaoAPF, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedDsc, AV21TFSolicitacaoMudancaItem_FuncaoAPF, AV22TFSolicitacaoMudancaItem_FuncaoAPF_To, AV7SolicitacaoMudanca_Codigo, AV23ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace, AV32Pgmname, A996SolicitacaoMudanca_Codigo, A995SolicitacaoMudancaItem_FuncaoAPF, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedDsc, AV21TFSolicitacaoMudancaItem_FuncaoAPF, AV22TFSolicitacaoMudancaItem_FuncaoAPF_To, AV7SolicitacaoMudanca_Codigo, AV23ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace, AV32Pgmname, A996SolicitacaoMudanca_Codigo, A995SolicitacaoMudancaItem_FuncaoAPF, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPH10( )
      {
         /* Before Start, stand alone formulas. */
         AV32Pgmname = "SolicitacaoMudancaSolicitacaoMudancaItemWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E15H12 */
         E15H12 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV24DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vSOLICITACAOMUDANCAITEM_FUNCAOAPFTITLEFILTERDATA"), AV20SolicitacaoMudancaItem_FuncaoAPFTitleFilterData);
            /* Read variables values. */
            A996SolicitacaoMudanca_Codigo = (int)(context.localUtil.CToN( cgiGet( edtSolicitacaoMudanca_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A996SolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A996SolicitacaoMudanca_Codigo), 6, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudancaitem_funcaoapf_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudancaitem_funcaoapf_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF");
               GX_FocusControl = edtavTfsolicitacaomudancaitem_funcaoapf_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV21TFSolicitacaoMudancaItem_FuncaoAPF = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFSolicitacaoMudancaItem_FuncaoAPF", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21TFSolicitacaoMudancaItem_FuncaoAPF), 6, 0)));
            }
            else
            {
               AV21TFSolicitacaoMudancaItem_FuncaoAPF = (int)(context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudancaitem_funcaoapf_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFSolicitacaoMudancaItem_FuncaoAPF", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21TFSolicitacaoMudancaItem_FuncaoAPF), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudancaitem_funcaoapf_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudancaitem_funcaoapf_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF_TO");
               GX_FocusControl = edtavTfsolicitacaomudancaitem_funcaoapf_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV22TFSolicitacaoMudancaItem_FuncaoAPF_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFSolicitacaoMudancaItem_FuncaoAPF_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22TFSolicitacaoMudancaItem_FuncaoAPF_To), 6, 0)));
            }
            else
            {
               AV22TFSolicitacaoMudancaItem_FuncaoAPF_To = (int)(context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudancaitem_funcaoapf_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFSolicitacaoMudancaItem_FuncaoAPF_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22TFSolicitacaoMudancaItem_FuncaoAPF_To), 6, 0)));
            }
            AV23ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace = cgiGet( edtavDdo_solicitacaomudancaitem_funcaoapftitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace", AV23ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_25 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_25"), ",", "."));
            AV26GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV27GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7SolicitacaoMudanca_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7SolicitacaoMudanca_Codigo"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_solicitacaomudancaitem_funcaoapf_Caption = cgiGet( sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Caption");
            Ddo_solicitacaomudancaitem_funcaoapf_Tooltip = cgiGet( sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Tooltip");
            Ddo_solicitacaomudancaitem_funcaoapf_Cls = cgiGet( sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Cls");
            Ddo_solicitacaomudancaitem_funcaoapf_Filteredtext_set = cgiGet( sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Filteredtext_set");
            Ddo_solicitacaomudancaitem_funcaoapf_Filteredtextto_set = cgiGet( sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Filteredtextto_set");
            Ddo_solicitacaomudancaitem_funcaoapf_Dropdownoptionstype = cgiGet( sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Dropdownoptionstype");
            Ddo_solicitacaomudancaitem_funcaoapf_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Titlecontrolidtoreplace");
            Ddo_solicitacaomudancaitem_funcaoapf_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Includesortasc"));
            Ddo_solicitacaomudancaitem_funcaoapf_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Includesortdsc"));
            Ddo_solicitacaomudancaitem_funcaoapf_Sortedstatus = cgiGet( sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Sortedstatus");
            Ddo_solicitacaomudancaitem_funcaoapf_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Includefilter"));
            Ddo_solicitacaomudancaitem_funcaoapf_Filtertype = cgiGet( sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Filtertype");
            Ddo_solicitacaomudancaitem_funcaoapf_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Filterisrange"));
            Ddo_solicitacaomudancaitem_funcaoapf_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Includedatalist"));
            Ddo_solicitacaomudancaitem_funcaoapf_Sortasc = cgiGet( sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Sortasc");
            Ddo_solicitacaomudancaitem_funcaoapf_Sortdsc = cgiGet( sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Sortdsc");
            Ddo_solicitacaomudancaitem_funcaoapf_Cleanfilter = cgiGet( sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Cleanfilter");
            Ddo_solicitacaomudancaitem_funcaoapf_Rangefilterfrom = cgiGet( sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Rangefilterfrom");
            Ddo_solicitacaomudancaitem_funcaoapf_Rangefilterto = cgiGet( sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Rangefilterto");
            Ddo_solicitacaomudancaitem_funcaoapf_Searchbuttontext = cgiGet( sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_solicitacaomudancaitem_funcaoapf_Activeeventkey = cgiGet( sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Activeeventkey");
            Ddo_solicitacaomudancaitem_funcaoapf_Filteredtext_get = cgiGet( sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Filteredtext_get");
            Ddo_solicitacaomudancaitem_funcaoapf_Filteredtextto_get = cgiGet( sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = sPrefix + "hsh" + "SolicitacaoMudancaSolicitacaoMudancaItemWC";
            A996SolicitacaoMudanca_Codigo = (int)(context.localUtil.CToN( cgiGet( edtSolicitacaoMudanca_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A996SolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A996SolicitacaoMudanca_Codigo), 6, 0)));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A996SolicitacaoMudanca_Codigo), "ZZZZZ9");
            hsh = cgiGet( sPrefix+"hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("solicitacaomudancasolicitacaomudancaitemwc:[SecurityCheckFailed value for]"+"SolicitacaoMudanca_Codigo:"+context.localUtil.Format( (decimal)(A996SolicitacaoMudanca_Codigo), "ZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
            /* Check if conditions changed and reset current page numbers */
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF"), ",", ".") != Convert.ToDecimal( AV21TFSolicitacaoMudancaItem_FuncaoAPF )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF_TO"), ",", ".") != Convert.ToDecimal( AV22TFSolicitacaoMudancaItem_FuncaoAPF_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E15H12 */
         E15H12 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E15H12( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTfsolicitacaomudancaitem_funcaoapf_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfsolicitacaomudancaitem_funcaoapf_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsolicitacaomudancaitem_funcaoapf_Visible), 5, 0)));
         edtavTfsolicitacaomudancaitem_funcaoapf_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfsolicitacaomudancaitem_funcaoapf_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsolicitacaomudancaitem_funcaoapf_to_Visible), 5, 0)));
         Ddo_solicitacaomudancaitem_funcaoapf_Titlecontrolidtoreplace = subGrid_Internalname+"_SolicitacaoMudancaItem_FuncaoAPF";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_solicitacaomudancaitem_funcaoapf_Internalname, "TitleControlIdToReplace", Ddo_solicitacaomudancaitem_funcaoapf_Titlecontrolidtoreplace);
         AV23ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace = Ddo_solicitacaomudancaitem_funcaoapf_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace", AV23ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace);
         edtavDdo_solicitacaomudancaitem_funcaoapftitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_solicitacaomudancaitem_funcaoapftitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_solicitacaomudancaitem_funcaoapftitlecontrolidtoreplace_Visible), 5, 0)));
         edtSolicitacaoMudanca_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtSolicitacaoMudanca_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSolicitacaoMudanca_Codigo_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV24DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV24DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E16H12( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV20SolicitacaoMudancaItem_FuncaoAPFTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtSolicitacaoMudancaItem_FuncaoAPF_Titleformat = 2;
         edtSolicitacaoMudancaItem_FuncaoAPF_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Item", AV23ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtSolicitacaoMudancaItem_FuncaoAPF_Internalname, "Title", edtSolicitacaoMudancaItem_FuncaoAPF_Title);
         AV26GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26GridCurrentPage), 10, 0)));
         AV27GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV20SolicitacaoMudancaItem_FuncaoAPFTitleFilterData", AV20SolicitacaoMudancaItem_FuncaoAPFTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
      }

      protected void E11H12( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV25PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV25PageToGo) ;
         }
      }

      protected void E12H12( )
      {
         /* Ddo_solicitacaomudancaitem_funcaoapf_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_solicitacaomudancaitem_funcaoapf_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_solicitacaomudancaitem_funcaoapf_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_solicitacaomudancaitem_funcaoapf_Internalname, "SortedStatus", Ddo_solicitacaomudancaitem_funcaoapf_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_solicitacaomudancaitem_funcaoapf_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_solicitacaomudancaitem_funcaoapf_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_solicitacaomudancaitem_funcaoapf_Internalname, "SortedStatus", Ddo_solicitacaomudancaitem_funcaoapf_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_solicitacaomudancaitem_funcaoapf_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV21TFSolicitacaoMudancaItem_FuncaoAPF = (int)(NumberUtil.Val( Ddo_solicitacaomudancaitem_funcaoapf_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFSolicitacaoMudancaItem_FuncaoAPF", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21TFSolicitacaoMudancaItem_FuncaoAPF), 6, 0)));
            AV22TFSolicitacaoMudancaItem_FuncaoAPF_To = (int)(NumberUtil.Val( Ddo_solicitacaomudancaitem_funcaoapf_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFSolicitacaoMudancaItem_FuncaoAPF_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22TFSolicitacaoMudancaItem_FuncaoAPF_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
      }

      private void E17H12( )
      {
         /* Grid_Load Routine */
         AV15Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV15Update);
         AV30Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("solicitacaomudancaitem.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A996SolicitacaoMudanca_Codigo) + "," + UrlEncode("" +A995SolicitacaoMudancaItem_FuncaoAPF);
         AV16Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV16Delete);
         AV31Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("solicitacaomudancaitem.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A996SolicitacaoMudanca_Codigo) + "," + UrlEncode("" +A995SolicitacaoMudancaItem_FuncaoAPF);
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 25;
         }
         sendrow_252( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_25_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(25, GridRow);
         }
      }

      protected void E13H12( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E14H12( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("solicitacaomudancaitem.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +AV7SolicitacaoMudanca_Codigo) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         Ddo_solicitacaomudancaitem_funcaoapf_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_solicitacaomudancaitem_funcaoapf_Internalname, "SortedStatus", Ddo_solicitacaomudancaitem_funcaoapf_Sortedstatus);
      }

      protected void S142( )
      {
         /* 'CLEANFILTERS' Routine */
         AV21TFSolicitacaoMudancaItem_FuncaoAPF = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFSolicitacaoMudancaItem_FuncaoAPF", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21TFSolicitacaoMudancaItem_FuncaoAPF), 6, 0)));
         Ddo_solicitacaomudancaitem_funcaoapf_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_solicitacaomudancaitem_funcaoapf_Internalname, "FilteredText_set", Ddo_solicitacaomudancaitem_funcaoapf_Filteredtext_set);
         AV22TFSolicitacaoMudancaItem_FuncaoAPF_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFSolicitacaoMudancaItem_FuncaoAPF_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22TFSolicitacaoMudancaItem_FuncaoAPF_To), 6, 0)));
         Ddo_solicitacaomudancaitem_funcaoapf_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_solicitacaomudancaitem_funcaoapf_Internalname, "FilteredTextTo_set", Ddo_solicitacaomudancaitem_funcaoapf_Filteredtextto_set);
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV17Session.Get(AV32Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV32Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV17Session.Get(AV32Pgmname+"GridState"), "");
         }
         AV14OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S162( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV33GXV1 = 1;
         while ( AV33GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV33GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSOLICITACAOMUDANCAITEM_FUNCAOAPF") == 0 )
            {
               AV21TFSolicitacaoMudancaItem_FuncaoAPF = (int)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFSolicitacaoMudancaItem_FuncaoAPF", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21TFSolicitacaoMudancaItem_FuncaoAPF), 6, 0)));
               AV22TFSolicitacaoMudancaItem_FuncaoAPF_To = (int)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFSolicitacaoMudancaItem_FuncaoAPF_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22TFSolicitacaoMudancaItem_FuncaoAPF_To), 6, 0)));
               if ( ! (0==AV21TFSolicitacaoMudancaItem_FuncaoAPF) )
               {
                  Ddo_solicitacaomudancaitem_funcaoapf_Filteredtext_set = StringUtil.Str( (decimal)(AV21TFSolicitacaoMudancaItem_FuncaoAPF), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_solicitacaomudancaitem_funcaoapf_Internalname, "FilteredText_set", Ddo_solicitacaomudancaitem_funcaoapf_Filteredtext_set);
               }
               if ( ! (0==AV22TFSolicitacaoMudancaItem_FuncaoAPF_To) )
               {
                  Ddo_solicitacaomudancaitem_funcaoapf_Filteredtextto_set = StringUtil.Str( (decimal)(AV22TFSolicitacaoMudancaItem_FuncaoAPF_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_solicitacaomudancaitem_funcaoapf_Internalname, "FilteredTextTo_set", Ddo_solicitacaomudancaitem_funcaoapf_Filteredtextto_set);
               }
            }
            AV33GXV1 = (int)(AV33GXV1+1);
         }
      }

      protected void S132( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV17Session.Get(AV32Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV21TFSolicitacaoMudancaItem_FuncaoAPF) && (0==AV22TFSolicitacaoMudancaItem_FuncaoAPF_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSOLICITACAOMUDANCAITEM_FUNCAOAPF";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV21TFSolicitacaoMudancaItem_FuncaoAPF), 6, 0);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV22TFSolicitacaoMudancaItem_FuncaoAPF_To), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV32Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV32Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "SolicitacaoMudancaItem";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "SolicitacaoMudanca_Codigo";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7SolicitacaoMudanca_Codigo), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV17Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_H12( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableSearchCell'>") ;
            wb_table2_8_H12( true) ;
         }
         else
         {
            wb_table2_8_H12( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_H12e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_22_H12( true) ;
         }
         else
         {
            wb_table3_22_H12( false) ;
         }
         return  ;
      }

      protected void wb_table3_22_H12e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_H12e( true) ;
         }
         else
         {
            wb_table1_2_H12e( false) ;
         }
      }

      protected void wb_table3_22_H12( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"25\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(46), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSolicitacaoMudancaItem_FuncaoAPF_Titleformat == 0 )
               {
                  context.SendWebValue( edtSolicitacaoMudancaItem_FuncaoAPF_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSolicitacaoMudancaItem_FuncaoAPF_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV15Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV16Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A995SolicitacaoMudancaItem_FuncaoAPF), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSolicitacaoMudancaItem_FuncaoAPF_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSolicitacaoMudancaItem_FuncaoAPF_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 25 )
         {
            wbEnd = 0;
            nRC_GXsfl_25 = (short)(nGXsfl_25_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_22_H12e( true) ;
         }
         else
         {
            wb_table3_22_H12e( false) ;
         }
      }

      protected void wb_table2_8_H12( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='Width100'>") ;
            wb_table4_11_H12( true) ;
         }
         else
         {
            wb_table4_11_H12( false) ;
         }
         return  ;
      }

      protected void wb_table4_11_H12e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_16_H12( true) ;
         }
         else
         {
            wb_table5_16_H12( false) ;
         }
         return  ;
      }

      protected void wb_table5_16_H12e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_H12e( true) ;
         }
         else
         {
            wb_table2_8_H12e( false) ;
         }
      }

      protected void wb_table5_16_H12( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SolicitacaoMudancaSolicitacaoMudancaItemWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_16_H12e( true) ;
         }
         else
         {
            wb_table5_16_H12e( false) ;
         }
      }

      protected void wb_table4_11_H12( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SolicitacaoMudancaSolicitacaoMudancaItemWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_11_H12e( true) ;
         }
         else
         {
            wb_table4_11_H12e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7SolicitacaoMudanca_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7SolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7SolicitacaoMudanca_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAH12( ) ;
         WSH12( ) ;
         WEH12( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7SolicitacaoMudanca_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAH12( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "solicitacaomudancasolicitacaomudancaitemwc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAH12( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7SolicitacaoMudanca_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7SolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7SolicitacaoMudanca_Codigo), 6, 0)));
         }
         wcpOAV7SolicitacaoMudanca_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7SolicitacaoMudanca_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7SolicitacaoMudanca_Codigo != wcpOAV7SolicitacaoMudanca_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV7SolicitacaoMudanca_Codigo = AV7SolicitacaoMudanca_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7SolicitacaoMudanca_Codigo = cgiGet( sPrefix+"AV7SolicitacaoMudanca_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV7SolicitacaoMudanca_Codigo) > 0 )
         {
            AV7SolicitacaoMudanca_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7SolicitacaoMudanca_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7SolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7SolicitacaoMudanca_Codigo), 6, 0)));
         }
         else
         {
            AV7SolicitacaoMudanca_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7SolicitacaoMudanca_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAH12( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSH12( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSH12( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7SolicitacaoMudanca_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7SolicitacaoMudanca_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7SolicitacaoMudanca_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7SolicitacaoMudanca_Codigo_CTRL", StringUtil.RTrim( sCtrlAV7SolicitacaoMudanca_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEH12( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117261226");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("solicitacaomudancasolicitacaomudancaitemwc.js", "?20203117261226");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_252( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_25_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_25_idx;
         edtSolicitacaoMudancaItem_FuncaoAPF_Internalname = sPrefix+"SOLICITACAOMUDANCAITEM_FUNCAOAPF_"+sGXsfl_25_idx;
         Solicitacaomudancaitem_descricao_Internalname = sPrefix+"SOLICITACAOMUDANCAITEM_DESCRICAO_"+sGXsfl_25_idx;
      }

      protected void SubsflControlProps_fel_252( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_25_fel_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_25_fel_idx;
         edtSolicitacaoMudancaItem_FuncaoAPF_Internalname = sPrefix+"SOLICITACAOMUDANCAITEM_FUNCAOAPF_"+sGXsfl_25_fel_idx;
         Solicitacaomudancaitem_descricao_Internalname = sPrefix+"SOLICITACAOMUDANCAITEM_DESCRICAO_"+sGXsfl_25_fel_idx;
      }

      protected void sendrow_252( )
      {
         SubsflControlProps_252( ) ;
         WBH10( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_25_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_25_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_25_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV15Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV15Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV30Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV15Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV15Update)) ? AV30Update_GXI : context.PathToRelativeUrl( AV15Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV15Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV16Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV16Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV31Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV16Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV16Delete)) ? AV31Delete_GXI : context.PathToRelativeUrl( AV16Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV16Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSolicitacaoMudancaItem_FuncaoAPF_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A995SolicitacaoMudancaItem_FuncaoAPF), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A995SolicitacaoMudancaItem_FuncaoAPF), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSolicitacaoMudancaItem_FuncaoAPF_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)46,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)25,(short)1,(short)0,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td>") ;
            }
            /* User Defined Control */
            GridRow.AddColumnProperties("usercontrol", -1, isAjaxCallMode( ), new Object[] {(String)sPrefix+"SOLICITACAOMUDANCAITEM_DESCRICAOContainer"+"_"+sGXsfl_25_idx,(short)1});
            GXCCtl = "SOLICITACAOMUDANCAITEM_DESCRICAO_" + sGXsfl_25_idx;
            GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, A998SolicitacaoMudancaItem_Descricao);
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SOLICITACAOMUDANCAITEM_FUNCAOAPF"+"_"+sGXsfl_25_idx, GetSecureSignedToken( sPrefix+sGXsfl_25_idx, context.localUtil.Format( (decimal)(A995SolicitacaoMudancaItem_FuncaoAPF), "ZZZZZ9")));
            GXCCtl = "SOLICITACAOMUDANCAITEM_DESCRICAO_Enabled_" + sGXsfl_25_idx;
            GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.BoolToStr( Solicitacaomudancaitem_descricao_Enabled));
            GridContainer.AddRow(GridRow);
            nGXsfl_25_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_25_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_25_idx+1));
            sGXsfl_25_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_25_idx), 4, 0)), 4, "0");
            SubsflControlProps_252( ) ;
         }
         /* End function sendrow_252 */
      }

      protected void init_default_properties( )
      {
         imgInsert_Internalname = sPrefix+"INSERT";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         imgCleanfilters_Internalname = sPrefix+"CLEANFILTERS";
         tblTablefilters_Internalname = sPrefix+"TABLEFILTERS";
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         edtavUpdate_Internalname = sPrefix+"vUPDATE";
         edtavDelete_Internalname = sPrefix+"vDELETE";
         edtSolicitacaoMudancaItem_FuncaoAPF_Internalname = sPrefix+"SOLICITACAOMUDANCAITEM_FUNCAOAPF";
         Solicitacaomudancaitem_descricao_Internalname = sPrefix+"SOLICITACAOMUDANCAITEM_DESCRICAO";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         edtSolicitacaoMudanca_Codigo_Internalname = sPrefix+"SOLICITACAOMUDANCA_CODIGO";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         edtavTfsolicitacaomudancaitem_funcaoapf_Internalname = sPrefix+"vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF";
         edtavTfsolicitacaomudancaitem_funcaoapf_to_Internalname = sPrefix+"vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF_TO";
         Ddo_solicitacaomudancaitem_funcaoapf_Internalname = sPrefix+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF";
         edtavDdo_solicitacaomudancaitem_funcaoapftitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_SOLICITACAOMUDANCAITEM_FUNCAOAPFTITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         Solicitacaomudancaitem_descricao_Enabled = Convert.ToBoolean( 0);
         edtSolicitacaoMudancaItem_FuncaoAPF_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtSolicitacaoMudancaItem_FuncaoAPF_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtSolicitacaoMudancaItem_FuncaoAPF_Title = "Item";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_solicitacaomudancaitem_funcaoapftitlecontrolidtoreplace_Visible = 1;
         edtavTfsolicitacaomudancaitem_funcaoapf_to_Jsonclick = "";
         edtavTfsolicitacaomudancaitem_funcaoapf_to_Visible = 1;
         edtavTfsolicitacaomudancaitem_funcaoapf_Jsonclick = "";
         edtavTfsolicitacaomudancaitem_funcaoapf_Visible = 1;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         edtSolicitacaoMudanca_Codigo_Jsonclick = "";
         edtSolicitacaoMudanca_Codigo_Visible = 1;
         Ddo_solicitacaomudancaitem_funcaoapf_Searchbuttontext = "Pesquisar";
         Ddo_solicitacaomudancaitem_funcaoapf_Rangefilterto = "At�";
         Ddo_solicitacaomudancaitem_funcaoapf_Rangefilterfrom = "Desde";
         Ddo_solicitacaomudancaitem_funcaoapf_Cleanfilter = "Limpar pesquisa";
         Ddo_solicitacaomudancaitem_funcaoapf_Sortdsc = "Ordenar de Z � A";
         Ddo_solicitacaomudancaitem_funcaoapf_Sortasc = "Ordenar de A � Z";
         Ddo_solicitacaomudancaitem_funcaoapf_Includedatalist = Convert.ToBoolean( 0);
         Ddo_solicitacaomudancaitem_funcaoapf_Filterisrange = Convert.ToBoolean( -1);
         Ddo_solicitacaomudancaitem_funcaoapf_Filtertype = "Numeric";
         Ddo_solicitacaomudancaitem_funcaoapf_Includefilter = Convert.ToBoolean( -1);
         Ddo_solicitacaomudancaitem_funcaoapf_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_solicitacaomudancaitem_funcaoapf_Includesortasc = Convert.ToBoolean( -1);
         Ddo_solicitacaomudancaitem_funcaoapf_Titlecontrolidtoreplace = "";
         Ddo_solicitacaomudancaitem_funcaoapf_Dropdownoptionstype = "GridTitleSettings";
         Ddo_solicitacaomudancaitem_funcaoapf_Cls = "ColumnSettings";
         Ddo_solicitacaomudancaitem_funcaoapf_Tooltip = "Op��es";
         Ddo_solicitacaomudancaitem_funcaoapf_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV7SolicitacaoMudanca_Codigo',fld:'vSOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A996SolicitacaoMudanca_Codigo',fld:'SOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A995SolicitacaoMudancaItem_FuncaoAPF',fld:'SOLICITACAOMUDANCAITEM_FUNCAOAPF',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'AV23ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCAITEM_FUNCAOAPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV21TFSolicitacaoMudancaItem_FuncaoAPF',fld:'vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF',pic:'ZZZZZ9',nv:0},{av:'AV22TFSolicitacaoMudancaItem_FuncaoAPF_To',fld:'vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF_TO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV20SolicitacaoMudancaItem_FuncaoAPFTitleFilterData',fld:'vSOLICITACAOMUDANCAITEM_FUNCAOAPFTITLEFILTERDATA',pic:'',nv:null},{av:'edtSolicitacaoMudancaItem_FuncaoAPF_Titleformat',ctrl:'SOLICITACAOMUDANCAITEM_FUNCAOAPF',prop:'Titleformat'},{av:'edtSolicitacaoMudancaItem_FuncaoAPF_Title',ctrl:'SOLICITACAOMUDANCAITEM_FUNCAOAPF',prop:'Title'},{av:'AV26GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV27GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11H12',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV21TFSolicitacaoMudancaItem_FuncaoAPF',fld:'vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF',pic:'ZZZZZ9',nv:0},{av:'AV22TFSolicitacaoMudancaItem_FuncaoAPF_To',fld:'vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF_TO',pic:'ZZZZZ9',nv:0},{av:'AV7SolicitacaoMudanca_Codigo',fld:'vSOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV23ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCAITEM_FUNCAOAPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A996SolicitacaoMudanca_Codigo',fld:'SOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A995SolicitacaoMudancaItem_FuncaoAPF',fld:'SOLICITACAOMUDANCAITEM_FUNCAOAPF',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF.ONOPTIONCLICKED","{handler:'E12H12',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV21TFSolicitacaoMudancaItem_FuncaoAPF',fld:'vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF',pic:'ZZZZZ9',nv:0},{av:'AV22TFSolicitacaoMudancaItem_FuncaoAPF_To',fld:'vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF_TO',pic:'ZZZZZ9',nv:0},{av:'AV7SolicitacaoMudanca_Codigo',fld:'vSOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV23ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCAITEM_FUNCAOAPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A996SolicitacaoMudanca_Codigo',fld:'SOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A995SolicitacaoMudancaItem_FuncaoAPF',fld:'SOLICITACAOMUDANCAITEM_FUNCAOAPF',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Ddo_solicitacaomudancaitem_funcaoapf_Activeeventkey',ctrl:'DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF',prop:'ActiveEventKey'},{av:'Ddo_solicitacaomudancaitem_funcaoapf_Filteredtext_get',ctrl:'DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF',prop:'FilteredText_get'},{av:'Ddo_solicitacaomudancaitem_funcaoapf_Filteredtextto_get',ctrl:'DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF',prop:'FilteredTextTo_get'}],oparms:[{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_solicitacaomudancaitem_funcaoapf_Sortedstatus',ctrl:'DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF',prop:'SortedStatus'},{av:'AV21TFSolicitacaoMudancaItem_FuncaoAPF',fld:'vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF',pic:'ZZZZZ9',nv:0},{av:'AV22TFSolicitacaoMudancaItem_FuncaoAPF_To',fld:'vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF_TO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("GRID.LOAD","{handler:'E17H12',iparms:[{av:'A996SolicitacaoMudanca_Codigo',fld:'SOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A995SolicitacaoMudancaItem_FuncaoAPF',fld:'SOLICITACAOMUDANCAITEM_FUNCAOAPF',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV15Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV16Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E13H12',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV21TFSolicitacaoMudancaItem_FuncaoAPF',fld:'vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF',pic:'ZZZZZ9',nv:0},{av:'AV22TFSolicitacaoMudancaItem_FuncaoAPF_To',fld:'vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF_TO',pic:'ZZZZZ9',nv:0},{av:'AV7SolicitacaoMudanca_Codigo',fld:'vSOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV23ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCAITEM_FUNCAOAPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A996SolicitacaoMudanca_Codigo',fld:'SOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A995SolicitacaoMudancaItem_FuncaoAPF',fld:'SOLICITACAOMUDANCAITEM_FUNCAOAPF',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV21TFSolicitacaoMudancaItem_FuncaoAPF',fld:'vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF',pic:'ZZZZZ9',nv:0},{av:'Ddo_solicitacaomudancaitem_funcaoapf_Filteredtext_set',ctrl:'DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF',prop:'FilteredText_set'},{av:'AV22TFSolicitacaoMudancaItem_FuncaoAPF_To',fld:'vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_solicitacaomudancaitem_funcaoapf_Filteredtextto_set',ctrl:'DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF',prop:'FilteredTextTo_set'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E14H12',iparms:[{av:'AV7SolicitacaoMudanca_Codigo',fld:'vSOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A995SolicitacaoMudancaItem_FuncaoAPF',fld:'SOLICITACAOMUDANCAITEM_FUNCAOAPF',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_solicitacaomudancaitem_funcaoapf_Activeeventkey = "";
         Ddo_solicitacaomudancaitem_funcaoapf_Filteredtext_get = "";
         Ddo_solicitacaomudancaitem_funcaoapf_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV23ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace = "";
         AV32Pgmname = "";
         GXKey = "";
         forbiddenHiddens = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV24DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV20SolicitacaoMudancaItem_FuncaoAPFTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_solicitacaomudancaitem_funcaoapf_Filteredtext_set = "";
         Ddo_solicitacaomudancaitem_funcaoapf_Filteredtextto_set = "";
         Ddo_solicitacaomudancaitem_funcaoapf_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         GXCCtl = "";
         AV15Update = "";
         AV30Update_GXI = "";
         AV16Delete = "";
         AV31Delete_GXI = "";
         A998SolicitacaoMudancaItem_Descricao = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         H00H12_A998SolicitacaoMudancaItem_Descricao = new String[] {""} ;
         H00H12_A996SolicitacaoMudanca_Codigo = new int[1] ;
         H00H12_A995SolicitacaoMudancaItem_FuncaoAPF = new int[1] ;
         H00H13_AGRID_nRecordCount = new long[1] ;
         hsh = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GridRow = new GXWebRow();
         AV17Session = context.GetSession();
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         imgCleanfilters_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7SolicitacaoMudanca_Codigo = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.solicitacaomudancasolicitacaomudancaitemwc__default(),
            new Object[][] {
                new Object[] {
               H00H12_A998SolicitacaoMudancaItem_Descricao, H00H12_A996SolicitacaoMudanca_Codigo, H00H12_A995SolicitacaoMudancaItem_FuncaoAPF
               }
               , new Object[] {
               H00H13_AGRID_nRecordCount
               }
            }
         );
         AV32Pgmname = "SolicitacaoMudancaSolicitacaoMudancaItemWC";
         /* GeneXus formulas. */
         AV32Pgmname = "SolicitacaoMudancaSolicitacaoMudancaItemWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_25 ;
      private short nGXsfl_25_idx=1 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_25_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtSolicitacaoMudancaItem_FuncaoAPF_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7SolicitacaoMudanca_Codigo ;
      private int wcpOAV7SolicitacaoMudanca_Codigo ;
      private int subGrid_Rows ;
      private int AV21TFSolicitacaoMudancaItem_FuncaoAPF ;
      private int AV22TFSolicitacaoMudancaItem_FuncaoAPF_To ;
      private int A996SolicitacaoMudanca_Codigo ;
      private int A995SolicitacaoMudancaItem_FuncaoAPF ;
      private int Gridpaginationbar_Pagestoshow ;
      private int edtSolicitacaoMudanca_Codigo_Visible ;
      private int edtavOrdereddsc_Visible ;
      private int edtavTfsolicitacaomudancaitem_funcaoapf_Visible ;
      private int edtavTfsolicitacaomudancaitem_funcaoapf_to_Visible ;
      private int edtavDdo_solicitacaomudancaitem_funcaoapftitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV25PageToGo ;
      private int AV33GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV26GridCurrentPage ;
      private long AV27GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_solicitacaomudancaitem_funcaoapf_Activeeventkey ;
      private String Ddo_solicitacaomudancaitem_funcaoapf_Filteredtext_get ;
      private String Ddo_solicitacaomudancaitem_funcaoapf_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_25_idx="0001" ;
      private String AV32Pgmname ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_solicitacaomudancaitem_funcaoapf_Caption ;
      private String Ddo_solicitacaomudancaitem_funcaoapf_Tooltip ;
      private String Ddo_solicitacaomudancaitem_funcaoapf_Cls ;
      private String Ddo_solicitacaomudancaitem_funcaoapf_Filteredtext_set ;
      private String Ddo_solicitacaomudancaitem_funcaoapf_Filteredtextto_set ;
      private String Ddo_solicitacaomudancaitem_funcaoapf_Dropdownoptionstype ;
      private String Ddo_solicitacaomudancaitem_funcaoapf_Titlecontrolidtoreplace ;
      private String Ddo_solicitacaomudancaitem_funcaoapf_Sortedstatus ;
      private String Ddo_solicitacaomudancaitem_funcaoapf_Filtertype ;
      private String Ddo_solicitacaomudancaitem_funcaoapf_Sortasc ;
      private String Ddo_solicitacaomudancaitem_funcaoapf_Sortdsc ;
      private String Ddo_solicitacaomudancaitem_funcaoapf_Cleanfilter ;
      private String Ddo_solicitacaomudancaitem_funcaoapf_Rangefilterfrom ;
      private String Ddo_solicitacaomudancaitem_funcaoapf_Rangefilterto ;
      private String Ddo_solicitacaomudancaitem_funcaoapf_Searchbuttontext ;
      private String GX_FocusControl ;
      private String edtSolicitacaoMudanca_Codigo_Internalname ;
      private String edtSolicitacaoMudanca_Codigo_Jsonclick ;
      private String TempTags ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String edtavTfsolicitacaomudancaitem_funcaoapf_Internalname ;
      private String edtavTfsolicitacaomudancaitem_funcaoapf_Jsonclick ;
      private String edtavTfsolicitacaomudancaitem_funcaoapf_to_Internalname ;
      private String edtavTfsolicitacaomudancaitem_funcaoapf_to_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_solicitacaomudancaitem_funcaoapftitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String GXCCtl ;
      private String Solicitacaomudancaitem_descricao_Internalname ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtSolicitacaoMudancaItem_FuncaoAPF_Internalname ;
      private String scmdbuf ;
      private String hsh ;
      private String subGrid_Internalname ;
      private String Ddo_solicitacaomudancaitem_funcaoapf_Internalname ;
      private String edtSolicitacaoMudancaItem_FuncaoAPF_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String tblTablefilters_Internalname ;
      private String imgCleanfilters_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sCtrlAV7SolicitacaoMudanca_Codigo ;
      private String sGXsfl_25_fel_idx="0001" ;
      private String ROClassString ;
      private String edtSolicitacaoMudancaItem_FuncaoAPF_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_solicitacaomudancaitem_funcaoapf_Includesortasc ;
      private bool Ddo_solicitacaomudancaitem_funcaoapf_Includesortdsc ;
      private bool Ddo_solicitacaomudancaitem_funcaoapf_Includefilter ;
      private bool Ddo_solicitacaomudancaitem_funcaoapf_Filterisrange ;
      private bool Ddo_solicitacaomudancaitem_funcaoapf_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool Solicitacaomudancaitem_descricao_Enabled ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV15Update_IsBlob ;
      private bool AV16Delete_IsBlob ;
      private String A998SolicitacaoMudancaItem_Descricao ;
      private String AV23ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace ;
      private String AV30Update_GXI ;
      private String AV31Delete_GXI ;
      private String AV15Update ;
      private String AV16Delete ;
      private IGxSession AV17Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] H00H12_A998SolicitacaoMudancaItem_Descricao ;
      private int[] H00H12_A996SolicitacaoMudanca_Codigo ;
      private int[] H00H12_A995SolicitacaoMudancaItem_FuncaoAPF ;
      private long[] H00H13_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV20SolicitacaoMudancaItem_FuncaoAPFTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV24DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class solicitacaomudancasolicitacaomudancaitemwc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00H12( IGxContext context ,
                                             int AV21TFSolicitacaoMudancaItem_FuncaoAPF ,
                                             int AV22TFSolicitacaoMudancaItem_FuncaoAPF_To ,
                                             int A995SolicitacaoMudancaItem_FuncaoAPF ,
                                             bool AV14OrderedDsc ,
                                             int A996SolicitacaoMudanca_Codigo ,
                                             int AV7SolicitacaoMudanca_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [8] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [SolicitacaoMudancaItem_Descricao], [SolicitacaoMudanca_Codigo], [SolicitacaoMudancaItem_FuncaoAPF]";
         sFromString = " FROM [SolicitacaoMudancaItem] WITH (NOLOCK)";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE ([SolicitacaoMudanca_Codigo] = @AV7SolicitacaoMudanca_Codigo)";
         if ( ! (0==AV21TFSolicitacaoMudancaItem_FuncaoAPF) )
         {
            sWhereString = sWhereString + " and ([SolicitacaoMudancaItem_FuncaoAPF] >= @AV21TFSolicitacaoMudancaItem_FuncaoAPF)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ! (0==AV22TFSolicitacaoMudancaItem_FuncaoAPF_To) )
         {
            sWhereString = sWhereString + " and ([SolicitacaoMudancaItem_FuncaoAPF] <= @AV22TFSolicitacaoMudancaItem_FuncaoAPF_To)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [SolicitacaoMudanca_Codigo], [SolicitacaoMudancaItem_FuncaoAPF]";
         }
         else if ( AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [SolicitacaoMudanca_Codigo] DESC, [SolicitacaoMudancaItem_FuncaoAPF] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [SolicitacaoMudanca_Codigo], [SolicitacaoMudancaItem_FuncaoAPF]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00H13( IGxContext context ,
                                             int AV21TFSolicitacaoMudancaItem_FuncaoAPF ,
                                             int AV22TFSolicitacaoMudancaItem_FuncaoAPF_To ,
                                             int A995SolicitacaoMudancaItem_FuncaoAPF ,
                                             bool AV14OrderedDsc ,
                                             int A996SolicitacaoMudanca_Codigo ,
                                             int AV7SolicitacaoMudanca_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [3] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [SolicitacaoMudancaItem] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([SolicitacaoMudanca_Codigo] = @AV7SolicitacaoMudanca_Codigo)";
         if ( ! (0==AV21TFSolicitacaoMudancaItem_FuncaoAPF) )
         {
            sWhereString = sWhereString + " and ([SolicitacaoMudancaItem_FuncaoAPF] >= @AV21TFSolicitacaoMudancaItem_FuncaoAPF)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ! (0==AV22TFSolicitacaoMudancaItem_FuncaoAPF_To) )
         {
            sWhereString = sWhereString + " and ([SolicitacaoMudancaItem_FuncaoAPF] <= @AV22TFSolicitacaoMudancaItem_FuncaoAPF_To)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00H12(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (bool)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] );
               case 1 :
                     return conditional_H00H13(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (bool)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00H12 ;
          prmH00H12 = new Object[] {
          new Object[] {"@AV7SolicitacaoMudanca_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21TFSolicitacaoMudancaItem_FuncaoAPF",SqlDbType.Int,6,0} ,
          new Object[] {"@AV22TFSolicitacaoMudancaItem_FuncaoAPF_To",SqlDbType.Int,6,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00H13 ;
          prmH00H13 = new Object[] {
          new Object[] {"@AV7SolicitacaoMudanca_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21TFSolicitacaoMudancaItem_FuncaoAPF",SqlDbType.Int,6,0} ,
          new Object[] {"@AV22TFSolicitacaoMudancaItem_FuncaoAPF_To",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00H12", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00H12,11,0,true,false )
             ,new CursorDef("H00H13", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00H13,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                return;
       }
    }

 }

}
