/*
               File: GetWWContratoArquivosAnexosFilterData
        Description: Get WWContrato Arquivos Anexos Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:37.6
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwcontratoarquivosanexosfilterdata : GXProcedure
   {
      public getwwcontratoarquivosanexosfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwcontratoarquivosanexosfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV26DDOName = aP0_DDOName;
         this.AV24SearchTxt = aP1_SearchTxt;
         this.AV25SearchTxtTo = aP2_SearchTxtTo;
         this.AV30OptionsJson = "" ;
         this.AV33OptionsDescJson = "" ;
         this.AV35OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV30OptionsJson;
         aP4_OptionsDescJson=this.AV33OptionsDescJson;
         aP5_OptionIndexesJson=this.AV35OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV26DDOName = aP0_DDOName;
         this.AV24SearchTxt = aP1_SearchTxt;
         this.AV25SearchTxtTo = aP2_SearchTxtTo;
         this.AV30OptionsJson = "" ;
         this.AV33OptionsDescJson = "" ;
         this.AV35OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV30OptionsJson;
         aP4_OptionsDescJson=this.AV33OptionsDescJson;
         aP5_OptionIndexesJson=this.AV35OptionIndexesJson;
         return AV35OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwcontratoarquivosanexosfilterdata objgetwwcontratoarquivosanexosfilterdata;
         objgetwwcontratoarquivosanexosfilterdata = new getwwcontratoarquivosanexosfilterdata();
         objgetwwcontratoarquivosanexosfilterdata.AV26DDOName = aP0_DDOName;
         objgetwwcontratoarquivosanexosfilterdata.AV24SearchTxt = aP1_SearchTxt;
         objgetwwcontratoarquivosanexosfilterdata.AV25SearchTxtTo = aP2_SearchTxtTo;
         objgetwwcontratoarquivosanexosfilterdata.AV30OptionsJson = "" ;
         objgetwwcontratoarquivosanexosfilterdata.AV33OptionsDescJson = "" ;
         objgetwwcontratoarquivosanexosfilterdata.AV35OptionIndexesJson = "" ;
         objgetwwcontratoarquivosanexosfilterdata.context.SetSubmitInitialConfig(context);
         objgetwwcontratoarquivosanexosfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwcontratoarquivosanexosfilterdata);
         aP3_OptionsJson=this.AV30OptionsJson;
         aP4_OptionsDescJson=this.AV33OptionsDescJson;
         aP5_OptionIndexesJson=this.AV35OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwcontratoarquivosanexosfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV29Options = (IGxCollection)(new GxSimpleCollection());
         AV32OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV34OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV26DDOName), "DDO_CONTRATO_NUMERO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATO_NUMEROOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV26DDOName), "DDO_CONTRATADA_PESSOANOM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATADA_PESSOANOMOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV26DDOName), "DDO_CONTRATADA_PESSOACNPJ") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATADA_PESSOACNPJOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV26DDOName), "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOARQUIVOSANEXOS_DESCRICAOOPTIONS' */
            S151 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV26DDOName), "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOARQUIVOSANEXOS_NOMEARQOPTIONS' */
            S161 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV26DDOName), "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOARQUIVOSANEXOS_TIPOARQOPTIONS' */
            S171 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV30OptionsJson = AV29Options.ToJSonString(false);
         AV33OptionsDescJson = AV32OptionsDesc.ToJSonString(false);
         AV35OptionIndexesJson = AV34OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV37Session.Get("WWContratoArquivosAnexosGridState"), "") == 0 )
         {
            AV39GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWContratoArquivosAnexosGridState"), "");
         }
         else
         {
            AV39GridState.FromXml(AV37Session.Get("WWContratoArquivosAnexosGridState"), "");
         }
         AV61GXV1 = 1;
         while ( AV61GXV1 <= AV39GridState.gxTpr_Filtervalues.Count )
         {
            AV40GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV39GridState.gxTpr_Filtervalues.Item(AV61GXV1));
            if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO") == 0 )
            {
               AV10TFContrato_Numero = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO_SEL") == 0 )
            {
               AV11TFContrato_Numero_Sel = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOANOM") == 0 )
            {
               AV12TFContratada_PessoaNom = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOANOM_SEL") == 0 )
            {
               AV13TFContratada_PessoaNom_Sel = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOACNPJ") == 0 )
            {
               AV14TFContratada_PessoaCNPJ = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOACNPJ_SEL") == 0 )
            {
               AV15TFContratada_PessoaCNPJ_Sel = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 )
            {
               AV16TFContratoArquivosAnexos_Descricao = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL") == 0 )
            {
               AV17TFContratoArquivosAnexos_Descricao_Sel = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTRATOARQUIVOSANEXOS_NOMEARQ") == 0 )
            {
               AV18TFContratoArquivosAnexos_NomeArq = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTRATOARQUIVOSANEXOS_NOMEARQ_SEL") == 0 )
            {
               AV19TFContratoArquivosAnexos_NomeArq_Sel = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTRATOARQUIVOSANEXOS_TIPOARQ") == 0 )
            {
               AV20TFContratoArquivosAnexos_TipoArq = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTRATOARQUIVOSANEXOS_TIPOARQ_SEL") == 0 )
            {
               AV21TFContratoArquivosAnexos_TipoArq_Sel = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTRATOARQUIVOSANEXOS_DATA") == 0 )
            {
               AV22TFContratoArquivosAnexos_Data = context.localUtil.CToT( AV40GridStateFilterValue.gxTpr_Value, 2);
               AV23TFContratoArquivosAnexos_Data_To = context.localUtil.CToT( AV40GridStateFilterValue.gxTpr_Valueto, 2);
            }
            AV61GXV1 = (int)(AV61GXV1+1);
         }
         if ( AV39GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV41GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV39GridState.gxTpr_Dynamicfilters.Item(1));
            AV42DynamicFiltersSelector1 = AV41GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV42DynamicFiltersSelector1, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 )
            {
               AV43DynamicFiltersOperator1 = AV41GridStateDynamicFilter.gxTpr_Operator;
               AV44ContratoArquivosAnexos_Descricao1 = AV41GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV42DynamicFiltersSelector1, "CONTRATOARQUIVOSANEXOS_DATA") == 0 )
            {
               AV45ContratoArquivosAnexos_Data1 = context.localUtil.CToT( AV41GridStateDynamicFilter.gxTpr_Value, 2);
               AV46ContratoArquivosAnexos_Data_To1 = context.localUtil.CToT( AV41GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            if ( AV39GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV47DynamicFiltersEnabled2 = true;
               AV41GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV39GridState.gxTpr_Dynamicfilters.Item(2));
               AV48DynamicFiltersSelector2 = AV41GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV48DynamicFiltersSelector2, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 )
               {
                  AV49DynamicFiltersOperator2 = AV41GridStateDynamicFilter.gxTpr_Operator;
                  AV50ContratoArquivosAnexos_Descricao2 = AV41GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV48DynamicFiltersSelector2, "CONTRATOARQUIVOSANEXOS_DATA") == 0 )
               {
                  AV51ContratoArquivosAnexos_Data2 = context.localUtil.CToT( AV41GridStateDynamicFilter.gxTpr_Value, 2);
                  AV52ContratoArquivosAnexos_Data_To2 = context.localUtil.CToT( AV41GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATO_NUMEROOPTIONS' Routine */
         AV10TFContrato_Numero = AV24SearchTxt;
         AV11TFContrato_Numero_Sel = "";
         AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1 = AV42DynamicFiltersSelector1;
         AV64WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 = AV43DynamicFiltersOperator1;
         AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 = AV44ContratoArquivosAnexos_Descricao1;
         AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1 = AV45ContratoArquivosAnexos_Data1;
         AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1 = AV46ContratoArquivosAnexos_Data_To1;
         AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 = AV47DynamicFiltersEnabled2;
         AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2 = AV48DynamicFiltersSelector2;
         AV70WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 = AV49DynamicFiltersOperator2;
         AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 = AV50ContratoArquivosAnexos_Descricao2;
         AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2 = AV51ContratoArquivosAnexos_Data2;
         AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2 = AV52ContratoArquivosAnexos_Data_To2;
         AV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero = AV10TFContrato_Numero;
         AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel = AV11TFContrato_Numero_Sel;
         AV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom = AV12TFContratada_PessoaNom;
         AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel = AV13TFContratada_PessoaNom_Sel;
         AV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj = AV14TFContratada_PessoaCNPJ;
         AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel = AV15TFContratada_PessoaCNPJ_Sel;
         AV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao = AV16TFContratoArquivosAnexos_Descricao;
         AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel = AV17TFContratoArquivosAnexos_Descricao_Sel;
         AV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq = AV18TFContratoArquivosAnexos_NomeArq;
         AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel = AV19TFContratoArquivosAnexos_NomeArq_Sel;
         AV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq = AV20TFContratoArquivosAnexos_TipoArq;
         AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel = AV21TFContratoArquivosAnexos_TipoArq_Sel;
         AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data = AV22TFContratoArquivosAnexos_Data;
         AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to = AV23TFContratoArquivosAnexos_Data_To;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1 ,
                                              AV64WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 ,
                                              AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 ,
                                              AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1 ,
                                              AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1 ,
                                              AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 ,
                                              AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2 ,
                                              AV70WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 ,
                                              AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 ,
                                              AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2 ,
                                              AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2 ,
                                              AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel ,
                                              AV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero ,
                                              AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel ,
                                              AV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom ,
                                              AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel ,
                                              AV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj ,
                                              AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel ,
                                              AV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao ,
                                              AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel ,
                                              AV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq ,
                                              AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel ,
                                              AV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq ,
                                              AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data ,
                                              AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to ,
                                              A110ContratoArquivosAnexos_Descricao ,
                                              A113ContratoArquivosAnexos_Data ,
                                              A77Contrato_Numero ,
                                              A41Contratada_PessoaNom ,
                                              A42Contratada_PessoaCNPJ ,
                                              A112ContratoArquivosAnexos_NomeArq ,
                                              A109ContratoArquivosAnexos_TipoArq },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1), "%", "");
         lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1), "%", "");
         lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2), "%", "");
         lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2), "%", "");
         lV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero), 20, "%");
         lV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom = StringUtil.PadR( StringUtil.RTrim( AV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom), 100, "%");
         lV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj = StringUtil.Concat( StringUtil.RTrim( AV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj), "%", "");
         lV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao = StringUtil.Concat( StringUtil.RTrim( AV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao), "%", "");
         lV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq = StringUtil.PadR( StringUtil.RTrim( AV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq), 50, "%");
         lV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq = StringUtil.PadR( StringUtil.RTrim( AV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq), 10, "%");
         /* Using cursor P00J72 */
         pr_default.execute(0, new Object[] {lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1, lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1, AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1, AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1, lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2, lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2, AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2, AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2, lV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero, AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel, lV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom, AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel, lV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj, AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel, lV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao, AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel, lV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq, AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel, lV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq, AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel, AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data, AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKJ72 = false;
            A74Contrato_Codigo = P00J72_A74Contrato_Codigo[0];
            A39Contratada_Codigo = P00J72_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00J72_A40Contratada_PessoaCod[0];
            A77Contrato_Numero = P00J72_A77Contrato_Numero[0];
            A109ContratoArquivosAnexos_TipoArq = P00J72_A109ContratoArquivosAnexos_TipoArq[0];
            A112ContratoArquivosAnexos_NomeArq = P00J72_A112ContratoArquivosAnexos_NomeArq[0];
            A42Contratada_PessoaCNPJ = P00J72_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00J72_n42Contratada_PessoaCNPJ[0];
            A41Contratada_PessoaNom = P00J72_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00J72_n41Contratada_PessoaNom[0];
            A113ContratoArquivosAnexos_Data = P00J72_A113ContratoArquivosAnexos_Data[0];
            A110ContratoArquivosAnexos_Descricao = P00J72_A110ContratoArquivosAnexos_Descricao[0];
            A108ContratoArquivosAnexos_Codigo = P00J72_A108ContratoArquivosAnexos_Codigo[0];
            A39Contratada_Codigo = P00J72_A39Contratada_Codigo[0];
            A77Contrato_Numero = P00J72_A77Contrato_Numero[0];
            A40Contratada_PessoaCod = P00J72_A40Contratada_PessoaCod[0];
            A42Contratada_PessoaCNPJ = P00J72_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00J72_n42Contratada_PessoaCNPJ[0];
            A41Contratada_PessoaNom = P00J72_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00J72_n41Contratada_PessoaNom[0];
            AV36count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00J72_A77Contrato_Numero[0], A77Contrato_Numero) == 0 ) )
            {
               BRKJ72 = false;
               A74Contrato_Codigo = P00J72_A74Contrato_Codigo[0];
               A108ContratoArquivosAnexos_Codigo = P00J72_A108ContratoArquivosAnexos_Codigo[0];
               AV36count = (long)(AV36count+1);
               BRKJ72 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A77Contrato_Numero)) )
            {
               AV28Option = A77Contrato_Numero;
               AV29Options.Add(AV28Option, 0);
               AV34OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV36count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV29Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKJ72 )
            {
               BRKJ72 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATADA_PESSOANOMOPTIONS' Routine */
         AV12TFContratada_PessoaNom = AV24SearchTxt;
         AV13TFContratada_PessoaNom_Sel = "";
         AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1 = AV42DynamicFiltersSelector1;
         AV64WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 = AV43DynamicFiltersOperator1;
         AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 = AV44ContratoArquivosAnexos_Descricao1;
         AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1 = AV45ContratoArquivosAnexos_Data1;
         AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1 = AV46ContratoArquivosAnexos_Data_To1;
         AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 = AV47DynamicFiltersEnabled2;
         AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2 = AV48DynamicFiltersSelector2;
         AV70WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 = AV49DynamicFiltersOperator2;
         AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 = AV50ContratoArquivosAnexos_Descricao2;
         AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2 = AV51ContratoArquivosAnexos_Data2;
         AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2 = AV52ContratoArquivosAnexos_Data_To2;
         AV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero = AV10TFContrato_Numero;
         AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel = AV11TFContrato_Numero_Sel;
         AV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom = AV12TFContratada_PessoaNom;
         AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel = AV13TFContratada_PessoaNom_Sel;
         AV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj = AV14TFContratada_PessoaCNPJ;
         AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel = AV15TFContratada_PessoaCNPJ_Sel;
         AV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao = AV16TFContratoArquivosAnexos_Descricao;
         AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel = AV17TFContratoArquivosAnexos_Descricao_Sel;
         AV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq = AV18TFContratoArquivosAnexos_NomeArq;
         AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel = AV19TFContratoArquivosAnexos_NomeArq_Sel;
         AV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq = AV20TFContratoArquivosAnexos_TipoArq;
         AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel = AV21TFContratoArquivosAnexos_TipoArq_Sel;
         AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data = AV22TFContratoArquivosAnexos_Data;
         AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to = AV23TFContratoArquivosAnexos_Data_To;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1 ,
                                              AV64WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 ,
                                              AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 ,
                                              AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1 ,
                                              AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1 ,
                                              AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 ,
                                              AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2 ,
                                              AV70WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 ,
                                              AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 ,
                                              AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2 ,
                                              AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2 ,
                                              AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel ,
                                              AV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero ,
                                              AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel ,
                                              AV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom ,
                                              AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel ,
                                              AV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj ,
                                              AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel ,
                                              AV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao ,
                                              AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel ,
                                              AV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq ,
                                              AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel ,
                                              AV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq ,
                                              AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data ,
                                              AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to ,
                                              A110ContratoArquivosAnexos_Descricao ,
                                              A113ContratoArquivosAnexos_Data ,
                                              A77Contrato_Numero ,
                                              A41Contratada_PessoaNom ,
                                              A42Contratada_PessoaCNPJ ,
                                              A112ContratoArquivosAnexos_NomeArq ,
                                              A109ContratoArquivosAnexos_TipoArq },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1), "%", "");
         lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1), "%", "");
         lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2), "%", "");
         lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2), "%", "");
         lV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero), 20, "%");
         lV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom = StringUtil.PadR( StringUtil.RTrim( AV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom), 100, "%");
         lV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj = StringUtil.Concat( StringUtil.RTrim( AV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj), "%", "");
         lV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao = StringUtil.Concat( StringUtil.RTrim( AV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao), "%", "");
         lV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq = StringUtil.PadR( StringUtil.RTrim( AV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq), 50, "%");
         lV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq = StringUtil.PadR( StringUtil.RTrim( AV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq), 10, "%");
         /* Using cursor P00J73 */
         pr_default.execute(1, new Object[] {lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1, lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1, AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1, AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1, lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2, lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2, AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2, AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2, lV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero, AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel, lV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom, AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel, lV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj, AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel, lV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao, AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel, lV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq, AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel, lV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq, AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel, AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data, AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKJ74 = false;
            A74Contrato_Codigo = P00J73_A74Contrato_Codigo[0];
            A39Contratada_Codigo = P00J73_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00J73_A40Contratada_PessoaCod[0];
            A41Contratada_PessoaNom = P00J73_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00J73_n41Contratada_PessoaNom[0];
            A109ContratoArquivosAnexos_TipoArq = P00J73_A109ContratoArquivosAnexos_TipoArq[0];
            A112ContratoArquivosAnexos_NomeArq = P00J73_A112ContratoArquivosAnexos_NomeArq[0];
            A42Contratada_PessoaCNPJ = P00J73_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00J73_n42Contratada_PessoaCNPJ[0];
            A77Contrato_Numero = P00J73_A77Contrato_Numero[0];
            A113ContratoArquivosAnexos_Data = P00J73_A113ContratoArquivosAnexos_Data[0];
            A110ContratoArquivosAnexos_Descricao = P00J73_A110ContratoArquivosAnexos_Descricao[0];
            A108ContratoArquivosAnexos_Codigo = P00J73_A108ContratoArquivosAnexos_Codigo[0];
            A39Contratada_Codigo = P00J73_A39Contratada_Codigo[0];
            A77Contrato_Numero = P00J73_A77Contrato_Numero[0];
            A40Contratada_PessoaCod = P00J73_A40Contratada_PessoaCod[0];
            A41Contratada_PessoaNom = P00J73_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00J73_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = P00J73_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00J73_n42Contratada_PessoaCNPJ[0];
            AV36count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00J73_A41Contratada_PessoaNom[0], A41Contratada_PessoaNom) == 0 ) )
            {
               BRKJ74 = false;
               A74Contrato_Codigo = P00J73_A74Contrato_Codigo[0];
               A39Contratada_Codigo = P00J73_A39Contratada_Codigo[0];
               A40Contratada_PessoaCod = P00J73_A40Contratada_PessoaCod[0];
               A108ContratoArquivosAnexos_Codigo = P00J73_A108ContratoArquivosAnexos_Codigo[0];
               A39Contratada_Codigo = P00J73_A39Contratada_Codigo[0];
               A40Contratada_PessoaCod = P00J73_A40Contratada_PessoaCod[0];
               AV36count = (long)(AV36count+1);
               BRKJ74 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A41Contratada_PessoaNom)) )
            {
               AV28Option = A41Contratada_PessoaNom;
               AV29Options.Add(AV28Option, 0);
               AV34OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV36count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV29Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKJ74 )
            {
               BRKJ74 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADCONTRATADA_PESSOACNPJOPTIONS' Routine */
         AV14TFContratada_PessoaCNPJ = AV24SearchTxt;
         AV15TFContratada_PessoaCNPJ_Sel = "";
         AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1 = AV42DynamicFiltersSelector1;
         AV64WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 = AV43DynamicFiltersOperator1;
         AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 = AV44ContratoArquivosAnexos_Descricao1;
         AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1 = AV45ContratoArquivosAnexos_Data1;
         AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1 = AV46ContratoArquivosAnexos_Data_To1;
         AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 = AV47DynamicFiltersEnabled2;
         AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2 = AV48DynamicFiltersSelector2;
         AV70WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 = AV49DynamicFiltersOperator2;
         AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 = AV50ContratoArquivosAnexos_Descricao2;
         AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2 = AV51ContratoArquivosAnexos_Data2;
         AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2 = AV52ContratoArquivosAnexos_Data_To2;
         AV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero = AV10TFContrato_Numero;
         AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel = AV11TFContrato_Numero_Sel;
         AV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom = AV12TFContratada_PessoaNom;
         AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel = AV13TFContratada_PessoaNom_Sel;
         AV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj = AV14TFContratada_PessoaCNPJ;
         AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel = AV15TFContratada_PessoaCNPJ_Sel;
         AV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao = AV16TFContratoArquivosAnexos_Descricao;
         AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel = AV17TFContratoArquivosAnexos_Descricao_Sel;
         AV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq = AV18TFContratoArquivosAnexos_NomeArq;
         AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel = AV19TFContratoArquivosAnexos_NomeArq_Sel;
         AV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq = AV20TFContratoArquivosAnexos_TipoArq;
         AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel = AV21TFContratoArquivosAnexos_TipoArq_Sel;
         AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data = AV22TFContratoArquivosAnexos_Data;
         AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to = AV23TFContratoArquivosAnexos_Data_To;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1 ,
                                              AV64WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 ,
                                              AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 ,
                                              AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1 ,
                                              AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1 ,
                                              AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 ,
                                              AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2 ,
                                              AV70WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 ,
                                              AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 ,
                                              AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2 ,
                                              AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2 ,
                                              AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel ,
                                              AV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero ,
                                              AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel ,
                                              AV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom ,
                                              AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel ,
                                              AV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj ,
                                              AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel ,
                                              AV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao ,
                                              AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel ,
                                              AV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq ,
                                              AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel ,
                                              AV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq ,
                                              AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data ,
                                              AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to ,
                                              A110ContratoArquivosAnexos_Descricao ,
                                              A113ContratoArquivosAnexos_Data ,
                                              A77Contrato_Numero ,
                                              A41Contratada_PessoaNom ,
                                              A42Contratada_PessoaCNPJ ,
                                              A112ContratoArquivosAnexos_NomeArq ,
                                              A109ContratoArquivosAnexos_TipoArq },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1), "%", "");
         lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1), "%", "");
         lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2), "%", "");
         lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2), "%", "");
         lV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero), 20, "%");
         lV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom = StringUtil.PadR( StringUtil.RTrim( AV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom), 100, "%");
         lV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj = StringUtil.Concat( StringUtil.RTrim( AV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj), "%", "");
         lV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao = StringUtil.Concat( StringUtil.RTrim( AV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao), "%", "");
         lV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq = StringUtil.PadR( StringUtil.RTrim( AV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq), 50, "%");
         lV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq = StringUtil.PadR( StringUtil.RTrim( AV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq), 10, "%");
         /* Using cursor P00J74 */
         pr_default.execute(2, new Object[] {lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1, lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1, AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1, AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1, lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2, lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2, AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2, AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2, lV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero, AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel, lV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom, AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel, lV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj, AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel, lV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao, AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel, lV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq, AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel, lV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq, AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel, AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data, AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKJ76 = false;
            A74Contrato_Codigo = P00J74_A74Contrato_Codigo[0];
            A39Contratada_Codigo = P00J74_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00J74_A40Contratada_PessoaCod[0];
            A42Contratada_PessoaCNPJ = P00J74_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00J74_n42Contratada_PessoaCNPJ[0];
            A109ContratoArquivosAnexos_TipoArq = P00J74_A109ContratoArquivosAnexos_TipoArq[0];
            A112ContratoArquivosAnexos_NomeArq = P00J74_A112ContratoArquivosAnexos_NomeArq[0];
            A41Contratada_PessoaNom = P00J74_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00J74_n41Contratada_PessoaNom[0];
            A77Contrato_Numero = P00J74_A77Contrato_Numero[0];
            A113ContratoArquivosAnexos_Data = P00J74_A113ContratoArquivosAnexos_Data[0];
            A110ContratoArquivosAnexos_Descricao = P00J74_A110ContratoArquivosAnexos_Descricao[0];
            A108ContratoArquivosAnexos_Codigo = P00J74_A108ContratoArquivosAnexos_Codigo[0];
            A39Contratada_Codigo = P00J74_A39Contratada_Codigo[0];
            A77Contrato_Numero = P00J74_A77Contrato_Numero[0];
            A40Contratada_PessoaCod = P00J74_A40Contratada_PessoaCod[0];
            A42Contratada_PessoaCNPJ = P00J74_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00J74_n42Contratada_PessoaCNPJ[0];
            A41Contratada_PessoaNom = P00J74_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00J74_n41Contratada_PessoaNom[0];
            AV36count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00J74_A42Contratada_PessoaCNPJ[0], A42Contratada_PessoaCNPJ) == 0 ) )
            {
               BRKJ76 = false;
               A74Contrato_Codigo = P00J74_A74Contrato_Codigo[0];
               A39Contratada_Codigo = P00J74_A39Contratada_Codigo[0];
               A40Contratada_PessoaCod = P00J74_A40Contratada_PessoaCod[0];
               A108ContratoArquivosAnexos_Codigo = P00J74_A108ContratoArquivosAnexos_Codigo[0];
               A39Contratada_Codigo = P00J74_A39Contratada_Codigo[0];
               A40Contratada_PessoaCod = P00J74_A40Contratada_PessoaCod[0];
               AV36count = (long)(AV36count+1);
               BRKJ76 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A42Contratada_PessoaCNPJ)) )
            {
               AV28Option = A42Contratada_PessoaCNPJ;
               AV29Options.Add(AV28Option, 0);
               AV34OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV36count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV29Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKJ76 )
            {
               BRKJ76 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      protected void S151( )
      {
         /* 'LOADCONTRATOARQUIVOSANEXOS_DESCRICAOOPTIONS' Routine */
         AV16TFContratoArquivosAnexos_Descricao = AV24SearchTxt;
         AV17TFContratoArquivosAnexos_Descricao_Sel = "";
         AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1 = AV42DynamicFiltersSelector1;
         AV64WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 = AV43DynamicFiltersOperator1;
         AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 = AV44ContratoArquivosAnexos_Descricao1;
         AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1 = AV45ContratoArquivosAnexos_Data1;
         AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1 = AV46ContratoArquivosAnexos_Data_To1;
         AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 = AV47DynamicFiltersEnabled2;
         AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2 = AV48DynamicFiltersSelector2;
         AV70WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 = AV49DynamicFiltersOperator2;
         AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 = AV50ContratoArquivosAnexos_Descricao2;
         AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2 = AV51ContratoArquivosAnexos_Data2;
         AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2 = AV52ContratoArquivosAnexos_Data_To2;
         AV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero = AV10TFContrato_Numero;
         AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel = AV11TFContrato_Numero_Sel;
         AV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom = AV12TFContratada_PessoaNom;
         AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel = AV13TFContratada_PessoaNom_Sel;
         AV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj = AV14TFContratada_PessoaCNPJ;
         AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel = AV15TFContratada_PessoaCNPJ_Sel;
         AV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao = AV16TFContratoArquivosAnexos_Descricao;
         AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel = AV17TFContratoArquivosAnexos_Descricao_Sel;
         AV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq = AV18TFContratoArquivosAnexos_NomeArq;
         AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel = AV19TFContratoArquivosAnexos_NomeArq_Sel;
         AV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq = AV20TFContratoArquivosAnexos_TipoArq;
         AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel = AV21TFContratoArquivosAnexos_TipoArq_Sel;
         AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data = AV22TFContratoArquivosAnexos_Data;
         AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to = AV23TFContratoArquivosAnexos_Data_To;
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1 ,
                                              AV64WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 ,
                                              AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 ,
                                              AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1 ,
                                              AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1 ,
                                              AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 ,
                                              AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2 ,
                                              AV70WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 ,
                                              AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 ,
                                              AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2 ,
                                              AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2 ,
                                              AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel ,
                                              AV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero ,
                                              AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel ,
                                              AV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom ,
                                              AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel ,
                                              AV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj ,
                                              AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel ,
                                              AV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao ,
                                              AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel ,
                                              AV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq ,
                                              AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel ,
                                              AV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq ,
                                              AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data ,
                                              AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to ,
                                              A110ContratoArquivosAnexos_Descricao ,
                                              A113ContratoArquivosAnexos_Data ,
                                              A77Contrato_Numero ,
                                              A41Contratada_PessoaNom ,
                                              A42Contratada_PessoaCNPJ ,
                                              A112ContratoArquivosAnexos_NomeArq ,
                                              A109ContratoArquivosAnexos_TipoArq },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1), "%", "");
         lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1), "%", "");
         lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2), "%", "");
         lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2), "%", "");
         lV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero), 20, "%");
         lV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom = StringUtil.PadR( StringUtil.RTrim( AV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom), 100, "%");
         lV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj = StringUtil.Concat( StringUtil.RTrim( AV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj), "%", "");
         lV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao = StringUtil.Concat( StringUtil.RTrim( AV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao), "%", "");
         lV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq = StringUtil.PadR( StringUtil.RTrim( AV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq), 50, "%");
         lV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq = StringUtil.PadR( StringUtil.RTrim( AV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq), 10, "%");
         /* Using cursor P00J75 */
         pr_default.execute(3, new Object[] {lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1, lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1, AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1, AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1, lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2, lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2, AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2, AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2, lV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero, AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel, lV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom, AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel, lV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj, AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel, lV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao, AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel, lV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq, AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel, lV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq, AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel, AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data, AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to});
         while ( (pr_default.getStatus(3) != 101) )
         {
            BRKJ78 = false;
            A74Contrato_Codigo = P00J75_A74Contrato_Codigo[0];
            A39Contratada_Codigo = P00J75_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00J75_A40Contratada_PessoaCod[0];
            A110ContratoArquivosAnexos_Descricao = P00J75_A110ContratoArquivosAnexos_Descricao[0];
            A109ContratoArquivosAnexos_TipoArq = P00J75_A109ContratoArquivosAnexos_TipoArq[0];
            A112ContratoArquivosAnexos_NomeArq = P00J75_A112ContratoArquivosAnexos_NomeArq[0];
            A42Contratada_PessoaCNPJ = P00J75_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00J75_n42Contratada_PessoaCNPJ[0];
            A41Contratada_PessoaNom = P00J75_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00J75_n41Contratada_PessoaNom[0];
            A77Contrato_Numero = P00J75_A77Contrato_Numero[0];
            A113ContratoArquivosAnexos_Data = P00J75_A113ContratoArquivosAnexos_Data[0];
            A108ContratoArquivosAnexos_Codigo = P00J75_A108ContratoArquivosAnexos_Codigo[0];
            A39Contratada_Codigo = P00J75_A39Contratada_Codigo[0];
            A77Contrato_Numero = P00J75_A77Contrato_Numero[0];
            A40Contratada_PessoaCod = P00J75_A40Contratada_PessoaCod[0];
            A42Contratada_PessoaCNPJ = P00J75_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00J75_n42Contratada_PessoaCNPJ[0];
            A41Contratada_PessoaNom = P00J75_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00J75_n41Contratada_PessoaNom[0];
            AV36count = 0;
            while ( (pr_default.getStatus(3) != 101) && ( StringUtil.StrCmp(P00J75_A110ContratoArquivosAnexos_Descricao[0], A110ContratoArquivosAnexos_Descricao) == 0 ) )
            {
               BRKJ78 = false;
               A108ContratoArquivosAnexos_Codigo = P00J75_A108ContratoArquivosAnexos_Codigo[0];
               AV36count = (long)(AV36count+1);
               BRKJ78 = true;
               pr_default.readNext(3);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A110ContratoArquivosAnexos_Descricao)) )
            {
               AV28Option = A110ContratoArquivosAnexos_Descricao;
               AV29Options.Add(AV28Option, 0);
               AV34OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV36count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV29Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKJ78 )
            {
               BRKJ78 = true;
               pr_default.readNext(3);
            }
         }
         pr_default.close(3);
      }

      protected void S161( )
      {
         /* 'LOADCONTRATOARQUIVOSANEXOS_NOMEARQOPTIONS' Routine */
         AV18TFContratoArquivosAnexos_NomeArq = AV24SearchTxt;
         AV19TFContratoArquivosAnexos_NomeArq_Sel = "";
         AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1 = AV42DynamicFiltersSelector1;
         AV64WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 = AV43DynamicFiltersOperator1;
         AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 = AV44ContratoArquivosAnexos_Descricao1;
         AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1 = AV45ContratoArquivosAnexos_Data1;
         AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1 = AV46ContratoArquivosAnexos_Data_To1;
         AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 = AV47DynamicFiltersEnabled2;
         AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2 = AV48DynamicFiltersSelector2;
         AV70WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 = AV49DynamicFiltersOperator2;
         AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 = AV50ContratoArquivosAnexos_Descricao2;
         AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2 = AV51ContratoArquivosAnexos_Data2;
         AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2 = AV52ContratoArquivosAnexos_Data_To2;
         AV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero = AV10TFContrato_Numero;
         AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel = AV11TFContrato_Numero_Sel;
         AV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom = AV12TFContratada_PessoaNom;
         AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel = AV13TFContratada_PessoaNom_Sel;
         AV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj = AV14TFContratada_PessoaCNPJ;
         AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel = AV15TFContratada_PessoaCNPJ_Sel;
         AV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao = AV16TFContratoArquivosAnexos_Descricao;
         AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel = AV17TFContratoArquivosAnexos_Descricao_Sel;
         AV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq = AV18TFContratoArquivosAnexos_NomeArq;
         AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel = AV19TFContratoArquivosAnexos_NomeArq_Sel;
         AV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq = AV20TFContratoArquivosAnexos_TipoArq;
         AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel = AV21TFContratoArquivosAnexos_TipoArq_Sel;
         AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data = AV22TFContratoArquivosAnexos_Data;
         AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to = AV23TFContratoArquivosAnexos_Data_To;
         pr_default.dynParam(4, new Object[]{ new Object[]{
                                              AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1 ,
                                              AV64WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 ,
                                              AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 ,
                                              AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1 ,
                                              AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1 ,
                                              AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 ,
                                              AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2 ,
                                              AV70WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 ,
                                              AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 ,
                                              AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2 ,
                                              AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2 ,
                                              AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel ,
                                              AV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero ,
                                              AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel ,
                                              AV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom ,
                                              AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel ,
                                              AV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj ,
                                              AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel ,
                                              AV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao ,
                                              AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel ,
                                              AV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq ,
                                              AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel ,
                                              AV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq ,
                                              AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data ,
                                              AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to ,
                                              A110ContratoArquivosAnexos_Descricao ,
                                              A113ContratoArquivosAnexos_Data ,
                                              A77Contrato_Numero ,
                                              A41Contratada_PessoaNom ,
                                              A42Contratada_PessoaCNPJ ,
                                              A112ContratoArquivosAnexos_NomeArq ,
                                              A109ContratoArquivosAnexos_TipoArq },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1), "%", "");
         lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1), "%", "");
         lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2), "%", "");
         lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2), "%", "");
         lV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero), 20, "%");
         lV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom = StringUtil.PadR( StringUtil.RTrim( AV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom), 100, "%");
         lV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj = StringUtil.Concat( StringUtil.RTrim( AV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj), "%", "");
         lV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao = StringUtil.Concat( StringUtil.RTrim( AV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao), "%", "");
         lV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq = StringUtil.PadR( StringUtil.RTrim( AV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq), 50, "%");
         lV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq = StringUtil.PadR( StringUtil.RTrim( AV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq), 10, "%");
         /* Using cursor P00J76 */
         pr_default.execute(4, new Object[] {lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1, lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1, AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1, AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1, lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2, lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2, AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2, AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2, lV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero, AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel, lV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom, AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel, lV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj, AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel, lV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao, AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel, lV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq, AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel, lV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq, AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel, AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data, AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to});
         while ( (pr_default.getStatus(4) != 101) )
         {
            BRKJ710 = false;
            A74Contrato_Codigo = P00J76_A74Contrato_Codigo[0];
            A39Contratada_Codigo = P00J76_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00J76_A40Contratada_PessoaCod[0];
            A112ContratoArquivosAnexos_NomeArq = P00J76_A112ContratoArquivosAnexos_NomeArq[0];
            A109ContratoArquivosAnexos_TipoArq = P00J76_A109ContratoArquivosAnexos_TipoArq[0];
            A42Contratada_PessoaCNPJ = P00J76_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00J76_n42Contratada_PessoaCNPJ[0];
            A41Contratada_PessoaNom = P00J76_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00J76_n41Contratada_PessoaNom[0];
            A77Contrato_Numero = P00J76_A77Contrato_Numero[0];
            A113ContratoArquivosAnexos_Data = P00J76_A113ContratoArquivosAnexos_Data[0];
            A110ContratoArquivosAnexos_Descricao = P00J76_A110ContratoArquivosAnexos_Descricao[0];
            A108ContratoArquivosAnexos_Codigo = P00J76_A108ContratoArquivosAnexos_Codigo[0];
            A39Contratada_Codigo = P00J76_A39Contratada_Codigo[0];
            A77Contrato_Numero = P00J76_A77Contrato_Numero[0];
            A40Contratada_PessoaCod = P00J76_A40Contratada_PessoaCod[0];
            A42Contratada_PessoaCNPJ = P00J76_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00J76_n42Contratada_PessoaCNPJ[0];
            A41Contratada_PessoaNom = P00J76_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00J76_n41Contratada_PessoaNom[0];
            AV36count = 0;
            while ( (pr_default.getStatus(4) != 101) && ( StringUtil.StrCmp(P00J76_A112ContratoArquivosAnexos_NomeArq[0], A112ContratoArquivosAnexos_NomeArq) == 0 ) )
            {
               BRKJ710 = false;
               A108ContratoArquivosAnexos_Codigo = P00J76_A108ContratoArquivosAnexos_Codigo[0];
               AV36count = (long)(AV36count+1);
               BRKJ710 = true;
               pr_default.readNext(4);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A112ContratoArquivosAnexos_NomeArq)) )
            {
               AV28Option = A112ContratoArquivosAnexos_NomeArq;
               AV29Options.Add(AV28Option, 0);
               AV34OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV36count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV29Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKJ710 )
            {
               BRKJ710 = true;
               pr_default.readNext(4);
            }
         }
         pr_default.close(4);
      }

      protected void S171( )
      {
         /* 'LOADCONTRATOARQUIVOSANEXOS_TIPOARQOPTIONS' Routine */
         AV20TFContratoArquivosAnexos_TipoArq = AV24SearchTxt;
         AV21TFContratoArquivosAnexos_TipoArq_Sel = "";
         AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1 = AV42DynamicFiltersSelector1;
         AV64WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 = AV43DynamicFiltersOperator1;
         AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 = AV44ContratoArquivosAnexos_Descricao1;
         AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1 = AV45ContratoArquivosAnexos_Data1;
         AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1 = AV46ContratoArquivosAnexos_Data_To1;
         AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 = AV47DynamicFiltersEnabled2;
         AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2 = AV48DynamicFiltersSelector2;
         AV70WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 = AV49DynamicFiltersOperator2;
         AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 = AV50ContratoArquivosAnexos_Descricao2;
         AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2 = AV51ContratoArquivosAnexos_Data2;
         AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2 = AV52ContratoArquivosAnexos_Data_To2;
         AV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero = AV10TFContrato_Numero;
         AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel = AV11TFContrato_Numero_Sel;
         AV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom = AV12TFContratada_PessoaNom;
         AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel = AV13TFContratada_PessoaNom_Sel;
         AV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj = AV14TFContratada_PessoaCNPJ;
         AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel = AV15TFContratada_PessoaCNPJ_Sel;
         AV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao = AV16TFContratoArquivosAnexos_Descricao;
         AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel = AV17TFContratoArquivosAnexos_Descricao_Sel;
         AV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq = AV18TFContratoArquivosAnexos_NomeArq;
         AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel = AV19TFContratoArquivosAnexos_NomeArq_Sel;
         AV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq = AV20TFContratoArquivosAnexos_TipoArq;
         AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel = AV21TFContratoArquivosAnexos_TipoArq_Sel;
         AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data = AV22TFContratoArquivosAnexos_Data;
         AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to = AV23TFContratoArquivosAnexos_Data_To;
         pr_default.dynParam(5, new Object[]{ new Object[]{
                                              AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1 ,
                                              AV64WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 ,
                                              AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 ,
                                              AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1 ,
                                              AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1 ,
                                              AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 ,
                                              AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2 ,
                                              AV70WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 ,
                                              AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 ,
                                              AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2 ,
                                              AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2 ,
                                              AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel ,
                                              AV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero ,
                                              AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel ,
                                              AV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom ,
                                              AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel ,
                                              AV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj ,
                                              AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel ,
                                              AV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao ,
                                              AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel ,
                                              AV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq ,
                                              AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel ,
                                              AV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq ,
                                              AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data ,
                                              AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to ,
                                              A110ContratoArquivosAnexos_Descricao ,
                                              A113ContratoArquivosAnexos_Data ,
                                              A77Contrato_Numero ,
                                              A41Contratada_PessoaNom ,
                                              A42Contratada_PessoaCNPJ ,
                                              A112ContratoArquivosAnexos_NomeArq ,
                                              A109ContratoArquivosAnexos_TipoArq },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1), "%", "");
         lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1), "%", "");
         lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2), "%", "");
         lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2), "%", "");
         lV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero), 20, "%");
         lV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom = StringUtil.PadR( StringUtil.RTrim( AV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom), 100, "%");
         lV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj = StringUtil.Concat( StringUtil.RTrim( AV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj), "%", "");
         lV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao = StringUtil.Concat( StringUtil.RTrim( AV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao), "%", "");
         lV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq = StringUtil.PadR( StringUtil.RTrim( AV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq), 50, "%");
         lV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq = StringUtil.PadR( StringUtil.RTrim( AV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq), 10, "%");
         /* Using cursor P00J77 */
         pr_default.execute(5, new Object[] {lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1, lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1, AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1, AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1, lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2, lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2, AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2, AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2, lV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero, AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel, lV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom, AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel, lV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj, AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel, lV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao, AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel, lV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq, AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel, lV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq, AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel, AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data, AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to});
         while ( (pr_default.getStatus(5) != 101) )
         {
            BRKJ712 = false;
            A74Contrato_Codigo = P00J77_A74Contrato_Codigo[0];
            A39Contratada_Codigo = P00J77_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00J77_A40Contratada_PessoaCod[0];
            A109ContratoArquivosAnexos_TipoArq = P00J77_A109ContratoArquivosAnexos_TipoArq[0];
            A112ContratoArquivosAnexos_NomeArq = P00J77_A112ContratoArquivosAnexos_NomeArq[0];
            A42Contratada_PessoaCNPJ = P00J77_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00J77_n42Contratada_PessoaCNPJ[0];
            A41Contratada_PessoaNom = P00J77_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00J77_n41Contratada_PessoaNom[0];
            A77Contrato_Numero = P00J77_A77Contrato_Numero[0];
            A113ContratoArquivosAnexos_Data = P00J77_A113ContratoArquivosAnexos_Data[0];
            A110ContratoArquivosAnexos_Descricao = P00J77_A110ContratoArquivosAnexos_Descricao[0];
            A108ContratoArquivosAnexos_Codigo = P00J77_A108ContratoArquivosAnexos_Codigo[0];
            A39Contratada_Codigo = P00J77_A39Contratada_Codigo[0];
            A77Contrato_Numero = P00J77_A77Contrato_Numero[0];
            A40Contratada_PessoaCod = P00J77_A40Contratada_PessoaCod[0];
            A42Contratada_PessoaCNPJ = P00J77_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00J77_n42Contratada_PessoaCNPJ[0];
            A41Contratada_PessoaNom = P00J77_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00J77_n41Contratada_PessoaNom[0];
            AV36count = 0;
            while ( (pr_default.getStatus(5) != 101) && ( StringUtil.StrCmp(P00J77_A109ContratoArquivosAnexos_TipoArq[0], A109ContratoArquivosAnexos_TipoArq) == 0 ) )
            {
               BRKJ712 = false;
               A108ContratoArquivosAnexos_Codigo = P00J77_A108ContratoArquivosAnexos_Codigo[0];
               AV36count = (long)(AV36count+1);
               BRKJ712 = true;
               pr_default.readNext(5);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A109ContratoArquivosAnexos_TipoArq)) )
            {
               AV28Option = A109ContratoArquivosAnexos_TipoArq;
               AV29Options.Add(AV28Option, 0);
               AV34OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV36count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV29Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKJ712 )
            {
               BRKJ712 = true;
               pr_default.readNext(5);
            }
         }
         pr_default.close(5);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV29Options = new GxSimpleCollection();
         AV32OptionsDesc = new GxSimpleCollection();
         AV34OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV37Session = context.GetSession();
         AV39GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV40GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContrato_Numero = "";
         AV11TFContrato_Numero_Sel = "";
         AV12TFContratada_PessoaNom = "";
         AV13TFContratada_PessoaNom_Sel = "";
         AV14TFContratada_PessoaCNPJ = "";
         AV15TFContratada_PessoaCNPJ_Sel = "";
         AV16TFContratoArquivosAnexos_Descricao = "";
         AV17TFContratoArquivosAnexos_Descricao_Sel = "";
         AV18TFContratoArquivosAnexos_NomeArq = "";
         AV19TFContratoArquivosAnexos_NomeArq_Sel = "";
         AV20TFContratoArquivosAnexos_TipoArq = "";
         AV21TFContratoArquivosAnexos_TipoArq_Sel = "";
         AV22TFContratoArquivosAnexos_Data = (DateTime)(DateTime.MinValue);
         AV23TFContratoArquivosAnexos_Data_To = (DateTime)(DateTime.MinValue);
         AV41GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV42DynamicFiltersSelector1 = "";
         AV44ContratoArquivosAnexos_Descricao1 = "";
         AV45ContratoArquivosAnexos_Data1 = (DateTime)(DateTime.MinValue);
         AV46ContratoArquivosAnexos_Data_To1 = (DateTime)(DateTime.MinValue);
         AV48DynamicFiltersSelector2 = "";
         AV50ContratoArquivosAnexos_Descricao2 = "";
         AV51ContratoArquivosAnexos_Data2 = (DateTime)(DateTime.MinValue);
         AV52ContratoArquivosAnexos_Data_To2 = (DateTime)(DateTime.MinValue);
         AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1 = "";
         AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 = "";
         AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1 = (DateTime)(DateTime.MinValue);
         AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1 = (DateTime)(DateTime.MinValue);
         AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2 = "";
         AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 = "";
         AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2 = (DateTime)(DateTime.MinValue);
         AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2 = (DateTime)(DateTime.MinValue);
         AV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero = "";
         AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel = "";
         AV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom = "";
         AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel = "";
         AV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj = "";
         AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel = "";
         AV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao = "";
         AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel = "";
         AV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq = "";
         AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel = "";
         AV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq = "";
         AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel = "";
         AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data = (DateTime)(DateTime.MinValue);
         AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to = (DateTime)(DateTime.MinValue);
         scmdbuf = "";
         lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 = "";
         lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 = "";
         lV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero = "";
         lV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom = "";
         lV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj = "";
         lV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao = "";
         lV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq = "";
         lV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq = "";
         A110ContratoArquivosAnexos_Descricao = "";
         A113ContratoArquivosAnexos_Data = (DateTime)(DateTime.MinValue);
         A77Contrato_Numero = "";
         A41Contratada_PessoaNom = "";
         A42Contratada_PessoaCNPJ = "";
         A112ContratoArquivosAnexos_NomeArq = "";
         A109ContratoArquivosAnexos_TipoArq = "";
         P00J72_A74Contrato_Codigo = new int[1] ;
         P00J72_A39Contratada_Codigo = new int[1] ;
         P00J72_A40Contratada_PessoaCod = new int[1] ;
         P00J72_A77Contrato_Numero = new String[] {""} ;
         P00J72_A109ContratoArquivosAnexos_TipoArq = new String[] {""} ;
         P00J72_A112ContratoArquivosAnexos_NomeArq = new String[] {""} ;
         P00J72_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P00J72_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P00J72_A41Contratada_PessoaNom = new String[] {""} ;
         P00J72_n41Contratada_PessoaNom = new bool[] {false} ;
         P00J72_A113ContratoArquivosAnexos_Data = new DateTime[] {DateTime.MinValue} ;
         P00J72_A110ContratoArquivosAnexos_Descricao = new String[] {""} ;
         P00J72_A108ContratoArquivosAnexos_Codigo = new int[1] ;
         AV28Option = "";
         P00J73_A74Contrato_Codigo = new int[1] ;
         P00J73_A39Contratada_Codigo = new int[1] ;
         P00J73_A40Contratada_PessoaCod = new int[1] ;
         P00J73_A41Contratada_PessoaNom = new String[] {""} ;
         P00J73_n41Contratada_PessoaNom = new bool[] {false} ;
         P00J73_A109ContratoArquivosAnexos_TipoArq = new String[] {""} ;
         P00J73_A112ContratoArquivosAnexos_NomeArq = new String[] {""} ;
         P00J73_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P00J73_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P00J73_A77Contrato_Numero = new String[] {""} ;
         P00J73_A113ContratoArquivosAnexos_Data = new DateTime[] {DateTime.MinValue} ;
         P00J73_A110ContratoArquivosAnexos_Descricao = new String[] {""} ;
         P00J73_A108ContratoArquivosAnexos_Codigo = new int[1] ;
         P00J74_A74Contrato_Codigo = new int[1] ;
         P00J74_A39Contratada_Codigo = new int[1] ;
         P00J74_A40Contratada_PessoaCod = new int[1] ;
         P00J74_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P00J74_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P00J74_A109ContratoArquivosAnexos_TipoArq = new String[] {""} ;
         P00J74_A112ContratoArquivosAnexos_NomeArq = new String[] {""} ;
         P00J74_A41Contratada_PessoaNom = new String[] {""} ;
         P00J74_n41Contratada_PessoaNom = new bool[] {false} ;
         P00J74_A77Contrato_Numero = new String[] {""} ;
         P00J74_A113ContratoArquivosAnexos_Data = new DateTime[] {DateTime.MinValue} ;
         P00J74_A110ContratoArquivosAnexos_Descricao = new String[] {""} ;
         P00J74_A108ContratoArquivosAnexos_Codigo = new int[1] ;
         P00J75_A74Contrato_Codigo = new int[1] ;
         P00J75_A39Contratada_Codigo = new int[1] ;
         P00J75_A40Contratada_PessoaCod = new int[1] ;
         P00J75_A110ContratoArquivosAnexos_Descricao = new String[] {""} ;
         P00J75_A109ContratoArquivosAnexos_TipoArq = new String[] {""} ;
         P00J75_A112ContratoArquivosAnexos_NomeArq = new String[] {""} ;
         P00J75_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P00J75_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P00J75_A41Contratada_PessoaNom = new String[] {""} ;
         P00J75_n41Contratada_PessoaNom = new bool[] {false} ;
         P00J75_A77Contrato_Numero = new String[] {""} ;
         P00J75_A113ContratoArquivosAnexos_Data = new DateTime[] {DateTime.MinValue} ;
         P00J75_A108ContratoArquivosAnexos_Codigo = new int[1] ;
         P00J76_A74Contrato_Codigo = new int[1] ;
         P00J76_A39Contratada_Codigo = new int[1] ;
         P00J76_A40Contratada_PessoaCod = new int[1] ;
         P00J76_A112ContratoArquivosAnexos_NomeArq = new String[] {""} ;
         P00J76_A109ContratoArquivosAnexos_TipoArq = new String[] {""} ;
         P00J76_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P00J76_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P00J76_A41Contratada_PessoaNom = new String[] {""} ;
         P00J76_n41Contratada_PessoaNom = new bool[] {false} ;
         P00J76_A77Contrato_Numero = new String[] {""} ;
         P00J76_A113ContratoArquivosAnexos_Data = new DateTime[] {DateTime.MinValue} ;
         P00J76_A110ContratoArquivosAnexos_Descricao = new String[] {""} ;
         P00J76_A108ContratoArquivosAnexos_Codigo = new int[1] ;
         P00J77_A74Contrato_Codigo = new int[1] ;
         P00J77_A39Contratada_Codigo = new int[1] ;
         P00J77_A40Contratada_PessoaCod = new int[1] ;
         P00J77_A109ContratoArquivosAnexos_TipoArq = new String[] {""} ;
         P00J77_A112ContratoArquivosAnexos_NomeArq = new String[] {""} ;
         P00J77_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P00J77_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P00J77_A41Contratada_PessoaNom = new String[] {""} ;
         P00J77_n41Contratada_PessoaNom = new bool[] {false} ;
         P00J77_A77Contrato_Numero = new String[] {""} ;
         P00J77_A113ContratoArquivosAnexos_Data = new DateTime[] {DateTime.MinValue} ;
         P00J77_A110ContratoArquivosAnexos_Descricao = new String[] {""} ;
         P00J77_A108ContratoArquivosAnexos_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwcontratoarquivosanexosfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00J72_A74Contrato_Codigo, P00J72_A39Contratada_Codigo, P00J72_A40Contratada_PessoaCod, P00J72_A77Contrato_Numero, P00J72_A109ContratoArquivosAnexos_TipoArq, P00J72_A112ContratoArquivosAnexos_NomeArq, P00J72_A42Contratada_PessoaCNPJ, P00J72_n42Contratada_PessoaCNPJ, P00J72_A41Contratada_PessoaNom, P00J72_n41Contratada_PessoaNom,
               P00J72_A113ContratoArquivosAnexos_Data, P00J72_A110ContratoArquivosAnexos_Descricao, P00J72_A108ContratoArquivosAnexos_Codigo
               }
               , new Object[] {
               P00J73_A74Contrato_Codigo, P00J73_A39Contratada_Codigo, P00J73_A40Contratada_PessoaCod, P00J73_A41Contratada_PessoaNom, P00J73_n41Contratada_PessoaNom, P00J73_A109ContratoArquivosAnexos_TipoArq, P00J73_A112ContratoArquivosAnexos_NomeArq, P00J73_A42Contratada_PessoaCNPJ, P00J73_n42Contratada_PessoaCNPJ, P00J73_A77Contrato_Numero,
               P00J73_A113ContratoArquivosAnexos_Data, P00J73_A110ContratoArquivosAnexos_Descricao, P00J73_A108ContratoArquivosAnexos_Codigo
               }
               , new Object[] {
               P00J74_A74Contrato_Codigo, P00J74_A39Contratada_Codigo, P00J74_A40Contratada_PessoaCod, P00J74_A42Contratada_PessoaCNPJ, P00J74_n42Contratada_PessoaCNPJ, P00J74_A109ContratoArquivosAnexos_TipoArq, P00J74_A112ContratoArquivosAnexos_NomeArq, P00J74_A41Contratada_PessoaNom, P00J74_n41Contratada_PessoaNom, P00J74_A77Contrato_Numero,
               P00J74_A113ContratoArquivosAnexos_Data, P00J74_A110ContratoArquivosAnexos_Descricao, P00J74_A108ContratoArquivosAnexos_Codigo
               }
               , new Object[] {
               P00J75_A74Contrato_Codigo, P00J75_A39Contratada_Codigo, P00J75_A40Contratada_PessoaCod, P00J75_A110ContratoArquivosAnexos_Descricao, P00J75_A109ContratoArquivosAnexos_TipoArq, P00J75_A112ContratoArquivosAnexos_NomeArq, P00J75_A42Contratada_PessoaCNPJ, P00J75_n42Contratada_PessoaCNPJ, P00J75_A41Contratada_PessoaNom, P00J75_n41Contratada_PessoaNom,
               P00J75_A77Contrato_Numero, P00J75_A113ContratoArquivosAnexos_Data, P00J75_A108ContratoArquivosAnexos_Codigo
               }
               , new Object[] {
               P00J76_A74Contrato_Codigo, P00J76_A39Contratada_Codigo, P00J76_A40Contratada_PessoaCod, P00J76_A112ContratoArquivosAnexos_NomeArq, P00J76_A109ContratoArquivosAnexos_TipoArq, P00J76_A42Contratada_PessoaCNPJ, P00J76_n42Contratada_PessoaCNPJ, P00J76_A41Contratada_PessoaNom, P00J76_n41Contratada_PessoaNom, P00J76_A77Contrato_Numero,
               P00J76_A113ContratoArquivosAnexos_Data, P00J76_A110ContratoArquivosAnexos_Descricao, P00J76_A108ContratoArquivosAnexos_Codigo
               }
               , new Object[] {
               P00J77_A74Contrato_Codigo, P00J77_A39Contratada_Codigo, P00J77_A40Contratada_PessoaCod, P00J77_A109ContratoArquivosAnexos_TipoArq, P00J77_A112ContratoArquivosAnexos_NomeArq, P00J77_A42Contratada_PessoaCNPJ, P00J77_n42Contratada_PessoaCNPJ, P00J77_A41Contratada_PessoaNom, P00J77_n41Contratada_PessoaNom, P00J77_A77Contrato_Numero,
               P00J77_A113ContratoArquivosAnexos_Data, P00J77_A110ContratoArquivosAnexos_Descricao, P00J77_A108ContratoArquivosAnexos_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV43DynamicFiltersOperator1 ;
      private short AV49DynamicFiltersOperator2 ;
      private short AV64WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 ;
      private short AV70WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 ;
      private int AV61GXV1 ;
      private int A74Contrato_Codigo ;
      private int A39Contratada_Codigo ;
      private int A40Contratada_PessoaCod ;
      private int A108ContratoArquivosAnexos_Codigo ;
      private long AV36count ;
      private String AV10TFContrato_Numero ;
      private String AV11TFContrato_Numero_Sel ;
      private String AV12TFContratada_PessoaNom ;
      private String AV13TFContratada_PessoaNom_Sel ;
      private String AV18TFContratoArquivosAnexos_NomeArq ;
      private String AV19TFContratoArquivosAnexos_NomeArq_Sel ;
      private String AV20TFContratoArquivosAnexos_TipoArq ;
      private String AV21TFContratoArquivosAnexos_TipoArq_Sel ;
      private String AV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero ;
      private String AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel ;
      private String AV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom ;
      private String AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel ;
      private String AV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq ;
      private String AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel ;
      private String AV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq ;
      private String AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel ;
      private String scmdbuf ;
      private String lV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero ;
      private String lV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom ;
      private String lV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq ;
      private String lV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq ;
      private String A77Contrato_Numero ;
      private String A41Contratada_PessoaNom ;
      private String A112ContratoArquivosAnexos_NomeArq ;
      private String A109ContratoArquivosAnexos_TipoArq ;
      private DateTime AV22TFContratoArquivosAnexos_Data ;
      private DateTime AV23TFContratoArquivosAnexos_Data_To ;
      private DateTime AV45ContratoArquivosAnexos_Data1 ;
      private DateTime AV46ContratoArquivosAnexos_Data_To1 ;
      private DateTime AV51ContratoArquivosAnexos_Data2 ;
      private DateTime AV52ContratoArquivosAnexos_Data_To2 ;
      private DateTime AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1 ;
      private DateTime AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1 ;
      private DateTime AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2 ;
      private DateTime AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2 ;
      private DateTime AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data ;
      private DateTime AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to ;
      private DateTime A113ContratoArquivosAnexos_Data ;
      private bool returnInSub ;
      private bool AV47DynamicFiltersEnabled2 ;
      private bool AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 ;
      private bool BRKJ72 ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool n41Contratada_PessoaNom ;
      private bool BRKJ74 ;
      private bool BRKJ76 ;
      private bool BRKJ78 ;
      private bool BRKJ710 ;
      private bool BRKJ712 ;
      private String AV35OptionIndexesJson ;
      private String AV30OptionsJson ;
      private String AV33OptionsDescJson ;
      private String AV44ContratoArquivosAnexos_Descricao1 ;
      private String AV50ContratoArquivosAnexos_Descricao2 ;
      private String AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 ;
      private String AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 ;
      private String lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 ;
      private String lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 ;
      private String A110ContratoArquivosAnexos_Descricao ;
      private String AV26DDOName ;
      private String AV24SearchTxt ;
      private String AV25SearchTxtTo ;
      private String AV14TFContratada_PessoaCNPJ ;
      private String AV15TFContratada_PessoaCNPJ_Sel ;
      private String AV16TFContratoArquivosAnexos_Descricao ;
      private String AV17TFContratoArquivosAnexos_Descricao_Sel ;
      private String AV42DynamicFiltersSelector1 ;
      private String AV48DynamicFiltersSelector2 ;
      private String AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1 ;
      private String AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2 ;
      private String AV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj ;
      private String AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel ;
      private String AV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao ;
      private String AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel ;
      private String lV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj ;
      private String lV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao ;
      private String A42Contratada_PessoaCNPJ ;
      private String AV28Option ;
      private IGxSession AV37Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00J72_A74Contrato_Codigo ;
      private int[] P00J72_A39Contratada_Codigo ;
      private int[] P00J72_A40Contratada_PessoaCod ;
      private String[] P00J72_A77Contrato_Numero ;
      private String[] P00J72_A109ContratoArquivosAnexos_TipoArq ;
      private String[] P00J72_A112ContratoArquivosAnexos_NomeArq ;
      private String[] P00J72_A42Contratada_PessoaCNPJ ;
      private bool[] P00J72_n42Contratada_PessoaCNPJ ;
      private String[] P00J72_A41Contratada_PessoaNom ;
      private bool[] P00J72_n41Contratada_PessoaNom ;
      private DateTime[] P00J72_A113ContratoArquivosAnexos_Data ;
      private String[] P00J72_A110ContratoArquivosAnexos_Descricao ;
      private int[] P00J72_A108ContratoArquivosAnexos_Codigo ;
      private int[] P00J73_A74Contrato_Codigo ;
      private int[] P00J73_A39Contratada_Codigo ;
      private int[] P00J73_A40Contratada_PessoaCod ;
      private String[] P00J73_A41Contratada_PessoaNom ;
      private bool[] P00J73_n41Contratada_PessoaNom ;
      private String[] P00J73_A109ContratoArquivosAnexos_TipoArq ;
      private String[] P00J73_A112ContratoArquivosAnexos_NomeArq ;
      private String[] P00J73_A42Contratada_PessoaCNPJ ;
      private bool[] P00J73_n42Contratada_PessoaCNPJ ;
      private String[] P00J73_A77Contrato_Numero ;
      private DateTime[] P00J73_A113ContratoArquivosAnexos_Data ;
      private String[] P00J73_A110ContratoArquivosAnexos_Descricao ;
      private int[] P00J73_A108ContratoArquivosAnexos_Codigo ;
      private int[] P00J74_A74Contrato_Codigo ;
      private int[] P00J74_A39Contratada_Codigo ;
      private int[] P00J74_A40Contratada_PessoaCod ;
      private String[] P00J74_A42Contratada_PessoaCNPJ ;
      private bool[] P00J74_n42Contratada_PessoaCNPJ ;
      private String[] P00J74_A109ContratoArquivosAnexos_TipoArq ;
      private String[] P00J74_A112ContratoArquivosAnexos_NomeArq ;
      private String[] P00J74_A41Contratada_PessoaNom ;
      private bool[] P00J74_n41Contratada_PessoaNom ;
      private String[] P00J74_A77Contrato_Numero ;
      private DateTime[] P00J74_A113ContratoArquivosAnexos_Data ;
      private String[] P00J74_A110ContratoArquivosAnexos_Descricao ;
      private int[] P00J74_A108ContratoArquivosAnexos_Codigo ;
      private int[] P00J75_A74Contrato_Codigo ;
      private int[] P00J75_A39Contratada_Codigo ;
      private int[] P00J75_A40Contratada_PessoaCod ;
      private String[] P00J75_A110ContratoArquivosAnexos_Descricao ;
      private String[] P00J75_A109ContratoArquivosAnexos_TipoArq ;
      private String[] P00J75_A112ContratoArquivosAnexos_NomeArq ;
      private String[] P00J75_A42Contratada_PessoaCNPJ ;
      private bool[] P00J75_n42Contratada_PessoaCNPJ ;
      private String[] P00J75_A41Contratada_PessoaNom ;
      private bool[] P00J75_n41Contratada_PessoaNom ;
      private String[] P00J75_A77Contrato_Numero ;
      private DateTime[] P00J75_A113ContratoArquivosAnexos_Data ;
      private int[] P00J75_A108ContratoArquivosAnexos_Codigo ;
      private int[] P00J76_A74Contrato_Codigo ;
      private int[] P00J76_A39Contratada_Codigo ;
      private int[] P00J76_A40Contratada_PessoaCod ;
      private String[] P00J76_A112ContratoArquivosAnexos_NomeArq ;
      private String[] P00J76_A109ContratoArquivosAnexos_TipoArq ;
      private String[] P00J76_A42Contratada_PessoaCNPJ ;
      private bool[] P00J76_n42Contratada_PessoaCNPJ ;
      private String[] P00J76_A41Contratada_PessoaNom ;
      private bool[] P00J76_n41Contratada_PessoaNom ;
      private String[] P00J76_A77Contrato_Numero ;
      private DateTime[] P00J76_A113ContratoArquivosAnexos_Data ;
      private String[] P00J76_A110ContratoArquivosAnexos_Descricao ;
      private int[] P00J76_A108ContratoArquivosAnexos_Codigo ;
      private int[] P00J77_A74Contrato_Codigo ;
      private int[] P00J77_A39Contratada_Codigo ;
      private int[] P00J77_A40Contratada_PessoaCod ;
      private String[] P00J77_A109ContratoArquivosAnexos_TipoArq ;
      private String[] P00J77_A112ContratoArquivosAnexos_NomeArq ;
      private String[] P00J77_A42Contratada_PessoaCNPJ ;
      private bool[] P00J77_n42Contratada_PessoaCNPJ ;
      private String[] P00J77_A41Contratada_PessoaNom ;
      private bool[] P00J77_n41Contratada_PessoaNom ;
      private String[] P00J77_A77Contrato_Numero ;
      private DateTime[] P00J77_A113ContratoArquivosAnexos_Data ;
      private String[] P00J77_A110ContratoArquivosAnexos_Descricao ;
      private int[] P00J77_A108ContratoArquivosAnexos_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV29Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV32OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV34OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV39GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV40GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV41GridStateDynamicFilter ;
   }

   public class getwwcontratoarquivosanexosfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00J72( IGxContext context ,
                                             String AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1 ,
                                             short AV64WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 ,
                                             String AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 ,
                                             DateTime AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1 ,
                                             DateTime AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1 ,
                                             bool AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 ,
                                             String AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2 ,
                                             short AV70WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 ,
                                             String AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 ,
                                             DateTime AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2 ,
                                             DateTime AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2 ,
                                             String AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel ,
                                             String AV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero ,
                                             String AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel ,
                                             String AV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom ,
                                             String AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel ,
                                             String AV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj ,
                                             String AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel ,
                                             String AV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao ,
                                             String AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel ,
                                             String AV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq ,
                                             String AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel ,
                                             String AV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq ,
                                             DateTime AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data ,
                                             DateTime AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to ,
                                             String A110ContratoArquivosAnexos_Descricao ,
                                             DateTime A113ContratoArquivosAnexos_Data ,
                                             String A77Contrato_Numero ,
                                             String A41Contratada_PessoaNom ,
                                             String A42Contratada_PessoaCNPJ ,
                                             String A112ContratoArquivosAnexos_NomeArq ,
                                             String A109ContratoArquivosAnexos_TipoArq )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [22] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contrato_Codigo], T2.[Contratada_Codigo], T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, T2.[Contrato_Numero], T1.[ContratoArquivosAnexos_TipoArq], T1.[ContratoArquivosAnexos_NomeArq], T4.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T4.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[ContratoArquivosAnexos_Data], T1.[ContratoArquivosAnexos_Descricao], T1.[ContratoArquivosAnexos_Codigo] FROM ((([ContratoArquivosAnexos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV64WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV64WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1, "CONTRATOARQUIVOSANEXOS_DATA") == 0 ) && ( ! (DateTime.MinValue==AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] >= @AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] >= @AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1, "CONTRATOARQUIVOSANEXOS_DATA") == 0 ) && ( ! (DateTime.MinValue==AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] <= @AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] <= @AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV70WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV70WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2, "CONTRATOARQUIVOSANEXOS_DATA") == 0 ) && ( ! (DateTime.MinValue==AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] >= @AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] >= @AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2, "CONTRATOARQUIVOSANEXOS_DATA") == 0 ) && ( ! (DateTime.MinValue==AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] <= @AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] <= @AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] like @lV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] = @AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] = @AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] = @AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_NomeArq] like @lV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_NomeArq] like @lV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_NomeArq] = @AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_NomeArq] = @AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_TipoArq] like @lV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_TipoArq] like @lV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_TipoArq] = @AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_TipoArq] = @AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel)";
            }
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( ! (DateTime.MinValue==AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] >= @AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] >= @AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data)";
            }
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( ! (DateTime.MinValue==AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] <= @AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] <= @AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to)";
            }
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T2.[Contrato_Numero]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00J73( IGxContext context ,
                                             String AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1 ,
                                             short AV64WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 ,
                                             String AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 ,
                                             DateTime AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1 ,
                                             DateTime AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1 ,
                                             bool AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 ,
                                             String AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2 ,
                                             short AV70WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 ,
                                             String AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 ,
                                             DateTime AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2 ,
                                             DateTime AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2 ,
                                             String AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel ,
                                             String AV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero ,
                                             String AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel ,
                                             String AV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom ,
                                             String AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel ,
                                             String AV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj ,
                                             String AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel ,
                                             String AV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao ,
                                             String AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel ,
                                             String AV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq ,
                                             String AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel ,
                                             String AV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq ,
                                             DateTime AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data ,
                                             DateTime AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to ,
                                             String A110ContratoArquivosAnexos_Descricao ,
                                             DateTime A113ContratoArquivosAnexos_Data ,
                                             String A77Contrato_Numero ,
                                             String A41Contratada_PessoaNom ,
                                             String A42Contratada_PessoaCNPJ ,
                                             String A112ContratoArquivosAnexos_NomeArq ,
                                             String A109ContratoArquivosAnexos_TipoArq )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [22] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contrato_Codigo], T2.[Contratada_Codigo], T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, T4.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[ContratoArquivosAnexos_TipoArq], T1.[ContratoArquivosAnexos_NomeArq], T4.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T2.[Contrato_Numero], T1.[ContratoArquivosAnexos_Data], T1.[ContratoArquivosAnexos_Descricao], T1.[ContratoArquivosAnexos_Codigo] FROM ((([ContratoArquivosAnexos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV64WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV64WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1, "CONTRATOARQUIVOSANEXOS_DATA") == 0 ) && ( ! (DateTime.MinValue==AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] >= @AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] >= @AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1, "CONTRATOARQUIVOSANEXOS_DATA") == 0 ) && ( ! (DateTime.MinValue==AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] <= @AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] <= @AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV70WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV70WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2, "CONTRATOARQUIVOSANEXOS_DATA") == 0 ) && ( ! (DateTime.MinValue==AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] >= @AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] >= @AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2, "CONTRATOARQUIVOSANEXOS_DATA") == 0 ) && ( ! (DateTime.MinValue==AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] <= @AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] <= @AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] like @lV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] = @AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] = @AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] = @AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_NomeArq] like @lV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_NomeArq] like @lV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_NomeArq] = @AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_NomeArq] = @AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_TipoArq] like @lV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_TipoArq] like @lV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq)";
            }
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_TipoArq] = @AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_TipoArq] = @AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel)";
            }
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( ! (DateTime.MinValue==AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] >= @AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] >= @AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data)";
            }
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( ! (DateTime.MinValue==AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] <= @AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] <= @AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to)";
            }
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T4.[Pessoa_Nome]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00J74( IGxContext context ,
                                             String AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1 ,
                                             short AV64WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 ,
                                             String AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 ,
                                             DateTime AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1 ,
                                             DateTime AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1 ,
                                             bool AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 ,
                                             String AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2 ,
                                             short AV70WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 ,
                                             String AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 ,
                                             DateTime AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2 ,
                                             DateTime AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2 ,
                                             String AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel ,
                                             String AV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero ,
                                             String AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel ,
                                             String AV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom ,
                                             String AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel ,
                                             String AV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj ,
                                             String AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel ,
                                             String AV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao ,
                                             String AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel ,
                                             String AV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq ,
                                             String AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel ,
                                             String AV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq ,
                                             DateTime AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data ,
                                             DateTime AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to ,
                                             String A110ContratoArquivosAnexos_Descricao ,
                                             DateTime A113ContratoArquivosAnexos_Data ,
                                             String A77Contrato_Numero ,
                                             String A41Contratada_PessoaNom ,
                                             String A42Contratada_PessoaCNPJ ,
                                             String A112ContratoArquivosAnexos_NomeArq ,
                                             String A109ContratoArquivosAnexos_TipoArq )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [22] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contrato_Codigo], T2.[Contratada_Codigo], T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, T4.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T1.[ContratoArquivosAnexos_TipoArq], T1.[ContratoArquivosAnexos_NomeArq], T4.[Pessoa_Nome] AS Contratada_PessoaNom, T2.[Contrato_Numero], T1.[ContratoArquivosAnexos_Data], T1.[ContratoArquivosAnexos_Descricao], T1.[ContratoArquivosAnexos_Codigo] FROM ((([ContratoArquivosAnexos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV64WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV64WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1, "CONTRATOARQUIVOSANEXOS_DATA") == 0 ) && ( ! (DateTime.MinValue==AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] >= @AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] >= @AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1, "CONTRATOARQUIVOSANEXOS_DATA") == 0 ) && ( ! (DateTime.MinValue==AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] <= @AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] <= @AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV70WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV70WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2, "CONTRATOARQUIVOSANEXOS_DATA") == 0 ) && ( ! (DateTime.MinValue==AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] >= @AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] >= @AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2, "CONTRATOARQUIVOSANEXOS_DATA") == 0 ) && ( ! (DateTime.MinValue==AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] <= @AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] <= @AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] like @lV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] = @AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao)";
            }
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] = @AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] = @AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel)";
            }
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_NomeArq] like @lV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_NomeArq] like @lV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq)";
            }
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_NomeArq] = @AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_NomeArq] = @AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel)";
            }
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_TipoArq] like @lV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_TipoArq] like @lV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq)";
            }
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_TipoArq] = @AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_TipoArq] = @AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel)";
            }
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( ! (DateTime.MinValue==AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] >= @AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] >= @AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data)";
            }
         }
         else
         {
            GXv_int5[20] = 1;
         }
         if ( ! (DateTime.MinValue==AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] <= @AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] <= @AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to)";
            }
         }
         else
         {
            GXv_int5[21] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T4.[Pessoa_Docto]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      protected Object[] conditional_P00J75( IGxContext context ,
                                             String AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1 ,
                                             short AV64WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 ,
                                             String AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 ,
                                             DateTime AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1 ,
                                             DateTime AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1 ,
                                             bool AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 ,
                                             String AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2 ,
                                             short AV70WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 ,
                                             String AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 ,
                                             DateTime AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2 ,
                                             DateTime AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2 ,
                                             String AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel ,
                                             String AV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero ,
                                             String AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel ,
                                             String AV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom ,
                                             String AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel ,
                                             String AV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj ,
                                             String AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel ,
                                             String AV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao ,
                                             String AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel ,
                                             String AV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq ,
                                             String AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel ,
                                             String AV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq ,
                                             DateTime AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data ,
                                             DateTime AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to ,
                                             String A110ContratoArquivosAnexos_Descricao ,
                                             DateTime A113ContratoArquivosAnexos_Data ,
                                             String A77Contrato_Numero ,
                                             String A41Contratada_PessoaNom ,
                                             String A42Contratada_PessoaCNPJ ,
                                             String A112ContratoArquivosAnexos_NomeArq ,
                                             String A109ContratoArquivosAnexos_TipoArq )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int7 ;
         GXv_int7 = new short [22] ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contrato_Codigo], T2.[Contratada_Codigo], T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[ContratoArquivosAnexos_Descricao], T1.[ContratoArquivosAnexos_TipoArq], T1.[ContratoArquivosAnexos_NomeArq], T4.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T4.[Pessoa_Nome] AS Contratada_PessoaNom, T2.[Contrato_Numero], T1.[ContratoArquivosAnexos_Data], T1.[ContratoArquivosAnexos_Codigo] FROM ((([ContratoArquivosAnexos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV64WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)";
            }
         }
         else
         {
            GXv_int7[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV64WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)";
            }
         }
         else
         {
            GXv_int7[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1, "CONTRATOARQUIVOSANEXOS_DATA") == 0 ) && ( ! (DateTime.MinValue==AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] >= @AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] >= @AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1)";
            }
         }
         else
         {
            GXv_int7[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1, "CONTRATOARQUIVOSANEXOS_DATA") == 0 ) && ( ! (DateTime.MinValue==AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] <= @AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] <= @AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1)";
            }
         }
         else
         {
            GXv_int7[3] = 1;
         }
         if ( AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV70WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)";
            }
         }
         else
         {
            GXv_int7[4] = 1;
         }
         if ( AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV70WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)";
            }
         }
         else
         {
            GXv_int7[5] = 1;
         }
         if ( AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2, "CONTRATOARQUIVOSANEXOS_DATA") == 0 ) && ( ! (DateTime.MinValue==AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] >= @AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] >= @AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2)";
            }
         }
         else
         {
            GXv_int7[6] = 1;
         }
         if ( AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2, "CONTRATOARQUIVOSANEXOS_DATA") == 0 ) && ( ! (DateTime.MinValue==AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] <= @AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] <= @AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2)";
            }
         }
         else
         {
            GXv_int7[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero)";
            }
         }
         else
         {
            GXv_int7[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel)";
            }
         }
         else
         {
            GXv_int7[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom)";
            }
         }
         else
         {
            GXv_int7[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel)";
            }
         }
         else
         {
            GXv_int7[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] like @lV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj)";
            }
         }
         else
         {
            GXv_int7[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] = @AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel)";
            }
         }
         else
         {
            GXv_int7[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao)";
            }
         }
         else
         {
            GXv_int7[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] = @AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] = @AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel)";
            }
         }
         else
         {
            GXv_int7[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_NomeArq] like @lV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_NomeArq] like @lV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq)";
            }
         }
         else
         {
            GXv_int7[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_NomeArq] = @AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_NomeArq] = @AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel)";
            }
         }
         else
         {
            GXv_int7[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_TipoArq] like @lV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_TipoArq] like @lV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq)";
            }
         }
         else
         {
            GXv_int7[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_TipoArq] = @AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_TipoArq] = @AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel)";
            }
         }
         else
         {
            GXv_int7[19] = 1;
         }
         if ( ! (DateTime.MinValue==AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] >= @AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] >= @AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data)";
            }
         }
         else
         {
            GXv_int7[20] = 1;
         }
         if ( ! (DateTime.MinValue==AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] <= @AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] <= @AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to)";
            }
         }
         else
         {
            GXv_int7[21] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoArquivosAnexos_Descricao]";
         GXv_Object8[0] = scmdbuf;
         GXv_Object8[1] = GXv_int7;
         return GXv_Object8 ;
      }

      protected Object[] conditional_P00J76( IGxContext context ,
                                             String AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1 ,
                                             short AV64WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 ,
                                             String AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 ,
                                             DateTime AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1 ,
                                             DateTime AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1 ,
                                             bool AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 ,
                                             String AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2 ,
                                             short AV70WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 ,
                                             String AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 ,
                                             DateTime AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2 ,
                                             DateTime AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2 ,
                                             String AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel ,
                                             String AV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero ,
                                             String AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel ,
                                             String AV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom ,
                                             String AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel ,
                                             String AV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj ,
                                             String AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel ,
                                             String AV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao ,
                                             String AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel ,
                                             String AV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq ,
                                             String AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel ,
                                             String AV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq ,
                                             DateTime AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data ,
                                             DateTime AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to ,
                                             String A110ContratoArquivosAnexos_Descricao ,
                                             DateTime A113ContratoArquivosAnexos_Data ,
                                             String A77Contrato_Numero ,
                                             String A41Contratada_PessoaNom ,
                                             String A42Contratada_PessoaCNPJ ,
                                             String A112ContratoArquivosAnexos_NomeArq ,
                                             String A109ContratoArquivosAnexos_TipoArq )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int9 ;
         GXv_int9 = new short [22] ;
         Object[] GXv_Object10 ;
         GXv_Object10 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contrato_Codigo], T2.[Contratada_Codigo], T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[ContratoArquivosAnexos_NomeArq], T1.[ContratoArquivosAnexos_TipoArq], T4.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T4.[Pessoa_Nome] AS Contratada_PessoaNom, T2.[Contrato_Numero], T1.[ContratoArquivosAnexos_Data], T1.[ContratoArquivosAnexos_Descricao], T1.[ContratoArquivosAnexos_Codigo] FROM ((([ContratoArquivosAnexos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV64WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)";
            }
         }
         else
         {
            GXv_int9[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV64WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)";
            }
         }
         else
         {
            GXv_int9[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1, "CONTRATOARQUIVOSANEXOS_DATA") == 0 ) && ( ! (DateTime.MinValue==AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] >= @AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] >= @AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1)";
            }
         }
         else
         {
            GXv_int9[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1, "CONTRATOARQUIVOSANEXOS_DATA") == 0 ) && ( ! (DateTime.MinValue==AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] <= @AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] <= @AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1)";
            }
         }
         else
         {
            GXv_int9[3] = 1;
         }
         if ( AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV70WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)";
            }
         }
         else
         {
            GXv_int9[4] = 1;
         }
         if ( AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV70WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)";
            }
         }
         else
         {
            GXv_int9[5] = 1;
         }
         if ( AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2, "CONTRATOARQUIVOSANEXOS_DATA") == 0 ) && ( ! (DateTime.MinValue==AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] >= @AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] >= @AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2)";
            }
         }
         else
         {
            GXv_int9[6] = 1;
         }
         if ( AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2, "CONTRATOARQUIVOSANEXOS_DATA") == 0 ) && ( ! (DateTime.MinValue==AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] <= @AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] <= @AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2)";
            }
         }
         else
         {
            GXv_int9[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero)";
            }
         }
         else
         {
            GXv_int9[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel)";
            }
         }
         else
         {
            GXv_int9[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom)";
            }
         }
         else
         {
            GXv_int9[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel)";
            }
         }
         else
         {
            GXv_int9[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] like @lV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj)";
            }
         }
         else
         {
            GXv_int9[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] = @AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel)";
            }
         }
         else
         {
            GXv_int9[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao)";
            }
         }
         else
         {
            GXv_int9[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] = @AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] = @AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel)";
            }
         }
         else
         {
            GXv_int9[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_NomeArq] like @lV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_NomeArq] like @lV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq)";
            }
         }
         else
         {
            GXv_int9[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_NomeArq] = @AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_NomeArq] = @AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel)";
            }
         }
         else
         {
            GXv_int9[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_TipoArq] like @lV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_TipoArq] like @lV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq)";
            }
         }
         else
         {
            GXv_int9[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_TipoArq] = @AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_TipoArq] = @AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel)";
            }
         }
         else
         {
            GXv_int9[19] = 1;
         }
         if ( ! (DateTime.MinValue==AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] >= @AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] >= @AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data)";
            }
         }
         else
         {
            GXv_int9[20] = 1;
         }
         if ( ! (DateTime.MinValue==AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] <= @AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] <= @AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to)";
            }
         }
         else
         {
            GXv_int9[21] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoArquivosAnexos_NomeArq]";
         GXv_Object10[0] = scmdbuf;
         GXv_Object10[1] = GXv_int9;
         return GXv_Object10 ;
      }

      protected Object[] conditional_P00J77( IGxContext context ,
                                             String AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1 ,
                                             short AV64WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 ,
                                             String AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1 ,
                                             DateTime AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1 ,
                                             DateTime AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1 ,
                                             bool AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 ,
                                             String AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2 ,
                                             short AV70WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 ,
                                             String AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2 ,
                                             DateTime AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2 ,
                                             DateTime AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2 ,
                                             String AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel ,
                                             String AV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero ,
                                             String AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel ,
                                             String AV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom ,
                                             String AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel ,
                                             String AV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj ,
                                             String AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel ,
                                             String AV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao ,
                                             String AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel ,
                                             String AV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq ,
                                             String AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel ,
                                             String AV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq ,
                                             DateTime AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data ,
                                             DateTime AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to ,
                                             String A110ContratoArquivosAnexos_Descricao ,
                                             DateTime A113ContratoArquivosAnexos_Data ,
                                             String A77Contrato_Numero ,
                                             String A41Contratada_PessoaNom ,
                                             String A42Contratada_PessoaCNPJ ,
                                             String A112ContratoArquivosAnexos_NomeArq ,
                                             String A109ContratoArquivosAnexos_TipoArq )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int11 ;
         GXv_int11 = new short [22] ;
         Object[] GXv_Object12 ;
         GXv_Object12 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contrato_Codigo], T2.[Contratada_Codigo], T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[ContratoArquivosAnexos_TipoArq], T1.[ContratoArquivosAnexos_NomeArq], T4.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T4.[Pessoa_Nome] AS Contratada_PessoaNom, T2.[Contrato_Numero], T1.[ContratoArquivosAnexos_Data], T1.[ContratoArquivosAnexos_Descricao], T1.[ContratoArquivosAnexos_Codigo] FROM ((([ContratoArquivosAnexos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV64WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)";
            }
         }
         else
         {
            GXv_int11[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV64WWContratoArquivosAnexosDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1)";
            }
         }
         else
         {
            GXv_int11[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1, "CONTRATOARQUIVOSANEXOS_DATA") == 0 ) && ( ! (DateTime.MinValue==AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] >= @AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] >= @AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1)";
            }
         }
         else
         {
            GXv_int11[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV63WWContratoArquivosAnexosDS_1_Dynamicfiltersselector1, "CONTRATOARQUIVOSANEXOS_DATA") == 0 ) && ( ! (DateTime.MinValue==AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] <= @AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] <= @AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1)";
            }
         }
         else
         {
            GXv_int11[3] = 1;
         }
         if ( AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV70WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)";
            }
         }
         else
         {
            GXv_int11[4] = 1;
         }
         if ( AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV70WWContratoArquivosAnexosDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2)";
            }
         }
         else
         {
            GXv_int11[5] = 1;
         }
         if ( AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2, "CONTRATOARQUIVOSANEXOS_DATA") == 0 ) && ( ! (DateTime.MinValue==AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] >= @AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] >= @AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2)";
            }
         }
         else
         {
            GXv_int11[6] = 1;
         }
         if ( AV68WWContratoArquivosAnexosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV69WWContratoArquivosAnexosDS_7_Dynamicfiltersselector2, "CONTRATOARQUIVOSANEXOS_DATA") == 0 ) && ( ! (DateTime.MinValue==AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] <= @AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] <= @AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2)";
            }
         }
         else
         {
            GXv_int11[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero)";
            }
         }
         else
         {
            GXv_int11[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel)";
            }
         }
         else
         {
            GXv_int11[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom)";
            }
         }
         else
         {
            GXv_int11[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel)";
            }
         }
         else
         {
            GXv_int11[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] like @lV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj)";
            }
         }
         else
         {
            GXv_int11[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] = @AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel)";
            }
         }
         else
         {
            GXv_int11[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao)";
            }
         }
         else
         {
            GXv_int11[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] = @AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] = @AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel)";
            }
         }
         else
         {
            GXv_int11[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_NomeArq] like @lV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_NomeArq] like @lV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq)";
            }
         }
         else
         {
            GXv_int11[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_NomeArq] = @AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_NomeArq] = @AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel)";
            }
         }
         else
         {
            GXv_int11[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_TipoArq] like @lV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_TipoArq] like @lV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq)";
            }
         }
         else
         {
            GXv_int11[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_TipoArq] = @AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_TipoArq] = @AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel)";
            }
         }
         else
         {
            GXv_int11[19] = 1;
         }
         if ( ! (DateTime.MinValue==AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] >= @AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] >= @AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data)";
            }
         }
         else
         {
            GXv_int11[20] = 1;
         }
         if ( ! (DateTime.MinValue==AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] <= @AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] <= @AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to)";
            }
         }
         else
         {
            GXv_int11[21] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoArquivosAnexos_TipoArq]";
         GXv_Object12[0] = scmdbuf;
         GXv_Object12[1] = GXv_int11;
         return GXv_Object12 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00J72(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (DateTime)dynConstraints[23] , (DateTime)dynConstraints[24] , (String)dynConstraints[25] , (DateTime)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] );
               case 1 :
                     return conditional_P00J73(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (DateTime)dynConstraints[23] , (DateTime)dynConstraints[24] , (String)dynConstraints[25] , (DateTime)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] );
               case 2 :
                     return conditional_P00J74(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (DateTime)dynConstraints[23] , (DateTime)dynConstraints[24] , (String)dynConstraints[25] , (DateTime)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] );
               case 3 :
                     return conditional_P00J75(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (DateTime)dynConstraints[23] , (DateTime)dynConstraints[24] , (String)dynConstraints[25] , (DateTime)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] );
               case 4 :
                     return conditional_P00J76(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (DateTime)dynConstraints[23] , (DateTime)dynConstraints[24] , (String)dynConstraints[25] , (DateTime)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] );
               case 5 :
                     return conditional_P00J77(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (DateTime)dynConstraints[23] , (DateTime)dynConstraints[24] , (String)dynConstraints[25] , (DateTime)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00J72 ;
          prmP00J72 = new Object[] {
          new Object[] {"@lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq",SqlDbType.Char,10,0} ,
          new Object[] {"@AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmP00J73 ;
          prmP00J73 = new Object[] {
          new Object[] {"@lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq",SqlDbType.Char,10,0} ,
          new Object[] {"@AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmP00J74 ;
          prmP00J74 = new Object[] {
          new Object[] {"@lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq",SqlDbType.Char,10,0} ,
          new Object[] {"@AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmP00J75 ;
          prmP00J75 = new Object[] {
          new Object[] {"@lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq",SqlDbType.Char,10,0} ,
          new Object[] {"@AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmP00J76 ;
          prmP00J76 = new Object[] {
          new Object[] {"@lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq",SqlDbType.Char,10,0} ,
          new Object[] {"@AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmP00J77 ;
          prmP00J77 = new Object[] {
          new Object[] {"@lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV65WWContratoArquivosAnexosDS_3_Contratoarquivosanexos_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV66WWContratoArquivosAnexosDS_4_Contratoarquivosanexos_data1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV67WWContratoArquivosAnexosDS_5_Contratoarquivosanexos_data_to1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV71WWContratoArquivosAnexosDS_9_Contratoarquivosanexos_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV72WWContratoArquivosAnexosDS_10_Contratoarquivosanexos_data2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV73WWContratoArquivosAnexosDS_11_Contratoarquivosanexos_data_to2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV74WWContratoArquivosAnexosDS_12_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV75WWContratoArquivosAnexosDS_13_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV76WWContratoArquivosAnexosDS_14_Tfcontratada_pessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV77WWContratoArquivosAnexosDS_15_Tfcontratada_pessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV78WWContratoArquivosAnexosDS_16_Tfcontratada_pessoacnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV79WWContratoArquivosAnexosDS_17_Tfcontratada_pessoacnpj_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV80WWContratoArquivosAnexosDS_18_Tfcontratoarquivosanexos_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV81WWContratoArquivosAnexosDS_19_Tfcontratoarquivosanexos_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV82WWContratoArquivosAnexosDS_20_Tfcontratoarquivosanexos_nomearq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV83WWContratoArquivosAnexosDS_21_Tfcontratoarquivosanexos_nomearq_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV84WWContratoArquivosAnexosDS_22_Tfcontratoarquivosanexos_tipoarq",SqlDbType.Char,10,0} ,
          new Object[] {"@AV85WWContratoArquivosAnexosDS_23_Tfcontratoarquivosanexos_tipoarq_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@AV86WWContratoArquivosAnexosDS_24_Tfcontratoarquivosanexos_data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV87WWContratoArquivosAnexosDS_25_Tfcontratoarquivosanexos_data_to",SqlDbType.DateTime,8,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00J72", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00J72,100,0,true,false )
             ,new CursorDef("P00J73", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00J73,100,0,true,false )
             ,new CursorDef("P00J74", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00J74,100,0,true,false )
             ,new CursorDef("P00J75", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00J75,100,0,true,false )
             ,new CursorDef("P00J76", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00J76,100,0,true,false )
             ,new CursorDef("P00J77", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00J77,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 20) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 10) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 50) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(7);
                ((String[]) buf[8])[0] = rslt.getString(8, 100) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[10])[0] = rslt.getGXDateTime(9) ;
                ((String[]) buf[11])[0] = rslt.getLongVarchar(10) ;
                ((int[]) buf[12])[0] = rslt.getInt(11) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getString(5, 10) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 50) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((String[]) buf[9])[0] = rslt.getString(8, 20) ;
                ((DateTime[]) buf[10])[0] = rslt.getGXDateTime(9) ;
                ((String[]) buf[11])[0] = rslt.getLongVarchar(10) ;
                ((int[]) buf[12])[0] = rslt.getInt(11) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getString(5, 10) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 50) ;
                ((String[]) buf[7])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((String[]) buf[9])[0] = rslt.getString(8, 20) ;
                ((DateTime[]) buf[10])[0] = rslt.getGXDateTime(9) ;
                ((String[]) buf[11])[0] = rslt.getLongVarchar(10) ;
                ((int[]) buf[12])[0] = rslt.getInt(11) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 10) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 50) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(7);
                ((String[]) buf[8])[0] = rslt.getString(8, 100) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(8);
                ((String[]) buf[10])[0] = rslt.getString(9, 20) ;
                ((DateTime[]) buf[11])[0] = rslt.getGXDateTime(10) ;
                ((int[]) buf[12])[0] = rslt.getInt(11) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 10) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((String[]) buf[7])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((String[]) buf[9])[0] = rslt.getString(8, 20) ;
                ((DateTime[]) buf[10])[0] = rslt.getGXDateTime(9) ;
                ((String[]) buf[11])[0] = rslt.getLongVarchar(10) ;
                ((int[]) buf[12])[0] = rslt.getInt(11) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 10) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 50) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((String[]) buf[7])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((String[]) buf[9])[0] = rslt.getString(8, 20) ;
                ((DateTime[]) buf[10])[0] = rslt.getGXDateTime(9) ;
                ((String[]) buf[11])[0] = rslt.getLongVarchar(10) ;
                ((int[]) buf[12])[0] = rslt.getInt(11) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[25]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[28]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[29]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[42]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[43]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[25]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[28]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[29]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[42]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[43]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[25]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[28]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[29]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[42]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[43]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[25]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[28]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[29]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[42]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[43]);
                }
                return;
             case 4 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[25]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[28]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[29]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[42]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[43]);
                }
                return;
             case 5 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[25]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[28]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[29]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[42]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[43]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwcontratoarquivosanexosfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwcontratoarquivosanexosfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwcontratoarquivosanexosfilterdata") )
          {
             return  ;
          }
          getwwcontratoarquivosanexosfilterdata worker = new getwwcontratoarquivosanexosfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
