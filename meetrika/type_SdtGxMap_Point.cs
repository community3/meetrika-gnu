/*
               File: type_SdtGxMap_Point
        Description: GxMap
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:36:56.59
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "GxMap.Point" )]
   [XmlType(TypeName =  "GxMap.Point" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtGxMap_Point : GxUserType
   {
      public SdtGxMap_Point( )
      {
         /* Constructor for serialization */
         gxTv_SdtGxMap_Point_Pointlat = "";
         gxTv_SdtGxMap_Point_Pointlong = "";
         gxTv_SdtGxMap_Point_Pointicon = "";
         gxTv_SdtGxMap_Point_Pointstreet = "";
         gxTv_SdtGxMap_Point_Pointstreetnumber = "";
         gxTv_SdtGxMap_Point_Pointcrossstreet = "";
         gxTv_SdtGxMap_Point_Pointinfowintit = "";
         gxTv_SdtGxMap_Point_Pointinfowindesc = "";
         gxTv_SdtGxMap_Point_Pointinfowinlink = "";
         gxTv_SdtGxMap_Point_Pointinfowinlinkdsc = "";
         gxTv_SdtGxMap_Point_Pointinfowinimg = "";
      }

      public SdtGxMap_Point( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtGxMap_Point deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtGxMap_Point)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtGxMap_Point obj ;
         obj = this;
         obj.gxTpr_Pointlat = deserialized.gxTpr_Pointlat;
         obj.gxTpr_Pointlong = deserialized.gxTpr_Pointlong;
         obj.gxTpr_Pointicon = deserialized.gxTpr_Pointicon;
         obj.gxTpr_Pointstreet = deserialized.gxTpr_Pointstreet;
         obj.gxTpr_Pointstreetnumber = deserialized.gxTpr_Pointstreetnumber;
         obj.gxTpr_Pointcrossstreet = deserialized.gxTpr_Pointcrossstreet;
         obj.gxTpr_Pointinfowintit = deserialized.gxTpr_Pointinfowintit;
         obj.gxTpr_Pointinfowindesc = deserialized.gxTpr_Pointinfowindesc;
         obj.gxTpr_Pointinfowinlink = deserialized.gxTpr_Pointinfowinlink;
         obj.gxTpr_Pointinfowinlinkdsc = deserialized.gxTpr_Pointinfowinlinkdsc;
         obj.gxTpr_Pointinfowinimg = deserialized.gxTpr_Pointinfowinimg;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "PointLat") )
               {
                  gxTv_SdtGxMap_Point_Pointlat = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "PointLong") )
               {
                  gxTv_SdtGxMap_Point_Pointlong = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "PointIcon") )
               {
                  gxTv_SdtGxMap_Point_Pointicon = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "PointStreet") )
               {
                  gxTv_SdtGxMap_Point_Pointstreet = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "PointStreetNumber") )
               {
                  gxTv_SdtGxMap_Point_Pointstreetnumber = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "PointCrossStreet") )
               {
                  gxTv_SdtGxMap_Point_Pointcrossstreet = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "PointInfowinTit") )
               {
                  gxTv_SdtGxMap_Point_Pointinfowintit = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "PointInfowinDesc") )
               {
                  gxTv_SdtGxMap_Point_Pointinfowindesc = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "PointInfowinLink") )
               {
                  gxTv_SdtGxMap_Point_Pointinfowinlink = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "PointInfowinLinkDsc") )
               {
                  gxTv_SdtGxMap_Point_Pointinfowinlinkdsc = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "PointInfowinImg") )
               {
                  gxTv_SdtGxMap_Point_Pointinfowinimg = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "GxMap.Point";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("PointLat", StringUtil.RTrim( gxTv_SdtGxMap_Point_Pointlat));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("PointLong", StringUtil.RTrim( gxTv_SdtGxMap_Point_Pointlong));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("PointIcon", StringUtil.RTrim( gxTv_SdtGxMap_Point_Pointicon));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("PointStreet", StringUtil.RTrim( gxTv_SdtGxMap_Point_Pointstreet));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("PointStreetNumber", StringUtil.RTrim( gxTv_SdtGxMap_Point_Pointstreetnumber));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("PointCrossStreet", StringUtil.RTrim( gxTv_SdtGxMap_Point_Pointcrossstreet));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("PointInfowinTit", StringUtil.RTrim( gxTv_SdtGxMap_Point_Pointinfowintit));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("PointInfowinDesc", StringUtil.RTrim( gxTv_SdtGxMap_Point_Pointinfowindesc));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("PointInfowinLink", StringUtil.RTrim( gxTv_SdtGxMap_Point_Pointinfowinlink));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("PointInfowinLinkDsc", StringUtil.RTrim( gxTv_SdtGxMap_Point_Pointinfowinlinkdsc));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("PointInfowinImg", StringUtil.RTrim( gxTv_SdtGxMap_Point_Pointinfowinimg));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("PointLat", gxTv_SdtGxMap_Point_Pointlat, false);
         AddObjectProperty("PointLong", gxTv_SdtGxMap_Point_Pointlong, false);
         AddObjectProperty("PointIcon", gxTv_SdtGxMap_Point_Pointicon, false);
         AddObjectProperty("PointStreet", gxTv_SdtGxMap_Point_Pointstreet, false);
         AddObjectProperty("PointStreetNumber", gxTv_SdtGxMap_Point_Pointstreetnumber, false);
         AddObjectProperty("PointCrossStreet", gxTv_SdtGxMap_Point_Pointcrossstreet, false);
         AddObjectProperty("PointInfowinTit", gxTv_SdtGxMap_Point_Pointinfowintit, false);
         AddObjectProperty("PointInfowinDesc", gxTv_SdtGxMap_Point_Pointinfowindesc, false);
         AddObjectProperty("PointInfowinLink", gxTv_SdtGxMap_Point_Pointinfowinlink, false);
         AddObjectProperty("PointInfowinLinkDsc", gxTv_SdtGxMap_Point_Pointinfowinlinkdsc, false);
         AddObjectProperty("PointInfowinImg", gxTv_SdtGxMap_Point_Pointinfowinimg, false);
         return  ;
      }

      [  SoapElement( ElementName = "PointLat" )]
      [  XmlElement( ElementName = "PointLat"   )]
      public String gxTpr_Pointlat
      {
         get {
            return gxTv_SdtGxMap_Point_Pointlat ;
         }

         set {
            gxTv_SdtGxMap_Point_Pointlat = (String)(value);
         }

      }

      [  SoapElement( ElementName = "PointLong" )]
      [  XmlElement( ElementName = "PointLong"   )]
      public String gxTpr_Pointlong
      {
         get {
            return gxTv_SdtGxMap_Point_Pointlong ;
         }

         set {
            gxTv_SdtGxMap_Point_Pointlong = (String)(value);
         }

      }

      [  SoapElement( ElementName = "PointIcon" )]
      [  XmlElement( ElementName = "PointIcon"   )]
      public String gxTpr_Pointicon
      {
         get {
            return gxTv_SdtGxMap_Point_Pointicon ;
         }

         set {
            gxTv_SdtGxMap_Point_Pointicon = (String)(value);
         }

      }

      [  SoapElement( ElementName = "PointStreet" )]
      [  XmlElement( ElementName = "PointStreet"   )]
      public String gxTpr_Pointstreet
      {
         get {
            return gxTv_SdtGxMap_Point_Pointstreet ;
         }

         set {
            gxTv_SdtGxMap_Point_Pointstreet = (String)(value);
         }

      }

      [  SoapElement( ElementName = "PointStreetNumber" )]
      [  XmlElement( ElementName = "PointStreetNumber"   )]
      public String gxTpr_Pointstreetnumber
      {
         get {
            return gxTv_SdtGxMap_Point_Pointstreetnumber ;
         }

         set {
            gxTv_SdtGxMap_Point_Pointstreetnumber = (String)(value);
         }

      }

      [  SoapElement( ElementName = "PointCrossStreet" )]
      [  XmlElement( ElementName = "PointCrossStreet"   )]
      public String gxTpr_Pointcrossstreet
      {
         get {
            return gxTv_SdtGxMap_Point_Pointcrossstreet ;
         }

         set {
            gxTv_SdtGxMap_Point_Pointcrossstreet = (String)(value);
         }

      }

      [  SoapElement( ElementName = "PointInfowinTit" )]
      [  XmlElement( ElementName = "PointInfowinTit"   )]
      public String gxTpr_Pointinfowintit
      {
         get {
            return gxTv_SdtGxMap_Point_Pointinfowintit ;
         }

         set {
            gxTv_SdtGxMap_Point_Pointinfowintit = (String)(value);
         }

      }

      [  SoapElement( ElementName = "PointInfowinDesc" )]
      [  XmlElement( ElementName = "PointInfowinDesc"   )]
      public String gxTpr_Pointinfowindesc
      {
         get {
            return gxTv_SdtGxMap_Point_Pointinfowindesc ;
         }

         set {
            gxTv_SdtGxMap_Point_Pointinfowindesc = (String)(value);
         }

      }

      [  SoapElement( ElementName = "PointInfowinLink" )]
      [  XmlElement( ElementName = "PointInfowinLink"   )]
      public String gxTpr_Pointinfowinlink
      {
         get {
            return gxTv_SdtGxMap_Point_Pointinfowinlink ;
         }

         set {
            gxTv_SdtGxMap_Point_Pointinfowinlink = (String)(value);
         }

      }

      [  SoapElement( ElementName = "PointInfowinLinkDsc" )]
      [  XmlElement( ElementName = "PointInfowinLinkDsc"   )]
      public String gxTpr_Pointinfowinlinkdsc
      {
         get {
            return gxTv_SdtGxMap_Point_Pointinfowinlinkdsc ;
         }

         set {
            gxTv_SdtGxMap_Point_Pointinfowinlinkdsc = (String)(value);
         }

      }

      [  SoapElement( ElementName = "PointInfowinImg" )]
      [  XmlElement( ElementName = "PointInfowinImg"   )]
      public String gxTpr_Pointinfowinimg
      {
         get {
            return gxTv_SdtGxMap_Point_Pointinfowinimg ;
         }

         set {
            gxTv_SdtGxMap_Point_Pointinfowinimg = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtGxMap_Point_Pointlat = "";
         gxTv_SdtGxMap_Point_Pointlong = "";
         gxTv_SdtGxMap_Point_Pointicon = "";
         gxTv_SdtGxMap_Point_Pointstreet = "";
         gxTv_SdtGxMap_Point_Pointstreetnumber = "";
         gxTv_SdtGxMap_Point_Pointcrossstreet = "";
         gxTv_SdtGxMap_Point_Pointinfowintit = "";
         gxTv_SdtGxMap_Point_Pointinfowindesc = "";
         gxTv_SdtGxMap_Point_Pointinfowinlink = "";
         gxTv_SdtGxMap_Point_Pointinfowinlinkdsc = "";
         gxTv_SdtGxMap_Point_Pointinfowinimg = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtGxMap_Point_Pointlat ;
      protected String gxTv_SdtGxMap_Point_Pointlong ;
      protected String gxTv_SdtGxMap_Point_Pointicon ;
      protected String gxTv_SdtGxMap_Point_Pointstreet ;
      protected String gxTv_SdtGxMap_Point_Pointstreetnumber ;
      protected String gxTv_SdtGxMap_Point_Pointcrossstreet ;
      protected String gxTv_SdtGxMap_Point_Pointinfowintit ;
      protected String gxTv_SdtGxMap_Point_Pointinfowindesc ;
      protected String gxTv_SdtGxMap_Point_Pointinfowinlink ;
      protected String gxTv_SdtGxMap_Point_Pointinfowinlinkdsc ;
      protected String gxTv_SdtGxMap_Point_Pointinfowinimg ;
      protected String sTagName ;
   }

   [DataContract(Name = @"GxMap.Point", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtGxMap_Point_RESTInterface : GxGenericCollectionItem<SdtGxMap_Point>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtGxMap_Point_RESTInterface( ) : base()
      {
      }

      public SdtGxMap_Point_RESTInterface( SdtGxMap_Point psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "PointLat" , Order = 0 )]
      public String gxTpr_Pointlat
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Pointlat) ;
         }

         set {
            sdt.gxTpr_Pointlat = (String)(value);
         }

      }

      [DataMember( Name = "PointLong" , Order = 1 )]
      public String gxTpr_Pointlong
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Pointlong) ;
         }

         set {
            sdt.gxTpr_Pointlong = (String)(value);
         }

      }

      [DataMember( Name = "PointIcon" , Order = 2 )]
      public String gxTpr_Pointicon
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Pointicon) ;
         }

         set {
            sdt.gxTpr_Pointicon = (String)(value);
         }

      }

      [DataMember( Name = "PointStreet" , Order = 3 )]
      public String gxTpr_Pointstreet
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Pointstreet) ;
         }

         set {
            sdt.gxTpr_Pointstreet = (String)(value);
         }

      }

      [DataMember( Name = "PointStreetNumber" , Order = 4 )]
      public String gxTpr_Pointstreetnumber
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Pointstreetnumber) ;
         }

         set {
            sdt.gxTpr_Pointstreetnumber = (String)(value);
         }

      }

      [DataMember( Name = "PointCrossStreet" , Order = 5 )]
      public String gxTpr_Pointcrossstreet
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Pointcrossstreet) ;
         }

         set {
            sdt.gxTpr_Pointcrossstreet = (String)(value);
         }

      }

      [DataMember( Name = "PointInfowinTit" , Order = 6 )]
      public String gxTpr_Pointinfowintit
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Pointinfowintit) ;
         }

         set {
            sdt.gxTpr_Pointinfowintit = (String)(value);
         }

      }

      [DataMember( Name = "PointInfowinDesc" , Order = 7 )]
      public String gxTpr_Pointinfowindesc
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Pointinfowindesc) ;
         }

         set {
            sdt.gxTpr_Pointinfowindesc = (String)(value);
         }

      }

      [DataMember( Name = "PointInfowinLink" , Order = 8 )]
      public String gxTpr_Pointinfowinlink
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Pointinfowinlink) ;
         }

         set {
            sdt.gxTpr_Pointinfowinlink = (String)(value);
         }

      }

      [DataMember( Name = "PointInfowinLinkDsc" , Order = 9 )]
      public String gxTpr_Pointinfowinlinkdsc
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Pointinfowinlinkdsc) ;
         }

         set {
            sdt.gxTpr_Pointinfowinlinkdsc = (String)(value);
         }

      }

      [DataMember( Name = "PointInfowinImg" , Order = 10 )]
      public String gxTpr_Pointinfowinimg
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Pointinfowinimg) ;
         }

         set {
            sdt.gxTpr_Pointinfowinimg = (String)(value);
         }

      }

      public SdtGxMap_Point sdt
      {
         get {
            return (SdtGxMap_Point)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtGxMap_Point() ;
         }
      }

   }

}
