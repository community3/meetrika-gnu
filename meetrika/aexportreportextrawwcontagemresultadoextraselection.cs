/*
               File: ExportReportExtraWWContagemResultadoExtraSelection
        Description: Export Report Extra WWContagem Resultado Extra Selection
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 22:58:1.24
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class aexportreportextrawwcontagemresultadoextraselection : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            if ( ! entryPointCalled )
            {
               AV12Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV14ContagemResultado_StatusCnt = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV220TFContagemResultado_Agrupador = GetNextPar( );
                  AV221TFContagemResultado_Agrupador_Sel = GetNextPar( );
                  AV224TFContagemResultado_DemandaFM = GetNextPar( );
                  AV225TFContagemResultado_DemandaFM_Sel = GetNextPar( );
                  AV222TFContagemResultado_Demanda = GetNextPar( );
                  AV223TFContagemResultado_Demanda_Sel = GetNextPar( );
                  AV226TFContagemResultado_Descricao = GetNextPar( );
                  AV227TFContagemResultado_Descricao_Sel = GetNextPar( );
                  AV273TFContagemResultado_DataPrevista = context.localUtil.ParseDTimeParm( GetNextPar( ));
                  AV274TFContagemResultado_DataPrevista_To = context.localUtil.ParseDTimeParm( GetNextPar( ));
                  AV271TFContagemResultado_DataDmn = context.localUtil.ParseDateParm( GetNextPar( ));
                  AV272TFContagemResultado_DataDmn_To = context.localUtil.ParseDateParm( GetNextPar( ));
                  AV228TFContagemResultado_DataUltCnt = context.localUtil.ParseDateParm( GetNextPar( ));
                  AV229TFContagemResultado_DataUltCnt_To = context.localUtil.ParseDateParm( GetNextPar( ));
                  AV230TFContagemResultado_ContratadaSigla = GetNextPar( );
                  AV231TFContagemResultado_ContratadaSigla_Sel = GetNextPar( );
                  AV232TFContagemResultado_SistemaCoord = GetNextPar( );
                  AV233TFContagemResultado_SistemaCoord_Sel = GetNextPar( );
                  AV234TFContagemResultado_StatusDmn_SelsJson = GetNextPar( );
                  AV238TFContagemResultado_Baseline_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV239TFContagemResultado_ServicoSigla = GetNextPar( );
                  AV240TFContagemResultado_ServicoSigla_Sel = GetNextPar( );
                  AV241TFContagemResultado_PFFinal = NumberUtil.Val( GetNextPar( ), ".");
                  AV242TFContagemResultado_PFFinal_To = NumberUtil.Val( GetNextPar( ), ".");
                  AV243TFContagemResultado_ValorPF = NumberUtil.Val( GetNextPar( ), ".");
                  AV244TFContagemResultado_ValorPF_To = NumberUtil.Val( GetNextPar( ), ".");
                  AV10OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV11OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  AV49GridStateXML = GetNextPar( );
               }
            }
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public aexportreportextrawwcontagemresultadoextraselection( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public aexportreportextrawwcontagemresultadoextraselection( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_AreaTrabalhoCod ,
                           short aP1_ContagemResultado_StatusCnt ,
                           String aP2_TFContagemResultado_Agrupador ,
                           String aP3_TFContagemResultado_Agrupador_Sel ,
                           String aP4_TFContagemResultado_DemandaFM ,
                           String aP5_TFContagemResultado_DemandaFM_Sel ,
                           String aP6_TFContagemResultado_Demanda ,
                           String aP7_TFContagemResultado_Demanda_Sel ,
                           String aP8_TFContagemResultado_Descricao ,
                           String aP9_TFContagemResultado_Descricao_Sel ,
                           DateTime aP10_TFContagemResultado_DataPrevista ,
                           DateTime aP11_TFContagemResultado_DataPrevista_To ,
                           DateTime aP12_TFContagemResultado_DataDmn ,
                           DateTime aP13_TFContagemResultado_DataDmn_To ,
                           DateTime aP14_TFContagemResultado_DataUltCnt ,
                           DateTime aP15_TFContagemResultado_DataUltCnt_To ,
                           String aP16_TFContagemResultado_ContratadaSigla ,
                           String aP17_TFContagemResultado_ContratadaSigla_Sel ,
                           String aP18_TFContagemResultado_SistemaCoord ,
                           String aP19_TFContagemResultado_SistemaCoord_Sel ,
                           String aP20_TFContagemResultado_StatusDmn_SelsJson ,
                           short aP21_TFContagemResultado_Baseline_Sel ,
                           String aP22_TFContagemResultado_ServicoSigla ,
                           String aP23_TFContagemResultado_ServicoSigla_Sel ,
                           decimal aP24_TFContagemResultado_PFFinal ,
                           decimal aP25_TFContagemResultado_PFFinal_To ,
                           decimal aP26_TFContagemResultado_ValorPF ,
                           decimal aP27_TFContagemResultado_ValorPF_To ,
                           short aP28_OrderedBy ,
                           bool aP29_OrderedDsc ,
                           String aP30_GridStateXML )
      {
         this.AV12Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         this.AV14ContagemResultado_StatusCnt = aP1_ContagemResultado_StatusCnt;
         this.AV220TFContagemResultado_Agrupador = aP2_TFContagemResultado_Agrupador;
         this.AV221TFContagemResultado_Agrupador_Sel = aP3_TFContagemResultado_Agrupador_Sel;
         this.AV224TFContagemResultado_DemandaFM = aP4_TFContagemResultado_DemandaFM;
         this.AV225TFContagemResultado_DemandaFM_Sel = aP5_TFContagemResultado_DemandaFM_Sel;
         this.AV222TFContagemResultado_Demanda = aP6_TFContagemResultado_Demanda;
         this.AV223TFContagemResultado_Demanda_Sel = aP7_TFContagemResultado_Demanda_Sel;
         this.AV226TFContagemResultado_Descricao = aP8_TFContagemResultado_Descricao;
         this.AV227TFContagemResultado_Descricao_Sel = aP9_TFContagemResultado_Descricao_Sel;
         this.AV273TFContagemResultado_DataPrevista = aP10_TFContagemResultado_DataPrevista;
         this.AV274TFContagemResultado_DataPrevista_To = aP11_TFContagemResultado_DataPrevista_To;
         this.AV271TFContagemResultado_DataDmn = aP12_TFContagemResultado_DataDmn;
         this.AV272TFContagemResultado_DataDmn_To = aP13_TFContagemResultado_DataDmn_To;
         this.AV228TFContagemResultado_DataUltCnt = aP14_TFContagemResultado_DataUltCnt;
         this.AV229TFContagemResultado_DataUltCnt_To = aP15_TFContagemResultado_DataUltCnt_To;
         this.AV230TFContagemResultado_ContratadaSigla = aP16_TFContagemResultado_ContratadaSigla;
         this.AV231TFContagemResultado_ContratadaSigla_Sel = aP17_TFContagemResultado_ContratadaSigla_Sel;
         this.AV232TFContagemResultado_SistemaCoord = aP18_TFContagemResultado_SistemaCoord;
         this.AV233TFContagemResultado_SistemaCoord_Sel = aP19_TFContagemResultado_SistemaCoord_Sel;
         this.AV234TFContagemResultado_StatusDmn_SelsJson = aP20_TFContagemResultado_StatusDmn_SelsJson;
         this.AV238TFContagemResultado_Baseline_Sel = aP21_TFContagemResultado_Baseline_Sel;
         this.AV239TFContagemResultado_ServicoSigla = aP22_TFContagemResultado_ServicoSigla;
         this.AV240TFContagemResultado_ServicoSigla_Sel = aP23_TFContagemResultado_ServicoSigla_Sel;
         this.AV241TFContagemResultado_PFFinal = aP24_TFContagemResultado_PFFinal;
         this.AV242TFContagemResultado_PFFinal_To = aP25_TFContagemResultado_PFFinal_To;
         this.AV243TFContagemResultado_ValorPF = aP26_TFContagemResultado_ValorPF;
         this.AV244TFContagemResultado_ValorPF_To = aP27_TFContagemResultado_ValorPF_To;
         this.AV10OrderedBy = aP28_OrderedBy;
         this.AV11OrderedDsc = aP29_OrderedDsc;
         this.AV49GridStateXML = aP30_GridStateXML;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Contratada_AreaTrabalhoCod ,
                                 short aP1_ContagemResultado_StatusCnt ,
                                 String aP2_TFContagemResultado_Agrupador ,
                                 String aP3_TFContagemResultado_Agrupador_Sel ,
                                 String aP4_TFContagemResultado_DemandaFM ,
                                 String aP5_TFContagemResultado_DemandaFM_Sel ,
                                 String aP6_TFContagemResultado_Demanda ,
                                 String aP7_TFContagemResultado_Demanda_Sel ,
                                 String aP8_TFContagemResultado_Descricao ,
                                 String aP9_TFContagemResultado_Descricao_Sel ,
                                 DateTime aP10_TFContagemResultado_DataPrevista ,
                                 DateTime aP11_TFContagemResultado_DataPrevista_To ,
                                 DateTime aP12_TFContagemResultado_DataDmn ,
                                 DateTime aP13_TFContagemResultado_DataDmn_To ,
                                 DateTime aP14_TFContagemResultado_DataUltCnt ,
                                 DateTime aP15_TFContagemResultado_DataUltCnt_To ,
                                 String aP16_TFContagemResultado_ContratadaSigla ,
                                 String aP17_TFContagemResultado_ContratadaSigla_Sel ,
                                 String aP18_TFContagemResultado_SistemaCoord ,
                                 String aP19_TFContagemResultado_SistemaCoord_Sel ,
                                 String aP20_TFContagemResultado_StatusDmn_SelsJson ,
                                 short aP21_TFContagemResultado_Baseline_Sel ,
                                 String aP22_TFContagemResultado_ServicoSigla ,
                                 String aP23_TFContagemResultado_ServicoSigla_Sel ,
                                 decimal aP24_TFContagemResultado_PFFinal ,
                                 decimal aP25_TFContagemResultado_PFFinal_To ,
                                 decimal aP26_TFContagemResultado_ValorPF ,
                                 decimal aP27_TFContagemResultado_ValorPF_To ,
                                 short aP28_OrderedBy ,
                                 bool aP29_OrderedDsc ,
                                 String aP30_GridStateXML )
      {
         aexportreportextrawwcontagemresultadoextraselection objaexportreportextrawwcontagemresultadoextraselection;
         objaexportreportextrawwcontagemresultadoextraselection = new aexportreportextrawwcontagemresultadoextraselection();
         objaexportreportextrawwcontagemresultadoextraselection.AV12Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         objaexportreportextrawwcontagemresultadoextraselection.AV14ContagemResultado_StatusCnt = aP1_ContagemResultado_StatusCnt;
         objaexportreportextrawwcontagemresultadoextraselection.AV220TFContagemResultado_Agrupador = aP2_TFContagemResultado_Agrupador;
         objaexportreportextrawwcontagemresultadoextraselection.AV221TFContagemResultado_Agrupador_Sel = aP3_TFContagemResultado_Agrupador_Sel;
         objaexportreportextrawwcontagemresultadoextraselection.AV224TFContagemResultado_DemandaFM = aP4_TFContagemResultado_DemandaFM;
         objaexportreportextrawwcontagemresultadoextraselection.AV225TFContagemResultado_DemandaFM_Sel = aP5_TFContagemResultado_DemandaFM_Sel;
         objaexportreportextrawwcontagemresultadoextraselection.AV222TFContagemResultado_Demanda = aP6_TFContagemResultado_Demanda;
         objaexportreportextrawwcontagemresultadoextraselection.AV223TFContagemResultado_Demanda_Sel = aP7_TFContagemResultado_Demanda_Sel;
         objaexportreportextrawwcontagemresultadoextraselection.AV226TFContagemResultado_Descricao = aP8_TFContagemResultado_Descricao;
         objaexportreportextrawwcontagemresultadoextraselection.AV227TFContagemResultado_Descricao_Sel = aP9_TFContagemResultado_Descricao_Sel;
         objaexportreportextrawwcontagemresultadoextraselection.AV273TFContagemResultado_DataPrevista = aP10_TFContagemResultado_DataPrevista;
         objaexportreportextrawwcontagemresultadoextraselection.AV274TFContagemResultado_DataPrevista_To = aP11_TFContagemResultado_DataPrevista_To;
         objaexportreportextrawwcontagemresultadoextraselection.AV271TFContagemResultado_DataDmn = aP12_TFContagemResultado_DataDmn;
         objaexportreportextrawwcontagemresultadoextraselection.AV272TFContagemResultado_DataDmn_To = aP13_TFContagemResultado_DataDmn_To;
         objaexportreportextrawwcontagemresultadoextraselection.AV228TFContagemResultado_DataUltCnt = aP14_TFContagemResultado_DataUltCnt;
         objaexportreportextrawwcontagemresultadoextraselection.AV229TFContagemResultado_DataUltCnt_To = aP15_TFContagemResultado_DataUltCnt_To;
         objaexportreportextrawwcontagemresultadoextraselection.AV230TFContagemResultado_ContratadaSigla = aP16_TFContagemResultado_ContratadaSigla;
         objaexportreportextrawwcontagemresultadoextraselection.AV231TFContagemResultado_ContratadaSigla_Sel = aP17_TFContagemResultado_ContratadaSigla_Sel;
         objaexportreportextrawwcontagemresultadoextraselection.AV232TFContagemResultado_SistemaCoord = aP18_TFContagemResultado_SistemaCoord;
         objaexportreportextrawwcontagemresultadoextraselection.AV233TFContagemResultado_SistemaCoord_Sel = aP19_TFContagemResultado_SistemaCoord_Sel;
         objaexportreportextrawwcontagemresultadoextraselection.AV234TFContagemResultado_StatusDmn_SelsJson = aP20_TFContagemResultado_StatusDmn_SelsJson;
         objaexportreportextrawwcontagemresultadoextraselection.AV238TFContagemResultado_Baseline_Sel = aP21_TFContagemResultado_Baseline_Sel;
         objaexportreportextrawwcontagemresultadoextraselection.AV239TFContagemResultado_ServicoSigla = aP22_TFContagemResultado_ServicoSigla;
         objaexportreportextrawwcontagemresultadoextraselection.AV240TFContagemResultado_ServicoSigla_Sel = aP23_TFContagemResultado_ServicoSigla_Sel;
         objaexportreportextrawwcontagemresultadoextraselection.AV241TFContagemResultado_PFFinal = aP24_TFContagemResultado_PFFinal;
         objaexportreportextrawwcontagemresultadoextraselection.AV242TFContagemResultado_PFFinal_To = aP25_TFContagemResultado_PFFinal_To;
         objaexportreportextrawwcontagemresultadoextraselection.AV243TFContagemResultado_ValorPF = aP26_TFContagemResultado_ValorPF;
         objaexportreportextrawwcontagemresultadoextraselection.AV244TFContagemResultado_ValorPF_To = aP27_TFContagemResultado_ValorPF_To;
         objaexportreportextrawwcontagemresultadoextraselection.AV10OrderedBy = aP28_OrderedBy;
         objaexportreportextrawwcontagemresultadoextraselection.AV11OrderedDsc = aP29_OrderedDsc;
         objaexportreportextrawwcontagemresultadoextraselection.AV49GridStateXML = aP30_GridStateXML;
         objaexportreportextrawwcontagemresultadoextraselection.context.SetSubmitInitialConfig(context);
         objaexportreportextrawwcontagemresultadoextraselection.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objaexportreportextrawwcontagemresultadoextraselection);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((aexportreportextrawwcontagemresultadoextraselection)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 6;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 1, 1, 15840, 12240, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*6));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            AV197Contratadas.FromXml(AV198WebSession.Get("Contratadas"), "Collection");
            new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
            /* Execute user subroutine: 'PRINTMAINTITLE' */
            S111 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Execute user subroutine: 'PRINTFILTERS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Execute user subroutine: 'PRINTCOLUMNTITLES' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Execute user subroutine: 'PRINTDATA' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Execute user subroutine: 'PRINTFOOTER' */
            S171 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            H320( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'PRINTMAINTITLE' Routine */
         if ( AV9WWPContext.gxTpr_Userehgestor )
         {
            AV175i = 1;
            while ( true )
            {
               AV176p = (short)(StringUtil.StringSearch( AV9WWPContext.gxTpr_Servicosgeridos, ",", (int)(AV175i)));
               if ( AV176p == 0 )
               {
                  if (true) break;
               }
               AV176p = (short)(AV176p-1);
               AV177Servico = (int)(NumberUtil.Val( StringUtil.Substring( AV9WWPContext.gxTpr_Servicosgeridos, (int)(AV175i), AV176p), "."));
               AV178ServicosGeridos.Add(AV177Servico, 0);
               AV175i = (long)(AV176p+2);
            }
         }
         H320( false, 56) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 18, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("Lista de Resultado das Contagens", 5, Gx_line+5, 785, Gx_line+35, 1, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+56);
      }

      protected void S121( )
      {
         /* 'PRINTFILTERS' Routine */
         if ( ! (0==AV12Contratada_AreaTrabalhoCod) )
         {
            H320( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("�rea de Trabalho", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV12Contratada_AreaTrabalhoCod), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! (0==AV14ContagemResultado_StatusCnt) )
         {
            AV13FilterContagemResultado_StatusCntValueDescription = gxdomainstatuscontagem.getDescription(context,AV14ContagemResultado_StatusCnt);
            H320( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Status", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV13FilterContagemResultado_StatusCntValueDescription, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         AV50GridState.gxTpr_Dynamicfilters.FromXml(AV49GridStateXML, "");
         if ( AV50GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV51GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV50GridState.gxTpr_Dynamicfilters.Item(1));
            AV16DynamicFiltersSelector1 = AV51GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATACNT") == 0 )
            {
               AV17ContagemResultado_DataCnt1 = context.localUtil.CToD( AV51GridStateDynamicFilter.gxTpr_Value, 2);
               AV18ContagemResultado_DataCnt_To1 = context.localUtil.CToD( AV51GridStateDynamicFilter.gxTpr_Valueto, 2);
               if ( ! (DateTime.MinValue==AV17ContagemResultado_DataCnt1) )
               {
                  AV52FilterContagemResultado_DataCntDescription = "Data da Cnt";
                  AV53ContagemResultado_DataCnt = AV17ContagemResultado_DataCnt1;
                  H320( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV52FilterContagemResultado_DataCntDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV53ContagemResultado_DataCnt, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
               if ( ! (DateTime.MinValue==AV18ContagemResultado_DataCnt_To1) )
               {
                  AV52FilterContagemResultado_DataCntDescription = "Data da Cnt (at�)";
                  AV53ContagemResultado_DataCnt = AV18ContagemResultado_DataCnt_To1;
                  H320( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV52FilterContagemResultado_DataCntDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV53ContagemResultado_DataCnt, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
            {
               AV24ContagemResultado_StatusDmn1 = AV51GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24ContagemResultado_StatusDmn1)) )
               {
                  AV26FilterContagemResultado_StatusDmn1ValueDescription = gxdomainstatusdemanda.getDescription(context,AV24ContagemResultado_StatusDmn1);
                  AV25FilterContagemResultado_StatusDmnValueDescription = AV26FilterContagemResultado_StatusDmn1ValueDescription;
                  H320( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Status da Dmn", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV25FilterContagemResultado_StatusDmnValueDescription, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
            {
               AV191DynamicFiltersOperator1 = AV51GridStateDynamicFilter.gxTpr_Operator;
               AV19ContagemResultado_OsFsOsFm1 = AV51GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19ContagemResultado_OsFsOsFm1)) )
               {
                  if ( AV191DynamicFiltersOperator1 == 0 )
                  {
                     AV192FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Igual)";
                  }
                  else if ( AV191DynamicFiltersOperator1 == 1 )
                  {
                     AV192FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Come�a com)";
                  }
                  else if ( AV191DynamicFiltersOperator1 == 2 )
                  {
                     AV192FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Cont�m)";
                  }
                  AV54ContagemResultado_OsFsOsFm = AV19ContagemResultado_OsFsOsFm1;
                  H320( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV192FilterContagemResultado_OsFsOsFmDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV54ContagemResultado_OsFsOsFm, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 )
            {
               AV247ContagemResultado_DataDmn1 = context.localUtil.CToD( AV51GridStateDynamicFilter.gxTpr_Value, 2);
               AV248ContagemResultado_DataDmn_To1 = context.localUtil.CToD( AV51GridStateDynamicFilter.gxTpr_Valueto, 2);
               if ( ! (DateTime.MinValue==AV247ContagemResultado_DataDmn1) )
               {
                  AV249FilterContagemResultado_DataDmnDescription = "Data da Dmn";
                  AV250ContagemResultado_DataDmn = AV247ContagemResultado_DataDmn1;
                  H320( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV249FilterContagemResultado_DataDmnDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV250ContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
               if ( ! (DateTime.MinValue==AV248ContagemResultado_DataDmn_To1) )
               {
                  AV249FilterContagemResultado_DataDmnDescription = "Data da Dmn (at�)";
                  AV250ContagemResultado_DataDmn = AV248ContagemResultado_DataDmn_To1;
                  H320( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV249FilterContagemResultado_DataDmnDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV250ContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
            {
               AV251ContagemResultado_DataPrevista1 = context.localUtil.CToD( AV51GridStateDynamicFilter.gxTpr_Value, 2);
               AV252ContagemResultado_DataPrevista_To1 = context.localUtil.CToD( AV51GridStateDynamicFilter.gxTpr_Valueto, 2);
               if ( ! (DateTime.MinValue==AV251ContagemResultado_DataPrevista1) )
               {
                  AV253FilterContagemResultado_DataPrevistaDescription = "Data Prevista";
                  AV254ContagemResultado_DataPrevista = AV251ContagemResultado_DataPrevista1;
                  H320( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV253FilterContagemResultado_DataPrevistaDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV254ContagemResultado_DataPrevista, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
               if ( ! (DateTime.MinValue==AV252ContagemResultado_DataPrevista_To1) )
               {
                  AV253FilterContagemResultado_DataPrevistaDescription = "Data Prevista (at�)";
                  AV254ContagemResultado_DataPrevista = AV252ContagemResultado_DataPrevista_To1;
                  H320( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV253FilterContagemResultado_DataPrevistaDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV254ContagemResultado_DataPrevista, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADO_SERVICO") == 0 )
            {
               AV102ContagemResultado_Servico1 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV102ContagemResultado_Servico1) )
               {
                  AV105ContagemResultado_Servico = AV102ContagemResultado_Servico1;
                  H320( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Servi�o", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV105ContagemResultado_Servico), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
            {
               AV118ContagemResultado_ContadorFM1 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV118ContagemResultado_ContadorFM1) )
               {
                  AV155ContagemResultado_ContadorFM = AV118ContagemResultado_ContadorFM1;
                  H320( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Usu�rio da Prestadora", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV155ContagemResultado_ContadorFM), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
            {
               AV21ContagemResultado_SistemaCod1 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV21ContagemResultado_SistemaCod1) )
               {
                  AV56ContagemResultado_SistemaCod = AV21ContagemResultado_SistemaCod1;
                  H320( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Sistema", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV56ContagemResultado_SistemaCod), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
            {
               AV22ContagemResultado_ContratadaCod1 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV22ContagemResultado_ContratadaCod1) )
               {
                  AV57ContagemResultado_ContratadaCod = AV22ContagemResultado_ContratadaCod1;
                  H320( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Prestadora", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV57ContagemResultado_ContratadaCod), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 )
            {
               AV114ContagemResultado_ServicoGrupo1 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV114ContagemResultado_ServicoGrupo1) )
               {
                  AV117ContagemResultado_ServicoGrupo = AV114ContagemResultado_ServicoGrupo1;
                  H320( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Grupo de Servi�os", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV117ContagemResultado_ServicoGrupo), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADO_BASELINE") == 0 )
            {
               AV90ContagemResultado_Baseline1 = AV51GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90ContagemResultado_Baseline1)) )
               {
                  if ( StringUtil.StrCmp(StringUtil.Trim( AV90ContagemResultado_Baseline1), "/") == 0 )
                  {
                     AV95FilterContagemResultado_Baseline1ValueDescription = "(Todos)";
                  }
                  else if ( StringUtil.StrCmp(StringUtil.Trim( AV90ContagemResultado_Baseline1), "S") == 0 )
                  {
                     AV95FilterContagemResultado_Baseline1ValueDescription = "Sim";
                  }
                  else if ( StringUtil.StrCmp(StringUtil.Trim( AV90ContagemResultado_Baseline1), "N") == 0 )
                  {
                     AV95FilterContagemResultado_Baseline1ValueDescription = "N�o";
                  }
                  AV94FilterContagemResultado_BaselineValueDescription = AV95FilterContagemResultado_Baseline1ValueDescription;
                  H320( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Baseline", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV94FilterContagemResultado_BaselineValueDescription, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
            {
               AV23ContagemResultado_NaoCnfDmnCod1 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV23ContagemResultado_NaoCnfDmnCod1) )
               {
                  AV58ContagemResultado_NaoCnfDmnCod = AV23ContagemResultado_NaoCnfDmnCod1;
                  H320( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("N�o Conformidade", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV58ContagemResultado_NaoCnfDmnCod), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADO_PFBFM") == 0 )
            {
               AV77ContagemResultado_PFBFM1 = NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, ".");
               if ( ! (Convert.ToDecimal(0)==AV77ContagemResultado_PFBFM1) )
               {
                  AV83ContagemResultado_PFBFM = AV77ContagemResultado_PFBFM1;
                  H320( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("PFB Diferentes", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV83ContagemResultado_PFBFM, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADO_PFBFS") == 0 )
            {
               AV78ContagemResultado_PFBFS1 = NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, ".");
               if ( ! (Convert.ToDecimal(0)==AV78ContagemResultado_PFBFS1) )
               {
                  AV84ContagemResultado_PFBFS = AV78ContagemResultado_PFBFS1;
                  H320( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("PFL Diferentes", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV84ContagemResultado_PFBFS, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
            {
               AV168ContagemResultado_Agrupador1 = AV51GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV168ContagemResultado_Agrupador1)) )
               {
                  AV173ContagemResultado_Agrupador = AV168ContagemResultado_Agrupador1;
                  H320( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Agrupador", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV173ContagemResultado_Agrupador, "@!")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
            {
               AV185ContagemResultado_Descricao1 = AV51GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV185ContagemResultado_Descricao1)) )
               {
                  AV186ContagemResultado_Descricao = AV185ContagemResultado_Descricao1;
                  H320( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Titulo", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV186ContagemResultado_Descricao, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADO_OWNER") == 0 )
            {
               AV179ContagemResultado_Owner1 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV179ContagemResultado_Owner1) )
               {
                  AV180ContagemResultado_Owner = AV179ContagemResultado_Owner1;
                  H320( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Solicitante", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV180ContagemResultado_Owner), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 )
            {
               AV200ContagemResultado_TemPndHmlg1 = AV51GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV200ContagemResultado_TemPndHmlg1)) )
               {
                  if ( StringUtil.StrCmp(StringUtil.Trim( AV200ContagemResultado_TemPndHmlg1), "S") == 0 )
                  {
                     AV211FilterContagemResultado_TemPndHmlg1ValueDescription = "Sem pend�ncias";
                  }
                  else if ( StringUtil.StrCmp(StringUtil.Trim( AV200ContagemResultado_TemPndHmlg1), "C") == 0 )
                  {
                     AV211FilterContagemResultado_TemPndHmlg1ValueDescription = "Com pend�ncias";
                  }
                  AV217FilterContagemResultado_TemPndHmlgValueDescription = AV211FilterContagemResultado_TemPndHmlg1ValueDescription;
                  H320( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Pend�ncias vinculadas", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV217FilterContagemResultado_TemPndHmlgValueDescription, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADO_CNTCOD") == 0 )
            {
               AV282ContagemResultado_CntCod1 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV282ContagemResultado_CntCod1) )
               {
                  AV283ContagemResultado_CntCod = AV282ContagemResultado_CntCod1;
                  H320( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Contrato", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV283ContagemResultado_CntCod), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            if ( AV50GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV27DynamicFiltersEnabled2 = true;
               AV51GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV50GridState.gxTpr_Dynamicfilters.Item(2));
               AV28DynamicFiltersSelector2 = AV51GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV28DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATACNT") == 0 )
               {
                  AV29ContagemResultado_DataCnt2 = context.localUtil.CToD( AV51GridStateDynamicFilter.gxTpr_Value, 2);
                  AV30ContagemResultado_DataCnt_To2 = context.localUtil.CToD( AV51GridStateDynamicFilter.gxTpr_Valueto, 2);
                  if ( ! (DateTime.MinValue==AV29ContagemResultado_DataCnt2) )
                  {
                     AV52FilterContagemResultado_DataCntDescription = "Data da Cnt";
                     AV53ContagemResultado_DataCnt = AV29ContagemResultado_DataCnt2;
                     H320( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV52FilterContagemResultado_DataCntDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(context.localUtil.Format( AV53ContagemResultado_DataCnt, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
                  if ( ! (DateTime.MinValue==AV30ContagemResultado_DataCnt_To2) )
                  {
                     AV52FilterContagemResultado_DataCntDescription = "Data da Cnt (at�)";
                     AV53ContagemResultado_DataCnt = AV30ContagemResultado_DataCnt_To2;
                     H320( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV52FilterContagemResultado_DataCntDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(context.localUtil.Format( AV53ContagemResultado_DataCnt, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
               {
                  AV36ContagemResultado_StatusDmn2 = AV51GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36ContagemResultado_StatusDmn2)) )
                  {
                     AV37FilterContagemResultado_StatusDmn2ValueDescription = gxdomainstatusdemanda.getDescription(context,AV36ContagemResultado_StatusDmn2);
                     AV25FilterContagemResultado_StatusDmnValueDescription = AV37FilterContagemResultado_StatusDmn2ValueDescription;
                     H320( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Status da Dmn", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV25FilterContagemResultado_StatusDmnValueDescription, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
               {
                  AV193DynamicFiltersOperator2 = AV51GridStateDynamicFilter.gxTpr_Operator;
                  AV31ContagemResultado_OsFsOsFm2 = AV51GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31ContagemResultado_OsFsOsFm2)) )
                  {
                     if ( AV193DynamicFiltersOperator2 == 0 )
                     {
                        AV192FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Igual)";
                     }
                     else if ( AV193DynamicFiltersOperator2 == 1 )
                     {
                        AV192FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Come�a com)";
                     }
                     else if ( AV193DynamicFiltersOperator2 == 2 )
                     {
                        AV192FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Cont�m)";
                     }
                     AV54ContagemResultado_OsFsOsFm = AV31ContagemResultado_OsFsOsFm2;
                     H320( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV192FilterContagemResultado_OsFsOsFmDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV54ContagemResultado_OsFsOsFm, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 )
               {
                  AV255ContagemResultado_DataDmn2 = context.localUtil.CToD( AV51GridStateDynamicFilter.gxTpr_Value, 2);
                  AV256ContagemResultado_DataDmn_To2 = context.localUtil.CToD( AV51GridStateDynamicFilter.gxTpr_Valueto, 2);
                  if ( ! (DateTime.MinValue==AV255ContagemResultado_DataDmn2) )
                  {
                     AV249FilterContagemResultado_DataDmnDescription = "Data da Dmn";
                     AV250ContagemResultado_DataDmn = AV255ContagemResultado_DataDmn2;
                     H320( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV249FilterContagemResultado_DataDmnDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(context.localUtil.Format( AV250ContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
                  if ( ! (DateTime.MinValue==AV256ContagemResultado_DataDmn_To2) )
                  {
                     AV249FilterContagemResultado_DataDmnDescription = "Data da Dmn (at�)";
                     AV250ContagemResultado_DataDmn = AV256ContagemResultado_DataDmn_To2;
                     H320( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV249FilterContagemResultado_DataDmnDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(context.localUtil.Format( AV250ContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
               {
                  AV257ContagemResultado_DataPrevista2 = context.localUtil.CToD( AV51GridStateDynamicFilter.gxTpr_Value, 2);
                  AV258ContagemResultado_DataPrevista_To2 = context.localUtil.CToD( AV51GridStateDynamicFilter.gxTpr_Valueto, 2);
                  if ( ! (DateTime.MinValue==AV257ContagemResultado_DataPrevista2) )
                  {
                     AV253FilterContagemResultado_DataPrevistaDescription = "Data Prevista";
                     AV254ContagemResultado_DataPrevista = AV257ContagemResultado_DataPrevista2;
                     H320( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV253FilterContagemResultado_DataPrevistaDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(context.localUtil.Format( AV254ContagemResultado_DataPrevista, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
                  if ( ! (DateTime.MinValue==AV258ContagemResultado_DataPrevista_To2) )
                  {
                     AV253FilterContagemResultado_DataPrevistaDescription = "Data Prevista (at�)";
                     AV254ContagemResultado_DataPrevista = AV258ContagemResultado_DataPrevista_To2;
                     H320( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV253FilterContagemResultado_DataPrevistaDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(context.localUtil.Format( AV254ContagemResultado_DataPrevista, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector2, "CONTAGEMRESULTADO_SERVICO") == 0 )
               {
                  AV103ContagemResultado_Servico2 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV103ContagemResultado_Servico2) )
                  {
                     AV105ContagemResultado_Servico = AV103ContagemResultado_Servico2;
                     H320( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Servi�o", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV105ContagemResultado_Servico), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector2, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
               {
                  AV119ContagemResultado_ContadorFM2 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV119ContagemResultado_ContadorFM2) )
                  {
                     AV155ContagemResultado_ContadorFM = AV119ContagemResultado_ContadorFM2;
                     H320( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Usu�rio da Prestadora", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV155ContagemResultado_ContadorFM), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
               {
                  AV33ContagemResultado_SistemaCod2 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV33ContagemResultado_SistemaCod2) )
                  {
                     AV56ContagemResultado_SistemaCod = AV33ContagemResultado_SistemaCod2;
                     H320( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Sistema", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV56ContagemResultado_SistemaCod), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
               {
                  AV34ContagemResultado_ContratadaCod2 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV34ContagemResultado_ContratadaCod2) )
                  {
                     AV57ContagemResultado_ContratadaCod = AV34ContagemResultado_ContratadaCod2;
                     H320( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Prestadora", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV57ContagemResultado_ContratadaCod), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector2, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 )
               {
                  AV115ContagemResultado_ServicoGrupo2 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV115ContagemResultado_ServicoGrupo2) )
                  {
                     AV117ContagemResultado_ServicoGrupo = AV115ContagemResultado_ServicoGrupo2;
                     H320( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Grupo de Servi�os", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV117ContagemResultado_ServicoGrupo), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector2, "CONTAGEMRESULTADO_BASELINE") == 0 )
               {
                  AV91ContagemResultado_Baseline2 = AV51GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91ContagemResultado_Baseline2)) )
                  {
                     if ( StringUtil.StrCmp(StringUtil.Trim( AV91ContagemResultado_Baseline2), "/") == 0 )
                     {
                        AV96FilterContagemResultado_Baseline2ValueDescription = "(Todos)";
                     }
                     else if ( StringUtil.StrCmp(StringUtil.Trim( AV91ContagemResultado_Baseline2), "S") == 0 )
                     {
                        AV96FilterContagemResultado_Baseline2ValueDescription = "Sim";
                     }
                     else if ( StringUtil.StrCmp(StringUtil.Trim( AV91ContagemResultado_Baseline2), "N") == 0 )
                     {
                        AV96FilterContagemResultado_Baseline2ValueDescription = "N�o";
                     }
                     AV94FilterContagemResultado_BaselineValueDescription = AV96FilterContagemResultado_Baseline2ValueDescription;
                     H320( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Baseline", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV94FilterContagemResultado_BaselineValueDescription, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector2, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
               {
                  AV35ContagemResultado_NaoCnfDmnCod2 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV35ContagemResultado_NaoCnfDmnCod2) )
                  {
                     AV58ContagemResultado_NaoCnfDmnCod = AV35ContagemResultado_NaoCnfDmnCod2;
                     H320( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("N�o Conformidade", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV58ContagemResultado_NaoCnfDmnCod), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector2, "CONTAGEMRESULTADO_PFBFM") == 0 )
               {
                  AV79ContagemResultado_PFBFM2 = NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, ".");
                  if ( ! (Convert.ToDecimal(0)==AV79ContagemResultado_PFBFM2) )
                  {
                     AV83ContagemResultado_PFBFM = AV79ContagemResultado_PFBFM2;
                     H320( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("PFB Diferentes", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV83ContagemResultado_PFBFM, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector2, "CONTAGEMRESULTADO_PFBFS") == 0 )
               {
                  AV80ContagemResultado_PFBFS2 = NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, ".");
                  if ( ! (Convert.ToDecimal(0)==AV80ContagemResultado_PFBFS2) )
                  {
                     AV84ContagemResultado_PFBFS = AV80ContagemResultado_PFBFS2;
                     H320( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("PFL Diferentes", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV84ContagemResultado_PFBFS, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
               {
                  AV169ContagemResultado_Agrupador2 = AV51GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV169ContagemResultado_Agrupador2)) )
                  {
                     AV173ContagemResultado_Agrupador = AV169ContagemResultado_Agrupador2;
                     H320( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Agrupador", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV173ContagemResultado_Agrupador, "@!")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
               {
                  AV187ContagemResultado_Descricao2 = AV51GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV187ContagemResultado_Descricao2)) )
                  {
                     AV186ContagemResultado_Descricao = AV187ContagemResultado_Descricao2;
                     H320( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Titulo", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV186ContagemResultado_Descricao, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector2, "CONTAGEMRESULTADO_OWNER") == 0 )
               {
                  AV181ContagemResultado_Owner2 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV181ContagemResultado_Owner2) )
                  {
                     AV180ContagemResultado_Owner = AV181ContagemResultado_Owner2;
                     H320( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Solicitante", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV180ContagemResultado_Owner), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector2, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 )
               {
                  AV201ContagemResultado_TemPndHmlg2 = AV51GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV201ContagemResultado_TemPndHmlg2)) )
                  {
                     if ( StringUtil.StrCmp(StringUtil.Trim( AV201ContagemResultado_TemPndHmlg2), "S") == 0 )
                     {
                        AV212FilterContagemResultado_TemPndHmlg2ValueDescription = "Sem pend�ncias";
                     }
                     else if ( StringUtil.StrCmp(StringUtil.Trim( AV201ContagemResultado_TemPndHmlg2), "C") == 0 )
                     {
                        AV212FilterContagemResultado_TemPndHmlg2ValueDescription = "Com pend�ncias";
                     }
                     AV217FilterContagemResultado_TemPndHmlgValueDescription = AV212FilterContagemResultado_TemPndHmlg2ValueDescription;
                     H320( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Pend�ncias vinculadas", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV217FilterContagemResultado_TemPndHmlgValueDescription, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector2, "CONTAGEMRESULTADO_CNTCOD") == 0 )
               {
                  AV284ContagemResultado_CntCod2 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV284ContagemResultado_CntCod2) )
                  {
                     AV283ContagemResultado_CntCod = AV284ContagemResultado_CntCod2;
                     H320( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Contrato", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV283ContagemResultado_CntCod), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               if ( AV50GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV38DynamicFiltersEnabled3 = true;
                  AV51GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV50GridState.gxTpr_Dynamicfilters.Item(3));
                  AV39DynamicFiltersSelector3 = AV51GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATACNT") == 0 )
                  {
                     AV40ContagemResultado_DataCnt3 = context.localUtil.CToD( AV51GridStateDynamicFilter.gxTpr_Value, 2);
                     AV41ContagemResultado_DataCnt_To3 = context.localUtil.CToD( AV51GridStateDynamicFilter.gxTpr_Valueto, 2);
                     if ( ! (DateTime.MinValue==AV40ContagemResultado_DataCnt3) )
                     {
                        AV52FilterContagemResultado_DataCntDescription = "Data da Cnt";
                        AV53ContagemResultado_DataCnt = AV40ContagemResultado_DataCnt3;
                        H320( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV52FilterContagemResultado_DataCntDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(context.localUtil.Format( AV53ContagemResultado_DataCnt, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                     if ( ! (DateTime.MinValue==AV41ContagemResultado_DataCnt_To3) )
                     {
                        AV52FilterContagemResultado_DataCntDescription = "Data da Cnt (at�)";
                        AV53ContagemResultado_DataCnt = AV41ContagemResultado_DataCnt_To3;
                        H320( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV52FilterContagemResultado_DataCntDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(context.localUtil.Format( AV53ContagemResultado_DataCnt, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                  {
                     AV47ContagemResultado_StatusDmn3 = AV51GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47ContagemResultado_StatusDmn3)) )
                     {
                        AV48FilterContagemResultado_StatusDmn3ValueDescription = gxdomainstatusdemanda.getDescription(context,AV47ContagemResultado_StatusDmn3);
                        AV25FilterContagemResultado_StatusDmnValueDescription = AV48FilterContagemResultado_StatusDmn3ValueDescription;
                        H320( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Status da Dmn", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV25FilterContagemResultado_StatusDmnValueDescription, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
                  {
                     AV194DynamicFiltersOperator3 = AV51GridStateDynamicFilter.gxTpr_Operator;
                     AV42ContagemResultado_OsFsOsFm3 = AV51GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42ContagemResultado_OsFsOsFm3)) )
                     {
                        if ( AV194DynamicFiltersOperator3 == 0 )
                        {
                           AV192FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Igual)";
                        }
                        else if ( AV194DynamicFiltersOperator3 == 1 )
                        {
                           AV192FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Come�a com)";
                        }
                        else if ( AV194DynamicFiltersOperator3 == 2 )
                        {
                           AV192FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Cont�m)";
                        }
                        AV54ContagemResultado_OsFsOsFm = AV42ContagemResultado_OsFsOsFm3;
                        H320( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV192FilterContagemResultado_OsFsOsFmDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV54ContagemResultado_OsFsOsFm, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 )
                  {
                     AV259ContagemResultado_DataDmn3 = context.localUtil.CToD( AV51GridStateDynamicFilter.gxTpr_Value, 2);
                     AV260ContagemResultado_DataDmn_To3 = context.localUtil.CToD( AV51GridStateDynamicFilter.gxTpr_Valueto, 2);
                     if ( ! (DateTime.MinValue==AV259ContagemResultado_DataDmn3) )
                     {
                        AV249FilterContagemResultado_DataDmnDescription = "Data da Dmn";
                        AV250ContagemResultado_DataDmn = AV259ContagemResultado_DataDmn3;
                        H320( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV249FilterContagemResultado_DataDmnDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(context.localUtil.Format( AV250ContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                     if ( ! (DateTime.MinValue==AV260ContagemResultado_DataDmn_To3) )
                     {
                        AV249FilterContagemResultado_DataDmnDescription = "Data da Dmn (at�)";
                        AV250ContagemResultado_DataDmn = AV260ContagemResultado_DataDmn_To3;
                        H320( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV249FilterContagemResultado_DataDmnDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(context.localUtil.Format( AV250ContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
                  {
                     AV261ContagemResultado_DataPrevista3 = context.localUtil.CToD( AV51GridStateDynamicFilter.gxTpr_Value, 2);
                     AV262ContagemResultado_DataPrevista_To3 = context.localUtil.CToD( AV51GridStateDynamicFilter.gxTpr_Valueto, 2);
                     if ( ! (DateTime.MinValue==AV261ContagemResultado_DataPrevista3) )
                     {
                        AV253FilterContagemResultado_DataPrevistaDescription = "Data Prevista";
                        AV254ContagemResultado_DataPrevista = AV261ContagemResultado_DataPrevista3;
                        H320( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV253FilterContagemResultado_DataPrevistaDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(context.localUtil.Format( AV254ContagemResultado_DataPrevista, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                     if ( ! (DateTime.MinValue==AV262ContagemResultado_DataPrevista_To3) )
                     {
                        AV253FilterContagemResultado_DataPrevistaDescription = "Data Prevista (at�)";
                        AV254ContagemResultado_DataPrevista = AV262ContagemResultado_DataPrevista_To3;
                        H320( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV253FilterContagemResultado_DataPrevistaDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(context.localUtil.Format( AV254ContagemResultado_DataPrevista, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "CONTAGEMRESULTADO_SERVICO") == 0 )
                  {
                     AV104ContagemResultado_Servico3 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV104ContagemResultado_Servico3) )
                     {
                        AV105ContagemResultado_Servico = AV104ContagemResultado_Servico3;
                        H320( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Servi�o", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV105ContagemResultado_Servico), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
                  {
                     AV120ContagemResultado_ContadorFM3 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV120ContagemResultado_ContadorFM3) )
                     {
                        AV155ContagemResultado_ContadorFM = AV120ContagemResultado_ContadorFM3;
                        H320( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Usu�rio da Prestadora", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV155ContagemResultado_ContadorFM), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                  {
                     AV44ContagemResultado_SistemaCod3 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV44ContagemResultado_SistemaCod3) )
                     {
                        AV56ContagemResultado_SistemaCod = AV44ContagemResultado_SistemaCod3;
                        H320( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Sistema", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV56ContagemResultado_SistemaCod), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                  {
                     AV45ContagemResultado_ContratadaCod3 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV45ContagemResultado_ContratadaCod3) )
                     {
                        AV57ContagemResultado_ContratadaCod = AV45ContagemResultado_ContratadaCod3;
                        H320( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Prestadora", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV57ContagemResultado_ContratadaCod), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 )
                  {
                     AV116ContagemResultado_ServicoGrupo3 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV116ContagemResultado_ServicoGrupo3) )
                     {
                        AV117ContagemResultado_ServicoGrupo = AV116ContagemResultado_ServicoGrupo3;
                        H320( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Grupo de Servi�os", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV117ContagemResultado_ServicoGrupo), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "CONTAGEMRESULTADO_BASELINE") == 0 )
                  {
                     AV92ContagemResultado_Baseline3 = AV51GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92ContagemResultado_Baseline3)) )
                     {
                        if ( StringUtil.StrCmp(StringUtil.Trim( AV92ContagemResultado_Baseline3), "/") == 0 )
                        {
                           AV97FilterContagemResultado_Baseline3ValueDescription = "(Todos)";
                        }
                        else if ( StringUtil.StrCmp(StringUtil.Trim( AV92ContagemResultado_Baseline3), "S") == 0 )
                        {
                           AV97FilterContagemResultado_Baseline3ValueDescription = "Sim";
                        }
                        else if ( StringUtil.StrCmp(StringUtil.Trim( AV92ContagemResultado_Baseline3), "N") == 0 )
                        {
                           AV97FilterContagemResultado_Baseline3ValueDescription = "N�o";
                        }
                        AV94FilterContagemResultado_BaselineValueDescription = AV97FilterContagemResultado_Baseline3ValueDescription;
                        H320( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Baseline", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV94FilterContagemResultado_BaselineValueDescription, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
                  {
                     AV46ContagemResultado_NaoCnfDmnCod3 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV46ContagemResultado_NaoCnfDmnCod3) )
                     {
                        AV58ContagemResultado_NaoCnfDmnCod = AV46ContagemResultado_NaoCnfDmnCod3;
                        H320( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("N�o Conformidade", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV58ContagemResultado_NaoCnfDmnCod), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "CONTAGEMRESULTADO_PFBFM") == 0 )
                  {
                     AV81ContagemResultado_PFBFM3 = NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, ".");
                     if ( ! (Convert.ToDecimal(0)==AV81ContagemResultado_PFBFM3) )
                     {
                        AV83ContagemResultado_PFBFM = AV81ContagemResultado_PFBFM3;
                        H320( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("PFB Diferentes", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV83ContagemResultado_PFBFM, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "CONTAGEMRESULTADO_PFBFS") == 0 )
                  {
                     AV82ContagemResultado_PFBFS3 = NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, ".");
                     if ( ! (Convert.ToDecimal(0)==AV82ContagemResultado_PFBFS3) )
                     {
                        AV84ContagemResultado_PFBFS = AV82ContagemResultado_PFBFS3;
                        H320( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("PFL Diferentes", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV84ContagemResultado_PFBFS, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
                  {
                     AV170ContagemResultado_Agrupador3 = AV51GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV170ContagemResultado_Agrupador3)) )
                     {
                        AV173ContagemResultado_Agrupador = AV170ContagemResultado_Agrupador3;
                        H320( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Agrupador", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV173ContagemResultado_Agrupador, "@!")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
                  {
                     AV188ContagemResultado_Descricao3 = AV51GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV188ContagemResultado_Descricao3)) )
                     {
                        AV186ContagemResultado_Descricao = AV188ContagemResultado_Descricao3;
                        H320( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Titulo", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV186ContagemResultado_Descricao, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "CONTAGEMRESULTADO_OWNER") == 0 )
                  {
                     AV182ContagemResultado_Owner3 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV182ContagemResultado_Owner3) )
                     {
                        AV180ContagemResultado_Owner = AV182ContagemResultado_Owner3;
                        H320( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Solicitante", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV180ContagemResultado_Owner), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 )
                  {
                     AV202ContagemResultado_TemPndHmlg3 = AV51GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV202ContagemResultado_TemPndHmlg3)) )
                     {
                        if ( StringUtil.StrCmp(StringUtil.Trim( AV202ContagemResultado_TemPndHmlg3), "S") == 0 )
                        {
                           AV213FilterContagemResultado_TemPndHmlg3ValueDescription = "Sem pend�ncias";
                        }
                        else if ( StringUtil.StrCmp(StringUtil.Trim( AV202ContagemResultado_TemPndHmlg3), "C") == 0 )
                        {
                           AV213FilterContagemResultado_TemPndHmlg3ValueDescription = "Com pend�ncias";
                        }
                        AV217FilterContagemResultado_TemPndHmlgValueDescription = AV213FilterContagemResultado_TemPndHmlg3ValueDescription;
                        H320( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Pend�ncias vinculadas", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV217FilterContagemResultado_TemPndHmlgValueDescription, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "CONTAGEMRESULTADO_CNTCOD") == 0 )
                  {
                     AV285ContagemResultado_CntCod3 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV285ContagemResultado_CntCod3) )
                     {
                        AV283ContagemResultado_CntCod = AV285ContagemResultado_CntCod3;
                        H320( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Contrato", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV283ContagemResultado_CntCod), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  if ( AV50GridState.gxTpr_Dynamicfilters.Count >= 4 )
                  {
                     AV121DynamicFiltersEnabled4 = true;
                     AV51GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV50GridState.gxTpr_Dynamicfilters.Item(4));
                     AV122DynamicFiltersSelector4 = AV51GridStateDynamicFilter.gxTpr_Selected;
                     if ( StringUtil.StrCmp(AV122DynamicFiltersSelector4, "CONTAGEMRESULTADO_DATACNT") == 0 )
                     {
                        AV123ContagemResultado_DataCnt4 = context.localUtil.CToD( AV51GridStateDynamicFilter.gxTpr_Value, 2);
                        AV124ContagemResultado_DataCnt_To4 = context.localUtil.CToD( AV51GridStateDynamicFilter.gxTpr_Valueto, 2);
                        if ( ! (DateTime.MinValue==AV123ContagemResultado_DataCnt4) )
                        {
                           AV52FilterContagemResultado_DataCntDescription = "Data da Cnt";
                           AV53ContagemResultado_DataCnt = AV123ContagemResultado_DataCnt4;
                           H320( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV52FilterContagemResultado_DataCntDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(context.localUtil.Format( AV53ContagemResultado_DataCnt, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                        if ( ! (DateTime.MinValue==AV124ContagemResultado_DataCnt_To4) )
                        {
                           AV52FilterContagemResultado_DataCntDescription = "Data da Cnt (at�)";
                           AV53ContagemResultado_DataCnt = AV124ContagemResultado_DataCnt_To4;
                           H320( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV52FilterContagemResultado_DataCntDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(context.localUtil.Format( AV53ContagemResultado_DataCnt, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                     }
                     else if ( StringUtil.StrCmp(AV122DynamicFiltersSelector4, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                     {
                        AV126ContagemResultado_StatusDmn4 = AV51GridStateDynamicFilter.gxTpr_Value;
                        if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV126ContagemResultado_StatusDmn4)) )
                        {
                           AV127FilterContagemResultado_StatusDmn4ValueDescription = gxdomainstatusdemanda.getDescription(context,AV126ContagemResultado_StatusDmn4);
                           AV25FilterContagemResultado_StatusDmnValueDescription = AV127FilterContagemResultado_StatusDmn4ValueDescription;
                           H320( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText("Status da Dmn", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV25FilterContagemResultado_StatusDmnValueDescription, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                     }
                     else if ( StringUtil.StrCmp(AV122DynamicFiltersSelector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
                     {
                        AV195DynamicFiltersOperator4 = AV51GridStateDynamicFilter.gxTpr_Operator;
                        AV125ContagemResultado_OsFsOsFm4 = AV51GridStateDynamicFilter.gxTpr_Value;
                        if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV125ContagemResultado_OsFsOsFm4)) )
                        {
                           if ( AV195DynamicFiltersOperator4 == 0 )
                           {
                              AV192FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Igual)";
                           }
                           else if ( AV195DynamicFiltersOperator4 == 1 )
                           {
                              AV192FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Come�a com)";
                           }
                           else if ( AV195DynamicFiltersOperator4 == 2 )
                           {
                              AV192FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Cont�m)";
                           }
                           AV54ContagemResultado_OsFsOsFm = AV125ContagemResultado_OsFsOsFm4;
                           H320( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV192FilterContagemResultado_OsFsOsFmDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV54ContagemResultado_OsFsOsFm, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                     }
                     else if ( StringUtil.StrCmp(AV122DynamicFiltersSelector4, "CONTAGEMRESULTADO_DATADMN") == 0 )
                     {
                        AV263ContagemResultado_DataDmn4 = context.localUtil.CToD( AV51GridStateDynamicFilter.gxTpr_Value, 2);
                        AV264ContagemResultado_DataDmn_To4 = context.localUtil.CToD( AV51GridStateDynamicFilter.gxTpr_Valueto, 2);
                        if ( ! (DateTime.MinValue==AV263ContagemResultado_DataDmn4) )
                        {
                           AV249FilterContagemResultado_DataDmnDescription = "Data da Dmn";
                           AV250ContagemResultado_DataDmn = AV263ContagemResultado_DataDmn4;
                           H320( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV249FilterContagemResultado_DataDmnDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(context.localUtil.Format( AV250ContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                        if ( ! (DateTime.MinValue==AV264ContagemResultado_DataDmn_To4) )
                        {
                           AV249FilterContagemResultado_DataDmnDescription = "Data da Dmn (at�)";
                           AV250ContagemResultado_DataDmn = AV264ContagemResultado_DataDmn_To4;
                           H320( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV249FilterContagemResultado_DataDmnDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(context.localUtil.Format( AV250ContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                     }
                     else if ( StringUtil.StrCmp(AV122DynamicFiltersSelector4, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
                     {
                        AV265ContagemResultado_DataPrevista4 = context.localUtil.CToD( AV51GridStateDynamicFilter.gxTpr_Value, 2);
                        AV266ContagemResultado_DataPrevista_To4 = context.localUtil.CToD( AV51GridStateDynamicFilter.gxTpr_Valueto, 2);
                        if ( ! (DateTime.MinValue==AV265ContagemResultado_DataPrevista4) )
                        {
                           AV253FilterContagemResultado_DataPrevistaDescription = "Data Prevista";
                           AV254ContagemResultado_DataPrevista = AV265ContagemResultado_DataPrevista4;
                           H320( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV253FilterContagemResultado_DataPrevistaDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(context.localUtil.Format( AV254ContagemResultado_DataPrevista, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                        if ( ! (DateTime.MinValue==AV266ContagemResultado_DataPrevista_To4) )
                        {
                           AV253FilterContagemResultado_DataPrevistaDescription = "Data Prevista (at�)";
                           AV254ContagemResultado_DataPrevista = AV266ContagemResultado_DataPrevista_To4;
                           H320( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV253FilterContagemResultado_DataPrevistaDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(context.localUtil.Format( AV254ContagemResultado_DataPrevista, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                     }
                     else if ( StringUtil.StrCmp(AV122DynamicFiltersSelector4, "CONTAGEMRESULTADO_SERVICO") == 0 )
                     {
                        AV128ContagemResultado_Servico4 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
                        if ( ! (0==AV128ContagemResultado_Servico4) )
                        {
                           AV105ContagemResultado_Servico = AV128ContagemResultado_Servico4;
                           H320( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText("Servi�o", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV105ContagemResultado_Servico), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                     }
                     else if ( StringUtil.StrCmp(AV122DynamicFiltersSelector4, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
                     {
                        AV129ContagemResultado_ContadorFM4 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
                        if ( ! (0==AV129ContagemResultado_ContadorFM4) )
                        {
                           AV155ContagemResultado_ContadorFM = AV129ContagemResultado_ContadorFM4;
                           H320( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText("Usu�rio da Prestadora", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV155ContagemResultado_ContadorFM), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                     }
                     else if ( StringUtil.StrCmp(AV122DynamicFiltersSelector4, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                     {
                        AV130ContagemResultado_SistemaCod4 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
                        if ( ! (0==AV130ContagemResultado_SistemaCod4) )
                        {
                           AV56ContagemResultado_SistemaCod = AV130ContagemResultado_SistemaCod4;
                           H320( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText("Sistema", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV56ContagemResultado_SistemaCod), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                     }
                     else if ( StringUtil.StrCmp(AV122DynamicFiltersSelector4, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                     {
                        AV131ContagemResultado_ContratadaCod4 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
                        if ( ! (0==AV131ContagemResultado_ContratadaCod4) )
                        {
                           AV57ContagemResultado_ContratadaCod = AV131ContagemResultado_ContratadaCod4;
                           H320( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText("Prestadora", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV57ContagemResultado_ContratadaCod), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                     }
                     else if ( StringUtil.StrCmp(AV122DynamicFiltersSelector4, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 )
                     {
                        AV132ContagemResultado_ServicoGrupo4 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
                        if ( ! (0==AV132ContagemResultado_ServicoGrupo4) )
                        {
                           AV117ContagemResultado_ServicoGrupo = AV132ContagemResultado_ServicoGrupo4;
                           H320( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText("Grupo de Servi�os", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV117ContagemResultado_ServicoGrupo), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                     }
                     else if ( StringUtil.StrCmp(AV122DynamicFiltersSelector4, "CONTAGEMRESULTADO_BASELINE") == 0 )
                     {
                        AV133ContagemResultado_Baseline4 = AV51GridStateDynamicFilter.gxTpr_Value;
                        if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV133ContagemResultado_Baseline4)) )
                        {
                           if ( StringUtil.StrCmp(StringUtil.Trim( AV133ContagemResultado_Baseline4), "/") == 0 )
                           {
                              AV134FilterContagemResultado_Baseline4ValueDescription = "(Todos)";
                           }
                           else if ( StringUtil.StrCmp(StringUtil.Trim( AV133ContagemResultado_Baseline4), "S") == 0 )
                           {
                              AV134FilterContagemResultado_Baseline4ValueDescription = "Sim";
                           }
                           else if ( StringUtil.StrCmp(StringUtil.Trim( AV133ContagemResultado_Baseline4), "N") == 0 )
                           {
                              AV134FilterContagemResultado_Baseline4ValueDescription = "N�o";
                           }
                           AV94FilterContagemResultado_BaselineValueDescription = AV134FilterContagemResultado_Baseline4ValueDescription;
                           H320( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText("Baseline", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV94FilterContagemResultado_BaselineValueDescription, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                     }
                     else if ( StringUtil.StrCmp(AV122DynamicFiltersSelector4, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
                     {
                        AV135ContagemResultado_NaoCnfDmnCod4 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
                        if ( ! (0==AV135ContagemResultado_NaoCnfDmnCod4) )
                        {
                           AV58ContagemResultado_NaoCnfDmnCod = AV135ContagemResultado_NaoCnfDmnCod4;
                           H320( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText("N�o Conformidade", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV58ContagemResultado_NaoCnfDmnCod), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                     }
                     else if ( StringUtil.StrCmp(AV122DynamicFiltersSelector4, "CONTAGEMRESULTADO_PFBFM") == 0 )
                     {
                        AV136ContagemResultado_PFBFM4 = NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, ".");
                        if ( ! (Convert.ToDecimal(0)==AV136ContagemResultado_PFBFM4) )
                        {
                           AV83ContagemResultado_PFBFM = AV136ContagemResultado_PFBFM4;
                           H320( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText("PFB Diferentes", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV83ContagemResultado_PFBFM, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                     }
                     else if ( StringUtil.StrCmp(AV122DynamicFiltersSelector4, "CONTAGEMRESULTADO_PFBFS") == 0 )
                     {
                        AV137ContagemResultado_PFBFS4 = NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, ".");
                        if ( ! (Convert.ToDecimal(0)==AV137ContagemResultado_PFBFS4) )
                        {
                           AV84ContagemResultado_PFBFS = AV137ContagemResultado_PFBFS4;
                           H320( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText("PFL Diferentes", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV84ContagemResultado_PFBFS, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                     }
                     else if ( StringUtil.StrCmp(AV122DynamicFiltersSelector4, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
                     {
                        AV171ContagemResultado_Agrupador4 = AV51GridStateDynamicFilter.gxTpr_Value;
                        if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV171ContagemResultado_Agrupador4)) )
                        {
                           AV173ContagemResultado_Agrupador = AV171ContagemResultado_Agrupador4;
                           H320( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText("Agrupador", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV173ContagemResultado_Agrupador, "@!")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                     }
                     else if ( StringUtil.StrCmp(AV122DynamicFiltersSelector4, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
                     {
                        AV189ContagemResultado_Descricao4 = AV51GridStateDynamicFilter.gxTpr_Value;
                        if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV189ContagemResultado_Descricao4)) )
                        {
                           AV186ContagemResultado_Descricao = AV189ContagemResultado_Descricao4;
                           H320( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText("Titulo", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV186ContagemResultado_Descricao, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                     }
                     else if ( StringUtil.StrCmp(AV122DynamicFiltersSelector4, "CONTAGEMRESULTADO_OWNER") == 0 )
                     {
                        AV183ContagemResultado_Owner4 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
                        if ( ! (0==AV183ContagemResultado_Owner4) )
                        {
                           AV180ContagemResultado_Owner = AV183ContagemResultado_Owner4;
                           H320( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText("Solicitante", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV180ContagemResultado_Owner), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                     }
                     else if ( StringUtil.StrCmp(AV122DynamicFiltersSelector4, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 )
                     {
                        AV203ContagemResultado_TemPndHmlg4 = AV51GridStateDynamicFilter.gxTpr_Value;
                        if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV203ContagemResultado_TemPndHmlg4)) )
                        {
                           if ( StringUtil.StrCmp(StringUtil.Trim( AV203ContagemResultado_TemPndHmlg4), "S") == 0 )
                           {
                              AV214FilterContagemResultado_TemPndHmlg4ValueDescription = "Sem pend�ncias";
                           }
                           else if ( StringUtil.StrCmp(StringUtil.Trim( AV203ContagemResultado_TemPndHmlg4), "C") == 0 )
                           {
                              AV214FilterContagemResultado_TemPndHmlg4ValueDescription = "Com pend�ncias";
                           }
                           AV217FilterContagemResultado_TemPndHmlgValueDescription = AV214FilterContagemResultado_TemPndHmlg4ValueDescription;
                           H320( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText("Pend�ncias vinculadas", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV217FilterContagemResultado_TemPndHmlgValueDescription, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                     }
                     else if ( StringUtil.StrCmp(AV122DynamicFiltersSelector4, "CONTAGEMRESULTADO_CNTCOD") == 0 )
                     {
                        AV286ContagemResultado_CntCod4 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
                        if ( ! (0==AV286ContagemResultado_CntCod4) )
                        {
                           AV283ContagemResultado_CntCod = AV286ContagemResultado_CntCod4;
                           H320( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText("Contrato", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV283ContagemResultado_CntCod), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                     }
                     if ( AV50GridState.gxTpr_Dynamicfilters.Count >= 5 )
                     {
                        AV138DynamicFiltersEnabled5 = true;
                        AV51GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV50GridState.gxTpr_Dynamicfilters.Item(5));
                        AV139DynamicFiltersSelector5 = AV51GridStateDynamicFilter.gxTpr_Selected;
                        if ( StringUtil.StrCmp(AV139DynamicFiltersSelector5, "CONTAGEMRESULTADO_DATACNT") == 0 )
                        {
                           AV140ContagemResultado_DataCnt5 = context.localUtil.CToD( AV51GridStateDynamicFilter.gxTpr_Value, 2);
                           AV141ContagemResultado_DataCnt_To5 = context.localUtil.CToD( AV51GridStateDynamicFilter.gxTpr_Valueto, 2);
                           if ( ! (DateTime.MinValue==AV140ContagemResultado_DataCnt5) )
                           {
                              AV52FilterContagemResultado_DataCntDescription = "Data da Cnt";
                              AV53ContagemResultado_DataCnt = AV140ContagemResultado_DataCnt5;
                              H320( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV52FilterContagemResultado_DataCntDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(context.localUtil.Format( AV53ContagemResultado_DataCnt, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                           if ( ! (DateTime.MinValue==AV141ContagemResultado_DataCnt_To5) )
                           {
                              AV52FilterContagemResultado_DataCntDescription = "Data da Cnt (at�)";
                              AV53ContagemResultado_DataCnt = AV141ContagemResultado_DataCnt_To5;
                              H320( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV52FilterContagemResultado_DataCntDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(context.localUtil.Format( AV53ContagemResultado_DataCnt, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                        }
                        else if ( StringUtil.StrCmp(AV139DynamicFiltersSelector5, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                        {
                           AV143ContagemResultado_StatusDmn5 = AV51GridStateDynamicFilter.gxTpr_Value;
                           if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV143ContagemResultado_StatusDmn5)) )
                           {
                              AV144FilterContagemResultado_StatusDmn5ValueDescription = gxdomainstatusdemanda.getDescription(context,AV143ContagemResultado_StatusDmn5);
                              AV25FilterContagemResultado_StatusDmnValueDescription = AV144FilterContagemResultado_StatusDmn5ValueDescription;
                              H320( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText("Status da Dmn", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV25FilterContagemResultado_StatusDmnValueDescription, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                        }
                        else if ( StringUtil.StrCmp(AV139DynamicFiltersSelector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
                        {
                           AV196DynamicFiltersOperator5 = AV51GridStateDynamicFilter.gxTpr_Operator;
                           AV142ContagemResultado_OsFsOsFm5 = AV51GridStateDynamicFilter.gxTpr_Value;
                           if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV142ContagemResultado_OsFsOsFm5)) )
                           {
                              if ( AV196DynamicFiltersOperator5 == 0 )
                              {
                                 AV192FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Igual)";
                              }
                              else if ( AV196DynamicFiltersOperator5 == 1 )
                              {
                                 AV192FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Come�a com)";
                              }
                              else if ( AV196DynamicFiltersOperator5 == 2 )
                              {
                                 AV192FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Cont�m)";
                              }
                              AV54ContagemResultado_OsFsOsFm = AV142ContagemResultado_OsFsOsFm5;
                              H320( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV192FilterContagemResultado_OsFsOsFmDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV54ContagemResultado_OsFsOsFm, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                        }
                        else if ( StringUtil.StrCmp(AV139DynamicFiltersSelector5, "CONTAGEMRESULTADO_DATADMN") == 0 )
                        {
                           AV267ContagemResultado_DataDmn5 = context.localUtil.CToD( AV51GridStateDynamicFilter.gxTpr_Value, 2);
                           AV268ContagemResultado_DataDmn_To5 = context.localUtil.CToD( AV51GridStateDynamicFilter.gxTpr_Valueto, 2);
                           if ( ! (DateTime.MinValue==AV267ContagemResultado_DataDmn5) )
                           {
                              AV249FilterContagemResultado_DataDmnDescription = "Data da Dmn";
                              AV250ContagemResultado_DataDmn = AV267ContagemResultado_DataDmn5;
                              H320( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV249FilterContagemResultado_DataDmnDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(context.localUtil.Format( AV250ContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                           if ( ! (DateTime.MinValue==AV268ContagemResultado_DataDmn_To5) )
                           {
                              AV249FilterContagemResultado_DataDmnDescription = "Data da Dmn (at�)";
                              AV250ContagemResultado_DataDmn = AV268ContagemResultado_DataDmn_To5;
                              H320( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV249FilterContagemResultado_DataDmnDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(context.localUtil.Format( AV250ContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                        }
                        else if ( StringUtil.StrCmp(AV139DynamicFiltersSelector5, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
                        {
                           AV269ContagemResultado_DataPrevista5 = context.localUtil.CToD( AV51GridStateDynamicFilter.gxTpr_Value, 2);
                           AV270ContagemResultado_DataPrevista_To5 = context.localUtil.CToD( AV51GridStateDynamicFilter.gxTpr_Valueto, 2);
                           if ( ! (DateTime.MinValue==AV269ContagemResultado_DataPrevista5) )
                           {
                              AV253FilterContagemResultado_DataPrevistaDescription = "Data Prevista";
                              AV254ContagemResultado_DataPrevista = AV269ContagemResultado_DataPrevista5;
                              H320( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV253FilterContagemResultado_DataPrevistaDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(context.localUtil.Format( AV254ContagemResultado_DataPrevista, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                           if ( ! (DateTime.MinValue==AV270ContagemResultado_DataPrevista_To5) )
                           {
                              AV253FilterContagemResultado_DataPrevistaDescription = "Data Prevista (at�)";
                              AV254ContagemResultado_DataPrevista = AV270ContagemResultado_DataPrevista_To5;
                              H320( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV253FilterContagemResultado_DataPrevistaDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(context.localUtil.Format( AV254ContagemResultado_DataPrevista, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                        }
                        else if ( StringUtil.StrCmp(AV139DynamicFiltersSelector5, "CONTAGEMRESULTADO_SERVICO") == 0 )
                        {
                           AV145ContagemResultado_Servico5 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
                           if ( ! (0==AV145ContagemResultado_Servico5) )
                           {
                              AV105ContagemResultado_Servico = AV145ContagemResultado_Servico5;
                              H320( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText("Servi�o", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV105ContagemResultado_Servico), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                        }
                        else if ( StringUtil.StrCmp(AV139DynamicFiltersSelector5, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
                        {
                           AV146ContagemResultado_ContadorFM5 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
                           if ( ! (0==AV146ContagemResultado_ContadorFM5) )
                           {
                              AV155ContagemResultado_ContadorFM = AV146ContagemResultado_ContadorFM5;
                              H320( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText("Usu�rio da Prestadora", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV155ContagemResultado_ContadorFM), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                        }
                        else if ( StringUtil.StrCmp(AV139DynamicFiltersSelector5, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                        {
                           AV147ContagemResultado_SistemaCod5 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
                           if ( ! (0==AV147ContagemResultado_SistemaCod5) )
                           {
                              AV56ContagemResultado_SistemaCod = AV147ContagemResultado_SistemaCod5;
                              H320( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText("Sistema", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV56ContagemResultado_SistemaCod), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                        }
                        else if ( StringUtil.StrCmp(AV139DynamicFiltersSelector5, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                        {
                           AV148ContagemResultado_ContratadaCod5 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
                           if ( ! (0==AV148ContagemResultado_ContratadaCod5) )
                           {
                              AV57ContagemResultado_ContratadaCod = AV148ContagemResultado_ContratadaCod5;
                              H320( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText("Prestadora", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV57ContagemResultado_ContratadaCod), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                        }
                        else if ( StringUtil.StrCmp(AV139DynamicFiltersSelector5, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 )
                        {
                           AV149ContagemResultado_ServicoGrupo5 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
                           if ( ! (0==AV149ContagemResultado_ServicoGrupo5) )
                           {
                              AV117ContagemResultado_ServicoGrupo = AV149ContagemResultado_ServicoGrupo5;
                              H320( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText("Grupo de Servi�os", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV117ContagemResultado_ServicoGrupo), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                        }
                        else if ( StringUtil.StrCmp(AV139DynamicFiltersSelector5, "CONTAGEMRESULTADO_BASELINE") == 0 )
                        {
                           AV150ContagemResultado_Baseline5 = AV51GridStateDynamicFilter.gxTpr_Value;
                           if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV150ContagemResultado_Baseline5)) )
                           {
                              if ( StringUtil.StrCmp(StringUtil.Trim( AV150ContagemResultado_Baseline5), "/") == 0 )
                              {
                                 AV151FilterContagemResultado_Baseline5ValueDescription = "(Todos)";
                              }
                              else if ( StringUtil.StrCmp(StringUtil.Trim( AV150ContagemResultado_Baseline5), "S") == 0 )
                              {
                                 AV151FilterContagemResultado_Baseline5ValueDescription = "Sim";
                              }
                              else if ( StringUtil.StrCmp(StringUtil.Trim( AV150ContagemResultado_Baseline5), "N") == 0 )
                              {
                                 AV151FilterContagemResultado_Baseline5ValueDescription = "N�o";
                              }
                              AV94FilterContagemResultado_BaselineValueDescription = AV151FilterContagemResultado_Baseline5ValueDescription;
                              H320( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText("Baseline", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV94FilterContagemResultado_BaselineValueDescription, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                        }
                        else if ( StringUtil.StrCmp(AV139DynamicFiltersSelector5, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
                        {
                           AV152ContagemResultado_NaoCnfDmnCod5 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
                           if ( ! (0==AV152ContagemResultado_NaoCnfDmnCod5) )
                           {
                              AV58ContagemResultado_NaoCnfDmnCod = AV152ContagemResultado_NaoCnfDmnCod5;
                              H320( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText("N�o Conformidade", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV58ContagemResultado_NaoCnfDmnCod), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                        }
                        else if ( StringUtil.StrCmp(AV139DynamicFiltersSelector5, "CONTAGEMRESULTADO_PFBFM") == 0 )
                        {
                           AV153ContagemResultado_PFBFM5 = NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, ".");
                           if ( ! (Convert.ToDecimal(0)==AV153ContagemResultado_PFBFM5) )
                           {
                              AV83ContagemResultado_PFBFM = AV153ContagemResultado_PFBFM5;
                              H320( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText("PFB Diferentes", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV83ContagemResultado_PFBFM, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                        }
                        else if ( StringUtil.StrCmp(AV139DynamicFiltersSelector5, "CONTAGEMRESULTADO_PFBFS") == 0 )
                        {
                           AV154ContagemResultado_PFBFS5 = NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, ".");
                           if ( ! (Convert.ToDecimal(0)==AV154ContagemResultado_PFBFS5) )
                           {
                              AV84ContagemResultado_PFBFS = AV154ContagemResultado_PFBFS5;
                              H320( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText("PFL Diferentes", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV84ContagemResultado_PFBFS, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                        }
                        else if ( StringUtil.StrCmp(AV139DynamicFiltersSelector5, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
                        {
                           AV172ContagemResultado_Agrupador5 = AV51GridStateDynamicFilter.gxTpr_Value;
                           if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV172ContagemResultado_Agrupador5)) )
                           {
                              AV173ContagemResultado_Agrupador = AV172ContagemResultado_Agrupador5;
                              H320( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText("Agrupador", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV173ContagemResultado_Agrupador, "@!")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                        }
                        else if ( StringUtil.StrCmp(AV139DynamicFiltersSelector5, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
                        {
                           AV190ContagemResultado_Descricao5 = AV51GridStateDynamicFilter.gxTpr_Value;
                           if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV190ContagemResultado_Descricao5)) )
                           {
                              AV186ContagemResultado_Descricao = AV190ContagemResultado_Descricao5;
                              H320( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText("Titulo", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV186ContagemResultado_Descricao, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                        }
                        else if ( StringUtil.StrCmp(AV139DynamicFiltersSelector5, "CONTAGEMRESULTADO_OWNER") == 0 )
                        {
                           AV184ContagemResultado_Owner5 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
                           if ( ! (0==AV184ContagemResultado_Owner5) )
                           {
                              AV180ContagemResultado_Owner = AV184ContagemResultado_Owner5;
                              H320( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText("Solicitante", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV180ContagemResultado_Owner), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                        }
                        else if ( StringUtil.StrCmp(AV139DynamicFiltersSelector5, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 )
                        {
                           AV204ContagemResultado_TemPndHmlg5 = AV51GridStateDynamicFilter.gxTpr_Value;
                           if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV204ContagemResultado_TemPndHmlg5)) )
                           {
                              if ( StringUtil.StrCmp(StringUtil.Trim( AV204ContagemResultado_TemPndHmlg5), "S") == 0 )
                              {
                                 AV215FilterContagemResultado_TemPndHmlg5ValueDescription = "Sem pend�ncias";
                              }
                              else if ( StringUtil.StrCmp(StringUtil.Trim( AV204ContagemResultado_TemPndHmlg5), "C") == 0 )
                              {
                                 AV215FilterContagemResultado_TemPndHmlg5ValueDescription = "Com pend�ncias";
                              }
                              AV217FilterContagemResultado_TemPndHmlgValueDescription = AV215FilterContagemResultado_TemPndHmlg5ValueDescription;
                              H320( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText("Pend�ncias vinculadas", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV217FilterContagemResultado_TemPndHmlgValueDescription, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                        }
                        else if ( StringUtil.StrCmp(AV139DynamicFiltersSelector5, "CONTAGEMRESULTADO_CNTCOD") == 0 )
                        {
                           AV287ContagemResultado_CntCod5 = (int)(NumberUtil.Val( AV51GridStateDynamicFilter.gxTpr_Value, "."));
                           if ( ! (0==AV287ContagemResultado_CntCod5) )
                           {
                              AV283ContagemResultado_CntCod = AV287ContagemResultado_CntCod5;
                              H320( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText("Contrato", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV283ContagemResultado_CntCod), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                        }
                     }
                  }
               }
            }
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV221TFContagemResultado_Agrupador_Sel)) )
         {
            H320( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Agrupador", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV221TFContagemResultado_Agrupador_Sel, "@!")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         else
         {
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV220TFContagemResultado_Agrupador)) )
            {
               H320( false, 20) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Agrupador", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV220TFContagemResultado_Agrupador, "@!")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+20);
            }
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV225TFContagemResultado_DemandaFM_Sel)) )
         {
            H320( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("N� OS", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV225TFContagemResultado_DemandaFM_Sel, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         else
         {
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV224TFContagemResultado_DemandaFM)) )
            {
               H320( false, 20) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("N� OS", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV224TFContagemResultado_DemandaFM, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+20);
            }
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV223TFContagemResultado_Demanda_Sel)) )
         {
            H320( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("N� OS Ref.", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV223TFContagemResultado_Demanda_Sel, "@!")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         else
         {
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV222TFContagemResultado_Demanda)) )
            {
               H320( false, 20) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("N� OS Ref.", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV222TFContagemResultado_Demanda, "@!")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+20);
            }
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV227TFContagemResultado_Descricao_Sel)) )
         {
            H320( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Titulo", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV227TFContagemResultado_Descricao_Sel, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         else
         {
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV226TFContagemResultado_Descricao)) )
            {
               H320( false, 20) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Titulo", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV226TFContagemResultado_Descricao, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+20);
            }
         }
         if ( ! ( (DateTime.MinValue==AV273TFContagemResultado_DataPrevista) && (DateTime.MinValue==AV274TFContagemResultado_DataPrevista_To) ) )
         {
            H320( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Previs�o", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(context.localUtil.Format( AV273TFContagemResultado_DataPrevista, "99/99/99 99:99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
            H320( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Previs�o (At�)", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(context.localUtil.Format( AV274TFContagemResultado_DataPrevista_To, "99/99/99 99:99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! ( (DateTime.MinValue==AV271TFContagemResultado_DataDmn) && (DateTime.MinValue==AV272TFContagemResultado_DataDmn_To) ) )
         {
            H320( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Demanda", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(context.localUtil.Format( AV271TFContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
            H320( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Demanda (At�)", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(context.localUtil.Format( AV272TFContagemResultado_DataDmn_To, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! ( (DateTime.MinValue==AV228TFContagemResultado_DataUltCnt) && (DateTime.MinValue==AV229TFContagemResultado_DataUltCnt_To) ) )
         {
            H320( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Contagem", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(context.localUtil.Format( AV228TFContagemResultado_DataUltCnt, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
            H320( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Contagem (At�)", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(context.localUtil.Format( AV229TFContagemResultado_DataUltCnt_To, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV231TFContagemResultado_ContratadaSigla_Sel)) )
         {
            H320( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Prestadora", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV231TFContagemResultado_ContratadaSigla_Sel, "@!")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         else
         {
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV230TFContagemResultado_ContratadaSigla)) )
            {
               H320( false, 20) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Prestadora", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV230TFContagemResultado_ContratadaSigla, "@!")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+20);
            }
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV233TFContagemResultado_SistemaCoord_Sel)) )
         {
            H320( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Coordena��o", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV233TFContagemResultado_SistemaCoord_Sel, "@!")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         else
         {
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV232TFContagemResultado_SistemaCoord)) )
            {
               H320( false, 20) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Coordena��o", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV232TFContagemResultado_SistemaCoord, "@!")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+20);
            }
         }
         AV236TFContagemResultado_StatusDmn_Sels.FromJSonString(AV234TFContagemResultado_StatusDmn_SelsJson);
         if ( ! ( AV236TFContagemResultado_StatusDmn_Sels.Count == 0 ) )
         {
            AV175i = 1;
            AV292GXV1 = 1;
            while ( AV292GXV1 <= AV236TFContagemResultado_StatusDmn_Sels.Count )
            {
               AV237TFContagemResultado_StatusDmn_Sel = AV236TFContagemResultado_StatusDmn_Sels.GetString(AV292GXV1);
               if ( AV175i == 1 )
               {
                  AV235TFContagemResultado_StatusDmn_SelDscs = "";
               }
               else
               {
                  AV235TFContagemResultado_StatusDmn_SelDscs = AV235TFContagemResultado_StatusDmn_SelDscs + ", ";
               }
               AV245FilterTFContagemResultado_StatusDmn_SelValueDescription = gxdomainstatusdemanda.getDescription(context,AV237TFContagemResultado_StatusDmn_Sel);
               AV235TFContagemResultado_StatusDmn_SelDscs = AV235TFContagemResultado_StatusDmn_SelDscs + AV245FilterTFContagemResultado_StatusDmn_SelValueDescription;
               AV175i = (long)(AV175i+1);
               AV292GXV1 = (int)(AV292GXV1+1);
            }
            H320( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Status", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV235TFContagemResultado_StatusDmn_SelDscs, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! (0==AV238TFContagemResultado_Baseline_Sel) )
         {
            if ( AV238TFContagemResultado_Baseline_Sel == 1 )
            {
               AV246FilterTFContagemResultado_Baseline_SelValueDescription = "Marcado";
            }
            else if ( AV238TFContagemResultado_Baseline_Sel == 2 )
            {
               AV246FilterTFContagemResultado_Baseline_SelValueDescription = "Desmarcado";
            }
            H320( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("BS", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV246FilterTFContagemResultado_Baseline_SelValueDescription, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV240TFContagemResultado_ServicoSigla_Sel)) )
         {
            H320( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Servi�o", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV240TFContagemResultado_ServicoSigla_Sel, "@!")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         else
         {
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV239TFContagemResultado_ServicoSigla)) )
            {
               H320( false, 20) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Servi�o", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV239TFContagemResultado_ServicoSigla, "@!")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+20);
            }
         }
         if ( ! ( (Convert.ToDecimal(0)==AV241TFContagemResultado_PFFinal) && (Convert.ToDecimal(0)==AV242TFContagemResultado_PFFinal_To) ) )
         {
            H320( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Qtd Faturar", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV241TFContagemResultado_PFFinal, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
            H320( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Qtd Faturar (At�)", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV242TFContagemResultado_PFFinal_To, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV243TFContagemResultado_ValorPF) && (Convert.ToDecimal(0)==AV244TFContagemResultado_ValorPF_To) ) )
         {
            H320( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Valor Unit�rio", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV243TFContagemResultado_ValorPF, "ZZ,ZZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
            H320( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Valor Unit�rio (At�)", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV244TFContagemResultado_ValorPF_To, "ZZ,ZZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
      }

      protected void S131( )
      {
         /* 'PRINTCOLUMNTITLES' Routine */
         H320( false, 35) ;
         getPrinter().GxDrawLine(5, Gx_line+30, 49, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(54, Gx_line+30, 98, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(103, Gx_line+30, 147, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(152, Gx_line+30, 196, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(201, Gx_line+30, 245, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(250, Gx_line+30, 294, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(299, Gx_line+30, 343, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(348, Gx_line+30, 392, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(397, Gx_line+30, 441, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(446, Gx_line+30, 490, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(495, Gx_line+30, 539, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(544, Gx_line+30, 588, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(593, Gx_line+30, 637, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(642, Gx_line+30, 686, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(691, Gx_line+30, 735, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(740, Gx_line+30, 784, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("Agrupador", 5, Gx_line+15, 49, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("N� OS", 54, Gx_line+15, 98, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("N� OS Ref.", 103, Gx_line+15, 147, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Sistema", 152, Gx_line+15, 196, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Titulo", 201, Gx_line+15, 245, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Previs�o", 250, Gx_line+15, 294, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Demanda", 299, Gx_line+15, 343, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Contagem", 348, Gx_line+15, 392, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Prestadora", 397, Gx_line+15, 441, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Contrato", 446, Gx_line+15, 490, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Coordena��o", 495, Gx_line+15, 539, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Status", 544, Gx_line+15, 588, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("BS", 593, Gx_line+15, 637, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Servi�o", 642, Gx_line+15, 686, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Qtd Faturar", 691, Gx_line+15, 735, Gx_line+29, 2, 0, 0, 0) ;
         getPrinter().GxDrawText("Valor Unit�rio", 740, Gx_line+15, 784, Gx_line+29, 2, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+35);
      }

      protected void S141( )
      {
         /* 'PRINTDATA' Routine */
         AV294ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod = AV12Contratada_AreaTrabalhoCod;
         AV295ExtraWWContagemResultadoExtraSelectionDS_2_Contagemresultado_statuscnt = AV14ContagemResultado_StatusCnt;
         AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV297ExtraWWContagemResultadoExtraSelectionDS_4_Dynamicfiltersoperator1 = AV191DynamicFiltersOperator1;
         AV298ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1 = AV17ContagemResultado_DataCnt1;
         AV299ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1 = AV18ContagemResultado_DataCnt_To1;
         AV300ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1 = AV24ContagemResultado_StatusDmn1;
         AV301ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 = AV19ContagemResultado_OsFsOsFm1;
         AV302ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1 = AV247ContagemResultado_DataDmn1;
         AV303ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1 = AV248ContagemResultado_DataDmn_To1;
         AV304ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1 = AV251ContagemResultado_DataPrevista1;
         AV305ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1 = AV252ContagemResultado_DataPrevista_To1;
         AV306ExtraWWContagemResultadoExtraSelectionDS_13_Contagemresultado_servico1 = AV102ContagemResultado_Servico1;
         AV307ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1 = AV118ContagemResultado_ContadorFM1;
         AV308ExtraWWContagemResultadoExtraSelectionDS_15_Contagemresultado_sistemacod1 = AV21ContagemResultado_SistemaCod1;
         AV309ExtraWWContagemResultadoExtraSelectionDS_16_Contagemresultado_contratadacod1 = AV22ContagemResultado_ContratadaCod1;
         AV310ExtraWWContagemResultadoExtraSelectionDS_17_Contagemresultado_servicogrupo1 = AV114ContagemResultado_ServicoGrupo1;
         AV311ExtraWWContagemResultadoExtraSelectionDS_18_Contagemresultado_baseline1 = AV90ContagemResultado_Baseline1;
         AV312ExtraWWContagemResultadoExtraSelectionDS_19_Contagemresultado_naocnfdmncod1 = AV23ContagemResultado_NaoCnfDmnCod1;
         AV313ExtraWWContagemResultadoExtraSelectionDS_20_Contagemresultado_pfbfm1 = AV77ContagemResultado_PFBFM1;
         AV314ExtraWWContagemResultadoExtraSelectionDS_21_Contagemresultado_pfbfs1 = AV78ContagemResultado_PFBFS1;
         AV315ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1 = AV168ContagemResultado_Agrupador1;
         AV316ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 = AV185ContagemResultado_Descricao1;
         AV317ExtraWWContagemResultadoExtraSelectionDS_24_Contagemresultado_owner1 = AV179ContagemResultado_Owner1;
         AV318ExtraWWContagemResultadoExtraSelectionDS_25_Contagemresultado_tempndhmlg1 = AV200ContagemResultado_TemPndHmlg1;
         AV319ExtraWWContagemResultadoExtraSelectionDS_26_Contagemresultado_cntcod1 = AV282ContagemResultado_CntCod1;
         AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 = AV27DynamicFiltersEnabled2;
         AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 = AV28DynamicFiltersSelector2;
         AV322ExtraWWContagemResultadoExtraSelectionDS_29_Dynamicfiltersoperator2 = AV193DynamicFiltersOperator2;
         AV323ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2 = AV29ContagemResultado_DataCnt2;
         AV324ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2 = AV30ContagemResultado_DataCnt_To2;
         AV325ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2 = AV36ContagemResultado_StatusDmn2;
         AV326ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 = AV31ContagemResultado_OsFsOsFm2;
         AV327ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2 = AV255ContagemResultado_DataDmn2;
         AV328ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2 = AV256ContagemResultado_DataDmn_To2;
         AV329ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2 = AV257ContagemResultado_DataPrevista2;
         AV330ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2 = AV258ContagemResultado_DataPrevista_To2;
         AV331ExtraWWContagemResultadoExtraSelectionDS_38_Contagemresultado_servico2 = AV103ContagemResultado_Servico2;
         AV332ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2 = AV119ContagemResultado_ContadorFM2;
         AV333ExtraWWContagemResultadoExtraSelectionDS_40_Contagemresultado_sistemacod2 = AV33ContagemResultado_SistemaCod2;
         AV334ExtraWWContagemResultadoExtraSelectionDS_41_Contagemresultado_contratadacod2 = AV34ContagemResultado_ContratadaCod2;
         AV335ExtraWWContagemResultadoExtraSelectionDS_42_Contagemresultado_servicogrupo2 = AV115ContagemResultado_ServicoGrupo2;
         AV336ExtraWWContagemResultadoExtraSelectionDS_43_Contagemresultado_baseline2 = AV91ContagemResultado_Baseline2;
         AV337ExtraWWContagemResultadoExtraSelectionDS_44_Contagemresultado_naocnfdmncod2 = AV35ContagemResultado_NaoCnfDmnCod2;
         AV338ExtraWWContagemResultadoExtraSelectionDS_45_Contagemresultado_pfbfm2 = AV79ContagemResultado_PFBFM2;
         AV339ExtraWWContagemResultadoExtraSelectionDS_46_Contagemresultado_pfbfs2 = AV80ContagemResultado_PFBFS2;
         AV340ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2 = AV169ContagemResultado_Agrupador2;
         AV341ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 = AV187ContagemResultado_Descricao2;
         AV342ExtraWWContagemResultadoExtraSelectionDS_49_Contagemresultado_owner2 = AV181ContagemResultado_Owner2;
         AV343ExtraWWContagemResultadoExtraSelectionDS_50_Contagemresultado_tempndhmlg2 = AV201ContagemResultado_TemPndHmlg2;
         AV344ExtraWWContagemResultadoExtraSelectionDS_51_Contagemresultado_cntcod2 = AV284ContagemResultado_CntCod2;
         AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 = AV38DynamicFiltersEnabled3;
         AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 = AV39DynamicFiltersSelector3;
         AV347ExtraWWContagemResultadoExtraSelectionDS_54_Dynamicfiltersoperator3 = AV194DynamicFiltersOperator3;
         AV348ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3 = AV40ContagemResultado_DataCnt3;
         AV349ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3 = AV41ContagemResultado_DataCnt_To3;
         AV350ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3 = AV47ContagemResultado_StatusDmn3;
         AV351ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 = AV42ContagemResultado_OsFsOsFm3;
         AV352ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3 = AV259ContagemResultado_DataDmn3;
         AV353ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3 = AV260ContagemResultado_DataDmn_To3;
         AV354ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3 = AV261ContagemResultado_DataPrevista3;
         AV355ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3 = AV262ContagemResultado_DataPrevista_To3;
         AV356ExtraWWContagemResultadoExtraSelectionDS_63_Contagemresultado_servico3 = AV104ContagemResultado_Servico3;
         AV357ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3 = AV120ContagemResultado_ContadorFM3;
         AV358ExtraWWContagemResultadoExtraSelectionDS_65_Contagemresultado_sistemacod3 = AV44ContagemResultado_SistemaCod3;
         AV359ExtraWWContagemResultadoExtraSelectionDS_66_Contagemresultado_contratadacod3 = AV45ContagemResultado_ContratadaCod3;
         AV360ExtraWWContagemResultadoExtraSelectionDS_67_Contagemresultado_servicogrupo3 = AV116ContagemResultado_ServicoGrupo3;
         AV361ExtraWWContagemResultadoExtraSelectionDS_68_Contagemresultado_baseline3 = AV92ContagemResultado_Baseline3;
         AV362ExtraWWContagemResultadoExtraSelectionDS_69_Contagemresultado_naocnfdmncod3 = AV46ContagemResultado_NaoCnfDmnCod3;
         AV363ExtraWWContagemResultadoExtraSelectionDS_70_Contagemresultado_pfbfm3 = AV81ContagemResultado_PFBFM3;
         AV364ExtraWWContagemResultadoExtraSelectionDS_71_Contagemresultado_pfbfs3 = AV82ContagemResultado_PFBFS3;
         AV365ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3 = AV170ContagemResultado_Agrupador3;
         AV366ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 = AV188ContagemResultado_Descricao3;
         AV367ExtraWWContagemResultadoExtraSelectionDS_74_Contagemresultado_owner3 = AV182ContagemResultado_Owner3;
         AV368ExtraWWContagemResultadoExtraSelectionDS_75_Contagemresultado_tempndhmlg3 = AV202ContagemResultado_TemPndHmlg3;
         AV369ExtraWWContagemResultadoExtraSelectionDS_76_Contagemresultado_cntcod3 = AV285ContagemResultado_CntCod3;
         AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 = AV121DynamicFiltersEnabled4;
         AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 = AV122DynamicFiltersSelector4;
         AV372ExtraWWContagemResultadoExtraSelectionDS_79_Dynamicfiltersoperator4 = AV195DynamicFiltersOperator4;
         AV373ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4 = AV123ContagemResultado_DataCnt4;
         AV374ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4 = AV124ContagemResultado_DataCnt_To4;
         AV375ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4 = AV126ContagemResultado_StatusDmn4;
         AV376ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 = AV125ContagemResultado_OsFsOsFm4;
         AV377ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4 = AV263ContagemResultado_DataDmn4;
         AV378ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4 = AV264ContagemResultado_DataDmn_To4;
         AV379ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4 = AV265ContagemResultado_DataPrevista4;
         AV380ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4 = AV266ContagemResultado_DataPrevista_To4;
         AV381ExtraWWContagemResultadoExtraSelectionDS_88_Contagemresultado_servico4 = AV128ContagemResultado_Servico4;
         AV382ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4 = AV129ContagemResultado_ContadorFM4;
         AV383ExtraWWContagemResultadoExtraSelectionDS_90_Contagemresultado_sistemacod4 = AV130ContagemResultado_SistemaCod4;
         AV384ExtraWWContagemResultadoExtraSelectionDS_91_Contagemresultado_contratadacod4 = AV131ContagemResultado_ContratadaCod4;
         AV385ExtraWWContagemResultadoExtraSelectionDS_92_Contagemresultado_servicogrupo4 = AV132ContagemResultado_ServicoGrupo4;
         AV386ExtraWWContagemResultadoExtraSelectionDS_93_Contagemresultado_baseline4 = AV133ContagemResultado_Baseline4;
         AV387ExtraWWContagemResultadoExtraSelectionDS_94_Contagemresultado_naocnfdmncod4 = AV135ContagemResultado_NaoCnfDmnCod4;
         AV388ExtraWWContagemResultadoExtraSelectionDS_95_Contagemresultado_pfbfm4 = AV136ContagemResultado_PFBFM4;
         AV389ExtraWWContagemResultadoExtraSelectionDS_96_Contagemresultado_pfbfs4 = AV137ContagemResultado_PFBFS4;
         AV390ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4 = AV171ContagemResultado_Agrupador4;
         AV391ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 = AV189ContagemResultado_Descricao4;
         AV392ExtraWWContagemResultadoExtraSelectionDS_99_Contagemresultado_owner4 = AV183ContagemResultado_Owner4;
         AV393ExtraWWContagemResultadoExtraSelectionDS_100_Contagemresultado_tempndhmlg4 = AV203ContagemResultado_TemPndHmlg4;
         AV394ExtraWWContagemResultadoExtraSelectionDS_101_Contagemresultado_cntcod4 = AV286ContagemResultado_CntCod4;
         AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 = AV138DynamicFiltersEnabled5;
         AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 = AV139DynamicFiltersSelector5;
         AV397ExtraWWContagemResultadoExtraSelectionDS_104_Dynamicfiltersoperator5 = AV196DynamicFiltersOperator5;
         AV398ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5 = AV140ContagemResultado_DataCnt5;
         AV399ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5 = AV141ContagemResultado_DataCnt_To5;
         AV400ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5 = AV143ContagemResultado_StatusDmn5;
         AV401ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 = AV142ContagemResultado_OsFsOsFm5;
         AV402ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5 = AV267ContagemResultado_DataDmn5;
         AV403ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5 = AV268ContagemResultado_DataDmn_To5;
         AV404ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5 = AV269ContagemResultado_DataPrevista5;
         AV405ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5 = AV270ContagemResultado_DataPrevista_To5;
         AV406ExtraWWContagemResultadoExtraSelectionDS_113_Contagemresultado_servico5 = AV145ContagemResultado_Servico5;
         AV407ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5 = AV146ContagemResultado_ContadorFM5;
         AV408ExtraWWContagemResultadoExtraSelectionDS_115_Contagemresultado_sistemacod5 = AV147ContagemResultado_SistemaCod5;
         AV409ExtraWWContagemResultadoExtraSelectionDS_116_Contagemresultado_contratadacod5 = AV148ContagemResultado_ContratadaCod5;
         AV410ExtraWWContagemResultadoExtraSelectionDS_117_Contagemresultado_servicogrupo5 = AV149ContagemResultado_ServicoGrupo5;
         AV411ExtraWWContagemResultadoExtraSelectionDS_118_Contagemresultado_baseline5 = AV150ContagemResultado_Baseline5;
         AV412ExtraWWContagemResultadoExtraSelectionDS_119_Contagemresultado_naocnfdmncod5 = AV152ContagemResultado_NaoCnfDmnCod5;
         AV413ExtraWWContagemResultadoExtraSelectionDS_120_Contagemresultado_pfbfm5 = AV153ContagemResultado_PFBFM5;
         AV414ExtraWWContagemResultadoExtraSelectionDS_121_Contagemresultado_pfbfs5 = AV154ContagemResultado_PFBFS5;
         AV415ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5 = AV172ContagemResultado_Agrupador5;
         AV416ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 = AV190ContagemResultado_Descricao5;
         AV417ExtraWWContagemResultadoExtraSelectionDS_124_Contagemresultado_owner5 = AV184ContagemResultado_Owner5;
         AV418ExtraWWContagemResultadoExtraSelectionDS_125_Contagemresultado_tempndhmlg5 = AV204ContagemResultado_TemPndHmlg5;
         AV419ExtraWWContagemResultadoExtraSelectionDS_126_Contagemresultado_cntcod5 = AV287ContagemResultado_CntCod5;
         AV420ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador = AV220TFContagemResultado_Agrupador;
         AV421ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel = AV221TFContagemResultado_Agrupador_Sel;
         AV422ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm = AV224TFContagemResultado_DemandaFM;
         AV423ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel = AV225TFContagemResultado_DemandaFM_Sel;
         AV424ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda = AV222TFContagemResultado_Demanda;
         AV425ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel = AV223TFContagemResultado_Demanda_Sel;
         AV426ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao = AV226TFContagemResultado_Descricao;
         AV427ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel = AV227TFContagemResultado_Descricao_Sel;
         AV428ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista = AV273TFContagemResultado_DataPrevista;
         AV429ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to = AV274TFContagemResultado_DataPrevista_To;
         AV430ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn = AV271TFContagemResultado_DataDmn;
         AV431ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to = AV272TFContagemResultado_DataDmn_To;
         AV432ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt = AV228TFContagemResultado_DataUltCnt;
         AV433ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to = AV229TFContagemResultado_DataUltCnt_To;
         AV434ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla = AV230TFContagemResultado_ContratadaSigla;
         AV435ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel = AV231TFContagemResultado_ContratadaSigla_Sel;
         AV436ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord = AV232TFContagemResultado_SistemaCoord;
         AV437ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel = AV233TFContagemResultado_SistemaCoord_Sel;
         AV438ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels = AV236TFContagemResultado_StatusDmn_Sels;
         AV439ExtraWWContagemResultadoExtraSelectionDS_146_Tfcontagemresultado_baseline_sel = AV238TFContagemResultado_Baseline_Sel;
         AV440ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla = AV239TFContagemResultado_ServicoSigla;
         AV441ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel = AV240TFContagemResultado_ServicoSigla_Sel;
         AV442ExtraWWContagemResultadoExtraSelectionDS_149_Tfcontagemresultado_pffinal = AV241TFContagemResultado_PFFinal;
         AV443ExtraWWContagemResultadoExtraSelectionDS_150_Tfcontagemresultado_pffinal_to = AV242TFContagemResultado_PFFinal_To;
         AV444ExtraWWContagemResultadoExtraSelectionDS_151_Tfcontagemresultado_valorpf = AV243TFContagemResultado_ValorPF;
         AV445ExtraWWContagemResultadoExtraSelectionDS_152_Tfcontagemresultado_valorpf_to = AV244TFContagemResultado_ValorPF_To;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A484ContagemResultado_StatusDmn ,
                                              AV438ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels ,
                                              A490ContagemResultado_ContratadaCod ,
                                              AV197Contratadas ,
                                              A601ContagemResultado_Servico ,
                                              AV178ServicosGeridos ,
                                              AV9WWPContext.gxTpr_Contratada_codigo ,
                                              AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 ,
                                              AV300ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1 ,
                                              AV297ExtraWWContagemResultadoExtraSelectionDS_4_Dynamicfiltersoperator1 ,
                                              AV301ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 ,
                                              AV302ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1 ,
                                              AV303ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1 ,
                                              AV304ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1 ,
                                              AV305ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1 ,
                                              AV306ExtraWWContagemResultadoExtraSelectionDS_13_Contagemresultado_servico1 ,
                                              AV308ExtraWWContagemResultadoExtraSelectionDS_15_Contagemresultado_sistemacod1 ,
                                              AV309ExtraWWContagemResultadoExtraSelectionDS_16_Contagemresultado_contratadacod1 ,
                                              AV294ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod ,
                                              AV310ExtraWWContagemResultadoExtraSelectionDS_17_Contagemresultado_servicogrupo1 ,
                                              AV311ExtraWWContagemResultadoExtraSelectionDS_18_Contagemresultado_baseline1 ,
                                              AV312ExtraWWContagemResultadoExtraSelectionDS_19_Contagemresultado_naocnfdmncod1 ,
                                              AV315ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1 ,
                                              AV316ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 ,
                                              AV317ExtraWWContagemResultadoExtraSelectionDS_24_Contagemresultado_owner1 ,
                                              AV319ExtraWWContagemResultadoExtraSelectionDS_26_Contagemresultado_cntcod1 ,
                                              AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 ,
                                              AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 ,
                                              AV325ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2 ,
                                              AV322ExtraWWContagemResultadoExtraSelectionDS_29_Dynamicfiltersoperator2 ,
                                              AV326ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 ,
                                              AV327ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2 ,
                                              AV328ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2 ,
                                              AV329ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2 ,
                                              AV330ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2 ,
                                              AV331ExtraWWContagemResultadoExtraSelectionDS_38_Contagemresultado_servico2 ,
                                              AV333ExtraWWContagemResultadoExtraSelectionDS_40_Contagemresultado_sistemacod2 ,
                                              AV334ExtraWWContagemResultadoExtraSelectionDS_41_Contagemresultado_contratadacod2 ,
                                              AV335ExtraWWContagemResultadoExtraSelectionDS_42_Contagemresultado_servicogrupo2 ,
                                              AV336ExtraWWContagemResultadoExtraSelectionDS_43_Contagemresultado_baseline2 ,
                                              AV337ExtraWWContagemResultadoExtraSelectionDS_44_Contagemresultado_naocnfdmncod2 ,
                                              AV340ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2 ,
                                              AV341ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 ,
                                              AV342ExtraWWContagemResultadoExtraSelectionDS_49_Contagemresultado_owner2 ,
                                              AV344ExtraWWContagemResultadoExtraSelectionDS_51_Contagemresultado_cntcod2 ,
                                              AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 ,
                                              AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 ,
                                              AV350ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3 ,
                                              AV347ExtraWWContagemResultadoExtraSelectionDS_54_Dynamicfiltersoperator3 ,
                                              AV351ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 ,
                                              AV352ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3 ,
                                              AV353ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3 ,
                                              AV354ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3 ,
                                              AV355ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3 ,
                                              AV356ExtraWWContagemResultadoExtraSelectionDS_63_Contagemresultado_servico3 ,
                                              AV358ExtraWWContagemResultadoExtraSelectionDS_65_Contagemresultado_sistemacod3 ,
                                              AV359ExtraWWContagemResultadoExtraSelectionDS_66_Contagemresultado_contratadacod3 ,
                                              AV360ExtraWWContagemResultadoExtraSelectionDS_67_Contagemresultado_servicogrupo3 ,
                                              AV361ExtraWWContagemResultadoExtraSelectionDS_68_Contagemresultado_baseline3 ,
                                              AV362ExtraWWContagemResultadoExtraSelectionDS_69_Contagemresultado_naocnfdmncod3 ,
                                              AV365ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3 ,
                                              AV366ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 ,
                                              AV367ExtraWWContagemResultadoExtraSelectionDS_74_Contagemresultado_owner3 ,
                                              AV369ExtraWWContagemResultadoExtraSelectionDS_76_Contagemresultado_cntcod3 ,
                                              AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 ,
                                              AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 ,
                                              AV375ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4 ,
                                              AV372ExtraWWContagemResultadoExtraSelectionDS_79_Dynamicfiltersoperator4 ,
                                              AV376ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 ,
                                              AV377ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4 ,
                                              AV378ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4 ,
                                              AV379ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4 ,
                                              AV380ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4 ,
                                              AV381ExtraWWContagemResultadoExtraSelectionDS_88_Contagemresultado_servico4 ,
                                              AV383ExtraWWContagemResultadoExtraSelectionDS_90_Contagemresultado_sistemacod4 ,
                                              AV384ExtraWWContagemResultadoExtraSelectionDS_91_Contagemresultado_contratadacod4 ,
                                              AV385ExtraWWContagemResultadoExtraSelectionDS_92_Contagemresultado_servicogrupo4 ,
                                              AV386ExtraWWContagemResultadoExtraSelectionDS_93_Contagemresultado_baseline4 ,
                                              AV387ExtraWWContagemResultadoExtraSelectionDS_94_Contagemresultado_naocnfdmncod4 ,
                                              AV390ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4 ,
                                              AV391ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 ,
                                              AV392ExtraWWContagemResultadoExtraSelectionDS_99_Contagemresultado_owner4 ,
                                              AV394ExtraWWContagemResultadoExtraSelectionDS_101_Contagemresultado_cntcod4 ,
                                              AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 ,
                                              AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 ,
                                              AV400ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5 ,
                                              AV397ExtraWWContagemResultadoExtraSelectionDS_104_Dynamicfiltersoperator5 ,
                                              AV401ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 ,
                                              AV402ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5 ,
                                              AV403ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5 ,
                                              AV404ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5 ,
                                              AV405ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5 ,
                                              AV406ExtraWWContagemResultadoExtraSelectionDS_113_Contagemresultado_servico5 ,
                                              AV408ExtraWWContagemResultadoExtraSelectionDS_115_Contagemresultado_sistemacod5 ,
                                              AV409ExtraWWContagemResultadoExtraSelectionDS_116_Contagemresultado_contratadacod5 ,
                                              AV410ExtraWWContagemResultadoExtraSelectionDS_117_Contagemresultado_servicogrupo5 ,
                                              AV411ExtraWWContagemResultadoExtraSelectionDS_118_Contagemresultado_baseline5 ,
                                              AV412ExtraWWContagemResultadoExtraSelectionDS_119_Contagemresultado_naocnfdmncod5 ,
                                              AV415ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5 ,
                                              AV416ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 ,
                                              AV417ExtraWWContagemResultadoExtraSelectionDS_124_Contagemresultado_owner5 ,
                                              AV419ExtraWWContagemResultadoExtraSelectionDS_126_Contagemresultado_cntcod5 ,
                                              AV421ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel ,
                                              AV420ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador ,
                                              AV423ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel ,
                                              AV422ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm ,
                                              AV425ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel ,
                                              AV424ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda ,
                                              AV427ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel ,
                                              AV426ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao ,
                                              AV428ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista ,
                                              AV429ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to ,
                                              AV430ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn ,
                                              AV431ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to ,
                                              AV435ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel ,
                                              AV434ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla ,
                                              AV437ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel ,
                                              AV436ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord ,
                                              AV438ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels.Count ,
                                              AV439ExtraWWContagemResultadoExtraSelectionDS_146_Tfcontagemresultado_baseline_sel ,
                                              AV441ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel ,
                                              AV440ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla ,
                                              AV444ExtraWWContagemResultadoExtraSelectionDS_151_Tfcontagemresultado_valorpf ,
                                              AV445ExtraWWContagemResultadoExtraSelectionDS_152_Tfcontagemresultado_valorpf_to ,
                                              AV50GridState.gxTpr_Dynamicfilters.Count ,
                                              AV197Contratadas.Count ,
                                              AV9WWPContext.gxTpr_Userehfinanceiro ,
                                              AV9WWPContext.gxTpr_Userehgestor ,
                                              A457ContagemResultado_Demanda ,
                                              A493ContagemResultado_DemandaFM ,
                                              A471ContagemResultado_DataDmn ,
                                              A1351ContagemResultado_DataPrevista ,
                                              A489ContagemResultado_SistemaCod ,
                                              A764ContagemResultado_ServicoGrupo ,
                                              A598ContagemResultado_Baseline ,
                                              A468ContagemResultado_NaoCnfDmnCod ,
                                              A1046ContagemResultado_Agrupador ,
                                              A494ContagemResultado_Descricao ,
                                              A508ContagemResultado_Owner ,
                                              A1603ContagemResultado_CntCod ,
                                              A803ContagemResultado_ContratadaSigla ,
                                              A515ContagemResultado_SistemaCoord ,
                                              A801ContagemResultado_ServicoSigla ,
                                              A512ContagemResultado_ValorPF ,
                                              A456ContagemResultado_Codigo ,
                                              AV10OrderedBy ,
                                              AV11OrderedDsc ,
                                              A531ContagemResultado_StatusUltCnt ,
                                              A1854ContagemResultado_VlrCnc ,
                                              AV298ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1 ,
                                              A566ContagemResultado_DataUltCnt ,
                                              AV299ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1 ,
                                              AV307ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1 ,
                                              A584ContagemResultado_ContadorFM ,
                                              A684ContagemResultado_PFBFSUltima ,
                                              A682ContagemResultado_PFBFMUltima ,
                                              A685ContagemResultado_PFLFSUltima ,
                                              A683ContagemResultado_PFLFMUltima ,
                                              AV318ExtraWWContagemResultadoExtraSelectionDS_25_Contagemresultado_tempndhmlg1 ,
                                              A1802ContagemResultado_TemPndHmlg ,
                                              AV323ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2 ,
                                              AV324ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2 ,
                                              AV332ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2 ,
                                              AV343ExtraWWContagemResultadoExtraSelectionDS_50_Contagemresultado_tempndhmlg2 ,
                                              AV348ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3 ,
                                              AV349ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3 ,
                                              AV357ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3 ,
                                              AV368ExtraWWContagemResultadoExtraSelectionDS_75_Contagemresultado_tempndhmlg3 ,
                                              AV373ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4 ,
                                              AV374ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4 ,
                                              AV382ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4 ,
                                              AV393ExtraWWContagemResultadoExtraSelectionDS_100_Contagemresultado_tempndhmlg4 ,
                                              AV398ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5 ,
                                              AV399ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5 ,
                                              AV407ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5 ,
                                              AV418ExtraWWContagemResultadoExtraSelectionDS_125_Contagemresultado_tempndhmlg5 ,
                                              AV432ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt ,
                                              AV433ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to ,
                                              AV442ExtraWWContagemResultadoExtraSelectionDS_149_Tfcontagemresultado_pffinal ,
                                              A574ContagemResultado_PFFinal ,
                                              AV443ExtraWWContagemResultadoExtraSelectionDS_150_Tfcontagemresultado_pffinal_to ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo ,
                                              A1583ContagemResultado_TipoRegistro },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT,
                                              TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.INT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT
                                              }
         });
         lV301ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV301ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1), "%", "");
         lV301ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV301ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1), "%", "");
         lV301ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV301ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1), "%", "");
         lV301ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV301ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1), "%", "");
         lV316ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV316ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1), "%", "");
         lV326ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV326ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2), "%", "");
         lV326ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV326ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2), "%", "");
         lV326ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV326ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2), "%", "");
         lV326ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV326ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2), "%", "");
         lV341ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV341ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2), "%", "");
         lV351ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV351ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3), "%", "");
         lV351ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV351ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3), "%", "");
         lV351ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV351ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3), "%", "");
         lV351ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV351ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3), "%", "");
         lV366ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV366ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3), "%", "");
         lV376ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV376ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4), "%", "");
         lV376ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV376ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4), "%", "");
         lV376ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV376ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4), "%", "");
         lV376ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV376ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4), "%", "");
         lV391ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 = StringUtil.Concat( StringUtil.RTrim( AV391ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4), "%", "");
         lV401ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV401ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5), "%", "");
         lV401ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV401ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5), "%", "");
         lV401ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV401ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5), "%", "");
         lV401ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV401ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5), "%", "");
         lV416ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 = StringUtil.Concat( StringUtil.RTrim( AV416ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5), "%", "");
         lV420ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador = StringUtil.PadR( StringUtil.RTrim( AV420ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador), 15, "%");
         lV422ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm = StringUtil.Concat( StringUtil.RTrim( AV422ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm), "%", "");
         lV424ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda = StringUtil.Concat( StringUtil.RTrim( AV424ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda), "%", "");
         lV426ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao = StringUtil.Concat( StringUtil.RTrim( AV426ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao), "%", "");
         lV434ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla = StringUtil.PadR( StringUtil.RTrim( AV434ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla), 15, "%");
         lV436ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord = StringUtil.Concat( StringUtil.RTrim( AV436ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord), "%", "");
         lV440ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla = StringUtil.PadR( StringUtil.RTrim( AV440ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla), 15, "%");
         /* Using cursor P00323 */
         pr_default.execute(0, new Object[] {AV300ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1, AV325ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2, AV350ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3, AV375ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4, AV400ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5, AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, AV298ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1, AV298ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1, AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, AV299ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1, AV299ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1, AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, AV307ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1, AV307ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1, AV307ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1, AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2, AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, AV323ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2, AV323ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2, AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2, AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, AV324ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2, AV324ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2, AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2, AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, AV332ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2, AV332ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2, AV332ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2, AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2, AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2, AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3, AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, AV348ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3, AV348ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3, AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3, AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, AV349ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3, AV349ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3, AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3, AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, AV357ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3, AV357ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3, AV357ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3, AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3, AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3, AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4, AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, AV373ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4, AV373ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4, AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4, AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, AV374ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4, AV374ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4, AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4, AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, AV382ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4, AV382ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4, AV382ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4, AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4, AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4, AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5, AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, AV398ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5, AV398ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5, AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5, AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, AV399ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5, AV399ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5, AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5, AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, AV407ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5, AV407ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5, AV407ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5, AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5, AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5, AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, AV432ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt, AV432ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt, AV433ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to, AV433ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to, AV9WWPContext.gxTpr_Areatrabalho_codigo, AV9WWPContext.gxTpr_Contratada_codigo, AV300ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1, AV301ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1, AV301ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1, lV301ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1, lV301ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1, lV301ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1, lV301ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1, AV302ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1, AV303ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1, AV304ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1, AV305ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1, AV306ExtraWWContagemResultadoExtraSelectionDS_13_Contagemresultado_servico1, AV308ExtraWWContagemResultadoExtraSelectionDS_15_Contagemresultado_sistemacod1, AV309ExtraWWContagemResultadoExtraSelectionDS_16_Contagemresultado_contratadacod1, AV310ExtraWWContagemResultadoExtraSelectionDS_17_Contagemresultado_servicogrupo1, AV312ExtraWWContagemResultadoExtraSelectionDS_19_Contagemresultado_naocnfdmncod1, AV315ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1, lV316ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1, AV317ExtraWWContagemResultadoExtraSelectionDS_24_Contagemresultado_owner1, AV319ExtraWWContagemResultadoExtraSelectionDS_26_Contagemresultado_cntcod1, AV325ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2, AV326ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2, AV326ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2, lV326ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2, lV326ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2, lV326ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2, lV326ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2, AV327ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2, AV328ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2, AV329ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2, AV330ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2,
         AV331ExtraWWContagemResultadoExtraSelectionDS_38_Contagemresultado_servico2, AV333ExtraWWContagemResultadoExtraSelectionDS_40_Contagemresultado_sistemacod2, AV334ExtraWWContagemResultadoExtraSelectionDS_41_Contagemresultado_contratadacod2, AV335ExtraWWContagemResultadoExtraSelectionDS_42_Contagemresultado_servicogrupo2, AV337ExtraWWContagemResultadoExtraSelectionDS_44_Contagemresultado_naocnfdmncod2, AV340ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2, lV341ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2, AV342ExtraWWContagemResultadoExtraSelectionDS_49_Contagemresultado_owner2, AV344ExtraWWContagemResultadoExtraSelectionDS_51_Contagemresultado_cntcod2, AV350ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3, AV351ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3, AV351ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3, lV351ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3, lV351ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3, lV351ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3, lV351ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3, AV352ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3, AV353ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3, AV354ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3, AV355ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3, AV356ExtraWWContagemResultadoExtraSelectionDS_63_Contagemresultado_servico3, AV358ExtraWWContagemResultadoExtraSelectionDS_65_Contagemresultado_sistemacod3, AV359ExtraWWContagemResultadoExtraSelectionDS_66_Contagemresultado_contratadacod3, AV360ExtraWWContagemResultadoExtraSelectionDS_67_Contagemresultado_servicogrupo3, AV362ExtraWWContagemResultadoExtraSelectionDS_69_Contagemresultado_naocnfdmncod3, AV365ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3, lV366ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3, AV367ExtraWWContagemResultadoExtraSelectionDS_74_Contagemresultado_owner3, AV369ExtraWWContagemResultadoExtraSelectionDS_76_Contagemresultado_cntcod3, AV375ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4, AV376ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4, AV376ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4, lV376ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4, lV376ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4, lV376ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4, lV376ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4, AV377ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4, AV378ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4, AV379ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4, AV380ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4, AV381ExtraWWContagemResultadoExtraSelectionDS_88_Contagemresultado_servico4, AV383ExtraWWContagemResultadoExtraSelectionDS_90_Contagemresultado_sistemacod4, AV384ExtraWWContagemResultadoExtraSelectionDS_91_Contagemresultado_contratadacod4, AV385ExtraWWContagemResultadoExtraSelectionDS_92_Contagemresultado_servicogrupo4, AV387ExtraWWContagemResultadoExtraSelectionDS_94_Contagemresultado_naocnfdmncod4, AV390ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4, lV391ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4, AV392ExtraWWContagemResultadoExtraSelectionDS_99_Contagemresultado_owner4, AV394ExtraWWContagemResultadoExtraSelectionDS_101_Contagemresultado_cntcod4, AV400ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5, AV401ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5, AV401ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5, lV401ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5, lV401ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5, lV401ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5, lV401ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5, AV402ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5, AV403ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5, AV404ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5, AV405ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5, AV406ExtraWWContagemResultadoExtraSelectionDS_113_Contagemresultado_servico5, AV408ExtraWWContagemResultadoExtraSelectionDS_115_Contagemresultado_sistemacod5, AV409ExtraWWContagemResultadoExtraSelectionDS_116_Contagemresultado_contratadacod5, AV410ExtraWWContagemResultadoExtraSelectionDS_117_Contagemresultado_servicogrupo5, AV412ExtraWWContagemResultadoExtraSelectionDS_119_Contagemresultado_naocnfdmncod5, AV415ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5, lV416ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5, AV417ExtraWWContagemResultadoExtraSelectionDS_124_Contagemresultado_owner5, AV419ExtraWWContagemResultadoExtraSelectionDS_126_Contagemresultado_cntcod5, lV420ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador, AV421ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel, lV422ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm, AV423ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel, lV424ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda, AV425ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel, lV426ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao, AV427ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel, AV428ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista, AV429ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to, AV430ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn, AV431ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to, lV434ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla, AV435ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel, lV436ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord, AV437ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel, lV440ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla, AV441ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel, AV444ExtraWWContagemResultadoExtraSelectionDS_151_Tfcontagemresultado_valorpf, AV445ExtraWWContagemResultadoExtraSelectionDS_152_Tfcontagemresultado_valorpf_to});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P00323_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00323_n1553ContagemResultado_CntSrvCod[0];
            A1583ContagemResultado_TipoRegistro = P00323_A1583ContagemResultado_TipoRegistro[0];
            A512ContagemResultado_ValorPF = P00323_A512ContagemResultado_ValorPF[0];
            n512ContagemResultado_ValorPF = P00323_n512ContagemResultado_ValorPF[0];
            A801ContagemResultado_ServicoSigla = P00323_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00323_n801ContagemResultado_ServicoSigla[0];
            A515ContagemResultado_SistemaCoord = P00323_A515ContagemResultado_SistemaCoord[0];
            n515ContagemResultado_SistemaCoord = P00323_n515ContagemResultado_SistemaCoord[0];
            A803ContagemResultado_ContratadaSigla = P00323_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P00323_n803ContagemResultado_ContratadaSigla[0];
            A1603ContagemResultado_CntCod = P00323_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P00323_n1603ContagemResultado_CntCod[0];
            A494ContagemResultado_Descricao = P00323_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P00323_n494ContagemResultado_Descricao[0];
            A1046ContagemResultado_Agrupador = P00323_A1046ContagemResultado_Agrupador[0];
            n1046ContagemResultado_Agrupador = P00323_n1046ContagemResultado_Agrupador[0];
            A468ContagemResultado_NaoCnfDmnCod = P00323_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = P00323_n468ContagemResultado_NaoCnfDmnCod[0];
            A598ContagemResultado_Baseline = P00323_A598ContagemResultado_Baseline[0];
            n598ContagemResultado_Baseline = P00323_n598ContagemResultado_Baseline[0];
            A764ContagemResultado_ServicoGrupo = P00323_A764ContagemResultado_ServicoGrupo[0];
            n764ContagemResultado_ServicoGrupo = P00323_n764ContagemResultado_ServicoGrupo[0];
            A489ContagemResultado_SistemaCod = P00323_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00323_n489ContagemResultado_SistemaCod[0];
            A508ContagemResultado_Owner = P00323_A508ContagemResultado_Owner[0];
            A601ContagemResultado_Servico = P00323_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00323_n601ContagemResultado_Servico[0];
            A1351ContagemResultado_DataPrevista = P00323_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = P00323_n1351ContagemResultado_DataPrevista[0];
            A471ContagemResultado_DataDmn = P00323_A471ContagemResultado_DataDmn[0];
            A493ContagemResultado_DemandaFM = P00323_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00323_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = P00323_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00323_n457ContagemResultado_Demanda[0];
            A484ContagemResultado_StatusDmn = P00323_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00323_n484ContagemResultado_StatusDmn[0];
            A1854ContagemResultado_VlrCnc = P00323_A1854ContagemResultado_VlrCnc[0];
            n1854ContagemResultado_VlrCnc = P00323_n1854ContagemResultado_VlrCnc[0];
            A490ContagemResultado_ContratadaCod = P00323_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00323_n490ContagemResultado_ContratadaCod[0];
            A52Contratada_AreaTrabalhoCod = P00323_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00323_n52Contratada_AreaTrabalhoCod[0];
            A1612ContagemResultado_CntNum = P00323_A1612ContagemResultado_CntNum[0];
            n1612ContagemResultado_CntNum = P00323_n1612ContagemResultado_CntNum[0];
            A495ContagemResultado_SistemaNom = P00323_A495ContagemResultado_SistemaNom[0];
            n495ContagemResultado_SistemaNom = P00323_n495ContagemResultado_SistemaNom[0];
            A683ContagemResultado_PFLFMUltima = P00323_A683ContagemResultado_PFLFMUltima[0];
            A685ContagemResultado_PFLFSUltima = P00323_A685ContagemResultado_PFLFSUltima[0];
            A682ContagemResultado_PFBFMUltima = P00323_A682ContagemResultado_PFBFMUltima[0];
            A684ContagemResultado_PFBFSUltima = P00323_A684ContagemResultado_PFBFSUltima[0];
            A584ContagemResultado_ContadorFM = P00323_A584ContagemResultado_ContadorFM[0];
            A566ContagemResultado_DataUltCnt = P00323_A566ContagemResultado_DataUltCnt[0];
            A531ContagemResultado_StatusUltCnt = P00323_A531ContagemResultado_StatusUltCnt[0];
            A456ContagemResultado_Codigo = P00323_A456ContagemResultado_Codigo[0];
            A1603ContagemResultado_CntCod = P00323_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P00323_n1603ContagemResultado_CntCod[0];
            A601ContagemResultado_Servico = P00323_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00323_n601ContagemResultado_Servico[0];
            A1612ContagemResultado_CntNum = P00323_A1612ContagemResultado_CntNum[0];
            n1612ContagemResultado_CntNum = P00323_n1612ContagemResultado_CntNum[0];
            A801ContagemResultado_ServicoSigla = P00323_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00323_n801ContagemResultado_ServicoSigla[0];
            A764ContagemResultado_ServicoGrupo = P00323_A764ContagemResultado_ServicoGrupo[0];
            n764ContagemResultado_ServicoGrupo = P00323_n764ContagemResultado_ServicoGrupo[0];
            A515ContagemResultado_SistemaCoord = P00323_A515ContagemResultado_SistemaCoord[0];
            n515ContagemResultado_SistemaCoord = P00323_n515ContagemResultado_SistemaCoord[0];
            A495ContagemResultado_SistemaNom = P00323_A495ContagemResultado_SistemaNom[0];
            n495ContagemResultado_SistemaNom = P00323_n495ContagemResultado_SistemaNom[0];
            A803ContagemResultado_ContratadaSigla = P00323_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P00323_n803ContagemResultado_ContratadaSigla[0];
            A52Contratada_AreaTrabalhoCod = P00323_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00323_n52Contratada_AreaTrabalhoCod[0];
            A683ContagemResultado_PFLFMUltima = P00323_A683ContagemResultado_PFLFMUltima[0];
            A685ContagemResultado_PFLFSUltima = P00323_A685ContagemResultado_PFLFSUltima[0];
            A682ContagemResultado_PFBFMUltima = P00323_A682ContagemResultado_PFBFMUltima[0];
            A684ContagemResultado_PFBFSUltima = P00323_A684ContagemResultado_PFBFSUltima[0];
            A584ContagemResultado_ContadorFM = P00323_A584ContagemResultado_ContadorFM[0];
            A566ContagemResultado_DataUltCnt = P00323_A566ContagemResultado_DataUltCnt[0];
            A531ContagemResultado_StatusUltCnt = P00323_A531ContagemResultado_StatusUltCnt[0];
            GXt_boolean1 = A1802ContagemResultado_TemPndHmlg;
            new prc_tempndparahomologar(context ).execute( ref  A456ContagemResultado_Codigo, out  GXt_boolean1) ;
            A1802ContagemResultado_TemPndHmlg = GXt_boolean1;
            if ( ! ( ( StringUtil.StrCmp(AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV318ExtraWWContagemResultadoExtraSelectionDS_25_Contagemresultado_tempndhmlg1, "C") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && A1802ContagemResultado_TemPndHmlg ) )
            {
               if ( ! ( ( StringUtil.StrCmp(AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV318ExtraWWContagemResultadoExtraSelectionDS_25_Contagemresultado_tempndhmlg1, "S") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && ( (false==A1802ContagemResultado_TemPndHmlg) || ! A1802ContagemResultado_TemPndHmlg ) ) )
               {
                  if ( ! ( AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV343ExtraWWContagemResultadoExtraSelectionDS_50_Contagemresultado_tempndhmlg2, "C") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && A1802ContagemResultado_TemPndHmlg ) )
                  {
                     if ( ! ( AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV343ExtraWWContagemResultadoExtraSelectionDS_50_Contagemresultado_tempndhmlg2, "S") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && ( (false==A1802ContagemResultado_TemPndHmlg) || ! A1802ContagemResultado_TemPndHmlg ) ) )
                     {
                        if ( ! ( AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV368ExtraWWContagemResultadoExtraSelectionDS_75_Contagemresultado_tempndhmlg3, "C") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && A1802ContagemResultado_TemPndHmlg ) )
                        {
                           if ( ! ( AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV368ExtraWWContagemResultadoExtraSelectionDS_75_Contagemresultado_tempndhmlg3, "S") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && ( (false==A1802ContagemResultado_TemPndHmlg) || ! A1802ContagemResultado_TemPndHmlg ) ) )
                           {
                              if ( ! ( AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV393ExtraWWContagemResultadoExtraSelectionDS_100_Contagemresultado_tempndhmlg4, "C") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && A1802ContagemResultado_TemPndHmlg ) )
                              {
                                 if ( ! ( AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV393ExtraWWContagemResultadoExtraSelectionDS_100_Contagemresultado_tempndhmlg4, "S") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && ( (false==A1802ContagemResultado_TemPndHmlg) || ! A1802ContagemResultado_TemPndHmlg ) ) )
                                 {
                                    if ( ! ( AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV418ExtraWWContagemResultadoExtraSelectionDS_125_Contagemresultado_tempndhmlg5, "C") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && A1802ContagemResultado_TemPndHmlg ) )
                                    {
                                       if ( ! ( AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV418ExtraWWContagemResultadoExtraSelectionDS_125_Contagemresultado_tempndhmlg5, "S") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && ( (false==A1802ContagemResultado_TemPndHmlg) || ! A1802ContagemResultado_TemPndHmlg ) ) )
                                       {
                                          GXt_decimal2 = A574ContagemResultado_PFFinal;
                                          new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal2) ;
                                          A574ContagemResultado_PFFinal = GXt_decimal2;
                                          if ( (Convert.ToDecimal(0)==AV442ExtraWWContagemResultadoExtraSelectionDS_149_Tfcontagemresultado_pffinal) || ( ( A574ContagemResultado_PFFinal >= AV442ExtraWWContagemResultadoExtraSelectionDS_149_Tfcontagemresultado_pffinal ) ) )
                                          {
                                             if ( (Convert.ToDecimal(0)==AV443ExtraWWContagemResultadoExtraSelectionDS_150_Tfcontagemresultado_pffinal_to) || ( ( A574ContagemResultado_PFFinal <= AV443ExtraWWContagemResultadoExtraSelectionDS_150_Tfcontagemresultado_pffinal_to ) ) )
                                             {
                                                AV15ContagemResultado_StatusDmnDescription = gxdomainstatusdemanda.getDescription(context,A484ContagemResultado_StatusDmn);
                                                if ( StringUtil.StrCmp(StringUtil.Trim( StringUtil.BoolToStr( A598ContagemResultado_Baseline)), "True") == 0 )
                                                {
                                                   AV89ContagemResultado_BaselineDescription = "*";
                                                }
                                                else if ( StringUtil.StrCmp(StringUtil.Trim( StringUtil.BoolToStr( A598ContagemResultado_Baseline)), "False") == 0 )
                                                {
                                                   AV89ContagemResultado_BaselineDescription = "";
                                                }
                                                /* Execute user subroutine: 'BEFOREPRINTLINE' */
                                                S152 ();
                                                if ( returnInSub )
                                                {
                                                   pr_default.close(0);
                                                   returnInSub = true;
                                                   if (true) return;
                                                }
                                                H320( false, 20) ;
                                                getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                                                getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A1046ContagemResultado_Agrupador, "@!")), 5, Gx_line+2, 49, Gx_line+17, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A493ContagemResultado_DemandaFM, "")), 54, Gx_line+2, 98, Gx_line+17, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A457ContagemResultado_Demanda, "@!")), 103, Gx_line+2, 147, Gx_line+17, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A495ContagemResultado_SistemaNom, "@!")), 152, Gx_line+2, 196, Gx_line+17, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A494ContagemResultado_Descricao, "")), 201, Gx_line+2, 245, Gx_line+17, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText(context.localUtil.Format( A1351ContagemResultado_DataPrevista, "99/99/99 99:99"), 250, Gx_line+2, 294, Gx_line+17, 2, 0, 0, 0) ;
                                                getPrinter().GxDrawText(context.localUtil.Format( A471ContagemResultado_DataDmn, "99/99/99"), 299, Gx_line+2, 343, Gx_line+17, 2, 0, 0, 0) ;
                                                getPrinter().GxDrawText(context.localUtil.Format( A566ContagemResultado_DataUltCnt, "99/99/99"), 348, Gx_line+2, 392, Gx_line+17, 2, 0, 0, 0) ;
                                                getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A803ContagemResultado_ContratadaSigla, "@!")), 397, Gx_line+2, 441, Gx_line+17, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A1612ContagemResultado_CntNum, "")), 446, Gx_line+2, 490, Gx_line+17, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A515ContagemResultado_SistemaCoord, "@!")), 495, Gx_line+2, 539, Gx_line+17, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV15ContagemResultado_StatusDmnDescription, "")), 544, Gx_line+2, 588, Gx_line+17, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV89ContagemResultado_BaselineDescription, "")), 593, Gx_line+2, 637, Gx_line+17, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A801ContagemResultado_ServicoSigla, "@!")), 642, Gx_line+2, 686, Gx_line+17, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A574ContagemResultado_PFFinal, "ZZ,ZZZ,ZZ9.999")), 691, Gx_line+2, 735, Gx_line+17, 2, 0, 0, 0) ;
                                                getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A512ContagemResultado_ValorPF, "ZZ,ZZZ,ZZZ,ZZ9.999")), 740, Gx_line+2, 784, Gx_line+17, 2, 0, 0, 0) ;
                                                Gx_OldLine = Gx_line;
                                                Gx_line = (int)(Gx_line+20);
                                                /* Execute user subroutine: 'AFTERPRINTLINE' */
                                                S162 ();
                                                if ( returnInSub )
                                                {
                                                   pr_default.close(0);
                                                   returnInSub = true;
                                                   if (true) return;
                                                }
                                             }
                                          }
                                       }
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void S152( )
      {
         /* 'BEFOREPRINTLINE' Routine */
      }

      protected void S162( )
      {
         /* 'AFTERPRINTLINE' Routine */
      }

      protected void S171( )
      {
         /* 'PRINTFOOTER' Routine */
      }

      protected void H320( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
         add_metrics1( ) ;
         add_metrics2( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics1( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      protected void add_metrics2( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, true, 56, 14, 70, 118,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 18, 18, 22, 35, 35, 56, 42, 12, 21, 21, 25, 37, 18, 21, 18, 18, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 18, 18, 37, 37, 37, 35, 64, 42, 42, 45, 45, 42, 38, 49, 45, 18, 32, 42, 35, 53, 45, 49, 42, 49, 45, 42, 38, 45, 42, 61, 42, 42, 38, 18, 18, 18, 30, 35, 21, 35, 35, 32, 35, 35, 18, 35, 35, 14, 14, 32, 14, 52, 35, 35, 35, 35, 21, 32, 18, 35, 32, 45, 32, 32, 29, 21, 16, 21, 37, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 18, 21, 35, 35, 34, 35, 16, 35, 21, 46, 23, 35, 37, 21, 46, 35, 25, 35, 21, 20, 21, 35, 34, 21, 21, 20, 23, 35, 53, 53, 53, 38, 42, 42, 42, 42, 42, 42, 63, 45, 42, 42, 42, 42, 18, 18, 18, 18, 45, 45, 49, 49, 49, 49, 49, 37, 49, 45, 45, 45, 45, 42, 42, 38, 35, 35, 35, 35, 35, 35, 56, 32, 35, 35, 35, 35, 18, 18, 18, 18, 35, 35, 35, 35, 35, 35, 35, 35, 38, 35, 35, 35, 35, 32, 35, 32}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if (IsMain)	waitPrinterEnd();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         AV197Contratadas = new GxSimpleCollection();
         AV198WebSession = context.GetSession();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV178ServicosGeridos = new GxSimpleCollection();
         AV13FilterContagemResultado_StatusCntValueDescription = "";
         AV50GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV51GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV16DynamicFiltersSelector1 = "";
         AV17ContagemResultado_DataCnt1 = DateTime.MinValue;
         AV18ContagemResultado_DataCnt_To1 = DateTime.MinValue;
         AV52FilterContagemResultado_DataCntDescription = "";
         AV53ContagemResultado_DataCnt = DateTime.MinValue;
         AV24ContagemResultado_StatusDmn1 = "";
         AV26FilterContagemResultado_StatusDmn1ValueDescription = "";
         AV25FilterContagemResultado_StatusDmnValueDescription = "";
         AV19ContagemResultado_OsFsOsFm1 = "";
         AV192FilterContagemResultado_OsFsOsFmDescription = "";
         AV54ContagemResultado_OsFsOsFm = "";
         AV247ContagemResultado_DataDmn1 = DateTime.MinValue;
         AV248ContagemResultado_DataDmn_To1 = DateTime.MinValue;
         AV249FilterContagemResultado_DataDmnDescription = "";
         AV250ContagemResultado_DataDmn = DateTime.MinValue;
         AV251ContagemResultado_DataPrevista1 = DateTime.MinValue;
         AV252ContagemResultado_DataPrevista_To1 = DateTime.MinValue;
         AV253FilterContagemResultado_DataPrevistaDescription = "";
         AV254ContagemResultado_DataPrevista = DateTime.MinValue;
         AV90ContagemResultado_Baseline1 = "";
         AV95FilterContagemResultado_Baseline1ValueDescription = "";
         AV94FilterContagemResultado_BaselineValueDescription = "";
         AV168ContagemResultado_Agrupador1 = "";
         AV173ContagemResultado_Agrupador = "";
         AV185ContagemResultado_Descricao1 = "";
         AV186ContagemResultado_Descricao = "";
         AV200ContagemResultado_TemPndHmlg1 = "";
         AV211FilterContagemResultado_TemPndHmlg1ValueDescription = "";
         AV217FilterContagemResultado_TemPndHmlgValueDescription = "";
         AV28DynamicFiltersSelector2 = "";
         AV29ContagemResultado_DataCnt2 = DateTime.MinValue;
         AV30ContagemResultado_DataCnt_To2 = DateTime.MinValue;
         AV36ContagemResultado_StatusDmn2 = "";
         AV37FilterContagemResultado_StatusDmn2ValueDescription = "";
         AV31ContagemResultado_OsFsOsFm2 = "";
         AV255ContagemResultado_DataDmn2 = DateTime.MinValue;
         AV256ContagemResultado_DataDmn_To2 = DateTime.MinValue;
         AV257ContagemResultado_DataPrevista2 = DateTime.MinValue;
         AV258ContagemResultado_DataPrevista_To2 = DateTime.MinValue;
         AV91ContagemResultado_Baseline2 = "";
         AV96FilterContagemResultado_Baseline2ValueDescription = "";
         AV169ContagemResultado_Agrupador2 = "";
         AV187ContagemResultado_Descricao2 = "";
         AV201ContagemResultado_TemPndHmlg2 = "";
         AV212FilterContagemResultado_TemPndHmlg2ValueDescription = "";
         AV39DynamicFiltersSelector3 = "";
         AV40ContagemResultado_DataCnt3 = DateTime.MinValue;
         AV41ContagemResultado_DataCnt_To3 = DateTime.MinValue;
         AV47ContagemResultado_StatusDmn3 = "";
         AV48FilterContagemResultado_StatusDmn3ValueDescription = "";
         AV42ContagemResultado_OsFsOsFm3 = "";
         AV259ContagemResultado_DataDmn3 = DateTime.MinValue;
         AV260ContagemResultado_DataDmn_To3 = DateTime.MinValue;
         AV261ContagemResultado_DataPrevista3 = DateTime.MinValue;
         AV262ContagemResultado_DataPrevista_To3 = DateTime.MinValue;
         AV92ContagemResultado_Baseline3 = "";
         AV97FilterContagemResultado_Baseline3ValueDescription = "";
         AV170ContagemResultado_Agrupador3 = "";
         AV188ContagemResultado_Descricao3 = "";
         AV202ContagemResultado_TemPndHmlg3 = "";
         AV213FilterContagemResultado_TemPndHmlg3ValueDescription = "";
         AV122DynamicFiltersSelector4 = "";
         AV123ContagemResultado_DataCnt4 = DateTime.MinValue;
         AV124ContagemResultado_DataCnt_To4 = DateTime.MinValue;
         AV126ContagemResultado_StatusDmn4 = "";
         AV127FilterContagemResultado_StatusDmn4ValueDescription = "";
         AV125ContagemResultado_OsFsOsFm4 = "";
         AV263ContagemResultado_DataDmn4 = DateTime.MinValue;
         AV264ContagemResultado_DataDmn_To4 = DateTime.MinValue;
         AV265ContagemResultado_DataPrevista4 = DateTime.MinValue;
         AV266ContagemResultado_DataPrevista_To4 = DateTime.MinValue;
         AV133ContagemResultado_Baseline4 = "";
         AV134FilterContagemResultado_Baseline4ValueDescription = "";
         AV171ContagemResultado_Agrupador4 = "";
         AV189ContagemResultado_Descricao4 = "";
         AV203ContagemResultado_TemPndHmlg4 = "";
         AV214FilterContagemResultado_TemPndHmlg4ValueDescription = "";
         AV139DynamicFiltersSelector5 = "";
         AV140ContagemResultado_DataCnt5 = DateTime.MinValue;
         AV141ContagemResultado_DataCnt_To5 = DateTime.MinValue;
         AV143ContagemResultado_StatusDmn5 = "";
         AV144FilterContagemResultado_StatusDmn5ValueDescription = "";
         AV142ContagemResultado_OsFsOsFm5 = "";
         AV267ContagemResultado_DataDmn5 = DateTime.MinValue;
         AV268ContagemResultado_DataDmn_To5 = DateTime.MinValue;
         AV269ContagemResultado_DataPrevista5 = DateTime.MinValue;
         AV270ContagemResultado_DataPrevista_To5 = DateTime.MinValue;
         AV150ContagemResultado_Baseline5 = "";
         AV151FilterContagemResultado_Baseline5ValueDescription = "";
         AV172ContagemResultado_Agrupador5 = "";
         AV190ContagemResultado_Descricao5 = "";
         AV204ContagemResultado_TemPndHmlg5 = "";
         AV215FilterContagemResultado_TemPndHmlg5ValueDescription = "";
         AV236TFContagemResultado_StatusDmn_Sels = new GxSimpleCollection();
         AV237TFContagemResultado_StatusDmn_Sel = "";
         AV235TFContagemResultado_StatusDmn_SelDscs = "";
         AV245FilterTFContagemResultado_StatusDmn_SelValueDescription = "";
         AV246FilterTFContagemResultado_Baseline_SelValueDescription = "";
         AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 = "";
         AV298ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1 = DateTime.MinValue;
         AV299ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1 = DateTime.MinValue;
         AV300ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1 = "";
         AV301ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 = "";
         AV302ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1 = DateTime.MinValue;
         AV303ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1 = DateTime.MinValue;
         AV304ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1 = DateTime.MinValue;
         AV305ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1 = DateTime.MinValue;
         AV311ExtraWWContagemResultadoExtraSelectionDS_18_Contagemresultado_baseline1 = "";
         AV315ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1 = "";
         AV316ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 = "";
         AV318ExtraWWContagemResultadoExtraSelectionDS_25_Contagemresultado_tempndhmlg1 = "";
         AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 = "";
         AV323ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2 = DateTime.MinValue;
         AV324ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2 = DateTime.MinValue;
         AV325ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2 = "";
         AV326ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 = "";
         AV327ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2 = DateTime.MinValue;
         AV328ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2 = DateTime.MinValue;
         AV329ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2 = DateTime.MinValue;
         AV330ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2 = DateTime.MinValue;
         AV336ExtraWWContagemResultadoExtraSelectionDS_43_Contagemresultado_baseline2 = "";
         AV340ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2 = "";
         AV341ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 = "";
         AV343ExtraWWContagemResultadoExtraSelectionDS_50_Contagemresultado_tempndhmlg2 = "";
         AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 = "";
         AV348ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3 = DateTime.MinValue;
         AV349ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3 = DateTime.MinValue;
         AV350ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3 = "";
         AV351ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 = "";
         AV352ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3 = DateTime.MinValue;
         AV353ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3 = DateTime.MinValue;
         AV354ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3 = DateTime.MinValue;
         AV355ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3 = DateTime.MinValue;
         AV361ExtraWWContagemResultadoExtraSelectionDS_68_Contagemresultado_baseline3 = "";
         AV365ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3 = "";
         AV366ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 = "";
         AV368ExtraWWContagemResultadoExtraSelectionDS_75_Contagemresultado_tempndhmlg3 = "";
         AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 = "";
         AV373ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4 = DateTime.MinValue;
         AV374ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4 = DateTime.MinValue;
         AV375ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4 = "";
         AV376ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 = "";
         AV377ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4 = DateTime.MinValue;
         AV378ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4 = DateTime.MinValue;
         AV379ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4 = DateTime.MinValue;
         AV380ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4 = DateTime.MinValue;
         AV386ExtraWWContagemResultadoExtraSelectionDS_93_Contagemresultado_baseline4 = "";
         AV390ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4 = "";
         AV391ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 = "";
         AV393ExtraWWContagemResultadoExtraSelectionDS_100_Contagemresultado_tempndhmlg4 = "";
         AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 = "";
         AV398ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5 = DateTime.MinValue;
         AV399ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5 = DateTime.MinValue;
         AV400ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5 = "";
         AV401ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 = "";
         AV402ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5 = DateTime.MinValue;
         AV403ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5 = DateTime.MinValue;
         AV404ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5 = DateTime.MinValue;
         AV405ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5 = DateTime.MinValue;
         AV411ExtraWWContagemResultadoExtraSelectionDS_118_Contagemresultado_baseline5 = "";
         AV415ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5 = "";
         AV416ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 = "";
         AV418ExtraWWContagemResultadoExtraSelectionDS_125_Contagemresultado_tempndhmlg5 = "";
         AV420ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador = "";
         AV421ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel = "";
         AV422ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm = "";
         AV423ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel = "";
         AV424ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda = "";
         AV425ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel = "";
         AV426ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao = "";
         AV427ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel = "";
         AV428ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista = (DateTime)(DateTime.MinValue);
         AV429ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to = (DateTime)(DateTime.MinValue);
         AV430ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn = DateTime.MinValue;
         AV431ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to = DateTime.MinValue;
         AV432ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt = DateTime.MinValue;
         AV433ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to = DateTime.MinValue;
         AV434ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla = "";
         AV435ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel = "";
         AV436ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord = "";
         AV437ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel = "";
         AV438ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels = new GxSimpleCollection();
         AV440ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla = "";
         AV441ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel = "";
         scmdbuf = "";
         lV301ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 = "";
         lV316ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 = "";
         lV326ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 = "";
         lV341ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 = "";
         lV351ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 = "";
         lV366ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 = "";
         lV376ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 = "";
         lV391ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 = "";
         lV401ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 = "";
         lV416ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 = "";
         lV420ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador = "";
         lV422ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm = "";
         lV424ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda = "";
         lV426ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao = "";
         lV434ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla = "";
         lV436ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord = "";
         lV440ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla = "";
         A484ContagemResultado_StatusDmn = "";
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         A1046ContagemResultado_Agrupador = "";
         A494ContagemResultado_Descricao = "";
         A803ContagemResultado_ContratadaSigla = "";
         A515ContagemResultado_SistemaCoord = "";
         A801ContagemResultado_ServicoSigla = "";
         A566ContagemResultado_DataUltCnt = DateTime.MinValue;
         P00323_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00323_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00323_A1583ContagemResultado_TipoRegistro = new short[1] ;
         P00323_A512ContagemResultado_ValorPF = new decimal[1] ;
         P00323_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P00323_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00323_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P00323_A515ContagemResultado_SistemaCoord = new String[] {""} ;
         P00323_n515ContagemResultado_SistemaCoord = new bool[] {false} ;
         P00323_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         P00323_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         P00323_A1603ContagemResultado_CntCod = new int[1] ;
         P00323_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P00323_A494ContagemResultado_Descricao = new String[] {""} ;
         P00323_n494ContagemResultado_Descricao = new bool[] {false} ;
         P00323_A1046ContagemResultado_Agrupador = new String[] {""} ;
         P00323_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         P00323_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         P00323_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         P00323_A598ContagemResultado_Baseline = new bool[] {false} ;
         P00323_n598ContagemResultado_Baseline = new bool[] {false} ;
         P00323_A764ContagemResultado_ServicoGrupo = new int[1] ;
         P00323_n764ContagemResultado_ServicoGrupo = new bool[] {false} ;
         P00323_A489ContagemResultado_SistemaCod = new int[1] ;
         P00323_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00323_A508ContagemResultado_Owner = new int[1] ;
         P00323_A601ContagemResultado_Servico = new int[1] ;
         P00323_n601ContagemResultado_Servico = new bool[] {false} ;
         P00323_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P00323_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P00323_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00323_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00323_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00323_A457ContagemResultado_Demanda = new String[] {""} ;
         P00323_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00323_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00323_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00323_A1854ContagemResultado_VlrCnc = new decimal[1] ;
         P00323_n1854ContagemResultado_VlrCnc = new bool[] {false} ;
         P00323_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00323_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00323_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00323_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00323_A1612ContagemResultado_CntNum = new String[] {""} ;
         P00323_n1612ContagemResultado_CntNum = new bool[] {false} ;
         P00323_A495ContagemResultado_SistemaNom = new String[] {""} ;
         P00323_n495ContagemResultado_SistemaNom = new bool[] {false} ;
         P00323_A683ContagemResultado_PFLFMUltima = new decimal[1] ;
         P00323_A685ContagemResultado_PFLFSUltima = new decimal[1] ;
         P00323_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         P00323_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         P00323_A584ContagemResultado_ContadorFM = new int[1] ;
         P00323_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         P00323_A531ContagemResultado_StatusUltCnt = new short[1] ;
         P00323_A456ContagemResultado_Codigo = new int[1] ;
         A1612ContagemResultado_CntNum = "";
         A495ContagemResultado_SistemaNom = "";
         AV15ContagemResultado_StatusDmnDescription = "";
         AV89ContagemResultado_BaselineDescription = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.aexportreportextrawwcontagemresultadoextraselection__default(),
            new Object[][] {
                new Object[] {
               P00323_A1553ContagemResultado_CntSrvCod, P00323_n1553ContagemResultado_CntSrvCod, P00323_A1583ContagemResultado_TipoRegistro, P00323_A512ContagemResultado_ValorPF, P00323_n512ContagemResultado_ValorPF, P00323_A801ContagemResultado_ServicoSigla, P00323_n801ContagemResultado_ServicoSigla, P00323_A515ContagemResultado_SistemaCoord, P00323_n515ContagemResultado_SistemaCoord, P00323_A803ContagemResultado_ContratadaSigla,
               P00323_n803ContagemResultado_ContratadaSigla, P00323_A1603ContagemResultado_CntCod, P00323_n1603ContagemResultado_CntCod, P00323_A494ContagemResultado_Descricao, P00323_n494ContagemResultado_Descricao, P00323_A1046ContagemResultado_Agrupador, P00323_n1046ContagemResultado_Agrupador, P00323_A468ContagemResultado_NaoCnfDmnCod, P00323_n468ContagemResultado_NaoCnfDmnCod, P00323_A598ContagemResultado_Baseline,
               P00323_n598ContagemResultado_Baseline, P00323_A764ContagemResultado_ServicoGrupo, P00323_n764ContagemResultado_ServicoGrupo, P00323_A489ContagemResultado_SistemaCod, P00323_n489ContagemResultado_SistemaCod, P00323_A508ContagemResultado_Owner, P00323_A601ContagemResultado_Servico, P00323_n601ContagemResultado_Servico, P00323_A1351ContagemResultado_DataPrevista, P00323_n1351ContagemResultado_DataPrevista,
               P00323_A471ContagemResultado_DataDmn, P00323_A493ContagemResultado_DemandaFM, P00323_n493ContagemResultado_DemandaFM, P00323_A457ContagemResultado_Demanda, P00323_n457ContagemResultado_Demanda, P00323_A484ContagemResultado_StatusDmn, P00323_n484ContagemResultado_StatusDmn, P00323_A1854ContagemResultado_VlrCnc, P00323_n1854ContagemResultado_VlrCnc, P00323_A490ContagemResultado_ContratadaCod,
               P00323_n490ContagemResultado_ContratadaCod, P00323_A52Contratada_AreaTrabalhoCod, P00323_n52Contratada_AreaTrabalhoCod, P00323_A1612ContagemResultado_CntNum, P00323_n1612ContagemResultado_CntNum, P00323_A495ContagemResultado_SistemaNom, P00323_n495ContagemResultado_SistemaNom, P00323_A683ContagemResultado_PFLFMUltima, P00323_A685ContagemResultado_PFLFSUltima, P00323_A682ContagemResultado_PFBFMUltima,
               P00323_A684ContagemResultado_PFBFSUltima, P00323_A584ContagemResultado_ContadorFM, P00323_A566ContagemResultado_DataUltCnt, P00323_A531ContagemResultado_StatusUltCnt, P00323_A456ContagemResultado_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         Gx_line = 0;
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short AV14ContagemResultado_StatusCnt ;
      private short AV238TFContagemResultado_Baseline_Sel ;
      private short AV10OrderedBy ;
      private short GxWebError ;
      private short AV176p ;
      private short AV191DynamicFiltersOperator1 ;
      private short AV193DynamicFiltersOperator2 ;
      private short AV194DynamicFiltersOperator3 ;
      private short AV195DynamicFiltersOperator4 ;
      private short AV196DynamicFiltersOperator5 ;
      private short AV295ExtraWWContagemResultadoExtraSelectionDS_2_Contagemresultado_statuscnt ;
      private short AV297ExtraWWContagemResultadoExtraSelectionDS_4_Dynamicfiltersoperator1 ;
      private short AV322ExtraWWContagemResultadoExtraSelectionDS_29_Dynamicfiltersoperator2 ;
      private short AV347ExtraWWContagemResultadoExtraSelectionDS_54_Dynamicfiltersoperator3 ;
      private short AV372ExtraWWContagemResultadoExtraSelectionDS_79_Dynamicfiltersoperator4 ;
      private short AV397ExtraWWContagemResultadoExtraSelectionDS_104_Dynamicfiltersoperator5 ;
      private short AV439ExtraWWContagemResultadoExtraSelectionDS_146_Tfcontagemresultado_baseline_sel ;
      private short A531ContagemResultado_StatusUltCnt ;
      private short A1583ContagemResultado_TipoRegistro ;
      private int AV12Contratada_AreaTrabalhoCod ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int AV177Servico ;
      private int Gx_OldLine ;
      private int AV102ContagemResultado_Servico1 ;
      private int AV105ContagemResultado_Servico ;
      private int AV118ContagemResultado_ContadorFM1 ;
      private int AV155ContagemResultado_ContadorFM ;
      private int AV21ContagemResultado_SistemaCod1 ;
      private int AV56ContagemResultado_SistemaCod ;
      private int AV22ContagemResultado_ContratadaCod1 ;
      private int AV57ContagemResultado_ContratadaCod ;
      private int AV114ContagemResultado_ServicoGrupo1 ;
      private int AV117ContagemResultado_ServicoGrupo ;
      private int AV23ContagemResultado_NaoCnfDmnCod1 ;
      private int AV58ContagemResultado_NaoCnfDmnCod ;
      private int AV179ContagemResultado_Owner1 ;
      private int AV180ContagemResultado_Owner ;
      private int AV282ContagemResultado_CntCod1 ;
      private int AV283ContagemResultado_CntCod ;
      private int AV103ContagemResultado_Servico2 ;
      private int AV119ContagemResultado_ContadorFM2 ;
      private int AV33ContagemResultado_SistemaCod2 ;
      private int AV34ContagemResultado_ContratadaCod2 ;
      private int AV115ContagemResultado_ServicoGrupo2 ;
      private int AV35ContagemResultado_NaoCnfDmnCod2 ;
      private int AV181ContagemResultado_Owner2 ;
      private int AV284ContagemResultado_CntCod2 ;
      private int AV104ContagemResultado_Servico3 ;
      private int AV120ContagemResultado_ContadorFM3 ;
      private int AV44ContagemResultado_SistemaCod3 ;
      private int AV45ContagemResultado_ContratadaCod3 ;
      private int AV116ContagemResultado_ServicoGrupo3 ;
      private int AV46ContagemResultado_NaoCnfDmnCod3 ;
      private int AV182ContagemResultado_Owner3 ;
      private int AV285ContagemResultado_CntCod3 ;
      private int AV128ContagemResultado_Servico4 ;
      private int AV129ContagemResultado_ContadorFM4 ;
      private int AV130ContagemResultado_SistemaCod4 ;
      private int AV131ContagemResultado_ContratadaCod4 ;
      private int AV132ContagemResultado_ServicoGrupo4 ;
      private int AV135ContagemResultado_NaoCnfDmnCod4 ;
      private int AV183ContagemResultado_Owner4 ;
      private int AV286ContagemResultado_CntCod4 ;
      private int AV145ContagemResultado_Servico5 ;
      private int AV146ContagemResultado_ContadorFM5 ;
      private int AV147ContagemResultado_SistemaCod5 ;
      private int AV148ContagemResultado_ContratadaCod5 ;
      private int AV149ContagemResultado_ServicoGrupo5 ;
      private int AV152ContagemResultado_NaoCnfDmnCod5 ;
      private int AV184ContagemResultado_Owner5 ;
      private int AV287ContagemResultado_CntCod5 ;
      private int AV292GXV1 ;
      private int AV294ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod ;
      private int AV306ExtraWWContagemResultadoExtraSelectionDS_13_Contagemresultado_servico1 ;
      private int AV307ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1 ;
      private int AV308ExtraWWContagemResultadoExtraSelectionDS_15_Contagemresultado_sistemacod1 ;
      private int AV309ExtraWWContagemResultadoExtraSelectionDS_16_Contagemresultado_contratadacod1 ;
      private int AV310ExtraWWContagemResultadoExtraSelectionDS_17_Contagemresultado_servicogrupo1 ;
      private int AV312ExtraWWContagemResultadoExtraSelectionDS_19_Contagemresultado_naocnfdmncod1 ;
      private int AV317ExtraWWContagemResultadoExtraSelectionDS_24_Contagemresultado_owner1 ;
      private int AV319ExtraWWContagemResultadoExtraSelectionDS_26_Contagemresultado_cntcod1 ;
      private int AV331ExtraWWContagemResultadoExtraSelectionDS_38_Contagemresultado_servico2 ;
      private int AV332ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2 ;
      private int AV333ExtraWWContagemResultadoExtraSelectionDS_40_Contagemresultado_sistemacod2 ;
      private int AV334ExtraWWContagemResultadoExtraSelectionDS_41_Contagemresultado_contratadacod2 ;
      private int AV335ExtraWWContagemResultadoExtraSelectionDS_42_Contagemresultado_servicogrupo2 ;
      private int AV337ExtraWWContagemResultadoExtraSelectionDS_44_Contagemresultado_naocnfdmncod2 ;
      private int AV342ExtraWWContagemResultadoExtraSelectionDS_49_Contagemresultado_owner2 ;
      private int AV344ExtraWWContagemResultadoExtraSelectionDS_51_Contagemresultado_cntcod2 ;
      private int AV356ExtraWWContagemResultadoExtraSelectionDS_63_Contagemresultado_servico3 ;
      private int AV357ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3 ;
      private int AV358ExtraWWContagemResultadoExtraSelectionDS_65_Contagemresultado_sistemacod3 ;
      private int AV359ExtraWWContagemResultadoExtraSelectionDS_66_Contagemresultado_contratadacod3 ;
      private int AV360ExtraWWContagemResultadoExtraSelectionDS_67_Contagemresultado_servicogrupo3 ;
      private int AV362ExtraWWContagemResultadoExtraSelectionDS_69_Contagemresultado_naocnfdmncod3 ;
      private int AV367ExtraWWContagemResultadoExtraSelectionDS_74_Contagemresultado_owner3 ;
      private int AV369ExtraWWContagemResultadoExtraSelectionDS_76_Contagemresultado_cntcod3 ;
      private int AV381ExtraWWContagemResultadoExtraSelectionDS_88_Contagemresultado_servico4 ;
      private int AV382ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4 ;
      private int AV383ExtraWWContagemResultadoExtraSelectionDS_90_Contagemresultado_sistemacod4 ;
      private int AV384ExtraWWContagemResultadoExtraSelectionDS_91_Contagemresultado_contratadacod4 ;
      private int AV385ExtraWWContagemResultadoExtraSelectionDS_92_Contagemresultado_servicogrupo4 ;
      private int AV387ExtraWWContagemResultadoExtraSelectionDS_94_Contagemresultado_naocnfdmncod4 ;
      private int AV392ExtraWWContagemResultadoExtraSelectionDS_99_Contagemresultado_owner4 ;
      private int AV394ExtraWWContagemResultadoExtraSelectionDS_101_Contagemresultado_cntcod4 ;
      private int AV406ExtraWWContagemResultadoExtraSelectionDS_113_Contagemresultado_servico5 ;
      private int AV407ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5 ;
      private int AV408ExtraWWContagemResultadoExtraSelectionDS_115_Contagemresultado_sistemacod5 ;
      private int AV409ExtraWWContagemResultadoExtraSelectionDS_116_Contagemresultado_contratadacod5 ;
      private int AV410ExtraWWContagemResultadoExtraSelectionDS_117_Contagemresultado_servicogrupo5 ;
      private int AV412ExtraWWContagemResultadoExtraSelectionDS_119_Contagemresultado_naocnfdmncod5 ;
      private int AV417ExtraWWContagemResultadoExtraSelectionDS_124_Contagemresultado_owner5 ;
      private int AV419ExtraWWContagemResultadoExtraSelectionDS_126_Contagemresultado_cntcod5 ;
      private int AV9WWPContext_gxTpr_Contratada_codigo ;
      private int AV438ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels_Count ;
      private int AV50GridState_gxTpr_Dynamicfilters_Count ;
      private int AV197Contratadas_Count ;
      private int AV9WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A601ContagemResultado_Servico ;
      private int A489ContagemResultado_SistemaCod ;
      private int A764ContagemResultado_ServicoGrupo ;
      private int A468ContagemResultado_NaoCnfDmnCod ;
      private int A508ContagemResultado_Owner ;
      private int A1603ContagemResultado_CntCod ;
      private int A456ContagemResultado_Codigo ;
      private int A584ContagemResultado_ContadorFM ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private long AV175i ;
      private decimal AV241TFContagemResultado_PFFinal ;
      private decimal AV242TFContagemResultado_PFFinal_To ;
      private decimal AV243TFContagemResultado_ValorPF ;
      private decimal AV244TFContagemResultado_ValorPF_To ;
      private decimal AV77ContagemResultado_PFBFM1 ;
      private decimal AV83ContagemResultado_PFBFM ;
      private decimal AV78ContagemResultado_PFBFS1 ;
      private decimal AV84ContagemResultado_PFBFS ;
      private decimal AV79ContagemResultado_PFBFM2 ;
      private decimal AV80ContagemResultado_PFBFS2 ;
      private decimal AV81ContagemResultado_PFBFM3 ;
      private decimal AV82ContagemResultado_PFBFS3 ;
      private decimal AV136ContagemResultado_PFBFM4 ;
      private decimal AV137ContagemResultado_PFBFS4 ;
      private decimal AV153ContagemResultado_PFBFM5 ;
      private decimal AV154ContagemResultado_PFBFS5 ;
      private decimal AV313ExtraWWContagemResultadoExtraSelectionDS_20_Contagemresultado_pfbfm1 ;
      private decimal AV314ExtraWWContagemResultadoExtraSelectionDS_21_Contagemresultado_pfbfs1 ;
      private decimal AV338ExtraWWContagemResultadoExtraSelectionDS_45_Contagemresultado_pfbfm2 ;
      private decimal AV339ExtraWWContagemResultadoExtraSelectionDS_46_Contagemresultado_pfbfs2 ;
      private decimal AV363ExtraWWContagemResultadoExtraSelectionDS_70_Contagemresultado_pfbfm3 ;
      private decimal AV364ExtraWWContagemResultadoExtraSelectionDS_71_Contagemresultado_pfbfs3 ;
      private decimal AV388ExtraWWContagemResultadoExtraSelectionDS_95_Contagemresultado_pfbfm4 ;
      private decimal AV389ExtraWWContagemResultadoExtraSelectionDS_96_Contagemresultado_pfbfs4 ;
      private decimal AV413ExtraWWContagemResultadoExtraSelectionDS_120_Contagemresultado_pfbfm5 ;
      private decimal AV414ExtraWWContagemResultadoExtraSelectionDS_121_Contagemresultado_pfbfs5 ;
      private decimal AV442ExtraWWContagemResultadoExtraSelectionDS_149_Tfcontagemresultado_pffinal ;
      private decimal AV443ExtraWWContagemResultadoExtraSelectionDS_150_Tfcontagemresultado_pffinal_to ;
      private decimal AV444ExtraWWContagemResultadoExtraSelectionDS_151_Tfcontagemresultado_valorpf ;
      private decimal AV445ExtraWWContagemResultadoExtraSelectionDS_152_Tfcontagemresultado_valorpf_to ;
      private decimal A512ContagemResultado_ValorPF ;
      private decimal A1854ContagemResultado_VlrCnc ;
      private decimal A684ContagemResultado_PFBFSUltima ;
      private decimal A682ContagemResultado_PFBFMUltima ;
      private decimal A685ContagemResultado_PFLFSUltima ;
      private decimal A683ContagemResultado_PFLFMUltima ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal GXt_decimal2 ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String AV220TFContagemResultado_Agrupador ;
      private String AV221TFContagemResultado_Agrupador_Sel ;
      private String AV230TFContagemResultado_ContratadaSigla ;
      private String AV231TFContagemResultado_ContratadaSigla_Sel ;
      private String AV239TFContagemResultado_ServicoSigla ;
      private String AV240TFContagemResultado_ServicoSigla_Sel ;
      private String AV24ContagemResultado_StatusDmn1 ;
      private String AV90ContagemResultado_Baseline1 ;
      private String AV168ContagemResultado_Agrupador1 ;
      private String AV173ContagemResultado_Agrupador ;
      private String AV200ContagemResultado_TemPndHmlg1 ;
      private String AV36ContagemResultado_StatusDmn2 ;
      private String AV91ContagemResultado_Baseline2 ;
      private String AV169ContagemResultado_Agrupador2 ;
      private String AV201ContagemResultado_TemPndHmlg2 ;
      private String AV47ContagemResultado_StatusDmn3 ;
      private String AV92ContagemResultado_Baseline3 ;
      private String AV170ContagemResultado_Agrupador3 ;
      private String AV202ContagemResultado_TemPndHmlg3 ;
      private String AV126ContagemResultado_StatusDmn4 ;
      private String AV133ContagemResultado_Baseline4 ;
      private String AV171ContagemResultado_Agrupador4 ;
      private String AV203ContagemResultado_TemPndHmlg4 ;
      private String AV143ContagemResultado_StatusDmn5 ;
      private String AV150ContagemResultado_Baseline5 ;
      private String AV172ContagemResultado_Agrupador5 ;
      private String AV204ContagemResultado_TemPndHmlg5 ;
      private String AV237TFContagemResultado_StatusDmn_Sel ;
      private String AV300ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1 ;
      private String AV311ExtraWWContagemResultadoExtraSelectionDS_18_Contagemresultado_baseline1 ;
      private String AV315ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1 ;
      private String AV318ExtraWWContagemResultadoExtraSelectionDS_25_Contagemresultado_tempndhmlg1 ;
      private String AV325ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2 ;
      private String AV336ExtraWWContagemResultadoExtraSelectionDS_43_Contagemresultado_baseline2 ;
      private String AV340ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2 ;
      private String AV343ExtraWWContagemResultadoExtraSelectionDS_50_Contagemresultado_tempndhmlg2 ;
      private String AV350ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3 ;
      private String AV361ExtraWWContagemResultadoExtraSelectionDS_68_Contagemresultado_baseline3 ;
      private String AV365ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3 ;
      private String AV368ExtraWWContagemResultadoExtraSelectionDS_75_Contagemresultado_tempndhmlg3 ;
      private String AV375ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4 ;
      private String AV386ExtraWWContagemResultadoExtraSelectionDS_93_Contagemresultado_baseline4 ;
      private String AV390ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4 ;
      private String AV393ExtraWWContagemResultadoExtraSelectionDS_100_Contagemresultado_tempndhmlg4 ;
      private String AV400ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5 ;
      private String AV411ExtraWWContagemResultadoExtraSelectionDS_118_Contagemresultado_baseline5 ;
      private String AV415ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5 ;
      private String AV418ExtraWWContagemResultadoExtraSelectionDS_125_Contagemresultado_tempndhmlg5 ;
      private String AV420ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador ;
      private String AV421ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel ;
      private String AV434ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla ;
      private String AV435ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel ;
      private String AV440ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla ;
      private String AV441ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel ;
      private String scmdbuf ;
      private String lV420ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador ;
      private String lV434ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla ;
      private String lV440ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla ;
      private String A484ContagemResultado_StatusDmn ;
      private String A1046ContagemResultado_Agrupador ;
      private String A803ContagemResultado_ContratadaSigla ;
      private String A801ContagemResultado_ServicoSigla ;
      private String A1612ContagemResultado_CntNum ;
      private DateTime AV273TFContagemResultado_DataPrevista ;
      private DateTime AV274TFContagemResultado_DataPrevista_To ;
      private DateTime AV428ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista ;
      private DateTime AV429ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to ;
      private DateTime A1351ContagemResultado_DataPrevista ;
      private DateTime AV271TFContagemResultado_DataDmn ;
      private DateTime AV272TFContagemResultado_DataDmn_To ;
      private DateTime AV228TFContagemResultado_DataUltCnt ;
      private DateTime AV229TFContagemResultado_DataUltCnt_To ;
      private DateTime AV17ContagemResultado_DataCnt1 ;
      private DateTime AV18ContagemResultado_DataCnt_To1 ;
      private DateTime AV53ContagemResultado_DataCnt ;
      private DateTime AV247ContagemResultado_DataDmn1 ;
      private DateTime AV248ContagemResultado_DataDmn_To1 ;
      private DateTime AV250ContagemResultado_DataDmn ;
      private DateTime AV251ContagemResultado_DataPrevista1 ;
      private DateTime AV252ContagemResultado_DataPrevista_To1 ;
      private DateTime AV254ContagemResultado_DataPrevista ;
      private DateTime AV29ContagemResultado_DataCnt2 ;
      private DateTime AV30ContagemResultado_DataCnt_To2 ;
      private DateTime AV255ContagemResultado_DataDmn2 ;
      private DateTime AV256ContagemResultado_DataDmn_To2 ;
      private DateTime AV257ContagemResultado_DataPrevista2 ;
      private DateTime AV258ContagemResultado_DataPrevista_To2 ;
      private DateTime AV40ContagemResultado_DataCnt3 ;
      private DateTime AV41ContagemResultado_DataCnt_To3 ;
      private DateTime AV259ContagemResultado_DataDmn3 ;
      private DateTime AV260ContagemResultado_DataDmn_To3 ;
      private DateTime AV261ContagemResultado_DataPrevista3 ;
      private DateTime AV262ContagemResultado_DataPrevista_To3 ;
      private DateTime AV123ContagemResultado_DataCnt4 ;
      private DateTime AV124ContagemResultado_DataCnt_To4 ;
      private DateTime AV263ContagemResultado_DataDmn4 ;
      private DateTime AV264ContagemResultado_DataDmn_To4 ;
      private DateTime AV265ContagemResultado_DataPrevista4 ;
      private DateTime AV266ContagemResultado_DataPrevista_To4 ;
      private DateTime AV140ContagemResultado_DataCnt5 ;
      private DateTime AV141ContagemResultado_DataCnt_To5 ;
      private DateTime AV267ContagemResultado_DataDmn5 ;
      private DateTime AV268ContagemResultado_DataDmn_To5 ;
      private DateTime AV269ContagemResultado_DataPrevista5 ;
      private DateTime AV270ContagemResultado_DataPrevista_To5 ;
      private DateTime AV298ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1 ;
      private DateTime AV299ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1 ;
      private DateTime AV302ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1 ;
      private DateTime AV303ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1 ;
      private DateTime AV304ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1 ;
      private DateTime AV305ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1 ;
      private DateTime AV323ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2 ;
      private DateTime AV324ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2 ;
      private DateTime AV327ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2 ;
      private DateTime AV328ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2 ;
      private DateTime AV329ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2 ;
      private DateTime AV330ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2 ;
      private DateTime AV348ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3 ;
      private DateTime AV349ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3 ;
      private DateTime AV352ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3 ;
      private DateTime AV353ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3 ;
      private DateTime AV354ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3 ;
      private DateTime AV355ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3 ;
      private DateTime AV373ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4 ;
      private DateTime AV374ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4 ;
      private DateTime AV377ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4 ;
      private DateTime AV378ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4 ;
      private DateTime AV379ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4 ;
      private DateTime AV380ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4 ;
      private DateTime AV398ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5 ;
      private DateTime AV399ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5 ;
      private DateTime AV402ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5 ;
      private DateTime AV403ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5 ;
      private DateTime AV404ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5 ;
      private DateTime AV405ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5 ;
      private DateTime AV430ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn ;
      private DateTime AV431ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to ;
      private DateTime AV432ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt ;
      private DateTime AV433ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime A566ContagemResultado_DataUltCnt ;
      private bool entryPointCalled ;
      private bool AV11OrderedDsc ;
      private bool returnInSub ;
      private bool AV27DynamicFiltersEnabled2 ;
      private bool AV38DynamicFiltersEnabled3 ;
      private bool AV121DynamicFiltersEnabled4 ;
      private bool AV138DynamicFiltersEnabled5 ;
      private bool AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 ;
      private bool AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 ;
      private bool AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 ;
      private bool AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 ;
      private bool AV9WWPContext_gxTpr_Userehfinanceiro ;
      private bool AV9WWPContext_gxTpr_Userehgestor ;
      private bool A598ContagemResultado_Baseline ;
      private bool A1802ContagemResultado_TemPndHmlg ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n512ContagemResultado_ValorPF ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool n515ContagemResultado_SistemaCoord ;
      private bool n803ContagemResultado_ContratadaSigla ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n494ContagemResultado_Descricao ;
      private bool n1046ContagemResultado_Agrupador ;
      private bool n468ContagemResultado_NaoCnfDmnCod ;
      private bool n598ContagemResultado_Baseline ;
      private bool n764ContagemResultado_ServicoGrupo ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n1351ContagemResultado_DataPrevista ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n457ContagemResultado_Demanda ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n1854ContagemResultado_VlrCnc ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n1612ContagemResultado_CntNum ;
      private bool n495ContagemResultado_SistemaNom ;
      private bool GXt_boolean1 ;
      private String AV234TFContagemResultado_StatusDmn_SelsJson ;
      private String AV49GridStateXML ;
      private String AV224TFContagemResultado_DemandaFM ;
      private String AV225TFContagemResultado_DemandaFM_Sel ;
      private String AV222TFContagemResultado_Demanda ;
      private String AV223TFContagemResultado_Demanda_Sel ;
      private String AV226TFContagemResultado_Descricao ;
      private String AV227TFContagemResultado_Descricao_Sel ;
      private String AV232TFContagemResultado_SistemaCoord ;
      private String AV233TFContagemResultado_SistemaCoord_Sel ;
      private String AV13FilterContagemResultado_StatusCntValueDescription ;
      private String AV16DynamicFiltersSelector1 ;
      private String AV52FilterContagemResultado_DataCntDescription ;
      private String AV26FilterContagemResultado_StatusDmn1ValueDescription ;
      private String AV25FilterContagemResultado_StatusDmnValueDescription ;
      private String AV19ContagemResultado_OsFsOsFm1 ;
      private String AV192FilterContagemResultado_OsFsOsFmDescription ;
      private String AV54ContagemResultado_OsFsOsFm ;
      private String AV249FilterContagemResultado_DataDmnDescription ;
      private String AV253FilterContagemResultado_DataPrevistaDescription ;
      private String AV95FilterContagemResultado_Baseline1ValueDescription ;
      private String AV94FilterContagemResultado_BaselineValueDescription ;
      private String AV185ContagemResultado_Descricao1 ;
      private String AV186ContagemResultado_Descricao ;
      private String AV211FilterContagemResultado_TemPndHmlg1ValueDescription ;
      private String AV217FilterContagemResultado_TemPndHmlgValueDescription ;
      private String AV28DynamicFiltersSelector2 ;
      private String AV37FilterContagemResultado_StatusDmn2ValueDescription ;
      private String AV31ContagemResultado_OsFsOsFm2 ;
      private String AV96FilterContagemResultado_Baseline2ValueDescription ;
      private String AV187ContagemResultado_Descricao2 ;
      private String AV212FilterContagemResultado_TemPndHmlg2ValueDescription ;
      private String AV39DynamicFiltersSelector3 ;
      private String AV48FilterContagemResultado_StatusDmn3ValueDescription ;
      private String AV42ContagemResultado_OsFsOsFm3 ;
      private String AV97FilterContagemResultado_Baseline3ValueDescription ;
      private String AV188ContagemResultado_Descricao3 ;
      private String AV213FilterContagemResultado_TemPndHmlg3ValueDescription ;
      private String AV122DynamicFiltersSelector4 ;
      private String AV127FilterContagemResultado_StatusDmn4ValueDescription ;
      private String AV125ContagemResultado_OsFsOsFm4 ;
      private String AV134FilterContagemResultado_Baseline4ValueDescription ;
      private String AV189ContagemResultado_Descricao4 ;
      private String AV214FilterContagemResultado_TemPndHmlg4ValueDescription ;
      private String AV139DynamicFiltersSelector5 ;
      private String AV144FilterContagemResultado_StatusDmn5ValueDescription ;
      private String AV142ContagemResultado_OsFsOsFm5 ;
      private String AV151FilterContagemResultado_Baseline5ValueDescription ;
      private String AV190ContagemResultado_Descricao5 ;
      private String AV215FilterContagemResultado_TemPndHmlg5ValueDescription ;
      private String AV235TFContagemResultado_StatusDmn_SelDscs ;
      private String AV245FilterTFContagemResultado_StatusDmn_SelValueDescription ;
      private String AV246FilterTFContagemResultado_Baseline_SelValueDescription ;
      private String AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 ;
      private String AV301ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 ;
      private String AV316ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 ;
      private String AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 ;
      private String AV326ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 ;
      private String AV341ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 ;
      private String AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 ;
      private String AV351ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 ;
      private String AV366ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 ;
      private String AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 ;
      private String AV376ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 ;
      private String AV391ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 ;
      private String AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 ;
      private String AV401ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 ;
      private String AV416ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 ;
      private String AV422ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm ;
      private String AV423ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel ;
      private String AV424ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda ;
      private String AV425ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel ;
      private String AV426ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao ;
      private String AV427ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel ;
      private String AV436ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord ;
      private String AV437ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel ;
      private String lV301ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 ;
      private String lV316ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 ;
      private String lV326ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 ;
      private String lV341ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 ;
      private String lV351ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 ;
      private String lV366ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 ;
      private String lV376ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 ;
      private String lV391ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 ;
      private String lV401ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 ;
      private String lV416ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 ;
      private String lV422ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm ;
      private String lV424ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda ;
      private String lV426ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao ;
      private String lV436ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String A494ContagemResultado_Descricao ;
      private String A515ContagemResultado_SistemaCoord ;
      private String A495ContagemResultado_SistemaNom ;
      private String AV15ContagemResultado_StatusDmnDescription ;
      private String AV89ContagemResultado_BaselineDescription ;
      private IGxSession AV198WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00323_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00323_n1553ContagemResultado_CntSrvCod ;
      private short[] P00323_A1583ContagemResultado_TipoRegistro ;
      private decimal[] P00323_A512ContagemResultado_ValorPF ;
      private bool[] P00323_n512ContagemResultado_ValorPF ;
      private String[] P00323_A801ContagemResultado_ServicoSigla ;
      private bool[] P00323_n801ContagemResultado_ServicoSigla ;
      private String[] P00323_A515ContagemResultado_SistemaCoord ;
      private bool[] P00323_n515ContagemResultado_SistemaCoord ;
      private String[] P00323_A803ContagemResultado_ContratadaSigla ;
      private bool[] P00323_n803ContagemResultado_ContratadaSigla ;
      private int[] P00323_A1603ContagemResultado_CntCod ;
      private bool[] P00323_n1603ContagemResultado_CntCod ;
      private String[] P00323_A494ContagemResultado_Descricao ;
      private bool[] P00323_n494ContagemResultado_Descricao ;
      private String[] P00323_A1046ContagemResultado_Agrupador ;
      private bool[] P00323_n1046ContagemResultado_Agrupador ;
      private int[] P00323_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] P00323_n468ContagemResultado_NaoCnfDmnCod ;
      private bool[] P00323_A598ContagemResultado_Baseline ;
      private bool[] P00323_n598ContagemResultado_Baseline ;
      private int[] P00323_A764ContagemResultado_ServicoGrupo ;
      private bool[] P00323_n764ContagemResultado_ServicoGrupo ;
      private int[] P00323_A489ContagemResultado_SistemaCod ;
      private bool[] P00323_n489ContagemResultado_SistemaCod ;
      private int[] P00323_A508ContagemResultado_Owner ;
      private int[] P00323_A601ContagemResultado_Servico ;
      private bool[] P00323_n601ContagemResultado_Servico ;
      private DateTime[] P00323_A1351ContagemResultado_DataPrevista ;
      private bool[] P00323_n1351ContagemResultado_DataPrevista ;
      private DateTime[] P00323_A471ContagemResultado_DataDmn ;
      private String[] P00323_A493ContagemResultado_DemandaFM ;
      private bool[] P00323_n493ContagemResultado_DemandaFM ;
      private String[] P00323_A457ContagemResultado_Demanda ;
      private bool[] P00323_n457ContagemResultado_Demanda ;
      private String[] P00323_A484ContagemResultado_StatusDmn ;
      private bool[] P00323_n484ContagemResultado_StatusDmn ;
      private decimal[] P00323_A1854ContagemResultado_VlrCnc ;
      private bool[] P00323_n1854ContagemResultado_VlrCnc ;
      private int[] P00323_A490ContagemResultado_ContratadaCod ;
      private bool[] P00323_n490ContagemResultado_ContratadaCod ;
      private int[] P00323_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00323_n52Contratada_AreaTrabalhoCod ;
      private String[] P00323_A1612ContagemResultado_CntNum ;
      private bool[] P00323_n1612ContagemResultado_CntNum ;
      private String[] P00323_A495ContagemResultado_SistemaNom ;
      private bool[] P00323_n495ContagemResultado_SistemaNom ;
      private decimal[] P00323_A683ContagemResultado_PFLFMUltima ;
      private decimal[] P00323_A685ContagemResultado_PFLFSUltima ;
      private decimal[] P00323_A682ContagemResultado_PFBFMUltima ;
      private decimal[] P00323_A684ContagemResultado_PFBFSUltima ;
      private int[] P00323_A584ContagemResultado_ContadorFM ;
      private DateTime[] P00323_A566ContagemResultado_DataUltCnt ;
      private short[] P00323_A531ContagemResultado_StatusUltCnt ;
      private int[] P00323_A456ContagemResultado_Codigo ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV197Contratadas ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV178ServicosGeridos ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV236TFContagemResultado_StatusDmn_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV438ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels ;
      private wwpbaseobjects.SdtWWPGridState AV50GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV51GridStateDynamicFilter ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
   }

   public class aexportreportextrawwcontagemresultadoextraselection__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00323( IGxContext context ,
                                             String A484ContagemResultado_StatusDmn ,
                                             IGxCollection AV438ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             IGxCollection AV197Contratadas ,
                                             int A601ContagemResultado_Servico ,
                                             IGxCollection AV178ServicosGeridos ,
                                             int AV9WWPContext_gxTpr_Contratada_codigo ,
                                             String AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 ,
                                             String AV300ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1 ,
                                             short AV297ExtraWWContagemResultadoExtraSelectionDS_4_Dynamicfiltersoperator1 ,
                                             String AV301ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 ,
                                             DateTime AV302ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1 ,
                                             DateTime AV303ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1 ,
                                             DateTime AV304ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1 ,
                                             DateTime AV305ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1 ,
                                             int AV306ExtraWWContagemResultadoExtraSelectionDS_13_Contagemresultado_servico1 ,
                                             int AV308ExtraWWContagemResultadoExtraSelectionDS_15_Contagemresultado_sistemacod1 ,
                                             int AV309ExtraWWContagemResultadoExtraSelectionDS_16_Contagemresultado_contratadacod1 ,
                                             int AV294ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod ,
                                             int AV310ExtraWWContagemResultadoExtraSelectionDS_17_Contagemresultado_servicogrupo1 ,
                                             String AV311ExtraWWContagemResultadoExtraSelectionDS_18_Contagemresultado_baseline1 ,
                                             int AV312ExtraWWContagemResultadoExtraSelectionDS_19_Contagemresultado_naocnfdmncod1 ,
                                             String AV315ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1 ,
                                             String AV316ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 ,
                                             int AV317ExtraWWContagemResultadoExtraSelectionDS_24_Contagemresultado_owner1 ,
                                             int AV319ExtraWWContagemResultadoExtraSelectionDS_26_Contagemresultado_cntcod1 ,
                                             bool AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 ,
                                             String AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 ,
                                             String AV325ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2 ,
                                             short AV322ExtraWWContagemResultadoExtraSelectionDS_29_Dynamicfiltersoperator2 ,
                                             String AV326ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 ,
                                             DateTime AV327ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2 ,
                                             DateTime AV328ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2 ,
                                             DateTime AV329ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2 ,
                                             DateTime AV330ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2 ,
                                             int AV331ExtraWWContagemResultadoExtraSelectionDS_38_Contagemresultado_servico2 ,
                                             int AV333ExtraWWContagemResultadoExtraSelectionDS_40_Contagemresultado_sistemacod2 ,
                                             int AV334ExtraWWContagemResultadoExtraSelectionDS_41_Contagemresultado_contratadacod2 ,
                                             int AV335ExtraWWContagemResultadoExtraSelectionDS_42_Contagemresultado_servicogrupo2 ,
                                             String AV336ExtraWWContagemResultadoExtraSelectionDS_43_Contagemresultado_baseline2 ,
                                             int AV337ExtraWWContagemResultadoExtraSelectionDS_44_Contagemresultado_naocnfdmncod2 ,
                                             String AV340ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2 ,
                                             String AV341ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 ,
                                             int AV342ExtraWWContagemResultadoExtraSelectionDS_49_Contagemresultado_owner2 ,
                                             int AV344ExtraWWContagemResultadoExtraSelectionDS_51_Contagemresultado_cntcod2 ,
                                             bool AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 ,
                                             String AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 ,
                                             String AV350ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3 ,
                                             short AV347ExtraWWContagemResultadoExtraSelectionDS_54_Dynamicfiltersoperator3 ,
                                             String AV351ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 ,
                                             DateTime AV352ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3 ,
                                             DateTime AV353ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3 ,
                                             DateTime AV354ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3 ,
                                             DateTime AV355ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3 ,
                                             int AV356ExtraWWContagemResultadoExtraSelectionDS_63_Contagemresultado_servico3 ,
                                             int AV358ExtraWWContagemResultadoExtraSelectionDS_65_Contagemresultado_sistemacod3 ,
                                             int AV359ExtraWWContagemResultadoExtraSelectionDS_66_Contagemresultado_contratadacod3 ,
                                             int AV360ExtraWWContagemResultadoExtraSelectionDS_67_Contagemresultado_servicogrupo3 ,
                                             String AV361ExtraWWContagemResultadoExtraSelectionDS_68_Contagemresultado_baseline3 ,
                                             int AV362ExtraWWContagemResultadoExtraSelectionDS_69_Contagemresultado_naocnfdmncod3 ,
                                             String AV365ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3 ,
                                             String AV366ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 ,
                                             int AV367ExtraWWContagemResultadoExtraSelectionDS_74_Contagemresultado_owner3 ,
                                             int AV369ExtraWWContagemResultadoExtraSelectionDS_76_Contagemresultado_cntcod3 ,
                                             bool AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 ,
                                             String AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 ,
                                             String AV375ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4 ,
                                             short AV372ExtraWWContagemResultadoExtraSelectionDS_79_Dynamicfiltersoperator4 ,
                                             String AV376ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 ,
                                             DateTime AV377ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4 ,
                                             DateTime AV378ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4 ,
                                             DateTime AV379ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4 ,
                                             DateTime AV380ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4 ,
                                             int AV381ExtraWWContagemResultadoExtraSelectionDS_88_Contagemresultado_servico4 ,
                                             int AV383ExtraWWContagemResultadoExtraSelectionDS_90_Contagemresultado_sistemacod4 ,
                                             int AV384ExtraWWContagemResultadoExtraSelectionDS_91_Contagemresultado_contratadacod4 ,
                                             int AV385ExtraWWContagemResultadoExtraSelectionDS_92_Contagemresultado_servicogrupo4 ,
                                             String AV386ExtraWWContagemResultadoExtraSelectionDS_93_Contagemresultado_baseline4 ,
                                             int AV387ExtraWWContagemResultadoExtraSelectionDS_94_Contagemresultado_naocnfdmncod4 ,
                                             String AV390ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4 ,
                                             String AV391ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 ,
                                             int AV392ExtraWWContagemResultadoExtraSelectionDS_99_Contagemresultado_owner4 ,
                                             int AV394ExtraWWContagemResultadoExtraSelectionDS_101_Contagemresultado_cntcod4 ,
                                             bool AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 ,
                                             String AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 ,
                                             String AV400ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5 ,
                                             short AV397ExtraWWContagemResultadoExtraSelectionDS_104_Dynamicfiltersoperator5 ,
                                             String AV401ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 ,
                                             DateTime AV402ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5 ,
                                             DateTime AV403ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5 ,
                                             DateTime AV404ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5 ,
                                             DateTime AV405ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5 ,
                                             int AV406ExtraWWContagemResultadoExtraSelectionDS_113_Contagemresultado_servico5 ,
                                             int AV408ExtraWWContagemResultadoExtraSelectionDS_115_Contagemresultado_sistemacod5 ,
                                             int AV409ExtraWWContagemResultadoExtraSelectionDS_116_Contagemresultado_contratadacod5 ,
                                             int AV410ExtraWWContagemResultadoExtraSelectionDS_117_Contagemresultado_servicogrupo5 ,
                                             String AV411ExtraWWContagemResultadoExtraSelectionDS_118_Contagemresultado_baseline5 ,
                                             int AV412ExtraWWContagemResultadoExtraSelectionDS_119_Contagemresultado_naocnfdmncod5 ,
                                             String AV415ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5 ,
                                             String AV416ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 ,
                                             int AV417ExtraWWContagemResultadoExtraSelectionDS_124_Contagemresultado_owner5 ,
                                             int AV419ExtraWWContagemResultadoExtraSelectionDS_126_Contagemresultado_cntcod5 ,
                                             String AV421ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel ,
                                             String AV420ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador ,
                                             String AV423ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel ,
                                             String AV422ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm ,
                                             String AV425ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel ,
                                             String AV424ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda ,
                                             String AV427ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel ,
                                             String AV426ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao ,
                                             DateTime AV428ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista ,
                                             DateTime AV429ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to ,
                                             DateTime AV430ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn ,
                                             DateTime AV431ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to ,
                                             String AV435ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel ,
                                             String AV434ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla ,
                                             String AV437ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel ,
                                             String AV436ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord ,
                                             int AV438ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels_Count ,
                                             short AV439ExtraWWContagemResultadoExtraSelectionDS_146_Tfcontagemresultado_baseline_sel ,
                                             String AV441ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel ,
                                             String AV440ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla ,
                                             decimal AV444ExtraWWContagemResultadoExtraSelectionDS_151_Tfcontagemresultado_valorpf ,
                                             decimal AV445ExtraWWContagemResultadoExtraSelectionDS_152_Tfcontagemresultado_valorpf_to ,
                                             int AV50GridState_gxTpr_Dynamicfilters_Count ,
                                             int AV197Contratadas_Count ,
                                             bool AV9WWPContext_gxTpr_Userehfinanceiro ,
                                             bool AV9WWPContext_gxTpr_Userehgestor ,
                                             String A457ContagemResultado_Demanda ,
                                             String A493ContagemResultado_DemandaFM ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             DateTime A1351ContagemResultado_DataPrevista ,
                                             int A489ContagemResultado_SistemaCod ,
                                             int A764ContagemResultado_ServicoGrupo ,
                                             bool A598ContagemResultado_Baseline ,
                                             int A468ContagemResultado_NaoCnfDmnCod ,
                                             String A1046ContagemResultado_Agrupador ,
                                             String A494ContagemResultado_Descricao ,
                                             int A508ContagemResultado_Owner ,
                                             int A1603ContagemResultado_CntCod ,
                                             String A803ContagemResultado_ContratadaSigla ,
                                             String A515ContagemResultado_SistemaCoord ,
                                             String A801ContagemResultado_ServicoSigla ,
                                             decimal A512ContagemResultado_ValorPF ,
                                             int A456ContagemResultado_Codigo ,
                                             short AV10OrderedBy ,
                                             bool AV11OrderedDsc ,
                                             short A531ContagemResultado_StatusUltCnt ,
                                             decimal A1854ContagemResultado_VlrCnc ,
                                             DateTime AV298ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1 ,
                                             DateTime A566ContagemResultado_DataUltCnt ,
                                             DateTime AV299ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1 ,
                                             int AV307ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1 ,
                                             int A584ContagemResultado_ContadorFM ,
                                             decimal A684ContagemResultado_PFBFSUltima ,
                                             decimal A682ContagemResultado_PFBFMUltima ,
                                             decimal A685ContagemResultado_PFLFSUltima ,
                                             decimal A683ContagemResultado_PFLFMUltima ,
                                             String AV318ExtraWWContagemResultadoExtraSelectionDS_25_Contagemresultado_tempndhmlg1 ,
                                             bool A1802ContagemResultado_TemPndHmlg ,
                                             DateTime AV323ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2 ,
                                             DateTime AV324ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2 ,
                                             int AV332ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2 ,
                                             String AV343ExtraWWContagemResultadoExtraSelectionDS_50_Contagemresultado_tempndhmlg2 ,
                                             DateTime AV348ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3 ,
                                             DateTime AV349ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3 ,
                                             int AV357ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3 ,
                                             String AV368ExtraWWContagemResultadoExtraSelectionDS_75_Contagemresultado_tempndhmlg3 ,
                                             DateTime AV373ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4 ,
                                             DateTime AV374ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4 ,
                                             int AV382ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4 ,
                                             String AV393ExtraWWContagemResultadoExtraSelectionDS_100_Contagemresultado_tempndhmlg4 ,
                                             DateTime AV398ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5 ,
                                             DateTime AV399ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5 ,
                                             int AV407ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5 ,
                                             String AV418ExtraWWContagemResultadoExtraSelectionDS_125_Contagemresultado_tempndhmlg5 ,
                                             DateTime AV432ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt ,
                                             DateTime AV433ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to ,
                                             decimal AV442ExtraWWContagemResultadoExtraSelectionDS_149_Tfcontagemresultado_pffinal ,
                                             decimal A574ContagemResultado_PFFinal ,
                                             decimal AV443ExtraWWContagemResultadoExtraSelectionDS_150_Tfcontagemresultado_pffinal_to ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo ,
                                             short A1583ContagemResultado_TipoRegistro )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [211] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_TipoRegistro], T1.[ContagemResultado_ValorPF], T4.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T5.[Sistema_Coordenacao] AS ContagemResultado_SistemaCoord, T6.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, T2.[Contrato_Codigo] AS ContagemResultado_CntCod, T1.[ContagemResultado_Descricao], T1.[ContagemResultado_Agrupador], T1.[ContagemResultado_NaoCnfDmnCod], T1.[ContagemResultado_Baseline], T4.[ServicoGrupo_Codigo] AS ContagemResultado_ServicoGrupo, T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_Owner], T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_DataPrevista], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_VlrCnc], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T6.[Contratada_AreaTrabalhoCod], T3.[Contrato_Numero] AS ContagemResultado_CntNum, T5.[Sistema_Nome] AS ContagemResultado_SistemaNom, COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) AS ContagemResultado_PFLFMUltima, COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) AS ContagemResultado_PFLFSUltima, COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T7.[ContagemResultado_ContadorFM], 0) AS ContagemResultado_ContadorFM, COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt, COALESCE( T7.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt, T1.[ContagemResultado_Codigo] FROM (((((([ContagemResultado] T1 WITH (NOLOCK)";
         scmdbuf = scmdbuf + " LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [Servico] T4 WITH (NOLOCK) ON T4.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN [Sistema] T5 WITH (NOLOCK) ON T5.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [Contratada] T6 WITH (NOLOCK) ON T6.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_PFLFM]) AS ContagemResultado_PFLFMUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_PFLFS]) AS ContagemResultado_PFLFSUltima, MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM, MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T7 ON T7.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo])";
         scmdbuf = scmdbuf + " WHERE (Not ( Not ( @AV300ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1 = 'N' or @AV325ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2 = 'N' or @AV350ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3 = 'N' or @AV375ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4 = 'N' or @AV400ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5 = 'N')) or ( COALESCE( T7.[ContagemResultado_StatusUltCnt], 0) = 5 or T1.[ContagemResultado_VlrCnc] > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV298ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV298ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1))";
         scmdbuf = scmdbuf + " and (Not ( @AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV299ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV299ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1))";
         scmdbuf = scmdbuf + " and (Not ( @AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV307ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1 = convert(int, 0)))) or ( COALESCE( T7.[ContagemResultado_ContadorFM], 0) = @AV307ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1 or ( (COALESCE( T7.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV307ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1)))";
         scmdbuf = scmdbuf + " and (@AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 <> 'CONTAGEMRESULTADO_PFBFM' or ( COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (@AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 <> 'CONTAGEMRESULTADO_PFBFS' or ( COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 = 1 and @AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV323ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV323ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2))";
         scmdbuf = scmdbuf + " and (Not ( @AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 = 1 and @AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV324ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV324ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2))";
         scmdbuf = scmdbuf + " and (Not ( @AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 = 1 and @AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV332ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2 = convert(int, 0)))) or ( COALESCE( T7.[ContagemResultado_ContadorFM], 0) = @AV332ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2 or ( (COALESCE( T7.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV332ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2)))";
         scmdbuf = scmdbuf + " and (Not ( @AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 = 1 and @AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_PFBFM') or ( COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 = 1 and @AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_PFBFS') or ( COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 = 1 and @AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV348ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV348ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3))";
         scmdbuf = scmdbuf + " and (Not ( @AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 = 1 and @AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV349ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV349ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3))";
         scmdbuf = scmdbuf + " and (Not ( @AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 = 1 and @AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV357ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3 = convert(int, 0)))) or ( COALESCE( T7.[ContagemResultado_ContadorFM], 0) = @AV357ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3 or ( (COALESCE( T7.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV357ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3)))";
         scmdbuf = scmdbuf + " and (Not ( @AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 = 1 and @AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_PFBFM') or ( COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 = 1 and @AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_PFBFS') or ( COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 = 1 and @AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV373ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV373ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4))";
         scmdbuf = scmdbuf + " and (Not ( @AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 = 1 and @AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV374ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV374ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4))";
         scmdbuf = scmdbuf + " and (Not ( @AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 = 1 and @AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV382ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4 = convert(int, 0)))) or ( COALESCE( T7.[ContagemResultado_ContadorFM], 0) = @AV382ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4 or ( (COALESCE( T7.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV382ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4)))";
         scmdbuf = scmdbuf + " and (Not ( @AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 = 1 and @AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_PFBFM') or ( COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 = 1 and @AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_PFBFS') or ( COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 = 1 and @AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV398ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV398ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5))";
         scmdbuf = scmdbuf + " and (Not ( @AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 = 1 and @AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV399ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV399ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5))";
         scmdbuf = scmdbuf + " and (Not ( @AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 = 1 and @AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV407ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5 = convert(int, 0)))) or ( COALESCE( T7.[ContagemResultado_ContadorFM], 0) = @AV407ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5 or ( (COALESCE( T7.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV407ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5)))";
         scmdbuf = scmdbuf + " and (Not ( @AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 = 1 and @AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_PFBFM') or ( COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 = 1 and @AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_PFBFS') or ( COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and ((@AV432ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV432ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt))";
         scmdbuf = scmdbuf + " and ((@AV433ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV433ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to))";
         scmdbuf = scmdbuf + " and (T6.[Contratada_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_TipoRegistro] = 1)";
         if ( AV9WWPContext_gxTpr_Contratada_codigo > 0 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV9WWPCo_2Contratada_codigo)";
         }
         else
         {
            GXv_int3[90] = 1;
         }
         if ( ( StringUtil.StrCmp(AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV300ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1)) && ! ( StringUtil.StrCmp(AV300ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV300ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1)";
         }
         else
         {
            GXv_int3[91] = 1;
         }
         if ( ( StringUtil.StrCmp(AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV300ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'X'))";
         }
         if ( ( StringUtil.StrCmp(AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV297ExtraWWContagemResultadoExtraSelectionDS_4_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV301ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV301ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] = @AV301ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int3[92] = 1;
            GXv_int3[93] = 1;
         }
         if ( ( StringUtil.StrCmp(AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV297ExtraWWContagemResultadoExtraSelectionDS_4_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV301ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV301ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] like @lV301ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int3[94] = 1;
            GXv_int3[95] = 1;
         }
         if ( ( StringUtil.StrCmp(AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV297ExtraWWContagemResultadoExtraSelectionDS_4_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV301ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV301ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] like '%' + @lV301ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int3[96] = 1;
            GXv_int3[97] = 1;
         }
         if ( ( StringUtil.StrCmp(AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV302ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV302ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1)";
         }
         else
         {
            GXv_int3[98] = 1;
         }
         if ( ( StringUtil.StrCmp(AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV303ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV303ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1)";
         }
         else
         {
            GXv_int3[99] = 1;
         }
         if ( ( StringUtil.StrCmp(AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV304ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV304ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1)";
         }
         else
         {
            GXv_int3[100] = 1;
         }
         if ( ( StringUtil.StrCmp(AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV305ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV305ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1))";
         }
         else
         {
            GXv_int3[101] = 1;
         }
         if ( ( StringUtil.StrCmp(AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV306ExtraWWContagemResultadoExtraSelectionDS_13_Contagemresultado_servico1) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV306ExtraWWContagemResultadoExtraSelectionDS_13_Contagemresultado_servico1)";
         }
         else
         {
            GXv_int3[102] = 1;
         }
         if ( ( StringUtil.StrCmp(AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV308ExtraWWContagemResultadoExtraSelectionDS_15_Contagemresultado_sistemacod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV308ExtraWWContagemResultadoExtraSelectionDS_15_Contagemresultado_sistemacod1)";
         }
         else
         {
            GXv_int3[103] = 1;
         }
         if ( ( StringUtil.StrCmp(AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV309ExtraWWContagemResultadoExtraSelectionDS_16_Contagemresultado_contratadacod1 > 0 ) && ( AV294ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV309ExtraWWContagemResultadoExtraSelectionDS_16_Contagemresultado_contratadacod1)";
         }
         else
         {
            GXv_int3[104] = 1;
         }
         if ( ( StringUtil.StrCmp(AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 ) && ( ! (0==AV310ExtraWWContagemResultadoExtraSelectionDS_17_Contagemresultado_servicogrupo1) ) )
         {
            sWhereString = sWhereString + " and (T4.[ServicoGrupo_Codigo] = @AV310ExtraWWContagemResultadoExtraSelectionDS_17_Contagemresultado_servicogrupo1)";
         }
         else
         {
            GXv_int3[105] = 1;
         }
         if ( ( StringUtil.StrCmp(AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV311ExtraWWContagemResultadoExtraSelectionDS_18_Contagemresultado_baseline1, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( ( StringUtil.StrCmp(AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV311ExtraWWContagemResultadoExtraSelectionDS_18_Contagemresultado_baseline1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( ( StringUtil.StrCmp(AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV312ExtraWWContagemResultadoExtraSelectionDS_19_Contagemresultado_naocnfdmncod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV312ExtraWWContagemResultadoExtraSelectionDS_19_Contagemresultado_naocnfdmncod1)";
         }
         else
         {
            GXv_int3[106] = 1;
         }
         if ( ( StringUtil.StrCmp(AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV315ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV315ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1)";
         }
         else
         {
            GXv_int3[107] = 1;
         }
         if ( ( StringUtil.StrCmp(AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV316ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV316ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 + '%')";
         }
         else
         {
            GXv_int3[108] = 1;
         }
         if ( ( StringUtil.StrCmp(AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV317ExtraWWContagemResultadoExtraSelectionDS_24_Contagemresultado_owner1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV317ExtraWWContagemResultadoExtraSelectionDS_24_Contagemresultado_owner1)";
         }
         else
         {
            GXv_int3[109] = 1;
         }
         if ( ( StringUtil.StrCmp(AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CNTCOD") == 0 ) && ( ! (0==AV319ExtraWWContagemResultadoExtraSelectionDS_26_Contagemresultado_cntcod1) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contrato_Codigo] = @AV319ExtraWWContagemResultadoExtraSelectionDS_26_Contagemresultado_cntcod1)";
         }
         else
         {
            GXv_int3[110] = 1;
         }
         if ( AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV325ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2)) && ! ( StringUtil.StrCmp(AV325ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV325ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2)";
         }
         else
         {
            GXv_int3[111] = 1;
         }
         if ( AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV325ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'X'))";
         }
         if ( AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV322ExtraWWContagemResultadoExtraSelectionDS_29_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV326ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV326ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] = @AV326ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int3[112] = 1;
            GXv_int3[113] = 1;
         }
         if ( AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV322ExtraWWContagemResultadoExtraSelectionDS_29_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV326ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV326ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] like @lV326ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int3[114] = 1;
            GXv_int3[115] = 1;
         }
         if ( AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV322ExtraWWContagemResultadoExtraSelectionDS_29_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV326ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV326ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] like '%' + @lV326ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int3[116] = 1;
            GXv_int3[117] = 1;
         }
         if ( AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV327ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV327ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2)";
         }
         else
         {
            GXv_int3[118] = 1;
         }
         if ( AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV328ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV328ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2)";
         }
         else
         {
            GXv_int3[119] = 1;
         }
         if ( AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV329ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV329ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2)";
         }
         else
         {
            GXv_int3[120] = 1;
         }
         if ( AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV330ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV330ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2))";
         }
         else
         {
            GXv_int3[121] = 1;
         }
         if ( AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV331ExtraWWContagemResultadoExtraSelectionDS_38_Contagemresultado_servico2) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV331ExtraWWContagemResultadoExtraSelectionDS_38_Contagemresultado_servico2)";
         }
         else
         {
            GXv_int3[122] = 1;
         }
         if ( AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV333ExtraWWContagemResultadoExtraSelectionDS_40_Contagemresultado_sistemacod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV333ExtraWWContagemResultadoExtraSelectionDS_40_Contagemresultado_sistemacod2)";
         }
         else
         {
            GXv_int3[123] = 1;
         }
         if ( AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV334ExtraWWContagemResultadoExtraSelectionDS_41_Contagemresultado_contratadacod2 > 0 ) && ( AV294ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV334ExtraWWContagemResultadoExtraSelectionDS_41_Contagemresultado_contratadacod2)";
         }
         else
         {
            GXv_int3[124] = 1;
         }
         if ( AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 ) && ( ! (0==AV335ExtraWWContagemResultadoExtraSelectionDS_42_Contagemresultado_servicogrupo2) ) )
         {
            sWhereString = sWhereString + " and (T4.[ServicoGrupo_Codigo] = @AV335ExtraWWContagemResultadoExtraSelectionDS_42_Contagemresultado_servicogrupo2)";
         }
         else
         {
            GXv_int3[125] = 1;
         }
         if ( AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV336ExtraWWContagemResultadoExtraSelectionDS_43_Contagemresultado_baseline2, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV336ExtraWWContagemResultadoExtraSelectionDS_43_Contagemresultado_baseline2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV337ExtraWWContagemResultadoExtraSelectionDS_44_Contagemresultado_naocnfdmncod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV337ExtraWWContagemResultadoExtraSelectionDS_44_Contagemresultado_naocnfdmncod2)";
         }
         else
         {
            GXv_int3[126] = 1;
         }
         if ( AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV340ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV340ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2)";
         }
         else
         {
            GXv_int3[127] = 1;
         }
         if ( AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV341ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV341ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 + '%')";
         }
         else
         {
            GXv_int3[128] = 1;
         }
         if ( AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV342ExtraWWContagemResultadoExtraSelectionDS_49_Contagemresultado_owner2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV342ExtraWWContagemResultadoExtraSelectionDS_49_Contagemresultado_owner2)";
         }
         else
         {
            GXv_int3[129] = 1;
         }
         if ( AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CNTCOD") == 0 ) && ( ! (0==AV344ExtraWWContagemResultadoExtraSelectionDS_51_Contagemresultado_cntcod2) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contrato_Codigo] = @AV344ExtraWWContagemResultadoExtraSelectionDS_51_Contagemresultado_cntcod2)";
         }
         else
         {
            GXv_int3[130] = 1;
         }
         if ( AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV350ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3)) && ! ( StringUtil.StrCmp(AV350ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV350ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3)";
         }
         else
         {
            GXv_int3[131] = 1;
         }
         if ( AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV350ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'X'))";
         }
         if ( AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV347ExtraWWContagemResultadoExtraSelectionDS_54_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV351ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV351ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] = @AV351ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int3[132] = 1;
            GXv_int3[133] = 1;
         }
         if ( AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV347ExtraWWContagemResultadoExtraSelectionDS_54_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV351ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV351ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] like @lV351ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int3[134] = 1;
            GXv_int3[135] = 1;
         }
         if ( AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV347ExtraWWContagemResultadoExtraSelectionDS_54_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV351ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV351ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] like '%' + @lV351ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int3[136] = 1;
            GXv_int3[137] = 1;
         }
         if ( AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV352ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV352ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3)";
         }
         else
         {
            GXv_int3[138] = 1;
         }
         if ( AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV353ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV353ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3)";
         }
         else
         {
            GXv_int3[139] = 1;
         }
         if ( AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV354ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV354ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3)";
         }
         else
         {
            GXv_int3[140] = 1;
         }
         if ( AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV355ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV355ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3))";
         }
         else
         {
            GXv_int3[141] = 1;
         }
         if ( AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV356ExtraWWContagemResultadoExtraSelectionDS_63_Contagemresultado_servico3) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV356ExtraWWContagemResultadoExtraSelectionDS_63_Contagemresultado_servico3)";
         }
         else
         {
            GXv_int3[142] = 1;
         }
         if ( AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV358ExtraWWContagemResultadoExtraSelectionDS_65_Contagemresultado_sistemacod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV358ExtraWWContagemResultadoExtraSelectionDS_65_Contagemresultado_sistemacod3)";
         }
         else
         {
            GXv_int3[143] = 1;
         }
         if ( AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV359ExtraWWContagemResultadoExtraSelectionDS_66_Contagemresultado_contratadacod3 > 0 ) && ( AV294ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV359ExtraWWContagemResultadoExtraSelectionDS_66_Contagemresultado_contratadacod3)";
         }
         else
         {
            GXv_int3[144] = 1;
         }
         if ( AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 ) && ( ! (0==AV360ExtraWWContagemResultadoExtraSelectionDS_67_Contagemresultado_servicogrupo3) ) )
         {
            sWhereString = sWhereString + " and (T4.[ServicoGrupo_Codigo] = @AV360ExtraWWContagemResultadoExtraSelectionDS_67_Contagemresultado_servicogrupo3)";
         }
         else
         {
            GXv_int3[145] = 1;
         }
         if ( AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV361ExtraWWContagemResultadoExtraSelectionDS_68_Contagemresultado_baseline3, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV361ExtraWWContagemResultadoExtraSelectionDS_68_Contagemresultado_baseline3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV362ExtraWWContagemResultadoExtraSelectionDS_69_Contagemresultado_naocnfdmncod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV362ExtraWWContagemResultadoExtraSelectionDS_69_Contagemresultado_naocnfdmncod3)";
         }
         else
         {
            GXv_int3[146] = 1;
         }
         if ( AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV365ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV365ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3)";
         }
         else
         {
            GXv_int3[147] = 1;
         }
         if ( AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV366ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV366ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 + '%')";
         }
         else
         {
            GXv_int3[148] = 1;
         }
         if ( AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV367ExtraWWContagemResultadoExtraSelectionDS_74_Contagemresultado_owner3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV367ExtraWWContagemResultadoExtraSelectionDS_74_Contagemresultado_owner3)";
         }
         else
         {
            GXv_int3[149] = 1;
         }
         if ( AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CNTCOD") == 0 ) && ( ! (0==AV369ExtraWWContagemResultadoExtraSelectionDS_76_Contagemresultado_cntcod3) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contrato_Codigo] = @AV369ExtraWWContagemResultadoExtraSelectionDS_76_Contagemresultado_cntcod3)";
         }
         else
         {
            GXv_int3[150] = 1;
         }
         if ( AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV375ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4)) && ! ( StringUtil.StrCmp(AV375ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV375ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4)";
         }
         else
         {
            GXv_int3[151] = 1;
         }
         if ( AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV375ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'X'))";
         }
         if ( AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV372ExtraWWContagemResultadoExtraSelectionDS_79_Dynamicfiltersoperator4 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV376ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV376ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 or T1.[ContagemResultado_DemandaFM] = @AV376ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4)";
         }
         else
         {
            GXv_int3[152] = 1;
            GXv_int3[153] = 1;
         }
         if ( AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV372ExtraWWContagemResultadoExtraSelectionDS_79_Dynamicfiltersoperator4 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV376ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV376ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 or T1.[ContagemResultado_DemandaFM] like @lV376ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4)";
         }
         else
         {
            GXv_int3[154] = 1;
            GXv_int3[155] = 1;
         }
         if ( AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV372ExtraWWContagemResultadoExtraSelectionDS_79_Dynamicfiltersoperator4 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV376ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV376ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 or T1.[ContagemResultado_DemandaFM] like '%' + @lV376ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4)";
         }
         else
         {
            GXv_int3[156] = 1;
            GXv_int3[157] = 1;
         }
         if ( AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV377ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV377ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4)";
         }
         else
         {
            GXv_int3[158] = 1;
         }
         if ( AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV378ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV378ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4)";
         }
         else
         {
            GXv_int3[159] = 1;
         }
         if ( AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV379ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV379ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4)";
         }
         else
         {
            GXv_int3[160] = 1;
         }
         if ( AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV380ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV380ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4))";
         }
         else
         {
            GXv_int3[161] = 1;
         }
         if ( AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV381ExtraWWContagemResultadoExtraSelectionDS_88_Contagemresultado_servico4) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV381ExtraWWContagemResultadoExtraSelectionDS_88_Contagemresultado_servico4)";
         }
         else
         {
            GXv_int3[162] = 1;
         }
         if ( AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV383ExtraWWContagemResultadoExtraSelectionDS_90_Contagemresultado_sistemacod4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV383ExtraWWContagemResultadoExtraSelectionDS_90_Contagemresultado_sistemacod4)";
         }
         else
         {
            GXv_int3[163] = 1;
         }
         if ( AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV384ExtraWWContagemResultadoExtraSelectionDS_91_Contagemresultado_contratadacod4 > 0 ) && ( AV294ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV384ExtraWWContagemResultadoExtraSelectionDS_91_Contagemresultado_contratadacod4)";
         }
         else
         {
            GXv_int3[164] = 1;
         }
         if ( AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 ) && ( ! (0==AV385ExtraWWContagemResultadoExtraSelectionDS_92_Contagemresultado_servicogrupo4) ) )
         {
            sWhereString = sWhereString + " and (T4.[ServicoGrupo_Codigo] = @AV385ExtraWWContagemResultadoExtraSelectionDS_92_Contagemresultado_servicogrupo4)";
         }
         else
         {
            GXv_int3[165] = 1;
         }
         if ( AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV386ExtraWWContagemResultadoExtraSelectionDS_93_Contagemresultado_baseline4, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV386ExtraWWContagemResultadoExtraSelectionDS_93_Contagemresultado_baseline4, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV387ExtraWWContagemResultadoExtraSelectionDS_94_Contagemresultado_naocnfdmncod4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV387ExtraWWContagemResultadoExtraSelectionDS_94_Contagemresultado_naocnfdmncod4)";
         }
         else
         {
            GXv_int3[166] = 1;
         }
         if ( AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV390ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV390ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4)";
         }
         else
         {
            GXv_int3[167] = 1;
         }
         if ( AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV391ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV391ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 + '%')";
         }
         else
         {
            GXv_int3[168] = 1;
         }
         if ( AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV392ExtraWWContagemResultadoExtraSelectionDS_99_Contagemresultado_owner4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV392ExtraWWContagemResultadoExtraSelectionDS_99_Contagemresultado_owner4)";
         }
         else
         {
            GXv_int3[169] = 1;
         }
         if ( AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_CNTCOD") == 0 ) && ( ! (0==AV394ExtraWWContagemResultadoExtraSelectionDS_101_Contagemresultado_cntcod4) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contrato_Codigo] = @AV394ExtraWWContagemResultadoExtraSelectionDS_101_Contagemresultado_cntcod4)";
         }
         else
         {
            GXv_int3[170] = 1;
         }
         if ( AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV400ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5)) && ! ( StringUtil.StrCmp(AV400ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV400ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5)";
         }
         else
         {
            GXv_int3[171] = 1;
         }
         if ( AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV400ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'X'))";
         }
         if ( AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV397ExtraWWContagemResultadoExtraSelectionDS_104_Dynamicfiltersoperator5 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV401ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV401ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 or T1.[ContagemResultado_DemandaFM] = @AV401ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5)";
         }
         else
         {
            GXv_int3[172] = 1;
            GXv_int3[173] = 1;
         }
         if ( AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV397ExtraWWContagemResultadoExtraSelectionDS_104_Dynamicfiltersoperator5 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV401ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV401ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 or T1.[ContagemResultado_DemandaFM] like @lV401ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5)";
         }
         else
         {
            GXv_int3[174] = 1;
            GXv_int3[175] = 1;
         }
         if ( AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV397ExtraWWContagemResultadoExtraSelectionDS_104_Dynamicfiltersoperator5 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV401ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV401ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 or T1.[ContagemResultado_DemandaFM] like '%' + @lV401ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5)";
         }
         else
         {
            GXv_int3[176] = 1;
            GXv_int3[177] = 1;
         }
         if ( AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV402ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV402ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5)";
         }
         else
         {
            GXv_int3[178] = 1;
         }
         if ( AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV403ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV403ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5)";
         }
         else
         {
            GXv_int3[179] = 1;
         }
         if ( AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV404ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV404ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5)";
         }
         else
         {
            GXv_int3[180] = 1;
         }
         if ( AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV405ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV405ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5))";
         }
         else
         {
            GXv_int3[181] = 1;
         }
         if ( AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV406ExtraWWContagemResultadoExtraSelectionDS_113_Contagemresultado_servico5) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV406ExtraWWContagemResultadoExtraSelectionDS_113_Contagemresultado_servico5)";
         }
         else
         {
            GXv_int3[182] = 1;
         }
         if ( AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV408ExtraWWContagemResultadoExtraSelectionDS_115_Contagemresultado_sistemacod5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV408ExtraWWContagemResultadoExtraSelectionDS_115_Contagemresultado_sistemacod5)";
         }
         else
         {
            GXv_int3[183] = 1;
         }
         if ( AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV409ExtraWWContagemResultadoExtraSelectionDS_116_Contagemresultado_contratadacod5 > 0 ) && ( AV294ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV409ExtraWWContagemResultadoExtraSelectionDS_116_Contagemresultado_contratadacod5)";
         }
         else
         {
            GXv_int3[184] = 1;
         }
         if ( AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 ) && ( ! (0==AV410ExtraWWContagemResultadoExtraSelectionDS_117_Contagemresultado_servicogrupo5) ) )
         {
            sWhereString = sWhereString + " and (T4.[ServicoGrupo_Codigo] = @AV410ExtraWWContagemResultadoExtraSelectionDS_117_Contagemresultado_servicogrupo5)";
         }
         else
         {
            GXv_int3[185] = 1;
         }
         if ( AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV411ExtraWWContagemResultadoExtraSelectionDS_118_Contagemresultado_baseline5, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV411ExtraWWContagemResultadoExtraSelectionDS_118_Contagemresultado_baseline5, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV412ExtraWWContagemResultadoExtraSelectionDS_119_Contagemresultado_naocnfdmncod5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV412ExtraWWContagemResultadoExtraSelectionDS_119_Contagemresultado_naocnfdmncod5)";
         }
         else
         {
            GXv_int3[186] = 1;
         }
         if ( AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV415ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV415ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5)";
         }
         else
         {
            GXv_int3[187] = 1;
         }
         if ( AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV416ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV416ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 + '%')";
         }
         else
         {
            GXv_int3[188] = 1;
         }
         if ( AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV417ExtraWWContagemResultadoExtraSelectionDS_124_Contagemresultado_owner5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV417ExtraWWContagemResultadoExtraSelectionDS_124_Contagemresultado_owner5)";
         }
         else
         {
            GXv_int3[189] = 1;
         }
         if ( AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_CNTCOD") == 0 ) && ( ! (0==AV419ExtraWWContagemResultadoExtraSelectionDS_126_Contagemresultado_cntcod5) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contrato_Codigo] = @AV419ExtraWWContagemResultadoExtraSelectionDS_126_Contagemresultado_cntcod5)";
         }
         else
         {
            GXv_int3[190] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV421ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV420ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like @lV420ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador)";
         }
         else
         {
            GXv_int3[191] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV421ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV421ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel)";
         }
         else
         {
            GXv_int3[192] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV423ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV422ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like @lV422ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm)";
         }
         else
         {
            GXv_int3[193] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV423ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] = @AV423ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel)";
         }
         else
         {
            GXv_int3[194] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV425ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV424ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV424ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda)";
         }
         else
         {
            GXv_int3[195] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV425ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV425ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel)";
         }
         else
         {
            GXv_int3[196] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV427ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV426ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like @lV426ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao)";
         }
         else
         {
            GXv_int3[197] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV427ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] = @AV427ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel)";
         }
         else
         {
            GXv_int3[198] = 1;
         }
         if ( ! (DateTime.MinValue==AV428ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV428ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista)";
         }
         else
         {
            GXv_int3[199] = 1;
         }
         if ( ! (DateTime.MinValue==AV429ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] <= @AV429ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to)";
         }
         else
         {
            GXv_int3[200] = 1;
         }
         if ( ! (DateTime.MinValue==AV430ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV430ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn)";
         }
         else
         {
            GXv_int3[201] = 1;
         }
         if ( ! (DateTime.MinValue==AV431ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV431ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to)";
         }
         else
         {
            GXv_int3[202] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV435ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV434ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Contratada_Sigla] like @lV434ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla)";
         }
         else
         {
            GXv_int3[203] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV435ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel)) )
         {
            sWhereString = sWhereString + " and (T6.[Contratada_Sigla] = @AV435ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel)";
         }
         else
         {
            GXv_int3[204] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV437ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV436ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Sistema_Coordenacao] like @lV436ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord)";
         }
         else
         {
            GXv_int3[205] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV437ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Sistema_Coordenacao] = @AV437ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel)";
         }
         else
         {
            GXv_int3[206] = 1;
         }
         if ( AV438ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV438ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels, "T1.[ContagemResultado_StatusDmn] IN (", ")") + ")";
         }
         if ( AV439ExtraWWContagemResultadoExtraSelectionDS_146_Tfcontagemresultado_baseline_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV439ExtraWWContagemResultadoExtraSelectionDS_146_Tfcontagemresultado_baseline_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 0)";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV441ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV440ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Servico_Sigla] like @lV440ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla)";
         }
         else
         {
            GXv_int3[207] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV441ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Servico_Sigla] = @AV441ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel)";
         }
         else
         {
            GXv_int3[208] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV444ExtraWWContagemResultadoExtraSelectionDS_151_Tfcontagemresultado_valorpf) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ValorPF] >= @AV444ExtraWWContagemResultadoExtraSelectionDS_151_Tfcontagemresultado_valorpf)";
         }
         else
         {
            GXv_int3[209] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV445ExtraWWContagemResultadoExtraSelectionDS_152_Tfcontagemresultado_valorpf_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ValorPF] <= @AV445ExtraWWContagemResultadoExtraSelectionDS_152_Tfcontagemresultado_valorpf_to)";
         }
         else
         {
            GXv_int3[210] = 1;
         }
         if ( AV50GridState_gxTpr_Dynamicfilters_Count < 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Codigo] = 0)";
         }
         if ( AV197Contratadas_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV197Contratadas, "T1.[ContagemResultado_ContratadaCod] IN (", ")") + ")";
         }
         if ( ( AV9WWPContext_gxTpr_Contratada_codigo > 0 ) && ! AV9WWPContext_gxTpr_Userehfinanceiro && AV9WWPContext_gxTpr_Userehgestor )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV178ServicosGeridos, "T2.[Servico_Codigo] IN (", ")") + ")";
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV10OrderedBy == 1 ) && ! AV11OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_DataDmn]";
         }
         else if ( ( AV10OrderedBy == 1 ) && ( AV11OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_DataDmn] DESC";
         }
         else if ( ( AV10OrderedBy == 2 ) && ! AV11OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_DemandaFM]";
         }
         else if ( ( AV10OrderedBy == 2 ) && ( AV11OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_DemandaFM] DESC";
         }
         else if ( ( AV10OrderedBy == 3 ) && ! AV11OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Demanda]";
         }
         else if ( ( AV10OrderedBy == 3 ) && ( AV11OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Demanda] DESC";
         }
         else if ( ( AV10OrderedBy == 4 ) && ! AV11OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Descricao]";
         }
         else if ( ( AV10OrderedBy == 4 ) && ( AV11OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Descricao] DESC";
         }
         else if ( ( AV10OrderedBy == 5 ) && ! AV11OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_DataPrevista]";
         }
         else if ( ( AV10OrderedBy == 5 ) && ( AV11OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_DataPrevista] DESC";
         }
         else if ( ( AV10OrderedBy == 6 ) && ! AV11OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_StatusDmn]";
         }
         else if ( ( AV10OrderedBy == 6 ) && ( AV11OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_StatusDmn] DESC";
         }
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00323(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (int)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (int)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (DateTime)dynConstraints[11] , (DateTime)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (int)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (int)dynConstraints[24] , (int)dynConstraints[25] , (bool)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (short)dynConstraints[29] , (String)dynConstraints[30] , (DateTime)dynConstraints[31] , (DateTime)dynConstraints[32] , (DateTime)dynConstraints[33] , (DateTime)dynConstraints[34] , (int)dynConstraints[35] , (int)dynConstraints[36] , (int)dynConstraints[37] , (int)dynConstraints[38] , (String)dynConstraints[39] , (int)dynConstraints[40] , (String)dynConstraints[41] , (String)dynConstraints[42] , (int)dynConstraints[43] , (int)dynConstraints[44] , (bool)dynConstraints[45] , (String)dynConstraints[46] , (String)dynConstraints[47] , (short)dynConstraints[48] , (String)dynConstraints[49] , (DateTime)dynConstraints[50] , (DateTime)dynConstraints[51] , (DateTime)dynConstraints[52] , (DateTime)dynConstraints[53] , (int)dynConstraints[54] , (int)dynConstraints[55] , (int)dynConstraints[56] , (int)dynConstraints[57] , (String)dynConstraints[58] , (int)dynConstraints[59] , (String)dynConstraints[60] , (String)dynConstraints[61] , (int)dynConstraints[62] , (int)dynConstraints[63] , (bool)dynConstraints[64] , (String)dynConstraints[65] , (String)dynConstraints[66] , (short)dynConstraints[67] , (String)dynConstraints[68] , (DateTime)dynConstraints[69] , (DateTime)dynConstraints[70] , (DateTime)dynConstraints[71] , (DateTime)dynConstraints[72] , (int)dynConstraints[73] , (int)dynConstraints[74] , (int)dynConstraints[75] , (int)dynConstraints[76] , (String)dynConstraints[77] , (int)dynConstraints[78] , (String)dynConstraints[79] , (String)dynConstraints[80] , (int)dynConstraints[81] , (int)dynConstraints[82] , (bool)dynConstraints[83] , (String)dynConstraints[84] , (String)dynConstraints[85] , (short)dynConstraints[86] , (String)dynConstraints[87] , (DateTime)dynConstraints[88] , (DateTime)dynConstraints[89] , (DateTime)dynConstraints[90] , (DateTime)dynConstraints[91] , (int)dynConstraints[92] , (int)dynConstraints[93] , (int)dynConstraints[94] , (int)dynConstraints[95] , (String)dynConstraints[96] , (int)dynConstraints[97] , (String)dynConstraints[98] , (String)dynConstraints[99] , (int)dynConstraints[100] , (int)dynConstraints[101] , (String)dynConstraints[102] , (String)dynConstraints[103] , (String)dynConstraints[104] , (String)dynConstraints[105] , (String)dynConstraints[106] , (String)dynConstraints[107] , (String)dynConstraints[108] , (String)dynConstraints[109] , (DateTime)dynConstraints[110] , (DateTime)dynConstraints[111] , (DateTime)dynConstraints[112] , (DateTime)dynConstraints[113] , (String)dynConstraints[114] , (String)dynConstraints[115] , (String)dynConstraints[116] , (String)dynConstraints[117] , (int)dynConstraints[118] , (short)dynConstraints[119] , (String)dynConstraints[120] , (String)dynConstraints[121] , (decimal)dynConstraints[122] , (decimal)dynConstraints[123] , (int)dynConstraints[124] , (int)dynConstraints[125] , (bool)dynConstraints[126] , (bool)dynConstraints[127] , (String)dynConstraints[128] , (String)dynConstraints[129] , (DateTime)dynConstraints[130] , (DateTime)dynConstraints[131] , (int)dynConstraints[132] , (int)dynConstraints[133] , (bool)dynConstraints[134] , (int)dynConstraints[135] , (String)dynConstraints[136] , (String)dynConstraints[137] , (int)dynConstraints[138] , (int)dynConstraints[139] , (String)dynConstraints[140] , (String)dynConstraints[141] , (String)dynConstraints[142] , (decimal)dynConstraints[143] , (int)dynConstraints[144] , (short)dynConstraints[145] , (bool)dynConstraints[146] , (short)dynConstraints[147] , (decimal)dynConstraints[148] , (DateTime)dynConstraints[149] , (DateTime)dynConstraints[150] , (DateTime)dynConstraints[151] , (int)dynConstraints[152] , (int)dynConstraints[153] , (decimal)dynConstraints[154] , (decimal)dynConstraints[155] , (decimal)dynConstraints[156] , (decimal)dynConstraints[157] , (String)dynConstraints[158] , (bool)dynConstraints[159] , (DateTime)dynConstraints[160] , (DateTime)dynConstraints[161] , (int)dynConstraints[162] , (String)dynConstraints[163] , (DateTime)dynConstraints[164] , (DateTime)dynConstraints[165] , (int)dynConstraints[166] , (String)dynConstraints[167] , (DateTime)dynConstraints[168] , (DateTime)dynConstraints[169] , (int)dynConstraints[170] , (String)dynConstraints[171] , (DateTime)dynConstraints[172] , (DateTime)dynConstraints[173] , (int)dynConstraints[174] , (String)dynConstraints[175] , (DateTime)dynConstraints[176] , (DateTime)dynConstraints[177] , (decimal)dynConstraints[178] , (decimal)dynConstraints[179] , (decimal)dynConstraints[180] , (int)dynConstraints[181] , (int)dynConstraints[182] , (short)dynConstraints[183] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00323 ;
          prmP00323 = new Object[] {
          new Object[] {"@AV300ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV325ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV350ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV375ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4",SqlDbType.Char,1,0} ,
          new Object[] {"@AV400ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5",SqlDbType.Char,1,0} ,
          new Object[] {"@AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV298ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV298ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV299ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV299ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV307ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV307ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV307ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV296ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV323ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV323ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV324ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV324ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV332ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV332ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV332ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV320ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV321ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV348ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV348ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV349ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV349ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV357ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV357ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV357ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV345ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV346ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV373ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV373ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV374ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV374ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV382ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV382ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV382ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV370ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV371ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV398ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV398ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV399ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV399ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV407ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV407ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV407ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV395ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV396ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV432ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV432ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV433ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV433ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9WWPCo_2Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV300ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV301ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV301ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV301ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV301ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV301ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV301ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV302ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV303ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV304ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV305ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV306ExtraWWContagemResultadoExtraSelectionDS_13_Contagemresultado_servico1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV308ExtraWWContagemResultadoExtraSelectionDS_15_Contagemresultado_sistemacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV309ExtraWWContagemResultadoExtraSelectionDS_16_Contagemresultado_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV310ExtraWWContagemResultadoExtraSelectionDS_17_Contagemresultado_servicogrupo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV312ExtraWWContagemResultadoExtraSelectionDS_19_Contagemresultado_naocnfdmncod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV315ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV316ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV317ExtraWWContagemResultadoExtraSelectionDS_24_Contagemresultado_owner1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV319ExtraWWContagemResultadoExtraSelectionDS_26_Contagemresultado_cntcod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV325ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV326ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV326ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV326ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV326ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV326ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV326ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV327ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV328ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV329ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV330ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV331ExtraWWContagemResultadoExtraSelectionDS_38_Contagemresultado_servico2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV333ExtraWWContagemResultadoExtraSelectionDS_40_Contagemresultado_sistemacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV334ExtraWWContagemResultadoExtraSelectionDS_41_Contagemresultado_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV335ExtraWWContagemResultadoExtraSelectionDS_42_Contagemresultado_servicogrupo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV337ExtraWWContagemResultadoExtraSelectionDS_44_Contagemresultado_naocnfdmncod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV340ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV341ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV342ExtraWWContagemResultadoExtraSelectionDS_49_Contagemresultado_owner2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV344ExtraWWContagemResultadoExtraSelectionDS_51_Contagemresultado_cntcod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV350ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV351ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV351ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV351ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV351ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV351ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV351ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV352ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV353ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV354ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV355ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV356ExtraWWContagemResultadoExtraSelectionDS_63_Contagemresultado_servico3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV358ExtraWWContagemResultadoExtraSelectionDS_65_Contagemresultado_sistemacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV359ExtraWWContagemResultadoExtraSelectionDS_66_Contagemresultado_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV360ExtraWWContagemResultadoExtraSelectionDS_67_Contagemresultado_servicogrupo3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV362ExtraWWContagemResultadoExtraSelectionDS_69_Contagemresultado_naocnfdmncod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV365ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV366ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV367ExtraWWContagemResultadoExtraSelectionDS_74_Contagemresultado_owner3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV369ExtraWWContagemResultadoExtraSelectionDS_76_Contagemresultado_cntcod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV375ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4",SqlDbType.Char,1,0} ,
          new Object[] {"@AV376ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV376ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV376ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV376ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV376ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV376ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV377ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV378ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV379ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV380ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV381ExtraWWContagemResultadoExtraSelectionDS_88_Contagemresultado_servico4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV383ExtraWWContagemResultadoExtraSelectionDS_90_Contagemresultado_sistemacod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV384ExtraWWContagemResultadoExtraSelectionDS_91_Contagemresultado_contratadacod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV385ExtraWWContagemResultadoExtraSelectionDS_92_Contagemresultado_servicogrupo4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV387ExtraWWContagemResultadoExtraSelectionDS_94_Contagemresultado_naocnfdmncod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV390ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4",SqlDbType.Char,15,0} ,
          new Object[] {"@lV391ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV392ExtraWWContagemResultadoExtraSelectionDS_99_Contagemresultado_owner4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV394ExtraWWContagemResultadoExtraSelectionDS_101_Contagemresultado_cntcod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV400ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5",SqlDbType.Char,1,0} ,
          new Object[] {"@AV401ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV401ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV401ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV401ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV401ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV401ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV402ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV403ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV404ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV405ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV406ExtraWWContagemResultadoExtraSelectionDS_113_Contagemresultado_servico5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV408ExtraWWContagemResultadoExtraSelectionDS_115_Contagemresultado_sistemacod5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV409ExtraWWContagemResultadoExtraSelectionDS_116_Contagemresultado_contratadacod5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV410ExtraWWContagemResultadoExtraSelectionDS_117_Contagemresultado_servicogrupo5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV412ExtraWWContagemResultadoExtraSelectionDS_119_Contagemresultado_naocnfdmncod5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV415ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5",SqlDbType.Char,15,0} ,
          new Object[] {"@lV416ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV417ExtraWWContagemResultadoExtraSelectionDS_124_Contagemresultado_owner5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV419ExtraWWContagemResultadoExtraSelectionDS_126_Contagemresultado_cntcod5",SqlDbType.Int,6,0} ,
          new Object[] {"@lV420ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador",SqlDbType.Char,15,0} ,
          new Object[] {"@AV421ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV422ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV423ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV424ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV425ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV426ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV427ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV428ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV429ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV430ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV431ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV434ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV435ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV436ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV437ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV440ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV441ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV444ExtraWWContagemResultadoExtraSelectionDS_151_Tfcontagemresultado_valorpf",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV445ExtraWWContagemResultadoExtraSelectionDS_152_Tfcontagemresultado_valorpf_to",SqlDbType.Decimal,18,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00323", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00323,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((short[]) buf[2])[0] = rslt.getShort(2) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((bool[]) buf[19])[0] = rslt.getBool(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((int[]) buf[21])[0] = rslt.getInt(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((int[]) buf[23])[0] = rslt.getInt(13) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(13);
                ((int[]) buf[25])[0] = rslt.getInt(14) ;
                ((int[]) buf[26])[0] = rslt.getInt(15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((DateTime[]) buf[28])[0] = rslt.getGXDateTime(16) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((DateTime[]) buf[30])[0] = rslt.getGXDate(17) ;
                ((String[]) buf[31])[0] = rslt.getVarchar(18) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(18);
                ((String[]) buf[33])[0] = rslt.getVarchar(19) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(19);
                ((String[]) buf[35])[0] = rslt.getString(20, 1) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(20);
                ((decimal[]) buf[37])[0] = rslt.getDecimal(21) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(21);
                ((int[]) buf[39])[0] = rslt.getInt(22) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(22);
                ((int[]) buf[41])[0] = rslt.getInt(23) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(23);
                ((String[]) buf[43])[0] = rslt.getString(24, 20) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(24);
                ((String[]) buf[45])[0] = rslt.getVarchar(25) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(25);
                ((decimal[]) buf[47])[0] = rslt.getDecimal(26) ;
                ((decimal[]) buf[48])[0] = rslt.getDecimal(27) ;
                ((decimal[]) buf[49])[0] = rslt.getDecimal(28) ;
                ((decimal[]) buf[50])[0] = rslt.getDecimal(29) ;
                ((int[]) buf[51])[0] = rslt.getInt(30) ;
                ((DateTime[]) buf[52])[0] = rslt.getGXDate(31) ;
                ((short[]) buf[53])[0] = rslt.getShort(32) ;
                ((int[]) buf[54])[0] = rslt.getInt(33) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[211]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[212]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[213]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[214]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[215]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[216]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[217]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[218]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[219]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[220]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[221]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[222]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[223]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[224]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[225]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[226]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[227]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[228]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[229]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[230]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[231]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[232]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[233]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[234]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[235]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[236]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[237]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[238]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[239]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[240]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[241]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[242]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[243]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[244]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[245]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[246]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[247]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[248]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[249]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[250]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[251]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[252]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[253]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[254]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[255]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[256]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[257]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[258]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[259]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[260]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[261]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[262]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[263]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[264]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[265]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[266]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[267]);
                }
                if ( (short)parms[57] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[268]);
                }
                if ( (short)parms[58] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[269]);
                }
                if ( (short)parms[59] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[270]);
                }
                if ( (short)parms[60] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[271]);
                }
                if ( (short)parms[61] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[272]);
                }
                if ( (short)parms[62] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[273]);
                }
                if ( (short)parms[63] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[274]);
                }
                if ( (short)parms[64] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[275]);
                }
                if ( (short)parms[65] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[276]);
                }
                if ( (short)parms[66] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[277]);
                }
                if ( (short)parms[67] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[278]);
                }
                if ( (short)parms[68] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[279]);
                }
                if ( (short)parms[69] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[280]);
                }
                if ( (short)parms[70] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[281]);
                }
                if ( (short)parms[71] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[282]);
                }
                if ( (short)parms[72] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[283]);
                }
                if ( (short)parms[73] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[284]);
                }
                if ( (short)parms[74] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[285]);
                }
                if ( (short)parms[75] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[286]);
                }
                if ( (short)parms[76] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[287]);
                }
                if ( (short)parms[77] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[288]);
                }
                if ( (short)parms[78] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[289]);
                }
                if ( (short)parms[79] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[290]);
                }
                if ( (short)parms[80] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[291]);
                }
                if ( (short)parms[81] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[292]);
                }
                if ( (short)parms[82] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[293]);
                }
                if ( (short)parms[83] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[294]);
                }
                if ( (short)parms[84] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[295]);
                }
                if ( (short)parms[85] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[296]);
                }
                if ( (short)parms[86] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[297]);
                }
                if ( (short)parms[87] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[298]);
                }
                if ( (short)parms[88] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[299]);
                }
                if ( (short)parms[89] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[300]);
                }
                if ( (short)parms[90] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[301]);
                }
                if ( (short)parms[91] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[302]);
                }
                if ( (short)parms[92] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[303]);
                }
                if ( (short)parms[93] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[304]);
                }
                if ( (short)parms[94] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[305]);
                }
                if ( (short)parms[95] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[306]);
                }
                if ( (short)parms[96] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[307]);
                }
                if ( (short)parms[97] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[308]);
                }
                if ( (short)parms[98] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[309]);
                }
                if ( (short)parms[99] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[310]);
                }
                if ( (short)parms[100] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[311]);
                }
                if ( (short)parms[101] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[312]);
                }
                if ( (short)parms[102] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[313]);
                }
                if ( (short)parms[103] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[314]);
                }
                if ( (short)parms[104] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[315]);
                }
                if ( (short)parms[105] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[316]);
                }
                if ( (short)parms[106] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[317]);
                }
                if ( (short)parms[107] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[318]);
                }
                if ( (short)parms[108] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[319]);
                }
                if ( (short)parms[109] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[320]);
                }
                if ( (short)parms[110] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[321]);
                }
                if ( (short)parms[111] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[322]);
                }
                if ( (short)parms[112] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[323]);
                }
                if ( (short)parms[113] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[324]);
                }
                if ( (short)parms[114] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[325]);
                }
                if ( (short)parms[115] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[326]);
                }
                if ( (short)parms[116] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[327]);
                }
                if ( (short)parms[117] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[328]);
                }
                if ( (short)parms[118] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[329]);
                }
                if ( (short)parms[119] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[330]);
                }
                if ( (short)parms[120] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[331]);
                }
                if ( (short)parms[121] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[332]);
                }
                if ( (short)parms[122] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[333]);
                }
                if ( (short)parms[123] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[334]);
                }
                if ( (short)parms[124] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[335]);
                }
                if ( (short)parms[125] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[336]);
                }
                if ( (short)parms[126] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[337]);
                }
                if ( (short)parms[127] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[338]);
                }
                if ( (short)parms[128] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[339]);
                }
                if ( (short)parms[129] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[340]);
                }
                if ( (short)parms[130] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[341]);
                }
                if ( (short)parms[131] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[342]);
                }
                if ( (short)parms[132] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[343]);
                }
                if ( (short)parms[133] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[344]);
                }
                if ( (short)parms[134] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[345]);
                }
                if ( (short)parms[135] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[346]);
                }
                if ( (short)parms[136] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[347]);
                }
                if ( (short)parms[137] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[348]);
                }
                if ( (short)parms[138] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[349]);
                }
                if ( (short)parms[139] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[350]);
                }
                if ( (short)parms[140] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[351]);
                }
                if ( (short)parms[141] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[352]);
                }
                if ( (short)parms[142] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[353]);
                }
                if ( (short)parms[143] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[354]);
                }
                if ( (short)parms[144] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[355]);
                }
                if ( (short)parms[145] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[356]);
                }
                if ( (short)parms[146] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[357]);
                }
                if ( (short)parms[147] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[358]);
                }
                if ( (short)parms[148] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[359]);
                }
                if ( (short)parms[149] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[360]);
                }
                if ( (short)parms[150] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[361]);
                }
                if ( (short)parms[151] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[362]);
                }
                if ( (short)parms[152] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[363]);
                }
                if ( (short)parms[153] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[364]);
                }
                if ( (short)parms[154] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[365]);
                }
                if ( (short)parms[155] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[366]);
                }
                if ( (short)parms[156] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[367]);
                }
                if ( (short)parms[157] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[368]);
                }
                if ( (short)parms[158] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[369]);
                }
                if ( (short)parms[159] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[370]);
                }
                if ( (short)parms[160] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[371]);
                }
                if ( (short)parms[161] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[372]);
                }
                if ( (short)parms[162] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[373]);
                }
                if ( (short)parms[163] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[374]);
                }
                if ( (short)parms[164] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[375]);
                }
                if ( (short)parms[165] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[376]);
                }
                if ( (short)parms[166] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[377]);
                }
                if ( (short)parms[167] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[378]);
                }
                if ( (short)parms[168] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[379]);
                }
                if ( (short)parms[169] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[380]);
                }
                if ( (short)parms[170] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[381]);
                }
                if ( (short)parms[171] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[382]);
                }
                if ( (short)parms[172] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[383]);
                }
                if ( (short)parms[173] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[384]);
                }
                if ( (short)parms[174] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[385]);
                }
                if ( (short)parms[175] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[386]);
                }
                if ( (short)parms[176] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[387]);
                }
                if ( (short)parms[177] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[388]);
                }
                if ( (short)parms[178] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[389]);
                }
                if ( (short)parms[179] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[390]);
                }
                if ( (short)parms[180] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[391]);
                }
                if ( (short)parms[181] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[392]);
                }
                if ( (short)parms[182] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[393]);
                }
                if ( (short)parms[183] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[394]);
                }
                if ( (short)parms[184] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[395]);
                }
                if ( (short)parms[185] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[396]);
                }
                if ( (short)parms[186] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[397]);
                }
                if ( (short)parms[187] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[398]);
                }
                if ( (short)parms[188] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[399]);
                }
                if ( (short)parms[189] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[400]);
                }
                if ( (short)parms[190] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[401]);
                }
                if ( (short)parms[191] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[402]);
                }
                if ( (short)parms[192] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[403]);
                }
                if ( (short)parms[193] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[404]);
                }
                if ( (short)parms[194] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[405]);
                }
                if ( (short)parms[195] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[406]);
                }
                if ( (short)parms[196] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[407]);
                }
                if ( (short)parms[197] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[408]);
                }
                if ( (short)parms[198] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[409]);
                }
                if ( (short)parms[199] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[410]);
                }
                if ( (short)parms[200] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[411]);
                }
                if ( (short)parms[201] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[412]);
                }
                if ( (short)parms[202] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[413]);
                }
                if ( (short)parms[203] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[414]);
                }
                if ( (short)parms[204] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[415]);
                }
                if ( (short)parms[205] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[416]);
                }
                if ( (short)parms[206] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[417]);
                }
                if ( (short)parms[207] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[418]);
                }
                if ( (short)parms[208] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[419]);
                }
                if ( (short)parms[209] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[420]);
                }
                if ( (short)parms[210] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[421]);
                }
                return;
       }
    }

 }

}
