/*
               File: WWGlosario
        Description:  Glosario
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 18:51:46.86
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwglosario : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwglosario( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwglosario( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_82 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_82_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_82_idx = GetNextPar( );
               edtGlosario_Arquivo_Display = (short)(NumberUtil.Val( GetNextPar( ), "."));
               edtGlosario_Arquivo_Tooltiptext = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_Arquivo_Internalname, "Tooltiptext", edtGlosario_Arquivo_Tooltiptext);
               edtGlosario_Arquivo_Linktarget = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_Arquivo_Internalname, "Linktarget", edtGlosario_Arquivo_Linktarget);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV17Glosario_Termo1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Glosario_Termo1", AV17Glosario_Termo1);
               AV34Descricao1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Descricao1", AV34Descricao1);
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV21Glosario_Termo2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Glosario_Termo2", AV21Glosario_Termo2);
               AV35Descricao2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Descricao2", AV35Descricao2);
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV25Glosario_Termo3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Glosario_Termo3", AV25Glosario_Termo3);
               AV36Descricao3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Descricao3", AV36Descricao3);
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV41TFGlosario_Termo = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFGlosario_Termo", AV41TFGlosario_Termo);
               AV42TFGlosario_Termo_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFGlosario_Termo_Sel", AV42TFGlosario_Termo_Sel);
               AV45TFGlosario_NomeArq = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFGlosario_NomeArq", AV45TFGlosario_NomeArq);
               AV46TFGlosario_NomeArq_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFGlosario_NomeArq_Sel", AV46TFGlosario_NomeArq_Sel);
               edtGlosario_Arquivo_Display = (short)(NumberUtil.Val( GetNextPar( ), "."));
               edtGlosario_Arquivo_Tooltiptext = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_Arquivo_Internalname, "Tooltiptext", edtGlosario_Arquivo_Tooltiptext);
               edtGlosario_Arquivo_Linktarget = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_Arquivo_Internalname, "Linktarget", edtGlosario_Arquivo_Linktarget);
               AV43ddo_Glosario_TermoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_Glosario_TermoTitleControlIdToReplace", AV43ddo_Glosario_TermoTitleControlIdToReplace);
               AV47ddo_Glosario_NomeArqTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_Glosario_NomeArqTitleControlIdToReplace", AV47ddo_Glosario_NomeArqTitleControlIdToReplace);
               AV37Glosario_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Glosario_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37Glosario_AreaTrabalhoCod), 6, 0)));
               AV72Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               A1347Glosario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1347Glosario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1347Glosario_Codigo), 6, 0)));
               A859Glosario_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A859Glosario_Descricao", A859Glosario_Descricao);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17Glosario_Termo1, AV34Descricao1, AV19DynamicFiltersSelector2, AV21Glosario_Termo2, AV35Descricao2, AV23DynamicFiltersSelector3, AV25Glosario_Termo3, AV36Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV41TFGlosario_Termo, AV42TFGlosario_Termo_Sel, AV45TFGlosario_NomeArq, AV46TFGlosario_NomeArq_Sel, AV43ddo_Glosario_TermoTitleControlIdToReplace, AV47ddo_Glosario_NomeArqTitleControlIdToReplace, AV37Glosario_AreaTrabalhoCod, AV72Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1347Glosario_Codigo, A859Glosario_Descricao) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAF82( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTF82( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203118514719");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwglosario.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vGLOSARIO_TERMO1", AV17Glosario_Termo1);
         GxWebStd.gx_hidden_field( context, "GXH_vDESCRICAO1", StringUtil.RTrim( AV34Descricao1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vGLOSARIO_TERMO2", AV21Glosario_Termo2);
         GxWebStd.gx_hidden_field( context, "GXH_vDESCRICAO2", StringUtil.RTrim( AV35Descricao2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vGLOSARIO_TERMO3", AV25Glosario_Termo3);
         GxWebStd.gx_hidden_field( context, "GXH_vDESCRICAO3", StringUtil.RTrim( AV36Descricao3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFGLOSARIO_TERMO", AV41TFGlosario_Termo);
         GxWebStd.gx_hidden_field( context, "GXH_vTFGLOSARIO_TERMO_SEL", AV42TFGlosario_Termo_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFGLOSARIO_NOMEARQ", StringUtil.RTrim( AV45TFGlosario_NomeArq));
         GxWebStd.gx_hidden_field( context, "GXH_vTFGLOSARIO_NOMEARQ_SEL", StringUtil.RTrim( AV46TFGlosario_NomeArq_Sel));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_82", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_82), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV50GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV51GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV48DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV48DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGLOSARIO_TERMOTITLEFILTERDATA", AV40Glosario_TermoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGLOSARIO_TERMOTITLEFILTERDATA", AV40Glosario_TermoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGLOSARIO_NOMEARQTITLEFILTERDATA", AV44Glosario_NomeArqTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGLOSARIO_NOMEARQTITLEFILTERDATA", AV44Glosario_NomeArqTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV72Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "GLOSARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1347Glosario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GLOSARIO_DESCRICAO", A859Glosario_Descricao);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Caption", StringUtil.RTrim( Ddo_glosario_termo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Tooltip", StringUtil.RTrim( Ddo_glosario_termo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Cls", StringUtil.RTrim( Ddo_glosario_termo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Filteredtext_set", StringUtil.RTrim( Ddo_glosario_termo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Selectedvalue_set", StringUtil.RTrim( Ddo_glosario_termo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Dropdownoptionstype", StringUtil.RTrim( Ddo_glosario_termo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_glosario_termo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Includesortasc", StringUtil.BoolToStr( Ddo_glosario_termo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Includesortdsc", StringUtil.BoolToStr( Ddo_glosario_termo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Sortedstatus", StringUtil.RTrim( Ddo_glosario_termo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Includefilter", StringUtil.BoolToStr( Ddo_glosario_termo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Filtertype", StringUtil.RTrim( Ddo_glosario_termo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Filterisrange", StringUtil.BoolToStr( Ddo_glosario_termo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Includedatalist", StringUtil.BoolToStr( Ddo_glosario_termo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Datalisttype", StringUtil.RTrim( Ddo_glosario_termo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Datalistproc", StringUtil.RTrim( Ddo_glosario_termo_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_glosario_termo_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Sortasc", StringUtil.RTrim( Ddo_glosario_termo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Sortdsc", StringUtil.RTrim( Ddo_glosario_termo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Loadingdata", StringUtil.RTrim( Ddo_glosario_termo_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Cleanfilter", StringUtil.RTrim( Ddo_glosario_termo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Noresultsfound", StringUtil.RTrim( Ddo_glosario_termo_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Searchbuttontext", StringUtil.RTrim( Ddo_glosario_termo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_NOMEARQ_Caption", StringUtil.RTrim( Ddo_glosario_nomearq_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_NOMEARQ_Tooltip", StringUtil.RTrim( Ddo_glosario_nomearq_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_NOMEARQ_Cls", StringUtil.RTrim( Ddo_glosario_nomearq_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_NOMEARQ_Filteredtext_set", StringUtil.RTrim( Ddo_glosario_nomearq_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_NOMEARQ_Selectedvalue_set", StringUtil.RTrim( Ddo_glosario_nomearq_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_NOMEARQ_Dropdownoptionstype", StringUtil.RTrim( Ddo_glosario_nomearq_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_NOMEARQ_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_glosario_nomearq_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_NOMEARQ_Includesortasc", StringUtil.BoolToStr( Ddo_glosario_nomearq_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_NOMEARQ_Includesortdsc", StringUtil.BoolToStr( Ddo_glosario_nomearq_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_NOMEARQ_Sortedstatus", StringUtil.RTrim( Ddo_glosario_nomearq_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_NOMEARQ_Includefilter", StringUtil.BoolToStr( Ddo_glosario_nomearq_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_NOMEARQ_Filtertype", StringUtil.RTrim( Ddo_glosario_nomearq_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_NOMEARQ_Filterisrange", StringUtil.BoolToStr( Ddo_glosario_nomearq_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_NOMEARQ_Includedatalist", StringUtil.BoolToStr( Ddo_glosario_nomearq_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_NOMEARQ_Datalisttype", StringUtil.RTrim( Ddo_glosario_nomearq_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_NOMEARQ_Datalistproc", StringUtil.RTrim( Ddo_glosario_nomearq_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_NOMEARQ_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_glosario_nomearq_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_NOMEARQ_Sortasc", StringUtil.RTrim( Ddo_glosario_nomearq_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_NOMEARQ_Sortdsc", StringUtil.RTrim( Ddo_glosario_nomearq_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_NOMEARQ_Loadingdata", StringUtil.RTrim( Ddo_glosario_nomearq_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_NOMEARQ_Cleanfilter", StringUtil.RTrim( Ddo_glosario_nomearq_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_NOMEARQ_Noresultsfound", StringUtil.RTrim( Ddo_glosario_nomearq_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_NOMEARQ_Searchbuttontext", StringUtil.RTrim( Ddo_glosario_nomearq_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GLOSARIO_ARQUIVO_Display", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtGlosario_Arquivo_Display), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GLOSARIO_ARQUIVO_Tooltiptext", StringUtil.RTrim( edtGlosario_Arquivo_Tooltiptext));
         GxWebStd.gx_hidden_field( context, "GLOSARIO_ARQUIVO_Linktarget", StringUtil.RTrim( edtGlosario_Arquivo_Linktarget));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Activeeventkey", StringUtil.RTrim( Ddo_glosario_termo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Filteredtext_get", StringUtil.RTrim( Ddo_glosario_termo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_TERMO_Selectedvalue_get", StringUtil.RTrim( Ddo_glosario_termo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_NOMEARQ_Activeeventkey", StringUtil.RTrim( Ddo_glosario_nomearq_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_NOMEARQ_Filteredtext_get", StringUtil.RTrim( Ddo_glosario_nomearq_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_GLOSARIO_NOMEARQ_Selectedvalue_get", StringUtil.RTrim( Ddo_glosario_nomearq_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEF82( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTF82( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwglosario.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWGlosario" ;
      }

      public override String GetPgmdesc( )
      {
         return " Glosario" ;
      }

      protected void WBF80( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_F82( true) ;
         }
         else
         {
            wb_table1_2_F82( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_F82e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'" + sGXsfl_82_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(92, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,92);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'',false,'" + sGXsfl_82_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(93, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,93);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'" + sGXsfl_82_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfglosario_termo_Internalname, AV41TFGlosario_Termo, StringUtil.RTrim( context.localUtil.Format( AV41TFGlosario_Termo, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,94);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfglosario_termo_Jsonclick, 0, "Attribute", "", "", "", edtavTfglosario_termo_Visible, 1, 0, "text", "", 570, "px", 1, "row", 255, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGlosario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'" + sGXsfl_82_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfglosario_termo_sel_Internalname, AV42TFGlosario_Termo_Sel, StringUtil.RTrim( context.localUtil.Format( AV42TFGlosario_Termo_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,95);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfglosario_termo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfglosario_termo_sel_Visible, 1, 0, "text", "", 570, "px", 1, "row", 255, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGlosario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_82_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfglosario_nomearq_Internalname, StringUtil.RTrim( AV45TFGlosario_NomeArq), StringUtil.RTrim( context.localUtil.Format( AV45TFGlosario_NomeArq, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,96);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfglosario_nomearq_Jsonclick, 0, "Attribute", "", "", "", edtavTfglosario_nomearq_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGlosario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_82_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfglosario_nomearq_sel_Internalname, StringUtil.RTrim( AV46TFGlosario_NomeArq_Sel), StringUtil.RTrim( context.localUtil.Format( AV46TFGlosario_NomeArq_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,97);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfglosario_nomearq_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfglosario_nomearq_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGlosario.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_GLOSARIO_TERMOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'" + sGXsfl_82_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_glosario_termotitlecontrolidtoreplace_Internalname, AV43ddo_Glosario_TermoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,99);\"", 0, edtavDdo_glosario_termotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWGlosario.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_GLOSARIO_NOMEARQContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_82_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_glosario_nomearqtitlecontrolidtoreplace_Internalname, AV47ddo_Glosario_NomeArqTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,101);\"", 0, edtavDdo_glosario_nomearqtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWGlosario.htm");
         }
         wbLoad = true;
      }

      protected void STARTF82( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Glosario", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPF80( ) ;
      }

      protected void WSF82( )
      {
         STARTF82( ) ;
         EVTF82( ) ;
      }

      protected void EVTF82( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11F82 */
                              E11F82 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_GLOSARIO_TERMO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12F82 */
                              E12F82 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_GLOSARIO_NOMEARQ.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13F82 */
                              E13F82 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14F82 */
                              E14F82 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15F82 */
                              E15F82 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16F82 */
                              E16F82 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17F82 */
                              E17F82 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18F82 */
                              E18F82 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19F82 */
                              E19F82 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20F82 */
                              E20F82 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21F82 */
                              E21F82 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22F82 */
                              E22F82 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23F82 */
                              E23F82 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E24F82 */
                              E24F82 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E25F82 */
                              E25F82 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_82_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_82_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_82_idx), 4, 0)), 4, "0");
                              SubsflControlProps_822( ) ;
                              AV28Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV70Update_GXI : context.convertURL( context.PathToRelativeUrl( AV28Update))));
                              AV29Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV71Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV29Delete))));
                              A858Glosario_Termo = cgiGet( edtGlosario_Termo_Internalname);
                              A1342Glosario_Arquivo = cgiGet( edtGlosario_Arquivo_Internalname);
                              n1342Glosario_Arquivo = false;
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A1342Glosario_Arquivo));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E26F82 */
                                    E26F82 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E27F82 */
                                    E27F82 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Glosario_termo1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vGLOSARIO_TERMO1"), AV17Glosario_Termo1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Descricao1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDESCRICAO1"), AV34Descricao1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Glosario_termo2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vGLOSARIO_TERMO2"), AV21Glosario_Termo2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Descricao2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDESCRICAO2"), AV35Descricao2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Glosario_termo3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vGLOSARIO_TERMO3"), AV25Glosario_Termo3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Descricao3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDESCRICAO3"), AV36Descricao3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfglosario_termo Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFGLOSARIO_TERMO"), AV41TFGlosario_Termo) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfglosario_termo_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFGLOSARIO_TERMO_SEL"), AV42TFGlosario_Termo_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfglosario_nomearq Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFGLOSARIO_NOMEARQ"), AV45TFGlosario_NomeArq) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfglosario_nomearq_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFGLOSARIO_NOMEARQ_SEL"), AV46TFGlosario_NomeArq_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEF82( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAF82( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("GLOSARIO_TERMO", "Termo", 0);
            cmbavDynamicfiltersselector1.addItem("DESCRICAO", "Descri��o", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("GLOSARIO_TERMO", "Termo", 0);
            cmbavDynamicfiltersselector2.addItem("DESCRICAO", "Descri��o", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("GLOSARIO_TERMO", "Termo", 0);
            cmbavDynamicfiltersselector3.addItem("DESCRICAO", "Descri��o", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_822( ) ;
         while ( nGXsfl_82_idx <= nRC_GXsfl_82 )
         {
            sendrow_822( ) ;
            nGXsfl_82_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_82_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_82_idx+1));
            sGXsfl_82_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_82_idx), 4, 0)), 4, "0");
            SubsflControlProps_822( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       String AV17Glosario_Termo1 ,
                                       String AV34Descricao1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       String AV21Glosario_Termo2 ,
                                       String AV35Descricao2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       String AV25Glosario_Termo3 ,
                                       String AV36Descricao3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       String AV41TFGlosario_Termo ,
                                       String AV42TFGlosario_Termo_Sel ,
                                       String AV45TFGlosario_NomeArq ,
                                       String AV46TFGlosario_NomeArq_Sel ,
                                       String AV43ddo_Glosario_TermoTitleControlIdToReplace ,
                                       String AV47ddo_Glosario_NomeArqTitleControlIdToReplace ,
                                       int AV37Glosario_AreaTrabalhoCod ,
                                       String AV72Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving ,
                                       int A1347Glosario_Codigo ,
                                       String A859Glosario_Descricao )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFF82( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_GLOSARIO_TERMO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A858Glosario_Termo, ""))));
         GxWebStd.gx_hidden_field( context, "GLOSARIO_TERMO", A858Glosario_Termo);
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFF82( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV72Pgmname = "WWGlosario";
         context.Gx_err = 0;
      }

      protected void RFF82( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 82;
         /* Execute user event: E14F82 */
         E14F82 ();
         nGXsfl_82_idx = 1;
         sGXsfl_82_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_82_idx), 4, 0)), 4, "0");
         SubsflControlProps_822( ) ;
         nGXsfl_82_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_822( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV55WWGlosarioDS_2_Dynamicfiltersselector1 ,
                                                 AV56WWGlosarioDS_3_Glosario_termo1 ,
                                                 AV57WWGlosarioDS_4_Descricao1 ,
                                                 AV58WWGlosarioDS_5_Dynamicfiltersenabled2 ,
                                                 AV59WWGlosarioDS_6_Dynamicfiltersselector2 ,
                                                 AV60WWGlosarioDS_7_Glosario_termo2 ,
                                                 AV61WWGlosarioDS_8_Descricao2 ,
                                                 AV62WWGlosarioDS_9_Dynamicfiltersenabled3 ,
                                                 AV63WWGlosarioDS_10_Dynamicfiltersselector3 ,
                                                 AV64WWGlosarioDS_11_Glosario_termo3 ,
                                                 AV65WWGlosarioDS_12_Descricao3 ,
                                                 AV67WWGlosarioDS_14_Tfglosario_termo_sel ,
                                                 AV66WWGlosarioDS_13_Tfglosario_termo ,
                                                 AV69WWGlosarioDS_16_Tfglosario_nomearq_sel ,
                                                 AV68WWGlosarioDS_15_Tfglosario_nomearq ,
                                                 A858Glosario_Termo ,
                                                 A859Glosario_Descricao ,
                                                 A1343Glosario_NomeArq ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A1346Glosario_AreaTrabalhoCod ,
                                                 AV6WWPContext.gxTpr_Areatrabalho_codigo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                                 TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            lV56WWGlosarioDS_3_Glosario_termo1 = StringUtil.Concat( StringUtil.RTrim( AV56WWGlosarioDS_3_Glosario_termo1), "%", "");
            lV57WWGlosarioDS_4_Descricao1 = StringUtil.PadR( StringUtil.RTrim( AV57WWGlosarioDS_4_Descricao1), 40, "%");
            lV60WWGlosarioDS_7_Glosario_termo2 = StringUtil.Concat( StringUtil.RTrim( AV60WWGlosarioDS_7_Glosario_termo2), "%", "");
            lV61WWGlosarioDS_8_Descricao2 = StringUtil.PadR( StringUtil.RTrim( AV61WWGlosarioDS_8_Descricao2), 40, "%");
            lV64WWGlosarioDS_11_Glosario_termo3 = StringUtil.Concat( StringUtil.RTrim( AV64WWGlosarioDS_11_Glosario_termo3), "%", "");
            lV65WWGlosarioDS_12_Descricao3 = StringUtil.PadR( StringUtil.RTrim( AV65WWGlosarioDS_12_Descricao3), 40, "%");
            lV66WWGlosarioDS_13_Tfglosario_termo = StringUtil.Concat( StringUtil.RTrim( AV66WWGlosarioDS_13_Tfglosario_termo), "%", "");
            lV68WWGlosarioDS_15_Tfglosario_nomearq = StringUtil.PadR( StringUtil.RTrim( AV68WWGlosarioDS_15_Tfglosario_nomearq), 50, "%");
            /* Using cursor H00F82 */
            pr_default.execute(0, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, lV56WWGlosarioDS_3_Glosario_termo1, lV57WWGlosarioDS_4_Descricao1, lV60WWGlosarioDS_7_Glosario_termo2, lV61WWGlosarioDS_8_Descricao2, lV64WWGlosarioDS_11_Glosario_termo3, lV65WWGlosarioDS_12_Descricao3, lV66WWGlosarioDS_13_Tfglosario_termo, AV67WWGlosarioDS_14_Tfglosario_termo_sel, lV68WWGlosarioDS_15_Tfglosario_nomearq, AV69WWGlosarioDS_16_Tfglosario_nomearq_sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_82_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1343Glosario_NomeArq = H00F82_A1343Glosario_NomeArq[0];
               n1343Glosario_NomeArq = H00F82_n1343Glosario_NomeArq[0];
               edtGlosario_Arquivo_Filename = A1343Glosario_NomeArq;
               A1344Glosario_TipoArq = H00F82_A1344Glosario_TipoArq[0];
               n1344Glosario_TipoArq = H00F82_n1344Glosario_TipoArq[0];
               edtGlosario_Arquivo_Filetype = A1344Glosario_TipoArq;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_Arquivo_Internalname, "Filetype", edtGlosario_Arquivo_Filetype);
               A1346Glosario_AreaTrabalhoCod = H00F82_A1346Glosario_AreaTrabalhoCod[0];
               A1347Glosario_Codigo = H00F82_A1347Glosario_Codigo[0];
               A859Glosario_Descricao = H00F82_A859Glosario_Descricao[0];
               A858Glosario_Termo = H00F82_A858Glosario_Termo[0];
               A1342Glosario_Arquivo = H00F82_A1342Glosario_Arquivo[0];
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A1342Glosario_Arquivo));
               n1342Glosario_Arquivo = H00F82_n1342Glosario_Arquivo[0];
               /* Execute user event: E27F82 */
               E27F82 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 82;
            WBF80( ) ;
         }
         nGXsfl_82_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV54WWGlosarioDS_1_Glosario_areatrabalhocod = AV37Glosario_AreaTrabalhoCod;
         AV55WWGlosarioDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV56WWGlosarioDS_3_Glosario_termo1 = AV17Glosario_Termo1;
         AV57WWGlosarioDS_4_Descricao1 = AV34Descricao1;
         AV58WWGlosarioDS_5_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV59WWGlosarioDS_6_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV60WWGlosarioDS_7_Glosario_termo2 = AV21Glosario_Termo2;
         AV61WWGlosarioDS_8_Descricao2 = AV35Descricao2;
         AV62WWGlosarioDS_9_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV63WWGlosarioDS_10_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV64WWGlosarioDS_11_Glosario_termo3 = AV25Glosario_Termo3;
         AV65WWGlosarioDS_12_Descricao3 = AV36Descricao3;
         AV66WWGlosarioDS_13_Tfglosario_termo = AV41TFGlosario_Termo;
         AV67WWGlosarioDS_14_Tfglosario_termo_sel = AV42TFGlosario_Termo_Sel;
         AV68WWGlosarioDS_15_Tfglosario_nomearq = AV45TFGlosario_NomeArq;
         AV69WWGlosarioDS_16_Tfglosario_nomearq_sel = AV46TFGlosario_NomeArq_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV55WWGlosarioDS_2_Dynamicfiltersselector1 ,
                                              AV56WWGlosarioDS_3_Glosario_termo1 ,
                                              AV57WWGlosarioDS_4_Descricao1 ,
                                              AV58WWGlosarioDS_5_Dynamicfiltersenabled2 ,
                                              AV59WWGlosarioDS_6_Dynamicfiltersselector2 ,
                                              AV60WWGlosarioDS_7_Glosario_termo2 ,
                                              AV61WWGlosarioDS_8_Descricao2 ,
                                              AV62WWGlosarioDS_9_Dynamicfiltersenabled3 ,
                                              AV63WWGlosarioDS_10_Dynamicfiltersselector3 ,
                                              AV64WWGlosarioDS_11_Glosario_termo3 ,
                                              AV65WWGlosarioDS_12_Descricao3 ,
                                              AV67WWGlosarioDS_14_Tfglosario_termo_sel ,
                                              AV66WWGlosarioDS_13_Tfglosario_termo ,
                                              AV69WWGlosarioDS_16_Tfglosario_nomearq_sel ,
                                              AV68WWGlosarioDS_15_Tfglosario_nomearq ,
                                              A858Glosario_Termo ,
                                              A859Glosario_Descricao ,
                                              A1343Glosario_NomeArq ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              A1346Glosario_AreaTrabalhoCod ,
                                              AV6WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV56WWGlosarioDS_3_Glosario_termo1 = StringUtil.Concat( StringUtil.RTrim( AV56WWGlosarioDS_3_Glosario_termo1), "%", "");
         lV57WWGlosarioDS_4_Descricao1 = StringUtil.PadR( StringUtil.RTrim( AV57WWGlosarioDS_4_Descricao1), 40, "%");
         lV60WWGlosarioDS_7_Glosario_termo2 = StringUtil.Concat( StringUtil.RTrim( AV60WWGlosarioDS_7_Glosario_termo2), "%", "");
         lV61WWGlosarioDS_8_Descricao2 = StringUtil.PadR( StringUtil.RTrim( AV61WWGlosarioDS_8_Descricao2), 40, "%");
         lV64WWGlosarioDS_11_Glosario_termo3 = StringUtil.Concat( StringUtil.RTrim( AV64WWGlosarioDS_11_Glosario_termo3), "%", "");
         lV65WWGlosarioDS_12_Descricao3 = StringUtil.PadR( StringUtil.RTrim( AV65WWGlosarioDS_12_Descricao3), 40, "%");
         lV66WWGlosarioDS_13_Tfglosario_termo = StringUtil.Concat( StringUtil.RTrim( AV66WWGlosarioDS_13_Tfglosario_termo), "%", "");
         lV68WWGlosarioDS_15_Tfglosario_nomearq = StringUtil.PadR( StringUtil.RTrim( AV68WWGlosarioDS_15_Tfglosario_nomearq), 50, "%");
         /* Using cursor H00F83 */
         pr_default.execute(1, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, lV56WWGlosarioDS_3_Glosario_termo1, lV57WWGlosarioDS_4_Descricao1, lV60WWGlosarioDS_7_Glosario_termo2, lV61WWGlosarioDS_8_Descricao2, lV64WWGlosarioDS_11_Glosario_termo3, lV65WWGlosarioDS_12_Descricao3, lV66WWGlosarioDS_13_Tfglosario_termo, AV67WWGlosarioDS_14_Tfglosario_termo_sel, lV68WWGlosarioDS_15_Tfglosario_nomearq, AV69WWGlosarioDS_16_Tfglosario_nomearq_sel});
         GRID_nRecordCount = H00F83_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV54WWGlosarioDS_1_Glosario_areatrabalhocod = AV37Glosario_AreaTrabalhoCod;
         AV55WWGlosarioDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV56WWGlosarioDS_3_Glosario_termo1 = AV17Glosario_Termo1;
         AV57WWGlosarioDS_4_Descricao1 = AV34Descricao1;
         AV58WWGlosarioDS_5_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV59WWGlosarioDS_6_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV60WWGlosarioDS_7_Glosario_termo2 = AV21Glosario_Termo2;
         AV61WWGlosarioDS_8_Descricao2 = AV35Descricao2;
         AV62WWGlosarioDS_9_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV63WWGlosarioDS_10_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV64WWGlosarioDS_11_Glosario_termo3 = AV25Glosario_Termo3;
         AV65WWGlosarioDS_12_Descricao3 = AV36Descricao3;
         AV66WWGlosarioDS_13_Tfglosario_termo = AV41TFGlosario_Termo;
         AV67WWGlosarioDS_14_Tfglosario_termo_sel = AV42TFGlosario_Termo_Sel;
         AV68WWGlosarioDS_15_Tfglosario_nomearq = AV45TFGlosario_NomeArq;
         AV69WWGlosarioDS_16_Tfglosario_nomearq_sel = AV46TFGlosario_NomeArq_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17Glosario_Termo1, AV34Descricao1, AV19DynamicFiltersSelector2, AV21Glosario_Termo2, AV35Descricao2, AV23DynamicFiltersSelector3, AV25Glosario_Termo3, AV36Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV41TFGlosario_Termo, AV42TFGlosario_Termo_Sel, AV45TFGlosario_NomeArq, AV46TFGlosario_NomeArq_Sel, AV43ddo_Glosario_TermoTitleControlIdToReplace, AV47ddo_Glosario_NomeArqTitleControlIdToReplace, AV37Glosario_AreaTrabalhoCod, AV72Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1347Glosario_Codigo, A859Glosario_Descricao) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV54WWGlosarioDS_1_Glosario_areatrabalhocod = AV37Glosario_AreaTrabalhoCod;
         AV55WWGlosarioDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV56WWGlosarioDS_3_Glosario_termo1 = AV17Glosario_Termo1;
         AV57WWGlosarioDS_4_Descricao1 = AV34Descricao1;
         AV58WWGlosarioDS_5_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV59WWGlosarioDS_6_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV60WWGlosarioDS_7_Glosario_termo2 = AV21Glosario_Termo2;
         AV61WWGlosarioDS_8_Descricao2 = AV35Descricao2;
         AV62WWGlosarioDS_9_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV63WWGlosarioDS_10_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV64WWGlosarioDS_11_Glosario_termo3 = AV25Glosario_Termo3;
         AV65WWGlosarioDS_12_Descricao3 = AV36Descricao3;
         AV66WWGlosarioDS_13_Tfglosario_termo = AV41TFGlosario_Termo;
         AV67WWGlosarioDS_14_Tfglosario_termo_sel = AV42TFGlosario_Termo_Sel;
         AV68WWGlosarioDS_15_Tfglosario_nomearq = AV45TFGlosario_NomeArq;
         AV69WWGlosarioDS_16_Tfglosario_nomearq_sel = AV46TFGlosario_NomeArq_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17Glosario_Termo1, AV34Descricao1, AV19DynamicFiltersSelector2, AV21Glosario_Termo2, AV35Descricao2, AV23DynamicFiltersSelector3, AV25Glosario_Termo3, AV36Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV41TFGlosario_Termo, AV42TFGlosario_Termo_Sel, AV45TFGlosario_NomeArq, AV46TFGlosario_NomeArq_Sel, AV43ddo_Glosario_TermoTitleControlIdToReplace, AV47ddo_Glosario_NomeArqTitleControlIdToReplace, AV37Glosario_AreaTrabalhoCod, AV72Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1347Glosario_Codigo, A859Glosario_Descricao) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV54WWGlosarioDS_1_Glosario_areatrabalhocod = AV37Glosario_AreaTrabalhoCod;
         AV55WWGlosarioDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV56WWGlosarioDS_3_Glosario_termo1 = AV17Glosario_Termo1;
         AV57WWGlosarioDS_4_Descricao1 = AV34Descricao1;
         AV58WWGlosarioDS_5_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV59WWGlosarioDS_6_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV60WWGlosarioDS_7_Glosario_termo2 = AV21Glosario_Termo2;
         AV61WWGlosarioDS_8_Descricao2 = AV35Descricao2;
         AV62WWGlosarioDS_9_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV63WWGlosarioDS_10_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV64WWGlosarioDS_11_Glosario_termo3 = AV25Glosario_Termo3;
         AV65WWGlosarioDS_12_Descricao3 = AV36Descricao3;
         AV66WWGlosarioDS_13_Tfglosario_termo = AV41TFGlosario_Termo;
         AV67WWGlosarioDS_14_Tfglosario_termo_sel = AV42TFGlosario_Termo_Sel;
         AV68WWGlosarioDS_15_Tfglosario_nomearq = AV45TFGlosario_NomeArq;
         AV69WWGlosarioDS_16_Tfglosario_nomearq_sel = AV46TFGlosario_NomeArq_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17Glosario_Termo1, AV34Descricao1, AV19DynamicFiltersSelector2, AV21Glosario_Termo2, AV35Descricao2, AV23DynamicFiltersSelector3, AV25Glosario_Termo3, AV36Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV41TFGlosario_Termo, AV42TFGlosario_Termo_Sel, AV45TFGlosario_NomeArq, AV46TFGlosario_NomeArq_Sel, AV43ddo_Glosario_TermoTitleControlIdToReplace, AV47ddo_Glosario_NomeArqTitleControlIdToReplace, AV37Glosario_AreaTrabalhoCod, AV72Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1347Glosario_Codigo, A859Glosario_Descricao) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV54WWGlosarioDS_1_Glosario_areatrabalhocod = AV37Glosario_AreaTrabalhoCod;
         AV55WWGlosarioDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV56WWGlosarioDS_3_Glosario_termo1 = AV17Glosario_Termo1;
         AV57WWGlosarioDS_4_Descricao1 = AV34Descricao1;
         AV58WWGlosarioDS_5_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV59WWGlosarioDS_6_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV60WWGlosarioDS_7_Glosario_termo2 = AV21Glosario_Termo2;
         AV61WWGlosarioDS_8_Descricao2 = AV35Descricao2;
         AV62WWGlosarioDS_9_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV63WWGlosarioDS_10_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV64WWGlosarioDS_11_Glosario_termo3 = AV25Glosario_Termo3;
         AV65WWGlosarioDS_12_Descricao3 = AV36Descricao3;
         AV66WWGlosarioDS_13_Tfglosario_termo = AV41TFGlosario_Termo;
         AV67WWGlosarioDS_14_Tfglosario_termo_sel = AV42TFGlosario_Termo_Sel;
         AV68WWGlosarioDS_15_Tfglosario_nomearq = AV45TFGlosario_NomeArq;
         AV69WWGlosarioDS_16_Tfglosario_nomearq_sel = AV46TFGlosario_NomeArq_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17Glosario_Termo1, AV34Descricao1, AV19DynamicFiltersSelector2, AV21Glosario_Termo2, AV35Descricao2, AV23DynamicFiltersSelector3, AV25Glosario_Termo3, AV36Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV41TFGlosario_Termo, AV42TFGlosario_Termo_Sel, AV45TFGlosario_NomeArq, AV46TFGlosario_NomeArq_Sel, AV43ddo_Glosario_TermoTitleControlIdToReplace, AV47ddo_Glosario_NomeArqTitleControlIdToReplace, AV37Glosario_AreaTrabalhoCod, AV72Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1347Glosario_Codigo, A859Glosario_Descricao) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV54WWGlosarioDS_1_Glosario_areatrabalhocod = AV37Glosario_AreaTrabalhoCod;
         AV55WWGlosarioDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV56WWGlosarioDS_3_Glosario_termo1 = AV17Glosario_Termo1;
         AV57WWGlosarioDS_4_Descricao1 = AV34Descricao1;
         AV58WWGlosarioDS_5_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV59WWGlosarioDS_6_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV60WWGlosarioDS_7_Glosario_termo2 = AV21Glosario_Termo2;
         AV61WWGlosarioDS_8_Descricao2 = AV35Descricao2;
         AV62WWGlosarioDS_9_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV63WWGlosarioDS_10_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV64WWGlosarioDS_11_Glosario_termo3 = AV25Glosario_Termo3;
         AV65WWGlosarioDS_12_Descricao3 = AV36Descricao3;
         AV66WWGlosarioDS_13_Tfglosario_termo = AV41TFGlosario_Termo;
         AV67WWGlosarioDS_14_Tfglosario_termo_sel = AV42TFGlosario_Termo_Sel;
         AV68WWGlosarioDS_15_Tfglosario_nomearq = AV45TFGlosario_NomeArq;
         AV69WWGlosarioDS_16_Tfglosario_nomearq_sel = AV46TFGlosario_NomeArq_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17Glosario_Termo1, AV34Descricao1, AV19DynamicFiltersSelector2, AV21Glosario_Termo2, AV35Descricao2, AV23DynamicFiltersSelector3, AV25Glosario_Termo3, AV36Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV41TFGlosario_Termo, AV42TFGlosario_Termo_Sel, AV45TFGlosario_NomeArq, AV46TFGlosario_NomeArq_Sel, AV43ddo_Glosario_TermoTitleControlIdToReplace, AV47ddo_Glosario_NomeArqTitleControlIdToReplace, AV37Glosario_AreaTrabalhoCod, AV72Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1347Glosario_Codigo, A859Glosario_Descricao) ;
         }
         return (int)(0) ;
      }

      protected void STRUPF80( )
      {
         /* Before Start, stand alone formulas. */
         AV72Pgmname = "WWGlosario";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E26F82 */
         E26F82 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV48DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vGLOSARIO_TERMOTITLEFILTERDATA"), AV40Glosario_TermoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vGLOSARIO_NOMEARQTITLEFILTERDATA"), AV44Glosario_NomeArqTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavGlosario_areatrabalhocod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavGlosario_areatrabalhocod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vGLOSARIO_AREATRABALHOCOD");
               GX_FocusControl = edtavGlosario_areatrabalhocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV37Glosario_AreaTrabalhoCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Glosario_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37Glosario_AreaTrabalhoCod), 6, 0)));
            }
            else
            {
               AV37Glosario_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtavGlosario_areatrabalhocod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Glosario_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37Glosario_AreaTrabalhoCod), 6, 0)));
            }
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            AV17Glosario_Termo1 = cgiGet( edtavGlosario_termo1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Glosario_Termo1", AV17Glosario_Termo1);
            AV34Descricao1 = cgiGet( edtavDescricao1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Descricao1", AV34Descricao1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            AV21Glosario_Termo2 = cgiGet( edtavGlosario_termo2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Glosario_Termo2", AV21Glosario_Termo2);
            AV35Descricao2 = cgiGet( edtavDescricao2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Descricao2", AV35Descricao2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            AV25Glosario_Termo3 = cgiGet( edtavGlosario_termo3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Glosario_Termo3", AV25Glosario_Termo3);
            AV36Descricao3 = cgiGet( edtavDescricao3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Descricao3", AV36Descricao3);
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            AV41TFGlosario_Termo = cgiGet( edtavTfglosario_termo_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFGlosario_Termo", AV41TFGlosario_Termo);
            AV42TFGlosario_Termo_Sel = cgiGet( edtavTfglosario_termo_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFGlosario_Termo_Sel", AV42TFGlosario_Termo_Sel);
            AV45TFGlosario_NomeArq = cgiGet( edtavTfglosario_nomearq_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFGlosario_NomeArq", AV45TFGlosario_NomeArq);
            AV46TFGlosario_NomeArq_Sel = cgiGet( edtavTfglosario_nomearq_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFGlosario_NomeArq_Sel", AV46TFGlosario_NomeArq_Sel);
            AV43ddo_Glosario_TermoTitleControlIdToReplace = cgiGet( edtavDdo_glosario_termotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_Glosario_TermoTitleControlIdToReplace", AV43ddo_Glosario_TermoTitleControlIdToReplace);
            AV47ddo_Glosario_NomeArqTitleControlIdToReplace = cgiGet( edtavDdo_glosario_nomearqtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_Glosario_NomeArqTitleControlIdToReplace", AV47ddo_Glosario_NomeArqTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_82 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_82"), ",", "."));
            AV50GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV51GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_glosario_termo_Caption = cgiGet( "DDO_GLOSARIO_TERMO_Caption");
            Ddo_glosario_termo_Tooltip = cgiGet( "DDO_GLOSARIO_TERMO_Tooltip");
            Ddo_glosario_termo_Cls = cgiGet( "DDO_GLOSARIO_TERMO_Cls");
            Ddo_glosario_termo_Filteredtext_set = cgiGet( "DDO_GLOSARIO_TERMO_Filteredtext_set");
            Ddo_glosario_termo_Selectedvalue_set = cgiGet( "DDO_GLOSARIO_TERMO_Selectedvalue_set");
            Ddo_glosario_termo_Dropdownoptionstype = cgiGet( "DDO_GLOSARIO_TERMO_Dropdownoptionstype");
            Ddo_glosario_termo_Titlecontrolidtoreplace = cgiGet( "DDO_GLOSARIO_TERMO_Titlecontrolidtoreplace");
            Ddo_glosario_termo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_GLOSARIO_TERMO_Includesortasc"));
            Ddo_glosario_termo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_GLOSARIO_TERMO_Includesortdsc"));
            Ddo_glosario_termo_Sortedstatus = cgiGet( "DDO_GLOSARIO_TERMO_Sortedstatus");
            Ddo_glosario_termo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_GLOSARIO_TERMO_Includefilter"));
            Ddo_glosario_termo_Filtertype = cgiGet( "DDO_GLOSARIO_TERMO_Filtertype");
            Ddo_glosario_termo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_GLOSARIO_TERMO_Filterisrange"));
            Ddo_glosario_termo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_GLOSARIO_TERMO_Includedatalist"));
            Ddo_glosario_termo_Datalisttype = cgiGet( "DDO_GLOSARIO_TERMO_Datalisttype");
            Ddo_glosario_termo_Datalistproc = cgiGet( "DDO_GLOSARIO_TERMO_Datalistproc");
            Ddo_glosario_termo_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_GLOSARIO_TERMO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_glosario_termo_Sortasc = cgiGet( "DDO_GLOSARIO_TERMO_Sortasc");
            Ddo_glosario_termo_Sortdsc = cgiGet( "DDO_GLOSARIO_TERMO_Sortdsc");
            Ddo_glosario_termo_Loadingdata = cgiGet( "DDO_GLOSARIO_TERMO_Loadingdata");
            Ddo_glosario_termo_Cleanfilter = cgiGet( "DDO_GLOSARIO_TERMO_Cleanfilter");
            Ddo_glosario_termo_Noresultsfound = cgiGet( "DDO_GLOSARIO_TERMO_Noresultsfound");
            Ddo_glosario_termo_Searchbuttontext = cgiGet( "DDO_GLOSARIO_TERMO_Searchbuttontext");
            Ddo_glosario_nomearq_Caption = cgiGet( "DDO_GLOSARIO_NOMEARQ_Caption");
            Ddo_glosario_nomearq_Tooltip = cgiGet( "DDO_GLOSARIO_NOMEARQ_Tooltip");
            Ddo_glosario_nomearq_Cls = cgiGet( "DDO_GLOSARIO_NOMEARQ_Cls");
            Ddo_glosario_nomearq_Filteredtext_set = cgiGet( "DDO_GLOSARIO_NOMEARQ_Filteredtext_set");
            Ddo_glosario_nomearq_Selectedvalue_set = cgiGet( "DDO_GLOSARIO_NOMEARQ_Selectedvalue_set");
            Ddo_glosario_nomearq_Dropdownoptionstype = cgiGet( "DDO_GLOSARIO_NOMEARQ_Dropdownoptionstype");
            Ddo_glosario_nomearq_Titlecontrolidtoreplace = cgiGet( "DDO_GLOSARIO_NOMEARQ_Titlecontrolidtoreplace");
            Ddo_glosario_nomearq_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_GLOSARIO_NOMEARQ_Includesortasc"));
            Ddo_glosario_nomearq_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_GLOSARIO_NOMEARQ_Includesortdsc"));
            Ddo_glosario_nomearq_Sortedstatus = cgiGet( "DDO_GLOSARIO_NOMEARQ_Sortedstatus");
            Ddo_glosario_nomearq_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_GLOSARIO_NOMEARQ_Includefilter"));
            Ddo_glosario_nomearq_Filtertype = cgiGet( "DDO_GLOSARIO_NOMEARQ_Filtertype");
            Ddo_glosario_nomearq_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_GLOSARIO_NOMEARQ_Filterisrange"));
            Ddo_glosario_nomearq_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_GLOSARIO_NOMEARQ_Includedatalist"));
            Ddo_glosario_nomearq_Datalisttype = cgiGet( "DDO_GLOSARIO_NOMEARQ_Datalisttype");
            Ddo_glosario_nomearq_Datalistproc = cgiGet( "DDO_GLOSARIO_NOMEARQ_Datalistproc");
            Ddo_glosario_nomearq_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_GLOSARIO_NOMEARQ_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_glosario_nomearq_Sortasc = cgiGet( "DDO_GLOSARIO_NOMEARQ_Sortasc");
            Ddo_glosario_nomearq_Sortdsc = cgiGet( "DDO_GLOSARIO_NOMEARQ_Sortdsc");
            Ddo_glosario_nomearq_Loadingdata = cgiGet( "DDO_GLOSARIO_NOMEARQ_Loadingdata");
            Ddo_glosario_nomearq_Cleanfilter = cgiGet( "DDO_GLOSARIO_NOMEARQ_Cleanfilter");
            Ddo_glosario_nomearq_Noresultsfound = cgiGet( "DDO_GLOSARIO_NOMEARQ_Noresultsfound");
            Ddo_glosario_nomearq_Searchbuttontext = cgiGet( "DDO_GLOSARIO_NOMEARQ_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_glosario_termo_Activeeventkey = cgiGet( "DDO_GLOSARIO_TERMO_Activeeventkey");
            Ddo_glosario_termo_Filteredtext_get = cgiGet( "DDO_GLOSARIO_TERMO_Filteredtext_get");
            Ddo_glosario_termo_Selectedvalue_get = cgiGet( "DDO_GLOSARIO_TERMO_Selectedvalue_get");
            Ddo_glosario_nomearq_Activeeventkey = cgiGet( "DDO_GLOSARIO_NOMEARQ_Activeeventkey");
            Ddo_glosario_nomearq_Filteredtext_get = cgiGet( "DDO_GLOSARIO_NOMEARQ_Filteredtext_get");
            Ddo_glosario_nomearq_Selectedvalue_get = cgiGet( "DDO_GLOSARIO_NOMEARQ_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vGLOSARIO_TERMO1"), AV17Glosario_Termo1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDESCRICAO1"), AV34Descricao1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vGLOSARIO_TERMO2"), AV21Glosario_Termo2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDESCRICAO2"), AV35Descricao2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vGLOSARIO_TERMO3"), AV25Glosario_Termo3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDESCRICAO3"), AV36Descricao3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFGLOSARIO_TERMO"), AV41TFGlosario_Termo) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFGLOSARIO_TERMO_SEL"), AV42TFGlosario_Termo_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFGLOSARIO_NOMEARQ"), AV45TFGlosario_NomeArq) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFGLOSARIO_NOMEARQ_SEL"), AV46TFGlosario_NomeArq_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E26F82 */
         E26F82 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E26F82( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "GLOSARIO_TERMO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "GLOSARIO_TERMO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "GLOSARIO_TERMO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfglosario_termo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfglosario_termo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfglosario_termo_Visible), 5, 0)));
         edtavTfglosario_termo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfglosario_termo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfglosario_termo_sel_Visible), 5, 0)));
         edtavTfglosario_nomearq_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfglosario_nomearq_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfglosario_nomearq_Visible), 5, 0)));
         edtavTfglosario_nomearq_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfglosario_nomearq_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfglosario_nomearq_sel_Visible), 5, 0)));
         Ddo_glosario_termo_Titlecontrolidtoreplace = subGrid_Internalname+"_Glosario_Termo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_glosario_termo_Internalname, "TitleControlIdToReplace", Ddo_glosario_termo_Titlecontrolidtoreplace);
         AV43ddo_Glosario_TermoTitleControlIdToReplace = Ddo_glosario_termo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_Glosario_TermoTitleControlIdToReplace", AV43ddo_Glosario_TermoTitleControlIdToReplace);
         edtavDdo_glosario_termotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_glosario_termotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_glosario_termotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_glosario_nomearq_Titlecontrolidtoreplace = subGrid_Internalname+"_Glosario_NomeArq";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_glosario_nomearq_Internalname, "TitleControlIdToReplace", Ddo_glosario_nomearq_Titlecontrolidtoreplace);
         AV47ddo_Glosario_NomeArqTitleControlIdToReplace = Ddo_glosario_nomearq_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_Glosario_NomeArqTitleControlIdToReplace", AV47ddo_Glosario_NomeArqTitleControlIdToReplace);
         edtavDdo_glosario_nomearqtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_glosario_nomearqtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_glosario_nomearqtitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Glosario";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Termo", 0);
         cmbavOrderedby.addItem("2", "Arquivo", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV48DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV48DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
         edtGlosario_Arquivo_Display = 1;
         edtGlosario_Arquivo_Tooltiptext = "Arquivo anexo";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_Arquivo_Internalname, "Tooltiptext", edtGlosario_Arquivo_Tooltiptext);
         edtGlosario_Arquivo_Linktarget = "_blank";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_Arquivo_Internalname, "Linktarget", edtGlosario_Arquivo_Linktarget);
      }

      protected void E14F82( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV40Glosario_TermoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV44Glosario_NomeArqTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtGlosario_Termo_Titleformat = 2;
         edtGlosario_Termo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Termos", AV43ddo_Glosario_TermoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_Termo_Internalname, "Title", edtGlosario_Termo_Title);
         edtGlosario_NomeArq_Titleformat = 2;
         edtGlosario_NomeArq_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Arquivo", AV47ddo_Glosario_NomeArqTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_NomeArq_Internalname, "Title", edtGlosario_NomeArq_Title);
         AV50GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50GridCurrentPage), 10, 0)));
         AV51GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51GridPageCount), 10, 0)));
         imgInsert_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Visible), 5, 0)));
         edtavUpdate_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUpdate_Visible), 5, 0)));
         edtavDelete_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDelete_Visible), 5, 0)));
         AV54WWGlosarioDS_1_Glosario_areatrabalhocod = AV37Glosario_AreaTrabalhoCod;
         AV55WWGlosarioDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV56WWGlosarioDS_3_Glosario_termo1 = AV17Glosario_Termo1;
         AV57WWGlosarioDS_4_Descricao1 = AV34Descricao1;
         AV58WWGlosarioDS_5_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV59WWGlosarioDS_6_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV60WWGlosarioDS_7_Glosario_termo2 = AV21Glosario_Termo2;
         AV61WWGlosarioDS_8_Descricao2 = AV35Descricao2;
         AV62WWGlosarioDS_9_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV63WWGlosarioDS_10_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV64WWGlosarioDS_11_Glosario_termo3 = AV25Glosario_Termo3;
         AV65WWGlosarioDS_12_Descricao3 = AV36Descricao3;
         AV66WWGlosarioDS_13_Tfglosario_termo = AV41TFGlosario_Termo;
         AV67WWGlosarioDS_14_Tfglosario_termo_sel = AV42TFGlosario_Termo_Sel;
         AV68WWGlosarioDS_15_Tfglosario_nomearq = AV45TFGlosario_NomeArq;
         AV69WWGlosarioDS_16_Tfglosario_nomearq_sel = AV46TFGlosario_NomeArq_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV40Glosario_TermoTitleFilterData", AV40Glosario_TermoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV44Glosario_NomeArqTitleFilterData", AV44Glosario_NomeArqTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11F82( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV49PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV49PageToGo) ;
         }
      }

      protected void E12F82( )
      {
         /* Ddo_glosario_termo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_glosario_termo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_glosario_termo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_glosario_termo_Internalname, "SortedStatus", Ddo_glosario_termo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_glosario_termo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_glosario_termo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_glosario_termo_Internalname, "SortedStatus", Ddo_glosario_termo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_glosario_termo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV41TFGlosario_Termo = Ddo_glosario_termo_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFGlosario_Termo", AV41TFGlosario_Termo);
            AV42TFGlosario_Termo_Sel = Ddo_glosario_termo_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFGlosario_Termo_Sel", AV42TFGlosario_Termo_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13F82( )
      {
         /* Ddo_glosario_nomearq_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_glosario_nomearq_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_glosario_nomearq_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_glosario_nomearq_Internalname, "SortedStatus", Ddo_glosario_nomearq_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_glosario_nomearq_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_glosario_nomearq_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_glosario_nomearq_Internalname, "SortedStatus", Ddo_glosario_nomearq_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_glosario_nomearq_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV45TFGlosario_NomeArq = Ddo_glosario_nomearq_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFGlosario_NomeArq", AV45TFGlosario_NomeArq);
            AV46TFGlosario_NomeArq_Sel = Ddo_glosario_nomearq_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFGlosario_NomeArq_Sel", AV46TFGlosario_NomeArq_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E27F82( )
      {
         /* Grid_Load Routine */
         AV28Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV28Update);
         AV70Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("glosario.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1347Glosario_Codigo);
         AV29Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV29Delete);
         AV71Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("glosario.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A1347Glosario_Codigo);
         edtGlosario_Termo_Link = formatLink("viewglosario.aspx") + "?" + UrlEncode("" +A1347Glosario_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtGlosario_Termo_Tooltiptext = A859Glosario_Descricao;
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 82;
         }
         sendrow_822( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_82_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(82, GridRow);
         }
      }

      protected void E15F82( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E21F82( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E16F82( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17Glosario_Termo1, AV34Descricao1, AV19DynamicFiltersSelector2, AV21Glosario_Termo2, AV35Descricao2, AV23DynamicFiltersSelector3, AV25Glosario_Termo3, AV36Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV41TFGlosario_Termo, AV42TFGlosario_Termo_Sel, AV45TFGlosario_NomeArq, AV46TFGlosario_NomeArq_Sel, AV43ddo_Glosario_TermoTitleControlIdToReplace, AV47ddo_Glosario_NomeArqTitleControlIdToReplace, AV37Glosario_AreaTrabalhoCod, AV72Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1347Glosario_Codigo, A859Glosario_Descricao) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E22F82( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E23F82( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E17F82( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17Glosario_Termo1, AV34Descricao1, AV19DynamicFiltersSelector2, AV21Glosario_Termo2, AV35Descricao2, AV23DynamicFiltersSelector3, AV25Glosario_Termo3, AV36Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV41TFGlosario_Termo, AV42TFGlosario_Termo_Sel, AV45TFGlosario_NomeArq, AV46TFGlosario_NomeArq_Sel, AV43ddo_Glosario_TermoTitleControlIdToReplace, AV47ddo_Glosario_NomeArqTitleControlIdToReplace, AV37Glosario_AreaTrabalhoCod, AV72Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1347Glosario_Codigo, A859Glosario_Descricao) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E24F82( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E18F82( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17Glosario_Termo1, AV34Descricao1, AV19DynamicFiltersSelector2, AV21Glosario_Termo2, AV35Descricao2, AV23DynamicFiltersSelector3, AV25Glosario_Termo3, AV36Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV41TFGlosario_Termo, AV42TFGlosario_Termo_Sel, AV45TFGlosario_NomeArq, AV46TFGlosario_NomeArq_Sel, AV43ddo_Glosario_TermoTitleControlIdToReplace, AV47ddo_Glosario_NomeArqTitleControlIdToReplace, AV37Glosario_AreaTrabalhoCod, AV72Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1347Glosario_Codigo, A859Glosario_Descricao) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E25F82( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E19F82( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
      }

      protected void E20F82( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("glosario.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S182( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_glosario_termo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_glosario_termo_Internalname, "SortedStatus", Ddo_glosario_termo_Sortedstatus);
         Ddo_glosario_nomearq_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_glosario_nomearq_Internalname, "SortedStatus", Ddo_glosario_nomearq_Sortedstatus);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_glosario_termo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_glosario_termo_Internalname, "SortedStatus", Ddo_glosario_termo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_glosario_nomearq_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_glosario_nomearq_Internalname, "SortedStatus", Ddo_glosario_nomearq_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavGlosario_termo1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGlosario_termo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGlosario_termo1_Visible), 5, 0)));
         edtavDescricao1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDescricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDescricao1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "GLOSARIO_TERMO") == 0 )
         {
            edtavGlosario_termo1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGlosario_termo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGlosario_termo1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "DESCRICAO") == 0 )
         {
            edtavDescricao1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDescricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDescricao1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavGlosario_termo2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGlosario_termo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGlosario_termo2_Visible), 5, 0)));
         edtavDescricao2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDescricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDescricao2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "GLOSARIO_TERMO") == 0 )
         {
            edtavGlosario_termo2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGlosario_termo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGlosario_termo2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "DESCRICAO") == 0 )
         {
            edtavDescricao2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDescricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDescricao2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavGlosario_termo3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGlosario_termo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGlosario_termo3_Visible), 5, 0)));
         edtavDescricao3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDescricao3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDescricao3_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "GLOSARIO_TERMO") == 0 )
         {
            edtavGlosario_termo3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGlosario_termo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGlosario_termo3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "DESCRICAO") == 0 )
         {
            edtavDescricao3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDescricao3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDescricao3_Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "GLOSARIO_TERMO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV21Glosario_Termo2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Glosario_Termo2", AV21Glosario_Termo2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "GLOSARIO_TERMO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV25Glosario_Termo3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Glosario_Termo3", AV25Glosario_Termo3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'CLEANFILTERS' Routine */
         AV37Glosario_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Glosario_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37Glosario_AreaTrabalhoCod), 6, 0)));
         AV41TFGlosario_Termo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFGlosario_Termo", AV41TFGlosario_Termo);
         Ddo_glosario_termo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_glosario_termo_Internalname, "FilteredText_set", Ddo_glosario_termo_Filteredtext_set);
         AV42TFGlosario_Termo_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFGlosario_Termo_Sel", AV42TFGlosario_Termo_Sel);
         Ddo_glosario_termo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_glosario_termo_Internalname, "SelectedValue_set", Ddo_glosario_termo_Selectedvalue_set);
         AV45TFGlosario_NomeArq = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFGlosario_NomeArq", AV45TFGlosario_NomeArq);
         Ddo_glosario_nomearq_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_glosario_nomearq_Internalname, "FilteredText_set", Ddo_glosario_nomearq_Filteredtext_set);
         AV46TFGlosario_NomeArq_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFGlosario_NomeArq_Sel", AV46TFGlosario_NomeArq_Sel);
         Ddo_glosario_nomearq_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_glosario_nomearq_Internalname, "SelectedValue_set", Ddo_glosario_nomearq_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "GLOSARIO_TERMO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV17Glosario_Termo1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Glosario_Termo1", AV17Glosario_Termo1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV30Session.Get(AV72Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV72Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV30Session.Get(AV72Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S232( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV73GXV1 = 1;
         while ( AV73GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV73GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "GLOSARIO_AREATRABALHOCOD") == 0 )
            {
               AV37Glosario_AreaTrabalhoCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Glosario_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37Glosario_AreaTrabalhoCod), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFGLOSARIO_TERMO") == 0 )
            {
               AV41TFGlosario_Termo = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFGlosario_Termo", AV41TFGlosario_Termo);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFGlosario_Termo)) )
               {
                  Ddo_glosario_termo_Filteredtext_set = AV41TFGlosario_Termo;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_glosario_termo_Internalname, "FilteredText_set", Ddo_glosario_termo_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFGLOSARIO_TERMO_SEL") == 0 )
            {
               AV42TFGlosario_Termo_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFGlosario_Termo_Sel", AV42TFGlosario_Termo_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFGlosario_Termo_Sel)) )
               {
                  Ddo_glosario_termo_Selectedvalue_set = AV42TFGlosario_Termo_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_glosario_termo_Internalname, "SelectedValue_set", Ddo_glosario_termo_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFGLOSARIO_NOMEARQ") == 0 )
            {
               AV45TFGlosario_NomeArq = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFGlosario_NomeArq", AV45TFGlosario_NomeArq);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TFGlosario_NomeArq)) )
               {
                  Ddo_glosario_nomearq_Filteredtext_set = AV45TFGlosario_NomeArq;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_glosario_nomearq_Internalname, "FilteredText_set", Ddo_glosario_nomearq_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFGLOSARIO_NOMEARQ_SEL") == 0 )
            {
               AV46TFGlosario_NomeArq_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFGlosario_NomeArq_Sel", AV46TFGlosario_NomeArq_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46TFGlosario_NomeArq_Sel)) )
               {
                  Ddo_glosario_nomearq_Selectedvalue_set = AV46TFGlosario_NomeArq_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_glosario_nomearq_Internalname, "SelectedValue_set", Ddo_glosario_nomearq_Selectedvalue_set);
               }
            }
            AV73GXV1 = (int)(AV73GXV1+1);
         }
      }

      protected void S212( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "GLOSARIO_TERMO") == 0 )
            {
               AV17Glosario_Termo1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Glosario_Termo1", AV17Glosario_Termo1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "DESCRICAO") == 0 )
            {
               AV34Descricao1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Descricao1", AV34Descricao1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "GLOSARIO_TERMO") == 0 )
               {
                  AV21Glosario_Termo2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Glosario_Termo2", AV21Glosario_Termo2);
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "DESCRICAO") == 0 )
               {
                  AV35Descricao2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Descricao2", AV35Descricao2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "GLOSARIO_TERMO") == 0 )
                  {
                     AV25Glosario_Termo3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Glosario_Termo3", AV25Glosario_Termo3);
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "DESCRICAO") == 0 )
                  {
                     AV36Descricao3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Descricao3", AV36Descricao3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV30Session.Get(AV72Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! (0==AV37Glosario_AreaTrabalhoCod) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "GLOSARIO_AREATRABALHOCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV37Glosario_AreaTrabalhoCod), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFGlosario_Termo)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFGLOSARIO_TERMO";
            AV11GridStateFilterValue.gxTpr_Value = AV41TFGlosario_Termo;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFGlosario_Termo_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFGLOSARIO_TERMO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV42TFGlosario_Termo_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TFGlosario_NomeArq)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFGLOSARIO_NOMEARQ";
            AV11GridStateFilterValue.gxTpr_Value = AV45TFGlosario_NomeArq;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46TFGlosario_NomeArq_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFGLOSARIO_NOMEARQ_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV46TFGlosario_NomeArq_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV72Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S192( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "GLOSARIO_TERMO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Glosario_Termo1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17Glosario_Termo1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV34Descricao1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV34Descricao1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "GLOSARIO_TERMO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV21Glosario_Termo2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV21Glosario_Termo2;
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV35Descricao2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV35Descricao2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "GLOSARIO_TERMO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Glosario_Termo3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV25Glosario_Termo3;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV36Descricao3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV36Descricao3;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV72Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Glosario";
         AV30Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_F82( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_F82( true) ;
         }
         else
         {
            wb_table2_8_F82( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_F82e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_76_F82( true) ;
         }
         else
         {
            wb_table3_76_F82( false) ;
         }
         return  ;
      }

      protected void wb_table3_76_F82e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_F82e( true) ;
         }
         else
         {
            wb_table1_2_F82e( false) ;
         }
      }

      protected void wb_table3_76_F82( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_79_F82( true) ;
         }
         else
         {
            wb_table4_79_F82( false) ;
         }
         return  ;
      }

      protected void wb_table4_79_F82e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_76_F82e( true) ;
         }
         else
         {
            wb_table3_76_F82e( false) ;
         }
      }

      protected void wb_table4_79_F82( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"82\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtGlosario_Termo_Titleformat == 0 )
               {
                  context.SendWebValue( edtGlosario_Termo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtGlosario_Termo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtGlosario_NomeArq_Titleformat == 0 )
               {
                  context.SendWebValue( edtGlosario_NomeArq_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtGlosario_NomeArq_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A858Glosario_Termo);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtGlosario_Termo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtGlosario_Termo_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtGlosario_Termo_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtGlosario_Termo_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1343Glosario_NomeArq));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtGlosario_NomeArq_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtGlosario_NomeArq_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1342Glosario_Arquivo);
               GridColumn.AddObjectProperty("Linktarget", StringUtil.RTrim( edtGlosario_Arquivo_Linktarget));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtGlosario_Arquivo_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 82 )
         {
            wbEnd = 0;
            nRC_GXsfl_82 = (short)(nGXsfl_82_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_79_F82e( true) ;
         }
         else
         {
            wb_table4_79_F82e( false) ;
         }
      }

      protected void wb_table2_8_F82( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearchCell", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblGlosariotitle_Internalname, "Gloss�rio", "", "", lblGlosariotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWGlosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_13_F82( true) ;
         }
         else
         {
            wb_table5_13_F82( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_F82e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWGlosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_82_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWGlosario.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_82_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWGlosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_23_F82( true) ;
         }
         else
         {
            wb_table6_23_F82( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_F82e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_F82e( true) ;
         }
         else
         {
            wb_table2_8_F82e( false) ;
         }
      }

      protected void wb_table6_23_F82( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWGlosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextglosario_areatrabalhocod_Internalname, "de Trabalho", "", "", lblFiltertextglosario_areatrabalhocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWGlosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'" + sGXsfl_82_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGlosario_areatrabalhocod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV37Glosario_AreaTrabalhoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV37Glosario_AreaTrabalhoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,30);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGlosario_areatrabalhocod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWGlosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_32_F82( true) ;
         }
         else
         {
            wb_table7_32_F82( false) ;
         }
         return  ;
      }

      protected void wb_table7_32_F82e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWGlosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_search_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(82), 2, 0)+","+"null"+");", "Procurar", bttBtn_search_Jsonclick, 5, "Procurar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EREFRESH."+"'", TempTags, "", context.GetButtonType( ), "HLP_WWGlosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_F82e( true) ;
         }
         else
         {
            wb_table6_23_F82e( false) ;
         }
      }

      protected void wb_table7_32_F82( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWGlosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_82_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"", "", true, "HLP_WWGlosario.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWGlosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'" + sGXsfl_82_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGlosario_termo1_Internalname, AV17Glosario_Termo1, StringUtil.RTrim( context.localUtil.Format( AV17Glosario_Termo1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,41);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGlosario_termo1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavGlosario_termo1_Visible, 1, 0, "text", "", 570, "px", 1, "row", 255, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGlosario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_82_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavDescricao1_Internalname, StringUtil.RTrim( AV34Descricao1), StringUtil.RTrim( context.localUtil.Format( AV34Descricao1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDescricao1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavDescricao1_Visible, 1, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGlosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWGlosario.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWGlosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWGlosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_82_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "", true, "HLP_WWGlosario.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWGlosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_82_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGlosario_termo2_Internalname, AV21Glosario_Termo2, StringUtil.RTrim( context.localUtil.Format( AV21Glosario_Termo2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGlosario_termo2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavGlosario_termo2_Visible, 1, 0, "text", "", 570, "px", 1, "row", 255, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGlosario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'" + sGXsfl_82_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavDescricao2_Internalname, StringUtil.RTrim( AV35Descricao2), StringUtil.RTrim( context.localUtil.Format( AV35Descricao2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,55);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDescricao2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavDescricao2_Visible, 1, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGlosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWGlosario.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWGlosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWGlosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'" + sGXsfl_82_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,63);\"", "", true, "HLP_WWGlosario.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWGlosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_82_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGlosario_termo3_Internalname, AV25Glosario_Termo3, StringUtil.RTrim( context.localUtil.Format( AV25Glosario_Termo3, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,67);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGlosario_termo3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavGlosario_termo3_Visible, 1, 0, "text", "", 570, "px", 1, "row", 255, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGlosario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'" + sGXsfl_82_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavDescricao3_Internalname, StringUtil.RTrim( AV36Descricao3), StringUtil.RTrim( context.localUtil.Format( AV36Descricao3, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,68);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDescricao3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavDescricao3_Visible, 1, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGlosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWGlosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_32_F82e( true) ;
         }
         else
         {
            wb_table7_32_F82e( false) ;
         }
      }

      protected void wb_table5_13_F82( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgInsert_Visible, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWGlosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_F82e( true) ;
         }
         else
         {
            wb_table5_13_F82e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAF82( ) ;
         WSF82( ) ;
         WEF82( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203118515146");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwglosario.js", "?20203118515146");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_822( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_82_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_82_idx;
         edtGlosario_Termo_Internalname = "GLOSARIO_TERMO_"+sGXsfl_82_idx;
         edtGlosario_NomeArq_Internalname = "GLOSARIO_NOMEARQ_"+sGXsfl_82_idx;
         edtGlosario_Arquivo_Internalname = "GLOSARIO_ARQUIVO_"+sGXsfl_82_idx;
      }

      protected void SubsflControlProps_fel_822( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_82_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_82_fel_idx;
         edtGlosario_Termo_Internalname = "GLOSARIO_TERMO_"+sGXsfl_82_fel_idx;
         edtGlosario_NomeArq_Internalname = "GLOSARIO_NOMEARQ_"+sGXsfl_82_fel_idx;
         edtGlosario_Arquivo_Internalname = "GLOSARIO_ARQUIVO_"+sGXsfl_82_fel_idx;
      }

      protected void sendrow_822( )
      {
         SubsflControlProps_822( ) ;
         WBF80( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_82_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_82_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_82_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV28Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV70Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV70Update_GXI : context.PathToRelativeUrl( AV28Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavUpdate_Visible,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV28Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV29Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV71Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV71Delete_GXI : context.PathToRelativeUrl( AV29Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDelete_Visible,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV29Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtGlosario_Termo_Internalname,(String)A858Glosario_Termo,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtGlosario_Termo_Link,(String)"",(String)edtGlosario_Termo_Tooltiptext,(String)"",(String)edtGlosario_Termo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(short)255,(short)0,(short)0,(short)82,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtGlosario_NomeArq_Internalname,StringUtil.RTrim( A1343Glosario_NomeArq),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtGlosario_NomeArq_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)82,(short)1,(short)-1,(short)-1,(bool)true,(String)"NomeArq",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            ClassString = "Image";
            StyleString = "";
            edtGlosario_Arquivo_Filename = A1343Glosario_NomeArq;
            edtGlosario_Arquivo_Filetype = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_Arquivo_Internalname, "Filetype", edtGlosario_Arquivo_Filetype);
            edtGlosario_Arquivo_Filetype = A1344Glosario_TipoArq;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_Arquivo_Internalname, "Filetype", edtGlosario_Arquivo_Filetype);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1342Glosario_Arquivo)) )
            {
               gxblobfileaux.Source = A1342Glosario_Arquivo;
               if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtGlosario_Arquivo_Filetype, "tmp") != 0 ) )
               {
                  gxblobfileaux.SetExtension(StringUtil.Trim( edtGlosario_Arquivo_Filetype));
               }
               if ( gxblobfileaux.ErrCode == 0 )
               {
                  A1342Glosario_Arquivo = gxblobfileaux.GetAbsoluteName();
                  n1342Glosario_Arquivo = false;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A1342Glosario_Arquivo));
                  edtGlosario_Arquivo_Filetype = gxblobfileaux.GetExtension();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_Arquivo_Internalname, "Filetype", edtGlosario_Arquivo_Filetype);
               }
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A1342Glosario_Arquivo));
            }
            GridRow.AddColumnProperties("blob", 2, isAjaxCallMode( ), new Object[] {(String)edtGlosario_Arquivo_Internalname,StringUtil.RTrim( A1342Glosario_Arquivo),context.PathToRelativeUrl( A1342Glosario_Arquivo),(String.IsNullOrEmpty(StringUtil.RTrim( edtGlosario_Arquivo_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtGlosario_Arquivo_Filetype)) ? A1342Glosario_Arquivo : edtGlosario_Arquivo_Filetype)) : edtGlosario_Arquivo_Contenttype),(bool)true,(String)edtGlosario_Arquivo_Linktarget,(String)edtGlosario_Arquivo_Parameters,(short)edtGlosario_Arquivo_Display,(short)0,(short)-1,(String)"",(String)edtGlosario_Arquivo_Tooltiptext,(short)0,(short)-1,(short)0,(String)"px",(short)60,(String)"px",(short)0,(short)0,(short)0,(String)edtGlosario_Arquivo_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)StyleString,(String)ClassString,(String)"",(String)""+"",(String)"",(String)""});
            GxWebStd.gx_hidden_field( context, "gxhash_GLOSARIO_TERMO"+"_"+sGXsfl_82_idx, GetSecureSignedToken( sGXsfl_82_idx, StringUtil.RTrim( context.localUtil.Format( A858Glosario_Termo, ""))));
            GridContainer.AddRow(GridRow);
            nGXsfl_82_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_82_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_82_idx+1));
            sGXsfl_82_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_82_idx), 4, 0)), 4, "0");
            SubsflControlProps_822( ) ;
         }
         /* End function sendrow_822 */
      }

      protected void init_default_properties( )
      {
         lblGlosariotitle_Internalname = "GLOSARIOTITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblFiltertextglosario_areatrabalhocod_Internalname = "FILTERTEXTGLOSARIO_AREATRABALHOCOD";
         edtavGlosario_areatrabalhocod_Internalname = "vGLOSARIO_AREATRABALHOCOD";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavGlosario_termo1_Internalname = "vGLOSARIO_TERMO1";
         edtavDescricao1_Internalname = "vDESCRICAO1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavGlosario_termo2_Internalname = "vGLOSARIO_TERMO2";
         edtavDescricao2_Internalname = "vDESCRICAO2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavGlosario_termo3_Internalname = "vGLOSARIO_TERMO3";
         edtavDescricao3_Internalname = "vDESCRICAO3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         bttBtn_search_Internalname = "BTN_SEARCH";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtGlosario_Termo_Internalname = "GLOSARIO_TERMO";
         edtGlosario_NomeArq_Internalname = "GLOSARIO_NOMEARQ";
         edtGlosario_Arquivo_Internalname = "GLOSARIO_ARQUIVO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfglosario_termo_Internalname = "vTFGLOSARIO_TERMO";
         edtavTfglosario_termo_sel_Internalname = "vTFGLOSARIO_TERMO_SEL";
         edtavTfglosario_nomearq_Internalname = "vTFGLOSARIO_NOMEARQ";
         edtavTfglosario_nomearq_sel_Internalname = "vTFGLOSARIO_NOMEARQ_SEL";
         Ddo_glosario_termo_Internalname = "DDO_GLOSARIO_TERMO";
         edtavDdo_glosario_termotitlecontrolidtoreplace_Internalname = "vDDO_GLOSARIO_TERMOTITLECONTROLIDTOREPLACE";
         Ddo_glosario_nomearq_Internalname = "DDO_GLOSARIO_NOMEARQ";
         edtavDdo_glosario_nomearqtitlecontrolidtoreplace_Internalname = "vDDO_GLOSARIO_NOMEARQTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtGlosario_Arquivo_Jsonclick = "";
         edtGlosario_Arquivo_Parameters = "";
         edtGlosario_Arquivo_Contenttype = "";
         edtGlosario_NomeArq_Jsonclick = "";
         edtGlosario_Termo_Jsonclick = "";
         imgInsert_Visible = 1;
         edtavDescricao3_Jsonclick = "";
         edtavGlosario_termo3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         edtavDescricao2_Jsonclick = "";
         edtavGlosario_termo2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         edtavDescricao1_Jsonclick = "";
         edtavGlosario_termo1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavGlosario_areatrabalhocod_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtGlosario_Termo_Tooltiptext = "";
         edtGlosario_Termo_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtGlosario_NomeArq_Titleformat = 0;
         edtGlosario_Termo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavDescricao3_Visible = 1;
         edtavGlosario_termo3_Visible = 1;
         edtavDescricao2_Visible = 1;
         edtavGlosario_termo2_Visible = 1;
         edtavDescricao1_Visible = 1;
         edtavGlosario_termo1_Visible = 1;
         edtavDelete_Visible = -1;
         edtavUpdate_Visible = -1;
         edtGlosario_NomeArq_Title = "Arquivo";
         edtGlosario_Termo_Title = "Termos";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         edtGlosario_Arquivo_Filetype = "";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_glosario_nomearqtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_glosario_termotitlecontrolidtoreplace_Visible = 1;
         edtavTfglosario_nomearq_sel_Jsonclick = "";
         edtavTfglosario_nomearq_sel_Visible = 1;
         edtavTfglosario_nomearq_Jsonclick = "";
         edtavTfglosario_nomearq_Visible = 1;
         edtavTfglosario_termo_sel_Jsonclick = "";
         edtavTfglosario_termo_sel_Visible = 1;
         edtavTfglosario_termo_Jsonclick = "";
         edtavTfglosario_termo_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         edtGlosario_Arquivo_Display = 0;
         Ddo_glosario_nomearq_Searchbuttontext = "Pesquisar";
         Ddo_glosario_nomearq_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_glosario_nomearq_Cleanfilter = "Limpar pesquisa";
         Ddo_glosario_nomearq_Loadingdata = "Carregando dados...";
         Ddo_glosario_nomearq_Sortdsc = "Ordenar de Z � A";
         Ddo_glosario_nomearq_Sortasc = "Ordenar de A � Z";
         Ddo_glosario_nomearq_Datalistupdateminimumcharacters = 0;
         Ddo_glosario_nomearq_Datalistproc = "GetWWGlosarioFilterData";
         Ddo_glosario_nomearq_Datalisttype = "Dynamic";
         Ddo_glosario_nomearq_Includedatalist = Convert.ToBoolean( -1);
         Ddo_glosario_nomearq_Filterisrange = Convert.ToBoolean( 0);
         Ddo_glosario_nomearq_Filtertype = "Character";
         Ddo_glosario_nomearq_Includefilter = Convert.ToBoolean( -1);
         Ddo_glosario_nomearq_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_glosario_nomearq_Includesortasc = Convert.ToBoolean( -1);
         Ddo_glosario_nomearq_Titlecontrolidtoreplace = "";
         Ddo_glosario_nomearq_Dropdownoptionstype = "GridTitleSettings";
         Ddo_glosario_nomearq_Cls = "ColumnSettings";
         Ddo_glosario_nomearq_Tooltip = "Op��es";
         Ddo_glosario_nomearq_Caption = "";
         Ddo_glosario_termo_Searchbuttontext = "Pesquisar";
         Ddo_glosario_termo_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_glosario_termo_Cleanfilter = "Limpar pesquisa";
         Ddo_glosario_termo_Loadingdata = "Carregando dados...";
         Ddo_glosario_termo_Sortdsc = "Ordenar de Z � A";
         Ddo_glosario_termo_Sortasc = "Ordenar de A � Z";
         Ddo_glosario_termo_Datalistupdateminimumcharacters = 0;
         Ddo_glosario_termo_Datalistproc = "GetWWGlosarioFilterData";
         Ddo_glosario_termo_Datalisttype = "Dynamic";
         Ddo_glosario_termo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_glosario_termo_Filterisrange = Convert.ToBoolean( 0);
         Ddo_glosario_termo_Filtertype = "Character";
         Ddo_glosario_termo_Includefilter = Convert.ToBoolean( -1);
         Ddo_glosario_termo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_glosario_termo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_glosario_termo_Titlecontrolidtoreplace = "";
         Ddo_glosario_termo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_glosario_termo_Cls = "ColumnSettings";
         Ddo_glosario_termo_Tooltip = "Op��es";
         Ddo_glosario_termo_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Glosario";
         subGrid_Rows = 0;
         edtGlosario_Arquivo_Linktarget = "";
         edtGlosario_Arquivo_Tooltiptext = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'edtGlosario_Arquivo_Display',ctrl:'GLOSARIO_ARQUIVO',prop:'Display'},{av:'edtGlosario_Arquivo_Tooltiptext',ctrl:'GLOSARIO_ARQUIVO',prop:'Tooltiptext'},{av:'edtGlosario_Arquivo_Linktarget',ctrl:'GLOSARIO_ARQUIVO',prop:'Linktarget'},{av:'A1347Glosario_Codigo',fld:'GLOSARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A859Glosario_Descricao',fld:'GLOSARIO_DESCRICAO',pic:'',nv:''},{av:'AV43ddo_Glosario_TermoTitleControlIdToReplace',fld:'vDDO_GLOSARIO_TERMOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Glosario_NomeArqTitleControlIdToReplace',fld:'vDDO_GLOSARIO_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37Glosario_AreaTrabalhoCod',fld:'vGLOSARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Glosario_Termo1',fld:'vGLOSARIO_TERMO1',pic:'',nv:''},{av:'AV34Descricao1',fld:'vDESCRICAO1',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Glosario_Termo2',fld:'vGLOSARIO_TERMO2',pic:'',nv:''},{av:'AV35Descricao2',fld:'vDESCRICAO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25Glosario_Termo3',fld:'vGLOSARIO_TERMO3',pic:'',nv:''},{av:'AV36Descricao3',fld:'vDESCRICAO3',pic:'',nv:''},{av:'AV41TFGlosario_Termo',fld:'vTFGLOSARIO_TERMO',pic:'',nv:''},{av:'AV42TFGlosario_Termo_Sel',fld:'vTFGLOSARIO_TERMO_SEL',pic:'',nv:''},{av:'AV45TFGlosario_NomeArq',fld:'vTFGLOSARIO_NOMEARQ',pic:'',nv:''},{av:'AV46TFGlosario_NomeArq_Sel',fld:'vTFGLOSARIO_NOMEARQ_SEL',pic:'',nv:''},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV40Glosario_TermoTitleFilterData',fld:'vGLOSARIO_TERMOTITLEFILTERDATA',pic:'',nv:null},{av:'AV44Glosario_NomeArqTitleFilterData',fld:'vGLOSARIO_NOMEARQTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtGlosario_Termo_Titleformat',ctrl:'GLOSARIO_TERMO',prop:'Titleformat'},{av:'edtGlosario_Termo_Title',ctrl:'GLOSARIO_TERMO',prop:'Title'},{av:'edtGlosario_NomeArq_Titleformat',ctrl:'GLOSARIO_NOMEARQ',prop:'Titleformat'},{av:'edtGlosario_NomeArq_Title',ctrl:'GLOSARIO_NOMEARQ',prop:'Title'},{av:'AV50GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV51GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11F82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Glosario_Termo1',fld:'vGLOSARIO_TERMO1',pic:'',nv:''},{av:'AV34Descricao1',fld:'vDESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Glosario_Termo2',fld:'vGLOSARIO_TERMO2',pic:'',nv:''},{av:'AV35Descricao2',fld:'vDESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25Glosario_Termo3',fld:'vGLOSARIO_TERMO3',pic:'',nv:''},{av:'AV36Descricao3',fld:'vDESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV41TFGlosario_Termo',fld:'vTFGLOSARIO_TERMO',pic:'',nv:''},{av:'AV42TFGlosario_Termo_Sel',fld:'vTFGLOSARIO_TERMO_SEL',pic:'',nv:''},{av:'AV45TFGlosario_NomeArq',fld:'vTFGLOSARIO_NOMEARQ',pic:'',nv:''},{av:'AV46TFGlosario_NomeArq_Sel',fld:'vTFGLOSARIO_NOMEARQ_SEL',pic:'',nv:''},{av:'edtGlosario_Arquivo_Display',ctrl:'GLOSARIO_ARQUIVO',prop:'Display'},{av:'edtGlosario_Arquivo_Tooltiptext',ctrl:'GLOSARIO_ARQUIVO',prop:'Tooltiptext'},{av:'edtGlosario_Arquivo_Linktarget',ctrl:'GLOSARIO_ARQUIVO',prop:'Linktarget'},{av:'AV43ddo_Glosario_TermoTitleControlIdToReplace',fld:'vDDO_GLOSARIO_TERMOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Glosario_NomeArqTitleControlIdToReplace',fld:'vDDO_GLOSARIO_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37Glosario_AreaTrabalhoCod',fld:'vGLOSARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1347Glosario_Codigo',fld:'GLOSARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A859Glosario_Descricao',fld:'GLOSARIO_DESCRICAO',pic:'',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_GLOSARIO_TERMO.ONOPTIONCLICKED","{handler:'E12F82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Glosario_Termo1',fld:'vGLOSARIO_TERMO1',pic:'',nv:''},{av:'AV34Descricao1',fld:'vDESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Glosario_Termo2',fld:'vGLOSARIO_TERMO2',pic:'',nv:''},{av:'AV35Descricao2',fld:'vDESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25Glosario_Termo3',fld:'vGLOSARIO_TERMO3',pic:'',nv:''},{av:'AV36Descricao3',fld:'vDESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV41TFGlosario_Termo',fld:'vTFGLOSARIO_TERMO',pic:'',nv:''},{av:'AV42TFGlosario_Termo_Sel',fld:'vTFGLOSARIO_TERMO_SEL',pic:'',nv:''},{av:'AV45TFGlosario_NomeArq',fld:'vTFGLOSARIO_NOMEARQ',pic:'',nv:''},{av:'AV46TFGlosario_NomeArq_Sel',fld:'vTFGLOSARIO_NOMEARQ_SEL',pic:'',nv:''},{av:'edtGlosario_Arquivo_Display',ctrl:'GLOSARIO_ARQUIVO',prop:'Display'},{av:'edtGlosario_Arquivo_Tooltiptext',ctrl:'GLOSARIO_ARQUIVO',prop:'Tooltiptext'},{av:'edtGlosario_Arquivo_Linktarget',ctrl:'GLOSARIO_ARQUIVO',prop:'Linktarget'},{av:'AV43ddo_Glosario_TermoTitleControlIdToReplace',fld:'vDDO_GLOSARIO_TERMOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Glosario_NomeArqTitleControlIdToReplace',fld:'vDDO_GLOSARIO_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37Glosario_AreaTrabalhoCod',fld:'vGLOSARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1347Glosario_Codigo',fld:'GLOSARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A859Glosario_Descricao',fld:'GLOSARIO_DESCRICAO',pic:'',nv:''},{av:'Ddo_glosario_termo_Activeeventkey',ctrl:'DDO_GLOSARIO_TERMO',prop:'ActiveEventKey'},{av:'Ddo_glosario_termo_Filteredtext_get',ctrl:'DDO_GLOSARIO_TERMO',prop:'FilteredText_get'},{av:'Ddo_glosario_termo_Selectedvalue_get',ctrl:'DDO_GLOSARIO_TERMO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_glosario_termo_Sortedstatus',ctrl:'DDO_GLOSARIO_TERMO',prop:'SortedStatus'},{av:'AV41TFGlosario_Termo',fld:'vTFGLOSARIO_TERMO',pic:'',nv:''},{av:'AV42TFGlosario_Termo_Sel',fld:'vTFGLOSARIO_TERMO_SEL',pic:'',nv:''},{av:'Ddo_glosario_nomearq_Sortedstatus',ctrl:'DDO_GLOSARIO_NOMEARQ',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_GLOSARIO_NOMEARQ.ONOPTIONCLICKED","{handler:'E13F82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Glosario_Termo1',fld:'vGLOSARIO_TERMO1',pic:'',nv:''},{av:'AV34Descricao1',fld:'vDESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Glosario_Termo2',fld:'vGLOSARIO_TERMO2',pic:'',nv:''},{av:'AV35Descricao2',fld:'vDESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25Glosario_Termo3',fld:'vGLOSARIO_TERMO3',pic:'',nv:''},{av:'AV36Descricao3',fld:'vDESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV41TFGlosario_Termo',fld:'vTFGLOSARIO_TERMO',pic:'',nv:''},{av:'AV42TFGlosario_Termo_Sel',fld:'vTFGLOSARIO_TERMO_SEL',pic:'',nv:''},{av:'AV45TFGlosario_NomeArq',fld:'vTFGLOSARIO_NOMEARQ',pic:'',nv:''},{av:'AV46TFGlosario_NomeArq_Sel',fld:'vTFGLOSARIO_NOMEARQ_SEL',pic:'',nv:''},{av:'edtGlosario_Arquivo_Display',ctrl:'GLOSARIO_ARQUIVO',prop:'Display'},{av:'edtGlosario_Arquivo_Tooltiptext',ctrl:'GLOSARIO_ARQUIVO',prop:'Tooltiptext'},{av:'edtGlosario_Arquivo_Linktarget',ctrl:'GLOSARIO_ARQUIVO',prop:'Linktarget'},{av:'AV43ddo_Glosario_TermoTitleControlIdToReplace',fld:'vDDO_GLOSARIO_TERMOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Glosario_NomeArqTitleControlIdToReplace',fld:'vDDO_GLOSARIO_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37Glosario_AreaTrabalhoCod',fld:'vGLOSARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1347Glosario_Codigo',fld:'GLOSARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A859Glosario_Descricao',fld:'GLOSARIO_DESCRICAO',pic:'',nv:''},{av:'Ddo_glosario_nomearq_Activeeventkey',ctrl:'DDO_GLOSARIO_NOMEARQ',prop:'ActiveEventKey'},{av:'Ddo_glosario_nomearq_Filteredtext_get',ctrl:'DDO_GLOSARIO_NOMEARQ',prop:'FilteredText_get'},{av:'Ddo_glosario_nomearq_Selectedvalue_get',ctrl:'DDO_GLOSARIO_NOMEARQ',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_glosario_nomearq_Sortedstatus',ctrl:'DDO_GLOSARIO_NOMEARQ',prop:'SortedStatus'},{av:'AV45TFGlosario_NomeArq',fld:'vTFGLOSARIO_NOMEARQ',pic:'',nv:''},{av:'AV46TFGlosario_NomeArq_Sel',fld:'vTFGLOSARIO_NOMEARQ_SEL',pic:'',nv:''},{av:'Ddo_glosario_termo_Sortedstatus',ctrl:'DDO_GLOSARIO_TERMO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E27F82',iparms:[{av:'A1347Glosario_Codigo',fld:'GLOSARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A859Glosario_Descricao',fld:'GLOSARIO_DESCRICAO',pic:'',nv:''}],oparms:[{av:'AV28Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV29Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'edtGlosario_Termo_Link',ctrl:'GLOSARIO_TERMO',prop:'Link'},{av:'edtGlosario_Termo_Tooltiptext',ctrl:'GLOSARIO_TERMO',prop:'Tooltiptext'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E15F82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Glosario_Termo1',fld:'vGLOSARIO_TERMO1',pic:'',nv:''},{av:'AV34Descricao1',fld:'vDESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Glosario_Termo2',fld:'vGLOSARIO_TERMO2',pic:'',nv:''},{av:'AV35Descricao2',fld:'vDESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25Glosario_Termo3',fld:'vGLOSARIO_TERMO3',pic:'',nv:''},{av:'AV36Descricao3',fld:'vDESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV41TFGlosario_Termo',fld:'vTFGLOSARIO_TERMO',pic:'',nv:''},{av:'AV42TFGlosario_Termo_Sel',fld:'vTFGLOSARIO_TERMO_SEL',pic:'',nv:''},{av:'AV45TFGlosario_NomeArq',fld:'vTFGLOSARIO_NOMEARQ',pic:'',nv:''},{av:'AV46TFGlosario_NomeArq_Sel',fld:'vTFGLOSARIO_NOMEARQ_SEL',pic:'',nv:''},{av:'edtGlosario_Arquivo_Display',ctrl:'GLOSARIO_ARQUIVO',prop:'Display'},{av:'edtGlosario_Arquivo_Tooltiptext',ctrl:'GLOSARIO_ARQUIVO',prop:'Tooltiptext'},{av:'edtGlosario_Arquivo_Linktarget',ctrl:'GLOSARIO_ARQUIVO',prop:'Linktarget'},{av:'AV43ddo_Glosario_TermoTitleControlIdToReplace',fld:'vDDO_GLOSARIO_TERMOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Glosario_NomeArqTitleControlIdToReplace',fld:'vDDO_GLOSARIO_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37Glosario_AreaTrabalhoCod',fld:'vGLOSARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1347Glosario_Codigo',fld:'GLOSARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A859Glosario_Descricao',fld:'GLOSARIO_DESCRICAO',pic:'',nv:''}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E21F82',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E16F82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Glosario_Termo1',fld:'vGLOSARIO_TERMO1',pic:'',nv:''},{av:'AV34Descricao1',fld:'vDESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Glosario_Termo2',fld:'vGLOSARIO_TERMO2',pic:'',nv:''},{av:'AV35Descricao2',fld:'vDESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25Glosario_Termo3',fld:'vGLOSARIO_TERMO3',pic:'',nv:''},{av:'AV36Descricao3',fld:'vDESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV41TFGlosario_Termo',fld:'vTFGLOSARIO_TERMO',pic:'',nv:''},{av:'AV42TFGlosario_Termo_Sel',fld:'vTFGLOSARIO_TERMO_SEL',pic:'',nv:''},{av:'AV45TFGlosario_NomeArq',fld:'vTFGLOSARIO_NOMEARQ',pic:'',nv:''},{av:'AV46TFGlosario_NomeArq_Sel',fld:'vTFGLOSARIO_NOMEARQ_SEL',pic:'',nv:''},{av:'edtGlosario_Arquivo_Display',ctrl:'GLOSARIO_ARQUIVO',prop:'Display'},{av:'edtGlosario_Arquivo_Tooltiptext',ctrl:'GLOSARIO_ARQUIVO',prop:'Tooltiptext'},{av:'edtGlosario_Arquivo_Linktarget',ctrl:'GLOSARIO_ARQUIVO',prop:'Linktarget'},{av:'AV43ddo_Glosario_TermoTitleControlIdToReplace',fld:'vDDO_GLOSARIO_TERMOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Glosario_NomeArqTitleControlIdToReplace',fld:'vDDO_GLOSARIO_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37Glosario_AreaTrabalhoCod',fld:'vGLOSARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1347Glosario_Codigo',fld:'GLOSARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A859Glosario_Descricao',fld:'GLOSARIO_DESCRICAO',pic:'',nv:''}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Glosario_Termo2',fld:'vGLOSARIO_TERMO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25Glosario_Termo3',fld:'vGLOSARIO_TERMO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Glosario_Termo1',fld:'vGLOSARIO_TERMO1',pic:'',nv:''},{av:'AV34Descricao1',fld:'vDESCRICAO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV35Descricao2',fld:'vDESCRICAO2',pic:'',nv:''},{av:'AV36Descricao3',fld:'vDESCRICAO3',pic:'',nv:''},{av:'edtavGlosario_termo2_Visible',ctrl:'vGLOSARIO_TERMO2',prop:'Visible'},{av:'edtavDescricao2_Visible',ctrl:'vDESCRICAO2',prop:'Visible'},{av:'edtavGlosario_termo3_Visible',ctrl:'vGLOSARIO_TERMO3',prop:'Visible'},{av:'edtavDescricao3_Visible',ctrl:'vDESCRICAO3',prop:'Visible'},{av:'edtavGlosario_termo1_Visible',ctrl:'vGLOSARIO_TERMO1',prop:'Visible'},{av:'edtavDescricao1_Visible',ctrl:'vDESCRICAO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E22F82',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavGlosario_termo1_Visible',ctrl:'vGLOSARIO_TERMO1',prop:'Visible'},{av:'edtavDescricao1_Visible',ctrl:'vDESCRICAO1',prop:'Visible'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E23F82',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E17F82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Glosario_Termo1',fld:'vGLOSARIO_TERMO1',pic:'',nv:''},{av:'AV34Descricao1',fld:'vDESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Glosario_Termo2',fld:'vGLOSARIO_TERMO2',pic:'',nv:''},{av:'AV35Descricao2',fld:'vDESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25Glosario_Termo3',fld:'vGLOSARIO_TERMO3',pic:'',nv:''},{av:'AV36Descricao3',fld:'vDESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV41TFGlosario_Termo',fld:'vTFGLOSARIO_TERMO',pic:'',nv:''},{av:'AV42TFGlosario_Termo_Sel',fld:'vTFGLOSARIO_TERMO_SEL',pic:'',nv:''},{av:'AV45TFGlosario_NomeArq',fld:'vTFGLOSARIO_NOMEARQ',pic:'',nv:''},{av:'AV46TFGlosario_NomeArq_Sel',fld:'vTFGLOSARIO_NOMEARQ_SEL',pic:'',nv:''},{av:'edtGlosario_Arquivo_Display',ctrl:'GLOSARIO_ARQUIVO',prop:'Display'},{av:'edtGlosario_Arquivo_Tooltiptext',ctrl:'GLOSARIO_ARQUIVO',prop:'Tooltiptext'},{av:'edtGlosario_Arquivo_Linktarget',ctrl:'GLOSARIO_ARQUIVO',prop:'Linktarget'},{av:'AV43ddo_Glosario_TermoTitleControlIdToReplace',fld:'vDDO_GLOSARIO_TERMOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Glosario_NomeArqTitleControlIdToReplace',fld:'vDDO_GLOSARIO_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37Glosario_AreaTrabalhoCod',fld:'vGLOSARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1347Glosario_Codigo',fld:'GLOSARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A859Glosario_Descricao',fld:'GLOSARIO_DESCRICAO',pic:'',nv:''}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Glosario_Termo2',fld:'vGLOSARIO_TERMO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25Glosario_Termo3',fld:'vGLOSARIO_TERMO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Glosario_Termo1',fld:'vGLOSARIO_TERMO1',pic:'',nv:''},{av:'AV34Descricao1',fld:'vDESCRICAO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV35Descricao2',fld:'vDESCRICAO2',pic:'',nv:''},{av:'AV36Descricao3',fld:'vDESCRICAO3',pic:'',nv:''},{av:'edtavGlosario_termo2_Visible',ctrl:'vGLOSARIO_TERMO2',prop:'Visible'},{av:'edtavDescricao2_Visible',ctrl:'vDESCRICAO2',prop:'Visible'},{av:'edtavGlosario_termo3_Visible',ctrl:'vGLOSARIO_TERMO3',prop:'Visible'},{av:'edtavDescricao3_Visible',ctrl:'vDESCRICAO3',prop:'Visible'},{av:'edtavGlosario_termo1_Visible',ctrl:'vGLOSARIO_TERMO1',prop:'Visible'},{av:'edtavDescricao1_Visible',ctrl:'vDESCRICAO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E24F82',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavGlosario_termo2_Visible',ctrl:'vGLOSARIO_TERMO2',prop:'Visible'},{av:'edtavDescricao2_Visible',ctrl:'vDESCRICAO2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E18F82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Glosario_Termo1',fld:'vGLOSARIO_TERMO1',pic:'',nv:''},{av:'AV34Descricao1',fld:'vDESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Glosario_Termo2',fld:'vGLOSARIO_TERMO2',pic:'',nv:''},{av:'AV35Descricao2',fld:'vDESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25Glosario_Termo3',fld:'vGLOSARIO_TERMO3',pic:'',nv:''},{av:'AV36Descricao3',fld:'vDESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV41TFGlosario_Termo',fld:'vTFGLOSARIO_TERMO',pic:'',nv:''},{av:'AV42TFGlosario_Termo_Sel',fld:'vTFGLOSARIO_TERMO_SEL',pic:'',nv:''},{av:'AV45TFGlosario_NomeArq',fld:'vTFGLOSARIO_NOMEARQ',pic:'',nv:''},{av:'AV46TFGlosario_NomeArq_Sel',fld:'vTFGLOSARIO_NOMEARQ_SEL',pic:'',nv:''},{av:'edtGlosario_Arquivo_Display',ctrl:'GLOSARIO_ARQUIVO',prop:'Display'},{av:'edtGlosario_Arquivo_Tooltiptext',ctrl:'GLOSARIO_ARQUIVO',prop:'Tooltiptext'},{av:'edtGlosario_Arquivo_Linktarget',ctrl:'GLOSARIO_ARQUIVO',prop:'Linktarget'},{av:'AV43ddo_Glosario_TermoTitleControlIdToReplace',fld:'vDDO_GLOSARIO_TERMOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Glosario_NomeArqTitleControlIdToReplace',fld:'vDDO_GLOSARIO_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37Glosario_AreaTrabalhoCod',fld:'vGLOSARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1347Glosario_Codigo',fld:'GLOSARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A859Glosario_Descricao',fld:'GLOSARIO_DESCRICAO',pic:'',nv:''}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Glosario_Termo2',fld:'vGLOSARIO_TERMO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25Glosario_Termo3',fld:'vGLOSARIO_TERMO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Glosario_Termo1',fld:'vGLOSARIO_TERMO1',pic:'',nv:''},{av:'AV34Descricao1',fld:'vDESCRICAO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV35Descricao2',fld:'vDESCRICAO2',pic:'',nv:''},{av:'AV36Descricao3',fld:'vDESCRICAO3',pic:'',nv:''},{av:'edtavGlosario_termo2_Visible',ctrl:'vGLOSARIO_TERMO2',prop:'Visible'},{av:'edtavDescricao2_Visible',ctrl:'vDESCRICAO2',prop:'Visible'},{av:'edtavGlosario_termo3_Visible',ctrl:'vGLOSARIO_TERMO3',prop:'Visible'},{av:'edtavDescricao3_Visible',ctrl:'vDESCRICAO3',prop:'Visible'},{av:'edtavGlosario_termo1_Visible',ctrl:'vGLOSARIO_TERMO1',prop:'Visible'},{av:'edtavDescricao1_Visible',ctrl:'vDESCRICAO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E25F82',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavGlosario_termo3_Visible',ctrl:'vGLOSARIO_TERMO3',prop:'Visible'},{av:'edtavDescricao3_Visible',ctrl:'vDESCRICAO3',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E19F82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Glosario_Termo1',fld:'vGLOSARIO_TERMO1',pic:'',nv:''},{av:'AV34Descricao1',fld:'vDESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Glosario_Termo2',fld:'vGLOSARIO_TERMO2',pic:'',nv:''},{av:'AV35Descricao2',fld:'vDESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25Glosario_Termo3',fld:'vGLOSARIO_TERMO3',pic:'',nv:''},{av:'AV36Descricao3',fld:'vDESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV41TFGlosario_Termo',fld:'vTFGLOSARIO_TERMO',pic:'',nv:''},{av:'AV42TFGlosario_Termo_Sel',fld:'vTFGLOSARIO_TERMO_SEL',pic:'',nv:''},{av:'AV45TFGlosario_NomeArq',fld:'vTFGLOSARIO_NOMEARQ',pic:'',nv:''},{av:'AV46TFGlosario_NomeArq_Sel',fld:'vTFGLOSARIO_NOMEARQ_SEL',pic:'',nv:''},{av:'edtGlosario_Arquivo_Display',ctrl:'GLOSARIO_ARQUIVO',prop:'Display'},{av:'edtGlosario_Arquivo_Tooltiptext',ctrl:'GLOSARIO_ARQUIVO',prop:'Tooltiptext'},{av:'edtGlosario_Arquivo_Linktarget',ctrl:'GLOSARIO_ARQUIVO',prop:'Linktarget'},{av:'AV43ddo_Glosario_TermoTitleControlIdToReplace',fld:'vDDO_GLOSARIO_TERMOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Glosario_NomeArqTitleControlIdToReplace',fld:'vDDO_GLOSARIO_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37Glosario_AreaTrabalhoCod',fld:'vGLOSARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1347Glosario_Codigo',fld:'GLOSARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A859Glosario_Descricao',fld:'GLOSARIO_DESCRICAO',pic:'',nv:''}],oparms:[{av:'AV37Glosario_AreaTrabalhoCod',fld:'vGLOSARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV41TFGlosario_Termo',fld:'vTFGLOSARIO_TERMO',pic:'',nv:''},{av:'Ddo_glosario_termo_Filteredtext_set',ctrl:'DDO_GLOSARIO_TERMO',prop:'FilteredText_set'},{av:'AV42TFGlosario_Termo_Sel',fld:'vTFGLOSARIO_TERMO_SEL',pic:'',nv:''},{av:'Ddo_glosario_termo_Selectedvalue_set',ctrl:'DDO_GLOSARIO_TERMO',prop:'SelectedValue_set'},{av:'AV45TFGlosario_NomeArq',fld:'vTFGLOSARIO_NOMEARQ',pic:'',nv:''},{av:'Ddo_glosario_nomearq_Filteredtext_set',ctrl:'DDO_GLOSARIO_NOMEARQ',prop:'FilteredText_set'},{av:'AV46TFGlosario_NomeArq_Sel',fld:'vTFGLOSARIO_NOMEARQ_SEL',pic:'',nv:''},{av:'Ddo_glosario_nomearq_Selectedvalue_set',ctrl:'DDO_GLOSARIO_NOMEARQ',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Glosario_Termo1',fld:'vGLOSARIO_TERMO1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavGlosario_termo1_Visible',ctrl:'vGLOSARIO_TERMO1',prop:'Visible'},{av:'edtavDescricao1_Visible',ctrl:'vDESCRICAO1',prop:'Visible'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Glosario_Termo2',fld:'vGLOSARIO_TERMO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25Glosario_Termo3',fld:'vGLOSARIO_TERMO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV34Descricao1',fld:'vDESCRICAO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV35Descricao2',fld:'vDESCRICAO2',pic:'',nv:''},{av:'AV36Descricao3',fld:'vDESCRICAO3',pic:'',nv:''},{av:'edtavGlosario_termo2_Visible',ctrl:'vGLOSARIO_TERMO2',prop:'Visible'},{av:'edtavDescricao2_Visible',ctrl:'vDESCRICAO2',prop:'Visible'},{av:'edtavGlosario_termo3_Visible',ctrl:'vGLOSARIO_TERMO3',prop:'Visible'},{av:'edtavDescricao3_Visible',ctrl:'vDESCRICAO3',prop:'Visible'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E20F82',iparms:[{av:'A1347Glosario_Codigo',fld:'GLOSARIO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_glosario_termo_Activeeventkey = "";
         Ddo_glosario_termo_Filteredtext_get = "";
         Ddo_glosario_termo_Selectedvalue_get = "";
         Ddo_glosario_nomearq_Activeeventkey = "";
         Ddo_glosario_nomearq_Filteredtext_get = "";
         Ddo_glosario_nomearq_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17Glosario_Termo1 = "";
         AV34Descricao1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV21Glosario_Termo2 = "";
         AV35Descricao2 = "";
         AV23DynamicFiltersSelector3 = "";
         AV25Glosario_Termo3 = "";
         AV36Descricao3 = "";
         AV41TFGlosario_Termo = "";
         AV42TFGlosario_Termo_Sel = "";
         AV45TFGlosario_NomeArq = "";
         AV46TFGlosario_NomeArq_Sel = "";
         AV43ddo_Glosario_TermoTitleControlIdToReplace = "";
         AV47ddo_Glosario_NomeArqTitleControlIdToReplace = "";
         AV72Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         A859Glosario_Descricao = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV48DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV40Glosario_TermoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV44Glosario_NomeArqTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_glosario_termo_Filteredtext_set = "";
         Ddo_glosario_termo_Selectedvalue_set = "";
         Ddo_glosario_termo_Sortedstatus = "";
         Ddo_glosario_nomearq_Filteredtext_set = "";
         Ddo_glosario_nomearq_Selectedvalue_set = "";
         Ddo_glosario_nomearq_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Update = "";
         AV70Update_GXI = "";
         AV29Delete = "";
         AV71Delete_GXI = "";
         A858Glosario_Termo = "";
         A1342Glosario_Arquivo = "";
         GridContainer = new GXWebGrid( context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         scmdbuf = "";
         lV56WWGlosarioDS_3_Glosario_termo1 = "";
         lV57WWGlosarioDS_4_Descricao1 = "";
         lV60WWGlosarioDS_7_Glosario_termo2 = "";
         lV61WWGlosarioDS_8_Descricao2 = "";
         lV64WWGlosarioDS_11_Glosario_termo3 = "";
         lV65WWGlosarioDS_12_Descricao3 = "";
         lV66WWGlosarioDS_13_Tfglosario_termo = "";
         lV68WWGlosarioDS_15_Tfglosario_nomearq = "";
         AV55WWGlosarioDS_2_Dynamicfiltersselector1 = "";
         AV56WWGlosarioDS_3_Glosario_termo1 = "";
         AV57WWGlosarioDS_4_Descricao1 = "";
         AV59WWGlosarioDS_6_Dynamicfiltersselector2 = "";
         AV60WWGlosarioDS_7_Glosario_termo2 = "";
         AV61WWGlosarioDS_8_Descricao2 = "";
         AV63WWGlosarioDS_10_Dynamicfiltersselector3 = "";
         AV64WWGlosarioDS_11_Glosario_termo3 = "";
         AV65WWGlosarioDS_12_Descricao3 = "";
         AV67WWGlosarioDS_14_Tfglosario_termo_sel = "";
         AV66WWGlosarioDS_13_Tfglosario_termo = "";
         AV69WWGlosarioDS_16_Tfglosario_nomearq_sel = "";
         AV68WWGlosarioDS_15_Tfglosario_nomearq = "";
         A1343Glosario_NomeArq = "";
         H00F82_A1343Glosario_NomeArq = new String[] {""} ;
         H00F82_n1343Glosario_NomeArq = new bool[] {false} ;
         H00F82_A1344Glosario_TipoArq = new String[] {""} ;
         H00F82_n1344Glosario_TipoArq = new bool[] {false} ;
         H00F82_A1346Glosario_AreaTrabalhoCod = new int[1] ;
         H00F82_A1347Glosario_Codigo = new int[1] ;
         H00F82_A859Glosario_Descricao = new String[] {""} ;
         H00F82_A858Glosario_Termo = new String[] {""} ;
         H00F82_A1342Glosario_Arquivo = new String[] {""} ;
         H00F82_n1342Glosario_Arquivo = new bool[] {false} ;
         edtGlosario_Arquivo_Filename = "";
         A1344Glosario_TipoArq = "";
         H00F83_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         GridRow = new GXWebRow();
         AV30Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblGlosariotitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblFiltertextglosario_areatrabalhocod_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         bttBtn_search_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         gxblobfileaux = new GxFile(context.GetPhysicalPath());
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwglosario__default(),
            new Object[][] {
                new Object[] {
               H00F82_A1343Glosario_NomeArq, H00F82_n1343Glosario_NomeArq, H00F82_A1344Glosario_TipoArq, H00F82_n1344Glosario_TipoArq, H00F82_A1346Glosario_AreaTrabalhoCod, H00F82_A1347Glosario_Codigo, H00F82_A859Glosario_Descricao, H00F82_A858Glosario_Termo, H00F82_A1342Glosario_Arquivo, H00F82_n1342Glosario_Arquivo
               }
               , new Object[] {
               H00F83_AGRID_nRecordCount
               }
            }
         );
         AV72Pgmname = "WWGlosario";
         /* GeneXus formulas. */
         AV72Pgmname = "WWGlosario";
         context.Gx_err = 0;
      }

      private short edtGlosario_Arquivo_Display ;
      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_82 ;
      private short nGXsfl_82_idx=1 ;
      private short AV13OrderedBy ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_82_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtGlosario_Termo_Titleformat ;
      private short edtGlosario_NomeArq_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV37Glosario_AreaTrabalhoCod ;
      private int A1347Glosario_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_glosario_termo_Datalistupdateminimumcharacters ;
      private int Ddo_glosario_nomearq_Datalistupdateminimumcharacters ;
      private int edtavTfglosario_termo_Visible ;
      private int edtavTfglosario_termo_sel_Visible ;
      private int edtavTfglosario_nomearq_Visible ;
      private int edtavTfglosario_nomearq_sel_Visible ;
      private int edtavDdo_glosario_termotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_glosario_nomearqtitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV6WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A1346Glosario_AreaTrabalhoCod ;
      private int AV54WWGlosarioDS_1_Glosario_areatrabalhocod ;
      private int edtavOrdereddsc_Visible ;
      private int imgInsert_Visible ;
      private int edtavUpdate_Visible ;
      private int edtavDelete_Visible ;
      private int AV49PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavGlosario_termo1_Visible ;
      private int edtavDescricao1_Visible ;
      private int edtavGlosario_termo2_Visible ;
      private int edtavDescricao2_Visible ;
      private int edtavGlosario_termo3_Visible ;
      private int edtavDescricao3_Visible ;
      private int AV73GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV50GridCurrentPage ;
      private long AV51GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String edtGlosario_Arquivo_Tooltiptext ;
      private String edtGlosario_Arquivo_Linktarget ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_glosario_termo_Activeeventkey ;
      private String Ddo_glosario_termo_Filteredtext_get ;
      private String Ddo_glosario_termo_Selectedvalue_get ;
      private String Ddo_glosario_nomearq_Activeeventkey ;
      private String Ddo_glosario_nomearq_Filteredtext_get ;
      private String Ddo_glosario_nomearq_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_82_idx="0001" ;
      private String edtGlosario_Arquivo_Internalname ;
      private String AV34Descricao1 ;
      private String AV35Descricao2 ;
      private String AV36Descricao3 ;
      private String AV45TFGlosario_NomeArq ;
      private String AV46TFGlosario_NomeArq_Sel ;
      private String AV72Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_glosario_termo_Caption ;
      private String Ddo_glosario_termo_Tooltip ;
      private String Ddo_glosario_termo_Cls ;
      private String Ddo_glosario_termo_Filteredtext_set ;
      private String Ddo_glosario_termo_Selectedvalue_set ;
      private String Ddo_glosario_termo_Dropdownoptionstype ;
      private String Ddo_glosario_termo_Titlecontrolidtoreplace ;
      private String Ddo_glosario_termo_Sortedstatus ;
      private String Ddo_glosario_termo_Filtertype ;
      private String Ddo_glosario_termo_Datalisttype ;
      private String Ddo_glosario_termo_Datalistproc ;
      private String Ddo_glosario_termo_Sortasc ;
      private String Ddo_glosario_termo_Sortdsc ;
      private String Ddo_glosario_termo_Loadingdata ;
      private String Ddo_glosario_termo_Cleanfilter ;
      private String Ddo_glosario_termo_Noresultsfound ;
      private String Ddo_glosario_termo_Searchbuttontext ;
      private String Ddo_glosario_nomearq_Caption ;
      private String Ddo_glosario_nomearq_Tooltip ;
      private String Ddo_glosario_nomearq_Cls ;
      private String Ddo_glosario_nomearq_Filteredtext_set ;
      private String Ddo_glosario_nomearq_Selectedvalue_set ;
      private String Ddo_glosario_nomearq_Dropdownoptionstype ;
      private String Ddo_glosario_nomearq_Titlecontrolidtoreplace ;
      private String Ddo_glosario_nomearq_Sortedstatus ;
      private String Ddo_glosario_nomearq_Filtertype ;
      private String Ddo_glosario_nomearq_Datalisttype ;
      private String Ddo_glosario_nomearq_Datalistproc ;
      private String Ddo_glosario_nomearq_Sortasc ;
      private String Ddo_glosario_nomearq_Sortdsc ;
      private String Ddo_glosario_nomearq_Loadingdata ;
      private String Ddo_glosario_nomearq_Cleanfilter ;
      private String Ddo_glosario_nomearq_Noresultsfound ;
      private String Ddo_glosario_nomearq_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfglosario_termo_Internalname ;
      private String edtavTfglosario_termo_Jsonclick ;
      private String edtavTfglosario_termo_sel_Internalname ;
      private String edtavTfglosario_termo_sel_Jsonclick ;
      private String edtavTfglosario_nomearq_Internalname ;
      private String edtavTfglosario_nomearq_Jsonclick ;
      private String edtavTfglosario_nomearq_sel_Internalname ;
      private String edtavTfglosario_nomearq_sel_Jsonclick ;
      private String edtavDdo_glosario_termotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_glosario_nomearqtitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtGlosario_Termo_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV57WWGlosarioDS_4_Descricao1 ;
      private String lV61WWGlosarioDS_8_Descricao2 ;
      private String lV65WWGlosarioDS_12_Descricao3 ;
      private String lV68WWGlosarioDS_15_Tfglosario_nomearq ;
      private String AV57WWGlosarioDS_4_Descricao1 ;
      private String AV61WWGlosarioDS_8_Descricao2 ;
      private String AV65WWGlosarioDS_12_Descricao3 ;
      private String AV69WWGlosarioDS_16_Tfglosario_nomearq_sel ;
      private String AV68WWGlosarioDS_15_Tfglosario_nomearq ;
      private String A1343Glosario_NomeArq ;
      private String edtGlosario_Arquivo_Filename ;
      private String A1344Glosario_TipoArq ;
      private String edtGlosario_Arquivo_Filetype ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavGlosario_areatrabalhocod_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavGlosario_termo1_Internalname ;
      private String edtavDescricao1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavGlosario_termo2_Internalname ;
      private String edtavDescricao2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavGlosario_termo3_Internalname ;
      private String edtavDescricao3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_glosario_termo_Internalname ;
      private String Ddo_glosario_nomearq_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtGlosario_Termo_Title ;
      private String edtGlosario_NomeArq_Title ;
      private String edtGlosario_NomeArq_Internalname ;
      private String imgInsert_Internalname ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtGlosario_Termo_Link ;
      private String edtGlosario_Termo_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblGlosariotitle_Internalname ;
      private String lblGlosariotitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblFiltertextglosario_areatrabalhocod_Internalname ;
      private String lblFiltertextglosario_areatrabalhocod_Jsonclick ;
      private String edtavGlosario_areatrabalhocod_Jsonclick ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String bttBtn_search_Internalname ;
      private String bttBtn_search_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavGlosario_termo1_Jsonclick ;
      private String edtavDescricao1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavGlosario_termo2_Jsonclick ;
      private String edtavDescricao2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavGlosario_termo3_Jsonclick ;
      private String edtavDescricao3_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_82_fel_idx="0001" ;
      private String ROClassString ;
      private String edtGlosario_Termo_Jsonclick ;
      private String edtGlosario_NomeArq_Jsonclick ;
      private String edtGlosario_Arquivo_Contenttype ;
      private String edtGlosario_Arquivo_Parameters ;
      private String edtGlosario_Arquivo_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_glosario_termo_Includesortasc ;
      private bool Ddo_glosario_termo_Includesortdsc ;
      private bool Ddo_glosario_termo_Includefilter ;
      private bool Ddo_glosario_termo_Filterisrange ;
      private bool Ddo_glosario_termo_Includedatalist ;
      private bool Ddo_glosario_nomearq_Includesortasc ;
      private bool Ddo_glosario_nomearq_Includesortdsc ;
      private bool Ddo_glosario_nomearq_Includefilter ;
      private bool Ddo_glosario_nomearq_Filterisrange ;
      private bool Ddo_glosario_nomearq_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1342Glosario_Arquivo ;
      private bool AV58WWGlosarioDS_5_Dynamicfiltersenabled2 ;
      private bool AV62WWGlosarioDS_9_Dynamicfiltersenabled3 ;
      private bool n1343Glosario_NomeArq ;
      private bool n1344Glosario_TipoArq ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Update_IsBlob ;
      private bool AV29Delete_IsBlob ;
      private String A859Glosario_Descricao ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV17Glosario_Termo1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV21Glosario_Termo2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV25Glosario_Termo3 ;
      private String AV41TFGlosario_Termo ;
      private String AV42TFGlosario_Termo_Sel ;
      private String AV43ddo_Glosario_TermoTitleControlIdToReplace ;
      private String AV47ddo_Glosario_NomeArqTitleControlIdToReplace ;
      private String AV70Update_GXI ;
      private String AV71Delete_GXI ;
      private String A858Glosario_Termo ;
      private String lV56WWGlosarioDS_3_Glosario_termo1 ;
      private String lV60WWGlosarioDS_7_Glosario_termo2 ;
      private String lV64WWGlosarioDS_11_Glosario_termo3 ;
      private String lV66WWGlosarioDS_13_Tfglosario_termo ;
      private String AV55WWGlosarioDS_2_Dynamicfiltersselector1 ;
      private String AV56WWGlosarioDS_3_Glosario_termo1 ;
      private String AV59WWGlosarioDS_6_Dynamicfiltersselector2 ;
      private String AV60WWGlosarioDS_7_Glosario_termo2 ;
      private String AV63WWGlosarioDS_10_Dynamicfiltersselector3 ;
      private String AV64WWGlosarioDS_11_Glosario_termo3 ;
      private String AV67WWGlosarioDS_14_Tfglosario_termo_sel ;
      private String AV66WWGlosarioDS_13_Tfglosario_termo ;
      private String AV28Update ;
      private String AV29Delete ;
      private String A1342Glosario_Arquivo ;
      private IGxSession AV30Session ;
      private GxFile gxblobfileaux ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private String[] H00F82_A1343Glosario_NomeArq ;
      private bool[] H00F82_n1343Glosario_NomeArq ;
      private String[] H00F82_A1344Glosario_TipoArq ;
      private bool[] H00F82_n1344Glosario_TipoArq ;
      private int[] H00F82_A1346Glosario_AreaTrabalhoCod ;
      private int[] H00F82_A1347Glosario_Codigo ;
      private String[] H00F82_A859Glosario_Descricao ;
      private String[] H00F82_A858Glosario_Termo ;
      private String[] H00F82_A1342Glosario_Arquivo ;
      private bool[] H00F82_n1342Glosario_Arquivo ;
      private long[] H00F83_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV40Glosario_TermoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV44Glosario_NomeArqTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV48DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwglosario__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00F82( IGxContext context ,
                                             String AV55WWGlosarioDS_2_Dynamicfiltersselector1 ,
                                             String AV56WWGlosarioDS_3_Glosario_termo1 ,
                                             String AV57WWGlosarioDS_4_Descricao1 ,
                                             bool AV58WWGlosarioDS_5_Dynamicfiltersenabled2 ,
                                             String AV59WWGlosarioDS_6_Dynamicfiltersselector2 ,
                                             String AV60WWGlosarioDS_7_Glosario_termo2 ,
                                             String AV61WWGlosarioDS_8_Descricao2 ,
                                             bool AV62WWGlosarioDS_9_Dynamicfiltersenabled3 ,
                                             String AV63WWGlosarioDS_10_Dynamicfiltersselector3 ,
                                             String AV64WWGlosarioDS_11_Glosario_termo3 ,
                                             String AV65WWGlosarioDS_12_Descricao3 ,
                                             String AV67WWGlosarioDS_14_Tfglosario_termo_sel ,
                                             String AV66WWGlosarioDS_13_Tfglosario_termo ,
                                             String AV69WWGlosarioDS_16_Tfglosario_nomearq_sel ,
                                             String AV68WWGlosarioDS_15_Tfglosario_nomearq ,
                                             String A858Glosario_Termo ,
                                             String A859Glosario_Descricao ,
                                             String A1343Glosario_NomeArq ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A1346Glosario_AreaTrabalhoCod ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [16] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [Glosario_NomeArq], [Glosario_TipoArq], [Glosario_AreaTrabalhoCod], [Glosario_Codigo], [Glosario_Descricao], [Glosario_Termo], [Glosario_Arquivo]";
         sFromString = " FROM [Glosario] WITH (NOLOCK)";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE ([Glosario_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV55WWGlosarioDS_2_Dynamicfiltersselector1, "GLOSARIO_TERMO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWGlosarioDS_3_Glosario_termo1)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Termo] like '%' + @lV56WWGlosarioDS_3_Glosario_termo1)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV55WWGlosarioDS_2_Dynamicfiltersselector1, "DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWGlosarioDS_4_Descricao1)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Descricao] like '%' + @lV57WWGlosarioDS_4_Descricao1 + '%')";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV58WWGlosarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV59WWGlosarioDS_6_Dynamicfiltersselector2, "GLOSARIO_TERMO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWGlosarioDS_7_Glosario_termo2)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Termo] like '%' + @lV60WWGlosarioDS_7_Glosario_termo2)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV58WWGlosarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV59WWGlosarioDS_6_Dynamicfiltersselector2, "DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWGlosarioDS_8_Descricao2)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Descricao] like '%' + @lV61WWGlosarioDS_8_Descricao2 + '%')";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV62WWGlosarioDS_9_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV63WWGlosarioDS_10_Dynamicfiltersselector3, "GLOSARIO_TERMO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWGlosarioDS_11_Glosario_termo3)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Termo] like '%' + @lV64WWGlosarioDS_11_Glosario_termo3)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV62WWGlosarioDS_9_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV63WWGlosarioDS_10_Dynamicfiltersselector3, "DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWGlosarioDS_12_Descricao3)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Descricao] like '%' + @lV65WWGlosarioDS_12_Descricao3 + '%')";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV67WWGlosarioDS_14_Tfglosario_termo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWGlosarioDS_13_Tfglosario_termo)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Termo] like @lV66WWGlosarioDS_13_Tfglosario_termo)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWGlosarioDS_14_Tfglosario_termo_sel)) )
         {
            sWhereString = sWhereString + " and ([Glosario_Termo] = @AV67WWGlosarioDS_14_Tfglosario_termo_sel)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV69WWGlosarioDS_16_Tfglosario_nomearq_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWGlosarioDS_15_Tfglosario_nomearq)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_NomeArq] like @lV68WWGlosarioDS_15_Tfglosario_nomearq)";
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWGlosarioDS_16_Tfglosario_nomearq_sel)) )
         {
            sWhereString = sWhereString + " and ([Glosario_NomeArq] = @AV69WWGlosarioDS_16_Tfglosario_nomearq_sel)";
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Glosario_Termo]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Glosario_Termo] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Glosario_NomeArq]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Glosario_NomeArq] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [Glosario_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00F83( IGxContext context ,
                                             String AV55WWGlosarioDS_2_Dynamicfiltersselector1 ,
                                             String AV56WWGlosarioDS_3_Glosario_termo1 ,
                                             String AV57WWGlosarioDS_4_Descricao1 ,
                                             bool AV58WWGlosarioDS_5_Dynamicfiltersenabled2 ,
                                             String AV59WWGlosarioDS_6_Dynamicfiltersselector2 ,
                                             String AV60WWGlosarioDS_7_Glosario_termo2 ,
                                             String AV61WWGlosarioDS_8_Descricao2 ,
                                             bool AV62WWGlosarioDS_9_Dynamicfiltersenabled3 ,
                                             String AV63WWGlosarioDS_10_Dynamicfiltersselector3 ,
                                             String AV64WWGlosarioDS_11_Glosario_termo3 ,
                                             String AV65WWGlosarioDS_12_Descricao3 ,
                                             String AV67WWGlosarioDS_14_Tfglosario_termo_sel ,
                                             String AV66WWGlosarioDS_13_Tfglosario_termo ,
                                             String AV69WWGlosarioDS_16_Tfglosario_nomearq_sel ,
                                             String AV68WWGlosarioDS_15_Tfglosario_nomearq ,
                                             String A858Glosario_Termo ,
                                             String A859Glosario_Descricao ,
                                             String A1343Glosario_NomeArq ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A1346Glosario_AreaTrabalhoCod ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [11] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [Glosario] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Glosario_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV55WWGlosarioDS_2_Dynamicfiltersselector1, "GLOSARIO_TERMO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWGlosarioDS_3_Glosario_termo1)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Termo] like '%' + @lV56WWGlosarioDS_3_Glosario_termo1)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV55WWGlosarioDS_2_Dynamicfiltersselector1, "DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWGlosarioDS_4_Descricao1)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Descricao] like '%' + @lV57WWGlosarioDS_4_Descricao1 + '%')";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV58WWGlosarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV59WWGlosarioDS_6_Dynamicfiltersselector2, "GLOSARIO_TERMO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWGlosarioDS_7_Glosario_termo2)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Termo] like '%' + @lV60WWGlosarioDS_7_Glosario_termo2)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV58WWGlosarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV59WWGlosarioDS_6_Dynamicfiltersselector2, "DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWGlosarioDS_8_Descricao2)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Descricao] like '%' + @lV61WWGlosarioDS_8_Descricao2 + '%')";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV62WWGlosarioDS_9_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV63WWGlosarioDS_10_Dynamicfiltersselector3, "GLOSARIO_TERMO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWGlosarioDS_11_Glosario_termo3)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Termo] like '%' + @lV64WWGlosarioDS_11_Glosario_termo3)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV62WWGlosarioDS_9_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV63WWGlosarioDS_10_Dynamicfiltersselector3, "DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWGlosarioDS_12_Descricao3)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Descricao] like '%' + @lV65WWGlosarioDS_12_Descricao3 + '%')";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV67WWGlosarioDS_14_Tfglosario_termo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWGlosarioDS_13_Tfglosario_termo)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Termo] like @lV66WWGlosarioDS_13_Tfglosario_termo)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWGlosarioDS_14_Tfglosario_termo_sel)) )
         {
            sWhereString = sWhereString + " and ([Glosario_Termo] = @AV67WWGlosarioDS_14_Tfglosario_termo_sel)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV69WWGlosarioDS_16_Tfglosario_nomearq_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWGlosarioDS_15_Tfglosario_nomearq)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_NomeArq] like @lV68WWGlosarioDS_15_Tfglosario_nomearq)";
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWGlosarioDS_16_Tfglosario_nomearq_sel)) )
         {
            sWhereString = sWhereString + " and ([Glosario_NomeArq] = @AV69WWGlosarioDS_16_Tfglosario_nomearq_sel)";
         }
         else
         {
            GXv_int4[10] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00F82(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (short)dynConstraints[18] , (bool)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] );
               case 1 :
                     return conditional_H00F83(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (short)dynConstraints[18] , (bool)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00F82 ;
          prmH00F82 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV56WWGlosarioDS_3_Glosario_termo1",SqlDbType.VarChar,255,0} ,
          new Object[] {"@lV57WWGlosarioDS_4_Descricao1",SqlDbType.Char,40,0} ,
          new Object[] {"@lV60WWGlosarioDS_7_Glosario_termo2",SqlDbType.VarChar,255,0} ,
          new Object[] {"@lV61WWGlosarioDS_8_Descricao2",SqlDbType.Char,40,0} ,
          new Object[] {"@lV64WWGlosarioDS_11_Glosario_termo3",SqlDbType.VarChar,255,0} ,
          new Object[] {"@lV65WWGlosarioDS_12_Descricao3",SqlDbType.Char,40,0} ,
          new Object[] {"@lV66WWGlosarioDS_13_Tfglosario_termo",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV67WWGlosarioDS_14_Tfglosario_termo_sel",SqlDbType.VarChar,255,0} ,
          new Object[] {"@lV68WWGlosarioDS_15_Tfglosario_nomearq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV69WWGlosarioDS_16_Tfglosario_nomearq_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00F83 ;
          prmH00F83 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV56WWGlosarioDS_3_Glosario_termo1",SqlDbType.VarChar,255,0} ,
          new Object[] {"@lV57WWGlosarioDS_4_Descricao1",SqlDbType.Char,40,0} ,
          new Object[] {"@lV60WWGlosarioDS_7_Glosario_termo2",SqlDbType.VarChar,255,0} ,
          new Object[] {"@lV61WWGlosarioDS_8_Descricao2",SqlDbType.Char,40,0} ,
          new Object[] {"@lV64WWGlosarioDS_11_Glosario_termo3",SqlDbType.VarChar,255,0} ,
          new Object[] {"@lV65WWGlosarioDS_12_Descricao3",SqlDbType.Char,40,0} ,
          new Object[] {"@lV66WWGlosarioDS_13_Tfglosario_termo",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV67WWGlosarioDS_14_Tfglosario_termo_sel",SqlDbType.VarChar,255,0} ,
          new Object[] {"@lV68WWGlosarioDS_15_Tfglosario_nomearq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV69WWGlosarioDS_16_Tfglosario_nomearq_sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00F82", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00F82,11,0,true,false )
             ,new CursorDef("H00F83", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00F83,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 10) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((String[]) buf[6])[0] = rslt.getLongVarchar(5) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
                ((String[]) buf[8])[0] = rslt.getBLOBFile(7, rslt.getString(2, 10), rslt.getString(1, 50)) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                return;
       }
    }

 }

}
