/*
               File: PRC_Usuario_Nome
        Description: Retorna nome do usu�rio GAM
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:42.6
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_usuario_nome : GXProcedure
   {
      public prc_usuario_nome( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_usuario_nome( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( String aP0_Usuario_UserGamGuid ,
                           out String aP1_Usuario_Nome )
      {
         this.A341Usuario_UserGamGuid = aP0_Usuario_UserGamGuid;
         this.AV8Usuario_Nome = "" ;
         initialize();
         executePrivate();
         aP1_Usuario_Nome=this.AV8Usuario_Nome;
      }

      public String executeUdp( String aP0_Usuario_UserGamGuid )
      {
         this.A341Usuario_UserGamGuid = aP0_Usuario_UserGamGuid;
         this.AV8Usuario_Nome = "" ;
         initialize();
         executePrivate();
         aP1_Usuario_Nome=this.AV8Usuario_Nome;
         return AV8Usuario_Nome ;
      }

      public void executeSubmit( String aP0_Usuario_UserGamGuid ,
                                 out String aP1_Usuario_Nome )
      {
         prc_usuario_nome objprc_usuario_nome;
         objprc_usuario_nome = new prc_usuario_nome();
         objprc_usuario_nome.A341Usuario_UserGamGuid = aP0_Usuario_UserGamGuid;
         objprc_usuario_nome.AV8Usuario_Nome = "" ;
         objprc_usuario_nome.context.SetSubmitInitialConfig(context);
         objprc_usuario_nome.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_usuario_nome);
         aP1_Usuario_Nome=this.AV8Usuario_Nome;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_usuario_nome)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV9GamUser.load( A341Usuario_UserGamGuid);
         AV8Usuario_Nome = AV9GamUser.gxTpr_Name;
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV9GamUser = new SdtGAMUser(context);
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private String A341Usuario_UserGamGuid ;
      private String AV8Usuario_Nome ;
      private String aP1_Usuario_Nome ;
      private SdtGAMUser AV9GamUser ;
   }

}
