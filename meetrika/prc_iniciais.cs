/*
               File: PRC_Iniciais
        Description: PRC_Iniciais
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:59.76
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_iniciais : GXProcedure
   {
      public prc_iniciais( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_iniciais( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( String aP0_Nome ,
                           out String aP1_Iniciais )
      {
         this.AV9Nome = aP0_Nome;
         this.AV8Iniciais = "" ;
         initialize();
         executePrivate();
         aP1_Iniciais=this.AV8Iniciais;
      }

      public String executeUdp( String aP0_Nome )
      {
         this.AV9Nome = aP0_Nome;
         this.AV8Iniciais = "" ;
         initialize();
         executePrivate();
         aP1_Iniciais=this.AV8Iniciais;
         return AV8Iniciais ;
      }

      public void executeSubmit( String aP0_Nome ,
                                 out String aP1_Iniciais )
      {
         prc_iniciais objprc_iniciais;
         objprc_iniciais = new prc_iniciais();
         objprc_iniciais.AV9Nome = aP0_Nome;
         objprc_iniciais.AV8Iniciais = "" ;
         objprc_iniciais.context.SetSubmitInitialConfig(context);
         objprc_iniciais.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_iniciais);
         aP1_Iniciais=this.AV8Iniciais;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_iniciais)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8Iniciais = StringUtil.Upper( StringUtil.Substring( StringUtil.Trim( AV9Nome), 1, 1));
         if ( StringUtil.StringSearchRev( StringUtil.Trim( AV9Nome), " ", -1) > 0 )
         {
            AV8Iniciais = AV8Iniciais + StringUtil.Upper( StringUtil.Substring( AV9Nome, StringUtil.StringSearchRev( StringUtil.Trim( AV9Nome), " ", -1)+1, 1));
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private String AV9Nome ;
      private String AV8Iniciais ;
      private String aP1_Iniciais ;
   }

}
