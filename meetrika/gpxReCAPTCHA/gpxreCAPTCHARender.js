function gpxreCAPTCHA($)
{
	this.Theme;
	this.SiteKey;
	this.Response;
	
	this.show = function()
	{
		///UserCodeRegionStart:[show] (do not remove this comment.)
    	if(!this.IsPostBack){
			var _me = this;
			$(_me.getContainerControl()).html('<div id="'+_me.ContainerName+'UC"></div>');
			this.render();
		}
		
		
		///UserCodeRegionEnd: (do not remove this comment.)
	}
	///UserCodeRegionStart:[User Functions] (do not remove this comment.)
	var recaptchaObjects = [];
	
	this.render = function(){
		var _me = this;
		var onfinish = function(){
			if (typeof grecaptcha == "undefined")
				return false;
			else{		
				grecaptcha.render(_me.ContainerName+'UC', {
						'sitekey' : _me.SiteKey,
						'callback' : function(response){_me.Response = response;}
				});
				return true;
			}
		}
		this.execToFinish(onfinish);
	}
	this.execToFinish = function(fnc){
		if (!fnc())
			setTimeout(fnc, 250);		
	}
	
	this.Reload = function(){
        grecaptcha.reset(0);
		this.Response = "";
    }
	
	///UserCodeRegionEnd: (do not remove this comment.):
}
