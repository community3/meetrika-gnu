/*
               File: GetDemandaNotificacoesWCFilterData
        Description: Get Demanda Notificacoes WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:13:8.66
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getdemandanotificacoeswcfilterdata : GXProcedure
   {
      public getdemandanotificacoeswcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getdemandanotificacoeswcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV31DDOName = aP0_DDOName;
         this.AV29SearchTxt = aP1_SearchTxt;
         this.AV30SearchTxtTo = aP2_SearchTxtTo;
         this.AV35OptionsJson = "" ;
         this.AV38OptionsDescJson = "" ;
         this.AV40OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV35OptionsJson;
         aP4_OptionsDescJson=this.AV38OptionsDescJson;
         aP5_OptionIndexesJson=this.AV40OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV31DDOName = aP0_DDOName;
         this.AV29SearchTxt = aP1_SearchTxt;
         this.AV30SearchTxtTo = aP2_SearchTxtTo;
         this.AV35OptionsJson = "" ;
         this.AV38OptionsDescJson = "" ;
         this.AV40OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV35OptionsJson;
         aP4_OptionsDescJson=this.AV38OptionsDescJson;
         aP5_OptionIndexesJson=this.AV40OptionIndexesJson;
         return AV40OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getdemandanotificacoeswcfilterdata objgetdemandanotificacoeswcfilterdata;
         objgetdemandanotificacoeswcfilterdata = new getdemandanotificacoeswcfilterdata();
         objgetdemandanotificacoeswcfilterdata.AV31DDOName = aP0_DDOName;
         objgetdemandanotificacoeswcfilterdata.AV29SearchTxt = aP1_SearchTxt;
         objgetdemandanotificacoeswcfilterdata.AV30SearchTxtTo = aP2_SearchTxtTo;
         objgetdemandanotificacoeswcfilterdata.AV35OptionsJson = "" ;
         objgetdemandanotificacoeswcfilterdata.AV38OptionsDescJson = "" ;
         objgetdemandanotificacoeswcfilterdata.AV40OptionIndexesJson = "" ;
         objgetdemandanotificacoeswcfilterdata.context.SetSubmitInitialConfig(context);
         objgetdemandanotificacoeswcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetdemandanotificacoeswcfilterdata);
         aP3_OptionsJson=this.AV35OptionsJson;
         aP4_OptionsDescJson=this.AV38OptionsDescJson;
         aP5_OptionIndexesJson=this.AV40OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getdemandanotificacoeswcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV34Options = (IGxCollection)(new GxSimpleCollection());
         AV37OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV39OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV31DDOName), "DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADONOTIFICACAO_USUNOMOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV31DDOName), "DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADONOTIFICACAO_ASSUNTOOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV31DDOName), "DDO_CONTAGEMRESULTADONOTIFICACAO_HOST") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADONOTIFICACAO_HOSTOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV31DDOName), "DDO_CONTAGEMRESULTADONOTIFICACAO_USER") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADONOTIFICACAO_USEROPTIONS' */
            S151 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV35OptionsJson = AV34Options.ToJSonString(false);
         AV38OptionsDescJson = AV37OptionsDesc.ToJSonString(false);
         AV40OptionIndexesJson = AV39OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV42Session.Get("DemandaNotificacoesWCGridState"), "") == 0 )
         {
            AV44GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "DemandaNotificacoesWCGridState"), "");
         }
         else
         {
            AV44GridState.FromXml(AV42Session.Get("DemandaNotificacoesWCGridState"), "");
         }
         AV52GXV1 = 1;
         while ( AV52GXV1 <= AV44GridState.gxTpr_Filtervalues.Count )
         {
            AV45GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV44GridState.gxTpr_Filtervalues.Item(AV52GXV1));
            if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 )
            {
               AV10TFContagemResultadoNotificacao_DataHora = context.localUtil.CToT( AV45GridStateFilterValue.gxTpr_Value, 2);
               AV11TFContagemResultadoNotificacao_DataHora_To = context.localUtil.CToT( AV45GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 )
            {
               AV12TFContagemResultadoNotificacao_UsuNom = AV45GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_USUNOM_SEL") == 0 )
            {
               AV13TFContagemResultadoNotificacao_UsuNom_Sel = AV45GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 )
            {
               AV48TFContagemResultadoNotificacao_Assunto = AV45GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO_SEL") == 0 )
            {
               AV49TFContagemResultadoNotificacao_Assunto_Sel = AV45GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS") == 0 )
            {
               AV14TFContagemResultadoNotificacao_Destinatarios = (int)(NumberUtil.Val( AV45GridStateFilterValue.gxTpr_Value, "."));
               AV15TFContagemResultadoNotificacao_Destinatarios_To = (int)(NumberUtil.Val( AV45GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS") == 0 )
            {
               AV16TFContagemResultadoNotificacao_Demandas = (int)(NumberUtil.Val( AV45GridStateFilterValue.gxTpr_Value, "."));
               AV17TFContagemResultadoNotificacao_Demandas_To = (int)(NumberUtil.Val( AV45GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_HOST") == 0 )
            {
               AV18TFContagemResultadoNotificacao_Host = AV45GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_HOST_SEL") == 0 )
            {
               AV19TFContagemResultadoNotificacao_Host_Sel = AV45GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_USER") == 0 )
            {
               AV20TFContagemResultadoNotificacao_User = AV45GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_USER_SEL") == 0 )
            {
               AV21TFContagemResultadoNotificacao_User_Sel = AV45GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_PORT") == 0 )
            {
               AV22TFContagemResultadoNotificacao_Port = (short)(NumberUtil.Val( AV45GridStateFilterValue.gxTpr_Value, "."));
               AV23TFContagemResultadoNotificacao_Port_To = (short)(NumberUtil.Val( AV45GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_AUT_SEL") == 0 )
            {
               AV24TFContagemResultadoNotificacao_Aut_Sel = (short)(NumberUtil.Val( AV45GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_SEC_SEL") == 0 )
            {
               AV25TFContagemResultadoNotificacao_Sec_SelsJson = AV45GridStateFilterValue.gxTpr_Value;
               AV26TFContagemResultadoNotificacao_Sec_Sels.FromJSonString(AV25TFContagemResultadoNotificacao_Sec_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_LOGGED_SEL") == 0 )
            {
               AV27TFContagemResultadoNotificacao_Logged_SelsJson = AV45GridStateFilterValue.gxTpr_Value;
               AV28TFContagemResultadoNotificacao_Logged_Sels.FromJSonString(AV27TFContagemResultadoNotificacao_Logged_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "PARM_&CONTAGEMRESULTADO_CODIGO") == 0 )
            {
               AV47ContagemResultado_Codigo = (int)(NumberUtil.Val( AV45GridStateFilterValue.gxTpr_Value, "."));
            }
            AV52GXV1 = (int)(AV52GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTAGEMRESULTADONOTIFICACAO_USUNOMOPTIONS' Routine */
         AV12TFContagemResultadoNotificacao_UsuNom = AV29SearchTxt;
         AV13TFContagemResultadoNotificacao_UsuNom_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A1960ContagemResultadoNotificacao_Sec ,
                                              AV26TFContagemResultadoNotificacao_Sec_Sels ,
                                              A1961ContagemResultadoNotificacao_Logged ,
                                              AV28TFContagemResultadoNotificacao_Logged_Sels ,
                                              AV10TFContagemResultadoNotificacao_DataHora ,
                                              AV11TFContagemResultadoNotificacao_DataHora_To ,
                                              AV13TFContagemResultadoNotificacao_UsuNom_Sel ,
                                              AV12TFContagemResultadoNotificacao_UsuNom ,
                                              AV49TFContagemResultadoNotificacao_Assunto_Sel ,
                                              AV48TFContagemResultadoNotificacao_Assunto ,
                                              AV19TFContagemResultadoNotificacao_Host_Sel ,
                                              AV18TFContagemResultadoNotificacao_Host ,
                                              AV21TFContagemResultadoNotificacao_User_Sel ,
                                              AV20TFContagemResultadoNotificacao_User ,
                                              AV22TFContagemResultadoNotificacao_Port ,
                                              AV23TFContagemResultadoNotificacao_Port_To ,
                                              AV24TFContagemResultadoNotificacao_Aut_Sel ,
                                              AV26TFContagemResultadoNotificacao_Sec_Sels.Count ,
                                              AV28TFContagemResultadoNotificacao_Logged_Sels.Count ,
                                              A1416ContagemResultadoNotificacao_DataHora ,
                                              A1422ContagemResultadoNotificacao_UsuNom ,
                                              A1417ContagemResultadoNotificacao_Assunto ,
                                              A1956ContagemResultadoNotificacao_Host ,
                                              A1957ContagemResultadoNotificacao_User ,
                                              A1958ContagemResultadoNotificacao_Port ,
                                              A1959ContagemResultadoNotificacao_Aut ,
                                              AV14TFContagemResultadoNotificacao_Destinatarios ,
                                              A1962ContagemResultadoNotificacao_Destinatarios ,
                                              AV15TFContagemResultadoNotificacao_Destinatarios_To ,
                                              AV16TFContagemResultadoNotificacao_Demandas ,
                                              A1963ContagemResultadoNotificacao_Demandas ,
                                              AV17TFContagemResultadoNotificacao_Demandas_To ,
                                              AV47ContagemResultado_Codigo ,
                                              A456ContagemResultado_Codigo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                              TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV12TFContagemResultadoNotificacao_UsuNom = StringUtil.PadR( StringUtil.RTrim( AV12TFContagemResultadoNotificacao_UsuNom), 100, "%");
         lV48TFContagemResultadoNotificacao_Assunto = StringUtil.Concat( StringUtil.RTrim( AV48TFContagemResultadoNotificacao_Assunto), "%", "");
         lV18TFContagemResultadoNotificacao_Host = StringUtil.PadR( StringUtil.RTrim( AV18TFContagemResultadoNotificacao_Host), 50, "%");
         lV20TFContagemResultadoNotificacao_User = StringUtil.PadR( StringUtil.RTrim( AV20TFContagemResultadoNotificacao_User), 50, "%");
         /* Using cursor P00VE4 */
         pr_default.execute(0, new Object[] {AV47ContagemResultado_Codigo, AV14TFContagemResultadoNotificacao_Destinatarios, AV14TFContagemResultadoNotificacao_Destinatarios, AV15TFContagemResultadoNotificacao_Destinatarios_To, AV15TFContagemResultadoNotificacao_Destinatarios_To, AV16TFContagemResultadoNotificacao_Demandas, AV16TFContagemResultadoNotificacao_Demandas, AV17TFContagemResultadoNotificacao_Demandas_To, AV17TFContagemResultadoNotificacao_Demandas_To, AV10TFContagemResultadoNotificacao_DataHora, AV11TFContagemResultadoNotificacao_DataHora_To, lV12TFContagemResultadoNotificacao_UsuNom, AV13TFContagemResultadoNotificacao_UsuNom_Sel, lV48TFContagemResultadoNotificacao_Assunto, AV49TFContagemResultadoNotificacao_Assunto_Sel, lV18TFContagemResultadoNotificacao_Host, AV19TFContagemResultadoNotificacao_Host_Sel, lV20TFContagemResultadoNotificacao_User, AV21TFContagemResultadoNotificacao_User_Sel, AV22TFContagemResultadoNotificacao_Port, AV23TFContagemResultadoNotificacao_Port_To});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKVE2 = false;
            A1412ContagemResultadoNotificacao_Codigo = P00VE4_A1412ContagemResultadoNotificacao_Codigo[0];
            A1413ContagemResultadoNotificacao_UsuCod = P00VE4_A1413ContagemResultadoNotificacao_UsuCod[0];
            A1427ContagemResultadoNotificacao_UsuPesCod = P00VE4_A1427ContagemResultadoNotificacao_UsuPesCod[0];
            n1427ContagemResultadoNotificacao_UsuPesCod = P00VE4_n1427ContagemResultadoNotificacao_UsuPesCod[0];
            A456ContagemResultado_Codigo = P00VE4_A456ContagemResultado_Codigo[0];
            A1422ContagemResultadoNotificacao_UsuNom = P00VE4_A1422ContagemResultadoNotificacao_UsuNom[0];
            n1422ContagemResultadoNotificacao_UsuNom = P00VE4_n1422ContagemResultadoNotificacao_UsuNom[0];
            A1961ContagemResultadoNotificacao_Logged = P00VE4_A1961ContagemResultadoNotificacao_Logged[0];
            n1961ContagemResultadoNotificacao_Logged = P00VE4_n1961ContagemResultadoNotificacao_Logged[0];
            A1960ContagemResultadoNotificacao_Sec = P00VE4_A1960ContagemResultadoNotificacao_Sec[0];
            n1960ContagemResultadoNotificacao_Sec = P00VE4_n1960ContagemResultadoNotificacao_Sec[0];
            A1959ContagemResultadoNotificacao_Aut = P00VE4_A1959ContagemResultadoNotificacao_Aut[0];
            n1959ContagemResultadoNotificacao_Aut = P00VE4_n1959ContagemResultadoNotificacao_Aut[0];
            A1958ContagemResultadoNotificacao_Port = P00VE4_A1958ContagemResultadoNotificacao_Port[0];
            n1958ContagemResultadoNotificacao_Port = P00VE4_n1958ContagemResultadoNotificacao_Port[0];
            A1957ContagemResultadoNotificacao_User = P00VE4_A1957ContagemResultadoNotificacao_User[0];
            n1957ContagemResultadoNotificacao_User = P00VE4_n1957ContagemResultadoNotificacao_User[0];
            A1956ContagemResultadoNotificacao_Host = P00VE4_A1956ContagemResultadoNotificacao_Host[0];
            n1956ContagemResultadoNotificacao_Host = P00VE4_n1956ContagemResultadoNotificacao_Host[0];
            A1417ContagemResultadoNotificacao_Assunto = P00VE4_A1417ContagemResultadoNotificacao_Assunto[0];
            n1417ContagemResultadoNotificacao_Assunto = P00VE4_n1417ContagemResultadoNotificacao_Assunto[0];
            A1416ContagemResultadoNotificacao_DataHora = P00VE4_A1416ContagemResultadoNotificacao_DataHora[0];
            n1416ContagemResultadoNotificacao_DataHora = P00VE4_n1416ContagemResultadoNotificacao_DataHora[0];
            A1963ContagemResultadoNotificacao_Demandas = P00VE4_A1963ContagemResultadoNotificacao_Demandas[0];
            n1963ContagemResultadoNotificacao_Demandas = P00VE4_n1963ContagemResultadoNotificacao_Demandas[0];
            A1962ContagemResultadoNotificacao_Destinatarios = P00VE4_A1962ContagemResultadoNotificacao_Destinatarios[0];
            n1962ContagemResultadoNotificacao_Destinatarios = P00VE4_n1962ContagemResultadoNotificacao_Destinatarios[0];
            A1413ContagemResultadoNotificacao_UsuCod = P00VE4_A1413ContagemResultadoNotificacao_UsuCod[0];
            A1961ContagemResultadoNotificacao_Logged = P00VE4_A1961ContagemResultadoNotificacao_Logged[0];
            n1961ContagemResultadoNotificacao_Logged = P00VE4_n1961ContagemResultadoNotificacao_Logged[0];
            A1960ContagemResultadoNotificacao_Sec = P00VE4_A1960ContagemResultadoNotificacao_Sec[0];
            n1960ContagemResultadoNotificacao_Sec = P00VE4_n1960ContagemResultadoNotificacao_Sec[0];
            A1959ContagemResultadoNotificacao_Aut = P00VE4_A1959ContagemResultadoNotificacao_Aut[0];
            n1959ContagemResultadoNotificacao_Aut = P00VE4_n1959ContagemResultadoNotificacao_Aut[0];
            A1958ContagemResultadoNotificacao_Port = P00VE4_A1958ContagemResultadoNotificacao_Port[0];
            n1958ContagemResultadoNotificacao_Port = P00VE4_n1958ContagemResultadoNotificacao_Port[0];
            A1957ContagemResultadoNotificacao_User = P00VE4_A1957ContagemResultadoNotificacao_User[0];
            n1957ContagemResultadoNotificacao_User = P00VE4_n1957ContagemResultadoNotificacao_User[0];
            A1956ContagemResultadoNotificacao_Host = P00VE4_A1956ContagemResultadoNotificacao_Host[0];
            n1956ContagemResultadoNotificacao_Host = P00VE4_n1956ContagemResultadoNotificacao_Host[0];
            A1417ContagemResultadoNotificacao_Assunto = P00VE4_A1417ContagemResultadoNotificacao_Assunto[0];
            n1417ContagemResultadoNotificacao_Assunto = P00VE4_n1417ContagemResultadoNotificacao_Assunto[0];
            A1416ContagemResultadoNotificacao_DataHora = P00VE4_A1416ContagemResultadoNotificacao_DataHora[0];
            n1416ContagemResultadoNotificacao_DataHora = P00VE4_n1416ContagemResultadoNotificacao_DataHora[0];
            A1427ContagemResultadoNotificacao_UsuPesCod = P00VE4_A1427ContagemResultadoNotificacao_UsuPesCod[0];
            n1427ContagemResultadoNotificacao_UsuPesCod = P00VE4_n1427ContagemResultadoNotificacao_UsuPesCod[0];
            A1422ContagemResultadoNotificacao_UsuNom = P00VE4_A1422ContagemResultadoNotificacao_UsuNom[0];
            n1422ContagemResultadoNotificacao_UsuNom = P00VE4_n1422ContagemResultadoNotificacao_UsuNom[0];
            A1963ContagemResultadoNotificacao_Demandas = P00VE4_A1963ContagemResultadoNotificacao_Demandas[0];
            n1963ContagemResultadoNotificacao_Demandas = P00VE4_n1963ContagemResultadoNotificacao_Demandas[0];
            A1962ContagemResultadoNotificacao_Destinatarios = P00VE4_A1962ContagemResultadoNotificacao_Destinatarios[0];
            n1962ContagemResultadoNotificacao_Destinatarios = P00VE4_n1962ContagemResultadoNotificacao_Destinatarios[0];
            AV41count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00VE4_A456ContagemResultado_Codigo[0] == A456ContagemResultado_Codigo ) && ( StringUtil.StrCmp(P00VE4_A1422ContagemResultadoNotificacao_UsuNom[0], A1422ContagemResultadoNotificacao_UsuNom) == 0 ) )
            {
               BRKVE2 = false;
               A1412ContagemResultadoNotificacao_Codigo = P00VE4_A1412ContagemResultadoNotificacao_Codigo[0];
               A1413ContagemResultadoNotificacao_UsuCod = P00VE4_A1413ContagemResultadoNotificacao_UsuCod[0];
               A1427ContagemResultadoNotificacao_UsuPesCod = P00VE4_A1427ContagemResultadoNotificacao_UsuPesCod[0];
               n1427ContagemResultadoNotificacao_UsuPesCod = P00VE4_n1427ContagemResultadoNotificacao_UsuPesCod[0];
               A1413ContagemResultadoNotificacao_UsuCod = P00VE4_A1413ContagemResultadoNotificacao_UsuCod[0];
               A1427ContagemResultadoNotificacao_UsuPesCod = P00VE4_A1427ContagemResultadoNotificacao_UsuPesCod[0];
               n1427ContagemResultadoNotificacao_UsuPesCod = P00VE4_n1427ContagemResultadoNotificacao_UsuPesCod[0];
               AV41count = (long)(AV41count+1);
               BRKVE2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1422ContagemResultadoNotificacao_UsuNom)) )
            {
               AV33Option = A1422ContagemResultadoNotificacao_UsuNom;
               AV34Options.Add(AV33Option, 0);
               AV39OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV41count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV34Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKVE2 )
            {
               BRKVE2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTAGEMRESULTADONOTIFICACAO_ASSUNTOOPTIONS' Routine */
         AV48TFContagemResultadoNotificacao_Assunto = AV29SearchTxt;
         AV49TFContagemResultadoNotificacao_Assunto_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A1960ContagemResultadoNotificacao_Sec ,
                                              AV26TFContagemResultadoNotificacao_Sec_Sels ,
                                              A1961ContagemResultadoNotificacao_Logged ,
                                              AV28TFContagemResultadoNotificacao_Logged_Sels ,
                                              AV10TFContagemResultadoNotificacao_DataHora ,
                                              AV11TFContagemResultadoNotificacao_DataHora_To ,
                                              AV13TFContagemResultadoNotificacao_UsuNom_Sel ,
                                              AV12TFContagemResultadoNotificacao_UsuNom ,
                                              AV49TFContagemResultadoNotificacao_Assunto_Sel ,
                                              AV48TFContagemResultadoNotificacao_Assunto ,
                                              AV19TFContagemResultadoNotificacao_Host_Sel ,
                                              AV18TFContagemResultadoNotificacao_Host ,
                                              AV21TFContagemResultadoNotificacao_User_Sel ,
                                              AV20TFContagemResultadoNotificacao_User ,
                                              AV22TFContagemResultadoNotificacao_Port ,
                                              AV23TFContagemResultadoNotificacao_Port_To ,
                                              AV24TFContagemResultadoNotificacao_Aut_Sel ,
                                              AV26TFContagemResultadoNotificacao_Sec_Sels.Count ,
                                              AV28TFContagemResultadoNotificacao_Logged_Sels.Count ,
                                              A1416ContagemResultadoNotificacao_DataHora ,
                                              A1422ContagemResultadoNotificacao_UsuNom ,
                                              A1417ContagemResultadoNotificacao_Assunto ,
                                              A1956ContagemResultadoNotificacao_Host ,
                                              A1957ContagemResultadoNotificacao_User ,
                                              A1958ContagemResultadoNotificacao_Port ,
                                              A1959ContagemResultadoNotificacao_Aut ,
                                              AV14TFContagemResultadoNotificacao_Destinatarios ,
                                              A1962ContagemResultadoNotificacao_Destinatarios ,
                                              AV15TFContagemResultadoNotificacao_Destinatarios_To ,
                                              AV16TFContagemResultadoNotificacao_Demandas ,
                                              A1963ContagemResultadoNotificacao_Demandas ,
                                              AV17TFContagemResultadoNotificacao_Demandas_To ,
                                              AV47ContagemResultado_Codigo ,
                                              A456ContagemResultado_Codigo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                              TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV12TFContagemResultadoNotificacao_UsuNom = StringUtil.PadR( StringUtil.RTrim( AV12TFContagemResultadoNotificacao_UsuNom), 100, "%");
         lV48TFContagemResultadoNotificacao_Assunto = StringUtil.Concat( StringUtil.RTrim( AV48TFContagemResultadoNotificacao_Assunto), "%", "");
         lV18TFContagemResultadoNotificacao_Host = StringUtil.PadR( StringUtil.RTrim( AV18TFContagemResultadoNotificacao_Host), 50, "%");
         lV20TFContagemResultadoNotificacao_User = StringUtil.PadR( StringUtil.RTrim( AV20TFContagemResultadoNotificacao_User), 50, "%");
         /* Using cursor P00VE7 */
         pr_default.execute(1, new Object[] {AV47ContagemResultado_Codigo, AV14TFContagemResultadoNotificacao_Destinatarios, AV14TFContagemResultadoNotificacao_Destinatarios, AV15TFContagemResultadoNotificacao_Destinatarios_To, AV15TFContagemResultadoNotificacao_Destinatarios_To, AV16TFContagemResultadoNotificacao_Demandas, AV16TFContagemResultadoNotificacao_Demandas, AV17TFContagemResultadoNotificacao_Demandas_To, AV17TFContagemResultadoNotificacao_Demandas_To, AV10TFContagemResultadoNotificacao_DataHora, AV11TFContagemResultadoNotificacao_DataHora_To, lV12TFContagemResultadoNotificacao_UsuNom, AV13TFContagemResultadoNotificacao_UsuNom_Sel, lV48TFContagemResultadoNotificacao_Assunto, AV49TFContagemResultadoNotificacao_Assunto_Sel, lV18TFContagemResultadoNotificacao_Host, AV19TFContagemResultadoNotificacao_Host_Sel, lV20TFContagemResultadoNotificacao_User, AV21TFContagemResultadoNotificacao_User_Sel, AV22TFContagemResultadoNotificacao_Port, AV23TFContagemResultadoNotificacao_Port_To});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKVE4 = false;
            A1412ContagemResultadoNotificacao_Codigo = P00VE7_A1412ContagemResultadoNotificacao_Codigo[0];
            A1413ContagemResultadoNotificacao_UsuCod = P00VE7_A1413ContagemResultadoNotificacao_UsuCod[0];
            A1427ContagemResultadoNotificacao_UsuPesCod = P00VE7_A1427ContagemResultadoNotificacao_UsuPesCod[0];
            n1427ContagemResultadoNotificacao_UsuPesCod = P00VE7_n1427ContagemResultadoNotificacao_UsuPesCod[0];
            A456ContagemResultado_Codigo = P00VE7_A456ContagemResultado_Codigo[0];
            A1417ContagemResultadoNotificacao_Assunto = P00VE7_A1417ContagemResultadoNotificacao_Assunto[0];
            n1417ContagemResultadoNotificacao_Assunto = P00VE7_n1417ContagemResultadoNotificacao_Assunto[0];
            A1961ContagemResultadoNotificacao_Logged = P00VE7_A1961ContagemResultadoNotificacao_Logged[0];
            n1961ContagemResultadoNotificacao_Logged = P00VE7_n1961ContagemResultadoNotificacao_Logged[0];
            A1960ContagemResultadoNotificacao_Sec = P00VE7_A1960ContagemResultadoNotificacao_Sec[0];
            n1960ContagemResultadoNotificacao_Sec = P00VE7_n1960ContagemResultadoNotificacao_Sec[0];
            A1959ContagemResultadoNotificacao_Aut = P00VE7_A1959ContagemResultadoNotificacao_Aut[0];
            n1959ContagemResultadoNotificacao_Aut = P00VE7_n1959ContagemResultadoNotificacao_Aut[0];
            A1958ContagemResultadoNotificacao_Port = P00VE7_A1958ContagemResultadoNotificacao_Port[0];
            n1958ContagemResultadoNotificacao_Port = P00VE7_n1958ContagemResultadoNotificacao_Port[0];
            A1957ContagemResultadoNotificacao_User = P00VE7_A1957ContagemResultadoNotificacao_User[0];
            n1957ContagemResultadoNotificacao_User = P00VE7_n1957ContagemResultadoNotificacao_User[0];
            A1956ContagemResultadoNotificacao_Host = P00VE7_A1956ContagemResultadoNotificacao_Host[0];
            n1956ContagemResultadoNotificacao_Host = P00VE7_n1956ContagemResultadoNotificacao_Host[0];
            A1422ContagemResultadoNotificacao_UsuNom = P00VE7_A1422ContagemResultadoNotificacao_UsuNom[0];
            n1422ContagemResultadoNotificacao_UsuNom = P00VE7_n1422ContagemResultadoNotificacao_UsuNom[0];
            A1416ContagemResultadoNotificacao_DataHora = P00VE7_A1416ContagemResultadoNotificacao_DataHora[0];
            n1416ContagemResultadoNotificacao_DataHora = P00VE7_n1416ContagemResultadoNotificacao_DataHora[0];
            A1963ContagemResultadoNotificacao_Demandas = P00VE7_A1963ContagemResultadoNotificacao_Demandas[0];
            n1963ContagemResultadoNotificacao_Demandas = P00VE7_n1963ContagemResultadoNotificacao_Demandas[0];
            A1962ContagemResultadoNotificacao_Destinatarios = P00VE7_A1962ContagemResultadoNotificacao_Destinatarios[0];
            n1962ContagemResultadoNotificacao_Destinatarios = P00VE7_n1962ContagemResultadoNotificacao_Destinatarios[0];
            A1413ContagemResultadoNotificacao_UsuCod = P00VE7_A1413ContagemResultadoNotificacao_UsuCod[0];
            A1417ContagemResultadoNotificacao_Assunto = P00VE7_A1417ContagemResultadoNotificacao_Assunto[0];
            n1417ContagemResultadoNotificacao_Assunto = P00VE7_n1417ContagemResultadoNotificacao_Assunto[0];
            A1961ContagemResultadoNotificacao_Logged = P00VE7_A1961ContagemResultadoNotificacao_Logged[0];
            n1961ContagemResultadoNotificacao_Logged = P00VE7_n1961ContagemResultadoNotificacao_Logged[0];
            A1960ContagemResultadoNotificacao_Sec = P00VE7_A1960ContagemResultadoNotificacao_Sec[0];
            n1960ContagemResultadoNotificacao_Sec = P00VE7_n1960ContagemResultadoNotificacao_Sec[0];
            A1959ContagemResultadoNotificacao_Aut = P00VE7_A1959ContagemResultadoNotificacao_Aut[0];
            n1959ContagemResultadoNotificacao_Aut = P00VE7_n1959ContagemResultadoNotificacao_Aut[0];
            A1958ContagemResultadoNotificacao_Port = P00VE7_A1958ContagemResultadoNotificacao_Port[0];
            n1958ContagemResultadoNotificacao_Port = P00VE7_n1958ContagemResultadoNotificacao_Port[0];
            A1957ContagemResultadoNotificacao_User = P00VE7_A1957ContagemResultadoNotificacao_User[0];
            n1957ContagemResultadoNotificacao_User = P00VE7_n1957ContagemResultadoNotificacao_User[0];
            A1956ContagemResultadoNotificacao_Host = P00VE7_A1956ContagemResultadoNotificacao_Host[0];
            n1956ContagemResultadoNotificacao_Host = P00VE7_n1956ContagemResultadoNotificacao_Host[0];
            A1416ContagemResultadoNotificacao_DataHora = P00VE7_A1416ContagemResultadoNotificacao_DataHora[0];
            n1416ContagemResultadoNotificacao_DataHora = P00VE7_n1416ContagemResultadoNotificacao_DataHora[0];
            A1427ContagemResultadoNotificacao_UsuPesCod = P00VE7_A1427ContagemResultadoNotificacao_UsuPesCod[0];
            n1427ContagemResultadoNotificacao_UsuPesCod = P00VE7_n1427ContagemResultadoNotificacao_UsuPesCod[0];
            A1422ContagemResultadoNotificacao_UsuNom = P00VE7_A1422ContagemResultadoNotificacao_UsuNom[0];
            n1422ContagemResultadoNotificacao_UsuNom = P00VE7_n1422ContagemResultadoNotificacao_UsuNom[0];
            A1963ContagemResultadoNotificacao_Demandas = P00VE7_A1963ContagemResultadoNotificacao_Demandas[0];
            n1963ContagemResultadoNotificacao_Demandas = P00VE7_n1963ContagemResultadoNotificacao_Demandas[0];
            A1962ContagemResultadoNotificacao_Destinatarios = P00VE7_A1962ContagemResultadoNotificacao_Destinatarios[0];
            n1962ContagemResultadoNotificacao_Destinatarios = P00VE7_n1962ContagemResultadoNotificacao_Destinatarios[0];
            AV41count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( P00VE7_A456ContagemResultado_Codigo[0] == A456ContagemResultado_Codigo ) && ( StringUtil.StrCmp(P00VE7_A1417ContagemResultadoNotificacao_Assunto[0], A1417ContagemResultadoNotificacao_Assunto) == 0 ) )
            {
               BRKVE4 = false;
               A1412ContagemResultadoNotificacao_Codigo = P00VE7_A1412ContagemResultadoNotificacao_Codigo[0];
               AV41count = (long)(AV41count+1);
               BRKVE4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1417ContagemResultadoNotificacao_Assunto)) )
            {
               AV33Option = A1417ContagemResultadoNotificacao_Assunto;
               AV34Options.Add(AV33Option, 0);
               AV39OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV41count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV34Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKVE4 )
            {
               BRKVE4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADCONTAGEMRESULTADONOTIFICACAO_HOSTOPTIONS' Routine */
         AV18TFContagemResultadoNotificacao_Host = AV29SearchTxt;
         AV19TFContagemResultadoNotificacao_Host_Sel = "";
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A1960ContagemResultadoNotificacao_Sec ,
                                              AV26TFContagemResultadoNotificacao_Sec_Sels ,
                                              A1961ContagemResultadoNotificacao_Logged ,
                                              AV28TFContagemResultadoNotificacao_Logged_Sels ,
                                              AV10TFContagemResultadoNotificacao_DataHora ,
                                              AV11TFContagemResultadoNotificacao_DataHora_To ,
                                              AV13TFContagemResultadoNotificacao_UsuNom_Sel ,
                                              AV12TFContagemResultadoNotificacao_UsuNom ,
                                              AV49TFContagemResultadoNotificacao_Assunto_Sel ,
                                              AV48TFContagemResultadoNotificacao_Assunto ,
                                              AV19TFContagemResultadoNotificacao_Host_Sel ,
                                              AV18TFContagemResultadoNotificacao_Host ,
                                              AV21TFContagemResultadoNotificacao_User_Sel ,
                                              AV20TFContagemResultadoNotificacao_User ,
                                              AV22TFContagemResultadoNotificacao_Port ,
                                              AV23TFContagemResultadoNotificacao_Port_To ,
                                              AV24TFContagemResultadoNotificacao_Aut_Sel ,
                                              AV26TFContagemResultadoNotificacao_Sec_Sels.Count ,
                                              AV28TFContagemResultadoNotificacao_Logged_Sels.Count ,
                                              A1416ContagemResultadoNotificacao_DataHora ,
                                              A1422ContagemResultadoNotificacao_UsuNom ,
                                              A1417ContagemResultadoNotificacao_Assunto ,
                                              A1956ContagemResultadoNotificacao_Host ,
                                              A1957ContagemResultadoNotificacao_User ,
                                              A1958ContagemResultadoNotificacao_Port ,
                                              A1959ContagemResultadoNotificacao_Aut ,
                                              AV14TFContagemResultadoNotificacao_Destinatarios ,
                                              A1962ContagemResultadoNotificacao_Destinatarios ,
                                              AV15TFContagemResultadoNotificacao_Destinatarios_To ,
                                              AV16TFContagemResultadoNotificacao_Demandas ,
                                              A1963ContagemResultadoNotificacao_Demandas ,
                                              AV17TFContagemResultadoNotificacao_Demandas_To ,
                                              AV47ContagemResultado_Codigo ,
                                              A456ContagemResultado_Codigo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                              TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV12TFContagemResultadoNotificacao_UsuNom = StringUtil.PadR( StringUtil.RTrim( AV12TFContagemResultadoNotificacao_UsuNom), 100, "%");
         lV48TFContagemResultadoNotificacao_Assunto = StringUtil.Concat( StringUtil.RTrim( AV48TFContagemResultadoNotificacao_Assunto), "%", "");
         lV18TFContagemResultadoNotificacao_Host = StringUtil.PadR( StringUtil.RTrim( AV18TFContagemResultadoNotificacao_Host), 50, "%");
         lV20TFContagemResultadoNotificacao_User = StringUtil.PadR( StringUtil.RTrim( AV20TFContagemResultadoNotificacao_User), 50, "%");
         /* Using cursor P00VE10 */
         pr_default.execute(2, new Object[] {AV47ContagemResultado_Codigo, AV14TFContagemResultadoNotificacao_Destinatarios, AV14TFContagemResultadoNotificacao_Destinatarios, AV15TFContagemResultadoNotificacao_Destinatarios_To, AV15TFContagemResultadoNotificacao_Destinatarios_To, AV16TFContagemResultadoNotificacao_Demandas, AV16TFContagemResultadoNotificacao_Demandas, AV17TFContagemResultadoNotificacao_Demandas_To, AV17TFContagemResultadoNotificacao_Demandas_To, AV10TFContagemResultadoNotificacao_DataHora, AV11TFContagemResultadoNotificacao_DataHora_To, lV12TFContagemResultadoNotificacao_UsuNom, AV13TFContagemResultadoNotificacao_UsuNom_Sel, lV48TFContagemResultadoNotificacao_Assunto, AV49TFContagemResultadoNotificacao_Assunto_Sel, lV18TFContagemResultadoNotificacao_Host, AV19TFContagemResultadoNotificacao_Host_Sel, lV20TFContagemResultadoNotificacao_User, AV21TFContagemResultadoNotificacao_User_Sel, AV22TFContagemResultadoNotificacao_Port, AV23TFContagemResultadoNotificacao_Port_To});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKVE6 = false;
            A1412ContagemResultadoNotificacao_Codigo = P00VE10_A1412ContagemResultadoNotificacao_Codigo[0];
            A1413ContagemResultadoNotificacao_UsuCod = P00VE10_A1413ContagemResultadoNotificacao_UsuCod[0];
            A1427ContagemResultadoNotificacao_UsuPesCod = P00VE10_A1427ContagemResultadoNotificacao_UsuPesCod[0];
            n1427ContagemResultadoNotificacao_UsuPesCod = P00VE10_n1427ContagemResultadoNotificacao_UsuPesCod[0];
            A456ContagemResultado_Codigo = P00VE10_A456ContagemResultado_Codigo[0];
            A1956ContagemResultadoNotificacao_Host = P00VE10_A1956ContagemResultadoNotificacao_Host[0];
            n1956ContagemResultadoNotificacao_Host = P00VE10_n1956ContagemResultadoNotificacao_Host[0];
            A1961ContagemResultadoNotificacao_Logged = P00VE10_A1961ContagemResultadoNotificacao_Logged[0];
            n1961ContagemResultadoNotificacao_Logged = P00VE10_n1961ContagemResultadoNotificacao_Logged[0];
            A1960ContagemResultadoNotificacao_Sec = P00VE10_A1960ContagemResultadoNotificacao_Sec[0];
            n1960ContagemResultadoNotificacao_Sec = P00VE10_n1960ContagemResultadoNotificacao_Sec[0];
            A1959ContagemResultadoNotificacao_Aut = P00VE10_A1959ContagemResultadoNotificacao_Aut[0];
            n1959ContagemResultadoNotificacao_Aut = P00VE10_n1959ContagemResultadoNotificacao_Aut[0];
            A1958ContagemResultadoNotificacao_Port = P00VE10_A1958ContagemResultadoNotificacao_Port[0];
            n1958ContagemResultadoNotificacao_Port = P00VE10_n1958ContagemResultadoNotificacao_Port[0];
            A1957ContagemResultadoNotificacao_User = P00VE10_A1957ContagemResultadoNotificacao_User[0];
            n1957ContagemResultadoNotificacao_User = P00VE10_n1957ContagemResultadoNotificacao_User[0];
            A1417ContagemResultadoNotificacao_Assunto = P00VE10_A1417ContagemResultadoNotificacao_Assunto[0];
            n1417ContagemResultadoNotificacao_Assunto = P00VE10_n1417ContagemResultadoNotificacao_Assunto[0];
            A1422ContagemResultadoNotificacao_UsuNom = P00VE10_A1422ContagemResultadoNotificacao_UsuNom[0];
            n1422ContagemResultadoNotificacao_UsuNom = P00VE10_n1422ContagemResultadoNotificacao_UsuNom[0];
            A1416ContagemResultadoNotificacao_DataHora = P00VE10_A1416ContagemResultadoNotificacao_DataHora[0];
            n1416ContagemResultadoNotificacao_DataHora = P00VE10_n1416ContagemResultadoNotificacao_DataHora[0];
            A1963ContagemResultadoNotificacao_Demandas = P00VE10_A1963ContagemResultadoNotificacao_Demandas[0];
            n1963ContagemResultadoNotificacao_Demandas = P00VE10_n1963ContagemResultadoNotificacao_Demandas[0];
            A1962ContagemResultadoNotificacao_Destinatarios = P00VE10_A1962ContagemResultadoNotificacao_Destinatarios[0];
            n1962ContagemResultadoNotificacao_Destinatarios = P00VE10_n1962ContagemResultadoNotificacao_Destinatarios[0];
            A1413ContagemResultadoNotificacao_UsuCod = P00VE10_A1413ContagemResultadoNotificacao_UsuCod[0];
            A1956ContagemResultadoNotificacao_Host = P00VE10_A1956ContagemResultadoNotificacao_Host[0];
            n1956ContagemResultadoNotificacao_Host = P00VE10_n1956ContagemResultadoNotificacao_Host[0];
            A1961ContagemResultadoNotificacao_Logged = P00VE10_A1961ContagemResultadoNotificacao_Logged[0];
            n1961ContagemResultadoNotificacao_Logged = P00VE10_n1961ContagemResultadoNotificacao_Logged[0];
            A1960ContagemResultadoNotificacao_Sec = P00VE10_A1960ContagemResultadoNotificacao_Sec[0];
            n1960ContagemResultadoNotificacao_Sec = P00VE10_n1960ContagemResultadoNotificacao_Sec[0];
            A1959ContagemResultadoNotificacao_Aut = P00VE10_A1959ContagemResultadoNotificacao_Aut[0];
            n1959ContagemResultadoNotificacao_Aut = P00VE10_n1959ContagemResultadoNotificacao_Aut[0];
            A1958ContagemResultadoNotificacao_Port = P00VE10_A1958ContagemResultadoNotificacao_Port[0];
            n1958ContagemResultadoNotificacao_Port = P00VE10_n1958ContagemResultadoNotificacao_Port[0];
            A1957ContagemResultadoNotificacao_User = P00VE10_A1957ContagemResultadoNotificacao_User[0];
            n1957ContagemResultadoNotificacao_User = P00VE10_n1957ContagemResultadoNotificacao_User[0];
            A1417ContagemResultadoNotificacao_Assunto = P00VE10_A1417ContagemResultadoNotificacao_Assunto[0];
            n1417ContagemResultadoNotificacao_Assunto = P00VE10_n1417ContagemResultadoNotificacao_Assunto[0];
            A1416ContagemResultadoNotificacao_DataHora = P00VE10_A1416ContagemResultadoNotificacao_DataHora[0];
            n1416ContagemResultadoNotificacao_DataHora = P00VE10_n1416ContagemResultadoNotificacao_DataHora[0];
            A1427ContagemResultadoNotificacao_UsuPesCod = P00VE10_A1427ContagemResultadoNotificacao_UsuPesCod[0];
            n1427ContagemResultadoNotificacao_UsuPesCod = P00VE10_n1427ContagemResultadoNotificacao_UsuPesCod[0];
            A1422ContagemResultadoNotificacao_UsuNom = P00VE10_A1422ContagemResultadoNotificacao_UsuNom[0];
            n1422ContagemResultadoNotificacao_UsuNom = P00VE10_n1422ContagemResultadoNotificacao_UsuNom[0];
            A1963ContagemResultadoNotificacao_Demandas = P00VE10_A1963ContagemResultadoNotificacao_Demandas[0];
            n1963ContagemResultadoNotificacao_Demandas = P00VE10_n1963ContagemResultadoNotificacao_Demandas[0];
            A1962ContagemResultadoNotificacao_Destinatarios = P00VE10_A1962ContagemResultadoNotificacao_Destinatarios[0];
            n1962ContagemResultadoNotificacao_Destinatarios = P00VE10_n1962ContagemResultadoNotificacao_Destinatarios[0];
            AV41count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( P00VE10_A456ContagemResultado_Codigo[0] == A456ContagemResultado_Codigo ) && ( StringUtil.StrCmp(P00VE10_A1956ContagemResultadoNotificacao_Host[0], A1956ContagemResultadoNotificacao_Host) == 0 ) )
            {
               BRKVE6 = false;
               A1412ContagemResultadoNotificacao_Codigo = P00VE10_A1412ContagemResultadoNotificacao_Codigo[0];
               AV41count = (long)(AV41count+1);
               BRKVE6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1956ContagemResultadoNotificacao_Host)) )
            {
               AV33Option = A1956ContagemResultadoNotificacao_Host;
               AV34Options.Add(AV33Option, 0);
               AV39OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV41count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV34Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKVE6 )
            {
               BRKVE6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      protected void S151( )
      {
         /* 'LOADCONTAGEMRESULTADONOTIFICACAO_USEROPTIONS' Routine */
         AV20TFContagemResultadoNotificacao_User = AV29SearchTxt;
         AV21TFContagemResultadoNotificacao_User_Sel = "";
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              A1960ContagemResultadoNotificacao_Sec ,
                                              AV26TFContagemResultadoNotificacao_Sec_Sels ,
                                              A1961ContagemResultadoNotificacao_Logged ,
                                              AV28TFContagemResultadoNotificacao_Logged_Sels ,
                                              AV10TFContagemResultadoNotificacao_DataHora ,
                                              AV11TFContagemResultadoNotificacao_DataHora_To ,
                                              AV13TFContagemResultadoNotificacao_UsuNom_Sel ,
                                              AV12TFContagemResultadoNotificacao_UsuNom ,
                                              AV49TFContagemResultadoNotificacao_Assunto_Sel ,
                                              AV48TFContagemResultadoNotificacao_Assunto ,
                                              AV19TFContagemResultadoNotificacao_Host_Sel ,
                                              AV18TFContagemResultadoNotificacao_Host ,
                                              AV21TFContagemResultadoNotificacao_User_Sel ,
                                              AV20TFContagemResultadoNotificacao_User ,
                                              AV22TFContagemResultadoNotificacao_Port ,
                                              AV23TFContagemResultadoNotificacao_Port_To ,
                                              AV24TFContagemResultadoNotificacao_Aut_Sel ,
                                              AV26TFContagemResultadoNotificacao_Sec_Sels.Count ,
                                              AV28TFContagemResultadoNotificacao_Logged_Sels.Count ,
                                              A1416ContagemResultadoNotificacao_DataHora ,
                                              A1422ContagemResultadoNotificacao_UsuNom ,
                                              A1417ContagemResultadoNotificacao_Assunto ,
                                              A1956ContagemResultadoNotificacao_Host ,
                                              A1957ContagemResultadoNotificacao_User ,
                                              A1958ContagemResultadoNotificacao_Port ,
                                              A1959ContagemResultadoNotificacao_Aut ,
                                              AV14TFContagemResultadoNotificacao_Destinatarios ,
                                              A1962ContagemResultadoNotificacao_Destinatarios ,
                                              AV15TFContagemResultadoNotificacao_Destinatarios_To ,
                                              AV16TFContagemResultadoNotificacao_Demandas ,
                                              A1963ContagemResultadoNotificacao_Demandas ,
                                              AV17TFContagemResultadoNotificacao_Demandas_To ,
                                              AV47ContagemResultado_Codigo ,
                                              A456ContagemResultado_Codigo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                              TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV12TFContagemResultadoNotificacao_UsuNom = StringUtil.PadR( StringUtil.RTrim( AV12TFContagemResultadoNotificacao_UsuNom), 100, "%");
         lV48TFContagemResultadoNotificacao_Assunto = StringUtil.Concat( StringUtil.RTrim( AV48TFContagemResultadoNotificacao_Assunto), "%", "");
         lV18TFContagemResultadoNotificacao_Host = StringUtil.PadR( StringUtil.RTrim( AV18TFContagemResultadoNotificacao_Host), 50, "%");
         lV20TFContagemResultadoNotificacao_User = StringUtil.PadR( StringUtil.RTrim( AV20TFContagemResultadoNotificacao_User), 50, "%");
         /* Using cursor P00VE13 */
         pr_default.execute(3, new Object[] {AV47ContagemResultado_Codigo, AV14TFContagemResultadoNotificacao_Destinatarios, AV14TFContagemResultadoNotificacao_Destinatarios, AV15TFContagemResultadoNotificacao_Destinatarios_To, AV15TFContagemResultadoNotificacao_Destinatarios_To, AV16TFContagemResultadoNotificacao_Demandas, AV16TFContagemResultadoNotificacao_Demandas, AV17TFContagemResultadoNotificacao_Demandas_To, AV17TFContagemResultadoNotificacao_Demandas_To, AV10TFContagemResultadoNotificacao_DataHora, AV11TFContagemResultadoNotificacao_DataHora_To, lV12TFContagemResultadoNotificacao_UsuNom, AV13TFContagemResultadoNotificacao_UsuNom_Sel, lV48TFContagemResultadoNotificacao_Assunto, AV49TFContagemResultadoNotificacao_Assunto_Sel, lV18TFContagemResultadoNotificacao_Host, AV19TFContagemResultadoNotificacao_Host_Sel, lV20TFContagemResultadoNotificacao_User, AV21TFContagemResultadoNotificacao_User_Sel, AV22TFContagemResultadoNotificacao_Port, AV23TFContagemResultadoNotificacao_Port_To});
         while ( (pr_default.getStatus(3) != 101) )
         {
            BRKVE8 = false;
            A1412ContagemResultadoNotificacao_Codigo = P00VE13_A1412ContagemResultadoNotificacao_Codigo[0];
            A1413ContagemResultadoNotificacao_UsuCod = P00VE13_A1413ContagemResultadoNotificacao_UsuCod[0];
            A1427ContagemResultadoNotificacao_UsuPesCod = P00VE13_A1427ContagemResultadoNotificacao_UsuPesCod[0];
            n1427ContagemResultadoNotificacao_UsuPesCod = P00VE13_n1427ContagemResultadoNotificacao_UsuPesCod[0];
            A456ContagemResultado_Codigo = P00VE13_A456ContagemResultado_Codigo[0];
            A1957ContagemResultadoNotificacao_User = P00VE13_A1957ContagemResultadoNotificacao_User[0];
            n1957ContagemResultadoNotificacao_User = P00VE13_n1957ContagemResultadoNotificacao_User[0];
            A1961ContagemResultadoNotificacao_Logged = P00VE13_A1961ContagemResultadoNotificacao_Logged[0];
            n1961ContagemResultadoNotificacao_Logged = P00VE13_n1961ContagemResultadoNotificacao_Logged[0];
            A1960ContagemResultadoNotificacao_Sec = P00VE13_A1960ContagemResultadoNotificacao_Sec[0];
            n1960ContagemResultadoNotificacao_Sec = P00VE13_n1960ContagemResultadoNotificacao_Sec[0];
            A1959ContagemResultadoNotificacao_Aut = P00VE13_A1959ContagemResultadoNotificacao_Aut[0];
            n1959ContagemResultadoNotificacao_Aut = P00VE13_n1959ContagemResultadoNotificacao_Aut[0];
            A1958ContagemResultadoNotificacao_Port = P00VE13_A1958ContagemResultadoNotificacao_Port[0];
            n1958ContagemResultadoNotificacao_Port = P00VE13_n1958ContagemResultadoNotificacao_Port[0];
            A1956ContagemResultadoNotificacao_Host = P00VE13_A1956ContagemResultadoNotificacao_Host[0];
            n1956ContagemResultadoNotificacao_Host = P00VE13_n1956ContagemResultadoNotificacao_Host[0];
            A1417ContagemResultadoNotificacao_Assunto = P00VE13_A1417ContagemResultadoNotificacao_Assunto[0];
            n1417ContagemResultadoNotificacao_Assunto = P00VE13_n1417ContagemResultadoNotificacao_Assunto[0];
            A1422ContagemResultadoNotificacao_UsuNom = P00VE13_A1422ContagemResultadoNotificacao_UsuNom[0];
            n1422ContagemResultadoNotificacao_UsuNom = P00VE13_n1422ContagemResultadoNotificacao_UsuNom[0];
            A1416ContagemResultadoNotificacao_DataHora = P00VE13_A1416ContagemResultadoNotificacao_DataHora[0];
            n1416ContagemResultadoNotificacao_DataHora = P00VE13_n1416ContagemResultadoNotificacao_DataHora[0];
            A1963ContagemResultadoNotificacao_Demandas = P00VE13_A1963ContagemResultadoNotificacao_Demandas[0];
            n1963ContagemResultadoNotificacao_Demandas = P00VE13_n1963ContagemResultadoNotificacao_Demandas[0];
            A1962ContagemResultadoNotificacao_Destinatarios = P00VE13_A1962ContagemResultadoNotificacao_Destinatarios[0];
            n1962ContagemResultadoNotificacao_Destinatarios = P00VE13_n1962ContagemResultadoNotificacao_Destinatarios[0];
            A1413ContagemResultadoNotificacao_UsuCod = P00VE13_A1413ContagemResultadoNotificacao_UsuCod[0];
            A1957ContagemResultadoNotificacao_User = P00VE13_A1957ContagemResultadoNotificacao_User[0];
            n1957ContagemResultadoNotificacao_User = P00VE13_n1957ContagemResultadoNotificacao_User[0];
            A1961ContagemResultadoNotificacao_Logged = P00VE13_A1961ContagemResultadoNotificacao_Logged[0];
            n1961ContagemResultadoNotificacao_Logged = P00VE13_n1961ContagemResultadoNotificacao_Logged[0];
            A1960ContagemResultadoNotificacao_Sec = P00VE13_A1960ContagemResultadoNotificacao_Sec[0];
            n1960ContagemResultadoNotificacao_Sec = P00VE13_n1960ContagemResultadoNotificacao_Sec[0];
            A1959ContagemResultadoNotificacao_Aut = P00VE13_A1959ContagemResultadoNotificacao_Aut[0];
            n1959ContagemResultadoNotificacao_Aut = P00VE13_n1959ContagemResultadoNotificacao_Aut[0];
            A1958ContagemResultadoNotificacao_Port = P00VE13_A1958ContagemResultadoNotificacao_Port[0];
            n1958ContagemResultadoNotificacao_Port = P00VE13_n1958ContagemResultadoNotificacao_Port[0];
            A1956ContagemResultadoNotificacao_Host = P00VE13_A1956ContagemResultadoNotificacao_Host[0];
            n1956ContagemResultadoNotificacao_Host = P00VE13_n1956ContagemResultadoNotificacao_Host[0];
            A1417ContagemResultadoNotificacao_Assunto = P00VE13_A1417ContagemResultadoNotificacao_Assunto[0];
            n1417ContagemResultadoNotificacao_Assunto = P00VE13_n1417ContagemResultadoNotificacao_Assunto[0];
            A1416ContagemResultadoNotificacao_DataHora = P00VE13_A1416ContagemResultadoNotificacao_DataHora[0];
            n1416ContagemResultadoNotificacao_DataHora = P00VE13_n1416ContagemResultadoNotificacao_DataHora[0];
            A1427ContagemResultadoNotificacao_UsuPesCod = P00VE13_A1427ContagemResultadoNotificacao_UsuPesCod[0];
            n1427ContagemResultadoNotificacao_UsuPesCod = P00VE13_n1427ContagemResultadoNotificacao_UsuPesCod[0];
            A1422ContagemResultadoNotificacao_UsuNom = P00VE13_A1422ContagemResultadoNotificacao_UsuNom[0];
            n1422ContagemResultadoNotificacao_UsuNom = P00VE13_n1422ContagemResultadoNotificacao_UsuNom[0];
            A1963ContagemResultadoNotificacao_Demandas = P00VE13_A1963ContagemResultadoNotificacao_Demandas[0];
            n1963ContagemResultadoNotificacao_Demandas = P00VE13_n1963ContagemResultadoNotificacao_Demandas[0];
            A1962ContagemResultadoNotificacao_Destinatarios = P00VE13_A1962ContagemResultadoNotificacao_Destinatarios[0];
            n1962ContagemResultadoNotificacao_Destinatarios = P00VE13_n1962ContagemResultadoNotificacao_Destinatarios[0];
            AV41count = 0;
            while ( (pr_default.getStatus(3) != 101) && ( P00VE13_A456ContagemResultado_Codigo[0] == A456ContagemResultado_Codigo ) && ( StringUtil.StrCmp(P00VE13_A1957ContagemResultadoNotificacao_User[0], A1957ContagemResultadoNotificacao_User) == 0 ) )
            {
               BRKVE8 = false;
               A1412ContagemResultadoNotificacao_Codigo = P00VE13_A1412ContagemResultadoNotificacao_Codigo[0];
               AV41count = (long)(AV41count+1);
               BRKVE8 = true;
               pr_default.readNext(3);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1957ContagemResultadoNotificacao_User)) )
            {
               AV33Option = A1957ContagemResultadoNotificacao_User;
               AV34Options.Add(AV33Option, 0);
               AV39OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV41count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV34Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKVE8 )
            {
               BRKVE8 = true;
               pr_default.readNext(3);
            }
         }
         pr_default.close(3);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV34Options = new GxSimpleCollection();
         AV37OptionsDesc = new GxSimpleCollection();
         AV39OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV42Session = context.GetSession();
         AV44GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV45GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContagemResultadoNotificacao_DataHora = (DateTime)(DateTime.MinValue);
         AV11TFContagemResultadoNotificacao_DataHora_To = (DateTime)(DateTime.MinValue);
         AV12TFContagemResultadoNotificacao_UsuNom = "";
         AV13TFContagemResultadoNotificacao_UsuNom_Sel = "";
         AV48TFContagemResultadoNotificacao_Assunto = "";
         AV49TFContagemResultadoNotificacao_Assunto_Sel = "";
         AV18TFContagemResultadoNotificacao_Host = "";
         AV19TFContagemResultadoNotificacao_Host_Sel = "";
         AV20TFContagemResultadoNotificacao_User = "";
         AV21TFContagemResultadoNotificacao_User_Sel = "";
         AV25TFContagemResultadoNotificacao_Sec_SelsJson = "";
         AV26TFContagemResultadoNotificacao_Sec_Sels = new GxSimpleCollection();
         AV27TFContagemResultadoNotificacao_Logged_SelsJson = "";
         AV28TFContagemResultadoNotificacao_Logged_Sels = new GxSimpleCollection();
         scmdbuf = "";
         lV12TFContagemResultadoNotificacao_UsuNom = "";
         lV48TFContagemResultadoNotificacao_Assunto = "";
         lV18TFContagemResultadoNotificacao_Host = "";
         lV20TFContagemResultadoNotificacao_User = "";
         A1416ContagemResultadoNotificacao_DataHora = (DateTime)(DateTime.MinValue);
         A1422ContagemResultadoNotificacao_UsuNom = "";
         A1417ContagemResultadoNotificacao_Assunto = "";
         A1956ContagemResultadoNotificacao_Host = "";
         A1957ContagemResultadoNotificacao_User = "";
         P00VE4_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         P00VE4_A1413ContagemResultadoNotificacao_UsuCod = new int[1] ;
         P00VE4_A1427ContagemResultadoNotificacao_UsuPesCod = new int[1] ;
         P00VE4_n1427ContagemResultadoNotificacao_UsuPesCod = new bool[] {false} ;
         P00VE4_A456ContagemResultado_Codigo = new int[1] ;
         P00VE4_A1422ContagemResultadoNotificacao_UsuNom = new String[] {""} ;
         P00VE4_n1422ContagemResultadoNotificacao_UsuNom = new bool[] {false} ;
         P00VE4_A1961ContagemResultadoNotificacao_Logged = new short[1] ;
         P00VE4_n1961ContagemResultadoNotificacao_Logged = new bool[] {false} ;
         P00VE4_A1960ContagemResultadoNotificacao_Sec = new short[1] ;
         P00VE4_n1960ContagemResultadoNotificacao_Sec = new bool[] {false} ;
         P00VE4_A1959ContagemResultadoNotificacao_Aut = new bool[] {false} ;
         P00VE4_n1959ContagemResultadoNotificacao_Aut = new bool[] {false} ;
         P00VE4_A1958ContagemResultadoNotificacao_Port = new short[1] ;
         P00VE4_n1958ContagemResultadoNotificacao_Port = new bool[] {false} ;
         P00VE4_A1957ContagemResultadoNotificacao_User = new String[] {""} ;
         P00VE4_n1957ContagemResultadoNotificacao_User = new bool[] {false} ;
         P00VE4_A1956ContagemResultadoNotificacao_Host = new String[] {""} ;
         P00VE4_n1956ContagemResultadoNotificacao_Host = new bool[] {false} ;
         P00VE4_A1417ContagemResultadoNotificacao_Assunto = new String[] {""} ;
         P00VE4_n1417ContagemResultadoNotificacao_Assunto = new bool[] {false} ;
         P00VE4_A1416ContagemResultadoNotificacao_DataHora = new DateTime[] {DateTime.MinValue} ;
         P00VE4_n1416ContagemResultadoNotificacao_DataHora = new bool[] {false} ;
         P00VE4_A1963ContagemResultadoNotificacao_Demandas = new int[1] ;
         P00VE4_n1963ContagemResultadoNotificacao_Demandas = new bool[] {false} ;
         P00VE4_A1962ContagemResultadoNotificacao_Destinatarios = new int[1] ;
         P00VE4_n1962ContagemResultadoNotificacao_Destinatarios = new bool[] {false} ;
         AV33Option = "";
         P00VE7_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         P00VE7_A1413ContagemResultadoNotificacao_UsuCod = new int[1] ;
         P00VE7_A1427ContagemResultadoNotificacao_UsuPesCod = new int[1] ;
         P00VE7_n1427ContagemResultadoNotificacao_UsuPesCod = new bool[] {false} ;
         P00VE7_A456ContagemResultado_Codigo = new int[1] ;
         P00VE7_A1417ContagemResultadoNotificacao_Assunto = new String[] {""} ;
         P00VE7_n1417ContagemResultadoNotificacao_Assunto = new bool[] {false} ;
         P00VE7_A1961ContagemResultadoNotificacao_Logged = new short[1] ;
         P00VE7_n1961ContagemResultadoNotificacao_Logged = new bool[] {false} ;
         P00VE7_A1960ContagemResultadoNotificacao_Sec = new short[1] ;
         P00VE7_n1960ContagemResultadoNotificacao_Sec = new bool[] {false} ;
         P00VE7_A1959ContagemResultadoNotificacao_Aut = new bool[] {false} ;
         P00VE7_n1959ContagemResultadoNotificacao_Aut = new bool[] {false} ;
         P00VE7_A1958ContagemResultadoNotificacao_Port = new short[1] ;
         P00VE7_n1958ContagemResultadoNotificacao_Port = new bool[] {false} ;
         P00VE7_A1957ContagemResultadoNotificacao_User = new String[] {""} ;
         P00VE7_n1957ContagemResultadoNotificacao_User = new bool[] {false} ;
         P00VE7_A1956ContagemResultadoNotificacao_Host = new String[] {""} ;
         P00VE7_n1956ContagemResultadoNotificacao_Host = new bool[] {false} ;
         P00VE7_A1422ContagemResultadoNotificacao_UsuNom = new String[] {""} ;
         P00VE7_n1422ContagemResultadoNotificacao_UsuNom = new bool[] {false} ;
         P00VE7_A1416ContagemResultadoNotificacao_DataHora = new DateTime[] {DateTime.MinValue} ;
         P00VE7_n1416ContagemResultadoNotificacao_DataHora = new bool[] {false} ;
         P00VE7_A1963ContagemResultadoNotificacao_Demandas = new int[1] ;
         P00VE7_n1963ContagemResultadoNotificacao_Demandas = new bool[] {false} ;
         P00VE7_A1962ContagemResultadoNotificacao_Destinatarios = new int[1] ;
         P00VE7_n1962ContagemResultadoNotificacao_Destinatarios = new bool[] {false} ;
         P00VE10_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         P00VE10_A1413ContagemResultadoNotificacao_UsuCod = new int[1] ;
         P00VE10_A1427ContagemResultadoNotificacao_UsuPesCod = new int[1] ;
         P00VE10_n1427ContagemResultadoNotificacao_UsuPesCod = new bool[] {false} ;
         P00VE10_A456ContagemResultado_Codigo = new int[1] ;
         P00VE10_A1956ContagemResultadoNotificacao_Host = new String[] {""} ;
         P00VE10_n1956ContagemResultadoNotificacao_Host = new bool[] {false} ;
         P00VE10_A1961ContagemResultadoNotificacao_Logged = new short[1] ;
         P00VE10_n1961ContagemResultadoNotificacao_Logged = new bool[] {false} ;
         P00VE10_A1960ContagemResultadoNotificacao_Sec = new short[1] ;
         P00VE10_n1960ContagemResultadoNotificacao_Sec = new bool[] {false} ;
         P00VE10_A1959ContagemResultadoNotificacao_Aut = new bool[] {false} ;
         P00VE10_n1959ContagemResultadoNotificacao_Aut = new bool[] {false} ;
         P00VE10_A1958ContagemResultadoNotificacao_Port = new short[1] ;
         P00VE10_n1958ContagemResultadoNotificacao_Port = new bool[] {false} ;
         P00VE10_A1957ContagemResultadoNotificacao_User = new String[] {""} ;
         P00VE10_n1957ContagemResultadoNotificacao_User = new bool[] {false} ;
         P00VE10_A1417ContagemResultadoNotificacao_Assunto = new String[] {""} ;
         P00VE10_n1417ContagemResultadoNotificacao_Assunto = new bool[] {false} ;
         P00VE10_A1422ContagemResultadoNotificacao_UsuNom = new String[] {""} ;
         P00VE10_n1422ContagemResultadoNotificacao_UsuNom = new bool[] {false} ;
         P00VE10_A1416ContagemResultadoNotificacao_DataHora = new DateTime[] {DateTime.MinValue} ;
         P00VE10_n1416ContagemResultadoNotificacao_DataHora = new bool[] {false} ;
         P00VE10_A1963ContagemResultadoNotificacao_Demandas = new int[1] ;
         P00VE10_n1963ContagemResultadoNotificacao_Demandas = new bool[] {false} ;
         P00VE10_A1962ContagemResultadoNotificacao_Destinatarios = new int[1] ;
         P00VE10_n1962ContagemResultadoNotificacao_Destinatarios = new bool[] {false} ;
         P00VE13_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         P00VE13_A1413ContagemResultadoNotificacao_UsuCod = new int[1] ;
         P00VE13_A1427ContagemResultadoNotificacao_UsuPesCod = new int[1] ;
         P00VE13_n1427ContagemResultadoNotificacao_UsuPesCod = new bool[] {false} ;
         P00VE13_A456ContagemResultado_Codigo = new int[1] ;
         P00VE13_A1957ContagemResultadoNotificacao_User = new String[] {""} ;
         P00VE13_n1957ContagemResultadoNotificacao_User = new bool[] {false} ;
         P00VE13_A1961ContagemResultadoNotificacao_Logged = new short[1] ;
         P00VE13_n1961ContagemResultadoNotificacao_Logged = new bool[] {false} ;
         P00VE13_A1960ContagemResultadoNotificacao_Sec = new short[1] ;
         P00VE13_n1960ContagemResultadoNotificacao_Sec = new bool[] {false} ;
         P00VE13_A1959ContagemResultadoNotificacao_Aut = new bool[] {false} ;
         P00VE13_n1959ContagemResultadoNotificacao_Aut = new bool[] {false} ;
         P00VE13_A1958ContagemResultadoNotificacao_Port = new short[1] ;
         P00VE13_n1958ContagemResultadoNotificacao_Port = new bool[] {false} ;
         P00VE13_A1956ContagemResultadoNotificacao_Host = new String[] {""} ;
         P00VE13_n1956ContagemResultadoNotificacao_Host = new bool[] {false} ;
         P00VE13_A1417ContagemResultadoNotificacao_Assunto = new String[] {""} ;
         P00VE13_n1417ContagemResultadoNotificacao_Assunto = new bool[] {false} ;
         P00VE13_A1422ContagemResultadoNotificacao_UsuNom = new String[] {""} ;
         P00VE13_n1422ContagemResultadoNotificacao_UsuNom = new bool[] {false} ;
         P00VE13_A1416ContagemResultadoNotificacao_DataHora = new DateTime[] {DateTime.MinValue} ;
         P00VE13_n1416ContagemResultadoNotificacao_DataHora = new bool[] {false} ;
         P00VE13_A1963ContagemResultadoNotificacao_Demandas = new int[1] ;
         P00VE13_n1963ContagemResultadoNotificacao_Demandas = new bool[] {false} ;
         P00VE13_A1962ContagemResultadoNotificacao_Destinatarios = new int[1] ;
         P00VE13_n1962ContagemResultadoNotificacao_Destinatarios = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getdemandanotificacoeswcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00VE4_A1412ContagemResultadoNotificacao_Codigo, P00VE4_A1413ContagemResultadoNotificacao_UsuCod, P00VE4_A1427ContagemResultadoNotificacao_UsuPesCod, P00VE4_n1427ContagemResultadoNotificacao_UsuPesCod, P00VE4_A456ContagemResultado_Codigo, P00VE4_A1422ContagemResultadoNotificacao_UsuNom, P00VE4_n1422ContagemResultadoNotificacao_UsuNom, P00VE4_A1961ContagemResultadoNotificacao_Logged, P00VE4_n1961ContagemResultadoNotificacao_Logged, P00VE4_A1960ContagemResultadoNotificacao_Sec,
               P00VE4_n1960ContagemResultadoNotificacao_Sec, P00VE4_A1959ContagemResultadoNotificacao_Aut, P00VE4_n1959ContagemResultadoNotificacao_Aut, P00VE4_A1958ContagemResultadoNotificacao_Port, P00VE4_n1958ContagemResultadoNotificacao_Port, P00VE4_A1957ContagemResultadoNotificacao_User, P00VE4_n1957ContagemResultadoNotificacao_User, P00VE4_A1956ContagemResultadoNotificacao_Host, P00VE4_n1956ContagemResultadoNotificacao_Host, P00VE4_A1417ContagemResultadoNotificacao_Assunto,
               P00VE4_n1417ContagemResultadoNotificacao_Assunto, P00VE4_A1416ContagemResultadoNotificacao_DataHora, P00VE4_n1416ContagemResultadoNotificacao_DataHora, P00VE4_A1963ContagemResultadoNotificacao_Demandas, P00VE4_n1963ContagemResultadoNotificacao_Demandas, P00VE4_A1962ContagemResultadoNotificacao_Destinatarios, P00VE4_n1962ContagemResultadoNotificacao_Destinatarios
               }
               , new Object[] {
               P00VE7_A1412ContagemResultadoNotificacao_Codigo, P00VE7_A1413ContagemResultadoNotificacao_UsuCod, P00VE7_A1427ContagemResultadoNotificacao_UsuPesCod, P00VE7_n1427ContagemResultadoNotificacao_UsuPesCod, P00VE7_A456ContagemResultado_Codigo, P00VE7_A1417ContagemResultadoNotificacao_Assunto, P00VE7_n1417ContagemResultadoNotificacao_Assunto, P00VE7_A1961ContagemResultadoNotificacao_Logged, P00VE7_n1961ContagemResultadoNotificacao_Logged, P00VE7_A1960ContagemResultadoNotificacao_Sec,
               P00VE7_n1960ContagemResultadoNotificacao_Sec, P00VE7_A1959ContagemResultadoNotificacao_Aut, P00VE7_n1959ContagemResultadoNotificacao_Aut, P00VE7_A1958ContagemResultadoNotificacao_Port, P00VE7_n1958ContagemResultadoNotificacao_Port, P00VE7_A1957ContagemResultadoNotificacao_User, P00VE7_n1957ContagemResultadoNotificacao_User, P00VE7_A1956ContagemResultadoNotificacao_Host, P00VE7_n1956ContagemResultadoNotificacao_Host, P00VE7_A1422ContagemResultadoNotificacao_UsuNom,
               P00VE7_n1422ContagemResultadoNotificacao_UsuNom, P00VE7_A1416ContagemResultadoNotificacao_DataHora, P00VE7_n1416ContagemResultadoNotificacao_DataHora, P00VE7_A1963ContagemResultadoNotificacao_Demandas, P00VE7_n1963ContagemResultadoNotificacao_Demandas, P00VE7_A1962ContagemResultadoNotificacao_Destinatarios, P00VE7_n1962ContagemResultadoNotificacao_Destinatarios
               }
               , new Object[] {
               P00VE10_A1412ContagemResultadoNotificacao_Codigo, P00VE10_A1413ContagemResultadoNotificacao_UsuCod, P00VE10_A1427ContagemResultadoNotificacao_UsuPesCod, P00VE10_n1427ContagemResultadoNotificacao_UsuPesCod, P00VE10_A456ContagemResultado_Codigo, P00VE10_A1956ContagemResultadoNotificacao_Host, P00VE10_n1956ContagemResultadoNotificacao_Host, P00VE10_A1961ContagemResultadoNotificacao_Logged, P00VE10_n1961ContagemResultadoNotificacao_Logged, P00VE10_A1960ContagemResultadoNotificacao_Sec,
               P00VE10_n1960ContagemResultadoNotificacao_Sec, P00VE10_A1959ContagemResultadoNotificacao_Aut, P00VE10_n1959ContagemResultadoNotificacao_Aut, P00VE10_A1958ContagemResultadoNotificacao_Port, P00VE10_n1958ContagemResultadoNotificacao_Port, P00VE10_A1957ContagemResultadoNotificacao_User, P00VE10_n1957ContagemResultadoNotificacao_User, P00VE10_A1417ContagemResultadoNotificacao_Assunto, P00VE10_n1417ContagemResultadoNotificacao_Assunto, P00VE10_A1422ContagemResultadoNotificacao_UsuNom,
               P00VE10_n1422ContagemResultadoNotificacao_UsuNom, P00VE10_A1416ContagemResultadoNotificacao_DataHora, P00VE10_n1416ContagemResultadoNotificacao_DataHora, P00VE10_A1963ContagemResultadoNotificacao_Demandas, P00VE10_n1963ContagemResultadoNotificacao_Demandas, P00VE10_A1962ContagemResultadoNotificacao_Destinatarios, P00VE10_n1962ContagemResultadoNotificacao_Destinatarios
               }
               , new Object[] {
               P00VE13_A1412ContagemResultadoNotificacao_Codigo, P00VE13_A1413ContagemResultadoNotificacao_UsuCod, P00VE13_A1427ContagemResultadoNotificacao_UsuPesCod, P00VE13_n1427ContagemResultadoNotificacao_UsuPesCod, P00VE13_A456ContagemResultado_Codigo, P00VE13_A1957ContagemResultadoNotificacao_User, P00VE13_n1957ContagemResultadoNotificacao_User, P00VE13_A1961ContagemResultadoNotificacao_Logged, P00VE13_n1961ContagemResultadoNotificacao_Logged, P00VE13_A1960ContagemResultadoNotificacao_Sec,
               P00VE13_n1960ContagemResultadoNotificacao_Sec, P00VE13_A1959ContagemResultadoNotificacao_Aut, P00VE13_n1959ContagemResultadoNotificacao_Aut, P00VE13_A1958ContagemResultadoNotificacao_Port, P00VE13_n1958ContagemResultadoNotificacao_Port, P00VE13_A1956ContagemResultadoNotificacao_Host, P00VE13_n1956ContagemResultadoNotificacao_Host, P00VE13_A1417ContagemResultadoNotificacao_Assunto, P00VE13_n1417ContagemResultadoNotificacao_Assunto, P00VE13_A1422ContagemResultadoNotificacao_UsuNom,
               P00VE13_n1422ContagemResultadoNotificacao_UsuNom, P00VE13_A1416ContagemResultadoNotificacao_DataHora, P00VE13_n1416ContagemResultadoNotificacao_DataHora, P00VE13_A1963ContagemResultadoNotificacao_Demandas, P00VE13_n1963ContagemResultadoNotificacao_Demandas, P00VE13_A1962ContagemResultadoNotificacao_Destinatarios, P00VE13_n1962ContagemResultadoNotificacao_Destinatarios
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV22TFContagemResultadoNotificacao_Port ;
      private short AV23TFContagemResultadoNotificacao_Port_To ;
      private short AV24TFContagemResultadoNotificacao_Aut_Sel ;
      private short A1960ContagemResultadoNotificacao_Sec ;
      private short A1961ContagemResultadoNotificacao_Logged ;
      private short A1958ContagemResultadoNotificacao_Port ;
      private int AV52GXV1 ;
      private int AV14TFContagemResultadoNotificacao_Destinatarios ;
      private int AV15TFContagemResultadoNotificacao_Destinatarios_To ;
      private int AV16TFContagemResultadoNotificacao_Demandas ;
      private int AV17TFContagemResultadoNotificacao_Demandas_To ;
      private int AV47ContagemResultado_Codigo ;
      private int AV26TFContagemResultadoNotificacao_Sec_Sels_Count ;
      private int AV28TFContagemResultadoNotificacao_Logged_Sels_Count ;
      private int A1962ContagemResultadoNotificacao_Destinatarios ;
      private int A1963ContagemResultadoNotificacao_Demandas ;
      private int A456ContagemResultado_Codigo ;
      private int A1413ContagemResultadoNotificacao_UsuCod ;
      private int A1427ContagemResultadoNotificacao_UsuPesCod ;
      private long A1412ContagemResultadoNotificacao_Codigo ;
      private long AV41count ;
      private String AV12TFContagemResultadoNotificacao_UsuNom ;
      private String AV13TFContagemResultadoNotificacao_UsuNom_Sel ;
      private String AV18TFContagemResultadoNotificacao_Host ;
      private String AV19TFContagemResultadoNotificacao_Host_Sel ;
      private String AV20TFContagemResultadoNotificacao_User ;
      private String AV21TFContagemResultadoNotificacao_User_Sel ;
      private String scmdbuf ;
      private String lV12TFContagemResultadoNotificacao_UsuNom ;
      private String lV18TFContagemResultadoNotificacao_Host ;
      private String lV20TFContagemResultadoNotificacao_User ;
      private String A1422ContagemResultadoNotificacao_UsuNom ;
      private String A1956ContagemResultadoNotificacao_Host ;
      private String A1957ContagemResultadoNotificacao_User ;
      private DateTime AV10TFContagemResultadoNotificacao_DataHora ;
      private DateTime AV11TFContagemResultadoNotificacao_DataHora_To ;
      private DateTime A1416ContagemResultadoNotificacao_DataHora ;
      private bool returnInSub ;
      private bool A1959ContagemResultadoNotificacao_Aut ;
      private bool BRKVE2 ;
      private bool n1427ContagemResultadoNotificacao_UsuPesCod ;
      private bool n1422ContagemResultadoNotificacao_UsuNom ;
      private bool n1961ContagemResultadoNotificacao_Logged ;
      private bool n1960ContagemResultadoNotificacao_Sec ;
      private bool n1959ContagemResultadoNotificacao_Aut ;
      private bool n1958ContagemResultadoNotificacao_Port ;
      private bool n1957ContagemResultadoNotificacao_User ;
      private bool n1956ContagemResultadoNotificacao_Host ;
      private bool n1417ContagemResultadoNotificacao_Assunto ;
      private bool n1416ContagemResultadoNotificacao_DataHora ;
      private bool n1963ContagemResultadoNotificacao_Demandas ;
      private bool n1962ContagemResultadoNotificacao_Destinatarios ;
      private bool BRKVE4 ;
      private bool BRKVE6 ;
      private bool BRKVE8 ;
      private String AV40OptionIndexesJson ;
      private String AV35OptionsJson ;
      private String AV38OptionsDescJson ;
      private String AV25TFContagemResultadoNotificacao_Sec_SelsJson ;
      private String AV27TFContagemResultadoNotificacao_Logged_SelsJson ;
      private String A1417ContagemResultadoNotificacao_Assunto ;
      private String AV31DDOName ;
      private String AV29SearchTxt ;
      private String AV30SearchTxtTo ;
      private String AV48TFContagemResultadoNotificacao_Assunto ;
      private String AV49TFContagemResultadoNotificacao_Assunto_Sel ;
      private String lV48TFContagemResultadoNotificacao_Assunto ;
      private String AV33Option ;
      private IGxSession AV42Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private long[] P00VE4_A1412ContagemResultadoNotificacao_Codigo ;
      private int[] P00VE4_A1413ContagemResultadoNotificacao_UsuCod ;
      private int[] P00VE4_A1427ContagemResultadoNotificacao_UsuPesCod ;
      private bool[] P00VE4_n1427ContagemResultadoNotificacao_UsuPesCod ;
      private int[] P00VE4_A456ContagemResultado_Codigo ;
      private String[] P00VE4_A1422ContagemResultadoNotificacao_UsuNom ;
      private bool[] P00VE4_n1422ContagemResultadoNotificacao_UsuNom ;
      private short[] P00VE4_A1961ContagemResultadoNotificacao_Logged ;
      private bool[] P00VE4_n1961ContagemResultadoNotificacao_Logged ;
      private short[] P00VE4_A1960ContagemResultadoNotificacao_Sec ;
      private bool[] P00VE4_n1960ContagemResultadoNotificacao_Sec ;
      private bool[] P00VE4_A1959ContagemResultadoNotificacao_Aut ;
      private bool[] P00VE4_n1959ContagemResultadoNotificacao_Aut ;
      private short[] P00VE4_A1958ContagemResultadoNotificacao_Port ;
      private bool[] P00VE4_n1958ContagemResultadoNotificacao_Port ;
      private String[] P00VE4_A1957ContagemResultadoNotificacao_User ;
      private bool[] P00VE4_n1957ContagemResultadoNotificacao_User ;
      private String[] P00VE4_A1956ContagemResultadoNotificacao_Host ;
      private bool[] P00VE4_n1956ContagemResultadoNotificacao_Host ;
      private String[] P00VE4_A1417ContagemResultadoNotificacao_Assunto ;
      private bool[] P00VE4_n1417ContagemResultadoNotificacao_Assunto ;
      private DateTime[] P00VE4_A1416ContagemResultadoNotificacao_DataHora ;
      private bool[] P00VE4_n1416ContagemResultadoNotificacao_DataHora ;
      private int[] P00VE4_A1963ContagemResultadoNotificacao_Demandas ;
      private bool[] P00VE4_n1963ContagemResultadoNotificacao_Demandas ;
      private int[] P00VE4_A1962ContagemResultadoNotificacao_Destinatarios ;
      private bool[] P00VE4_n1962ContagemResultadoNotificacao_Destinatarios ;
      private long[] P00VE7_A1412ContagemResultadoNotificacao_Codigo ;
      private int[] P00VE7_A1413ContagemResultadoNotificacao_UsuCod ;
      private int[] P00VE7_A1427ContagemResultadoNotificacao_UsuPesCod ;
      private bool[] P00VE7_n1427ContagemResultadoNotificacao_UsuPesCod ;
      private int[] P00VE7_A456ContagemResultado_Codigo ;
      private String[] P00VE7_A1417ContagemResultadoNotificacao_Assunto ;
      private bool[] P00VE7_n1417ContagemResultadoNotificacao_Assunto ;
      private short[] P00VE7_A1961ContagemResultadoNotificacao_Logged ;
      private bool[] P00VE7_n1961ContagemResultadoNotificacao_Logged ;
      private short[] P00VE7_A1960ContagemResultadoNotificacao_Sec ;
      private bool[] P00VE7_n1960ContagemResultadoNotificacao_Sec ;
      private bool[] P00VE7_A1959ContagemResultadoNotificacao_Aut ;
      private bool[] P00VE7_n1959ContagemResultadoNotificacao_Aut ;
      private short[] P00VE7_A1958ContagemResultadoNotificacao_Port ;
      private bool[] P00VE7_n1958ContagemResultadoNotificacao_Port ;
      private String[] P00VE7_A1957ContagemResultadoNotificacao_User ;
      private bool[] P00VE7_n1957ContagemResultadoNotificacao_User ;
      private String[] P00VE7_A1956ContagemResultadoNotificacao_Host ;
      private bool[] P00VE7_n1956ContagemResultadoNotificacao_Host ;
      private String[] P00VE7_A1422ContagemResultadoNotificacao_UsuNom ;
      private bool[] P00VE7_n1422ContagemResultadoNotificacao_UsuNom ;
      private DateTime[] P00VE7_A1416ContagemResultadoNotificacao_DataHora ;
      private bool[] P00VE7_n1416ContagemResultadoNotificacao_DataHora ;
      private int[] P00VE7_A1963ContagemResultadoNotificacao_Demandas ;
      private bool[] P00VE7_n1963ContagemResultadoNotificacao_Demandas ;
      private int[] P00VE7_A1962ContagemResultadoNotificacao_Destinatarios ;
      private bool[] P00VE7_n1962ContagemResultadoNotificacao_Destinatarios ;
      private long[] P00VE10_A1412ContagemResultadoNotificacao_Codigo ;
      private int[] P00VE10_A1413ContagemResultadoNotificacao_UsuCod ;
      private int[] P00VE10_A1427ContagemResultadoNotificacao_UsuPesCod ;
      private bool[] P00VE10_n1427ContagemResultadoNotificacao_UsuPesCod ;
      private int[] P00VE10_A456ContagemResultado_Codigo ;
      private String[] P00VE10_A1956ContagemResultadoNotificacao_Host ;
      private bool[] P00VE10_n1956ContagemResultadoNotificacao_Host ;
      private short[] P00VE10_A1961ContagemResultadoNotificacao_Logged ;
      private bool[] P00VE10_n1961ContagemResultadoNotificacao_Logged ;
      private short[] P00VE10_A1960ContagemResultadoNotificacao_Sec ;
      private bool[] P00VE10_n1960ContagemResultadoNotificacao_Sec ;
      private bool[] P00VE10_A1959ContagemResultadoNotificacao_Aut ;
      private bool[] P00VE10_n1959ContagemResultadoNotificacao_Aut ;
      private short[] P00VE10_A1958ContagemResultadoNotificacao_Port ;
      private bool[] P00VE10_n1958ContagemResultadoNotificacao_Port ;
      private String[] P00VE10_A1957ContagemResultadoNotificacao_User ;
      private bool[] P00VE10_n1957ContagemResultadoNotificacao_User ;
      private String[] P00VE10_A1417ContagemResultadoNotificacao_Assunto ;
      private bool[] P00VE10_n1417ContagemResultadoNotificacao_Assunto ;
      private String[] P00VE10_A1422ContagemResultadoNotificacao_UsuNom ;
      private bool[] P00VE10_n1422ContagemResultadoNotificacao_UsuNom ;
      private DateTime[] P00VE10_A1416ContagemResultadoNotificacao_DataHora ;
      private bool[] P00VE10_n1416ContagemResultadoNotificacao_DataHora ;
      private int[] P00VE10_A1963ContagemResultadoNotificacao_Demandas ;
      private bool[] P00VE10_n1963ContagemResultadoNotificacao_Demandas ;
      private int[] P00VE10_A1962ContagemResultadoNotificacao_Destinatarios ;
      private bool[] P00VE10_n1962ContagemResultadoNotificacao_Destinatarios ;
      private long[] P00VE13_A1412ContagemResultadoNotificacao_Codigo ;
      private int[] P00VE13_A1413ContagemResultadoNotificacao_UsuCod ;
      private int[] P00VE13_A1427ContagemResultadoNotificacao_UsuPesCod ;
      private bool[] P00VE13_n1427ContagemResultadoNotificacao_UsuPesCod ;
      private int[] P00VE13_A456ContagemResultado_Codigo ;
      private String[] P00VE13_A1957ContagemResultadoNotificacao_User ;
      private bool[] P00VE13_n1957ContagemResultadoNotificacao_User ;
      private short[] P00VE13_A1961ContagemResultadoNotificacao_Logged ;
      private bool[] P00VE13_n1961ContagemResultadoNotificacao_Logged ;
      private short[] P00VE13_A1960ContagemResultadoNotificacao_Sec ;
      private bool[] P00VE13_n1960ContagemResultadoNotificacao_Sec ;
      private bool[] P00VE13_A1959ContagemResultadoNotificacao_Aut ;
      private bool[] P00VE13_n1959ContagemResultadoNotificacao_Aut ;
      private short[] P00VE13_A1958ContagemResultadoNotificacao_Port ;
      private bool[] P00VE13_n1958ContagemResultadoNotificacao_Port ;
      private String[] P00VE13_A1956ContagemResultadoNotificacao_Host ;
      private bool[] P00VE13_n1956ContagemResultadoNotificacao_Host ;
      private String[] P00VE13_A1417ContagemResultadoNotificacao_Assunto ;
      private bool[] P00VE13_n1417ContagemResultadoNotificacao_Assunto ;
      private String[] P00VE13_A1422ContagemResultadoNotificacao_UsuNom ;
      private bool[] P00VE13_n1422ContagemResultadoNotificacao_UsuNom ;
      private DateTime[] P00VE13_A1416ContagemResultadoNotificacao_DataHora ;
      private bool[] P00VE13_n1416ContagemResultadoNotificacao_DataHora ;
      private int[] P00VE13_A1963ContagemResultadoNotificacao_Demandas ;
      private bool[] P00VE13_n1963ContagemResultadoNotificacao_Demandas ;
      private int[] P00VE13_A1962ContagemResultadoNotificacao_Destinatarios ;
      private bool[] P00VE13_n1962ContagemResultadoNotificacao_Destinatarios ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV26TFContagemResultadoNotificacao_Sec_Sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV28TFContagemResultadoNotificacao_Logged_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV34Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV37OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV39OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV44GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV45GridStateFilterValue ;
   }

   public class getdemandanotificacoeswcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00VE4( IGxContext context ,
                                             short A1960ContagemResultadoNotificacao_Sec ,
                                             IGxCollection AV26TFContagemResultadoNotificacao_Sec_Sels ,
                                             short A1961ContagemResultadoNotificacao_Logged ,
                                             IGxCollection AV28TFContagemResultadoNotificacao_Logged_Sels ,
                                             DateTime AV10TFContagemResultadoNotificacao_DataHora ,
                                             DateTime AV11TFContagemResultadoNotificacao_DataHora_To ,
                                             String AV13TFContagemResultadoNotificacao_UsuNom_Sel ,
                                             String AV12TFContagemResultadoNotificacao_UsuNom ,
                                             String AV49TFContagemResultadoNotificacao_Assunto_Sel ,
                                             String AV48TFContagemResultadoNotificacao_Assunto ,
                                             String AV19TFContagemResultadoNotificacao_Host_Sel ,
                                             String AV18TFContagemResultadoNotificacao_Host ,
                                             String AV21TFContagemResultadoNotificacao_User_Sel ,
                                             String AV20TFContagemResultadoNotificacao_User ,
                                             short AV22TFContagemResultadoNotificacao_Port ,
                                             short AV23TFContagemResultadoNotificacao_Port_To ,
                                             short AV24TFContagemResultadoNotificacao_Aut_Sel ,
                                             int AV26TFContagemResultadoNotificacao_Sec_Sels_Count ,
                                             int AV28TFContagemResultadoNotificacao_Logged_Sels_Count ,
                                             DateTime A1416ContagemResultadoNotificacao_DataHora ,
                                             String A1422ContagemResultadoNotificacao_UsuNom ,
                                             String A1417ContagemResultadoNotificacao_Assunto ,
                                             String A1956ContagemResultadoNotificacao_Host ,
                                             String A1957ContagemResultadoNotificacao_User ,
                                             short A1958ContagemResultadoNotificacao_Port ,
                                             bool A1959ContagemResultadoNotificacao_Aut ,
                                             int AV14TFContagemResultadoNotificacao_Destinatarios ,
                                             int A1962ContagemResultadoNotificacao_Destinatarios ,
                                             int AV15TFContagemResultadoNotificacao_Destinatarios_To ,
                                             int AV16TFContagemResultadoNotificacao_Demandas ,
                                             int A1963ContagemResultadoNotificacao_Demandas ,
                                             int AV17TFContagemResultadoNotificacao_Demandas_To ,
                                             int AV47ContagemResultado_Codigo ,
                                             int A456ContagemResultado_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [21] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultadoNotificacao_Codigo], T2.[ContagemResultadoNotificacao_UsuCod] AS ContagemResultadoNotificacao_UsuCod, T3.[Usuario_PessoaCod] AS ContagemResultadoNotificacao_UsuPesCod, T1.[ContagemResultado_Codigo], T4.[Pessoa_Nome] AS ContagemResultadoNotificacao_UsuNom, T2.[ContagemResultadoNotificacao_Logged], T2.[ContagemResultadoNotificacao_Sec], T2.[ContagemResultadoNotificacao_Aut], T2.[ContagemResultadoNotificacao_Port], T2.[ContagemResultadoNotificacao_User], T2.[ContagemResultadoNotificacao_Host], T2.[ContagemResultadoNotificacao_Assunto], T2.[ContagemResultadoNotificacao_DataHora], COALESCE( T5.[ContagemResultadoNotificacao_Demandas], 0) AS ContagemResultadoNotificacao_Demandas, COALESCE( T6.[ContagemResultadoNotificacao_Destinatarios], 0) AS ContagemResultadoNotificacao_Destinatarios FROM ((((([ContagemResultadoNotificacaoDemanda] T1 WITH (NOLOCK) INNER JOIN [ContagemResultadoNotificacao] T2 WITH (NOLOCK) ON T2.[ContagemResultadoNotificacao_Codigo] = T1.[ContagemResultadoNotificacao_Codigo]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T2.[ContagemResultadoNotificacao_UsuCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) LEFT JOIN (SELECT COUNT(*) AS ContagemResultadoNotificacao_Demandas, [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacaoDemanda] WITH (NOLOCK) GROUP BY [ContagemResultadoNotificacao_Codigo] ) T5 ON T5.[ContagemResultadoNotificacao_Codigo] = T1.[ContagemResultadoNotificacao_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContagemResultadoNotificacao_Destinatarios, [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacaoDestinatario] WITH (NOLOCK) GROUP BY [ContagemResultadoNotificacao_Codigo] ) T6 ON T6.[ContagemResultadoNotificacao_Codigo] = T1.[ContagemResultadoNotificacao_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultado_Codigo] = @AV47ContagemResultado_Codigo)";
         scmdbuf = scmdbuf + " and ((@AV14TFContagemResultadoNotificacao_Destinatarios = convert(int, 0)) or ( COALESCE( T6.[ContagemResultadoNotificacao_Destinatarios], 0) >= @AV14TFContagemResultadoNotificacao_Destinatarios))";
         scmdbuf = scmdbuf + " and ((@AV15TFContagemResultadoNotificacao_Destinatarios_To = convert(int, 0)) or ( COALESCE( T6.[ContagemResultadoNotificacao_Destinatarios], 0) <= @AV15TFContagemResultadoNotificacao_Destinatarios_To))";
         scmdbuf = scmdbuf + " and ((@AV16TFContagemResultadoNotificacao_Demandas = convert(int, 0)) or ( COALESCE( T5.[ContagemResultadoNotificacao_Demandas], 0) >= @AV16TFContagemResultadoNotificacao_Demandas))";
         scmdbuf = scmdbuf + " and ((@AV17TFContagemResultadoNotificacao_Demandas_To = convert(int, 0)) or ( COALESCE( T5.[ContagemResultadoNotificacao_Demandas], 0) <= @AV17TFContagemResultadoNotificacao_Demandas_To))";
         if ( ! (DateTime.MinValue==AV10TFContagemResultadoNotificacao_DataHora) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_DataHora] >= @AV10TFContagemResultadoNotificacao_DataHora)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV11TFContagemResultadoNotificacao_DataHora_To) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_DataHora] <= @AV11TFContagemResultadoNotificacao_DataHora_To)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContagemResultadoNotificacao_UsuNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContagemResultadoNotificacao_UsuNom)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV12TFContagemResultadoNotificacao_UsuNom)";
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContagemResultadoNotificacao_UsuNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV13TFContagemResultadoNotificacao_UsuNom_Sel)";
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV49TFContagemResultadoNotificacao_Assunto_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFContagemResultadoNotificacao_Assunto)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Assunto] like @lV48TFContagemResultadoNotificacao_Assunto)";
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFContagemResultadoNotificacao_Assunto_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Assunto] = @AV49TFContagemResultadoNotificacao_Assunto_Sel)";
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContagemResultadoNotificacao_Host_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFContagemResultadoNotificacao_Host)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Host] like @lV18TFContagemResultadoNotificacao_Host)";
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContagemResultadoNotificacao_Host_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Host] = @AV19TFContagemResultadoNotificacao_Host_Sel)";
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContagemResultadoNotificacao_User_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFContagemResultadoNotificacao_User)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_User] like @lV20TFContagemResultadoNotificacao_User)";
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContagemResultadoNotificacao_User_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_User] = @AV21TFContagemResultadoNotificacao_User_Sel)";
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ! (0==AV22TFContagemResultadoNotificacao_Port) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Port] >= @AV22TFContagemResultadoNotificacao_Port)";
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( ! (0==AV23TFContagemResultadoNotificacao_Port_To) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Port] <= @AV23TFContagemResultadoNotificacao_Port_To)";
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( AV24TFContagemResultadoNotificacao_Aut_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Aut] = 1)";
         }
         if ( AV24TFContagemResultadoNotificacao_Aut_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Aut] = 0)";
         }
         if ( AV26TFContagemResultadoNotificacao_Sec_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV26TFContagemResultadoNotificacao_Sec_Sels, "T2.[ContagemResultadoNotificacao_Sec] IN (", ")") + ")";
         }
         if ( AV28TFContagemResultadoNotificacao_Logged_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV28TFContagemResultadoNotificacao_Logged_Sels, "T2.[ContagemResultadoNotificacao_Logged] IN (", ")") + ")";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Codigo], T4.[Pessoa_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00VE7( IGxContext context ,
                                             short A1960ContagemResultadoNotificacao_Sec ,
                                             IGxCollection AV26TFContagemResultadoNotificacao_Sec_Sels ,
                                             short A1961ContagemResultadoNotificacao_Logged ,
                                             IGxCollection AV28TFContagemResultadoNotificacao_Logged_Sels ,
                                             DateTime AV10TFContagemResultadoNotificacao_DataHora ,
                                             DateTime AV11TFContagemResultadoNotificacao_DataHora_To ,
                                             String AV13TFContagemResultadoNotificacao_UsuNom_Sel ,
                                             String AV12TFContagemResultadoNotificacao_UsuNom ,
                                             String AV49TFContagemResultadoNotificacao_Assunto_Sel ,
                                             String AV48TFContagemResultadoNotificacao_Assunto ,
                                             String AV19TFContagemResultadoNotificacao_Host_Sel ,
                                             String AV18TFContagemResultadoNotificacao_Host ,
                                             String AV21TFContagemResultadoNotificacao_User_Sel ,
                                             String AV20TFContagemResultadoNotificacao_User ,
                                             short AV22TFContagemResultadoNotificacao_Port ,
                                             short AV23TFContagemResultadoNotificacao_Port_To ,
                                             short AV24TFContagemResultadoNotificacao_Aut_Sel ,
                                             int AV26TFContagemResultadoNotificacao_Sec_Sels_Count ,
                                             int AV28TFContagemResultadoNotificacao_Logged_Sels_Count ,
                                             DateTime A1416ContagemResultadoNotificacao_DataHora ,
                                             String A1422ContagemResultadoNotificacao_UsuNom ,
                                             String A1417ContagemResultadoNotificacao_Assunto ,
                                             String A1956ContagemResultadoNotificacao_Host ,
                                             String A1957ContagemResultadoNotificacao_User ,
                                             short A1958ContagemResultadoNotificacao_Port ,
                                             bool A1959ContagemResultadoNotificacao_Aut ,
                                             int AV14TFContagemResultadoNotificacao_Destinatarios ,
                                             int A1962ContagemResultadoNotificacao_Destinatarios ,
                                             int AV15TFContagemResultadoNotificacao_Destinatarios_To ,
                                             int AV16TFContagemResultadoNotificacao_Demandas ,
                                             int A1963ContagemResultadoNotificacao_Demandas ,
                                             int AV17TFContagemResultadoNotificacao_Demandas_To ,
                                             int AV47ContagemResultado_Codigo ,
                                             int A456ContagemResultado_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [21] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultadoNotificacao_Codigo], T2.[ContagemResultadoNotificacao_UsuCod] AS ContagemResultadoNotificacao_UsuCod, T3.[Usuario_PessoaCod] AS ContagemResultadoNotificacao_UsuPesCod, T1.[ContagemResultado_Codigo], T2.[ContagemResultadoNotificacao_Assunto], T2.[ContagemResultadoNotificacao_Logged], T2.[ContagemResultadoNotificacao_Sec], T2.[ContagemResultadoNotificacao_Aut], T2.[ContagemResultadoNotificacao_Port], T2.[ContagemResultadoNotificacao_User], T2.[ContagemResultadoNotificacao_Host], T4.[Pessoa_Nome] AS ContagemResultadoNotificacao_UsuNom, T2.[ContagemResultadoNotificacao_DataHora], COALESCE( T5.[ContagemResultadoNotificacao_Demandas], 0) AS ContagemResultadoNotificacao_Demandas, COALESCE( T6.[ContagemResultadoNotificacao_Destinatarios], 0) AS ContagemResultadoNotificacao_Destinatarios FROM ((((([ContagemResultadoNotificacaoDemanda] T1 WITH (NOLOCK) INNER JOIN [ContagemResultadoNotificacao] T2 WITH (NOLOCK) ON T2.[ContagemResultadoNotificacao_Codigo] = T1.[ContagemResultadoNotificacao_Codigo]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T2.[ContagemResultadoNotificacao_UsuCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) LEFT JOIN (SELECT COUNT(*) AS ContagemResultadoNotificacao_Demandas, [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacaoDemanda] WITH (NOLOCK) GROUP BY [ContagemResultadoNotificacao_Codigo] ) T5 ON T5.[ContagemResultadoNotificacao_Codigo] = T1.[ContagemResultadoNotificacao_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContagemResultadoNotificacao_Destinatarios, [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacaoDestinatario] WITH (NOLOCK) GROUP BY [ContagemResultadoNotificacao_Codigo] ) T6 ON T6.[ContagemResultadoNotificacao_Codigo] = T1.[ContagemResultadoNotificacao_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultado_Codigo] = @AV47ContagemResultado_Codigo)";
         scmdbuf = scmdbuf + " and ((@AV14TFContagemResultadoNotificacao_Destinatarios = convert(int, 0)) or ( COALESCE( T6.[ContagemResultadoNotificacao_Destinatarios], 0) >= @AV14TFContagemResultadoNotificacao_Destinatarios))";
         scmdbuf = scmdbuf + " and ((@AV15TFContagemResultadoNotificacao_Destinatarios_To = convert(int, 0)) or ( COALESCE( T6.[ContagemResultadoNotificacao_Destinatarios], 0) <= @AV15TFContagemResultadoNotificacao_Destinatarios_To))";
         scmdbuf = scmdbuf + " and ((@AV16TFContagemResultadoNotificacao_Demandas = convert(int, 0)) or ( COALESCE( T5.[ContagemResultadoNotificacao_Demandas], 0) >= @AV16TFContagemResultadoNotificacao_Demandas))";
         scmdbuf = scmdbuf + " and ((@AV17TFContagemResultadoNotificacao_Demandas_To = convert(int, 0)) or ( COALESCE( T5.[ContagemResultadoNotificacao_Demandas], 0) <= @AV17TFContagemResultadoNotificacao_Demandas_To))";
         if ( ! (DateTime.MinValue==AV10TFContagemResultadoNotificacao_DataHora) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_DataHora] >= @AV10TFContagemResultadoNotificacao_DataHora)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV11TFContagemResultadoNotificacao_DataHora_To) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_DataHora] <= @AV11TFContagemResultadoNotificacao_DataHora_To)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContagemResultadoNotificacao_UsuNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContagemResultadoNotificacao_UsuNom)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV12TFContagemResultadoNotificacao_UsuNom)";
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContagemResultadoNotificacao_UsuNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV13TFContagemResultadoNotificacao_UsuNom_Sel)";
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV49TFContagemResultadoNotificacao_Assunto_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFContagemResultadoNotificacao_Assunto)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Assunto] like @lV48TFContagemResultadoNotificacao_Assunto)";
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFContagemResultadoNotificacao_Assunto_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Assunto] = @AV49TFContagemResultadoNotificacao_Assunto_Sel)";
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContagemResultadoNotificacao_Host_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFContagemResultadoNotificacao_Host)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Host] like @lV18TFContagemResultadoNotificacao_Host)";
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContagemResultadoNotificacao_Host_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Host] = @AV19TFContagemResultadoNotificacao_Host_Sel)";
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContagemResultadoNotificacao_User_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFContagemResultadoNotificacao_User)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_User] like @lV20TFContagemResultadoNotificacao_User)";
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContagemResultadoNotificacao_User_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_User] = @AV21TFContagemResultadoNotificacao_User_Sel)";
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( ! (0==AV22TFContagemResultadoNotificacao_Port) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Port] >= @AV22TFContagemResultadoNotificacao_Port)";
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( ! (0==AV23TFContagemResultadoNotificacao_Port_To) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Port] <= @AV23TFContagemResultadoNotificacao_Port_To)";
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( AV24TFContagemResultadoNotificacao_Aut_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Aut] = 1)";
         }
         if ( AV24TFContagemResultadoNotificacao_Aut_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Aut] = 0)";
         }
         if ( AV26TFContagemResultadoNotificacao_Sec_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV26TFContagemResultadoNotificacao_Sec_Sels, "T2.[ContagemResultadoNotificacao_Sec] IN (", ")") + ")";
         }
         if ( AV28TFContagemResultadoNotificacao_Logged_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV28TFContagemResultadoNotificacao_Logged_Sels, "T2.[ContagemResultadoNotificacao_Logged] IN (", ")") + ")";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Codigo], T2.[ContagemResultadoNotificacao_Assunto]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00VE10( IGxContext context ,
                                              short A1960ContagemResultadoNotificacao_Sec ,
                                              IGxCollection AV26TFContagemResultadoNotificacao_Sec_Sels ,
                                              short A1961ContagemResultadoNotificacao_Logged ,
                                              IGxCollection AV28TFContagemResultadoNotificacao_Logged_Sels ,
                                              DateTime AV10TFContagemResultadoNotificacao_DataHora ,
                                              DateTime AV11TFContagemResultadoNotificacao_DataHora_To ,
                                              String AV13TFContagemResultadoNotificacao_UsuNom_Sel ,
                                              String AV12TFContagemResultadoNotificacao_UsuNom ,
                                              String AV49TFContagemResultadoNotificacao_Assunto_Sel ,
                                              String AV48TFContagemResultadoNotificacao_Assunto ,
                                              String AV19TFContagemResultadoNotificacao_Host_Sel ,
                                              String AV18TFContagemResultadoNotificacao_Host ,
                                              String AV21TFContagemResultadoNotificacao_User_Sel ,
                                              String AV20TFContagemResultadoNotificacao_User ,
                                              short AV22TFContagemResultadoNotificacao_Port ,
                                              short AV23TFContagemResultadoNotificacao_Port_To ,
                                              short AV24TFContagemResultadoNotificacao_Aut_Sel ,
                                              int AV26TFContagemResultadoNotificacao_Sec_Sels_Count ,
                                              int AV28TFContagemResultadoNotificacao_Logged_Sels_Count ,
                                              DateTime A1416ContagemResultadoNotificacao_DataHora ,
                                              String A1422ContagemResultadoNotificacao_UsuNom ,
                                              String A1417ContagemResultadoNotificacao_Assunto ,
                                              String A1956ContagemResultadoNotificacao_Host ,
                                              String A1957ContagemResultadoNotificacao_User ,
                                              short A1958ContagemResultadoNotificacao_Port ,
                                              bool A1959ContagemResultadoNotificacao_Aut ,
                                              int AV14TFContagemResultadoNotificacao_Destinatarios ,
                                              int A1962ContagemResultadoNotificacao_Destinatarios ,
                                              int AV15TFContagemResultadoNotificacao_Destinatarios_To ,
                                              int AV16TFContagemResultadoNotificacao_Demandas ,
                                              int A1963ContagemResultadoNotificacao_Demandas ,
                                              int AV17TFContagemResultadoNotificacao_Demandas_To ,
                                              int AV47ContagemResultado_Codigo ,
                                              int A456ContagemResultado_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [21] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultadoNotificacao_Codigo], T2.[ContagemResultadoNotificacao_UsuCod] AS ContagemResultadoNotificacao_UsuCod, T3.[Usuario_PessoaCod] AS ContagemResultadoNotificacao_UsuPesCod, T1.[ContagemResultado_Codigo], T2.[ContagemResultadoNotificacao_Host], T2.[ContagemResultadoNotificacao_Logged], T2.[ContagemResultadoNotificacao_Sec], T2.[ContagemResultadoNotificacao_Aut], T2.[ContagemResultadoNotificacao_Port], T2.[ContagemResultadoNotificacao_User], T2.[ContagemResultadoNotificacao_Assunto], T4.[Pessoa_Nome] AS ContagemResultadoNotificacao_UsuNom, T2.[ContagemResultadoNotificacao_DataHora], COALESCE( T5.[ContagemResultadoNotificacao_Demandas], 0) AS ContagemResultadoNotificacao_Demandas, COALESCE( T6.[ContagemResultadoNotificacao_Destinatarios], 0) AS ContagemResultadoNotificacao_Destinatarios FROM ((((([ContagemResultadoNotificacaoDemanda] T1 WITH (NOLOCK) INNER JOIN [ContagemResultadoNotificacao] T2 WITH (NOLOCK) ON T2.[ContagemResultadoNotificacao_Codigo] = T1.[ContagemResultadoNotificacao_Codigo]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T2.[ContagemResultadoNotificacao_UsuCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) LEFT JOIN (SELECT COUNT(*) AS ContagemResultadoNotificacao_Demandas, [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacaoDemanda] WITH (NOLOCK) GROUP BY [ContagemResultadoNotificacao_Codigo] ) T5 ON T5.[ContagemResultadoNotificacao_Codigo] = T1.[ContagemResultadoNotificacao_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContagemResultadoNotificacao_Destinatarios, [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacaoDestinatario] WITH (NOLOCK) GROUP BY [ContagemResultadoNotificacao_Codigo] ) T6 ON T6.[ContagemResultadoNotificacao_Codigo] = T1.[ContagemResultadoNotificacao_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultado_Codigo] = @AV47ContagemResultado_Codigo)";
         scmdbuf = scmdbuf + " and ((@AV14TFContagemResultadoNotificacao_Destinatarios = convert(int, 0)) or ( COALESCE( T6.[ContagemResultadoNotificacao_Destinatarios], 0) >= @AV14TFContagemResultadoNotificacao_Destinatarios))";
         scmdbuf = scmdbuf + " and ((@AV15TFContagemResultadoNotificacao_Destinatarios_To = convert(int, 0)) or ( COALESCE( T6.[ContagemResultadoNotificacao_Destinatarios], 0) <= @AV15TFContagemResultadoNotificacao_Destinatarios_To))";
         scmdbuf = scmdbuf + " and ((@AV16TFContagemResultadoNotificacao_Demandas = convert(int, 0)) or ( COALESCE( T5.[ContagemResultadoNotificacao_Demandas], 0) >= @AV16TFContagemResultadoNotificacao_Demandas))";
         scmdbuf = scmdbuf + " and ((@AV17TFContagemResultadoNotificacao_Demandas_To = convert(int, 0)) or ( COALESCE( T5.[ContagemResultadoNotificacao_Demandas], 0) <= @AV17TFContagemResultadoNotificacao_Demandas_To))";
         if ( ! (DateTime.MinValue==AV10TFContagemResultadoNotificacao_DataHora) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_DataHora] >= @AV10TFContagemResultadoNotificacao_DataHora)";
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV11TFContagemResultadoNotificacao_DataHora_To) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_DataHora] <= @AV11TFContagemResultadoNotificacao_DataHora_To)";
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContagemResultadoNotificacao_UsuNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContagemResultadoNotificacao_UsuNom)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV12TFContagemResultadoNotificacao_UsuNom)";
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContagemResultadoNotificacao_UsuNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV13TFContagemResultadoNotificacao_UsuNom_Sel)";
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV49TFContagemResultadoNotificacao_Assunto_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFContagemResultadoNotificacao_Assunto)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Assunto] like @lV48TFContagemResultadoNotificacao_Assunto)";
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFContagemResultadoNotificacao_Assunto_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Assunto] = @AV49TFContagemResultadoNotificacao_Assunto_Sel)";
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContagemResultadoNotificacao_Host_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFContagemResultadoNotificacao_Host)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Host] like @lV18TFContagemResultadoNotificacao_Host)";
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContagemResultadoNotificacao_Host_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Host] = @AV19TFContagemResultadoNotificacao_Host_Sel)";
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContagemResultadoNotificacao_User_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFContagemResultadoNotificacao_User)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_User] like @lV20TFContagemResultadoNotificacao_User)";
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContagemResultadoNotificacao_User_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_User] = @AV21TFContagemResultadoNotificacao_User_Sel)";
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( ! (0==AV22TFContagemResultadoNotificacao_Port) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Port] >= @AV22TFContagemResultadoNotificacao_Port)";
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( ! (0==AV23TFContagemResultadoNotificacao_Port_To) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Port] <= @AV23TFContagemResultadoNotificacao_Port_To)";
         }
         else
         {
            GXv_int5[20] = 1;
         }
         if ( AV24TFContagemResultadoNotificacao_Aut_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Aut] = 1)";
         }
         if ( AV24TFContagemResultadoNotificacao_Aut_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Aut] = 0)";
         }
         if ( AV26TFContagemResultadoNotificacao_Sec_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV26TFContagemResultadoNotificacao_Sec_Sels, "T2.[ContagemResultadoNotificacao_Sec] IN (", ")") + ")";
         }
         if ( AV28TFContagemResultadoNotificacao_Logged_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV28TFContagemResultadoNotificacao_Logged_Sels, "T2.[ContagemResultadoNotificacao_Logged] IN (", ")") + ")";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Codigo], T2.[ContagemResultadoNotificacao_Host]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      protected Object[] conditional_P00VE13( IGxContext context ,
                                              short A1960ContagemResultadoNotificacao_Sec ,
                                              IGxCollection AV26TFContagemResultadoNotificacao_Sec_Sels ,
                                              short A1961ContagemResultadoNotificacao_Logged ,
                                              IGxCollection AV28TFContagemResultadoNotificacao_Logged_Sels ,
                                              DateTime AV10TFContagemResultadoNotificacao_DataHora ,
                                              DateTime AV11TFContagemResultadoNotificacao_DataHora_To ,
                                              String AV13TFContagemResultadoNotificacao_UsuNom_Sel ,
                                              String AV12TFContagemResultadoNotificacao_UsuNom ,
                                              String AV49TFContagemResultadoNotificacao_Assunto_Sel ,
                                              String AV48TFContagemResultadoNotificacao_Assunto ,
                                              String AV19TFContagemResultadoNotificacao_Host_Sel ,
                                              String AV18TFContagemResultadoNotificacao_Host ,
                                              String AV21TFContagemResultadoNotificacao_User_Sel ,
                                              String AV20TFContagemResultadoNotificacao_User ,
                                              short AV22TFContagemResultadoNotificacao_Port ,
                                              short AV23TFContagemResultadoNotificacao_Port_To ,
                                              short AV24TFContagemResultadoNotificacao_Aut_Sel ,
                                              int AV26TFContagemResultadoNotificacao_Sec_Sels_Count ,
                                              int AV28TFContagemResultadoNotificacao_Logged_Sels_Count ,
                                              DateTime A1416ContagemResultadoNotificacao_DataHora ,
                                              String A1422ContagemResultadoNotificacao_UsuNom ,
                                              String A1417ContagemResultadoNotificacao_Assunto ,
                                              String A1956ContagemResultadoNotificacao_Host ,
                                              String A1957ContagemResultadoNotificacao_User ,
                                              short A1958ContagemResultadoNotificacao_Port ,
                                              bool A1959ContagemResultadoNotificacao_Aut ,
                                              int AV14TFContagemResultadoNotificacao_Destinatarios ,
                                              int A1962ContagemResultadoNotificacao_Destinatarios ,
                                              int AV15TFContagemResultadoNotificacao_Destinatarios_To ,
                                              int AV16TFContagemResultadoNotificacao_Demandas ,
                                              int A1963ContagemResultadoNotificacao_Demandas ,
                                              int AV17TFContagemResultadoNotificacao_Demandas_To ,
                                              int AV47ContagemResultado_Codigo ,
                                              int A456ContagemResultado_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int7 ;
         GXv_int7 = new short [21] ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultadoNotificacao_Codigo], T2.[ContagemResultadoNotificacao_UsuCod] AS ContagemResultadoNotificacao_UsuCod, T3.[Usuario_PessoaCod] AS ContagemResultadoNotificacao_UsuPesCod, T1.[ContagemResultado_Codigo], T2.[ContagemResultadoNotificacao_User], T2.[ContagemResultadoNotificacao_Logged], T2.[ContagemResultadoNotificacao_Sec], T2.[ContagemResultadoNotificacao_Aut], T2.[ContagemResultadoNotificacao_Port], T2.[ContagemResultadoNotificacao_Host], T2.[ContagemResultadoNotificacao_Assunto], T4.[Pessoa_Nome] AS ContagemResultadoNotificacao_UsuNom, T2.[ContagemResultadoNotificacao_DataHora], COALESCE( T5.[ContagemResultadoNotificacao_Demandas], 0) AS ContagemResultadoNotificacao_Demandas, COALESCE( T6.[ContagemResultadoNotificacao_Destinatarios], 0) AS ContagemResultadoNotificacao_Destinatarios FROM ((((([ContagemResultadoNotificacaoDemanda] T1 WITH (NOLOCK) INNER JOIN [ContagemResultadoNotificacao] T2 WITH (NOLOCK) ON T2.[ContagemResultadoNotificacao_Codigo] = T1.[ContagemResultadoNotificacao_Codigo]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T2.[ContagemResultadoNotificacao_UsuCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) LEFT JOIN (SELECT COUNT(*) AS ContagemResultadoNotificacao_Demandas, [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacaoDemanda] WITH (NOLOCK) GROUP BY [ContagemResultadoNotificacao_Codigo] ) T5 ON T5.[ContagemResultadoNotificacao_Codigo] = T1.[ContagemResultadoNotificacao_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContagemResultadoNotificacao_Destinatarios, [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacaoDestinatario] WITH (NOLOCK) GROUP BY [ContagemResultadoNotificacao_Codigo] ) T6 ON T6.[ContagemResultadoNotificacao_Codigo] = T1.[ContagemResultadoNotificacao_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultado_Codigo] = @AV47ContagemResultado_Codigo)";
         scmdbuf = scmdbuf + " and ((@AV14TFContagemResultadoNotificacao_Destinatarios = convert(int, 0)) or ( COALESCE( T6.[ContagemResultadoNotificacao_Destinatarios], 0) >= @AV14TFContagemResultadoNotificacao_Destinatarios))";
         scmdbuf = scmdbuf + " and ((@AV15TFContagemResultadoNotificacao_Destinatarios_To = convert(int, 0)) or ( COALESCE( T6.[ContagemResultadoNotificacao_Destinatarios], 0) <= @AV15TFContagemResultadoNotificacao_Destinatarios_To))";
         scmdbuf = scmdbuf + " and ((@AV16TFContagemResultadoNotificacao_Demandas = convert(int, 0)) or ( COALESCE( T5.[ContagemResultadoNotificacao_Demandas], 0) >= @AV16TFContagemResultadoNotificacao_Demandas))";
         scmdbuf = scmdbuf + " and ((@AV17TFContagemResultadoNotificacao_Demandas_To = convert(int, 0)) or ( COALESCE( T5.[ContagemResultadoNotificacao_Demandas], 0) <= @AV17TFContagemResultadoNotificacao_Demandas_To))";
         if ( ! (DateTime.MinValue==AV10TFContagemResultadoNotificacao_DataHora) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_DataHora] >= @AV10TFContagemResultadoNotificacao_DataHora)";
         }
         else
         {
            GXv_int7[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV11TFContagemResultadoNotificacao_DataHora_To) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_DataHora] <= @AV11TFContagemResultadoNotificacao_DataHora_To)";
         }
         else
         {
            GXv_int7[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContagemResultadoNotificacao_UsuNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContagemResultadoNotificacao_UsuNom)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV12TFContagemResultadoNotificacao_UsuNom)";
         }
         else
         {
            GXv_int7[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContagemResultadoNotificacao_UsuNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV13TFContagemResultadoNotificacao_UsuNom_Sel)";
         }
         else
         {
            GXv_int7[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV49TFContagemResultadoNotificacao_Assunto_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFContagemResultadoNotificacao_Assunto)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Assunto] like @lV48TFContagemResultadoNotificacao_Assunto)";
         }
         else
         {
            GXv_int7[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFContagemResultadoNotificacao_Assunto_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Assunto] = @AV49TFContagemResultadoNotificacao_Assunto_Sel)";
         }
         else
         {
            GXv_int7[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContagemResultadoNotificacao_Host_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFContagemResultadoNotificacao_Host)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Host] like @lV18TFContagemResultadoNotificacao_Host)";
         }
         else
         {
            GXv_int7[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContagemResultadoNotificacao_Host_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Host] = @AV19TFContagemResultadoNotificacao_Host_Sel)";
         }
         else
         {
            GXv_int7[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContagemResultadoNotificacao_User_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFContagemResultadoNotificacao_User)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_User] like @lV20TFContagemResultadoNotificacao_User)";
         }
         else
         {
            GXv_int7[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContagemResultadoNotificacao_User_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_User] = @AV21TFContagemResultadoNotificacao_User_Sel)";
         }
         else
         {
            GXv_int7[18] = 1;
         }
         if ( ! (0==AV22TFContagemResultadoNotificacao_Port) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Port] >= @AV22TFContagemResultadoNotificacao_Port)";
         }
         else
         {
            GXv_int7[19] = 1;
         }
         if ( ! (0==AV23TFContagemResultadoNotificacao_Port_To) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Port] <= @AV23TFContagemResultadoNotificacao_Port_To)";
         }
         else
         {
            GXv_int7[20] = 1;
         }
         if ( AV24TFContagemResultadoNotificacao_Aut_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Aut] = 1)";
         }
         if ( AV24TFContagemResultadoNotificacao_Aut_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Aut] = 0)";
         }
         if ( AV26TFContagemResultadoNotificacao_Sec_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV26TFContagemResultadoNotificacao_Sec_Sels, "T2.[ContagemResultadoNotificacao_Sec] IN (", ")") + ")";
         }
         if ( AV28TFContagemResultadoNotificacao_Logged_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV28TFContagemResultadoNotificacao_Logged_Sels, "T2.[ContagemResultadoNotificacao_Logged] IN (", ")") + ")";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Codigo], T2.[ContagemResultadoNotificacao_User]";
         GXv_Object8[0] = scmdbuf;
         GXv_Object8[1] = GXv_int7;
         return GXv_Object8 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00VE4(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (short)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (short)dynConstraints[14] , (short)dynConstraints[15] , (short)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (DateTime)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (short)dynConstraints[24] , (bool)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] , (int)dynConstraints[28] , (int)dynConstraints[29] , (int)dynConstraints[30] , (int)dynConstraints[31] , (int)dynConstraints[32] , (int)dynConstraints[33] );
               case 1 :
                     return conditional_P00VE7(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (short)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (short)dynConstraints[14] , (short)dynConstraints[15] , (short)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (DateTime)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (short)dynConstraints[24] , (bool)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] , (int)dynConstraints[28] , (int)dynConstraints[29] , (int)dynConstraints[30] , (int)dynConstraints[31] , (int)dynConstraints[32] , (int)dynConstraints[33] );
               case 2 :
                     return conditional_P00VE10(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (short)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (short)dynConstraints[14] , (short)dynConstraints[15] , (short)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (DateTime)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (short)dynConstraints[24] , (bool)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] , (int)dynConstraints[28] , (int)dynConstraints[29] , (int)dynConstraints[30] , (int)dynConstraints[31] , (int)dynConstraints[32] , (int)dynConstraints[33] );
               case 3 :
                     return conditional_P00VE13(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (short)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (short)dynConstraints[14] , (short)dynConstraints[15] , (short)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (DateTime)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (short)dynConstraints[24] , (bool)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] , (int)dynConstraints[28] , (int)dynConstraints[29] , (int)dynConstraints[30] , (int)dynConstraints[31] , (int)dynConstraints[32] , (int)dynConstraints[33] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00VE4 ;
          prmP00VE4 = new Object[] {
          new Object[] {"@AV47ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV14TFContagemResultadoNotificacao_Destinatarios",SqlDbType.Int,6,0} ,
          new Object[] {"@AV14TFContagemResultadoNotificacao_Destinatarios",SqlDbType.Int,6,0} ,
          new Object[] {"@AV15TFContagemResultadoNotificacao_Destinatarios_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV15TFContagemResultadoNotificacao_Destinatarios_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV16TFContagemResultadoNotificacao_Demandas",SqlDbType.Int,6,0} ,
          new Object[] {"@AV16TFContagemResultadoNotificacao_Demandas",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFContagemResultadoNotificacao_Demandas_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFContagemResultadoNotificacao_Demandas_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV10TFContagemResultadoNotificacao_DataHora",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV11TFContagemResultadoNotificacao_DataHora_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV12TFContagemResultadoNotificacao_UsuNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV13TFContagemResultadoNotificacao_UsuNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV48TFContagemResultadoNotificacao_Assunto",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV49TFContagemResultadoNotificacao_Assunto_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV18TFContagemResultadoNotificacao_Host",SqlDbType.Char,50,0} ,
          new Object[] {"@AV19TFContagemResultadoNotificacao_Host_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV20TFContagemResultadoNotificacao_User",SqlDbType.Char,50,0} ,
          new Object[] {"@AV21TFContagemResultadoNotificacao_User_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV22TFContagemResultadoNotificacao_Port",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV23TFContagemResultadoNotificacao_Port_To",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmP00VE7 ;
          prmP00VE7 = new Object[] {
          new Object[] {"@AV47ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV14TFContagemResultadoNotificacao_Destinatarios",SqlDbType.Int,6,0} ,
          new Object[] {"@AV14TFContagemResultadoNotificacao_Destinatarios",SqlDbType.Int,6,0} ,
          new Object[] {"@AV15TFContagemResultadoNotificacao_Destinatarios_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV15TFContagemResultadoNotificacao_Destinatarios_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV16TFContagemResultadoNotificacao_Demandas",SqlDbType.Int,6,0} ,
          new Object[] {"@AV16TFContagemResultadoNotificacao_Demandas",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFContagemResultadoNotificacao_Demandas_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFContagemResultadoNotificacao_Demandas_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV10TFContagemResultadoNotificacao_DataHora",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV11TFContagemResultadoNotificacao_DataHora_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV12TFContagemResultadoNotificacao_UsuNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV13TFContagemResultadoNotificacao_UsuNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV48TFContagemResultadoNotificacao_Assunto",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV49TFContagemResultadoNotificacao_Assunto_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV18TFContagemResultadoNotificacao_Host",SqlDbType.Char,50,0} ,
          new Object[] {"@AV19TFContagemResultadoNotificacao_Host_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV20TFContagemResultadoNotificacao_User",SqlDbType.Char,50,0} ,
          new Object[] {"@AV21TFContagemResultadoNotificacao_User_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV22TFContagemResultadoNotificacao_Port",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV23TFContagemResultadoNotificacao_Port_To",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmP00VE10 ;
          prmP00VE10 = new Object[] {
          new Object[] {"@AV47ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV14TFContagemResultadoNotificacao_Destinatarios",SqlDbType.Int,6,0} ,
          new Object[] {"@AV14TFContagemResultadoNotificacao_Destinatarios",SqlDbType.Int,6,0} ,
          new Object[] {"@AV15TFContagemResultadoNotificacao_Destinatarios_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV15TFContagemResultadoNotificacao_Destinatarios_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV16TFContagemResultadoNotificacao_Demandas",SqlDbType.Int,6,0} ,
          new Object[] {"@AV16TFContagemResultadoNotificacao_Demandas",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFContagemResultadoNotificacao_Demandas_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFContagemResultadoNotificacao_Demandas_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV10TFContagemResultadoNotificacao_DataHora",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV11TFContagemResultadoNotificacao_DataHora_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV12TFContagemResultadoNotificacao_UsuNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV13TFContagemResultadoNotificacao_UsuNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV48TFContagemResultadoNotificacao_Assunto",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV49TFContagemResultadoNotificacao_Assunto_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV18TFContagemResultadoNotificacao_Host",SqlDbType.Char,50,0} ,
          new Object[] {"@AV19TFContagemResultadoNotificacao_Host_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV20TFContagemResultadoNotificacao_User",SqlDbType.Char,50,0} ,
          new Object[] {"@AV21TFContagemResultadoNotificacao_User_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV22TFContagemResultadoNotificacao_Port",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV23TFContagemResultadoNotificacao_Port_To",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmP00VE13 ;
          prmP00VE13 = new Object[] {
          new Object[] {"@AV47ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV14TFContagemResultadoNotificacao_Destinatarios",SqlDbType.Int,6,0} ,
          new Object[] {"@AV14TFContagemResultadoNotificacao_Destinatarios",SqlDbType.Int,6,0} ,
          new Object[] {"@AV15TFContagemResultadoNotificacao_Destinatarios_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV15TFContagemResultadoNotificacao_Destinatarios_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV16TFContagemResultadoNotificacao_Demandas",SqlDbType.Int,6,0} ,
          new Object[] {"@AV16TFContagemResultadoNotificacao_Demandas",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFContagemResultadoNotificacao_Demandas_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFContagemResultadoNotificacao_Demandas_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV10TFContagemResultadoNotificacao_DataHora",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV11TFContagemResultadoNotificacao_DataHora_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV12TFContagemResultadoNotificacao_UsuNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV13TFContagemResultadoNotificacao_UsuNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV48TFContagemResultadoNotificacao_Assunto",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV49TFContagemResultadoNotificacao_Assunto_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV18TFContagemResultadoNotificacao_Host",SqlDbType.Char,50,0} ,
          new Object[] {"@AV19TFContagemResultadoNotificacao_Host_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV20TFContagemResultadoNotificacao_User",SqlDbType.Char,50,0} ,
          new Object[] {"@AV21TFContagemResultadoNotificacao_User_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV22TFContagemResultadoNotificacao_Port",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV23TFContagemResultadoNotificacao_Port_To",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00VE4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VE4,100,0,true,false )
             ,new CursorDef("P00VE7", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VE7,100,0,true,false )
             ,new CursorDef("P00VE10", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VE10,100,0,true,false )
             ,new CursorDef("P00VE13", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VE13,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((short[]) buf[7])[0] = rslt.getShort(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((short[]) buf[9])[0] = rslt.getShort(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((bool[]) buf[11])[0] = rslt.getBool(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((short[]) buf[13])[0] = rslt.getShort(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getString(10, 50) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((String[]) buf[17])[0] = rslt.getString(11, 50) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((String[]) buf[19])[0] = rslt.getLongVarchar(12) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((DateTime[]) buf[21])[0] = rslt.getGXDateTime(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((int[]) buf[23])[0] = rslt.getInt(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((short[]) buf[7])[0] = rslt.getShort(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((short[]) buf[9])[0] = rslt.getShort(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((bool[]) buf[11])[0] = rslt.getBool(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((short[]) buf[13])[0] = rslt.getShort(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getString(10, 50) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((String[]) buf[17])[0] = rslt.getString(11, 50) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((String[]) buf[19])[0] = rslt.getString(12, 100) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((DateTime[]) buf[21])[0] = rslt.getGXDateTime(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((int[]) buf[23])[0] = rslt.getInt(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                return;
             case 2 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((short[]) buf[7])[0] = rslt.getShort(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((short[]) buf[9])[0] = rslt.getShort(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((bool[]) buf[11])[0] = rslt.getBool(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((short[]) buf[13])[0] = rslt.getShort(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getString(10, 50) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((String[]) buf[17])[0] = rslt.getLongVarchar(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((String[]) buf[19])[0] = rslt.getString(12, 100) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((DateTime[]) buf[21])[0] = rslt.getGXDateTime(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((int[]) buf[23])[0] = rslt.getInt(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                return;
             case 3 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((short[]) buf[7])[0] = rslt.getShort(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((short[]) buf[9])[0] = rslt.getShort(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((bool[]) buf[11])[0] = rslt.getBool(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((short[]) buf[13])[0] = rslt.getShort(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getString(10, 50) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((String[]) buf[17])[0] = rslt.getLongVarchar(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((String[]) buf[19])[0] = rslt.getString(12, 100) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((DateTime[]) buf[21])[0] = rslt.getGXDateTime(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((int[]) buf[23])[0] = rslt.getInt(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[30]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[40]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[41]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[30]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[40]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[41]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[30]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[40]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[41]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[30]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[40]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[41]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getdemandanotificacoeswcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getdemandanotificacoeswcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getdemandanotificacoeswcfilterdata") )
          {
             return  ;
          }
          getdemandanotificacoeswcfilterdata worker = new getdemandanotificacoeswcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
