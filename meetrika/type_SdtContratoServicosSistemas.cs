/*
               File: type_SdtContratoServicosSistemas
        Description: Sistemas do Servi�o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:29:36.87
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "ContratoServicosSistemas" )]
   [XmlType(TypeName =  "ContratoServicosSistemas" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtContratoServicosSistemas : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContratoServicosSistemas( )
      {
         /* Constructor for serialization */
         gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicosigla = "";
         gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemasigla = "";
         gxTv_SdtContratoServicosSistemas_Mode = "";
         gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicosigla_Z = "";
         gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemasigla_Z = "";
      }

      public SdtContratoServicosSistemas( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV160ContratoServicos_Codigo ,
                        int AV1067ContratoServicosSistemas_ServicoCod ,
                        int AV1063ContratoServicosSistemas_SistemaCod )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV160ContratoServicos_Codigo,(int)AV1067ContratoServicosSistemas_ServicoCod,(int)AV1063ContratoServicosSistemas_SistemaCod});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"ContratoServicos_Codigo", typeof(int)}, new Object[]{"ContratoServicosSistemas_ServicoCod", typeof(int)}, new Object[]{"ContratoServicosSistemas_SistemaCod", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "ContratoServicosSistemas");
         metadata.Set("BT", "ContratoServicosSistemas");
         metadata.Set("PK", "[ \"ContratoServicos_Codigo\",\"ContratoServicosSistemas_ServicoCod\",\"ContratoServicosSistemas_SistemaCod\" ]");
         metadata.Set("PKAssigned", "[ \"ContratoServicosSistemas_ServicoCod\",\"ContratoServicosSistemas_SistemaCod\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"ContratoServicos_Codigo\" ],\"FKMap\":[  ] },{ \"FK\":[ \"Servico_Codigo\" ],\"FKMap\":[ \"ContratoServicosSistemas_ServicoCod-Servico_Codigo\" ] },{ \"FK\":[ \"Sistema_Codigo\" ],\"FKMap\":[ \"ContratoServicosSistemas_SistemaCod-Sistema_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicossistemas_servicocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicossistemas_servicosigla_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicossistemas_sistemacod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicossistemas_sistemasigla_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicossistemas_servicosigla_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicossistemas_sistemasigla_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtContratoServicosSistemas deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtContratoServicosSistemas)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtContratoServicosSistemas obj ;
         obj = this;
         obj.gxTpr_Contratoservicos_codigo = deserialized.gxTpr_Contratoservicos_codigo;
         obj.gxTpr_Contratoservicossistemas_servicocod = deserialized.gxTpr_Contratoservicossistemas_servicocod;
         obj.gxTpr_Contratoservicossistemas_servicosigla = deserialized.gxTpr_Contratoservicossistemas_servicosigla;
         obj.gxTpr_Contratoservicossistemas_sistemacod = deserialized.gxTpr_Contratoservicossistemas_sistemacod;
         obj.gxTpr_Contratoservicossistemas_sistemasigla = deserialized.gxTpr_Contratoservicossistemas_sistemasigla;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Contratoservicos_codigo_Z = deserialized.gxTpr_Contratoservicos_codigo_Z;
         obj.gxTpr_Contratoservicossistemas_servicocod_Z = deserialized.gxTpr_Contratoservicossistemas_servicocod_Z;
         obj.gxTpr_Contratoservicossistemas_servicosigla_Z = deserialized.gxTpr_Contratoservicossistemas_servicosigla_Z;
         obj.gxTpr_Contratoservicossistemas_sistemacod_Z = deserialized.gxTpr_Contratoservicossistemas_sistemacod_Z;
         obj.gxTpr_Contratoservicossistemas_sistemasigla_Z = deserialized.gxTpr_Contratoservicossistemas_sistemasigla_Z;
         obj.gxTpr_Contratoservicossistemas_servicosigla_N = deserialized.gxTpr_Contratoservicossistemas_servicosigla_N;
         obj.gxTpr_Contratoservicossistemas_sistemasigla_N = deserialized.gxTpr_Contratoservicossistemas_sistemasigla_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_Codigo") )
               {
                  gxTv_SdtContratoServicosSistemas_Contratoservicos_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosSistemas_ServicoCod") )
               {
                  gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosSistemas_ServicoSigla") )
               {
                  gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicosigla = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosSistemas_SistemaCod") )
               {
                  gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosSistemas_SistemaSigla") )
               {
                  gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemasigla = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtContratoServicosSistemas_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtContratoServicosSistemas_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_Codigo_Z") )
               {
                  gxTv_SdtContratoServicosSistemas_Contratoservicos_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosSistemas_ServicoCod_Z") )
               {
                  gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosSistemas_ServicoSigla_Z") )
               {
                  gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicosigla_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosSistemas_SistemaCod_Z") )
               {
                  gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemacod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosSistemas_SistemaSigla_Z") )
               {
                  gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemasigla_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosSistemas_ServicoSigla_N") )
               {
                  gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicosigla_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosSistemas_SistemaSigla_N") )
               {
                  gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemasigla_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "ContratoServicosSistemas";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ContratoServicos_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosSistemas_Contratoservicos_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratoServicosSistemas_ServicoCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratoServicosSistemas_ServicoSigla", StringUtil.RTrim( gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicosigla));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratoServicosSistemas_SistemaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratoServicosSistemas_SistemaSigla", StringUtil.RTrim( gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemasigla));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtContratoServicosSistemas_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosSistemas_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratoServicos_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosSistemas_Contratoservicos_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratoServicosSistemas_ServicoCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratoServicosSistemas_ServicoSigla_Z", StringUtil.RTrim( gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicosigla_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratoServicosSistemas_SistemaCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemacod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratoServicosSistemas_SistemaSigla_Z", StringUtil.RTrim( gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemasigla_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratoServicosSistemas_ServicoSigla_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicosigla_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratoServicosSistemas_SistemaSigla_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemasigla_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ContratoServicos_Codigo", gxTv_SdtContratoServicosSistemas_Contratoservicos_codigo, false);
         AddObjectProperty("ContratoServicosSistemas_ServicoCod", gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicocod, false);
         AddObjectProperty("ContratoServicosSistemas_ServicoSigla", gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicosigla, false);
         AddObjectProperty("ContratoServicosSistemas_SistemaCod", gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemacod, false);
         AddObjectProperty("ContratoServicosSistemas_SistemaSigla", gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemasigla, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtContratoServicosSistemas_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtContratoServicosSistemas_Initialized, false);
            AddObjectProperty("ContratoServicos_Codigo_Z", gxTv_SdtContratoServicosSistemas_Contratoservicos_codigo_Z, false);
            AddObjectProperty("ContratoServicosSistemas_ServicoCod_Z", gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicocod_Z, false);
            AddObjectProperty("ContratoServicosSistemas_ServicoSigla_Z", gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicosigla_Z, false);
            AddObjectProperty("ContratoServicosSistemas_SistemaCod_Z", gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemacod_Z, false);
            AddObjectProperty("ContratoServicosSistemas_SistemaSigla_Z", gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemasigla_Z, false);
            AddObjectProperty("ContratoServicosSistemas_ServicoSigla_N", gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicosigla_N, false);
            AddObjectProperty("ContratoServicosSistemas_SistemaSigla_N", gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemasigla_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "ContratoServicos_Codigo" )]
      [  XmlElement( ElementName = "ContratoServicos_Codigo"   )]
      public int gxTpr_Contratoservicos_codigo
      {
         get {
            return gxTv_SdtContratoServicosSistemas_Contratoservicos_codigo ;
         }

         set {
            if ( gxTv_SdtContratoServicosSistemas_Contratoservicos_codigo != value )
            {
               gxTv_SdtContratoServicosSistemas_Mode = "INS";
               this.gxTv_SdtContratoServicosSistemas_Contratoservicos_codigo_Z_SetNull( );
               this.gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicocod_Z_SetNull( );
               this.gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicosigla_Z_SetNull( );
               this.gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemacod_Z_SetNull( );
               this.gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemasigla_Z_SetNull( );
            }
            gxTv_SdtContratoServicosSistemas_Contratoservicos_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContratoServicosSistemas_ServicoCod" )]
      [  XmlElement( ElementName = "ContratoServicosSistemas_ServicoCod"   )]
      public int gxTpr_Contratoservicossistemas_servicocod
      {
         get {
            return gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicocod ;
         }

         set {
            if ( gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicocod != value )
            {
               gxTv_SdtContratoServicosSistemas_Mode = "INS";
               this.gxTv_SdtContratoServicosSistemas_Contratoservicos_codigo_Z_SetNull( );
               this.gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicocod_Z_SetNull( );
               this.gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicosigla_Z_SetNull( );
               this.gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemacod_Z_SetNull( );
               this.gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemasigla_Z_SetNull( );
            }
            gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicocod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContratoServicosSistemas_ServicoSigla" )]
      [  XmlElement( ElementName = "ContratoServicosSistemas_ServicoSigla"   )]
      public String gxTpr_Contratoservicossistemas_servicosigla
      {
         get {
            return gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicosigla ;
         }

         set {
            gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicosigla_N = 0;
            gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicosigla = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicosigla_SetNull( )
      {
         gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicosigla_N = 1;
         gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicosigla = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicosigla_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosSistemas_SistemaCod" )]
      [  XmlElement( ElementName = "ContratoServicosSistemas_SistemaCod"   )]
      public int gxTpr_Contratoservicossistemas_sistemacod
      {
         get {
            return gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemacod ;
         }

         set {
            if ( gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemacod != value )
            {
               gxTv_SdtContratoServicosSistemas_Mode = "INS";
               this.gxTv_SdtContratoServicosSistemas_Contratoservicos_codigo_Z_SetNull( );
               this.gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicocod_Z_SetNull( );
               this.gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicosigla_Z_SetNull( );
               this.gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemacod_Z_SetNull( );
               this.gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemasigla_Z_SetNull( );
            }
            gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemacod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContratoServicosSistemas_SistemaSigla" )]
      [  XmlElement( ElementName = "ContratoServicosSistemas_SistemaSigla"   )]
      public String gxTpr_Contratoservicossistemas_sistemasigla
      {
         get {
            return gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemasigla ;
         }

         set {
            gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemasigla_N = 0;
            gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemasigla = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemasigla_SetNull( )
      {
         gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemasigla_N = 1;
         gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemasigla = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemasigla_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtContratoServicosSistemas_Mode ;
         }

         set {
            gxTv_SdtContratoServicosSistemas_Mode = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicosSistemas_Mode_SetNull( )
      {
         gxTv_SdtContratoServicosSistemas_Mode = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicosSistemas_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtContratoServicosSistemas_Initialized ;
         }

         set {
            gxTv_SdtContratoServicosSistemas_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicosSistemas_Initialized_SetNull( )
      {
         gxTv_SdtContratoServicosSistemas_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicosSistemas_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_Codigo_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_Codigo_Z"   )]
      public int gxTpr_Contratoservicos_codigo_Z
      {
         get {
            return gxTv_SdtContratoServicosSistemas_Contratoservicos_codigo_Z ;
         }

         set {
            gxTv_SdtContratoServicosSistemas_Contratoservicos_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratoServicosSistemas_Contratoservicos_codigo_Z_SetNull( )
      {
         gxTv_SdtContratoServicosSistemas_Contratoservicos_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicosSistemas_Contratoservicos_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosSistemas_ServicoCod_Z" )]
      [  XmlElement( ElementName = "ContratoServicosSistemas_ServicoCod_Z"   )]
      public int gxTpr_Contratoservicossistemas_servicocod_Z
      {
         get {
            return gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicocod_Z ;
         }

         set {
            gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicocod_Z_SetNull( )
      {
         gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosSistemas_ServicoSigla_Z" )]
      [  XmlElement( ElementName = "ContratoServicosSistemas_ServicoSigla_Z"   )]
      public String gxTpr_Contratoservicossistemas_servicosigla_Z
      {
         get {
            return gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicosigla_Z ;
         }

         set {
            gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicosigla_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicosigla_Z_SetNull( )
      {
         gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicosigla_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicosigla_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosSistemas_SistemaCod_Z" )]
      [  XmlElement( ElementName = "ContratoServicosSistemas_SistemaCod_Z"   )]
      public int gxTpr_Contratoservicossistemas_sistemacod_Z
      {
         get {
            return gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemacod_Z ;
         }

         set {
            gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemacod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemacod_Z_SetNull( )
      {
         gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemacod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemacod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosSistemas_SistemaSigla_Z" )]
      [  XmlElement( ElementName = "ContratoServicosSistemas_SistemaSigla_Z"   )]
      public String gxTpr_Contratoservicossistemas_sistemasigla_Z
      {
         get {
            return gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemasigla_Z ;
         }

         set {
            gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemasigla_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemasigla_Z_SetNull( )
      {
         gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemasigla_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemasigla_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosSistemas_ServicoSigla_N" )]
      [  XmlElement( ElementName = "ContratoServicosSistemas_ServicoSigla_N"   )]
      public short gxTpr_Contratoservicossistemas_servicosigla_N
      {
         get {
            return gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicosigla_N ;
         }

         set {
            gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicosigla_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicosigla_N_SetNull( )
      {
         gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicosigla_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicosigla_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosSistemas_SistemaSigla_N" )]
      [  XmlElement( ElementName = "ContratoServicosSistemas_SistemaSigla_N"   )]
      public short gxTpr_Contratoservicossistemas_sistemasigla_N
      {
         get {
            return gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemasigla_N ;
         }

         set {
            gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemasigla_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemasigla_N_SetNull( )
      {
         gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemasigla_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemasigla_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicosigla = "";
         gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemasigla = "";
         gxTv_SdtContratoServicosSistemas_Mode = "";
         gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicosigla_Z = "";
         gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemasigla_Z = "";
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "contratoservicossistemas", "GeneXus.Programs.contratoservicossistemas_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtContratoServicosSistemas_Initialized ;
      private short gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicosigla_N ;
      private short gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemasigla_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtContratoServicosSistemas_Contratoservicos_codigo ;
      private int gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicocod ;
      private int gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemacod ;
      private int gxTv_SdtContratoServicosSistemas_Contratoservicos_codigo_Z ;
      private int gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicocod_Z ;
      private int gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemacod_Z ;
      private String gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicosigla ;
      private String gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemasigla ;
      private String gxTv_SdtContratoServicosSistemas_Mode ;
      private String gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_servicosigla_Z ;
      private String gxTv_SdtContratoServicosSistemas_Contratoservicossistemas_sistemasigla_Z ;
      private String sTagName ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"ContratoServicosSistemas", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtContratoServicosSistemas_RESTInterface : GxGenericCollectionItem<SdtContratoServicosSistemas>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContratoServicosSistemas_RESTInterface( ) : base()
      {
      }

      public SdtContratoServicosSistemas_RESTInterface( SdtContratoServicosSistemas psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ContratoServicos_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratoservicos_codigo
      {
         get {
            return sdt.gxTpr_Contratoservicos_codigo ;
         }

         set {
            sdt.gxTpr_Contratoservicos_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoServicosSistemas_ServicoCod" , Order = 1 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratoservicossistemas_servicocod
      {
         get {
            return sdt.gxTpr_Contratoservicossistemas_servicocod ;
         }

         set {
            sdt.gxTpr_Contratoservicossistemas_servicocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoServicosSistemas_ServicoSigla" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Contratoservicossistemas_servicosigla
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratoservicossistemas_servicosigla) ;
         }

         set {
            sdt.gxTpr_Contratoservicossistemas_servicosigla = (String)(value);
         }

      }

      [DataMember( Name = "ContratoServicosSistemas_SistemaCod" , Order = 3 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratoservicossistemas_sistemacod
      {
         get {
            return sdt.gxTpr_Contratoservicossistemas_sistemacod ;
         }

         set {
            sdt.gxTpr_Contratoservicossistemas_sistemacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoServicosSistemas_SistemaSigla" , Order = 4 )]
      [GxSeudo()]
      public String gxTpr_Contratoservicossistemas_sistemasigla
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratoservicossistemas_sistemasigla) ;
         }

         set {
            sdt.gxTpr_Contratoservicossistemas_sistemasigla = (String)(value);
         }

      }

      public SdtContratoServicosSistemas sdt
      {
         get {
            return (SdtContratoServicosSistemas)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtContratoServicosSistemas() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 14 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
