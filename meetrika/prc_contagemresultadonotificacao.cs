/*
               File: PRC_ContagemResultadoNotificacao
        Description: Grava as Notificações
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:28:14.78
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_contagemresultadonotificacao : GXProcedure
   {
      public prc_contagemresultadonotificacao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_contagemresultadonotificacao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( IGxCollection aP0_Usuarios ,
                           String aP1_ContagemResultadoNotificacao_Assunto ,
                           String aP2_ContagemResultadoNotificacao_CorpoEmail ,
                           String aP3_ContagemResultadoNotificacao_Midia ,
                           short aP4_ContagemResultadoNotificacao_Logged ,
                           String aP5_Resultado )
      {
         this.AV10Usuarios = aP0_Usuarios;
         this.AV18ContagemResultadoNotificacao_Assunto = aP1_ContagemResultadoNotificacao_Assunto;
         this.AV19ContagemResultadoNotificacao_CorpoEmail = aP2_ContagemResultadoNotificacao_CorpoEmail;
         this.AV20ContagemResultadoNotificacao_Midia = aP3_ContagemResultadoNotificacao_Midia;
         this.AV25ContagemResultadoNotificacao_Logged = aP4_ContagemResultadoNotificacao_Logged;
         this.AV17Resultado = aP5_Resultado;
         initialize();
         executePrivate();
      }

      public void executeSubmit( IGxCollection aP0_Usuarios ,
                                 String aP1_ContagemResultadoNotificacao_Assunto ,
                                 String aP2_ContagemResultadoNotificacao_CorpoEmail ,
                                 String aP3_ContagemResultadoNotificacao_Midia ,
                                 short aP4_ContagemResultadoNotificacao_Logged ,
                                 String aP5_Resultado )
      {
         prc_contagemresultadonotificacao objprc_contagemresultadonotificacao;
         objprc_contagemresultadonotificacao = new prc_contagemresultadonotificacao();
         objprc_contagemresultadonotificacao.AV10Usuarios = aP0_Usuarios;
         objprc_contagemresultadonotificacao.AV18ContagemResultadoNotificacao_Assunto = aP1_ContagemResultadoNotificacao_Assunto;
         objprc_contagemresultadonotificacao.AV19ContagemResultadoNotificacao_CorpoEmail = aP2_ContagemResultadoNotificacao_CorpoEmail;
         objprc_contagemresultadonotificacao.AV20ContagemResultadoNotificacao_Midia = aP3_ContagemResultadoNotificacao_Midia;
         objprc_contagemresultadonotificacao.AV25ContagemResultadoNotificacao_Logged = aP4_ContagemResultadoNotificacao_Logged;
         objprc_contagemresultadonotificacao.AV17Resultado = aP5_Resultado;
         objprc_contagemresultadonotificacao.context.SetSubmitInitialConfig(context);
         objprc_contagemresultadonotificacao.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_contagemresultadonotificacao);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_contagemresultadonotificacao)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV15ContagemResultado_Codigos.FromXml(AV11WebSession.Get("DemandaCodigo"), "Collection");
         AV27ContagemResultadoNotificacao_ReenvioCod = (long)(NumberUtil.Val( AV11WebSession.Get("CodigoNtf"), "."));
         AV11WebSession.Remove("CodigoNtf");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV21WWPContext) ;
         AV8ContagemResultadoNotificacao = new SdtContagemResultadoNotificacao(context);
         AV8ContagemResultadoNotificacao.gxTpr_Contagemresultadonotificacao_assunto = AV18ContagemResultadoNotificacao_Assunto;
         AV8ContagemResultadoNotificacao.gxTpr_Contagemresultadonotificacao_corpoemail = AV19ContagemResultadoNotificacao_CorpoEmail;
         AV8ContagemResultadoNotificacao.gxTpr_Contagemresultadonotificacao_datahora = DateTimeUtil.ServerNow( context, "DEFAULT");
         AV8ContagemResultadoNotificacao.gxTpr_Contagemresultadonotificacao_midia = AV20ContagemResultadoNotificacao_Midia;
         AV8ContagemResultadoNotificacao.gxTpr_Contagemresultadonotificacao_observacao = AV17Resultado;
         AV8ContagemResultadoNotificacao.gxTpr_Contagemresultadonotificacao_usucod = AV21WWPContext.gxTpr_Userid;
         AV8ContagemResultadoNotificacao.gxTpr_Contagemresultadonotificacao_areatrabalhocod = AV21WWPContext.gxTpr_Areatrabalho_codigo;
         if ( AV27ContagemResultadoNotificacao_ReenvioCod > 0 )
         {
            AV8ContagemResultadoNotificacao.gxTpr_Contagemresultadonotificacao_reenviocod = AV27ContagemResultadoNotificacao_ReenvioCod;
         }
         else
         {
            AV8ContagemResultadoNotificacao.gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_reenviocod_SetNull();
         }
         /* Using cursor P009Y2 */
         pr_default.execute(0, new Object[] {AV21WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A29Contratante_Codigo = P009Y2_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P009Y2_n29Contratante_Codigo[0];
            A5AreaTrabalho_Codigo = P009Y2_A5AreaTrabalho_Codigo[0];
            A547Contratante_EmailSdaHost = P009Y2_A547Contratante_EmailSdaHost[0];
            n547Contratante_EmailSdaHost = P009Y2_n547Contratante_EmailSdaHost[0];
            A548Contratante_EmailSdaUser = P009Y2_A548Contratante_EmailSdaUser[0];
            n548Contratante_EmailSdaUser = P009Y2_n548Contratante_EmailSdaUser[0];
            A552Contratante_EmailSdaPort = P009Y2_A552Contratante_EmailSdaPort[0];
            n552Contratante_EmailSdaPort = P009Y2_n552Contratante_EmailSdaPort[0];
            A551Contratante_EmailSdaAut = P009Y2_A551Contratante_EmailSdaAut[0];
            n551Contratante_EmailSdaAut = P009Y2_n551Contratante_EmailSdaAut[0];
            A1048Contratante_EmailSdaSec = P009Y2_A1048Contratante_EmailSdaSec[0];
            n1048Contratante_EmailSdaSec = P009Y2_n1048Contratante_EmailSdaSec[0];
            A547Contratante_EmailSdaHost = P009Y2_A547Contratante_EmailSdaHost[0];
            n547Contratante_EmailSdaHost = P009Y2_n547Contratante_EmailSdaHost[0];
            A548Contratante_EmailSdaUser = P009Y2_A548Contratante_EmailSdaUser[0];
            n548Contratante_EmailSdaUser = P009Y2_n548Contratante_EmailSdaUser[0];
            A552Contratante_EmailSdaPort = P009Y2_A552Contratante_EmailSdaPort[0];
            n552Contratante_EmailSdaPort = P009Y2_n552Contratante_EmailSdaPort[0];
            A551Contratante_EmailSdaAut = P009Y2_A551Contratante_EmailSdaAut[0];
            n551Contratante_EmailSdaAut = P009Y2_n551Contratante_EmailSdaAut[0];
            A1048Contratante_EmailSdaSec = P009Y2_A1048Contratante_EmailSdaSec[0];
            n1048Contratante_EmailSdaSec = P009Y2_n1048Contratante_EmailSdaSec[0];
            AV8ContagemResultadoNotificacao.gxTpr_Contagemresultadonotificacao_host = A547Contratante_EmailSdaHost;
            AV8ContagemResultadoNotificacao.gxTpr_Contagemresultadonotificacao_user = A548Contratante_EmailSdaUser;
            AV8ContagemResultadoNotificacao.gxTpr_Contagemresultadonotificacao_port = A552Contratante_EmailSdaPort;
            AV8ContagemResultadoNotificacao.gxTpr_Contagemresultadonotificacao_aut = A551Contratante_EmailSdaAut;
            AV8ContagemResultadoNotificacao.gxTpr_Contagemresultadonotificacao_sec = A1048Contratante_EmailSdaSec;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         AV8ContagemResultadoNotificacao.gxTpr_Contagemresultadonotificacao_logged = AV25ContagemResultadoNotificacao_Logged;
         AV33GXV1 = 1;
         while ( AV33GXV1 <= AV15ContagemResultado_Codigos.Count )
         {
            AV14ContagemResultado_Codigo = (int)(AV15ContagemResultado_Codigos.GetNumeric(AV33GXV1));
            AV16ContagemResultadoNotificacaoDemanda = new SdtContagemResultadoNotificacao_Demanda(context);
            AV16ContagemResultadoNotificacaoDemanda.gxTpr_Contagemresultado_codigo = AV14ContagemResultado_Codigo;
            AV8ContagemResultadoNotificacao.gxTpr_Demanda.Add(AV16ContagemResultadoNotificacaoDemanda, 0);
            AV33GXV1 = (int)(AV33GXV1+1);
         }
         AV29Repetidos = (IGxCollection)(AV10Usuarios.Clone());
         AV10Usuarios.Clear();
         AV34GXV2 = 1;
         while ( AV34GXV2 <= AV29Repetidos.Count )
         {
            AV12Usuario_Codigo = (int)(AV29Repetidos.GetNumeric(AV34GXV2));
            if ( (0==AV10Usuarios.IndexOf(AV12Usuario_Codigo)) )
            {
               AV10Usuarios.Add(AV12Usuario_Codigo, 0);
            }
            AV34GXV2 = (int)(AV34GXV2+1);
         }
         AV35GXV3 = 1;
         while ( AV35GXV3 <= AV10Usuarios.Count )
         {
            AV12Usuario_Codigo = (int)(AV10Usuarios.GetNumeric(AV35GXV3));
            /* Using cursor P009Y3 */
            pr_default.execute(1, new Object[] {AV12Usuario_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A1Usuario_Codigo = P009Y3_A1Usuario_Codigo[0];
               A1647Usuario_Email = P009Y3_A1647Usuario_Email[0];
               n1647Usuario_Email = P009Y3_n1647Usuario_Email[0];
               A341Usuario_UserGamGuid = P009Y3_A341Usuario_UserGamGuid[0];
               AV28Email = A1647Usuario_Email;
               if ( String.IsNullOrEmpty(StringUtil.RTrim( AV28Email)) )
               {
                  GXt_char1 = AV28Email;
                  new prc_usuarioemailget(context ).execute(  A341Usuario_UserGamGuid, out  GXt_char1) ;
                  AV28Email = GXt_char1;
               }
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
            AV9ContagemResultadoNotificacaoDestinatario = new SdtContagemResultadoNotificacao_Destinatario(context);
            AV9ContagemResultadoNotificacaoDestinatario.gxTpr_Contagemresultadonotificacao_destcod = AV12Usuario_Codigo;
            AV9ContagemResultadoNotificacaoDestinatario.gxTpr_Contagemresultadonotificacao_destemail = AV28Email;
            AV8ContagemResultadoNotificacao.gxTpr_Destinatario.Add(AV9ContagemResultadoNotificacaoDestinatario, 0);
            AV35GXV3 = (int)(AV35GXV3+1);
         }
         AV8ContagemResultadoNotificacao.Save();
         if ( AV8ContagemResultadoNotificacao.Success() )
         {
            context.CommitDataStores( "PRC_ContagemResultadoNotificacao");
         }
         else
         {
            context.RollbackDataStores( "PRC_ContagemResultadoNotificacao");
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV15ContagemResultado_Codigos = new GxSimpleCollection();
         AV11WebSession = context.GetSession();
         AV21WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8ContagemResultadoNotificacao = new SdtContagemResultadoNotificacao(context);
         scmdbuf = "";
         P009Y2_A29Contratante_Codigo = new int[1] ;
         P009Y2_n29Contratante_Codigo = new bool[] {false} ;
         P009Y2_A5AreaTrabalho_Codigo = new int[1] ;
         P009Y2_A547Contratante_EmailSdaHost = new String[] {""} ;
         P009Y2_n547Contratante_EmailSdaHost = new bool[] {false} ;
         P009Y2_A548Contratante_EmailSdaUser = new String[] {""} ;
         P009Y2_n548Contratante_EmailSdaUser = new bool[] {false} ;
         P009Y2_A552Contratante_EmailSdaPort = new short[1] ;
         P009Y2_n552Contratante_EmailSdaPort = new bool[] {false} ;
         P009Y2_A551Contratante_EmailSdaAut = new bool[] {false} ;
         P009Y2_n551Contratante_EmailSdaAut = new bool[] {false} ;
         P009Y2_A1048Contratante_EmailSdaSec = new short[1] ;
         P009Y2_n1048Contratante_EmailSdaSec = new bool[] {false} ;
         A547Contratante_EmailSdaHost = "";
         A548Contratante_EmailSdaUser = "";
         AV16ContagemResultadoNotificacaoDemanda = new SdtContagemResultadoNotificacao_Demanda(context);
         AV29Repetidos = new GxSimpleCollection();
         P009Y3_A1Usuario_Codigo = new int[1] ;
         P009Y3_A1647Usuario_Email = new String[] {""} ;
         P009Y3_n1647Usuario_Email = new bool[] {false} ;
         P009Y3_A341Usuario_UserGamGuid = new String[] {""} ;
         A1647Usuario_Email = "";
         A341Usuario_UserGamGuid = "";
         AV28Email = "";
         GXt_char1 = "";
         AV9ContagemResultadoNotificacaoDestinatario = new SdtContagemResultadoNotificacao_Destinatario(context);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_contagemresultadonotificacao__default(),
            new Object[][] {
                new Object[] {
               P009Y2_A29Contratante_Codigo, P009Y2_n29Contratante_Codigo, P009Y2_A5AreaTrabalho_Codigo, P009Y2_A547Contratante_EmailSdaHost, P009Y2_n547Contratante_EmailSdaHost, P009Y2_A548Contratante_EmailSdaUser, P009Y2_n548Contratante_EmailSdaUser, P009Y2_A552Contratante_EmailSdaPort, P009Y2_n552Contratante_EmailSdaPort, P009Y2_A551Contratante_EmailSdaAut,
               P009Y2_n551Contratante_EmailSdaAut, P009Y2_A1048Contratante_EmailSdaSec, P009Y2_n1048Contratante_EmailSdaSec
               }
               , new Object[] {
               P009Y3_A1Usuario_Codigo, P009Y3_A1647Usuario_Email, P009Y3_n1647Usuario_Email, P009Y3_A341Usuario_UserGamGuid
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV25ContagemResultadoNotificacao_Logged ;
      private short A552Contratante_EmailSdaPort ;
      private short A1048Contratante_EmailSdaSec ;
      private int A29Contratante_Codigo ;
      private int A5AreaTrabalho_Codigo ;
      private int AV33GXV1 ;
      private int AV14ContagemResultado_Codigo ;
      private int AV34GXV2 ;
      private int AV12Usuario_Codigo ;
      private int AV35GXV3 ;
      private int A1Usuario_Codigo ;
      private long AV27ContagemResultadoNotificacao_ReenvioCod ;
      private String AV20ContagemResultadoNotificacao_Midia ;
      private String AV17Resultado ;
      private String scmdbuf ;
      private String A341Usuario_UserGamGuid ;
      private String GXt_char1 ;
      private bool n29Contratante_Codigo ;
      private bool n547Contratante_EmailSdaHost ;
      private bool n548Contratante_EmailSdaUser ;
      private bool n552Contratante_EmailSdaPort ;
      private bool A551Contratante_EmailSdaAut ;
      private bool n551Contratante_EmailSdaAut ;
      private bool n1048Contratante_EmailSdaSec ;
      private bool n1647Usuario_Email ;
      private String AV18ContagemResultadoNotificacao_Assunto ;
      private String AV19ContagemResultadoNotificacao_CorpoEmail ;
      private String A547Contratante_EmailSdaHost ;
      private String A548Contratante_EmailSdaUser ;
      private String A1647Usuario_Email ;
      private String AV28Email ;
      private IGxSession AV11WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P009Y2_A29Contratante_Codigo ;
      private bool[] P009Y2_n29Contratante_Codigo ;
      private int[] P009Y2_A5AreaTrabalho_Codigo ;
      private String[] P009Y2_A547Contratante_EmailSdaHost ;
      private bool[] P009Y2_n547Contratante_EmailSdaHost ;
      private String[] P009Y2_A548Contratante_EmailSdaUser ;
      private bool[] P009Y2_n548Contratante_EmailSdaUser ;
      private short[] P009Y2_A552Contratante_EmailSdaPort ;
      private bool[] P009Y2_n552Contratante_EmailSdaPort ;
      private bool[] P009Y2_A551Contratante_EmailSdaAut ;
      private bool[] P009Y2_n551Contratante_EmailSdaAut ;
      private short[] P009Y2_A1048Contratante_EmailSdaSec ;
      private bool[] P009Y2_n1048Contratante_EmailSdaSec ;
      private int[] P009Y3_A1Usuario_Codigo ;
      private String[] P009Y3_A1647Usuario_Email ;
      private bool[] P009Y3_n1647Usuario_Email ;
      private String[] P009Y3_A341Usuario_UserGamGuid ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV10Usuarios ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV15ContagemResultado_Codigos ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV29Repetidos ;
      private SdtContagemResultadoNotificacao AV8ContagemResultadoNotificacao ;
      private SdtContagemResultadoNotificacao_Destinatario AV9ContagemResultadoNotificacaoDestinatario ;
      private SdtContagemResultadoNotificacao_Demanda AV16ContagemResultadoNotificacaoDemanda ;
      private wwpbaseobjects.SdtWWPContext AV21WWPContext ;
   }

   public class prc_contagemresultadonotificacao__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP009Y2 ;
          prmP009Y2 = new Object[] {
          new Object[] {"@AV21WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009Y3 ;
          prmP009Y3 = new Object[] {
          new Object[] {"@AV12Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P009Y2", "SELECT TOP 1 T1.[Contratante_Codigo], T1.[AreaTrabalho_Codigo], T2.[Contratante_EmailSdaHost], T2.[Contratante_EmailSdaUser], T2.[Contratante_EmailSdaPort], T2.[Contratante_EmailSdaAut], T2.[Contratante_EmailSdaSec] FROM ([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) WHERE T1.[AreaTrabalho_Codigo] = @AV21WWPC_1Areatrabalho_codigo ORDER BY T1.[AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009Y2,1,0,false,true )
             ,new CursorDef("P009Y3", "SELECT TOP 1 [Usuario_Codigo], [Usuario_Email], [Usuario_UserGamGuid] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @AV12Usuario_Codigo ORDER BY [Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009Y3,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((short[]) buf[7])[0] = rslt.getShort(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((bool[]) buf[9])[0] = rslt.getBool(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((short[]) buf[11])[0] = rslt.getShort(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 40) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
