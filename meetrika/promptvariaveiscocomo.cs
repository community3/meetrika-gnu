/*
               File: PromptVariaveisCocomo
        Description: Selecione Variaveis Cocomo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 18:55:9.64
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptvariaveiscocomo : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptvariaveiscocomo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptvariaveiscocomo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutVariavelCocomo_AreaTrabalhoCod ,
                           ref String aP1_InOutVariavelCocomo_Sigla ,
                           ref String aP2_InOutVariavelCocomo_Tipo ,
                           ref short aP3_InOutVariavelCocomo_Sequencial ,
                           ref String aP4_InOutVariavelCocomo_Nome )
      {
         this.AV7InOutVariavelCocomo_AreaTrabalhoCod = aP0_InOutVariavelCocomo_AreaTrabalhoCod;
         this.AV8InOutVariavelCocomo_Sigla = aP1_InOutVariavelCocomo_Sigla;
         this.AV9InOutVariavelCocomo_Tipo = aP2_InOutVariavelCocomo_Tipo;
         this.AV33InOutVariavelCocomo_Sequencial = aP3_InOutVariavelCocomo_Sequencial;
         this.AV10InOutVariavelCocomo_Nome = aP4_InOutVariavelCocomo_Nome;
         executePrivate();
         aP0_InOutVariavelCocomo_AreaTrabalhoCod=this.AV7InOutVariavelCocomo_AreaTrabalhoCod;
         aP1_InOutVariavelCocomo_Sigla=this.AV8InOutVariavelCocomo_Sigla;
         aP2_InOutVariavelCocomo_Tipo=this.AV9InOutVariavelCocomo_Tipo;
         aP3_InOutVariavelCocomo_Sequencial=this.AV33InOutVariavelCocomo_Sequencial;
         aP4_InOutVariavelCocomo_Nome=this.AV10InOutVariavelCocomo_Nome;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         cmbVariavelCocomo_Tipo = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_80 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_80_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_80_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV15OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
               AV16OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16OrderedDsc", AV16OrderedDsc);
               AV17DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersSelector1", AV17DynamicFiltersSelector1);
               AV18DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18DynamicFiltersOperator1), 4, 0)));
               AV19VariavelCocomo_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19VariavelCocomo_Nome1", AV19VariavelCocomo_Nome1);
               AV21DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
               AV23VariavelCocomo_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23VariavelCocomo_Nome2", AV23VariavelCocomo_Nome2);
               AV25DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
               AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
               AV27VariavelCocomo_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27VariavelCocomo_Nome3", AV27VariavelCocomo_Nome3);
               AV20DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV24DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
               AV35TFVariavelCocomo_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFVariavelCocomo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFVariavelCocomo_AreaTrabalhoCod), 6, 0)));
               AV36TFVariavelCocomo_AreaTrabalhoCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFVariavelCocomo_AreaTrabalhoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFVariavelCocomo_AreaTrabalhoCod_To), 6, 0)));
               AV39TFVariavelCocomo_Sigla = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFVariavelCocomo_Sigla", AV39TFVariavelCocomo_Sigla);
               AV40TFVariavelCocomo_Sigla_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFVariavelCocomo_Sigla_Sel", AV40TFVariavelCocomo_Sigla_Sel);
               AV43TFVariavelCocomo_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFVariavelCocomo_Nome", AV43TFVariavelCocomo_Nome);
               AV44TFVariavelCocomo_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFVariavelCocomo_Nome_Sel", AV44TFVariavelCocomo_Nome_Sel);
               AV51TFVariavelCocomo_ProjetoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFVariavelCocomo_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFVariavelCocomo_ProjetoCod), 6, 0)));
               AV52TFVariavelCocomo_ProjetoCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFVariavelCocomo_ProjetoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52TFVariavelCocomo_ProjetoCod_To), 6, 0)));
               AV55TFVariavelCocomo_Data = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFVariavelCocomo_Data", context.localUtil.Format(AV55TFVariavelCocomo_Data, "99/99/99"));
               AV56TFVariavelCocomo_Data_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFVariavelCocomo_Data_To", context.localUtil.Format(AV56TFVariavelCocomo_Data_To, "99/99/99"));
               AV61TFVariavelCocomo_ExtraBaixo = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFVariavelCocomo_ExtraBaixo", StringUtil.LTrim( StringUtil.Str( AV61TFVariavelCocomo_ExtraBaixo, 12, 2)));
               AV62TFVariavelCocomo_ExtraBaixo_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFVariavelCocomo_ExtraBaixo_To", StringUtil.LTrim( StringUtil.Str( AV62TFVariavelCocomo_ExtraBaixo_To, 12, 2)));
               AV65TFVariavelCocomo_MuitoBaixo = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFVariavelCocomo_MuitoBaixo", StringUtil.LTrim( StringUtil.Str( AV65TFVariavelCocomo_MuitoBaixo, 12, 2)));
               AV66TFVariavelCocomo_MuitoBaixo_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFVariavelCocomo_MuitoBaixo_To", StringUtil.LTrim( StringUtil.Str( AV66TFVariavelCocomo_MuitoBaixo_To, 12, 2)));
               AV69TFVariavelCocomo_Baixo = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFVariavelCocomo_Baixo", StringUtil.LTrim( StringUtil.Str( AV69TFVariavelCocomo_Baixo, 12, 2)));
               AV70TFVariavelCocomo_Baixo_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFVariavelCocomo_Baixo_To", StringUtil.LTrim( StringUtil.Str( AV70TFVariavelCocomo_Baixo_To, 12, 2)));
               AV73TFVariavelCocomo_Nominal = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFVariavelCocomo_Nominal", StringUtil.LTrim( StringUtil.Str( AV73TFVariavelCocomo_Nominal, 12, 2)));
               AV74TFVariavelCocomo_Nominal_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFVariavelCocomo_Nominal_To", StringUtil.LTrim( StringUtil.Str( AV74TFVariavelCocomo_Nominal_To, 12, 2)));
               AV77TFVariavelCocomo_Alto = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFVariavelCocomo_Alto", StringUtil.LTrim( StringUtil.Str( AV77TFVariavelCocomo_Alto, 12, 2)));
               AV78TFVariavelCocomo_Alto_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFVariavelCocomo_Alto_To", StringUtil.LTrim( StringUtil.Str( AV78TFVariavelCocomo_Alto_To, 12, 2)));
               AV81TFVariavelCocomo_MuitoAlto = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFVariavelCocomo_MuitoAlto", StringUtil.LTrim( StringUtil.Str( AV81TFVariavelCocomo_MuitoAlto, 12, 2)));
               AV82TFVariavelCocomo_MuitoAlto_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFVariavelCocomo_MuitoAlto_To", StringUtil.LTrim( StringUtil.Str( AV82TFVariavelCocomo_MuitoAlto_To, 12, 2)));
               AV85TFVariavelCocomo_ExtraAlto = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TFVariavelCocomo_ExtraAlto", StringUtil.LTrim( StringUtil.Str( AV85TFVariavelCocomo_ExtraAlto, 12, 2)));
               AV86TFVariavelCocomo_ExtraAlto_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86TFVariavelCocomo_ExtraAlto_To", StringUtil.LTrim( StringUtil.Str( AV86TFVariavelCocomo_ExtraAlto_To, 12, 2)));
               AV89TFVariavelCocomo_Usado = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89TFVariavelCocomo_Usado", StringUtil.LTrim( StringUtil.Str( AV89TFVariavelCocomo_Usado, 12, 2)));
               AV90TFVariavelCocomo_Usado_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90TFVariavelCocomo_Usado_To", StringUtil.LTrim( StringUtil.Str( AV90TFVariavelCocomo_Usado_To, 12, 2)));
               AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace", AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace);
               AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace", AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace);
               AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace", AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace);
               AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace", AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace);
               AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace", AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace);
               AV59ddo_VariavelCocomo_DataTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59ddo_VariavelCocomo_DataTitleControlIdToReplace", AV59ddo_VariavelCocomo_DataTitleControlIdToReplace);
               AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace", AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace);
               AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace", AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace);
               AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace", AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace);
               AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace", AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace);
               AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace", AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace);
               AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace", AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace);
               AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace", AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace);
               AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace", AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV48TFVariavelCocomo_Tipo_Sels);
               AV99Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV12GridState);
               AV29DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersIgnoreFirst", AV29DynamicFiltersIgnoreFirst);
               AV28DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersRemoving", AV28DynamicFiltersRemoving);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV15OrderedBy, AV16OrderedDsc, AV17DynamicFiltersSelector1, AV18DynamicFiltersOperator1, AV19VariavelCocomo_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23VariavelCocomo_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27VariavelCocomo_Nome3, AV20DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV35TFVariavelCocomo_AreaTrabalhoCod, AV36TFVariavelCocomo_AreaTrabalhoCod_To, AV39TFVariavelCocomo_Sigla, AV40TFVariavelCocomo_Sigla_Sel, AV43TFVariavelCocomo_Nome, AV44TFVariavelCocomo_Nome_Sel, AV51TFVariavelCocomo_ProjetoCod, AV52TFVariavelCocomo_ProjetoCod_To, AV55TFVariavelCocomo_Data, AV56TFVariavelCocomo_Data_To, AV61TFVariavelCocomo_ExtraBaixo, AV62TFVariavelCocomo_ExtraBaixo_To, AV65TFVariavelCocomo_MuitoBaixo, AV66TFVariavelCocomo_MuitoBaixo_To, AV69TFVariavelCocomo_Baixo, AV70TFVariavelCocomo_Baixo_To, AV73TFVariavelCocomo_Nominal, AV74TFVariavelCocomo_Nominal_To, AV77TFVariavelCocomo_Alto, AV78TFVariavelCocomo_Alto_To, AV81TFVariavelCocomo_MuitoAlto, AV82TFVariavelCocomo_MuitoAlto_To, AV85TFVariavelCocomo_ExtraAlto, AV86TFVariavelCocomo_ExtraAlto_To, AV89TFVariavelCocomo_Usado, AV90TFVariavelCocomo_Usado_To, AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace, AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace, AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace, AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace, AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace, AV59ddo_VariavelCocomo_DataTitleControlIdToReplace, AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace, AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace, AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace, AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace, AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace, AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace, AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace, AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace, AV48TFVariavelCocomo_Tipo_Sels, AV99Pgmname, AV12GridState, AV29DynamicFiltersIgnoreFirst, AV28DynamicFiltersRemoving) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "PromptVariaveisCocomo";
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A992VariavelCocomo_Sequencial), "ZZ9");
               GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
               GXUtil.WriteLog("promptvariaveiscocomo:[SendSecurityCheck value for]"+"VariavelCocomo_Sequencial:"+context.localUtil.Format( (decimal)(A992VariavelCocomo_Sequencial), "ZZ9"));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutVariavelCocomo_AreaTrabalhoCod = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutVariavelCocomo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutVariavelCocomo_AreaTrabalhoCod), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutVariavelCocomo_Sigla = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutVariavelCocomo_Sigla", AV8InOutVariavelCocomo_Sigla);
                  AV9InOutVariavelCocomo_Tipo = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9InOutVariavelCocomo_Tipo", AV9InOutVariavelCocomo_Tipo);
                  AV33InOutVariavelCocomo_Sequencial = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33InOutVariavelCocomo_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33InOutVariavelCocomo_Sequencial), 3, 0)));
                  AV10InOutVariavelCocomo_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10InOutVariavelCocomo_Nome", AV10InOutVariavelCocomo_Nome);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAGL2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV99Pgmname = "PromptVariaveisCocomo";
               context.Gx_err = 0;
               WSGL2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEGL2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203118551039");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptvariaveiscocomo.aspx") + "?" + UrlEncode("" +AV7InOutVariavelCocomo_AreaTrabalhoCod) + "," + UrlEncode(StringUtil.RTrim(AV8InOutVariavelCocomo_Sigla)) + "," + UrlEncode(StringUtil.RTrim(AV9InOutVariavelCocomo_Tipo)) + "," + UrlEncode("" +AV33InOutVariavelCocomo_Sequencial) + "," + UrlEncode(StringUtil.RTrim(AV10InOutVariavelCocomo_Nome))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV16OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV17DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vVARIAVELCOCOMO_NOME1", StringUtil.RTrim( AV19VariavelCocomo_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV21DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vVARIAVELCOCOMO_NOME2", StringUtil.RTrim( AV23VariavelCocomo_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV25DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV26DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vVARIAVELCOCOMO_NOME3", StringUtil.RTrim( AV27VariavelCocomo_Nome3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV20DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV24DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35TFVariavelCocomo_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_AREATRABALHOCOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36TFVariavelCocomo_AreaTrabalhoCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_SIGLA", StringUtil.RTrim( AV39TFVariavelCocomo_Sigla));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_SIGLA_SEL", StringUtil.RTrim( AV40TFVariavelCocomo_Sigla_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_NOME", StringUtil.RTrim( AV43TFVariavelCocomo_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_NOME_SEL", StringUtil.RTrim( AV44TFVariavelCocomo_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_PROJETOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV51TFVariavelCocomo_ProjetoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_PROJETOCOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV52TFVariavelCocomo_ProjetoCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_DATA", context.localUtil.Format(AV55TFVariavelCocomo_Data, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_DATA_TO", context.localUtil.Format(AV56TFVariavelCocomo_Data_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_EXTRABAIXO", StringUtil.LTrim( StringUtil.NToC( AV61TFVariavelCocomo_ExtraBaixo, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_EXTRABAIXO_TO", StringUtil.LTrim( StringUtil.NToC( AV62TFVariavelCocomo_ExtraBaixo_To, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_MUITOBAIXO", StringUtil.LTrim( StringUtil.NToC( AV65TFVariavelCocomo_MuitoBaixo, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_MUITOBAIXO_TO", StringUtil.LTrim( StringUtil.NToC( AV66TFVariavelCocomo_MuitoBaixo_To, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_BAIXO", StringUtil.LTrim( StringUtil.NToC( AV69TFVariavelCocomo_Baixo, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_BAIXO_TO", StringUtil.LTrim( StringUtil.NToC( AV70TFVariavelCocomo_Baixo_To, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_NOMINAL", StringUtil.LTrim( StringUtil.NToC( AV73TFVariavelCocomo_Nominal, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_NOMINAL_TO", StringUtil.LTrim( StringUtil.NToC( AV74TFVariavelCocomo_Nominal_To, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_ALTO", StringUtil.LTrim( StringUtil.NToC( AV77TFVariavelCocomo_Alto, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_ALTO_TO", StringUtil.LTrim( StringUtil.NToC( AV78TFVariavelCocomo_Alto_To, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_MUITOALTO", StringUtil.LTrim( StringUtil.NToC( AV81TFVariavelCocomo_MuitoAlto, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_MUITOALTO_TO", StringUtil.LTrim( StringUtil.NToC( AV82TFVariavelCocomo_MuitoAlto_To, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_EXTRAALTO", StringUtil.LTrim( StringUtil.NToC( AV85TFVariavelCocomo_ExtraAlto, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_EXTRAALTO_TO", StringUtil.LTrim( StringUtil.NToC( AV86TFVariavelCocomo_ExtraAlto_To, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_USADO", StringUtil.LTrim( StringUtil.NToC( AV89TFVariavelCocomo_Usado, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_USADO_TO", StringUtil.LTrim( StringUtil.NToC( AV90TFVariavelCocomo_Usado_To, 12, 2, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_80", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_80), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV94GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV95GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV92DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV92DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vVARIAVELCOCOMO_AREATRABALHOCODTITLEFILTERDATA", AV34VariavelCocomo_AreaTrabalhoCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vVARIAVELCOCOMO_AREATRABALHOCODTITLEFILTERDATA", AV34VariavelCocomo_AreaTrabalhoCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vVARIAVELCOCOMO_SIGLATITLEFILTERDATA", AV38VariavelCocomo_SiglaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vVARIAVELCOCOMO_SIGLATITLEFILTERDATA", AV38VariavelCocomo_SiglaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vVARIAVELCOCOMO_NOMETITLEFILTERDATA", AV42VariavelCocomo_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vVARIAVELCOCOMO_NOMETITLEFILTERDATA", AV42VariavelCocomo_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vVARIAVELCOCOMO_TIPOTITLEFILTERDATA", AV46VariavelCocomo_TipoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vVARIAVELCOCOMO_TIPOTITLEFILTERDATA", AV46VariavelCocomo_TipoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vVARIAVELCOCOMO_PROJETOCODTITLEFILTERDATA", AV50VariavelCocomo_ProjetoCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vVARIAVELCOCOMO_PROJETOCODTITLEFILTERDATA", AV50VariavelCocomo_ProjetoCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vVARIAVELCOCOMO_DATATITLEFILTERDATA", AV54VariavelCocomo_DataTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vVARIAVELCOCOMO_DATATITLEFILTERDATA", AV54VariavelCocomo_DataTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vVARIAVELCOCOMO_EXTRABAIXOTITLEFILTERDATA", AV60VariavelCocomo_ExtraBaixoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vVARIAVELCOCOMO_EXTRABAIXOTITLEFILTERDATA", AV60VariavelCocomo_ExtraBaixoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vVARIAVELCOCOMO_MUITOBAIXOTITLEFILTERDATA", AV64VariavelCocomo_MuitoBaixoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vVARIAVELCOCOMO_MUITOBAIXOTITLEFILTERDATA", AV64VariavelCocomo_MuitoBaixoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vVARIAVELCOCOMO_BAIXOTITLEFILTERDATA", AV68VariavelCocomo_BaixoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vVARIAVELCOCOMO_BAIXOTITLEFILTERDATA", AV68VariavelCocomo_BaixoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vVARIAVELCOCOMO_NOMINALTITLEFILTERDATA", AV72VariavelCocomo_NominalTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vVARIAVELCOCOMO_NOMINALTITLEFILTERDATA", AV72VariavelCocomo_NominalTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vVARIAVELCOCOMO_ALTOTITLEFILTERDATA", AV76VariavelCocomo_AltoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vVARIAVELCOCOMO_ALTOTITLEFILTERDATA", AV76VariavelCocomo_AltoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vVARIAVELCOCOMO_MUITOALTOTITLEFILTERDATA", AV80VariavelCocomo_MuitoAltoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vVARIAVELCOCOMO_MUITOALTOTITLEFILTERDATA", AV80VariavelCocomo_MuitoAltoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vVARIAVELCOCOMO_EXTRAALTOTITLEFILTERDATA", AV84VariavelCocomo_ExtraAltoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vVARIAVELCOCOMO_EXTRAALTOTITLEFILTERDATA", AV84VariavelCocomo_ExtraAltoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vVARIAVELCOCOMO_USADOTITLEFILTERDATA", AV88VariavelCocomo_UsadoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vVARIAVELCOCOMO_USADOTITLEFILTERDATA", AV88VariavelCocomo_UsadoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFVARIAVELCOCOMO_TIPO_SELS", AV48TFVariavelCocomo_Tipo_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFVARIAVELCOCOMO_TIPO_SELS", AV48TFVariavelCocomo_Tipo_Sels);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV99Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV12GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV12GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV29DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV28DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTVARIAVELCOCOMO_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutVariavelCocomo_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTVARIAVELCOCOMO_SIGLA", StringUtil.RTrim( AV8InOutVariavelCocomo_Sigla));
         GxWebStd.gx_hidden_field( context, "vINOUTVARIAVELCOCOMO_TIPO", StringUtil.RTrim( AV9InOutVariavelCocomo_Tipo));
         GxWebStd.gx_hidden_field( context, "vINOUTVARIAVELCOCOMO_SEQUENCIAL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV33InOutVariavelCocomo_Sequencial), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTVARIAVELCOCOMO_NOME", StringUtil.RTrim( AV10InOutVariavelCocomo_Nome));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Caption", StringUtil.RTrim( Ddo_variavelcocomo_areatrabalhocod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Tooltip", StringUtil.RTrim( Ddo_variavelcocomo_areatrabalhocod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Cls", StringUtil.RTrim( Ddo_variavelcocomo_areatrabalhocod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Filteredtext_set", StringUtil.RTrim( Ddo_variavelcocomo_areatrabalhocod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Filteredtextto_set", StringUtil.RTrim( Ddo_variavelcocomo_areatrabalhocod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_variavelcocomo_areatrabalhocod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_variavelcocomo_areatrabalhocod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Includesortasc", StringUtil.BoolToStr( Ddo_variavelcocomo_areatrabalhocod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Includesortdsc", StringUtil.BoolToStr( Ddo_variavelcocomo_areatrabalhocod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Sortedstatus", StringUtil.RTrim( Ddo_variavelcocomo_areatrabalhocod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Includefilter", StringUtil.BoolToStr( Ddo_variavelcocomo_areatrabalhocod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Filtertype", StringUtil.RTrim( Ddo_variavelcocomo_areatrabalhocod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Filterisrange", StringUtil.BoolToStr( Ddo_variavelcocomo_areatrabalhocod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Includedatalist", StringUtil.BoolToStr( Ddo_variavelcocomo_areatrabalhocod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Sortasc", StringUtil.RTrim( Ddo_variavelcocomo_areatrabalhocod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Sortdsc", StringUtil.RTrim( Ddo_variavelcocomo_areatrabalhocod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Cleanfilter", StringUtil.RTrim( Ddo_variavelcocomo_areatrabalhocod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Rangefilterfrom", StringUtil.RTrim( Ddo_variavelcocomo_areatrabalhocod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Rangefilterto", StringUtil.RTrim( Ddo_variavelcocomo_areatrabalhocod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Searchbuttontext", StringUtil.RTrim( Ddo_variavelcocomo_areatrabalhocod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Caption", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Tooltip", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Cls", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Filteredtext_set", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Selectedvalue_set", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Dropdownoptionstype", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Includesortasc", StringUtil.BoolToStr( Ddo_variavelcocomo_sigla_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Includesortdsc", StringUtil.BoolToStr( Ddo_variavelcocomo_sigla_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Sortedstatus", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Includefilter", StringUtil.BoolToStr( Ddo_variavelcocomo_sigla_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Filtertype", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Filterisrange", StringUtil.BoolToStr( Ddo_variavelcocomo_sigla_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Includedatalist", StringUtil.BoolToStr( Ddo_variavelcocomo_sigla_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Datalisttype", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Datalistproc", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_variavelcocomo_sigla_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Sortasc", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Sortdsc", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Loadingdata", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Cleanfilter", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Noresultsfound", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Searchbuttontext", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOME_Caption", StringUtil.RTrim( Ddo_variavelcocomo_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOME_Tooltip", StringUtil.RTrim( Ddo_variavelcocomo_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOME_Cls", StringUtil.RTrim( Ddo_variavelcocomo_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_variavelcocomo_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_variavelcocomo_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_variavelcocomo_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_variavelcocomo_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_variavelcocomo_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_variavelcocomo_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_variavelcocomo_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_variavelcocomo_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOME_Filtertype", StringUtil.RTrim( Ddo_variavelcocomo_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_variavelcocomo_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_variavelcocomo_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOME_Datalisttype", StringUtil.RTrim( Ddo_variavelcocomo_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOME_Datalistproc", StringUtil.RTrim( Ddo_variavelcocomo_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_variavelcocomo_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOME_Sortasc", StringUtil.RTrim( Ddo_variavelcocomo_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOME_Sortdsc", StringUtil.RTrim( Ddo_variavelcocomo_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOME_Loadingdata", StringUtil.RTrim( Ddo_variavelcocomo_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_variavelcocomo_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_variavelcocomo_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_variavelcocomo_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Caption", StringUtil.RTrim( Ddo_variavelcocomo_tipo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Tooltip", StringUtil.RTrim( Ddo_variavelcocomo_tipo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Cls", StringUtil.RTrim( Ddo_variavelcocomo_tipo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Selectedvalue_set", StringUtil.RTrim( Ddo_variavelcocomo_tipo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Dropdownoptionstype", StringUtil.RTrim( Ddo_variavelcocomo_tipo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_variavelcocomo_tipo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Includesortasc", StringUtil.BoolToStr( Ddo_variavelcocomo_tipo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Includesortdsc", StringUtil.BoolToStr( Ddo_variavelcocomo_tipo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Sortedstatus", StringUtil.RTrim( Ddo_variavelcocomo_tipo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Includefilter", StringUtil.BoolToStr( Ddo_variavelcocomo_tipo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Includedatalist", StringUtil.BoolToStr( Ddo_variavelcocomo_tipo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Datalisttype", StringUtil.RTrim( Ddo_variavelcocomo_tipo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Allowmultipleselection", StringUtil.BoolToStr( Ddo_variavelcocomo_tipo_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Datalistfixedvalues", StringUtil.RTrim( Ddo_variavelcocomo_tipo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Sortasc", StringUtil.RTrim( Ddo_variavelcocomo_tipo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Sortdsc", StringUtil.RTrim( Ddo_variavelcocomo_tipo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Cleanfilter", StringUtil.RTrim( Ddo_variavelcocomo_tipo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Searchbuttontext", StringUtil.RTrim( Ddo_variavelcocomo_tipo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_PROJETOCOD_Caption", StringUtil.RTrim( Ddo_variavelcocomo_projetocod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_PROJETOCOD_Tooltip", StringUtil.RTrim( Ddo_variavelcocomo_projetocod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_PROJETOCOD_Cls", StringUtil.RTrim( Ddo_variavelcocomo_projetocod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_PROJETOCOD_Filteredtext_set", StringUtil.RTrim( Ddo_variavelcocomo_projetocod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_PROJETOCOD_Filteredtextto_set", StringUtil.RTrim( Ddo_variavelcocomo_projetocod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_PROJETOCOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_variavelcocomo_projetocod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_PROJETOCOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_variavelcocomo_projetocod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_PROJETOCOD_Includesortasc", StringUtil.BoolToStr( Ddo_variavelcocomo_projetocod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_PROJETOCOD_Includesortdsc", StringUtil.BoolToStr( Ddo_variavelcocomo_projetocod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_PROJETOCOD_Sortedstatus", StringUtil.RTrim( Ddo_variavelcocomo_projetocod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_PROJETOCOD_Includefilter", StringUtil.BoolToStr( Ddo_variavelcocomo_projetocod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_PROJETOCOD_Filtertype", StringUtil.RTrim( Ddo_variavelcocomo_projetocod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_PROJETOCOD_Filterisrange", StringUtil.BoolToStr( Ddo_variavelcocomo_projetocod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_PROJETOCOD_Includedatalist", StringUtil.BoolToStr( Ddo_variavelcocomo_projetocod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_PROJETOCOD_Sortasc", StringUtil.RTrim( Ddo_variavelcocomo_projetocod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_PROJETOCOD_Sortdsc", StringUtil.RTrim( Ddo_variavelcocomo_projetocod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_PROJETOCOD_Cleanfilter", StringUtil.RTrim( Ddo_variavelcocomo_projetocod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_PROJETOCOD_Rangefilterfrom", StringUtil.RTrim( Ddo_variavelcocomo_projetocod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_PROJETOCOD_Rangefilterto", StringUtil.RTrim( Ddo_variavelcocomo_projetocod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_PROJETOCOD_Searchbuttontext", StringUtil.RTrim( Ddo_variavelcocomo_projetocod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Caption", StringUtil.RTrim( Ddo_variavelcocomo_data_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Tooltip", StringUtil.RTrim( Ddo_variavelcocomo_data_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Cls", StringUtil.RTrim( Ddo_variavelcocomo_data_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Filteredtext_set", StringUtil.RTrim( Ddo_variavelcocomo_data_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Filteredtextto_set", StringUtil.RTrim( Ddo_variavelcocomo_data_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Dropdownoptionstype", StringUtil.RTrim( Ddo_variavelcocomo_data_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_variavelcocomo_data_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Includesortasc", StringUtil.BoolToStr( Ddo_variavelcocomo_data_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Includesortdsc", StringUtil.BoolToStr( Ddo_variavelcocomo_data_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Sortedstatus", StringUtil.RTrim( Ddo_variavelcocomo_data_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Includefilter", StringUtil.BoolToStr( Ddo_variavelcocomo_data_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Filtertype", StringUtil.RTrim( Ddo_variavelcocomo_data_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Filterisrange", StringUtil.BoolToStr( Ddo_variavelcocomo_data_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Includedatalist", StringUtil.BoolToStr( Ddo_variavelcocomo_data_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Sortasc", StringUtil.RTrim( Ddo_variavelcocomo_data_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Sortdsc", StringUtil.RTrim( Ddo_variavelcocomo_data_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Cleanfilter", StringUtil.RTrim( Ddo_variavelcocomo_data_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Rangefilterfrom", StringUtil.RTrim( Ddo_variavelcocomo_data_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Rangefilterto", StringUtil.RTrim( Ddo_variavelcocomo_data_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Searchbuttontext", StringUtil.RTrim( Ddo_variavelcocomo_data_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Caption", StringUtil.RTrim( Ddo_variavelcocomo_extrabaixo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Tooltip", StringUtil.RTrim( Ddo_variavelcocomo_extrabaixo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Cls", StringUtil.RTrim( Ddo_variavelcocomo_extrabaixo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Filteredtext_set", StringUtil.RTrim( Ddo_variavelcocomo_extrabaixo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Filteredtextto_set", StringUtil.RTrim( Ddo_variavelcocomo_extrabaixo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Dropdownoptionstype", StringUtil.RTrim( Ddo_variavelcocomo_extrabaixo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_variavelcocomo_extrabaixo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Includesortasc", StringUtil.BoolToStr( Ddo_variavelcocomo_extrabaixo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Includesortdsc", StringUtil.BoolToStr( Ddo_variavelcocomo_extrabaixo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Sortedstatus", StringUtil.RTrim( Ddo_variavelcocomo_extrabaixo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Includefilter", StringUtil.BoolToStr( Ddo_variavelcocomo_extrabaixo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Filtertype", StringUtil.RTrim( Ddo_variavelcocomo_extrabaixo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Filterisrange", StringUtil.BoolToStr( Ddo_variavelcocomo_extrabaixo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Includedatalist", StringUtil.BoolToStr( Ddo_variavelcocomo_extrabaixo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Sortasc", StringUtil.RTrim( Ddo_variavelcocomo_extrabaixo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Sortdsc", StringUtil.RTrim( Ddo_variavelcocomo_extrabaixo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Cleanfilter", StringUtil.RTrim( Ddo_variavelcocomo_extrabaixo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Rangefilterfrom", StringUtil.RTrim( Ddo_variavelcocomo_extrabaixo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Rangefilterto", StringUtil.RTrim( Ddo_variavelcocomo_extrabaixo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Searchbuttontext", StringUtil.RTrim( Ddo_variavelcocomo_extrabaixo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Caption", StringUtil.RTrim( Ddo_variavelcocomo_muitobaixo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Tooltip", StringUtil.RTrim( Ddo_variavelcocomo_muitobaixo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Cls", StringUtil.RTrim( Ddo_variavelcocomo_muitobaixo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Filteredtext_set", StringUtil.RTrim( Ddo_variavelcocomo_muitobaixo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Filteredtextto_set", StringUtil.RTrim( Ddo_variavelcocomo_muitobaixo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Dropdownoptionstype", StringUtil.RTrim( Ddo_variavelcocomo_muitobaixo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_variavelcocomo_muitobaixo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Includesortasc", StringUtil.BoolToStr( Ddo_variavelcocomo_muitobaixo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Includesortdsc", StringUtil.BoolToStr( Ddo_variavelcocomo_muitobaixo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Sortedstatus", StringUtil.RTrim( Ddo_variavelcocomo_muitobaixo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Includefilter", StringUtil.BoolToStr( Ddo_variavelcocomo_muitobaixo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Filtertype", StringUtil.RTrim( Ddo_variavelcocomo_muitobaixo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Filterisrange", StringUtil.BoolToStr( Ddo_variavelcocomo_muitobaixo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Includedatalist", StringUtil.BoolToStr( Ddo_variavelcocomo_muitobaixo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Sortasc", StringUtil.RTrim( Ddo_variavelcocomo_muitobaixo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Sortdsc", StringUtil.RTrim( Ddo_variavelcocomo_muitobaixo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Cleanfilter", StringUtil.RTrim( Ddo_variavelcocomo_muitobaixo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Rangefilterfrom", StringUtil.RTrim( Ddo_variavelcocomo_muitobaixo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Rangefilterto", StringUtil.RTrim( Ddo_variavelcocomo_muitobaixo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Searchbuttontext", StringUtil.RTrim( Ddo_variavelcocomo_muitobaixo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Caption", StringUtil.RTrim( Ddo_variavelcocomo_baixo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Tooltip", StringUtil.RTrim( Ddo_variavelcocomo_baixo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Cls", StringUtil.RTrim( Ddo_variavelcocomo_baixo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Filteredtext_set", StringUtil.RTrim( Ddo_variavelcocomo_baixo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Filteredtextto_set", StringUtil.RTrim( Ddo_variavelcocomo_baixo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Dropdownoptionstype", StringUtil.RTrim( Ddo_variavelcocomo_baixo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_variavelcocomo_baixo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Includesortasc", StringUtil.BoolToStr( Ddo_variavelcocomo_baixo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Includesortdsc", StringUtil.BoolToStr( Ddo_variavelcocomo_baixo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Sortedstatus", StringUtil.RTrim( Ddo_variavelcocomo_baixo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Includefilter", StringUtil.BoolToStr( Ddo_variavelcocomo_baixo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Filtertype", StringUtil.RTrim( Ddo_variavelcocomo_baixo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Filterisrange", StringUtil.BoolToStr( Ddo_variavelcocomo_baixo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Includedatalist", StringUtil.BoolToStr( Ddo_variavelcocomo_baixo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Sortasc", StringUtil.RTrim( Ddo_variavelcocomo_baixo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Sortdsc", StringUtil.RTrim( Ddo_variavelcocomo_baixo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Cleanfilter", StringUtil.RTrim( Ddo_variavelcocomo_baixo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Rangefilterfrom", StringUtil.RTrim( Ddo_variavelcocomo_baixo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Rangefilterto", StringUtil.RTrim( Ddo_variavelcocomo_baixo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Searchbuttontext", StringUtil.RTrim( Ddo_variavelcocomo_baixo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Caption", StringUtil.RTrim( Ddo_variavelcocomo_nominal_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Tooltip", StringUtil.RTrim( Ddo_variavelcocomo_nominal_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Cls", StringUtil.RTrim( Ddo_variavelcocomo_nominal_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Filteredtext_set", StringUtil.RTrim( Ddo_variavelcocomo_nominal_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Filteredtextto_set", StringUtil.RTrim( Ddo_variavelcocomo_nominal_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Dropdownoptionstype", StringUtil.RTrim( Ddo_variavelcocomo_nominal_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_variavelcocomo_nominal_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Includesortasc", StringUtil.BoolToStr( Ddo_variavelcocomo_nominal_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Includesortdsc", StringUtil.BoolToStr( Ddo_variavelcocomo_nominal_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Sortedstatus", StringUtil.RTrim( Ddo_variavelcocomo_nominal_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Includefilter", StringUtil.BoolToStr( Ddo_variavelcocomo_nominal_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Filtertype", StringUtil.RTrim( Ddo_variavelcocomo_nominal_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Filterisrange", StringUtil.BoolToStr( Ddo_variavelcocomo_nominal_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Includedatalist", StringUtil.BoolToStr( Ddo_variavelcocomo_nominal_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Sortasc", StringUtil.RTrim( Ddo_variavelcocomo_nominal_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Sortdsc", StringUtil.RTrim( Ddo_variavelcocomo_nominal_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Cleanfilter", StringUtil.RTrim( Ddo_variavelcocomo_nominal_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Rangefilterfrom", StringUtil.RTrim( Ddo_variavelcocomo_nominal_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Rangefilterto", StringUtil.RTrim( Ddo_variavelcocomo_nominal_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Searchbuttontext", StringUtil.RTrim( Ddo_variavelcocomo_nominal_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Caption", StringUtil.RTrim( Ddo_variavelcocomo_alto_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Tooltip", StringUtil.RTrim( Ddo_variavelcocomo_alto_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Cls", StringUtil.RTrim( Ddo_variavelcocomo_alto_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Filteredtext_set", StringUtil.RTrim( Ddo_variavelcocomo_alto_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Filteredtextto_set", StringUtil.RTrim( Ddo_variavelcocomo_alto_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Dropdownoptionstype", StringUtil.RTrim( Ddo_variavelcocomo_alto_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_variavelcocomo_alto_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Includesortasc", StringUtil.BoolToStr( Ddo_variavelcocomo_alto_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Includesortdsc", StringUtil.BoolToStr( Ddo_variavelcocomo_alto_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Sortedstatus", StringUtil.RTrim( Ddo_variavelcocomo_alto_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Includefilter", StringUtil.BoolToStr( Ddo_variavelcocomo_alto_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Filtertype", StringUtil.RTrim( Ddo_variavelcocomo_alto_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Filterisrange", StringUtil.BoolToStr( Ddo_variavelcocomo_alto_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Includedatalist", StringUtil.BoolToStr( Ddo_variavelcocomo_alto_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Sortasc", StringUtil.RTrim( Ddo_variavelcocomo_alto_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Sortdsc", StringUtil.RTrim( Ddo_variavelcocomo_alto_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Cleanfilter", StringUtil.RTrim( Ddo_variavelcocomo_alto_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Rangefilterfrom", StringUtil.RTrim( Ddo_variavelcocomo_alto_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Rangefilterto", StringUtil.RTrim( Ddo_variavelcocomo_alto_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Searchbuttontext", StringUtil.RTrim( Ddo_variavelcocomo_alto_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Caption", StringUtil.RTrim( Ddo_variavelcocomo_muitoalto_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Tooltip", StringUtil.RTrim( Ddo_variavelcocomo_muitoalto_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Cls", StringUtil.RTrim( Ddo_variavelcocomo_muitoalto_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Filteredtext_set", StringUtil.RTrim( Ddo_variavelcocomo_muitoalto_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Filteredtextto_set", StringUtil.RTrim( Ddo_variavelcocomo_muitoalto_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Dropdownoptionstype", StringUtil.RTrim( Ddo_variavelcocomo_muitoalto_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_variavelcocomo_muitoalto_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Includesortasc", StringUtil.BoolToStr( Ddo_variavelcocomo_muitoalto_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Includesortdsc", StringUtil.BoolToStr( Ddo_variavelcocomo_muitoalto_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Sortedstatus", StringUtil.RTrim( Ddo_variavelcocomo_muitoalto_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Includefilter", StringUtil.BoolToStr( Ddo_variavelcocomo_muitoalto_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Filtertype", StringUtil.RTrim( Ddo_variavelcocomo_muitoalto_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Filterisrange", StringUtil.BoolToStr( Ddo_variavelcocomo_muitoalto_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Includedatalist", StringUtil.BoolToStr( Ddo_variavelcocomo_muitoalto_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Sortasc", StringUtil.RTrim( Ddo_variavelcocomo_muitoalto_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Sortdsc", StringUtil.RTrim( Ddo_variavelcocomo_muitoalto_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Cleanfilter", StringUtil.RTrim( Ddo_variavelcocomo_muitoalto_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Rangefilterfrom", StringUtil.RTrim( Ddo_variavelcocomo_muitoalto_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Rangefilterto", StringUtil.RTrim( Ddo_variavelcocomo_muitoalto_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Searchbuttontext", StringUtil.RTrim( Ddo_variavelcocomo_muitoalto_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Caption", StringUtil.RTrim( Ddo_variavelcocomo_extraalto_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Tooltip", StringUtil.RTrim( Ddo_variavelcocomo_extraalto_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Cls", StringUtil.RTrim( Ddo_variavelcocomo_extraalto_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Filteredtext_set", StringUtil.RTrim( Ddo_variavelcocomo_extraalto_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Filteredtextto_set", StringUtil.RTrim( Ddo_variavelcocomo_extraalto_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Dropdownoptionstype", StringUtil.RTrim( Ddo_variavelcocomo_extraalto_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_variavelcocomo_extraalto_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Includesortasc", StringUtil.BoolToStr( Ddo_variavelcocomo_extraalto_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Includesortdsc", StringUtil.BoolToStr( Ddo_variavelcocomo_extraalto_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Sortedstatus", StringUtil.RTrim( Ddo_variavelcocomo_extraalto_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Includefilter", StringUtil.BoolToStr( Ddo_variavelcocomo_extraalto_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Filtertype", StringUtil.RTrim( Ddo_variavelcocomo_extraalto_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Filterisrange", StringUtil.BoolToStr( Ddo_variavelcocomo_extraalto_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Includedatalist", StringUtil.BoolToStr( Ddo_variavelcocomo_extraalto_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Sortasc", StringUtil.RTrim( Ddo_variavelcocomo_extraalto_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Sortdsc", StringUtil.RTrim( Ddo_variavelcocomo_extraalto_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Cleanfilter", StringUtil.RTrim( Ddo_variavelcocomo_extraalto_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Rangefilterfrom", StringUtil.RTrim( Ddo_variavelcocomo_extraalto_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Rangefilterto", StringUtil.RTrim( Ddo_variavelcocomo_extraalto_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Searchbuttontext", StringUtil.RTrim( Ddo_variavelcocomo_extraalto_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_USADO_Caption", StringUtil.RTrim( Ddo_variavelcocomo_usado_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_USADO_Tooltip", StringUtil.RTrim( Ddo_variavelcocomo_usado_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_USADO_Cls", StringUtil.RTrim( Ddo_variavelcocomo_usado_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_USADO_Filteredtext_set", StringUtil.RTrim( Ddo_variavelcocomo_usado_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_USADO_Filteredtextto_set", StringUtil.RTrim( Ddo_variavelcocomo_usado_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_USADO_Dropdownoptionstype", StringUtil.RTrim( Ddo_variavelcocomo_usado_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_USADO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_variavelcocomo_usado_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_USADO_Includesortasc", StringUtil.BoolToStr( Ddo_variavelcocomo_usado_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_USADO_Includesortdsc", StringUtil.BoolToStr( Ddo_variavelcocomo_usado_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_USADO_Sortedstatus", StringUtil.RTrim( Ddo_variavelcocomo_usado_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_USADO_Includefilter", StringUtil.BoolToStr( Ddo_variavelcocomo_usado_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_USADO_Filtertype", StringUtil.RTrim( Ddo_variavelcocomo_usado_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_USADO_Filterisrange", StringUtil.BoolToStr( Ddo_variavelcocomo_usado_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_USADO_Includedatalist", StringUtil.BoolToStr( Ddo_variavelcocomo_usado_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_USADO_Sortasc", StringUtil.RTrim( Ddo_variavelcocomo_usado_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_USADO_Sortdsc", StringUtil.RTrim( Ddo_variavelcocomo_usado_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_USADO_Cleanfilter", StringUtil.RTrim( Ddo_variavelcocomo_usado_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_USADO_Rangefilterfrom", StringUtil.RTrim( Ddo_variavelcocomo_usado_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_USADO_Rangefilterto", StringUtil.RTrim( Ddo_variavelcocomo_usado_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_USADO_Searchbuttontext", StringUtil.RTrim( Ddo_variavelcocomo_usado_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Activeeventkey", StringUtil.RTrim( Ddo_variavelcocomo_areatrabalhocod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Filteredtext_get", StringUtil.RTrim( Ddo_variavelcocomo_areatrabalhocod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Filteredtextto_get", StringUtil.RTrim( Ddo_variavelcocomo_areatrabalhocod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Activeeventkey", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Filteredtext_get", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Selectedvalue_get", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_variavelcocomo_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_variavelcocomo_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_variavelcocomo_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Activeeventkey", StringUtil.RTrim( Ddo_variavelcocomo_tipo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Selectedvalue_get", StringUtil.RTrim( Ddo_variavelcocomo_tipo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_PROJETOCOD_Activeeventkey", StringUtil.RTrim( Ddo_variavelcocomo_projetocod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_PROJETOCOD_Filteredtext_get", StringUtil.RTrim( Ddo_variavelcocomo_projetocod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_PROJETOCOD_Filteredtextto_get", StringUtil.RTrim( Ddo_variavelcocomo_projetocod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Activeeventkey", StringUtil.RTrim( Ddo_variavelcocomo_data_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Filteredtext_get", StringUtil.RTrim( Ddo_variavelcocomo_data_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Filteredtextto_get", StringUtil.RTrim( Ddo_variavelcocomo_data_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Activeeventkey", StringUtil.RTrim( Ddo_variavelcocomo_extrabaixo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Filteredtext_get", StringUtil.RTrim( Ddo_variavelcocomo_extrabaixo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Filteredtextto_get", StringUtil.RTrim( Ddo_variavelcocomo_extrabaixo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Activeeventkey", StringUtil.RTrim( Ddo_variavelcocomo_muitobaixo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Filteredtext_get", StringUtil.RTrim( Ddo_variavelcocomo_muitobaixo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Filteredtextto_get", StringUtil.RTrim( Ddo_variavelcocomo_muitobaixo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Activeeventkey", StringUtil.RTrim( Ddo_variavelcocomo_baixo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Filteredtext_get", StringUtil.RTrim( Ddo_variavelcocomo_baixo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Filteredtextto_get", StringUtil.RTrim( Ddo_variavelcocomo_baixo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Activeeventkey", StringUtil.RTrim( Ddo_variavelcocomo_nominal_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Filteredtext_get", StringUtil.RTrim( Ddo_variavelcocomo_nominal_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Filteredtextto_get", StringUtil.RTrim( Ddo_variavelcocomo_nominal_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Activeeventkey", StringUtil.RTrim( Ddo_variavelcocomo_alto_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Filteredtext_get", StringUtil.RTrim( Ddo_variavelcocomo_alto_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Filteredtextto_get", StringUtil.RTrim( Ddo_variavelcocomo_alto_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Activeeventkey", StringUtil.RTrim( Ddo_variavelcocomo_muitoalto_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Filteredtext_get", StringUtil.RTrim( Ddo_variavelcocomo_muitoalto_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Filteredtextto_get", StringUtil.RTrim( Ddo_variavelcocomo_muitoalto_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Activeeventkey", StringUtil.RTrim( Ddo_variavelcocomo_extraalto_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Filteredtext_get", StringUtil.RTrim( Ddo_variavelcocomo_extraalto_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Filteredtextto_get", StringUtil.RTrim( Ddo_variavelcocomo_extraalto_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_USADO_Activeeventkey", StringUtil.RTrim( Ddo_variavelcocomo_usado_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_USADO_Filteredtext_get", StringUtil.RTrim( Ddo_variavelcocomo_usado_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_USADO_Filteredtextto_get", StringUtil.RTrim( Ddo_variavelcocomo_usado_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "PromptVariaveisCocomo";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A992VariavelCocomo_Sequencial), "ZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("promptvariaveiscocomo:[SendSecurityCheck value for]"+"VariavelCocomo_Sequencial:"+context.localUtil.Format( (decimal)(A992VariavelCocomo_Sequencial), "ZZ9"));
      }

      protected void RenderHtmlCloseFormGL2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptVariaveisCocomo" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Variaveis Cocomo" ;
      }

      protected void WBGL0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_GL2( true) ;
         }
         else
         {
            wb_table1_2_GL2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_GL2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtVariavelCocomo_Sequencial_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A992VariavelCocomo_Sequencial), 3, 0, ",", "")), context.localUtil.Format( (decimal)(A992VariavelCocomo_Sequencial), "ZZ9"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtVariavelCocomo_Sequencial_Jsonclick, 0, "Attribute", "", "", "", edtVariavelCocomo_Sequencial_Visible, 0, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Sequencial", "right", false, "HLP_PromptVariaveisCocomo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV20DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(101, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,101);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV24DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(102, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,102);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_areatrabalhocod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35TFVariavelCocomo_AreaTrabalhoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV35TFVariavelCocomo_AreaTrabalhoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,103);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_areatrabalhocod_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_areatrabalhocod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptVariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_areatrabalhocod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36TFVariavelCocomo_AreaTrabalhoCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV36TFVariavelCocomo_AreaTrabalhoCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_areatrabalhocod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_areatrabalhocod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptVariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_sigla_Internalname, StringUtil.RTrim( AV39TFVariavelCocomo_Sigla), StringUtil.RTrim( context.localUtil.Format( AV39TFVariavelCocomo_Sigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,105);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_sigla_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_sigla_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptVariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_sigla_sel_Internalname, StringUtil.RTrim( AV40TFVariavelCocomo_Sigla_Sel), StringUtil.RTrim( context.localUtil.Format( AV40TFVariavelCocomo_Sigla_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,106);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_sigla_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_sigla_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptVariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_nome_Internalname, StringUtil.RTrim( AV43TFVariavelCocomo_Nome), StringUtil.RTrim( context.localUtil.Format( AV43TFVariavelCocomo_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,107);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptVariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_nome_sel_Internalname, StringUtil.RTrim( AV44TFVariavelCocomo_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV44TFVariavelCocomo_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,108);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptVariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_projetocod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV51TFVariavelCocomo_ProjetoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV51TFVariavelCocomo_ProjetoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,109);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_projetocod_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_projetocod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptVariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_projetocod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV52TFVariavelCocomo_ProjetoCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV52TFVariavelCocomo_ProjetoCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,110);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_projetocod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_projetocod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptVariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'" + sGXsfl_80_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfvariavelcocomo_data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_data_Internalname, context.localUtil.Format(AV55TFVariavelCocomo_Data, "99/99/99"), context.localUtil.Format( AV55TFVariavelCocomo_Data, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,111);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_data_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_data_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptVariaveisCocomo.htm");
            GxWebStd.gx_bitmap( context, edtavTfvariavelcocomo_data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfvariavelcocomo_data_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptVariaveisCocomo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_80_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfvariavelcocomo_data_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_data_to_Internalname, context.localUtil.Format(AV56TFVariavelCocomo_Data_To, "99/99/99"), context.localUtil.Format( AV56TFVariavelCocomo_Data_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,112);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_data_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_data_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptVariaveisCocomo.htm");
            GxWebStd.gx_bitmap( context, edtavTfvariavelcocomo_data_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfvariavelcocomo_data_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptVariaveisCocomo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_variavelcocomo_dataauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_80_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_variavelcocomo_dataauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_variavelcocomo_dataauxdate_Internalname, context.localUtil.Format(AV57DDO_VariavelCocomo_DataAuxDate, "99/99/99"), context.localUtil.Format( AV57DDO_VariavelCocomo_DataAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,114);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_variavelcocomo_dataauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptVariaveisCocomo.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_variavelcocomo_dataauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptVariaveisCocomo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_80_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_variavelcocomo_dataauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_variavelcocomo_dataauxdateto_Internalname, context.localUtil.Format(AV58DDO_VariavelCocomo_DataAuxDateTo, "99/99/99"), context.localUtil.Format( AV58DDO_VariavelCocomo_DataAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,115);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_variavelcocomo_dataauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptVariaveisCocomo.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_variavelcocomo_dataauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptVariaveisCocomo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_extrabaixo_Internalname, StringUtil.LTrim( StringUtil.NToC( AV61TFVariavelCocomo_ExtraBaixo, 18, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV61TFVariavelCocomo_ExtraBaixo, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,116);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_extrabaixo_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_extrabaixo_Visible, 1, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptVariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_extrabaixo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV62TFVariavelCocomo_ExtraBaixo_To, 18, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV62TFVariavelCocomo_ExtraBaixo_To, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,117);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_extrabaixo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_extrabaixo_to_Visible, 1, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptVariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_muitobaixo_Internalname, StringUtil.LTrim( StringUtil.NToC( AV65TFVariavelCocomo_MuitoBaixo, 18, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV65TFVariavelCocomo_MuitoBaixo, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,118);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_muitobaixo_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_muitobaixo_Visible, 1, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptVariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_muitobaixo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV66TFVariavelCocomo_MuitoBaixo_To, 18, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV66TFVariavelCocomo_MuitoBaixo_To, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,119);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_muitobaixo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_muitobaixo_to_Visible, 1, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptVariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 120,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_baixo_Internalname, StringUtil.LTrim( StringUtil.NToC( AV69TFVariavelCocomo_Baixo, 18, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV69TFVariavelCocomo_Baixo, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,120);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_baixo_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_baixo_Visible, 1, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptVariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_baixo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV70TFVariavelCocomo_Baixo_To, 18, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV70TFVariavelCocomo_Baixo_To, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,121);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_baixo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_baixo_to_Visible, 1, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptVariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 122,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_nominal_Internalname, StringUtil.LTrim( StringUtil.NToC( AV73TFVariavelCocomo_Nominal, 18, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV73TFVariavelCocomo_Nominal, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,122);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_nominal_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_nominal_Visible, 1, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptVariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 123,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_nominal_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV74TFVariavelCocomo_Nominal_To, 18, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV74TFVariavelCocomo_Nominal_To, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,123);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_nominal_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_nominal_to_Visible, 1, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptVariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 124,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_alto_Internalname, StringUtil.LTrim( StringUtil.NToC( AV77TFVariavelCocomo_Alto, 18, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV77TFVariavelCocomo_Alto, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,124);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_alto_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_alto_Visible, 1, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptVariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 125,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_alto_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV78TFVariavelCocomo_Alto_To, 18, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV78TFVariavelCocomo_Alto_To, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,125);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_alto_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_alto_to_Visible, 1, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptVariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 126,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_muitoalto_Internalname, StringUtil.LTrim( StringUtil.NToC( AV81TFVariavelCocomo_MuitoAlto, 18, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV81TFVariavelCocomo_MuitoAlto, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,126);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_muitoalto_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_muitoalto_Visible, 1, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptVariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 127,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_muitoalto_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV82TFVariavelCocomo_MuitoAlto_To, 18, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV82TFVariavelCocomo_MuitoAlto_To, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,127);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_muitoalto_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_muitoalto_to_Visible, 1, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptVariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 128,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_extraalto_Internalname, StringUtil.LTrim( StringUtil.NToC( AV85TFVariavelCocomo_ExtraAlto, 18, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV85TFVariavelCocomo_ExtraAlto, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,128);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_extraalto_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_extraalto_Visible, 1, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptVariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 129,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_extraalto_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV86TFVariavelCocomo_ExtraAlto_To, 18, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV86TFVariavelCocomo_ExtraAlto_To, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,129);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_extraalto_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_extraalto_to_Visible, 1, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptVariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 130,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_usado_Internalname, StringUtil.LTrim( StringUtil.NToC( AV89TFVariavelCocomo_Usado, 18, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV89TFVariavelCocomo_Usado, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,130);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_usado_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_usado_Visible, 1, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptVariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 131,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_usado_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV90TFVariavelCocomo_Usado_To, 18, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV90TFVariavelCocomo_Usado_To, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,131);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_usado_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_usado_to_Visible, 1, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptVariaveisCocomo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_VARIAVELCOCOMO_AREATRABALHOCODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 133,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_variavelcocomo_areatrabalhocodtitlecontrolidtoreplace_Internalname, AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,133);\"", 0, edtavDdo_variavelcocomo_areatrabalhocodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptVariaveisCocomo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_VARIAVELCOCOMO_SIGLAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 135,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_variavelcocomo_siglatitlecontrolidtoreplace_Internalname, AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,135);\"", 0, edtavDdo_variavelcocomo_siglatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptVariaveisCocomo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_VARIAVELCOCOMO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 137,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_variavelcocomo_nometitlecontrolidtoreplace_Internalname, AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,137);\"", 0, edtavDdo_variavelcocomo_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptVariaveisCocomo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_VARIAVELCOCOMO_TIPOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 139,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_variavelcocomo_tipotitlecontrolidtoreplace_Internalname, AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,139);\"", 0, edtavDdo_variavelcocomo_tipotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptVariaveisCocomo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_VARIAVELCOCOMO_PROJETOCODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 141,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_variavelcocomo_projetocodtitlecontrolidtoreplace_Internalname, AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,141);\"", 0, edtavDdo_variavelcocomo_projetocodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptVariaveisCocomo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_VARIAVELCOCOMO_DATAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 143,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_variavelcocomo_datatitlecontrolidtoreplace_Internalname, AV59ddo_VariavelCocomo_DataTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,143);\"", 0, edtavDdo_variavelcocomo_datatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptVariaveisCocomo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_VARIAVELCOCOMO_EXTRABAIXOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 145,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_variavelcocomo_extrabaixotitlecontrolidtoreplace_Internalname, AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,145);\"", 0, edtavDdo_variavelcocomo_extrabaixotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptVariaveisCocomo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_VARIAVELCOCOMO_MUITOBAIXOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 147,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_variavelcocomo_muitobaixotitlecontrolidtoreplace_Internalname, AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,147);\"", 0, edtavDdo_variavelcocomo_muitobaixotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptVariaveisCocomo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_VARIAVELCOCOMO_BAIXOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 149,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_variavelcocomo_baixotitlecontrolidtoreplace_Internalname, AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,149);\"", 0, edtavDdo_variavelcocomo_baixotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptVariaveisCocomo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_VARIAVELCOCOMO_NOMINALContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 151,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_variavelcocomo_nominaltitlecontrolidtoreplace_Internalname, AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,151);\"", 0, edtavDdo_variavelcocomo_nominaltitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptVariaveisCocomo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_VARIAVELCOCOMO_ALTOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 153,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_variavelcocomo_altotitlecontrolidtoreplace_Internalname, AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,153);\"", 0, edtavDdo_variavelcocomo_altotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptVariaveisCocomo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_VARIAVELCOCOMO_MUITOALTOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 155,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_variavelcocomo_muitoaltotitlecontrolidtoreplace_Internalname, AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,155);\"", 0, edtavDdo_variavelcocomo_muitoaltotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptVariaveisCocomo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_VARIAVELCOCOMO_EXTRAALTOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 157,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_variavelcocomo_extraaltotitlecontrolidtoreplace_Internalname, AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,157);\"", 0, edtavDdo_variavelcocomo_extraaltotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptVariaveisCocomo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_VARIAVELCOCOMO_USADOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 159,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_variavelcocomo_usadotitlecontrolidtoreplace_Internalname, AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,159);\"", 0, edtavDdo_variavelcocomo_usadotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptVariaveisCocomo.htm");
         }
         wbLoad = true;
      }

      protected void STARTGL2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Variaveis Cocomo", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPGL0( ) ;
      }

      protected void WSGL2( )
      {
         STARTGL2( ) ;
         EVTGL2( ) ;
      }

      protected void EVTGL2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11GL2 */
                           E11GL2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_VARIAVELCOCOMO_AREATRABALHOCOD.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12GL2 */
                           E12GL2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_VARIAVELCOCOMO_SIGLA.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13GL2 */
                           E13GL2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_VARIAVELCOCOMO_NOME.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14GL2 */
                           E14GL2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_VARIAVELCOCOMO_TIPO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15GL2 */
                           E15GL2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_VARIAVELCOCOMO_PROJETOCOD.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16GL2 */
                           E16GL2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_VARIAVELCOCOMO_DATA.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17GL2 */
                           E17GL2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_VARIAVELCOCOMO_EXTRABAIXO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18GL2 */
                           E18GL2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_VARIAVELCOCOMO_MUITOBAIXO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19GL2 */
                           E19GL2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_VARIAVELCOCOMO_BAIXO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20GL2 */
                           E20GL2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_VARIAVELCOCOMO_NOMINAL.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E21GL2 */
                           E21GL2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_VARIAVELCOCOMO_ALTO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E22GL2 */
                           E22GL2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_VARIAVELCOCOMO_MUITOALTO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E23GL2 */
                           E23GL2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_VARIAVELCOCOMO_EXTRAALTO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E24GL2 */
                           E24GL2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_VARIAVELCOCOMO_USADO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E25GL2 */
                           E25GL2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E26GL2 */
                           E26GL2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E27GL2 */
                           E27GL2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E28GL2 */
                           E28GL2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E29GL2 */
                           E29GL2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E30GL2 */
                           E30GL2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E31GL2 */
                           E31GL2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E32GL2 */
                           E32GL2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E33GL2 */
                           E33GL2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E34GL2 */
                           E34GL2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E35GL2 */
                           E35GL2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_80_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
                           SubsflControlProps_802( ) ;
                           AV30Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV30Select)) ? AV98Select_GXI : context.convertURL( context.PathToRelativeUrl( AV30Select))));
                           A961VariavelCocomo_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtVariavelCocomo_AreaTrabalhoCod_Internalname), ",", "."));
                           A962VariavelCocomo_Sigla = StringUtil.Upper( cgiGet( edtVariavelCocomo_Sigla_Internalname));
                           A963VariavelCocomo_Nome = StringUtil.Upper( cgiGet( edtVariavelCocomo_Nome_Internalname));
                           cmbVariavelCocomo_Tipo.Name = cmbVariavelCocomo_Tipo_Internalname;
                           cmbVariavelCocomo_Tipo.CurrentValue = cgiGet( cmbVariavelCocomo_Tipo_Internalname);
                           A964VariavelCocomo_Tipo = cgiGet( cmbVariavelCocomo_Tipo_Internalname);
                           A965VariavelCocomo_ProjetoCod = (int)(context.localUtil.CToN( cgiGet( edtVariavelCocomo_ProjetoCod_Internalname), ",", "."));
                           n965VariavelCocomo_ProjetoCod = false;
                           A966VariavelCocomo_Data = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtVariavelCocomo_Data_Internalname), 0));
                           n966VariavelCocomo_Data = false;
                           A986VariavelCocomo_ExtraBaixo = context.localUtil.CToN( cgiGet( edtVariavelCocomo_ExtraBaixo_Internalname), ",", ".");
                           n986VariavelCocomo_ExtraBaixo = false;
                           A967VariavelCocomo_MuitoBaixo = context.localUtil.CToN( cgiGet( edtVariavelCocomo_MuitoBaixo_Internalname), ",", ".");
                           n967VariavelCocomo_MuitoBaixo = false;
                           A968VariavelCocomo_Baixo = context.localUtil.CToN( cgiGet( edtVariavelCocomo_Baixo_Internalname), ",", ".");
                           n968VariavelCocomo_Baixo = false;
                           A969VariavelCocomo_Nominal = context.localUtil.CToN( cgiGet( edtVariavelCocomo_Nominal_Internalname), ",", ".");
                           n969VariavelCocomo_Nominal = false;
                           A970VariavelCocomo_Alto = context.localUtil.CToN( cgiGet( edtVariavelCocomo_Alto_Internalname), ",", ".");
                           n970VariavelCocomo_Alto = false;
                           A971VariavelCocomo_MuitoAlto = context.localUtil.CToN( cgiGet( edtVariavelCocomo_MuitoAlto_Internalname), ",", ".");
                           n971VariavelCocomo_MuitoAlto = false;
                           A972VariavelCocomo_ExtraAlto = context.localUtil.CToN( cgiGet( edtVariavelCocomo_ExtraAlto_Internalname), ",", ".");
                           n972VariavelCocomo_ExtraAlto = false;
                           A973VariavelCocomo_Usado = context.localUtil.CToN( cgiGet( edtVariavelCocomo_Usado_Internalname), ",", ".");
                           n973VariavelCocomo_Usado = false;
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E36GL2 */
                                 E36GL2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E37GL2 */
                                 E37GL2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E38GL2 */
                                 E38GL2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV15OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV16OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV17DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV18DynamicFiltersOperator1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Variavelcocomo_nome1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vVARIAVELCOCOMO_NOME1"), AV19VariavelCocomo_Nome1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV22DynamicFiltersOperator2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Variavelcocomo_nome2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vVARIAVELCOCOMO_NOME2"), AV23VariavelCocomo_Nome2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV25DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV26DynamicFiltersOperator3 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Variavelcocomo_nome3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vVARIAVELCOCOMO_NOME3"), AV27VariavelCocomo_Nome3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV24DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfvariavelcocomo_areatrabalhocod Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_AREATRABALHOCOD"), ",", ".") != Convert.ToDecimal( AV35TFVariavelCocomo_AreaTrabalhoCod )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfvariavelcocomo_areatrabalhocod_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_AREATRABALHOCOD_TO"), ",", ".") != Convert.ToDecimal( AV36TFVariavelCocomo_AreaTrabalhoCod_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfvariavelcocomo_sigla Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFVARIAVELCOCOMO_SIGLA"), AV39TFVariavelCocomo_Sigla) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfvariavelcocomo_sigla_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFVARIAVELCOCOMO_SIGLA_SEL"), AV40TFVariavelCocomo_Sigla_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfvariavelcocomo_nome Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFVARIAVELCOCOMO_NOME"), AV43TFVariavelCocomo_Nome) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfvariavelcocomo_nome_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFVARIAVELCOCOMO_NOME_SEL"), AV44TFVariavelCocomo_Nome_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfvariavelcocomo_projetocod Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_PROJETOCOD"), ",", ".") != Convert.ToDecimal( AV51TFVariavelCocomo_ProjetoCod )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfvariavelcocomo_projetocod_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_PROJETOCOD_TO"), ",", ".") != Convert.ToDecimal( AV52TFVariavelCocomo_ProjetoCod_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfvariavelcocomo_data Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFVARIAVELCOCOMO_DATA"), 0) != AV55TFVariavelCocomo_Data )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfvariavelcocomo_data_to Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFVARIAVELCOCOMO_DATA_TO"), 0) != AV56TFVariavelCocomo_Data_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfvariavelcocomo_extrabaixo Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_EXTRABAIXO"), ",", ".") != AV61TFVariavelCocomo_ExtraBaixo )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfvariavelcocomo_extrabaixo_to Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_EXTRABAIXO_TO"), ",", ".") != AV62TFVariavelCocomo_ExtraBaixo_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfvariavelcocomo_muitobaixo Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_MUITOBAIXO"), ",", ".") != AV65TFVariavelCocomo_MuitoBaixo )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfvariavelcocomo_muitobaixo_to Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_MUITOBAIXO_TO"), ",", ".") != AV66TFVariavelCocomo_MuitoBaixo_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfvariavelcocomo_baixo Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_BAIXO"), ",", ".") != AV69TFVariavelCocomo_Baixo )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfvariavelcocomo_baixo_to Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_BAIXO_TO"), ",", ".") != AV70TFVariavelCocomo_Baixo_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfvariavelcocomo_nominal Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_NOMINAL"), ",", ".") != AV73TFVariavelCocomo_Nominal )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfvariavelcocomo_nominal_to Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_NOMINAL_TO"), ",", ".") != AV74TFVariavelCocomo_Nominal_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfvariavelcocomo_alto Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_ALTO"), ",", ".") != AV77TFVariavelCocomo_Alto )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfvariavelcocomo_alto_to Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_ALTO_TO"), ",", ".") != AV78TFVariavelCocomo_Alto_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfvariavelcocomo_muitoalto Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_MUITOALTO"), ",", ".") != AV81TFVariavelCocomo_MuitoAlto )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfvariavelcocomo_muitoalto_to Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_MUITOALTO_TO"), ",", ".") != AV82TFVariavelCocomo_MuitoAlto_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfvariavelcocomo_extraalto Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_EXTRAALTO"), ",", ".") != AV85TFVariavelCocomo_ExtraAlto )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfvariavelcocomo_extraalto_to Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_EXTRAALTO_TO"), ",", ".") != AV86TFVariavelCocomo_ExtraAlto_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfvariavelcocomo_usado Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_USADO"), ",", ".") != AV89TFVariavelCocomo_Usado )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfvariavelcocomo_usado_to Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_USADO_TO"), ",", ".") != AV90TFVariavelCocomo_Usado_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E39GL2 */
                                       E39GL2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEGL2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormGL2( ) ;
            }
         }
      }

      protected void PAGL2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV15OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("VARIAVELCOCOMO_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV17DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV17DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersSelector1", AV17DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV18DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV18DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("VARIAVELCOCOMO_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("VARIAVELCOCOMO_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV25DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV25DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
            }
            GXCCtl = "VARIAVELCOCOMO_TIPO_" + sGXsfl_80_idx;
            cmbVariavelCocomo_Tipo.Name = GXCCtl;
            cmbVariavelCocomo_Tipo.WebTags = "";
            cmbVariavelCocomo_Tipo.addItem("", "(Selecionar)", 0);
            cmbVariavelCocomo_Tipo.addItem("E", "Escala", 0);
            cmbVariavelCocomo_Tipo.addItem("M", "Multiplicador", 0);
            if ( cmbVariavelCocomo_Tipo.ItemCount > 0 )
            {
               A964VariavelCocomo_Tipo = cmbVariavelCocomo_Tipo.getValidValue(A964VariavelCocomo_Tipo);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_802( ) ;
         while ( nGXsfl_80_idx <= nRC_GXsfl_80 )
         {
            sendrow_802( ) ;
            nGXsfl_80_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_80_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_80_idx+1));
            sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
            SubsflControlProps_802( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV15OrderedBy ,
                                       bool AV16OrderedDsc ,
                                       String AV17DynamicFiltersSelector1 ,
                                       short AV18DynamicFiltersOperator1 ,
                                       String AV19VariavelCocomo_Nome1 ,
                                       String AV21DynamicFiltersSelector2 ,
                                       short AV22DynamicFiltersOperator2 ,
                                       String AV23VariavelCocomo_Nome2 ,
                                       String AV25DynamicFiltersSelector3 ,
                                       short AV26DynamicFiltersOperator3 ,
                                       String AV27VariavelCocomo_Nome3 ,
                                       bool AV20DynamicFiltersEnabled2 ,
                                       bool AV24DynamicFiltersEnabled3 ,
                                       int AV35TFVariavelCocomo_AreaTrabalhoCod ,
                                       int AV36TFVariavelCocomo_AreaTrabalhoCod_To ,
                                       String AV39TFVariavelCocomo_Sigla ,
                                       String AV40TFVariavelCocomo_Sigla_Sel ,
                                       String AV43TFVariavelCocomo_Nome ,
                                       String AV44TFVariavelCocomo_Nome_Sel ,
                                       int AV51TFVariavelCocomo_ProjetoCod ,
                                       int AV52TFVariavelCocomo_ProjetoCod_To ,
                                       DateTime AV55TFVariavelCocomo_Data ,
                                       DateTime AV56TFVariavelCocomo_Data_To ,
                                       decimal AV61TFVariavelCocomo_ExtraBaixo ,
                                       decimal AV62TFVariavelCocomo_ExtraBaixo_To ,
                                       decimal AV65TFVariavelCocomo_MuitoBaixo ,
                                       decimal AV66TFVariavelCocomo_MuitoBaixo_To ,
                                       decimal AV69TFVariavelCocomo_Baixo ,
                                       decimal AV70TFVariavelCocomo_Baixo_To ,
                                       decimal AV73TFVariavelCocomo_Nominal ,
                                       decimal AV74TFVariavelCocomo_Nominal_To ,
                                       decimal AV77TFVariavelCocomo_Alto ,
                                       decimal AV78TFVariavelCocomo_Alto_To ,
                                       decimal AV81TFVariavelCocomo_MuitoAlto ,
                                       decimal AV82TFVariavelCocomo_MuitoAlto_To ,
                                       decimal AV85TFVariavelCocomo_ExtraAlto ,
                                       decimal AV86TFVariavelCocomo_ExtraAlto_To ,
                                       decimal AV89TFVariavelCocomo_Usado ,
                                       decimal AV90TFVariavelCocomo_Usado_To ,
                                       String AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace ,
                                       String AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace ,
                                       String AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace ,
                                       String AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace ,
                                       String AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace ,
                                       String AV59ddo_VariavelCocomo_DataTitleControlIdToReplace ,
                                       String AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace ,
                                       String AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace ,
                                       String AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace ,
                                       String AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace ,
                                       String AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace ,
                                       String AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace ,
                                       String AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace ,
                                       String AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace ,
                                       IGxCollection AV48TFVariavelCocomo_Tipo_Sels ,
                                       String AV99Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV12GridState ,
                                       bool AV29DynamicFiltersIgnoreFirst ,
                                       bool AV28DynamicFiltersRemoving )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFGL2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_AREATRABALHOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A961VariavelCocomo_AreaTrabalhoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "VARIAVELCOCOMO_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A961VariavelCocomo_AreaTrabalhoCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A962VariavelCocomo_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, "VARIAVELCOCOMO_SIGLA", StringUtil.RTrim( A962VariavelCocomo_Sigla));
         GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A963VariavelCocomo_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "VARIAVELCOCOMO_NOME", StringUtil.RTrim( A963VariavelCocomo_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_TIPO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A964VariavelCocomo_Tipo, ""))));
         GxWebStd.gx_hidden_field( context, "VARIAVELCOCOMO_TIPO", StringUtil.RTrim( A964VariavelCocomo_Tipo));
         GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_PROJETOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A965VariavelCocomo_ProjetoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "VARIAVELCOCOMO_PROJETOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A965VariavelCocomo_ProjetoCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_DATA", GetSecureSignedToken( "", A966VariavelCocomo_Data));
         GxWebStd.gx_hidden_field( context, "VARIAVELCOCOMO_DATA", context.localUtil.Format(A966VariavelCocomo_Data, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_EXTRABAIXO", GetSecureSignedToken( "", context.localUtil.Format( A986VariavelCocomo_ExtraBaixo, "ZZ,ZZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "VARIAVELCOCOMO_EXTRABAIXO", StringUtil.LTrim( StringUtil.NToC( A986VariavelCocomo_ExtraBaixo, 12, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_MUITOBAIXO", GetSecureSignedToken( "", context.localUtil.Format( A967VariavelCocomo_MuitoBaixo, "ZZ,ZZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "VARIAVELCOCOMO_MUITOBAIXO", StringUtil.LTrim( StringUtil.NToC( A967VariavelCocomo_MuitoBaixo, 12, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_BAIXO", GetSecureSignedToken( "", context.localUtil.Format( A968VariavelCocomo_Baixo, "ZZ,ZZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "VARIAVELCOCOMO_BAIXO", StringUtil.LTrim( StringUtil.NToC( A968VariavelCocomo_Baixo, 12, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_NOMINAL", GetSecureSignedToken( "", context.localUtil.Format( A969VariavelCocomo_Nominal, "ZZ,ZZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "VARIAVELCOCOMO_NOMINAL", StringUtil.LTrim( StringUtil.NToC( A969VariavelCocomo_Nominal, 12, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_ALTO", GetSecureSignedToken( "", context.localUtil.Format( A970VariavelCocomo_Alto, "ZZ,ZZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "VARIAVELCOCOMO_ALTO", StringUtil.LTrim( StringUtil.NToC( A970VariavelCocomo_Alto, 12, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_MUITOALTO", GetSecureSignedToken( "", context.localUtil.Format( A971VariavelCocomo_MuitoAlto, "ZZ,ZZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "VARIAVELCOCOMO_MUITOALTO", StringUtil.LTrim( StringUtil.NToC( A971VariavelCocomo_MuitoAlto, 12, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_EXTRAALTO", GetSecureSignedToken( "", context.localUtil.Format( A972VariavelCocomo_ExtraAlto, "ZZ,ZZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "VARIAVELCOCOMO_EXTRAALTO", StringUtil.LTrim( StringUtil.NToC( A972VariavelCocomo_ExtraAlto, 12, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_USADO", GetSecureSignedToken( "", context.localUtil.Format( A973VariavelCocomo_Usado, "ZZ,ZZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "VARIAVELCOCOMO_USADO", StringUtil.LTrim( StringUtil.NToC( A973VariavelCocomo_Usado, 12, 2, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV15OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV17DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV17DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersSelector1", AV17DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV18DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV18DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV25DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV25DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFGL2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV99Pgmname = "PromptVariaveisCocomo";
         context.Gx_err = 0;
      }

      protected void RFGL2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 80;
         /* Execute user event: E37GL2 */
         E37GL2 ();
         nGXsfl_80_idx = 1;
         sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
         SubsflControlProps_802( ) ;
         nGXsfl_80_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_802( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A964VariavelCocomo_Tipo ,
                                                 AV48TFVariavelCocomo_Tipo_Sels ,
                                                 AV17DynamicFiltersSelector1 ,
                                                 AV18DynamicFiltersOperator1 ,
                                                 AV19VariavelCocomo_Nome1 ,
                                                 AV20DynamicFiltersEnabled2 ,
                                                 AV21DynamicFiltersSelector2 ,
                                                 AV22DynamicFiltersOperator2 ,
                                                 AV23VariavelCocomo_Nome2 ,
                                                 AV24DynamicFiltersEnabled3 ,
                                                 AV25DynamicFiltersSelector3 ,
                                                 AV26DynamicFiltersOperator3 ,
                                                 AV27VariavelCocomo_Nome3 ,
                                                 AV35TFVariavelCocomo_AreaTrabalhoCod ,
                                                 AV36TFVariavelCocomo_AreaTrabalhoCod_To ,
                                                 AV40TFVariavelCocomo_Sigla_Sel ,
                                                 AV39TFVariavelCocomo_Sigla ,
                                                 AV44TFVariavelCocomo_Nome_Sel ,
                                                 AV43TFVariavelCocomo_Nome ,
                                                 AV48TFVariavelCocomo_Tipo_Sels.Count ,
                                                 AV51TFVariavelCocomo_ProjetoCod ,
                                                 AV52TFVariavelCocomo_ProjetoCod_To ,
                                                 AV55TFVariavelCocomo_Data ,
                                                 AV56TFVariavelCocomo_Data_To ,
                                                 AV61TFVariavelCocomo_ExtraBaixo ,
                                                 AV62TFVariavelCocomo_ExtraBaixo_To ,
                                                 AV65TFVariavelCocomo_MuitoBaixo ,
                                                 AV66TFVariavelCocomo_MuitoBaixo_To ,
                                                 AV69TFVariavelCocomo_Baixo ,
                                                 AV70TFVariavelCocomo_Baixo_To ,
                                                 AV73TFVariavelCocomo_Nominal ,
                                                 AV74TFVariavelCocomo_Nominal_To ,
                                                 AV77TFVariavelCocomo_Alto ,
                                                 AV78TFVariavelCocomo_Alto_To ,
                                                 AV81TFVariavelCocomo_MuitoAlto ,
                                                 AV82TFVariavelCocomo_MuitoAlto_To ,
                                                 AV85TFVariavelCocomo_ExtraAlto ,
                                                 AV86TFVariavelCocomo_ExtraAlto_To ,
                                                 AV89TFVariavelCocomo_Usado ,
                                                 AV90TFVariavelCocomo_Usado_To ,
                                                 A963VariavelCocomo_Nome ,
                                                 A961VariavelCocomo_AreaTrabalhoCod ,
                                                 A962VariavelCocomo_Sigla ,
                                                 A965VariavelCocomo_ProjetoCod ,
                                                 A966VariavelCocomo_Data ,
                                                 A986VariavelCocomo_ExtraBaixo ,
                                                 A967VariavelCocomo_MuitoBaixo ,
                                                 A968VariavelCocomo_Baixo ,
                                                 A969VariavelCocomo_Nominal ,
                                                 A970VariavelCocomo_Alto ,
                                                 A971VariavelCocomo_MuitoAlto ,
                                                 A972VariavelCocomo_ExtraAlto ,
                                                 A973VariavelCocomo_Usado ,
                                                 AV15OrderedBy ,
                                                 AV16OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                                 TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                                 TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                                 TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING,
                                                 TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN,
                                                 TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN,
                                                 TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV19VariavelCocomo_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV19VariavelCocomo_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19VariavelCocomo_Nome1", AV19VariavelCocomo_Nome1);
            lV19VariavelCocomo_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV19VariavelCocomo_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19VariavelCocomo_Nome1", AV19VariavelCocomo_Nome1);
            lV23VariavelCocomo_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV23VariavelCocomo_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23VariavelCocomo_Nome2", AV23VariavelCocomo_Nome2);
            lV23VariavelCocomo_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV23VariavelCocomo_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23VariavelCocomo_Nome2", AV23VariavelCocomo_Nome2);
            lV27VariavelCocomo_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV27VariavelCocomo_Nome3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27VariavelCocomo_Nome3", AV27VariavelCocomo_Nome3);
            lV27VariavelCocomo_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV27VariavelCocomo_Nome3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27VariavelCocomo_Nome3", AV27VariavelCocomo_Nome3);
            lV39TFVariavelCocomo_Sigla = StringUtil.PadR( StringUtil.RTrim( AV39TFVariavelCocomo_Sigla), 15, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFVariavelCocomo_Sigla", AV39TFVariavelCocomo_Sigla);
            lV43TFVariavelCocomo_Nome = StringUtil.PadR( StringUtil.RTrim( AV43TFVariavelCocomo_Nome), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFVariavelCocomo_Nome", AV43TFVariavelCocomo_Nome);
            /* Using cursor H00GL2 */
            pr_default.execute(0, new Object[] {lV19VariavelCocomo_Nome1, lV19VariavelCocomo_Nome1, lV23VariavelCocomo_Nome2, lV23VariavelCocomo_Nome2, lV27VariavelCocomo_Nome3, lV27VariavelCocomo_Nome3, AV35TFVariavelCocomo_AreaTrabalhoCod, AV36TFVariavelCocomo_AreaTrabalhoCod_To, lV39TFVariavelCocomo_Sigla, AV40TFVariavelCocomo_Sigla_Sel, lV43TFVariavelCocomo_Nome, AV44TFVariavelCocomo_Nome_Sel, AV51TFVariavelCocomo_ProjetoCod, AV52TFVariavelCocomo_ProjetoCod_To, AV55TFVariavelCocomo_Data, AV56TFVariavelCocomo_Data_To, AV61TFVariavelCocomo_ExtraBaixo, AV62TFVariavelCocomo_ExtraBaixo_To, AV65TFVariavelCocomo_MuitoBaixo, AV66TFVariavelCocomo_MuitoBaixo_To, AV69TFVariavelCocomo_Baixo, AV70TFVariavelCocomo_Baixo_To, AV73TFVariavelCocomo_Nominal, AV74TFVariavelCocomo_Nominal_To, AV77TFVariavelCocomo_Alto, AV78TFVariavelCocomo_Alto_To, AV81TFVariavelCocomo_MuitoAlto, AV82TFVariavelCocomo_MuitoAlto_To, AV85TFVariavelCocomo_ExtraAlto, AV86TFVariavelCocomo_ExtraAlto_To, AV89TFVariavelCocomo_Usado, AV90TFVariavelCocomo_Usado_To, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_80_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A992VariavelCocomo_Sequencial = H00GL2_A992VariavelCocomo_Sequencial[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A992VariavelCocomo_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A992VariavelCocomo_Sequencial), 3, 0)));
               A973VariavelCocomo_Usado = H00GL2_A973VariavelCocomo_Usado[0];
               n973VariavelCocomo_Usado = H00GL2_n973VariavelCocomo_Usado[0];
               A972VariavelCocomo_ExtraAlto = H00GL2_A972VariavelCocomo_ExtraAlto[0];
               n972VariavelCocomo_ExtraAlto = H00GL2_n972VariavelCocomo_ExtraAlto[0];
               A971VariavelCocomo_MuitoAlto = H00GL2_A971VariavelCocomo_MuitoAlto[0];
               n971VariavelCocomo_MuitoAlto = H00GL2_n971VariavelCocomo_MuitoAlto[0];
               A970VariavelCocomo_Alto = H00GL2_A970VariavelCocomo_Alto[0];
               n970VariavelCocomo_Alto = H00GL2_n970VariavelCocomo_Alto[0];
               A969VariavelCocomo_Nominal = H00GL2_A969VariavelCocomo_Nominal[0];
               n969VariavelCocomo_Nominal = H00GL2_n969VariavelCocomo_Nominal[0];
               A968VariavelCocomo_Baixo = H00GL2_A968VariavelCocomo_Baixo[0];
               n968VariavelCocomo_Baixo = H00GL2_n968VariavelCocomo_Baixo[0];
               A967VariavelCocomo_MuitoBaixo = H00GL2_A967VariavelCocomo_MuitoBaixo[0];
               n967VariavelCocomo_MuitoBaixo = H00GL2_n967VariavelCocomo_MuitoBaixo[0];
               A986VariavelCocomo_ExtraBaixo = H00GL2_A986VariavelCocomo_ExtraBaixo[0];
               n986VariavelCocomo_ExtraBaixo = H00GL2_n986VariavelCocomo_ExtraBaixo[0];
               A966VariavelCocomo_Data = H00GL2_A966VariavelCocomo_Data[0];
               n966VariavelCocomo_Data = H00GL2_n966VariavelCocomo_Data[0];
               A965VariavelCocomo_ProjetoCod = H00GL2_A965VariavelCocomo_ProjetoCod[0];
               n965VariavelCocomo_ProjetoCod = H00GL2_n965VariavelCocomo_ProjetoCod[0];
               A964VariavelCocomo_Tipo = H00GL2_A964VariavelCocomo_Tipo[0];
               A963VariavelCocomo_Nome = H00GL2_A963VariavelCocomo_Nome[0];
               A962VariavelCocomo_Sigla = H00GL2_A962VariavelCocomo_Sigla[0];
               A961VariavelCocomo_AreaTrabalhoCod = H00GL2_A961VariavelCocomo_AreaTrabalhoCod[0];
               /* Execute user event: E38GL2 */
               E38GL2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 80;
            WBGL0( ) ;
         }
         nGXsfl_80_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A964VariavelCocomo_Tipo ,
                                              AV48TFVariavelCocomo_Tipo_Sels ,
                                              AV17DynamicFiltersSelector1 ,
                                              AV18DynamicFiltersOperator1 ,
                                              AV19VariavelCocomo_Nome1 ,
                                              AV20DynamicFiltersEnabled2 ,
                                              AV21DynamicFiltersSelector2 ,
                                              AV22DynamicFiltersOperator2 ,
                                              AV23VariavelCocomo_Nome2 ,
                                              AV24DynamicFiltersEnabled3 ,
                                              AV25DynamicFiltersSelector3 ,
                                              AV26DynamicFiltersOperator3 ,
                                              AV27VariavelCocomo_Nome3 ,
                                              AV35TFVariavelCocomo_AreaTrabalhoCod ,
                                              AV36TFVariavelCocomo_AreaTrabalhoCod_To ,
                                              AV40TFVariavelCocomo_Sigla_Sel ,
                                              AV39TFVariavelCocomo_Sigla ,
                                              AV44TFVariavelCocomo_Nome_Sel ,
                                              AV43TFVariavelCocomo_Nome ,
                                              AV48TFVariavelCocomo_Tipo_Sels.Count ,
                                              AV51TFVariavelCocomo_ProjetoCod ,
                                              AV52TFVariavelCocomo_ProjetoCod_To ,
                                              AV55TFVariavelCocomo_Data ,
                                              AV56TFVariavelCocomo_Data_To ,
                                              AV61TFVariavelCocomo_ExtraBaixo ,
                                              AV62TFVariavelCocomo_ExtraBaixo_To ,
                                              AV65TFVariavelCocomo_MuitoBaixo ,
                                              AV66TFVariavelCocomo_MuitoBaixo_To ,
                                              AV69TFVariavelCocomo_Baixo ,
                                              AV70TFVariavelCocomo_Baixo_To ,
                                              AV73TFVariavelCocomo_Nominal ,
                                              AV74TFVariavelCocomo_Nominal_To ,
                                              AV77TFVariavelCocomo_Alto ,
                                              AV78TFVariavelCocomo_Alto_To ,
                                              AV81TFVariavelCocomo_MuitoAlto ,
                                              AV82TFVariavelCocomo_MuitoAlto_To ,
                                              AV85TFVariavelCocomo_ExtraAlto ,
                                              AV86TFVariavelCocomo_ExtraAlto_To ,
                                              AV89TFVariavelCocomo_Usado ,
                                              AV90TFVariavelCocomo_Usado_To ,
                                              A963VariavelCocomo_Nome ,
                                              A961VariavelCocomo_AreaTrabalhoCod ,
                                              A962VariavelCocomo_Sigla ,
                                              A965VariavelCocomo_ProjetoCod ,
                                              A966VariavelCocomo_Data ,
                                              A986VariavelCocomo_ExtraBaixo ,
                                              A967VariavelCocomo_MuitoBaixo ,
                                              A968VariavelCocomo_Baixo ,
                                              A969VariavelCocomo_Nominal ,
                                              A970VariavelCocomo_Alto ,
                                              A971VariavelCocomo_MuitoAlto ,
                                              A972VariavelCocomo_ExtraAlto ,
                                              A973VariavelCocomo_Usado ,
                                              AV15OrderedBy ,
                                              AV16OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN,
                                              TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN,
                                              TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV19VariavelCocomo_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV19VariavelCocomo_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19VariavelCocomo_Nome1", AV19VariavelCocomo_Nome1);
         lV19VariavelCocomo_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV19VariavelCocomo_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19VariavelCocomo_Nome1", AV19VariavelCocomo_Nome1);
         lV23VariavelCocomo_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV23VariavelCocomo_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23VariavelCocomo_Nome2", AV23VariavelCocomo_Nome2);
         lV23VariavelCocomo_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV23VariavelCocomo_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23VariavelCocomo_Nome2", AV23VariavelCocomo_Nome2);
         lV27VariavelCocomo_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV27VariavelCocomo_Nome3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27VariavelCocomo_Nome3", AV27VariavelCocomo_Nome3);
         lV27VariavelCocomo_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV27VariavelCocomo_Nome3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27VariavelCocomo_Nome3", AV27VariavelCocomo_Nome3);
         lV39TFVariavelCocomo_Sigla = StringUtil.PadR( StringUtil.RTrim( AV39TFVariavelCocomo_Sigla), 15, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFVariavelCocomo_Sigla", AV39TFVariavelCocomo_Sigla);
         lV43TFVariavelCocomo_Nome = StringUtil.PadR( StringUtil.RTrim( AV43TFVariavelCocomo_Nome), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFVariavelCocomo_Nome", AV43TFVariavelCocomo_Nome);
         /* Using cursor H00GL3 */
         pr_default.execute(1, new Object[] {lV19VariavelCocomo_Nome1, lV19VariavelCocomo_Nome1, lV23VariavelCocomo_Nome2, lV23VariavelCocomo_Nome2, lV27VariavelCocomo_Nome3, lV27VariavelCocomo_Nome3, AV35TFVariavelCocomo_AreaTrabalhoCod, AV36TFVariavelCocomo_AreaTrabalhoCod_To, lV39TFVariavelCocomo_Sigla, AV40TFVariavelCocomo_Sigla_Sel, lV43TFVariavelCocomo_Nome, AV44TFVariavelCocomo_Nome_Sel, AV51TFVariavelCocomo_ProjetoCod, AV52TFVariavelCocomo_ProjetoCod_To, AV55TFVariavelCocomo_Data, AV56TFVariavelCocomo_Data_To, AV61TFVariavelCocomo_ExtraBaixo, AV62TFVariavelCocomo_ExtraBaixo_To, AV65TFVariavelCocomo_MuitoBaixo, AV66TFVariavelCocomo_MuitoBaixo_To, AV69TFVariavelCocomo_Baixo, AV70TFVariavelCocomo_Baixo_To, AV73TFVariavelCocomo_Nominal, AV74TFVariavelCocomo_Nominal_To, AV77TFVariavelCocomo_Alto, AV78TFVariavelCocomo_Alto_To, AV81TFVariavelCocomo_MuitoAlto, AV82TFVariavelCocomo_MuitoAlto_To, AV85TFVariavelCocomo_ExtraAlto, AV86TFVariavelCocomo_ExtraAlto_To, AV89TFVariavelCocomo_Usado, AV90TFVariavelCocomo_Usado_To});
         GRID_nRecordCount = H00GL3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15OrderedBy, AV16OrderedDsc, AV17DynamicFiltersSelector1, AV18DynamicFiltersOperator1, AV19VariavelCocomo_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23VariavelCocomo_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27VariavelCocomo_Nome3, AV20DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV35TFVariavelCocomo_AreaTrabalhoCod, AV36TFVariavelCocomo_AreaTrabalhoCod_To, AV39TFVariavelCocomo_Sigla, AV40TFVariavelCocomo_Sigla_Sel, AV43TFVariavelCocomo_Nome, AV44TFVariavelCocomo_Nome_Sel, AV51TFVariavelCocomo_ProjetoCod, AV52TFVariavelCocomo_ProjetoCod_To, AV55TFVariavelCocomo_Data, AV56TFVariavelCocomo_Data_To, AV61TFVariavelCocomo_ExtraBaixo, AV62TFVariavelCocomo_ExtraBaixo_To, AV65TFVariavelCocomo_MuitoBaixo, AV66TFVariavelCocomo_MuitoBaixo_To, AV69TFVariavelCocomo_Baixo, AV70TFVariavelCocomo_Baixo_To, AV73TFVariavelCocomo_Nominal, AV74TFVariavelCocomo_Nominal_To, AV77TFVariavelCocomo_Alto, AV78TFVariavelCocomo_Alto_To, AV81TFVariavelCocomo_MuitoAlto, AV82TFVariavelCocomo_MuitoAlto_To, AV85TFVariavelCocomo_ExtraAlto, AV86TFVariavelCocomo_ExtraAlto_To, AV89TFVariavelCocomo_Usado, AV90TFVariavelCocomo_Usado_To, AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace, AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace, AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace, AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace, AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace, AV59ddo_VariavelCocomo_DataTitleControlIdToReplace, AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace, AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace, AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace, AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace, AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace, AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace, AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace, AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace, AV48TFVariavelCocomo_Tipo_Sels, AV99Pgmname, AV12GridState, AV29DynamicFiltersIgnoreFirst, AV28DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15OrderedBy, AV16OrderedDsc, AV17DynamicFiltersSelector1, AV18DynamicFiltersOperator1, AV19VariavelCocomo_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23VariavelCocomo_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27VariavelCocomo_Nome3, AV20DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV35TFVariavelCocomo_AreaTrabalhoCod, AV36TFVariavelCocomo_AreaTrabalhoCod_To, AV39TFVariavelCocomo_Sigla, AV40TFVariavelCocomo_Sigla_Sel, AV43TFVariavelCocomo_Nome, AV44TFVariavelCocomo_Nome_Sel, AV51TFVariavelCocomo_ProjetoCod, AV52TFVariavelCocomo_ProjetoCod_To, AV55TFVariavelCocomo_Data, AV56TFVariavelCocomo_Data_To, AV61TFVariavelCocomo_ExtraBaixo, AV62TFVariavelCocomo_ExtraBaixo_To, AV65TFVariavelCocomo_MuitoBaixo, AV66TFVariavelCocomo_MuitoBaixo_To, AV69TFVariavelCocomo_Baixo, AV70TFVariavelCocomo_Baixo_To, AV73TFVariavelCocomo_Nominal, AV74TFVariavelCocomo_Nominal_To, AV77TFVariavelCocomo_Alto, AV78TFVariavelCocomo_Alto_To, AV81TFVariavelCocomo_MuitoAlto, AV82TFVariavelCocomo_MuitoAlto_To, AV85TFVariavelCocomo_ExtraAlto, AV86TFVariavelCocomo_ExtraAlto_To, AV89TFVariavelCocomo_Usado, AV90TFVariavelCocomo_Usado_To, AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace, AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace, AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace, AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace, AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace, AV59ddo_VariavelCocomo_DataTitleControlIdToReplace, AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace, AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace, AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace, AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace, AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace, AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace, AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace, AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace, AV48TFVariavelCocomo_Tipo_Sels, AV99Pgmname, AV12GridState, AV29DynamicFiltersIgnoreFirst, AV28DynamicFiltersRemoving) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15OrderedBy, AV16OrderedDsc, AV17DynamicFiltersSelector1, AV18DynamicFiltersOperator1, AV19VariavelCocomo_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23VariavelCocomo_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27VariavelCocomo_Nome3, AV20DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV35TFVariavelCocomo_AreaTrabalhoCod, AV36TFVariavelCocomo_AreaTrabalhoCod_To, AV39TFVariavelCocomo_Sigla, AV40TFVariavelCocomo_Sigla_Sel, AV43TFVariavelCocomo_Nome, AV44TFVariavelCocomo_Nome_Sel, AV51TFVariavelCocomo_ProjetoCod, AV52TFVariavelCocomo_ProjetoCod_To, AV55TFVariavelCocomo_Data, AV56TFVariavelCocomo_Data_To, AV61TFVariavelCocomo_ExtraBaixo, AV62TFVariavelCocomo_ExtraBaixo_To, AV65TFVariavelCocomo_MuitoBaixo, AV66TFVariavelCocomo_MuitoBaixo_To, AV69TFVariavelCocomo_Baixo, AV70TFVariavelCocomo_Baixo_To, AV73TFVariavelCocomo_Nominal, AV74TFVariavelCocomo_Nominal_To, AV77TFVariavelCocomo_Alto, AV78TFVariavelCocomo_Alto_To, AV81TFVariavelCocomo_MuitoAlto, AV82TFVariavelCocomo_MuitoAlto_To, AV85TFVariavelCocomo_ExtraAlto, AV86TFVariavelCocomo_ExtraAlto_To, AV89TFVariavelCocomo_Usado, AV90TFVariavelCocomo_Usado_To, AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace, AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace, AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace, AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace, AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace, AV59ddo_VariavelCocomo_DataTitleControlIdToReplace, AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace, AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace, AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace, AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace, AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace, AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace, AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace, AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace, AV48TFVariavelCocomo_Tipo_Sels, AV99Pgmname, AV12GridState, AV29DynamicFiltersIgnoreFirst, AV28DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15OrderedBy, AV16OrderedDsc, AV17DynamicFiltersSelector1, AV18DynamicFiltersOperator1, AV19VariavelCocomo_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23VariavelCocomo_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27VariavelCocomo_Nome3, AV20DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV35TFVariavelCocomo_AreaTrabalhoCod, AV36TFVariavelCocomo_AreaTrabalhoCod_To, AV39TFVariavelCocomo_Sigla, AV40TFVariavelCocomo_Sigla_Sel, AV43TFVariavelCocomo_Nome, AV44TFVariavelCocomo_Nome_Sel, AV51TFVariavelCocomo_ProjetoCod, AV52TFVariavelCocomo_ProjetoCod_To, AV55TFVariavelCocomo_Data, AV56TFVariavelCocomo_Data_To, AV61TFVariavelCocomo_ExtraBaixo, AV62TFVariavelCocomo_ExtraBaixo_To, AV65TFVariavelCocomo_MuitoBaixo, AV66TFVariavelCocomo_MuitoBaixo_To, AV69TFVariavelCocomo_Baixo, AV70TFVariavelCocomo_Baixo_To, AV73TFVariavelCocomo_Nominal, AV74TFVariavelCocomo_Nominal_To, AV77TFVariavelCocomo_Alto, AV78TFVariavelCocomo_Alto_To, AV81TFVariavelCocomo_MuitoAlto, AV82TFVariavelCocomo_MuitoAlto_To, AV85TFVariavelCocomo_ExtraAlto, AV86TFVariavelCocomo_ExtraAlto_To, AV89TFVariavelCocomo_Usado, AV90TFVariavelCocomo_Usado_To, AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace, AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace, AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace, AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace, AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace, AV59ddo_VariavelCocomo_DataTitleControlIdToReplace, AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace, AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace, AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace, AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace, AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace, AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace, AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace, AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace, AV48TFVariavelCocomo_Tipo_Sels, AV99Pgmname, AV12GridState, AV29DynamicFiltersIgnoreFirst, AV28DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15OrderedBy, AV16OrderedDsc, AV17DynamicFiltersSelector1, AV18DynamicFiltersOperator1, AV19VariavelCocomo_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23VariavelCocomo_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27VariavelCocomo_Nome3, AV20DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV35TFVariavelCocomo_AreaTrabalhoCod, AV36TFVariavelCocomo_AreaTrabalhoCod_To, AV39TFVariavelCocomo_Sigla, AV40TFVariavelCocomo_Sigla_Sel, AV43TFVariavelCocomo_Nome, AV44TFVariavelCocomo_Nome_Sel, AV51TFVariavelCocomo_ProjetoCod, AV52TFVariavelCocomo_ProjetoCod_To, AV55TFVariavelCocomo_Data, AV56TFVariavelCocomo_Data_To, AV61TFVariavelCocomo_ExtraBaixo, AV62TFVariavelCocomo_ExtraBaixo_To, AV65TFVariavelCocomo_MuitoBaixo, AV66TFVariavelCocomo_MuitoBaixo_To, AV69TFVariavelCocomo_Baixo, AV70TFVariavelCocomo_Baixo_To, AV73TFVariavelCocomo_Nominal, AV74TFVariavelCocomo_Nominal_To, AV77TFVariavelCocomo_Alto, AV78TFVariavelCocomo_Alto_To, AV81TFVariavelCocomo_MuitoAlto, AV82TFVariavelCocomo_MuitoAlto_To, AV85TFVariavelCocomo_ExtraAlto, AV86TFVariavelCocomo_ExtraAlto_To, AV89TFVariavelCocomo_Usado, AV90TFVariavelCocomo_Usado_To, AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace, AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace, AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace, AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace, AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace, AV59ddo_VariavelCocomo_DataTitleControlIdToReplace, AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace, AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace, AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace, AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace, AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace, AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace, AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace, AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace, AV48TFVariavelCocomo_Tipo_Sels, AV99Pgmname, AV12GridState, AV29DynamicFiltersIgnoreFirst, AV28DynamicFiltersRemoving) ;
         }
         return (int)(0) ;
      }

      protected void STRUPGL0( )
      {
         /* Before Start, stand alone formulas. */
         AV99Pgmname = "PromptVariaveisCocomo";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E36GL2 */
         E36GL2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV92DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vVARIAVELCOCOMO_AREATRABALHOCODTITLEFILTERDATA"), AV34VariavelCocomo_AreaTrabalhoCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vVARIAVELCOCOMO_SIGLATITLEFILTERDATA"), AV38VariavelCocomo_SiglaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vVARIAVELCOCOMO_NOMETITLEFILTERDATA"), AV42VariavelCocomo_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vVARIAVELCOCOMO_TIPOTITLEFILTERDATA"), AV46VariavelCocomo_TipoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vVARIAVELCOCOMO_PROJETOCODTITLEFILTERDATA"), AV50VariavelCocomo_ProjetoCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vVARIAVELCOCOMO_DATATITLEFILTERDATA"), AV54VariavelCocomo_DataTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vVARIAVELCOCOMO_EXTRABAIXOTITLEFILTERDATA"), AV60VariavelCocomo_ExtraBaixoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vVARIAVELCOCOMO_MUITOBAIXOTITLEFILTERDATA"), AV64VariavelCocomo_MuitoBaixoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vVARIAVELCOCOMO_BAIXOTITLEFILTERDATA"), AV68VariavelCocomo_BaixoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vVARIAVELCOCOMO_NOMINALTITLEFILTERDATA"), AV72VariavelCocomo_NominalTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vVARIAVELCOCOMO_ALTOTITLEFILTERDATA"), AV76VariavelCocomo_AltoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vVARIAVELCOCOMO_MUITOALTOTITLEFILTERDATA"), AV80VariavelCocomo_MuitoAltoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vVARIAVELCOCOMO_EXTRAALTOTITLEFILTERDATA"), AV84VariavelCocomo_ExtraAltoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vVARIAVELCOCOMO_USADOTITLEFILTERDATA"), AV88VariavelCocomo_UsadoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV15OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV16OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16OrderedDsc", AV16OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV17DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersSelector1", AV17DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV18DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18DynamicFiltersOperator1), 4, 0)));
            AV19VariavelCocomo_Nome1 = StringUtil.Upper( cgiGet( edtavVariavelcocomo_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19VariavelCocomo_Nome1", AV19VariavelCocomo_Nome1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV21DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            AV23VariavelCocomo_Nome2 = StringUtil.Upper( cgiGet( edtavVariavelcocomo_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23VariavelCocomo_Nome2", AV23VariavelCocomo_Nome2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV25DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
            AV27VariavelCocomo_Nome3 = StringUtil.Upper( cgiGet( edtavVariavelcocomo_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27VariavelCocomo_Nome3", AV27VariavelCocomo_Nome3);
            A992VariavelCocomo_Sequencial = (short)(context.localUtil.CToN( cgiGet( edtVariavelCocomo_Sequencial_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A992VariavelCocomo_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A992VariavelCocomo_Sequencial), 3, 0)));
            AV20DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
            AV24DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_areatrabalhocod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_areatrabalhocod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFVARIAVELCOCOMO_AREATRABALHOCOD");
               GX_FocusControl = edtavTfvariavelcocomo_areatrabalhocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV35TFVariavelCocomo_AreaTrabalhoCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFVariavelCocomo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFVariavelCocomo_AreaTrabalhoCod), 6, 0)));
            }
            else
            {
               AV35TFVariavelCocomo_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_areatrabalhocod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFVariavelCocomo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFVariavelCocomo_AreaTrabalhoCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_areatrabalhocod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_areatrabalhocod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFVARIAVELCOCOMO_AREATRABALHOCOD_TO");
               GX_FocusControl = edtavTfvariavelcocomo_areatrabalhocod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36TFVariavelCocomo_AreaTrabalhoCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFVariavelCocomo_AreaTrabalhoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFVariavelCocomo_AreaTrabalhoCod_To), 6, 0)));
            }
            else
            {
               AV36TFVariavelCocomo_AreaTrabalhoCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_areatrabalhocod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFVariavelCocomo_AreaTrabalhoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFVariavelCocomo_AreaTrabalhoCod_To), 6, 0)));
            }
            AV39TFVariavelCocomo_Sigla = StringUtil.Upper( cgiGet( edtavTfvariavelcocomo_sigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFVariavelCocomo_Sigla", AV39TFVariavelCocomo_Sigla);
            AV40TFVariavelCocomo_Sigla_Sel = StringUtil.Upper( cgiGet( edtavTfvariavelcocomo_sigla_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFVariavelCocomo_Sigla_Sel", AV40TFVariavelCocomo_Sigla_Sel);
            AV43TFVariavelCocomo_Nome = StringUtil.Upper( cgiGet( edtavTfvariavelcocomo_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFVariavelCocomo_Nome", AV43TFVariavelCocomo_Nome);
            AV44TFVariavelCocomo_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfvariavelcocomo_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFVariavelCocomo_Nome_Sel", AV44TFVariavelCocomo_Nome_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_projetocod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_projetocod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFVARIAVELCOCOMO_PROJETOCOD");
               GX_FocusControl = edtavTfvariavelcocomo_projetocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV51TFVariavelCocomo_ProjetoCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFVariavelCocomo_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFVariavelCocomo_ProjetoCod), 6, 0)));
            }
            else
            {
               AV51TFVariavelCocomo_ProjetoCod = (int)(context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_projetocod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFVariavelCocomo_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFVariavelCocomo_ProjetoCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_projetocod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_projetocod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFVARIAVELCOCOMO_PROJETOCOD_TO");
               GX_FocusControl = edtavTfvariavelcocomo_projetocod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV52TFVariavelCocomo_ProjetoCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFVariavelCocomo_ProjetoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52TFVariavelCocomo_ProjetoCod_To), 6, 0)));
            }
            else
            {
               AV52TFVariavelCocomo_ProjetoCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_projetocod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFVariavelCocomo_ProjetoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52TFVariavelCocomo_ProjetoCod_To), 6, 0)));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfvariavelcocomo_data_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFVariavel Cocomo_Data"}), 1, "vTFVARIAVELCOCOMO_DATA");
               GX_FocusControl = edtavTfvariavelcocomo_data_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV55TFVariavelCocomo_Data = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFVariavelCocomo_Data", context.localUtil.Format(AV55TFVariavelCocomo_Data, "99/99/99"));
            }
            else
            {
               AV55TFVariavelCocomo_Data = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfvariavelcocomo_data_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFVariavelCocomo_Data", context.localUtil.Format(AV55TFVariavelCocomo_Data, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfvariavelcocomo_data_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFVariavel Cocomo_Data_To"}), 1, "vTFVARIAVELCOCOMO_DATA_TO");
               GX_FocusControl = edtavTfvariavelcocomo_data_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV56TFVariavelCocomo_Data_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFVariavelCocomo_Data_To", context.localUtil.Format(AV56TFVariavelCocomo_Data_To, "99/99/99"));
            }
            else
            {
               AV56TFVariavelCocomo_Data_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfvariavelcocomo_data_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFVariavelCocomo_Data_To", context.localUtil.Format(AV56TFVariavelCocomo_Data_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_variavelcocomo_dataauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Variavel Cocomo_Data Aux Date"}), 1, "vDDO_VARIAVELCOCOMO_DATAAUXDATE");
               GX_FocusControl = edtavDdo_variavelcocomo_dataauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV57DDO_VariavelCocomo_DataAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57DDO_VariavelCocomo_DataAuxDate", context.localUtil.Format(AV57DDO_VariavelCocomo_DataAuxDate, "99/99/99"));
            }
            else
            {
               AV57DDO_VariavelCocomo_DataAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_variavelcocomo_dataauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57DDO_VariavelCocomo_DataAuxDate", context.localUtil.Format(AV57DDO_VariavelCocomo_DataAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_variavelcocomo_dataauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Variavel Cocomo_Data Aux Date To"}), 1, "vDDO_VARIAVELCOCOMO_DATAAUXDATETO");
               GX_FocusControl = edtavDdo_variavelcocomo_dataauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV58DDO_VariavelCocomo_DataAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58DDO_VariavelCocomo_DataAuxDateTo", context.localUtil.Format(AV58DDO_VariavelCocomo_DataAuxDateTo, "99/99/99"));
            }
            else
            {
               AV58DDO_VariavelCocomo_DataAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_variavelcocomo_dataauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58DDO_VariavelCocomo_DataAuxDateTo", context.localUtil.Format(AV58DDO_VariavelCocomo_DataAuxDateTo, "99/99/99"));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_extrabaixo_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_extrabaixo_Internalname), ",", ".") > 999999999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFVARIAVELCOCOMO_EXTRABAIXO");
               GX_FocusControl = edtavTfvariavelcocomo_extrabaixo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV61TFVariavelCocomo_ExtraBaixo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFVariavelCocomo_ExtraBaixo", StringUtil.LTrim( StringUtil.Str( AV61TFVariavelCocomo_ExtraBaixo, 12, 2)));
            }
            else
            {
               AV61TFVariavelCocomo_ExtraBaixo = context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_extrabaixo_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFVariavelCocomo_ExtraBaixo", StringUtil.LTrim( StringUtil.Str( AV61TFVariavelCocomo_ExtraBaixo, 12, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_extrabaixo_to_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_extrabaixo_to_Internalname), ",", ".") > 999999999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFVARIAVELCOCOMO_EXTRABAIXO_TO");
               GX_FocusControl = edtavTfvariavelcocomo_extrabaixo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV62TFVariavelCocomo_ExtraBaixo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFVariavelCocomo_ExtraBaixo_To", StringUtil.LTrim( StringUtil.Str( AV62TFVariavelCocomo_ExtraBaixo_To, 12, 2)));
            }
            else
            {
               AV62TFVariavelCocomo_ExtraBaixo_To = context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_extrabaixo_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFVariavelCocomo_ExtraBaixo_To", StringUtil.LTrim( StringUtil.Str( AV62TFVariavelCocomo_ExtraBaixo_To, 12, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_muitobaixo_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_muitobaixo_Internalname), ",", ".") > 999999999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFVARIAVELCOCOMO_MUITOBAIXO");
               GX_FocusControl = edtavTfvariavelcocomo_muitobaixo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV65TFVariavelCocomo_MuitoBaixo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFVariavelCocomo_MuitoBaixo", StringUtil.LTrim( StringUtil.Str( AV65TFVariavelCocomo_MuitoBaixo, 12, 2)));
            }
            else
            {
               AV65TFVariavelCocomo_MuitoBaixo = context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_muitobaixo_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFVariavelCocomo_MuitoBaixo", StringUtil.LTrim( StringUtil.Str( AV65TFVariavelCocomo_MuitoBaixo, 12, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_muitobaixo_to_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_muitobaixo_to_Internalname), ",", ".") > 999999999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFVARIAVELCOCOMO_MUITOBAIXO_TO");
               GX_FocusControl = edtavTfvariavelcocomo_muitobaixo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV66TFVariavelCocomo_MuitoBaixo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFVariavelCocomo_MuitoBaixo_To", StringUtil.LTrim( StringUtil.Str( AV66TFVariavelCocomo_MuitoBaixo_To, 12, 2)));
            }
            else
            {
               AV66TFVariavelCocomo_MuitoBaixo_To = context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_muitobaixo_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFVariavelCocomo_MuitoBaixo_To", StringUtil.LTrim( StringUtil.Str( AV66TFVariavelCocomo_MuitoBaixo_To, 12, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_baixo_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_baixo_Internalname), ",", ".") > 999999999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFVARIAVELCOCOMO_BAIXO");
               GX_FocusControl = edtavTfvariavelcocomo_baixo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV69TFVariavelCocomo_Baixo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFVariavelCocomo_Baixo", StringUtil.LTrim( StringUtil.Str( AV69TFVariavelCocomo_Baixo, 12, 2)));
            }
            else
            {
               AV69TFVariavelCocomo_Baixo = context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_baixo_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFVariavelCocomo_Baixo", StringUtil.LTrim( StringUtil.Str( AV69TFVariavelCocomo_Baixo, 12, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_baixo_to_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_baixo_to_Internalname), ",", ".") > 999999999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFVARIAVELCOCOMO_BAIXO_TO");
               GX_FocusControl = edtavTfvariavelcocomo_baixo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV70TFVariavelCocomo_Baixo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFVariavelCocomo_Baixo_To", StringUtil.LTrim( StringUtil.Str( AV70TFVariavelCocomo_Baixo_To, 12, 2)));
            }
            else
            {
               AV70TFVariavelCocomo_Baixo_To = context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_baixo_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFVariavelCocomo_Baixo_To", StringUtil.LTrim( StringUtil.Str( AV70TFVariavelCocomo_Baixo_To, 12, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_nominal_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_nominal_Internalname), ",", ".") > 999999999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFVARIAVELCOCOMO_NOMINAL");
               GX_FocusControl = edtavTfvariavelcocomo_nominal_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV73TFVariavelCocomo_Nominal = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFVariavelCocomo_Nominal", StringUtil.LTrim( StringUtil.Str( AV73TFVariavelCocomo_Nominal, 12, 2)));
            }
            else
            {
               AV73TFVariavelCocomo_Nominal = context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_nominal_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFVariavelCocomo_Nominal", StringUtil.LTrim( StringUtil.Str( AV73TFVariavelCocomo_Nominal, 12, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_nominal_to_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_nominal_to_Internalname), ",", ".") > 999999999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFVARIAVELCOCOMO_NOMINAL_TO");
               GX_FocusControl = edtavTfvariavelcocomo_nominal_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV74TFVariavelCocomo_Nominal_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFVariavelCocomo_Nominal_To", StringUtil.LTrim( StringUtil.Str( AV74TFVariavelCocomo_Nominal_To, 12, 2)));
            }
            else
            {
               AV74TFVariavelCocomo_Nominal_To = context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_nominal_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFVariavelCocomo_Nominal_To", StringUtil.LTrim( StringUtil.Str( AV74TFVariavelCocomo_Nominal_To, 12, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_alto_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_alto_Internalname), ",", ".") > 999999999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFVARIAVELCOCOMO_ALTO");
               GX_FocusControl = edtavTfvariavelcocomo_alto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV77TFVariavelCocomo_Alto = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFVariavelCocomo_Alto", StringUtil.LTrim( StringUtil.Str( AV77TFVariavelCocomo_Alto, 12, 2)));
            }
            else
            {
               AV77TFVariavelCocomo_Alto = context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_alto_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFVariavelCocomo_Alto", StringUtil.LTrim( StringUtil.Str( AV77TFVariavelCocomo_Alto, 12, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_alto_to_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_alto_to_Internalname), ",", ".") > 999999999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFVARIAVELCOCOMO_ALTO_TO");
               GX_FocusControl = edtavTfvariavelcocomo_alto_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV78TFVariavelCocomo_Alto_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFVariavelCocomo_Alto_To", StringUtil.LTrim( StringUtil.Str( AV78TFVariavelCocomo_Alto_To, 12, 2)));
            }
            else
            {
               AV78TFVariavelCocomo_Alto_To = context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_alto_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFVariavelCocomo_Alto_To", StringUtil.LTrim( StringUtil.Str( AV78TFVariavelCocomo_Alto_To, 12, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_muitoalto_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_muitoalto_Internalname), ",", ".") > 999999999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFVARIAVELCOCOMO_MUITOALTO");
               GX_FocusControl = edtavTfvariavelcocomo_muitoalto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV81TFVariavelCocomo_MuitoAlto = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFVariavelCocomo_MuitoAlto", StringUtil.LTrim( StringUtil.Str( AV81TFVariavelCocomo_MuitoAlto, 12, 2)));
            }
            else
            {
               AV81TFVariavelCocomo_MuitoAlto = context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_muitoalto_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFVariavelCocomo_MuitoAlto", StringUtil.LTrim( StringUtil.Str( AV81TFVariavelCocomo_MuitoAlto, 12, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_muitoalto_to_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_muitoalto_to_Internalname), ",", ".") > 999999999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFVARIAVELCOCOMO_MUITOALTO_TO");
               GX_FocusControl = edtavTfvariavelcocomo_muitoalto_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV82TFVariavelCocomo_MuitoAlto_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFVariavelCocomo_MuitoAlto_To", StringUtil.LTrim( StringUtil.Str( AV82TFVariavelCocomo_MuitoAlto_To, 12, 2)));
            }
            else
            {
               AV82TFVariavelCocomo_MuitoAlto_To = context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_muitoalto_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFVariavelCocomo_MuitoAlto_To", StringUtil.LTrim( StringUtil.Str( AV82TFVariavelCocomo_MuitoAlto_To, 12, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_extraalto_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_extraalto_Internalname), ",", ".") > 999999999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFVARIAVELCOCOMO_EXTRAALTO");
               GX_FocusControl = edtavTfvariavelcocomo_extraalto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV85TFVariavelCocomo_ExtraAlto = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TFVariavelCocomo_ExtraAlto", StringUtil.LTrim( StringUtil.Str( AV85TFVariavelCocomo_ExtraAlto, 12, 2)));
            }
            else
            {
               AV85TFVariavelCocomo_ExtraAlto = context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_extraalto_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TFVariavelCocomo_ExtraAlto", StringUtil.LTrim( StringUtil.Str( AV85TFVariavelCocomo_ExtraAlto, 12, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_extraalto_to_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_extraalto_to_Internalname), ",", ".") > 999999999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFVARIAVELCOCOMO_EXTRAALTO_TO");
               GX_FocusControl = edtavTfvariavelcocomo_extraalto_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV86TFVariavelCocomo_ExtraAlto_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86TFVariavelCocomo_ExtraAlto_To", StringUtil.LTrim( StringUtil.Str( AV86TFVariavelCocomo_ExtraAlto_To, 12, 2)));
            }
            else
            {
               AV86TFVariavelCocomo_ExtraAlto_To = context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_extraalto_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86TFVariavelCocomo_ExtraAlto_To", StringUtil.LTrim( StringUtil.Str( AV86TFVariavelCocomo_ExtraAlto_To, 12, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_usado_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_usado_Internalname), ",", ".") > 999999999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFVARIAVELCOCOMO_USADO");
               GX_FocusControl = edtavTfvariavelcocomo_usado_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV89TFVariavelCocomo_Usado = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89TFVariavelCocomo_Usado", StringUtil.LTrim( StringUtil.Str( AV89TFVariavelCocomo_Usado, 12, 2)));
            }
            else
            {
               AV89TFVariavelCocomo_Usado = context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_usado_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89TFVariavelCocomo_Usado", StringUtil.LTrim( StringUtil.Str( AV89TFVariavelCocomo_Usado, 12, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_usado_to_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_usado_to_Internalname), ",", ".") > 999999999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFVARIAVELCOCOMO_USADO_TO");
               GX_FocusControl = edtavTfvariavelcocomo_usado_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV90TFVariavelCocomo_Usado_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90TFVariavelCocomo_Usado_To", StringUtil.LTrim( StringUtil.Str( AV90TFVariavelCocomo_Usado_To, 12, 2)));
            }
            else
            {
               AV90TFVariavelCocomo_Usado_To = context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_usado_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90TFVariavelCocomo_Usado_To", StringUtil.LTrim( StringUtil.Str( AV90TFVariavelCocomo_Usado_To, 12, 2)));
            }
            AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace = cgiGet( edtavDdo_variavelcocomo_areatrabalhocodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace", AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace);
            AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace = cgiGet( edtavDdo_variavelcocomo_siglatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace", AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace);
            AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace = cgiGet( edtavDdo_variavelcocomo_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace", AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace);
            AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace = cgiGet( edtavDdo_variavelcocomo_tipotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace", AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace);
            AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace = cgiGet( edtavDdo_variavelcocomo_projetocodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace", AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace);
            AV59ddo_VariavelCocomo_DataTitleControlIdToReplace = cgiGet( edtavDdo_variavelcocomo_datatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59ddo_VariavelCocomo_DataTitleControlIdToReplace", AV59ddo_VariavelCocomo_DataTitleControlIdToReplace);
            AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace = cgiGet( edtavDdo_variavelcocomo_extrabaixotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace", AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace);
            AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace = cgiGet( edtavDdo_variavelcocomo_muitobaixotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace", AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace);
            AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace = cgiGet( edtavDdo_variavelcocomo_baixotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace", AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace);
            AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace = cgiGet( edtavDdo_variavelcocomo_nominaltitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace", AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace);
            AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace = cgiGet( edtavDdo_variavelcocomo_altotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace", AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace);
            AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace = cgiGet( edtavDdo_variavelcocomo_muitoaltotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace", AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace);
            AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace = cgiGet( edtavDdo_variavelcocomo_extraaltotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace", AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace);
            AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace = cgiGet( edtavDdo_variavelcocomo_usadotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace", AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_80 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_80"), ",", "."));
            AV94GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV95GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_variavelcocomo_areatrabalhocod_Caption = cgiGet( "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Caption");
            Ddo_variavelcocomo_areatrabalhocod_Tooltip = cgiGet( "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Tooltip");
            Ddo_variavelcocomo_areatrabalhocod_Cls = cgiGet( "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Cls");
            Ddo_variavelcocomo_areatrabalhocod_Filteredtext_set = cgiGet( "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Filteredtext_set");
            Ddo_variavelcocomo_areatrabalhocod_Filteredtextto_set = cgiGet( "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Filteredtextto_set");
            Ddo_variavelcocomo_areatrabalhocod_Dropdownoptionstype = cgiGet( "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Dropdownoptionstype");
            Ddo_variavelcocomo_areatrabalhocod_Titlecontrolidtoreplace = cgiGet( "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Titlecontrolidtoreplace");
            Ddo_variavelcocomo_areatrabalhocod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Includesortasc"));
            Ddo_variavelcocomo_areatrabalhocod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Includesortdsc"));
            Ddo_variavelcocomo_areatrabalhocod_Sortedstatus = cgiGet( "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Sortedstatus");
            Ddo_variavelcocomo_areatrabalhocod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Includefilter"));
            Ddo_variavelcocomo_areatrabalhocod_Filtertype = cgiGet( "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Filtertype");
            Ddo_variavelcocomo_areatrabalhocod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Filterisrange"));
            Ddo_variavelcocomo_areatrabalhocod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Includedatalist"));
            Ddo_variavelcocomo_areatrabalhocod_Sortasc = cgiGet( "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Sortasc");
            Ddo_variavelcocomo_areatrabalhocod_Sortdsc = cgiGet( "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Sortdsc");
            Ddo_variavelcocomo_areatrabalhocod_Cleanfilter = cgiGet( "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Cleanfilter");
            Ddo_variavelcocomo_areatrabalhocod_Rangefilterfrom = cgiGet( "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Rangefilterfrom");
            Ddo_variavelcocomo_areatrabalhocod_Rangefilterto = cgiGet( "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Rangefilterto");
            Ddo_variavelcocomo_areatrabalhocod_Searchbuttontext = cgiGet( "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Searchbuttontext");
            Ddo_variavelcocomo_sigla_Caption = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Caption");
            Ddo_variavelcocomo_sigla_Tooltip = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Tooltip");
            Ddo_variavelcocomo_sigla_Cls = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Cls");
            Ddo_variavelcocomo_sigla_Filteredtext_set = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Filteredtext_set");
            Ddo_variavelcocomo_sigla_Selectedvalue_set = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Selectedvalue_set");
            Ddo_variavelcocomo_sigla_Dropdownoptionstype = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Dropdownoptionstype");
            Ddo_variavelcocomo_sigla_Titlecontrolidtoreplace = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Titlecontrolidtoreplace");
            Ddo_variavelcocomo_sigla_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Includesortasc"));
            Ddo_variavelcocomo_sigla_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Includesortdsc"));
            Ddo_variavelcocomo_sigla_Sortedstatus = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Sortedstatus");
            Ddo_variavelcocomo_sigla_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Includefilter"));
            Ddo_variavelcocomo_sigla_Filtertype = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Filtertype");
            Ddo_variavelcocomo_sigla_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Filterisrange"));
            Ddo_variavelcocomo_sigla_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Includedatalist"));
            Ddo_variavelcocomo_sigla_Datalisttype = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Datalisttype");
            Ddo_variavelcocomo_sigla_Datalistproc = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Datalistproc");
            Ddo_variavelcocomo_sigla_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_variavelcocomo_sigla_Sortasc = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Sortasc");
            Ddo_variavelcocomo_sigla_Sortdsc = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Sortdsc");
            Ddo_variavelcocomo_sigla_Loadingdata = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Loadingdata");
            Ddo_variavelcocomo_sigla_Cleanfilter = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Cleanfilter");
            Ddo_variavelcocomo_sigla_Noresultsfound = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Noresultsfound");
            Ddo_variavelcocomo_sigla_Searchbuttontext = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Searchbuttontext");
            Ddo_variavelcocomo_nome_Caption = cgiGet( "DDO_VARIAVELCOCOMO_NOME_Caption");
            Ddo_variavelcocomo_nome_Tooltip = cgiGet( "DDO_VARIAVELCOCOMO_NOME_Tooltip");
            Ddo_variavelcocomo_nome_Cls = cgiGet( "DDO_VARIAVELCOCOMO_NOME_Cls");
            Ddo_variavelcocomo_nome_Filteredtext_set = cgiGet( "DDO_VARIAVELCOCOMO_NOME_Filteredtext_set");
            Ddo_variavelcocomo_nome_Selectedvalue_set = cgiGet( "DDO_VARIAVELCOCOMO_NOME_Selectedvalue_set");
            Ddo_variavelcocomo_nome_Dropdownoptionstype = cgiGet( "DDO_VARIAVELCOCOMO_NOME_Dropdownoptionstype");
            Ddo_variavelcocomo_nome_Titlecontrolidtoreplace = cgiGet( "DDO_VARIAVELCOCOMO_NOME_Titlecontrolidtoreplace");
            Ddo_variavelcocomo_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_NOME_Includesortasc"));
            Ddo_variavelcocomo_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_NOME_Includesortdsc"));
            Ddo_variavelcocomo_nome_Sortedstatus = cgiGet( "DDO_VARIAVELCOCOMO_NOME_Sortedstatus");
            Ddo_variavelcocomo_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_NOME_Includefilter"));
            Ddo_variavelcocomo_nome_Filtertype = cgiGet( "DDO_VARIAVELCOCOMO_NOME_Filtertype");
            Ddo_variavelcocomo_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_NOME_Filterisrange"));
            Ddo_variavelcocomo_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_NOME_Includedatalist"));
            Ddo_variavelcocomo_nome_Datalisttype = cgiGet( "DDO_VARIAVELCOCOMO_NOME_Datalisttype");
            Ddo_variavelcocomo_nome_Datalistproc = cgiGet( "DDO_VARIAVELCOCOMO_NOME_Datalistproc");
            Ddo_variavelcocomo_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_VARIAVELCOCOMO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_variavelcocomo_nome_Sortasc = cgiGet( "DDO_VARIAVELCOCOMO_NOME_Sortasc");
            Ddo_variavelcocomo_nome_Sortdsc = cgiGet( "DDO_VARIAVELCOCOMO_NOME_Sortdsc");
            Ddo_variavelcocomo_nome_Loadingdata = cgiGet( "DDO_VARIAVELCOCOMO_NOME_Loadingdata");
            Ddo_variavelcocomo_nome_Cleanfilter = cgiGet( "DDO_VARIAVELCOCOMO_NOME_Cleanfilter");
            Ddo_variavelcocomo_nome_Noresultsfound = cgiGet( "DDO_VARIAVELCOCOMO_NOME_Noresultsfound");
            Ddo_variavelcocomo_nome_Searchbuttontext = cgiGet( "DDO_VARIAVELCOCOMO_NOME_Searchbuttontext");
            Ddo_variavelcocomo_tipo_Caption = cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Caption");
            Ddo_variavelcocomo_tipo_Tooltip = cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Tooltip");
            Ddo_variavelcocomo_tipo_Cls = cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Cls");
            Ddo_variavelcocomo_tipo_Selectedvalue_set = cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Selectedvalue_set");
            Ddo_variavelcocomo_tipo_Dropdownoptionstype = cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Dropdownoptionstype");
            Ddo_variavelcocomo_tipo_Titlecontrolidtoreplace = cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Titlecontrolidtoreplace");
            Ddo_variavelcocomo_tipo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Includesortasc"));
            Ddo_variavelcocomo_tipo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Includesortdsc"));
            Ddo_variavelcocomo_tipo_Sortedstatus = cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Sortedstatus");
            Ddo_variavelcocomo_tipo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Includefilter"));
            Ddo_variavelcocomo_tipo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Includedatalist"));
            Ddo_variavelcocomo_tipo_Datalisttype = cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Datalisttype");
            Ddo_variavelcocomo_tipo_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Allowmultipleselection"));
            Ddo_variavelcocomo_tipo_Datalistfixedvalues = cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Datalistfixedvalues");
            Ddo_variavelcocomo_tipo_Sortasc = cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Sortasc");
            Ddo_variavelcocomo_tipo_Sortdsc = cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Sortdsc");
            Ddo_variavelcocomo_tipo_Cleanfilter = cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Cleanfilter");
            Ddo_variavelcocomo_tipo_Searchbuttontext = cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Searchbuttontext");
            Ddo_variavelcocomo_projetocod_Caption = cgiGet( "DDO_VARIAVELCOCOMO_PROJETOCOD_Caption");
            Ddo_variavelcocomo_projetocod_Tooltip = cgiGet( "DDO_VARIAVELCOCOMO_PROJETOCOD_Tooltip");
            Ddo_variavelcocomo_projetocod_Cls = cgiGet( "DDO_VARIAVELCOCOMO_PROJETOCOD_Cls");
            Ddo_variavelcocomo_projetocod_Filteredtext_set = cgiGet( "DDO_VARIAVELCOCOMO_PROJETOCOD_Filteredtext_set");
            Ddo_variavelcocomo_projetocod_Filteredtextto_set = cgiGet( "DDO_VARIAVELCOCOMO_PROJETOCOD_Filteredtextto_set");
            Ddo_variavelcocomo_projetocod_Dropdownoptionstype = cgiGet( "DDO_VARIAVELCOCOMO_PROJETOCOD_Dropdownoptionstype");
            Ddo_variavelcocomo_projetocod_Titlecontrolidtoreplace = cgiGet( "DDO_VARIAVELCOCOMO_PROJETOCOD_Titlecontrolidtoreplace");
            Ddo_variavelcocomo_projetocod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_PROJETOCOD_Includesortasc"));
            Ddo_variavelcocomo_projetocod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_PROJETOCOD_Includesortdsc"));
            Ddo_variavelcocomo_projetocod_Sortedstatus = cgiGet( "DDO_VARIAVELCOCOMO_PROJETOCOD_Sortedstatus");
            Ddo_variavelcocomo_projetocod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_PROJETOCOD_Includefilter"));
            Ddo_variavelcocomo_projetocod_Filtertype = cgiGet( "DDO_VARIAVELCOCOMO_PROJETOCOD_Filtertype");
            Ddo_variavelcocomo_projetocod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_PROJETOCOD_Filterisrange"));
            Ddo_variavelcocomo_projetocod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_PROJETOCOD_Includedatalist"));
            Ddo_variavelcocomo_projetocod_Sortasc = cgiGet( "DDO_VARIAVELCOCOMO_PROJETOCOD_Sortasc");
            Ddo_variavelcocomo_projetocod_Sortdsc = cgiGet( "DDO_VARIAVELCOCOMO_PROJETOCOD_Sortdsc");
            Ddo_variavelcocomo_projetocod_Cleanfilter = cgiGet( "DDO_VARIAVELCOCOMO_PROJETOCOD_Cleanfilter");
            Ddo_variavelcocomo_projetocod_Rangefilterfrom = cgiGet( "DDO_VARIAVELCOCOMO_PROJETOCOD_Rangefilterfrom");
            Ddo_variavelcocomo_projetocod_Rangefilterto = cgiGet( "DDO_VARIAVELCOCOMO_PROJETOCOD_Rangefilterto");
            Ddo_variavelcocomo_projetocod_Searchbuttontext = cgiGet( "DDO_VARIAVELCOCOMO_PROJETOCOD_Searchbuttontext");
            Ddo_variavelcocomo_data_Caption = cgiGet( "DDO_VARIAVELCOCOMO_DATA_Caption");
            Ddo_variavelcocomo_data_Tooltip = cgiGet( "DDO_VARIAVELCOCOMO_DATA_Tooltip");
            Ddo_variavelcocomo_data_Cls = cgiGet( "DDO_VARIAVELCOCOMO_DATA_Cls");
            Ddo_variavelcocomo_data_Filteredtext_set = cgiGet( "DDO_VARIAVELCOCOMO_DATA_Filteredtext_set");
            Ddo_variavelcocomo_data_Filteredtextto_set = cgiGet( "DDO_VARIAVELCOCOMO_DATA_Filteredtextto_set");
            Ddo_variavelcocomo_data_Dropdownoptionstype = cgiGet( "DDO_VARIAVELCOCOMO_DATA_Dropdownoptionstype");
            Ddo_variavelcocomo_data_Titlecontrolidtoreplace = cgiGet( "DDO_VARIAVELCOCOMO_DATA_Titlecontrolidtoreplace");
            Ddo_variavelcocomo_data_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_DATA_Includesortasc"));
            Ddo_variavelcocomo_data_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_DATA_Includesortdsc"));
            Ddo_variavelcocomo_data_Sortedstatus = cgiGet( "DDO_VARIAVELCOCOMO_DATA_Sortedstatus");
            Ddo_variavelcocomo_data_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_DATA_Includefilter"));
            Ddo_variavelcocomo_data_Filtertype = cgiGet( "DDO_VARIAVELCOCOMO_DATA_Filtertype");
            Ddo_variavelcocomo_data_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_DATA_Filterisrange"));
            Ddo_variavelcocomo_data_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_DATA_Includedatalist"));
            Ddo_variavelcocomo_data_Sortasc = cgiGet( "DDO_VARIAVELCOCOMO_DATA_Sortasc");
            Ddo_variavelcocomo_data_Sortdsc = cgiGet( "DDO_VARIAVELCOCOMO_DATA_Sortdsc");
            Ddo_variavelcocomo_data_Cleanfilter = cgiGet( "DDO_VARIAVELCOCOMO_DATA_Cleanfilter");
            Ddo_variavelcocomo_data_Rangefilterfrom = cgiGet( "DDO_VARIAVELCOCOMO_DATA_Rangefilterfrom");
            Ddo_variavelcocomo_data_Rangefilterto = cgiGet( "DDO_VARIAVELCOCOMO_DATA_Rangefilterto");
            Ddo_variavelcocomo_data_Searchbuttontext = cgiGet( "DDO_VARIAVELCOCOMO_DATA_Searchbuttontext");
            Ddo_variavelcocomo_extrabaixo_Caption = cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Caption");
            Ddo_variavelcocomo_extrabaixo_Tooltip = cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Tooltip");
            Ddo_variavelcocomo_extrabaixo_Cls = cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Cls");
            Ddo_variavelcocomo_extrabaixo_Filteredtext_set = cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Filteredtext_set");
            Ddo_variavelcocomo_extrabaixo_Filteredtextto_set = cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Filteredtextto_set");
            Ddo_variavelcocomo_extrabaixo_Dropdownoptionstype = cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Dropdownoptionstype");
            Ddo_variavelcocomo_extrabaixo_Titlecontrolidtoreplace = cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Titlecontrolidtoreplace");
            Ddo_variavelcocomo_extrabaixo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Includesortasc"));
            Ddo_variavelcocomo_extrabaixo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Includesortdsc"));
            Ddo_variavelcocomo_extrabaixo_Sortedstatus = cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Sortedstatus");
            Ddo_variavelcocomo_extrabaixo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Includefilter"));
            Ddo_variavelcocomo_extrabaixo_Filtertype = cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Filtertype");
            Ddo_variavelcocomo_extrabaixo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Filterisrange"));
            Ddo_variavelcocomo_extrabaixo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Includedatalist"));
            Ddo_variavelcocomo_extrabaixo_Sortasc = cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Sortasc");
            Ddo_variavelcocomo_extrabaixo_Sortdsc = cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Sortdsc");
            Ddo_variavelcocomo_extrabaixo_Cleanfilter = cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Cleanfilter");
            Ddo_variavelcocomo_extrabaixo_Rangefilterfrom = cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Rangefilterfrom");
            Ddo_variavelcocomo_extrabaixo_Rangefilterto = cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Rangefilterto");
            Ddo_variavelcocomo_extrabaixo_Searchbuttontext = cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Searchbuttontext");
            Ddo_variavelcocomo_muitobaixo_Caption = cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Caption");
            Ddo_variavelcocomo_muitobaixo_Tooltip = cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Tooltip");
            Ddo_variavelcocomo_muitobaixo_Cls = cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Cls");
            Ddo_variavelcocomo_muitobaixo_Filteredtext_set = cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Filteredtext_set");
            Ddo_variavelcocomo_muitobaixo_Filteredtextto_set = cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Filteredtextto_set");
            Ddo_variavelcocomo_muitobaixo_Dropdownoptionstype = cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Dropdownoptionstype");
            Ddo_variavelcocomo_muitobaixo_Titlecontrolidtoreplace = cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Titlecontrolidtoreplace");
            Ddo_variavelcocomo_muitobaixo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Includesortasc"));
            Ddo_variavelcocomo_muitobaixo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Includesortdsc"));
            Ddo_variavelcocomo_muitobaixo_Sortedstatus = cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Sortedstatus");
            Ddo_variavelcocomo_muitobaixo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Includefilter"));
            Ddo_variavelcocomo_muitobaixo_Filtertype = cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Filtertype");
            Ddo_variavelcocomo_muitobaixo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Filterisrange"));
            Ddo_variavelcocomo_muitobaixo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Includedatalist"));
            Ddo_variavelcocomo_muitobaixo_Sortasc = cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Sortasc");
            Ddo_variavelcocomo_muitobaixo_Sortdsc = cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Sortdsc");
            Ddo_variavelcocomo_muitobaixo_Cleanfilter = cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Cleanfilter");
            Ddo_variavelcocomo_muitobaixo_Rangefilterfrom = cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Rangefilterfrom");
            Ddo_variavelcocomo_muitobaixo_Rangefilterto = cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Rangefilterto");
            Ddo_variavelcocomo_muitobaixo_Searchbuttontext = cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Searchbuttontext");
            Ddo_variavelcocomo_baixo_Caption = cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Caption");
            Ddo_variavelcocomo_baixo_Tooltip = cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Tooltip");
            Ddo_variavelcocomo_baixo_Cls = cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Cls");
            Ddo_variavelcocomo_baixo_Filteredtext_set = cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Filteredtext_set");
            Ddo_variavelcocomo_baixo_Filteredtextto_set = cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Filteredtextto_set");
            Ddo_variavelcocomo_baixo_Dropdownoptionstype = cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Dropdownoptionstype");
            Ddo_variavelcocomo_baixo_Titlecontrolidtoreplace = cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Titlecontrolidtoreplace");
            Ddo_variavelcocomo_baixo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Includesortasc"));
            Ddo_variavelcocomo_baixo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Includesortdsc"));
            Ddo_variavelcocomo_baixo_Sortedstatus = cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Sortedstatus");
            Ddo_variavelcocomo_baixo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Includefilter"));
            Ddo_variavelcocomo_baixo_Filtertype = cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Filtertype");
            Ddo_variavelcocomo_baixo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Filterisrange"));
            Ddo_variavelcocomo_baixo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Includedatalist"));
            Ddo_variavelcocomo_baixo_Sortasc = cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Sortasc");
            Ddo_variavelcocomo_baixo_Sortdsc = cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Sortdsc");
            Ddo_variavelcocomo_baixo_Cleanfilter = cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Cleanfilter");
            Ddo_variavelcocomo_baixo_Rangefilterfrom = cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Rangefilterfrom");
            Ddo_variavelcocomo_baixo_Rangefilterto = cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Rangefilterto");
            Ddo_variavelcocomo_baixo_Searchbuttontext = cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Searchbuttontext");
            Ddo_variavelcocomo_nominal_Caption = cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Caption");
            Ddo_variavelcocomo_nominal_Tooltip = cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Tooltip");
            Ddo_variavelcocomo_nominal_Cls = cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Cls");
            Ddo_variavelcocomo_nominal_Filteredtext_set = cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Filteredtext_set");
            Ddo_variavelcocomo_nominal_Filteredtextto_set = cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Filteredtextto_set");
            Ddo_variavelcocomo_nominal_Dropdownoptionstype = cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Dropdownoptionstype");
            Ddo_variavelcocomo_nominal_Titlecontrolidtoreplace = cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Titlecontrolidtoreplace");
            Ddo_variavelcocomo_nominal_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Includesortasc"));
            Ddo_variavelcocomo_nominal_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Includesortdsc"));
            Ddo_variavelcocomo_nominal_Sortedstatus = cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Sortedstatus");
            Ddo_variavelcocomo_nominal_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Includefilter"));
            Ddo_variavelcocomo_nominal_Filtertype = cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Filtertype");
            Ddo_variavelcocomo_nominal_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Filterisrange"));
            Ddo_variavelcocomo_nominal_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Includedatalist"));
            Ddo_variavelcocomo_nominal_Sortasc = cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Sortasc");
            Ddo_variavelcocomo_nominal_Sortdsc = cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Sortdsc");
            Ddo_variavelcocomo_nominal_Cleanfilter = cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Cleanfilter");
            Ddo_variavelcocomo_nominal_Rangefilterfrom = cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Rangefilterfrom");
            Ddo_variavelcocomo_nominal_Rangefilterto = cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Rangefilterto");
            Ddo_variavelcocomo_nominal_Searchbuttontext = cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Searchbuttontext");
            Ddo_variavelcocomo_alto_Caption = cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Caption");
            Ddo_variavelcocomo_alto_Tooltip = cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Tooltip");
            Ddo_variavelcocomo_alto_Cls = cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Cls");
            Ddo_variavelcocomo_alto_Filteredtext_set = cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Filteredtext_set");
            Ddo_variavelcocomo_alto_Filteredtextto_set = cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Filteredtextto_set");
            Ddo_variavelcocomo_alto_Dropdownoptionstype = cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Dropdownoptionstype");
            Ddo_variavelcocomo_alto_Titlecontrolidtoreplace = cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Titlecontrolidtoreplace");
            Ddo_variavelcocomo_alto_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Includesortasc"));
            Ddo_variavelcocomo_alto_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Includesortdsc"));
            Ddo_variavelcocomo_alto_Sortedstatus = cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Sortedstatus");
            Ddo_variavelcocomo_alto_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Includefilter"));
            Ddo_variavelcocomo_alto_Filtertype = cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Filtertype");
            Ddo_variavelcocomo_alto_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Filterisrange"));
            Ddo_variavelcocomo_alto_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Includedatalist"));
            Ddo_variavelcocomo_alto_Sortasc = cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Sortasc");
            Ddo_variavelcocomo_alto_Sortdsc = cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Sortdsc");
            Ddo_variavelcocomo_alto_Cleanfilter = cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Cleanfilter");
            Ddo_variavelcocomo_alto_Rangefilterfrom = cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Rangefilterfrom");
            Ddo_variavelcocomo_alto_Rangefilterto = cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Rangefilterto");
            Ddo_variavelcocomo_alto_Searchbuttontext = cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Searchbuttontext");
            Ddo_variavelcocomo_muitoalto_Caption = cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Caption");
            Ddo_variavelcocomo_muitoalto_Tooltip = cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Tooltip");
            Ddo_variavelcocomo_muitoalto_Cls = cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Cls");
            Ddo_variavelcocomo_muitoalto_Filteredtext_set = cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Filteredtext_set");
            Ddo_variavelcocomo_muitoalto_Filteredtextto_set = cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Filteredtextto_set");
            Ddo_variavelcocomo_muitoalto_Dropdownoptionstype = cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Dropdownoptionstype");
            Ddo_variavelcocomo_muitoalto_Titlecontrolidtoreplace = cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Titlecontrolidtoreplace");
            Ddo_variavelcocomo_muitoalto_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Includesortasc"));
            Ddo_variavelcocomo_muitoalto_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Includesortdsc"));
            Ddo_variavelcocomo_muitoalto_Sortedstatus = cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Sortedstatus");
            Ddo_variavelcocomo_muitoalto_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Includefilter"));
            Ddo_variavelcocomo_muitoalto_Filtertype = cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Filtertype");
            Ddo_variavelcocomo_muitoalto_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Filterisrange"));
            Ddo_variavelcocomo_muitoalto_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Includedatalist"));
            Ddo_variavelcocomo_muitoalto_Sortasc = cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Sortasc");
            Ddo_variavelcocomo_muitoalto_Sortdsc = cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Sortdsc");
            Ddo_variavelcocomo_muitoalto_Cleanfilter = cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Cleanfilter");
            Ddo_variavelcocomo_muitoalto_Rangefilterfrom = cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Rangefilterfrom");
            Ddo_variavelcocomo_muitoalto_Rangefilterto = cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Rangefilterto");
            Ddo_variavelcocomo_muitoalto_Searchbuttontext = cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Searchbuttontext");
            Ddo_variavelcocomo_extraalto_Caption = cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Caption");
            Ddo_variavelcocomo_extraalto_Tooltip = cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Tooltip");
            Ddo_variavelcocomo_extraalto_Cls = cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Cls");
            Ddo_variavelcocomo_extraalto_Filteredtext_set = cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Filteredtext_set");
            Ddo_variavelcocomo_extraalto_Filteredtextto_set = cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Filteredtextto_set");
            Ddo_variavelcocomo_extraalto_Dropdownoptionstype = cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Dropdownoptionstype");
            Ddo_variavelcocomo_extraalto_Titlecontrolidtoreplace = cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Titlecontrolidtoreplace");
            Ddo_variavelcocomo_extraalto_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Includesortasc"));
            Ddo_variavelcocomo_extraalto_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Includesortdsc"));
            Ddo_variavelcocomo_extraalto_Sortedstatus = cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Sortedstatus");
            Ddo_variavelcocomo_extraalto_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Includefilter"));
            Ddo_variavelcocomo_extraalto_Filtertype = cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Filtertype");
            Ddo_variavelcocomo_extraalto_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Filterisrange"));
            Ddo_variavelcocomo_extraalto_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Includedatalist"));
            Ddo_variavelcocomo_extraalto_Sortasc = cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Sortasc");
            Ddo_variavelcocomo_extraalto_Sortdsc = cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Sortdsc");
            Ddo_variavelcocomo_extraalto_Cleanfilter = cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Cleanfilter");
            Ddo_variavelcocomo_extraalto_Rangefilterfrom = cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Rangefilterfrom");
            Ddo_variavelcocomo_extraalto_Rangefilterto = cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Rangefilterto");
            Ddo_variavelcocomo_extraalto_Searchbuttontext = cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Searchbuttontext");
            Ddo_variavelcocomo_usado_Caption = cgiGet( "DDO_VARIAVELCOCOMO_USADO_Caption");
            Ddo_variavelcocomo_usado_Tooltip = cgiGet( "DDO_VARIAVELCOCOMO_USADO_Tooltip");
            Ddo_variavelcocomo_usado_Cls = cgiGet( "DDO_VARIAVELCOCOMO_USADO_Cls");
            Ddo_variavelcocomo_usado_Filteredtext_set = cgiGet( "DDO_VARIAVELCOCOMO_USADO_Filteredtext_set");
            Ddo_variavelcocomo_usado_Filteredtextto_set = cgiGet( "DDO_VARIAVELCOCOMO_USADO_Filteredtextto_set");
            Ddo_variavelcocomo_usado_Dropdownoptionstype = cgiGet( "DDO_VARIAVELCOCOMO_USADO_Dropdownoptionstype");
            Ddo_variavelcocomo_usado_Titlecontrolidtoreplace = cgiGet( "DDO_VARIAVELCOCOMO_USADO_Titlecontrolidtoreplace");
            Ddo_variavelcocomo_usado_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_USADO_Includesortasc"));
            Ddo_variavelcocomo_usado_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_USADO_Includesortdsc"));
            Ddo_variavelcocomo_usado_Sortedstatus = cgiGet( "DDO_VARIAVELCOCOMO_USADO_Sortedstatus");
            Ddo_variavelcocomo_usado_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_USADO_Includefilter"));
            Ddo_variavelcocomo_usado_Filtertype = cgiGet( "DDO_VARIAVELCOCOMO_USADO_Filtertype");
            Ddo_variavelcocomo_usado_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_USADO_Filterisrange"));
            Ddo_variavelcocomo_usado_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_USADO_Includedatalist"));
            Ddo_variavelcocomo_usado_Sortasc = cgiGet( "DDO_VARIAVELCOCOMO_USADO_Sortasc");
            Ddo_variavelcocomo_usado_Sortdsc = cgiGet( "DDO_VARIAVELCOCOMO_USADO_Sortdsc");
            Ddo_variavelcocomo_usado_Cleanfilter = cgiGet( "DDO_VARIAVELCOCOMO_USADO_Cleanfilter");
            Ddo_variavelcocomo_usado_Rangefilterfrom = cgiGet( "DDO_VARIAVELCOCOMO_USADO_Rangefilterfrom");
            Ddo_variavelcocomo_usado_Rangefilterto = cgiGet( "DDO_VARIAVELCOCOMO_USADO_Rangefilterto");
            Ddo_variavelcocomo_usado_Searchbuttontext = cgiGet( "DDO_VARIAVELCOCOMO_USADO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_variavelcocomo_areatrabalhocod_Activeeventkey = cgiGet( "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Activeeventkey");
            Ddo_variavelcocomo_areatrabalhocod_Filteredtext_get = cgiGet( "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Filteredtext_get");
            Ddo_variavelcocomo_areatrabalhocod_Filteredtextto_get = cgiGet( "DDO_VARIAVELCOCOMO_AREATRABALHOCOD_Filteredtextto_get");
            Ddo_variavelcocomo_sigla_Activeeventkey = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Activeeventkey");
            Ddo_variavelcocomo_sigla_Filteredtext_get = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Filteredtext_get");
            Ddo_variavelcocomo_sigla_Selectedvalue_get = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Selectedvalue_get");
            Ddo_variavelcocomo_nome_Activeeventkey = cgiGet( "DDO_VARIAVELCOCOMO_NOME_Activeeventkey");
            Ddo_variavelcocomo_nome_Filteredtext_get = cgiGet( "DDO_VARIAVELCOCOMO_NOME_Filteredtext_get");
            Ddo_variavelcocomo_nome_Selectedvalue_get = cgiGet( "DDO_VARIAVELCOCOMO_NOME_Selectedvalue_get");
            Ddo_variavelcocomo_tipo_Activeeventkey = cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Activeeventkey");
            Ddo_variavelcocomo_tipo_Selectedvalue_get = cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Selectedvalue_get");
            Ddo_variavelcocomo_projetocod_Activeeventkey = cgiGet( "DDO_VARIAVELCOCOMO_PROJETOCOD_Activeeventkey");
            Ddo_variavelcocomo_projetocod_Filteredtext_get = cgiGet( "DDO_VARIAVELCOCOMO_PROJETOCOD_Filteredtext_get");
            Ddo_variavelcocomo_projetocod_Filteredtextto_get = cgiGet( "DDO_VARIAVELCOCOMO_PROJETOCOD_Filteredtextto_get");
            Ddo_variavelcocomo_data_Activeeventkey = cgiGet( "DDO_VARIAVELCOCOMO_DATA_Activeeventkey");
            Ddo_variavelcocomo_data_Filteredtext_get = cgiGet( "DDO_VARIAVELCOCOMO_DATA_Filteredtext_get");
            Ddo_variavelcocomo_data_Filteredtextto_get = cgiGet( "DDO_VARIAVELCOCOMO_DATA_Filteredtextto_get");
            Ddo_variavelcocomo_extrabaixo_Activeeventkey = cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Activeeventkey");
            Ddo_variavelcocomo_extrabaixo_Filteredtext_get = cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Filteredtext_get");
            Ddo_variavelcocomo_extrabaixo_Filteredtextto_get = cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Filteredtextto_get");
            Ddo_variavelcocomo_muitobaixo_Activeeventkey = cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Activeeventkey");
            Ddo_variavelcocomo_muitobaixo_Filteredtext_get = cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Filteredtext_get");
            Ddo_variavelcocomo_muitobaixo_Filteredtextto_get = cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Filteredtextto_get");
            Ddo_variavelcocomo_baixo_Activeeventkey = cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Activeeventkey");
            Ddo_variavelcocomo_baixo_Filteredtext_get = cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Filteredtext_get");
            Ddo_variavelcocomo_baixo_Filteredtextto_get = cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Filteredtextto_get");
            Ddo_variavelcocomo_nominal_Activeeventkey = cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Activeeventkey");
            Ddo_variavelcocomo_nominal_Filteredtext_get = cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Filteredtext_get");
            Ddo_variavelcocomo_nominal_Filteredtextto_get = cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Filteredtextto_get");
            Ddo_variavelcocomo_alto_Activeeventkey = cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Activeeventkey");
            Ddo_variavelcocomo_alto_Filteredtext_get = cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Filteredtext_get");
            Ddo_variavelcocomo_alto_Filteredtextto_get = cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Filteredtextto_get");
            Ddo_variavelcocomo_muitoalto_Activeeventkey = cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Activeeventkey");
            Ddo_variavelcocomo_muitoalto_Filteredtext_get = cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Filteredtext_get");
            Ddo_variavelcocomo_muitoalto_Filteredtextto_get = cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Filteredtextto_get");
            Ddo_variavelcocomo_extraalto_Activeeventkey = cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Activeeventkey");
            Ddo_variavelcocomo_extraalto_Filteredtext_get = cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Filteredtext_get");
            Ddo_variavelcocomo_extraalto_Filteredtextto_get = cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Filteredtextto_get");
            Ddo_variavelcocomo_usado_Activeeventkey = cgiGet( "DDO_VARIAVELCOCOMO_USADO_Activeeventkey");
            Ddo_variavelcocomo_usado_Filteredtext_get = cgiGet( "DDO_VARIAVELCOCOMO_USADO_Filteredtext_get");
            Ddo_variavelcocomo_usado_Filteredtextto_get = cgiGet( "DDO_VARIAVELCOCOMO_USADO_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = "hsh" + "PromptVariaveisCocomo";
            A992VariavelCocomo_Sequencial = (short)(context.localUtil.CToN( cgiGet( edtVariavelCocomo_Sequencial_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A992VariavelCocomo_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A992VariavelCocomo_Sequencial), 3, 0)));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A992VariavelCocomo_Sequencial), "ZZ9");
            hsh = cgiGet( "hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("promptvariaveiscocomo:[SecurityCheckFailed value for]"+"VariavelCocomo_Sequencial:"+context.localUtil.Format( (decimal)(A992VariavelCocomo_Sequencial), "ZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV15OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV16OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV17DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV18DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vVARIAVELCOCOMO_NOME1"), AV19VariavelCocomo_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV22DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vVARIAVELCOCOMO_NOME2"), AV23VariavelCocomo_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV25DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV26DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vVARIAVELCOCOMO_NOME3"), AV27VariavelCocomo_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV24DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_AREATRABALHOCOD"), ",", ".") != Convert.ToDecimal( AV35TFVariavelCocomo_AreaTrabalhoCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_AREATRABALHOCOD_TO"), ",", ".") != Convert.ToDecimal( AV36TFVariavelCocomo_AreaTrabalhoCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFVARIAVELCOCOMO_SIGLA"), AV39TFVariavelCocomo_Sigla) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFVARIAVELCOCOMO_SIGLA_SEL"), AV40TFVariavelCocomo_Sigla_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFVARIAVELCOCOMO_NOME"), AV43TFVariavelCocomo_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFVARIAVELCOCOMO_NOME_SEL"), AV44TFVariavelCocomo_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_PROJETOCOD"), ",", ".") != Convert.ToDecimal( AV51TFVariavelCocomo_ProjetoCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_PROJETOCOD_TO"), ",", ".") != Convert.ToDecimal( AV52TFVariavelCocomo_ProjetoCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFVARIAVELCOCOMO_DATA"), 0) != AV55TFVariavelCocomo_Data )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFVARIAVELCOCOMO_DATA_TO"), 0) != AV56TFVariavelCocomo_Data_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_EXTRABAIXO"), ",", ".") != AV61TFVariavelCocomo_ExtraBaixo )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_EXTRABAIXO_TO"), ",", ".") != AV62TFVariavelCocomo_ExtraBaixo_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_MUITOBAIXO"), ",", ".") != AV65TFVariavelCocomo_MuitoBaixo )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_MUITOBAIXO_TO"), ",", ".") != AV66TFVariavelCocomo_MuitoBaixo_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_BAIXO"), ",", ".") != AV69TFVariavelCocomo_Baixo )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_BAIXO_TO"), ",", ".") != AV70TFVariavelCocomo_Baixo_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_NOMINAL"), ",", ".") != AV73TFVariavelCocomo_Nominal )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_NOMINAL_TO"), ",", ".") != AV74TFVariavelCocomo_Nominal_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_ALTO"), ",", ".") != AV77TFVariavelCocomo_Alto )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_ALTO_TO"), ",", ".") != AV78TFVariavelCocomo_Alto_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_MUITOALTO"), ",", ".") != AV81TFVariavelCocomo_MuitoAlto )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_MUITOALTO_TO"), ",", ".") != AV82TFVariavelCocomo_MuitoAlto_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_EXTRAALTO"), ",", ".") != AV85TFVariavelCocomo_ExtraAlto )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_EXTRAALTO_TO"), ",", ".") != AV86TFVariavelCocomo_ExtraAlto_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_USADO"), ",", ".") != AV89TFVariavelCocomo_Usado )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_USADO_TO"), ",", ".") != AV90TFVariavelCocomo_Usado_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E36GL2 */
         E36GL2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E36GL2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV17DynamicFiltersSelector1 = "VARIAVELCOCOMO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersSelector1", AV17DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21DynamicFiltersSelector2 = "VARIAVELCOCOMO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV25DynamicFiltersSelector3 = "VARIAVELCOCOMO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfvariavelcocomo_areatrabalhocod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_areatrabalhocod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_areatrabalhocod_Visible), 5, 0)));
         edtavTfvariavelcocomo_areatrabalhocod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_areatrabalhocod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_areatrabalhocod_to_Visible), 5, 0)));
         edtavTfvariavelcocomo_sigla_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_sigla_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_sigla_Visible), 5, 0)));
         edtavTfvariavelcocomo_sigla_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_sigla_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_sigla_sel_Visible), 5, 0)));
         edtavTfvariavelcocomo_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_nome_Visible), 5, 0)));
         edtavTfvariavelcocomo_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_nome_sel_Visible), 5, 0)));
         edtavTfvariavelcocomo_projetocod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_projetocod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_projetocod_Visible), 5, 0)));
         edtavTfvariavelcocomo_projetocod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_projetocod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_projetocod_to_Visible), 5, 0)));
         edtavTfvariavelcocomo_data_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_data_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_data_Visible), 5, 0)));
         edtavTfvariavelcocomo_data_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_data_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_data_to_Visible), 5, 0)));
         edtavTfvariavelcocomo_extrabaixo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_extrabaixo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_extrabaixo_Visible), 5, 0)));
         edtavTfvariavelcocomo_extrabaixo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_extrabaixo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_extrabaixo_to_Visible), 5, 0)));
         edtavTfvariavelcocomo_muitobaixo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_muitobaixo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_muitobaixo_Visible), 5, 0)));
         edtavTfvariavelcocomo_muitobaixo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_muitobaixo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_muitobaixo_to_Visible), 5, 0)));
         edtavTfvariavelcocomo_baixo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_baixo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_baixo_Visible), 5, 0)));
         edtavTfvariavelcocomo_baixo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_baixo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_baixo_to_Visible), 5, 0)));
         edtavTfvariavelcocomo_nominal_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_nominal_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_nominal_Visible), 5, 0)));
         edtavTfvariavelcocomo_nominal_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_nominal_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_nominal_to_Visible), 5, 0)));
         edtavTfvariavelcocomo_alto_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_alto_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_alto_Visible), 5, 0)));
         edtavTfvariavelcocomo_alto_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_alto_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_alto_to_Visible), 5, 0)));
         edtavTfvariavelcocomo_muitoalto_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_muitoalto_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_muitoalto_Visible), 5, 0)));
         edtavTfvariavelcocomo_muitoalto_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_muitoalto_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_muitoalto_to_Visible), 5, 0)));
         edtavTfvariavelcocomo_extraalto_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_extraalto_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_extraalto_Visible), 5, 0)));
         edtavTfvariavelcocomo_extraalto_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_extraalto_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_extraalto_to_Visible), 5, 0)));
         edtavTfvariavelcocomo_usado_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_usado_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_usado_Visible), 5, 0)));
         edtavTfvariavelcocomo_usado_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_usado_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_usado_to_Visible), 5, 0)));
         Ddo_variavelcocomo_areatrabalhocod_Titlecontrolidtoreplace = subGrid_Internalname+"_VariavelCocomo_AreaTrabalhoCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_areatrabalhocod_Internalname, "TitleControlIdToReplace", Ddo_variavelcocomo_areatrabalhocod_Titlecontrolidtoreplace);
         AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace = Ddo_variavelcocomo_areatrabalhocod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace", AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace);
         edtavDdo_variavelcocomo_areatrabalhocodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_variavelcocomo_areatrabalhocodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_variavelcocomo_areatrabalhocodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_variavelcocomo_sigla_Titlecontrolidtoreplace = subGrid_Internalname+"_VariavelCocomo_Sigla";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_sigla_Internalname, "TitleControlIdToReplace", Ddo_variavelcocomo_sigla_Titlecontrolidtoreplace);
         AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace = Ddo_variavelcocomo_sigla_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace", AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace);
         edtavDdo_variavelcocomo_siglatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_variavelcocomo_siglatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_variavelcocomo_siglatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_variavelcocomo_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_VariavelCocomo_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_nome_Internalname, "TitleControlIdToReplace", Ddo_variavelcocomo_nome_Titlecontrolidtoreplace);
         AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace = Ddo_variavelcocomo_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace", AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace);
         edtavDdo_variavelcocomo_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_variavelcocomo_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_variavelcocomo_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_variavelcocomo_tipo_Titlecontrolidtoreplace = subGrid_Internalname+"_VariavelCocomo_Tipo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_tipo_Internalname, "TitleControlIdToReplace", Ddo_variavelcocomo_tipo_Titlecontrolidtoreplace);
         AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace = Ddo_variavelcocomo_tipo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace", AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace);
         edtavDdo_variavelcocomo_tipotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_variavelcocomo_tipotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_variavelcocomo_tipotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_variavelcocomo_projetocod_Titlecontrolidtoreplace = subGrid_Internalname+"_VariavelCocomo_ProjetoCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_projetocod_Internalname, "TitleControlIdToReplace", Ddo_variavelcocomo_projetocod_Titlecontrolidtoreplace);
         AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace = Ddo_variavelcocomo_projetocod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace", AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace);
         edtavDdo_variavelcocomo_projetocodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_variavelcocomo_projetocodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_variavelcocomo_projetocodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_variavelcocomo_data_Titlecontrolidtoreplace = subGrid_Internalname+"_VariavelCocomo_Data";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_data_Internalname, "TitleControlIdToReplace", Ddo_variavelcocomo_data_Titlecontrolidtoreplace);
         AV59ddo_VariavelCocomo_DataTitleControlIdToReplace = Ddo_variavelcocomo_data_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59ddo_VariavelCocomo_DataTitleControlIdToReplace", AV59ddo_VariavelCocomo_DataTitleControlIdToReplace);
         edtavDdo_variavelcocomo_datatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_variavelcocomo_datatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_variavelcocomo_datatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_variavelcocomo_extrabaixo_Titlecontrolidtoreplace = subGrid_Internalname+"_VariavelCocomo_ExtraBaixo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_extrabaixo_Internalname, "TitleControlIdToReplace", Ddo_variavelcocomo_extrabaixo_Titlecontrolidtoreplace);
         AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace = Ddo_variavelcocomo_extrabaixo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace", AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace);
         edtavDdo_variavelcocomo_extrabaixotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_variavelcocomo_extrabaixotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_variavelcocomo_extrabaixotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_variavelcocomo_muitobaixo_Titlecontrolidtoreplace = subGrid_Internalname+"_VariavelCocomo_MuitoBaixo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_muitobaixo_Internalname, "TitleControlIdToReplace", Ddo_variavelcocomo_muitobaixo_Titlecontrolidtoreplace);
         AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace = Ddo_variavelcocomo_muitobaixo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace", AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace);
         edtavDdo_variavelcocomo_muitobaixotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_variavelcocomo_muitobaixotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_variavelcocomo_muitobaixotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_variavelcocomo_baixo_Titlecontrolidtoreplace = subGrid_Internalname+"_VariavelCocomo_Baixo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_baixo_Internalname, "TitleControlIdToReplace", Ddo_variavelcocomo_baixo_Titlecontrolidtoreplace);
         AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace = Ddo_variavelcocomo_baixo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace", AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace);
         edtavDdo_variavelcocomo_baixotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_variavelcocomo_baixotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_variavelcocomo_baixotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_variavelcocomo_nominal_Titlecontrolidtoreplace = subGrid_Internalname+"_VariavelCocomo_Nominal";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_nominal_Internalname, "TitleControlIdToReplace", Ddo_variavelcocomo_nominal_Titlecontrolidtoreplace);
         AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace = Ddo_variavelcocomo_nominal_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace", AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace);
         edtavDdo_variavelcocomo_nominaltitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_variavelcocomo_nominaltitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_variavelcocomo_nominaltitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_variavelcocomo_alto_Titlecontrolidtoreplace = subGrid_Internalname+"_VariavelCocomo_Alto";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_alto_Internalname, "TitleControlIdToReplace", Ddo_variavelcocomo_alto_Titlecontrolidtoreplace);
         AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace = Ddo_variavelcocomo_alto_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace", AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace);
         edtavDdo_variavelcocomo_altotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_variavelcocomo_altotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_variavelcocomo_altotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_variavelcocomo_muitoalto_Titlecontrolidtoreplace = subGrid_Internalname+"_VariavelCocomo_MuitoAlto";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_muitoalto_Internalname, "TitleControlIdToReplace", Ddo_variavelcocomo_muitoalto_Titlecontrolidtoreplace);
         AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace = Ddo_variavelcocomo_muitoalto_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace", AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace);
         edtavDdo_variavelcocomo_muitoaltotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_variavelcocomo_muitoaltotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_variavelcocomo_muitoaltotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_variavelcocomo_extraalto_Titlecontrolidtoreplace = subGrid_Internalname+"_VariavelCocomo_ExtraAlto";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_extraalto_Internalname, "TitleControlIdToReplace", Ddo_variavelcocomo_extraalto_Titlecontrolidtoreplace);
         AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace = Ddo_variavelcocomo_extraalto_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace", AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace);
         edtavDdo_variavelcocomo_extraaltotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_variavelcocomo_extraaltotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_variavelcocomo_extraaltotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_variavelcocomo_usado_Titlecontrolidtoreplace = subGrid_Internalname+"_VariavelCocomo_Usado";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_usado_Internalname, "TitleControlIdToReplace", Ddo_variavelcocomo_usado_Titlecontrolidtoreplace);
         AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace = Ddo_variavelcocomo_usado_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace", AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace);
         edtavDdo_variavelcocomo_usadotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_variavelcocomo_usadotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_variavelcocomo_usadotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione Variaveis Cocomo";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         edtVariavelCocomo_Sequencial_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_Sequencial_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtVariavelCocomo_Sequencial_Visible), 5, 0)));
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Nome", 0);
         cmbavOrderedby.addItem("2", "de Trabalho", 0);
         cmbavOrderedby.addItem("3", "Sigla", 0);
         cmbavOrderedby.addItem("4", "Tipo", 0);
         cmbavOrderedby.addItem("5", "Sistema", 0);
         cmbavOrderedby.addItem("6", "Data", 0);
         cmbavOrderedby.addItem("7", "Extra Baixo", 0);
         cmbavOrderedby.addItem("8", "Baixo", 0);
         cmbavOrderedby.addItem("9", "Baixo", 0);
         cmbavOrderedby.addItem("10", "Nominal", 0);
         cmbavOrderedby.addItem("11", "Alto", 0);
         cmbavOrderedby.addItem("12", "Alto", 0);
         cmbavOrderedby.addItem("13", "Alto", 0);
         cmbavOrderedby.addItem("14", "usado", 0);
         if ( AV15OrderedBy < 1 )
         {
            AV15OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV92DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV92DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E37GL2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV34VariavelCocomo_AreaTrabalhoCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38VariavelCocomo_SiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42VariavelCocomo_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46VariavelCocomo_TipoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV50VariavelCocomo_ProjetoCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV54VariavelCocomo_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV60VariavelCocomo_ExtraBaixoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV64VariavelCocomo_MuitoBaixoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV68VariavelCocomo_BaixoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV72VariavelCocomo_NominalTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV76VariavelCocomo_AltoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV80VariavelCocomo_MuitoAltoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV84VariavelCocomo_ExtraAltoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV88VariavelCocomo_UsadoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtVariavelCocomo_AreaTrabalhoCod_Titleformat = 2;
         edtVariavelCocomo_AreaTrabalhoCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "de Trabalho", AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_AreaTrabalhoCod_Internalname, "Title", edtVariavelCocomo_AreaTrabalhoCod_Title);
         edtVariavelCocomo_Sigla_Titleformat = 2;
         edtVariavelCocomo_Sigla_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Sigla", AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_Sigla_Internalname, "Title", edtVariavelCocomo_Sigla_Title);
         edtVariavelCocomo_Nome_Titleformat = 2;
         edtVariavelCocomo_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_Nome_Internalname, "Title", edtVariavelCocomo_Nome_Title);
         cmbVariavelCocomo_Tipo_Titleformat = 2;
         cmbVariavelCocomo_Tipo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tipo", AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbVariavelCocomo_Tipo_Internalname, "Title", cmbVariavelCocomo_Tipo.Title.Text);
         edtVariavelCocomo_ProjetoCod_Titleformat = 2;
         edtVariavelCocomo_ProjetoCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Sistema", AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_ProjetoCod_Internalname, "Title", edtVariavelCocomo_ProjetoCod_Title);
         edtVariavelCocomo_Data_Titleformat = 2;
         edtVariavelCocomo_Data_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Data", AV59ddo_VariavelCocomo_DataTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_Data_Internalname, "Title", edtVariavelCocomo_Data_Title);
         edtVariavelCocomo_ExtraBaixo_Titleformat = 2;
         edtVariavelCocomo_ExtraBaixo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Extra Baixo", AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_ExtraBaixo_Internalname, "Title", edtVariavelCocomo_ExtraBaixo_Title);
         edtVariavelCocomo_MuitoBaixo_Titleformat = 2;
         edtVariavelCocomo_MuitoBaixo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Baixo", AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_MuitoBaixo_Internalname, "Title", edtVariavelCocomo_MuitoBaixo_Title);
         edtVariavelCocomo_Baixo_Titleformat = 2;
         edtVariavelCocomo_Baixo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Baixo", AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_Baixo_Internalname, "Title", edtVariavelCocomo_Baixo_Title);
         edtVariavelCocomo_Nominal_Titleformat = 2;
         edtVariavelCocomo_Nominal_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nominal", AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_Nominal_Internalname, "Title", edtVariavelCocomo_Nominal_Title);
         edtVariavelCocomo_Alto_Titleformat = 2;
         edtVariavelCocomo_Alto_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Alto", AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_Alto_Internalname, "Title", edtVariavelCocomo_Alto_Title);
         edtVariavelCocomo_MuitoAlto_Titleformat = 2;
         edtVariavelCocomo_MuitoAlto_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Alto", AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_MuitoAlto_Internalname, "Title", edtVariavelCocomo_MuitoAlto_Title);
         edtVariavelCocomo_ExtraAlto_Titleformat = 2;
         edtVariavelCocomo_ExtraAlto_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Alto", AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_ExtraAlto_Internalname, "Title", edtVariavelCocomo_ExtraAlto_Title);
         edtVariavelCocomo_Usado_Titleformat = 2;
         edtVariavelCocomo_Usado_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "usado", AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_Usado_Internalname, "Title", edtVariavelCocomo_Usado_Title);
         AV94GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV94GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV94GridCurrentPage), 10, 0)));
         AV95GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV95GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV95GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV34VariavelCocomo_AreaTrabalhoCodTitleFilterData", AV34VariavelCocomo_AreaTrabalhoCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV38VariavelCocomo_SiglaTitleFilterData", AV38VariavelCocomo_SiglaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV42VariavelCocomo_NomeTitleFilterData", AV42VariavelCocomo_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV46VariavelCocomo_TipoTitleFilterData", AV46VariavelCocomo_TipoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV50VariavelCocomo_ProjetoCodTitleFilterData", AV50VariavelCocomo_ProjetoCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV54VariavelCocomo_DataTitleFilterData", AV54VariavelCocomo_DataTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV60VariavelCocomo_ExtraBaixoTitleFilterData", AV60VariavelCocomo_ExtraBaixoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV64VariavelCocomo_MuitoBaixoTitleFilterData", AV64VariavelCocomo_MuitoBaixoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV68VariavelCocomo_BaixoTitleFilterData", AV68VariavelCocomo_BaixoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV72VariavelCocomo_NominalTitleFilterData", AV72VariavelCocomo_NominalTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV76VariavelCocomo_AltoTitleFilterData", AV76VariavelCocomo_AltoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV80VariavelCocomo_MuitoAltoTitleFilterData", AV80VariavelCocomo_MuitoAltoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV84VariavelCocomo_ExtraAltoTitleFilterData", AV84VariavelCocomo_ExtraAltoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV88VariavelCocomo_UsadoTitleFilterData", AV88VariavelCocomo_UsadoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV12GridState", AV12GridState);
      }

      protected void E11GL2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV93PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV93PageToGo) ;
         }
      }

      protected void E12GL2( )
      {
         /* Ddo_variavelcocomo_areatrabalhocod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_variavelcocomo_areatrabalhocod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV15OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV16OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16OrderedDsc", AV16OrderedDsc);
            Ddo_variavelcocomo_areatrabalhocod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_areatrabalhocod_Internalname, "SortedStatus", Ddo_variavelcocomo_areatrabalhocod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_variavelcocomo_areatrabalhocod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV15OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV16OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16OrderedDsc", AV16OrderedDsc);
            Ddo_variavelcocomo_areatrabalhocod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_areatrabalhocod_Internalname, "SortedStatus", Ddo_variavelcocomo_areatrabalhocod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_variavelcocomo_areatrabalhocod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV35TFVariavelCocomo_AreaTrabalhoCod = (int)(NumberUtil.Val( Ddo_variavelcocomo_areatrabalhocod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFVariavelCocomo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFVariavelCocomo_AreaTrabalhoCod), 6, 0)));
            AV36TFVariavelCocomo_AreaTrabalhoCod_To = (int)(NumberUtil.Val( Ddo_variavelcocomo_areatrabalhocod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFVariavelCocomo_AreaTrabalhoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFVariavelCocomo_AreaTrabalhoCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13GL2( )
      {
         /* Ddo_variavelcocomo_sigla_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_variavelcocomo_sigla_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV15OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV16OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16OrderedDsc", AV16OrderedDsc);
            Ddo_variavelcocomo_sigla_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_sigla_Internalname, "SortedStatus", Ddo_variavelcocomo_sigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_variavelcocomo_sigla_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV15OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV16OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16OrderedDsc", AV16OrderedDsc);
            Ddo_variavelcocomo_sigla_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_sigla_Internalname, "SortedStatus", Ddo_variavelcocomo_sigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_variavelcocomo_sigla_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV39TFVariavelCocomo_Sigla = Ddo_variavelcocomo_sigla_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFVariavelCocomo_Sigla", AV39TFVariavelCocomo_Sigla);
            AV40TFVariavelCocomo_Sigla_Sel = Ddo_variavelcocomo_sigla_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFVariavelCocomo_Sigla_Sel", AV40TFVariavelCocomo_Sigla_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14GL2( )
      {
         /* Ddo_variavelcocomo_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_variavelcocomo_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV15OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV16OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16OrderedDsc", AV16OrderedDsc);
            Ddo_variavelcocomo_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_nome_Internalname, "SortedStatus", Ddo_variavelcocomo_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_variavelcocomo_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV15OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV16OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16OrderedDsc", AV16OrderedDsc);
            Ddo_variavelcocomo_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_nome_Internalname, "SortedStatus", Ddo_variavelcocomo_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_variavelcocomo_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV43TFVariavelCocomo_Nome = Ddo_variavelcocomo_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFVariavelCocomo_Nome", AV43TFVariavelCocomo_Nome);
            AV44TFVariavelCocomo_Nome_Sel = Ddo_variavelcocomo_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFVariavelCocomo_Nome_Sel", AV44TFVariavelCocomo_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15GL2( )
      {
         /* Ddo_variavelcocomo_tipo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_variavelcocomo_tipo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV15OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV16OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16OrderedDsc", AV16OrderedDsc);
            Ddo_variavelcocomo_tipo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_tipo_Internalname, "SortedStatus", Ddo_variavelcocomo_tipo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_variavelcocomo_tipo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV15OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV16OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16OrderedDsc", AV16OrderedDsc);
            Ddo_variavelcocomo_tipo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_tipo_Internalname, "SortedStatus", Ddo_variavelcocomo_tipo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_variavelcocomo_tipo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV47TFVariavelCocomo_Tipo_SelsJson = Ddo_variavelcocomo_tipo_Selectedvalue_get;
            AV48TFVariavelCocomo_Tipo_Sels.FromJSonString(AV47TFVariavelCocomo_Tipo_SelsJson);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV48TFVariavelCocomo_Tipo_Sels", AV48TFVariavelCocomo_Tipo_Sels);
      }

      protected void E16GL2( )
      {
         /* Ddo_variavelcocomo_projetocod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_variavelcocomo_projetocod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV15OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV16OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16OrderedDsc", AV16OrderedDsc);
            Ddo_variavelcocomo_projetocod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_projetocod_Internalname, "SortedStatus", Ddo_variavelcocomo_projetocod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_variavelcocomo_projetocod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV15OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV16OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16OrderedDsc", AV16OrderedDsc);
            Ddo_variavelcocomo_projetocod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_projetocod_Internalname, "SortedStatus", Ddo_variavelcocomo_projetocod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_variavelcocomo_projetocod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV51TFVariavelCocomo_ProjetoCod = (int)(NumberUtil.Val( Ddo_variavelcocomo_projetocod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFVariavelCocomo_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFVariavelCocomo_ProjetoCod), 6, 0)));
            AV52TFVariavelCocomo_ProjetoCod_To = (int)(NumberUtil.Val( Ddo_variavelcocomo_projetocod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFVariavelCocomo_ProjetoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52TFVariavelCocomo_ProjetoCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E17GL2( )
      {
         /* Ddo_variavelcocomo_data_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_variavelcocomo_data_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV15OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV16OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16OrderedDsc", AV16OrderedDsc);
            Ddo_variavelcocomo_data_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_data_Internalname, "SortedStatus", Ddo_variavelcocomo_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_variavelcocomo_data_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV15OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV16OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16OrderedDsc", AV16OrderedDsc);
            Ddo_variavelcocomo_data_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_data_Internalname, "SortedStatus", Ddo_variavelcocomo_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_variavelcocomo_data_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV55TFVariavelCocomo_Data = context.localUtil.CToD( Ddo_variavelcocomo_data_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFVariavelCocomo_Data", context.localUtil.Format(AV55TFVariavelCocomo_Data, "99/99/99"));
            AV56TFVariavelCocomo_Data_To = context.localUtil.CToD( Ddo_variavelcocomo_data_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFVariavelCocomo_Data_To", context.localUtil.Format(AV56TFVariavelCocomo_Data_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E18GL2( )
      {
         /* Ddo_variavelcocomo_extrabaixo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_variavelcocomo_extrabaixo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV15OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV16OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16OrderedDsc", AV16OrderedDsc);
            Ddo_variavelcocomo_extrabaixo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_extrabaixo_Internalname, "SortedStatus", Ddo_variavelcocomo_extrabaixo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_variavelcocomo_extrabaixo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV15OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV16OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16OrderedDsc", AV16OrderedDsc);
            Ddo_variavelcocomo_extrabaixo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_extrabaixo_Internalname, "SortedStatus", Ddo_variavelcocomo_extrabaixo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_variavelcocomo_extrabaixo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV61TFVariavelCocomo_ExtraBaixo = NumberUtil.Val( Ddo_variavelcocomo_extrabaixo_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFVariavelCocomo_ExtraBaixo", StringUtil.LTrim( StringUtil.Str( AV61TFVariavelCocomo_ExtraBaixo, 12, 2)));
            AV62TFVariavelCocomo_ExtraBaixo_To = NumberUtil.Val( Ddo_variavelcocomo_extrabaixo_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFVariavelCocomo_ExtraBaixo_To", StringUtil.LTrim( StringUtil.Str( AV62TFVariavelCocomo_ExtraBaixo_To, 12, 2)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E19GL2( )
      {
         /* Ddo_variavelcocomo_muitobaixo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_variavelcocomo_muitobaixo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV15OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV16OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16OrderedDsc", AV16OrderedDsc);
            Ddo_variavelcocomo_muitobaixo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_muitobaixo_Internalname, "SortedStatus", Ddo_variavelcocomo_muitobaixo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_variavelcocomo_muitobaixo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV15OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV16OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16OrderedDsc", AV16OrderedDsc);
            Ddo_variavelcocomo_muitobaixo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_muitobaixo_Internalname, "SortedStatus", Ddo_variavelcocomo_muitobaixo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_variavelcocomo_muitobaixo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV65TFVariavelCocomo_MuitoBaixo = NumberUtil.Val( Ddo_variavelcocomo_muitobaixo_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFVariavelCocomo_MuitoBaixo", StringUtil.LTrim( StringUtil.Str( AV65TFVariavelCocomo_MuitoBaixo, 12, 2)));
            AV66TFVariavelCocomo_MuitoBaixo_To = NumberUtil.Val( Ddo_variavelcocomo_muitobaixo_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFVariavelCocomo_MuitoBaixo_To", StringUtil.LTrim( StringUtil.Str( AV66TFVariavelCocomo_MuitoBaixo_To, 12, 2)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E20GL2( )
      {
         /* Ddo_variavelcocomo_baixo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_variavelcocomo_baixo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV15OrderedBy = 9;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV16OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16OrderedDsc", AV16OrderedDsc);
            Ddo_variavelcocomo_baixo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_baixo_Internalname, "SortedStatus", Ddo_variavelcocomo_baixo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_variavelcocomo_baixo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV15OrderedBy = 9;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV16OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16OrderedDsc", AV16OrderedDsc);
            Ddo_variavelcocomo_baixo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_baixo_Internalname, "SortedStatus", Ddo_variavelcocomo_baixo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_variavelcocomo_baixo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV69TFVariavelCocomo_Baixo = NumberUtil.Val( Ddo_variavelcocomo_baixo_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFVariavelCocomo_Baixo", StringUtil.LTrim( StringUtil.Str( AV69TFVariavelCocomo_Baixo, 12, 2)));
            AV70TFVariavelCocomo_Baixo_To = NumberUtil.Val( Ddo_variavelcocomo_baixo_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFVariavelCocomo_Baixo_To", StringUtil.LTrim( StringUtil.Str( AV70TFVariavelCocomo_Baixo_To, 12, 2)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E21GL2( )
      {
         /* Ddo_variavelcocomo_nominal_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_variavelcocomo_nominal_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV15OrderedBy = 10;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV16OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16OrderedDsc", AV16OrderedDsc);
            Ddo_variavelcocomo_nominal_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_nominal_Internalname, "SortedStatus", Ddo_variavelcocomo_nominal_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_variavelcocomo_nominal_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV15OrderedBy = 10;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV16OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16OrderedDsc", AV16OrderedDsc);
            Ddo_variavelcocomo_nominal_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_nominal_Internalname, "SortedStatus", Ddo_variavelcocomo_nominal_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_variavelcocomo_nominal_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV73TFVariavelCocomo_Nominal = NumberUtil.Val( Ddo_variavelcocomo_nominal_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFVariavelCocomo_Nominal", StringUtil.LTrim( StringUtil.Str( AV73TFVariavelCocomo_Nominal, 12, 2)));
            AV74TFVariavelCocomo_Nominal_To = NumberUtil.Val( Ddo_variavelcocomo_nominal_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFVariavelCocomo_Nominal_To", StringUtil.LTrim( StringUtil.Str( AV74TFVariavelCocomo_Nominal_To, 12, 2)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E22GL2( )
      {
         /* Ddo_variavelcocomo_alto_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_variavelcocomo_alto_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV15OrderedBy = 11;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV16OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16OrderedDsc", AV16OrderedDsc);
            Ddo_variavelcocomo_alto_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_alto_Internalname, "SortedStatus", Ddo_variavelcocomo_alto_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_variavelcocomo_alto_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV15OrderedBy = 11;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV16OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16OrderedDsc", AV16OrderedDsc);
            Ddo_variavelcocomo_alto_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_alto_Internalname, "SortedStatus", Ddo_variavelcocomo_alto_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_variavelcocomo_alto_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV77TFVariavelCocomo_Alto = NumberUtil.Val( Ddo_variavelcocomo_alto_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFVariavelCocomo_Alto", StringUtil.LTrim( StringUtil.Str( AV77TFVariavelCocomo_Alto, 12, 2)));
            AV78TFVariavelCocomo_Alto_To = NumberUtil.Val( Ddo_variavelcocomo_alto_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFVariavelCocomo_Alto_To", StringUtil.LTrim( StringUtil.Str( AV78TFVariavelCocomo_Alto_To, 12, 2)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E23GL2( )
      {
         /* Ddo_variavelcocomo_muitoalto_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_variavelcocomo_muitoalto_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV15OrderedBy = 12;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV16OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16OrderedDsc", AV16OrderedDsc);
            Ddo_variavelcocomo_muitoalto_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_muitoalto_Internalname, "SortedStatus", Ddo_variavelcocomo_muitoalto_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_variavelcocomo_muitoalto_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV15OrderedBy = 12;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV16OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16OrderedDsc", AV16OrderedDsc);
            Ddo_variavelcocomo_muitoalto_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_muitoalto_Internalname, "SortedStatus", Ddo_variavelcocomo_muitoalto_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_variavelcocomo_muitoalto_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV81TFVariavelCocomo_MuitoAlto = NumberUtil.Val( Ddo_variavelcocomo_muitoalto_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFVariavelCocomo_MuitoAlto", StringUtil.LTrim( StringUtil.Str( AV81TFVariavelCocomo_MuitoAlto, 12, 2)));
            AV82TFVariavelCocomo_MuitoAlto_To = NumberUtil.Val( Ddo_variavelcocomo_muitoalto_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFVariavelCocomo_MuitoAlto_To", StringUtil.LTrim( StringUtil.Str( AV82TFVariavelCocomo_MuitoAlto_To, 12, 2)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E24GL2( )
      {
         /* Ddo_variavelcocomo_extraalto_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_variavelcocomo_extraalto_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV15OrderedBy = 13;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV16OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16OrderedDsc", AV16OrderedDsc);
            Ddo_variavelcocomo_extraalto_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_extraalto_Internalname, "SortedStatus", Ddo_variavelcocomo_extraalto_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_variavelcocomo_extraalto_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV15OrderedBy = 13;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV16OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16OrderedDsc", AV16OrderedDsc);
            Ddo_variavelcocomo_extraalto_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_extraalto_Internalname, "SortedStatus", Ddo_variavelcocomo_extraalto_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_variavelcocomo_extraalto_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV85TFVariavelCocomo_ExtraAlto = NumberUtil.Val( Ddo_variavelcocomo_extraalto_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TFVariavelCocomo_ExtraAlto", StringUtil.LTrim( StringUtil.Str( AV85TFVariavelCocomo_ExtraAlto, 12, 2)));
            AV86TFVariavelCocomo_ExtraAlto_To = NumberUtil.Val( Ddo_variavelcocomo_extraalto_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86TFVariavelCocomo_ExtraAlto_To", StringUtil.LTrim( StringUtil.Str( AV86TFVariavelCocomo_ExtraAlto_To, 12, 2)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E25GL2( )
      {
         /* Ddo_variavelcocomo_usado_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_variavelcocomo_usado_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV15OrderedBy = 14;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV16OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16OrderedDsc", AV16OrderedDsc);
            Ddo_variavelcocomo_usado_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_usado_Internalname, "SortedStatus", Ddo_variavelcocomo_usado_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_variavelcocomo_usado_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV15OrderedBy = 14;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV16OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16OrderedDsc", AV16OrderedDsc);
            Ddo_variavelcocomo_usado_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_usado_Internalname, "SortedStatus", Ddo_variavelcocomo_usado_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_variavelcocomo_usado_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV89TFVariavelCocomo_Usado = NumberUtil.Val( Ddo_variavelcocomo_usado_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89TFVariavelCocomo_Usado", StringUtil.LTrim( StringUtil.Str( AV89TFVariavelCocomo_Usado, 12, 2)));
            AV90TFVariavelCocomo_Usado_To = NumberUtil.Val( Ddo_variavelcocomo_usado_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90TFVariavelCocomo_Usado_To", StringUtil.LTrim( StringUtil.Str( AV90TFVariavelCocomo_Usado_To, 12, 2)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E38GL2( )
      {
         /* Grid_Load Routine */
         AV30Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV30Select);
         AV98Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 80;
         }
         sendrow_802( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_80_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(80, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E39GL2 */
         E39GL2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E39GL2( )
      {
         /* Enter Routine */
         AV7InOutVariavelCocomo_AreaTrabalhoCod = A961VariavelCocomo_AreaTrabalhoCod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutVariavelCocomo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutVariavelCocomo_AreaTrabalhoCod), 6, 0)));
         AV8InOutVariavelCocomo_Sigla = A962VariavelCocomo_Sigla;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutVariavelCocomo_Sigla", AV8InOutVariavelCocomo_Sigla);
         AV9InOutVariavelCocomo_Tipo = A964VariavelCocomo_Tipo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9InOutVariavelCocomo_Tipo", AV9InOutVariavelCocomo_Tipo);
         AV33InOutVariavelCocomo_Sequencial = A992VariavelCocomo_Sequencial;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33InOutVariavelCocomo_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33InOutVariavelCocomo_Sequencial), 3, 0)));
         AV10InOutVariavelCocomo_Nome = A963VariavelCocomo_Nome;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10InOutVariavelCocomo_Nome", AV10InOutVariavelCocomo_Nome);
         context.setWebReturnParms(new Object[] {(int)AV7InOutVariavelCocomo_AreaTrabalhoCod,(String)AV8InOutVariavelCocomo_Sigla,(String)AV9InOutVariavelCocomo_Tipo,(short)AV33InOutVariavelCocomo_Sequencial,(String)AV10InOutVariavelCocomo_Nome});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E26GL2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E31GL2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV20DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E27GL2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV28DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersRemoving", AV28DynamicFiltersRemoving);
         AV29DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersIgnoreFirst", AV29DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV28DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersRemoving", AV28DynamicFiltersRemoving);
         AV29DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersIgnoreFirst", AV29DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV15OrderedBy, AV16OrderedDsc, AV17DynamicFiltersSelector1, AV18DynamicFiltersOperator1, AV19VariavelCocomo_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23VariavelCocomo_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27VariavelCocomo_Nome3, AV20DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV35TFVariavelCocomo_AreaTrabalhoCod, AV36TFVariavelCocomo_AreaTrabalhoCod_To, AV39TFVariavelCocomo_Sigla, AV40TFVariavelCocomo_Sigla_Sel, AV43TFVariavelCocomo_Nome, AV44TFVariavelCocomo_Nome_Sel, AV51TFVariavelCocomo_ProjetoCod, AV52TFVariavelCocomo_ProjetoCod_To, AV55TFVariavelCocomo_Data, AV56TFVariavelCocomo_Data_To, AV61TFVariavelCocomo_ExtraBaixo, AV62TFVariavelCocomo_ExtraBaixo_To, AV65TFVariavelCocomo_MuitoBaixo, AV66TFVariavelCocomo_MuitoBaixo_To, AV69TFVariavelCocomo_Baixo, AV70TFVariavelCocomo_Baixo_To, AV73TFVariavelCocomo_Nominal, AV74TFVariavelCocomo_Nominal_To, AV77TFVariavelCocomo_Alto, AV78TFVariavelCocomo_Alto_To, AV81TFVariavelCocomo_MuitoAlto, AV82TFVariavelCocomo_MuitoAlto_To, AV85TFVariavelCocomo_ExtraAlto, AV86TFVariavelCocomo_ExtraAlto_To, AV89TFVariavelCocomo_Usado, AV90TFVariavelCocomo_Usado_To, AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace, AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace, AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace, AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace, AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace, AV59ddo_VariavelCocomo_DataTitleControlIdToReplace, AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace, AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace, AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace, AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace, AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace, AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace, AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace, AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace, AV48TFVariavelCocomo_Tipo_Sels, AV99Pgmname, AV12GridState, AV29DynamicFiltersIgnoreFirst, AV28DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV12GridState", AV12GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV17DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV18DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E32GL2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E33GL2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV24DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E28GL2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV28DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersRemoving", AV28DynamicFiltersRemoving);
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV28DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersRemoving", AV28DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV15OrderedBy, AV16OrderedDsc, AV17DynamicFiltersSelector1, AV18DynamicFiltersOperator1, AV19VariavelCocomo_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23VariavelCocomo_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27VariavelCocomo_Nome3, AV20DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV35TFVariavelCocomo_AreaTrabalhoCod, AV36TFVariavelCocomo_AreaTrabalhoCod_To, AV39TFVariavelCocomo_Sigla, AV40TFVariavelCocomo_Sigla_Sel, AV43TFVariavelCocomo_Nome, AV44TFVariavelCocomo_Nome_Sel, AV51TFVariavelCocomo_ProjetoCod, AV52TFVariavelCocomo_ProjetoCod_To, AV55TFVariavelCocomo_Data, AV56TFVariavelCocomo_Data_To, AV61TFVariavelCocomo_ExtraBaixo, AV62TFVariavelCocomo_ExtraBaixo_To, AV65TFVariavelCocomo_MuitoBaixo, AV66TFVariavelCocomo_MuitoBaixo_To, AV69TFVariavelCocomo_Baixo, AV70TFVariavelCocomo_Baixo_To, AV73TFVariavelCocomo_Nominal, AV74TFVariavelCocomo_Nominal_To, AV77TFVariavelCocomo_Alto, AV78TFVariavelCocomo_Alto_To, AV81TFVariavelCocomo_MuitoAlto, AV82TFVariavelCocomo_MuitoAlto_To, AV85TFVariavelCocomo_ExtraAlto, AV86TFVariavelCocomo_ExtraAlto_To, AV89TFVariavelCocomo_Usado, AV90TFVariavelCocomo_Usado_To, AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace, AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace, AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace, AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace, AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace, AV59ddo_VariavelCocomo_DataTitleControlIdToReplace, AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace, AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace, AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace, AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace, AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace, AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace, AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace, AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace, AV48TFVariavelCocomo_Tipo_Sels, AV99Pgmname, AV12GridState, AV29DynamicFiltersIgnoreFirst, AV28DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV12GridState", AV12GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV17DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV18DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E34GL2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E29GL2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV28DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersRemoving", AV28DynamicFiltersRemoving);
         AV24DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV28DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersRemoving", AV28DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV15OrderedBy, AV16OrderedDsc, AV17DynamicFiltersSelector1, AV18DynamicFiltersOperator1, AV19VariavelCocomo_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23VariavelCocomo_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27VariavelCocomo_Nome3, AV20DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV35TFVariavelCocomo_AreaTrabalhoCod, AV36TFVariavelCocomo_AreaTrabalhoCod_To, AV39TFVariavelCocomo_Sigla, AV40TFVariavelCocomo_Sigla_Sel, AV43TFVariavelCocomo_Nome, AV44TFVariavelCocomo_Nome_Sel, AV51TFVariavelCocomo_ProjetoCod, AV52TFVariavelCocomo_ProjetoCod_To, AV55TFVariavelCocomo_Data, AV56TFVariavelCocomo_Data_To, AV61TFVariavelCocomo_ExtraBaixo, AV62TFVariavelCocomo_ExtraBaixo_To, AV65TFVariavelCocomo_MuitoBaixo, AV66TFVariavelCocomo_MuitoBaixo_To, AV69TFVariavelCocomo_Baixo, AV70TFVariavelCocomo_Baixo_To, AV73TFVariavelCocomo_Nominal, AV74TFVariavelCocomo_Nominal_To, AV77TFVariavelCocomo_Alto, AV78TFVariavelCocomo_Alto_To, AV81TFVariavelCocomo_MuitoAlto, AV82TFVariavelCocomo_MuitoAlto_To, AV85TFVariavelCocomo_ExtraAlto, AV86TFVariavelCocomo_ExtraAlto_To, AV89TFVariavelCocomo_Usado, AV90TFVariavelCocomo_Usado_To, AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace, AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace, AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace, AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace, AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace, AV59ddo_VariavelCocomo_DataTitleControlIdToReplace, AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace, AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace, AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace, AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace, AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace, AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace, AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace, AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace, AV48TFVariavelCocomo_Tipo_Sels, AV99Pgmname, AV12GridState, AV29DynamicFiltersIgnoreFirst, AV28DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV12GridState", AV12GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV17DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV18DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E35GL2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E30GL2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV48TFVariavelCocomo_Tipo_Sels", AV48TFVariavelCocomo_Tipo_Sels);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV17DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV18DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV12GridState", AV12GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_variavelcocomo_areatrabalhocod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_areatrabalhocod_Internalname, "SortedStatus", Ddo_variavelcocomo_areatrabalhocod_Sortedstatus);
         Ddo_variavelcocomo_sigla_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_sigla_Internalname, "SortedStatus", Ddo_variavelcocomo_sigla_Sortedstatus);
         Ddo_variavelcocomo_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_nome_Internalname, "SortedStatus", Ddo_variavelcocomo_nome_Sortedstatus);
         Ddo_variavelcocomo_tipo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_tipo_Internalname, "SortedStatus", Ddo_variavelcocomo_tipo_Sortedstatus);
         Ddo_variavelcocomo_projetocod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_projetocod_Internalname, "SortedStatus", Ddo_variavelcocomo_projetocod_Sortedstatus);
         Ddo_variavelcocomo_data_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_data_Internalname, "SortedStatus", Ddo_variavelcocomo_data_Sortedstatus);
         Ddo_variavelcocomo_extrabaixo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_extrabaixo_Internalname, "SortedStatus", Ddo_variavelcocomo_extrabaixo_Sortedstatus);
         Ddo_variavelcocomo_muitobaixo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_muitobaixo_Internalname, "SortedStatus", Ddo_variavelcocomo_muitobaixo_Sortedstatus);
         Ddo_variavelcocomo_baixo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_baixo_Internalname, "SortedStatus", Ddo_variavelcocomo_baixo_Sortedstatus);
         Ddo_variavelcocomo_nominal_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_nominal_Internalname, "SortedStatus", Ddo_variavelcocomo_nominal_Sortedstatus);
         Ddo_variavelcocomo_alto_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_alto_Internalname, "SortedStatus", Ddo_variavelcocomo_alto_Sortedstatus);
         Ddo_variavelcocomo_muitoalto_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_muitoalto_Internalname, "SortedStatus", Ddo_variavelcocomo_muitoalto_Sortedstatus);
         Ddo_variavelcocomo_extraalto_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_extraalto_Internalname, "SortedStatus", Ddo_variavelcocomo_extraalto_Sortedstatus);
         Ddo_variavelcocomo_usado_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_usado_Internalname, "SortedStatus", Ddo_variavelcocomo_usado_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV15OrderedBy == 2 )
         {
            Ddo_variavelcocomo_areatrabalhocod_Sortedstatus = (AV16OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_areatrabalhocod_Internalname, "SortedStatus", Ddo_variavelcocomo_areatrabalhocod_Sortedstatus);
         }
         else if ( AV15OrderedBy == 3 )
         {
            Ddo_variavelcocomo_sigla_Sortedstatus = (AV16OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_sigla_Internalname, "SortedStatus", Ddo_variavelcocomo_sigla_Sortedstatus);
         }
         else if ( AV15OrderedBy == 1 )
         {
            Ddo_variavelcocomo_nome_Sortedstatus = (AV16OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_nome_Internalname, "SortedStatus", Ddo_variavelcocomo_nome_Sortedstatus);
         }
         else if ( AV15OrderedBy == 4 )
         {
            Ddo_variavelcocomo_tipo_Sortedstatus = (AV16OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_tipo_Internalname, "SortedStatus", Ddo_variavelcocomo_tipo_Sortedstatus);
         }
         else if ( AV15OrderedBy == 5 )
         {
            Ddo_variavelcocomo_projetocod_Sortedstatus = (AV16OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_projetocod_Internalname, "SortedStatus", Ddo_variavelcocomo_projetocod_Sortedstatus);
         }
         else if ( AV15OrderedBy == 6 )
         {
            Ddo_variavelcocomo_data_Sortedstatus = (AV16OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_data_Internalname, "SortedStatus", Ddo_variavelcocomo_data_Sortedstatus);
         }
         else if ( AV15OrderedBy == 7 )
         {
            Ddo_variavelcocomo_extrabaixo_Sortedstatus = (AV16OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_extrabaixo_Internalname, "SortedStatus", Ddo_variavelcocomo_extrabaixo_Sortedstatus);
         }
         else if ( AV15OrderedBy == 8 )
         {
            Ddo_variavelcocomo_muitobaixo_Sortedstatus = (AV16OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_muitobaixo_Internalname, "SortedStatus", Ddo_variavelcocomo_muitobaixo_Sortedstatus);
         }
         else if ( AV15OrderedBy == 9 )
         {
            Ddo_variavelcocomo_baixo_Sortedstatus = (AV16OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_baixo_Internalname, "SortedStatus", Ddo_variavelcocomo_baixo_Sortedstatus);
         }
         else if ( AV15OrderedBy == 10 )
         {
            Ddo_variavelcocomo_nominal_Sortedstatus = (AV16OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_nominal_Internalname, "SortedStatus", Ddo_variavelcocomo_nominal_Sortedstatus);
         }
         else if ( AV15OrderedBy == 11 )
         {
            Ddo_variavelcocomo_alto_Sortedstatus = (AV16OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_alto_Internalname, "SortedStatus", Ddo_variavelcocomo_alto_Sortedstatus);
         }
         else if ( AV15OrderedBy == 12 )
         {
            Ddo_variavelcocomo_muitoalto_Sortedstatus = (AV16OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_muitoalto_Internalname, "SortedStatus", Ddo_variavelcocomo_muitoalto_Sortedstatus);
         }
         else if ( AV15OrderedBy == 13 )
         {
            Ddo_variavelcocomo_extraalto_Sortedstatus = (AV16OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_extraalto_Internalname, "SortedStatus", Ddo_variavelcocomo_extraalto_Sortedstatus);
         }
         else if ( AV15OrderedBy == 14 )
         {
            Ddo_variavelcocomo_usado_Sortedstatus = (AV16OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_usado_Internalname, "SortedStatus", Ddo_variavelcocomo_usado_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavVariavelcocomo_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavVariavelcocomo_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVariavelcocomo_nome1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "VARIAVELCOCOMO_NOME") == 0 )
         {
            edtavVariavelcocomo_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavVariavelcocomo_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVariavelcocomo_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavVariavelcocomo_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavVariavelcocomo_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVariavelcocomo_nome2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "VARIAVELCOCOMO_NOME") == 0 )
         {
            edtavVariavelcocomo_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavVariavelcocomo_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVariavelcocomo_nome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavVariavelcocomo_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavVariavelcocomo_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVariavelcocomo_nome3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "VARIAVELCOCOMO_NOME") == 0 )
         {
            edtavVariavelcocomo_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavVariavelcocomo_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVariavelcocomo_nome3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         AV21DynamicFiltersSelector2 = "VARIAVELCOCOMO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         AV22DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         AV23VariavelCocomo_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23VariavelCocomo_Nome2", AV23VariavelCocomo_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV24DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         AV25DynamicFiltersSelector3 = "VARIAVELCOCOMO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         AV26DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
         AV27VariavelCocomo_Nome3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27VariavelCocomo_Nome3", AV27VariavelCocomo_Nome3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV35TFVariavelCocomo_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFVariavelCocomo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFVariavelCocomo_AreaTrabalhoCod), 6, 0)));
         Ddo_variavelcocomo_areatrabalhocod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_areatrabalhocod_Internalname, "FilteredText_set", Ddo_variavelcocomo_areatrabalhocod_Filteredtext_set);
         AV36TFVariavelCocomo_AreaTrabalhoCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFVariavelCocomo_AreaTrabalhoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFVariavelCocomo_AreaTrabalhoCod_To), 6, 0)));
         Ddo_variavelcocomo_areatrabalhocod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_areatrabalhocod_Internalname, "FilteredTextTo_set", Ddo_variavelcocomo_areatrabalhocod_Filteredtextto_set);
         AV39TFVariavelCocomo_Sigla = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFVariavelCocomo_Sigla", AV39TFVariavelCocomo_Sigla);
         Ddo_variavelcocomo_sigla_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_sigla_Internalname, "FilteredText_set", Ddo_variavelcocomo_sigla_Filteredtext_set);
         AV40TFVariavelCocomo_Sigla_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFVariavelCocomo_Sigla_Sel", AV40TFVariavelCocomo_Sigla_Sel);
         Ddo_variavelcocomo_sigla_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_sigla_Internalname, "SelectedValue_set", Ddo_variavelcocomo_sigla_Selectedvalue_set);
         AV43TFVariavelCocomo_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFVariavelCocomo_Nome", AV43TFVariavelCocomo_Nome);
         Ddo_variavelcocomo_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_nome_Internalname, "FilteredText_set", Ddo_variavelcocomo_nome_Filteredtext_set);
         AV44TFVariavelCocomo_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFVariavelCocomo_Nome_Sel", AV44TFVariavelCocomo_Nome_Sel);
         Ddo_variavelcocomo_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_nome_Internalname, "SelectedValue_set", Ddo_variavelcocomo_nome_Selectedvalue_set);
         AV48TFVariavelCocomo_Tipo_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_variavelcocomo_tipo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_tipo_Internalname, "SelectedValue_set", Ddo_variavelcocomo_tipo_Selectedvalue_set);
         AV51TFVariavelCocomo_ProjetoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFVariavelCocomo_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFVariavelCocomo_ProjetoCod), 6, 0)));
         Ddo_variavelcocomo_projetocod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_projetocod_Internalname, "FilteredText_set", Ddo_variavelcocomo_projetocod_Filteredtext_set);
         AV52TFVariavelCocomo_ProjetoCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFVariavelCocomo_ProjetoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52TFVariavelCocomo_ProjetoCod_To), 6, 0)));
         Ddo_variavelcocomo_projetocod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_projetocod_Internalname, "FilteredTextTo_set", Ddo_variavelcocomo_projetocod_Filteredtextto_set);
         AV55TFVariavelCocomo_Data = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFVariavelCocomo_Data", context.localUtil.Format(AV55TFVariavelCocomo_Data, "99/99/99"));
         Ddo_variavelcocomo_data_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_data_Internalname, "FilteredText_set", Ddo_variavelcocomo_data_Filteredtext_set);
         AV56TFVariavelCocomo_Data_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFVariavelCocomo_Data_To", context.localUtil.Format(AV56TFVariavelCocomo_Data_To, "99/99/99"));
         Ddo_variavelcocomo_data_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_data_Internalname, "FilteredTextTo_set", Ddo_variavelcocomo_data_Filteredtextto_set);
         AV61TFVariavelCocomo_ExtraBaixo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFVariavelCocomo_ExtraBaixo", StringUtil.LTrim( StringUtil.Str( AV61TFVariavelCocomo_ExtraBaixo, 12, 2)));
         Ddo_variavelcocomo_extrabaixo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_extrabaixo_Internalname, "FilteredText_set", Ddo_variavelcocomo_extrabaixo_Filteredtext_set);
         AV62TFVariavelCocomo_ExtraBaixo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFVariavelCocomo_ExtraBaixo_To", StringUtil.LTrim( StringUtil.Str( AV62TFVariavelCocomo_ExtraBaixo_To, 12, 2)));
         Ddo_variavelcocomo_extrabaixo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_extrabaixo_Internalname, "FilteredTextTo_set", Ddo_variavelcocomo_extrabaixo_Filteredtextto_set);
         AV65TFVariavelCocomo_MuitoBaixo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFVariavelCocomo_MuitoBaixo", StringUtil.LTrim( StringUtil.Str( AV65TFVariavelCocomo_MuitoBaixo, 12, 2)));
         Ddo_variavelcocomo_muitobaixo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_muitobaixo_Internalname, "FilteredText_set", Ddo_variavelcocomo_muitobaixo_Filteredtext_set);
         AV66TFVariavelCocomo_MuitoBaixo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFVariavelCocomo_MuitoBaixo_To", StringUtil.LTrim( StringUtil.Str( AV66TFVariavelCocomo_MuitoBaixo_To, 12, 2)));
         Ddo_variavelcocomo_muitobaixo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_muitobaixo_Internalname, "FilteredTextTo_set", Ddo_variavelcocomo_muitobaixo_Filteredtextto_set);
         AV69TFVariavelCocomo_Baixo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFVariavelCocomo_Baixo", StringUtil.LTrim( StringUtil.Str( AV69TFVariavelCocomo_Baixo, 12, 2)));
         Ddo_variavelcocomo_baixo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_baixo_Internalname, "FilteredText_set", Ddo_variavelcocomo_baixo_Filteredtext_set);
         AV70TFVariavelCocomo_Baixo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFVariavelCocomo_Baixo_To", StringUtil.LTrim( StringUtil.Str( AV70TFVariavelCocomo_Baixo_To, 12, 2)));
         Ddo_variavelcocomo_baixo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_baixo_Internalname, "FilteredTextTo_set", Ddo_variavelcocomo_baixo_Filteredtextto_set);
         AV73TFVariavelCocomo_Nominal = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFVariavelCocomo_Nominal", StringUtil.LTrim( StringUtil.Str( AV73TFVariavelCocomo_Nominal, 12, 2)));
         Ddo_variavelcocomo_nominal_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_nominal_Internalname, "FilteredText_set", Ddo_variavelcocomo_nominal_Filteredtext_set);
         AV74TFVariavelCocomo_Nominal_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFVariavelCocomo_Nominal_To", StringUtil.LTrim( StringUtil.Str( AV74TFVariavelCocomo_Nominal_To, 12, 2)));
         Ddo_variavelcocomo_nominal_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_nominal_Internalname, "FilteredTextTo_set", Ddo_variavelcocomo_nominal_Filteredtextto_set);
         AV77TFVariavelCocomo_Alto = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFVariavelCocomo_Alto", StringUtil.LTrim( StringUtil.Str( AV77TFVariavelCocomo_Alto, 12, 2)));
         Ddo_variavelcocomo_alto_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_alto_Internalname, "FilteredText_set", Ddo_variavelcocomo_alto_Filteredtext_set);
         AV78TFVariavelCocomo_Alto_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFVariavelCocomo_Alto_To", StringUtil.LTrim( StringUtil.Str( AV78TFVariavelCocomo_Alto_To, 12, 2)));
         Ddo_variavelcocomo_alto_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_alto_Internalname, "FilteredTextTo_set", Ddo_variavelcocomo_alto_Filteredtextto_set);
         AV81TFVariavelCocomo_MuitoAlto = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFVariavelCocomo_MuitoAlto", StringUtil.LTrim( StringUtil.Str( AV81TFVariavelCocomo_MuitoAlto, 12, 2)));
         Ddo_variavelcocomo_muitoalto_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_muitoalto_Internalname, "FilteredText_set", Ddo_variavelcocomo_muitoalto_Filteredtext_set);
         AV82TFVariavelCocomo_MuitoAlto_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFVariavelCocomo_MuitoAlto_To", StringUtil.LTrim( StringUtil.Str( AV82TFVariavelCocomo_MuitoAlto_To, 12, 2)));
         Ddo_variavelcocomo_muitoalto_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_muitoalto_Internalname, "FilteredTextTo_set", Ddo_variavelcocomo_muitoalto_Filteredtextto_set);
         AV85TFVariavelCocomo_ExtraAlto = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TFVariavelCocomo_ExtraAlto", StringUtil.LTrim( StringUtil.Str( AV85TFVariavelCocomo_ExtraAlto, 12, 2)));
         Ddo_variavelcocomo_extraalto_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_extraalto_Internalname, "FilteredText_set", Ddo_variavelcocomo_extraalto_Filteredtext_set);
         AV86TFVariavelCocomo_ExtraAlto_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86TFVariavelCocomo_ExtraAlto_To", StringUtil.LTrim( StringUtil.Str( AV86TFVariavelCocomo_ExtraAlto_To, 12, 2)));
         Ddo_variavelcocomo_extraalto_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_extraalto_Internalname, "FilteredTextTo_set", Ddo_variavelcocomo_extraalto_Filteredtextto_set);
         AV89TFVariavelCocomo_Usado = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89TFVariavelCocomo_Usado", StringUtil.LTrim( StringUtil.Str( AV89TFVariavelCocomo_Usado, 12, 2)));
         Ddo_variavelcocomo_usado_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_usado_Internalname, "FilteredText_set", Ddo_variavelcocomo_usado_Filteredtext_set);
         AV90TFVariavelCocomo_Usado_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90TFVariavelCocomo_Usado_To", StringUtil.LTrim( StringUtil.Str( AV90TFVariavelCocomo_Usado_To, 12, 2)));
         Ddo_variavelcocomo_usado_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_usado_Internalname, "FilteredTextTo_set", Ddo_variavelcocomo_usado_Filteredtextto_set);
         AV17DynamicFiltersSelector1 = "VARIAVELCOCOMO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersSelector1", AV17DynamicFiltersSelector1);
         AV18DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18DynamicFiltersOperator1), 4, 0)));
         AV19VariavelCocomo_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19VariavelCocomo_Nome1", AV19VariavelCocomo_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV12GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV12GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV14GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV12GridState.gxTpr_Dynamicfilters.Item(1));
            AV17DynamicFiltersSelector1 = AV14GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersSelector1", AV17DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "VARIAVELCOCOMO_NOME") == 0 )
            {
               AV18DynamicFiltersOperator1 = AV14GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18DynamicFiltersOperator1), 4, 0)));
               AV19VariavelCocomo_Nome1 = AV14GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19VariavelCocomo_Nome1", AV19VariavelCocomo_Nome1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV12GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV20DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV14GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV12GridState.gxTpr_Dynamicfilters.Item(2));
               AV21DynamicFiltersSelector2 = AV14GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "VARIAVELCOCOMO_NOME") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV14GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV23VariavelCocomo_Nome2 = AV14GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23VariavelCocomo_Nome2", AV23VariavelCocomo_Nome2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV12GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV24DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
                  AV14GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV12GridState.gxTpr_Dynamicfilters.Item(3));
                  AV25DynamicFiltersSelector3 = AV14GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "VARIAVELCOCOMO_NOME") == 0 )
                  {
                     AV26DynamicFiltersOperator3 = AV14GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
                     AV27VariavelCocomo_Nome3 = AV14GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27VariavelCocomo_Nome3", AV27VariavelCocomo_Nome3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV28DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV12GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV12GridState.gxTpr_Orderedby = AV15OrderedBy;
         AV12GridState.gxTpr_Ordereddsc = AV16OrderedDsc;
         AV12GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV35TFVariavelCocomo_AreaTrabalhoCod) && (0==AV36TFVariavelCocomo_AreaTrabalhoCod_To) ) )
         {
            AV13GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV13GridStateFilterValue.gxTpr_Name = "TFVARIAVELCOCOMO_AREATRABALHOCOD";
            AV13GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV35TFVariavelCocomo_AreaTrabalhoCod), 6, 0);
            AV13GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV36TFVariavelCocomo_AreaTrabalhoCod_To), 6, 0);
            AV12GridState.gxTpr_Filtervalues.Add(AV13GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFVariavelCocomo_Sigla)) )
         {
            AV13GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV13GridStateFilterValue.gxTpr_Name = "TFVARIAVELCOCOMO_SIGLA";
            AV13GridStateFilterValue.gxTpr_Value = AV39TFVariavelCocomo_Sigla;
            AV12GridState.gxTpr_Filtervalues.Add(AV13GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFVariavelCocomo_Sigla_Sel)) )
         {
            AV13GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV13GridStateFilterValue.gxTpr_Name = "TFVARIAVELCOCOMO_SIGLA_SEL";
            AV13GridStateFilterValue.gxTpr_Value = AV40TFVariavelCocomo_Sigla_Sel;
            AV12GridState.gxTpr_Filtervalues.Add(AV13GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFVariavelCocomo_Nome)) )
         {
            AV13GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV13GridStateFilterValue.gxTpr_Name = "TFVARIAVELCOCOMO_NOME";
            AV13GridStateFilterValue.gxTpr_Value = AV43TFVariavelCocomo_Nome;
            AV12GridState.gxTpr_Filtervalues.Add(AV13GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFVariavelCocomo_Nome_Sel)) )
         {
            AV13GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV13GridStateFilterValue.gxTpr_Name = "TFVARIAVELCOCOMO_NOME_SEL";
            AV13GridStateFilterValue.gxTpr_Value = AV44TFVariavelCocomo_Nome_Sel;
            AV12GridState.gxTpr_Filtervalues.Add(AV13GridStateFilterValue, 0);
         }
         if ( ! ( AV48TFVariavelCocomo_Tipo_Sels.Count == 0 ) )
         {
            AV13GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV13GridStateFilterValue.gxTpr_Name = "TFVARIAVELCOCOMO_TIPO_SEL";
            AV13GridStateFilterValue.gxTpr_Value = AV48TFVariavelCocomo_Tipo_Sels.ToJSonString(false);
            AV12GridState.gxTpr_Filtervalues.Add(AV13GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV51TFVariavelCocomo_ProjetoCod) && (0==AV52TFVariavelCocomo_ProjetoCod_To) ) )
         {
            AV13GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV13GridStateFilterValue.gxTpr_Name = "TFVARIAVELCOCOMO_PROJETOCOD";
            AV13GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV51TFVariavelCocomo_ProjetoCod), 6, 0);
            AV13GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV52TFVariavelCocomo_ProjetoCod_To), 6, 0);
            AV12GridState.gxTpr_Filtervalues.Add(AV13GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV55TFVariavelCocomo_Data) && (DateTime.MinValue==AV56TFVariavelCocomo_Data_To) ) )
         {
            AV13GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV13GridStateFilterValue.gxTpr_Name = "TFVARIAVELCOCOMO_DATA";
            AV13GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV55TFVariavelCocomo_Data, 2, "/");
            AV13GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV56TFVariavelCocomo_Data_To, 2, "/");
            AV12GridState.gxTpr_Filtervalues.Add(AV13GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV61TFVariavelCocomo_ExtraBaixo) && (Convert.ToDecimal(0)==AV62TFVariavelCocomo_ExtraBaixo_To) ) )
         {
            AV13GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV13GridStateFilterValue.gxTpr_Name = "TFVARIAVELCOCOMO_EXTRABAIXO";
            AV13GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV61TFVariavelCocomo_ExtraBaixo, 12, 2);
            AV13GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV62TFVariavelCocomo_ExtraBaixo_To, 12, 2);
            AV12GridState.gxTpr_Filtervalues.Add(AV13GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV65TFVariavelCocomo_MuitoBaixo) && (Convert.ToDecimal(0)==AV66TFVariavelCocomo_MuitoBaixo_To) ) )
         {
            AV13GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV13GridStateFilterValue.gxTpr_Name = "TFVARIAVELCOCOMO_MUITOBAIXO";
            AV13GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV65TFVariavelCocomo_MuitoBaixo, 12, 2);
            AV13GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV66TFVariavelCocomo_MuitoBaixo_To, 12, 2);
            AV12GridState.gxTpr_Filtervalues.Add(AV13GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV69TFVariavelCocomo_Baixo) && (Convert.ToDecimal(0)==AV70TFVariavelCocomo_Baixo_To) ) )
         {
            AV13GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV13GridStateFilterValue.gxTpr_Name = "TFVARIAVELCOCOMO_BAIXO";
            AV13GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV69TFVariavelCocomo_Baixo, 12, 2);
            AV13GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV70TFVariavelCocomo_Baixo_To, 12, 2);
            AV12GridState.gxTpr_Filtervalues.Add(AV13GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV73TFVariavelCocomo_Nominal) && (Convert.ToDecimal(0)==AV74TFVariavelCocomo_Nominal_To) ) )
         {
            AV13GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV13GridStateFilterValue.gxTpr_Name = "TFVARIAVELCOCOMO_NOMINAL";
            AV13GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV73TFVariavelCocomo_Nominal, 12, 2);
            AV13GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV74TFVariavelCocomo_Nominal_To, 12, 2);
            AV12GridState.gxTpr_Filtervalues.Add(AV13GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV77TFVariavelCocomo_Alto) && (Convert.ToDecimal(0)==AV78TFVariavelCocomo_Alto_To) ) )
         {
            AV13GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV13GridStateFilterValue.gxTpr_Name = "TFVARIAVELCOCOMO_ALTO";
            AV13GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV77TFVariavelCocomo_Alto, 12, 2);
            AV13GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV78TFVariavelCocomo_Alto_To, 12, 2);
            AV12GridState.gxTpr_Filtervalues.Add(AV13GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV81TFVariavelCocomo_MuitoAlto) && (Convert.ToDecimal(0)==AV82TFVariavelCocomo_MuitoAlto_To) ) )
         {
            AV13GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV13GridStateFilterValue.gxTpr_Name = "TFVARIAVELCOCOMO_MUITOALTO";
            AV13GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV81TFVariavelCocomo_MuitoAlto, 12, 2);
            AV13GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV82TFVariavelCocomo_MuitoAlto_To, 12, 2);
            AV12GridState.gxTpr_Filtervalues.Add(AV13GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV85TFVariavelCocomo_ExtraAlto) && (Convert.ToDecimal(0)==AV86TFVariavelCocomo_ExtraAlto_To) ) )
         {
            AV13GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV13GridStateFilterValue.gxTpr_Name = "TFVARIAVELCOCOMO_EXTRAALTO";
            AV13GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV85TFVariavelCocomo_ExtraAlto, 12, 2);
            AV13GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV86TFVariavelCocomo_ExtraAlto_To, 12, 2);
            AV12GridState.gxTpr_Filtervalues.Add(AV13GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV89TFVariavelCocomo_Usado) && (Convert.ToDecimal(0)==AV90TFVariavelCocomo_Usado_To) ) )
         {
            AV13GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV13GridStateFilterValue.gxTpr_Name = "TFVARIAVELCOCOMO_USADO";
            AV13GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV89TFVariavelCocomo_Usado, 12, 2);
            AV13GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV90TFVariavelCocomo_Usado_To, 12, 2);
            AV12GridState.gxTpr_Filtervalues.Add(AV13GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV99Pgmname+"GridState",  AV12GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV12GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV29DynamicFiltersIgnoreFirst )
         {
            AV14GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV14GridStateDynamicFilter.gxTpr_Selected = AV17DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "VARIAVELCOCOMO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV19VariavelCocomo_Nome1)) )
            {
               AV14GridStateDynamicFilter.gxTpr_Value = AV19VariavelCocomo_Nome1;
               AV14GridStateDynamicFilter.gxTpr_Operator = AV18DynamicFiltersOperator1;
            }
            if ( AV28DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV14GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV12GridState.gxTpr_Dynamicfilters.Add(AV14GridStateDynamicFilter, 0);
            }
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            AV14GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV14GridStateDynamicFilter.gxTpr_Selected = AV21DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "VARIAVELCOCOMO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV23VariavelCocomo_Nome2)) )
            {
               AV14GridStateDynamicFilter.gxTpr_Value = AV23VariavelCocomo_Nome2;
               AV14GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            if ( AV28DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV14GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV12GridState.gxTpr_Dynamicfilters.Add(AV14GridStateDynamicFilter, 0);
            }
         }
         if ( AV24DynamicFiltersEnabled3 )
         {
            AV14GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV14GridStateDynamicFilter.gxTpr_Selected = AV25DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "VARIAVELCOCOMO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV27VariavelCocomo_Nome3)) )
            {
               AV14GridStateDynamicFilter.gxTpr_Value = AV27VariavelCocomo_Nome3;
               AV14GridStateDynamicFilter.gxTpr_Operator = AV26DynamicFiltersOperator3;
            }
            if ( AV28DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV14GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV12GridState.gxTpr_Dynamicfilters.Add(AV14GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_GL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_GL2( true) ;
         }
         else
         {
            wb_table2_5_GL2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_GL2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_74_GL2( true) ;
         }
         else
         {
            wb_table3_74_GL2( false) ;
         }
         return  ;
      }

      protected void wb_table3_74_GL2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_GL2e( true) ;
         }
         else
         {
            wb_table1_2_GL2e( false) ;
         }
      }

      protected void wb_table3_74_GL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_77_GL2( true) ;
         }
         else
         {
            wb_table4_77_GL2( false) ;
         }
         return  ;
      }

      protected void wb_table4_77_GL2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_74_GL2e( true) ;
         }
         else
         {
            wb_table3_74_GL2e( false) ;
         }
      }

      protected void wb_table4_77_GL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"80\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtVariavelCocomo_AreaTrabalhoCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtVariavelCocomo_AreaTrabalhoCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtVariavelCocomo_AreaTrabalhoCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtVariavelCocomo_Sigla_Titleformat == 0 )
               {
                  context.SendWebValue( edtVariavelCocomo_Sigla_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtVariavelCocomo_Sigla_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtVariavelCocomo_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtVariavelCocomo_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtVariavelCocomo_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbVariavelCocomo_Tipo_Titleformat == 0 )
               {
                  context.SendWebValue( cmbVariavelCocomo_Tipo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbVariavelCocomo_Tipo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtVariavelCocomo_ProjetoCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtVariavelCocomo_ProjetoCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtVariavelCocomo_ProjetoCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtVariavelCocomo_Data_Titleformat == 0 )
               {
                  context.SendWebValue( edtVariavelCocomo_Data_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtVariavelCocomo_Data_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(50), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtVariavelCocomo_ExtraBaixo_Titleformat == 0 )
               {
                  context.SendWebValue( edtVariavelCocomo_ExtraBaixo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtVariavelCocomo_ExtraBaixo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(118), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtVariavelCocomo_MuitoBaixo_Titleformat == 0 )
               {
                  context.SendWebValue( edtVariavelCocomo_MuitoBaixo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtVariavelCocomo_MuitoBaixo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(118), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtVariavelCocomo_Baixo_Titleformat == 0 )
               {
                  context.SendWebValue( edtVariavelCocomo_Baixo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtVariavelCocomo_Baixo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(118), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtVariavelCocomo_Nominal_Titleformat == 0 )
               {
                  context.SendWebValue( edtVariavelCocomo_Nominal_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtVariavelCocomo_Nominal_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(118), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtVariavelCocomo_Alto_Titleformat == 0 )
               {
                  context.SendWebValue( edtVariavelCocomo_Alto_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtVariavelCocomo_Alto_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(118), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtVariavelCocomo_MuitoAlto_Titleformat == 0 )
               {
                  context.SendWebValue( edtVariavelCocomo_MuitoAlto_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtVariavelCocomo_MuitoAlto_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(118), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtVariavelCocomo_ExtraAlto_Titleformat == 0 )
               {
                  context.SendWebValue( edtVariavelCocomo_ExtraAlto_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtVariavelCocomo_ExtraAlto_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(118), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtVariavelCocomo_Usado_Titleformat == 0 )
               {
                  context.SendWebValue( edtVariavelCocomo_Usado_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtVariavelCocomo_Usado_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV30Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A961VariavelCocomo_AreaTrabalhoCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtVariavelCocomo_AreaTrabalhoCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtVariavelCocomo_AreaTrabalhoCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A962VariavelCocomo_Sigla));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtVariavelCocomo_Sigla_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtVariavelCocomo_Sigla_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A963VariavelCocomo_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtVariavelCocomo_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtVariavelCocomo_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A964VariavelCocomo_Tipo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbVariavelCocomo_Tipo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbVariavelCocomo_Tipo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A965VariavelCocomo_ProjetoCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtVariavelCocomo_ProjetoCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtVariavelCocomo_ProjetoCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A966VariavelCocomo_Data, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtVariavelCocomo_Data_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtVariavelCocomo_Data_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A986VariavelCocomo_ExtraBaixo, 18, 2, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtVariavelCocomo_ExtraBaixo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtVariavelCocomo_ExtraBaixo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A967VariavelCocomo_MuitoBaixo, 18, 2, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtVariavelCocomo_MuitoBaixo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtVariavelCocomo_MuitoBaixo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A968VariavelCocomo_Baixo, 18, 2, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtVariavelCocomo_Baixo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtVariavelCocomo_Baixo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A969VariavelCocomo_Nominal, 18, 2, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtVariavelCocomo_Nominal_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtVariavelCocomo_Nominal_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A970VariavelCocomo_Alto, 18, 2, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtVariavelCocomo_Alto_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtVariavelCocomo_Alto_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A971VariavelCocomo_MuitoAlto, 18, 2, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtVariavelCocomo_MuitoAlto_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtVariavelCocomo_MuitoAlto_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A972VariavelCocomo_ExtraAlto, 18, 2, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtVariavelCocomo_ExtraAlto_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtVariavelCocomo_ExtraAlto_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A973VariavelCocomo_Usado, 18, 2, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtVariavelCocomo_Usado_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtVariavelCocomo_Usado_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 80 )
         {
            wbEnd = 0;
            nRC_GXsfl_80 = (short)(nGXsfl_80_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_77_GL2e( true) ;
         }
         else
         {
            wb_table4_77_GL2e( false) ;
         }
      }

      protected void wb_table2_5_GL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptVariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptVariaveisCocomo.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV16OrderedDsc), StringUtil.BoolToStr( AV16OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptVariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_GL2( true) ;
         }
         else
         {
            wb_table5_14_GL2( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_GL2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_GL2e( true) ;
         }
         else
         {
            wb_table2_5_GL2e( false) ;
         }
      }

      protected void wb_table5_14_GL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptVariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_19_GL2( true) ;
         }
         else
         {
            wb_table6_19_GL2( false) ;
         }
         return  ;
      }

      protected void wb_table6_19_GL2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptVariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_GL2e( true) ;
         }
         else
         {
            wb_table5_14_GL2e( false) ;
         }
      }

      protected void wb_table6_19_GL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptVariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV17DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_PromptVariaveisCocomo.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV17DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptVariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_GL2( true) ;
         }
         else
         {
            wb_table7_28_GL2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_GL2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptVariaveisCocomo.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptVariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptVariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV21DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,41);\"", "", true, "HLP_PromptVariaveisCocomo.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptVariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_45_GL2( true) ;
         }
         else
         {
            wb_table8_45_GL2( false) ;
         }
         return  ;
      }

      protected void wb_table8_45_GL2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptVariaveisCocomo.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptVariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptVariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV25DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,58);\"", "", true, "HLP_PromptVariaveisCocomo.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptVariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_62_GL2( true) ;
         }
         else
         {
            wb_table9_62_GL2( false) ;
         }
         return  ;
      }

      protected void wb_table9_62_GL2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptVariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_19_GL2e( true) ;
         }
         else
         {
            wb_table6_19_GL2e( false) ;
         }
      }

      protected void wb_table9_62_GL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,65);\"", "", true, "HLP_PromptVariaveisCocomo.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavVariavelcocomo_nome3_Internalname, StringUtil.RTrim( AV27VariavelCocomo_Nome3), StringUtil.RTrim( context.localUtil.Format( AV27VariavelCocomo_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,67);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavVariavelcocomo_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavVariavelcocomo_nome3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptVariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_62_GL2e( true) ;
         }
         else
         {
            wb_table9_62_GL2e( false) ;
         }
      }

      protected void wb_table8_45_GL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,48);\"", "", true, "HLP_PromptVariaveisCocomo.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavVariavelcocomo_nome2_Internalname, StringUtil.RTrim( AV23VariavelCocomo_Nome2), StringUtil.RTrim( context.localUtil.Format( AV23VariavelCocomo_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavVariavelcocomo_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavVariavelcocomo_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptVariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_45_GL2e( true) ;
         }
         else
         {
            wb_table8_45_GL2e( false) ;
         }
      }

      protected void wb_table7_28_GL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV18DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_PromptVariaveisCocomo.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV18DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavVariavelcocomo_nome1_Internalname, StringUtil.RTrim( AV19VariavelCocomo_Nome1), StringUtil.RTrim( context.localUtil.Format( AV19VariavelCocomo_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavVariavelcocomo_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavVariavelcocomo_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptVariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_GL2e( true) ;
         }
         else
         {
            wb_table7_28_GL2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutVariavelCocomo_AreaTrabalhoCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutVariavelCocomo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutVariavelCocomo_AreaTrabalhoCod), 6, 0)));
         AV8InOutVariavelCocomo_Sigla = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutVariavelCocomo_Sigla", AV8InOutVariavelCocomo_Sigla);
         AV9InOutVariavelCocomo_Tipo = (String)getParm(obj,2);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9InOutVariavelCocomo_Tipo", AV9InOutVariavelCocomo_Tipo);
         AV33InOutVariavelCocomo_Sequencial = Convert.ToInt16(getParm(obj,3));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33InOutVariavelCocomo_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33InOutVariavelCocomo_Sequencial), 3, 0)));
         AV10InOutVariavelCocomo_Nome = (String)getParm(obj,4);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10InOutVariavelCocomo_Nome", AV10InOutVariavelCocomo_Nome);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAGL2( ) ;
         WSGL2( ) ;
         WEGL2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203118553324");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptvariaveiscocomo.js", "?20203118553325");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_802( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_80_idx;
         edtVariavelCocomo_AreaTrabalhoCod_Internalname = "VARIAVELCOCOMO_AREATRABALHOCOD_"+sGXsfl_80_idx;
         edtVariavelCocomo_Sigla_Internalname = "VARIAVELCOCOMO_SIGLA_"+sGXsfl_80_idx;
         edtVariavelCocomo_Nome_Internalname = "VARIAVELCOCOMO_NOME_"+sGXsfl_80_idx;
         cmbVariavelCocomo_Tipo_Internalname = "VARIAVELCOCOMO_TIPO_"+sGXsfl_80_idx;
         edtVariavelCocomo_ProjetoCod_Internalname = "VARIAVELCOCOMO_PROJETOCOD_"+sGXsfl_80_idx;
         edtVariavelCocomo_Data_Internalname = "VARIAVELCOCOMO_DATA_"+sGXsfl_80_idx;
         edtVariavelCocomo_ExtraBaixo_Internalname = "VARIAVELCOCOMO_EXTRABAIXO_"+sGXsfl_80_idx;
         edtVariavelCocomo_MuitoBaixo_Internalname = "VARIAVELCOCOMO_MUITOBAIXO_"+sGXsfl_80_idx;
         edtVariavelCocomo_Baixo_Internalname = "VARIAVELCOCOMO_BAIXO_"+sGXsfl_80_idx;
         edtVariavelCocomo_Nominal_Internalname = "VARIAVELCOCOMO_NOMINAL_"+sGXsfl_80_idx;
         edtVariavelCocomo_Alto_Internalname = "VARIAVELCOCOMO_ALTO_"+sGXsfl_80_idx;
         edtVariavelCocomo_MuitoAlto_Internalname = "VARIAVELCOCOMO_MUITOALTO_"+sGXsfl_80_idx;
         edtVariavelCocomo_ExtraAlto_Internalname = "VARIAVELCOCOMO_EXTRAALTO_"+sGXsfl_80_idx;
         edtVariavelCocomo_Usado_Internalname = "VARIAVELCOCOMO_USADO_"+sGXsfl_80_idx;
      }

      protected void SubsflControlProps_fel_802( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_80_fel_idx;
         edtVariavelCocomo_AreaTrabalhoCod_Internalname = "VARIAVELCOCOMO_AREATRABALHOCOD_"+sGXsfl_80_fel_idx;
         edtVariavelCocomo_Sigla_Internalname = "VARIAVELCOCOMO_SIGLA_"+sGXsfl_80_fel_idx;
         edtVariavelCocomo_Nome_Internalname = "VARIAVELCOCOMO_NOME_"+sGXsfl_80_fel_idx;
         cmbVariavelCocomo_Tipo_Internalname = "VARIAVELCOCOMO_TIPO_"+sGXsfl_80_fel_idx;
         edtVariavelCocomo_ProjetoCod_Internalname = "VARIAVELCOCOMO_PROJETOCOD_"+sGXsfl_80_fel_idx;
         edtVariavelCocomo_Data_Internalname = "VARIAVELCOCOMO_DATA_"+sGXsfl_80_fel_idx;
         edtVariavelCocomo_ExtraBaixo_Internalname = "VARIAVELCOCOMO_EXTRABAIXO_"+sGXsfl_80_fel_idx;
         edtVariavelCocomo_MuitoBaixo_Internalname = "VARIAVELCOCOMO_MUITOBAIXO_"+sGXsfl_80_fel_idx;
         edtVariavelCocomo_Baixo_Internalname = "VARIAVELCOCOMO_BAIXO_"+sGXsfl_80_fel_idx;
         edtVariavelCocomo_Nominal_Internalname = "VARIAVELCOCOMO_NOMINAL_"+sGXsfl_80_fel_idx;
         edtVariavelCocomo_Alto_Internalname = "VARIAVELCOCOMO_ALTO_"+sGXsfl_80_fel_idx;
         edtVariavelCocomo_MuitoAlto_Internalname = "VARIAVELCOCOMO_MUITOALTO_"+sGXsfl_80_fel_idx;
         edtVariavelCocomo_ExtraAlto_Internalname = "VARIAVELCOCOMO_EXTRAALTO_"+sGXsfl_80_fel_idx;
         edtVariavelCocomo_Usado_Internalname = "VARIAVELCOCOMO_USADO_"+sGXsfl_80_fel_idx;
      }

      protected void sendrow_802( )
      {
         SubsflControlProps_802( ) ;
         WBGL0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_80_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_80_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_80_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 81,'',false,'',80)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV30Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV30Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV98Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV30Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV30Select)) ? AV98Select_GXI : context.PathToRelativeUrl( AV30Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_80_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV30Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtVariavelCocomo_AreaTrabalhoCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A961VariavelCocomo_AreaTrabalhoCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A961VariavelCocomo_AreaTrabalhoCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtVariavelCocomo_AreaTrabalhoCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtVariavelCocomo_Sigla_Internalname,StringUtil.RTrim( A962VariavelCocomo_Sigla),StringUtil.RTrim( context.localUtil.Format( A962VariavelCocomo_Sigla, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtVariavelCocomo_Sigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtVariavelCocomo_Nome_Internalname,StringUtil.RTrim( A963VariavelCocomo_Nome),StringUtil.RTrim( context.localUtil.Format( A963VariavelCocomo_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtVariavelCocomo_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_80_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "VARIAVELCOCOMO_TIPO_" + sGXsfl_80_idx;
               cmbVariavelCocomo_Tipo.Name = GXCCtl;
               cmbVariavelCocomo_Tipo.WebTags = "";
               cmbVariavelCocomo_Tipo.addItem("", "(Selecionar)", 0);
               cmbVariavelCocomo_Tipo.addItem("E", "Escala", 0);
               cmbVariavelCocomo_Tipo.addItem("M", "Multiplicador", 0);
               if ( cmbVariavelCocomo_Tipo.ItemCount > 0 )
               {
                  A964VariavelCocomo_Tipo = cmbVariavelCocomo_Tipo.getValidValue(A964VariavelCocomo_Tipo);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbVariavelCocomo_Tipo,(String)cmbVariavelCocomo_Tipo_Internalname,StringUtil.RTrim( A964VariavelCocomo_Tipo),(short)1,(String)cmbVariavelCocomo_Tipo_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbVariavelCocomo_Tipo.CurrentValue = StringUtil.RTrim( A964VariavelCocomo_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbVariavelCocomo_Tipo_Internalname, "Values", (String)(cmbVariavelCocomo_Tipo.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtVariavelCocomo_ProjetoCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A965VariavelCocomo_ProjetoCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A965VariavelCocomo_ProjetoCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtVariavelCocomo_ProjetoCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtVariavelCocomo_Data_Internalname,context.localUtil.Format(A966VariavelCocomo_Data, "99/99/99"),context.localUtil.Format( A966VariavelCocomo_Data, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtVariavelCocomo_Data_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Data",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtVariavelCocomo_ExtraBaixo_Internalname,StringUtil.LTrim( StringUtil.NToC( A986VariavelCocomo_ExtraBaixo, 18, 2, ",", "")),context.localUtil.Format( A986VariavelCocomo_ExtraBaixo, "ZZ,ZZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtVariavelCocomo_ExtraBaixo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)50,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor2Casas",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtVariavelCocomo_MuitoBaixo_Internalname,StringUtil.LTrim( StringUtil.NToC( A967VariavelCocomo_MuitoBaixo, 18, 2, ",", "")),context.localUtil.Format( A967VariavelCocomo_MuitoBaixo, "ZZ,ZZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtVariavelCocomo_MuitoBaixo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)118,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor2Casas",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtVariavelCocomo_Baixo_Internalname,StringUtil.LTrim( StringUtil.NToC( A968VariavelCocomo_Baixo, 18, 2, ",", "")),context.localUtil.Format( A968VariavelCocomo_Baixo, "ZZ,ZZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtVariavelCocomo_Baixo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)118,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor2Casas",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtVariavelCocomo_Nominal_Internalname,StringUtil.LTrim( StringUtil.NToC( A969VariavelCocomo_Nominal, 18, 2, ",", "")),context.localUtil.Format( A969VariavelCocomo_Nominal, "ZZ,ZZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtVariavelCocomo_Nominal_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)118,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor2Casas",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtVariavelCocomo_Alto_Internalname,StringUtil.LTrim( StringUtil.NToC( A970VariavelCocomo_Alto, 18, 2, ",", "")),context.localUtil.Format( A970VariavelCocomo_Alto, "ZZ,ZZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtVariavelCocomo_Alto_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)118,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor2Casas",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtVariavelCocomo_MuitoAlto_Internalname,StringUtil.LTrim( StringUtil.NToC( A971VariavelCocomo_MuitoAlto, 18, 2, ",", "")),context.localUtil.Format( A971VariavelCocomo_MuitoAlto, "ZZ,ZZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtVariavelCocomo_MuitoAlto_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)118,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor2Casas",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtVariavelCocomo_ExtraAlto_Internalname,StringUtil.LTrim( StringUtil.NToC( A972VariavelCocomo_ExtraAlto, 18, 2, ",", "")),context.localUtil.Format( A972VariavelCocomo_ExtraAlto, "ZZ,ZZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtVariavelCocomo_ExtraAlto_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)118,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor2Casas",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtVariavelCocomo_Usado_Internalname,StringUtil.LTrim( StringUtil.NToC( A973VariavelCocomo_Usado, 18, 2, ",", "")),context.localUtil.Format( A973VariavelCocomo_Usado, "ZZ,ZZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtVariavelCocomo_Usado_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)118,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor2Casas",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_AREATRABALHOCOD"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( (decimal)(A961VariavelCocomo_AreaTrabalhoCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_SIGLA"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, StringUtil.RTrim( context.localUtil.Format( A962VariavelCocomo_Sigla, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_NOME"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, StringUtil.RTrim( context.localUtil.Format( A963VariavelCocomo_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_TIPO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, StringUtil.RTrim( context.localUtil.Format( A964VariavelCocomo_Tipo, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_PROJETOCOD"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( (decimal)(A965VariavelCocomo_ProjetoCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_DATA"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, A966VariavelCocomo_Data));
            GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_EXTRABAIXO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( A986VariavelCocomo_ExtraBaixo, "ZZ,ZZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_MUITOBAIXO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( A967VariavelCocomo_MuitoBaixo, "ZZ,ZZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_BAIXO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( A968VariavelCocomo_Baixo, "ZZ,ZZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_NOMINAL"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( A969VariavelCocomo_Nominal, "ZZ,ZZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_ALTO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( A970VariavelCocomo_Alto, "ZZ,ZZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_MUITOALTO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( A971VariavelCocomo_MuitoAlto, "ZZ,ZZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_EXTRAALTO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( A972VariavelCocomo_ExtraAlto, "ZZ,ZZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_USADO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( A973VariavelCocomo_Usado, "ZZ,ZZZ,ZZZ,ZZ9.999")));
            GridContainer.AddRow(GridRow);
            nGXsfl_80_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_80_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_80_idx+1));
            sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
            SubsflControlProps_802( ) ;
         }
         /* End function sendrow_802 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavVariavelcocomo_nome1_Internalname = "vVARIAVELCOCOMO_NOME1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavVariavelcocomo_nome2_Internalname = "vVARIAVELCOCOMO_NOME2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavVariavelcocomo_nome3_Internalname = "vVARIAVELCOCOMO_NOME3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtVariavelCocomo_AreaTrabalhoCod_Internalname = "VARIAVELCOCOMO_AREATRABALHOCOD";
         edtVariavelCocomo_Sigla_Internalname = "VARIAVELCOCOMO_SIGLA";
         edtVariavelCocomo_Nome_Internalname = "VARIAVELCOCOMO_NOME";
         cmbVariavelCocomo_Tipo_Internalname = "VARIAVELCOCOMO_TIPO";
         edtVariavelCocomo_ProjetoCod_Internalname = "VARIAVELCOCOMO_PROJETOCOD";
         edtVariavelCocomo_Data_Internalname = "VARIAVELCOCOMO_DATA";
         edtVariavelCocomo_ExtraBaixo_Internalname = "VARIAVELCOCOMO_EXTRABAIXO";
         edtVariavelCocomo_MuitoBaixo_Internalname = "VARIAVELCOCOMO_MUITOBAIXO";
         edtVariavelCocomo_Baixo_Internalname = "VARIAVELCOCOMO_BAIXO";
         edtVariavelCocomo_Nominal_Internalname = "VARIAVELCOCOMO_NOMINAL";
         edtVariavelCocomo_Alto_Internalname = "VARIAVELCOCOMO_ALTO";
         edtVariavelCocomo_MuitoAlto_Internalname = "VARIAVELCOCOMO_MUITOALTO";
         edtVariavelCocomo_ExtraAlto_Internalname = "VARIAVELCOCOMO_EXTRAALTO";
         edtVariavelCocomo_Usado_Internalname = "VARIAVELCOCOMO_USADO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         edtVariavelCocomo_Sequencial_Internalname = "VARIAVELCOCOMO_SEQUENCIAL";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfvariavelcocomo_areatrabalhocod_Internalname = "vTFVARIAVELCOCOMO_AREATRABALHOCOD";
         edtavTfvariavelcocomo_areatrabalhocod_to_Internalname = "vTFVARIAVELCOCOMO_AREATRABALHOCOD_TO";
         edtavTfvariavelcocomo_sigla_Internalname = "vTFVARIAVELCOCOMO_SIGLA";
         edtavTfvariavelcocomo_sigla_sel_Internalname = "vTFVARIAVELCOCOMO_SIGLA_SEL";
         edtavTfvariavelcocomo_nome_Internalname = "vTFVARIAVELCOCOMO_NOME";
         edtavTfvariavelcocomo_nome_sel_Internalname = "vTFVARIAVELCOCOMO_NOME_SEL";
         edtavTfvariavelcocomo_projetocod_Internalname = "vTFVARIAVELCOCOMO_PROJETOCOD";
         edtavTfvariavelcocomo_projetocod_to_Internalname = "vTFVARIAVELCOCOMO_PROJETOCOD_TO";
         edtavTfvariavelcocomo_data_Internalname = "vTFVARIAVELCOCOMO_DATA";
         edtavTfvariavelcocomo_data_to_Internalname = "vTFVARIAVELCOCOMO_DATA_TO";
         edtavDdo_variavelcocomo_dataauxdate_Internalname = "vDDO_VARIAVELCOCOMO_DATAAUXDATE";
         edtavDdo_variavelcocomo_dataauxdateto_Internalname = "vDDO_VARIAVELCOCOMO_DATAAUXDATETO";
         divDdo_variavelcocomo_dataauxdates_Internalname = "DDO_VARIAVELCOCOMO_DATAAUXDATES";
         edtavTfvariavelcocomo_extrabaixo_Internalname = "vTFVARIAVELCOCOMO_EXTRABAIXO";
         edtavTfvariavelcocomo_extrabaixo_to_Internalname = "vTFVARIAVELCOCOMO_EXTRABAIXO_TO";
         edtavTfvariavelcocomo_muitobaixo_Internalname = "vTFVARIAVELCOCOMO_MUITOBAIXO";
         edtavTfvariavelcocomo_muitobaixo_to_Internalname = "vTFVARIAVELCOCOMO_MUITOBAIXO_TO";
         edtavTfvariavelcocomo_baixo_Internalname = "vTFVARIAVELCOCOMO_BAIXO";
         edtavTfvariavelcocomo_baixo_to_Internalname = "vTFVARIAVELCOCOMO_BAIXO_TO";
         edtavTfvariavelcocomo_nominal_Internalname = "vTFVARIAVELCOCOMO_NOMINAL";
         edtavTfvariavelcocomo_nominal_to_Internalname = "vTFVARIAVELCOCOMO_NOMINAL_TO";
         edtavTfvariavelcocomo_alto_Internalname = "vTFVARIAVELCOCOMO_ALTO";
         edtavTfvariavelcocomo_alto_to_Internalname = "vTFVARIAVELCOCOMO_ALTO_TO";
         edtavTfvariavelcocomo_muitoalto_Internalname = "vTFVARIAVELCOCOMO_MUITOALTO";
         edtavTfvariavelcocomo_muitoalto_to_Internalname = "vTFVARIAVELCOCOMO_MUITOALTO_TO";
         edtavTfvariavelcocomo_extraalto_Internalname = "vTFVARIAVELCOCOMO_EXTRAALTO";
         edtavTfvariavelcocomo_extraalto_to_Internalname = "vTFVARIAVELCOCOMO_EXTRAALTO_TO";
         edtavTfvariavelcocomo_usado_Internalname = "vTFVARIAVELCOCOMO_USADO";
         edtavTfvariavelcocomo_usado_to_Internalname = "vTFVARIAVELCOCOMO_USADO_TO";
         Ddo_variavelcocomo_areatrabalhocod_Internalname = "DDO_VARIAVELCOCOMO_AREATRABALHOCOD";
         edtavDdo_variavelcocomo_areatrabalhocodtitlecontrolidtoreplace_Internalname = "vDDO_VARIAVELCOCOMO_AREATRABALHOCODTITLECONTROLIDTOREPLACE";
         Ddo_variavelcocomo_sigla_Internalname = "DDO_VARIAVELCOCOMO_SIGLA";
         edtavDdo_variavelcocomo_siglatitlecontrolidtoreplace_Internalname = "vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE";
         Ddo_variavelcocomo_nome_Internalname = "DDO_VARIAVELCOCOMO_NOME";
         edtavDdo_variavelcocomo_nometitlecontrolidtoreplace_Internalname = "vDDO_VARIAVELCOCOMO_NOMETITLECONTROLIDTOREPLACE";
         Ddo_variavelcocomo_tipo_Internalname = "DDO_VARIAVELCOCOMO_TIPO";
         edtavDdo_variavelcocomo_tipotitlecontrolidtoreplace_Internalname = "vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE";
         Ddo_variavelcocomo_projetocod_Internalname = "DDO_VARIAVELCOCOMO_PROJETOCOD";
         edtavDdo_variavelcocomo_projetocodtitlecontrolidtoreplace_Internalname = "vDDO_VARIAVELCOCOMO_PROJETOCODTITLECONTROLIDTOREPLACE";
         Ddo_variavelcocomo_data_Internalname = "DDO_VARIAVELCOCOMO_DATA";
         edtavDdo_variavelcocomo_datatitlecontrolidtoreplace_Internalname = "vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE";
         Ddo_variavelcocomo_extrabaixo_Internalname = "DDO_VARIAVELCOCOMO_EXTRABAIXO";
         edtavDdo_variavelcocomo_extrabaixotitlecontrolidtoreplace_Internalname = "vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE";
         Ddo_variavelcocomo_muitobaixo_Internalname = "DDO_VARIAVELCOCOMO_MUITOBAIXO";
         edtavDdo_variavelcocomo_muitobaixotitlecontrolidtoreplace_Internalname = "vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE";
         Ddo_variavelcocomo_baixo_Internalname = "DDO_VARIAVELCOCOMO_BAIXO";
         edtavDdo_variavelcocomo_baixotitlecontrolidtoreplace_Internalname = "vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE";
         Ddo_variavelcocomo_nominal_Internalname = "DDO_VARIAVELCOCOMO_NOMINAL";
         edtavDdo_variavelcocomo_nominaltitlecontrolidtoreplace_Internalname = "vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE";
         Ddo_variavelcocomo_alto_Internalname = "DDO_VARIAVELCOCOMO_ALTO";
         edtavDdo_variavelcocomo_altotitlecontrolidtoreplace_Internalname = "vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE";
         Ddo_variavelcocomo_muitoalto_Internalname = "DDO_VARIAVELCOCOMO_MUITOALTO";
         edtavDdo_variavelcocomo_muitoaltotitlecontrolidtoreplace_Internalname = "vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE";
         Ddo_variavelcocomo_extraalto_Internalname = "DDO_VARIAVELCOCOMO_EXTRAALTO";
         edtavDdo_variavelcocomo_extraaltotitlecontrolidtoreplace_Internalname = "vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE";
         Ddo_variavelcocomo_usado_Internalname = "DDO_VARIAVELCOCOMO_USADO";
         edtavDdo_variavelcocomo_usadotitlecontrolidtoreplace_Internalname = "vDDO_VARIAVELCOCOMO_USADOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtVariavelCocomo_Usado_Jsonclick = "";
         edtVariavelCocomo_ExtraAlto_Jsonclick = "";
         edtVariavelCocomo_MuitoAlto_Jsonclick = "";
         edtVariavelCocomo_Alto_Jsonclick = "";
         edtVariavelCocomo_Nominal_Jsonclick = "";
         edtVariavelCocomo_Baixo_Jsonclick = "";
         edtVariavelCocomo_MuitoBaixo_Jsonclick = "";
         edtVariavelCocomo_ExtraBaixo_Jsonclick = "";
         edtVariavelCocomo_Data_Jsonclick = "";
         edtVariavelCocomo_ProjetoCod_Jsonclick = "";
         cmbVariavelCocomo_Tipo_Jsonclick = "";
         edtVariavelCocomo_Nome_Jsonclick = "";
         edtVariavelCocomo_Sigla_Jsonclick = "";
         edtVariavelCocomo_AreaTrabalhoCod_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavVariavelcocomo_nome1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavVariavelcocomo_nome2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavVariavelcocomo_nome3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         edtVariavelCocomo_Usado_Titleformat = 0;
         edtVariavelCocomo_ExtraAlto_Titleformat = 0;
         edtVariavelCocomo_MuitoAlto_Titleformat = 0;
         edtVariavelCocomo_Alto_Titleformat = 0;
         edtVariavelCocomo_Nominal_Titleformat = 0;
         edtVariavelCocomo_Baixo_Titleformat = 0;
         edtVariavelCocomo_MuitoBaixo_Titleformat = 0;
         edtVariavelCocomo_ExtraBaixo_Titleformat = 0;
         edtVariavelCocomo_Data_Titleformat = 0;
         edtVariavelCocomo_ProjetoCod_Titleformat = 0;
         cmbVariavelCocomo_Tipo_Titleformat = 0;
         edtVariavelCocomo_Nome_Titleformat = 0;
         edtVariavelCocomo_Sigla_Titleformat = 0;
         edtVariavelCocomo_AreaTrabalhoCod_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavVariavelcocomo_nome3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavVariavelcocomo_nome2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavVariavelcocomo_nome1_Visible = 1;
         edtVariavelCocomo_Usado_Title = "usado";
         edtVariavelCocomo_ExtraAlto_Title = "Alto";
         edtVariavelCocomo_MuitoAlto_Title = "Alto";
         edtVariavelCocomo_Alto_Title = "Alto";
         edtVariavelCocomo_Nominal_Title = "Nominal";
         edtVariavelCocomo_Baixo_Title = "Baixo";
         edtVariavelCocomo_MuitoBaixo_Title = "Baixo";
         edtVariavelCocomo_ExtraBaixo_Title = "Extra Baixo";
         edtVariavelCocomo_Data_Title = "Data";
         edtVariavelCocomo_ProjetoCod_Title = "Sistema";
         cmbVariavelCocomo_Tipo.Title.Text = "Tipo";
         edtVariavelCocomo_Nome_Title = "Nome";
         edtVariavelCocomo_Sigla_Title = "Sigla";
         edtVariavelCocomo_AreaTrabalhoCod_Title = "de Trabalho";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_variavelcocomo_usadotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_variavelcocomo_extraaltotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_variavelcocomo_muitoaltotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_variavelcocomo_altotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_variavelcocomo_nominaltitlecontrolidtoreplace_Visible = 1;
         edtavDdo_variavelcocomo_baixotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_variavelcocomo_muitobaixotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_variavelcocomo_extrabaixotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_variavelcocomo_datatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_variavelcocomo_projetocodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_variavelcocomo_tipotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_variavelcocomo_nometitlecontrolidtoreplace_Visible = 1;
         edtavDdo_variavelcocomo_siglatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_variavelcocomo_areatrabalhocodtitlecontrolidtoreplace_Visible = 1;
         edtavTfvariavelcocomo_usado_to_Jsonclick = "";
         edtavTfvariavelcocomo_usado_to_Visible = 1;
         edtavTfvariavelcocomo_usado_Jsonclick = "";
         edtavTfvariavelcocomo_usado_Visible = 1;
         edtavTfvariavelcocomo_extraalto_to_Jsonclick = "";
         edtavTfvariavelcocomo_extraalto_to_Visible = 1;
         edtavTfvariavelcocomo_extraalto_Jsonclick = "";
         edtavTfvariavelcocomo_extraalto_Visible = 1;
         edtavTfvariavelcocomo_muitoalto_to_Jsonclick = "";
         edtavTfvariavelcocomo_muitoalto_to_Visible = 1;
         edtavTfvariavelcocomo_muitoalto_Jsonclick = "";
         edtavTfvariavelcocomo_muitoalto_Visible = 1;
         edtavTfvariavelcocomo_alto_to_Jsonclick = "";
         edtavTfvariavelcocomo_alto_to_Visible = 1;
         edtavTfvariavelcocomo_alto_Jsonclick = "";
         edtavTfvariavelcocomo_alto_Visible = 1;
         edtavTfvariavelcocomo_nominal_to_Jsonclick = "";
         edtavTfvariavelcocomo_nominal_to_Visible = 1;
         edtavTfvariavelcocomo_nominal_Jsonclick = "";
         edtavTfvariavelcocomo_nominal_Visible = 1;
         edtavTfvariavelcocomo_baixo_to_Jsonclick = "";
         edtavTfvariavelcocomo_baixo_to_Visible = 1;
         edtavTfvariavelcocomo_baixo_Jsonclick = "";
         edtavTfvariavelcocomo_baixo_Visible = 1;
         edtavTfvariavelcocomo_muitobaixo_to_Jsonclick = "";
         edtavTfvariavelcocomo_muitobaixo_to_Visible = 1;
         edtavTfvariavelcocomo_muitobaixo_Jsonclick = "";
         edtavTfvariavelcocomo_muitobaixo_Visible = 1;
         edtavTfvariavelcocomo_extrabaixo_to_Jsonclick = "";
         edtavTfvariavelcocomo_extrabaixo_to_Visible = 1;
         edtavTfvariavelcocomo_extrabaixo_Jsonclick = "";
         edtavTfvariavelcocomo_extrabaixo_Visible = 1;
         edtavDdo_variavelcocomo_dataauxdateto_Jsonclick = "";
         edtavDdo_variavelcocomo_dataauxdate_Jsonclick = "";
         edtavTfvariavelcocomo_data_to_Jsonclick = "";
         edtavTfvariavelcocomo_data_to_Visible = 1;
         edtavTfvariavelcocomo_data_Jsonclick = "";
         edtavTfvariavelcocomo_data_Visible = 1;
         edtavTfvariavelcocomo_projetocod_to_Jsonclick = "";
         edtavTfvariavelcocomo_projetocod_to_Visible = 1;
         edtavTfvariavelcocomo_projetocod_Jsonclick = "";
         edtavTfvariavelcocomo_projetocod_Visible = 1;
         edtavTfvariavelcocomo_nome_sel_Jsonclick = "";
         edtavTfvariavelcocomo_nome_sel_Visible = 1;
         edtavTfvariavelcocomo_nome_Jsonclick = "";
         edtavTfvariavelcocomo_nome_Visible = 1;
         edtavTfvariavelcocomo_sigla_sel_Jsonclick = "";
         edtavTfvariavelcocomo_sigla_sel_Visible = 1;
         edtavTfvariavelcocomo_sigla_Jsonclick = "";
         edtavTfvariavelcocomo_sigla_Visible = 1;
         edtavTfvariavelcocomo_areatrabalhocod_to_Jsonclick = "";
         edtavTfvariavelcocomo_areatrabalhocod_to_Visible = 1;
         edtavTfvariavelcocomo_areatrabalhocod_Jsonclick = "";
         edtavTfvariavelcocomo_areatrabalhocod_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         edtVariavelCocomo_Sequencial_Jsonclick = "";
         edtVariavelCocomo_Sequencial_Visible = 1;
         Ddo_variavelcocomo_usado_Searchbuttontext = "Pesquisar";
         Ddo_variavelcocomo_usado_Rangefilterto = "At�";
         Ddo_variavelcocomo_usado_Rangefilterfrom = "Desde";
         Ddo_variavelcocomo_usado_Cleanfilter = "Limpar pesquisa";
         Ddo_variavelcocomo_usado_Sortdsc = "Ordenar de Z � A";
         Ddo_variavelcocomo_usado_Sortasc = "Ordenar de A � Z";
         Ddo_variavelcocomo_usado_Includedatalist = Convert.ToBoolean( 0);
         Ddo_variavelcocomo_usado_Filterisrange = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_usado_Filtertype = "Numeric";
         Ddo_variavelcocomo_usado_Includefilter = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_usado_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_usado_Includesortasc = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_usado_Titlecontrolidtoreplace = "";
         Ddo_variavelcocomo_usado_Dropdownoptionstype = "GridTitleSettings";
         Ddo_variavelcocomo_usado_Cls = "ColumnSettings";
         Ddo_variavelcocomo_usado_Tooltip = "Op��es";
         Ddo_variavelcocomo_usado_Caption = "";
         Ddo_variavelcocomo_extraalto_Searchbuttontext = "Pesquisar";
         Ddo_variavelcocomo_extraalto_Rangefilterto = "At�";
         Ddo_variavelcocomo_extraalto_Rangefilterfrom = "Desde";
         Ddo_variavelcocomo_extraalto_Cleanfilter = "Limpar pesquisa";
         Ddo_variavelcocomo_extraalto_Sortdsc = "Ordenar de Z � A";
         Ddo_variavelcocomo_extraalto_Sortasc = "Ordenar de A � Z";
         Ddo_variavelcocomo_extraalto_Includedatalist = Convert.ToBoolean( 0);
         Ddo_variavelcocomo_extraalto_Filterisrange = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_extraalto_Filtertype = "Numeric";
         Ddo_variavelcocomo_extraalto_Includefilter = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_extraalto_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_extraalto_Includesortasc = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_extraalto_Titlecontrolidtoreplace = "";
         Ddo_variavelcocomo_extraalto_Dropdownoptionstype = "GridTitleSettings";
         Ddo_variavelcocomo_extraalto_Cls = "ColumnSettings";
         Ddo_variavelcocomo_extraalto_Tooltip = "Op��es";
         Ddo_variavelcocomo_extraalto_Caption = "";
         Ddo_variavelcocomo_muitoalto_Searchbuttontext = "Pesquisar";
         Ddo_variavelcocomo_muitoalto_Rangefilterto = "At�";
         Ddo_variavelcocomo_muitoalto_Rangefilterfrom = "Desde";
         Ddo_variavelcocomo_muitoalto_Cleanfilter = "Limpar pesquisa";
         Ddo_variavelcocomo_muitoalto_Sortdsc = "Ordenar de Z � A";
         Ddo_variavelcocomo_muitoalto_Sortasc = "Ordenar de A � Z";
         Ddo_variavelcocomo_muitoalto_Includedatalist = Convert.ToBoolean( 0);
         Ddo_variavelcocomo_muitoalto_Filterisrange = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_muitoalto_Filtertype = "Numeric";
         Ddo_variavelcocomo_muitoalto_Includefilter = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_muitoalto_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_muitoalto_Includesortasc = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_muitoalto_Titlecontrolidtoreplace = "";
         Ddo_variavelcocomo_muitoalto_Dropdownoptionstype = "GridTitleSettings";
         Ddo_variavelcocomo_muitoalto_Cls = "ColumnSettings";
         Ddo_variavelcocomo_muitoalto_Tooltip = "Op��es";
         Ddo_variavelcocomo_muitoalto_Caption = "";
         Ddo_variavelcocomo_alto_Searchbuttontext = "Pesquisar";
         Ddo_variavelcocomo_alto_Rangefilterto = "At�";
         Ddo_variavelcocomo_alto_Rangefilterfrom = "Desde";
         Ddo_variavelcocomo_alto_Cleanfilter = "Limpar pesquisa";
         Ddo_variavelcocomo_alto_Sortdsc = "Ordenar de Z � A";
         Ddo_variavelcocomo_alto_Sortasc = "Ordenar de A � Z";
         Ddo_variavelcocomo_alto_Includedatalist = Convert.ToBoolean( 0);
         Ddo_variavelcocomo_alto_Filterisrange = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_alto_Filtertype = "Numeric";
         Ddo_variavelcocomo_alto_Includefilter = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_alto_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_alto_Includesortasc = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_alto_Titlecontrolidtoreplace = "";
         Ddo_variavelcocomo_alto_Dropdownoptionstype = "GridTitleSettings";
         Ddo_variavelcocomo_alto_Cls = "ColumnSettings";
         Ddo_variavelcocomo_alto_Tooltip = "Op��es";
         Ddo_variavelcocomo_alto_Caption = "";
         Ddo_variavelcocomo_nominal_Searchbuttontext = "Pesquisar";
         Ddo_variavelcocomo_nominal_Rangefilterto = "At�";
         Ddo_variavelcocomo_nominal_Rangefilterfrom = "Desde";
         Ddo_variavelcocomo_nominal_Cleanfilter = "Limpar pesquisa";
         Ddo_variavelcocomo_nominal_Sortdsc = "Ordenar de Z � A";
         Ddo_variavelcocomo_nominal_Sortasc = "Ordenar de A � Z";
         Ddo_variavelcocomo_nominal_Includedatalist = Convert.ToBoolean( 0);
         Ddo_variavelcocomo_nominal_Filterisrange = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_nominal_Filtertype = "Numeric";
         Ddo_variavelcocomo_nominal_Includefilter = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_nominal_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_nominal_Includesortasc = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_nominal_Titlecontrolidtoreplace = "";
         Ddo_variavelcocomo_nominal_Dropdownoptionstype = "GridTitleSettings";
         Ddo_variavelcocomo_nominal_Cls = "ColumnSettings";
         Ddo_variavelcocomo_nominal_Tooltip = "Op��es";
         Ddo_variavelcocomo_nominal_Caption = "";
         Ddo_variavelcocomo_baixo_Searchbuttontext = "Pesquisar";
         Ddo_variavelcocomo_baixo_Rangefilterto = "At�";
         Ddo_variavelcocomo_baixo_Rangefilterfrom = "Desde";
         Ddo_variavelcocomo_baixo_Cleanfilter = "Limpar pesquisa";
         Ddo_variavelcocomo_baixo_Sortdsc = "Ordenar de Z � A";
         Ddo_variavelcocomo_baixo_Sortasc = "Ordenar de A � Z";
         Ddo_variavelcocomo_baixo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_variavelcocomo_baixo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_baixo_Filtertype = "Numeric";
         Ddo_variavelcocomo_baixo_Includefilter = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_baixo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_baixo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_baixo_Titlecontrolidtoreplace = "";
         Ddo_variavelcocomo_baixo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_variavelcocomo_baixo_Cls = "ColumnSettings";
         Ddo_variavelcocomo_baixo_Tooltip = "Op��es";
         Ddo_variavelcocomo_baixo_Caption = "";
         Ddo_variavelcocomo_muitobaixo_Searchbuttontext = "Pesquisar";
         Ddo_variavelcocomo_muitobaixo_Rangefilterto = "At�";
         Ddo_variavelcocomo_muitobaixo_Rangefilterfrom = "Desde";
         Ddo_variavelcocomo_muitobaixo_Cleanfilter = "Limpar pesquisa";
         Ddo_variavelcocomo_muitobaixo_Sortdsc = "Ordenar de Z � A";
         Ddo_variavelcocomo_muitobaixo_Sortasc = "Ordenar de A � Z";
         Ddo_variavelcocomo_muitobaixo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_variavelcocomo_muitobaixo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_muitobaixo_Filtertype = "Numeric";
         Ddo_variavelcocomo_muitobaixo_Includefilter = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_muitobaixo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_muitobaixo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_muitobaixo_Titlecontrolidtoreplace = "";
         Ddo_variavelcocomo_muitobaixo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_variavelcocomo_muitobaixo_Cls = "ColumnSettings";
         Ddo_variavelcocomo_muitobaixo_Tooltip = "Op��es";
         Ddo_variavelcocomo_muitobaixo_Caption = "";
         Ddo_variavelcocomo_extrabaixo_Searchbuttontext = "Pesquisar";
         Ddo_variavelcocomo_extrabaixo_Rangefilterto = "At�";
         Ddo_variavelcocomo_extrabaixo_Rangefilterfrom = "Desde";
         Ddo_variavelcocomo_extrabaixo_Cleanfilter = "Limpar pesquisa";
         Ddo_variavelcocomo_extrabaixo_Sortdsc = "Ordenar de Z � A";
         Ddo_variavelcocomo_extrabaixo_Sortasc = "Ordenar de A � Z";
         Ddo_variavelcocomo_extrabaixo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_variavelcocomo_extrabaixo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_extrabaixo_Filtertype = "Numeric";
         Ddo_variavelcocomo_extrabaixo_Includefilter = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_extrabaixo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_extrabaixo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_extrabaixo_Titlecontrolidtoreplace = "";
         Ddo_variavelcocomo_extrabaixo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_variavelcocomo_extrabaixo_Cls = "ColumnSettings";
         Ddo_variavelcocomo_extrabaixo_Tooltip = "Op��es";
         Ddo_variavelcocomo_extrabaixo_Caption = "";
         Ddo_variavelcocomo_data_Searchbuttontext = "Pesquisar";
         Ddo_variavelcocomo_data_Rangefilterto = "At�";
         Ddo_variavelcocomo_data_Rangefilterfrom = "Desde";
         Ddo_variavelcocomo_data_Cleanfilter = "Limpar pesquisa";
         Ddo_variavelcocomo_data_Sortdsc = "Ordenar de Z � A";
         Ddo_variavelcocomo_data_Sortasc = "Ordenar de A � Z";
         Ddo_variavelcocomo_data_Includedatalist = Convert.ToBoolean( 0);
         Ddo_variavelcocomo_data_Filterisrange = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_data_Filtertype = "Date";
         Ddo_variavelcocomo_data_Includefilter = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_data_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_data_Includesortasc = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_data_Titlecontrolidtoreplace = "";
         Ddo_variavelcocomo_data_Dropdownoptionstype = "GridTitleSettings";
         Ddo_variavelcocomo_data_Cls = "ColumnSettings";
         Ddo_variavelcocomo_data_Tooltip = "Op��es";
         Ddo_variavelcocomo_data_Caption = "";
         Ddo_variavelcocomo_projetocod_Searchbuttontext = "Pesquisar";
         Ddo_variavelcocomo_projetocod_Rangefilterto = "At�";
         Ddo_variavelcocomo_projetocod_Rangefilterfrom = "Desde";
         Ddo_variavelcocomo_projetocod_Cleanfilter = "Limpar pesquisa";
         Ddo_variavelcocomo_projetocod_Sortdsc = "Ordenar de Z � A";
         Ddo_variavelcocomo_projetocod_Sortasc = "Ordenar de A � Z";
         Ddo_variavelcocomo_projetocod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_variavelcocomo_projetocod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_projetocod_Filtertype = "Numeric";
         Ddo_variavelcocomo_projetocod_Includefilter = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_projetocod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_projetocod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_projetocod_Titlecontrolidtoreplace = "";
         Ddo_variavelcocomo_projetocod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_variavelcocomo_projetocod_Cls = "ColumnSettings";
         Ddo_variavelcocomo_projetocod_Tooltip = "Op��es";
         Ddo_variavelcocomo_projetocod_Caption = "";
         Ddo_variavelcocomo_tipo_Searchbuttontext = "Filtrar Selecionados";
         Ddo_variavelcocomo_tipo_Cleanfilter = "Limpar pesquisa";
         Ddo_variavelcocomo_tipo_Sortdsc = "Ordenar de Z � A";
         Ddo_variavelcocomo_tipo_Sortasc = "Ordenar de A � Z";
         Ddo_variavelcocomo_tipo_Datalistfixedvalues = "E:Escala,M:Multiplicador";
         Ddo_variavelcocomo_tipo_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_tipo_Datalisttype = "FixedValues";
         Ddo_variavelcocomo_tipo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_tipo_Includefilter = Convert.ToBoolean( 0);
         Ddo_variavelcocomo_tipo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_tipo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_tipo_Titlecontrolidtoreplace = "";
         Ddo_variavelcocomo_tipo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_variavelcocomo_tipo_Cls = "ColumnSettings";
         Ddo_variavelcocomo_tipo_Tooltip = "Op��es";
         Ddo_variavelcocomo_tipo_Caption = "";
         Ddo_variavelcocomo_nome_Searchbuttontext = "Pesquisar";
         Ddo_variavelcocomo_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_variavelcocomo_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_variavelcocomo_nome_Loadingdata = "Carregando dados...";
         Ddo_variavelcocomo_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_variavelcocomo_nome_Sortasc = "Ordenar de A � Z";
         Ddo_variavelcocomo_nome_Datalistupdateminimumcharacters = 0;
         Ddo_variavelcocomo_nome_Datalistproc = "GetPromptVariaveisCocomoFilterData";
         Ddo_variavelcocomo_nome_Datalisttype = "Dynamic";
         Ddo_variavelcocomo_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_variavelcocomo_nome_Filtertype = "Character";
         Ddo_variavelcocomo_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_nome_Titlecontrolidtoreplace = "";
         Ddo_variavelcocomo_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_variavelcocomo_nome_Cls = "ColumnSettings";
         Ddo_variavelcocomo_nome_Tooltip = "Op��es";
         Ddo_variavelcocomo_nome_Caption = "";
         Ddo_variavelcocomo_sigla_Searchbuttontext = "Pesquisar";
         Ddo_variavelcocomo_sigla_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_variavelcocomo_sigla_Cleanfilter = "Limpar pesquisa";
         Ddo_variavelcocomo_sigla_Loadingdata = "Carregando dados...";
         Ddo_variavelcocomo_sigla_Sortdsc = "Ordenar de Z � A";
         Ddo_variavelcocomo_sigla_Sortasc = "Ordenar de A � Z";
         Ddo_variavelcocomo_sigla_Datalistupdateminimumcharacters = 0;
         Ddo_variavelcocomo_sigla_Datalistproc = "GetPromptVariaveisCocomoFilterData";
         Ddo_variavelcocomo_sigla_Datalisttype = "Dynamic";
         Ddo_variavelcocomo_sigla_Includedatalist = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_sigla_Filterisrange = Convert.ToBoolean( 0);
         Ddo_variavelcocomo_sigla_Filtertype = "Character";
         Ddo_variavelcocomo_sigla_Includefilter = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_sigla_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_sigla_Includesortasc = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_sigla_Titlecontrolidtoreplace = "";
         Ddo_variavelcocomo_sigla_Dropdownoptionstype = "GridTitleSettings";
         Ddo_variavelcocomo_sigla_Cls = "ColumnSettings";
         Ddo_variavelcocomo_sigla_Tooltip = "Op��es";
         Ddo_variavelcocomo_sigla_Caption = "";
         Ddo_variavelcocomo_areatrabalhocod_Searchbuttontext = "Pesquisar";
         Ddo_variavelcocomo_areatrabalhocod_Rangefilterto = "At�";
         Ddo_variavelcocomo_areatrabalhocod_Rangefilterfrom = "Desde";
         Ddo_variavelcocomo_areatrabalhocod_Cleanfilter = "Limpar pesquisa";
         Ddo_variavelcocomo_areatrabalhocod_Sortdsc = "Ordenar de Z � A";
         Ddo_variavelcocomo_areatrabalhocod_Sortasc = "Ordenar de A � Z";
         Ddo_variavelcocomo_areatrabalhocod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_variavelcocomo_areatrabalhocod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_areatrabalhocod_Filtertype = "Numeric";
         Ddo_variavelcocomo_areatrabalhocod_Includefilter = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_areatrabalhocod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_areatrabalhocod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_areatrabalhocod_Titlecontrolidtoreplace = "";
         Ddo_variavelcocomo_areatrabalhocod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_variavelcocomo_areatrabalhocod_Cls = "ColumnSettings";
         Ddo_variavelcocomo_areatrabalhocod_Tooltip = "Op��es";
         Ddo_variavelcocomo_areatrabalhocod_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Variaveis Cocomo";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_PROJETOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_USADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV16OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV35TFVariavelCocomo_AreaTrabalhoCod',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFVariavelCocomo_AreaTrabalhoCod_To',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV40TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV43TFVariavelCocomo_Nome',fld:'vTFVARIAVELCOCOMO_NOME',pic:'@!',nv:''},{av:'AV44TFVariavelCocomo_Nome_Sel',fld:'vTFVARIAVELCOCOMO_NOME_SEL',pic:'@!',nv:''},{av:'AV48TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV51TFVariavelCocomo_ProjetoCod',fld:'vTFVARIAVELCOCOMO_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFVariavelCocomo_ProjetoCod_To',fld:'vTFVARIAVELCOCOMO_PROJETOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV56TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV61TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV62TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV65TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV69TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV70TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV73TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV74TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV77TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV78TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV81TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV82TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV85TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV86TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV89TFVariavelCocomo_Usado',fld:'vTFVARIAVELCOCOMO_USADO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV90TFVariavelCocomo_Usado_To',fld:'vTFVARIAVELCOCOMO_USADO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV99Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV12GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19VariavelCocomo_Nome1',fld:'vVARIAVELCOCOMO_NOME1',pic:'@!',nv:''},{av:'AV18DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23VariavelCocomo_Nome2',fld:'vVARIAVELCOCOMO_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27VariavelCocomo_Nome3',fld:'vVARIAVELCOCOMO_NOME3',pic:'@!',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0}],oparms:[{av:'AV34VariavelCocomo_AreaTrabalhoCodTitleFilterData',fld:'vVARIAVELCOCOMO_AREATRABALHOCODTITLEFILTERDATA',pic:'',nv:null},{av:'AV38VariavelCocomo_SiglaTitleFilterData',fld:'vVARIAVELCOCOMO_SIGLATITLEFILTERDATA',pic:'',nv:null},{av:'AV42VariavelCocomo_NomeTitleFilterData',fld:'vVARIAVELCOCOMO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV46VariavelCocomo_TipoTitleFilterData',fld:'vVARIAVELCOCOMO_TIPOTITLEFILTERDATA',pic:'',nv:null},{av:'AV50VariavelCocomo_ProjetoCodTitleFilterData',fld:'vVARIAVELCOCOMO_PROJETOCODTITLEFILTERDATA',pic:'',nv:null},{av:'AV54VariavelCocomo_DataTitleFilterData',fld:'vVARIAVELCOCOMO_DATATITLEFILTERDATA',pic:'',nv:null},{av:'AV60VariavelCocomo_ExtraBaixoTitleFilterData',fld:'vVARIAVELCOCOMO_EXTRABAIXOTITLEFILTERDATA',pic:'',nv:null},{av:'AV64VariavelCocomo_MuitoBaixoTitleFilterData',fld:'vVARIAVELCOCOMO_MUITOBAIXOTITLEFILTERDATA',pic:'',nv:null},{av:'AV68VariavelCocomo_BaixoTitleFilterData',fld:'vVARIAVELCOCOMO_BAIXOTITLEFILTERDATA',pic:'',nv:null},{av:'AV72VariavelCocomo_NominalTitleFilterData',fld:'vVARIAVELCOCOMO_NOMINALTITLEFILTERDATA',pic:'',nv:null},{av:'AV76VariavelCocomo_AltoTitleFilterData',fld:'vVARIAVELCOCOMO_ALTOTITLEFILTERDATA',pic:'',nv:null},{av:'AV80VariavelCocomo_MuitoAltoTitleFilterData',fld:'vVARIAVELCOCOMO_MUITOALTOTITLEFILTERDATA',pic:'',nv:null},{av:'AV84VariavelCocomo_ExtraAltoTitleFilterData',fld:'vVARIAVELCOCOMO_EXTRAALTOTITLEFILTERDATA',pic:'',nv:null},{av:'AV88VariavelCocomo_UsadoTitleFilterData',fld:'vVARIAVELCOCOMO_USADOTITLEFILTERDATA',pic:'',nv:null},{av:'edtVariavelCocomo_AreaTrabalhoCod_Titleformat',ctrl:'VARIAVELCOCOMO_AREATRABALHOCOD',prop:'Titleformat'},{av:'edtVariavelCocomo_AreaTrabalhoCod_Title',ctrl:'VARIAVELCOCOMO_AREATRABALHOCOD',prop:'Title'},{av:'edtVariavelCocomo_Sigla_Titleformat',ctrl:'VARIAVELCOCOMO_SIGLA',prop:'Titleformat'},{av:'edtVariavelCocomo_Sigla_Title',ctrl:'VARIAVELCOCOMO_SIGLA',prop:'Title'},{av:'edtVariavelCocomo_Nome_Titleformat',ctrl:'VARIAVELCOCOMO_NOME',prop:'Titleformat'},{av:'edtVariavelCocomo_Nome_Title',ctrl:'VARIAVELCOCOMO_NOME',prop:'Title'},{av:'cmbVariavelCocomo_Tipo'},{av:'edtVariavelCocomo_ProjetoCod_Titleformat',ctrl:'VARIAVELCOCOMO_PROJETOCOD',prop:'Titleformat'},{av:'edtVariavelCocomo_ProjetoCod_Title',ctrl:'VARIAVELCOCOMO_PROJETOCOD',prop:'Title'},{av:'edtVariavelCocomo_Data_Titleformat',ctrl:'VARIAVELCOCOMO_DATA',prop:'Titleformat'},{av:'edtVariavelCocomo_Data_Title',ctrl:'VARIAVELCOCOMO_DATA',prop:'Title'},{av:'edtVariavelCocomo_ExtraBaixo_Titleformat',ctrl:'VARIAVELCOCOMO_EXTRABAIXO',prop:'Titleformat'},{av:'edtVariavelCocomo_ExtraBaixo_Title',ctrl:'VARIAVELCOCOMO_EXTRABAIXO',prop:'Title'},{av:'edtVariavelCocomo_MuitoBaixo_Titleformat',ctrl:'VARIAVELCOCOMO_MUITOBAIXO',prop:'Titleformat'},{av:'edtVariavelCocomo_MuitoBaixo_Title',ctrl:'VARIAVELCOCOMO_MUITOBAIXO',prop:'Title'},{av:'edtVariavelCocomo_Baixo_Titleformat',ctrl:'VARIAVELCOCOMO_BAIXO',prop:'Titleformat'},{av:'edtVariavelCocomo_Baixo_Title',ctrl:'VARIAVELCOCOMO_BAIXO',prop:'Title'},{av:'edtVariavelCocomo_Nominal_Titleformat',ctrl:'VARIAVELCOCOMO_NOMINAL',prop:'Titleformat'},{av:'edtVariavelCocomo_Nominal_Title',ctrl:'VARIAVELCOCOMO_NOMINAL',prop:'Title'},{av:'edtVariavelCocomo_Alto_Titleformat',ctrl:'VARIAVELCOCOMO_ALTO',prop:'Titleformat'},{av:'edtVariavelCocomo_Alto_Title',ctrl:'VARIAVELCOCOMO_ALTO',prop:'Title'},{av:'edtVariavelCocomo_MuitoAlto_Titleformat',ctrl:'VARIAVELCOCOMO_MUITOALTO',prop:'Titleformat'},{av:'edtVariavelCocomo_MuitoAlto_Title',ctrl:'VARIAVELCOCOMO_MUITOALTO',prop:'Title'},{av:'edtVariavelCocomo_ExtraAlto_Titleformat',ctrl:'VARIAVELCOCOMO_EXTRAALTO',prop:'Titleformat'},{av:'edtVariavelCocomo_ExtraAlto_Title',ctrl:'VARIAVELCOCOMO_EXTRAALTO',prop:'Title'},{av:'edtVariavelCocomo_Usado_Titleformat',ctrl:'VARIAVELCOCOMO_USADO',prop:'Titleformat'},{av:'edtVariavelCocomo_Usado_Title',ctrl:'VARIAVELCOCOMO_USADO',prop:'Title'},{av:'AV94GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV95GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV12GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11GL2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV16OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19VariavelCocomo_Nome1',fld:'vVARIAVELCOCOMO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23VariavelCocomo_Nome2',fld:'vVARIAVELCOCOMO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27VariavelCocomo_Nome3',fld:'vVARIAVELCOCOMO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFVariavelCocomo_AreaTrabalhoCod',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFVariavelCocomo_AreaTrabalhoCod_To',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV40TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV43TFVariavelCocomo_Nome',fld:'vTFVARIAVELCOCOMO_NOME',pic:'@!',nv:''},{av:'AV44TFVariavelCocomo_Nome_Sel',fld:'vTFVARIAVELCOCOMO_NOME_SEL',pic:'@!',nv:''},{av:'AV51TFVariavelCocomo_ProjetoCod',fld:'vTFVARIAVELCOCOMO_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFVariavelCocomo_ProjetoCod_To',fld:'vTFVARIAVELCOCOMO_PROJETOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV56TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV61TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV62TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV65TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV69TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV70TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV73TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV74TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV77TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV78TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV81TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV82TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV85TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV86TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV89TFVariavelCocomo_Usado',fld:'vTFVARIAVELCOCOMO_USADO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV90TFVariavelCocomo_Usado_To',fld:'vTFVARIAVELCOCOMO_USADO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_PROJETOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_USADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV99Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV12GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_VARIAVELCOCOMO_AREATRABALHOCOD.ONOPTIONCLICKED","{handler:'E12GL2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV16OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19VariavelCocomo_Nome1',fld:'vVARIAVELCOCOMO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23VariavelCocomo_Nome2',fld:'vVARIAVELCOCOMO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27VariavelCocomo_Nome3',fld:'vVARIAVELCOCOMO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFVariavelCocomo_AreaTrabalhoCod',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFVariavelCocomo_AreaTrabalhoCod_To',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV40TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV43TFVariavelCocomo_Nome',fld:'vTFVARIAVELCOCOMO_NOME',pic:'@!',nv:''},{av:'AV44TFVariavelCocomo_Nome_Sel',fld:'vTFVARIAVELCOCOMO_NOME_SEL',pic:'@!',nv:''},{av:'AV51TFVariavelCocomo_ProjetoCod',fld:'vTFVARIAVELCOCOMO_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFVariavelCocomo_ProjetoCod_To',fld:'vTFVARIAVELCOCOMO_PROJETOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV56TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV61TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV62TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV65TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV69TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV70TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV73TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV74TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV77TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV78TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV81TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV82TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV85TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV86TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV89TFVariavelCocomo_Usado',fld:'vTFVARIAVELCOCOMO_USADO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV90TFVariavelCocomo_Usado_To',fld:'vTFVARIAVELCOCOMO_USADO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_PROJETOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_USADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV99Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV12GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_variavelcocomo_areatrabalhocod_Activeeventkey',ctrl:'DDO_VARIAVELCOCOMO_AREATRABALHOCOD',prop:'ActiveEventKey'},{av:'Ddo_variavelcocomo_areatrabalhocod_Filteredtext_get',ctrl:'DDO_VARIAVELCOCOMO_AREATRABALHOCOD',prop:'FilteredText_get'},{av:'Ddo_variavelcocomo_areatrabalhocod_Filteredtextto_get',ctrl:'DDO_VARIAVELCOCOMO_AREATRABALHOCOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV16OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_variavelcocomo_areatrabalhocod_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_AREATRABALHOCOD',prop:'SortedStatus'},{av:'AV35TFVariavelCocomo_AreaTrabalhoCod',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFVariavelCocomo_AreaTrabalhoCod_To',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_variavelcocomo_sigla_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_SIGLA',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_nome_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_NOME',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_tipo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_TIPO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_projetocod_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_PROJETOCOD',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_data_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_DATA',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_extrabaixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_EXTRABAIXO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_muitobaixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_MUITOBAIXO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_baixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_BAIXO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_nominal_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_NOMINAL',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_alto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_ALTO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_muitoalto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_MUITOALTO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_extraalto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_EXTRAALTO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_usado_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_USADO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_VARIAVELCOCOMO_SIGLA.ONOPTIONCLICKED","{handler:'E13GL2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV16OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19VariavelCocomo_Nome1',fld:'vVARIAVELCOCOMO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23VariavelCocomo_Nome2',fld:'vVARIAVELCOCOMO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27VariavelCocomo_Nome3',fld:'vVARIAVELCOCOMO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFVariavelCocomo_AreaTrabalhoCod',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFVariavelCocomo_AreaTrabalhoCod_To',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV40TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV43TFVariavelCocomo_Nome',fld:'vTFVARIAVELCOCOMO_NOME',pic:'@!',nv:''},{av:'AV44TFVariavelCocomo_Nome_Sel',fld:'vTFVARIAVELCOCOMO_NOME_SEL',pic:'@!',nv:''},{av:'AV51TFVariavelCocomo_ProjetoCod',fld:'vTFVARIAVELCOCOMO_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFVariavelCocomo_ProjetoCod_To',fld:'vTFVARIAVELCOCOMO_PROJETOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV56TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV61TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV62TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV65TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV69TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV70TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV73TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV74TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV77TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV78TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV81TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV82TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV85TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV86TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV89TFVariavelCocomo_Usado',fld:'vTFVARIAVELCOCOMO_USADO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV90TFVariavelCocomo_Usado_To',fld:'vTFVARIAVELCOCOMO_USADO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_PROJETOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_USADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV99Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV12GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_variavelcocomo_sigla_Activeeventkey',ctrl:'DDO_VARIAVELCOCOMO_SIGLA',prop:'ActiveEventKey'},{av:'Ddo_variavelcocomo_sigla_Filteredtext_get',ctrl:'DDO_VARIAVELCOCOMO_SIGLA',prop:'FilteredText_get'},{av:'Ddo_variavelcocomo_sigla_Selectedvalue_get',ctrl:'DDO_VARIAVELCOCOMO_SIGLA',prop:'SelectedValue_get'}],oparms:[{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV16OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_variavelcocomo_sigla_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_SIGLA',prop:'SortedStatus'},{av:'AV39TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV40TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_variavelcocomo_areatrabalhocod_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_AREATRABALHOCOD',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_nome_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_NOME',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_tipo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_TIPO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_projetocod_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_PROJETOCOD',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_data_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_DATA',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_extrabaixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_EXTRABAIXO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_muitobaixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_MUITOBAIXO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_baixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_BAIXO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_nominal_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_NOMINAL',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_alto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_ALTO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_muitoalto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_MUITOALTO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_extraalto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_EXTRAALTO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_usado_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_USADO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_VARIAVELCOCOMO_NOME.ONOPTIONCLICKED","{handler:'E14GL2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV16OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19VariavelCocomo_Nome1',fld:'vVARIAVELCOCOMO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23VariavelCocomo_Nome2',fld:'vVARIAVELCOCOMO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27VariavelCocomo_Nome3',fld:'vVARIAVELCOCOMO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFVariavelCocomo_AreaTrabalhoCod',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFVariavelCocomo_AreaTrabalhoCod_To',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV40TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV43TFVariavelCocomo_Nome',fld:'vTFVARIAVELCOCOMO_NOME',pic:'@!',nv:''},{av:'AV44TFVariavelCocomo_Nome_Sel',fld:'vTFVARIAVELCOCOMO_NOME_SEL',pic:'@!',nv:''},{av:'AV51TFVariavelCocomo_ProjetoCod',fld:'vTFVARIAVELCOCOMO_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFVariavelCocomo_ProjetoCod_To',fld:'vTFVARIAVELCOCOMO_PROJETOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV56TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV61TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV62TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV65TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV69TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV70TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV73TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV74TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV77TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV78TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV81TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV82TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV85TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV86TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV89TFVariavelCocomo_Usado',fld:'vTFVARIAVELCOCOMO_USADO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV90TFVariavelCocomo_Usado_To',fld:'vTFVARIAVELCOCOMO_USADO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_PROJETOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_USADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV99Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV12GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_variavelcocomo_nome_Activeeventkey',ctrl:'DDO_VARIAVELCOCOMO_NOME',prop:'ActiveEventKey'},{av:'Ddo_variavelcocomo_nome_Filteredtext_get',ctrl:'DDO_VARIAVELCOCOMO_NOME',prop:'FilteredText_get'},{av:'Ddo_variavelcocomo_nome_Selectedvalue_get',ctrl:'DDO_VARIAVELCOCOMO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV16OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_variavelcocomo_nome_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_NOME',prop:'SortedStatus'},{av:'AV43TFVariavelCocomo_Nome',fld:'vTFVARIAVELCOCOMO_NOME',pic:'@!',nv:''},{av:'AV44TFVariavelCocomo_Nome_Sel',fld:'vTFVARIAVELCOCOMO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_variavelcocomo_areatrabalhocod_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_AREATRABALHOCOD',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_sigla_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_SIGLA',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_tipo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_TIPO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_projetocod_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_PROJETOCOD',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_data_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_DATA',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_extrabaixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_EXTRABAIXO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_muitobaixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_MUITOBAIXO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_baixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_BAIXO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_nominal_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_NOMINAL',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_alto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_ALTO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_muitoalto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_MUITOALTO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_extraalto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_EXTRAALTO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_usado_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_USADO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_VARIAVELCOCOMO_TIPO.ONOPTIONCLICKED","{handler:'E15GL2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV16OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19VariavelCocomo_Nome1',fld:'vVARIAVELCOCOMO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23VariavelCocomo_Nome2',fld:'vVARIAVELCOCOMO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27VariavelCocomo_Nome3',fld:'vVARIAVELCOCOMO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFVariavelCocomo_AreaTrabalhoCod',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFVariavelCocomo_AreaTrabalhoCod_To',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV40TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV43TFVariavelCocomo_Nome',fld:'vTFVARIAVELCOCOMO_NOME',pic:'@!',nv:''},{av:'AV44TFVariavelCocomo_Nome_Sel',fld:'vTFVARIAVELCOCOMO_NOME_SEL',pic:'@!',nv:''},{av:'AV51TFVariavelCocomo_ProjetoCod',fld:'vTFVARIAVELCOCOMO_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFVariavelCocomo_ProjetoCod_To',fld:'vTFVARIAVELCOCOMO_PROJETOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV56TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV61TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV62TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV65TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV69TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV70TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV73TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV74TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV77TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV78TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV81TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV82TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV85TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV86TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV89TFVariavelCocomo_Usado',fld:'vTFVARIAVELCOCOMO_USADO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV90TFVariavelCocomo_Usado_To',fld:'vTFVARIAVELCOCOMO_USADO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_PROJETOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_USADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV99Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV12GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_variavelcocomo_tipo_Activeeventkey',ctrl:'DDO_VARIAVELCOCOMO_TIPO',prop:'ActiveEventKey'},{av:'Ddo_variavelcocomo_tipo_Selectedvalue_get',ctrl:'DDO_VARIAVELCOCOMO_TIPO',prop:'SelectedValue_get'}],oparms:[{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV16OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_variavelcocomo_tipo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_TIPO',prop:'SortedStatus'},{av:'AV48TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'Ddo_variavelcocomo_areatrabalhocod_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_AREATRABALHOCOD',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_sigla_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_SIGLA',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_nome_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_NOME',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_projetocod_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_PROJETOCOD',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_data_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_DATA',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_extrabaixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_EXTRABAIXO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_muitobaixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_MUITOBAIXO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_baixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_BAIXO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_nominal_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_NOMINAL',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_alto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_ALTO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_muitoalto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_MUITOALTO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_extraalto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_EXTRAALTO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_usado_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_USADO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_VARIAVELCOCOMO_PROJETOCOD.ONOPTIONCLICKED","{handler:'E16GL2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV16OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19VariavelCocomo_Nome1',fld:'vVARIAVELCOCOMO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23VariavelCocomo_Nome2',fld:'vVARIAVELCOCOMO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27VariavelCocomo_Nome3',fld:'vVARIAVELCOCOMO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFVariavelCocomo_AreaTrabalhoCod',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFVariavelCocomo_AreaTrabalhoCod_To',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV40TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV43TFVariavelCocomo_Nome',fld:'vTFVARIAVELCOCOMO_NOME',pic:'@!',nv:''},{av:'AV44TFVariavelCocomo_Nome_Sel',fld:'vTFVARIAVELCOCOMO_NOME_SEL',pic:'@!',nv:''},{av:'AV51TFVariavelCocomo_ProjetoCod',fld:'vTFVARIAVELCOCOMO_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFVariavelCocomo_ProjetoCod_To',fld:'vTFVARIAVELCOCOMO_PROJETOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV56TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV61TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV62TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV65TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV69TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV70TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV73TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV74TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV77TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV78TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV81TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV82TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV85TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV86TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV89TFVariavelCocomo_Usado',fld:'vTFVARIAVELCOCOMO_USADO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV90TFVariavelCocomo_Usado_To',fld:'vTFVARIAVELCOCOMO_USADO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_PROJETOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_USADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV99Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV12GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_variavelcocomo_projetocod_Activeeventkey',ctrl:'DDO_VARIAVELCOCOMO_PROJETOCOD',prop:'ActiveEventKey'},{av:'Ddo_variavelcocomo_projetocod_Filteredtext_get',ctrl:'DDO_VARIAVELCOCOMO_PROJETOCOD',prop:'FilteredText_get'},{av:'Ddo_variavelcocomo_projetocod_Filteredtextto_get',ctrl:'DDO_VARIAVELCOCOMO_PROJETOCOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV16OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_variavelcocomo_projetocod_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_PROJETOCOD',prop:'SortedStatus'},{av:'AV51TFVariavelCocomo_ProjetoCod',fld:'vTFVARIAVELCOCOMO_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFVariavelCocomo_ProjetoCod_To',fld:'vTFVARIAVELCOCOMO_PROJETOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_variavelcocomo_areatrabalhocod_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_AREATRABALHOCOD',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_sigla_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_SIGLA',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_nome_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_NOME',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_tipo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_TIPO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_data_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_DATA',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_extrabaixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_EXTRABAIXO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_muitobaixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_MUITOBAIXO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_baixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_BAIXO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_nominal_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_NOMINAL',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_alto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_ALTO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_muitoalto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_MUITOALTO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_extraalto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_EXTRAALTO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_usado_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_USADO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_VARIAVELCOCOMO_DATA.ONOPTIONCLICKED","{handler:'E17GL2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV16OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19VariavelCocomo_Nome1',fld:'vVARIAVELCOCOMO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23VariavelCocomo_Nome2',fld:'vVARIAVELCOCOMO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27VariavelCocomo_Nome3',fld:'vVARIAVELCOCOMO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFVariavelCocomo_AreaTrabalhoCod',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFVariavelCocomo_AreaTrabalhoCod_To',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV40TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV43TFVariavelCocomo_Nome',fld:'vTFVARIAVELCOCOMO_NOME',pic:'@!',nv:''},{av:'AV44TFVariavelCocomo_Nome_Sel',fld:'vTFVARIAVELCOCOMO_NOME_SEL',pic:'@!',nv:''},{av:'AV51TFVariavelCocomo_ProjetoCod',fld:'vTFVARIAVELCOCOMO_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFVariavelCocomo_ProjetoCod_To',fld:'vTFVARIAVELCOCOMO_PROJETOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV56TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV61TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV62TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV65TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV69TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV70TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV73TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV74TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV77TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV78TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV81TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV82TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV85TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV86TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV89TFVariavelCocomo_Usado',fld:'vTFVARIAVELCOCOMO_USADO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV90TFVariavelCocomo_Usado_To',fld:'vTFVARIAVELCOCOMO_USADO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_PROJETOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_USADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV99Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV12GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_variavelcocomo_data_Activeeventkey',ctrl:'DDO_VARIAVELCOCOMO_DATA',prop:'ActiveEventKey'},{av:'Ddo_variavelcocomo_data_Filteredtext_get',ctrl:'DDO_VARIAVELCOCOMO_DATA',prop:'FilteredText_get'},{av:'Ddo_variavelcocomo_data_Filteredtextto_get',ctrl:'DDO_VARIAVELCOCOMO_DATA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV16OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_variavelcocomo_data_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_DATA',prop:'SortedStatus'},{av:'AV55TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV56TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'Ddo_variavelcocomo_areatrabalhocod_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_AREATRABALHOCOD',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_sigla_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_SIGLA',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_nome_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_NOME',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_tipo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_TIPO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_projetocod_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_PROJETOCOD',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_extrabaixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_EXTRABAIXO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_muitobaixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_MUITOBAIXO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_baixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_BAIXO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_nominal_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_NOMINAL',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_alto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_ALTO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_muitoalto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_MUITOALTO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_extraalto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_EXTRAALTO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_usado_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_USADO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_VARIAVELCOCOMO_EXTRABAIXO.ONOPTIONCLICKED","{handler:'E18GL2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV16OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19VariavelCocomo_Nome1',fld:'vVARIAVELCOCOMO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23VariavelCocomo_Nome2',fld:'vVARIAVELCOCOMO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27VariavelCocomo_Nome3',fld:'vVARIAVELCOCOMO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFVariavelCocomo_AreaTrabalhoCod',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFVariavelCocomo_AreaTrabalhoCod_To',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV40TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV43TFVariavelCocomo_Nome',fld:'vTFVARIAVELCOCOMO_NOME',pic:'@!',nv:''},{av:'AV44TFVariavelCocomo_Nome_Sel',fld:'vTFVARIAVELCOCOMO_NOME_SEL',pic:'@!',nv:''},{av:'AV51TFVariavelCocomo_ProjetoCod',fld:'vTFVARIAVELCOCOMO_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFVariavelCocomo_ProjetoCod_To',fld:'vTFVARIAVELCOCOMO_PROJETOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV56TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV61TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV62TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV65TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV69TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV70TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV73TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV74TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV77TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV78TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV81TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV82TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV85TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV86TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV89TFVariavelCocomo_Usado',fld:'vTFVARIAVELCOCOMO_USADO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV90TFVariavelCocomo_Usado_To',fld:'vTFVARIAVELCOCOMO_USADO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_PROJETOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_USADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV99Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV12GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_variavelcocomo_extrabaixo_Activeeventkey',ctrl:'DDO_VARIAVELCOCOMO_EXTRABAIXO',prop:'ActiveEventKey'},{av:'Ddo_variavelcocomo_extrabaixo_Filteredtext_get',ctrl:'DDO_VARIAVELCOCOMO_EXTRABAIXO',prop:'FilteredText_get'},{av:'Ddo_variavelcocomo_extrabaixo_Filteredtextto_get',ctrl:'DDO_VARIAVELCOCOMO_EXTRABAIXO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV16OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_variavelcocomo_extrabaixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_EXTRABAIXO',prop:'SortedStatus'},{av:'AV61TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV62TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_variavelcocomo_areatrabalhocod_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_AREATRABALHOCOD',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_sigla_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_SIGLA',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_nome_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_NOME',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_tipo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_TIPO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_projetocod_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_PROJETOCOD',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_data_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_DATA',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_muitobaixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_MUITOBAIXO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_baixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_BAIXO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_nominal_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_NOMINAL',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_alto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_ALTO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_muitoalto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_MUITOALTO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_extraalto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_EXTRAALTO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_usado_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_USADO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_VARIAVELCOCOMO_MUITOBAIXO.ONOPTIONCLICKED","{handler:'E19GL2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV16OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19VariavelCocomo_Nome1',fld:'vVARIAVELCOCOMO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23VariavelCocomo_Nome2',fld:'vVARIAVELCOCOMO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27VariavelCocomo_Nome3',fld:'vVARIAVELCOCOMO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFVariavelCocomo_AreaTrabalhoCod',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFVariavelCocomo_AreaTrabalhoCod_To',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV40TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV43TFVariavelCocomo_Nome',fld:'vTFVARIAVELCOCOMO_NOME',pic:'@!',nv:''},{av:'AV44TFVariavelCocomo_Nome_Sel',fld:'vTFVARIAVELCOCOMO_NOME_SEL',pic:'@!',nv:''},{av:'AV51TFVariavelCocomo_ProjetoCod',fld:'vTFVARIAVELCOCOMO_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFVariavelCocomo_ProjetoCod_To',fld:'vTFVARIAVELCOCOMO_PROJETOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV56TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV61TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV62TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV65TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV69TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV70TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV73TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV74TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV77TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV78TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV81TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV82TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV85TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV86TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV89TFVariavelCocomo_Usado',fld:'vTFVARIAVELCOCOMO_USADO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV90TFVariavelCocomo_Usado_To',fld:'vTFVARIAVELCOCOMO_USADO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_PROJETOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_USADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV99Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV12GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_variavelcocomo_muitobaixo_Activeeventkey',ctrl:'DDO_VARIAVELCOCOMO_MUITOBAIXO',prop:'ActiveEventKey'},{av:'Ddo_variavelcocomo_muitobaixo_Filteredtext_get',ctrl:'DDO_VARIAVELCOCOMO_MUITOBAIXO',prop:'FilteredText_get'},{av:'Ddo_variavelcocomo_muitobaixo_Filteredtextto_get',ctrl:'DDO_VARIAVELCOCOMO_MUITOBAIXO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV16OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_variavelcocomo_muitobaixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_MUITOBAIXO',prop:'SortedStatus'},{av:'AV65TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_variavelcocomo_areatrabalhocod_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_AREATRABALHOCOD',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_sigla_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_SIGLA',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_nome_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_NOME',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_tipo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_TIPO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_projetocod_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_PROJETOCOD',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_data_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_DATA',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_extrabaixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_EXTRABAIXO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_baixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_BAIXO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_nominal_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_NOMINAL',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_alto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_ALTO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_muitoalto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_MUITOALTO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_extraalto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_EXTRAALTO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_usado_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_USADO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_VARIAVELCOCOMO_BAIXO.ONOPTIONCLICKED","{handler:'E20GL2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV16OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19VariavelCocomo_Nome1',fld:'vVARIAVELCOCOMO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23VariavelCocomo_Nome2',fld:'vVARIAVELCOCOMO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27VariavelCocomo_Nome3',fld:'vVARIAVELCOCOMO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFVariavelCocomo_AreaTrabalhoCod',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFVariavelCocomo_AreaTrabalhoCod_To',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV40TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV43TFVariavelCocomo_Nome',fld:'vTFVARIAVELCOCOMO_NOME',pic:'@!',nv:''},{av:'AV44TFVariavelCocomo_Nome_Sel',fld:'vTFVARIAVELCOCOMO_NOME_SEL',pic:'@!',nv:''},{av:'AV51TFVariavelCocomo_ProjetoCod',fld:'vTFVARIAVELCOCOMO_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFVariavelCocomo_ProjetoCod_To',fld:'vTFVARIAVELCOCOMO_PROJETOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV56TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV61TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV62TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV65TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV69TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV70TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV73TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV74TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV77TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV78TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV81TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV82TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV85TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV86TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV89TFVariavelCocomo_Usado',fld:'vTFVARIAVELCOCOMO_USADO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV90TFVariavelCocomo_Usado_To',fld:'vTFVARIAVELCOCOMO_USADO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_PROJETOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_USADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV99Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV12GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_variavelcocomo_baixo_Activeeventkey',ctrl:'DDO_VARIAVELCOCOMO_BAIXO',prop:'ActiveEventKey'},{av:'Ddo_variavelcocomo_baixo_Filteredtext_get',ctrl:'DDO_VARIAVELCOCOMO_BAIXO',prop:'FilteredText_get'},{av:'Ddo_variavelcocomo_baixo_Filteredtextto_get',ctrl:'DDO_VARIAVELCOCOMO_BAIXO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV16OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_variavelcocomo_baixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_BAIXO',prop:'SortedStatus'},{av:'AV69TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV70TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_variavelcocomo_areatrabalhocod_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_AREATRABALHOCOD',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_sigla_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_SIGLA',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_nome_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_NOME',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_tipo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_TIPO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_projetocod_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_PROJETOCOD',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_data_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_DATA',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_extrabaixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_EXTRABAIXO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_muitobaixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_MUITOBAIXO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_nominal_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_NOMINAL',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_alto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_ALTO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_muitoalto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_MUITOALTO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_extraalto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_EXTRAALTO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_usado_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_USADO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_VARIAVELCOCOMO_NOMINAL.ONOPTIONCLICKED","{handler:'E21GL2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV16OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19VariavelCocomo_Nome1',fld:'vVARIAVELCOCOMO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23VariavelCocomo_Nome2',fld:'vVARIAVELCOCOMO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27VariavelCocomo_Nome3',fld:'vVARIAVELCOCOMO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFVariavelCocomo_AreaTrabalhoCod',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFVariavelCocomo_AreaTrabalhoCod_To',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV40TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV43TFVariavelCocomo_Nome',fld:'vTFVARIAVELCOCOMO_NOME',pic:'@!',nv:''},{av:'AV44TFVariavelCocomo_Nome_Sel',fld:'vTFVARIAVELCOCOMO_NOME_SEL',pic:'@!',nv:''},{av:'AV51TFVariavelCocomo_ProjetoCod',fld:'vTFVARIAVELCOCOMO_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFVariavelCocomo_ProjetoCod_To',fld:'vTFVARIAVELCOCOMO_PROJETOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV56TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV61TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV62TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV65TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV69TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV70TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV73TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV74TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV77TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV78TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV81TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV82TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV85TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV86TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV89TFVariavelCocomo_Usado',fld:'vTFVARIAVELCOCOMO_USADO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV90TFVariavelCocomo_Usado_To',fld:'vTFVARIAVELCOCOMO_USADO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_PROJETOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_USADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV99Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV12GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_variavelcocomo_nominal_Activeeventkey',ctrl:'DDO_VARIAVELCOCOMO_NOMINAL',prop:'ActiveEventKey'},{av:'Ddo_variavelcocomo_nominal_Filteredtext_get',ctrl:'DDO_VARIAVELCOCOMO_NOMINAL',prop:'FilteredText_get'},{av:'Ddo_variavelcocomo_nominal_Filteredtextto_get',ctrl:'DDO_VARIAVELCOCOMO_NOMINAL',prop:'FilteredTextTo_get'}],oparms:[{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV16OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_variavelcocomo_nominal_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_NOMINAL',prop:'SortedStatus'},{av:'AV73TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV74TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_variavelcocomo_areatrabalhocod_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_AREATRABALHOCOD',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_sigla_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_SIGLA',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_nome_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_NOME',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_tipo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_TIPO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_projetocod_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_PROJETOCOD',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_data_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_DATA',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_extrabaixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_EXTRABAIXO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_muitobaixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_MUITOBAIXO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_baixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_BAIXO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_alto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_ALTO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_muitoalto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_MUITOALTO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_extraalto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_EXTRAALTO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_usado_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_USADO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_VARIAVELCOCOMO_ALTO.ONOPTIONCLICKED","{handler:'E22GL2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV16OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19VariavelCocomo_Nome1',fld:'vVARIAVELCOCOMO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23VariavelCocomo_Nome2',fld:'vVARIAVELCOCOMO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27VariavelCocomo_Nome3',fld:'vVARIAVELCOCOMO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFVariavelCocomo_AreaTrabalhoCod',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFVariavelCocomo_AreaTrabalhoCod_To',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV40TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV43TFVariavelCocomo_Nome',fld:'vTFVARIAVELCOCOMO_NOME',pic:'@!',nv:''},{av:'AV44TFVariavelCocomo_Nome_Sel',fld:'vTFVARIAVELCOCOMO_NOME_SEL',pic:'@!',nv:''},{av:'AV51TFVariavelCocomo_ProjetoCod',fld:'vTFVARIAVELCOCOMO_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFVariavelCocomo_ProjetoCod_To',fld:'vTFVARIAVELCOCOMO_PROJETOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV56TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV61TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV62TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV65TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV69TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV70TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV73TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV74TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV77TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV78TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV81TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV82TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV85TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV86TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV89TFVariavelCocomo_Usado',fld:'vTFVARIAVELCOCOMO_USADO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV90TFVariavelCocomo_Usado_To',fld:'vTFVARIAVELCOCOMO_USADO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_PROJETOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_USADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV99Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV12GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_variavelcocomo_alto_Activeeventkey',ctrl:'DDO_VARIAVELCOCOMO_ALTO',prop:'ActiveEventKey'},{av:'Ddo_variavelcocomo_alto_Filteredtext_get',ctrl:'DDO_VARIAVELCOCOMO_ALTO',prop:'FilteredText_get'},{av:'Ddo_variavelcocomo_alto_Filteredtextto_get',ctrl:'DDO_VARIAVELCOCOMO_ALTO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV16OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_variavelcocomo_alto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_ALTO',prop:'SortedStatus'},{av:'AV77TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV78TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_variavelcocomo_areatrabalhocod_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_AREATRABALHOCOD',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_sigla_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_SIGLA',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_nome_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_NOME',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_tipo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_TIPO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_projetocod_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_PROJETOCOD',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_data_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_DATA',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_extrabaixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_EXTRABAIXO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_muitobaixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_MUITOBAIXO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_baixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_BAIXO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_nominal_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_NOMINAL',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_muitoalto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_MUITOALTO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_extraalto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_EXTRAALTO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_usado_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_USADO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_VARIAVELCOCOMO_MUITOALTO.ONOPTIONCLICKED","{handler:'E23GL2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV16OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19VariavelCocomo_Nome1',fld:'vVARIAVELCOCOMO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23VariavelCocomo_Nome2',fld:'vVARIAVELCOCOMO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27VariavelCocomo_Nome3',fld:'vVARIAVELCOCOMO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFVariavelCocomo_AreaTrabalhoCod',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFVariavelCocomo_AreaTrabalhoCod_To',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV40TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV43TFVariavelCocomo_Nome',fld:'vTFVARIAVELCOCOMO_NOME',pic:'@!',nv:''},{av:'AV44TFVariavelCocomo_Nome_Sel',fld:'vTFVARIAVELCOCOMO_NOME_SEL',pic:'@!',nv:''},{av:'AV51TFVariavelCocomo_ProjetoCod',fld:'vTFVARIAVELCOCOMO_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFVariavelCocomo_ProjetoCod_To',fld:'vTFVARIAVELCOCOMO_PROJETOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV56TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV61TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV62TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV65TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV69TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV70TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV73TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV74TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV77TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV78TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV81TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV82TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV85TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV86TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV89TFVariavelCocomo_Usado',fld:'vTFVARIAVELCOCOMO_USADO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV90TFVariavelCocomo_Usado_To',fld:'vTFVARIAVELCOCOMO_USADO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_PROJETOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_USADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV99Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV12GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_variavelcocomo_muitoalto_Activeeventkey',ctrl:'DDO_VARIAVELCOCOMO_MUITOALTO',prop:'ActiveEventKey'},{av:'Ddo_variavelcocomo_muitoalto_Filteredtext_get',ctrl:'DDO_VARIAVELCOCOMO_MUITOALTO',prop:'FilteredText_get'},{av:'Ddo_variavelcocomo_muitoalto_Filteredtextto_get',ctrl:'DDO_VARIAVELCOCOMO_MUITOALTO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV16OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_variavelcocomo_muitoalto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_MUITOALTO',prop:'SortedStatus'},{av:'AV81TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV82TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_variavelcocomo_areatrabalhocod_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_AREATRABALHOCOD',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_sigla_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_SIGLA',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_nome_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_NOME',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_tipo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_TIPO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_projetocod_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_PROJETOCOD',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_data_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_DATA',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_extrabaixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_EXTRABAIXO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_muitobaixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_MUITOBAIXO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_baixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_BAIXO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_nominal_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_NOMINAL',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_alto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_ALTO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_extraalto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_EXTRAALTO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_usado_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_USADO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_VARIAVELCOCOMO_EXTRAALTO.ONOPTIONCLICKED","{handler:'E24GL2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV16OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19VariavelCocomo_Nome1',fld:'vVARIAVELCOCOMO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23VariavelCocomo_Nome2',fld:'vVARIAVELCOCOMO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27VariavelCocomo_Nome3',fld:'vVARIAVELCOCOMO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFVariavelCocomo_AreaTrabalhoCod',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFVariavelCocomo_AreaTrabalhoCod_To',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV40TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV43TFVariavelCocomo_Nome',fld:'vTFVARIAVELCOCOMO_NOME',pic:'@!',nv:''},{av:'AV44TFVariavelCocomo_Nome_Sel',fld:'vTFVARIAVELCOCOMO_NOME_SEL',pic:'@!',nv:''},{av:'AV51TFVariavelCocomo_ProjetoCod',fld:'vTFVARIAVELCOCOMO_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFVariavelCocomo_ProjetoCod_To',fld:'vTFVARIAVELCOCOMO_PROJETOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV56TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV61TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV62TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV65TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV69TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV70TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV73TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV74TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV77TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV78TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV81TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV82TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV85TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV86TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV89TFVariavelCocomo_Usado',fld:'vTFVARIAVELCOCOMO_USADO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV90TFVariavelCocomo_Usado_To',fld:'vTFVARIAVELCOCOMO_USADO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_PROJETOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_USADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV99Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV12GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_variavelcocomo_extraalto_Activeeventkey',ctrl:'DDO_VARIAVELCOCOMO_EXTRAALTO',prop:'ActiveEventKey'},{av:'Ddo_variavelcocomo_extraalto_Filteredtext_get',ctrl:'DDO_VARIAVELCOCOMO_EXTRAALTO',prop:'FilteredText_get'},{av:'Ddo_variavelcocomo_extraalto_Filteredtextto_get',ctrl:'DDO_VARIAVELCOCOMO_EXTRAALTO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV16OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_variavelcocomo_extraalto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_EXTRAALTO',prop:'SortedStatus'},{av:'AV85TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV86TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_variavelcocomo_areatrabalhocod_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_AREATRABALHOCOD',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_sigla_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_SIGLA',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_nome_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_NOME',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_tipo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_TIPO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_projetocod_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_PROJETOCOD',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_data_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_DATA',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_extrabaixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_EXTRABAIXO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_muitobaixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_MUITOBAIXO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_baixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_BAIXO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_nominal_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_NOMINAL',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_alto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_ALTO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_muitoalto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_MUITOALTO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_usado_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_USADO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_VARIAVELCOCOMO_USADO.ONOPTIONCLICKED","{handler:'E25GL2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV16OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19VariavelCocomo_Nome1',fld:'vVARIAVELCOCOMO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23VariavelCocomo_Nome2',fld:'vVARIAVELCOCOMO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27VariavelCocomo_Nome3',fld:'vVARIAVELCOCOMO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFVariavelCocomo_AreaTrabalhoCod',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFVariavelCocomo_AreaTrabalhoCod_To',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV40TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV43TFVariavelCocomo_Nome',fld:'vTFVARIAVELCOCOMO_NOME',pic:'@!',nv:''},{av:'AV44TFVariavelCocomo_Nome_Sel',fld:'vTFVARIAVELCOCOMO_NOME_SEL',pic:'@!',nv:''},{av:'AV51TFVariavelCocomo_ProjetoCod',fld:'vTFVARIAVELCOCOMO_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFVariavelCocomo_ProjetoCod_To',fld:'vTFVARIAVELCOCOMO_PROJETOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV56TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV61TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV62TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV65TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV69TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV70TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV73TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV74TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV77TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV78TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV81TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV82TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV85TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV86TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV89TFVariavelCocomo_Usado',fld:'vTFVARIAVELCOCOMO_USADO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV90TFVariavelCocomo_Usado_To',fld:'vTFVARIAVELCOCOMO_USADO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_PROJETOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_USADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV99Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV12GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_variavelcocomo_usado_Activeeventkey',ctrl:'DDO_VARIAVELCOCOMO_USADO',prop:'ActiveEventKey'},{av:'Ddo_variavelcocomo_usado_Filteredtext_get',ctrl:'DDO_VARIAVELCOCOMO_USADO',prop:'FilteredText_get'},{av:'Ddo_variavelcocomo_usado_Filteredtextto_get',ctrl:'DDO_VARIAVELCOCOMO_USADO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV16OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_variavelcocomo_usado_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_USADO',prop:'SortedStatus'},{av:'AV89TFVariavelCocomo_Usado',fld:'vTFVARIAVELCOCOMO_USADO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV90TFVariavelCocomo_Usado_To',fld:'vTFVARIAVELCOCOMO_USADO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_variavelcocomo_areatrabalhocod_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_AREATRABALHOCOD',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_sigla_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_SIGLA',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_nome_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_NOME',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_tipo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_TIPO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_projetocod_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_PROJETOCOD',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_data_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_DATA',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_extrabaixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_EXTRABAIXO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_muitobaixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_MUITOBAIXO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_baixo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_BAIXO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_nominal_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_NOMINAL',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_alto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_ALTO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_muitoalto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_MUITOALTO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_extraalto_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_EXTRAALTO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E38GL2',iparms:[],oparms:[{av:'AV30Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E39GL2',iparms:[{av:'A961VariavelCocomo_AreaTrabalhoCod',fld:'VARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A962VariavelCocomo_Sigla',fld:'VARIAVELCOCOMO_SIGLA',pic:'@!',hsh:true,nv:''},{av:'A964VariavelCocomo_Tipo',fld:'VARIAVELCOCOMO_TIPO',pic:'',hsh:true,nv:''},{av:'A992VariavelCocomo_Sequencial',fld:'VARIAVELCOCOMO_SEQUENCIAL',pic:'ZZ9',nv:0},{av:'A963VariavelCocomo_Nome',fld:'VARIAVELCOCOMO_NOME',pic:'@!',hsh:true,nv:''}],oparms:[{av:'AV7InOutVariavelCocomo_AreaTrabalhoCod',fld:'vINOUTVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV8InOutVariavelCocomo_Sigla',fld:'vINOUTVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV9InOutVariavelCocomo_Tipo',fld:'vINOUTVARIAVELCOCOMO_TIPO',pic:'',nv:''},{av:'AV33InOutVariavelCocomo_Sequencial',fld:'vINOUTVARIAVELCOCOMO_SEQUENCIAL',pic:'ZZ9',nv:0},{av:'AV10InOutVariavelCocomo_Nome',fld:'vINOUTVARIAVELCOCOMO_NOME',pic:'@!',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E26GL2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV16OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19VariavelCocomo_Nome1',fld:'vVARIAVELCOCOMO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23VariavelCocomo_Nome2',fld:'vVARIAVELCOCOMO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27VariavelCocomo_Nome3',fld:'vVARIAVELCOCOMO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFVariavelCocomo_AreaTrabalhoCod',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFVariavelCocomo_AreaTrabalhoCod_To',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV40TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV43TFVariavelCocomo_Nome',fld:'vTFVARIAVELCOCOMO_NOME',pic:'@!',nv:''},{av:'AV44TFVariavelCocomo_Nome_Sel',fld:'vTFVARIAVELCOCOMO_NOME_SEL',pic:'@!',nv:''},{av:'AV51TFVariavelCocomo_ProjetoCod',fld:'vTFVARIAVELCOCOMO_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFVariavelCocomo_ProjetoCod_To',fld:'vTFVARIAVELCOCOMO_PROJETOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV56TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV61TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV62TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV65TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV69TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV70TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV73TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV74TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV77TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV78TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV81TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV82TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV85TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV86TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV89TFVariavelCocomo_Usado',fld:'vTFVARIAVELCOCOMO_USADO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV90TFVariavelCocomo_Usado_To',fld:'vTFVARIAVELCOCOMO_USADO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_PROJETOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_USADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV99Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV12GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E31GL2',iparms:[],oparms:[{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E27GL2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV16OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19VariavelCocomo_Nome1',fld:'vVARIAVELCOCOMO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23VariavelCocomo_Nome2',fld:'vVARIAVELCOCOMO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27VariavelCocomo_Nome3',fld:'vVARIAVELCOCOMO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFVariavelCocomo_AreaTrabalhoCod',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFVariavelCocomo_AreaTrabalhoCod_To',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV40TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV43TFVariavelCocomo_Nome',fld:'vTFVARIAVELCOCOMO_NOME',pic:'@!',nv:''},{av:'AV44TFVariavelCocomo_Nome_Sel',fld:'vTFVARIAVELCOCOMO_NOME_SEL',pic:'@!',nv:''},{av:'AV51TFVariavelCocomo_ProjetoCod',fld:'vTFVARIAVELCOCOMO_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFVariavelCocomo_ProjetoCod_To',fld:'vTFVARIAVELCOCOMO_PROJETOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV56TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV61TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV62TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV65TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV69TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV70TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV73TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV74TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV77TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV78TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV81TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV82TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV85TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV86TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV89TFVariavelCocomo_Usado',fld:'vTFVARIAVELCOCOMO_USADO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV90TFVariavelCocomo_Usado_To',fld:'vTFVARIAVELCOCOMO_USADO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_PROJETOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_USADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV99Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV12GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV12GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23VariavelCocomo_Nome2',fld:'vVARIAVELCOCOMO_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27VariavelCocomo_Nome3',fld:'vVARIAVELCOCOMO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19VariavelCocomo_Nome1',fld:'vVARIAVELCOCOMO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavVariavelcocomo_nome2_Visible',ctrl:'vVARIAVELCOCOMO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavVariavelcocomo_nome3_Visible',ctrl:'vVARIAVELCOCOMO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavVariavelcocomo_nome1_Visible',ctrl:'vVARIAVELCOCOMO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E32GL2',iparms:[{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavVariavelcocomo_nome1_Visible',ctrl:'vVARIAVELCOCOMO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E33GL2',iparms:[],oparms:[{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E28GL2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV16OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19VariavelCocomo_Nome1',fld:'vVARIAVELCOCOMO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23VariavelCocomo_Nome2',fld:'vVARIAVELCOCOMO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27VariavelCocomo_Nome3',fld:'vVARIAVELCOCOMO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFVariavelCocomo_AreaTrabalhoCod',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFVariavelCocomo_AreaTrabalhoCod_To',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV40TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV43TFVariavelCocomo_Nome',fld:'vTFVARIAVELCOCOMO_NOME',pic:'@!',nv:''},{av:'AV44TFVariavelCocomo_Nome_Sel',fld:'vTFVARIAVELCOCOMO_NOME_SEL',pic:'@!',nv:''},{av:'AV51TFVariavelCocomo_ProjetoCod',fld:'vTFVARIAVELCOCOMO_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFVariavelCocomo_ProjetoCod_To',fld:'vTFVARIAVELCOCOMO_PROJETOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV56TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV61TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV62TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV65TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV69TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV70TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV73TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV74TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV77TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV78TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV81TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV82TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV85TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV86TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV89TFVariavelCocomo_Usado',fld:'vTFVARIAVELCOCOMO_USADO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV90TFVariavelCocomo_Usado_To',fld:'vTFVARIAVELCOCOMO_USADO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_PROJETOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_USADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV99Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV12GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV12GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23VariavelCocomo_Nome2',fld:'vVARIAVELCOCOMO_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27VariavelCocomo_Nome3',fld:'vVARIAVELCOCOMO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19VariavelCocomo_Nome1',fld:'vVARIAVELCOCOMO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavVariavelcocomo_nome2_Visible',ctrl:'vVARIAVELCOCOMO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavVariavelcocomo_nome3_Visible',ctrl:'vVARIAVELCOCOMO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavVariavelcocomo_nome1_Visible',ctrl:'vVARIAVELCOCOMO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E34GL2',iparms:[{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavVariavelcocomo_nome2_Visible',ctrl:'vVARIAVELCOCOMO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E29GL2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV16OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19VariavelCocomo_Nome1',fld:'vVARIAVELCOCOMO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23VariavelCocomo_Nome2',fld:'vVARIAVELCOCOMO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27VariavelCocomo_Nome3',fld:'vVARIAVELCOCOMO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFVariavelCocomo_AreaTrabalhoCod',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFVariavelCocomo_AreaTrabalhoCod_To',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV40TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV43TFVariavelCocomo_Nome',fld:'vTFVARIAVELCOCOMO_NOME',pic:'@!',nv:''},{av:'AV44TFVariavelCocomo_Nome_Sel',fld:'vTFVARIAVELCOCOMO_NOME_SEL',pic:'@!',nv:''},{av:'AV51TFVariavelCocomo_ProjetoCod',fld:'vTFVARIAVELCOCOMO_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFVariavelCocomo_ProjetoCod_To',fld:'vTFVARIAVELCOCOMO_PROJETOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV56TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV61TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV62TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV65TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV69TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV70TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV73TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV74TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV77TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV78TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV81TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV82TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV85TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV86TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV89TFVariavelCocomo_Usado',fld:'vTFVARIAVELCOCOMO_USADO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV90TFVariavelCocomo_Usado_To',fld:'vTFVARIAVELCOCOMO_USADO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_PROJETOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_USADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV99Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV12GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV12GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23VariavelCocomo_Nome2',fld:'vVARIAVELCOCOMO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27VariavelCocomo_Nome3',fld:'vVARIAVELCOCOMO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19VariavelCocomo_Nome1',fld:'vVARIAVELCOCOMO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavVariavelcocomo_nome2_Visible',ctrl:'vVARIAVELCOCOMO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavVariavelcocomo_nome3_Visible',ctrl:'vVARIAVELCOCOMO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavVariavelcocomo_nome1_Visible',ctrl:'vVARIAVELCOCOMO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E35GL2',iparms:[{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavVariavelcocomo_nome3_Visible',ctrl:'vVARIAVELCOCOMO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E30GL2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV16OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19VariavelCocomo_Nome1',fld:'vVARIAVELCOCOMO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23VariavelCocomo_Nome2',fld:'vVARIAVELCOCOMO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27VariavelCocomo_Nome3',fld:'vVARIAVELCOCOMO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFVariavelCocomo_AreaTrabalhoCod',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFVariavelCocomo_AreaTrabalhoCod_To',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV40TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV43TFVariavelCocomo_Nome',fld:'vTFVARIAVELCOCOMO_NOME',pic:'@!',nv:''},{av:'AV44TFVariavelCocomo_Nome_Sel',fld:'vTFVARIAVELCOCOMO_NOME_SEL',pic:'@!',nv:''},{av:'AV51TFVariavelCocomo_ProjetoCod',fld:'vTFVARIAVELCOCOMO_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFVariavelCocomo_ProjetoCod_To',fld:'vTFVARIAVELCOCOMO_PROJETOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV56TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV61TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV62TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV65TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV69TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV70TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV73TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV74TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV77TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV78TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV81TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV82TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV85TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV86TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV89TFVariavelCocomo_Usado',fld:'vTFVARIAVELCOCOMO_USADO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV90TFVariavelCocomo_Usado_To',fld:'vTFVARIAVELCOCOMO_USADO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_PROJETOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_USADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV99Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV12GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV35TFVariavelCocomo_AreaTrabalhoCod',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_variavelcocomo_areatrabalhocod_Filteredtext_set',ctrl:'DDO_VARIAVELCOCOMO_AREATRABALHOCOD',prop:'FilteredText_set'},{av:'AV36TFVariavelCocomo_AreaTrabalhoCod_To',fld:'vTFVARIAVELCOCOMO_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_variavelcocomo_areatrabalhocod_Filteredtextto_set',ctrl:'DDO_VARIAVELCOCOMO_AREATRABALHOCOD',prop:'FilteredTextTo_set'},{av:'AV39TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'Ddo_variavelcocomo_sigla_Filteredtext_set',ctrl:'DDO_VARIAVELCOCOMO_SIGLA',prop:'FilteredText_set'},{av:'AV40TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_variavelcocomo_sigla_Selectedvalue_set',ctrl:'DDO_VARIAVELCOCOMO_SIGLA',prop:'SelectedValue_set'},{av:'AV43TFVariavelCocomo_Nome',fld:'vTFVARIAVELCOCOMO_NOME',pic:'@!',nv:''},{av:'Ddo_variavelcocomo_nome_Filteredtext_set',ctrl:'DDO_VARIAVELCOCOMO_NOME',prop:'FilteredText_set'},{av:'AV44TFVariavelCocomo_Nome_Sel',fld:'vTFVARIAVELCOCOMO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_variavelcocomo_nome_Selectedvalue_set',ctrl:'DDO_VARIAVELCOCOMO_NOME',prop:'SelectedValue_set'},{av:'AV48TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'Ddo_variavelcocomo_tipo_Selectedvalue_set',ctrl:'DDO_VARIAVELCOCOMO_TIPO',prop:'SelectedValue_set'},{av:'AV51TFVariavelCocomo_ProjetoCod',fld:'vTFVARIAVELCOCOMO_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_variavelcocomo_projetocod_Filteredtext_set',ctrl:'DDO_VARIAVELCOCOMO_PROJETOCOD',prop:'FilteredText_set'},{av:'AV52TFVariavelCocomo_ProjetoCod_To',fld:'vTFVARIAVELCOCOMO_PROJETOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_variavelcocomo_projetocod_Filteredtextto_set',ctrl:'DDO_VARIAVELCOCOMO_PROJETOCOD',prop:'FilteredTextTo_set'},{av:'AV55TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'Ddo_variavelcocomo_data_Filteredtext_set',ctrl:'DDO_VARIAVELCOCOMO_DATA',prop:'FilteredText_set'},{av:'AV56TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'Ddo_variavelcocomo_data_Filteredtextto_set',ctrl:'DDO_VARIAVELCOCOMO_DATA',prop:'FilteredTextTo_set'},{av:'AV61TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_variavelcocomo_extrabaixo_Filteredtext_set',ctrl:'DDO_VARIAVELCOCOMO_EXTRABAIXO',prop:'FilteredText_set'},{av:'AV62TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_variavelcocomo_extrabaixo_Filteredtextto_set',ctrl:'DDO_VARIAVELCOCOMO_EXTRABAIXO',prop:'FilteredTextTo_set'},{av:'AV65TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_variavelcocomo_muitobaixo_Filteredtext_set',ctrl:'DDO_VARIAVELCOCOMO_MUITOBAIXO',prop:'FilteredText_set'},{av:'AV66TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_variavelcocomo_muitobaixo_Filteredtextto_set',ctrl:'DDO_VARIAVELCOCOMO_MUITOBAIXO',prop:'FilteredTextTo_set'},{av:'AV69TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_variavelcocomo_baixo_Filteredtext_set',ctrl:'DDO_VARIAVELCOCOMO_BAIXO',prop:'FilteredText_set'},{av:'AV70TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_variavelcocomo_baixo_Filteredtextto_set',ctrl:'DDO_VARIAVELCOCOMO_BAIXO',prop:'FilteredTextTo_set'},{av:'AV73TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_variavelcocomo_nominal_Filteredtext_set',ctrl:'DDO_VARIAVELCOCOMO_NOMINAL',prop:'FilteredText_set'},{av:'AV74TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_variavelcocomo_nominal_Filteredtextto_set',ctrl:'DDO_VARIAVELCOCOMO_NOMINAL',prop:'FilteredTextTo_set'},{av:'AV77TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_variavelcocomo_alto_Filteredtext_set',ctrl:'DDO_VARIAVELCOCOMO_ALTO',prop:'FilteredText_set'},{av:'AV78TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_variavelcocomo_alto_Filteredtextto_set',ctrl:'DDO_VARIAVELCOCOMO_ALTO',prop:'FilteredTextTo_set'},{av:'AV81TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_variavelcocomo_muitoalto_Filteredtext_set',ctrl:'DDO_VARIAVELCOCOMO_MUITOALTO',prop:'FilteredText_set'},{av:'AV82TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_variavelcocomo_muitoalto_Filteredtextto_set',ctrl:'DDO_VARIAVELCOCOMO_MUITOALTO',prop:'FilteredTextTo_set'},{av:'AV85TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_variavelcocomo_extraalto_Filteredtext_set',ctrl:'DDO_VARIAVELCOCOMO_EXTRAALTO',prop:'FilteredText_set'},{av:'AV86TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_variavelcocomo_extraalto_Filteredtextto_set',ctrl:'DDO_VARIAVELCOCOMO_EXTRAALTO',prop:'FilteredTextTo_set'},{av:'AV89TFVariavelCocomo_Usado',fld:'vTFVARIAVELCOCOMO_USADO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_variavelcocomo_usado_Filteredtext_set',ctrl:'DDO_VARIAVELCOCOMO_USADO',prop:'FilteredText_set'},{av:'AV90TFVariavelCocomo_Usado_To',fld:'vTFVARIAVELCOCOMO_USADO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_variavelcocomo_usado_Filteredtextto_set',ctrl:'DDO_VARIAVELCOCOMO_USADO',prop:'FilteredTextTo_set'},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19VariavelCocomo_Nome1',fld:'vVARIAVELCOCOMO_NOME1',pic:'@!',nv:''},{av:'AV12GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavVariavelcocomo_nome1_Visible',ctrl:'vVARIAVELCOCOMO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23VariavelCocomo_Nome2',fld:'vVARIAVELCOCOMO_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27VariavelCocomo_Nome3',fld:'vVARIAVELCOCOMO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavVariavelcocomo_nome2_Visible',ctrl:'vVARIAVELCOCOMO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavVariavelcocomo_nome3_Visible',ctrl:'vVARIAVELCOCOMO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV8InOutVariavelCocomo_Sigla = "";
         wcpOAV9InOutVariavelCocomo_Tipo = "";
         wcpOAV10InOutVariavelCocomo_Nome = "";
         Gridpaginationbar_Selectedpage = "";
         Ddo_variavelcocomo_areatrabalhocod_Activeeventkey = "";
         Ddo_variavelcocomo_areatrabalhocod_Filteredtext_get = "";
         Ddo_variavelcocomo_areatrabalhocod_Filteredtextto_get = "";
         Ddo_variavelcocomo_sigla_Activeeventkey = "";
         Ddo_variavelcocomo_sigla_Filteredtext_get = "";
         Ddo_variavelcocomo_sigla_Selectedvalue_get = "";
         Ddo_variavelcocomo_nome_Activeeventkey = "";
         Ddo_variavelcocomo_nome_Filteredtext_get = "";
         Ddo_variavelcocomo_nome_Selectedvalue_get = "";
         Ddo_variavelcocomo_tipo_Activeeventkey = "";
         Ddo_variavelcocomo_tipo_Selectedvalue_get = "";
         Ddo_variavelcocomo_projetocod_Activeeventkey = "";
         Ddo_variavelcocomo_projetocod_Filteredtext_get = "";
         Ddo_variavelcocomo_projetocod_Filteredtextto_get = "";
         Ddo_variavelcocomo_data_Activeeventkey = "";
         Ddo_variavelcocomo_data_Filteredtext_get = "";
         Ddo_variavelcocomo_data_Filteredtextto_get = "";
         Ddo_variavelcocomo_extrabaixo_Activeeventkey = "";
         Ddo_variavelcocomo_extrabaixo_Filteredtext_get = "";
         Ddo_variavelcocomo_extrabaixo_Filteredtextto_get = "";
         Ddo_variavelcocomo_muitobaixo_Activeeventkey = "";
         Ddo_variavelcocomo_muitobaixo_Filteredtext_get = "";
         Ddo_variavelcocomo_muitobaixo_Filteredtextto_get = "";
         Ddo_variavelcocomo_baixo_Activeeventkey = "";
         Ddo_variavelcocomo_baixo_Filteredtext_get = "";
         Ddo_variavelcocomo_baixo_Filteredtextto_get = "";
         Ddo_variavelcocomo_nominal_Activeeventkey = "";
         Ddo_variavelcocomo_nominal_Filteredtext_get = "";
         Ddo_variavelcocomo_nominal_Filteredtextto_get = "";
         Ddo_variavelcocomo_alto_Activeeventkey = "";
         Ddo_variavelcocomo_alto_Filteredtext_get = "";
         Ddo_variavelcocomo_alto_Filteredtextto_get = "";
         Ddo_variavelcocomo_muitoalto_Activeeventkey = "";
         Ddo_variavelcocomo_muitoalto_Filteredtext_get = "";
         Ddo_variavelcocomo_muitoalto_Filteredtextto_get = "";
         Ddo_variavelcocomo_extraalto_Activeeventkey = "";
         Ddo_variavelcocomo_extraalto_Filteredtext_get = "";
         Ddo_variavelcocomo_extraalto_Filteredtextto_get = "";
         Ddo_variavelcocomo_usado_Activeeventkey = "";
         Ddo_variavelcocomo_usado_Filteredtext_get = "";
         Ddo_variavelcocomo_usado_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV17DynamicFiltersSelector1 = "";
         AV19VariavelCocomo_Nome1 = "";
         AV21DynamicFiltersSelector2 = "";
         AV23VariavelCocomo_Nome2 = "";
         AV25DynamicFiltersSelector3 = "";
         AV27VariavelCocomo_Nome3 = "";
         AV39TFVariavelCocomo_Sigla = "";
         AV40TFVariavelCocomo_Sigla_Sel = "";
         AV43TFVariavelCocomo_Nome = "";
         AV44TFVariavelCocomo_Nome_Sel = "";
         AV55TFVariavelCocomo_Data = DateTime.MinValue;
         AV56TFVariavelCocomo_Data_To = DateTime.MinValue;
         AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace = "";
         AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace = "";
         AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace = "";
         AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace = "";
         AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace = "";
         AV59ddo_VariavelCocomo_DataTitleControlIdToReplace = "";
         AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace = "";
         AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace = "";
         AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace = "";
         AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace = "";
         AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace = "";
         AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace = "";
         AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace = "";
         AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace = "";
         AV48TFVariavelCocomo_Tipo_Sels = new GxSimpleCollection();
         AV99Pgmname = "";
         AV12GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         forbiddenHiddens = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV92DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV34VariavelCocomo_AreaTrabalhoCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38VariavelCocomo_SiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42VariavelCocomo_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46VariavelCocomo_TipoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV50VariavelCocomo_ProjetoCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV54VariavelCocomo_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV60VariavelCocomo_ExtraBaixoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV64VariavelCocomo_MuitoBaixoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV68VariavelCocomo_BaixoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV72VariavelCocomo_NominalTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV76VariavelCocomo_AltoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV80VariavelCocomo_MuitoAltoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV84VariavelCocomo_ExtraAltoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV88VariavelCocomo_UsadoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_variavelcocomo_areatrabalhocod_Filteredtext_set = "";
         Ddo_variavelcocomo_areatrabalhocod_Filteredtextto_set = "";
         Ddo_variavelcocomo_areatrabalhocod_Sortedstatus = "";
         Ddo_variavelcocomo_sigla_Filteredtext_set = "";
         Ddo_variavelcocomo_sigla_Selectedvalue_set = "";
         Ddo_variavelcocomo_sigla_Sortedstatus = "";
         Ddo_variavelcocomo_nome_Filteredtext_set = "";
         Ddo_variavelcocomo_nome_Selectedvalue_set = "";
         Ddo_variavelcocomo_nome_Sortedstatus = "";
         Ddo_variavelcocomo_tipo_Selectedvalue_set = "";
         Ddo_variavelcocomo_tipo_Sortedstatus = "";
         Ddo_variavelcocomo_projetocod_Filteredtext_set = "";
         Ddo_variavelcocomo_projetocod_Filteredtextto_set = "";
         Ddo_variavelcocomo_projetocod_Sortedstatus = "";
         Ddo_variavelcocomo_data_Filteredtext_set = "";
         Ddo_variavelcocomo_data_Filteredtextto_set = "";
         Ddo_variavelcocomo_data_Sortedstatus = "";
         Ddo_variavelcocomo_extrabaixo_Filteredtext_set = "";
         Ddo_variavelcocomo_extrabaixo_Filteredtextto_set = "";
         Ddo_variavelcocomo_extrabaixo_Sortedstatus = "";
         Ddo_variavelcocomo_muitobaixo_Filteredtext_set = "";
         Ddo_variavelcocomo_muitobaixo_Filteredtextto_set = "";
         Ddo_variavelcocomo_muitobaixo_Sortedstatus = "";
         Ddo_variavelcocomo_baixo_Filteredtext_set = "";
         Ddo_variavelcocomo_baixo_Filteredtextto_set = "";
         Ddo_variavelcocomo_baixo_Sortedstatus = "";
         Ddo_variavelcocomo_nominal_Filteredtext_set = "";
         Ddo_variavelcocomo_nominal_Filteredtextto_set = "";
         Ddo_variavelcocomo_nominal_Sortedstatus = "";
         Ddo_variavelcocomo_alto_Filteredtext_set = "";
         Ddo_variavelcocomo_alto_Filteredtextto_set = "";
         Ddo_variavelcocomo_alto_Sortedstatus = "";
         Ddo_variavelcocomo_muitoalto_Filteredtext_set = "";
         Ddo_variavelcocomo_muitoalto_Filteredtextto_set = "";
         Ddo_variavelcocomo_muitoalto_Sortedstatus = "";
         Ddo_variavelcocomo_extraalto_Filteredtext_set = "";
         Ddo_variavelcocomo_extraalto_Filteredtextto_set = "";
         Ddo_variavelcocomo_extraalto_Sortedstatus = "";
         Ddo_variavelcocomo_usado_Filteredtext_set = "";
         Ddo_variavelcocomo_usado_Filteredtextto_set = "";
         Ddo_variavelcocomo_usado_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV57DDO_VariavelCocomo_DataAuxDate = DateTime.MinValue;
         AV58DDO_VariavelCocomo_DataAuxDateTo = DateTime.MinValue;
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV30Select = "";
         AV98Select_GXI = "";
         A962VariavelCocomo_Sigla = "";
         A963VariavelCocomo_Nome = "";
         A964VariavelCocomo_Tipo = "";
         A966VariavelCocomo_Data = DateTime.MinValue;
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV19VariavelCocomo_Nome1 = "";
         lV23VariavelCocomo_Nome2 = "";
         lV27VariavelCocomo_Nome3 = "";
         lV39TFVariavelCocomo_Sigla = "";
         lV43TFVariavelCocomo_Nome = "";
         H00GL2_A992VariavelCocomo_Sequencial = new short[1] ;
         H00GL2_A973VariavelCocomo_Usado = new decimal[1] ;
         H00GL2_n973VariavelCocomo_Usado = new bool[] {false} ;
         H00GL2_A972VariavelCocomo_ExtraAlto = new decimal[1] ;
         H00GL2_n972VariavelCocomo_ExtraAlto = new bool[] {false} ;
         H00GL2_A971VariavelCocomo_MuitoAlto = new decimal[1] ;
         H00GL2_n971VariavelCocomo_MuitoAlto = new bool[] {false} ;
         H00GL2_A970VariavelCocomo_Alto = new decimal[1] ;
         H00GL2_n970VariavelCocomo_Alto = new bool[] {false} ;
         H00GL2_A969VariavelCocomo_Nominal = new decimal[1] ;
         H00GL2_n969VariavelCocomo_Nominal = new bool[] {false} ;
         H00GL2_A968VariavelCocomo_Baixo = new decimal[1] ;
         H00GL2_n968VariavelCocomo_Baixo = new bool[] {false} ;
         H00GL2_A967VariavelCocomo_MuitoBaixo = new decimal[1] ;
         H00GL2_n967VariavelCocomo_MuitoBaixo = new bool[] {false} ;
         H00GL2_A986VariavelCocomo_ExtraBaixo = new decimal[1] ;
         H00GL2_n986VariavelCocomo_ExtraBaixo = new bool[] {false} ;
         H00GL2_A966VariavelCocomo_Data = new DateTime[] {DateTime.MinValue} ;
         H00GL2_n966VariavelCocomo_Data = new bool[] {false} ;
         H00GL2_A965VariavelCocomo_ProjetoCod = new int[1] ;
         H00GL2_n965VariavelCocomo_ProjetoCod = new bool[] {false} ;
         H00GL2_A964VariavelCocomo_Tipo = new String[] {""} ;
         H00GL2_A963VariavelCocomo_Nome = new String[] {""} ;
         H00GL2_A962VariavelCocomo_Sigla = new String[] {""} ;
         H00GL2_A961VariavelCocomo_AreaTrabalhoCod = new int[1] ;
         H00GL3_AGRID_nRecordCount = new long[1] ;
         hsh = "";
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV47TFVariavelCocomo_Tipo_SelsJson = "";
         GridRow = new GXWebRow();
         AV14GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV13GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptvariaveiscocomo__default(),
            new Object[][] {
                new Object[] {
               H00GL2_A992VariavelCocomo_Sequencial, H00GL2_A973VariavelCocomo_Usado, H00GL2_n973VariavelCocomo_Usado, H00GL2_A972VariavelCocomo_ExtraAlto, H00GL2_n972VariavelCocomo_ExtraAlto, H00GL2_A971VariavelCocomo_MuitoAlto, H00GL2_n971VariavelCocomo_MuitoAlto, H00GL2_A970VariavelCocomo_Alto, H00GL2_n970VariavelCocomo_Alto, H00GL2_A969VariavelCocomo_Nominal,
               H00GL2_n969VariavelCocomo_Nominal, H00GL2_A968VariavelCocomo_Baixo, H00GL2_n968VariavelCocomo_Baixo, H00GL2_A967VariavelCocomo_MuitoBaixo, H00GL2_n967VariavelCocomo_MuitoBaixo, H00GL2_A986VariavelCocomo_ExtraBaixo, H00GL2_n986VariavelCocomo_ExtraBaixo, H00GL2_A966VariavelCocomo_Data, H00GL2_n966VariavelCocomo_Data, H00GL2_A965VariavelCocomo_ProjetoCod,
               H00GL2_n965VariavelCocomo_ProjetoCod, H00GL2_A964VariavelCocomo_Tipo, H00GL2_A963VariavelCocomo_Nome, H00GL2_A962VariavelCocomo_Sigla, H00GL2_A961VariavelCocomo_AreaTrabalhoCod
               }
               , new Object[] {
               H00GL3_AGRID_nRecordCount
               }
            }
         );
         AV99Pgmname = "PromptVariaveisCocomo";
         /* GeneXus formulas. */
         AV99Pgmname = "PromptVariaveisCocomo";
         context.Gx_err = 0;
      }

      private short AV33InOutVariavelCocomo_Sequencial ;
      private short wcpOAV33InOutVariavelCocomo_Sequencial ;
      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_80 ;
      private short nGXsfl_80_idx=1 ;
      private short AV15OrderedBy ;
      private short AV18DynamicFiltersOperator1 ;
      private short AV22DynamicFiltersOperator2 ;
      private short AV26DynamicFiltersOperator3 ;
      private short A992VariavelCocomo_Sequencial ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_80_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtVariavelCocomo_AreaTrabalhoCod_Titleformat ;
      private short edtVariavelCocomo_Sigla_Titleformat ;
      private short edtVariavelCocomo_Nome_Titleformat ;
      private short cmbVariavelCocomo_Tipo_Titleformat ;
      private short edtVariavelCocomo_ProjetoCod_Titleformat ;
      private short edtVariavelCocomo_Data_Titleformat ;
      private short edtVariavelCocomo_ExtraBaixo_Titleformat ;
      private short edtVariavelCocomo_MuitoBaixo_Titleformat ;
      private short edtVariavelCocomo_Baixo_Titleformat ;
      private short edtVariavelCocomo_Nominal_Titleformat ;
      private short edtVariavelCocomo_Alto_Titleformat ;
      private short edtVariavelCocomo_MuitoAlto_Titleformat ;
      private short edtVariavelCocomo_ExtraAlto_Titleformat ;
      private short edtVariavelCocomo_Usado_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutVariavelCocomo_AreaTrabalhoCod ;
      private int wcpOAV7InOutVariavelCocomo_AreaTrabalhoCod ;
      private int subGrid_Rows ;
      private int AV35TFVariavelCocomo_AreaTrabalhoCod ;
      private int AV36TFVariavelCocomo_AreaTrabalhoCod_To ;
      private int AV51TFVariavelCocomo_ProjetoCod ;
      private int AV52TFVariavelCocomo_ProjetoCod_To ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_variavelcocomo_sigla_Datalistupdateminimumcharacters ;
      private int Ddo_variavelcocomo_nome_Datalistupdateminimumcharacters ;
      private int edtVariavelCocomo_Sequencial_Visible ;
      private int edtavTfvariavelcocomo_areatrabalhocod_Visible ;
      private int edtavTfvariavelcocomo_areatrabalhocod_to_Visible ;
      private int edtavTfvariavelcocomo_sigla_Visible ;
      private int edtavTfvariavelcocomo_sigla_sel_Visible ;
      private int edtavTfvariavelcocomo_nome_Visible ;
      private int edtavTfvariavelcocomo_nome_sel_Visible ;
      private int edtavTfvariavelcocomo_projetocod_Visible ;
      private int edtavTfvariavelcocomo_projetocod_to_Visible ;
      private int edtavTfvariavelcocomo_data_Visible ;
      private int edtavTfvariavelcocomo_data_to_Visible ;
      private int edtavTfvariavelcocomo_extrabaixo_Visible ;
      private int edtavTfvariavelcocomo_extrabaixo_to_Visible ;
      private int edtavTfvariavelcocomo_muitobaixo_Visible ;
      private int edtavTfvariavelcocomo_muitobaixo_to_Visible ;
      private int edtavTfvariavelcocomo_baixo_Visible ;
      private int edtavTfvariavelcocomo_baixo_to_Visible ;
      private int edtavTfvariavelcocomo_nominal_Visible ;
      private int edtavTfvariavelcocomo_nominal_to_Visible ;
      private int edtavTfvariavelcocomo_alto_Visible ;
      private int edtavTfvariavelcocomo_alto_to_Visible ;
      private int edtavTfvariavelcocomo_muitoalto_Visible ;
      private int edtavTfvariavelcocomo_muitoalto_to_Visible ;
      private int edtavTfvariavelcocomo_extraalto_Visible ;
      private int edtavTfvariavelcocomo_extraalto_to_Visible ;
      private int edtavTfvariavelcocomo_usado_Visible ;
      private int edtavTfvariavelcocomo_usado_to_Visible ;
      private int edtavDdo_variavelcocomo_areatrabalhocodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_variavelcocomo_siglatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_variavelcocomo_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_variavelcocomo_tipotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_variavelcocomo_projetocodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_variavelcocomo_datatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_variavelcocomo_extrabaixotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_variavelcocomo_muitobaixotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_variavelcocomo_baixotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_variavelcocomo_nominaltitlecontrolidtoreplace_Visible ;
      private int edtavDdo_variavelcocomo_altotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_variavelcocomo_muitoaltotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_variavelcocomo_extraaltotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_variavelcocomo_usadotitlecontrolidtoreplace_Visible ;
      private int A961VariavelCocomo_AreaTrabalhoCod ;
      private int A965VariavelCocomo_ProjetoCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV48TFVariavelCocomo_Tipo_Sels_Count ;
      private int edtavOrdereddsc_Visible ;
      private int AV93PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavVariavelcocomo_nome1_Visible ;
      private int edtavVariavelcocomo_nome2_Visible ;
      private int edtavVariavelcocomo_nome3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV94GridCurrentPage ;
      private long AV95GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV61TFVariavelCocomo_ExtraBaixo ;
      private decimal AV62TFVariavelCocomo_ExtraBaixo_To ;
      private decimal AV65TFVariavelCocomo_MuitoBaixo ;
      private decimal AV66TFVariavelCocomo_MuitoBaixo_To ;
      private decimal AV69TFVariavelCocomo_Baixo ;
      private decimal AV70TFVariavelCocomo_Baixo_To ;
      private decimal AV73TFVariavelCocomo_Nominal ;
      private decimal AV74TFVariavelCocomo_Nominal_To ;
      private decimal AV77TFVariavelCocomo_Alto ;
      private decimal AV78TFVariavelCocomo_Alto_To ;
      private decimal AV81TFVariavelCocomo_MuitoAlto ;
      private decimal AV82TFVariavelCocomo_MuitoAlto_To ;
      private decimal AV85TFVariavelCocomo_ExtraAlto ;
      private decimal AV86TFVariavelCocomo_ExtraAlto_To ;
      private decimal AV89TFVariavelCocomo_Usado ;
      private decimal AV90TFVariavelCocomo_Usado_To ;
      private decimal A986VariavelCocomo_ExtraBaixo ;
      private decimal A967VariavelCocomo_MuitoBaixo ;
      private decimal A968VariavelCocomo_Baixo ;
      private decimal A969VariavelCocomo_Nominal ;
      private decimal A970VariavelCocomo_Alto ;
      private decimal A971VariavelCocomo_MuitoAlto ;
      private decimal A972VariavelCocomo_ExtraAlto ;
      private decimal A973VariavelCocomo_Usado ;
      private String AV8InOutVariavelCocomo_Sigla ;
      private String AV9InOutVariavelCocomo_Tipo ;
      private String AV10InOutVariavelCocomo_Nome ;
      private String wcpOAV8InOutVariavelCocomo_Sigla ;
      private String wcpOAV9InOutVariavelCocomo_Tipo ;
      private String wcpOAV10InOutVariavelCocomo_Nome ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_variavelcocomo_areatrabalhocod_Activeeventkey ;
      private String Ddo_variavelcocomo_areatrabalhocod_Filteredtext_get ;
      private String Ddo_variavelcocomo_areatrabalhocod_Filteredtextto_get ;
      private String Ddo_variavelcocomo_sigla_Activeeventkey ;
      private String Ddo_variavelcocomo_sigla_Filteredtext_get ;
      private String Ddo_variavelcocomo_sigla_Selectedvalue_get ;
      private String Ddo_variavelcocomo_nome_Activeeventkey ;
      private String Ddo_variavelcocomo_nome_Filteredtext_get ;
      private String Ddo_variavelcocomo_nome_Selectedvalue_get ;
      private String Ddo_variavelcocomo_tipo_Activeeventkey ;
      private String Ddo_variavelcocomo_tipo_Selectedvalue_get ;
      private String Ddo_variavelcocomo_projetocod_Activeeventkey ;
      private String Ddo_variavelcocomo_projetocod_Filteredtext_get ;
      private String Ddo_variavelcocomo_projetocod_Filteredtextto_get ;
      private String Ddo_variavelcocomo_data_Activeeventkey ;
      private String Ddo_variavelcocomo_data_Filteredtext_get ;
      private String Ddo_variavelcocomo_data_Filteredtextto_get ;
      private String Ddo_variavelcocomo_extrabaixo_Activeeventkey ;
      private String Ddo_variavelcocomo_extrabaixo_Filteredtext_get ;
      private String Ddo_variavelcocomo_extrabaixo_Filteredtextto_get ;
      private String Ddo_variavelcocomo_muitobaixo_Activeeventkey ;
      private String Ddo_variavelcocomo_muitobaixo_Filteredtext_get ;
      private String Ddo_variavelcocomo_muitobaixo_Filteredtextto_get ;
      private String Ddo_variavelcocomo_baixo_Activeeventkey ;
      private String Ddo_variavelcocomo_baixo_Filteredtext_get ;
      private String Ddo_variavelcocomo_baixo_Filteredtextto_get ;
      private String Ddo_variavelcocomo_nominal_Activeeventkey ;
      private String Ddo_variavelcocomo_nominal_Filteredtext_get ;
      private String Ddo_variavelcocomo_nominal_Filteredtextto_get ;
      private String Ddo_variavelcocomo_alto_Activeeventkey ;
      private String Ddo_variavelcocomo_alto_Filteredtext_get ;
      private String Ddo_variavelcocomo_alto_Filteredtextto_get ;
      private String Ddo_variavelcocomo_muitoalto_Activeeventkey ;
      private String Ddo_variavelcocomo_muitoalto_Filteredtext_get ;
      private String Ddo_variavelcocomo_muitoalto_Filteredtextto_get ;
      private String Ddo_variavelcocomo_extraalto_Activeeventkey ;
      private String Ddo_variavelcocomo_extraalto_Filteredtext_get ;
      private String Ddo_variavelcocomo_extraalto_Filteredtextto_get ;
      private String Ddo_variavelcocomo_usado_Activeeventkey ;
      private String Ddo_variavelcocomo_usado_Filteredtext_get ;
      private String Ddo_variavelcocomo_usado_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_80_idx="0001" ;
      private String AV19VariavelCocomo_Nome1 ;
      private String AV23VariavelCocomo_Nome2 ;
      private String AV27VariavelCocomo_Nome3 ;
      private String AV39TFVariavelCocomo_Sigla ;
      private String AV40TFVariavelCocomo_Sigla_Sel ;
      private String AV43TFVariavelCocomo_Nome ;
      private String AV44TFVariavelCocomo_Nome_Sel ;
      private String AV99Pgmname ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_variavelcocomo_areatrabalhocod_Caption ;
      private String Ddo_variavelcocomo_areatrabalhocod_Tooltip ;
      private String Ddo_variavelcocomo_areatrabalhocod_Cls ;
      private String Ddo_variavelcocomo_areatrabalhocod_Filteredtext_set ;
      private String Ddo_variavelcocomo_areatrabalhocod_Filteredtextto_set ;
      private String Ddo_variavelcocomo_areatrabalhocod_Dropdownoptionstype ;
      private String Ddo_variavelcocomo_areatrabalhocod_Titlecontrolidtoreplace ;
      private String Ddo_variavelcocomo_areatrabalhocod_Sortedstatus ;
      private String Ddo_variavelcocomo_areatrabalhocod_Filtertype ;
      private String Ddo_variavelcocomo_areatrabalhocod_Sortasc ;
      private String Ddo_variavelcocomo_areatrabalhocod_Sortdsc ;
      private String Ddo_variavelcocomo_areatrabalhocod_Cleanfilter ;
      private String Ddo_variavelcocomo_areatrabalhocod_Rangefilterfrom ;
      private String Ddo_variavelcocomo_areatrabalhocod_Rangefilterto ;
      private String Ddo_variavelcocomo_areatrabalhocod_Searchbuttontext ;
      private String Ddo_variavelcocomo_sigla_Caption ;
      private String Ddo_variavelcocomo_sigla_Tooltip ;
      private String Ddo_variavelcocomo_sigla_Cls ;
      private String Ddo_variavelcocomo_sigla_Filteredtext_set ;
      private String Ddo_variavelcocomo_sigla_Selectedvalue_set ;
      private String Ddo_variavelcocomo_sigla_Dropdownoptionstype ;
      private String Ddo_variavelcocomo_sigla_Titlecontrolidtoreplace ;
      private String Ddo_variavelcocomo_sigla_Sortedstatus ;
      private String Ddo_variavelcocomo_sigla_Filtertype ;
      private String Ddo_variavelcocomo_sigla_Datalisttype ;
      private String Ddo_variavelcocomo_sigla_Datalistproc ;
      private String Ddo_variavelcocomo_sigla_Sortasc ;
      private String Ddo_variavelcocomo_sigla_Sortdsc ;
      private String Ddo_variavelcocomo_sigla_Loadingdata ;
      private String Ddo_variavelcocomo_sigla_Cleanfilter ;
      private String Ddo_variavelcocomo_sigla_Noresultsfound ;
      private String Ddo_variavelcocomo_sigla_Searchbuttontext ;
      private String Ddo_variavelcocomo_nome_Caption ;
      private String Ddo_variavelcocomo_nome_Tooltip ;
      private String Ddo_variavelcocomo_nome_Cls ;
      private String Ddo_variavelcocomo_nome_Filteredtext_set ;
      private String Ddo_variavelcocomo_nome_Selectedvalue_set ;
      private String Ddo_variavelcocomo_nome_Dropdownoptionstype ;
      private String Ddo_variavelcocomo_nome_Titlecontrolidtoreplace ;
      private String Ddo_variavelcocomo_nome_Sortedstatus ;
      private String Ddo_variavelcocomo_nome_Filtertype ;
      private String Ddo_variavelcocomo_nome_Datalisttype ;
      private String Ddo_variavelcocomo_nome_Datalistproc ;
      private String Ddo_variavelcocomo_nome_Sortasc ;
      private String Ddo_variavelcocomo_nome_Sortdsc ;
      private String Ddo_variavelcocomo_nome_Loadingdata ;
      private String Ddo_variavelcocomo_nome_Cleanfilter ;
      private String Ddo_variavelcocomo_nome_Noresultsfound ;
      private String Ddo_variavelcocomo_nome_Searchbuttontext ;
      private String Ddo_variavelcocomo_tipo_Caption ;
      private String Ddo_variavelcocomo_tipo_Tooltip ;
      private String Ddo_variavelcocomo_tipo_Cls ;
      private String Ddo_variavelcocomo_tipo_Selectedvalue_set ;
      private String Ddo_variavelcocomo_tipo_Dropdownoptionstype ;
      private String Ddo_variavelcocomo_tipo_Titlecontrolidtoreplace ;
      private String Ddo_variavelcocomo_tipo_Sortedstatus ;
      private String Ddo_variavelcocomo_tipo_Datalisttype ;
      private String Ddo_variavelcocomo_tipo_Datalistfixedvalues ;
      private String Ddo_variavelcocomo_tipo_Sortasc ;
      private String Ddo_variavelcocomo_tipo_Sortdsc ;
      private String Ddo_variavelcocomo_tipo_Cleanfilter ;
      private String Ddo_variavelcocomo_tipo_Searchbuttontext ;
      private String Ddo_variavelcocomo_projetocod_Caption ;
      private String Ddo_variavelcocomo_projetocod_Tooltip ;
      private String Ddo_variavelcocomo_projetocod_Cls ;
      private String Ddo_variavelcocomo_projetocod_Filteredtext_set ;
      private String Ddo_variavelcocomo_projetocod_Filteredtextto_set ;
      private String Ddo_variavelcocomo_projetocod_Dropdownoptionstype ;
      private String Ddo_variavelcocomo_projetocod_Titlecontrolidtoreplace ;
      private String Ddo_variavelcocomo_projetocod_Sortedstatus ;
      private String Ddo_variavelcocomo_projetocod_Filtertype ;
      private String Ddo_variavelcocomo_projetocod_Sortasc ;
      private String Ddo_variavelcocomo_projetocod_Sortdsc ;
      private String Ddo_variavelcocomo_projetocod_Cleanfilter ;
      private String Ddo_variavelcocomo_projetocod_Rangefilterfrom ;
      private String Ddo_variavelcocomo_projetocod_Rangefilterto ;
      private String Ddo_variavelcocomo_projetocod_Searchbuttontext ;
      private String Ddo_variavelcocomo_data_Caption ;
      private String Ddo_variavelcocomo_data_Tooltip ;
      private String Ddo_variavelcocomo_data_Cls ;
      private String Ddo_variavelcocomo_data_Filteredtext_set ;
      private String Ddo_variavelcocomo_data_Filteredtextto_set ;
      private String Ddo_variavelcocomo_data_Dropdownoptionstype ;
      private String Ddo_variavelcocomo_data_Titlecontrolidtoreplace ;
      private String Ddo_variavelcocomo_data_Sortedstatus ;
      private String Ddo_variavelcocomo_data_Filtertype ;
      private String Ddo_variavelcocomo_data_Sortasc ;
      private String Ddo_variavelcocomo_data_Sortdsc ;
      private String Ddo_variavelcocomo_data_Cleanfilter ;
      private String Ddo_variavelcocomo_data_Rangefilterfrom ;
      private String Ddo_variavelcocomo_data_Rangefilterto ;
      private String Ddo_variavelcocomo_data_Searchbuttontext ;
      private String Ddo_variavelcocomo_extrabaixo_Caption ;
      private String Ddo_variavelcocomo_extrabaixo_Tooltip ;
      private String Ddo_variavelcocomo_extrabaixo_Cls ;
      private String Ddo_variavelcocomo_extrabaixo_Filteredtext_set ;
      private String Ddo_variavelcocomo_extrabaixo_Filteredtextto_set ;
      private String Ddo_variavelcocomo_extrabaixo_Dropdownoptionstype ;
      private String Ddo_variavelcocomo_extrabaixo_Titlecontrolidtoreplace ;
      private String Ddo_variavelcocomo_extrabaixo_Sortedstatus ;
      private String Ddo_variavelcocomo_extrabaixo_Filtertype ;
      private String Ddo_variavelcocomo_extrabaixo_Sortasc ;
      private String Ddo_variavelcocomo_extrabaixo_Sortdsc ;
      private String Ddo_variavelcocomo_extrabaixo_Cleanfilter ;
      private String Ddo_variavelcocomo_extrabaixo_Rangefilterfrom ;
      private String Ddo_variavelcocomo_extrabaixo_Rangefilterto ;
      private String Ddo_variavelcocomo_extrabaixo_Searchbuttontext ;
      private String Ddo_variavelcocomo_muitobaixo_Caption ;
      private String Ddo_variavelcocomo_muitobaixo_Tooltip ;
      private String Ddo_variavelcocomo_muitobaixo_Cls ;
      private String Ddo_variavelcocomo_muitobaixo_Filteredtext_set ;
      private String Ddo_variavelcocomo_muitobaixo_Filteredtextto_set ;
      private String Ddo_variavelcocomo_muitobaixo_Dropdownoptionstype ;
      private String Ddo_variavelcocomo_muitobaixo_Titlecontrolidtoreplace ;
      private String Ddo_variavelcocomo_muitobaixo_Sortedstatus ;
      private String Ddo_variavelcocomo_muitobaixo_Filtertype ;
      private String Ddo_variavelcocomo_muitobaixo_Sortasc ;
      private String Ddo_variavelcocomo_muitobaixo_Sortdsc ;
      private String Ddo_variavelcocomo_muitobaixo_Cleanfilter ;
      private String Ddo_variavelcocomo_muitobaixo_Rangefilterfrom ;
      private String Ddo_variavelcocomo_muitobaixo_Rangefilterto ;
      private String Ddo_variavelcocomo_muitobaixo_Searchbuttontext ;
      private String Ddo_variavelcocomo_baixo_Caption ;
      private String Ddo_variavelcocomo_baixo_Tooltip ;
      private String Ddo_variavelcocomo_baixo_Cls ;
      private String Ddo_variavelcocomo_baixo_Filteredtext_set ;
      private String Ddo_variavelcocomo_baixo_Filteredtextto_set ;
      private String Ddo_variavelcocomo_baixo_Dropdownoptionstype ;
      private String Ddo_variavelcocomo_baixo_Titlecontrolidtoreplace ;
      private String Ddo_variavelcocomo_baixo_Sortedstatus ;
      private String Ddo_variavelcocomo_baixo_Filtertype ;
      private String Ddo_variavelcocomo_baixo_Sortasc ;
      private String Ddo_variavelcocomo_baixo_Sortdsc ;
      private String Ddo_variavelcocomo_baixo_Cleanfilter ;
      private String Ddo_variavelcocomo_baixo_Rangefilterfrom ;
      private String Ddo_variavelcocomo_baixo_Rangefilterto ;
      private String Ddo_variavelcocomo_baixo_Searchbuttontext ;
      private String Ddo_variavelcocomo_nominal_Caption ;
      private String Ddo_variavelcocomo_nominal_Tooltip ;
      private String Ddo_variavelcocomo_nominal_Cls ;
      private String Ddo_variavelcocomo_nominal_Filteredtext_set ;
      private String Ddo_variavelcocomo_nominal_Filteredtextto_set ;
      private String Ddo_variavelcocomo_nominal_Dropdownoptionstype ;
      private String Ddo_variavelcocomo_nominal_Titlecontrolidtoreplace ;
      private String Ddo_variavelcocomo_nominal_Sortedstatus ;
      private String Ddo_variavelcocomo_nominal_Filtertype ;
      private String Ddo_variavelcocomo_nominal_Sortasc ;
      private String Ddo_variavelcocomo_nominal_Sortdsc ;
      private String Ddo_variavelcocomo_nominal_Cleanfilter ;
      private String Ddo_variavelcocomo_nominal_Rangefilterfrom ;
      private String Ddo_variavelcocomo_nominal_Rangefilterto ;
      private String Ddo_variavelcocomo_nominal_Searchbuttontext ;
      private String Ddo_variavelcocomo_alto_Caption ;
      private String Ddo_variavelcocomo_alto_Tooltip ;
      private String Ddo_variavelcocomo_alto_Cls ;
      private String Ddo_variavelcocomo_alto_Filteredtext_set ;
      private String Ddo_variavelcocomo_alto_Filteredtextto_set ;
      private String Ddo_variavelcocomo_alto_Dropdownoptionstype ;
      private String Ddo_variavelcocomo_alto_Titlecontrolidtoreplace ;
      private String Ddo_variavelcocomo_alto_Sortedstatus ;
      private String Ddo_variavelcocomo_alto_Filtertype ;
      private String Ddo_variavelcocomo_alto_Sortasc ;
      private String Ddo_variavelcocomo_alto_Sortdsc ;
      private String Ddo_variavelcocomo_alto_Cleanfilter ;
      private String Ddo_variavelcocomo_alto_Rangefilterfrom ;
      private String Ddo_variavelcocomo_alto_Rangefilterto ;
      private String Ddo_variavelcocomo_alto_Searchbuttontext ;
      private String Ddo_variavelcocomo_muitoalto_Caption ;
      private String Ddo_variavelcocomo_muitoalto_Tooltip ;
      private String Ddo_variavelcocomo_muitoalto_Cls ;
      private String Ddo_variavelcocomo_muitoalto_Filteredtext_set ;
      private String Ddo_variavelcocomo_muitoalto_Filteredtextto_set ;
      private String Ddo_variavelcocomo_muitoalto_Dropdownoptionstype ;
      private String Ddo_variavelcocomo_muitoalto_Titlecontrolidtoreplace ;
      private String Ddo_variavelcocomo_muitoalto_Sortedstatus ;
      private String Ddo_variavelcocomo_muitoalto_Filtertype ;
      private String Ddo_variavelcocomo_muitoalto_Sortasc ;
      private String Ddo_variavelcocomo_muitoalto_Sortdsc ;
      private String Ddo_variavelcocomo_muitoalto_Cleanfilter ;
      private String Ddo_variavelcocomo_muitoalto_Rangefilterfrom ;
      private String Ddo_variavelcocomo_muitoalto_Rangefilterto ;
      private String Ddo_variavelcocomo_muitoalto_Searchbuttontext ;
      private String Ddo_variavelcocomo_extraalto_Caption ;
      private String Ddo_variavelcocomo_extraalto_Tooltip ;
      private String Ddo_variavelcocomo_extraalto_Cls ;
      private String Ddo_variavelcocomo_extraalto_Filteredtext_set ;
      private String Ddo_variavelcocomo_extraalto_Filteredtextto_set ;
      private String Ddo_variavelcocomo_extraalto_Dropdownoptionstype ;
      private String Ddo_variavelcocomo_extraalto_Titlecontrolidtoreplace ;
      private String Ddo_variavelcocomo_extraalto_Sortedstatus ;
      private String Ddo_variavelcocomo_extraalto_Filtertype ;
      private String Ddo_variavelcocomo_extraalto_Sortasc ;
      private String Ddo_variavelcocomo_extraalto_Sortdsc ;
      private String Ddo_variavelcocomo_extraalto_Cleanfilter ;
      private String Ddo_variavelcocomo_extraalto_Rangefilterfrom ;
      private String Ddo_variavelcocomo_extraalto_Rangefilterto ;
      private String Ddo_variavelcocomo_extraalto_Searchbuttontext ;
      private String Ddo_variavelcocomo_usado_Caption ;
      private String Ddo_variavelcocomo_usado_Tooltip ;
      private String Ddo_variavelcocomo_usado_Cls ;
      private String Ddo_variavelcocomo_usado_Filteredtext_set ;
      private String Ddo_variavelcocomo_usado_Filteredtextto_set ;
      private String Ddo_variavelcocomo_usado_Dropdownoptionstype ;
      private String Ddo_variavelcocomo_usado_Titlecontrolidtoreplace ;
      private String Ddo_variavelcocomo_usado_Sortedstatus ;
      private String Ddo_variavelcocomo_usado_Filtertype ;
      private String Ddo_variavelcocomo_usado_Sortasc ;
      private String Ddo_variavelcocomo_usado_Sortdsc ;
      private String Ddo_variavelcocomo_usado_Cleanfilter ;
      private String Ddo_variavelcocomo_usado_Rangefilterfrom ;
      private String Ddo_variavelcocomo_usado_Rangefilterto ;
      private String Ddo_variavelcocomo_usado_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String edtVariavelCocomo_Sequencial_Internalname ;
      private String edtVariavelCocomo_Sequencial_Jsonclick ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfvariavelcocomo_areatrabalhocod_Internalname ;
      private String edtavTfvariavelcocomo_areatrabalhocod_Jsonclick ;
      private String edtavTfvariavelcocomo_areatrabalhocod_to_Internalname ;
      private String edtavTfvariavelcocomo_areatrabalhocod_to_Jsonclick ;
      private String edtavTfvariavelcocomo_sigla_Internalname ;
      private String edtavTfvariavelcocomo_sigla_Jsonclick ;
      private String edtavTfvariavelcocomo_sigla_sel_Internalname ;
      private String edtavTfvariavelcocomo_sigla_sel_Jsonclick ;
      private String edtavTfvariavelcocomo_nome_Internalname ;
      private String edtavTfvariavelcocomo_nome_Jsonclick ;
      private String edtavTfvariavelcocomo_nome_sel_Internalname ;
      private String edtavTfvariavelcocomo_nome_sel_Jsonclick ;
      private String edtavTfvariavelcocomo_projetocod_Internalname ;
      private String edtavTfvariavelcocomo_projetocod_Jsonclick ;
      private String edtavTfvariavelcocomo_projetocod_to_Internalname ;
      private String edtavTfvariavelcocomo_projetocod_to_Jsonclick ;
      private String edtavTfvariavelcocomo_data_Internalname ;
      private String edtavTfvariavelcocomo_data_Jsonclick ;
      private String edtavTfvariavelcocomo_data_to_Internalname ;
      private String edtavTfvariavelcocomo_data_to_Jsonclick ;
      private String divDdo_variavelcocomo_dataauxdates_Internalname ;
      private String edtavDdo_variavelcocomo_dataauxdate_Internalname ;
      private String edtavDdo_variavelcocomo_dataauxdate_Jsonclick ;
      private String edtavDdo_variavelcocomo_dataauxdateto_Internalname ;
      private String edtavDdo_variavelcocomo_dataauxdateto_Jsonclick ;
      private String edtavTfvariavelcocomo_extrabaixo_Internalname ;
      private String edtavTfvariavelcocomo_extrabaixo_Jsonclick ;
      private String edtavTfvariavelcocomo_extrabaixo_to_Internalname ;
      private String edtavTfvariavelcocomo_extrabaixo_to_Jsonclick ;
      private String edtavTfvariavelcocomo_muitobaixo_Internalname ;
      private String edtavTfvariavelcocomo_muitobaixo_Jsonclick ;
      private String edtavTfvariavelcocomo_muitobaixo_to_Internalname ;
      private String edtavTfvariavelcocomo_muitobaixo_to_Jsonclick ;
      private String edtavTfvariavelcocomo_baixo_Internalname ;
      private String edtavTfvariavelcocomo_baixo_Jsonclick ;
      private String edtavTfvariavelcocomo_baixo_to_Internalname ;
      private String edtavTfvariavelcocomo_baixo_to_Jsonclick ;
      private String edtavTfvariavelcocomo_nominal_Internalname ;
      private String edtavTfvariavelcocomo_nominal_Jsonclick ;
      private String edtavTfvariavelcocomo_nominal_to_Internalname ;
      private String edtavTfvariavelcocomo_nominal_to_Jsonclick ;
      private String edtavTfvariavelcocomo_alto_Internalname ;
      private String edtavTfvariavelcocomo_alto_Jsonclick ;
      private String edtavTfvariavelcocomo_alto_to_Internalname ;
      private String edtavTfvariavelcocomo_alto_to_Jsonclick ;
      private String edtavTfvariavelcocomo_muitoalto_Internalname ;
      private String edtavTfvariavelcocomo_muitoalto_Jsonclick ;
      private String edtavTfvariavelcocomo_muitoalto_to_Internalname ;
      private String edtavTfvariavelcocomo_muitoalto_to_Jsonclick ;
      private String edtavTfvariavelcocomo_extraalto_Internalname ;
      private String edtavTfvariavelcocomo_extraalto_Jsonclick ;
      private String edtavTfvariavelcocomo_extraalto_to_Internalname ;
      private String edtavTfvariavelcocomo_extraalto_to_Jsonclick ;
      private String edtavTfvariavelcocomo_usado_Internalname ;
      private String edtavTfvariavelcocomo_usado_Jsonclick ;
      private String edtavTfvariavelcocomo_usado_to_Internalname ;
      private String edtavTfvariavelcocomo_usado_to_Jsonclick ;
      private String edtavDdo_variavelcocomo_areatrabalhocodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_variavelcocomo_siglatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_variavelcocomo_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_variavelcocomo_tipotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_variavelcocomo_projetocodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_variavelcocomo_datatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_variavelcocomo_extrabaixotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_variavelcocomo_muitobaixotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_variavelcocomo_baixotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_variavelcocomo_nominaltitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_variavelcocomo_altotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_variavelcocomo_muitoaltotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_variavelcocomo_extraaltotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_variavelcocomo_usadotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtVariavelCocomo_AreaTrabalhoCod_Internalname ;
      private String A962VariavelCocomo_Sigla ;
      private String edtVariavelCocomo_Sigla_Internalname ;
      private String A963VariavelCocomo_Nome ;
      private String edtVariavelCocomo_Nome_Internalname ;
      private String cmbVariavelCocomo_Tipo_Internalname ;
      private String A964VariavelCocomo_Tipo ;
      private String edtVariavelCocomo_ProjetoCod_Internalname ;
      private String edtVariavelCocomo_Data_Internalname ;
      private String edtVariavelCocomo_ExtraBaixo_Internalname ;
      private String edtVariavelCocomo_MuitoBaixo_Internalname ;
      private String edtVariavelCocomo_Baixo_Internalname ;
      private String edtVariavelCocomo_Nominal_Internalname ;
      private String edtVariavelCocomo_Alto_Internalname ;
      private String edtVariavelCocomo_MuitoAlto_Internalname ;
      private String edtVariavelCocomo_ExtraAlto_Internalname ;
      private String edtVariavelCocomo_Usado_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV19VariavelCocomo_Nome1 ;
      private String lV23VariavelCocomo_Nome2 ;
      private String lV27VariavelCocomo_Nome3 ;
      private String lV39TFVariavelCocomo_Sigla ;
      private String lV43TFVariavelCocomo_Nome ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavVariavelcocomo_nome1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavVariavelcocomo_nome2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavVariavelcocomo_nome3_Internalname ;
      private String hsh ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_variavelcocomo_areatrabalhocod_Internalname ;
      private String Ddo_variavelcocomo_sigla_Internalname ;
      private String Ddo_variavelcocomo_nome_Internalname ;
      private String Ddo_variavelcocomo_tipo_Internalname ;
      private String Ddo_variavelcocomo_projetocod_Internalname ;
      private String Ddo_variavelcocomo_data_Internalname ;
      private String Ddo_variavelcocomo_extrabaixo_Internalname ;
      private String Ddo_variavelcocomo_muitobaixo_Internalname ;
      private String Ddo_variavelcocomo_baixo_Internalname ;
      private String Ddo_variavelcocomo_nominal_Internalname ;
      private String Ddo_variavelcocomo_alto_Internalname ;
      private String Ddo_variavelcocomo_muitoalto_Internalname ;
      private String Ddo_variavelcocomo_extraalto_Internalname ;
      private String Ddo_variavelcocomo_usado_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtVariavelCocomo_AreaTrabalhoCod_Title ;
      private String edtVariavelCocomo_Sigla_Title ;
      private String edtVariavelCocomo_Nome_Title ;
      private String edtVariavelCocomo_ProjetoCod_Title ;
      private String edtVariavelCocomo_Data_Title ;
      private String edtVariavelCocomo_ExtraBaixo_Title ;
      private String edtVariavelCocomo_MuitoBaixo_Title ;
      private String edtVariavelCocomo_Baixo_Title ;
      private String edtVariavelCocomo_Nominal_Title ;
      private String edtVariavelCocomo_Alto_Title ;
      private String edtVariavelCocomo_MuitoAlto_Title ;
      private String edtVariavelCocomo_ExtraAlto_Title ;
      private String edtVariavelCocomo_Usado_Title ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavVariavelcocomo_nome3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavVariavelcocomo_nome2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavVariavelcocomo_nome1_Jsonclick ;
      private String sGXsfl_80_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtVariavelCocomo_AreaTrabalhoCod_Jsonclick ;
      private String edtVariavelCocomo_Sigla_Jsonclick ;
      private String edtVariavelCocomo_Nome_Jsonclick ;
      private String cmbVariavelCocomo_Tipo_Jsonclick ;
      private String edtVariavelCocomo_ProjetoCod_Jsonclick ;
      private String edtVariavelCocomo_Data_Jsonclick ;
      private String edtVariavelCocomo_ExtraBaixo_Jsonclick ;
      private String edtVariavelCocomo_MuitoBaixo_Jsonclick ;
      private String edtVariavelCocomo_Baixo_Jsonclick ;
      private String edtVariavelCocomo_Nominal_Jsonclick ;
      private String edtVariavelCocomo_Alto_Jsonclick ;
      private String edtVariavelCocomo_MuitoAlto_Jsonclick ;
      private String edtVariavelCocomo_ExtraAlto_Jsonclick ;
      private String edtVariavelCocomo_Usado_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV55TFVariavelCocomo_Data ;
      private DateTime AV56TFVariavelCocomo_Data_To ;
      private DateTime AV57DDO_VariavelCocomo_DataAuxDate ;
      private DateTime AV58DDO_VariavelCocomo_DataAuxDateTo ;
      private DateTime A966VariavelCocomo_Data ;
      private bool entryPointCalled ;
      private bool AV16OrderedDsc ;
      private bool AV20DynamicFiltersEnabled2 ;
      private bool AV24DynamicFiltersEnabled3 ;
      private bool AV29DynamicFiltersIgnoreFirst ;
      private bool AV28DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_variavelcocomo_areatrabalhocod_Includesortasc ;
      private bool Ddo_variavelcocomo_areatrabalhocod_Includesortdsc ;
      private bool Ddo_variavelcocomo_areatrabalhocod_Includefilter ;
      private bool Ddo_variavelcocomo_areatrabalhocod_Filterisrange ;
      private bool Ddo_variavelcocomo_areatrabalhocod_Includedatalist ;
      private bool Ddo_variavelcocomo_sigla_Includesortasc ;
      private bool Ddo_variavelcocomo_sigla_Includesortdsc ;
      private bool Ddo_variavelcocomo_sigla_Includefilter ;
      private bool Ddo_variavelcocomo_sigla_Filterisrange ;
      private bool Ddo_variavelcocomo_sigla_Includedatalist ;
      private bool Ddo_variavelcocomo_nome_Includesortasc ;
      private bool Ddo_variavelcocomo_nome_Includesortdsc ;
      private bool Ddo_variavelcocomo_nome_Includefilter ;
      private bool Ddo_variavelcocomo_nome_Filterisrange ;
      private bool Ddo_variavelcocomo_nome_Includedatalist ;
      private bool Ddo_variavelcocomo_tipo_Includesortasc ;
      private bool Ddo_variavelcocomo_tipo_Includesortdsc ;
      private bool Ddo_variavelcocomo_tipo_Includefilter ;
      private bool Ddo_variavelcocomo_tipo_Includedatalist ;
      private bool Ddo_variavelcocomo_tipo_Allowmultipleselection ;
      private bool Ddo_variavelcocomo_projetocod_Includesortasc ;
      private bool Ddo_variavelcocomo_projetocod_Includesortdsc ;
      private bool Ddo_variavelcocomo_projetocod_Includefilter ;
      private bool Ddo_variavelcocomo_projetocod_Filterisrange ;
      private bool Ddo_variavelcocomo_projetocod_Includedatalist ;
      private bool Ddo_variavelcocomo_data_Includesortasc ;
      private bool Ddo_variavelcocomo_data_Includesortdsc ;
      private bool Ddo_variavelcocomo_data_Includefilter ;
      private bool Ddo_variavelcocomo_data_Filterisrange ;
      private bool Ddo_variavelcocomo_data_Includedatalist ;
      private bool Ddo_variavelcocomo_extrabaixo_Includesortasc ;
      private bool Ddo_variavelcocomo_extrabaixo_Includesortdsc ;
      private bool Ddo_variavelcocomo_extrabaixo_Includefilter ;
      private bool Ddo_variavelcocomo_extrabaixo_Filterisrange ;
      private bool Ddo_variavelcocomo_extrabaixo_Includedatalist ;
      private bool Ddo_variavelcocomo_muitobaixo_Includesortasc ;
      private bool Ddo_variavelcocomo_muitobaixo_Includesortdsc ;
      private bool Ddo_variavelcocomo_muitobaixo_Includefilter ;
      private bool Ddo_variavelcocomo_muitobaixo_Filterisrange ;
      private bool Ddo_variavelcocomo_muitobaixo_Includedatalist ;
      private bool Ddo_variavelcocomo_baixo_Includesortasc ;
      private bool Ddo_variavelcocomo_baixo_Includesortdsc ;
      private bool Ddo_variavelcocomo_baixo_Includefilter ;
      private bool Ddo_variavelcocomo_baixo_Filterisrange ;
      private bool Ddo_variavelcocomo_baixo_Includedatalist ;
      private bool Ddo_variavelcocomo_nominal_Includesortasc ;
      private bool Ddo_variavelcocomo_nominal_Includesortdsc ;
      private bool Ddo_variavelcocomo_nominal_Includefilter ;
      private bool Ddo_variavelcocomo_nominal_Filterisrange ;
      private bool Ddo_variavelcocomo_nominal_Includedatalist ;
      private bool Ddo_variavelcocomo_alto_Includesortasc ;
      private bool Ddo_variavelcocomo_alto_Includesortdsc ;
      private bool Ddo_variavelcocomo_alto_Includefilter ;
      private bool Ddo_variavelcocomo_alto_Filterisrange ;
      private bool Ddo_variavelcocomo_alto_Includedatalist ;
      private bool Ddo_variavelcocomo_muitoalto_Includesortasc ;
      private bool Ddo_variavelcocomo_muitoalto_Includesortdsc ;
      private bool Ddo_variavelcocomo_muitoalto_Includefilter ;
      private bool Ddo_variavelcocomo_muitoalto_Filterisrange ;
      private bool Ddo_variavelcocomo_muitoalto_Includedatalist ;
      private bool Ddo_variavelcocomo_extraalto_Includesortasc ;
      private bool Ddo_variavelcocomo_extraalto_Includesortdsc ;
      private bool Ddo_variavelcocomo_extraalto_Includefilter ;
      private bool Ddo_variavelcocomo_extraalto_Filterisrange ;
      private bool Ddo_variavelcocomo_extraalto_Includedatalist ;
      private bool Ddo_variavelcocomo_usado_Includesortasc ;
      private bool Ddo_variavelcocomo_usado_Includesortdsc ;
      private bool Ddo_variavelcocomo_usado_Includefilter ;
      private bool Ddo_variavelcocomo_usado_Filterisrange ;
      private bool Ddo_variavelcocomo_usado_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n965VariavelCocomo_ProjetoCod ;
      private bool n966VariavelCocomo_Data ;
      private bool n986VariavelCocomo_ExtraBaixo ;
      private bool n967VariavelCocomo_MuitoBaixo ;
      private bool n968VariavelCocomo_Baixo ;
      private bool n969VariavelCocomo_Nominal ;
      private bool n970VariavelCocomo_Alto ;
      private bool n971VariavelCocomo_MuitoAlto ;
      private bool n972VariavelCocomo_ExtraAlto ;
      private bool n973VariavelCocomo_Usado ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV30Select_IsBlob ;
      private String AV47TFVariavelCocomo_Tipo_SelsJson ;
      private String AV17DynamicFiltersSelector1 ;
      private String AV21DynamicFiltersSelector2 ;
      private String AV25DynamicFiltersSelector3 ;
      private String AV37ddo_VariavelCocomo_AreaTrabalhoCodTitleControlIdToReplace ;
      private String AV41ddo_VariavelCocomo_SiglaTitleControlIdToReplace ;
      private String AV45ddo_VariavelCocomo_NomeTitleControlIdToReplace ;
      private String AV49ddo_VariavelCocomo_TipoTitleControlIdToReplace ;
      private String AV53ddo_VariavelCocomo_ProjetoCodTitleControlIdToReplace ;
      private String AV59ddo_VariavelCocomo_DataTitleControlIdToReplace ;
      private String AV63ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace ;
      private String AV67ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace ;
      private String AV71ddo_VariavelCocomo_BaixoTitleControlIdToReplace ;
      private String AV75ddo_VariavelCocomo_NominalTitleControlIdToReplace ;
      private String AV79ddo_VariavelCocomo_AltoTitleControlIdToReplace ;
      private String AV83ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace ;
      private String AV87ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace ;
      private String AV91ddo_VariavelCocomo_UsadoTitleControlIdToReplace ;
      private String AV98Select_GXI ;
      private String AV30Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutVariavelCocomo_AreaTrabalhoCod ;
      private String aP1_InOutVariavelCocomo_Sigla ;
      private String aP2_InOutVariavelCocomo_Tipo ;
      private short aP3_InOutVariavelCocomo_Sequencial ;
      private String aP4_InOutVariavelCocomo_Nome ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCombobox cmbVariavelCocomo_Tipo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private short[] H00GL2_A992VariavelCocomo_Sequencial ;
      private decimal[] H00GL2_A973VariavelCocomo_Usado ;
      private bool[] H00GL2_n973VariavelCocomo_Usado ;
      private decimal[] H00GL2_A972VariavelCocomo_ExtraAlto ;
      private bool[] H00GL2_n972VariavelCocomo_ExtraAlto ;
      private decimal[] H00GL2_A971VariavelCocomo_MuitoAlto ;
      private bool[] H00GL2_n971VariavelCocomo_MuitoAlto ;
      private decimal[] H00GL2_A970VariavelCocomo_Alto ;
      private bool[] H00GL2_n970VariavelCocomo_Alto ;
      private decimal[] H00GL2_A969VariavelCocomo_Nominal ;
      private bool[] H00GL2_n969VariavelCocomo_Nominal ;
      private decimal[] H00GL2_A968VariavelCocomo_Baixo ;
      private bool[] H00GL2_n968VariavelCocomo_Baixo ;
      private decimal[] H00GL2_A967VariavelCocomo_MuitoBaixo ;
      private bool[] H00GL2_n967VariavelCocomo_MuitoBaixo ;
      private decimal[] H00GL2_A986VariavelCocomo_ExtraBaixo ;
      private bool[] H00GL2_n986VariavelCocomo_ExtraBaixo ;
      private DateTime[] H00GL2_A966VariavelCocomo_Data ;
      private bool[] H00GL2_n966VariavelCocomo_Data ;
      private int[] H00GL2_A965VariavelCocomo_ProjetoCod ;
      private bool[] H00GL2_n965VariavelCocomo_ProjetoCod ;
      private String[] H00GL2_A964VariavelCocomo_Tipo ;
      private String[] H00GL2_A963VariavelCocomo_Nome ;
      private String[] H00GL2_A962VariavelCocomo_Sigla ;
      private int[] H00GL2_A961VariavelCocomo_AreaTrabalhoCod ;
      private long[] H00GL3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV48TFVariavelCocomo_Tipo_Sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV34VariavelCocomo_AreaTrabalhoCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV38VariavelCocomo_SiglaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV42VariavelCocomo_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV46VariavelCocomo_TipoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV50VariavelCocomo_ProjetoCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV54VariavelCocomo_DataTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV60VariavelCocomo_ExtraBaixoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV64VariavelCocomo_MuitoBaixoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV68VariavelCocomo_BaixoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV72VariavelCocomo_NominalTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV76VariavelCocomo_AltoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV80VariavelCocomo_MuitoAltoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV84VariavelCocomo_ExtraAltoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV88VariavelCocomo_UsadoTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV12GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV13GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV14GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV92DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptvariaveiscocomo__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00GL2( IGxContext context ,
                                             String A964VariavelCocomo_Tipo ,
                                             IGxCollection AV48TFVariavelCocomo_Tipo_Sels ,
                                             String AV17DynamicFiltersSelector1 ,
                                             short AV18DynamicFiltersOperator1 ,
                                             String AV19VariavelCocomo_Nome1 ,
                                             bool AV20DynamicFiltersEnabled2 ,
                                             String AV21DynamicFiltersSelector2 ,
                                             short AV22DynamicFiltersOperator2 ,
                                             String AV23VariavelCocomo_Nome2 ,
                                             bool AV24DynamicFiltersEnabled3 ,
                                             String AV25DynamicFiltersSelector3 ,
                                             short AV26DynamicFiltersOperator3 ,
                                             String AV27VariavelCocomo_Nome3 ,
                                             int AV35TFVariavelCocomo_AreaTrabalhoCod ,
                                             int AV36TFVariavelCocomo_AreaTrabalhoCod_To ,
                                             String AV40TFVariavelCocomo_Sigla_Sel ,
                                             String AV39TFVariavelCocomo_Sigla ,
                                             String AV44TFVariavelCocomo_Nome_Sel ,
                                             String AV43TFVariavelCocomo_Nome ,
                                             int AV48TFVariavelCocomo_Tipo_Sels_Count ,
                                             int AV51TFVariavelCocomo_ProjetoCod ,
                                             int AV52TFVariavelCocomo_ProjetoCod_To ,
                                             DateTime AV55TFVariavelCocomo_Data ,
                                             DateTime AV56TFVariavelCocomo_Data_To ,
                                             decimal AV61TFVariavelCocomo_ExtraBaixo ,
                                             decimal AV62TFVariavelCocomo_ExtraBaixo_To ,
                                             decimal AV65TFVariavelCocomo_MuitoBaixo ,
                                             decimal AV66TFVariavelCocomo_MuitoBaixo_To ,
                                             decimal AV69TFVariavelCocomo_Baixo ,
                                             decimal AV70TFVariavelCocomo_Baixo_To ,
                                             decimal AV73TFVariavelCocomo_Nominal ,
                                             decimal AV74TFVariavelCocomo_Nominal_To ,
                                             decimal AV77TFVariavelCocomo_Alto ,
                                             decimal AV78TFVariavelCocomo_Alto_To ,
                                             decimal AV81TFVariavelCocomo_MuitoAlto ,
                                             decimal AV82TFVariavelCocomo_MuitoAlto_To ,
                                             decimal AV85TFVariavelCocomo_ExtraAlto ,
                                             decimal AV86TFVariavelCocomo_ExtraAlto_To ,
                                             decimal AV89TFVariavelCocomo_Usado ,
                                             decimal AV90TFVariavelCocomo_Usado_To ,
                                             String A963VariavelCocomo_Nome ,
                                             int A961VariavelCocomo_AreaTrabalhoCod ,
                                             String A962VariavelCocomo_Sigla ,
                                             int A965VariavelCocomo_ProjetoCod ,
                                             DateTime A966VariavelCocomo_Data ,
                                             decimal A986VariavelCocomo_ExtraBaixo ,
                                             decimal A967VariavelCocomo_MuitoBaixo ,
                                             decimal A968VariavelCocomo_Baixo ,
                                             decimal A969VariavelCocomo_Nominal ,
                                             decimal A970VariavelCocomo_Alto ,
                                             decimal A971VariavelCocomo_MuitoAlto ,
                                             decimal A972VariavelCocomo_ExtraAlto ,
                                             decimal A973VariavelCocomo_Usado ,
                                             short AV15OrderedBy ,
                                             bool AV16OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [37] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [VariavelCocomo_Sequencial], [VariavelCocomo_Usado], [VariavelCocomo_ExtraAlto], [VariavelCocomo_MuitoAlto], [VariavelCocomo_Alto], [VariavelCocomo_Nominal], [VariavelCocomo_Baixo], [VariavelCocomo_MuitoBaixo], [VariavelCocomo_ExtraBaixo], [VariavelCocomo_Data], [VariavelCocomo_ProjetoCod], [VariavelCocomo_Tipo], [VariavelCocomo_Nome], [VariavelCocomo_Sigla], [VariavelCocomo_AreaTrabalhoCod]";
         sFromString = " FROM [VariaveisCocomo] WITH (NOLOCK)";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "VARIAVELCOCOMO_NOME") == 0 ) && ( AV18DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19VariavelCocomo_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nome] like @lV19VariavelCocomo_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nome] like @lV19VariavelCocomo_Nome1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "VARIAVELCOCOMO_NOME") == 0 ) && ( AV18DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19VariavelCocomo_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nome] like '%' + @lV19VariavelCocomo_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nome] like '%' + @lV19VariavelCocomo_Nome1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "VARIAVELCOCOMO_NOME") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23VariavelCocomo_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nome] like @lV23VariavelCocomo_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nome] like @lV23VariavelCocomo_Nome2)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "VARIAVELCOCOMO_NOME") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23VariavelCocomo_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nome] like '%' + @lV23VariavelCocomo_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nome] like '%' + @lV23VariavelCocomo_Nome2)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "VARIAVELCOCOMO_NOME") == 0 ) && ( AV26DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27VariavelCocomo_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nome] like @lV27VariavelCocomo_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nome] like @lV27VariavelCocomo_Nome3)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "VARIAVELCOCOMO_NOME") == 0 ) && ( AV26DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27VariavelCocomo_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nome] like '%' + @lV27VariavelCocomo_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nome] like '%' + @lV27VariavelCocomo_Nome3)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! (0==AV35TFVariavelCocomo_AreaTrabalhoCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_AreaTrabalhoCod] >= @AV35TFVariavelCocomo_AreaTrabalhoCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_AreaTrabalhoCod] >= @AV35TFVariavelCocomo_AreaTrabalhoCod)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! (0==AV36TFVariavelCocomo_AreaTrabalhoCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_AreaTrabalhoCod] <= @AV36TFVariavelCocomo_AreaTrabalhoCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_AreaTrabalhoCod] <= @AV36TFVariavelCocomo_AreaTrabalhoCod_To)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV40TFVariavelCocomo_Sigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFVariavelCocomo_Sigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Sigla] like @lV39TFVariavelCocomo_Sigla)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Sigla] like @lV39TFVariavelCocomo_Sigla)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFVariavelCocomo_Sigla_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Sigla] = @AV40TFVariavelCocomo_Sigla_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Sigla] = @AV40TFVariavelCocomo_Sigla_Sel)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV44TFVariavelCocomo_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFVariavelCocomo_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nome] like @lV43TFVariavelCocomo_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nome] like @lV43TFVariavelCocomo_Nome)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFVariavelCocomo_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nome] = @AV44TFVariavelCocomo_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nome] = @AV44TFVariavelCocomo_Nome_Sel)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( AV48TFVariavelCocomo_Tipo_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV48TFVariavelCocomo_Tipo_Sels, "[VariavelCocomo_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV48TFVariavelCocomo_Tipo_Sels, "[VariavelCocomo_Tipo] IN (", ")") + ")";
            }
         }
         if ( ! (0==AV51TFVariavelCocomo_ProjetoCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_ProjetoCod] >= @AV51TFVariavelCocomo_ProjetoCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_ProjetoCod] >= @AV51TFVariavelCocomo_ProjetoCod)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ! (0==AV52TFVariavelCocomo_ProjetoCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_ProjetoCod] <= @AV52TFVariavelCocomo_ProjetoCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_ProjetoCod] <= @AV52TFVariavelCocomo_ProjetoCod_To)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( ! (DateTime.MinValue==AV55TFVariavelCocomo_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Data] >= @AV55TFVariavelCocomo_Data)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Data] >= @AV55TFVariavelCocomo_Data)";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV56TFVariavelCocomo_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Data] <= @AV56TFVariavelCocomo_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Data] <= @AV56TFVariavelCocomo_Data_To)";
            }
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV61TFVariavelCocomo_ExtraBaixo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_ExtraBaixo] >= @AV61TFVariavelCocomo_ExtraBaixo)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_ExtraBaixo] >= @AV61TFVariavelCocomo_ExtraBaixo)";
            }
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV62TFVariavelCocomo_ExtraBaixo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_ExtraBaixo] <= @AV62TFVariavelCocomo_ExtraBaixo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_ExtraBaixo] <= @AV62TFVariavelCocomo_ExtraBaixo_To)";
            }
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV65TFVariavelCocomo_MuitoBaixo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_MuitoBaixo] >= @AV65TFVariavelCocomo_MuitoBaixo)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_MuitoBaixo] >= @AV65TFVariavelCocomo_MuitoBaixo)";
            }
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV66TFVariavelCocomo_MuitoBaixo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_MuitoBaixo] <= @AV66TFVariavelCocomo_MuitoBaixo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_MuitoBaixo] <= @AV66TFVariavelCocomo_MuitoBaixo_To)";
            }
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV69TFVariavelCocomo_Baixo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Baixo] >= @AV69TFVariavelCocomo_Baixo)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Baixo] >= @AV69TFVariavelCocomo_Baixo)";
            }
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV70TFVariavelCocomo_Baixo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Baixo] <= @AV70TFVariavelCocomo_Baixo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Baixo] <= @AV70TFVariavelCocomo_Baixo_To)";
            }
         }
         else
         {
            GXv_int2[21] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV73TFVariavelCocomo_Nominal) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nominal] >= @AV73TFVariavelCocomo_Nominal)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nominal] >= @AV73TFVariavelCocomo_Nominal)";
            }
         }
         else
         {
            GXv_int2[22] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV74TFVariavelCocomo_Nominal_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nominal] <= @AV74TFVariavelCocomo_Nominal_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nominal] <= @AV74TFVariavelCocomo_Nominal_To)";
            }
         }
         else
         {
            GXv_int2[23] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV77TFVariavelCocomo_Alto) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Alto] >= @AV77TFVariavelCocomo_Alto)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Alto] >= @AV77TFVariavelCocomo_Alto)";
            }
         }
         else
         {
            GXv_int2[24] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV78TFVariavelCocomo_Alto_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Alto] <= @AV78TFVariavelCocomo_Alto_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Alto] <= @AV78TFVariavelCocomo_Alto_To)";
            }
         }
         else
         {
            GXv_int2[25] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV81TFVariavelCocomo_MuitoAlto) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_MuitoAlto] >= @AV81TFVariavelCocomo_MuitoAlto)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_MuitoAlto] >= @AV81TFVariavelCocomo_MuitoAlto)";
            }
         }
         else
         {
            GXv_int2[26] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV82TFVariavelCocomo_MuitoAlto_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_MuitoAlto] <= @AV82TFVariavelCocomo_MuitoAlto_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_MuitoAlto] <= @AV82TFVariavelCocomo_MuitoAlto_To)";
            }
         }
         else
         {
            GXv_int2[27] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV85TFVariavelCocomo_ExtraAlto) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_ExtraAlto] >= @AV85TFVariavelCocomo_ExtraAlto)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_ExtraAlto] >= @AV85TFVariavelCocomo_ExtraAlto)";
            }
         }
         else
         {
            GXv_int2[28] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV86TFVariavelCocomo_ExtraAlto_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_ExtraAlto] <= @AV86TFVariavelCocomo_ExtraAlto_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_ExtraAlto] <= @AV86TFVariavelCocomo_ExtraAlto_To)";
            }
         }
         else
         {
            GXv_int2[29] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV89TFVariavelCocomo_Usado) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Usado] >= @AV89TFVariavelCocomo_Usado)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Usado] >= @AV89TFVariavelCocomo_Usado)";
            }
         }
         else
         {
            GXv_int2[30] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV90TFVariavelCocomo_Usado_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Usado] <= @AV90TFVariavelCocomo_Usado_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Usado] <= @AV90TFVariavelCocomo_Usado_To)";
            }
         }
         else
         {
            GXv_int2[31] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV15OrderedBy == 1 ) && ! AV16OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [VariavelCocomo_Nome]";
         }
         else if ( ( AV15OrderedBy == 1 ) && ( AV16OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [VariavelCocomo_Nome] DESC";
         }
         else if ( ( AV15OrderedBy == 2 ) && ! AV16OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [VariavelCocomo_AreaTrabalhoCod]";
         }
         else if ( ( AV15OrderedBy == 2 ) && ( AV16OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [VariavelCocomo_AreaTrabalhoCod] DESC";
         }
         else if ( ( AV15OrderedBy == 3 ) && ! AV16OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [VariavelCocomo_Sigla]";
         }
         else if ( ( AV15OrderedBy == 3 ) && ( AV16OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [VariavelCocomo_Sigla] DESC";
         }
         else if ( ( AV15OrderedBy == 4 ) && ! AV16OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [VariavelCocomo_Tipo]";
         }
         else if ( ( AV15OrderedBy == 4 ) && ( AV16OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [VariavelCocomo_Tipo] DESC";
         }
         else if ( ( AV15OrderedBy == 5 ) && ! AV16OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [VariavelCocomo_ProjetoCod]";
         }
         else if ( ( AV15OrderedBy == 5 ) && ( AV16OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [VariavelCocomo_ProjetoCod] DESC";
         }
         else if ( ( AV15OrderedBy == 6 ) && ! AV16OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [VariavelCocomo_Data]";
         }
         else if ( ( AV15OrderedBy == 6 ) && ( AV16OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [VariavelCocomo_Data] DESC";
         }
         else if ( ( AV15OrderedBy == 7 ) && ! AV16OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [VariavelCocomo_ExtraBaixo]";
         }
         else if ( ( AV15OrderedBy == 7 ) && ( AV16OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [VariavelCocomo_ExtraBaixo] DESC";
         }
         else if ( ( AV15OrderedBy == 8 ) && ! AV16OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [VariavelCocomo_MuitoBaixo]";
         }
         else if ( ( AV15OrderedBy == 8 ) && ( AV16OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [VariavelCocomo_MuitoBaixo] DESC";
         }
         else if ( ( AV15OrderedBy == 9 ) && ! AV16OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [VariavelCocomo_Baixo]";
         }
         else if ( ( AV15OrderedBy == 9 ) && ( AV16OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [VariavelCocomo_Baixo] DESC";
         }
         else if ( ( AV15OrderedBy == 10 ) && ! AV16OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [VariavelCocomo_Nominal]";
         }
         else if ( ( AV15OrderedBy == 10 ) && ( AV16OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [VariavelCocomo_Nominal] DESC";
         }
         else if ( ( AV15OrderedBy == 11 ) && ! AV16OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [VariavelCocomo_Alto]";
         }
         else if ( ( AV15OrderedBy == 11 ) && ( AV16OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [VariavelCocomo_Alto] DESC";
         }
         else if ( ( AV15OrderedBy == 12 ) && ! AV16OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [VariavelCocomo_MuitoAlto]";
         }
         else if ( ( AV15OrderedBy == 12 ) && ( AV16OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [VariavelCocomo_MuitoAlto] DESC";
         }
         else if ( ( AV15OrderedBy == 13 ) && ! AV16OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [VariavelCocomo_ExtraAlto]";
         }
         else if ( ( AV15OrderedBy == 13 ) && ( AV16OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [VariavelCocomo_ExtraAlto] DESC";
         }
         else if ( ( AV15OrderedBy == 14 ) && ! AV16OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [VariavelCocomo_Usado]";
         }
         else if ( ( AV15OrderedBy == 14 ) && ( AV16OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [VariavelCocomo_Usado] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [VariavelCocomo_AreaTrabalhoCod], [VariavelCocomo_Sigla], [VariavelCocomo_Tipo], [VariavelCocomo_Sequencial]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00GL3( IGxContext context ,
                                             String A964VariavelCocomo_Tipo ,
                                             IGxCollection AV48TFVariavelCocomo_Tipo_Sels ,
                                             String AV17DynamicFiltersSelector1 ,
                                             short AV18DynamicFiltersOperator1 ,
                                             String AV19VariavelCocomo_Nome1 ,
                                             bool AV20DynamicFiltersEnabled2 ,
                                             String AV21DynamicFiltersSelector2 ,
                                             short AV22DynamicFiltersOperator2 ,
                                             String AV23VariavelCocomo_Nome2 ,
                                             bool AV24DynamicFiltersEnabled3 ,
                                             String AV25DynamicFiltersSelector3 ,
                                             short AV26DynamicFiltersOperator3 ,
                                             String AV27VariavelCocomo_Nome3 ,
                                             int AV35TFVariavelCocomo_AreaTrabalhoCod ,
                                             int AV36TFVariavelCocomo_AreaTrabalhoCod_To ,
                                             String AV40TFVariavelCocomo_Sigla_Sel ,
                                             String AV39TFVariavelCocomo_Sigla ,
                                             String AV44TFVariavelCocomo_Nome_Sel ,
                                             String AV43TFVariavelCocomo_Nome ,
                                             int AV48TFVariavelCocomo_Tipo_Sels_Count ,
                                             int AV51TFVariavelCocomo_ProjetoCod ,
                                             int AV52TFVariavelCocomo_ProjetoCod_To ,
                                             DateTime AV55TFVariavelCocomo_Data ,
                                             DateTime AV56TFVariavelCocomo_Data_To ,
                                             decimal AV61TFVariavelCocomo_ExtraBaixo ,
                                             decimal AV62TFVariavelCocomo_ExtraBaixo_To ,
                                             decimal AV65TFVariavelCocomo_MuitoBaixo ,
                                             decimal AV66TFVariavelCocomo_MuitoBaixo_To ,
                                             decimal AV69TFVariavelCocomo_Baixo ,
                                             decimal AV70TFVariavelCocomo_Baixo_To ,
                                             decimal AV73TFVariavelCocomo_Nominal ,
                                             decimal AV74TFVariavelCocomo_Nominal_To ,
                                             decimal AV77TFVariavelCocomo_Alto ,
                                             decimal AV78TFVariavelCocomo_Alto_To ,
                                             decimal AV81TFVariavelCocomo_MuitoAlto ,
                                             decimal AV82TFVariavelCocomo_MuitoAlto_To ,
                                             decimal AV85TFVariavelCocomo_ExtraAlto ,
                                             decimal AV86TFVariavelCocomo_ExtraAlto_To ,
                                             decimal AV89TFVariavelCocomo_Usado ,
                                             decimal AV90TFVariavelCocomo_Usado_To ,
                                             String A963VariavelCocomo_Nome ,
                                             int A961VariavelCocomo_AreaTrabalhoCod ,
                                             String A962VariavelCocomo_Sigla ,
                                             int A965VariavelCocomo_ProjetoCod ,
                                             DateTime A966VariavelCocomo_Data ,
                                             decimal A986VariavelCocomo_ExtraBaixo ,
                                             decimal A967VariavelCocomo_MuitoBaixo ,
                                             decimal A968VariavelCocomo_Baixo ,
                                             decimal A969VariavelCocomo_Nominal ,
                                             decimal A970VariavelCocomo_Alto ,
                                             decimal A971VariavelCocomo_MuitoAlto ,
                                             decimal A972VariavelCocomo_ExtraAlto ,
                                             decimal A973VariavelCocomo_Usado ,
                                             short AV15OrderedBy ,
                                             bool AV16OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [32] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [VariaveisCocomo] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "VARIAVELCOCOMO_NOME") == 0 ) && ( AV18DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19VariavelCocomo_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nome] like @lV19VariavelCocomo_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nome] like @lV19VariavelCocomo_Nome1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "VARIAVELCOCOMO_NOME") == 0 ) && ( AV18DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19VariavelCocomo_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nome] like '%' + @lV19VariavelCocomo_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nome] like '%' + @lV19VariavelCocomo_Nome1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "VARIAVELCOCOMO_NOME") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23VariavelCocomo_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nome] like @lV23VariavelCocomo_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nome] like @lV23VariavelCocomo_Nome2)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "VARIAVELCOCOMO_NOME") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23VariavelCocomo_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nome] like '%' + @lV23VariavelCocomo_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nome] like '%' + @lV23VariavelCocomo_Nome2)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "VARIAVELCOCOMO_NOME") == 0 ) && ( AV26DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27VariavelCocomo_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nome] like @lV27VariavelCocomo_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nome] like @lV27VariavelCocomo_Nome3)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "VARIAVELCOCOMO_NOME") == 0 ) && ( AV26DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27VariavelCocomo_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nome] like '%' + @lV27VariavelCocomo_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nome] like '%' + @lV27VariavelCocomo_Nome3)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! (0==AV35TFVariavelCocomo_AreaTrabalhoCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_AreaTrabalhoCod] >= @AV35TFVariavelCocomo_AreaTrabalhoCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_AreaTrabalhoCod] >= @AV35TFVariavelCocomo_AreaTrabalhoCod)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! (0==AV36TFVariavelCocomo_AreaTrabalhoCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_AreaTrabalhoCod] <= @AV36TFVariavelCocomo_AreaTrabalhoCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_AreaTrabalhoCod] <= @AV36TFVariavelCocomo_AreaTrabalhoCod_To)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV40TFVariavelCocomo_Sigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFVariavelCocomo_Sigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Sigla] like @lV39TFVariavelCocomo_Sigla)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Sigla] like @lV39TFVariavelCocomo_Sigla)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFVariavelCocomo_Sigla_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Sigla] = @AV40TFVariavelCocomo_Sigla_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Sigla] = @AV40TFVariavelCocomo_Sigla_Sel)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV44TFVariavelCocomo_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFVariavelCocomo_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nome] like @lV43TFVariavelCocomo_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nome] like @lV43TFVariavelCocomo_Nome)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFVariavelCocomo_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nome] = @AV44TFVariavelCocomo_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nome] = @AV44TFVariavelCocomo_Nome_Sel)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( AV48TFVariavelCocomo_Tipo_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV48TFVariavelCocomo_Tipo_Sels, "[VariavelCocomo_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV48TFVariavelCocomo_Tipo_Sels, "[VariavelCocomo_Tipo] IN (", ")") + ")";
            }
         }
         if ( ! (0==AV51TFVariavelCocomo_ProjetoCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_ProjetoCod] >= @AV51TFVariavelCocomo_ProjetoCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_ProjetoCod] >= @AV51TFVariavelCocomo_ProjetoCod)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ! (0==AV52TFVariavelCocomo_ProjetoCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_ProjetoCod] <= @AV52TFVariavelCocomo_ProjetoCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_ProjetoCod] <= @AV52TFVariavelCocomo_ProjetoCod_To)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( ! (DateTime.MinValue==AV55TFVariavelCocomo_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Data] >= @AV55TFVariavelCocomo_Data)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Data] >= @AV55TFVariavelCocomo_Data)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV56TFVariavelCocomo_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Data] <= @AV56TFVariavelCocomo_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Data] <= @AV56TFVariavelCocomo_Data_To)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV61TFVariavelCocomo_ExtraBaixo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_ExtraBaixo] >= @AV61TFVariavelCocomo_ExtraBaixo)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_ExtraBaixo] >= @AV61TFVariavelCocomo_ExtraBaixo)";
            }
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV62TFVariavelCocomo_ExtraBaixo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_ExtraBaixo] <= @AV62TFVariavelCocomo_ExtraBaixo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_ExtraBaixo] <= @AV62TFVariavelCocomo_ExtraBaixo_To)";
            }
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV65TFVariavelCocomo_MuitoBaixo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_MuitoBaixo] >= @AV65TFVariavelCocomo_MuitoBaixo)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_MuitoBaixo] >= @AV65TFVariavelCocomo_MuitoBaixo)";
            }
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV66TFVariavelCocomo_MuitoBaixo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_MuitoBaixo] <= @AV66TFVariavelCocomo_MuitoBaixo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_MuitoBaixo] <= @AV66TFVariavelCocomo_MuitoBaixo_To)";
            }
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV69TFVariavelCocomo_Baixo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Baixo] >= @AV69TFVariavelCocomo_Baixo)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Baixo] >= @AV69TFVariavelCocomo_Baixo)";
            }
         }
         else
         {
            GXv_int4[20] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV70TFVariavelCocomo_Baixo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Baixo] <= @AV70TFVariavelCocomo_Baixo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Baixo] <= @AV70TFVariavelCocomo_Baixo_To)";
            }
         }
         else
         {
            GXv_int4[21] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV73TFVariavelCocomo_Nominal) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nominal] >= @AV73TFVariavelCocomo_Nominal)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nominal] >= @AV73TFVariavelCocomo_Nominal)";
            }
         }
         else
         {
            GXv_int4[22] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV74TFVariavelCocomo_Nominal_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nominal] <= @AV74TFVariavelCocomo_Nominal_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nominal] <= @AV74TFVariavelCocomo_Nominal_To)";
            }
         }
         else
         {
            GXv_int4[23] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV77TFVariavelCocomo_Alto) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Alto] >= @AV77TFVariavelCocomo_Alto)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Alto] >= @AV77TFVariavelCocomo_Alto)";
            }
         }
         else
         {
            GXv_int4[24] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV78TFVariavelCocomo_Alto_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Alto] <= @AV78TFVariavelCocomo_Alto_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Alto] <= @AV78TFVariavelCocomo_Alto_To)";
            }
         }
         else
         {
            GXv_int4[25] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV81TFVariavelCocomo_MuitoAlto) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_MuitoAlto] >= @AV81TFVariavelCocomo_MuitoAlto)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_MuitoAlto] >= @AV81TFVariavelCocomo_MuitoAlto)";
            }
         }
         else
         {
            GXv_int4[26] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV82TFVariavelCocomo_MuitoAlto_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_MuitoAlto] <= @AV82TFVariavelCocomo_MuitoAlto_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_MuitoAlto] <= @AV82TFVariavelCocomo_MuitoAlto_To)";
            }
         }
         else
         {
            GXv_int4[27] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV85TFVariavelCocomo_ExtraAlto) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_ExtraAlto] >= @AV85TFVariavelCocomo_ExtraAlto)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_ExtraAlto] >= @AV85TFVariavelCocomo_ExtraAlto)";
            }
         }
         else
         {
            GXv_int4[28] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV86TFVariavelCocomo_ExtraAlto_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_ExtraAlto] <= @AV86TFVariavelCocomo_ExtraAlto_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_ExtraAlto] <= @AV86TFVariavelCocomo_ExtraAlto_To)";
            }
         }
         else
         {
            GXv_int4[29] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV89TFVariavelCocomo_Usado) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Usado] >= @AV89TFVariavelCocomo_Usado)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Usado] >= @AV89TFVariavelCocomo_Usado)";
            }
         }
         else
         {
            GXv_int4[30] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV90TFVariavelCocomo_Usado_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Usado] <= @AV90TFVariavelCocomo_Usado_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Usado] <= @AV90TFVariavelCocomo_Usado_To)";
            }
         }
         else
         {
            GXv_int4[31] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV15OrderedBy == 1 ) && ! AV16OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV15OrderedBy == 1 ) && ( AV16OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV15OrderedBy == 2 ) && ! AV16OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV15OrderedBy == 2 ) && ( AV16OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV15OrderedBy == 3 ) && ! AV16OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV15OrderedBy == 3 ) && ( AV16OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV15OrderedBy == 4 ) && ! AV16OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV15OrderedBy == 4 ) && ( AV16OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV15OrderedBy == 5 ) && ! AV16OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV15OrderedBy == 5 ) && ( AV16OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV15OrderedBy == 6 ) && ! AV16OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV15OrderedBy == 6 ) && ( AV16OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV15OrderedBy == 7 ) && ! AV16OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV15OrderedBy == 7 ) && ( AV16OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV15OrderedBy == 8 ) && ! AV16OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV15OrderedBy == 8 ) && ( AV16OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV15OrderedBy == 9 ) && ! AV16OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV15OrderedBy == 9 ) && ( AV16OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV15OrderedBy == 10 ) && ! AV16OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV15OrderedBy == 10 ) && ( AV16OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV15OrderedBy == 11 ) && ! AV16OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV15OrderedBy == 11 ) && ( AV16OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV15OrderedBy == 12 ) && ! AV16OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV15OrderedBy == 12 ) && ( AV16OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV15OrderedBy == 13 ) && ! AV16OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV15OrderedBy == 13 ) && ( AV16OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV15OrderedBy == 14 ) && ! AV16OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV15OrderedBy == 14 ) && ( AV16OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00GL2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (DateTime)dynConstraints[22] , (DateTime)dynConstraints[23] , (decimal)dynConstraints[24] , (decimal)dynConstraints[25] , (decimal)dynConstraints[26] , (decimal)dynConstraints[27] , (decimal)dynConstraints[28] , (decimal)dynConstraints[29] , (decimal)dynConstraints[30] , (decimal)dynConstraints[31] , (decimal)dynConstraints[32] , (decimal)dynConstraints[33] , (decimal)dynConstraints[34] , (decimal)dynConstraints[35] , (decimal)dynConstraints[36] , (decimal)dynConstraints[37] , (decimal)dynConstraints[38] , (decimal)dynConstraints[39] , (String)dynConstraints[40] , (int)dynConstraints[41] , (String)dynConstraints[42] , (int)dynConstraints[43] , (DateTime)dynConstraints[44] , (decimal)dynConstraints[45] , (decimal)dynConstraints[46] , (decimal)dynConstraints[47] , (decimal)dynConstraints[48] , (decimal)dynConstraints[49] , (decimal)dynConstraints[50] , (decimal)dynConstraints[51] , (decimal)dynConstraints[52] , (short)dynConstraints[53] , (bool)dynConstraints[54] );
               case 1 :
                     return conditional_H00GL3(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (DateTime)dynConstraints[22] , (DateTime)dynConstraints[23] , (decimal)dynConstraints[24] , (decimal)dynConstraints[25] , (decimal)dynConstraints[26] , (decimal)dynConstraints[27] , (decimal)dynConstraints[28] , (decimal)dynConstraints[29] , (decimal)dynConstraints[30] , (decimal)dynConstraints[31] , (decimal)dynConstraints[32] , (decimal)dynConstraints[33] , (decimal)dynConstraints[34] , (decimal)dynConstraints[35] , (decimal)dynConstraints[36] , (decimal)dynConstraints[37] , (decimal)dynConstraints[38] , (decimal)dynConstraints[39] , (String)dynConstraints[40] , (int)dynConstraints[41] , (String)dynConstraints[42] , (int)dynConstraints[43] , (DateTime)dynConstraints[44] , (decimal)dynConstraints[45] , (decimal)dynConstraints[46] , (decimal)dynConstraints[47] , (decimal)dynConstraints[48] , (decimal)dynConstraints[49] , (decimal)dynConstraints[50] , (decimal)dynConstraints[51] , (decimal)dynConstraints[52] , (short)dynConstraints[53] , (bool)dynConstraints[54] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00GL2 ;
          prmH00GL2 = new Object[] {
          new Object[] {"@lV19VariavelCocomo_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV19VariavelCocomo_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV23VariavelCocomo_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV23VariavelCocomo_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV27VariavelCocomo_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV27VariavelCocomo_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV35TFVariavelCocomo_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV36TFVariavelCocomo_AreaTrabalhoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV39TFVariavelCocomo_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV40TFVariavelCocomo_Sigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV43TFVariavelCocomo_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV44TFVariavelCocomo_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV51TFVariavelCocomo_ProjetoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV52TFVariavelCocomo_ProjetoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV55TFVariavelCocomo_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV56TFVariavelCocomo_Data_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV61TFVariavelCocomo_ExtraBaixo",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV62TFVariavelCocomo_ExtraBaixo_To",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV65TFVariavelCocomo_MuitoBaixo",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV66TFVariavelCocomo_MuitoBaixo_To",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV69TFVariavelCocomo_Baixo",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV70TFVariavelCocomo_Baixo_To",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV73TFVariavelCocomo_Nominal",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV74TFVariavelCocomo_Nominal_To",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV77TFVariavelCocomo_Alto",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV78TFVariavelCocomo_Alto_To",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV81TFVariavelCocomo_MuitoAlto",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV82TFVariavelCocomo_MuitoAlto_To",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV85TFVariavelCocomo_ExtraAlto",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV86TFVariavelCocomo_ExtraAlto_To",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV89TFVariavelCocomo_Usado",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV90TFVariavelCocomo_Usado_To",SqlDbType.Decimal,12,2} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00GL3 ;
          prmH00GL3 = new Object[] {
          new Object[] {"@lV19VariavelCocomo_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV19VariavelCocomo_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV23VariavelCocomo_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV23VariavelCocomo_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV27VariavelCocomo_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV27VariavelCocomo_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV35TFVariavelCocomo_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV36TFVariavelCocomo_AreaTrabalhoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV39TFVariavelCocomo_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV40TFVariavelCocomo_Sigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV43TFVariavelCocomo_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV44TFVariavelCocomo_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV51TFVariavelCocomo_ProjetoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV52TFVariavelCocomo_ProjetoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV55TFVariavelCocomo_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV56TFVariavelCocomo_Data_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV61TFVariavelCocomo_ExtraBaixo",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV62TFVariavelCocomo_ExtraBaixo_To",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV65TFVariavelCocomo_MuitoBaixo",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV66TFVariavelCocomo_MuitoBaixo_To",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV69TFVariavelCocomo_Baixo",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV70TFVariavelCocomo_Baixo_To",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV73TFVariavelCocomo_Nominal",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV74TFVariavelCocomo_Nominal_To",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV77TFVariavelCocomo_Alto",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV78TFVariavelCocomo_Alto_To",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV81TFVariavelCocomo_MuitoAlto",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV82TFVariavelCocomo_MuitoAlto_To",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV85TFVariavelCocomo_ExtraAlto",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV86TFVariavelCocomo_ExtraAlto_To",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV89TFVariavelCocomo_Usado",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV90TFVariavelCocomo_Usado_To",SqlDbType.Decimal,12,2}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00GL2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00GL2,11,0,true,false )
             ,new CursorDef("H00GL3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00GL3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((decimal[]) buf[7])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((decimal[]) buf[9])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((decimal[]) buf[13])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((decimal[]) buf[15])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[17])[0] = rslt.getGXDate(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((int[]) buf[19])[0] = rslt.getInt(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((String[]) buf[21])[0] = rslt.getString(12, 1) ;
                ((String[]) buf[22])[0] = rslt.getString(13, 50) ;
                ((String[]) buf[23])[0] = rslt.getString(14, 15) ;
                ((int[]) buf[24])[0] = rslt.getInt(15) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[50]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[51]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[52]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[53]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[54]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[55]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[56]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[57]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[58]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[59]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[60]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[61]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[62]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[63]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[64]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[65]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[66]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[67]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[68]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[69]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[70]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[71]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[72]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[73]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[46]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[47]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[48]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[49]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[50]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[51]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[52]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[53]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[54]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[55]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[56]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[57]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[58]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[59]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[60]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[61]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[62]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[63]);
                }
                return;
       }
    }

 }

}
