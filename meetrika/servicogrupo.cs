/*
               File: ServicoGrupo
        Description: Grupo de Servi�o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:18:21.88
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class servicogrupo : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7ServicoGrupo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ServicoGrupo_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSERVICOGRUPO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ServicoGrupo_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         chkServicoGrupo_FlagRequerSoft.Name = "SERVICOGRUPO_FLAGREQUERSOFT";
         chkServicoGrupo_FlagRequerSoft.WebTags = "";
         chkServicoGrupo_FlagRequerSoft.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkServicoGrupo_FlagRequerSoft_Internalname, "TitleCaption", chkServicoGrupo_FlagRequerSoft.Caption);
         chkServicoGrupo_FlagRequerSoft.CheckedValue = "false";
         chkServicoGrupo_Ativo.Name = "SERVICOGRUPO_ATIVO";
         chkServicoGrupo_Ativo.WebTags = "";
         chkServicoGrupo_Ativo.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkServicoGrupo_Ativo_Internalname, "TitleCaption", chkServicoGrupo_Ativo.Caption);
         chkServicoGrupo_Ativo.CheckedValue = "false";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Grupo de Servi�o", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtServicoGrupo_Descricao_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public servicogrupo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public servicogrupo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_ServicoGrupo_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7ServicoGrupo_Codigo = aP1_ServicoGrupo_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         chkServicoGrupo_FlagRequerSoft = new GXCheckbox();
         chkServicoGrupo_Ativo = new GXCheckbox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_0W33( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_0W33e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServicoGrupo_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A157ServicoGrupo_Codigo), 6, 0, ",", "")), ((edtServicoGrupo_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A157ServicoGrupo_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A157ServicoGrupo_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServicoGrupo_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtServicoGrupo_Codigo_Visible, edtServicoGrupo_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ServicoGrupo.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_0W33( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_0W33( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_0W33e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_34_0W33( true) ;
         }
         return  ;
      }

      protected void wb_table3_34_0W33e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_0W33e( true) ;
         }
         else
         {
            wb_table1_2_0W33e( false) ;
         }
      }

      protected void wb_table3_34_0W33( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ServicoGrupo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ServicoGrupo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ServicoGrupo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_34_0W33e( true) ;
         }
         else
         {
            wb_table3_34_0W33e( false) ;
         }
      }

      protected void wb_table2_5_0W33( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_UNNAMEDTABLE1Container"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_UNNAMEDTABLE1Container"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_0W33( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_0W33e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_0W33e( true) ;
         }
         else
         {
            wb_table2_5_0W33e( false) ;
         }
      }

      protected void wb_table4_13_0W33( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_16_0W33( true) ;
         }
         return  ;
      }

      protected void wb_table5_16_0W33e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_0W33e( true) ;
         }
         else
         {
            wb_table4_13_0W33e( false) ;
         }
      }

      protected void wb_table5_16_0W33( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservicogrupo_descricao_Internalname, "Nome", "", "", lblTextblockservicogrupo_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoGrupo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtServicoGrupo_Descricao_Internalname, A158ServicoGrupo_Descricao, StringUtil.RTrim( context.localUtil.Format( A158ServicoGrupo_Descricao, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServicoGrupo_Descricao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtServicoGrupo_Descricao_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ServicoGrupo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservicogrupo_flagrequersoft_Internalname, "Requer Software?", "", "", lblTextblockservicogrupo_flagrequersoft_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoGrupo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkServicoGrupo_FlagRequerSoft_Internalname, StringUtil.BoolToStr( A1428ServicoGrupo_FlagRequerSoft), "", "", 1, chkServicoGrupo_FlagRequerSoft.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(26, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,26);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservicogrupo_ativo_Internalname, "Ativo?", "", "", lblTextblockservicogrupo_ativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockservicogrupo_ativo_Visible, 1, 0, "HLP_ServicoGrupo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkServicoGrupo_Ativo_Internalname, StringUtil.BoolToStr( A159ServicoGrupo_Ativo), "", "", chkServicoGrupo_Ativo.Visible, chkServicoGrupo_Ativo.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(31, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_16_0W33e( true) ;
         }
         else
         {
            wb_table5_16_0W33e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E110W2 */
         E110W2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A158ServicoGrupo_Descricao = cgiGet( edtServicoGrupo_Descricao_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A158ServicoGrupo_Descricao", A158ServicoGrupo_Descricao);
               A1428ServicoGrupo_FlagRequerSoft = StringUtil.StrToBool( cgiGet( chkServicoGrupo_FlagRequerSoft_Internalname));
               n1428ServicoGrupo_FlagRequerSoft = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1428ServicoGrupo_FlagRequerSoft", A1428ServicoGrupo_FlagRequerSoft);
               n1428ServicoGrupo_FlagRequerSoft = ((false==A1428ServicoGrupo_FlagRequerSoft) ? true : false);
               A159ServicoGrupo_Ativo = StringUtil.StrToBool( cgiGet( chkServicoGrupo_Ativo_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A159ServicoGrupo_Ativo", A159ServicoGrupo_Ativo);
               A157ServicoGrupo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtServicoGrupo_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A157ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0)));
               /* Read saved values. */
               Z157ServicoGrupo_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z157ServicoGrupo_Codigo"), ",", "."));
               Z158ServicoGrupo_Descricao = cgiGet( "Z158ServicoGrupo_Descricao");
               Z1428ServicoGrupo_FlagRequerSoft = StringUtil.StrToBool( cgiGet( "Z1428ServicoGrupo_FlagRequerSoft"));
               n1428ServicoGrupo_FlagRequerSoft = ((false==A1428ServicoGrupo_FlagRequerSoft) ? true : false);
               Z159ServicoGrupo_Ativo = StringUtil.StrToBool( cgiGet( "Z159ServicoGrupo_Ativo"));
               O158ServicoGrupo_Descricao = cgiGet( "O158ServicoGrupo_Descricao");
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               AV7ServicoGrupo_Codigo = (int)(context.localUtil.CToN( cgiGet( "vSERVICOGRUPO_CODIGO"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_unnamedtable1_Width = cgiGet( "DVPANEL_UNNAMEDTABLE1_Width");
               Dvpanel_unnamedtable1_Height = cgiGet( "DVPANEL_UNNAMEDTABLE1_Height");
               Dvpanel_unnamedtable1_Cls = cgiGet( "DVPANEL_UNNAMEDTABLE1_Cls");
               Dvpanel_unnamedtable1_Title = cgiGet( "DVPANEL_UNNAMEDTABLE1_Title");
               Dvpanel_unnamedtable1_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_UNNAMEDTABLE1_Collapsible"));
               Dvpanel_unnamedtable1_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_UNNAMEDTABLE1_Collapsed"));
               Dvpanel_unnamedtable1_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_UNNAMEDTABLE1_Enabled"));
               Dvpanel_unnamedtable1_Class = cgiGet( "DVPANEL_UNNAMEDTABLE1_Class");
               Dvpanel_unnamedtable1_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_UNNAMEDTABLE1_Autowidth"));
               Dvpanel_unnamedtable1_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_UNNAMEDTABLE1_Autoheight"));
               Dvpanel_unnamedtable1_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_UNNAMEDTABLE1_Showheader"));
               Dvpanel_unnamedtable1_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_UNNAMEDTABLE1_Showcollapseicon"));
               Dvpanel_unnamedtable1_Iconposition = cgiGet( "DVPANEL_UNNAMEDTABLE1_Iconposition");
               Dvpanel_unnamedtable1_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_UNNAMEDTABLE1_Autoscroll"));
               Dvpanel_unnamedtable1_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_UNNAMEDTABLE1_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "ServicoGrupo";
               A157ServicoGrupo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtServicoGrupo_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A157ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A157ServicoGrupo_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A157ServicoGrupo_Codigo != Z157ServicoGrupo_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("servicogrupo:[SecurityCheckFailed value for]"+"ServicoGrupo_Codigo:"+context.localUtil.Format( (decimal)(A157ServicoGrupo_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("servicogrupo:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A157ServicoGrupo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A157ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode33 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode33;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound33 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_0W0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "SERVICOGRUPO_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtServicoGrupo_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E110W2 */
                           E110W2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E120W2 */
                           E120W2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E120W2 */
            E120W2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll0W33( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes0W33( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_0W0( )
      {
         BeforeValidate0W33( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls0W33( ) ;
            }
            else
            {
               CheckExtendedTable0W33( ) ;
               CloseExtendedTableCursors0W33( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption0W0( )
      {
      }

      protected void E110W2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         edtServicoGrupo_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServicoGrupo_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServicoGrupo_Codigo_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            lblTextblockservicogrupo_ativo_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockservicogrupo_ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockservicogrupo_ativo_Visible), 5, 0)));
            chkServicoGrupo_Ativo.Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkServicoGrupo_Ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkServicoGrupo_Ativo.Visible), 5, 0)));
         }
      }

      protected void E120W2( )
      {
         /* After Trn Routine */
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            context.wjLoc = formatLink("viewservicogrupo.aspx") + "?" + UrlEncode("" +A157ServicoGrupo_Codigo) + "," + UrlEncode(StringUtil.RTrim("Servico"));
            context.wjLocDisableFrm = 1;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwservicogrupo.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM0W33( short GX_JID )
      {
         if ( ( GX_JID == 8 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z158ServicoGrupo_Descricao = T000W3_A158ServicoGrupo_Descricao[0];
               Z1428ServicoGrupo_FlagRequerSoft = T000W3_A1428ServicoGrupo_FlagRequerSoft[0];
               Z159ServicoGrupo_Ativo = T000W3_A159ServicoGrupo_Ativo[0];
            }
            else
            {
               Z158ServicoGrupo_Descricao = A158ServicoGrupo_Descricao;
               Z1428ServicoGrupo_FlagRequerSoft = A1428ServicoGrupo_FlagRequerSoft;
               Z159ServicoGrupo_Ativo = A159ServicoGrupo_Ativo;
            }
         }
         if ( GX_JID == -8 )
         {
            Z157ServicoGrupo_Codigo = A157ServicoGrupo_Codigo;
            Z158ServicoGrupo_Descricao = A158ServicoGrupo_Descricao;
            Z1428ServicoGrupo_FlagRequerSoft = A1428ServicoGrupo_FlagRequerSoft;
            Z159ServicoGrupo_Ativo = A159ServicoGrupo_Ativo;
         }
      }

      protected void standaloneNotModal( )
      {
         edtServicoGrupo_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServicoGrupo_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServicoGrupo_Codigo_Enabled), 5, 0)));
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         edtServicoGrupo_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServicoGrupo_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServicoGrupo_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7ServicoGrupo_Codigo) )
         {
            A157ServicoGrupo_Codigo = AV7ServicoGrupo_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A157ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0)));
         }
      }

      protected void standaloneModal( )
      {
         chkServicoGrupo_Ativo.Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkServicoGrupo_Ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkServicoGrupo_Ativo.Visible), 5, 0)));
         lblTextblockservicogrupo_ativo_Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockservicogrupo_ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockservicogrupo_ativo_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A159ServicoGrupo_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A159ServicoGrupo_Ativo = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A159ServicoGrupo_Ativo", A159ServicoGrupo_Ativo);
         }
      }

      protected void Load0W33( )
      {
         /* Using cursor T000W4 */
         pr_default.execute(2, new Object[] {A157ServicoGrupo_Codigo});
         if ( (pr_default.getStatus(2) != 101) )
         {
            RcdFound33 = 1;
            A158ServicoGrupo_Descricao = T000W4_A158ServicoGrupo_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A158ServicoGrupo_Descricao", A158ServicoGrupo_Descricao);
            A1428ServicoGrupo_FlagRequerSoft = T000W4_A1428ServicoGrupo_FlagRequerSoft[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1428ServicoGrupo_FlagRequerSoft", A1428ServicoGrupo_FlagRequerSoft);
            n1428ServicoGrupo_FlagRequerSoft = T000W4_n1428ServicoGrupo_FlagRequerSoft[0];
            A159ServicoGrupo_Ativo = T000W4_A159ServicoGrupo_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A159ServicoGrupo_Ativo", A159ServicoGrupo_Ativo);
            ZM0W33( -8) ;
         }
         pr_default.close(2);
         OnLoadActions0W33( ) ;
      }

      protected void OnLoadActions0W33( )
      {
      }

      protected void CheckExtendedTable0W33( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A158ServicoGrupo_Descricao)) )
         {
            GX_msglist.addItem("A Descri��o do Grupo � obrigatorio!", 1, "SERVICOGRUPO_DESCRICAO");
            AnyError = 1;
            GX_FocusControl = edtServicoGrupo_Descricao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors0W33( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey0W33( )
      {
         /* Using cursor T000W5 */
         pr_default.execute(3, new Object[] {A157ServicoGrupo_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound33 = 1;
         }
         else
         {
            RcdFound33 = 0;
         }
         pr_default.close(3);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T000W3 */
         pr_default.execute(1, new Object[] {A157ServicoGrupo_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM0W33( 8) ;
            RcdFound33 = 1;
            A157ServicoGrupo_Codigo = T000W3_A157ServicoGrupo_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A157ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0)));
            A158ServicoGrupo_Descricao = T000W3_A158ServicoGrupo_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A158ServicoGrupo_Descricao", A158ServicoGrupo_Descricao);
            A1428ServicoGrupo_FlagRequerSoft = T000W3_A1428ServicoGrupo_FlagRequerSoft[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1428ServicoGrupo_FlagRequerSoft", A1428ServicoGrupo_FlagRequerSoft);
            n1428ServicoGrupo_FlagRequerSoft = T000W3_n1428ServicoGrupo_FlagRequerSoft[0];
            A159ServicoGrupo_Ativo = T000W3_A159ServicoGrupo_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A159ServicoGrupo_Ativo", A159ServicoGrupo_Ativo);
            O158ServicoGrupo_Descricao = A158ServicoGrupo_Descricao;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A158ServicoGrupo_Descricao", A158ServicoGrupo_Descricao);
            Z157ServicoGrupo_Codigo = A157ServicoGrupo_Codigo;
            sMode33 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load0W33( ) ;
            if ( AnyError == 1 )
            {
               RcdFound33 = 0;
               InitializeNonKey0W33( ) ;
            }
            Gx_mode = sMode33;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound33 = 0;
            InitializeNonKey0W33( ) ;
            sMode33 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode33;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey0W33( ) ;
         if ( RcdFound33 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound33 = 0;
         /* Using cursor T000W6 */
         pr_default.execute(4, new Object[] {A157ServicoGrupo_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            while ( (pr_default.getStatus(4) != 101) && ( ( T000W6_A157ServicoGrupo_Codigo[0] < A157ServicoGrupo_Codigo ) ) )
            {
               pr_default.readNext(4);
            }
            if ( (pr_default.getStatus(4) != 101) && ( ( T000W6_A157ServicoGrupo_Codigo[0] > A157ServicoGrupo_Codigo ) ) )
            {
               A157ServicoGrupo_Codigo = T000W6_A157ServicoGrupo_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A157ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0)));
               RcdFound33 = 1;
            }
         }
         pr_default.close(4);
      }

      protected void move_previous( )
      {
         RcdFound33 = 0;
         /* Using cursor T000W7 */
         pr_default.execute(5, new Object[] {A157ServicoGrupo_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            while ( (pr_default.getStatus(5) != 101) && ( ( T000W7_A157ServicoGrupo_Codigo[0] > A157ServicoGrupo_Codigo ) ) )
            {
               pr_default.readNext(5);
            }
            if ( (pr_default.getStatus(5) != 101) && ( ( T000W7_A157ServicoGrupo_Codigo[0] < A157ServicoGrupo_Codigo ) ) )
            {
               A157ServicoGrupo_Codigo = T000W7_A157ServicoGrupo_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A157ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0)));
               RcdFound33 = 1;
            }
         }
         pr_default.close(5);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey0W33( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtServicoGrupo_Descricao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert0W33( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound33 == 1 )
            {
               if ( A157ServicoGrupo_Codigo != Z157ServicoGrupo_Codigo )
               {
                  A157ServicoGrupo_Codigo = Z157ServicoGrupo_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A157ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "SERVICOGRUPO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtServicoGrupo_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtServicoGrupo_Descricao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update0W33( ) ;
                  GX_FocusControl = edtServicoGrupo_Descricao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A157ServicoGrupo_Codigo != Z157ServicoGrupo_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtServicoGrupo_Descricao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert0W33( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "SERVICOGRUPO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtServicoGrupo_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtServicoGrupo_Descricao_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert0W33( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A157ServicoGrupo_Codigo != Z157ServicoGrupo_Codigo )
         {
            A157ServicoGrupo_Codigo = Z157ServicoGrupo_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A157ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "SERVICOGRUPO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtServicoGrupo_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtServicoGrupo_Descricao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency0W33( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T000W2 */
            pr_default.execute(0, new Object[] {A157ServicoGrupo_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ServicoGrupo"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z158ServicoGrupo_Descricao, T000W2_A158ServicoGrupo_Descricao[0]) != 0 ) || ( Z1428ServicoGrupo_FlagRequerSoft != T000W2_A1428ServicoGrupo_FlagRequerSoft[0] ) || ( Z159ServicoGrupo_Ativo != T000W2_A159ServicoGrupo_Ativo[0] ) )
            {
               if ( StringUtil.StrCmp(Z158ServicoGrupo_Descricao, T000W2_A158ServicoGrupo_Descricao[0]) != 0 )
               {
                  GXUtil.WriteLog("servicogrupo:[seudo value changed for attri]"+"ServicoGrupo_Descricao");
                  GXUtil.WriteLogRaw("Old: ",Z158ServicoGrupo_Descricao);
                  GXUtil.WriteLogRaw("Current: ",T000W2_A158ServicoGrupo_Descricao[0]);
               }
               if ( Z1428ServicoGrupo_FlagRequerSoft != T000W2_A1428ServicoGrupo_FlagRequerSoft[0] )
               {
                  GXUtil.WriteLog("servicogrupo:[seudo value changed for attri]"+"ServicoGrupo_FlagRequerSoft");
                  GXUtil.WriteLogRaw("Old: ",Z1428ServicoGrupo_FlagRequerSoft);
                  GXUtil.WriteLogRaw("Current: ",T000W2_A1428ServicoGrupo_FlagRequerSoft[0]);
               }
               if ( Z159ServicoGrupo_Ativo != T000W2_A159ServicoGrupo_Ativo[0] )
               {
                  GXUtil.WriteLog("servicogrupo:[seudo value changed for attri]"+"ServicoGrupo_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z159ServicoGrupo_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T000W2_A159ServicoGrupo_Ativo[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ServicoGrupo"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0W33( )
      {
         BeforeValidate0W33( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0W33( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0W33( 0) ;
            CheckOptimisticConcurrency0W33( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0W33( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0W33( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000W8 */
                     pr_default.execute(6, new Object[] {A158ServicoGrupo_Descricao, n1428ServicoGrupo_FlagRequerSoft, A1428ServicoGrupo_FlagRequerSoft, A159ServicoGrupo_Ativo});
                     A157ServicoGrupo_Codigo = T000W8_A157ServicoGrupo_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A157ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0)));
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("ServicoGrupo") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption0W0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0W33( ) ;
            }
            EndLevel0W33( ) ;
         }
         CloseExtendedTableCursors0W33( ) ;
      }

      protected void Update0W33( )
      {
         BeforeValidate0W33( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0W33( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0W33( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0W33( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0W33( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000W9 */
                     pr_default.execute(7, new Object[] {A158ServicoGrupo_Descricao, n1428ServicoGrupo_FlagRequerSoft, A1428ServicoGrupo_FlagRequerSoft, A159ServicoGrupo_Ativo, A157ServicoGrupo_Codigo});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("ServicoGrupo") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ServicoGrupo"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0W33( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0W33( ) ;
         }
         CloseExtendedTableCursors0W33( ) ;
      }

      protected void DeferredUpdate0W33( )
      {
      }

      protected void delete( )
      {
         BeforeValidate0W33( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0W33( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0W33( ) ;
            AfterConfirm0W33( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0W33( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000W10 */
                  pr_default.execute(8, new Object[] {A157ServicoGrupo_Codigo});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("ServicoGrupo") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode33 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel0W33( ) ;
         Gx_mode = sMode33;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls0W33( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
         if ( AnyError == 0 )
         {
            /* Using cursor T000W11 */
            pr_default.execute(9, new Object[] {A157ServicoGrupo_Codigo});
            if ( (pr_default.getStatus(9) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Servico"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(9);
         }
      }

      protected void EndLevel0W33( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0W33( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "ServicoGrupo");
            if ( AnyError == 0 )
            {
               ConfirmValues0W0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "ServicoGrupo");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart0W33( )
      {
         /* Scan By routine */
         /* Using cursor T000W12 */
         pr_default.execute(10);
         RcdFound33 = 0;
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound33 = 1;
            A157ServicoGrupo_Codigo = T000W12_A157ServicoGrupo_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A157ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext0W33( )
      {
         /* Scan next routine */
         pr_default.readNext(10);
         RcdFound33 = 0;
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound33 = 1;
            A157ServicoGrupo_Codigo = T000W12_A157ServicoGrupo_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A157ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd0W33( )
      {
         pr_default.close(10);
      }

      protected void AfterConfirm0W33( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0W33( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0W33( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0W33( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0W33( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0W33( )
      {
         /* Before Validate Rules */
         if ( ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  || ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  ) && ( StringUtil.StrCmp(A158ServicoGrupo_Descricao, O158ServicoGrupo_Descricao) != 0 ) && new prc_existenomecadastrado(context).executeUdp(  "GrpSrv",  A158ServicoGrupo_Descricao) )
         {
            GX_msglist.addItem("Essa descri��o j� foi cadastrada!", 1, "SERVICOGRUPO_DESCRICAO");
            AnyError = 1;
            GX_FocusControl = edtServicoGrupo_Descricao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void DisableAttributes0W33( )
      {
         edtServicoGrupo_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServicoGrupo_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServicoGrupo_Descricao_Enabled), 5, 0)));
         chkServicoGrupo_FlagRequerSoft.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkServicoGrupo_FlagRequerSoft_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkServicoGrupo_FlagRequerSoft.Enabled), 5, 0)));
         chkServicoGrupo_Ativo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkServicoGrupo_Ativo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkServicoGrupo_Ativo.Enabled), 5, 0)));
         edtServicoGrupo_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServicoGrupo_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServicoGrupo_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues0W0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117182282");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("servicogrupo.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ServicoGrupo_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z157ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z157ServicoGrupo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z158ServicoGrupo_Descricao", Z158ServicoGrupo_Descricao);
         GxWebStd.gx_boolean_hidden_field( context, "Z1428ServicoGrupo_FlagRequerSoft", Z1428ServicoGrupo_FlagRequerSoft);
         GxWebStd.gx_boolean_hidden_field( context, "Z159ServicoGrupo_Ativo", Z159ServicoGrupo_Ativo);
         GxWebStd.gx_hidden_field( context, "O158ServicoGrupo_Descricao", O158ServicoGrupo_Descricao);
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vSERVICOGRUPO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ServicoGrupo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vSERVICOGRUPO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ServicoGrupo_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Width", StringUtil.RTrim( Dvpanel_unnamedtable1_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Cls", StringUtil.RTrim( Dvpanel_unnamedtable1_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Title", StringUtil.RTrim( Dvpanel_unnamedtable1_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Collapsible", StringUtil.BoolToStr( Dvpanel_unnamedtable1_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Collapsed", StringUtil.BoolToStr( Dvpanel_unnamedtable1_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Enabled", StringUtil.BoolToStr( Dvpanel_unnamedtable1_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Autowidth", StringUtil.BoolToStr( Dvpanel_unnamedtable1_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Autoheight", StringUtil.BoolToStr( Dvpanel_unnamedtable1_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_unnamedtable1_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Iconposition", StringUtil.RTrim( Dvpanel_unnamedtable1_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Autoscroll", StringUtil.BoolToStr( Dvpanel_unnamedtable1_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ServicoGrupo";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A157ServicoGrupo_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("servicogrupo:[SendSecurityCheck value for]"+"ServicoGrupo_Codigo:"+context.localUtil.Format( (decimal)(A157ServicoGrupo_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("servicogrupo:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("servicogrupo.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ServicoGrupo_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "ServicoGrupo" ;
      }

      public override String GetPgmdesc( )
      {
         return "Grupo de Servi�o" ;
      }

      protected void InitializeNonKey0W33( )
      {
         A158ServicoGrupo_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A158ServicoGrupo_Descricao", A158ServicoGrupo_Descricao);
         A1428ServicoGrupo_FlagRequerSoft = false;
         n1428ServicoGrupo_FlagRequerSoft = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1428ServicoGrupo_FlagRequerSoft", A1428ServicoGrupo_FlagRequerSoft);
         n1428ServicoGrupo_FlagRequerSoft = ((false==A1428ServicoGrupo_FlagRequerSoft) ? true : false);
         A159ServicoGrupo_Ativo = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A159ServicoGrupo_Ativo", A159ServicoGrupo_Ativo);
         O158ServicoGrupo_Descricao = A158ServicoGrupo_Descricao;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A158ServicoGrupo_Descricao", A158ServicoGrupo_Descricao);
         Z158ServicoGrupo_Descricao = "";
         Z1428ServicoGrupo_FlagRequerSoft = false;
         Z159ServicoGrupo_Ativo = false;
      }

      protected void InitAll0W33( )
      {
         A157ServicoGrupo_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A157ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0)));
         InitializeNonKey0W33( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A159ServicoGrupo_Ativo = i159ServicoGrupo_Ativo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A159ServicoGrupo_Ativo", A159ServicoGrupo_Ativo);
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311718231");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("servicogrupo.js", "?2020311718231");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockservicogrupo_descricao_Internalname = "TEXTBLOCKSERVICOGRUPO_DESCRICAO";
         edtServicoGrupo_Descricao_Internalname = "SERVICOGRUPO_DESCRICAO";
         lblTextblockservicogrupo_flagrequersoft_Internalname = "TEXTBLOCKSERVICOGRUPO_FLAGREQUERSOFT";
         chkServicoGrupo_FlagRequerSoft_Internalname = "SERVICOGRUPO_FLAGREQUERSOFT";
         lblTextblockservicogrupo_ativo_Internalname = "TEXTBLOCKSERVICOGRUPO_ATIVO";
         chkServicoGrupo_Ativo_Internalname = "SERVICOGRUPO_ATIVO";
         tblUnnamedtable2_Internalname = "UNNAMEDTABLE2";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         Dvpanel_unnamedtable1_Internalname = "DVPANEL_UNNAMEDTABLE1";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtServicoGrupo_Codigo_Internalname = "SERVICOGRUPO_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_unnamedtable1_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_unnamedtable1_Iconposition = "left";
         Dvpanel_unnamedtable1_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_unnamedtable1_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_unnamedtable1_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_unnamedtable1_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_unnamedtable1_Collapsible = Convert.ToBoolean( -1);
         Dvpanel_unnamedtable1_Title = "Grupo de Servi�os";
         Dvpanel_unnamedtable1_Cls = "GXUI-DVelop-Panel";
         Dvpanel_unnamedtable1_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Grupo de Servi�o";
         chkServicoGrupo_Ativo.Enabled = 1;
         chkServicoGrupo_Ativo.Visible = 1;
         lblTextblockservicogrupo_ativo_Visible = 1;
         chkServicoGrupo_FlagRequerSoft.Enabled = 1;
         edtServicoGrupo_Descricao_Jsonclick = "";
         edtServicoGrupo_Descricao_Enabled = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtServicoGrupo_Codigo_Jsonclick = "";
         edtServicoGrupo_Codigo_Enabled = 0;
         edtServicoGrupo_Codigo_Visible = 1;
         chkServicoGrupo_Ativo.Caption = "";
         chkServicoGrupo_FlagRequerSoft.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E120W2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z158ServicoGrupo_Descricao = "";
         O158ServicoGrupo_Descricao = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         lblTextblockservicogrupo_descricao_Jsonclick = "";
         A158ServicoGrupo_Descricao = "";
         lblTextblockservicogrupo_flagrequersoft_Jsonclick = "";
         lblTextblockservicogrupo_ativo_Jsonclick = "";
         Dvpanel_unnamedtable1_Height = "";
         Dvpanel_unnamedtable1_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode33 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         T000W4_A157ServicoGrupo_Codigo = new int[1] ;
         T000W4_A158ServicoGrupo_Descricao = new String[] {""} ;
         T000W4_A1428ServicoGrupo_FlagRequerSoft = new bool[] {false} ;
         T000W4_n1428ServicoGrupo_FlagRequerSoft = new bool[] {false} ;
         T000W4_A159ServicoGrupo_Ativo = new bool[] {false} ;
         T000W5_A157ServicoGrupo_Codigo = new int[1] ;
         T000W3_A157ServicoGrupo_Codigo = new int[1] ;
         T000W3_A158ServicoGrupo_Descricao = new String[] {""} ;
         T000W3_A1428ServicoGrupo_FlagRequerSoft = new bool[] {false} ;
         T000W3_n1428ServicoGrupo_FlagRequerSoft = new bool[] {false} ;
         T000W3_A159ServicoGrupo_Ativo = new bool[] {false} ;
         T000W6_A157ServicoGrupo_Codigo = new int[1] ;
         T000W7_A157ServicoGrupo_Codigo = new int[1] ;
         T000W2_A157ServicoGrupo_Codigo = new int[1] ;
         T000W2_A158ServicoGrupo_Descricao = new String[] {""} ;
         T000W2_A1428ServicoGrupo_FlagRequerSoft = new bool[] {false} ;
         T000W2_n1428ServicoGrupo_FlagRequerSoft = new bool[] {false} ;
         T000W2_A159ServicoGrupo_Ativo = new bool[] {false} ;
         T000W8_A157ServicoGrupo_Codigo = new int[1] ;
         T000W11_A155Servico_Codigo = new int[1] ;
         T000W12_A157ServicoGrupo_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.servicogrupo__default(),
            new Object[][] {
                new Object[] {
               T000W2_A157ServicoGrupo_Codigo, T000W2_A158ServicoGrupo_Descricao, T000W2_A1428ServicoGrupo_FlagRequerSoft, T000W2_n1428ServicoGrupo_FlagRequerSoft, T000W2_A159ServicoGrupo_Ativo
               }
               , new Object[] {
               T000W3_A157ServicoGrupo_Codigo, T000W3_A158ServicoGrupo_Descricao, T000W3_A1428ServicoGrupo_FlagRequerSoft, T000W3_n1428ServicoGrupo_FlagRequerSoft, T000W3_A159ServicoGrupo_Ativo
               }
               , new Object[] {
               T000W4_A157ServicoGrupo_Codigo, T000W4_A158ServicoGrupo_Descricao, T000W4_A1428ServicoGrupo_FlagRequerSoft, T000W4_n1428ServicoGrupo_FlagRequerSoft, T000W4_A159ServicoGrupo_Ativo
               }
               , new Object[] {
               T000W5_A157ServicoGrupo_Codigo
               }
               , new Object[] {
               T000W6_A157ServicoGrupo_Codigo
               }
               , new Object[] {
               T000W7_A157ServicoGrupo_Codigo
               }
               , new Object[] {
               T000W8_A157ServicoGrupo_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000W11_A155Servico_Codigo
               }
               , new Object[] {
               T000W12_A157ServicoGrupo_Codigo
               }
            }
         );
         Z159ServicoGrupo_Ativo = true;
         A159ServicoGrupo_Ativo = true;
         i159ServicoGrupo_Ativo = true;
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short Gx_BScreen ;
      private short RcdFound33 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private int wcpOAV7ServicoGrupo_Codigo ;
      private int Z157ServicoGrupo_Codigo ;
      private int AV7ServicoGrupo_Codigo ;
      private int trnEnded ;
      private int A157ServicoGrupo_Codigo ;
      private int edtServicoGrupo_Codigo_Enabled ;
      private int edtServicoGrupo_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int edtServicoGrupo_Descricao_Enabled ;
      private int lblTextblockservicogrupo_ativo_Visible ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String chkServicoGrupo_FlagRequerSoft_Internalname ;
      private String chkServicoGrupo_Ativo_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtServicoGrupo_Descricao_Internalname ;
      private String edtServicoGrupo_Codigo_Internalname ;
      private String edtServicoGrupo_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblUnnamedtable1_Internalname ;
      private String tblUnnamedtable2_Internalname ;
      private String lblTextblockservicogrupo_descricao_Internalname ;
      private String lblTextblockservicogrupo_descricao_Jsonclick ;
      private String edtServicoGrupo_Descricao_Jsonclick ;
      private String lblTextblockservicogrupo_flagrequersoft_Internalname ;
      private String lblTextblockservicogrupo_flagrequersoft_Jsonclick ;
      private String lblTextblockservicogrupo_ativo_Internalname ;
      private String lblTextblockservicogrupo_ativo_Jsonclick ;
      private String Dvpanel_unnamedtable1_Width ;
      private String Dvpanel_unnamedtable1_Height ;
      private String Dvpanel_unnamedtable1_Cls ;
      private String Dvpanel_unnamedtable1_Title ;
      private String Dvpanel_unnamedtable1_Class ;
      private String Dvpanel_unnamedtable1_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode33 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_unnamedtable1_Internalname ;
      private bool Z1428ServicoGrupo_FlagRequerSoft ;
      private bool Z159ServicoGrupo_Ativo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool A1428ServicoGrupo_FlagRequerSoft ;
      private bool A159ServicoGrupo_Ativo ;
      private bool n1428ServicoGrupo_FlagRequerSoft ;
      private bool Dvpanel_unnamedtable1_Collapsible ;
      private bool Dvpanel_unnamedtable1_Collapsed ;
      private bool Dvpanel_unnamedtable1_Enabled ;
      private bool Dvpanel_unnamedtable1_Autowidth ;
      private bool Dvpanel_unnamedtable1_Autoheight ;
      private bool Dvpanel_unnamedtable1_Showheader ;
      private bool Dvpanel_unnamedtable1_Showcollapseicon ;
      private bool Dvpanel_unnamedtable1_Autoscroll ;
      private bool Dvpanel_unnamedtable1_Visible ;
      private bool returnInSub ;
      private bool i159ServicoGrupo_Ativo ;
      private String Z158ServicoGrupo_Descricao ;
      private String O158ServicoGrupo_Descricao ;
      private String A158ServicoGrupo_Descricao ;
      private IGxSession AV10WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCheckbox chkServicoGrupo_FlagRequerSoft ;
      private GXCheckbox chkServicoGrupo_Ativo ;
      private IDataStoreProvider pr_default ;
      private int[] T000W4_A157ServicoGrupo_Codigo ;
      private String[] T000W4_A158ServicoGrupo_Descricao ;
      private bool[] T000W4_A1428ServicoGrupo_FlagRequerSoft ;
      private bool[] T000W4_n1428ServicoGrupo_FlagRequerSoft ;
      private bool[] T000W4_A159ServicoGrupo_Ativo ;
      private int[] T000W5_A157ServicoGrupo_Codigo ;
      private int[] T000W3_A157ServicoGrupo_Codigo ;
      private String[] T000W3_A158ServicoGrupo_Descricao ;
      private bool[] T000W3_A1428ServicoGrupo_FlagRequerSoft ;
      private bool[] T000W3_n1428ServicoGrupo_FlagRequerSoft ;
      private bool[] T000W3_A159ServicoGrupo_Ativo ;
      private int[] T000W6_A157ServicoGrupo_Codigo ;
      private int[] T000W7_A157ServicoGrupo_Codigo ;
      private int[] T000W2_A157ServicoGrupo_Codigo ;
      private String[] T000W2_A158ServicoGrupo_Descricao ;
      private bool[] T000W2_A1428ServicoGrupo_FlagRequerSoft ;
      private bool[] T000W2_n1428ServicoGrupo_FlagRequerSoft ;
      private bool[] T000W2_A159ServicoGrupo_Ativo ;
      private int[] T000W8_A157ServicoGrupo_Codigo ;
      private int[] T000W11_A155Servico_Codigo ;
      private int[] T000W12_A157ServicoGrupo_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
   }

   public class servicogrupo__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT000W4 ;
          prmT000W4 = new Object[] {
          new Object[] {"@ServicoGrupo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000W5 ;
          prmT000W5 = new Object[] {
          new Object[] {"@ServicoGrupo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000W3 ;
          prmT000W3 = new Object[] {
          new Object[] {"@ServicoGrupo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000W6 ;
          prmT000W6 = new Object[] {
          new Object[] {"@ServicoGrupo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000W7 ;
          prmT000W7 = new Object[] {
          new Object[] {"@ServicoGrupo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000W2 ;
          prmT000W2 = new Object[] {
          new Object[] {"@ServicoGrupo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000W8 ;
          prmT000W8 = new Object[] {
          new Object[] {"@ServicoGrupo_Descricao",SqlDbType.VarChar,100,0} ,
          new Object[] {"@ServicoGrupo_FlagRequerSoft",SqlDbType.Bit,4,0} ,
          new Object[] {"@ServicoGrupo_Ativo",SqlDbType.Bit,4,0}
          } ;
          Object[] prmT000W9 ;
          prmT000W9 = new Object[] {
          new Object[] {"@ServicoGrupo_Descricao",SqlDbType.VarChar,100,0} ,
          new Object[] {"@ServicoGrupo_FlagRequerSoft",SqlDbType.Bit,4,0} ,
          new Object[] {"@ServicoGrupo_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@ServicoGrupo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000W10 ;
          prmT000W10 = new Object[] {
          new Object[] {"@ServicoGrupo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000W11 ;
          prmT000W11 = new Object[] {
          new Object[] {"@ServicoGrupo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000W12 ;
          prmT000W12 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("T000W2", "SELECT [ServicoGrupo_Codigo], [ServicoGrupo_Descricao], [ServicoGrupo_FlagRequerSoft], [ServicoGrupo_Ativo] FROM [ServicoGrupo] WITH (UPDLOCK) WHERE [ServicoGrupo_Codigo] = @ServicoGrupo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000W2,1,0,true,false )
             ,new CursorDef("T000W3", "SELECT [ServicoGrupo_Codigo], [ServicoGrupo_Descricao], [ServicoGrupo_FlagRequerSoft], [ServicoGrupo_Ativo] FROM [ServicoGrupo] WITH (NOLOCK) WHERE [ServicoGrupo_Codigo] = @ServicoGrupo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000W3,1,0,true,false )
             ,new CursorDef("T000W4", "SELECT TM1.[ServicoGrupo_Codigo], TM1.[ServicoGrupo_Descricao], TM1.[ServicoGrupo_FlagRequerSoft], TM1.[ServicoGrupo_Ativo] FROM [ServicoGrupo] TM1 WITH (NOLOCK) WHERE TM1.[ServicoGrupo_Codigo] = @ServicoGrupo_Codigo ORDER BY TM1.[ServicoGrupo_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000W4,100,0,true,false )
             ,new CursorDef("T000W5", "SELECT [ServicoGrupo_Codigo] FROM [ServicoGrupo] WITH (NOLOCK) WHERE [ServicoGrupo_Codigo] = @ServicoGrupo_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000W5,1,0,true,false )
             ,new CursorDef("T000W6", "SELECT TOP 1 [ServicoGrupo_Codigo] FROM [ServicoGrupo] WITH (NOLOCK) WHERE ( [ServicoGrupo_Codigo] > @ServicoGrupo_Codigo) ORDER BY [ServicoGrupo_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000W6,1,0,true,true )
             ,new CursorDef("T000W7", "SELECT TOP 1 [ServicoGrupo_Codigo] FROM [ServicoGrupo] WITH (NOLOCK) WHERE ( [ServicoGrupo_Codigo] < @ServicoGrupo_Codigo) ORDER BY [ServicoGrupo_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000W7,1,0,true,true )
             ,new CursorDef("T000W8", "INSERT INTO [ServicoGrupo]([ServicoGrupo_Descricao], [ServicoGrupo_FlagRequerSoft], [ServicoGrupo_Ativo]) VALUES(@ServicoGrupo_Descricao, @ServicoGrupo_FlagRequerSoft, @ServicoGrupo_Ativo); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000W8)
             ,new CursorDef("T000W9", "UPDATE [ServicoGrupo] SET [ServicoGrupo_Descricao]=@ServicoGrupo_Descricao, [ServicoGrupo_FlagRequerSoft]=@ServicoGrupo_FlagRequerSoft, [ServicoGrupo_Ativo]=@ServicoGrupo_Ativo  WHERE [ServicoGrupo_Codigo] = @ServicoGrupo_Codigo", GxErrorMask.GX_NOMASK,prmT000W9)
             ,new CursorDef("T000W10", "DELETE FROM [ServicoGrupo]  WHERE [ServicoGrupo_Codigo] = @ServicoGrupo_Codigo", GxErrorMask.GX_NOMASK,prmT000W10)
             ,new CursorDef("T000W11", "SELECT TOP 1 [Servico_Codigo] FROM [Servico] WITH (NOLOCK) WHERE [ServicoGrupo_Codigo] = @ServicoGrupo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000W11,1,0,true,true )
             ,new CursorDef("T000W12", "SELECT [ServicoGrupo_Codigo] FROM [ServicoGrupo] WITH (NOLOCK) ORDER BY [ServicoGrupo_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000W12,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(2, (bool)parms[2]);
                }
                stmt.SetParameter(3, (bool)parms[3]);
                return;
             case 7 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(2, (bool)parms[2]);
                }
                stmt.SetParameter(3, (bool)parms[3]);
                stmt.SetParameter(4, (int)parms[4]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
