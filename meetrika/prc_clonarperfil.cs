/*
               File: PRC_ClonarPerfil
        Description: Clonar Perfil
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:13:1.57
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_clonarperfil : GXProcedure
   {
      public prc_clonarperfil( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_clonarperfil( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ParaCod ,
                           int aP1_DoUsuario ,
                           int aP2_Contratada_Codigo ,
                           int aP3_Contratante_Codigo ,
                           IGxCollection aP4_Clonar ,
                           IGxCollection aP5_Apagar )
      {
         this.AV11ParaCod = aP0_ParaCod;
         this.AV12DoUsuario = aP1_DoUsuario;
         this.AV9Contratada_Codigo = aP2_Contratada_Codigo;
         this.AV8Contratante_Codigo = aP3_Contratante_Codigo;
         this.AV10Clonar = aP4_Clonar;
         this.AV18Apagar = aP5_Apagar;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_ParaCod ,
                                 int aP1_DoUsuario ,
                                 int aP2_Contratada_Codigo ,
                                 int aP3_Contratante_Codigo ,
                                 IGxCollection aP4_Clonar ,
                                 IGxCollection aP5_Apagar )
      {
         prc_clonarperfil objprc_clonarperfil;
         objprc_clonarperfil = new prc_clonarperfil();
         objprc_clonarperfil.AV11ParaCod = aP0_ParaCod;
         objprc_clonarperfil.AV12DoUsuario = aP1_DoUsuario;
         objprc_clonarperfil.AV9Contratada_Codigo = aP2_Contratada_Codigo;
         objprc_clonarperfil.AV8Contratante_Codigo = aP3_Contratante_Codigo;
         objprc_clonarperfil.AV10Clonar = aP4_Clonar;
         objprc_clonarperfil.AV18Apagar = aP5_Apagar;
         objprc_clonarperfil.context.SetSubmitInitialConfig(context);
         objprc_clonarperfil.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_clonarperfil);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_clonarperfil)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( ((bool)AV18Apagar.Item(1)) )
         {
            /* Optimized DELETE. */
            /* Using cursor P00VB2 */
            pr_default.execute(0, new Object[] {AV11ParaCod});
            pr_default.close(0);
            dsDefault.SmartCacheProvider.SetUpdated("UsuarioPerfil") ;
            /* End optimized DELETE. */
         }
         if ( ((bool)AV18Apagar.Item(2)) )
         {
            /* Optimized DELETE. */
            /* Using cursor P00VB3 */
            pr_default.execute(1, new Object[] {AV11ParaCod});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("UsuarioServicos") ;
            /* End optimized DELETE. */
         }
         if ( ((bool)AV18Apagar.Item(3)) )
         {
            /* Optimized DELETE. */
            /* Using cursor P00VB4 */
            pr_default.execute(2, new Object[] {AV11ParaCod});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("ContratoGestor") ;
            /* End optimized DELETE. */
            /* Optimized DELETE. */
            /* Using cursor P00VB5 */
            pr_default.execute(3, new Object[] {AV11ParaCod});
            pr_default.close(3);
            dsDefault.SmartCacheProvider.SetUpdated("ContratoAuxiliar") ;
            /* End optimized DELETE. */
         }
         if ( ((bool)AV18Apagar.Item(4)) )
         {
            /* Optimized DELETE. */
            /* Using cursor P00VB6 */
            pr_default.execute(4, new Object[] {AV8Contratante_Codigo, AV11ParaCod});
            pr_default.close(4);
            dsDefault.SmartCacheProvider.SetUpdated("ContratanteUsuarioSistema") ;
            /* End optimized DELETE. */
         }
         if ( ((bool)AV10Clonar.Item(1)) )
         {
            /* Using cursor P00VB7 */
            pr_default.execute(5, new Object[] {AV12DoUsuario});
            while ( (pr_default.getStatus(5) != 101) )
            {
               A3Perfil_Codigo = P00VB7_A3Perfil_Codigo[0];
               A1Usuario_Codigo = P00VB7_A1Usuario_Codigo[0];
               A659UsuarioPerfil_Update = P00VB7_A659UsuarioPerfil_Update[0];
               n659UsuarioPerfil_Update = P00VB7_n659UsuarioPerfil_Update[0];
               A546UsuarioPerfil_Delete = P00VB7_A546UsuarioPerfil_Delete[0];
               n546UsuarioPerfil_Delete = P00VB7_n546UsuarioPerfil_Delete[0];
               A544UsuarioPerfil_Insert = P00VB7_A544UsuarioPerfil_Insert[0];
               n544UsuarioPerfil_Insert = P00VB7_n544UsuarioPerfil_Insert[0];
               A543UsuarioPerfil_Display = P00VB7_A543UsuarioPerfil_Display[0];
               n543UsuarioPerfil_Display = P00VB7_n543UsuarioPerfil_Display[0];
               A276Perfil_Ativo = P00VB7_A276Perfil_Ativo[0];
               A276Perfil_Ativo = P00VB7_A276Perfil_Ativo[0];
               W1Usuario_Codigo = A1Usuario_Codigo;
               AV14Perfil_Codigo = A3Perfil_Codigo;
               /*
                  INSERT RECORD ON TABLE UsuarioPerfil

               */
               W1Usuario_Codigo = A1Usuario_Codigo;
               W3Perfil_Codigo = A3Perfil_Codigo;
               A1Usuario_Codigo = AV11ParaCod;
               A3Perfil_Codigo = AV14Perfil_Codigo;
               /* Using cursor P00VB8 */
               pr_default.execute(6, new Object[] {A1Usuario_Codigo, A3Perfil_Codigo, n543UsuarioPerfil_Display, A543UsuarioPerfil_Display, n544UsuarioPerfil_Insert, A544UsuarioPerfil_Insert, n546UsuarioPerfil_Delete, A546UsuarioPerfil_Delete, n659UsuarioPerfil_Update, A659UsuarioPerfil_Update});
               pr_default.close(6);
               dsDefault.SmartCacheProvider.SetUpdated("UsuarioPerfil") ;
               if ( (pr_default.getStatus(6) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               A1Usuario_Codigo = W1Usuario_Codigo;
               A3Perfil_Codigo = W3Perfil_Codigo;
               /* End Insert */
               A1Usuario_Codigo = W1Usuario_Codigo;
               pr_default.readNext(5);
            }
            pr_default.close(5);
         }
         if ( ((bool)AV10Clonar.Item(2)) )
         {
            /* Using cursor P00VB9 */
            pr_default.execute(7, new Object[] {AV12DoUsuario});
            while ( (pr_default.getStatus(7) != 101) )
            {
               A828UsuarioServicos_UsuarioCod = P00VB9_A828UsuarioServicos_UsuarioCod[0];
               A1517UsuarioServicos_TmpEstAnl = P00VB9_A1517UsuarioServicos_TmpEstAnl[0];
               n1517UsuarioServicos_TmpEstAnl = P00VB9_n1517UsuarioServicos_TmpEstAnl[0];
               A1514UsuarioServicos_TmpEstCrr = P00VB9_A1514UsuarioServicos_TmpEstCrr[0];
               n1514UsuarioServicos_TmpEstCrr = P00VB9_n1514UsuarioServicos_TmpEstCrr[0];
               A1513UsuarioServicos_TmpEstExc = P00VB9_A1513UsuarioServicos_TmpEstExc[0];
               n1513UsuarioServicos_TmpEstExc = P00VB9_n1513UsuarioServicos_TmpEstExc[0];
               A829UsuarioServicos_ServicoCod = P00VB9_A829UsuarioServicos_ServicoCod[0];
               W828UsuarioServicos_UsuarioCod = A828UsuarioServicos_UsuarioCod;
               /*
                  INSERT RECORD ON TABLE UsuarioServicos

               */
               W828UsuarioServicos_UsuarioCod = A828UsuarioServicos_UsuarioCod;
               A828UsuarioServicos_UsuarioCod = AV11ParaCod;
               /* Using cursor P00VB10 */
               pr_default.execute(8, new Object[] {A828UsuarioServicos_UsuarioCod, A829UsuarioServicos_ServicoCod, n1513UsuarioServicos_TmpEstExc, A1513UsuarioServicos_TmpEstExc, n1514UsuarioServicos_TmpEstCrr, A1514UsuarioServicos_TmpEstCrr, n1517UsuarioServicos_TmpEstAnl, A1517UsuarioServicos_TmpEstAnl});
               pr_default.close(8);
               dsDefault.SmartCacheProvider.SetUpdated("UsuarioServicos") ;
               if ( (pr_default.getStatus(8) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               A828UsuarioServicos_UsuarioCod = W828UsuarioServicos_UsuarioCod;
               /* End Insert */
               A828UsuarioServicos_UsuarioCod = W828UsuarioServicos_UsuarioCod;
               pr_default.readNext(7);
            }
            pr_default.close(7);
         }
         if ( ((bool)AV10Clonar.Item(3)) )
         {
            /* Using cursor P00VB11 */
            pr_default.execute(9, new Object[] {AV12DoUsuario});
            while ( (pr_default.getStatus(9) != 101) )
            {
               A1079ContratoGestor_UsuarioCod = P00VB11_A1079ContratoGestor_UsuarioCod[0];
               A2009ContratoGestor_Tipo = P00VB11_A2009ContratoGestor_Tipo[0];
               n2009ContratoGestor_Tipo = P00VB11_n2009ContratoGestor_Tipo[0];
               A1078ContratoGestor_ContratoCod = P00VB11_A1078ContratoGestor_ContratoCod[0];
               W1079ContratoGestor_UsuarioCod = A1079ContratoGestor_UsuarioCod;
               /*
                  INSERT RECORD ON TABLE ContratoGestor

               */
               W1079ContratoGestor_UsuarioCod = A1079ContratoGestor_UsuarioCod;
               A1079ContratoGestor_UsuarioCod = AV11ParaCod;
               /* Using cursor P00VB12 */
               pr_default.execute(10, new Object[] {A1078ContratoGestor_ContratoCod, A1079ContratoGestor_UsuarioCod, n2009ContratoGestor_Tipo, A2009ContratoGestor_Tipo});
               pr_default.close(10);
               dsDefault.SmartCacheProvider.SetUpdated("ContratoGestor") ;
               if ( (pr_default.getStatus(10) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               A1079ContratoGestor_UsuarioCod = W1079ContratoGestor_UsuarioCod;
               /* End Insert */
               A1079ContratoGestor_UsuarioCod = W1079ContratoGestor_UsuarioCod;
               pr_default.readNext(9);
            }
            pr_default.close(9);
            /* Using cursor P00VB13 */
            pr_default.execute(11, new Object[] {AV12DoUsuario});
            while ( (pr_default.getStatus(11) != 101) )
            {
               A1825ContratoAuxiliar_UsuarioCod = P00VB13_A1825ContratoAuxiliar_UsuarioCod[0];
               A1824ContratoAuxiliar_ContratoCod = P00VB13_A1824ContratoAuxiliar_ContratoCod[0];
               W1825ContratoAuxiliar_UsuarioCod = A1825ContratoAuxiliar_UsuarioCod;
               /*
                  INSERT RECORD ON TABLE ContratoAuxiliar

               */
               W1825ContratoAuxiliar_UsuarioCod = A1825ContratoAuxiliar_UsuarioCod;
               A1825ContratoAuxiliar_UsuarioCod = AV11ParaCod;
               /* Using cursor P00VB14 */
               pr_default.execute(12, new Object[] {A1824ContratoAuxiliar_ContratoCod, A1825ContratoAuxiliar_UsuarioCod});
               pr_default.close(12);
               dsDefault.SmartCacheProvider.SetUpdated("ContratoAuxiliar") ;
               if ( (pr_default.getStatus(12) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               A1825ContratoAuxiliar_UsuarioCod = W1825ContratoAuxiliar_UsuarioCod;
               /* End Insert */
               A1825ContratoAuxiliar_UsuarioCod = W1825ContratoAuxiliar_UsuarioCod;
               pr_default.readNext(11);
            }
            pr_default.close(11);
         }
         if ( AV9Contratada_Codigo > 0 )
         {
            if ( ((bool)AV10Clonar.Item(6)) )
            {
               /* Using cursor P00VB15 */
               pr_default.execute(13, new Object[] {AV12DoUsuario});
               while ( (pr_default.getStatus(13) != 101) )
               {
                  A69ContratadaUsuario_UsuarioCod = P00VB15_A69ContratadaUsuario_UsuarioCod[0];
                  A576ContratadaUsuario_CstUntPrdNrm = P00VB15_A576ContratadaUsuario_CstUntPrdNrm[0];
                  n576ContratadaUsuario_CstUntPrdNrm = P00VB15_n576ContratadaUsuario_CstUntPrdNrm[0];
                  A577ContratadaUsuario_CstUntPrdExt = P00VB15_A577ContratadaUsuario_CstUntPrdExt[0];
                  n577ContratadaUsuario_CstUntPrdExt = P00VB15_n577ContratadaUsuario_CstUntPrdExt[0];
                  A66ContratadaUsuario_ContratadaCod = P00VB15_A66ContratadaUsuario_ContratadaCod[0];
                  AV16ContratadaUsuario_CstUntPrdNrm = A576ContratadaUsuario_CstUntPrdNrm;
                  AV17ContratadaUsuario_CstUntPrdExt = A577ContratadaUsuario_CstUntPrdExt;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  pr_default.readNext(13);
               }
               pr_default.close(13);
               /* Using cursor P00VB16 */
               pr_default.execute(14, new Object[] {AV11ParaCod});
               while ( (pr_default.getStatus(14) != 101) )
               {
                  A69ContratadaUsuario_UsuarioCod = P00VB16_A69ContratadaUsuario_UsuarioCod[0];
                  A576ContratadaUsuario_CstUntPrdNrm = P00VB16_A576ContratadaUsuario_CstUntPrdNrm[0];
                  n576ContratadaUsuario_CstUntPrdNrm = P00VB16_n576ContratadaUsuario_CstUntPrdNrm[0];
                  A577ContratadaUsuario_CstUntPrdExt = P00VB16_A577ContratadaUsuario_CstUntPrdExt[0];
                  n577ContratadaUsuario_CstUntPrdExt = P00VB16_n577ContratadaUsuario_CstUntPrdExt[0];
                  A66ContratadaUsuario_ContratadaCod = P00VB16_A66ContratadaUsuario_ContratadaCod[0];
                  A576ContratadaUsuario_CstUntPrdNrm = AV16ContratadaUsuario_CstUntPrdNrm;
                  n576ContratadaUsuario_CstUntPrdNrm = false;
                  A577ContratadaUsuario_CstUntPrdExt = AV17ContratadaUsuario_CstUntPrdExt;
                  n577ContratadaUsuario_CstUntPrdExt = false;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  /* Using cursor P00VB17 */
                  pr_default.execute(15, new Object[] {n576ContratadaUsuario_CstUntPrdNrm, A576ContratadaUsuario_CstUntPrdNrm, n577ContratadaUsuario_CstUntPrdExt, A577ContratadaUsuario_CstUntPrdExt, A66ContratadaUsuario_ContratadaCod, A69ContratadaUsuario_UsuarioCod});
                  pr_default.close(15);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratadaUsuario") ;
                  if (true) break;
                  /* Using cursor P00VB18 */
                  pr_default.execute(16, new Object[] {n576ContratadaUsuario_CstUntPrdNrm, A576ContratadaUsuario_CstUntPrdNrm, n577ContratadaUsuario_CstUntPrdExt, A577ContratadaUsuario_CstUntPrdExt, A66ContratadaUsuario_ContratadaCod, A69ContratadaUsuario_UsuarioCod});
                  pr_default.close(16);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratadaUsuario") ;
                  pr_default.readNext(14);
               }
               pr_default.close(14);
            }
         }
         else if ( AV8Contratante_Codigo > 0 )
         {
            if ( ((bool)AV10Clonar.Item(4)) )
            {
               /* Using cursor P00VB19 */
               pr_default.execute(17, new Object[] {AV8Contratante_Codigo, AV12DoUsuario});
               while ( (pr_default.getStatus(17) != 101) )
               {
                  A1728ContratanteUsuario_EhFiscal = P00VB19_A1728ContratanteUsuario_EhFiscal[0];
                  n1728ContratanteUsuario_EhFiscal = P00VB19_n1728ContratanteUsuario_EhFiscal[0];
                  A63ContratanteUsuario_ContratanteCod = P00VB19_A63ContratanteUsuario_ContratanteCod[0];
                  A60ContratanteUsuario_UsuarioCod = P00VB19_A60ContratanteUsuario_UsuarioCod[0];
                  A127Sistema_Codigo = P00VB19_A127Sistema_Codigo[0];
                  A1728ContratanteUsuario_EhFiscal = P00VB19_A1728ContratanteUsuario_EhFiscal[0];
                  n1728ContratanteUsuario_EhFiscal = P00VB19_n1728ContratanteUsuario_EhFiscal[0];
                  W60ContratanteUsuario_UsuarioCod = A60ContratanteUsuario_UsuarioCod;
                  /*
                     INSERT RECORD ON TABLE ContratanteUsuario

                  */
                  W60ContratanteUsuario_UsuarioCod = A60ContratanteUsuario_UsuarioCod;
                  A60ContratanteUsuario_UsuarioCod = AV11ParaCod;
                  /* Using cursor P00VB20 */
                  pr_default.execute(18, new Object[] {A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod, n1728ContratanteUsuario_EhFiscal, A1728ContratanteUsuario_EhFiscal});
                  pr_default.close(18);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratanteUsuario") ;
                  if ( (pr_default.getStatus(18) == 1) )
                  {
                     context.Gx_err = 1;
                     Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
                  }
                  else
                  {
                     context.Gx_err = 0;
                     Gx_emsg = "";
                  }
                  A60ContratanteUsuario_UsuarioCod = W60ContratanteUsuario_UsuarioCod;
                  /* End Insert */
                  A60ContratanteUsuario_UsuarioCod = W60ContratanteUsuario_UsuarioCod;
                  pr_default.readNext(17);
               }
               pr_default.close(17);
            }
         }
         if ( ((bool)AV10Clonar.Item(5)) )
         {
            /* Using cursor P00VB21 */
            pr_default.execute(19, new Object[] {AV11ParaCod});
            while ( (pr_default.getStatus(19) != 101) )
            {
               A1Usuario_Codigo = P00VB21_A1Usuario_Codigo[0];
               A293Usuario_EhFinanceiro = P00VB21_A293Usuario_EhFinanceiro[0];
               A293Usuario_EhFinanceiro = true;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               /* Using cursor P00VB22 */
               pr_default.execute(20, new Object[] {A293Usuario_EhFinanceiro, A1Usuario_Codigo});
               pr_default.close(20);
               dsDefault.SmartCacheProvider.SetUpdated("Usuario") ;
               if (true) break;
               /* Using cursor P00VB23 */
               pr_default.execute(21, new Object[] {A293Usuario_EhFinanceiro, A1Usuario_Codigo});
               pr_default.close(21);
               dsDefault.SmartCacheProvider.SetUpdated("Usuario") ;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(19);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_ClonarPerfil");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00VB7_A3Perfil_Codigo = new int[1] ;
         P00VB7_A1Usuario_Codigo = new int[1] ;
         P00VB7_A659UsuarioPerfil_Update = new bool[] {false} ;
         P00VB7_n659UsuarioPerfil_Update = new bool[] {false} ;
         P00VB7_A546UsuarioPerfil_Delete = new bool[] {false} ;
         P00VB7_n546UsuarioPerfil_Delete = new bool[] {false} ;
         P00VB7_A544UsuarioPerfil_Insert = new bool[] {false} ;
         P00VB7_n544UsuarioPerfil_Insert = new bool[] {false} ;
         P00VB7_A543UsuarioPerfil_Display = new bool[] {false} ;
         P00VB7_n543UsuarioPerfil_Display = new bool[] {false} ;
         P00VB7_A276Perfil_Ativo = new bool[] {false} ;
         Gx_emsg = "";
         P00VB9_A828UsuarioServicos_UsuarioCod = new int[1] ;
         P00VB9_A1517UsuarioServicos_TmpEstAnl = new int[1] ;
         P00VB9_n1517UsuarioServicos_TmpEstAnl = new bool[] {false} ;
         P00VB9_A1514UsuarioServicos_TmpEstCrr = new int[1] ;
         P00VB9_n1514UsuarioServicos_TmpEstCrr = new bool[] {false} ;
         P00VB9_A1513UsuarioServicos_TmpEstExc = new int[1] ;
         P00VB9_n1513UsuarioServicos_TmpEstExc = new bool[] {false} ;
         P00VB9_A829UsuarioServicos_ServicoCod = new int[1] ;
         P00VB11_A1079ContratoGestor_UsuarioCod = new int[1] ;
         P00VB11_A2009ContratoGestor_Tipo = new short[1] ;
         P00VB11_n2009ContratoGestor_Tipo = new bool[] {false} ;
         P00VB11_A1078ContratoGestor_ContratoCod = new int[1] ;
         P00VB13_A1825ContratoAuxiliar_UsuarioCod = new int[1] ;
         P00VB13_A1824ContratoAuxiliar_ContratoCod = new int[1] ;
         P00VB15_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P00VB15_A576ContratadaUsuario_CstUntPrdNrm = new decimal[1] ;
         P00VB15_n576ContratadaUsuario_CstUntPrdNrm = new bool[] {false} ;
         P00VB15_A577ContratadaUsuario_CstUntPrdExt = new decimal[1] ;
         P00VB15_n577ContratadaUsuario_CstUntPrdExt = new bool[] {false} ;
         P00VB15_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P00VB16_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P00VB16_A576ContratadaUsuario_CstUntPrdNrm = new decimal[1] ;
         P00VB16_n576ContratadaUsuario_CstUntPrdNrm = new bool[] {false} ;
         P00VB16_A577ContratadaUsuario_CstUntPrdExt = new decimal[1] ;
         P00VB16_n577ContratadaUsuario_CstUntPrdExt = new bool[] {false} ;
         P00VB16_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P00VB19_A1728ContratanteUsuario_EhFiscal = new bool[] {false} ;
         P00VB19_n1728ContratanteUsuario_EhFiscal = new bool[] {false} ;
         P00VB19_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         P00VB19_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         P00VB19_A127Sistema_Codigo = new int[1] ;
         P00VB21_A1Usuario_Codigo = new int[1] ;
         P00VB21_A293Usuario_EhFinanceiro = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_clonarperfil__default(),
            new Object[][] {
                new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P00VB7_A3Perfil_Codigo, P00VB7_A1Usuario_Codigo, P00VB7_A659UsuarioPerfil_Update, P00VB7_n659UsuarioPerfil_Update, P00VB7_A546UsuarioPerfil_Delete, P00VB7_n546UsuarioPerfil_Delete, P00VB7_A544UsuarioPerfil_Insert, P00VB7_n544UsuarioPerfil_Insert, P00VB7_A543UsuarioPerfil_Display, P00VB7_n543UsuarioPerfil_Display,
               P00VB7_A276Perfil_Ativo
               }
               , new Object[] {
               }
               , new Object[] {
               P00VB9_A828UsuarioServicos_UsuarioCod, P00VB9_A1517UsuarioServicos_TmpEstAnl, P00VB9_n1517UsuarioServicos_TmpEstAnl, P00VB9_A1514UsuarioServicos_TmpEstCrr, P00VB9_n1514UsuarioServicos_TmpEstCrr, P00VB9_A1513UsuarioServicos_TmpEstExc, P00VB9_n1513UsuarioServicos_TmpEstExc, P00VB9_A829UsuarioServicos_ServicoCod
               }
               , new Object[] {
               }
               , new Object[] {
               P00VB11_A1079ContratoGestor_UsuarioCod, P00VB11_A2009ContratoGestor_Tipo, P00VB11_n2009ContratoGestor_Tipo, P00VB11_A1078ContratoGestor_ContratoCod
               }
               , new Object[] {
               }
               , new Object[] {
               P00VB13_A1825ContratoAuxiliar_UsuarioCod, P00VB13_A1824ContratoAuxiliar_ContratoCod
               }
               , new Object[] {
               }
               , new Object[] {
               P00VB15_A69ContratadaUsuario_UsuarioCod, P00VB15_A576ContratadaUsuario_CstUntPrdNrm, P00VB15_n576ContratadaUsuario_CstUntPrdNrm, P00VB15_A577ContratadaUsuario_CstUntPrdExt, P00VB15_n577ContratadaUsuario_CstUntPrdExt, P00VB15_A66ContratadaUsuario_ContratadaCod
               }
               , new Object[] {
               P00VB16_A69ContratadaUsuario_UsuarioCod, P00VB16_A576ContratadaUsuario_CstUntPrdNrm, P00VB16_n576ContratadaUsuario_CstUntPrdNrm, P00VB16_A577ContratadaUsuario_CstUntPrdExt, P00VB16_n577ContratadaUsuario_CstUntPrdExt, P00VB16_A66ContratadaUsuario_ContratadaCod
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P00VB19_A1728ContratanteUsuario_EhFiscal, P00VB19_n1728ContratanteUsuario_EhFiscal, P00VB19_A63ContratanteUsuario_ContratanteCod, P00VB19_A60ContratanteUsuario_UsuarioCod, P00VB19_A127Sistema_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               P00VB21_A1Usuario_Codigo, P00VB21_A293Usuario_EhFinanceiro
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A2009ContratoGestor_Tipo ;
      private int AV11ParaCod ;
      private int AV12DoUsuario ;
      private int AV9Contratada_Codigo ;
      private int AV8Contratante_Codigo ;
      private int A3Perfil_Codigo ;
      private int A1Usuario_Codigo ;
      private int W1Usuario_Codigo ;
      private int AV14Perfil_Codigo ;
      private int GX_INS3 ;
      private int W3Perfil_Codigo ;
      private int A828UsuarioServicos_UsuarioCod ;
      private int A1517UsuarioServicos_TmpEstAnl ;
      private int A1514UsuarioServicos_TmpEstCrr ;
      private int A1513UsuarioServicos_TmpEstExc ;
      private int A829UsuarioServicos_ServicoCod ;
      private int W828UsuarioServicos_UsuarioCod ;
      private int GX_INS102 ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int A1078ContratoGestor_ContratoCod ;
      private int W1079ContratoGestor_UsuarioCod ;
      private int GX_INS129 ;
      private int A1825ContratoAuxiliar_UsuarioCod ;
      private int A1824ContratoAuxiliar_ContratoCod ;
      private int W1825ContratoAuxiliar_UsuarioCod ;
      private int GX_INS200 ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int A127Sistema_Codigo ;
      private int W60ContratanteUsuario_UsuarioCod ;
      private int GX_INS15 ;
      private decimal A576ContratadaUsuario_CstUntPrdNrm ;
      private decimal A577ContratadaUsuario_CstUntPrdExt ;
      private decimal AV16ContratadaUsuario_CstUntPrdNrm ;
      private decimal AV17ContratadaUsuario_CstUntPrdExt ;
      private String scmdbuf ;
      private String Gx_emsg ;
      private bool A659UsuarioPerfil_Update ;
      private bool n659UsuarioPerfil_Update ;
      private bool A546UsuarioPerfil_Delete ;
      private bool n546UsuarioPerfil_Delete ;
      private bool A544UsuarioPerfil_Insert ;
      private bool n544UsuarioPerfil_Insert ;
      private bool A543UsuarioPerfil_Display ;
      private bool n543UsuarioPerfil_Display ;
      private bool A276Perfil_Ativo ;
      private bool n1517UsuarioServicos_TmpEstAnl ;
      private bool n1514UsuarioServicos_TmpEstCrr ;
      private bool n1513UsuarioServicos_TmpEstExc ;
      private bool n2009ContratoGestor_Tipo ;
      private bool n576ContratadaUsuario_CstUntPrdNrm ;
      private bool n577ContratadaUsuario_CstUntPrdExt ;
      private bool A1728ContratanteUsuario_EhFiscal ;
      private bool n1728ContratanteUsuario_EhFiscal ;
      private bool A293Usuario_EhFinanceiro ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00VB7_A3Perfil_Codigo ;
      private int[] P00VB7_A1Usuario_Codigo ;
      private bool[] P00VB7_A659UsuarioPerfil_Update ;
      private bool[] P00VB7_n659UsuarioPerfil_Update ;
      private bool[] P00VB7_A546UsuarioPerfil_Delete ;
      private bool[] P00VB7_n546UsuarioPerfil_Delete ;
      private bool[] P00VB7_A544UsuarioPerfil_Insert ;
      private bool[] P00VB7_n544UsuarioPerfil_Insert ;
      private bool[] P00VB7_A543UsuarioPerfil_Display ;
      private bool[] P00VB7_n543UsuarioPerfil_Display ;
      private bool[] P00VB7_A276Perfil_Ativo ;
      private int[] P00VB9_A828UsuarioServicos_UsuarioCod ;
      private int[] P00VB9_A1517UsuarioServicos_TmpEstAnl ;
      private bool[] P00VB9_n1517UsuarioServicos_TmpEstAnl ;
      private int[] P00VB9_A1514UsuarioServicos_TmpEstCrr ;
      private bool[] P00VB9_n1514UsuarioServicos_TmpEstCrr ;
      private int[] P00VB9_A1513UsuarioServicos_TmpEstExc ;
      private bool[] P00VB9_n1513UsuarioServicos_TmpEstExc ;
      private int[] P00VB9_A829UsuarioServicos_ServicoCod ;
      private int[] P00VB11_A1079ContratoGestor_UsuarioCod ;
      private short[] P00VB11_A2009ContratoGestor_Tipo ;
      private bool[] P00VB11_n2009ContratoGestor_Tipo ;
      private int[] P00VB11_A1078ContratoGestor_ContratoCod ;
      private int[] P00VB13_A1825ContratoAuxiliar_UsuarioCod ;
      private int[] P00VB13_A1824ContratoAuxiliar_ContratoCod ;
      private int[] P00VB15_A69ContratadaUsuario_UsuarioCod ;
      private decimal[] P00VB15_A576ContratadaUsuario_CstUntPrdNrm ;
      private bool[] P00VB15_n576ContratadaUsuario_CstUntPrdNrm ;
      private decimal[] P00VB15_A577ContratadaUsuario_CstUntPrdExt ;
      private bool[] P00VB15_n577ContratadaUsuario_CstUntPrdExt ;
      private int[] P00VB15_A66ContratadaUsuario_ContratadaCod ;
      private int[] P00VB16_A69ContratadaUsuario_UsuarioCod ;
      private decimal[] P00VB16_A576ContratadaUsuario_CstUntPrdNrm ;
      private bool[] P00VB16_n576ContratadaUsuario_CstUntPrdNrm ;
      private decimal[] P00VB16_A577ContratadaUsuario_CstUntPrdExt ;
      private bool[] P00VB16_n577ContratadaUsuario_CstUntPrdExt ;
      private int[] P00VB16_A66ContratadaUsuario_ContratadaCod ;
      private bool[] P00VB19_A1728ContratanteUsuario_EhFiscal ;
      private bool[] P00VB19_n1728ContratanteUsuario_EhFiscal ;
      private int[] P00VB19_A63ContratanteUsuario_ContratanteCod ;
      private int[] P00VB19_A60ContratanteUsuario_UsuarioCod ;
      private int[] P00VB19_A127Sistema_Codigo ;
      private int[] P00VB21_A1Usuario_Codigo ;
      private bool[] P00VB21_A293Usuario_EhFinanceiro ;
      [ObjectCollection(ItemType=typeof( bool ))]
      private IGxCollection AV10Clonar ;
      [ObjectCollection(ItemType=typeof( bool ))]
      private IGxCollection AV18Apagar ;
   }

   public class prc_clonarperfil__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new UpdateCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new UpdateCursor(def[3])
         ,new UpdateCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new UpdateCursor(def[15])
         ,new UpdateCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new UpdateCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new UpdateCursor(def[20])
         ,new UpdateCursor(def[21])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00VB2 ;
          prmP00VB2 = new Object[] {
          new Object[] {"@AV11ParaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VB3 ;
          prmP00VB3 = new Object[] {
          new Object[] {"@AV11ParaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VB4 ;
          prmP00VB4 = new Object[] {
          new Object[] {"@AV11ParaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VB5 ;
          prmP00VB5 = new Object[] {
          new Object[] {"@AV11ParaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VB6 ;
          prmP00VB6 = new Object[] {
          new Object[] {"@AV8Contratante_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11ParaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VB7 ;
          prmP00VB7 = new Object[] {
          new Object[] {"@AV12DoUsuario",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VB8 ;
          prmP00VB8 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@UsuarioPerfil_Display",SqlDbType.Bit,4,0} ,
          new Object[] {"@UsuarioPerfil_Insert",SqlDbType.Bit,4,0} ,
          new Object[] {"@UsuarioPerfil_Delete",SqlDbType.Bit,4,0} ,
          new Object[] {"@UsuarioPerfil_Update",SqlDbType.Bit,4,0}
          } ;
          Object[] prmP00VB9 ;
          prmP00VB9 = new Object[] {
          new Object[] {"@AV12DoUsuario",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VB10 ;
          prmP00VB10 = new Object[] {
          new Object[] {"@UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@UsuarioServicos_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@UsuarioServicos_TmpEstExc",SqlDbType.Int,8,0} ,
          new Object[] {"@UsuarioServicos_TmpEstCrr",SqlDbType.Int,8,0} ,
          new Object[] {"@UsuarioServicos_TmpEstAnl",SqlDbType.Int,8,0}
          } ;
          Object[] prmP00VB11 ;
          prmP00VB11 = new Object[] {
          new Object[] {"@AV12DoUsuario",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VB12 ;
          prmP00VB12 = new Object[] {
          new Object[] {"@ContratoGestor_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoGestor_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoGestor_Tipo",SqlDbType.SmallInt,1,0}
          } ;
          Object[] prmP00VB13 ;
          prmP00VB13 = new Object[] {
          new Object[] {"@AV12DoUsuario",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VB14 ;
          prmP00VB14 = new Object[] {
          new Object[] {"@ContratoAuxiliar_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoAuxiliar_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VB15 ;
          prmP00VB15 = new Object[] {
          new Object[] {"@AV12DoUsuario",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VB16 ;
          prmP00VB16 = new Object[] {
          new Object[] {"@AV11ParaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VB17 ;
          prmP00VB17 = new Object[] {
          new Object[] {"@ContratadaUsuario_CstUntPrdNrm",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContratadaUsuario_CstUntPrdExt",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VB18 ;
          prmP00VB18 = new Object[] {
          new Object[] {"@ContratadaUsuario_CstUntPrdNrm",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContratadaUsuario_CstUntPrdExt",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VB19 ;
          prmP00VB19 = new Object[] {
          new Object[] {"@AV8Contratante_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12DoUsuario",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VB20 ;
          prmP00VB20 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_EhFiscal",SqlDbType.Bit,4,0}
          } ;
          Object[] prmP00VB21 ;
          prmP00VB21 = new Object[] {
          new Object[] {"@AV11ParaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VB22 ;
          prmP00VB22 = new Object[] {
          new Object[] {"@Usuario_EhFinanceiro",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VB23 ;
          prmP00VB23 = new Object[] {
          new Object[] {"@Usuario_EhFinanceiro",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00VB2", "DELETE FROM [UsuarioPerfil]  WHERE [Usuario_Codigo] = @AV11ParaCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00VB2)
             ,new CursorDef("P00VB3", "DELETE FROM [UsuarioServicos]  WHERE [UsuarioServicos_UsuarioCod] = @AV11ParaCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00VB3)
             ,new CursorDef("P00VB4", "DELETE FROM [ContratoGestor]  WHERE [ContratoGestor_UsuarioCod] = @AV11ParaCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00VB4)
             ,new CursorDef("P00VB5", "DELETE FROM [ContratoAuxiliar]  WHERE [ContratoAuxiliar_UsuarioCod] = @AV11ParaCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00VB5)
             ,new CursorDef("P00VB6", "DELETE FROM [ContratanteUsuarioSistema]  WHERE [ContratanteUsuario_ContratanteCod] = @AV8Contratante_Codigo and [ContratanteUsuario_UsuarioCod] = @AV11ParaCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00VB6)
             ,new CursorDef("P00VB7", "SELECT T1.[Perfil_Codigo], T1.[Usuario_Codigo], T1.[UsuarioPerfil_Update], T1.[UsuarioPerfil_Delete], T1.[UsuarioPerfil_Insert], T1.[UsuarioPerfil_Display], T2.[Perfil_Ativo] FROM ([UsuarioPerfil] T1 WITH (NOLOCK) INNER JOIN [Perfil] T2 WITH (NOLOCK) ON T2.[Perfil_Codigo] = T1.[Perfil_Codigo]) WHERE (T1.[Usuario_Codigo] = @AV12DoUsuario) AND (T2.[Perfil_Ativo] = 1) ORDER BY T1.[Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VB7,100,0,true,false )
             ,new CursorDef("P00VB8", "INSERT INTO [UsuarioPerfil]([Usuario_Codigo], [Perfil_Codigo], [UsuarioPerfil_Display], [UsuarioPerfil_Insert], [UsuarioPerfil_Delete], [UsuarioPerfil_Update]) VALUES(@Usuario_Codigo, @Perfil_Codigo, @UsuarioPerfil_Display, @UsuarioPerfil_Insert, @UsuarioPerfil_Delete, @UsuarioPerfil_Update)", GxErrorMask.GX_NOMASK,prmP00VB8)
             ,new CursorDef("P00VB9", "SELECT [UsuarioServicos_UsuarioCod], [UsuarioServicos_TmpEstAnl], [UsuarioServicos_TmpEstCrr], [UsuarioServicos_TmpEstExc], [UsuarioServicos_ServicoCod] FROM [UsuarioServicos] WITH (NOLOCK) WHERE [UsuarioServicos_UsuarioCod] = @AV12DoUsuario ORDER BY [UsuarioServicos_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VB9,100,0,true,false )
             ,new CursorDef("P00VB10", "INSERT INTO [UsuarioServicos]([UsuarioServicos_UsuarioCod], [UsuarioServicos_ServicoCod], [UsuarioServicos_TmpEstExc], [UsuarioServicos_TmpEstCrr], [UsuarioServicos_TmpEstAnl]) VALUES(@UsuarioServicos_UsuarioCod, @UsuarioServicos_ServicoCod, @UsuarioServicos_TmpEstExc, @UsuarioServicos_TmpEstCrr, @UsuarioServicos_TmpEstAnl)", GxErrorMask.GX_NOMASK,prmP00VB10)
             ,new CursorDef("P00VB11", "SELECT [ContratoGestor_UsuarioCod], [ContratoGestor_Tipo], [ContratoGestor_ContratoCod] FROM [ContratoGestor] WITH (NOLOCK) WHERE [ContratoGestor_UsuarioCod] = @AV12DoUsuario ORDER BY [ContratoGestor_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VB11,100,0,true,false )
             ,new CursorDef("P00VB12", "INSERT INTO [ContratoGestor]([ContratoGestor_ContratoCod], [ContratoGestor_UsuarioCod], [ContratoGestor_Tipo]) VALUES(@ContratoGestor_ContratoCod, @ContratoGestor_UsuarioCod, @ContratoGestor_Tipo)", GxErrorMask.GX_NOMASK,prmP00VB12)
             ,new CursorDef("P00VB13", "SELECT [ContratoAuxiliar_UsuarioCod], [ContratoAuxiliar_ContratoCod] FROM [ContratoAuxiliar] WITH (NOLOCK) WHERE [ContratoAuxiliar_UsuarioCod] = @AV12DoUsuario ORDER BY [ContratoAuxiliar_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VB13,100,0,true,false )
             ,new CursorDef("P00VB14", "INSERT INTO [ContratoAuxiliar]([ContratoAuxiliar_ContratoCod], [ContratoAuxiliar_UsuarioCod]) VALUES(@ContratoAuxiliar_ContratoCod, @ContratoAuxiliar_UsuarioCod)", GxErrorMask.GX_NOMASK,prmP00VB14)
             ,new CursorDef("P00VB15", "SELECT TOP 1 [ContratadaUsuario_UsuarioCod], [ContratadaUsuario_CstUntPrdNrm], [ContratadaUsuario_CstUntPrdExt], [ContratadaUsuario_ContratadaCod] FROM [ContratadaUsuario] WITH (NOLOCK) WHERE [ContratadaUsuario_UsuarioCod] = @AV12DoUsuario ORDER BY [ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VB15,1,0,false,true )
             ,new CursorDef("P00VB16", "SELECT TOP 1 [ContratadaUsuario_UsuarioCod], [ContratadaUsuario_CstUntPrdNrm], [ContratadaUsuario_CstUntPrdExt], [ContratadaUsuario_ContratadaCod] FROM [ContratadaUsuario] WITH (UPDLOCK) WHERE [ContratadaUsuario_UsuarioCod] = @AV11ParaCod ORDER BY [ContratadaUsuario_UsuarioCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VB16,1,0,true,true )
             ,new CursorDef("P00VB17", "UPDATE [ContratadaUsuario] SET [ContratadaUsuario_CstUntPrdNrm]=@ContratadaUsuario_CstUntPrdNrm, [ContratadaUsuario_CstUntPrdExt]=@ContratadaUsuario_CstUntPrdExt  WHERE [ContratadaUsuario_ContratadaCod] = @ContratadaUsuario_ContratadaCod AND [ContratadaUsuario_UsuarioCod] = @ContratadaUsuario_UsuarioCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00VB17)
             ,new CursorDef("P00VB18", "UPDATE [ContratadaUsuario] SET [ContratadaUsuario_CstUntPrdNrm]=@ContratadaUsuario_CstUntPrdNrm, [ContratadaUsuario_CstUntPrdExt]=@ContratadaUsuario_CstUntPrdExt  WHERE [ContratadaUsuario_ContratadaCod] = @ContratadaUsuario_ContratadaCod AND [ContratadaUsuario_UsuarioCod] = @ContratadaUsuario_UsuarioCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00VB18)
             ,new CursorDef("P00VB19", "SELECT T2.[ContratanteUsuario_EhFiscal], T1.[ContratanteUsuario_ContratanteCod], T1.[ContratanteUsuario_UsuarioCod], T1.[Sistema_Codigo] FROM ([ContratanteUsuarioSistema] T1 WITH (NOLOCK) INNER JOIN [ContratanteUsuario] T2 WITH (NOLOCK) ON T2.[ContratanteUsuario_ContratanteCod] = T1.[ContratanteUsuario_ContratanteCod] AND T2.[ContratanteUsuario_UsuarioCod] = T1.[ContratanteUsuario_UsuarioCod]) WHERE T1.[ContratanteUsuario_ContratanteCod] = @AV8Contratante_Codigo and T1.[ContratanteUsuario_UsuarioCod] = @AV12DoUsuario ORDER BY T1.[ContratanteUsuario_ContratanteCod], T1.[ContratanteUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VB19,100,0,true,false )
             ,new CursorDef("P00VB20", "INSERT INTO [ContratanteUsuario]([ContratanteUsuario_ContratanteCod], [ContratanteUsuario_UsuarioCod], [ContratanteUsuario_EhFiscal]) VALUES(@ContratanteUsuario_ContratanteCod, @ContratanteUsuario_UsuarioCod, @ContratanteUsuario_EhFiscal)", GxErrorMask.GX_NOMASK,prmP00VB20)
             ,new CursorDef("P00VB21", "SELECT TOP 1 [Usuario_Codigo], [Usuario_EhFinanceiro] FROM [Usuario] WITH (UPDLOCK) WHERE [Usuario_Codigo] = @AV11ParaCod ORDER BY [Usuario_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VB21,1,0,true,true )
             ,new CursorDef("P00VB22", "UPDATE [Usuario] SET [Usuario_EhFinanceiro]=@Usuario_EhFinanceiro  WHERE [Usuario_Codigo] = @Usuario_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00VB22)
             ,new CursorDef("P00VB23", "UPDATE [Usuario] SET [Usuario_EhFinanceiro]=@Usuario_EhFinanceiro  WHERE [Usuario_Codigo] = @Usuario_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00VB23)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((bool[]) buf[8])[0] = rslt.getBool(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((bool[]) buf[10])[0] = rslt.getBool(7) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                return;
             case 17 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(3, (bool)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(4, (bool)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(5, (bool)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(6, (bool)parms[9]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[7]);
                }
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(3, (short)parms[3]);
                }
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                stmt.SetParameter(4, (int)parms[5]);
                return;
             case 16 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                stmt.SetParameter(4, (int)parms[5]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(3, (bool)parms[3]);
                }
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 20 :
                stmt.SetParameter(1, (bool)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 21 :
                stmt.SetParameter(1, (bool)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
