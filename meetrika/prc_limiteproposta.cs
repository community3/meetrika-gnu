/*
               File: PRC_LimiteProposta
        Description: Limite das Propostas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:7.20
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_limiteproposta : GXProcedure
   {
      public prc_limiteproposta( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_limiteproposta( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContratoServicos_Codigo ,
                           out decimal aP1_LimiteProposta )
      {
         this.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         this.AV8LimiteProposta = 0 ;
         initialize();
         executePrivate();
         aP0_ContratoServicos_Codigo=this.A160ContratoServicos_Codigo;
         aP1_LimiteProposta=this.AV8LimiteProposta;
      }

      public decimal executeUdp( ref int aP0_ContratoServicos_Codigo )
      {
         this.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         this.AV8LimiteProposta = 0 ;
         initialize();
         executePrivate();
         aP0_ContratoServicos_Codigo=this.A160ContratoServicos_Codigo;
         aP1_LimiteProposta=this.AV8LimiteProposta;
         return AV8LimiteProposta ;
      }

      public void executeSubmit( ref int aP0_ContratoServicos_Codigo ,
                                 out decimal aP1_LimiteProposta )
      {
         prc_limiteproposta objprc_limiteproposta;
         objprc_limiteproposta = new prc_limiteproposta();
         objprc_limiteproposta.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         objprc_limiteproposta.AV8LimiteProposta = 0 ;
         objprc_limiteproposta.context.SetSubmitInitialConfig(context);
         objprc_limiteproposta.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_limiteproposta);
         aP0_ContratoServicos_Codigo=this.A160ContratoServicos_Codigo;
         aP1_LimiteProposta=this.AV8LimiteProposta;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_limiteproposta)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00DE2 */
         pr_default.execute(0, new Object[] {A160ContratoServicos_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1799ContratoServicos_LimiteProposta = P00DE2_A1799ContratoServicos_LimiteProposta[0];
            n1799ContratoServicos_LimiteProposta = P00DE2_n1799ContratoServicos_LimiteProposta[0];
            AV8LimiteProposta = A1799ContratoServicos_LimiteProposta;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00DE2_A160ContratoServicos_Codigo = new int[1] ;
         P00DE2_A1799ContratoServicos_LimiteProposta = new decimal[1] ;
         P00DE2_n1799ContratoServicos_LimiteProposta = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_limiteproposta__default(),
            new Object[][] {
                new Object[] {
               P00DE2_A160ContratoServicos_Codigo, P00DE2_A1799ContratoServicos_LimiteProposta, P00DE2_n1799ContratoServicos_LimiteProposta
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A160ContratoServicos_Codigo ;
      private decimal AV8LimiteProposta ;
      private decimal A1799ContratoServicos_LimiteProposta ;
      private String scmdbuf ;
      private bool n1799ContratoServicos_LimiteProposta ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContratoServicos_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00DE2_A160ContratoServicos_Codigo ;
      private decimal[] P00DE2_A1799ContratoServicos_LimiteProposta ;
      private bool[] P00DE2_n1799ContratoServicos_LimiteProposta ;
      private decimal aP1_LimiteProposta ;
   }

   public class prc_limiteproposta__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00DE2 ;
          prmP00DE2 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00DE2", "SELECT TOP 1 [ContratoServicos_Codigo], [ContratoServicos_LimiteProposta] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ORDER BY [ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00DE2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
