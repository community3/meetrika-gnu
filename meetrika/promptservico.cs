/*
               File: PromptServico
        Description: Selecione Servico
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:36:5.77
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptservico : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptservico( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptservico( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutServico_Codigo ,
                           ref String aP1_InOutServico_Descricao )
      {
         this.AV7InOutServico_Codigo = aP0_InOutServico_Codigo;
         this.AV8InOutServico_Descricao = aP1_InOutServico_Descricao;
         executePrivate();
         aP0_InOutServico_Codigo=this.AV7InOutServico_Codigo;
         aP1_InOutServico_Descricao=this.AV8InOutServico_Descricao;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavServico_ativo = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         dynavServicogrupo_codigo1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         dynavServicogrupo_codigo2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         dynavServicogrupo_codigo3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_75 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_75_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_75_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV33Servico_Sigla1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Servico_Sigla1", AV33Servico_Sigla1);
               AV34Servico_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Servico_Nome1", AV34Servico_Nome1);
               AV35ServicoGrupo_Codigo1 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35ServicoGrupo_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35ServicoGrupo_Codigo1), 6, 0)));
               AV20DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               AV36Servico_Sigla2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Servico_Sigla2", AV36Servico_Sigla2);
               AV37Servico_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Servico_Nome2", AV37Servico_Nome2);
               AV38ServicoGrupo_Codigo2 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ServicoGrupo_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38ServicoGrupo_Codigo2), 6, 0)));
               AV25DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
               AV39Servico_Sigla3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Servico_Sigla3", AV39Servico_Sigla3);
               AV40Servico_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Servico_Nome3", AV40Servico_Nome3);
               AV41ServicoGrupo_Codigo3 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ServicoGrupo_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41ServicoGrupo_Codigo3), 6, 0)));
               AV19DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV24DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
               AV44TFServico_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFServico_Nome", AV44TFServico_Nome);
               AV45TFServico_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFServico_Nome_Sel", AV45TFServico_Nome_Sel);
               AV48TFServico_Sigla = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFServico_Sigla", AV48TFServico_Sigla);
               AV49TFServico_Sigla_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFServico_Sigla_Sel", AV49TFServico_Sigla_Sel);
               AV52TFServicoGrupo_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFServicoGrupo_Descricao", AV52TFServicoGrupo_Descricao);
               AV53TFServicoGrupo_Descricao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFServicoGrupo_Descricao_Sel", AV53TFServicoGrupo_Descricao_Sel);
               AV46ddo_Servico_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ddo_Servico_NomeTitleControlIdToReplace", AV46ddo_Servico_NomeTitleControlIdToReplace);
               AV50ddo_Servico_SiglaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ddo_Servico_SiglaTitleControlIdToReplace", AV50ddo_Servico_SiglaTitleControlIdToReplace);
               AV54ddo_ServicoGrupo_DescricaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ddo_ServicoGrupo_DescricaoTitleControlIdToReplace", AV54ddo_ServicoGrupo_DescricaoTitleControlIdToReplace);
               AV42Servico_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Servico_Ativo", AV42Servico_Ativo);
               AV62Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV30DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
               AV29DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV33Servico_Sigla1, AV34Servico_Nome1, AV35ServicoGrupo_Codigo1, AV20DynamicFiltersSelector2, AV36Servico_Sigla2, AV37Servico_Nome2, AV38ServicoGrupo_Codigo2, AV25DynamicFiltersSelector3, AV39Servico_Sigla3, AV40Servico_Nome3, AV41ServicoGrupo_Codigo3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV44TFServico_Nome, AV45TFServico_Nome_Sel, AV48TFServico_Sigla, AV49TFServico_Sigla_Sel, AV52TFServicoGrupo_Descricao, AV53TFServicoGrupo_Descricao_Sel, AV46ddo_Servico_NomeTitleControlIdToReplace, AV50ddo_Servico_SiglaTitleControlIdToReplace, AV54ddo_ServicoGrupo_DescricaoTitleControlIdToReplace, AV42Servico_Ativo, AV62Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "PromptServico";
               forbiddenHiddens = forbiddenHiddens + A156Servico_Descricao;
               GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
               GXUtil.WriteLog("promptservico:[SendSecurityCheck value for]"+"Servico_Descricao:"+A156Servico_Descricao);
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutServico_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutServico_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutServico_Descricao = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutServico_Descricao", AV8InOutServico_Descricao);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PA6Z2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV62Pgmname = "PromptServico";
               context.Gx_err = 0;
               WS6Z2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE6Z2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202031173666");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptservico.aspx") + "?" + UrlEncode("" +AV7InOutServico_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV8InOutServico_Descricao))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vSERVICO_SIGLA1", StringUtil.RTrim( AV33Servico_Sigla1));
         GxWebStd.gx_hidden_field( context, "GXH_vSERVICO_NOME1", StringUtil.RTrim( AV34Servico_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vSERVICOGRUPO_CODIGO1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35ServicoGrupo_Codigo1), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV20DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vSERVICO_SIGLA2", StringUtil.RTrim( AV36Servico_Sigla2));
         GxWebStd.gx_hidden_field( context, "GXH_vSERVICO_NOME2", StringUtil.RTrim( AV37Servico_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vSERVICOGRUPO_CODIGO2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38ServicoGrupo_Codigo2), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV25DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vSERVICO_SIGLA3", StringUtil.RTrim( AV39Servico_Sigla3));
         GxWebStd.gx_hidden_field( context, "GXH_vSERVICO_NOME3", StringUtil.RTrim( AV40Servico_Nome3));
         GxWebStd.gx_hidden_field( context, "GXH_vSERVICOGRUPO_CODIGO3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV41ServicoGrupo_Codigo3), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV19DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV24DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICO_NOME", StringUtil.RTrim( AV44TFServico_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICO_NOME_SEL", StringUtil.RTrim( AV45TFServico_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICO_SIGLA", StringUtil.RTrim( AV48TFServico_Sigla));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICO_SIGLA_SEL", StringUtil.RTrim( AV49TFServico_Sigla_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICOGRUPO_DESCRICAO", AV52TFServicoGrupo_Descricao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICOGRUPO_DESCRICAO_SEL", AV53TFServicoGrupo_Descricao_Sel);
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_75", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_75), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV57GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV58GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV55DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV55DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSERVICO_NOMETITLEFILTERDATA", AV43Servico_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSERVICO_NOMETITLEFILTERDATA", AV43Servico_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSERVICO_SIGLATITLEFILTERDATA", AV47Servico_SiglaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSERVICO_SIGLATITLEFILTERDATA", AV47Servico_SiglaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSERVICOGRUPO_DESCRICAOTITLEFILTERDATA", AV51ServicoGrupo_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSERVICOGRUPO_DESCRICAOTITLEFILTERDATA", AV51ServicoGrupo_DescricaoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV62Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV30DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV29DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTSERVICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutServico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTSERVICO_DESCRICAO", AV8InOutServico_Descricao);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Caption", StringUtil.RTrim( Ddo_servico_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Tooltip", StringUtil.RTrim( Ddo_servico_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Cls", StringUtil.RTrim( Ddo_servico_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_servico_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_servico_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_servico_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_servico_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_servico_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_servico_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_servico_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_servico_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Filtertype", StringUtil.RTrim( Ddo_servico_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_servico_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_servico_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Datalisttype", StringUtil.RTrim( Ddo_servico_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Datalistproc", StringUtil.RTrim( Ddo_servico_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_servico_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Sortasc", StringUtil.RTrim( Ddo_servico_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Sortdsc", StringUtil.RTrim( Ddo_servico_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Loadingdata", StringUtil.RTrim( Ddo_servico_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_servico_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_servico_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_servico_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Caption", StringUtil.RTrim( Ddo_servico_sigla_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Tooltip", StringUtil.RTrim( Ddo_servico_sigla_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Cls", StringUtil.RTrim( Ddo_servico_sigla_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Filteredtext_set", StringUtil.RTrim( Ddo_servico_sigla_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Selectedvalue_set", StringUtil.RTrim( Ddo_servico_sigla_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Dropdownoptionstype", StringUtil.RTrim( Ddo_servico_sigla_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_servico_sigla_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Includesortasc", StringUtil.BoolToStr( Ddo_servico_sigla_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Includesortdsc", StringUtil.BoolToStr( Ddo_servico_sigla_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Sortedstatus", StringUtil.RTrim( Ddo_servico_sigla_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Includefilter", StringUtil.BoolToStr( Ddo_servico_sigla_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Filtertype", StringUtil.RTrim( Ddo_servico_sigla_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Filterisrange", StringUtil.BoolToStr( Ddo_servico_sigla_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Includedatalist", StringUtil.BoolToStr( Ddo_servico_sigla_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Datalisttype", StringUtil.RTrim( Ddo_servico_sigla_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Datalistproc", StringUtil.RTrim( Ddo_servico_sigla_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_servico_sigla_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Sortasc", StringUtil.RTrim( Ddo_servico_sigla_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Sortdsc", StringUtil.RTrim( Ddo_servico_sigla_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Loadingdata", StringUtil.RTrim( Ddo_servico_sigla_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Cleanfilter", StringUtil.RTrim( Ddo_servico_sigla_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Noresultsfound", StringUtil.RTrim( Ddo_servico_sigla_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Searchbuttontext", StringUtil.RTrim( Ddo_servico_sigla_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Caption", StringUtil.RTrim( Ddo_servicogrupo_descricao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_servicogrupo_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Cls", StringUtil.RTrim( Ddo_servicogrupo_descricao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_servicogrupo_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_servicogrupo_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_servicogrupo_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_servicogrupo_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_servicogrupo_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_servicogrupo_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_servicogrupo_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_servicogrupo_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_servicogrupo_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_servicogrupo_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_servicogrupo_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_servicogrupo_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_servicogrupo_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_servicogrupo_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_servicogrupo_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_servicogrupo_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_servicogrupo_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_servicogrupo_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_servicogrupo_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_servicogrupo_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_servico_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_servico_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_servico_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Activeeventkey", StringUtil.RTrim( Ddo_servico_sigla_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Filteredtext_get", StringUtil.RTrim( Ddo_servico_sigla_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Selectedvalue_get", StringUtil.RTrim( Ddo_servico_sigla_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_servicogrupo_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_servicogrupo_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_servicogrupo_descricao_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "PromptServico";
         forbiddenHiddens = forbiddenHiddens + A156Servico_Descricao;
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("promptservico:[SendSecurityCheck value for]"+"Servico_Descricao:"+A156Servico_Descricao);
      }

      protected void RenderHtmlCloseForm6Z2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptServico" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Servico" ;
      }

      protected void WB6Z0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_6Z2( true) ;
         }
         else
         {
            wb_table1_2_6Z2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_6Z2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Multiple line edit */
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtServico_Descricao_Internalname, A156Servico_Descricao, "", "", 0, edtServico_Descricao_Visible, 0, 0, 600, "px", 2, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "DescricaoLonga", "HLP_PromptServico.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'',false,'" + sGXsfl_75_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV19DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(86, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,86);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'',false,'" + sGXsfl_75_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV24DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(87, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,87);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'',false,'" + sGXsfl_75_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservico_nome_Internalname, StringUtil.RTrim( AV44TFServico_Nome), StringUtil.RTrim( context.localUtil.Format( AV44TFServico_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,88);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservico_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfservico_nome_Visible, 1, 0, "text", "", 380, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptServico.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'" + sGXsfl_75_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservico_nome_sel_Internalname, StringUtil.RTrim( AV45TFServico_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV45TFServico_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,89);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservico_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfservico_nome_sel_Visible, 1, 0, "text", "", 380, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptServico.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'',false,'" + sGXsfl_75_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservico_sigla_Internalname, StringUtil.RTrim( AV48TFServico_Sigla), StringUtil.RTrim( context.localUtil.Format( AV48TFServico_Sigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,90);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservico_sigla_Jsonclick, 0, "Attribute", "", "", "", edtavTfservico_sigla_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptServico.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'" + sGXsfl_75_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservico_sigla_sel_Internalname, StringUtil.RTrim( AV49TFServico_Sigla_Sel), StringUtil.RTrim( context.localUtil.Format( AV49TFServico_Sigla_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,91);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservico_sigla_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfservico_sigla_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptServico.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'" + sGXsfl_75_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservicogrupo_descricao_Internalname, AV52TFServicoGrupo_Descricao, StringUtil.RTrim( context.localUtil.Format( AV52TFServicoGrupo_Descricao, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,92);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservicogrupo_descricao_Jsonclick, 0, "Attribute", "", "", "", edtavTfservicogrupo_descricao_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptServico.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'',false,'" + sGXsfl_75_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservicogrupo_descricao_sel_Internalname, AV53TFServicoGrupo_Descricao_Sel, StringUtil.RTrim( context.localUtil.Format( AV53TFServicoGrupo_Descricao_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,93);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservicogrupo_descricao_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfservicogrupo_descricao_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptServico.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SERVICO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'" + sGXsfl_75_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_servico_nometitlecontrolidtoreplace_Internalname, AV46ddo_Servico_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,95);\"", 0, edtavDdo_servico_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptServico.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SERVICO_SIGLAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_75_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_servico_siglatitlecontrolidtoreplace_Internalname, AV50ddo_Servico_SiglaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,97);\"", 0, edtavDdo_servico_siglatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptServico.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SERVICOGRUPO_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'" + sGXsfl_75_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_servicogrupo_descricaotitlecontrolidtoreplace_Internalname, AV54ddo_ServicoGrupo_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,99);\"", 0, edtavDdo_servicogrupo_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptServico.htm");
         }
         wbLoad = true;
      }

      protected void START6Z2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Servico", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP6Z0( ) ;
      }

      protected void WS6Z2( )
      {
         START6Z2( ) ;
         EVT6Z2( ) ;
      }

      protected void EVT6Z2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E116Z2 */
                           E116Z2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_SERVICO_NOME.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E126Z2 */
                           E126Z2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_SERVICO_SIGLA.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E136Z2 */
                           E136Z2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_SERVICOGRUPO_DESCRICAO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E146Z2 */
                           E146Z2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E156Z2 */
                           E156Z2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E166Z2 */
                           E166Z2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E176Z2 */
                           E176Z2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E186Z2 */
                           E186Z2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E196Z2 */
                           E196Z2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E206Z2 */
                           E206Z2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E216Z2 */
                           E216Z2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E226Z2 */
                           E226Z2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E236Z2 */
                           E236Z2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E246Z2 */
                           E246Z2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_75_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_75_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_75_idx), 4, 0)), 4, "0");
                           SubsflControlProps_752( ) ;
                           AV31Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV31Select)) ? AV61Select_GXI : context.convertURL( context.PathToRelativeUrl( AV31Select))));
                           A155Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( edtServico_Codigo_Internalname), ",", "."));
                           A608Servico_Nome = StringUtil.Upper( cgiGet( edtServico_Nome_Internalname));
                           A605Servico_Sigla = StringUtil.Upper( cgiGet( edtServico_Sigla_Internalname));
                           A158ServicoGrupo_Descricao = cgiGet( edtServicoGrupo_Descricao_Internalname);
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E256Z2 */
                                 E256Z2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E266Z2 */
                                 E266Z2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E276Z2 */
                                 E276Z2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Servico_sigla1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICO_SIGLA1"), AV33Servico_Sigla1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Servico_nome1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICO_NOME1"), AV34Servico_Nome1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Servicogrupo_codigo1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vSERVICOGRUPO_CODIGO1"), ",", ".") != Convert.ToDecimal( AV35ServicoGrupo_Codigo1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Servico_sigla2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICO_SIGLA2"), AV36Servico_Sigla2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Servico_nome2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICO_NOME2"), AV37Servico_Nome2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Servicogrupo_codigo2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vSERVICOGRUPO_CODIGO2"), ",", ".") != Convert.ToDecimal( AV38ServicoGrupo_Codigo2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV25DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Servico_sigla3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICO_SIGLA3"), AV39Servico_Sigla3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Servico_nome3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICO_NOME3"), AV40Servico_Nome3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Servicogrupo_codigo3 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vSERVICOGRUPO_CODIGO3"), ",", ".") != Convert.ToDecimal( AV41ServicoGrupo_Codigo3 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV24DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfservico_nome Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICO_NOME"), AV44TFServico_Nome) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfservico_nome_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICO_NOME_SEL"), AV45TFServico_Nome_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfservico_sigla Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICO_SIGLA"), AV48TFServico_Sigla) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfservico_sigla_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICO_SIGLA_SEL"), AV49TFServico_Sigla_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfservicogrupo_descricao Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICOGRUPO_DESCRICAO"), AV52TFServicoGrupo_Descricao) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfservicogrupo_descricao_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICOGRUPO_DESCRICAO_SEL"), AV53TFServicoGrupo_Descricao_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E286Z2 */
                                       E286Z2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE6Z2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm6Z2( ) ;
            }
         }
      }

      protected void PA6Z2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavServico_ativo.Name = "vSERVICO_ATIVO";
            cmbavServico_ativo.WebTags = "";
            cmbavServico_ativo.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbavServico_ativo.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbavServico_ativo.ItemCount > 0 )
            {
               AV42Servico_Ativo = StringUtil.StrToBool( cmbavServico_ativo.getValidValue(StringUtil.BoolToStr( AV42Servico_Ativo)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Servico_Ativo", AV42Servico_Ativo);
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("SERVICO_SIGLA", "Sigla", 0);
            cmbavDynamicfiltersselector1.addItem("SERVICO_NOME", "Nome", 0);
            cmbavDynamicfiltersselector1.addItem("SERVICOGRUPO_CODIGO", "Grupo", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            dynavServicogrupo_codigo1.Name = "vSERVICOGRUPO_CODIGO1";
            dynavServicogrupo_codigo1.WebTags = "";
            dynavServicogrupo_codigo1.removeAllItems();
            /* Using cursor H006Z2 */
            pr_default.execute(0);
            while ( (pr_default.getStatus(0) != 101) )
            {
               dynavServicogrupo_codigo1.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(H006Z2_A157ServicoGrupo_Codigo[0]), 6, 0)), H006Z2_A158ServicoGrupo_Descricao[0], 0);
               pr_default.readNext(0);
            }
            pr_default.close(0);
            if ( dynavServicogrupo_codigo1.ItemCount > 0 )
            {
               AV35ServicoGrupo_Codigo1 = (int)(NumberUtil.Val( dynavServicogrupo_codigo1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV35ServicoGrupo_Codigo1), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35ServicoGrupo_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35ServicoGrupo_Codigo1), 6, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("SERVICO_SIGLA", "Sigla", 0);
            cmbavDynamicfiltersselector2.addItem("SERVICO_NOME", "Nome", 0);
            cmbavDynamicfiltersselector2.addItem("SERVICOGRUPO_CODIGO", "Grupo", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            }
            dynavServicogrupo_codigo2.Name = "vSERVICOGRUPO_CODIGO2";
            dynavServicogrupo_codigo2.WebTags = "";
            dynavServicogrupo_codigo2.removeAllItems();
            /* Using cursor H006Z3 */
            pr_default.execute(1);
            while ( (pr_default.getStatus(1) != 101) )
            {
               dynavServicogrupo_codigo2.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(H006Z3_A157ServicoGrupo_Codigo[0]), 6, 0)), H006Z3_A158ServicoGrupo_Descricao[0], 0);
               pr_default.readNext(1);
            }
            pr_default.close(1);
            if ( dynavServicogrupo_codigo2.ItemCount > 0 )
            {
               AV38ServicoGrupo_Codigo2 = (int)(NumberUtil.Val( dynavServicogrupo_codigo2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV38ServicoGrupo_Codigo2), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ServicoGrupo_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38ServicoGrupo_Codigo2), 6, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("SERVICO_SIGLA", "Sigla", 0);
            cmbavDynamicfiltersselector3.addItem("SERVICO_NOME", "Nome", 0);
            cmbavDynamicfiltersselector3.addItem("SERVICOGRUPO_CODIGO", "Grupo", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV25DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV25DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
            }
            dynavServicogrupo_codigo3.Name = "vSERVICOGRUPO_CODIGO3";
            dynavServicogrupo_codigo3.WebTags = "";
            dynavServicogrupo_codigo3.removeAllItems();
            /* Using cursor H006Z4 */
            pr_default.execute(2);
            while ( (pr_default.getStatus(2) != 101) )
            {
               dynavServicogrupo_codigo3.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(H006Z4_A157ServicoGrupo_Codigo[0]), 6, 0)), H006Z4_A158ServicoGrupo_Descricao[0], 0);
               pr_default.readNext(2);
            }
            pr_default.close(2);
            if ( dynavServicogrupo_codigo3.ItemCount > 0 )
            {
               AV41ServicoGrupo_Codigo3 = (int)(NumberUtil.Val( dynavServicogrupo_codigo3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV41ServicoGrupo_Codigo3), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ServicoGrupo_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41ServicoGrupo_Codigo3), 6, 0)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvSERVICOGRUPO_CODIGO36Z1( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvSERVICOGRUPO_CODIGO3_data6Z1( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvSERVICOGRUPO_CODIGO3_html6Z1( )
      {
         int gxdynajaxvalue ;
         GXDLVvSERVICOGRUPO_CODIGO3_data6Z1( ) ;
         gxdynajaxindex = 1;
         dynavServicogrupo_codigo3.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavServicogrupo_codigo3.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavServicogrupo_codigo3.ItemCount > 0 )
         {
            AV41ServicoGrupo_Codigo3 = (int)(NumberUtil.Val( dynavServicogrupo_codigo3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV41ServicoGrupo_Codigo3), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ServicoGrupo_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41ServicoGrupo_Codigo3), 6, 0)));
         }
      }

      protected void GXDLVvSERVICOGRUPO_CODIGO3_data6Z1( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor H006Z5 */
         pr_default.execute(3);
         while ( (pr_default.getStatus(3) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H006Z5_A157ServicoGrupo_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H006Z5_A158ServicoGrupo_Descricao[0]);
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      protected void GXDLVvSERVICOGRUPO_CODIGO26Z1( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvSERVICOGRUPO_CODIGO2_data6Z1( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvSERVICOGRUPO_CODIGO2_html6Z1( )
      {
         int gxdynajaxvalue ;
         GXDLVvSERVICOGRUPO_CODIGO2_data6Z1( ) ;
         gxdynajaxindex = 1;
         dynavServicogrupo_codigo2.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavServicogrupo_codigo2.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavServicogrupo_codigo2.ItemCount > 0 )
         {
            AV38ServicoGrupo_Codigo2 = (int)(NumberUtil.Val( dynavServicogrupo_codigo2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV38ServicoGrupo_Codigo2), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ServicoGrupo_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38ServicoGrupo_Codigo2), 6, 0)));
         }
      }

      protected void GXDLVvSERVICOGRUPO_CODIGO2_data6Z1( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor H006Z6 */
         pr_default.execute(4);
         while ( (pr_default.getStatus(4) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H006Z6_A157ServicoGrupo_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H006Z6_A158ServicoGrupo_Descricao[0]);
            pr_default.readNext(4);
         }
         pr_default.close(4);
      }

      protected void GXDLVvSERVICOGRUPO_CODIGO16Z1( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvSERVICOGRUPO_CODIGO1_data6Z1( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvSERVICOGRUPO_CODIGO1_html6Z1( )
      {
         int gxdynajaxvalue ;
         GXDLVvSERVICOGRUPO_CODIGO1_data6Z1( ) ;
         gxdynajaxindex = 1;
         dynavServicogrupo_codigo1.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavServicogrupo_codigo1.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavServicogrupo_codigo1.ItemCount > 0 )
         {
            AV35ServicoGrupo_Codigo1 = (int)(NumberUtil.Val( dynavServicogrupo_codigo1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV35ServicoGrupo_Codigo1), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35ServicoGrupo_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35ServicoGrupo_Codigo1), 6, 0)));
         }
      }

      protected void GXDLVvSERVICOGRUPO_CODIGO1_data6Z1( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor H006Z7 */
         pr_default.execute(5);
         while ( (pr_default.getStatus(5) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H006Z7_A157ServicoGrupo_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H006Z7_A158ServicoGrupo_Descricao[0]);
            pr_default.readNext(5);
         }
         pr_default.close(5);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_752( ) ;
         while ( nGXsfl_75_idx <= nRC_GXsfl_75 )
         {
            sendrow_752( ) ;
            nGXsfl_75_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_75_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_75_idx+1));
            sGXsfl_75_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_75_idx), 4, 0)), 4, "0");
            SubsflControlProps_752( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       String AV33Servico_Sigla1 ,
                                       String AV34Servico_Nome1 ,
                                       int AV35ServicoGrupo_Codigo1 ,
                                       String AV20DynamicFiltersSelector2 ,
                                       String AV36Servico_Sigla2 ,
                                       String AV37Servico_Nome2 ,
                                       int AV38ServicoGrupo_Codigo2 ,
                                       String AV25DynamicFiltersSelector3 ,
                                       String AV39Servico_Sigla3 ,
                                       String AV40Servico_Nome3 ,
                                       int AV41ServicoGrupo_Codigo3 ,
                                       bool AV19DynamicFiltersEnabled2 ,
                                       bool AV24DynamicFiltersEnabled3 ,
                                       String AV44TFServico_Nome ,
                                       String AV45TFServico_Nome_Sel ,
                                       String AV48TFServico_Sigla ,
                                       String AV49TFServico_Sigla_Sel ,
                                       String AV52TFServicoGrupo_Descricao ,
                                       String AV53TFServicoGrupo_Descricao_Sel ,
                                       String AV46ddo_Servico_NomeTitleControlIdToReplace ,
                                       String AV50ddo_Servico_SiglaTitleControlIdToReplace ,
                                       String AV54ddo_ServicoGrupo_DescricaoTitleControlIdToReplace ,
                                       bool AV42Servico_Ativo ,
                                       String AV62Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV30DynamicFiltersIgnoreFirst ,
                                       bool AV29DynamicFiltersRemoving )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF6Z2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_SERVICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A155Servico_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "SERVICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_SERVICO_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A608Servico_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "SERVICO_NOME", StringUtil.RTrim( A608Servico_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_SERVICO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A605Servico_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, "SERVICO_SIGLA", StringUtil.RTrim( A605Servico_Sigla));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavServico_ativo.ItemCount > 0 )
         {
            AV42Servico_Ativo = StringUtil.StrToBool( cmbavServico_ativo.getValidValue(StringUtil.BoolToStr( AV42Servico_Ativo)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Servico_Ativo", AV42Servico_Ativo);
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( dynavServicogrupo_codigo1.ItemCount > 0 )
         {
            AV35ServicoGrupo_Codigo1 = (int)(NumberUtil.Val( dynavServicogrupo_codigo1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV35ServicoGrupo_Codigo1), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35ServicoGrupo_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35ServicoGrupo_Codigo1), 6, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         }
         if ( dynavServicogrupo_codigo2.ItemCount > 0 )
         {
            AV38ServicoGrupo_Codigo2 = (int)(NumberUtil.Val( dynavServicogrupo_codigo2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV38ServicoGrupo_Codigo2), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ServicoGrupo_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38ServicoGrupo_Codigo2), 6, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV25DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV25DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         }
         if ( dynavServicogrupo_codigo3.ItemCount > 0 )
         {
            AV41ServicoGrupo_Codigo3 = (int)(NumberUtil.Val( dynavServicogrupo_codigo3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV41ServicoGrupo_Codigo3), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ServicoGrupo_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41ServicoGrupo_Codigo3), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF6Z2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV62Pgmname = "PromptServico";
         context.Gx_err = 0;
      }

      protected void RF6Z2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 75;
         /* Execute user event: E266Z2 */
         E266Z2 ();
         nGXsfl_75_idx = 1;
         sGXsfl_75_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_75_idx), 4, 0)), 4, "0");
         SubsflControlProps_752( ) ;
         nGXsfl_75_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_752( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(6, new Object[]{ new Object[]{
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV33Servico_Sigla1 ,
                                                 AV34Servico_Nome1 ,
                                                 AV35ServicoGrupo_Codigo1 ,
                                                 AV19DynamicFiltersEnabled2 ,
                                                 AV20DynamicFiltersSelector2 ,
                                                 AV36Servico_Sigla2 ,
                                                 AV37Servico_Nome2 ,
                                                 AV38ServicoGrupo_Codigo2 ,
                                                 AV24DynamicFiltersEnabled3 ,
                                                 AV25DynamicFiltersSelector3 ,
                                                 AV39Servico_Sigla3 ,
                                                 AV40Servico_Nome3 ,
                                                 AV41ServicoGrupo_Codigo3 ,
                                                 AV45TFServico_Nome_Sel ,
                                                 AV44TFServico_Nome ,
                                                 AV49TFServico_Sigla_Sel ,
                                                 AV48TFServico_Sigla ,
                                                 AV53TFServicoGrupo_Descricao_Sel ,
                                                 AV52TFServicoGrupo_Descricao ,
                                                 A605Servico_Sigla ,
                                                 A608Servico_Nome ,
                                                 A157ServicoGrupo_Codigo ,
                                                 A159ServicoGrupo_Ativo ,
                                                 A158ServicoGrupo_Descricao ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A632Servico_Ativo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                                 }
            });
            lV34Servico_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV34Servico_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Servico_Nome1", AV34Servico_Nome1);
            lV37Servico_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV37Servico_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Servico_Nome2", AV37Servico_Nome2);
            lV40Servico_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV40Servico_Nome3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Servico_Nome3", AV40Servico_Nome3);
            lV44TFServico_Nome = StringUtil.PadR( StringUtil.RTrim( AV44TFServico_Nome), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFServico_Nome", AV44TFServico_Nome);
            lV48TFServico_Sigla = StringUtil.PadR( StringUtil.RTrim( AV48TFServico_Sigla), 15, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFServico_Sigla", AV48TFServico_Sigla);
            lV52TFServicoGrupo_Descricao = StringUtil.Concat( StringUtil.RTrim( AV52TFServicoGrupo_Descricao), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFServicoGrupo_Descricao", AV52TFServicoGrupo_Descricao);
            /* Using cursor H006Z8 */
            pr_default.execute(6, new Object[] {AV33Servico_Sigla1, lV34Servico_Nome1, AV35ServicoGrupo_Codigo1, AV36Servico_Sigla2, lV37Servico_Nome2, AV38ServicoGrupo_Codigo2, AV39Servico_Sigla3, lV40Servico_Nome3, AV41ServicoGrupo_Codigo3, lV44TFServico_Nome, AV45TFServico_Nome_Sel, lV48TFServico_Sigla, AV49TFServico_Sigla_Sel, lV52TFServicoGrupo_Descricao, AV53TFServicoGrupo_Descricao_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_75_idx = 1;
            while ( ( (pr_default.getStatus(6) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A159ServicoGrupo_Ativo = H006Z8_A159ServicoGrupo_Ativo[0];
               A157ServicoGrupo_Codigo = H006Z8_A157ServicoGrupo_Codigo[0];
               A632Servico_Ativo = H006Z8_A632Servico_Ativo[0];
               A156Servico_Descricao = H006Z8_A156Servico_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A156Servico_Descricao", A156Servico_Descricao);
               n156Servico_Descricao = H006Z8_n156Servico_Descricao[0];
               A158ServicoGrupo_Descricao = H006Z8_A158ServicoGrupo_Descricao[0];
               A605Servico_Sigla = H006Z8_A605Servico_Sigla[0];
               A608Servico_Nome = H006Z8_A608Servico_Nome[0];
               A155Servico_Codigo = H006Z8_A155Servico_Codigo[0];
               A159ServicoGrupo_Ativo = H006Z8_A159ServicoGrupo_Ativo[0];
               A158ServicoGrupo_Descricao = H006Z8_A158ServicoGrupo_Descricao[0];
               /* Execute user event: E276Z2 */
               E276Z2 ();
               pr_default.readNext(6);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(6) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(6);
            wbEnd = 75;
            WB6Z0( ) ;
         }
         nGXsfl_75_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(7, new Object[]{ new Object[]{
                                              AV15DynamicFiltersSelector1 ,
                                              AV33Servico_Sigla1 ,
                                              AV34Servico_Nome1 ,
                                              AV35ServicoGrupo_Codigo1 ,
                                              AV19DynamicFiltersEnabled2 ,
                                              AV20DynamicFiltersSelector2 ,
                                              AV36Servico_Sigla2 ,
                                              AV37Servico_Nome2 ,
                                              AV38ServicoGrupo_Codigo2 ,
                                              AV24DynamicFiltersEnabled3 ,
                                              AV25DynamicFiltersSelector3 ,
                                              AV39Servico_Sigla3 ,
                                              AV40Servico_Nome3 ,
                                              AV41ServicoGrupo_Codigo3 ,
                                              AV45TFServico_Nome_Sel ,
                                              AV44TFServico_Nome ,
                                              AV49TFServico_Sigla_Sel ,
                                              AV48TFServico_Sigla ,
                                              AV53TFServicoGrupo_Descricao_Sel ,
                                              AV52TFServicoGrupo_Descricao ,
                                              A605Servico_Sigla ,
                                              A608Servico_Nome ,
                                              A157ServicoGrupo_Codigo ,
                                              A159ServicoGrupo_Ativo ,
                                              A158ServicoGrupo_Descricao ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              A632Servico_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                              }
         });
         lV34Servico_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV34Servico_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Servico_Nome1", AV34Servico_Nome1);
         lV37Servico_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV37Servico_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Servico_Nome2", AV37Servico_Nome2);
         lV40Servico_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV40Servico_Nome3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Servico_Nome3", AV40Servico_Nome3);
         lV44TFServico_Nome = StringUtil.PadR( StringUtil.RTrim( AV44TFServico_Nome), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFServico_Nome", AV44TFServico_Nome);
         lV48TFServico_Sigla = StringUtil.PadR( StringUtil.RTrim( AV48TFServico_Sigla), 15, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFServico_Sigla", AV48TFServico_Sigla);
         lV52TFServicoGrupo_Descricao = StringUtil.Concat( StringUtil.RTrim( AV52TFServicoGrupo_Descricao), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFServicoGrupo_Descricao", AV52TFServicoGrupo_Descricao);
         /* Using cursor H006Z9 */
         pr_default.execute(7, new Object[] {AV33Servico_Sigla1, lV34Servico_Nome1, AV35ServicoGrupo_Codigo1, AV36Servico_Sigla2, lV37Servico_Nome2, AV38ServicoGrupo_Codigo2, AV39Servico_Sigla3, lV40Servico_Nome3, AV41ServicoGrupo_Codigo3, lV44TFServico_Nome, AV45TFServico_Nome_Sel, lV48TFServico_Sigla, AV49TFServico_Sigla_Sel, lV52TFServicoGrupo_Descricao, AV53TFServicoGrupo_Descricao_Sel});
         GRID_nRecordCount = H006Z9_AGRID_nRecordCount[0];
         pr_default.close(7);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV33Servico_Sigla1, AV34Servico_Nome1, AV35ServicoGrupo_Codigo1, AV20DynamicFiltersSelector2, AV36Servico_Sigla2, AV37Servico_Nome2, AV38ServicoGrupo_Codigo2, AV25DynamicFiltersSelector3, AV39Servico_Sigla3, AV40Servico_Nome3, AV41ServicoGrupo_Codigo3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV44TFServico_Nome, AV45TFServico_Nome_Sel, AV48TFServico_Sigla, AV49TFServico_Sigla_Sel, AV52TFServicoGrupo_Descricao, AV53TFServicoGrupo_Descricao_Sel, AV46ddo_Servico_NomeTitleControlIdToReplace, AV50ddo_Servico_SiglaTitleControlIdToReplace, AV54ddo_ServicoGrupo_DescricaoTitleControlIdToReplace, AV42Servico_Ativo, AV62Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV33Servico_Sigla1, AV34Servico_Nome1, AV35ServicoGrupo_Codigo1, AV20DynamicFiltersSelector2, AV36Servico_Sigla2, AV37Servico_Nome2, AV38ServicoGrupo_Codigo2, AV25DynamicFiltersSelector3, AV39Servico_Sigla3, AV40Servico_Nome3, AV41ServicoGrupo_Codigo3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV44TFServico_Nome, AV45TFServico_Nome_Sel, AV48TFServico_Sigla, AV49TFServico_Sigla_Sel, AV52TFServicoGrupo_Descricao, AV53TFServicoGrupo_Descricao_Sel, AV46ddo_Servico_NomeTitleControlIdToReplace, AV50ddo_Servico_SiglaTitleControlIdToReplace, AV54ddo_ServicoGrupo_DescricaoTitleControlIdToReplace, AV42Servico_Ativo, AV62Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV33Servico_Sigla1, AV34Servico_Nome1, AV35ServicoGrupo_Codigo1, AV20DynamicFiltersSelector2, AV36Servico_Sigla2, AV37Servico_Nome2, AV38ServicoGrupo_Codigo2, AV25DynamicFiltersSelector3, AV39Servico_Sigla3, AV40Servico_Nome3, AV41ServicoGrupo_Codigo3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV44TFServico_Nome, AV45TFServico_Nome_Sel, AV48TFServico_Sigla, AV49TFServico_Sigla_Sel, AV52TFServicoGrupo_Descricao, AV53TFServicoGrupo_Descricao_Sel, AV46ddo_Servico_NomeTitleControlIdToReplace, AV50ddo_Servico_SiglaTitleControlIdToReplace, AV54ddo_ServicoGrupo_DescricaoTitleControlIdToReplace, AV42Servico_Ativo, AV62Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV33Servico_Sigla1, AV34Servico_Nome1, AV35ServicoGrupo_Codigo1, AV20DynamicFiltersSelector2, AV36Servico_Sigla2, AV37Servico_Nome2, AV38ServicoGrupo_Codigo2, AV25DynamicFiltersSelector3, AV39Servico_Sigla3, AV40Servico_Nome3, AV41ServicoGrupo_Codigo3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV44TFServico_Nome, AV45TFServico_Nome_Sel, AV48TFServico_Sigla, AV49TFServico_Sigla_Sel, AV52TFServicoGrupo_Descricao, AV53TFServicoGrupo_Descricao_Sel, AV46ddo_Servico_NomeTitleControlIdToReplace, AV50ddo_Servico_SiglaTitleControlIdToReplace, AV54ddo_ServicoGrupo_DescricaoTitleControlIdToReplace, AV42Servico_Ativo, AV62Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV33Servico_Sigla1, AV34Servico_Nome1, AV35ServicoGrupo_Codigo1, AV20DynamicFiltersSelector2, AV36Servico_Sigla2, AV37Servico_Nome2, AV38ServicoGrupo_Codigo2, AV25DynamicFiltersSelector3, AV39Servico_Sigla3, AV40Servico_Nome3, AV41ServicoGrupo_Codigo3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV44TFServico_Nome, AV45TFServico_Nome_Sel, AV48TFServico_Sigla, AV49TFServico_Sigla_Sel, AV52TFServicoGrupo_Descricao, AV53TFServicoGrupo_Descricao_Sel, AV46ddo_Servico_NomeTitleControlIdToReplace, AV50ddo_Servico_SiglaTitleControlIdToReplace, AV54ddo_ServicoGrupo_DescricaoTitleControlIdToReplace, AV42Servico_Ativo, AV62Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         }
         return (int)(0) ;
      }

      protected void STRUP6Z0( )
      {
         /* Before Start, stand alone formulas. */
         AV62Pgmname = "PromptServico";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E256Z2 */
         E256Z2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV55DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vSERVICO_NOMETITLEFILTERDATA"), AV43Servico_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSERVICO_SIGLATITLEFILTERDATA"), AV47Servico_SiglaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSERVICOGRUPO_DESCRICAOTITLEFILTERDATA"), AV51ServicoGrupo_DescricaoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavServico_ativo.Name = cmbavServico_ativo_Internalname;
            cmbavServico_ativo.CurrentValue = cgiGet( cmbavServico_ativo_Internalname);
            AV42Servico_Ativo = StringUtil.StrToBool( cgiGet( cmbavServico_ativo_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Servico_Ativo", AV42Servico_Ativo);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            AV33Servico_Sigla1 = StringUtil.Upper( cgiGet( edtavServico_sigla1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Servico_Sigla1", AV33Servico_Sigla1);
            AV34Servico_Nome1 = StringUtil.Upper( cgiGet( edtavServico_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Servico_Nome1", AV34Servico_Nome1);
            dynavServicogrupo_codigo1.Name = dynavServicogrupo_codigo1_Internalname;
            dynavServicogrupo_codigo1.CurrentValue = cgiGet( dynavServicogrupo_codigo1_Internalname);
            AV35ServicoGrupo_Codigo1 = (int)(NumberUtil.Val( cgiGet( dynavServicogrupo_codigo1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35ServicoGrupo_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35ServicoGrupo_Codigo1), 6, 0)));
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV20DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            AV36Servico_Sigla2 = StringUtil.Upper( cgiGet( edtavServico_sigla2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Servico_Sigla2", AV36Servico_Sigla2);
            AV37Servico_Nome2 = StringUtil.Upper( cgiGet( edtavServico_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Servico_Nome2", AV37Servico_Nome2);
            dynavServicogrupo_codigo2.Name = dynavServicogrupo_codigo2_Internalname;
            dynavServicogrupo_codigo2.CurrentValue = cgiGet( dynavServicogrupo_codigo2_Internalname);
            AV38ServicoGrupo_Codigo2 = (int)(NumberUtil.Val( cgiGet( dynavServicogrupo_codigo2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ServicoGrupo_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38ServicoGrupo_Codigo2), 6, 0)));
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV25DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
            AV39Servico_Sigla3 = StringUtil.Upper( cgiGet( edtavServico_sigla3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Servico_Sigla3", AV39Servico_Sigla3);
            AV40Servico_Nome3 = StringUtil.Upper( cgiGet( edtavServico_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Servico_Nome3", AV40Servico_Nome3);
            dynavServicogrupo_codigo3.Name = dynavServicogrupo_codigo3_Internalname;
            dynavServicogrupo_codigo3.CurrentValue = cgiGet( dynavServicogrupo_codigo3_Internalname);
            AV41ServicoGrupo_Codigo3 = (int)(NumberUtil.Val( cgiGet( dynavServicogrupo_codigo3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ServicoGrupo_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41ServicoGrupo_Codigo3), 6, 0)));
            A156Servico_Descricao = cgiGet( edtServico_Descricao_Internalname);
            n156Servico_Descricao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A156Servico_Descricao", A156Servico_Descricao);
            AV19DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
            AV24DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
            AV44TFServico_Nome = StringUtil.Upper( cgiGet( edtavTfservico_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFServico_Nome", AV44TFServico_Nome);
            AV45TFServico_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfservico_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFServico_Nome_Sel", AV45TFServico_Nome_Sel);
            AV48TFServico_Sigla = StringUtil.Upper( cgiGet( edtavTfservico_sigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFServico_Sigla", AV48TFServico_Sigla);
            AV49TFServico_Sigla_Sel = StringUtil.Upper( cgiGet( edtavTfservico_sigla_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFServico_Sigla_Sel", AV49TFServico_Sigla_Sel);
            AV52TFServicoGrupo_Descricao = cgiGet( edtavTfservicogrupo_descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFServicoGrupo_Descricao", AV52TFServicoGrupo_Descricao);
            AV53TFServicoGrupo_Descricao_Sel = cgiGet( edtavTfservicogrupo_descricao_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFServicoGrupo_Descricao_Sel", AV53TFServicoGrupo_Descricao_Sel);
            AV46ddo_Servico_NomeTitleControlIdToReplace = cgiGet( edtavDdo_servico_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ddo_Servico_NomeTitleControlIdToReplace", AV46ddo_Servico_NomeTitleControlIdToReplace);
            AV50ddo_Servico_SiglaTitleControlIdToReplace = cgiGet( edtavDdo_servico_siglatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ddo_Servico_SiglaTitleControlIdToReplace", AV50ddo_Servico_SiglaTitleControlIdToReplace);
            AV54ddo_ServicoGrupo_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_servicogrupo_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ddo_ServicoGrupo_DescricaoTitleControlIdToReplace", AV54ddo_ServicoGrupo_DescricaoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_75 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_75"), ",", "."));
            AV57GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV58GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_servico_nome_Caption = cgiGet( "DDO_SERVICO_NOME_Caption");
            Ddo_servico_nome_Tooltip = cgiGet( "DDO_SERVICO_NOME_Tooltip");
            Ddo_servico_nome_Cls = cgiGet( "DDO_SERVICO_NOME_Cls");
            Ddo_servico_nome_Filteredtext_set = cgiGet( "DDO_SERVICO_NOME_Filteredtext_set");
            Ddo_servico_nome_Selectedvalue_set = cgiGet( "DDO_SERVICO_NOME_Selectedvalue_set");
            Ddo_servico_nome_Dropdownoptionstype = cgiGet( "DDO_SERVICO_NOME_Dropdownoptionstype");
            Ddo_servico_nome_Titlecontrolidtoreplace = cgiGet( "DDO_SERVICO_NOME_Titlecontrolidtoreplace");
            Ddo_servico_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_NOME_Includesortasc"));
            Ddo_servico_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_NOME_Includesortdsc"));
            Ddo_servico_nome_Sortedstatus = cgiGet( "DDO_SERVICO_NOME_Sortedstatus");
            Ddo_servico_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_NOME_Includefilter"));
            Ddo_servico_nome_Filtertype = cgiGet( "DDO_SERVICO_NOME_Filtertype");
            Ddo_servico_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_NOME_Filterisrange"));
            Ddo_servico_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_NOME_Includedatalist"));
            Ddo_servico_nome_Datalisttype = cgiGet( "DDO_SERVICO_NOME_Datalisttype");
            Ddo_servico_nome_Datalistproc = cgiGet( "DDO_SERVICO_NOME_Datalistproc");
            Ddo_servico_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_SERVICO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_servico_nome_Sortasc = cgiGet( "DDO_SERVICO_NOME_Sortasc");
            Ddo_servico_nome_Sortdsc = cgiGet( "DDO_SERVICO_NOME_Sortdsc");
            Ddo_servico_nome_Loadingdata = cgiGet( "DDO_SERVICO_NOME_Loadingdata");
            Ddo_servico_nome_Cleanfilter = cgiGet( "DDO_SERVICO_NOME_Cleanfilter");
            Ddo_servico_nome_Noresultsfound = cgiGet( "DDO_SERVICO_NOME_Noresultsfound");
            Ddo_servico_nome_Searchbuttontext = cgiGet( "DDO_SERVICO_NOME_Searchbuttontext");
            Ddo_servico_sigla_Caption = cgiGet( "DDO_SERVICO_SIGLA_Caption");
            Ddo_servico_sigla_Tooltip = cgiGet( "DDO_SERVICO_SIGLA_Tooltip");
            Ddo_servico_sigla_Cls = cgiGet( "DDO_SERVICO_SIGLA_Cls");
            Ddo_servico_sigla_Filteredtext_set = cgiGet( "DDO_SERVICO_SIGLA_Filteredtext_set");
            Ddo_servico_sigla_Selectedvalue_set = cgiGet( "DDO_SERVICO_SIGLA_Selectedvalue_set");
            Ddo_servico_sigla_Dropdownoptionstype = cgiGet( "DDO_SERVICO_SIGLA_Dropdownoptionstype");
            Ddo_servico_sigla_Titlecontrolidtoreplace = cgiGet( "DDO_SERVICO_SIGLA_Titlecontrolidtoreplace");
            Ddo_servico_sigla_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_SIGLA_Includesortasc"));
            Ddo_servico_sigla_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_SIGLA_Includesortdsc"));
            Ddo_servico_sigla_Sortedstatus = cgiGet( "DDO_SERVICO_SIGLA_Sortedstatus");
            Ddo_servico_sigla_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_SIGLA_Includefilter"));
            Ddo_servico_sigla_Filtertype = cgiGet( "DDO_SERVICO_SIGLA_Filtertype");
            Ddo_servico_sigla_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_SIGLA_Filterisrange"));
            Ddo_servico_sigla_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_SIGLA_Includedatalist"));
            Ddo_servico_sigla_Datalisttype = cgiGet( "DDO_SERVICO_SIGLA_Datalisttype");
            Ddo_servico_sigla_Datalistproc = cgiGet( "DDO_SERVICO_SIGLA_Datalistproc");
            Ddo_servico_sigla_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_SERVICO_SIGLA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_servico_sigla_Sortasc = cgiGet( "DDO_SERVICO_SIGLA_Sortasc");
            Ddo_servico_sigla_Sortdsc = cgiGet( "DDO_SERVICO_SIGLA_Sortdsc");
            Ddo_servico_sigla_Loadingdata = cgiGet( "DDO_SERVICO_SIGLA_Loadingdata");
            Ddo_servico_sigla_Cleanfilter = cgiGet( "DDO_SERVICO_SIGLA_Cleanfilter");
            Ddo_servico_sigla_Noresultsfound = cgiGet( "DDO_SERVICO_SIGLA_Noresultsfound");
            Ddo_servico_sigla_Searchbuttontext = cgiGet( "DDO_SERVICO_SIGLA_Searchbuttontext");
            Ddo_servicogrupo_descricao_Caption = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Caption");
            Ddo_servicogrupo_descricao_Tooltip = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Tooltip");
            Ddo_servicogrupo_descricao_Cls = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Cls");
            Ddo_servicogrupo_descricao_Filteredtext_set = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Filteredtext_set");
            Ddo_servicogrupo_descricao_Selectedvalue_set = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Selectedvalue_set");
            Ddo_servicogrupo_descricao_Dropdownoptionstype = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Dropdownoptionstype");
            Ddo_servicogrupo_descricao_Titlecontrolidtoreplace = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_servicogrupo_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Includesortasc"));
            Ddo_servicogrupo_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Includesortdsc"));
            Ddo_servicogrupo_descricao_Sortedstatus = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Sortedstatus");
            Ddo_servicogrupo_descricao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Includefilter"));
            Ddo_servicogrupo_descricao_Filtertype = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Filtertype");
            Ddo_servicogrupo_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Filterisrange"));
            Ddo_servicogrupo_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Includedatalist"));
            Ddo_servicogrupo_descricao_Datalisttype = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Datalisttype");
            Ddo_servicogrupo_descricao_Datalistproc = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Datalistproc");
            Ddo_servicogrupo_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_servicogrupo_descricao_Sortasc = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Sortasc");
            Ddo_servicogrupo_descricao_Sortdsc = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Sortdsc");
            Ddo_servicogrupo_descricao_Loadingdata = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Loadingdata");
            Ddo_servicogrupo_descricao_Cleanfilter = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Cleanfilter");
            Ddo_servicogrupo_descricao_Noresultsfound = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Noresultsfound");
            Ddo_servicogrupo_descricao_Searchbuttontext = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_servico_nome_Activeeventkey = cgiGet( "DDO_SERVICO_NOME_Activeeventkey");
            Ddo_servico_nome_Filteredtext_get = cgiGet( "DDO_SERVICO_NOME_Filteredtext_get");
            Ddo_servico_nome_Selectedvalue_get = cgiGet( "DDO_SERVICO_NOME_Selectedvalue_get");
            Ddo_servico_sigla_Activeeventkey = cgiGet( "DDO_SERVICO_SIGLA_Activeeventkey");
            Ddo_servico_sigla_Filteredtext_get = cgiGet( "DDO_SERVICO_SIGLA_Filteredtext_get");
            Ddo_servico_sigla_Selectedvalue_get = cgiGet( "DDO_SERVICO_SIGLA_Selectedvalue_get");
            Ddo_servicogrupo_descricao_Activeeventkey = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Activeeventkey");
            Ddo_servicogrupo_descricao_Filteredtext_get = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Filteredtext_get");
            Ddo_servicogrupo_descricao_Selectedvalue_get = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = "hsh" + "PromptServico";
            A156Servico_Descricao = cgiGet( edtServico_Descricao_Internalname);
            n156Servico_Descricao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A156Servico_Descricao", A156Servico_Descricao);
            forbiddenHiddens = forbiddenHiddens + A156Servico_Descricao;
            hsh = cgiGet( "hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("promptservico:[SecurityCheckFailed value for]"+"Servico_Descricao:"+A156Servico_Descricao);
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICO_SIGLA1"), AV33Servico_Sigla1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICO_NOME1"), AV34Servico_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vSERVICOGRUPO_CODIGO1"), ",", ".") != Convert.ToDecimal( AV35ServicoGrupo_Codigo1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICO_SIGLA2"), AV36Servico_Sigla2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICO_NOME2"), AV37Servico_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vSERVICOGRUPO_CODIGO2"), ",", ".") != Convert.ToDecimal( AV38ServicoGrupo_Codigo2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV25DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICO_SIGLA3"), AV39Servico_Sigla3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICO_NOME3"), AV40Servico_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vSERVICOGRUPO_CODIGO3"), ",", ".") != Convert.ToDecimal( AV41ServicoGrupo_Codigo3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV24DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICO_NOME"), AV44TFServico_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICO_NOME_SEL"), AV45TFServico_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICO_SIGLA"), AV48TFServico_Sigla) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICO_SIGLA_SEL"), AV49TFServico_Sigla_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICOGRUPO_DESCRICAO"), AV52TFServicoGrupo_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICOGRUPO_DESCRICAO_SEL"), AV53TFServicoGrupo_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E256Z2 */
         E256Z2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E256Z2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "SERVICO_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV20DynamicFiltersSelector2 = "SERVICO_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV25DynamicFiltersSelector3 = "SERVICO_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfservico_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservico_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservico_nome_Visible), 5, 0)));
         edtavTfservico_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservico_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservico_nome_sel_Visible), 5, 0)));
         edtavTfservico_sigla_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservico_sigla_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservico_sigla_Visible), 5, 0)));
         edtavTfservico_sigla_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservico_sigla_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservico_sigla_sel_Visible), 5, 0)));
         edtavTfservicogrupo_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservicogrupo_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicogrupo_descricao_Visible), 5, 0)));
         edtavTfservicogrupo_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservicogrupo_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicogrupo_descricao_sel_Visible), 5, 0)));
         Ddo_servico_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Servico_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_nome_Internalname, "TitleControlIdToReplace", Ddo_servico_nome_Titlecontrolidtoreplace);
         AV46ddo_Servico_NomeTitleControlIdToReplace = Ddo_servico_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ddo_Servico_NomeTitleControlIdToReplace", AV46ddo_Servico_NomeTitleControlIdToReplace);
         edtavDdo_servico_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_servico_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_servico_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_servico_sigla_Titlecontrolidtoreplace = subGrid_Internalname+"_Servico_Sigla";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_sigla_Internalname, "TitleControlIdToReplace", Ddo_servico_sigla_Titlecontrolidtoreplace);
         AV50ddo_Servico_SiglaTitleControlIdToReplace = Ddo_servico_sigla_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ddo_Servico_SiglaTitleControlIdToReplace", AV50ddo_Servico_SiglaTitleControlIdToReplace);
         edtavDdo_servico_siglatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_servico_siglatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_servico_siglatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_servicogrupo_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_ServicoGrupo_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicogrupo_descricao_Internalname, "TitleControlIdToReplace", Ddo_servicogrupo_descricao_Titlecontrolidtoreplace);
         AV54ddo_ServicoGrupo_DescricaoTitleControlIdToReplace = Ddo_servicogrupo_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ddo_ServicoGrupo_DescricaoTitleControlIdToReplace", AV54ddo_ServicoGrupo_DescricaoTitleControlIdToReplace);
         edtavDdo_servicogrupo_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_servicogrupo_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_servicogrupo_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione Servico";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         edtServico_Descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_Descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_Descricao_Visible), 5, 0)));
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Descri��o de Servi�o", 0);
         cmbavOrderedby.addItem("2", "Nome", 0);
         cmbavOrderedby.addItem("3", "Grupo", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV55DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV55DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E266Z2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV43Servico_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV47Servico_SiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV51ServicoGrupo_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtServico_Nome_Titleformat = 2;
         edtServico_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV46ddo_Servico_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_Nome_Internalname, "Title", edtServico_Nome_Title);
         edtServico_Sigla_Titleformat = 2;
         edtServico_Sigla_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Sigla", AV50ddo_Servico_SiglaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_Sigla_Internalname, "Title", edtServico_Sigla_Title);
         edtServicoGrupo_Descricao_Titleformat = 2;
         edtServicoGrupo_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Grupo", AV54ddo_ServicoGrupo_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServicoGrupo_Descricao_Internalname, "Title", edtServicoGrupo_Descricao_Title);
         AV57GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57GridCurrentPage), 10, 0)));
         AV58GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV43Servico_NomeTitleFilterData", AV43Servico_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV47Servico_SiglaTitleFilterData", AV47Servico_SiglaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV51ServicoGrupo_DescricaoTitleFilterData", AV51ServicoGrupo_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E116Z2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV56PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV56PageToGo) ;
         }
      }

      protected void E126Z2( )
      {
         /* Ddo_servico_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_servico_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servico_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_nome_Internalname, "SortedStatus", Ddo_servico_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servico_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servico_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_nome_Internalname, "SortedStatus", Ddo_servico_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servico_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV44TFServico_Nome = Ddo_servico_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFServico_Nome", AV44TFServico_Nome);
            AV45TFServico_Nome_Sel = Ddo_servico_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFServico_Nome_Sel", AV45TFServico_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E136Z2( )
      {
         /* Ddo_servico_sigla_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_servico_sigla_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servico_sigla_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_sigla_Internalname, "SortedStatus", Ddo_servico_sigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servico_sigla_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servico_sigla_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_sigla_Internalname, "SortedStatus", Ddo_servico_sigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servico_sigla_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV48TFServico_Sigla = Ddo_servico_sigla_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFServico_Sigla", AV48TFServico_Sigla);
            AV49TFServico_Sigla_Sel = Ddo_servico_sigla_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFServico_Sigla_Sel", AV49TFServico_Sigla_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E146Z2( )
      {
         /* Ddo_servicogrupo_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_servicogrupo_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servicogrupo_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicogrupo_descricao_Internalname, "SortedStatus", Ddo_servicogrupo_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servicogrupo_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servicogrupo_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicogrupo_descricao_Internalname, "SortedStatus", Ddo_servicogrupo_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servicogrupo_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV52TFServicoGrupo_Descricao = Ddo_servicogrupo_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFServicoGrupo_Descricao", AV52TFServicoGrupo_Descricao);
            AV53TFServicoGrupo_Descricao_Sel = Ddo_servicogrupo_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFServicoGrupo_Descricao_Sel", AV53TFServicoGrupo_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E276Z2( )
      {
         /* Grid_Load Routine */
         AV31Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV31Select);
         AV61Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 75;
         }
         sendrow_752( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_75_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(75, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E286Z2 */
         E286Z2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E286Z2( )
      {
         /* Enter Routine */
         AV7InOutServico_Codigo = A155Servico_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutServico_Codigo), 6, 0)));
         AV8InOutServico_Descricao = A156Servico_Descricao;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutServico_Descricao", AV8InOutServico_Descricao);
         context.setWebReturnParms(new Object[] {(int)AV7InOutServico_Codigo,(String)AV8InOutServico_Descricao});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E156Z2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E206Z2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV19DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E166Z2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV30DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV30DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV33Servico_Sigla1, AV34Servico_Nome1, AV35ServicoGrupo_Codigo1, AV20DynamicFiltersSelector2, AV36Servico_Sigla2, AV37Servico_Nome2, AV38ServicoGrupo_Codigo2, AV25DynamicFiltersSelector3, AV39Servico_Sigla3, AV40Servico_Nome3, AV41ServicoGrupo_Codigo3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV44TFServico_Nome, AV45TFServico_Nome_Sel, AV48TFServico_Sigla, AV49TFServico_Sigla_Sel, AV52TFServicoGrupo_Descricao, AV53TFServicoGrupo_Descricao_Sel, AV46ddo_Servico_NomeTitleControlIdToReplace, AV50ddo_Servico_SiglaTitleControlIdToReplace, AV54ddo_ServicoGrupo_DescricaoTitleControlIdToReplace, AV42Servico_Ativo, AV62Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         dynavServicogrupo_codigo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV35ServicoGrupo_Codigo1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServicogrupo_codigo1_Internalname, "Values", dynavServicogrupo_codigo1.ToJavascriptSource());
         dynavServicogrupo_codigo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV38ServicoGrupo_Codigo2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServicogrupo_codigo2_Internalname, "Values", dynavServicogrupo_codigo2.ToJavascriptSource());
         dynavServicogrupo_codigo3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV41ServicoGrupo_Codigo3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServicogrupo_codigo3_Internalname, "Values", dynavServicogrupo_codigo3.ToJavascriptSource());
      }

      protected void E216Z2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E226Z2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV24DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E176Z2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV33Servico_Sigla1, AV34Servico_Nome1, AV35ServicoGrupo_Codigo1, AV20DynamicFiltersSelector2, AV36Servico_Sigla2, AV37Servico_Nome2, AV38ServicoGrupo_Codigo2, AV25DynamicFiltersSelector3, AV39Servico_Sigla3, AV40Servico_Nome3, AV41ServicoGrupo_Codigo3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV44TFServico_Nome, AV45TFServico_Nome_Sel, AV48TFServico_Sigla, AV49TFServico_Sigla_Sel, AV52TFServicoGrupo_Descricao, AV53TFServicoGrupo_Descricao_Sel, AV46ddo_Servico_NomeTitleControlIdToReplace, AV50ddo_Servico_SiglaTitleControlIdToReplace, AV54ddo_ServicoGrupo_DescricaoTitleControlIdToReplace, AV42Servico_Ativo, AV62Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         dynavServicogrupo_codigo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV35ServicoGrupo_Codigo1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServicogrupo_codigo1_Internalname, "Values", dynavServicogrupo_codigo1.ToJavascriptSource());
         dynavServicogrupo_codigo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV38ServicoGrupo_Codigo2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServicogrupo_codigo2_Internalname, "Values", dynavServicogrupo_codigo2.ToJavascriptSource());
         dynavServicogrupo_codigo3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV41ServicoGrupo_Codigo3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServicogrupo_codigo3_Internalname, "Values", dynavServicogrupo_codigo3.ToJavascriptSource());
      }

      protected void E236Z2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E186Z2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV24DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV33Servico_Sigla1, AV34Servico_Nome1, AV35ServicoGrupo_Codigo1, AV20DynamicFiltersSelector2, AV36Servico_Sigla2, AV37Servico_Nome2, AV38ServicoGrupo_Codigo2, AV25DynamicFiltersSelector3, AV39Servico_Sigla3, AV40Servico_Nome3, AV41ServicoGrupo_Codigo3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV44TFServico_Nome, AV45TFServico_Nome_Sel, AV48TFServico_Sigla, AV49TFServico_Sigla_Sel, AV52TFServicoGrupo_Descricao, AV53TFServicoGrupo_Descricao_Sel, AV46ddo_Servico_NomeTitleControlIdToReplace, AV50ddo_Servico_SiglaTitleControlIdToReplace, AV54ddo_ServicoGrupo_DescricaoTitleControlIdToReplace, AV42Servico_Ativo, AV62Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         dynavServicogrupo_codigo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV35ServicoGrupo_Codigo1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServicogrupo_codigo1_Internalname, "Values", dynavServicogrupo_codigo1.ToJavascriptSource());
         dynavServicogrupo_codigo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV38ServicoGrupo_Codigo2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServicogrupo_codigo2_Internalname, "Values", dynavServicogrupo_codigo2.ToJavascriptSource());
         dynavServicogrupo_codigo3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV41ServicoGrupo_Codigo3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServicogrupo_codigo3_Internalname, "Values", dynavServicogrupo_codigo3.ToJavascriptSource());
      }

      protected void E246Z2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E196Z2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavServico_ativo.CurrentValue = StringUtil.BoolToStr( AV42Servico_Ativo);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServico_ativo_Internalname, "Values", cmbavServico_ativo.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         dynavServicogrupo_codigo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV35ServicoGrupo_Codigo1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServicogrupo_codigo1_Internalname, "Values", dynavServicogrupo_codigo1.ToJavascriptSource());
         dynavServicogrupo_codigo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV38ServicoGrupo_Codigo2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServicogrupo_codigo2_Internalname, "Values", dynavServicogrupo_codigo2.ToJavascriptSource());
         dynavServicogrupo_codigo3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV41ServicoGrupo_Codigo3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServicogrupo_codigo3_Internalname, "Values", dynavServicogrupo_codigo3.ToJavascriptSource());
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_servico_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_nome_Internalname, "SortedStatus", Ddo_servico_nome_Sortedstatus);
         Ddo_servico_sigla_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_sigla_Internalname, "SortedStatus", Ddo_servico_sigla_Sortedstatus);
         Ddo_servicogrupo_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicogrupo_descricao_Internalname, "SortedStatus", Ddo_servicogrupo_descricao_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_servico_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_nome_Internalname, "SortedStatus", Ddo_servico_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_servico_sigla_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_sigla_Internalname, "SortedStatus", Ddo_servico_sigla_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_servicogrupo_descricao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicogrupo_descricao_Internalname, "SortedStatus", Ddo_servicogrupo_descricao_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavServico_sigla1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_sigla1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_sigla1_Visible), 5, 0)));
         edtavServico_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_nome1_Visible), 5, 0)));
         dynavServicogrupo_codigo1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServicogrupo_codigo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavServicogrupo_codigo1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICO_SIGLA") == 0 )
         {
            edtavServico_sigla1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_sigla1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_sigla1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICO_NOME") == 0 )
         {
            edtavServico_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_nome1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICOGRUPO_CODIGO") == 0 )
         {
            dynavServicogrupo_codigo1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServicogrupo_codigo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavServicogrupo_codigo1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavServico_sigla2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_sigla2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_sigla2_Visible), 5, 0)));
         edtavServico_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_nome2_Visible), 5, 0)));
         dynavServicogrupo_codigo2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServicogrupo_codigo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavServicogrupo_codigo2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SERVICO_SIGLA") == 0 )
         {
            edtavServico_sigla2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_sigla2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_sigla2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SERVICO_NOME") == 0 )
         {
            edtavServico_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_nome2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SERVICOGRUPO_CODIGO") == 0 )
         {
            dynavServicogrupo_codigo2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServicogrupo_codigo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavServicogrupo_codigo2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavServico_sigla3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_sigla3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_sigla3_Visible), 5, 0)));
         edtavServico_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_nome3_Visible), 5, 0)));
         dynavServicogrupo_codigo3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServicogrupo_codigo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavServicogrupo_codigo3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "SERVICO_SIGLA") == 0 )
         {
            edtavServico_sigla3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_sigla3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_sigla3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "SERVICO_NOME") == 0 )
         {
            edtavServico_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_nome3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "SERVICOGRUPO_CODIGO") == 0 )
         {
            dynavServicogrupo_codigo3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServicogrupo_codigo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavServicogrupo_codigo3.Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         AV20DynamicFiltersSelector2 = "SERVICO_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         AV36Servico_Sigla2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Servico_Sigla2", AV36Servico_Sigla2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV24DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         AV25DynamicFiltersSelector3 = "SERVICO_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         AV39Servico_Sigla3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Servico_Sigla3", AV39Servico_Sigla3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV42Servico_Ativo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Servico_Ativo", AV42Servico_Ativo);
         AV44TFServico_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFServico_Nome", AV44TFServico_Nome);
         Ddo_servico_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_nome_Internalname, "FilteredText_set", Ddo_servico_nome_Filteredtext_set);
         AV45TFServico_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFServico_Nome_Sel", AV45TFServico_Nome_Sel);
         Ddo_servico_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_nome_Internalname, "SelectedValue_set", Ddo_servico_nome_Selectedvalue_set);
         AV48TFServico_Sigla = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFServico_Sigla", AV48TFServico_Sigla);
         Ddo_servico_sigla_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_sigla_Internalname, "FilteredText_set", Ddo_servico_sigla_Filteredtext_set);
         AV49TFServico_Sigla_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFServico_Sigla_Sel", AV49TFServico_Sigla_Sel);
         Ddo_servico_sigla_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_sigla_Internalname, "SelectedValue_set", Ddo_servico_sigla_Selectedvalue_set);
         AV52TFServicoGrupo_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFServicoGrupo_Descricao", AV52TFServicoGrupo_Descricao);
         Ddo_servicogrupo_descricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicogrupo_descricao_Internalname, "FilteredText_set", Ddo_servicogrupo_descricao_Filteredtext_set);
         AV53TFServicoGrupo_Descricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFServicoGrupo_Descricao_Sel", AV53TFServicoGrupo_Descricao_Sel);
         Ddo_servicogrupo_descricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicogrupo_descricao_Internalname, "SelectedValue_set", Ddo_servicogrupo_descricao_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "SERVICO_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV33Servico_Sigla1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Servico_Sigla1", AV33Servico_Sigla1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICO_SIGLA") == 0 )
            {
               AV33Servico_Sigla1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Servico_Sigla1", AV33Servico_Sigla1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICO_NOME") == 0 )
            {
               AV34Servico_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Servico_Nome1", AV34Servico_Nome1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICOGRUPO_CODIGO") == 0 )
            {
               AV35ServicoGrupo_Codigo1 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35ServicoGrupo_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35ServicoGrupo_Codigo1), 6, 0)));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV19DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV20DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SERVICO_SIGLA") == 0 )
               {
                  AV36Servico_Sigla2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Servico_Sigla2", AV36Servico_Sigla2);
               }
               else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SERVICO_NOME") == 0 )
               {
                  AV37Servico_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Servico_Nome2", AV37Servico_Nome2);
               }
               else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SERVICOGRUPO_CODIGO") == 0 )
               {
                  AV38ServicoGrupo_Codigo2 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ServicoGrupo_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38ServicoGrupo_Codigo2), 6, 0)));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV24DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV25DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "SERVICO_SIGLA") == 0 )
                  {
                     AV39Servico_Sigla3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Servico_Sigla3", AV39Servico_Sigla3);
                  }
                  else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "SERVICO_NOME") == 0 )
                  {
                     AV40Servico_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Servico_Nome3", AV40Servico_Nome3);
                  }
                  else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "SERVICOGRUPO_CODIGO") == 0 )
                  {
                     AV41ServicoGrupo_Codigo3 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ServicoGrupo_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41ServicoGrupo_Codigo3), 6, 0)));
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV29DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! (false==AV42Servico_Ativo) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "SERVICO_ATIVO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.BoolToStr( AV42Servico_Ativo);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFServico_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSERVICO_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV44TFServico_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TFServico_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSERVICO_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV45TFServico_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFServico_Sigla)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSERVICO_SIGLA";
            AV11GridStateFilterValue.gxTpr_Value = AV48TFServico_Sigla;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFServico_Sigla_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSERVICO_SIGLA_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV49TFServico_Sigla_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFServicoGrupo_Descricao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSERVICOGRUPO_DESCRICAO";
            AV11GridStateFilterValue.gxTpr_Value = AV52TFServicoGrupo_Descricao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53TFServicoGrupo_Descricao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSERVICOGRUPO_DESCRICAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV53TFServicoGrupo_Descricao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV62Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV30DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICO_SIGLA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV33Servico_Sigla1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV33Servico_Sigla1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV34Servico_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV34Servico_Nome1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICOGRUPO_CODIGO") == 0 ) && ! (0==AV35ServicoGrupo_Codigo1) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV35ServicoGrupo_Codigo1), 6, 0);
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV20DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SERVICO_SIGLA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV36Servico_Sigla2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV36Servico_Sigla2;
            }
            else if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SERVICO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV37Servico_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV37Servico_Nome2;
            }
            else if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SERVICOGRUPO_CODIGO") == 0 ) && ! (0==AV38ServicoGrupo_Codigo2) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV38ServicoGrupo_Codigo2), 6, 0);
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV24DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV25DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "SERVICO_SIGLA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV39Servico_Sigla3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV39Servico_Sigla3;
            }
            else if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "SERVICO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV40Servico_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV40Servico_Nome3;
            }
            else if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "SERVICOGRUPO_CODIGO") == 0 ) && ! (0==AV41ServicoGrupo_Codigo3) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV41ServicoGrupo_Codigo3), 6, 0);
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_6Z2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_6Z2( true) ;
         }
         else
         {
            wb_table2_5_6Z2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_6Z2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_69_6Z2( true) ;
         }
         else
         {
            wb_table3_69_6Z2( false) ;
         }
         return  ;
      }

      protected void wb_table3_69_6Z2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_6Z2e( true) ;
         }
         else
         {
            wb_table1_2_6Z2e( false) ;
         }
      }

      protected void wb_table3_69_6Z2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_72_6Z2( true) ;
         }
         else
         {
            wb_table4_72_6Z2( false) ;
         }
         return  ;
      }

      protected void wb_table4_72_6Z2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_69_6Z2e( true) ;
         }
         else
         {
            wb_table3_69_6Z2e( false) ;
         }
      }

      protected void wb_table4_72_6Z2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"75\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo de Servi�o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(380), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServico_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtServico_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServico_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServico_Sigla_Titleformat == 0 )
               {
                  context.SendWebValue( edtServico_Sigla_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServico_Sigla_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServicoGrupo_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtServicoGrupo_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServicoGrupo_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV31Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A608Servico_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServico_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServico_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A605Servico_Sigla));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServico_Sigla_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServico_Sigla_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A158ServicoGrupo_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServicoGrupo_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServicoGrupo_Descricao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 75 )
         {
            wbEnd = 0;
            nRC_GXsfl_75 = (short)(nGXsfl_75_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_72_6Z2e( true) ;
         }
         else
         {
            wb_table4_72_6Z2e( false) ;
         }
      }

      protected void wb_table2_5_6Z2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_75_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptServico.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_75_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_6Z2( true) ;
         }
         else
         {
            wb_table5_14_6Z2( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_6Z2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_6Z2e( true) ;
         }
         else
         {
            wb_table2_5_6Z2e( false) ;
         }
      }

      protected void wb_table5_14_6Z2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextservico_ativo_Internalname, "Ativo?", "", "", lblFiltertextservico_ativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_75_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavServico_ativo, cmbavServico_ativo_Internalname, StringUtil.BoolToStr( AV42Servico_Ativo), 1, cmbavServico_ativo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "", true, "HLP_PromptServico.htm");
            cmbavServico_ativo.CurrentValue = StringUtil.BoolToStr( AV42Servico_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServico_ativo_Internalname, "Values", (String)(cmbavServico_ativo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_23_6Z2( true) ;
         }
         else
         {
            wb_table6_23_6Z2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_6Z2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_6Z2e( true) ;
         }
         else
         {
            wb_table5_14_6Z2e( false) ;
         }
      }

      protected void wb_table6_23_6Z2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'" + sGXsfl_75_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "", true, "HLP_PromptServico.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'" + sGXsfl_75_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServico_sigla1_Internalname, StringUtil.RTrim( AV33Servico_Sigla1), StringUtil.RTrim( context.localUtil.Format( AV33Servico_Sigla1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,32);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServico_sigla1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavServico_sigla1_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptServico.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_75_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServico_nome1_Internalname, StringUtil.RTrim( AV34Servico_Nome1), StringUtil.RTrim( context.localUtil.Format( AV34Servico_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServico_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavServico_nome1_Visible, 1, 0, "text", "", 380, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptServico.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'" + sGXsfl_75_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavServicogrupo_codigo1, dynavServicogrupo_codigo1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV35ServicoGrupo_Codigo1), 6, 0)), 1, dynavServicogrupo_codigo1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavServicogrupo_codigo1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,34);\"", "", true, "HLP_PromptServico.htm");
            dynavServicogrupo_codigo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV35ServicoGrupo_Codigo1), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServicogrupo_codigo1_Internalname, "Values", (String)(dynavServicogrupo_codigo1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptServico.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_75_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV20DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", "", true, "HLP_PromptServico.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_75_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServico_sigla2_Internalname, StringUtil.RTrim( AV36Servico_Sigla2), StringUtil.RTrim( context.localUtil.Format( AV36Servico_Sigla2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,46);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServico_sigla2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavServico_sigla2_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptServico.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'" + sGXsfl_75_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServico_nome2_Internalname, StringUtil.RTrim( AV37Servico_Nome2), StringUtil.RTrim( context.localUtil.Format( AV37Servico_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,47);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServico_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavServico_nome2_Visible, 1, 0, "text", "", 380, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptServico.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'" + sGXsfl_75_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavServicogrupo_codigo2, dynavServicogrupo_codigo2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV38ServicoGrupo_Codigo2), 6, 0)), 1, dynavServicogrupo_codigo2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavServicogrupo_codigo2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,48);\"", "", true, "HLP_PromptServico.htm");
            dynavServicogrupo_codigo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV38ServicoGrupo_Codigo2), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServicogrupo_codigo2_Internalname, "Values", (String)(dynavServicogrupo_codigo2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptServico.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'" + sGXsfl_75_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV25DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", "", true, "HLP_PromptServico.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'" + sGXsfl_75_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServico_sigla3_Internalname, StringUtil.RTrim( AV39Servico_Sigla3), StringUtil.RTrim( context.localUtil.Format( AV39Servico_Sigla3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,60);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServico_sigla3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavServico_sigla3_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptServico.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'" + sGXsfl_75_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServico_nome3_Internalname, StringUtil.RTrim( AV40Servico_Nome3), StringUtil.RTrim( context.localUtil.Format( AV40Servico_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,61);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServico_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavServico_nome3_Visible, 1, 0, "text", "", 380, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptServico.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'" + sGXsfl_75_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavServicogrupo_codigo3, dynavServicogrupo_codigo3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV41ServicoGrupo_Codigo3), 6, 0)), 1, dynavServicogrupo_codigo3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavServicogrupo_codigo3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,62);\"", "", true, "HLP_PromptServico.htm");
            dynavServicogrupo_codigo3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV41ServicoGrupo_Codigo3), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServicogrupo_codigo3_Internalname, "Values", (String)(dynavServicogrupo_codigo3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_6Z2e( true) ;
         }
         else
         {
            wb_table6_23_6Z2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutServico_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutServico_Codigo), 6, 0)));
         AV8InOutServico_Descricao = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutServico_Descricao", AV8InOutServico_Descricao);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA6Z2( ) ;
         WS6Z2( ) ;
         WE6Z2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117361136");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptservico.js", "?20203117361136");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_752( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_75_idx;
         edtServico_Codigo_Internalname = "SERVICO_CODIGO_"+sGXsfl_75_idx;
         edtServico_Nome_Internalname = "SERVICO_NOME_"+sGXsfl_75_idx;
         edtServico_Sigla_Internalname = "SERVICO_SIGLA_"+sGXsfl_75_idx;
         edtServicoGrupo_Descricao_Internalname = "SERVICOGRUPO_DESCRICAO_"+sGXsfl_75_idx;
      }

      protected void SubsflControlProps_fel_752( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_75_fel_idx;
         edtServico_Codigo_Internalname = "SERVICO_CODIGO_"+sGXsfl_75_fel_idx;
         edtServico_Nome_Internalname = "SERVICO_NOME_"+sGXsfl_75_fel_idx;
         edtServico_Sigla_Internalname = "SERVICO_SIGLA_"+sGXsfl_75_fel_idx;
         edtServicoGrupo_Descricao_Internalname = "SERVICOGRUPO_DESCRICAO_"+sGXsfl_75_fel_idx;
      }

      protected void sendrow_752( )
      {
         SubsflControlProps_752( ) ;
         WB6Z0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_75_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_75_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_75_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 76,'',false,'',75)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV31Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV31Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV61Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV31Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV31Select)) ? AV61Select_GXI : context.PathToRelativeUrl( AV31Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_75_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV31Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServico_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A155Servico_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServico_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)75,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServico_Nome_Internalname,StringUtil.RTrim( A608Servico_Nome),StringUtil.RTrim( context.localUtil.Format( A608Servico_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServico_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)380,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)75,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServico_Sigla_Internalname,StringUtil.RTrim( A605Servico_Sigla),StringUtil.RTrim( context.localUtil.Format( A605Servico_Sigla, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServico_Sigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)75,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServicoGrupo_Descricao_Internalname,(String)A158ServicoGrupo_Descricao,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServicoGrupo_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)75,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, "gxhash_SERVICO_CODIGO"+"_"+sGXsfl_75_idx, GetSecureSignedToken( sGXsfl_75_idx, context.localUtil.Format( (decimal)(A155Servico_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_SERVICO_NOME"+"_"+sGXsfl_75_idx, GetSecureSignedToken( sGXsfl_75_idx, StringUtil.RTrim( context.localUtil.Format( A608Servico_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_SERVICO_SIGLA"+"_"+sGXsfl_75_idx, GetSecureSignedToken( sGXsfl_75_idx, StringUtil.RTrim( context.localUtil.Format( A605Servico_Sigla, "@!"))));
            GridContainer.AddRow(GridRow);
            nGXsfl_75_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_75_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_75_idx+1));
            sGXsfl_75_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_75_idx), 4, 0)), 4, "0");
            SubsflControlProps_752( ) ;
         }
         /* End function sendrow_752 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblFiltertextservico_ativo_Internalname = "FILTERTEXTSERVICO_ATIVO";
         cmbavServico_ativo_Internalname = "vSERVICO_ATIVO";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavServico_sigla1_Internalname = "vSERVICO_SIGLA1";
         edtavServico_nome1_Internalname = "vSERVICO_NOME1";
         dynavServicogrupo_codigo1_Internalname = "vSERVICOGRUPO_CODIGO1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavServico_sigla2_Internalname = "vSERVICO_SIGLA2";
         edtavServico_nome2_Internalname = "vSERVICO_NOME2";
         dynavServicogrupo_codigo2_Internalname = "vSERVICOGRUPO_CODIGO2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavServico_sigla3_Internalname = "vSERVICO_SIGLA3";
         edtavServico_nome3_Internalname = "vSERVICO_NOME3";
         dynavServicogrupo_codigo3_Internalname = "vSERVICOGRUPO_CODIGO3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtServico_Codigo_Internalname = "SERVICO_CODIGO";
         edtServico_Nome_Internalname = "SERVICO_NOME";
         edtServico_Sigla_Internalname = "SERVICO_SIGLA";
         edtServicoGrupo_Descricao_Internalname = "SERVICOGRUPO_DESCRICAO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         edtServico_Descricao_Internalname = "SERVICO_DESCRICAO";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfservico_nome_Internalname = "vTFSERVICO_NOME";
         edtavTfservico_nome_sel_Internalname = "vTFSERVICO_NOME_SEL";
         edtavTfservico_sigla_Internalname = "vTFSERVICO_SIGLA";
         edtavTfservico_sigla_sel_Internalname = "vTFSERVICO_SIGLA_SEL";
         edtavTfservicogrupo_descricao_Internalname = "vTFSERVICOGRUPO_DESCRICAO";
         edtavTfservicogrupo_descricao_sel_Internalname = "vTFSERVICOGRUPO_DESCRICAO_SEL";
         Ddo_servico_nome_Internalname = "DDO_SERVICO_NOME";
         edtavDdo_servico_nometitlecontrolidtoreplace_Internalname = "vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE";
         Ddo_servico_sigla_Internalname = "DDO_SERVICO_SIGLA";
         edtavDdo_servico_siglatitlecontrolidtoreplace_Internalname = "vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE";
         Ddo_servicogrupo_descricao_Internalname = "DDO_SERVICOGRUPO_DESCRICAO";
         edtavDdo_servicogrupo_descricaotitlecontrolidtoreplace_Internalname = "vDDO_SERVICOGRUPO_DESCRICAOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtServicoGrupo_Descricao_Jsonclick = "";
         edtServico_Sigla_Jsonclick = "";
         edtServico_Nome_Jsonclick = "";
         edtServico_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         dynavServicogrupo_codigo3_Jsonclick = "";
         edtavServico_nome3_Jsonclick = "";
         edtavServico_sigla3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         dynavServicogrupo_codigo2_Jsonclick = "";
         edtavServico_nome2_Jsonclick = "";
         edtavServico_sigla2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         dynavServicogrupo_codigo1_Jsonclick = "";
         edtavServico_nome1_Jsonclick = "";
         edtavServico_sigla1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         cmbavServico_ativo_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         edtServicoGrupo_Descricao_Titleformat = 0;
         edtServico_Sigla_Titleformat = 0;
         edtServico_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         dynavServicogrupo_codigo3.Visible = 1;
         edtavServico_nome3_Visible = 1;
         edtavServico_sigla3_Visible = 1;
         dynavServicogrupo_codigo2.Visible = 1;
         edtavServico_nome2_Visible = 1;
         edtavServico_sigla2_Visible = 1;
         dynavServicogrupo_codigo1.Visible = 1;
         edtavServico_nome1_Visible = 1;
         edtavServico_sigla1_Visible = 1;
         edtServicoGrupo_Descricao_Title = "Grupo";
         edtServico_Sigla_Title = "Sigla";
         edtServico_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_servicogrupo_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_servico_siglatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_servico_nometitlecontrolidtoreplace_Visible = 1;
         edtavTfservicogrupo_descricao_sel_Jsonclick = "";
         edtavTfservicogrupo_descricao_sel_Visible = 1;
         edtavTfservicogrupo_descricao_Jsonclick = "";
         edtavTfservicogrupo_descricao_Visible = 1;
         edtavTfservico_sigla_sel_Jsonclick = "";
         edtavTfservico_sigla_sel_Visible = 1;
         edtavTfservico_sigla_Jsonclick = "";
         edtavTfservico_sigla_Visible = 1;
         edtavTfservico_nome_sel_Jsonclick = "";
         edtavTfservico_nome_sel_Visible = 1;
         edtavTfservico_nome_Jsonclick = "";
         edtavTfservico_nome_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         edtServico_Descricao_Visible = 1;
         Ddo_servicogrupo_descricao_Searchbuttontext = "Pesquisar";
         Ddo_servicogrupo_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_servicogrupo_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_servicogrupo_descricao_Loadingdata = "Carregando dados...";
         Ddo_servicogrupo_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_servicogrupo_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_servicogrupo_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_servicogrupo_descricao_Datalistproc = "GetPromptServicoFilterData";
         Ddo_servicogrupo_descricao_Datalisttype = "Dynamic";
         Ddo_servicogrupo_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_servicogrupo_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_servicogrupo_descricao_Filtertype = "Character";
         Ddo_servicogrupo_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_servicogrupo_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_servicogrupo_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_servicogrupo_descricao_Titlecontrolidtoreplace = "";
         Ddo_servicogrupo_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_servicogrupo_descricao_Cls = "ColumnSettings";
         Ddo_servicogrupo_descricao_Tooltip = "Op��es";
         Ddo_servicogrupo_descricao_Caption = "";
         Ddo_servico_sigla_Searchbuttontext = "Pesquisar";
         Ddo_servico_sigla_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_servico_sigla_Cleanfilter = "Limpar pesquisa";
         Ddo_servico_sigla_Loadingdata = "Carregando dados...";
         Ddo_servico_sigla_Sortdsc = "Ordenar de Z � A";
         Ddo_servico_sigla_Sortasc = "Ordenar de A � Z";
         Ddo_servico_sigla_Datalistupdateminimumcharacters = 0;
         Ddo_servico_sigla_Datalistproc = "GetPromptServicoFilterData";
         Ddo_servico_sigla_Datalisttype = "Dynamic";
         Ddo_servico_sigla_Includedatalist = Convert.ToBoolean( -1);
         Ddo_servico_sigla_Filterisrange = Convert.ToBoolean( 0);
         Ddo_servico_sigla_Filtertype = "Character";
         Ddo_servico_sigla_Includefilter = Convert.ToBoolean( -1);
         Ddo_servico_sigla_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_servico_sigla_Includesortasc = Convert.ToBoolean( -1);
         Ddo_servico_sigla_Titlecontrolidtoreplace = "";
         Ddo_servico_sigla_Dropdownoptionstype = "GridTitleSettings";
         Ddo_servico_sigla_Cls = "ColumnSettings";
         Ddo_servico_sigla_Tooltip = "Op��es";
         Ddo_servico_sigla_Caption = "";
         Ddo_servico_nome_Searchbuttontext = "Pesquisar";
         Ddo_servico_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_servico_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_servico_nome_Loadingdata = "Carregando dados...";
         Ddo_servico_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_servico_nome_Sortasc = "Ordenar de A � Z";
         Ddo_servico_nome_Datalistupdateminimumcharacters = 0;
         Ddo_servico_nome_Datalistproc = "GetPromptServicoFilterData";
         Ddo_servico_nome_Datalisttype = "Dynamic";
         Ddo_servico_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_servico_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_servico_nome_Filtertype = "Character";
         Ddo_servico_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_servico_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_servico_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_servico_nome_Titlecontrolidtoreplace = "";
         Ddo_servico_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_servico_nome_Cls = "ColumnSettings";
         Ddo_servico_nome_Tooltip = "Op��es";
         Ddo_servico_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Servico";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV46ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Servico_SiglaTitleControlIdToReplace',fld:'vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ServicoGrupo_DescricaoTitleControlIdToReplace',fld:'vDDO_SERVICOGRUPO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV42Servico_Ativo',fld:'vSERVICO_ATIVO',pic:'',nv:false},{av:'AV44TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV45TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV48TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV49TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV52TFServicoGrupo_Descricao',fld:'vTFSERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'AV53TFServicoGrupo_Descricao_Sel',fld:'vTFSERVICOGRUPO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV62Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV33Servico_Sigla1',fld:'vSERVICO_SIGLA1',pic:'@!',nv:''},{av:'AV34Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV35ServicoGrupo_Codigo1',fld:'vSERVICOGRUPO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV36Servico_Sigla2',fld:'vSERVICO_SIGLA2',pic:'@!',nv:''},{av:'AV37Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV38ServicoGrupo_Codigo2',fld:'vSERVICOGRUPO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Servico_Sigla3',fld:'vSERVICO_SIGLA3',pic:'@!',nv:''},{av:'AV40Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV41ServicoGrupo_Codigo3',fld:'vSERVICOGRUPO_CODIGO3',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV43Servico_NomeTitleFilterData',fld:'vSERVICO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV47Servico_SiglaTitleFilterData',fld:'vSERVICO_SIGLATITLEFILTERDATA',pic:'',nv:null},{av:'AV51ServicoGrupo_DescricaoTitleFilterData',fld:'vSERVICOGRUPO_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'edtServico_Nome_Titleformat',ctrl:'SERVICO_NOME',prop:'Titleformat'},{av:'edtServico_Nome_Title',ctrl:'SERVICO_NOME',prop:'Title'},{av:'edtServico_Sigla_Titleformat',ctrl:'SERVICO_SIGLA',prop:'Titleformat'},{av:'edtServico_Sigla_Title',ctrl:'SERVICO_SIGLA',prop:'Title'},{av:'edtServicoGrupo_Descricao_Titleformat',ctrl:'SERVICOGRUPO_DESCRICAO',prop:'Titleformat'},{av:'edtServicoGrupo_Descricao_Title',ctrl:'SERVICOGRUPO_DESCRICAO',prop:'Title'},{av:'AV57GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV58GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E116Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV33Servico_Sigla1',fld:'vSERVICO_SIGLA1',pic:'@!',nv:''},{av:'AV34Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV35ServicoGrupo_Codigo1',fld:'vSERVICOGRUPO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV36Servico_Sigla2',fld:'vSERVICO_SIGLA2',pic:'@!',nv:''},{av:'AV37Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV38ServicoGrupo_Codigo2',fld:'vSERVICOGRUPO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Servico_Sigla3',fld:'vSERVICO_SIGLA3',pic:'@!',nv:''},{av:'AV40Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV41ServicoGrupo_Codigo3',fld:'vSERVICOGRUPO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV44TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV45TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV48TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV49TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV52TFServicoGrupo_Descricao',fld:'vTFSERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'AV53TFServicoGrupo_Descricao_Sel',fld:'vTFSERVICOGRUPO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV46ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Servico_SiglaTitleControlIdToReplace',fld:'vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ServicoGrupo_DescricaoTitleControlIdToReplace',fld:'vDDO_SERVICOGRUPO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42Servico_Ativo',fld:'vSERVICO_ATIVO',pic:'',nv:false},{av:'AV62Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_SERVICO_NOME.ONOPTIONCLICKED","{handler:'E126Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV33Servico_Sigla1',fld:'vSERVICO_SIGLA1',pic:'@!',nv:''},{av:'AV34Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV35ServicoGrupo_Codigo1',fld:'vSERVICOGRUPO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV36Servico_Sigla2',fld:'vSERVICO_SIGLA2',pic:'@!',nv:''},{av:'AV37Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV38ServicoGrupo_Codigo2',fld:'vSERVICOGRUPO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Servico_Sigla3',fld:'vSERVICO_SIGLA3',pic:'@!',nv:''},{av:'AV40Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV41ServicoGrupo_Codigo3',fld:'vSERVICOGRUPO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV44TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV45TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV48TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV49TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV52TFServicoGrupo_Descricao',fld:'vTFSERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'AV53TFServicoGrupo_Descricao_Sel',fld:'vTFSERVICOGRUPO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV46ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Servico_SiglaTitleControlIdToReplace',fld:'vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ServicoGrupo_DescricaoTitleControlIdToReplace',fld:'vDDO_SERVICOGRUPO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42Servico_Ativo',fld:'vSERVICO_ATIVO',pic:'',nv:false},{av:'AV62Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_servico_nome_Activeeventkey',ctrl:'DDO_SERVICO_NOME',prop:'ActiveEventKey'},{av:'Ddo_servico_nome_Filteredtext_get',ctrl:'DDO_SERVICO_NOME',prop:'FilteredText_get'},{av:'Ddo_servico_nome_Selectedvalue_get',ctrl:'DDO_SERVICO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_servico_nome_Sortedstatus',ctrl:'DDO_SERVICO_NOME',prop:'SortedStatus'},{av:'AV44TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV45TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_servico_sigla_Sortedstatus',ctrl:'DDO_SERVICO_SIGLA',prop:'SortedStatus'},{av:'Ddo_servicogrupo_descricao_Sortedstatus',ctrl:'DDO_SERVICOGRUPO_DESCRICAO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SERVICO_SIGLA.ONOPTIONCLICKED","{handler:'E136Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV33Servico_Sigla1',fld:'vSERVICO_SIGLA1',pic:'@!',nv:''},{av:'AV34Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV35ServicoGrupo_Codigo1',fld:'vSERVICOGRUPO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV36Servico_Sigla2',fld:'vSERVICO_SIGLA2',pic:'@!',nv:''},{av:'AV37Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV38ServicoGrupo_Codigo2',fld:'vSERVICOGRUPO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Servico_Sigla3',fld:'vSERVICO_SIGLA3',pic:'@!',nv:''},{av:'AV40Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV41ServicoGrupo_Codigo3',fld:'vSERVICOGRUPO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV44TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV45TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV48TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV49TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV52TFServicoGrupo_Descricao',fld:'vTFSERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'AV53TFServicoGrupo_Descricao_Sel',fld:'vTFSERVICOGRUPO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV46ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Servico_SiglaTitleControlIdToReplace',fld:'vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ServicoGrupo_DescricaoTitleControlIdToReplace',fld:'vDDO_SERVICOGRUPO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42Servico_Ativo',fld:'vSERVICO_ATIVO',pic:'',nv:false},{av:'AV62Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_servico_sigla_Activeeventkey',ctrl:'DDO_SERVICO_SIGLA',prop:'ActiveEventKey'},{av:'Ddo_servico_sigla_Filteredtext_get',ctrl:'DDO_SERVICO_SIGLA',prop:'FilteredText_get'},{av:'Ddo_servico_sigla_Selectedvalue_get',ctrl:'DDO_SERVICO_SIGLA',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_servico_sigla_Sortedstatus',ctrl:'DDO_SERVICO_SIGLA',prop:'SortedStatus'},{av:'AV48TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV49TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_servico_nome_Sortedstatus',ctrl:'DDO_SERVICO_NOME',prop:'SortedStatus'},{av:'Ddo_servicogrupo_descricao_Sortedstatus',ctrl:'DDO_SERVICOGRUPO_DESCRICAO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SERVICOGRUPO_DESCRICAO.ONOPTIONCLICKED","{handler:'E146Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV33Servico_Sigla1',fld:'vSERVICO_SIGLA1',pic:'@!',nv:''},{av:'AV34Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV35ServicoGrupo_Codigo1',fld:'vSERVICOGRUPO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV36Servico_Sigla2',fld:'vSERVICO_SIGLA2',pic:'@!',nv:''},{av:'AV37Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV38ServicoGrupo_Codigo2',fld:'vSERVICOGRUPO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Servico_Sigla3',fld:'vSERVICO_SIGLA3',pic:'@!',nv:''},{av:'AV40Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV41ServicoGrupo_Codigo3',fld:'vSERVICOGRUPO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV44TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV45TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV48TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV49TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV52TFServicoGrupo_Descricao',fld:'vTFSERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'AV53TFServicoGrupo_Descricao_Sel',fld:'vTFSERVICOGRUPO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV46ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Servico_SiglaTitleControlIdToReplace',fld:'vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ServicoGrupo_DescricaoTitleControlIdToReplace',fld:'vDDO_SERVICOGRUPO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42Servico_Ativo',fld:'vSERVICO_ATIVO',pic:'',nv:false},{av:'AV62Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_servicogrupo_descricao_Activeeventkey',ctrl:'DDO_SERVICOGRUPO_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_servicogrupo_descricao_Filteredtext_get',ctrl:'DDO_SERVICOGRUPO_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_servicogrupo_descricao_Selectedvalue_get',ctrl:'DDO_SERVICOGRUPO_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_servicogrupo_descricao_Sortedstatus',ctrl:'DDO_SERVICOGRUPO_DESCRICAO',prop:'SortedStatus'},{av:'AV52TFServicoGrupo_Descricao',fld:'vTFSERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'AV53TFServicoGrupo_Descricao_Sel',fld:'vTFSERVICOGRUPO_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_servico_nome_Sortedstatus',ctrl:'DDO_SERVICO_NOME',prop:'SortedStatus'},{av:'Ddo_servico_sigla_Sortedstatus',ctrl:'DDO_SERVICO_SIGLA',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E276Z2',iparms:[],oparms:[{av:'AV31Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E286Z2',iparms:[{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A156Servico_Descricao',fld:'SERVICO_DESCRICAO',pic:'',nv:''}],oparms:[{av:'AV7InOutServico_Codigo',fld:'vINOUTSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8InOutServico_Descricao',fld:'vINOUTSERVICO_DESCRICAO',pic:'',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E156Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV33Servico_Sigla1',fld:'vSERVICO_SIGLA1',pic:'@!',nv:''},{av:'AV34Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV35ServicoGrupo_Codigo1',fld:'vSERVICOGRUPO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV36Servico_Sigla2',fld:'vSERVICO_SIGLA2',pic:'@!',nv:''},{av:'AV37Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV38ServicoGrupo_Codigo2',fld:'vSERVICOGRUPO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Servico_Sigla3',fld:'vSERVICO_SIGLA3',pic:'@!',nv:''},{av:'AV40Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV41ServicoGrupo_Codigo3',fld:'vSERVICOGRUPO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV44TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV45TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV48TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV49TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV52TFServicoGrupo_Descricao',fld:'vTFSERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'AV53TFServicoGrupo_Descricao_Sel',fld:'vTFSERVICOGRUPO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV46ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Servico_SiglaTitleControlIdToReplace',fld:'vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ServicoGrupo_DescricaoTitleControlIdToReplace',fld:'vDDO_SERVICOGRUPO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42Servico_Ativo',fld:'vSERVICO_ATIVO',pic:'',nv:false},{av:'AV62Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E206Z2',iparms:[],oparms:[{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E166Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV33Servico_Sigla1',fld:'vSERVICO_SIGLA1',pic:'@!',nv:''},{av:'AV34Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV35ServicoGrupo_Codigo1',fld:'vSERVICOGRUPO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV36Servico_Sigla2',fld:'vSERVICO_SIGLA2',pic:'@!',nv:''},{av:'AV37Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV38ServicoGrupo_Codigo2',fld:'vSERVICOGRUPO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Servico_Sigla3',fld:'vSERVICO_SIGLA3',pic:'@!',nv:''},{av:'AV40Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV41ServicoGrupo_Codigo3',fld:'vSERVICOGRUPO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV44TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV45TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV48TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV49TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV52TFServicoGrupo_Descricao',fld:'vTFSERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'AV53TFServicoGrupo_Descricao_Sel',fld:'vTFSERVICOGRUPO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV46ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Servico_SiglaTitleControlIdToReplace',fld:'vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ServicoGrupo_DescricaoTitleControlIdToReplace',fld:'vDDO_SERVICOGRUPO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42Servico_Ativo',fld:'vSERVICO_ATIVO',pic:'',nv:false},{av:'AV62Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV36Servico_Sigla2',fld:'vSERVICO_SIGLA2',pic:'@!',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Servico_Sigla3',fld:'vSERVICO_SIGLA3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV33Servico_Sigla1',fld:'vSERVICO_SIGLA1',pic:'@!',nv:''},{av:'AV34Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV35ServicoGrupo_Codigo1',fld:'vSERVICOGRUPO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV37Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV38ServicoGrupo_Codigo2',fld:'vSERVICOGRUPO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV40Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV41ServicoGrupo_Codigo3',fld:'vSERVICOGRUPO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'edtavServico_sigla2_Visible',ctrl:'vSERVICO_SIGLA2',prop:'Visible'},{av:'edtavServico_nome2_Visible',ctrl:'vSERVICO_NOME2',prop:'Visible'},{av:'dynavServicogrupo_codigo2'},{av:'edtavServico_sigla3_Visible',ctrl:'vSERVICO_SIGLA3',prop:'Visible'},{av:'edtavServico_nome3_Visible',ctrl:'vSERVICO_NOME3',prop:'Visible'},{av:'dynavServicogrupo_codigo3'},{av:'edtavServico_sigla1_Visible',ctrl:'vSERVICO_SIGLA1',prop:'Visible'},{av:'edtavServico_nome1_Visible',ctrl:'vSERVICO_NOME1',prop:'Visible'},{av:'dynavServicogrupo_codigo1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E216Z2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavServico_sigla1_Visible',ctrl:'vSERVICO_SIGLA1',prop:'Visible'},{av:'edtavServico_nome1_Visible',ctrl:'vSERVICO_NOME1',prop:'Visible'},{av:'dynavServicogrupo_codigo1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E226Z2',iparms:[],oparms:[{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E176Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV33Servico_Sigla1',fld:'vSERVICO_SIGLA1',pic:'@!',nv:''},{av:'AV34Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV35ServicoGrupo_Codigo1',fld:'vSERVICOGRUPO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV36Servico_Sigla2',fld:'vSERVICO_SIGLA2',pic:'@!',nv:''},{av:'AV37Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV38ServicoGrupo_Codigo2',fld:'vSERVICOGRUPO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Servico_Sigla3',fld:'vSERVICO_SIGLA3',pic:'@!',nv:''},{av:'AV40Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV41ServicoGrupo_Codigo3',fld:'vSERVICOGRUPO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV44TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV45TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV48TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV49TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV52TFServicoGrupo_Descricao',fld:'vTFSERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'AV53TFServicoGrupo_Descricao_Sel',fld:'vTFSERVICOGRUPO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV46ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Servico_SiglaTitleControlIdToReplace',fld:'vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ServicoGrupo_DescricaoTitleControlIdToReplace',fld:'vDDO_SERVICOGRUPO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42Servico_Ativo',fld:'vSERVICO_ATIVO',pic:'',nv:false},{av:'AV62Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV36Servico_Sigla2',fld:'vSERVICO_SIGLA2',pic:'@!',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Servico_Sigla3',fld:'vSERVICO_SIGLA3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV33Servico_Sigla1',fld:'vSERVICO_SIGLA1',pic:'@!',nv:''},{av:'AV34Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV35ServicoGrupo_Codigo1',fld:'vSERVICOGRUPO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV37Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV38ServicoGrupo_Codigo2',fld:'vSERVICOGRUPO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV40Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV41ServicoGrupo_Codigo3',fld:'vSERVICOGRUPO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'edtavServico_sigla2_Visible',ctrl:'vSERVICO_SIGLA2',prop:'Visible'},{av:'edtavServico_nome2_Visible',ctrl:'vSERVICO_NOME2',prop:'Visible'},{av:'dynavServicogrupo_codigo2'},{av:'edtavServico_sigla3_Visible',ctrl:'vSERVICO_SIGLA3',prop:'Visible'},{av:'edtavServico_nome3_Visible',ctrl:'vSERVICO_NOME3',prop:'Visible'},{av:'dynavServicogrupo_codigo3'},{av:'edtavServico_sigla1_Visible',ctrl:'vSERVICO_SIGLA1',prop:'Visible'},{av:'edtavServico_nome1_Visible',ctrl:'vSERVICO_NOME1',prop:'Visible'},{av:'dynavServicogrupo_codigo1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E236Z2',iparms:[{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavServico_sigla2_Visible',ctrl:'vSERVICO_SIGLA2',prop:'Visible'},{av:'edtavServico_nome2_Visible',ctrl:'vSERVICO_NOME2',prop:'Visible'},{av:'dynavServicogrupo_codigo2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E186Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV33Servico_Sigla1',fld:'vSERVICO_SIGLA1',pic:'@!',nv:''},{av:'AV34Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV35ServicoGrupo_Codigo1',fld:'vSERVICOGRUPO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV36Servico_Sigla2',fld:'vSERVICO_SIGLA2',pic:'@!',nv:''},{av:'AV37Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV38ServicoGrupo_Codigo2',fld:'vSERVICOGRUPO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Servico_Sigla3',fld:'vSERVICO_SIGLA3',pic:'@!',nv:''},{av:'AV40Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV41ServicoGrupo_Codigo3',fld:'vSERVICOGRUPO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV44TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV45TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV48TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV49TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV52TFServicoGrupo_Descricao',fld:'vTFSERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'AV53TFServicoGrupo_Descricao_Sel',fld:'vTFSERVICOGRUPO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV46ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Servico_SiglaTitleControlIdToReplace',fld:'vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ServicoGrupo_DescricaoTitleControlIdToReplace',fld:'vDDO_SERVICOGRUPO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42Servico_Ativo',fld:'vSERVICO_ATIVO',pic:'',nv:false},{av:'AV62Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV36Servico_Sigla2',fld:'vSERVICO_SIGLA2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Servico_Sigla3',fld:'vSERVICO_SIGLA3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV33Servico_Sigla1',fld:'vSERVICO_SIGLA1',pic:'@!',nv:''},{av:'AV34Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV35ServicoGrupo_Codigo1',fld:'vSERVICOGRUPO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV37Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV38ServicoGrupo_Codigo2',fld:'vSERVICOGRUPO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV40Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV41ServicoGrupo_Codigo3',fld:'vSERVICOGRUPO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'edtavServico_sigla2_Visible',ctrl:'vSERVICO_SIGLA2',prop:'Visible'},{av:'edtavServico_nome2_Visible',ctrl:'vSERVICO_NOME2',prop:'Visible'},{av:'dynavServicogrupo_codigo2'},{av:'edtavServico_sigla3_Visible',ctrl:'vSERVICO_SIGLA3',prop:'Visible'},{av:'edtavServico_nome3_Visible',ctrl:'vSERVICO_NOME3',prop:'Visible'},{av:'dynavServicogrupo_codigo3'},{av:'edtavServico_sigla1_Visible',ctrl:'vSERVICO_SIGLA1',prop:'Visible'},{av:'edtavServico_nome1_Visible',ctrl:'vSERVICO_NOME1',prop:'Visible'},{av:'dynavServicogrupo_codigo1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E246Z2',iparms:[{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavServico_sigla3_Visible',ctrl:'vSERVICO_SIGLA3',prop:'Visible'},{av:'edtavServico_nome3_Visible',ctrl:'vSERVICO_NOME3',prop:'Visible'},{av:'dynavServicogrupo_codigo3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E196Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV33Servico_Sigla1',fld:'vSERVICO_SIGLA1',pic:'@!',nv:''},{av:'AV34Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV35ServicoGrupo_Codigo1',fld:'vSERVICOGRUPO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV36Servico_Sigla2',fld:'vSERVICO_SIGLA2',pic:'@!',nv:''},{av:'AV37Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV38ServicoGrupo_Codigo2',fld:'vSERVICOGRUPO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Servico_Sigla3',fld:'vSERVICO_SIGLA3',pic:'@!',nv:''},{av:'AV40Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV41ServicoGrupo_Codigo3',fld:'vSERVICOGRUPO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV44TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV45TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV48TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV49TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV52TFServicoGrupo_Descricao',fld:'vTFSERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'AV53TFServicoGrupo_Descricao_Sel',fld:'vTFSERVICOGRUPO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV46ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Servico_SiglaTitleControlIdToReplace',fld:'vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ServicoGrupo_DescricaoTitleControlIdToReplace',fld:'vDDO_SERVICOGRUPO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42Servico_Ativo',fld:'vSERVICO_ATIVO',pic:'',nv:false},{av:'AV62Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV42Servico_Ativo',fld:'vSERVICO_ATIVO',pic:'',nv:false},{av:'AV44TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'Ddo_servico_nome_Filteredtext_set',ctrl:'DDO_SERVICO_NOME',prop:'FilteredText_set'},{av:'AV45TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_servico_nome_Selectedvalue_set',ctrl:'DDO_SERVICO_NOME',prop:'SelectedValue_set'},{av:'AV48TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'Ddo_servico_sigla_Filteredtext_set',ctrl:'DDO_SERVICO_SIGLA',prop:'FilteredText_set'},{av:'AV49TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_servico_sigla_Selectedvalue_set',ctrl:'DDO_SERVICO_SIGLA',prop:'SelectedValue_set'},{av:'AV52TFServicoGrupo_Descricao',fld:'vTFSERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'Ddo_servicogrupo_descricao_Filteredtext_set',ctrl:'DDO_SERVICOGRUPO_DESCRICAO',prop:'FilteredText_set'},{av:'AV53TFServicoGrupo_Descricao_Sel',fld:'vTFSERVICOGRUPO_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_servicogrupo_descricao_Selectedvalue_set',ctrl:'DDO_SERVICOGRUPO_DESCRICAO',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV33Servico_Sigla1',fld:'vSERVICO_SIGLA1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavServico_sigla1_Visible',ctrl:'vSERVICO_SIGLA1',prop:'Visible'},{av:'edtavServico_nome1_Visible',ctrl:'vSERVICO_NOME1',prop:'Visible'},{av:'dynavServicogrupo_codigo1'},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV36Servico_Sigla2',fld:'vSERVICO_SIGLA2',pic:'@!',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Servico_Sigla3',fld:'vSERVICO_SIGLA3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV34Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV35ServicoGrupo_Codigo1',fld:'vSERVICOGRUPO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV37Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV38ServicoGrupo_Codigo2',fld:'vSERVICOGRUPO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV40Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV41ServicoGrupo_Codigo3',fld:'vSERVICOGRUPO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'edtavServico_sigla2_Visible',ctrl:'vSERVICO_SIGLA2',prop:'Visible'},{av:'edtavServico_nome2_Visible',ctrl:'vSERVICO_NOME2',prop:'Visible'},{av:'dynavServicogrupo_codigo2'},{av:'edtavServico_sigla3_Visible',ctrl:'vSERVICO_SIGLA3',prop:'Visible'},{av:'edtavServico_nome3_Visible',ctrl:'vSERVICO_NOME3',prop:'Visible'},{av:'dynavServicogrupo_codigo3'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV8InOutServico_Descricao = "";
         Gridpaginationbar_Selectedpage = "";
         Ddo_servico_nome_Activeeventkey = "";
         Ddo_servico_nome_Filteredtext_get = "";
         Ddo_servico_nome_Selectedvalue_get = "";
         Ddo_servico_sigla_Activeeventkey = "";
         Ddo_servico_sigla_Filteredtext_get = "";
         Ddo_servico_sigla_Selectedvalue_get = "";
         Ddo_servicogrupo_descricao_Activeeventkey = "";
         Ddo_servicogrupo_descricao_Filteredtext_get = "";
         Ddo_servicogrupo_descricao_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV33Servico_Sigla1 = "";
         AV34Servico_Nome1 = "";
         AV20DynamicFiltersSelector2 = "";
         AV36Servico_Sigla2 = "";
         AV37Servico_Nome2 = "";
         AV25DynamicFiltersSelector3 = "";
         AV39Servico_Sigla3 = "";
         AV40Servico_Nome3 = "";
         AV44TFServico_Nome = "";
         AV45TFServico_Nome_Sel = "";
         AV48TFServico_Sigla = "";
         AV49TFServico_Sigla_Sel = "";
         AV52TFServicoGrupo_Descricao = "";
         AV53TFServicoGrupo_Descricao_Sel = "";
         AV46ddo_Servico_NomeTitleControlIdToReplace = "";
         AV50ddo_Servico_SiglaTitleControlIdToReplace = "";
         AV54ddo_ServicoGrupo_DescricaoTitleControlIdToReplace = "";
         AV42Servico_Ativo = true;
         AV62Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         forbiddenHiddens = "";
         A156Servico_Descricao = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV55DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV43Servico_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV47Servico_SiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV51ServicoGrupo_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_servico_nome_Filteredtext_set = "";
         Ddo_servico_nome_Selectedvalue_set = "";
         Ddo_servico_nome_Sortedstatus = "";
         Ddo_servico_sigla_Filteredtext_set = "";
         Ddo_servico_sigla_Selectedvalue_set = "";
         Ddo_servico_sigla_Sortedstatus = "";
         Ddo_servicogrupo_descricao_Filteredtext_set = "";
         Ddo_servicogrupo_descricao_Selectedvalue_set = "";
         Ddo_servicogrupo_descricao_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV31Select = "";
         AV61Select_GXI = "";
         A608Servico_Nome = "";
         A605Servico_Sigla = "";
         A158ServicoGrupo_Descricao = "";
         scmdbuf = "";
         H006Z2_A157ServicoGrupo_Codigo = new int[1] ;
         H006Z2_A158ServicoGrupo_Descricao = new String[] {""} ;
         H006Z3_A157ServicoGrupo_Codigo = new int[1] ;
         H006Z3_A158ServicoGrupo_Descricao = new String[] {""} ;
         H006Z4_A157ServicoGrupo_Codigo = new int[1] ;
         H006Z4_A158ServicoGrupo_Descricao = new String[] {""} ;
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         H006Z5_A157ServicoGrupo_Codigo = new int[1] ;
         H006Z5_A158ServicoGrupo_Descricao = new String[] {""} ;
         H006Z6_A157ServicoGrupo_Codigo = new int[1] ;
         H006Z6_A158ServicoGrupo_Descricao = new String[] {""} ;
         H006Z7_A157ServicoGrupo_Codigo = new int[1] ;
         H006Z7_A158ServicoGrupo_Descricao = new String[] {""} ;
         GridContainer = new GXWebGrid( context);
         lV34Servico_Nome1 = "";
         lV37Servico_Nome2 = "";
         lV40Servico_Nome3 = "";
         lV44TFServico_Nome = "";
         lV48TFServico_Sigla = "";
         lV52TFServicoGrupo_Descricao = "";
         H006Z8_A159ServicoGrupo_Ativo = new bool[] {false} ;
         H006Z8_A157ServicoGrupo_Codigo = new int[1] ;
         H006Z8_A632Servico_Ativo = new bool[] {false} ;
         H006Z8_A156Servico_Descricao = new String[] {""} ;
         H006Z8_n156Servico_Descricao = new bool[] {false} ;
         H006Z8_A158ServicoGrupo_Descricao = new String[] {""} ;
         H006Z8_A605Servico_Sigla = new String[] {""} ;
         H006Z8_A608Servico_Nome = new String[] {""} ;
         H006Z8_A155Servico_Codigo = new int[1] ;
         H006Z9_AGRID_nRecordCount = new long[1] ;
         hsh = "";
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblFiltertextservico_ativo_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptservico__default(),
            new Object[][] {
                new Object[] {
               H006Z2_A157ServicoGrupo_Codigo, H006Z2_A158ServicoGrupo_Descricao
               }
               , new Object[] {
               H006Z3_A157ServicoGrupo_Codigo, H006Z3_A158ServicoGrupo_Descricao
               }
               , new Object[] {
               H006Z4_A157ServicoGrupo_Codigo, H006Z4_A158ServicoGrupo_Descricao
               }
               , new Object[] {
               H006Z5_A157ServicoGrupo_Codigo, H006Z5_A158ServicoGrupo_Descricao
               }
               , new Object[] {
               H006Z6_A157ServicoGrupo_Codigo, H006Z6_A158ServicoGrupo_Descricao
               }
               , new Object[] {
               H006Z7_A157ServicoGrupo_Codigo, H006Z7_A158ServicoGrupo_Descricao
               }
               , new Object[] {
               H006Z8_A159ServicoGrupo_Ativo, H006Z8_A157ServicoGrupo_Codigo, H006Z8_A632Servico_Ativo, H006Z8_A156Servico_Descricao, H006Z8_n156Servico_Descricao, H006Z8_A158ServicoGrupo_Descricao, H006Z8_A605Servico_Sigla, H006Z8_A608Servico_Nome, H006Z8_A155Servico_Codigo
               }
               , new Object[] {
               H006Z9_AGRID_nRecordCount
               }
            }
         );
         AV62Pgmname = "PromptServico";
         /* GeneXus formulas. */
         AV62Pgmname = "PromptServico";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_75 ;
      private short nGXsfl_75_idx=1 ;
      private short AV13OrderedBy ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_75_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtServico_Nome_Titleformat ;
      private short edtServico_Sigla_Titleformat ;
      private short edtServicoGrupo_Descricao_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutServico_Codigo ;
      private int wcpOAV7InOutServico_Codigo ;
      private int subGrid_Rows ;
      private int AV35ServicoGrupo_Codigo1 ;
      private int AV38ServicoGrupo_Codigo2 ;
      private int AV41ServicoGrupo_Codigo3 ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_servico_nome_Datalistupdateminimumcharacters ;
      private int Ddo_servico_sigla_Datalistupdateminimumcharacters ;
      private int Ddo_servicogrupo_descricao_Datalistupdateminimumcharacters ;
      private int edtServico_Descricao_Visible ;
      private int edtavTfservico_nome_Visible ;
      private int edtavTfservico_nome_sel_Visible ;
      private int edtavTfservico_sigla_Visible ;
      private int edtavTfservico_sigla_sel_Visible ;
      private int edtavTfservicogrupo_descricao_Visible ;
      private int edtavTfservicogrupo_descricao_sel_Visible ;
      private int edtavDdo_servico_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_servico_siglatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_servicogrupo_descricaotitlecontrolidtoreplace_Visible ;
      private int A155Servico_Codigo ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int A157ServicoGrupo_Codigo ;
      private int edtavOrdereddsc_Visible ;
      private int AV56PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavServico_sigla1_Visible ;
      private int edtavServico_nome1_Visible ;
      private int edtavServico_sigla2_Visible ;
      private int edtavServico_nome2_Visible ;
      private int edtavServico_sigla3_Visible ;
      private int edtavServico_nome3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV57GridCurrentPage ;
      private long AV58GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_servico_nome_Activeeventkey ;
      private String Ddo_servico_nome_Filteredtext_get ;
      private String Ddo_servico_nome_Selectedvalue_get ;
      private String Ddo_servico_sigla_Activeeventkey ;
      private String Ddo_servico_sigla_Filteredtext_get ;
      private String Ddo_servico_sigla_Selectedvalue_get ;
      private String Ddo_servicogrupo_descricao_Activeeventkey ;
      private String Ddo_servicogrupo_descricao_Filteredtext_get ;
      private String Ddo_servicogrupo_descricao_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_75_idx="0001" ;
      private String AV33Servico_Sigla1 ;
      private String AV34Servico_Nome1 ;
      private String AV36Servico_Sigla2 ;
      private String AV37Servico_Nome2 ;
      private String AV39Servico_Sigla3 ;
      private String AV40Servico_Nome3 ;
      private String AV44TFServico_Nome ;
      private String AV45TFServico_Nome_Sel ;
      private String AV48TFServico_Sigla ;
      private String AV49TFServico_Sigla_Sel ;
      private String AV62Pgmname ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_servico_nome_Caption ;
      private String Ddo_servico_nome_Tooltip ;
      private String Ddo_servico_nome_Cls ;
      private String Ddo_servico_nome_Filteredtext_set ;
      private String Ddo_servico_nome_Selectedvalue_set ;
      private String Ddo_servico_nome_Dropdownoptionstype ;
      private String Ddo_servico_nome_Titlecontrolidtoreplace ;
      private String Ddo_servico_nome_Sortedstatus ;
      private String Ddo_servico_nome_Filtertype ;
      private String Ddo_servico_nome_Datalisttype ;
      private String Ddo_servico_nome_Datalistproc ;
      private String Ddo_servico_nome_Sortasc ;
      private String Ddo_servico_nome_Sortdsc ;
      private String Ddo_servico_nome_Loadingdata ;
      private String Ddo_servico_nome_Cleanfilter ;
      private String Ddo_servico_nome_Noresultsfound ;
      private String Ddo_servico_nome_Searchbuttontext ;
      private String Ddo_servico_sigla_Caption ;
      private String Ddo_servico_sigla_Tooltip ;
      private String Ddo_servico_sigla_Cls ;
      private String Ddo_servico_sigla_Filteredtext_set ;
      private String Ddo_servico_sigla_Selectedvalue_set ;
      private String Ddo_servico_sigla_Dropdownoptionstype ;
      private String Ddo_servico_sigla_Titlecontrolidtoreplace ;
      private String Ddo_servico_sigla_Sortedstatus ;
      private String Ddo_servico_sigla_Filtertype ;
      private String Ddo_servico_sigla_Datalisttype ;
      private String Ddo_servico_sigla_Datalistproc ;
      private String Ddo_servico_sigla_Sortasc ;
      private String Ddo_servico_sigla_Sortdsc ;
      private String Ddo_servico_sigla_Loadingdata ;
      private String Ddo_servico_sigla_Cleanfilter ;
      private String Ddo_servico_sigla_Noresultsfound ;
      private String Ddo_servico_sigla_Searchbuttontext ;
      private String Ddo_servicogrupo_descricao_Caption ;
      private String Ddo_servicogrupo_descricao_Tooltip ;
      private String Ddo_servicogrupo_descricao_Cls ;
      private String Ddo_servicogrupo_descricao_Filteredtext_set ;
      private String Ddo_servicogrupo_descricao_Selectedvalue_set ;
      private String Ddo_servicogrupo_descricao_Dropdownoptionstype ;
      private String Ddo_servicogrupo_descricao_Titlecontrolidtoreplace ;
      private String Ddo_servicogrupo_descricao_Sortedstatus ;
      private String Ddo_servicogrupo_descricao_Filtertype ;
      private String Ddo_servicogrupo_descricao_Datalisttype ;
      private String Ddo_servicogrupo_descricao_Datalistproc ;
      private String Ddo_servicogrupo_descricao_Sortasc ;
      private String Ddo_servicogrupo_descricao_Sortdsc ;
      private String Ddo_servicogrupo_descricao_Loadingdata ;
      private String Ddo_servicogrupo_descricao_Cleanfilter ;
      private String Ddo_servicogrupo_descricao_Noresultsfound ;
      private String Ddo_servicogrupo_descricao_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String ClassString ;
      private String StyleString ;
      private String edtServico_Descricao_Internalname ;
      private String TempTags ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfservico_nome_Internalname ;
      private String edtavTfservico_nome_Jsonclick ;
      private String edtavTfservico_nome_sel_Internalname ;
      private String edtavTfservico_nome_sel_Jsonclick ;
      private String edtavTfservico_sigla_Internalname ;
      private String edtavTfservico_sigla_Jsonclick ;
      private String edtavTfservico_sigla_sel_Internalname ;
      private String edtavTfservico_sigla_sel_Jsonclick ;
      private String edtavTfservicogrupo_descricao_Internalname ;
      private String edtavTfservicogrupo_descricao_Jsonclick ;
      private String edtavTfservicogrupo_descricao_sel_Internalname ;
      private String edtavTfservicogrupo_descricao_sel_Jsonclick ;
      private String edtavDdo_servico_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_servico_siglatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_servicogrupo_descricaotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtServico_Codigo_Internalname ;
      private String A608Servico_Nome ;
      private String edtServico_Nome_Internalname ;
      private String A605Servico_Sigla ;
      private String edtServico_Sigla_Internalname ;
      private String edtServicoGrupo_Descricao_Internalname ;
      private String scmdbuf ;
      private String cmbavOrderedby_Internalname ;
      private String gxwrpcisep ;
      private String lV34Servico_Nome1 ;
      private String lV37Servico_Nome2 ;
      private String lV40Servico_Nome3 ;
      private String lV44TFServico_Nome ;
      private String lV48TFServico_Sigla ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavServico_ativo_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavServico_sigla1_Internalname ;
      private String edtavServico_nome1_Internalname ;
      private String dynavServicogrupo_codigo1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavServico_sigla2_Internalname ;
      private String edtavServico_nome2_Internalname ;
      private String dynavServicogrupo_codigo2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavServico_sigla3_Internalname ;
      private String edtavServico_nome3_Internalname ;
      private String dynavServicogrupo_codigo3_Internalname ;
      private String hsh ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_servico_nome_Internalname ;
      private String Ddo_servico_sigla_Internalname ;
      private String Ddo_servicogrupo_descricao_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtServico_Nome_Title ;
      private String edtServico_Sigla_Title ;
      private String edtServicoGrupo_Descricao_Title ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblFiltertextservico_ativo_Internalname ;
      private String lblFiltertextservico_ativo_Jsonclick ;
      private String cmbavServico_ativo_Jsonclick ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavServico_sigla1_Jsonclick ;
      private String edtavServico_nome1_Jsonclick ;
      private String dynavServicogrupo_codigo1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavServico_sigla2_Jsonclick ;
      private String edtavServico_nome2_Jsonclick ;
      private String dynavServicogrupo_codigo2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavServico_sigla3_Jsonclick ;
      private String edtavServico_nome3_Jsonclick ;
      private String dynavServicogrupo_codigo3_Jsonclick ;
      private String sGXsfl_75_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtServico_Codigo_Jsonclick ;
      private String edtServico_Nome_Jsonclick ;
      private String edtServico_Sigla_Jsonclick ;
      private String edtServicoGrupo_Descricao_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV19DynamicFiltersEnabled2 ;
      private bool AV24DynamicFiltersEnabled3 ;
      private bool AV42Servico_Ativo ;
      private bool AV30DynamicFiltersIgnoreFirst ;
      private bool AV29DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_servico_nome_Includesortasc ;
      private bool Ddo_servico_nome_Includesortdsc ;
      private bool Ddo_servico_nome_Includefilter ;
      private bool Ddo_servico_nome_Filterisrange ;
      private bool Ddo_servico_nome_Includedatalist ;
      private bool Ddo_servico_sigla_Includesortasc ;
      private bool Ddo_servico_sigla_Includesortdsc ;
      private bool Ddo_servico_sigla_Includefilter ;
      private bool Ddo_servico_sigla_Filterisrange ;
      private bool Ddo_servico_sigla_Includedatalist ;
      private bool Ddo_servicogrupo_descricao_Includesortasc ;
      private bool Ddo_servicogrupo_descricao_Includesortdsc ;
      private bool Ddo_servicogrupo_descricao_Includefilter ;
      private bool Ddo_servicogrupo_descricao_Filterisrange ;
      private bool Ddo_servicogrupo_descricao_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool A159ServicoGrupo_Ativo ;
      private bool A632Servico_Ativo ;
      private bool n156Servico_Descricao ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV31Select_IsBlob ;
      private String AV8InOutServico_Descricao ;
      private String wcpOAV8InOutServico_Descricao ;
      private String A156Servico_Descricao ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV20DynamicFiltersSelector2 ;
      private String AV25DynamicFiltersSelector3 ;
      private String AV52TFServicoGrupo_Descricao ;
      private String AV53TFServicoGrupo_Descricao_Sel ;
      private String AV46ddo_Servico_NomeTitleControlIdToReplace ;
      private String AV50ddo_Servico_SiglaTitleControlIdToReplace ;
      private String AV54ddo_ServicoGrupo_DescricaoTitleControlIdToReplace ;
      private String AV61Select_GXI ;
      private String A158ServicoGrupo_Descricao ;
      private String lV52TFServicoGrupo_Descricao ;
      private String AV31Select ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutServico_Codigo ;
      private String aP1_InOutServico_Descricao ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavServico_ativo ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox dynavServicogrupo_codigo1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox dynavServicogrupo_codigo2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox dynavServicogrupo_codigo3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H006Z2_A157ServicoGrupo_Codigo ;
      private String[] H006Z2_A158ServicoGrupo_Descricao ;
      private int[] H006Z3_A157ServicoGrupo_Codigo ;
      private String[] H006Z3_A158ServicoGrupo_Descricao ;
      private int[] H006Z4_A157ServicoGrupo_Codigo ;
      private String[] H006Z4_A158ServicoGrupo_Descricao ;
      private int[] H006Z5_A157ServicoGrupo_Codigo ;
      private String[] H006Z5_A158ServicoGrupo_Descricao ;
      private int[] H006Z6_A157ServicoGrupo_Codigo ;
      private String[] H006Z6_A158ServicoGrupo_Descricao ;
      private int[] H006Z7_A157ServicoGrupo_Codigo ;
      private String[] H006Z7_A158ServicoGrupo_Descricao ;
      private bool[] H006Z8_A159ServicoGrupo_Ativo ;
      private int[] H006Z8_A157ServicoGrupo_Codigo ;
      private bool[] H006Z8_A632Servico_Ativo ;
      private String[] H006Z8_A156Servico_Descricao ;
      private bool[] H006Z8_n156Servico_Descricao ;
      private String[] H006Z8_A158ServicoGrupo_Descricao ;
      private String[] H006Z8_A605Servico_Sigla ;
      private String[] H006Z8_A608Servico_Nome ;
      private int[] H006Z8_A155Servico_Codigo ;
      private long[] H006Z9_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV43Servico_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV47Servico_SiglaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV51ServicoGrupo_DescricaoTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV55DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptservico__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H006Z8( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             String AV33Servico_Sigla1 ,
                                             String AV34Servico_Nome1 ,
                                             int AV35ServicoGrupo_Codigo1 ,
                                             bool AV19DynamicFiltersEnabled2 ,
                                             String AV20DynamicFiltersSelector2 ,
                                             String AV36Servico_Sigla2 ,
                                             String AV37Servico_Nome2 ,
                                             int AV38ServicoGrupo_Codigo2 ,
                                             bool AV24DynamicFiltersEnabled3 ,
                                             String AV25DynamicFiltersSelector3 ,
                                             String AV39Servico_Sigla3 ,
                                             String AV40Servico_Nome3 ,
                                             int AV41ServicoGrupo_Codigo3 ,
                                             String AV45TFServico_Nome_Sel ,
                                             String AV44TFServico_Nome ,
                                             String AV49TFServico_Sigla_Sel ,
                                             String AV48TFServico_Sigla ,
                                             String AV53TFServicoGrupo_Descricao_Sel ,
                                             String AV52TFServicoGrupo_Descricao ,
                                             String A605Servico_Sigla ,
                                             String A608Servico_Nome ,
                                             int A157ServicoGrupo_Codigo ,
                                             bool A159ServicoGrupo_Ativo ,
                                             String A158ServicoGrupo_Descricao ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             bool A632Servico_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [20] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T2.[ServicoGrupo_Ativo], T1.[ServicoGrupo_Codigo], T1.[Servico_Ativo], T1.[Servico_Descricao], T2.[ServicoGrupo_Descricao], T1.[Servico_Sigla], T1.[Servico_Nome], T1.[Servico_Codigo]";
         sFromString = " FROM ([Servico] T1 WITH (NOLOCK) INNER JOIN [ServicoGrupo] T2 WITH (NOLOCK) ON T2.[ServicoGrupo_Codigo] = T1.[ServicoGrupo_Codigo])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[Servico_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33Servico_Sigla1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Sigla] = @AV33Servico_Sigla1)";
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34Servico_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Nome] like '%' + @lV34Servico_Nome1 + '%')";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICOGRUPO_CODIGO") == 0 ) && ( ! (0==AV35ServicoGrupo_Codigo1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ServicoGrupo_Codigo] = @AV35ServicoGrupo_Codigo1)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICOGRUPO_CODIGO") == 0 )
         {
            sWhereString = sWhereString + " and (T2.[ServicoGrupo_Ativo] = 1)";
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SERVICO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36Servico_Sigla2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Sigla] = @AV36Servico_Sigla2)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37Servico_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Nome] like '%' + @lV37Servico_Nome2 + '%')";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SERVICOGRUPO_CODIGO") == 0 ) && ( ! (0==AV38ServicoGrupo_Codigo2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ServicoGrupo_Codigo] = @AV38ServicoGrupo_Codigo2)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SERVICOGRUPO_CODIGO") == 0 ) )
         {
            sWhereString = sWhereString + " and (T2.[ServicoGrupo_Ativo] = 1)";
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "SERVICO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39Servico_Sigla3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Sigla] = @AV39Servico_Sigla3)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40Servico_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Nome] like '%' + @lV40Servico_Nome3 + '%')";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "SERVICOGRUPO_CODIGO") == 0 ) && ( ! (0==AV41ServicoGrupo_Codigo3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ServicoGrupo_Codigo] = @AV41ServicoGrupo_Codigo3)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "SERVICOGRUPO_CODIGO") == 0 ) )
         {
            sWhereString = sWhereString + " and (T2.[ServicoGrupo_Ativo] = 1)";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV45TFServico_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFServico_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Nome] like @lV44TFServico_Nome)";
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TFServico_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Nome] = @AV45TFServico_Nome_Sel)";
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV49TFServico_Sigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFServico_Sigla)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Sigla] like @lV48TFServico_Sigla)";
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFServico_Sigla_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Sigla] = @AV49TFServico_Sigla_Sel)";
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV53TFServicoGrupo_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFServicoGrupo_Descricao)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ServicoGrupo_Descricao] like @lV52TFServicoGrupo_Descricao)";
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53TFServicoGrupo_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ServicoGrupo_Descricao] = @AV53TFServicoGrupo_Descricao_Sel)";
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Sigla]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Sigla] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Nome]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[ServicoGrupo_Descricao]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[ServicoGrupo_Descricao] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H006Z9( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             String AV33Servico_Sigla1 ,
                                             String AV34Servico_Nome1 ,
                                             int AV35ServicoGrupo_Codigo1 ,
                                             bool AV19DynamicFiltersEnabled2 ,
                                             String AV20DynamicFiltersSelector2 ,
                                             String AV36Servico_Sigla2 ,
                                             String AV37Servico_Nome2 ,
                                             int AV38ServicoGrupo_Codigo2 ,
                                             bool AV24DynamicFiltersEnabled3 ,
                                             String AV25DynamicFiltersSelector3 ,
                                             String AV39Servico_Sigla3 ,
                                             String AV40Servico_Nome3 ,
                                             int AV41ServicoGrupo_Codigo3 ,
                                             String AV45TFServico_Nome_Sel ,
                                             String AV44TFServico_Nome ,
                                             String AV49TFServico_Sigla_Sel ,
                                             String AV48TFServico_Sigla ,
                                             String AV53TFServicoGrupo_Descricao_Sel ,
                                             String AV52TFServicoGrupo_Descricao ,
                                             String A605Servico_Sigla ,
                                             String A608Servico_Nome ,
                                             int A157ServicoGrupo_Codigo ,
                                             bool A159ServicoGrupo_Ativo ,
                                             String A158ServicoGrupo_Descricao ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             bool A632Servico_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [15] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([Servico] T1 WITH (NOLOCK) INNER JOIN [ServicoGrupo] T2 WITH (NOLOCK) ON T2.[ServicoGrupo_Codigo] = T1.[ServicoGrupo_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[Servico_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33Servico_Sigla1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Sigla] = @AV33Servico_Sigla1)";
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34Servico_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Nome] like '%' + @lV34Servico_Nome1 + '%')";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICOGRUPO_CODIGO") == 0 ) && ( ! (0==AV35ServicoGrupo_Codigo1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ServicoGrupo_Codigo] = @AV35ServicoGrupo_Codigo1)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICOGRUPO_CODIGO") == 0 )
         {
            sWhereString = sWhereString + " and (T2.[ServicoGrupo_Ativo] = 1)";
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SERVICO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36Servico_Sigla2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Sigla] = @AV36Servico_Sigla2)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37Servico_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Nome] like '%' + @lV37Servico_Nome2 + '%')";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SERVICOGRUPO_CODIGO") == 0 ) && ( ! (0==AV38ServicoGrupo_Codigo2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ServicoGrupo_Codigo] = @AV38ServicoGrupo_Codigo2)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SERVICOGRUPO_CODIGO") == 0 ) )
         {
            sWhereString = sWhereString + " and (T2.[ServicoGrupo_Ativo] = 1)";
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "SERVICO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39Servico_Sigla3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Sigla] = @AV39Servico_Sigla3)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40Servico_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Nome] like '%' + @lV40Servico_Nome3 + '%')";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "SERVICOGRUPO_CODIGO") == 0 ) && ( ! (0==AV41ServicoGrupo_Codigo3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ServicoGrupo_Codigo] = @AV41ServicoGrupo_Codigo3)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "SERVICOGRUPO_CODIGO") == 0 ) )
         {
            sWhereString = sWhereString + " and (T2.[ServicoGrupo_Ativo] = 1)";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV45TFServico_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFServico_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Nome] like @lV44TFServico_Nome)";
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TFServico_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Nome] = @AV45TFServico_Nome_Sel)";
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV49TFServico_Sigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFServico_Sigla)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Sigla] like @lV48TFServico_Sigla)";
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFServico_Sigla_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Sigla] = @AV49TFServico_Sigla_Sel)";
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV53TFServicoGrupo_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFServicoGrupo_Descricao)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ServicoGrupo_Descricao] like @lV52TFServicoGrupo_Descricao)";
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53TFServicoGrupo_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ServicoGrupo_Descricao] = @AV53TFServicoGrupo_Descricao_Sel)";
         }
         else
         {
            GXv_int4[14] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 6 :
                     return conditional_H006Z8(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (int)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (bool)dynConstraints[23] , (String)dynConstraints[24] , (short)dynConstraints[25] , (bool)dynConstraints[26] , (bool)dynConstraints[27] );
               case 7 :
                     return conditional_H006Z9(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (int)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (bool)dynConstraints[23] , (String)dynConstraints[24] , (short)dynConstraints[25] , (bool)dynConstraints[26] , (bool)dynConstraints[27] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH006Z2 ;
          prmH006Z2 = new Object[] {
          } ;
          Object[] prmH006Z3 ;
          prmH006Z3 = new Object[] {
          } ;
          Object[] prmH006Z4 ;
          prmH006Z4 = new Object[] {
          } ;
          Object[] prmH006Z5 ;
          prmH006Z5 = new Object[] {
          } ;
          Object[] prmH006Z6 ;
          prmH006Z6 = new Object[] {
          } ;
          Object[] prmH006Z7 ;
          prmH006Z7 = new Object[] {
          } ;
          Object[] prmH006Z8 ;
          prmH006Z8 = new Object[] {
          new Object[] {"@AV33Servico_Sigla1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV34Servico_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV35ServicoGrupo_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV36Servico_Sigla2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV37Servico_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV38ServicoGrupo_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV39Servico_Sigla3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV40Servico_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV41ServicoGrupo_Codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV44TFServico_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV45TFServico_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV48TFServico_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV49TFServico_Sigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV52TFServicoGrupo_Descricao",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV53TFServicoGrupo_Descricao_Sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH006Z9 ;
          prmH006Z9 = new Object[] {
          new Object[] {"@AV33Servico_Sigla1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV34Servico_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV35ServicoGrupo_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV36Servico_Sigla2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV37Servico_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV38ServicoGrupo_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV39Servico_Sigla3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV40Servico_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV41ServicoGrupo_Codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV44TFServico_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV45TFServico_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV48TFServico_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV49TFServico_Sigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV52TFServicoGrupo_Descricao",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV53TFServicoGrupo_Descricao_Sel",SqlDbType.VarChar,100,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H006Z2", "SELECT [ServicoGrupo_Codigo], [ServicoGrupo_Descricao] FROM [ServicoGrupo] WITH (NOLOCK) ORDER BY [ServicoGrupo_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006Z2,0,0,true,false )
             ,new CursorDef("H006Z3", "SELECT [ServicoGrupo_Codigo], [ServicoGrupo_Descricao] FROM [ServicoGrupo] WITH (NOLOCK) ORDER BY [ServicoGrupo_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006Z3,0,0,true,false )
             ,new CursorDef("H006Z4", "SELECT [ServicoGrupo_Codigo], [ServicoGrupo_Descricao] FROM [ServicoGrupo] WITH (NOLOCK) ORDER BY [ServicoGrupo_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006Z4,0,0,true,false )
             ,new CursorDef("H006Z5", "SELECT [ServicoGrupo_Codigo], [ServicoGrupo_Descricao] FROM [ServicoGrupo] WITH (NOLOCK) ORDER BY [ServicoGrupo_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006Z5,0,0,true,false )
             ,new CursorDef("H006Z6", "SELECT [ServicoGrupo_Codigo], [ServicoGrupo_Descricao] FROM [ServicoGrupo] WITH (NOLOCK) ORDER BY [ServicoGrupo_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006Z6,0,0,true,false )
             ,new CursorDef("H006Z7", "SELECT [ServicoGrupo_Codigo], [ServicoGrupo_Descricao] FROM [ServicoGrupo] WITH (NOLOCK) ORDER BY [ServicoGrupo_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006Z7,0,0,true,false )
             ,new CursorDef("H006Z8", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006Z8,11,0,true,false )
             ,new CursorDef("H006Z9", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006Z9,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 6 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 15) ;
                ((String[]) buf[7])[0] = rslt.getString(7, 50) ;
                ((int[]) buf[8])[0] = rslt.getInt(8) ;
                return;
             case 7 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 6 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                return;
             case 7 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                return;
       }
    }

 }

}
