/*
               File: PRC_EmailReplace_Novo_Usuario
        Description: PRC_Email Replace_Novo_Usuario
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:0.81
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class aprc_emailreplace_novo_usuario : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            if ( ! entryPointCalled )
            {
               AV8Email_Instancia_Guid = (Guid)(StringUtil.StrToGuid( gxfirstwebparm));
            }
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public aprc_emailreplace_novo_usuario( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public aprc_emailreplace_novo_usuario( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref Guid aP0_Email_Instancia_Guid )
      {
         this.AV8Email_Instancia_Guid = aP0_Email_Instancia_Guid;
         initialize();
         executePrivate();
         aP0_Email_Instancia_Guid=this.AV8Email_Instancia_Guid;
      }

      public Guid executeUdp( )
      {
         this.AV8Email_Instancia_Guid = aP0_Email_Instancia_Guid;
         initialize();
         executePrivate();
         aP0_Email_Instancia_Guid=this.AV8Email_Instancia_Guid;
         return AV8Email_Instancia_Guid ;
      }

      public void executeSubmit( ref Guid aP0_Email_Instancia_Guid )
      {
         aprc_emailreplace_novo_usuario objaprc_emailreplace_novo_usuario;
         objaprc_emailreplace_novo_usuario = new aprc_emailreplace_novo_usuario();
         objaprc_emailreplace_novo_usuario.AV8Email_Instancia_Guid = aP0_Email_Instancia_Guid;
         objaprc_emailreplace_novo_usuario.context.SetSubmitInitialConfig(context);
         objaprc_emailreplace_novo_usuario.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objaprc_emailreplace_novo_usuario);
         aP0_Email_Instancia_Guid=this.AV8Email_Instancia_Guid;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((aprc_emailreplace_novo_usuario)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00CU2 */
         pr_default.execute(0, new Object[] {AV8Email_Instancia_Guid});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1666Email_Instancia_Guid = (Guid)((Guid)(P00CU2_A1666Email_Instancia_Guid[0]));
            A1674Email_Instancia_Corpo = P00CU2_A1674Email_Instancia_Corpo[0];
            A1673Email_Instancia_Titulo = P00CU2_A1673Email_Instancia_Titulo[0];
            AV10i = 1;
            while ( AV10i <= AV9SDT_ID_Valor.Count )
            {
               A1674Email_Instancia_Corpo = StringUtil.StringReplace( A1674Email_Instancia_Corpo, ((SdtSDT_ID_Valor)AV9SDT_ID_Valor.Item(AV10i)).gxTpr_Id, ((SdtSDT_ID_Valor)AV9SDT_ID_Valor.Item(AV10i)).gxTpr_Valor);
               A1673Email_Instancia_Titulo = StringUtil.StringReplace( A1673Email_Instancia_Titulo, ((SdtSDT_ID_Valor)AV9SDT_ID_Valor.Item(AV10i)).gxTpr_Id, ((SdtSDT_ID_Valor)AV9SDT_ID_Valor.Item(AV10i)).gxTpr_Valor);
               AV10i = (short)(AV10i+1);
            }
            /* Using cursor P00CU3 */
            pr_default.execute(1, new Object[] {A1674Email_Instancia_Corpo, A1673Email_Instancia_Titulo, A1666Email_Instancia_Guid});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("Email_Instancia") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_EmailReplace_Novo_Usuario");
         CloseOpenCursors();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         scmdbuf = "";
         P00CU2_A1666Email_Instancia_Guid = new Guid[] {System.Guid.Empty} ;
         P00CU2_A1674Email_Instancia_Corpo = new String[] {""} ;
         P00CU2_A1673Email_Instancia_Titulo = new String[] {""} ;
         A1666Email_Instancia_Guid = (Guid)(System.Guid.Empty);
         A1674Email_Instancia_Corpo = "";
         A1673Email_Instancia_Titulo = "";
         AV9SDT_ID_Valor = new GxObjectCollection( context, "SDT_ID_Valor", "GxEv3Up14_Meetrika", "SdtSDT_ID_Valor", "GeneXus.Programs");
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.aprc_emailreplace_novo_usuario__default(),
            new Object[][] {
                new Object[] {
               P00CU2_A1666Email_Instancia_Guid, P00CU2_A1674Email_Instancia_Corpo, P00CU2_A1673Email_Instancia_Titulo
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short GxWebError ;
      private short AV10i ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String scmdbuf ;
      private bool entryPointCalled ;
      private String A1674Email_Instancia_Corpo ;
      private String A1673Email_Instancia_Titulo ;
      private Guid AV8Email_Instancia_Guid ;
      private Guid A1666Email_Instancia_Guid ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private Guid aP0_Email_Instancia_Guid ;
      private IDataStoreProvider pr_default ;
      private Guid[] P00CU2_A1666Email_Instancia_Guid ;
      private String[] P00CU2_A1674Email_Instancia_Corpo ;
      private String[] P00CU2_A1673Email_Instancia_Titulo ;
      [ObjectCollection(ItemType=typeof( SdtSDT_ID_Valor ))]
      private IGxCollection AV9SDT_ID_Valor ;
   }

   public class aprc_emailreplace_novo_usuario__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00CU2 ;
          prmP00CU2 = new Object[] {
          new Object[] {"@AV8Email_Instancia_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmP00CU3 ;
          prmP00CU3 = new Object[] {
          new Object[] {"@Email_Instancia_Corpo",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Email_Instancia_Titulo",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Email_Instancia_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00CU2", "SELECT [Email_Instancia_Guid], [Email_Instancia_Corpo], [Email_Instancia_Titulo] FROM [Email_Instancia] WITH (UPDLOCK) WHERE [Email_Instancia_Guid] = @AV8Email_Instancia_Guid ORDER BY [Email_Instancia_Guid] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00CU2,1,0,true,true )
             ,new CursorDef("P00CU3", "UPDATE [Email_Instancia] SET [Email_Instancia_Corpo]=@Email_Instancia_Corpo, [Email_Instancia_Titulo]=@Email_Instancia_Titulo  WHERE [Email_Instancia_Guid] = @Email_Instancia_Guid", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00CU3)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (Guid)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (Guid)parms[2]);
                return;
       }
    }

 }

}
