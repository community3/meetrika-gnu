/*
               File: Tecnologia
        Description: Tecnologia
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:17:21.38
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class tecnologia : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Tecnologia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Tecnologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Tecnologia_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTECNOLOGIA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Tecnologia_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbTecnologia_TipoTecnologia.Name = "TECNOLOGIA_TIPOTECNOLOGIA";
         cmbTecnologia_TipoTecnologia.WebTags = "";
         cmbTecnologia_TipoTecnologia.addItem("", "(Nenhum)", 0);
         cmbTecnologia_TipoTecnologia.addItem("OS", "Sistema Operacional", 0);
         cmbTecnologia_TipoTecnologia.addItem("LNG", "Linguagem", 0);
         cmbTecnologia_TipoTecnologia.addItem("DBM", "Banco de Dados", 0);
         cmbTecnologia_TipoTecnologia.addItem("SFT", "Software", 0);
         cmbTecnologia_TipoTecnologia.addItem("SRV", "Servidor", 0);
         cmbTecnologia_TipoTecnologia.addItem("DSK", "Desktop", 0);
         cmbTecnologia_TipoTecnologia.addItem("NTB", "Notebook", 0);
         cmbTecnologia_TipoTecnologia.addItem("PRN", "Impresora", 0);
         cmbTecnologia_TipoTecnologia.addItem("HRD", "Hardware", 0);
         if ( cmbTecnologia_TipoTecnologia.ItemCount > 0 )
         {
            A355Tecnologia_TipoTecnologia = cmbTecnologia_TipoTecnologia.getValidValue(A355Tecnologia_TipoTecnologia);
            n355Tecnologia_TipoTecnologia = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A355Tecnologia_TipoTecnologia", A355Tecnologia_TipoTecnologia);
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Tecnologia", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtTecnologia_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public tecnologia( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public tecnologia( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Tecnologia_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Tecnologia_Codigo = aP1_Tecnologia_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbTecnologia_TipoTecnologia = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbTecnologia_TipoTecnologia.ItemCount > 0 )
         {
            A355Tecnologia_TipoTecnologia = cmbTecnologia_TipoTecnologia.getValidValue(A355Tecnologia_TipoTecnologia);
            n355Tecnologia_TipoTecnologia = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A355Tecnologia_TipoTecnologia", A355Tecnologia_TipoTecnologia);
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_0P26( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_0P26e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtTecnologia_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A131Tecnologia_Codigo), 6, 0, ",", "")), ((edtTecnologia_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A131Tecnologia_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A131Tecnologia_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTecnologia_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtTecnologia_Codigo_Visible, edtTecnologia_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Tecnologia.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_0P26( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_0P26( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_0P26e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_30_0P26( true) ;
         }
         return  ;
      }

      protected void wb_table3_30_0P26e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_0P26e( true) ;
         }
         else
         {
            wb_table1_2_0P26e( false) ;
         }
      }

      protected void wb_table3_30_0P26( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Tecnologia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Tecnologia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Tecnologia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_30_0P26e( true) ;
         }
         else
         {
            wb_table3_30_0P26e( false) ;
         }
      }

      protected void wb_table2_5_0P26( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_0P26( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_0P26e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_0P26e( true) ;
         }
         else
         {
            wb_table2_5_0P26e( false) ;
         }
      }

      protected void wb_table4_13_0P26( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktecnologia_nome_Internalname, "Nome", "", "", lblTextblocktecnologia_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Tecnologia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtTecnologia_Nome_Internalname, StringUtil.RTrim( A132Tecnologia_Nome), StringUtil.RTrim( context.localUtil.Format( A132Tecnologia_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTecnologia_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtTecnologia_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_Tecnologia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktecnologia_tipotecnologia_Internalname, "Tipo", "", "", lblTextblocktecnologia_tipotecnologia_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Tecnologia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbTecnologia_TipoTecnologia, cmbTecnologia_TipoTecnologia_Internalname, StringUtil.RTrim( A355Tecnologia_TipoTecnologia), 1, cmbTecnologia_TipoTecnologia_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbTecnologia_TipoTecnologia.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,25);\"", "", true, "HLP_Tecnologia.htm");
            cmbTecnologia_TipoTecnologia.CurrentValue = StringUtil.RTrim( A355Tecnologia_TipoTecnologia);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbTecnologia_TipoTecnologia_Internalname, "Values", (String)(cmbTecnologia_TipoTecnologia.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_0P26e( true) ;
         }
         else
         {
            wb_table4_13_0P26e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E110P2 */
         E110P2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A132Tecnologia_Nome = StringUtil.Upper( cgiGet( edtTecnologia_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A132Tecnologia_Nome", A132Tecnologia_Nome);
               cmbTecnologia_TipoTecnologia.CurrentValue = cgiGet( cmbTecnologia_TipoTecnologia_Internalname);
               A355Tecnologia_TipoTecnologia = cgiGet( cmbTecnologia_TipoTecnologia_Internalname);
               n355Tecnologia_TipoTecnologia = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A355Tecnologia_TipoTecnologia", A355Tecnologia_TipoTecnologia);
               n355Tecnologia_TipoTecnologia = (String.IsNullOrEmpty(StringUtil.RTrim( A355Tecnologia_TipoTecnologia)) ? true : false);
               A131Tecnologia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtTecnologia_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A131Tecnologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A131Tecnologia_Codigo), 6, 0)));
               /* Read saved values. */
               Z131Tecnologia_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z131Tecnologia_Codigo"), ",", "."));
               Z132Tecnologia_Nome = cgiGet( "Z132Tecnologia_Nome");
               Z355Tecnologia_TipoTecnologia = cgiGet( "Z355Tecnologia_TipoTecnologia");
               n355Tecnologia_TipoTecnologia = (String.IsNullOrEmpty(StringUtil.RTrim( A355Tecnologia_TipoTecnologia)) ? true : false);
               O132Tecnologia_Nome = cgiGet( "O132Tecnologia_Nome");
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               AV7Tecnologia_Codigo = (int)(context.localUtil.CToN( cgiGet( "vTECNOLOGIA_CODIGO"), ",", "."));
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Tecnologia";
               A131Tecnologia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtTecnologia_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A131Tecnologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A131Tecnologia_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A131Tecnologia_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A131Tecnologia_Codigo != Z131Tecnologia_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("tecnologia:[SecurityCheckFailed value for]"+"Tecnologia_Codigo:"+context.localUtil.Format( (decimal)(A131Tecnologia_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("tecnologia:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A131Tecnologia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A131Tecnologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A131Tecnologia_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode26 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode26;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound26 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_0P0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "TECNOLOGIA_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtTecnologia_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E110P2 */
                           E110P2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E120P2 */
                           E120P2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E120P2 */
            E120P2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll0P26( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes0P26( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_0P0( )
      {
         BeforeValidate0P26( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls0P26( ) ;
            }
            else
            {
               CheckExtendedTable0P26( ) ;
               CloseExtendedTableCursors0P26( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption0P0( )
      {
      }

      protected void E110P2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         edtTecnologia_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTecnologia_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTecnologia_Codigo_Visible), 5, 0)));
      }

      protected void E120P2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwtecnologia.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM0P26( short GX_JID )
      {
         if ( ( GX_JID == 6 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z132Tecnologia_Nome = T000P3_A132Tecnologia_Nome[0];
               Z355Tecnologia_TipoTecnologia = T000P3_A355Tecnologia_TipoTecnologia[0];
            }
            else
            {
               Z132Tecnologia_Nome = A132Tecnologia_Nome;
               Z355Tecnologia_TipoTecnologia = A355Tecnologia_TipoTecnologia;
            }
         }
         if ( GX_JID == -6 )
         {
            Z131Tecnologia_Codigo = A131Tecnologia_Codigo;
            Z132Tecnologia_Nome = A132Tecnologia_Nome;
            Z355Tecnologia_TipoTecnologia = A355Tecnologia_TipoTecnologia;
         }
      }

      protected void standaloneNotModal( )
      {
         edtTecnologia_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTecnologia_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTecnologia_Codigo_Enabled), 5, 0)));
         edtTecnologia_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTecnologia_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTecnologia_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7Tecnologia_Codigo) )
         {
            A131Tecnologia_Codigo = AV7Tecnologia_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A131Tecnologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A131Tecnologia_Codigo), 6, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
      }

      protected void Load0P26( )
      {
         /* Using cursor T000P4 */
         pr_default.execute(2, new Object[] {A131Tecnologia_Codigo});
         if ( (pr_default.getStatus(2) != 101) )
         {
            RcdFound26 = 1;
            A132Tecnologia_Nome = T000P4_A132Tecnologia_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A132Tecnologia_Nome", A132Tecnologia_Nome);
            A355Tecnologia_TipoTecnologia = T000P4_A355Tecnologia_TipoTecnologia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A355Tecnologia_TipoTecnologia", A355Tecnologia_TipoTecnologia);
            n355Tecnologia_TipoTecnologia = T000P4_n355Tecnologia_TipoTecnologia[0];
            ZM0P26( -6) ;
         }
         pr_default.close(2);
         OnLoadActions0P26( ) ;
      }

      protected void OnLoadActions0P26( )
      {
      }

      protected void CheckExtendedTable0P26( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A132Tecnologia_Nome)) )
         {
            GX_msglist.addItem("O Nome da Tecnologia � obrigat�rio!", 1, "TECNOLOGIA_NOME");
            AnyError = 1;
            GX_FocusControl = edtTecnologia_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A355Tecnologia_TipoTecnologia)) )
         {
            GX_msglist.addItem("Tipo de Tecnolog�a � obrigat�rio!", 1, "TECNOLOGIA_TIPOTECNOLOGIA");
            AnyError = 1;
            GX_FocusControl = cmbTecnologia_TipoTecnologia_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors0P26( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey0P26( )
      {
         /* Using cursor T000P5 */
         pr_default.execute(3, new Object[] {A131Tecnologia_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound26 = 1;
         }
         else
         {
            RcdFound26 = 0;
         }
         pr_default.close(3);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T000P3 */
         pr_default.execute(1, new Object[] {A131Tecnologia_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM0P26( 6) ;
            RcdFound26 = 1;
            A131Tecnologia_Codigo = T000P3_A131Tecnologia_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A131Tecnologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A131Tecnologia_Codigo), 6, 0)));
            A132Tecnologia_Nome = T000P3_A132Tecnologia_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A132Tecnologia_Nome", A132Tecnologia_Nome);
            A355Tecnologia_TipoTecnologia = T000P3_A355Tecnologia_TipoTecnologia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A355Tecnologia_TipoTecnologia", A355Tecnologia_TipoTecnologia);
            n355Tecnologia_TipoTecnologia = T000P3_n355Tecnologia_TipoTecnologia[0];
            O132Tecnologia_Nome = A132Tecnologia_Nome;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A132Tecnologia_Nome", A132Tecnologia_Nome);
            Z131Tecnologia_Codigo = A131Tecnologia_Codigo;
            sMode26 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load0P26( ) ;
            if ( AnyError == 1 )
            {
               RcdFound26 = 0;
               InitializeNonKey0P26( ) ;
            }
            Gx_mode = sMode26;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound26 = 0;
            InitializeNonKey0P26( ) ;
            sMode26 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode26;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey0P26( ) ;
         if ( RcdFound26 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound26 = 0;
         /* Using cursor T000P6 */
         pr_default.execute(4, new Object[] {A131Tecnologia_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            while ( (pr_default.getStatus(4) != 101) && ( ( T000P6_A131Tecnologia_Codigo[0] < A131Tecnologia_Codigo ) ) )
            {
               pr_default.readNext(4);
            }
            if ( (pr_default.getStatus(4) != 101) && ( ( T000P6_A131Tecnologia_Codigo[0] > A131Tecnologia_Codigo ) ) )
            {
               A131Tecnologia_Codigo = T000P6_A131Tecnologia_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A131Tecnologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A131Tecnologia_Codigo), 6, 0)));
               RcdFound26 = 1;
            }
         }
         pr_default.close(4);
      }

      protected void move_previous( )
      {
         RcdFound26 = 0;
         /* Using cursor T000P7 */
         pr_default.execute(5, new Object[] {A131Tecnologia_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            while ( (pr_default.getStatus(5) != 101) && ( ( T000P7_A131Tecnologia_Codigo[0] > A131Tecnologia_Codigo ) ) )
            {
               pr_default.readNext(5);
            }
            if ( (pr_default.getStatus(5) != 101) && ( ( T000P7_A131Tecnologia_Codigo[0] < A131Tecnologia_Codigo ) ) )
            {
               A131Tecnologia_Codigo = T000P7_A131Tecnologia_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A131Tecnologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A131Tecnologia_Codigo), 6, 0)));
               RcdFound26 = 1;
            }
         }
         pr_default.close(5);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey0P26( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtTecnologia_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert0P26( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound26 == 1 )
            {
               if ( A131Tecnologia_Codigo != Z131Tecnologia_Codigo )
               {
                  A131Tecnologia_Codigo = Z131Tecnologia_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A131Tecnologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A131Tecnologia_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "TECNOLOGIA_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtTecnologia_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtTecnologia_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update0P26( ) ;
                  GX_FocusControl = edtTecnologia_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A131Tecnologia_Codigo != Z131Tecnologia_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtTecnologia_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert0P26( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "TECNOLOGIA_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtTecnologia_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtTecnologia_Nome_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert0P26( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A131Tecnologia_Codigo != Z131Tecnologia_Codigo )
         {
            A131Tecnologia_Codigo = Z131Tecnologia_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A131Tecnologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A131Tecnologia_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "TECNOLOGIA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtTecnologia_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtTecnologia_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency0P26( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T000P2 */
            pr_default.execute(0, new Object[] {A131Tecnologia_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Tecnologia"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z132Tecnologia_Nome, T000P2_A132Tecnologia_Nome[0]) != 0 ) || ( StringUtil.StrCmp(Z355Tecnologia_TipoTecnologia, T000P2_A355Tecnologia_TipoTecnologia[0]) != 0 ) )
            {
               if ( StringUtil.StrCmp(Z132Tecnologia_Nome, T000P2_A132Tecnologia_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("tecnologia:[seudo value changed for attri]"+"Tecnologia_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z132Tecnologia_Nome);
                  GXUtil.WriteLogRaw("Current: ",T000P2_A132Tecnologia_Nome[0]);
               }
               if ( StringUtil.StrCmp(Z355Tecnologia_TipoTecnologia, T000P2_A355Tecnologia_TipoTecnologia[0]) != 0 )
               {
                  GXUtil.WriteLog("tecnologia:[seudo value changed for attri]"+"Tecnologia_TipoTecnologia");
                  GXUtil.WriteLogRaw("Old: ",Z355Tecnologia_TipoTecnologia);
                  GXUtil.WriteLogRaw("Current: ",T000P2_A355Tecnologia_TipoTecnologia[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Tecnologia"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0P26( )
      {
         BeforeValidate0P26( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0P26( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0P26( 0) ;
            CheckOptimisticConcurrency0P26( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0P26( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0P26( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000P8 */
                     pr_default.execute(6, new Object[] {A132Tecnologia_Nome, n355Tecnologia_TipoTecnologia, A355Tecnologia_TipoTecnologia});
                     A131Tecnologia_Codigo = T000P8_A131Tecnologia_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A131Tecnologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A131Tecnologia_Codigo), 6, 0)));
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("Tecnologia") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption0P0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0P26( ) ;
            }
            EndLevel0P26( ) ;
         }
         CloseExtendedTableCursors0P26( ) ;
      }

      protected void Update0P26( )
      {
         BeforeValidate0P26( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0P26( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0P26( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0P26( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0P26( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000P9 */
                     pr_default.execute(7, new Object[] {A132Tecnologia_Nome, n355Tecnologia_TipoTecnologia, A355Tecnologia_TipoTecnologia, A131Tecnologia_Codigo});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("Tecnologia") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Tecnologia"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0P26( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0P26( ) ;
         }
         CloseExtendedTableCursors0P26( ) ;
      }

      protected void DeferredUpdate0P26( )
      {
      }

      protected void delete( )
      {
         BeforeValidate0P26( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0P26( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0P26( ) ;
            AfterConfirm0P26( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0P26( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000P10 */
                  pr_default.execute(8, new Object[] {A131Tecnologia_Codigo});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("Tecnologia") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode26 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel0P26( ) ;
         Gx_mode = sMode26;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls0P26( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
         if ( AnyError == 0 )
         {
            /* Using cursor T000P11 */
            pr_default.execute(9, new Object[] {A131Tecnologia_Codigo});
            if ( (pr_default.getStatus(9) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Tecnologia"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(9);
            /* Using cursor T000P12 */
            pr_default.execute(10, new Object[] {A131Tecnologia_Codigo});
            if ( (pr_default.getStatus(10) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Ambiente Tecnologico Tecnologias"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(10);
         }
      }

      protected void EndLevel0P26( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0P26( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "Tecnologia");
            if ( AnyError == 0 )
            {
               ConfirmValues0P0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "Tecnologia");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart0P26( )
      {
         /* Scan By routine */
         /* Using cursor T000P13 */
         pr_default.execute(11);
         RcdFound26 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound26 = 1;
            A131Tecnologia_Codigo = T000P13_A131Tecnologia_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A131Tecnologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A131Tecnologia_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext0P26( )
      {
         /* Scan next routine */
         pr_default.readNext(11);
         RcdFound26 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound26 = 1;
            A131Tecnologia_Codigo = T000P13_A131Tecnologia_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A131Tecnologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A131Tecnologia_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd0P26( )
      {
         pr_default.close(11);
      }

      protected void AfterConfirm0P26( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0P26( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0P26( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0P26( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0P26( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0P26( )
      {
         /* Before Validate Rules */
         if ( ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  || ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  ) && ( StringUtil.StrCmp(A132Tecnologia_Nome, O132Tecnologia_Nome) != 0 ) && new prc_existenomecadastrado(context).executeUdp(  "Tec",  A132Tecnologia_Nome) )
         {
            GX_msglist.addItem("Esse nome j� foi cadastrado!", 1, "TECNOLOGIA_NOME");
            AnyError = 1;
            GX_FocusControl = edtTecnologia_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void DisableAttributes0P26( )
      {
         edtTecnologia_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTecnologia_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTecnologia_Nome_Enabled), 5, 0)));
         cmbTecnologia_TipoTecnologia.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbTecnologia_TipoTecnologia_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbTecnologia_TipoTecnologia.Enabled), 5, 0)));
         edtTecnologia_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTecnologia_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTecnologia_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues0P0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117172234");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("tecnologia.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Tecnologia_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z131Tecnologia_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z131Tecnologia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z132Tecnologia_Nome", StringUtil.RTrim( Z132Tecnologia_Nome));
         GxWebStd.gx_hidden_field( context, "Z355Tecnologia_TipoTecnologia", StringUtil.RTrim( Z355Tecnologia_TipoTecnologia));
         GxWebStd.gx_hidden_field( context, "O132Tecnologia_Nome", StringUtil.RTrim( O132Tecnologia_Nome));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vTECNOLOGIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Tecnologia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vTECNOLOGIA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Tecnologia_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Tecnologia";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A131Tecnologia_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("tecnologia:[SendSecurityCheck value for]"+"Tecnologia_Codigo:"+context.localUtil.Format( (decimal)(A131Tecnologia_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("tecnologia:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("tecnologia.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Tecnologia_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "Tecnologia" ;
      }

      public override String GetPgmdesc( )
      {
         return "Tecnologia" ;
      }

      protected void InitializeNonKey0P26( )
      {
         A132Tecnologia_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A132Tecnologia_Nome", A132Tecnologia_Nome);
         A355Tecnologia_TipoTecnologia = "";
         n355Tecnologia_TipoTecnologia = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A355Tecnologia_TipoTecnologia", A355Tecnologia_TipoTecnologia);
         n355Tecnologia_TipoTecnologia = (String.IsNullOrEmpty(StringUtil.RTrim( A355Tecnologia_TipoTecnologia)) ? true : false);
         O132Tecnologia_Nome = A132Tecnologia_Nome;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A132Tecnologia_Nome", A132Tecnologia_Nome);
         Z132Tecnologia_Nome = "";
         Z355Tecnologia_TipoTecnologia = "";
      }

      protected void InitAll0P26( )
      {
         A131Tecnologia_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A131Tecnologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A131Tecnologia_Codigo), 6, 0)));
         InitializeNonKey0P26( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117172256");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("tecnologia.js", "?20203117172256");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblocktecnologia_nome_Internalname = "TEXTBLOCKTECNOLOGIA_NOME";
         edtTecnologia_Nome_Internalname = "TECNOLOGIA_NOME";
         lblTextblocktecnologia_tipotecnologia_Internalname = "TEXTBLOCKTECNOLOGIA_TIPOTECNOLOGIA";
         cmbTecnologia_TipoTecnologia_Internalname = "TECNOLOGIA_TIPOTECNOLOGIA";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtTecnologia_Codigo_Internalname = "TECNOLOGIA_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Tecnologia";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Tecnologia";
         cmbTecnologia_TipoTecnologia_Jsonclick = "";
         cmbTecnologia_TipoTecnologia.Enabled = 1;
         edtTecnologia_Nome_Jsonclick = "";
         edtTecnologia_Nome_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtTecnologia_Codigo_Jsonclick = "";
         edtTecnologia_Codigo_Enabled = 0;
         edtTecnologia_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7Tecnologia_Codigo',fld:'vTECNOLOGIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E120P2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z132Tecnologia_Nome = "";
         Z355Tecnologia_TipoTecnologia = "";
         O132Tecnologia_Nome = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         A355Tecnologia_TipoTecnologia = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblocktecnologia_nome_Jsonclick = "";
         A132Tecnologia_Nome = "";
         lblTextblocktecnologia_tipotecnologia_Jsonclick = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode26 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         T000P4_A131Tecnologia_Codigo = new int[1] ;
         T000P4_A132Tecnologia_Nome = new String[] {""} ;
         T000P4_A355Tecnologia_TipoTecnologia = new String[] {""} ;
         T000P4_n355Tecnologia_TipoTecnologia = new bool[] {false} ;
         T000P5_A131Tecnologia_Codigo = new int[1] ;
         T000P3_A131Tecnologia_Codigo = new int[1] ;
         T000P3_A132Tecnologia_Nome = new String[] {""} ;
         T000P3_A355Tecnologia_TipoTecnologia = new String[] {""} ;
         T000P3_n355Tecnologia_TipoTecnologia = new bool[] {false} ;
         T000P6_A131Tecnologia_Codigo = new int[1] ;
         T000P7_A131Tecnologia_Codigo = new int[1] ;
         T000P2_A131Tecnologia_Codigo = new int[1] ;
         T000P2_A132Tecnologia_Nome = new String[] {""} ;
         T000P2_A355Tecnologia_TipoTecnologia = new String[] {""} ;
         T000P2_n355Tecnologia_TipoTecnologia = new bool[] {false} ;
         T000P8_A131Tecnologia_Codigo = new int[1] ;
         T000P11_A127Sistema_Codigo = new int[1] ;
         T000P11_A2128SistemaTecnologiaLinha = new short[1] ;
         T000P12_A351AmbienteTecnologico_Codigo = new int[1] ;
         T000P12_A131Tecnologia_Codigo = new int[1] ;
         T000P13_A131Tecnologia_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.tecnologia__default(),
            new Object[][] {
                new Object[] {
               T000P2_A131Tecnologia_Codigo, T000P2_A132Tecnologia_Nome, T000P2_A355Tecnologia_TipoTecnologia, T000P2_n355Tecnologia_TipoTecnologia
               }
               , new Object[] {
               T000P3_A131Tecnologia_Codigo, T000P3_A132Tecnologia_Nome, T000P3_A355Tecnologia_TipoTecnologia, T000P3_n355Tecnologia_TipoTecnologia
               }
               , new Object[] {
               T000P4_A131Tecnologia_Codigo, T000P4_A132Tecnologia_Nome, T000P4_A355Tecnologia_TipoTecnologia, T000P4_n355Tecnologia_TipoTecnologia
               }
               , new Object[] {
               T000P5_A131Tecnologia_Codigo
               }
               , new Object[] {
               T000P6_A131Tecnologia_Codigo
               }
               , new Object[] {
               T000P7_A131Tecnologia_Codigo
               }
               , new Object[] {
               T000P8_A131Tecnologia_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000P11_A127Sistema_Codigo, T000P11_A2128SistemaTecnologiaLinha
               }
               , new Object[] {
               T000P12_A351AmbienteTecnologico_Codigo, T000P12_A131Tecnologia_Codigo
               }
               , new Object[] {
               T000P13_A131Tecnologia_Codigo
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short RcdFound26 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private int wcpOAV7Tecnologia_Codigo ;
      private int Z131Tecnologia_Codigo ;
      private int AV7Tecnologia_Codigo ;
      private int trnEnded ;
      private int A131Tecnologia_Codigo ;
      private int edtTecnologia_Codigo_Enabled ;
      private int edtTecnologia_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtTecnologia_Nome_Enabled ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z132Tecnologia_Nome ;
      private String Z355Tecnologia_TipoTecnologia ;
      private String O132Tecnologia_Nome ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String A355Tecnologia_TipoTecnologia ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtTecnologia_Nome_Internalname ;
      private String edtTecnologia_Codigo_Internalname ;
      private String edtTecnologia_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblocktecnologia_nome_Internalname ;
      private String lblTextblocktecnologia_nome_Jsonclick ;
      private String A132Tecnologia_Nome ;
      private String edtTecnologia_Nome_Jsonclick ;
      private String lblTextblocktecnologia_tipotecnologia_Internalname ;
      private String lblTextblocktecnologia_tipotecnologia_Jsonclick ;
      private String cmbTecnologia_TipoTecnologia_Internalname ;
      private String cmbTecnologia_TipoTecnologia_Jsonclick ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode26 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool n355Tecnologia_TipoTecnologia ;
      private bool wbErr ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private IGxSession AV10WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbTecnologia_TipoTecnologia ;
      private IDataStoreProvider pr_default ;
      private int[] T000P4_A131Tecnologia_Codigo ;
      private String[] T000P4_A132Tecnologia_Nome ;
      private String[] T000P4_A355Tecnologia_TipoTecnologia ;
      private bool[] T000P4_n355Tecnologia_TipoTecnologia ;
      private int[] T000P5_A131Tecnologia_Codigo ;
      private int[] T000P3_A131Tecnologia_Codigo ;
      private String[] T000P3_A132Tecnologia_Nome ;
      private String[] T000P3_A355Tecnologia_TipoTecnologia ;
      private bool[] T000P3_n355Tecnologia_TipoTecnologia ;
      private int[] T000P6_A131Tecnologia_Codigo ;
      private int[] T000P7_A131Tecnologia_Codigo ;
      private int[] T000P2_A131Tecnologia_Codigo ;
      private String[] T000P2_A132Tecnologia_Nome ;
      private String[] T000P2_A355Tecnologia_TipoTecnologia ;
      private bool[] T000P2_n355Tecnologia_TipoTecnologia ;
      private int[] T000P8_A131Tecnologia_Codigo ;
      private int[] T000P11_A127Sistema_Codigo ;
      private short[] T000P11_A2128SistemaTecnologiaLinha ;
      private int[] T000P12_A351AmbienteTecnologico_Codigo ;
      private int[] T000P12_A131Tecnologia_Codigo ;
      private int[] T000P13_A131Tecnologia_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class tecnologia__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT000P4 ;
          prmT000P4 = new Object[] {
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000P5 ;
          prmT000P5 = new Object[] {
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000P3 ;
          prmT000P3 = new Object[] {
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000P6 ;
          prmT000P6 = new Object[] {
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000P7 ;
          prmT000P7 = new Object[] {
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000P2 ;
          prmT000P2 = new Object[] {
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000P8 ;
          prmT000P8 = new Object[] {
          new Object[] {"@Tecnologia_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Tecnologia_TipoTecnologia",SqlDbType.Char,3,0}
          } ;
          Object[] prmT000P9 ;
          prmT000P9 = new Object[] {
          new Object[] {"@Tecnologia_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Tecnologia_TipoTecnologia",SqlDbType.Char,3,0} ,
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000P10 ;
          prmT000P10 = new Object[] {
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000P11 ;
          prmT000P11 = new Object[] {
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000P12 ;
          prmT000P12 = new Object[] {
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000P13 ;
          prmT000P13 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("T000P2", "SELECT [Tecnologia_Codigo], [Tecnologia_Nome], [Tecnologia_TipoTecnologia] FROM [Tecnologia] WITH (UPDLOCK) WHERE [Tecnologia_Codigo] = @Tecnologia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000P2,1,0,true,false )
             ,new CursorDef("T000P3", "SELECT [Tecnologia_Codigo], [Tecnologia_Nome], [Tecnologia_TipoTecnologia] FROM [Tecnologia] WITH (NOLOCK) WHERE [Tecnologia_Codigo] = @Tecnologia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000P3,1,0,true,false )
             ,new CursorDef("T000P4", "SELECT TM1.[Tecnologia_Codigo], TM1.[Tecnologia_Nome], TM1.[Tecnologia_TipoTecnologia] FROM [Tecnologia] TM1 WITH (NOLOCK) WHERE TM1.[Tecnologia_Codigo] = @Tecnologia_Codigo ORDER BY TM1.[Tecnologia_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000P4,100,0,true,false )
             ,new CursorDef("T000P5", "SELECT [Tecnologia_Codigo] FROM [Tecnologia] WITH (NOLOCK) WHERE [Tecnologia_Codigo] = @Tecnologia_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000P5,1,0,true,false )
             ,new CursorDef("T000P6", "SELECT TOP 1 [Tecnologia_Codigo] FROM [Tecnologia] WITH (NOLOCK) WHERE ( [Tecnologia_Codigo] > @Tecnologia_Codigo) ORDER BY [Tecnologia_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000P6,1,0,true,true )
             ,new CursorDef("T000P7", "SELECT TOP 1 [Tecnologia_Codigo] FROM [Tecnologia] WITH (NOLOCK) WHERE ( [Tecnologia_Codigo] < @Tecnologia_Codigo) ORDER BY [Tecnologia_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000P7,1,0,true,true )
             ,new CursorDef("T000P8", "INSERT INTO [Tecnologia]([Tecnologia_Nome], [Tecnologia_TipoTecnologia]) VALUES(@Tecnologia_Nome, @Tecnologia_TipoTecnologia); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000P8)
             ,new CursorDef("T000P9", "UPDATE [Tecnologia] SET [Tecnologia_Nome]=@Tecnologia_Nome, [Tecnologia_TipoTecnologia]=@Tecnologia_TipoTecnologia  WHERE [Tecnologia_Codigo] = @Tecnologia_Codigo", GxErrorMask.GX_NOMASK,prmT000P9)
             ,new CursorDef("T000P10", "DELETE FROM [Tecnologia]  WHERE [Tecnologia_Codigo] = @Tecnologia_Codigo", GxErrorMask.GX_NOMASK,prmT000P10)
             ,new CursorDef("T000P11", "SELECT TOP 1 [Sistema_Codigo], [SistemaTecnologiaLinha] FROM [SistemaTecnologia] WITH (NOLOCK) WHERE [Tecnologia_Codigo] = @Tecnologia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000P11,1,0,true,true )
             ,new CursorDef("T000P12", "SELECT TOP 1 [AmbienteTecnologico_Codigo], [Tecnologia_Codigo] FROM [AmbienteTecnologicoTecnologias] WITH (NOLOCK) WHERE [Tecnologia_Codigo] = @Tecnologia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000P12,1,0,true,true )
             ,new CursorDef("T000P13", "SELECT [Tecnologia_Codigo] FROM [Tecnologia] WITH (NOLOCK) ORDER BY [Tecnologia_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000P13,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameter(3, (int)parms[3]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
