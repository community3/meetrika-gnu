/*
               File: RequisitoGeneral
        Description: Requisito General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:30:10.55
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class requisitogeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public requisitogeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public requisitogeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Requisito_Codigo )
      {
         this.A1919Requisito_Codigo = aP0_Requisito_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         dynRequisito_TipoReqCod = new GXCombobox();
         cmbRequisito_Status = new GXCombobox();
         chkRequisito_Ativo = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A1919Requisito_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1919Requisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1919Requisito_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A1919Requisito_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"REQUISITO_TIPOREQCOD") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  GXDLAREQUISITO_TIPOREQCODQ22( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAQ22( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV14Pgmname = "RequisitoGeneral";
               context.Gx_err = 0;
               GXAREQUISITO_TIPOREQCOD_htmlQ22( ) ;
               WSQ22( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Requisito General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117301061");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("requisitogeneral.aspx") + "?" + UrlEncode("" +A1919Requisito_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA1919Requisito_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA1919Requisito_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_REQUISITO_ORDEM", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1931Requisito_Ordem), "ZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_REQUISITO_AGRUPADOR", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1926Requisito_Agrupador, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_REQUISITO_TIPOREQCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2049Requisito_TipoReqCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_REQUISITO_TITULO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1927Requisito_Titulo, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_REQUISITO_DESCRICAO", GetSecureSignedToken( sPrefix, A1923Requisito_Descricao));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_REQUISITO_RESTRICAO", GetSecureSignedToken( sPrefix, A1929Requisito_Restricao));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_REQUISITO_STATUS", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1934Requisito_Status), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_REQUISITO_PONTUACAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1932Requisito_Pontuacao, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_REQUISITO_ATIVO", GetSecureSignedToken( sPrefix, A1935Requisito_Ativo));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormQ22( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("requisitogeneral.js", "?20203117301064");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "RequisitoGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Requisito General" ;
      }

      protected void WBQ20( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "requisitogeneral.aspx");
            }
            wb_table1_2_Q22( true) ;
         }
         else
         {
            wb_table1_2_Q22( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_Q22e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtRequisito_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1919Requisito_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1919Requisito_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtRequisito_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtRequisito_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_RequisitoGeneral.htm");
         }
         wbLoad = true;
      }

      protected void STARTQ22( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Requisito General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPQ20( ) ;
            }
         }
      }

      protected void WSQ22( )
      {
         STARTQ22( ) ;
         EVTQ22( ) ;
      }

      protected void EVTQ22( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPQ20( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPQ20( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11Q22 */
                                    E11Q22 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPQ20( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12Q22 */
                                    E12Q22 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPQ20( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13Q22 */
                                    E13Q22 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPQ20( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14Q22 */
                                    E14Q22 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPQ20( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPQ20( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEQ22( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormQ22( ) ;
            }
         }
      }

      protected void PAQ22( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            dynRequisito_TipoReqCod.Name = "REQUISITO_TIPOREQCOD";
            dynRequisito_TipoReqCod.WebTags = "";
            cmbRequisito_Status.Name = "REQUISITO_STATUS";
            cmbRequisito_Status.WebTags = "";
            cmbRequisito_Status.addItem("0", "Rascunho", 0);
            cmbRequisito_Status.addItem("1", "Solicitado", 0);
            cmbRequisito_Status.addItem("2", "Aprovado", 0);
            cmbRequisito_Status.addItem("3", "N�o Aprovado", 0);
            cmbRequisito_Status.addItem("4", "Pausado", 0);
            cmbRequisito_Status.addItem("5", "Cancelado", 0);
            if ( cmbRequisito_Status.ItemCount > 0 )
            {
               A1934Requisito_Status = (short)(NumberUtil.Val( cmbRequisito_Status.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1934Requisito_Status), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1934Requisito_Status", StringUtil.LTrim( StringUtil.Str( (decimal)(A1934Requisito_Status), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REQUISITO_STATUS", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1934Requisito_Status), "ZZZ9")));
            }
            chkRequisito_Ativo.Name = "REQUISITO_ATIVO";
            chkRequisito_Ativo.WebTags = "";
            chkRequisito_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkRequisito_Ativo_Internalname, "TitleCaption", chkRequisito_Ativo.Caption);
            chkRequisito_Ativo.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLAREQUISITO_TIPOREQCODQ22( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAREQUISITO_TIPOREQCOD_dataQ22( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAREQUISITO_TIPOREQCOD_htmlQ22( )
      {
         int gxdynajaxvalue ;
         GXDLAREQUISITO_TIPOREQCOD_dataQ22( ) ;
         gxdynajaxindex = 1;
         dynRequisito_TipoReqCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynRequisito_TipoReqCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynRequisito_TipoReqCod.ItemCount > 0 )
         {
            A2049Requisito_TipoReqCod = (int)(NumberUtil.Val( dynRequisito_TipoReqCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2049Requisito_TipoReqCod), 6, 0))), "."));
            n2049Requisito_TipoReqCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2049Requisito_TipoReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2049Requisito_TipoReqCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REQUISITO_TIPOREQCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2049Requisito_TipoReqCod), "ZZZZZ9")));
         }
      }

      protected void GXDLAREQUISITO_TIPOREQCOD_dataQ22( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00Q22 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00Q22_A2049Requisito_TipoReqCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00Q22_A2042TipoRequisito_Identificador[0]);
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynRequisito_TipoReqCod.ItemCount > 0 )
         {
            A2049Requisito_TipoReqCod = (int)(NumberUtil.Val( dynRequisito_TipoReqCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2049Requisito_TipoReqCod), 6, 0))), "."));
            n2049Requisito_TipoReqCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2049Requisito_TipoReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2049Requisito_TipoReqCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REQUISITO_TIPOREQCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2049Requisito_TipoReqCod), "ZZZZZ9")));
         }
         if ( cmbRequisito_Status.ItemCount > 0 )
         {
            A1934Requisito_Status = (short)(NumberUtil.Val( cmbRequisito_Status.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1934Requisito_Status), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1934Requisito_Status", StringUtil.LTrim( StringUtil.Str( (decimal)(A1934Requisito_Status), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REQUISITO_STATUS", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1934Requisito_Status), "ZZZ9")));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFQ22( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV14Pgmname = "RequisitoGeneral";
         context.Gx_err = 0;
      }

      protected void RFQ22( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00Q23 */
            pr_default.execute(1, new Object[] {A1919Requisito_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A1935Requisito_Ativo = H00Q23_A1935Requisito_Ativo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1935Requisito_Ativo", A1935Requisito_Ativo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REQUISITO_ATIVO", GetSecureSignedToken( sPrefix, A1935Requisito_Ativo));
               A1932Requisito_Pontuacao = H00Q23_A1932Requisito_Pontuacao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1932Requisito_Pontuacao", StringUtil.LTrim( StringUtil.Str( A1932Requisito_Pontuacao, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REQUISITO_PONTUACAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1932Requisito_Pontuacao, "ZZ,ZZZ,ZZ9.999")));
               n1932Requisito_Pontuacao = H00Q23_n1932Requisito_Pontuacao[0];
               A1934Requisito_Status = H00Q23_A1934Requisito_Status[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1934Requisito_Status", StringUtil.LTrim( StringUtil.Str( (decimal)(A1934Requisito_Status), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REQUISITO_STATUS", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1934Requisito_Status), "ZZZ9")));
               A1929Requisito_Restricao = H00Q23_A1929Requisito_Restricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1929Requisito_Restricao", A1929Requisito_Restricao);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REQUISITO_RESTRICAO", GetSecureSignedToken( sPrefix, A1929Requisito_Restricao));
               n1929Requisito_Restricao = H00Q23_n1929Requisito_Restricao[0];
               A1923Requisito_Descricao = H00Q23_A1923Requisito_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1923Requisito_Descricao", A1923Requisito_Descricao);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REQUISITO_DESCRICAO", GetSecureSignedToken( sPrefix, A1923Requisito_Descricao));
               n1923Requisito_Descricao = H00Q23_n1923Requisito_Descricao[0];
               A1927Requisito_Titulo = H00Q23_A1927Requisito_Titulo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1927Requisito_Titulo", A1927Requisito_Titulo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REQUISITO_TITULO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1927Requisito_Titulo, ""))));
               n1927Requisito_Titulo = H00Q23_n1927Requisito_Titulo[0];
               A2049Requisito_TipoReqCod = H00Q23_A2049Requisito_TipoReqCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2049Requisito_TipoReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2049Requisito_TipoReqCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REQUISITO_TIPOREQCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2049Requisito_TipoReqCod), "ZZZZZ9")));
               n2049Requisito_TipoReqCod = H00Q23_n2049Requisito_TipoReqCod[0];
               A1926Requisito_Agrupador = H00Q23_A1926Requisito_Agrupador[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1926Requisito_Agrupador", A1926Requisito_Agrupador);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REQUISITO_AGRUPADOR", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1926Requisito_Agrupador, ""))));
               n1926Requisito_Agrupador = H00Q23_n1926Requisito_Agrupador[0];
               A1931Requisito_Ordem = H00Q23_A1931Requisito_Ordem[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1931Requisito_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A1931Requisito_Ordem), 3, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REQUISITO_ORDEM", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1931Requisito_Ordem), "ZZ9")));
               n1931Requisito_Ordem = H00Q23_n1931Requisito_Ordem[0];
               GXAREQUISITO_TIPOREQCOD_htmlQ22( ) ;
               /* Execute user event: E12Q22 */
               E12Q22 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
            WBQ20( ) ;
         }
      }

      protected void STRUPQ20( )
      {
         /* Before Start, stand alone formulas. */
         AV14Pgmname = "RequisitoGeneral";
         context.Gx_err = 0;
         GXAREQUISITO_TIPOREQCOD_htmlQ22( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11Q22 */
         E11Q22 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A1931Requisito_Ordem = (short)(context.localUtil.CToN( cgiGet( edtRequisito_Ordem_Internalname), ",", "."));
            n1931Requisito_Ordem = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1931Requisito_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A1931Requisito_Ordem), 3, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REQUISITO_ORDEM", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1931Requisito_Ordem), "ZZ9")));
            A1926Requisito_Agrupador = cgiGet( edtRequisito_Agrupador_Internalname);
            n1926Requisito_Agrupador = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1926Requisito_Agrupador", A1926Requisito_Agrupador);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REQUISITO_AGRUPADOR", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1926Requisito_Agrupador, ""))));
            dynRequisito_TipoReqCod.CurrentValue = cgiGet( dynRequisito_TipoReqCod_Internalname);
            A2049Requisito_TipoReqCod = (int)(NumberUtil.Val( cgiGet( dynRequisito_TipoReqCod_Internalname), "."));
            n2049Requisito_TipoReqCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2049Requisito_TipoReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2049Requisito_TipoReqCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REQUISITO_TIPOREQCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2049Requisito_TipoReqCod), "ZZZZZ9")));
            A1927Requisito_Titulo = cgiGet( edtRequisito_Titulo_Internalname);
            n1927Requisito_Titulo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1927Requisito_Titulo", A1927Requisito_Titulo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REQUISITO_TITULO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1927Requisito_Titulo, ""))));
            A1923Requisito_Descricao = cgiGet( edtRequisito_Descricao_Internalname);
            n1923Requisito_Descricao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1923Requisito_Descricao", A1923Requisito_Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REQUISITO_DESCRICAO", GetSecureSignedToken( sPrefix, A1923Requisito_Descricao));
            A1929Requisito_Restricao = cgiGet( edtRequisito_Restricao_Internalname);
            n1929Requisito_Restricao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1929Requisito_Restricao", A1929Requisito_Restricao);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REQUISITO_RESTRICAO", GetSecureSignedToken( sPrefix, A1929Requisito_Restricao));
            cmbRequisito_Status.CurrentValue = cgiGet( cmbRequisito_Status_Internalname);
            A1934Requisito_Status = (short)(NumberUtil.Val( cgiGet( cmbRequisito_Status_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1934Requisito_Status", StringUtil.LTrim( StringUtil.Str( (decimal)(A1934Requisito_Status), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REQUISITO_STATUS", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1934Requisito_Status), "ZZZ9")));
            A1932Requisito_Pontuacao = context.localUtil.CToN( cgiGet( edtRequisito_Pontuacao_Internalname), ",", ".");
            n1932Requisito_Pontuacao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1932Requisito_Pontuacao", StringUtil.LTrim( StringUtil.Str( A1932Requisito_Pontuacao, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REQUISITO_PONTUACAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1932Requisito_Pontuacao, "ZZ,ZZZ,ZZ9.999")));
            A1935Requisito_Ativo = StringUtil.StrToBool( cgiGet( chkRequisito_Ativo_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1935Requisito_Ativo", A1935Requisito_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REQUISITO_ATIVO", GetSecureSignedToken( sPrefix, A1935Requisito_Ativo));
            /* Read saved values. */
            wcpOA1919Requisito_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1919Requisito_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            GXAREQUISITO_TIPOREQCOD_htmlQ22( ) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11Q22 */
         E11Q22 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11Q22( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E12Q22( )
      {
         /* Load Routine */
         edtRequisito_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtRequisito_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRequisito_Codigo_Visible), 5, 0)));
         if ( ! ( ( AV6WWPContext.gxTpr_Update ) ) )
         {
            bttBtnupdate_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Enabled), 5, 0)));
         }
         if ( ! ( ( AV6WWPContext.gxTpr_Delete ) ) )
         {
            bttBtndelete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Enabled), 5, 0)));
         }
      }

      protected void E13Q22( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("requisito.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1919Requisito_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E14Q22( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("requisito.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A1919Requisito_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV14Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Requisito";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "Requisito_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Requisito_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_Q22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_Q22( true) ;
         }
         else
         {
            wb_table2_8_Q22( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_Q22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_56_Q22( true) ;
         }
         else
         {
            wb_table3_56_Q22( false) ;
         }
         return  ;
      }

      protected void wb_table3_56_Q22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_Q22e( true) ;
         }
         else
         {
            wb_table1_2_Q22e( false) ;
         }
      }

      protected void wb_table3_56_Q22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, 1, bttBtnupdate_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_RequisitoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, 1, bttBtndelete_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_RequisitoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 7, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+"e15q21_client"+"'", TempTags, "", 2, "HLP_RequisitoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_56_Q22e( true) ;
         }
         else
         {
            wb_table3_56_Q22e( false) ;
         }
      }

      protected void wb_table2_8_Q22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockrequisito_ordem_Internalname, "Ordem", "", "", lblTextblockrequisito_ordem_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_RequisitoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtRequisito_Ordem_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1931Requisito_Ordem), 3, 0, ",", "")), context.localUtil.Format( (decimal)(A1931Requisito_Ordem), "ZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtRequisito_Ordem_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Ordem", "right", false, "HLP_RequisitoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockrequisito_agrupador_Internalname, "Agrupador", "", "", lblTextblockrequisito_agrupador_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_RequisitoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtRequisito_Agrupador_Internalname, A1926Requisito_Agrupador, StringUtil.RTrim( context.localUtil.Format( A1926Requisito_Agrupador, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtRequisito_Agrupador_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 25, "chr", 1, "row", 25, 0, 0, 0, 1, -1, -1, true, "ReqAgrupador", "left", true, "HLP_RequisitoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockrequisito_tiporeqcod_Internalname, "Tipo", "", "", lblTextblockrequisito_tiporeqcod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_RequisitoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynRequisito_TipoReqCod, dynRequisito_TipoReqCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A2049Requisito_TipoReqCod), 6, 0)), 1, dynRequisito_TipoReqCod_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_RequisitoGeneral.htm");
            dynRequisito_TipoReqCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2049Requisito_TipoReqCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynRequisito_TipoReqCod_Internalname, "Values", (String)(dynRequisito_TipoReqCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockrequisito_titulo_Internalname, "T�tulo", "", "", lblTextblockrequisito_titulo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_RequisitoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtRequisito_Titulo_Internalname, A1927Requisito_Titulo, "", "", 0, 1, 0, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "250", -1, "", "", -1, true, "ReqNome", "HLP_RequisitoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockrequisito_descricao_Internalname, "Solu��o", "", "", lblTextblockrequisito_descricao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_RequisitoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtRequisito_Descricao_Internalname, A1923Requisito_Descricao, "", "", 2, 1, 0, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "ReqDescricao", "HLP_RequisitoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockrequisito_restricao_Internalname, "Restri��o", "", "", lblTextblockrequisito_restricao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_RequisitoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtRequisito_Restricao_Internalname, A1929Requisito_Restricao, "", "", 0, 1, 0, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_RequisitoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockrequisito_status_Internalname, "Status", "", "", lblTextblockrequisito_status_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_RequisitoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbRequisito_Status, cmbRequisito_Status_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1934Requisito_Status), 4, 0)), 1, cmbRequisito_Status_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_RequisitoGeneral.htm");
            cmbRequisito_Status.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1934Requisito_Status), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbRequisito_Status_Internalname, "Values", (String)(cmbRequisito_Status.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockrequisito_pontuacao_Internalname, "Prevista", "", "", lblTextblockrequisito_pontuacao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_RequisitoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtRequisito_Pontuacao_Internalname, StringUtil.LTrim( StringUtil.NToC( A1932Requisito_Pontuacao, 14, 5, ",", "")), context.localUtil.Format( A1932Requisito_Pontuacao, "ZZ,ZZZ,ZZ9.999"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtRequisito_Pontuacao_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_RequisitoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockrequisito_ativo_Internalname, "Ativo", "", "", lblTextblockrequisito_ativo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_RequisitoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Check box */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkRequisito_Ativo_Internalname, StringUtil.BoolToStr( A1935Requisito_Ativo), "", "", 1, 0, "true", "", StyleString, ClassString, "", "");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_Q22e( true) ;
         }
         else
         {
            wb_table2_8_Q22e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A1919Requisito_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1919Requisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1919Requisito_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAQ22( ) ;
         WSQ22( ) ;
         WEQ22( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA1919Requisito_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAQ22( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "requisitogeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAQ22( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A1919Requisito_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1919Requisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1919Requisito_Codigo), 6, 0)));
         }
         wcpOA1919Requisito_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1919Requisito_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A1919Requisito_Codigo != wcpOA1919Requisito_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA1919Requisito_Codigo = A1919Requisito_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA1919Requisito_Codigo = cgiGet( sPrefix+"A1919Requisito_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA1919Requisito_Codigo) > 0 )
         {
            A1919Requisito_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA1919Requisito_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1919Requisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1919Requisito_Codigo), 6, 0)));
         }
         else
         {
            A1919Requisito_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A1919Requisito_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAQ22( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSQ22( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSQ22( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A1919Requisito_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1919Requisito_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA1919Requisito_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A1919Requisito_Codigo_CTRL", StringUtil.RTrim( sCtrlA1919Requisito_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEQ22( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311730117");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("requisitogeneral.js", "?2020311730117");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockrequisito_ordem_Internalname = sPrefix+"TEXTBLOCKREQUISITO_ORDEM";
         edtRequisito_Ordem_Internalname = sPrefix+"REQUISITO_ORDEM";
         lblTextblockrequisito_agrupador_Internalname = sPrefix+"TEXTBLOCKREQUISITO_AGRUPADOR";
         edtRequisito_Agrupador_Internalname = sPrefix+"REQUISITO_AGRUPADOR";
         lblTextblockrequisito_tiporeqcod_Internalname = sPrefix+"TEXTBLOCKREQUISITO_TIPOREQCOD";
         dynRequisito_TipoReqCod_Internalname = sPrefix+"REQUISITO_TIPOREQCOD";
         lblTextblockrequisito_titulo_Internalname = sPrefix+"TEXTBLOCKREQUISITO_TITULO";
         edtRequisito_Titulo_Internalname = sPrefix+"REQUISITO_TITULO";
         lblTextblockrequisito_descricao_Internalname = sPrefix+"TEXTBLOCKREQUISITO_DESCRICAO";
         edtRequisito_Descricao_Internalname = sPrefix+"REQUISITO_DESCRICAO";
         lblTextblockrequisito_restricao_Internalname = sPrefix+"TEXTBLOCKREQUISITO_RESTRICAO";
         edtRequisito_Restricao_Internalname = sPrefix+"REQUISITO_RESTRICAO";
         lblTextblockrequisito_status_Internalname = sPrefix+"TEXTBLOCKREQUISITO_STATUS";
         cmbRequisito_Status_Internalname = sPrefix+"REQUISITO_STATUS";
         lblTextblockrequisito_pontuacao_Internalname = sPrefix+"TEXTBLOCKREQUISITO_PONTUACAO";
         edtRequisito_Pontuacao_Internalname = sPrefix+"REQUISITO_PONTUACAO";
         lblTextblockrequisito_ativo_Internalname = sPrefix+"TEXTBLOCKREQUISITO_ATIVO";
         chkRequisito_Ativo_Internalname = sPrefix+"REQUISITO_ATIVO";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         bttBtnfechar_Internalname = sPrefix+"BTNFECHAR";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtRequisito_Codigo_Internalname = sPrefix+"REQUISITO_CODIGO";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtRequisito_Pontuacao_Jsonclick = "";
         cmbRequisito_Status_Jsonclick = "";
         dynRequisito_TipoReqCod_Jsonclick = "";
         edtRequisito_Agrupador_Jsonclick = "";
         edtRequisito_Ordem_Jsonclick = "";
         bttBtndelete_Enabled = 1;
         bttBtnupdate_Enabled = 1;
         chkRequisito_Ativo.Caption = "";
         edtRequisito_Codigo_Jsonclick = "";
         edtRequisito_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13Q22',iparms:[{av:'A1919Requisito_Codigo',fld:'REQUISITO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E14Q22',iparms:[{av:'A1919Requisito_Codigo',fld:'REQUISITO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DOFECHAR'","{handler:'E15Q21',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV14Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A1926Requisito_Agrupador = "";
         A1927Requisito_Titulo = "";
         A1923Requisito_Descricao = "";
         A1929Requisito_Restricao = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00Q22_A2049Requisito_TipoReqCod = new int[1] ;
         H00Q22_n2049Requisito_TipoReqCod = new bool[] {false} ;
         H00Q22_A2042TipoRequisito_Identificador = new String[] {""} ;
         H00Q22_n2042TipoRequisito_Identificador = new bool[] {false} ;
         H00Q23_A1919Requisito_Codigo = new int[1] ;
         H00Q23_A1935Requisito_Ativo = new bool[] {false} ;
         H00Q23_A1932Requisito_Pontuacao = new decimal[1] ;
         H00Q23_n1932Requisito_Pontuacao = new bool[] {false} ;
         H00Q23_A1934Requisito_Status = new short[1] ;
         H00Q23_A1929Requisito_Restricao = new String[] {""} ;
         H00Q23_n1929Requisito_Restricao = new bool[] {false} ;
         H00Q23_A1923Requisito_Descricao = new String[] {""} ;
         H00Q23_n1923Requisito_Descricao = new bool[] {false} ;
         H00Q23_A1927Requisito_Titulo = new String[] {""} ;
         H00Q23_n1927Requisito_Titulo = new bool[] {false} ;
         H00Q23_A2049Requisito_TipoReqCod = new int[1] ;
         H00Q23_n2049Requisito_TipoReqCod = new bool[] {false} ;
         H00Q23_A1926Requisito_Agrupador = new String[] {""} ;
         H00Q23_n1926Requisito_Agrupador = new bool[] {false} ;
         H00Q23_A1931Requisito_Ordem = new short[1] ;
         H00Q23_n1931Requisito_Ordem = new bool[] {false} ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         lblTextblockrequisito_ordem_Jsonclick = "";
         lblTextblockrequisito_agrupador_Jsonclick = "";
         lblTextblockrequisito_tiporeqcod_Jsonclick = "";
         lblTextblockrequisito_titulo_Jsonclick = "";
         lblTextblockrequisito_descricao_Jsonclick = "";
         lblTextblockrequisito_restricao_Jsonclick = "";
         lblTextblockrequisito_status_Jsonclick = "";
         lblTextblockrequisito_pontuacao_Jsonclick = "";
         lblTextblockrequisito_ativo_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA1919Requisito_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.requisitogeneral__default(),
            new Object[][] {
                new Object[] {
               H00Q22_A2049Requisito_TipoReqCod, H00Q22_A2042TipoRequisito_Identificador, H00Q22_n2042TipoRequisito_Identificador
               }
               , new Object[] {
               H00Q23_A1919Requisito_Codigo, H00Q23_A1935Requisito_Ativo, H00Q23_A1932Requisito_Pontuacao, H00Q23_n1932Requisito_Pontuacao, H00Q23_A1934Requisito_Status, H00Q23_A1929Requisito_Restricao, H00Q23_n1929Requisito_Restricao, H00Q23_A1923Requisito_Descricao, H00Q23_n1923Requisito_Descricao, H00Q23_A1927Requisito_Titulo,
               H00Q23_n1927Requisito_Titulo, H00Q23_A2049Requisito_TipoReqCod, H00Q23_n2049Requisito_TipoReqCod, H00Q23_A1926Requisito_Agrupador, H00Q23_n1926Requisito_Agrupador, H00Q23_A1931Requisito_Ordem, H00Q23_n1931Requisito_Ordem
               }
            }
         );
         AV14Pgmname = "RequisitoGeneral";
         /* GeneXus formulas. */
         AV14Pgmname = "RequisitoGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short A1931Requisito_Ordem ;
      private short A1934Requisito_Status ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A1919Requisito_Codigo ;
      private int wcpOA1919Requisito_Codigo ;
      private int A2049Requisito_TipoReqCod ;
      private int edtRequisito_Codigo_Visible ;
      private int gxdynajaxindex ;
      private int bttBtnupdate_Enabled ;
      private int bttBtndelete_Enabled ;
      private int AV7Requisito_Codigo ;
      private int idxLst ;
      private decimal A1932Requisito_Pontuacao ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV14Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String edtRequisito_Codigo_Internalname ;
      private String edtRequisito_Codigo_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String chkRequisito_Ativo_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String edtRequisito_Ordem_Internalname ;
      private String edtRequisito_Agrupador_Internalname ;
      private String dynRequisito_TipoReqCod_Internalname ;
      private String edtRequisito_Titulo_Internalname ;
      private String edtRequisito_Descricao_Internalname ;
      private String edtRequisito_Restricao_Internalname ;
      private String cmbRequisito_Status_Internalname ;
      private String edtRequisito_Pontuacao_Internalname ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockrequisito_ordem_Internalname ;
      private String lblTextblockrequisito_ordem_Jsonclick ;
      private String edtRequisito_Ordem_Jsonclick ;
      private String lblTextblockrequisito_agrupador_Internalname ;
      private String lblTextblockrequisito_agrupador_Jsonclick ;
      private String edtRequisito_Agrupador_Jsonclick ;
      private String lblTextblockrequisito_tiporeqcod_Internalname ;
      private String lblTextblockrequisito_tiporeqcod_Jsonclick ;
      private String dynRequisito_TipoReqCod_Jsonclick ;
      private String lblTextblockrequisito_titulo_Internalname ;
      private String lblTextblockrequisito_titulo_Jsonclick ;
      private String lblTextblockrequisito_descricao_Internalname ;
      private String lblTextblockrequisito_descricao_Jsonclick ;
      private String lblTextblockrequisito_restricao_Internalname ;
      private String lblTextblockrequisito_restricao_Jsonclick ;
      private String lblTextblockrequisito_status_Internalname ;
      private String lblTextblockrequisito_status_Jsonclick ;
      private String cmbRequisito_Status_Jsonclick ;
      private String lblTextblockrequisito_pontuacao_Internalname ;
      private String lblTextblockrequisito_pontuacao_Jsonclick ;
      private String edtRequisito_Pontuacao_Jsonclick ;
      private String lblTextblockrequisito_ativo_Internalname ;
      private String lblTextblockrequisito_ativo_Jsonclick ;
      private String sCtrlA1919Requisito_Codigo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool A1935Requisito_Ativo ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n2049Requisito_TipoReqCod ;
      private bool n1932Requisito_Pontuacao ;
      private bool n1929Requisito_Restricao ;
      private bool n1923Requisito_Descricao ;
      private bool n1927Requisito_Titulo ;
      private bool n1926Requisito_Agrupador ;
      private bool n1931Requisito_Ordem ;
      private bool returnInSub ;
      private String A1923Requisito_Descricao ;
      private String A1929Requisito_Restricao ;
      private String A1926Requisito_Agrupador ;
      private String A1927Requisito_Titulo ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynRequisito_TipoReqCod ;
      private GXCombobox cmbRequisito_Status ;
      private GXCheckbox chkRequisito_Ativo ;
      private IDataStoreProvider pr_default ;
      private int[] H00Q22_A2049Requisito_TipoReqCod ;
      private bool[] H00Q22_n2049Requisito_TipoReqCod ;
      private String[] H00Q22_A2042TipoRequisito_Identificador ;
      private bool[] H00Q22_n2042TipoRequisito_Identificador ;
      private int[] H00Q23_A1919Requisito_Codigo ;
      private bool[] H00Q23_A1935Requisito_Ativo ;
      private decimal[] H00Q23_A1932Requisito_Pontuacao ;
      private bool[] H00Q23_n1932Requisito_Pontuacao ;
      private short[] H00Q23_A1934Requisito_Status ;
      private String[] H00Q23_A1929Requisito_Restricao ;
      private bool[] H00Q23_n1929Requisito_Restricao ;
      private String[] H00Q23_A1923Requisito_Descricao ;
      private bool[] H00Q23_n1923Requisito_Descricao ;
      private String[] H00Q23_A1927Requisito_Titulo ;
      private bool[] H00Q23_n1927Requisito_Titulo ;
      private int[] H00Q23_A2049Requisito_TipoReqCod ;
      private bool[] H00Q23_n2049Requisito_TipoReqCod ;
      private String[] H00Q23_A1926Requisito_Agrupador ;
      private bool[] H00Q23_n1926Requisito_Agrupador ;
      private short[] H00Q23_A1931Requisito_Ordem ;
      private bool[] H00Q23_n1931Requisito_Ordem ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class requisitogeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00Q22 ;
          prmH00Q22 = new Object[] {
          } ;
          Object[] prmH00Q23 ;
          prmH00Q23 = new Object[] {
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00Q22", "SELECT [TipoRequisito_Codigo] AS Requisito_TipoReqCod, [TipoRequisito_Identificador] FROM [TipoRequisito] WITH (NOLOCK) ORDER BY [TipoRequisito_Identificador] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00Q22,0,0,true,false )
             ,new CursorDef("H00Q23", "SELECT [Requisito_Codigo], [Requisito_Ativo], [Requisito_Pontuacao], [Requisito_Status], [Requisito_Restricao], [Requisito_Descricao], [Requisito_Titulo], [Requisito_TipoReqCod] AS Requisito_TipoReqCod, [Requisito_Agrupador], [Requisito_Ordem] FROM [Requisito] WITH (NOLOCK) WHERE [Requisito_Codigo] = @Requisito_Codigo ORDER BY [Requisito_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00Q23,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((short[]) buf[15])[0] = rslt.getShort(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
