/*
               File: type_SdtContratoServicosPrioridade
        Description: Prioridade
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:11:7.49
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "ContratoServicosPrioridade" )]
   [XmlType(TypeName =  "ContratoServicosPrioridade" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtContratoServicosPrioridade : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContratoServicosPrioridade( )
      {
         /* Constructor for serialization */
         gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_nome = "";
         gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_finalidade = "";
         gxTv_SdtContratoServicosPrioridade_Mode = "";
         gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_nome_Z = "";
      }

      public SdtContratoServicosPrioridade( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV1336ContratoServicosPrioridade_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV1336ContratoServicosPrioridade_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"ContratoServicosPrioridade_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "ContratoServicosPrioridade");
         metadata.Set("BT", "ContratoServicosPrioridade");
         metadata.Set("PK", "[ \"ContratoServicosPrioridade_Codigo\" ]");
         metadata.Set("PKAssigned", "[ \"ContratoServicosPrioridade_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"ContratoServicos_Codigo\" ],\"FKMap\":[ \"ContratoServicosPrioridade_CntSrvCod-ContratoServicos_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicosprioridade_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicosprioridade_cntsrvcod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicosprioridade_nome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicosprioridade_percvalorb_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicosprioridade_percprazo_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicosprioridade_ordem_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicosprioridade_peso_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicosprioridade_finalidade_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicosprioridade_percvalorb_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicosprioridade_percprazo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicosprioridade_ordem_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicosprioridade_peso_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtContratoServicosPrioridade deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtContratoServicosPrioridade)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtContratoServicosPrioridade obj ;
         obj = this;
         obj.gxTpr_Contratoservicosprioridade_codigo = deserialized.gxTpr_Contratoservicosprioridade_codigo;
         obj.gxTpr_Contratoservicosprioridade_cntsrvcod = deserialized.gxTpr_Contratoservicosprioridade_cntsrvcod;
         obj.gxTpr_Contratoservicosprioridade_nome = deserialized.gxTpr_Contratoservicosprioridade_nome;
         obj.gxTpr_Contratoservicosprioridade_finalidade = deserialized.gxTpr_Contratoservicosprioridade_finalidade;
         obj.gxTpr_Contratoservicosprioridade_percvalorb = deserialized.gxTpr_Contratoservicosprioridade_percvalorb;
         obj.gxTpr_Contratoservicosprioridade_percprazo = deserialized.gxTpr_Contratoservicosprioridade_percprazo;
         obj.gxTpr_Contratoservicosprioridade_ordem = deserialized.gxTpr_Contratoservicosprioridade_ordem;
         obj.gxTpr_Contratoservicosprioridade_peso = deserialized.gxTpr_Contratoservicosprioridade_peso;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Contratoservicosprioridade_codigo_Z = deserialized.gxTpr_Contratoservicosprioridade_codigo_Z;
         obj.gxTpr_Contratoservicosprioridade_cntsrvcod_Z = deserialized.gxTpr_Contratoservicosprioridade_cntsrvcod_Z;
         obj.gxTpr_Contratoservicosprioridade_nome_Z = deserialized.gxTpr_Contratoservicosprioridade_nome_Z;
         obj.gxTpr_Contratoservicosprioridade_percvalorb_Z = deserialized.gxTpr_Contratoservicosprioridade_percvalorb_Z;
         obj.gxTpr_Contratoservicosprioridade_percprazo_Z = deserialized.gxTpr_Contratoservicosprioridade_percprazo_Z;
         obj.gxTpr_Contratoservicosprioridade_ordem_Z = deserialized.gxTpr_Contratoservicosprioridade_ordem_Z;
         obj.gxTpr_Contratoservicosprioridade_peso_Z = deserialized.gxTpr_Contratoservicosprioridade_peso_Z;
         obj.gxTpr_Contratoservicosprioridade_finalidade_N = deserialized.gxTpr_Contratoservicosprioridade_finalidade_N;
         obj.gxTpr_Contratoservicosprioridade_percvalorb_N = deserialized.gxTpr_Contratoservicosprioridade_percvalorb_N;
         obj.gxTpr_Contratoservicosprioridade_percprazo_N = deserialized.gxTpr_Contratoservicosprioridade_percprazo_N;
         obj.gxTpr_Contratoservicosprioridade_ordem_N = deserialized.gxTpr_Contratoservicosprioridade_ordem_N;
         obj.gxTpr_Contratoservicosprioridade_peso_N = deserialized.gxTpr_Contratoservicosprioridade_peso_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosPrioridade_Codigo") )
               {
                  gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosPrioridade_CntSrvCod") )
               {
                  gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_cntsrvcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosPrioridade_Nome") )
               {
                  gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosPrioridade_Finalidade") )
               {
                  gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_finalidade = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosPrioridade_PercValorB") )
               {
                  gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percvalorb = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosPrioridade_PercPrazo") )
               {
                  gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percprazo = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosPrioridade_Ordem") )
               {
                  gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_ordem = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosPrioridade_Peso") )
               {
                  gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_peso = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtContratoServicosPrioridade_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtContratoServicosPrioridade_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosPrioridade_Codigo_Z") )
               {
                  gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosPrioridade_CntSrvCod_Z") )
               {
                  gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_cntsrvcod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosPrioridade_Nome_Z") )
               {
                  gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosPrioridade_PercValorB_Z") )
               {
                  gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percvalorb_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosPrioridade_PercPrazo_Z") )
               {
                  gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percprazo_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosPrioridade_Ordem_Z") )
               {
                  gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_ordem_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosPrioridade_Peso_Z") )
               {
                  gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_peso_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosPrioridade_Finalidade_N") )
               {
                  gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_finalidade_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosPrioridade_PercValorB_N") )
               {
                  gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percvalorb_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosPrioridade_PercPrazo_N") )
               {
                  gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percprazo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosPrioridade_Ordem_N") )
               {
                  gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_ordem_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosPrioridade_Peso_N") )
               {
                  gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_peso_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "ContratoServicosPrioridade";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ContratoServicosPrioridade_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratoServicosPrioridade_CntSrvCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_cntsrvcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratoServicosPrioridade_Nome", StringUtil.RTrim( gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratoServicosPrioridade_Finalidade", StringUtil.RTrim( gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_finalidade));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratoServicosPrioridade_PercValorB", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percvalorb, 6, 2)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratoServicosPrioridade_PercPrazo", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percprazo, 6, 2)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratoServicosPrioridade_Ordem", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_ordem), 3, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratoServicosPrioridade_Peso", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_peso), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtContratoServicosPrioridade_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosPrioridade_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratoServicosPrioridade_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratoServicosPrioridade_CntSrvCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_cntsrvcod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratoServicosPrioridade_Nome_Z", StringUtil.RTrim( gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratoServicosPrioridade_PercValorB_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percvalorb_Z, 6, 2)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratoServicosPrioridade_PercPrazo_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percprazo_Z, 6, 2)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratoServicosPrioridade_Ordem_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_ordem_Z), 3, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratoServicosPrioridade_Peso_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_peso_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratoServicosPrioridade_Finalidade_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_finalidade_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratoServicosPrioridade_PercValorB_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percvalorb_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratoServicosPrioridade_PercPrazo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percprazo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratoServicosPrioridade_Ordem_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_ordem_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratoServicosPrioridade_Peso_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_peso_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ContratoServicosPrioridade_Codigo", gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_codigo, false);
         AddObjectProperty("ContratoServicosPrioridade_CntSrvCod", gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_cntsrvcod, false);
         AddObjectProperty("ContratoServicosPrioridade_Nome", gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_nome, false);
         AddObjectProperty("ContratoServicosPrioridade_Finalidade", gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_finalidade, false);
         AddObjectProperty("ContratoServicosPrioridade_PercValorB", gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percvalorb, false);
         AddObjectProperty("ContratoServicosPrioridade_PercPrazo", gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percprazo, false);
         AddObjectProperty("ContratoServicosPrioridade_Ordem", gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_ordem, false);
         AddObjectProperty("ContratoServicosPrioridade_Peso", gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_peso, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtContratoServicosPrioridade_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtContratoServicosPrioridade_Initialized, false);
            AddObjectProperty("ContratoServicosPrioridade_Codigo_Z", gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_codigo_Z, false);
            AddObjectProperty("ContratoServicosPrioridade_CntSrvCod_Z", gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_cntsrvcod_Z, false);
            AddObjectProperty("ContratoServicosPrioridade_Nome_Z", gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_nome_Z, false);
            AddObjectProperty("ContratoServicosPrioridade_PercValorB_Z", gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percvalorb_Z, false);
            AddObjectProperty("ContratoServicosPrioridade_PercPrazo_Z", gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percprazo_Z, false);
            AddObjectProperty("ContratoServicosPrioridade_Ordem_Z", gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_ordem_Z, false);
            AddObjectProperty("ContratoServicosPrioridade_Peso_Z", gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_peso_Z, false);
            AddObjectProperty("ContratoServicosPrioridade_Finalidade_N", gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_finalidade_N, false);
            AddObjectProperty("ContratoServicosPrioridade_PercValorB_N", gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percvalorb_N, false);
            AddObjectProperty("ContratoServicosPrioridade_PercPrazo_N", gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percprazo_N, false);
            AddObjectProperty("ContratoServicosPrioridade_Ordem_N", gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_ordem_N, false);
            AddObjectProperty("ContratoServicosPrioridade_Peso_N", gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_peso_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "ContratoServicosPrioridade_Codigo" )]
      [  XmlElement( ElementName = "ContratoServicosPrioridade_Codigo"   )]
      public int gxTpr_Contratoservicosprioridade_codigo
      {
         get {
            return gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_codigo ;
         }

         set {
            if ( gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_codigo != value )
            {
               gxTv_SdtContratoServicosPrioridade_Mode = "INS";
               this.gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_codigo_Z_SetNull( );
               this.gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_cntsrvcod_Z_SetNull( );
               this.gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_nome_Z_SetNull( );
               this.gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percvalorb_Z_SetNull( );
               this.gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percprazo_Z_SetNull( );
               this.gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_ordem_Z_SetNull( );
               this.gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_peso_Z_SetNull( );
            }
            gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContratoServicosPrioridade_CntSrvCod" )]
      [  XmlElement( ElementName = "ContratoServicosPrioridade_CntSrvCod"   )]
      public int gxTpr_Contratoservicosprioridade_cntsrvcod
      {
         get {
            return gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_cntsrvcod ;
         }

         set {
            gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_cntsrvcod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContratoServicosPrioridade_Nome" )]
      [  XmlElement( ElementName = "ContratoServicosPrioridade_Nome"   )]
      public String gxTpr_Contratoservicosprioridade_nome
      {
         get {
            return gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_nome ;
         }

         set {
            gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContratoServicosPrioridade_Finalidade" )]
      [  XmlElement( ElementName = "ContratoServicosPrioridade_Finalidade"   )]
      public String gxTpr_Contratoservicosprioridade_finalidade
      {
         get {
            return gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_finalidade ;
         }

         set {
            gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_finalidade_N = 0;
            gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_finalidade = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_finalidade_SetNull( )
      {
         gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_finalidade_N = 1;
         gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_finalidade = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_finalidade_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosPrioridade_PercValorB" )]
      [  XmlElement( ElementName = "ContratoServicosPrioridade_PercValorB"   )]
      public double gxTpr_Contratoservicosprioridade_percvalorb_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percvalorb) ;
         }

         set {
            gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percvalorb_N = 0;
            gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percvalorb = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contratoservicosprioridade_percvalorb
      {
         get {
            return gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percvalorb ;
         }

         set {
            gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percvalorb_N = 0;
            gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percvalorb = (decimal)(value);
         }

      }

      public void gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percvalorb_SetNull( )
      {
         gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percvalorb_N = 1;
         gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percvalorb = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percvalorb_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosPrioridade_PercPrazo" )]
      [  XmlElement( ElementName = "ContratoServicosPrioridade_PercPrazo"   )]
      public double gxTpr_Contratoservicosprioridade_percprazo_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percprazo) ;
         }

         set {
            gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percprazo_N = 0;
            gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percprazo = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contratoservicosprioridade_percprazo
      {
         get {
            return gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percprazo ;
         }

         set {
            gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percprazo_N = 0;
            gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percprazo = (decimal)(value);
         }

      }

      public void gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percprazo_SetNull( )
      {
         gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percprazo_N = 1;
         gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percprazo = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percprazo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosPrioridade_Ordem" )]
      [  XmlElement( ElementName = "ContratoServicosPrioridade_Ordem"   )]
      public short gxTpr_Contratoservicosprioridade_ordem
      {
         get {
            return gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_ordem ;
         }

         set {
            gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_ordem_N = 0;
            gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_ordem = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_ordem_SetNull( )
      {
         gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_ordem_N = 1;
         gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_ordem = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_ordem_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosPrioridade_Peso" )]
      [  XmlElement( ElementName = "ContratoServicosPrioridade_Peso"   )]
      public short gxTpr_Contratoservicosprioridade_peso
      {
         get {
            return gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_peso ;
         }

         set {
            gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_peso_N = 0;
            gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_peso = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_peso_SetNull( )
      {
         gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_peso_N = 1;
         gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_peso = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_peso_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtContratoServicosPrioridade_Mode ;
         }

         set {
            gxTv_SdtContratoServicosPrioridade_Mode = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicosPrioridade_Mode_SetNull( )
      {
         gxTv_SdtContratoServicosPrioridade_Mode = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicosPrioridade_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtContratoServicosPrioridade_Initialized ;
         }

         set {
            gxTv_SdtContratoServicosPrioridade_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicosPrioridade_Initialized_SetNull( )
      {
         gxTv_SdtContratoServicosPrioridade_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicosPrioridade_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosPrioridade_Codigo_Z" )]
      [  XmlElement( ElementName = "ContratoServicosPrioridade_Codigo_Z"   )]
      public int gxTpr_Contratoservicosprioridade_codigo_Z
      {
         get {
            return gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_codigo_Z ;
         }

         set {
            gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_codigo_Z_SetNull( )
      {
         gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosPrioridade_CntSrvCod_Z" )]
      [  XmlElement( ElementName = "ContratoServicosPrioridade_CntSrvCod_Z"   )]
      public int gxTpr_Contratoservicosprioridade_cntsrvcod_Z
      {
         get {
            return gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_cntsrvcod_Z ;
         }

         set {
            gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_cntsrvcod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_cntsrvcod_Z_SetNull( )
      {
         gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_cntsrvcod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_cntsrvcod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosPrioridade_Nome_Z" )]
      [  XmlElement( ElementName = "ContratoServicosPrioridade_Nome_Z"   )]
      public String gxTpr_Contratoservicosprioridade_nome_Z
      {
         get {
            return gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_nome_Z ;
         }

         set {
            gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_nome_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_nome_Z_SetNull( )
      {
         gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_nome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosPrioridade_PercValorB_Z" )]
      [  XmlElement( ElementName = "ContratoServicosPrioridade_PercValorB_Z"   )]
      public double gxTpr_Contratoservicosprioridade_percvalorb_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percvalorb_Z) ;
         }

         set {
            gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percvalorb_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contratoservicosprioridade_percvalorb_Z
      {
         get {
            return gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percvalorb_Z ;
         }

         set {
            gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percvalorb_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percvalorb_Z_SetNull( )
      {
         gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percvalorb_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percvalorb_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosPrioridade_PercPrazo_Z" )]
      [  XmlElement( ElementName = "ContratoServicosPrioridade_PercPrazo_Z"   )]
      public double gxTpr_Contratoservicosprioridade_percprazo_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percprazo_Z) ;
         }

         set {
            gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percprazo_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contratoservicosprioridade_percprazo_Z
      {
         get {
            return gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percprazo_Z ;
         }

         set {
            gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percprazo_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percprazo_Z_SetNull( )
      {
         gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percprazo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percprazo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosPrioridade_Ordem_Z" )]
      [  XmlElement( ElementName = "ContratoServicosPrioridade_Ordem_Z"   )]
      public short gxTpr_Contratoservicosprioridade_ordem_Z
      {
         get {
            return gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_ordem_Z ;
         }

         set {
            gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_ordem_Z = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_ordem_Z_SetNull( )
      {
         gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_ordem_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_ordem_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosPrioridade_Peso_Z" )]
      [  XmlElement( ElementName = "ContratoServicosPrioridade_Peso_Z"   )]
      public short gxTpr_Contratoservicosprioridade_peso_Z
      {
         get {
            return gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_peso_Z ;
         }

         set {
            gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_peso_Z = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_peso_Z_SetNull( )
      {
         gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_peso_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_peso_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosPrioridade_Finalidade_N" )]
      [  XmlElement( ElementName = "ContratoServicosPrioridade_Finalidade_N"   )]
      public short gxTpr_Contratoservicosprioridade_finalidade_N
      {
         get {
            return gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_finalidade_N ;
         }

         set {
            gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_finalidade_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_finalidade_N_SetNull( )
      {
         gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_finalidade_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_finalidade_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosPrioridade_PercValorB_N" )]
      [  XmlElement( ElementName = "ContratoServicosPrioridade_PercValorB_N"   )]
      public short gxTpr_Contratoservicosprioridade_percvalorb_N
      {
         get {
            return gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percvalorb_N ;
         }

         set {
            gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percvalorb_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percvalorb_N_SetNull( )
      {
         gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percvalorb_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percvalorb_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosPrioridade_PercPrazo_N" )]
      [  XmlElement( ElementName = "ContratoServicosPrioridade_PercPrazo_N"   )]
      public short gxTpr_Contratoservicosprioridade_percprazo_N
      {
         get {
            return gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percprazo_N ;
         }

         set {
            gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percprazo_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percprazo_N_SetNull( )
      {
         gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percprazo_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percprazo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosPrioridade_Ordem_N" )]
      [  XmlElement( ElementName = "ContratoServicosPrioridade_Ordem_N"   )]
      public short gxTpr_Contratoservicosprioridade_ordem_N
      {
         get {
            return gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_ordem_N ;
         }

         set {
            gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_ordem_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_ordem_N_SetNull( )
      {
         gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_ordem_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_ordem_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosPrioridade_Peso_N" )]
      [  XmlElement( ElementName = "ContratoServicosPrioridade_Peso_N"   )]
      public short gxTpr_Contratoservicosprioridade_peso_N
      {
         get {
            return gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_peso_N ;
         }

         set {
            gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_peso_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_peso_N_SetNull( )
      {
         gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_peso_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_peso_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_nome = "";
         gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_finalidade = "";
         gxTv_SdtContratoServicosPrioridade_Mode = "";
         gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_nome_Z = "";
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "contratoservicosprioridade", "GeneXus.Programs.contratoservicosprioridade_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_ordem ;
      private short gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_peso ;
      private short gxTv_SdtContratoServicosPrioridade_Initialized ;
      private short gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_ordem_Z ;
      private short gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_peso_Z ;
      private short gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_finalidade_N ;
      private short gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percvalorb_N ;
      private short gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percprazo_N ;
      private short gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_ordem_N ;
      private short gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_peso_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_codigo ;
      private int gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_cntsrvcod ;
      private int gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_codigo_Z ;
      private int gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_cntsrvcod_Z ;
      private decimal gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percvalorb ;
      private decimal gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percprazo ;
      private decimal gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percvalorb_Z ;
      private decimal gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_percprazo_Z ;
      private String gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_nome ;
      private String gxTv_SdtContratoServicosPrioridade_Mode ;
      private String gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_nome_Z ;
      private String sTagName ;
      private String gxTv_SdtContratoServicosPrioridade_Contratoservicosprioridade_finalidade ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"ContratoServicosPrioridade", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtContratoServicosPrioridade_RESTInterface : GxGenericCollectionItem<SdtContratoServicosPrioridade>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContratoServicosPrioridade_RESTInterface( ) : base()
      {
      }

      public SdtContratoServicosPrioridade_RESTInterface( SdtContratoServicosPrioridade psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ContratoServicosPrioridade_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratoservicosprioridade_codigo
      {
         get {
            return sdt.gxTpr_Contratoservicosprioridade_codigo ;
         }

         set {
            sdt.gxTpr_Contratoservicosprioridade_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoServicosPrioridade_CntSrvCod" , Order = 1 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratoservicosprioridade_cntsrvcod
      {
         get {
            return sdt.gxTpr_Contratoservicosprioridade_cntsrvcod ;
         }

         set {
            sdt.gxTpr_Contratoservicosprioridade_cntsrvcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoServicosPrioridade_Nome" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Contratoservicosprioridade_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratoservicosprioridade_nome) ;
         }

         set {
            sdt.gxTpr_Contratoservicosprioridade_nome = (String)(value);
         }

      }

      [DataMember( Name = "ContratoServicosPrioridade_Finalidade" , Order = 3 )]
      public String gxTpr_Contratoservicosprioridade_finalidade
      {
         get {
            return sdt.gxTpr_Contratoservicosprioridade_finalidade ;
         }

         set {
            sdt.gxTpr_Contratoservicosprioridade_finalidade = (String)(value);
         }

      }

      [DataMember( Name = "ContratoServicosPrioridade_PercValorB" , Order = 4 )]
      [GxSeudo()]
      public Nullable<decimal> gxTpr_Contratoservicosprioridade_percvalorb
      {
         get {
            return sdt.gxTpr_Contratoservicosprioridade_percvalorb ;
         }

         set {
            sdt.gxTpr_Contratoservicosprioridade_percvalorb = (decimal)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoServicosPrioridade_PercPrazo" , Order = 5 )]
      [GxSeudo()]
      public Nullable<decimal> gxTpr_Contratoservicosprioridade_percprazo
      {
         get {
            return sdt.gxTpr_Contratoservicosprioridade_percprazo ;
         }

         set {
            sdt.gxTpr_Contratoservicosprioridade_percprazo = (decimal)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoServicosPrioridade_Ordem" , Order = 6 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contratoservicosprioridade_ordem
      {
         get {
            return sdt.gxTpr_Contratoservicosprioridade_ordem ;
         }

         set {
            sdt.gxTpr_Contratoservicosprioridade_ordem = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoServicosPrioridade_Peso" , Order = 7 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contratoservicosprioridade_peso
      {
         get {
            return sdt.gxTpr_Contratoservicosprioridade_peso ;
         }

         set {
            sdt.gxTpr_Contratoservicosprioridade_peso = (short)(value.HasValue ? value.Value : 0);
         }

      }

      public SdtContratoServicosPrioridade sdt
      {
         get {
            return (SdtContratoServicosPrioridade)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtContratoServicosPrioridade() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 22 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
