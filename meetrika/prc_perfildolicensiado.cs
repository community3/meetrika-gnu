/*
               File: PRC_PerfildoLicensiado
        Description: Retorna c�digo do Perfil do Licensiado
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:43.44
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_perfildolicensiado : GXProcedure
   {
      public prc_perfildolicensiado( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_perfildolicensiado( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( out int aP0_Perfil_Codigo ,
                           out long aP1_Perfil_GamId )
      {
         this.AV11Perfil_Codigo = 0 ;
         this.AV12Perfil_GamId = 0 ;
         initialize();
         executePrivate();
         aP0_Perfil_Codigo=this.AV11Perfil_Codigo;
         aP1_Perfil_GamId=this.AV12Perfil_GamId;
      }

      public long executeUdp( out int aP0_Perfil_Codigo )
      {
         this.AV11Perfil_Codigo = 0 ;
         this.AV12Perfil_GamId = 0 ;
         initialize();
         executePrivate();
         aP0_Perfil_Codigo=this.AV11Perfil_Codigo;
         aP1_Perfil_GamId=this.AV12Perfil_GamId;
         return AV12Perfil_GamId ;
      }

      public void executeSubmit( out int aP0_Perfil_Codigo ,
                                 out long aP1_Perfil_GamId )
      {
         prc_perfildolicensiado objprc_perfildolicensiado;
         objprc_perfildolicensiado = new prc_perfildolicensiado();
         objprc_perfildolicensiado.AV11Perfil_Codigo = 0 ;
         objprc_perfildolicensiado.AV12Perfil_GamId = 0 ;
         objprc_perfildolicensiado.context.SetSubmitInitialConfig(context);
         objprc_perfildolicensiado.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_perfildolicensiado);
         aP0_Perfil_Codigo=this.AV11Perfil_Codigo;
         aP1_Perfil_GamId=this.AV12Perfil_GamId;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_perfildolicensiado)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P001J2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            A275Perfil_Tipo = P001J2_A275Perfil_Tipo[0];
            A276Perfil_Ativo = P001J2_A276Perfil_Ativo[0];
            A3Perfil_Codigo = P001J2_A3Perfil_Codigo[0];
            A329Perfil_GamId = P001J2_A329Perfil_GamId[0];
            AV11Perfil_Codigo = A3Perfil_Codigo;
            AV12Perfil_GamId = A329Perfil_GamId;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P001J2_A275Perfil_Tipo = new short[1] ;
         P001J2_A276Perfil_Ativo = new bool[] {false} ;
         P001J2_A3Perfil_Codigo = new int[1] ;
         P001J2_A329Perfil_GamId = new long[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_perfildolicensiado__default(),
            new Object[][] {
                new Object[] {
               P001J2_A275Perfil_Tipo, P001J2_A276Perfil_Ativo, P001J2_A3Perfil_Codigo, P001J2_A329Perfil_GamId
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A275Perfil_Tipo ;
      private int A3Perfil_Codigo ;
      private int AV11Perfil_Codigo ;
      private long AV12Perfil_GamId ;
      private long A329Perfil_GamId ;
      private String scmdbuf ;
      private bool A276Perfil_Ativo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private short[] P001J2_A275Perfil_Tipo ;
      private bool[] P001J2_A276Perfil_Ativo ;
      private int[] P001J2_A3Perfil_Codigo ;
      private long[] P001J2_A329Perfil_GamId ;
      private int aP0_Perfil_Codigo ;
      private long aP1_Perfil_GamId ;
   }

   public class prc_perfildolicensiado__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP001J2 ;
          prmP001J2 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("P001J2", "SELECT TOP 1 [Perfil_Tipo], [Perfil_Ativo], [Perfil_Codigo], [Perfil_GamId] FROM [Perfil] WITH (NOLOCK) WHERE ([Perfil_Ativo] = 1) AND ([Perfil_Tipo] = 10) ORDER BY [Perfil_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP001J2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((long[]) buf[3])[0] = rslt.getLong(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
