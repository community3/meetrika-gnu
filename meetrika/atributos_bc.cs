/*
               File: Atributos_BC
        Description: Atributos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:18:50.12
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class atributos_bc : GXHttpHandler, IGxSilentTrn
   {
      public atributos_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public atributos_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow1340( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey1340( ) ;
         standaloneModal( ) ;
         AddRow1340( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E11132 */
            E11132 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z176Atributos_Codigo = A176Atributos_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_130( )
      {
         BeforeValidate1340( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls1340( ) ;
            }
            else
            {
               CheckExtendedTable1340( ) ;
               if ( AnyError == 0 )
               {
                  ZM1340( 8) ;
                  ZM1340( 9) ;
               }
               CloseExtendedTableCursors1340( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void E12132( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV23Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV24GXV1 = 1;
            while ( AV24GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV24GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Atributos_TabelaCod") == 0 )
               {
                  AV13Insert_Atributos_TabelaCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Atributos_MelhoraCod") == 0 )
               {
                  AV22Insert_Atributos_MelhoraCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               AV24GXV1 = (int)(AV24GXV1+1);
            }
         }
      }

      protected void E11132( )
      {
         /* After Trn Routine */
      }

      protected void E13132( )
      {
         /* 'DoEliminar' Routine */
         new prc_desativarregistro(context ).execute(  "Atr",  AV7Atributos_Codigo,  0) ;
      }

      protected void E14132( )
      {
         /* 'DoFechar' Routine */
         AV10WebSession.Remove("FiltroRecebido");
         context.wjLoc = formatLink("viewsistema.aspx") + "?" + UrlEncode("" +A190Tabela_SistemaCod) + "," + UrlEncode(StringUtil.RTrim(""));
         context.wjLocDisableFrm = 1;
      }

      protected void E15132( )
      {
         /* 'DoCopiarColarAtributos' Routine */
         context.wjLoc = formatLink("wp_copiarcolaratributos.aspx") + "?" + UrlEncode("" +A190Tabela_SistemaCod);
         context.wjLocDisableFrm = 1;
      }

      protected void E16132( )
      {
         /* 'DoImportarExcel' Routine */
      }

      protected void ZM1340( short GX_JID )
      {
         if ( ( GX_JID == 7 ) || ( GX_JID == 0 ) )
         {
            Z177Atributos_Nome = A177Atributos_Nome;
            Z390Atributos_Detalhes = A390Atributos_Detalhes;
            Z178Atributos_TipoDados = A178Atributos_TipoDados;
            Z400Atributos_PK = A400Atributos_PK;
            Z401Atributos_FK = A401Atributos_FK;
            Z180Atributos_Ativo = A180Atributos_Ativo;
            Z356Atributos_TabelaCod = A356Atributos_TabelaCod;
            Z747Atributos_MelhoraCod = A747Atributos_MelhoraCod;
         }
         if ( ( GX_JID == 8 ) || ( GX_JID == 0 ) )
         {
            Z357Atributos_TabelaNom = A357Atributos_TabelaNom;
            Z190Tabela_SistemaCod = A190Tabela_SistemaCod;
         }
         if ( ( GX_JID == 9 ) || ( GX_JID == 0 ) )
         {
         }
         if ( GX_JID == -7 )
         {
            Z176Atributos_Codigo = A176Atributos_Codigo;
            Z177Atributos_Nome = A177Atributos_Nome;
            Z390Atributos_Detalhes = A390Atributos_Detalhes;
            Z179Atributos_Descricao = A179Atributos_Descricao;
            Z178Atributos_TipoDados = A178Atributos_TipoDados;
            Z400Atributos_PK = A400Atributos_PK;
            Z401Atributos_FK = A401Atributos_FK;
            Z180Atributos_Ativo = A180Atributos_Ativo;
            Z356Atributos_TabelaCod = A356Atributos_TabelaCod;
            Z747Atributos_MelhoraCod = A747Atributos_MelhoraCod;
            Z357Atributos_TabelaNom = A357Atributos_TabelaNom;
            Z190Tabela_SistemaCod = A190Tabela_SistemaCod;
         }
      }

      protected void standaloneNotModal( )
      {
         Gx_BScreen = 0;
         AV23Pgmname = "Atributos_BC";
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A180Atributos_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A180Atributos_Ativo = true;
         }
      }

      protected void Load1340( )
      {
         /* Using cursor BC00136 */
         pr_default.execute(4, new Object[] {A176Atributos_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound40 = 1;
            A177Atributos_Nome = BC00136_A177Atributos_Nome[0];
            A390Atributos_Detalhes = BC00136_A390Atributos_Detalhes[0];
            n390Atributos_Detalhes = BC00136_n390Atributos_Detalhes[0];
            A179Atributos_Descricao = BC00136_A179Atributos_Descricao[0];
            n179Atributos_Descricao = BC00136_n179Atributos_Descricao[0];
            A178Atributos_TipoDados = BC00136_A178Atributos_TipoDados[0];
            n178Atributos_TipoDados = BC00136_n178Atributos_TipoDados[0];
            A400Atributos_PK = BC00136_A400Atributos_PK[0];
            n400Atributos_PK = BC00136_n400Atributos_PK[0];
            A401Atributos_FK = BC00136_A401Atributos_FK[0];
            n401Atributos_FK = BC00136_n401Atributos_FK[0];
            A357Atributos_TabelaNom = BC00136_A357Atributos_TabelaNom[0];
            n357Atributos_TabelaNom = BC00136_n357Atributos_TabelaNom[0];
            A180Atributos_Ativo = BC00136_A180Atributos_Ativo[0];
            A356Atributos_TabelaCod = BC00136_A356Atributos_TabelaCod[0];
            A747Atributos_MelhoraCod = BC00136_A747Atributos_MelhoraCod[0];
            n747Atributos_MelhoraCod = BC00136_n747Atributos_MelhoraCod[0];
            A190Tabela_SistemaCod = BC00136_A190Tabela_SistemaCod[0];
            n190Tabela_SistemaCod = BC00136_n190Tabela_SistemaCod[0];
            ZM1340( -7) ;
         }
         pr_default.close(4);
         OnLoadActions1340( ) ;
      }

      protected void OnLoadActions1340( )
      {
         AV10WebSession.Set("Tabela_Codigo", StringUtil.Str( (decimal)(A356Atributos_TabelaCod), 6, 0));
      }

      protected void CheckExtendedTable1340( )
      {
         standaloneModal( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A177Atributos_Nome)) )
         {
            GX_msglist.addItem("Nome � obrigat�rio.", 1, "");
            AnyError = 1;
         }
         if ( ! ( ( StringUtil.StrCmp(A178Atributos_TipoDados, "N") == 0 ) || ( StringUtil.StrCmp(A178Atributos_TipoDados, "C") == 0 ) || ( StringUtil.StrCmp(A178Atributos_TipoDados, "VC") == 0 ) || ( StringUtil.StrCmp(A178Atributos_TipoDados, "D") == 0 ) || ( StringUtil.StrCmp(A178Atributos_TipoDados, "DT") == 0 ) || ( StringUtil.StrCmp(A178Atributos_TipoDados, "Bool") == 0 ) || ( StringUtil.StrCmp(A178Atributos_TipoDados, "Blob") == 0 ) || ( StringUtil.StrCmp(A178Atributos_TipoDados, "Outr") == 0 ) || String.IsNullOrEmpty(StringUtil.RTrim( A178Atributos_TipoDados)) ) )
         {
            GX_msglist.addItem("Campo Tipo de Dados fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         /* Using cursor BC00134 */
         pr_default.execute(2, new Object[] {A356Atributos_TabelaCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Atributos_Tabela'.", "ForeignKeyNotFound", 1, "ATRIBUTOS_TABELACOD");
            AnyError = 1;
         }
         A357Atributos_TabelaNom = BC00134_A357Atributos_TabelaNom[0];
         n357Atributos_TabelaNom = BC00134_n357Atributos_TabelaNom[0];
         A190Tabela_SistemaCod = BC00134_A190Tabela_SistemaCod[0];
         n190Tabela_SistemaCod = BC00134_n190Tabela_SistemaCod[0];
         pr_default.close(2);
         AV10WebSession.Set("Tabela_Codigo", StringUtil.Str( (decimal)(A356Atributos_TabelaCod), 6, 0));
         /* Using cursor BC00135 */
         pr_default.execute(3, new Object[] {n747Atributos_MelhoraCod, A747Atributos_MelhoraCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (0==A747Atributos_MelhoraCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Atributo_Atributo Melhora'.", "ForeignKeyNotFound", 1, "ATRIBUTOS_MELHORACOD");
               AnyError = 1;
            }
         }
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors1340( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey1340( )
      {
         /* Using cursor BC00137 */
         pr_default.execute(5, new Object[] {A176Atributos_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound40 = 1;
         }
         else
         {
            RcdFound40 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC00133 */
         pr_default.execute(1, new Object[] {A176Atributos_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM1340( 7) ;
            RcdFound40 = 1;
            A176Atributos_Codigo = BC00133_A176Atributos_Codigo[0];
            A177Atributos_Nome = BC00133_A177Atributos_Nome[0];
            A390Atributos_Detalhes = BC00133_A390Atributos_Detalhes[0];
            n390Atributos_Detalhes = BC00133_n390Atributos_Detalhes[0];
            A179Atributos_Descricao = BC00133_A179Atributos_Descricao[0];
            n179Atributos_Descricao = BC00133_n179Atributos_Descricao[0];
            A178Atributos_TipoDados = BC00133_A178Atributos_TipoDados[0];
            n178Atributos_TipoDados = BC00133_n178Atributos_TipoDados[0];
            A400Atributos_PK = BC00133_A400Atributos_PK[0];
            n400Atributos_PK = BC00133_n400Atributos_PK[0];
            A401Atributos_FK = BC00133_A401Atributos_FK[0];
            n401Atributos_FK = BC00133_n401Atributos_FK[0];
            A180Atributos_Ativo = BC00133_A180Atributos_Ativo[0];
            A356Atributos_TabelaCod = BC00133_A356Atributos_TabelaCod[0];
            A747Atributos_MelhoraCod = BC00133_A747Atributos_MelhoraCod[0];
            n747Atributos_MelhoraCod = BC00133_n747Atributos_MelhoraCod[0];
            Z176Atributos_Codigo = A176Atributos_Codigo;
            sMode40 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load1340( ) ;
            if ( AnyError == 1 )
            {
               RcdFound40 = 0;
               InitializeNonKey1340( ) ;
            }
            Gx_mode = sMode40;
         }
         else
         {
            RcdFound40 = 0;
            InitializeNonKey1340( ) ;
            sMode40 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode40;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1340( ) ;
         if ( RcdFound40 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_130( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency1340( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC00132 */
            pr_default.execute(0, new Object[] {A176Atributos_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Atributos"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z177Atributos_Nome, BC00132_A177Atributos_Nome[0]) != 0 ) || ( StringUtil.StrCmp(Z390Atributos_Detalhes, BC00132_A390Atributos_Detalhes[0]) != 0 ) || ( StringUtil.StrCmp(Z178Atributos_TipoDados, BC00132_A178Atributos_TipoDados[0]) != 0 ) || ( Z400Atributos_PK != BC00132_A400Atributos_PK[0] ) || ( Z401Atributos_FK != BC00132_A401Atributos_FK[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z180Atributos_Ativo != BC00132_A180Atributos_Ativo[0] ) || ( Z356Atributos_TabelaCod != BC00132_A356Atributos_TabelaCod[0] ) || ( Z747Atributos_MelhoraCod != BC00132_A747Atributos_MelhoraCod[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Atributos"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1340( )
      {
         BeforeValidate1340( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1340( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1340( 0) ;
            CheckOptimisticConcurrency1340( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1340( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1340( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC00138 */
                     pr_default.execute(6, new Object[] {A177Atributos_Nome, n390Atributos_Detalhes, A390Atributos_Detalhes, n179Atributos_Descricao, A179Atributos_Descricao, n178Atributos_TipoDados, A178Atributos_TipoDados, n400Atributos_PK, A400Atributos_PK, n401Atributos_FK, A401Atributos_FK, A180Atributos_Ativo, A356Atributos_TabelaCod, n747Atributos_MelhoraCod, A747Atributos_MelhoraCod});
                     A176Atributos_Codigo = BC00138_A176Atributos_Codigo[0];
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("Atributos") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1340( ) ;
            }
            EndLevel1340( ) ;
         }
         CloseExtendedTableCursors1340( ) ;
      }

      protected void Update1340( )
      {
         BeforeValidate1340( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1340( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1340( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1340( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1340( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC00139 */
                     pr_default.execute(7, new Object[] {A177Atributos_Nome, n390Atributos_Detalhes, A390Atributos_Detalhes, n179Atributos_Descricao, A179Atributos_Descricao, n178Atributos_TipoDados, A178Atributos_TipoDados, n400Atributos_PK, A400Atributos_PK, n401Atributos_FK, A401Atributos_FK, A180Atributos_Ativo, A356Atributos_TabelaCod, n747Atributos_MelhoraCod, A747Atributos_MelhoraCod, A176Atributos_Codigo});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("Atributos") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Atributos"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate1340( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1340( ) ;
         }
         CloseExtendedTableCursors1340( ) ;
      }

      protected void DeferredUpdate1340( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate1340( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1340( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1340( ) ;
            AfterConfirm1340( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1340( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC001310 */
                  pr_default.execute(8, new Object[] {A176Atributos_Codigo});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("Atributos") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode40 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel1340( ) ;
         Gx_mode = sMode40;
      }

      protected void OnDeleteControls1340( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC001311 */
            pr_default.execute(9, new Object[] {A356Atributos_TabelaCod});
            A357Atributos_TabelaNom = BC001311_A357Atributos_TabelaNom[0];
            n357Atributos_TabelaNom = BC001311_n357Atributos_TabelaNom[0];
            A190Tabela_SistemaCod = BC001311_A190Tabela_SistemaCod[0];
            n190Tabela_SistemaCod = BC001311_n190Tabela_SistemaCod[0];
            pr_default.close(9);
            AV10WebSession.Set("Tabela_Codigo", StringUtil.Str( (decimal)(A356Atributos_TabelaCod), 6, 0));
         }
         if ( AnyError == 0 )
         {
            /* Using cursor BC001312 */
            pr_default.execute(10, new Object[] {A176Atributos_Codigo});
            if ( (pr_default.getStatus(10) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Atributos"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(10);
            /* Using cursor BC001313 */
            pr_default.execute(11, new Object[] {A176Atributos_Codigo});
            if ( (pr_default.getStatus(11) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Item Atributos FSoftware"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(11);
            /* Using cursor BC001314 */
            pr_default.execute(12, new Object[] {A176Atributos_Codigo});
            if ( (pr_default.getStatus(12) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Atributos das Fun��es de APF"+" ("+"Funcao APFAtributos_Funcao APFAtributosMelhora"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(12);
            /* Using cursor BC001315 */
            pr_default.execute(13, new Object[] {A176Atributos_Codigo});
            if ( (pr_default.getStatus(13) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Atributos das Fun��es de APF"+" ("+"Funcao APFAtributos_Atributos"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(13);
            /* Using cursor BC001316 */
            pr_default.execute(14, new Object[] {A176Atributos_Codigo});
            if ( (pr_default.getStatus(14) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Item Atributos FMetrica"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(14);
         }
      }

      protected void EndLevel1340( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1340( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart1340( )
      {
         /* Scan By routine */
         /* Using cursor BC001317 */
         pr_default.execute(15, new Object[] {A176Atributos_Codigo});
         RcdFound40 = 0;
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound40 = 1;
            A176Atributos_Codigo = BC001317_A176Atributos_Codigo[0];
            A177Atributos_Nome = BC001317_A177Atributos_Nome[0];
            A390Atributos_Detalhes = BC001317_A390Atributos_Detalhes[0];
            n390Atributos_Detalhes = BC001317_n390Atributos_Detalhes[0];
            A179Atributos_Descricao = BC001317_A179Atributos_Descricao[0];
            n179Atributos_Descricao = BC001317_n179Atributos_Descricao[0];
            A178Atributos_TipoDados = BC001317_A178Atributos_TipoDados[0];
            n178Atributos_TipoDados = BC001317_n178Atributos_TipoDados[0];
            A400Atributos_PK = BC001317_A400Atributos_PK[0];
            n400Atributos_PK = BC001317_n400Atributos_PK[0];
            A401Atributos_FK = BC001317_A401Atributos_FK[0];
            n401Atributos_FK = BC001317_n401Atributos_FK[0];
            A357Atributos_TabelaNom = BC001317_A357Atributos_TabelaNom[0];
            n357Atributos_TabelaNom = BC001317_n357Atributos_TabelaNom[0];
            A180Atributos_Ativo = BC001317_A180Atributos_Ativo[0];
            A356Atributos_TabelaCod = BC001317_A356Atributos_TabelaCod[0];
            A747Atributos_MelhoraCod = BC001317_A747Atributos_MelhoraCod[0];
            n747Atributos_MelhoraCod = BC001317_n747Atributos_MelhoraCod[0];
            A190Tabela_SistemaCod = BC001317_A190Tabela_SistemaCod[0];
            n190Tabela_SistemaCod = BC001317_n190Tabela_SistemaCod[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext1340( )
      {
         /* Scan next routine */
         pr_default.readNext(15);
         RcdFound40 = 0;
         ScanKeyLoad1340( ) ;
      }

      protected void ScanKeyLoad1340( )
      {
         sMode40 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound40 = 1;
            A176Atributos_Codigo = BC001317_A176Atributos_Codigo[0];
            A177Atributos_Nome = BC001317_A177Atributos_Nome[0];
            A390Atributos_Detalhes = BC001317_A390Atributos_Detalhes[0];
            n390Atributos_Detalhes = BC001317_n390Atributos_Detalhes[0];
            A179Atributos_Descricao = BC001317_A179Atributos_Descricao[0];
            n179Atributos_Descricao = BC001317_n179Atributos_Descricao[0];
            A178Atributos_TipoDados = BC001317_A178Atributos_TipoDados[0];
            n178Atributos_TipoDados = BC001317_n178Atributos_TipoDados[0];
            A400Atributos_PK = BC001317_A400Atributos_PK[0];
            n400Atributos_PK = BC001317_n400Atributos_PK[0];
            A401Atributos_FK = BC001317_A401Atributos_FK[0];
            n401Atributos_FK = BC001317_n401Atributos_FK[0];
            A357Atributos_TabelaNom = BC001317_A357Atributos_TabelaNom[0];
            n357Atributos_TabelaNom = BC001317_n357Atributos_TabelaNom[0];
            A180Atributos_Ativo = BC001317_A180Atributos_Ativo[0];
            A356Atributos_TabelaCod = BC001317_A356Atributos_TabelaCod[0];
            A747Atributos_MelhoraCod = BC001317_A747Atributos_MelhoraCod[0];
            n747Atributos_MelhoraCod = BC001317_n747Atributos_MelhoraCod[0];
            A190Tabela_SistemaCod = BC001317_A190Tabela_SistemaCod[0];
            n190Tabela_SistemaCod = BC001317_n190Tabela_SistemaCod[0];
         }
         Gx_mode = sMode40;
      }

      protected void ScanKeyEnd1340( )
      {
         pr_default.close(15);
      }

      protected void AfterConfirm1340( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1340( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1340( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1340( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1340( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1340( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes1340( )
      {
      }

      protected void AddRow1340( )
      {
         VarsToRow40( bcAtributos) ;
      }

      protected void ReadRow1340( )
      {
         RowToVars40( bcAtributos, 1) ;
      }

      protected void InitializeNonKey1340( )
      {
         A177Atributos_Nome = "";
         A390Atributos_Detalhes = "";
         n390Atributos_Detalhes = false;
         A179Atributos_Descricao = "";
         n179Atributos_Descricao = false;
         A178Atributos_TipoDados = "";
         n178Atributos_TipoDados = false;
         A400Atributos_PK = false;
         n400Atributos_PK = false;
         A401Atributos_FK = false;
         n401Atributos_FK = false;
         A356Atributos_TabelaCod = 0;
         A357Atributos_TabelaNom = "";
         n357Atributos_TabelaNom = false;
         A190Tabela_SistemaCod = 0;
         n190Tabela_SistemaCod = false;
         A747Atributos_MelhoraCod = 0;
         n747Atributos_MelhoraCod = false;
         A180Atributos_Ativo = true;
         Z177Atributos_Nome = "";
         Z390Atributos_Detalhes = "";
         Z178Atributos_TipoDados = "";
         Z400Atributos_PK = false;
         Z401Atributos_FK = false;
         Z180Atributos_Ativo = false;
         Z356Atributos_TabelaCod = 0;
         Z747Atributos_MelhoraCod = 0;
      }

      protected void InitAll1340( )
      {
         A176Atributos_Codigo = 0;
         InitializeNonKey1340( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A180Atributos_Ativo = i180Atributos_Ativo;
      }

      public void VarsToRow40( SdtAtributos obj40 )
      {
         obj40.gxTpr_Mode = Gx_mode;
         obj40.gxTpr_Atributos_nome = A177Atributos_Nome;
         obj40.gxTpr_Atributos_detalhes = A390Atributos_Detalhes;
         obj40.gxTpr_Atributos_descricao = A179Atributos_Descricao;
         obj40.gxTpr_Atributos_tipodados = A178Atributos_TipoDados;
         obj40.gxTpr_Atributos_pk = A400Atributos_PK;
         obj40.gxTpr_Atributos_fk = A401Atributos_FK;
         obj40.gxTpr_Atributos_tabelacod = A356Atributos_TabelaCod;
         obj40.gxTpr_Atributos_tabelanom = A357Atributos_TabelaNom;
         obj40.gxTpr_Tabela_sistemacod = A190Tabela_SistemaCod;
         obj40.gxTpr_Atributos_melhoracod = A747Atributos_MelhoraCod;
         obj40.gxTpr_Atributos_ativo = A180Atributos_Ativo;
         obj40.gxTpr_Atributos_codigo = A176Atributos_Codigo;
         obj40.gxTpr_Atributos_codigo_Z = Z176Atributos_Codigo;
         obj40.gxTpr_Atributos_nome_Z = Z177Atributos_Nome;
         obj40.gxTpr_Atributos_detalhes_Z = Z390Atributos_Detalhes;
         obj40.gxTpr_Atributos_tipodados_Z = Z178Atributos_TipoDados;
         obj40.gxTpr_Atributos_pk_Z = Z400Atributos_PK;
         obj40.gxTpr_Atributos_fk_Z = Z401Atributos_FK;
         obj40.gxTpr_Atributos_tabelacod_Z = Z356Atributos_TabelaCod;
         obj40.gxTpr_Atributos_tabelanom_Z = Z357Atributos_TabelaNom;
         obj40.gxTpr_Tabela_sistemacod_Z = Z190Tabela_SistemaCod;
         obj40.gxTpr_Atributos_melhoracod_Z = Z747Atributos_MelhoraCod;
         obj40.gxTpr_Atributos_ativo_Z = Z180Atributos_Ativo;
         obj40.gxTpr_Atributos_detalhes_N = (short)(Convert.ToInt16(n390Atributos_Detalhes));
         obj40.gxTpr_Atributos_descricao_N = (short)(Convert.ToInt16(n179Atributos_Descricao));
         obj40.gxTpr_Atributos_tipodados_N = (short)(Convert.ToInt16(n178Atributos_TipoDados));
         obj40.gxTpr_Atributos_pk_N = (short)(Convert.ToInt16(n400Atributos_PK));
         obj40.gxTpr_Atributos_fk_N = (short)(Convert.ToInt16(n401Atributos_FK));
         obj40.gxTpr_Atributos_tabelanom_N = (short)(Convert.ToInt16(n357Atributos_TabelaNom));
         obj40.gxTpr_Tabela_sistemacod_N = (short)(Convert.ToInt16(n190Tabela_SistemaCod));
         obj40.gxTpr_Atributos_melhoracod_N = (short)(Convert.ToInt16(n747Atributos_MelhoraCod));
         obj40.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow40( SdtAtributos obj40 )
      {
         obj40.gxTpr_Atributos_codigo = A176Atributos_Codigo;
         return  ;
      }

      public void RowToVars40( SdtAtributos obj40 ,
                               int forceLoad )
      {
         Gx_mode = obj40.gxTpr_Mode;
         A177Atributos_Nome = obj40.gxTpr_Atributos_nome;
         A390Atributos_Detalhes = obj40.gxTpr_Atributos_detalhes;
         n390Atributos_Detalhes = false;
         A179Atributos_Descricao = obj40.gxTpr_Atributos_descricao;
         n179Atributos_Descricao = false;
         A178Atributos_TipoDados = obj40.gxTpr_Atributos_tipodados;
         n178Atributos_TipoDados = false;
         A400Atributos_PK = obj40.gxTpr_Atributos_pk;
         n400Atributos_PK = false;
         A401Atributos_FK = obj40.gxTpr_Atributos_fk;
         n401Atributos_FK = false;
         A356Atributos_TabelaCod = obj40.gxTpr_Atributos_tabelacod;
         A357Atributos_TabelaNom = obj40.gxTpr_Atributos_tabelanom;
         n357Atributos_TabelaNom = false;
         A190Tabela_SistemaCod = obj40.gxTpr_Tabela_sistemacod;
         n190Tabela_SistemaCod = false;
         A747Atributos_MelhoraCod = obj40.gxTpr_Atributos_melhoracod;
         n747Atributos_MelhoraCod = false;
         A180Atributos_Ativo = obj40.gxTpr_Atributos_ativo;
         A176Atributos_Codigo = obj40.gxTpr_Atributos_codigo;
         Z176Atributos_Codigo = obj40.gxTpr_Atributos_codigo_Z;
         Z177Atributos_Nome = obj40.gxTpr_Atributos_nome_Z;
         Z390Atributos_Detalhes = obj40.gxTpr_Atributos_detalhes_Z;
         Z178Atributos_TipoDados = obj40.gxTpr_Atributos_tipodados_Z;
         Z400Atributos_PK = obj40.gxTpr_Atributos_pk_Z;
         Z401Atributos_FK = obj40.gxTpr_Atributos_fk_Z;
         Z356Atributos_TabelaCod = obj40.gxTpr_Atributos_tabelacod_Z;
         Z357Atributos_TabelaNom = obj40.gxTpr_Atributos_tabelanom_Z;
         Z190Tabela_SistemaCod = obj40.gxTpr_Tabela_sistemacod_Z;
         Z747Atributos_MelhoraCod = obj40.gxTpr_Atributos_melhoracod_Z;
         Z180Atributos_Ativo = obj40.gxTpr_Atributos_ativo_Z;
         n390Atributos_Detalhes = (bool)(Convert.ToBoolean(obj40.gxTpr_Atributos_detalhes_N));
         n179Atributos_Descricao = (bool)(Convert.ToBoolean(obj40.gxTpr_Atributos_descricao_N));
         n178Atributos_TipoDados = (bool)(Convert.ToBoolean(obj40.gxTpr_Atributos_tipodados_N));
         n400Atributos_PK = (bool)(Convert.ToBoolean(obj40.gxTpr_Atributos_pk_N));
         n401Atributos_FK = (bool)(Convert.ToBoolean(obj40.gxTpr_Atributos_fk_N));
         n357Atributos_TabelaNom = (bool)(Convert.ToBoolean(obj40.gxTpr_Atributos_tabelanom_N));
         n190Tabela_SistemaCod = (bool)(Convert.ToBoolean(obj40.gxTpr_Tabela_sistemacod_N));
         n747Atributos_MelhoraCod = (bool)(Convert.ToBoolean(obj40.gxTpr_Atributos_melhoracod_N));
         Gx_mode = obj40.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A176Atributos_Codigo = (int)getParm(obj,0);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey1340( ) ;
         ScanKeyStart1340( ) ;
         if ( RcdFound40 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z176Atributos_Codigo = A176Atributos_Codigo;
         }
         ZM1340( -7) ;
         OnLoadActions1340( ) ;
         AddRow1340( ) ;
         ScanKeyEnd1340( ) ;
         if ( RcdFound40 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars40( bcAtributos, 0) ;
         ScanKeyStart1340( ) ;
         if ( RcdFound40 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z176Atributos_Codigo = A176Atributos_Codigo;
         }
         ZM1340( -7) ;
         OnLoadActions1340( ) ;
         AddRow1340( ) ;
         ScanKeyEnd1340( ) ;
         if ( RcdFound40 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars40( bcAtributos, 0) ;
         nKeyPressed = 1;
         GetKey1340( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert1340( ) ;
         }
         else
         {
            if ( RcdFound40 == 1 )
            {
               if ( A176Atributos_Codigo != Z176Atributos_Codigo )
               {
                  A176Atributos_Codigo = Z176Atributos_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update1340( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A176Atributos_Codigo != Z176Atributos_Codigo )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert1340( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert1340( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow40( bcAtributos) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars40( bcAtributos, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey1340( ) ;
         if ( RcdFound40 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A176Atributos_Codigo != Z176Atributos_Codigo )
            {
               A176Atributos_Codigo = Z176Atributos_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A176Atributos_Codigo != Z176Atributos_Codigo )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(9);
         context.RollbackDataStores( "Atributos_BC");
         VarsToRow40( bcAtributos) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcAtributos.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcAtributos.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcAtributos )
         {
            bcAtributos = (SdtAtributos)(sdt);
            if ( StringUtil.StrCmp(bcAtributos.gxTpr_Mode, "") == 0 )
            {
               bcAtributos.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow40( bcAtributos) ;
            }
            else
            {
               RowToVars40( bcAtributos, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcAtributos.gxTpr_Mode, "") == 0 )
            {
               bcAtributos.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars40( bcAtributos, 1) ;
         return  ;
      }

      public SdtAtributos Atributos_BC
      {
         get {
            return bcAtributos ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(9);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV23Pgmname = "";
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z177Atributos_Nome = "";
         A177Atributos_Nome = "";
         Z390Atributos_Detalhes = "";
         A390Atributos_Detalhes = "";
         Z178Atributos_TipoDados = "";
         A178Atributos_TipoDados = "";
         Z357Atributos_TabelaNom = "";
         A357Atributos_TabelaNom = "";
         Z179Atributos_Descricao = "";
         A179Atributos_Descricao = "";
         BC00136_A176Atributos_Codigo = new int[1] ;
         BC00136_A177Atributos_Nome = new String[] {""} ;
         BC00136_A390Atributos_Detalhes = new String[] {""} ;
         BC00136_n390Atributos_Detalhes = new bool[] {false} ;
         BC00136_A179Atributos_Descricao = new String[] {""} ;
         BC00136_n179Atributos_Descricao = new bool[] {false} ;
         BC00136_A178Atributos_TipoDados = new String[] {""} ;
         BC00136_n178Atributos_TipoDados = new bool[] {false} ;
         BC00136_A400Atributos_PK = new bool[] {false} ;
         BC00136_n400Atributos_PK = new bool[] {false} ;
         BC00136_A401Atributos_FK = new bool[] {false} ;
         BC00136_n401Atributos_FK = new bool[] {false} ;
         BC00136_A357Atributos_TabelaNom = new String[] {""} ;
         BC00136_n357Atributos_TabelaNom = new bool[] {false} ;
         BC00136_A180Atributos_Ativo = new bool[] {false} ;
         BC00136_A356Atributos_TabelaCod = new int[1] ;
         BC00136_A747Atributos_MelhoraCod = new int[1] ;
         BC00136_n747Atributos_MelhoraCod = new bool[] {false} ;
         BC00136_A190Tabela_SistemaCod = new int[1] ;
         BC00136_n190Tabela_SistemaCod = new bool[] {false} ;
         BC00134_A357Atributos_TabelaNom = new String[] {""} ;
         BC00134_n357Atributos_TabelaNom = new bool[] {false} ;
         BC00134_A190Tabela_SistemaCod = new int[1] ;
         BC00134_n190Tabela_SistemaCod = new bool[] {false} ;
         BC00135_A747Atributos_MelhoraCod = new int[1] ;
         BC00135_n747Atributos_MelhoraCod = new bool[] {false} ;
         BC00137_A176Atributos_Codigo = new int[1] ;
         BC00133_A176Atributos_Codigo = new int[1] ;
         BC00133_A177Atributos_Nome = new String[] {""} ;
         BC00133_A390Atributos_Detalhes = new String[] {""} ;
         BC00133_n390Atributos_Detalhes = new bool[] {false} ;
         BC00133_A179Atributos_Descricao = new String[] {""} ;
         BC00133_n179Atributos_Descricao = new bool[] {false} ;
         BC00133_A178Atributos_TipoDados = new String[] {""} ;
         BC00133_n178Atributos_TipoDados = new bool[] {false} ;
         BC00133_A400Atributos_PK = new bool[] {false} ;
         BC00133_n400Atributos_PK = new bool[] {false} ;
         BC00133_A401Atributos_FK = new bool[] {false} ;
         BC00133_n401Atributos_FK = new bool[] {false} ;
         BC00133_A180Atributos_Ativo = new bool[] {false} ;
         BC00133_A356Atributos_TabelaCod = new int[1] ;
         BC00133_A747Atributos_MelhoraCod = new int[1] ;
         BC00133_n747Atributos_MelhoraCod = new bool[] {false} ;
         sMode40 = "";
         BC00132_A176Atributos_Codigo = new int[1] ;
         BC00132_A177Atributos_Nome = new String[] {""} ;
         BC00132_A390Atributos_Detalhes = new String[] {""} ;
         BC00132_n390Atributos_Detalhes = new bool[] {false} ;
         BC00132_A179Atributos_Descricao = new String[] {""} ;
         BC00132_n179Atributos_Descricao = new bool[] {false} ;
         BC00132_A178Atributos_TipoDados = new String[] {""} ;
         BC00132_n178Atributos_TipoDados = new bool[] {false} ;
         BC00132_A400Atributos_PK = new bool[] {false} ;
         BC00132_n400Atributos_PK = new bool[] {false} ;
         BC00132_A401Atributos_FK = new bool[] {false} ;
         BC00132_n401Atributos_FK = new bool[] {false} ;
         BC00132_A180Atributos_Ativo = new bool[] {false} ;
         BC00132_A356Atributos_TabelaCod = new int[1] ;
         BC00132_A747Atributos_MelhoraCod = new int[1] ;
         BC00132_n747Atributos_MelhoraCod = new bool[] {false} ;
         BC00138_A176Atributos_Codigo = new int[1] ;
         BC001311_A357Atributos_TabelaNom = new String[] {""} ;
         BC001311_n357Atributos_TabelaNom = new bool[] {false} ;
         BC001311_A190Tabela_SistemaCod = new int[1] ;
         BC001311_n190Tabela_SistemaCod = new bool[] {false} ;
         BC001312_A747Atributos_MelhoraCod = new int[1] ;
         BC001312_n747Atributos_MelhoraCod = new bool[] {false} ;
         BC001313_A224ContagemItem_Lancamento = new int[1] ;
         BC001313_A379ContagemItem_AtributosCod = new int[1] ;
         BC001314_A165FuncaoAPF_Codigo = new int[1] ;
         BC001314_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         BC001315_A165FuncaoAPF_Codigo = new int[1] ;
         BC001315_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         BC001316_A261ContagemItemAtributosFMetrica_ItemContagemLan = new int[1] ;
         BC001316_A249ContagemItem_FMetricasAtributosCod = new int[1] ;
         BC001317_A176Atributos_Codigo = new int[1] ;
         BC001317_A177Atributos_Nome = new String[] {""} ;
         BC001317_A390Atributos_Detalhes = new String[] {""} ;
         BC001317_n390Atributos_Detalhes = new bool[] {false} ;
         BC001317_A179Atributos_Descricao = new String[] {""} ;
         BC001317_n179Atributos_Descricao = new bool[] {false} ;
         BC001317_A178Atributos_TipoDados = new String[] {""} ;
         BC001317_n178Atributos_TipoDados = new bool[] {false} ;
         BC001317_A400Atributos_PK = new bool[] {false} ;
         BC001317_n400Atributos_PK = new bool[] {false} ;
         BC001317_A401Atributos_FK = new bool[] {false} ;
         BC001317_n401Atributos_FK = new bool[] {false} ;
         BC001317_A357Atributos_TabelaNom = new String[] {""} ;
         BC001317_n357Atributos_TabelaNom = new bool[] {false} ;
         BC001317_A180Atributos_Ativo = new bool[] {false} ;
         BC001317_A356Atributos_TabelaCod = new int[1] ;
         BC001317_A747Atributos_MelhoraCod = new int[1] ;
         BC001317_n747Atributos_MelhoraCod = new bool[] {false} ;
         BC001317_A190Tabela_SistemaCod = new int[1] ;
         BC001317_n190Tabela_SistemaCod = new bool[] {false} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.atributos_bc__default(),
            new Object[][] {
                new Object[] {
               BC00132_A176Atributos_Codigo, BC00132_A177Atributos_Nome, BC00132_A390Atributos_Detalhes, BC00132_n390Atributos_Detalhes, BC00132_A179Atributos_Descricao, BC00132_n179Atributos_Descricao, BC00132_A178Atributos_TipoDados, BC00132_n178Atributos_TipoDados, BC00132_A400Atributos_PK, BC00132_n400Atributos_PK,
               BC00132_A401Atributos_FK, BC00132_n401Atributos_FK, BC00132_A180Atributos_Ativo, BC00132_A356Atributos_TabelaCod, BC00132_A747Atributos_MelhoraCod, BC00132_n747Atributos_MelhoraCod
               }
               , new Object[] {
               BC00133_A176Atributos_Codigo, BC00133_A177Atributos_Nome, BC00133_A390Atributos_Detalhes, BC00133_n390Atributos_Detalhes, BC00133_A179Atributos_Descricao, BC00133_n179Atributos_Descricao, BC00133_A178Atributos_TipoDados, BC00133_n178Atributos_TipoDados, BC00133_A400Atributos_PK, BC00133_n400Atributos_PK,
               BC00133_A401Atributos_FK, BC00133_n401Atributos_FK, BC00133_A180Atributos_Ativo, BC00133_A356Atributos_TabelaCod, BC00133_A747Atributos_MelhoraCod, BC00133_n747Atributos_MelhoraCod
               }
               , new Object[] {
               BC00134_A357Atributos_TabelaNom, BC00134_n357Atributos_TabelaNom, BC00134_A190Tabela_SistemaCod, BC00134_n190Tabela_SistemaCod
               }
               , new Object[] {
               BC00135_A747Atributos_MelhoraCod
               }
               , new Object[] {
               BC00136_A176Atributos_Codigo, BC00136_A177Atributos_Nome, BC00136_A390Atributos_Detalhes, BC00136_n390Atributos_Detalhes, BC00136_A179Atributos_Descricao, BC00136_n179Atributos_Descricao, BC00136_A178Atributos_TipoDados, BC00136_n178Atributos_TipoDados, BC00136_A400Atributos_PK, BC00136_n400Atributos_PK,
               BC00136_A401Atributos_FK, BC00136_n401Atributos_FK, BC00136_A357Atributos_TabelaNom, BC00136_n357Atributos_TabelaNom, BC00136_A180Atributos_Ativo, BC00136_A356Atributos_TabelaCod, BC00136_A747Atributos_MelhoraCod, BC00136_n747Atributos_MelhoraCod, BC00136_A190Tabela_SistemaCod, BC00136_n190Tabela_SistemaCod
               }
               , new Object[] {
               BC00137_A176Atributos_Codigo
               }
               , new Object[] {
               BC00138_A176Atributos_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC001311_A357Atributos_TabelaNom, BC001311_n357Atributos_TabelaNom, BC001311_A190Tabela_SistemaCod, BC001311_n190Tabela_SistemaCod
               }
               , new Object[] {
               BC001312_A747Atributos_MelhoraCod
               }
               , new Object[] {
               BC001313_A224ContagemItem_Lancamento, BC001313_A379ContagemItem_AtributosCod
               }
               , new Object[] {
               BC001314_A165FuncaoAPF_Codigo, BC001314_A364FuncaoAPFAtributos_AtributosCod
               }
               , new Object[] {
               BC001315_A165FuncaoAPF_Codigo, BC001315_A364FuncaoAPFAtributos_AtributosCod
               }
               , new Object[] {
               BC001316_A261ContagemItemAtributosFMetrica_ItemContagemLan, BC001316_A249ContagemItem_FMetricasAtributosCod
               }
               , new Object[] {
               BC001317_A176Atributos_Codigo, BC001317_A177Atributos_Nome, BC001317_A390Atributos_Detalhes, BC001317_n390Atributos_Detalhes, BC001317_A179Atributos_Descricao, BC001317_n179Atributos_Descricao, BC001317_A178Atributos_TipoDados, BC001317_n178Atributos_TipoDados, BC001317_A400Atributos_PK, BC001317_n400Atributos_PK,
               BC001317_A401Atributos_FK, BC001317_n401Atributos_FK, BC001317_A357Atributos_TabelaNom, BC001317_n357Atributos_TabelaNom, BC001317_A180Atributos_Ativo, BC001317_A356Atributos_TabelaCod, BC001317_A747Atributos_MelhoraCod, BC001317_n747Atributos_MelhoraCod, BC001317_A190Tabela_SistemaCod, BC001317_n190Tabela_SistemaCod
               }
            }
         );
         Z180Atributos_Ativo = true;
         A180Atributos_Ativo = true;
         i180Atributos_Ativo = true;
         AV23Pgmname = "Atributos_BC";
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: E12132 */
         E12132 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short RcdFound40 ;
      private int trnEnded ;
      private int Z176Atributos_Codigo ;
      private int A176Atributos_Codigo ;
      private int AV24GXV1 ;
      private int AV13Insert_Atributos_TabelaCod ;
      private int AV22Insert_Atributos_MelhoraCod ;
      private int AV7Atributos_Codigo ;
      private int A190Tabela_SistemaCod ;
      private int Z356Atributos_TabelaCod ;
      private int A356Atributos_TabelaCod ;
      private int Z747Atributos_MelhoraCod ;
      private int A747Atributos_MelhoraCod ;
      private int Z190Tabela_SistemaCod ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String AV23Pgmname ;
      private String Z177Atributos_Nome ;
      private String A177Atributos_Nome ;
      private String Z390Atributos_Detalhes ;
      private String A390Atributos_Detalhes ;
      private String Z178Atributos_TipoDados ;
      private String A178Atributos_TipoDados ;
      private String Z357Atributos_TabelaNom ;
      private String A357Atributos_TabelaNom ;
      private String sMode40 ;
      private bool Z400Atributos_PK ;
      private bool A400Atributos_PK ;
      private bool Z401Atributos_FK ;
      private bool A401Atributos_FK ;
      private bool Z180Atributos_Ativo ;
      private bool A180Atributos_Ativo ;
      private bool n390Atributos_Detalhes ;
      private bool n179Atributos_Descricao ;
      private bool n178Atributos_TipoDados ;
      private bool n400Atributos_PK ;
      private bool n401Atributos_FK ;
      private bool n357Atributos_TabelaNom ;
      private bool n747Atributos_MelhoraCod ;
      private bool n190Tabela_SistemaCod ;
      private bool Gx_longc ;
      private bool i180Atributos_Ativo ;
      private String Z179Atributos_Descricao ;
      private String A179Atributos_Descricao ;
      private IGxSession AV10WebSession ;
      private SdtAtributos bcAtributos ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC00136_A176Atributos_Codigo ;
      private String[] BC00136_A177Atributos_Nome ;
      private String[] BC00136_A390Atributos_Detalhes ;
      private bool[] BC00136_n390Atributos_Detalhes ;
      private String[] BC00136_A179Atributos_Descricao ;
      private bool[] BC00136_n179Atributos_Descricao ;
      private String[] BC00136_A178Atributos_TipoDados ;
      private bool[] BC00136_n178Atributos_TipoDados ;
      private bool[] BC00136_A400Atributos_PK ;
      private bool[] BC00136_n400Atributos_PK ;
      private bool[] BC00136_A401Atributos_FK ;
      private bool[] BC00136_n401Atributos_FK ;
      private String[] BC00136_A357Atributos_TabelaNom ;
      private bool[] BC00136_n357Atributos_TabelaNom ;
      private bool[] BC00136_A180Atributos_Ativo ;
      private int[] BC00136_A356Atributos_TabelaCod ;
      private int[] BC00136_A747Atributos_MelhoraCod ;
      private bool[] BC00136_n747Atributos_MelhoraCod ;
      private int[] BC00136_A190Tabela_SistemaCod ;
      private bool[] BC00136_n190Tabela_SistemaCod ;
      private String[] BC00134_A357Atributos_TabelaNom ;
      private bool[] BC00134_n357Atributos_TabelaNom ;
      private int[] BC00134_A190Tabela_SistemaCod ;
      private bool[] BC00134_n190Tabela_SistemaCod ;
      private int[] BC00135_A747Atributos_MelhoraCod ;
      private bool[] BC00135_n747Atributos_MelhoraCod ;
      private int[] BC00137_A176Atributos_Codigo ;
      private int[] BC00133_A176Atributos_Codigo ;
      private String[] BC00133_A177Atributos_Nome ;
      private String[] BC00133_A390Atributos_Detalhes ;
      private bool[] BC00133_n390Atributos_Detalhes ;
      private String[] BC00133_A179Atributos_Descricao ;
      private bool[] BC00133_n179Atributos_Descricao ;
      private String[] BC00133_A178Atributos_TipoDados ;
      private bool[] BC00133_n178Atributos_TipoDados ;
      private bool[] BC00133_A400Atributos_PK ;
      private bool[] BC00133_n400Atributos_PK ;
      private bool[] BC00133_A401Atributos_FK ;
      private bool[] BC00133_n401Atributos_FK ;
      private bool[] BC00133_A180Atributos_Ativo ;
      private int[] BC00133_A356Atributos_TabelaCod ;
      private int[] BC00133_A747Atributos_MelhoraCod ;
      private bool[] BC00133_n747Atributos_MelhoraCod ;
      private int[] BC00132_A176Atributos_Codigo ;
      private String[] BC00132_A177Atributos_Nome ;
      private String[] BC00132_A390Atributos_Detalhes ;
      private bool[] BC00132_n390Atributos_Detalhes ;
      private String[] BC00132_A179Atributos_Descricao ;
      private bool[] BC00132_n179Atributos_Descricao ;
      private String[] BC00132_A178Atributos_TipoDados ;
      private bool[] BC00132_n178Atributos_TipoDados ;
      private bool[] BC00132_A400Atributos_PK ;
      private bool[] BC00132_n400Atributos_PK ;
      private bool[] BC00132_A401Atributos_FK ;
      private bool[] BC00132_n401Atributos_FK ;
      private bool[] BC00132_A180Atributos_Ativo ;
      private int[] BC00132_A356Atributos_TabelaCod ;
      private int[] BC00132_A747Atributos_MelhoraCod ;
      private bool[] BC00132_n747Atributos_MelhoraCod ;
      private int[] BC00138_A176Atributos_Codigo ;
      private String[] BC001311_A357Atributos_TabelaNom ;
      private bool[] BC001311_n357Atributos_TabelaNom ;
      private int[] BC001311_A190Tabela_SistemaCod ;
      private bool[] BC001311_n190Tabela_SistemaCod ;
      private int[] BC001312_A747Atributos_MelhoraCod ;
      private bool[] BC001312_n747Atributos_MelhoraCod ;
      private int[] BC001313_A224ContagemItem_Lancamento ;
      private int[] BC001313_A379ContagemItem_AtributosCod ;
      private int[] BC001314_A165FuncaoAPF_Codigo ;
      private int[] BC001314_A364FuncaoAPFAtributos_AtributosCod ;
      private int[] BC001315_A165FuncaoAPF_Codigo ;
      private int[] BC001315_A364FuncaoAPFAtributos_AtributosCod ;
      private int[] BC001316_A261ContagemItemAtributosFMetrica_ItemContagemLan ;
      private int[] BC001316_A249ContagemItem_FMetricasAtributosCod ;
      private int[] BC001317_A176Atributos_Codigo ;
      private String[] BC001317_A177Atributos_Nome ;
      private String[] BC001317_A390Atributos_Detalhes ;
      private bool[] BC001317_n390Atributos_Detalhes ;
      private String[] BC001317_A179Atributos_Descricao ;
      private bool[] BC001317_n179Atributos_Descricao ;
      private String[] BC001317_A178Atributos_TipoDados ;
      private bool[] BC001317_n178Atributos_TipoDados ;
      private bool[] BC001317_A400Atributos_PK ;
      private bool[] BC001317_n400Atributos_PK ;
      private bool[] BC001317_A401Atributos_FK ;
      private bool[] BC001317_n401Atributos_FK ;
      private String[] BC001317_A357Atributos_TabelaNom ;
      private bool[] BC001317_n357Atributos_TabelaNom ;
      private bool[] BC001317_A180Atributos_Ativo ;
      private int[] BC001317_A356Atributos_TabelaCod ;
      private int[] BC001317_A747Atributos_MelhoraCod ;
      private bool[] BC001317_n747Atributos_MelhoraCod ;
      private int[] BC001317_A190Tabela_SistemaCod ;
      private bool[] BC001317_n190Tabela_SistemaCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class atributos_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC00136 ;
          prmBC00136 = new Object[] {
          new Object[] {"@Atributos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00134 ;
          prmBC00134 = new Object[] {
          new Object[] {"@Atributos_TabelaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00135 ;
          prmBC00135 = new Object[] {
          new Object[] {"@Atributos_MelhoraCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00137 ;
          prmBC00137 = new Object[] {
          new Object[] {"@Atributos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00133 ;
          prmBC00133 = new Object[] {
          new Object[] {"@Atributos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00132 ;
          prmBC00132 = new Object[] {
          new Object[] {"@Atributos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00138 ;
          prmBC00138 = new Object[] {
          new Object[] {"@Atributos_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Atributos_Detalhes",SqlDbType.Char,10,0} ,
          new Object[] {"@Atributos_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Atributos_TipoDados",SqlDbType.Char,4,0} ,
          new Object[] {"@Atributos_PK",SqlDbType.Bit,4,0} ,
          new Object[] {"@Atributos_FK",SqlDbType.Bit,4,0} ,
          new Object[] {"@Atributos_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Atributos_TabelaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Atributos_MelhoraCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00139 ;
          prmBC00139 = new Object[] {
          new Object[] {"@Atributos_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Atributos_Detalhes",SqlDbType.Char,10,0} ,
          new Object[] {"@Atributos_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Atributos_TipoDados",SqlDbType.Char,4,0} ,
          new Object[] {"@Atributos_PK",SqlDbType.Bit,4,0} ,
          new Object[] {"@Atributos_FK",SqlDbType.Bit,4,0} ,
          new Object[] {"@Atributos_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Atributos_TabelaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Atributos_MelhoraCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Atributos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001310 ;
          prmBC001310 = new Object[] {
          new Object[] {"@Atributos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001311 ;
          prmBC001311 = new Object[] {
          new Object[] {"@Atributos_TabelaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001312 ;
          prmBC001312 = new Object[] {
          new Object[] {"@Atributos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001313 ;
          prmBC001313 = new Object[] {
          new Object[] {"@Atributos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001314 ;
          prmBC001314 = new Object[] {
          new Object[] {"@Atributos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001315 ;
          prmBC001315 = new Object[] {
          new Object[] {"@Atributos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001316 ;
          prmBC001316 = new Object[] {
          new Object[] {"@Atributos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001317 ;
          prmBC001317 = new Object[] {
          new Object[] {"@Atributos_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC00132", "SELECT [Atributos_Codigo], [Atributos_Nome], [Atributos_Detalhes], [Atributos_Descricao], [Atributos_TipoDados], [Atributos_PK], [Atributos_FK], [Atributos_Ativo], [Atributos_TabelaCod] AS Atributos_TabelaCod, [Atributos_MelhoraCod] AS Atributos_MelhoraCod FROM [Atributos] WITH (UPDLOCK) WHERE [Atributos_Codigo] = @Atributos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00132,1,0,true,false )
             ,new CursorDef("BC00133", "SELECT [Atributos_Codigo], [Atributos_Nome], [Atributos_Detalhes], [Atributos_Descricao], [Atributos_TipoDados], [Atributos_PK], [Atributos_FK], [Atributos_Ativo], [Atributos_TabelaCod] AS Atributos_TabelaCod, [Atributos_MelhoraCod] AS Atributos_MelhoraCod FROM [Atributos] WITH (NOLOCK) WHERE [Atributos_Codigo] = @Atributos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00133,1,0,true,false )
             ,new CursorDef("BC00134", "SELECT [Tabela_Nome] AS Atributos_TabelaNom, [Tabela_SistemaCod] FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @Atributos_TabelaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00134,1,0,true,false )
             ,new CursorDef("BC00135", "SELECT [Atributos_Codigo] AS Atributos_MelhoraCod FROM [Atributos] WITH (NOLOCK) WHERE [Atributos_Codigo] = @Atributos_MelhoraCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00135,1,0,true,false )
             ,new CursorDef("BC00136", "SELECT TM1.[Atributos_Codigo], TM1.[Atributos_Nome], TM1.[Atributos_Detalhes], TM1.[Atributos_Descricao], TM1.[Atributos_TipoDados], TM1.[Atributos_PK], TM1.[Atributos_FK], T2.[Tabela_Nome] AS Atributos_TabelaNom, TM1.[Atributos_Ativo], TM1.[Atributos_TabelaCod] AS Atributos_TabelaCod, TM1.[Atributos_MelhoraCod] AS Atributos_MelhoraCod, T2.[Tabela_SistemaCod] FROM ([Atributos] TM1 WITH (NOLOCK) INNER JOIN [Tabela] T2 WITH (NOLOCK) ON T2.[Tabela_Codigo] = TM1.[Atributos_TabelaCod]) WHERE TM1.[Atributos_Codigo] = @Atributos_Codigo ORDER BY TM1.[Atributos_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00136,100,0,true,false )
             ,new CursorDef("BC00137", "SELECT [Atributos_Codigo] FROM [Atributos] WITH (NOLOCK) WHERE [Atributos_Codigo] = @Atributos_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00137,1,0,true,false )
             ,new CursorDef("BC00138", "INSERT INTO [Atributos]([Atributos_Nome], [Atributos_Detalhes], [Atributos_Descricao], [Atributos_TipoDados], [Atributos_PK], [Atributos_FK], [Atributos_Ativo], [Atributos_TabelaCod], [Atributos_MelhoraCod]) VALUES(@Atributos_Nome, @Atributos_Detalhes, @Atributos_Descricao, @Atributos_TipoDados, @Atributos_PK, @Atributos_FK, @Atributos_Ativo, @Atributos_TabelaCod, @Atributos_MelhoraCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmBC00138)
             ,new CursorDef("BC00139", "UPDATE [Atributos] SET [Atributos_Nome]=@Atributos_Nome, [Atributos_Detalhes]=@Atributos_Detalhes, [Atributos_Descricao]=@Atributos_Descricao, [Atributos_TipoDados]=@Atributos_TipoDados, [Atributos_PK]=@Atributos_PK, [Atributos_FK]=@Atributos_FK, [Atributos_Ativo]=@Atributos_Ativo, [Atributos_TabelaCod]=@Atributos_TabelaCod, [Atributos_MelhoraCod]=@Atributos_MelhoraCod  WHERE [Atributos_Codigo] = @Atributos_Codigo", GxErrorMask.GX_NOMASK,prmBC00139)
             ,new CursorDef("BC001310", "DELETE FROM [Atributos]  WHERE [Atributos_Codigo] = @Atributos_Codigo", GxErrorMask.GX_NOMASK,prmBC001310)
             ,new CursorDef("BC001311", "SELECT [Tabela_Nome] AS Atributos_TabelaNom, [Tabela_SistemaCod] FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @Atributos_TabelaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001311,1,0,true,false )
             ,new CursorDef("BC001312", "SELECT TOP 1 [Atributos_Codigo] AS Atributos_MelhoraCod FROM [Atributos] WITH (NOLOCK) WHERE [Atributos_MelhoraCod] = @Atributos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001312,1,0,true,true )
             ,new CursorDef("BC001313", "SELECT TOP 1 [ContagemItem_Lancamento], [ContagemItem_AtributosCod] FROM [ContagemItemAtributosFSoftware] WITH (NOLOCK) WHERE [ContagemItem_AtributosCod] = @Atributos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001313,1,0,true,true )
             ,new CursorDef("BC001314", "SELECT TOP 1 [FuncaoAPF_Codigo], [FuncaoAPFAtributos_AtributosCod] FROM [FuncoesAPFAtributos] WITH (NOLOCK) WHERE [FuncaoAPFAtributos_MelhoraCod] = @Atributos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001314,1,0,true,true )
             ,new CursorDef("BC001315", "SELECT TOP 1 [FuncaoAPF_Codigo], [FuncaoAPFAtributos_AtributosCod] FROM [FuncoesAPFAtributos] WITH (NOLOCK) WHERE [FuncaoAPFAtributos_AtributosCod] = @Atributos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001315,1,0,true,true )
             ,new CursorDef("BC001316", "SELECT TOP 1 [ContagemItemAtributosFMetrica_ItemContagemLan], [ContagemItem_FMetricasAtributosCod] FROM [ContagemItemAtributosFMetrica] WITH (NOLOCK) WHERE [ContagemItem_FMetricasAtributosCod] = @Atributos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001316,1,0,true,true )
             ,new CursorDef("BC001317", "SELECT TM1.[Atributos_Codigo], TM1.[Atributos_Nome], TM1.[Atributos_Detalhes], TM1.[Atributos_Descricao], TM1.[Atributos_TipoDados], TM1.[Atributos_PK], TM1.[Atributos_FK], T2.[Tabela_Nome] AS Atributos_TabelaNom, TM1.[Atributos_Ativo], TM1.[Atributos_TabelaCod] AS Atributos_TabelaCod, TM1.[Atributos_MelhoraCod] AS Atributos_MelhoraCod, T2.[Tabela_SistemaCod] FROM ([Atributos] TM1 WITH (NOLOCK) INNER JOIN [Tabela] T2 WITH (NOLOCK) ON T2.[Tabela_Codigo] = TM1.[Atributos_TabelaCod]) WHERE TM1.[Atributos_Codigo] = @Atributos_Codigo ORDER BY TM1.[Atributos_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC001317,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((bool[]) buf[8])[0] = rslt.getBool(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((bool[]) buf[10])[0] = rslt.getBool(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((bool[]) buf[12])[0] = rslt.getBool(8) ;
                ((int[]) buf[13])[0] = rslt.getInt(9) ;
                ((int[]) buf[14])[0] = rslt.getInt(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((bool[]) buf[8])[0] = rslt.getBool(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((bool[]) buf[10])[0] = rslt.getBool(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((bool[]) buf[12])[0] = rslt.getBool(8) ;
                ((int[]) buf[13])[0] = rslt.getInt(9) ;
                ((int[]) buf[14])[0] = rslt.getInt(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((bool[]) buf[8])[0] = rslt.getBool(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((bool[]) buf[10])[0] = rslt.getBool(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((bool[]) buf[14])[0] = rslt.getBool(9) ;
                ((int[]) buf[15])[0] = rslt.getInt(10) ;
                ((int[]) buf[16])[0] = rslt.getInt(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((int[]) buf[18])[0] = rslt.getInt(12) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((bool[]) buf[8])[0] = rslt.getBool(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((bool[]) buf[10])[0] = rslt.getBool(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((bool[]) buf[14])[0] = rslt.getBool(9) ;
                ((int[]) buf[15])[0] = rslt.getInt(10) ;
                ((int[]) buf[16])[0] = rslt.getInt(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((int[]) buf[18])[0] = rslt.getInt(12) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(5, (bool)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(6, (bool)parms[10]);
                }
                stmt.SetParameter(7, (bool)parms[11]);
                stmt.SetParameter(8, (int)parms[12]);
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[14]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(5, (bool)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(6, (bool)parms[10]);
                }
                stmt.SetParameter(7, (bool)parms[11]);
                stmt.SetParameter(8, (int)parms[12]);
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[14]);
                }
                stmt.SetParameter(10, (int)parms[15]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
