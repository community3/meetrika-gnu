/*
               File: PRC_SDT_DemandasSolicitadas
        Description: PRC_SDT_DemandasSolicitadas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 8/14/2019 2:14:38.61
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class aprc_sdt_demandassolicitadas : GXProcedure
   {
      public aprc_sdt_demandassolicitadas( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public aprc_sdt_demandassolicitadas( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         initialize();
         executePrivate();
      }

      public void executeSubmit( )
      {
         aprc_sdt_demandassolicitadas objaprc_sdt_demandassolicitadas;
         objaprc_sdt_demandassolicitadas = new aprc_sdt_demandassolicitadas();
         objaprc_sdt_demandassolicitadas.context.SetSubmitInitialConfig(context);
         objaprc_sdt_demandassolicitadas.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objaprc_sdt_demandassolicitadas);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((aprc_sdt_demandassolicitadas)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV476Usuario_UserGamGuid = "576a2771-a6f9-4db2-8226-3acccbad9b8b";
         AV480AreaTrabalho_Codigo = 1032;
         /* Using cursor P00YE2 */
         pr_default.execute(0, new Object[] {AV476Usuario_UserGamGuid});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A341Usuario_UserGamGuid = P00YE2_A341Usuario_UserGamGuid[0];
            A1Usuario_Codigo = P00YE2_A1Usuario_Codigo[0];
            A291Usuario_EhContratada = P00YE2_A291Usuario_EhContratada[0];
            A292Usuario_EhContratante = P00YE2_A292Usuario_EhContratante[0];
            A538Usuario_EhGestor = P00YE2_A538Usuario_EhGestor[0];
            OV479Usuario_EhGestor = AV479Usuario_EhGestor;
            AV441Usuario_Codigo = A1Usuario_Codigo;
            AV477Usuario_EhContratada = A291Usuario_EhContratada;
            AV478Usuario_EhContratante = A292Usuario_EhContratante;
            AV479Usuario_EhGestor = A538Usuario_EhGestor;
            GXt_char1 = "dados do usuario: " + context.localUtil.Format( (decimal)(AV441Usuario_Codigo), "ZZZZZ9") + ":" + StringUtil.BoolToStr( AV477Usuario_EhContratada) + ":" + StringUtil.BoolToStr( AV478Usuario_EhContratante) + ":" + StringUtil.BoolToStr( AV479Usuario_EhGestor);
            new geralog(context ).execute( ref  GXt_char1) ;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         if ( AV478Usuario_EhContratante )
         {
            GXt_char1 = "&Usuario_EhContratante = True - vai listar todas as contratadas que � gestor";
            new geralog(context ).execute( ref  GXt_char1) ;
            AV481ContratadaCollection.Clear();
            /* Using cursor P00YE3 */
            pr_default.execute(1, new Object[] {AV441Usuario_Codigo, AV480AreaTrabalho_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A1078ContratoGestor_ContratoCod = P00YE3_A1078ContratoGestor_ContratoCod[0];
               A1079ContratoGestor_UsuarioCod = P00YE3_A1079ContratoGestor_UsuarioCod[0];
               A1446ContratoGestor_ContratadaAreaCod = P00YE3_A1446ContratoGestor_ContratadaAreaCod[0];
               n1446ContratoGestor_ContratadaAreaCod = P00YE3_n1446ContratoGestor_ContratadaAreaCod[0];
               A1223ContratoGestor_ContratadaSigla = P00YE3_A1223ContratoGestor_ContratadaSigla[0];
               n1223ContratoGestor_ContratadaSigla = P00YE3_n1223ContratoGestor_ContratadaSigla[0];
               A1136ContratoGestor_ContratadaCod = P00YE3_A1136ContratoGestor_ContratadaCod[0];
               n1136ContratoGestor_ContratadaCod = P00YE3_n1136ContratoGestor_ContratadaCod[0];
               A1446ContratoGestor_ContratadaAreaCod = P00YE3_A1446ContratoGestor_ContratadaAreaCod[0];
               n1446ContratoGestor_ContratadaAreaCod = P00YE3_n1446ContratoGestor_ContratadaAreaCod[0];
               A1136ContratoGestor_ContratadaCod = P00YE3_A1136ContratoGestor_ContratadaCod[0];
               n1136ContratoGestor_ContratadaCod = P00YE3_n1136ContratoGestor_ContratadaCod[0];
               A1223ContratoGestor_ContratadaSigla = P00YE3_A1223ContratoGestor_ContratadaSigla[0];
               n1223ContratoGestor_ContratadaSigla = P00YE3_n1223ContratoGestor_ContratadaSigla[0];
               GXt_char1 = "Contratada: " + context.localUtil.Format( (decimal)(A39Contratada_Codigo), "ZZZZZ9") + ":" + StringUtil.Trim( A1223ContratoGestor_ContratadaSigla);
               new geralog(context ).execute( ref  GXt_char1) ;
               AV481ContratadaCollection.Add(A1136ContratoGestor_ContratadaCod, 0);
               pr_default.readNext(1);
            }
            pr_default.close(1);
         }
         if ( AV477Usuario_EhContratada )
         {
         }
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A490ContagemResultado_ContratadaCod ,
                                              AV481ContratadaCollection ,
                                              AV478Usuario_EhContratante ,
                                              A484ContagemResultado_StatusDmn },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor P00YE5 */
         pr_default.execute(2);
         while ( (pr_default.getStatus(2) != 101) )
         {
            A489ContagemResultado_SistemaCod = P00YE5_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00YE5_n489ContagemResultado_SistemaCod[0];
            A1553ContagemResultado_CntSrvCod = P00YE5_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00YE5_n1553ContagemResultado_CntSrvCod[0];
            A601ContagemResultado_Servico = P00YE5_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00YE5_n601ContagemResultado_Servico[0];
            A508ContagemResultado_Owner = P00YE5_A508ContagemResultado_Owner[0];
            A490ContagemResultado_ContratadaCod = P00YE5_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00YE5_n490ContagemResultado_ContratadaCod[0];
            A484ContagemResultado_StatusDmn = P00YE5_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00YE5_n484ContagemResultado_StatusDmn[0];
            A457ContagemResultado_Demanda = P00YE5_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00YE5_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = P00YE5_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00YE5_n493ContagemResultado_DemandaFM[0];
            A494ContagemResultado_Descricao = P00YE5_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P00YE5_n494ContagemResultado_Descricao[0];
            A801ContagemResultado_ServicoSigla = P00YE5_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00YE5_n801ContagemResultado_ServicoSigla[0];
            A471ContagemResultado_DataDmn = P00YE5_A471ContagemResultado_DataDmn[0];
            A1351ContagemResultado_DataPrevista = P00YE5_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = P00YE5_n1351ContagemResultado_DataPrevista[0];
            A472ContagemResultado_DataEntrega = P00YE5_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = P00YE5_n472ContagemResultado_DataEntrega[0];
            A509ContagemrResultado_SistemaSigla = P00YE5_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00YE5_n509ContagemrResultado_SistemaSigla[0];
            A1046ContagemResultado_Agrupador = P00YE5_A1046ContagemResultado_Agrupador[0];
            n1046ContagemResultado_Agrupador = P00YE5_n1046ContagemResultado_Agrupador[0];
            A2118ContagemResultado_Owner_Identificao = P00YE5_A2118ContagemResultado_Owner_Identificao[0];
            n2118ContagemResultado_Owner_Identificao = P00YE5_n2118ContagemResultado_Owner_Identificao[0];
            A456ContagemResultado_Codigo = P00YE5_A456ContagemResultado_Codigo[0];
            A509ContagemrResultado_SistemaSigla = P00YE5_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00YE5_n509ContagemrResultado_SistemaSigla[0];
            A601ContagemResultado_Servico = P00YE5_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00YE5_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P00YE5_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00YE5_n801ContagemResultado_ServicoSigla[0];
            A2118ContagemResultado_Owner_Identificao = P00YE5_A2118ContagemResultado_Owner_Identificao[0];
            n2118ContagemResultado_Owner_Identificao = P00YE5_n2118ContagemResultado_Owner_Identificao[0];
            AV483SDT_WS_DemandaItens = new SdtSDT_WS_Demandas(context);
            AV483SDT_WS_DemandaItens.gxTpr_Contagemresultado_osreferencia = (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : StringUtil.Trim( A493ContagemResultado_DemandaFM))+(String.IsNullOrEmpty(StringUtil.RTrim( A457ContagemResultado_Demanda)) ? "" : "|"+StringUtil.Trim( A457ContagemResultado_Demanda));
            AV483SDT_WS_DemandaItens.gxTpr_Contagemresultado_descricao = StringUtil.Trim( A494ContagemResultado_Descricao);
            AV483SDT_WS_DemandaItens.gxTpr_Contagemresultado_servicosigla = StringUtil.Trim( A801ContagemResultado_ServicoSigla);
            AV483SDT_WS_DemandaItens.gxTpr_Contagemresultado_statusdmndescricao = gxdomainstatusdemanda.getDescription(context,A484ContagemResultado_StatusDmn);
            AV483SDT_WS_DemandaItens.gxTpr_Contagemresultado_datadmn = A471ContagemResultado_DataDmn;
            AV483SDT_WS_DemandaItens.gxTpr_Contagemresultado_dataprevista = A1351ContagemResultado_DataPrevista;
            AV483SDT_WS_DemandaItens.gxTpr_Contagemresultado_dataentrega = A472ContagemResultado_DataEntrega;
            AV483SDT_WS_DemandaItens.gxTpr_Contagemrresultado_sistemasigla = StringUtil.Trim( A509ContagemrResultado_SistemaSigla);
            AV483SDT_WS_DemandaItens.gxTpr_Contagemresultado_owner_identificao = StringUtil.Trim( A2118ContagemResultado_Owner_Identificao);
            AV483SDT_WS_DemandaItens.gxTpr_Contagemresultado_agrupador = StringUtil.Trim( A1046ContagemResultado_Agrupador);
            AV484SDT_WS_Demandas.Add(AV483SDT_WS_DemandaItens, 0);
            pr_default.readNext(2);
         }
         pr_default.close(2);
         GXt_char1 = "";
         new geralog(context ).execute( ref  GXt_char1) ;
         GXt_char2 = "";
         new geralog(context ).execute( ref  GXt_char2) ;
         GXt_char3 = "";
         new geralog(context ).execute( ref  GXt_char3) ;
         GXt_char4 = "&SDT_WS_Demandas";
         new geralog(context ).execute( ref  GXt_char4) ;
         GXt_char5 = AV484SDT_WS_Demandas.ToXml(false, true, "SDT_WS_DemandasCollection", "Demanda");
         new geralog(context ).execute( ref  GXt_char5) ;
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV476Usuario_UserGamGuid = "";
         scmdbuf = "";
         P00YE2_A341Usuario_UserGamGuid = new String[] {""} ;
         P00YE2_A1Usuario_Codigo = new int[1] ;
         P00YE2_A291Usuario_EhContratada = new bool[] {false} ;
         P00YE2_A292Usuario_EhContratante = new bool[] {false} ;
         P00YE2_A538Usuario_EhGestor = new bool[] {false} ;
         A341Usuario_UserGamGuid = "";
         AV479Usuario_EhGestor = false;
         AV481ContratadaCollection = new GxSimpleCollection();
         P00YE3_A1078ContratoGestor_ContratoCod = new int[1] ;
         P00YE3_A1079ContratoGestor_UsuarioCod = new int[1] ;
         P00YE3_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         P00YE3_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         P00YE3_A1223ContratoGestor_ContratadaSigla = new String[] {""} ;
         P00YE3_n1223ContratoGestor_ContratadaSigla = new bool[] {false} ;
         P00YE3_A1136ContratoGestor_ContratadaCod = new int[1] ;
         P00YE3_n1136ContratoGestor_ContratadaCod = new bool[] {false} ;
         A1223ContratoGestor_ContratadaSigla = "";
         A484ContagemResultado_StatusDmn = "";
         P00YE5_A489ContagemResultado_SistemaCod = new int[1] ;
         P00YE5_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00YE5_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00YE5_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00YE5_A601ContagemResultado_Servico = new int[1] ;
         P00YE5_n601ContagemResultado_Servico = new bool[] {false} ;
         P00YE5_A508ContagemResultado_Owner = new int[1] ;
         P00YE5_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00YE5_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00YE5_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00YE5_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00YE5_A457ContagemResultado_Demanda = new String[] {""} ;
         P00YE5_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00YE5_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00YE5_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00YE5_A494ContagemResultado_Descricao = new String[] {""} ;
         P00YE5_n494ContagemResultado_Descricao = new bool[] {false} ;
         P00YE5_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00YE5_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P00YE5_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00YE5_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P00YE5_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P00YE5_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P00YE5_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P00YE5_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P00YE5_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P00YE5_A1046ContagemResultado_Agrupador = new String[] {""} ;
         P00YE5_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         P00YE5_A2118ContagemResultado_Owner_Identificao = new String[] {""} ;
         P00YE5_n2118ContagemResultado_Owner_Identificao = new bool[] {false} ;
         P00YE5_A456ContagemResultado_Codigo = new int[1] ;
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         A494ContagemResultado_Descricao = "";
         A801ContagemResultado_ServicoSigla = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         A509ContagemrResultado_SistemaSigla = "";
         A1046ContagemResultado_Agrupador = "";
         A2118ContagemResultado_Owner_Identificao = "";
         AV483SDT_WS_DemandaItens = new SdtSDT_WS_Demandas(context);
         AV484SDT_WS_Demandas = new GxObjectCollection( context, "Demanda", "Demanda", "SdtSDT_WS_Demandas", "GeneXus.Programs");
         GXt_char1 = "";
         GXt_char2 = "";
         GXt_char3 = "";
         GXt_char4 = "";
         GXt_char5 = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.aprc_sdt_demandassolicitadas__default(),
            new Object[][] {
                new Object[] {
               P00YE2_A341Usuario_UserGamGuid, P00YE2_A1Usuario_Codigo, P00YE2_A291Usuario_EhContratada, P00YE2_A292Usuario_EhContratante, P00YE2_A538Usuario_EhGestor
               }
               , new Object[] {
               P00YE3_A1078ContratoGestor_ContratoCod, P00YE3_A1079ContratoGestor_UsuarioCod, P00YE3_A1446ContratoGestor_ContratadaAreaCod, P00YE3_n1446ContratoGestor_ContratadaAreaCod, P00YE3_A1223ContratoGestor_ContratadaSigla, P00YE3_n1223ContratoGestor_ContratadaSigla, P00YE3_A1136ContratoGestor_ContratadaCod, P00YE3_n1136ContratoGestor_ContratadaCod
               }
               , new Object[] {
               P00YE5_A489ContagemResultado_SistemaCod, P00YE5_n489ContagemResultado_SistemaCod, P00YE5_A1553ContagemResultado_CntSrvCod, P00YE5_n1553ContagemResultado_CntSrvCod, P00YE5_A601ContagemResultado_Servico, P00YE5_n601ContagemResultado_Servico, P00YE5_A508ContagemResultado_Owner, P00YE5_A490ContagemResultado_ContratadaCod, P00YE5_n490ContagemResultado_ContratadaCod, P00YE5_A484ContagemResultado_StatusDmn,
               P00YE5_n484ContagemResultado_StatusDmn, P00YE5_A457ContagemResultado_Demanda, P00YE5_n457ContagemResultado_Demanda, P00YE5_A493ContagemResultado_DemandaFM, P00YE5_n493ContagemResultado_DemandaFM, P00YE5_A494ContagemResultado_Descricao, P00YE5_n494ContagemResultado_Descricao, P00YE5_A801ContagemResultado_ServicoSigla, P00YE5_n801ContagemResultado_ServicoSigla, P00YE5_A471ContagemResultado_DataDmn,
               P00YE5_A1351ContagemResultado_DataPrevista, P00YE5_n1351ContagemResultado_DataPrevista, P00YE5_A472ContagemResultado_DataEntrega, P00YE5_n472ContagemResultado_DataEntrega, P00YE5_A509ContagemrResultado_SistemaSigla, P00YE5_n509ContagemrResultado_SistemaSigla, P00YE5_A1046ContagemResultado_Agrupador, P00YE5_n1046ContagemResultado_Agrupador, P00YE5_A2118ContagemResultado_Owner_Identificao, P00YE5_n2118ContagemResultado_Owner_Identificao,
               P00YE5_A456ContagemResultado_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV480AreaTrabalho_Codigo ;
      private int A1Usuario_Codigo ;
      private int AV441Usuario_Codigo ;
      private int A1078ContratoGestor_ContratoCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int A1446ContratoGestor_ContratadaAreaCod ;
      private int A1136ContratoGestor_ContratadaCod ;
      private int A39Contratada_Codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A489ContagemResultado_SistemaCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A601ContagemResultado_Servico ;
      private int A508ContagemResultado_Owner ;
      private int A456ContagemResultado_Codigo ;
      private String AV476Usuario_UserGamGuid ;
      private String scmdbuf ;
      private String A341Usuario_UserGamGuid ;
      private String A1223ContratoGestor_ContratadaSigla ;
      private String A484ContagemResultado_StatusDmn ;
      private String A801ContagemResultado_ServicoSigla ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String A1046ContagemResultado_Agrupador ;
      private String GXt_char1 ;
      private String GXt_char2 ;
      private String GXt_char3 ;
      private String GXt_char4 ;
      private String GXt_char5 ;
      private DateTime A1351ContagemResultado_DataPrevista ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private bool A291Usuario_EhContratada ;
      private bool A292Usuario_EhContratante ;
      private bool A538Usuario_EhGestor ;
      private bool OV479Usuario_EhGestor ;
      private bool AV479Usuario_EhGestor ;
      private bool AV477Usuario_EhContratada ;
      private bool AV478Usuario_EhContratante ;
      private bool n1446ContratoGestor_ContratadaAreaCod ;
      private bool n1223ContratoGestor_ContratadaSigla ;
      private bool n1136ContratoGestor_ContratadaCod ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n457ContagemResultado_Demanda ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n494ContagemResultado_Descricao ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool n1351ContagemResultado_DataPrevista ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool n1046ContagemResultado_Agrupador ;
      private bool n2118ContagemResultado_Owner_Identificao ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String A494ContagemResultado_Descricao ;
      private String A2118ContagemResultado_Owner_Identificao ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00YE2_A341Usuario_UserGamGuid ;
      private int[] P00YE2_A1Usuario_Codigo ;
      private bool[] P00YE2_A291Usuario_EhContratada ;
      private bool[] P00YE2_A292Usuario_EhContratante ;
      private bool[] P00YE2_A538Usuario_EhGestor ;
      private int[] P00YE3_A1078ContratoGestor_ContratoCod ;
      private int[] P00YE3_A1079ContratoGestor_UsuarioCod ;
      private int[] P00YE3_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] P00YE3_n1446ContratoGestor_ContratadaAreaCod ;
      private String[] P00YE3_A1223ContratoGestor_ContratadaSigla ;
      private bool[] P00YE3_n1223ContratoGestor_ContratadaSigla ;
      private int[] P00YE3_A1136ContratoGestor_ContratadaCod ;
      private bool[] P00YE3_n1136ContratoGestor_ContratadaCod ;
      private int[] P00YE5_A489ContagemResultado_SistemaCod ;
      private bool[] P00YE5_n489ContagemResultado_SistemaCod ;
      private int[] P00YE5_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00YE5_n1553ContagemResultado_CntSrvCod ;
      private int[] P00YE5_A601ContagemResultado_Servico ;
      private bool[] P00YE5_n601ContagemResultado_Servico ;
      private int[] P00YE5_A508ContagemResultado_Owner ;
      private int[] P00YE5_A490ContagemResultado_ContratadaCod ;
      private bool[] P00YE5_n490ContagemResultado_ContratadaCod ;
      private String[] P00YE5_A484ContagemResultado_StatusDmn ;
      private bool[] P00YE5_n484ContagemResultado_StatusDmn ;
      private String[] P00YE5_A457ContagemResultado_Demanda ;
      private bool[] P00YE5_n457ContagemResultado_Demanda ;
      private String[] P00YE5_A493ContagemResultado_DemandaFM ;
      private bool[] P00YE5_n493ContagemResultado_DemandaFM ;
      private String[] P00YE5_A494ContagemResultado_Descricao ;
      private bool[] P00YE5_n494ContagemResultado_Descricao ;
      private String[] P00YE5_A801ContagemResultado_ServicoSigla ;
      private bool[] P00YE5_n801ContagemResultado_ServicoSigla ;
      private DateTime[] P00YE5_A471ContagemResultado_DataDmn ;
      private DateTime[] P00YE5_A1351ContagemResultado_DataPrevista ;
      private bool[] P00YE5_n1351ContagemResultado_DataPrevista ;
      private DateTime[] P00YE5_A472ContagemResultado_DataEntrega ;
      private bool[] P00YE5_n472ContagemResultado_DataEntrega ;
      private String[] P00YE5_A509ContagemrResultado_SistemaSigla ;
      private bool[] P00YE5_n509ContagemrResultado_SistemaSigla ;
      private String[] P00YE5_A1046ContagemResultado_Agrupador ;
      private bool[] P00YE5_n1046ContagemResultado_Agrupador ;
      private String[] P00YE5_A2118ContagemResultado_Owner_Identificao ;
      private bool[] P00YE5_n2118ContagemResultado_Owner_Identificao ;
      private int[] P00YE5_A456ContagemResultado_Codigo ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV481ContratadaCollection ;
      [ObjectCollection(ItemType=typeof( SdtSDT_WS_Demandas ))]
      private IGxCollection AV484SDT_WS_Demandas ;
      private SdtSDT_WS_Demandas AV483SDT_WS_DemandaItens ;
   }

   public class aprc_sdt_demandassolicitadas__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00YE5( IGxContext context ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             IGxCollection AV481ContratadaCollection ,
                                             bool AV478Usuario_EhContratante ,
                                             String A484ContagemResultado_StatusDmn )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T3.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_Owner], T1.[ContagemResultado_ContratadaCod], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Descricao], T4.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_DataPrevista], T1.[ContagemResultado_DataEntrega], T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T1.[ContagemResultado_Agrupador], COALESCE( T5.[ContagemResultado_Owner_Identificao], '') AS ContagemResultado_Owner_Identificao, T1.[ContagemResultado_Codigo] FROM (((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T4 WITH (NOLOCK) ON T4.[Servico_Codigo] = T3.[Servico_Codigo]) LEFT JOIN (SELECT T7.[Pessoa_Nome] AS ContagemResultado_Owner_Identificao, T6.[Usuario_Codigo] FROM ([Usuario] T6 WITH (NOLOCK) INNER JOIN [Pessoa] T7 WITH (NOLOCK) ON T7.[Pessoa_Codigo] = T6.[Usuario_PessoaCod]) ) T5 ON T5.[Usuario_Codigo] = T1.[ContagemResultado_Owner])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultado_StatusDmn] = 'S' or T1.[ContagemResultado_StatusDmn] = 'J' or T1.[ContagemResultado_StatusDmn] = 'T' or T1.[ContagemResultado_StatusDmn] = 'G' or T1.[ContagemResultado_StatusDmn] = 'D')";
         if ( AV478Usuario_EhContratante )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV481ContratadaCollection, "T1.[ContagemResultado_ContratadaCod] IN (", ")") + ")";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Codigo]";
         GXv_Object6[0] = scmdbuf;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 2 :
                     return conditional_P00YE5(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00YE2 ;
          prmP00YE2 = new Object[] {
          new Object[] {"@AV476Usuario_UserGamGuid",SqlDbType.Char,40,0}
          } ;
          Object[] prmP00YE3 ;
          prmP00YE3 = new Object[] {
          new Object[] {"@AV441Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV480AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YE5 ;
          prmP00YE5 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("P00YE2", "SELECT [Usuario_UserGamGuid], [Usuario_Codigo], [Usuario_EhContratada], [Usuario_EhContratante], [Usuario_EhGestor] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_UserGamGuid] = RTRIM(LTRIM(@AV476Usuario_UserGamGuid)) ORDER BY [Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YE2,100,0,true,false )
             ,new CursorDef("P00YE3", "SELECT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod, T3.[Contratada_Sigla] AS ContratoGestor_ContratadaSigla, T2.[Contratada_Codigo] AS ContratoGestor_ContratadaCod FROM (([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) WHERE (T1.[ContratoGestor_UsuarioCod] = @AV441Usuario_Codigo) AND (T2.[Contrato_AreaTrabalhoCod] = @AV480AreaTrabalho_Codigo) ORDER BY T1.[ContratoGestor_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YE3,100,0,true,false )
             ,new CursorDef("P00YE5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YE5,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 40) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((bool[]) buf[4])[0] = rslt.getBool(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((String[]) buf[17])[0] = rslt.getString(10, 15) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((DateTime[]) buf[19])[0] = rslt.getGXDate(11) ;
                ((DateTime[]) buf[20])[0] = rslt.getGXDateTime(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((DateTime[]) buf[22])[0] = rslt.getGXDate(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((String[]) buf[24])[0] = rslt.getString(14, 25) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((String[]) buf[26])[0] = rslt.getString(15, 15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((String[]) buf[28])[0] = rslt.getVarchar(16) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((int[]) buf[30])[0] = rslt.getInt(17) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
