/*
               File: PRC_InserirSDTBaseline
        Description: Inserir SDTBaseline
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:56.26
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class aprc_inserirsdtbaseline : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            if ( ! entryPointCalled )
            {
               AV76Sistema_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV80Sistema_Sigla = GetNextPar( );
                  AV91FileName = GetNextPar( );
                  AV92Aba = GetNextPar( );
                  AV93Criar = (bool)(BooleanUtil.Val(GetNextPar( )));
               }
            }
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public aprc_inserirsdtbaseline( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public aprc_inserirsdtbaseline( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Sistema_Codigo ,
                           String aP1_Sistema_Sigla ,
                           String aP2_FileName ,
                           String aP3_Aba ,
                           ref bool aP4_Criar )
      {
         this.AV76Sistema_Codigo = aP0_Sistema_Codigo;
         this.AV80Sistema_Sigla = aP1_Sistema_Sigla;
         this.AV91FileName = aP2_FileName;
         this.AV92Aba = aP3_Aba;
         this.AV93Criar = aP4_Criar;
         initialize();
         executePrivate();
         aP4_Criar=this.AV93Criar;
      }

      public bool executeUdp( int aP0_Sistema_Codigo ,
                              String aP1_Sistema_Sigla ,
                              String aP2_FileName ,
                              String aP3_Aba )
      {
         this.AV76Sistema_Codigo = aP0_Sistema_Codigo;
         this.AV80Sistema_Sigla = aP1_Sistema_Sigla;
         this.AV91FileName = aP2_FileName;
         this.AV92Aba = aP3_Aba;
         this.AV93Criar = aP4_Criar;
         initialize();
         executePrivate();
         aP4_Criar=this.AV93Criar;
         return AV93Criar ;
      }

      public void executeSubmit( int aP0_Sistema_Codigo ,
                                 String aP1_Sistema_Sigla ,
                                 String aP2_FileName ,
                                 String aP3_Aba ,
                                 ref bool aP4_Criar )
      {
         aprc_inserirsdtbaseline objaprc_inserirsdtbaseline;
         objaprc_inserirsdtbaseline = new aprc_inserirsdtbaseline();
         objaprc_inserirsdtbaseline.AV76Sistema_Codigo = aP0_Sistema_Codigo;
         objaprc_inserirsdtbaseline.AV80Sistema_Sigla = aP1_Sistema_Sigla;
         objaprc_inserirsdtbaseline.AV91FileName = aP2_FileName;
         objaprc_inserirsdtbaseline.AV92Aba = aP3_Aba;
         objaprc_inserirsdtbaseline.AV93Criar = aP4_Criar;
         objaprc_inserirsdtbaseline.context.SetSubmitInitialConfig(context);
         objaprc_inserirsdtbaseline.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objaprc_inserirsdtbaseline);
         aP4_Criar=this.AV93Criar;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((aprc_inserirsdtbaseline)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 6;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 1, 256, 16834, 11909, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*6));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV72WWPContext) ;
            AV51SDT_Baseline.FromXml(AV71WebSession.Get("SDTBaseline"), "");
            AV66NomeArq = "Arquivo: " + AV91FileName;
            AV66NomeArq = AV66NomeArq + (String.IsNullOrEmpty(StringUtil.RTrim( AV92Aba)) ? "" : " (aba "+AV92Aba+")");
            AV30EmailText = StringUtil.NewLine( ) + StringUtil.NewLine( ) + "MEETRIKA - Sistema de Gest�o de Contratos de TI" + StringUtil.NewLine( ) + StringUtil.NewLine( );
            AV30EmailText = AV30EmailText + "Prezado(a) gestor," + StringUtil.NewLine( ) + StringUtil.NewLine( );
            AV35i = 0;
            AV75TipoExterna = "EESECE";
            AV101GXV1 = 1;
            while ( AV101GXV1 <= AV51SDT_Baseline.Count )
            {
               AV50SDT_Funcao = ((SdtSDT_Baselines_Funcao)AV51SDT_Baseline.Item(AV101GXV1));
               AV14Codigo = AV50SDT_Funcao.gxTpr_Codigo;
               if ( StringUtil.StringSearch( AV75TipoExterna, AV50SDT_Funcao.gxTpr_Tipo, 1) > 0 )
               {
                  if ( StringUtil.StrCmp(AV50SDT_Funcao.gxTpr_Tipo, "CE") == 0 )
                  {
                     AV82TipoFuncaoAPF = "CE";
                  }
                  else if ( StringUtil.StrCmp(AV50SDT_Funcao.gxTpr_Tipo, "EE") == 0 )
                  {
                     AV82TipoFuncaoAPF = "EE";
                  }
                  else if ( StringUtil.StrCmp(AV50SDT_Funcao.gxTpr_Tipo, "SE") == 0 )
                  {
                     AV82TipoFuncaoAPF = "SE";
                  }
                  if ( (0==AV14Codigo) )
                  {
                     /* Execute user subroutine: 'NEWFUNCAOAPF' */
                     S141 ();
                     if ( returnInSub )
                     {
                        this.cleanup();
                        if (true) return;
                     }
                  }
                  else
                  {
                     AV102GXLvl37 = 0;
                     /* Using cursor P008D2 */
                     pr_default.execute(0, new Object[] {AV14Codigo});
                     while ( (pr_default.getStatus(0) != 101) )
                     {
                        A1257FuncaoAPFPreImp_Sequencial = P008D2_A1257FuncaoAPFPreImp_Sequencial[0];
                        A1260FuncaoAPFPreImp_FnApfCodigo = P008D2_A1260FuncaoAPFPreImp_FnApfCodigo[0];
                        A184FuncaoAPF_Tipo = P008D2_A184FuncaoAPF_Tipo[0];
                        n184FuncaoAPF_Tipo = P008D2_n184FuncaoAPF_Tipo[0];
                        A1248FuncaoAPFPreImp_Tipo = P008D2_A1248FuncaoAPFPreImp_Tipo[0];
                        n1248FuncaoAPFPreImp_Tipo = P008D2_n1248FuncaoAPFPreImp_Tipo[0];
                        A1026FuncaoAPF_RAImp = P008D2_A1026FuncaoAPF_RAImp[0];
                        n1026FuncaoAPF_RAImp = P008D2_n1026FuncaoAPF_RAImp[0];
                        A1250FuncaoAPFPreImp_RAImp = P008D2_A1250FuncaoAPFPreImp_RAImp[0];
                        n1250FuncaoAPFPreImp_RAImp = P008D2_n1250FuncaoAPFPreImp_RAImp[0];
                        A1022FuncaoAPF_DERImp = P008D2_A1022FuncaoAPF_DERImp[0];
                        n1022FuncaoAPF_DERImp = P008D2_n1022FuncaoAPF_DERImp[0];
                        A1249FuncaoAPFPreImp_DERImp = P008D2_A1249FuncaoAPFPreImp_DERImp[0];
                        n1249FuncaoAPFPreImp_DERImp = P008D2_n1249FuncaoAPFPreImp_DERImp[0];
                        A1244FuncaoAPF_Observacao = P008D2_A1244FuncaoAPF_Observacao[0];
                        n1244FuncaoAPF_Observacao = P008D2_n1244FuncaoAPF_Observacao[0];
                        A1251FuncaoAPFPreImp_Observacao = P008D2_A1251FuncaoAPFPreImp_Observacao[0];
                        n1251FuncaoAPFPreImp_Observacao = P008D2_n1251FuncaoAPFPreImp_Observacao[0];
                        A1246FuncaoAPF_Importada = P008D2_A1246FuncaoAPF_Importada[0];
                        n1246FuncaoAPF_Importada = P008D2_n1246FuncaoAPF_Importada[0];
                        A184FuncaoAPF_Tipo = P008D2_A184FuncaoAPF_Tipo[0];
                        n184FuncaoAPF_Tipo = P008D2_n184FuncaoAPF_Tipo[0];
                        A1026FuncaoAPF_RAImp = P008D2_A1026FuncaoAPF_RAImp[0];
                        n1026FuncaoAPF_RAImp = P008D2_n1026FuncaoAPF_RAImp[0];
                        A1022FuncaoAPF_DERImp = P008D2_A1022FuncaoAPF_DERImp[0];
                        n1022FuncaoAPF_DERImp = P008D2_n1022FuncaoAPF_DERImp[0];
                        A1244FuncaoAPF_Observacao = P008D2_A1244FuncaoAPF_Observacao[0];
                        n1244FuncaoAPF_Observacao = P008D2_n1244FuncaoAPF_Observacao[0];
                        A1246FuncaoAPF_Importada = P008D2_A1246FuncaoAPF_Importada[0];
                        n1246FuncaoAPF_Importada = P008D2_n1246FuncaoAPF_Importada[0];
                        AV102GXLvl37 = 1;
                        A1248FuncaoAPFPreImp_Tipo = A184FuncaoAPF_Tipo;
                        n1248FuncaoAPFPreImp_Tipo = false;
                        A1250FuncaoAPFPreImp_RAImp = A1026FuncaoAPF_RAImp;
                        n1250FuncaoAPFPreImp_RAImp = false;
                        A1249FuncaoAPFPreImp_DERImp = A1022FuncaoAPF_DERImp;
                        n1249FuncaoAPFPreImp_DERImp = false;
                        A1251FuncaoAPFPreImp_Observacao = A1244FuncaoAPF_Observacao;
                        n1251FuncaoAPFPreImp_Observacao = false;
                        A184FuncaoAPF_Tipo = AV82TipoFuncaoAPF;
                        n184FuncaoAPF_Tipo = false;
                        A1026FuncaoAPF_RAImp = AV50SDT_Funcao.gxTpr_Rlr;
                        n1026FuncaoAPF_RAImp = false;
                        A1022FuncaoAPF_DERImp = AV50SDT_Funcao.gxTpr_Der;
                        n1022FuncaoAPF_DERImp = false;
                        A1244FuncaoAPF_Observacao = AV50SDT_Funcao.gxTpr_Observacao;
                        n1244FuncaoAPF_Observacao = false;
                        A1246FuncaoAPF_Importada = DateTimeUtil.ResetTime( DateTimeUtil.ServerDate( context, "DEFAULT") ) ;
                        n1246FuncaoAPF_Importada = false;
                        /* Exit For each command. Update data (if necessary), close cursors & exit. */
                        /* Using cursor P008D3 */
                        pr_default.execute(1, new Object[] {n184FuncaoAPF_Tipo, A184FuncaoAPF_Tipo, n1026FuncaoAPF_RAImp, A1026FuncaoAPF_RAImp, n1022FuncaoAPF_DERImp, A1022FuncaoAPF_DERImp, n1244FuncaoAPF_Observacao, A1244FuncaoAPF_Observacao, n1246FuncaoAPF_Importada, A1246FuncaoAPF_Importada, A1260FuncaoAPFPreImp_FnApfCodigo});
                        pr_default.close(1);
                        dsDefault.SmartCacheProvider.SetUpdated("FuncoesAPF") ;
                        /* Using cursor P008D4 */
                        pr_default.execute(2, new Object[] {n1248FuncaoAPFPreImp_Tipo, A1248FuncaoAPFPreImp_Tipo, n1250FuncaoAPFPreImp_RAImp, A1250FuncaoAPFPreImp_RAImp, n1249FuncaoAPFPreImp_DERImp, A1249FuncaoAPFPreImp_DERImp, n1251FuncaoAPFPreImp_Observacao, A1251FuncaoAPFPreImp_Observacao, A1260FuncaoAPFPreImp_FnApfCodigo, A1257FuncaoAPFPreImp_Sequencial});
                        pr_default.close(2);
                        dsDefault.SmartCacheProvider.SetUpdated("FuncaoAPFPreImp") ;
                        if (true) break;
                        /* Using cursor P008D5 */
                        pr_default.execute(3, new Object[] {n184FuncaoAPF_Tipo, A184FuncaoAPF_Tipo, n1026FuncaoAPF_RAImp, A1026FuncaoAPF_RAImp, n1022FuncaoAPF_DERImp, A1022FuncaoAPF_DERImp, n1244FuncaoAPF_Observacao, A1244FuncaoAPF_Observacao, n1246FuncaoAPF_Importada, A1246FuncaoAPF_Importada, A1260FuncaoAPFPreImp_FnApfCodigo});
                        pr_default.close(3);
                        dsDefault.SmartCacheProvider.SetUpdated("FuncoesAPF") ;
                        /* Using cursor P008D6 */
                        pr_default.execute(4, new Object[] {n1248FuncaoAPFPreImp_Tipo, A1248FuncaoAPFPreImp_Tipo, n1250FuncaoAPFPreImp_RAImp, A1250FuncaoAPFPreImp_RAImp, n1249FuncaoAPFPreImp_DERImp, A1249FuncaoAPFPreImp_DERImp, n1251FuncaoAPFPreImp_Observacao, A1251FuncaoAPFPreImp_Observacao, A1260FuncaoAPFPreImp_FnApfCodigo, A1257FuncaoAPFPreImp_Sequencial});
                        pr_default.close(4);
                        dsDefault.SmartCacheProvider.SetUpdated("FuncaoAPFPreImp") ;
                        /* Exiting from a For First loop. */
                        if (true) break;
                     }
                     pr_default.close(0);
                     if ( AV102GXLvl37 == 0 )
                     {
                        /* Using cursor P008D7 */
                        pr_default.execute(5, new Object[] {AV14Codigo});
                        while ( (pr_default.getStatus(5) != 101) )
                        {
                           A165FuncaoAPF_Codigo = P008D7_A165FuncaoAPF_Codigo[0];
                           A184FuncaoAPF_Tipo = P008D7_A184FuncaoAPF_Tipo[0];
                           n184FuncaoAPF_Tipo = P008D7_n184FuncaoAPF_Tipo[0];
                           A1026FuncaoAPF_RAImp = P008D7_A1026FuncaoAPF_RAImp[0];
                           n1026FuncaoAPF_RAImp = P008D7_n1026FuncaoAPF_RAImp[0];
                           A1022FuncaoAPF_DERImp = P008D7_A1022FuncaoAPF_DERImp[0];
                           n1022FuncaoAPF_DERImp = P008D7_n1022FuncaoAPF_DERImp[0];
                           A1244FuncaoAPF_Observacao = P008D7_A1244FuncaoAPF_Observacao[0];
                           n1244FuncaoAPF_Observacao = P008D7_n1244FuncaoAPF_Observacao[0];
                           A1246FuncaoAPF_Importada = P008D7_A1246FuncaoAPF_Importada[0];
                           n1246FuncaoAPF_Importada = P008D7_n1246FuncaoAPF_Importada[0];
                           AV87FuncaoAPF_Tipo = A184FuncaoAPF_Tipo;
                           AV88FuncaoAPF_RAImp = A1026FuncaoAPF_RAImp;
                           AV89FuncaoAPF_DERImp = A1022FuncaoAPF_DERImp;
                           AV90FuncaoAPF_Observacao = A1244FuncaoAPF_Observacao;
                           A184FuncaoAPF_Tipo = AV82TipoFuncaoAPF;
                           n184FuncaoAPF_Tipo = false;
                           A1026FuncaoAPF_RAImp = AV50SDT_Funcao.gxTpr_Rlr;
                           n1026FuncaoAPF_RAImp = false;
                           A1022FuncaoAPF_DERImp = AV50SDT_Funcao.gxTpr_Der;
                           n1022FuncaoAPF_DERImp = false;
                           A1244FuncaoAPF_Observacao = AV50SDT_Funcao.gxTpr_Observacao;
                           n1244FuncaoAPF_Observacao = false;
                           A1246FuncaoAPF_Importada = DateTimeUtil.ResetTime( DateTimeUtil.ServerDate( context, "DEFAULT") ) ;
                           n1246FuncaoAPF_Importada = false;
                           /* Exit For each command. Update data (if necessary), close cursors & exit. */
                           /* Using cursor P008D8 */
                           pr_default.execute(6, new Object[] {n184FuncaoAPF_Tipo, A184FuncaoAPF_Tipo, n1026FuncaoAPF_RAImp, A1026FuncaoAPF_RAImp, n1022FuncaoAPF_DERImp, A1022FuncaoAPF_DERImp, n1244FuncaoAPF_Observacao, A1244FuncaoAPF_Observacao, n1246FuncaoAPF_Importada, A1246FuncaoAPF_Importada, A165FuncaoAPF_Codigo});
                           pr_default.close(6);
                           dsDefault.SmartCacheProvider.SetUpdated("FuncoesAPF") ;
                           if (true) break;
                           /* Using cursor P008D9 */
                           pr_default.execute(7, new Object[] {n184FuncaoAPF_Tipo, A184FuncaoAPF_Tipo, n1026FuncaoAPF_RAImp, A1026FuncaoAPF_RAImp, n1022FuncaoAPF_DERImp, A1022FuncaoAPF_DERImp, n1244FuncaoAPF_Observacao, A1244FuncaoAPF_Observacao, n1246FuncaoAPF_Importada, A1246FuncaoAPF_Importada, A165FuncaoAPF_Codigo});
                           pr_default.close(7);
                           dsDefault.SmartCacheProvider.SetUpdated("FuncoesAPF") ;
                           /* Exiting from a For First loop. */
                           if (true) break;
                        }
                        pr_default.close(5);
                        /* Execute user subroutine: 'NEWAPFPREIMP' */
                        S161 ();
                        if ( returnInSub )
                        {
                           this.cleanup();
                           if (true) return;
                        }
                     }
                  }
                  AV78FncAPF = (short)(AV78FncAPF+1);
               }
               else
               {
                  if ( StringUtil.StrCmp(AV50SDT_Funcao.gxTpr_Tipo, "AIE") == 0 )
                  {
                     AV81TipoFuncaoDados = "AIE";
                  }
                  else if ( StringUtil.StrCmp(AV50SDT_Funcao.gxTpr_Tipo, "ALI") == 0 )
                  {
                     AV81TipoFuncaoDados = "ALI";
                  }
                  if ( (0==AV14Codigo) )
                  {
                     /* Execute user subroutine: 'NEWFUNCAODADOS' */
                     S111 ();
                     if ( returnInSub )
                     {
                        this.cleanup();
                        if (true) return;
                     }
                  }
                  else
                  {
                     AV104GXLvl89 = 0;
                     /* Using cursor P008D10 */
                     pr_default.execute(8, new Object[] {AV14Codigo});
                     while ( (pr_default.getStatus(8) != 101) )
                     {
                        A1259FuncaoDadosPreImp_Sequencial = P008D10_A1259FuncaoDadosPreImp_Sequencial[0];
                        A1261FuncaoDadosPreImp_FnDadosCodigo = P008D10_A1261FuncaoDadosPreImp_FnDadosCodigo[0];
                        A373FuncaoDados_Tipo = P008D10_A373FuncaoDados_Tipo[0];
                        n373FuncaoDados_Tipo = P008D10_n373FuncaoDados_Tipo[0];
                        A1253FuncaoDadosPreImp_Tipo = P008D10_A1253FuncaoDadosPreImp_Tipo[0];
                        A1025FuncaoDados_RAImp = P008D10_A1025FuncaoDados_RAImp[0];
                        n1025FuncaoDados_RAImp = P008D10_n1025FuncaoDados_RAImp[0];
                        A1255FuncaoDadosPreImp_RAImp = P008D10_A1255FuncaoDadosPreImp_RAImp[0];
                        A1024FuncaoDados_DERImp = P008D10_A1024FuncaoDados_DERImp[0];
                        n1024FuncaoDados_DERImp = P008D10_n1024FuncaoDados_DERImp[0];
                        A1254FuncaoDadosPreImp_DERImp = P008D10_A1254FuncaoDadosPreImp_DERImp[0];
                        A1245FuncaoDados_Observacao = P008D10_A1245FuncaoDados_Observacao[0];
                        n1245FuncaoDados_Observacao = P008D10_n1245FuncaoDados_Observacao[0];
                        A1256FuncaoDadosPreImp_Observacao = P008D10_A1256FuncaoDadosPreImp_Observacao[0];
                        A1258FuncaoDados_Importada = P008D10_A1258FuncaoDados_Importada[0];
                        n1258FuncaoDados_Importada = P008D10_n1258FuncaoDados_Importada[0];
                        A373FuncaoDados_Tipo = P008D10_A373FuncaoDados_Tipo[0];
                        n373FuncaoDados_Tipo = P008D10_n373FuncaoDados_Tipo[0];
                        A1025FuncaoDados_RAImp = P008D10_A1025FuncaoDados_RAImp[0];
                        n1025FuncaoDados_RAImp = P008D10_n1025FuncaoDados_RAImp[0];
                        A1024FuncaoDados_DERImp = P008D10_A1024FuncaoDados_DERImp[0];
                        n1024FuncaoDados_DERImp = P008D10_n1024FuncaoDados_DERImp[0];
                        A1245FuncaoDados_Observacao = P008D10_A1245FuncaoDados_Observacao[0];
                        n1245FuncaoDados_Observacao = P008D10_n1245FuncaoDados_Observacao[0];
                        A1258FuncaoDados_Importada = P008D10_A1258FuncaoDados_Importada[0];
                        n1258FuncaoDados_Importada = P008D10_n1258FuncaoDados_Importada[0];
                        AV104GXLvl89 = 1;
                        A1253FuncaoDadosPreImp_Tipo = A373FuncaoDados_Tipo;
                        A1255FuncaoDadosPreImp_RAImp = A1025FuncaoDados_RAImp;
                        A1254FuncaoDadosPreImp_DERImp = A1024FuncaoDados_DERImp;
                        A1256FuncaoDadosPreImp_Observacao = A1245FuncaoDados_Observacao;
                        A373FuncaoDados_Tipo = AV81TipoFuncaoDados;
                        n373FuncaoDados_Tipo = false;
                        A1025FuncaoDados_RAImp = AV50SDT_Funcao.gxTpr_Rlr;
                        n1025FuncaoDados_RAImp = false;
                        A1024FuncaoDados_DERImp = AV50SDT_Funcao.gxTpr_Der;
                        n1024FuncaoDados_DERImp = false;
                        A1245FuncaoDados_Observacao = AV50SDT_Funcao.gxTpr_Observacao;
                        n1245FuncaoDados_Observacao = false;
                        A1258FuncaoDados_Importada = DateTimeUtil.ServerDate( context, "DEFAULT");
                        n1258FuncaoDados_Importada = false;
                        /* Exit For each command. Update data (if necessary), close cursors & exit. */
                        /* Using cursor P008D11 */
                        pr_default.execute(9, new Object[] {n373FuncaoDados_Tipo, A373FuncaoDados_Tipo, n1025FuncaoDados_RAImp, A1025FuncaoDados_RAImp, n1024FuncaoDados_DERImp, A1024FuncaoDados_DERImp, n1245FuncaoDados_Observacao, A1245FuncaoDados_Observacao, n1258FuncaoDados_Importada, A1258FuncaoDados_Importada, A1261FuncaoDadosPreImp_FnDadosCodigo});
                        pr_default.close(9);
                        dsDefault.SmartCacheProvider.SetUpdated("FuncaoDados") ;
                        /* Using cursor P008D12 */
                        pr_default.execute(10, new Object[] {A1253FuncaoDadosPreImp_Tipo, A1255FuncaoDadosPreImp_RAImp, A1254FuncaoDadosPreImp_DERImp, A1256FuncaoDadosPreImp_Observacao, A1261FuncaoDadosPreImp_FnDadosCodigo, A1259FuncaoDadosPreImp_Sequencial});
                        pr_default.close(10);
                        dsDefault.SmartCacheProvider.SetUpdated("FuncaoDadosPreImp") ;
                        if (true) break;
                        /* Using cursor P008D13 */
                        pr_default.execute(11, new Object[] {n373FuncaoDados_Tipo, A373FuncaoDados_Tipo, n1025FuncaoDados_RAImp, A1025FuncaoDados_RAImp, n1024FuncaoDados_DERImp, A1024FuncaoDados_DERImp, n1245FuncaoDados_Observacao, A1245FuncaoDados_Observacao, n1258FuncaoDados_Importada, A1258FuncaoDados_Importada, A1261FuncaoDadosPreImp_FnDadosCodigo});
                        pr_default.close(11);
                        dsDefault.SmartCacheProvider.SetUpdated("FuncaoDados") ;
                        /* Using cursor P008D14 */
                        pr_default.execute(12, new Object[] {A1253FuncaoDadosPreImp_Tipo, A1255FuncaoDadosPreImp_RAImp, A1254FuncaoDadosPreImp_DERImp, A1256FuncaoDadosPreImp_Observacao, A1261FuncaoDadosPreImp_FnDadosCodigo, A1259FuncaoDadosPreImp_Sequencial});
                        pr_default.close(12);
                        dsDefault.SmartCacheProvider.SetUpdated("FuncaoDadosPreImp") ;
                        /* Exiting from a For First loop. */
                        if (true) break;
                     }
                     pr_default.close(8);
                     if ( AV104GXLvl89 == 0 )
                     {
                        /* Using cursor P008D15 */
                        pr_default.execute(13, new Object[] {AV14Codigo});
                        while ( (pr_default.getStatus(13) != 101) )
                        {
                           A368FuncaoDados_Codigo = P008D15_A368FuncaoDados_Codigo[0];
                           A373FuncaoDados_Tipo = P008D15_A373FuncaoDados_Tipo[0];
                           n373FuncaoDados_Tipo = P008D15_n373FuncaoDados_Tipo[0];
                           A1025FuncaoDados_RAImp = P008D15_A1025FuncaoDados_RAImp[0];
                           n1025FuncaoDados_RAImp = P008D15_n1025FuncaoDados_RAImp[0];
                           A1024FuncaoDados_DERImp = P008D15_A1024FuncaoDados_DERImp[0];
                           n1024FuncaoDados_DERImp = P008D15_n1024FuncaoDados_DERImp[0];
                           A1245FuncaoDados_Observacao = P008D15_A1245FuncaoDados_Observacao[0];
                           n1245FuncaoDados_Observacao = P008D15_n1245FuncaoDados_Observacao[0];
                           A1258FuncaoDados_Importada = P008D15_A1258FuncaoDados_Importada[0];
                           n1258FuncaoDados_Importada = P008D15_n1258FuncaoDados_Importada[0];
                           AV83FuncaoDados_Tipo = A373FuncaoDados_Tipo;
                           AV84FuncaoDados_RAImp = A1025FuncaoDados_RAImp;
                           AV85FuncaoDados_DERImp = A1024FuncaoDados_DERImp;
                           AV86FuncaoDados_Observacao = A1245FuncaoDados_Observacao;
                           A373FuncaoDados_Tipo = AV81TipoFuncaoDados;
                           n373FuncaoDados_Tipo = false;
                           A1025FuncaoDados_RAImp = AV50SDT_Funcao.gxTpr_Rlr;
                           n1025FuncaoDados_RAImp = false;
                           A1024FuncaoDados_DERImp = AV50SDT_Funcao.gxTpr_Der;
                           n1024FuncaoDados_DERImp = false;
                           A1245FuncaoDados_Observacao = AV50SDT_Funcao.gxTpr_Observacao;
                           n1245FuncaoDados_Observacao = false;
                           A1258FuncaoDados_Importada = DateTimeUtil.ServerDate( context, "DEFAULT");
                           n1258FuncaoDados_Importada = false;
                           /* Exit For each command. Update data (if necessary), close cursors & exit. */
                           /* Using cursor P008D16 */
                           pr_default.execute(14, new Object[] {n373FuncaoDados_Tipo, A373FuncaoDados_Tipo, n1025FuncaoDados_RAImp, A1025FuncaoDados_RAImp, n1024FuncaoDados_DERImp, A1024FuncaoDados_DERImp, n1245FuncaoDados_Observacao, A1245FuncaoDados_Observacao, n1258FuncaoDados_Importada, A1258FuncaoDados_Importada, A368FuncaoDados_Codigo});
                           pr_default.close(14);
                           dsDefault.SmartCacheProvider.SetUpdated("FuncaoDados") ;
                           if (true) break;
                           /* Using cursor P008D17 */
                           pr_default.execute(15, new Object[] {n373FuncaoDados_Tipo, A373FuncaoDados_Tipo, n1025FuncaoDados_RAImp, A1025FuncaoDados_RAImp, n1024FuncaoDados_DERImp, A1024FuncaoDados_DERImp, n1245FuncaoDados_Observacao, A1245FuncaoDados_Observacao, n1258FuncaoDados_Importada, A1258FuncaoDados_Importada, A368FuncaoDados_Codigo});
                           pr_default.close(15);
                           dsDefault.SmartCacheProvider.SetUpdated("FuncaoDados") ;
                           /* Exiting from a For First loop. */
                           if (true) break;
                        }
                        pr_default.close(13);
                        /* Execute user subroutine: 'NEWDADOSPREIMP' */
                        S131 ();
                        if ( returnInSub )
                        {
                           this.cleanup();
                           if (true) return;
                        }
                     }
                  }
                  AV77FncDados = (short)(AV77FncDados+1);
               }
               AV36Linha = AV50SDT_Funcao.gxTpr_Nome + ", " + AV50SDT_Funcao.gxTpr_Tipo + ", " + StringUtil.Trim( StringUtil.Str( (decimal)(AV50SDT_Funcao.gxTpr_Der), 4, 0)) + ", " + StringUtil.Trim( StringUtil.Str( (decimal)(AV50SDT_Funcao.gxTpr_Rlr), 4, 0));
               H8D0( false, 18) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV36Linha, "")), 17, Gx_line+0, 799, Gx_line+15, 0+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+18);
               AV101GXV1 = (int)(AV101GXV1+1);
            }
            AV67Totais = StringUtil.Trim( StringUtil.Str( (decimal)(AV78FncAPF), 4, 0)) + " fun��es de transa��o e " + StringUtil.Trim( StringUtil.Str( (decimal)(AV77FncDados), 4, 0)) + " fun��es de dados, de " + StringUtil.Trim( StringUtil.Str( (decimal)(AV51SDT_Baseline.Count), 9, 0)) + " linhas, inseridas com sucesso";
            H8D0( false, 39) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV67Totais, "")), 42, Gx_line+17, 772, Gx_line+35, 0+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+39);
            context.CommitDataStores( "PRC_InserirSDTBaseline");
            AV71WebSession.Remove("SDTBaseline");
            context.nUserReturn = 1;
            this.cleanup();
            if (true) return;
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            H8D0( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'NEWFUNCAODADOS' Routine */
         /*
            INSERT RECORD ON TABLE FuncaoDados

         */
         A369FuncaoDados_Nome = AV50SDT_Funcao.gxTpr_Nome;
         A373FuncaoDados_Tipo = AV81TipoFuncaoDados;
         n373FuncaoDados_Tipo = false;
         A1024FuncaoDados_DERImp = AV50SDT_Funcao.gxTpr_Der;
         n1024FuncaoDados_DERImp = false;
         A1025FuncaoDados_RAImp = AV50SDT_Funcao.gxTpr_Rlr;
         n1025FuncaoDados_RAImp = false;
         A370FuncaoDados_SistemaCod = AV76Sistema_Codigo;
         A1245FuncaoDados_Observacao = AV50SDT_Funcao.gxTpr_Observacao;
         n1245FuncaoDados_Observacao = false;
         A755FuncaoDados_Contar = true;
         n755FuncaoDados_Contar = false;
         A394FuncaoDados_Ativo = "A";
         A391FuncaoDados_FuncaoDadosCod = 0;
         n391FuncaoDados_FuncaoDadosCod = false;
         n391FuncaoDados_FuncaoDadosCod = true;
         A1258FuncaoDados_Importada = DateTimeUtil.ServerDate( context, "DEFAULT");
         n1258FuncaoDados_Importada = false;
         A745FuncaoDados_MelhoraCod = 0;
         n745FuncaoDados_MelhoraCod = false;
         n745FuncaoDados_MelhoraCod = true;
         A1267FuncaoDados_UpdAoImpBsln = true;
         n1267FuncaoDados_UpdAoImpBsln = false;
         /* Using cursor P008D18 */
         pr_default.execute(16, new Object[] {A369FuncaoDados_Nome, A370FuncaoDados_SistemaCod, n373FuncaoDados_Tipo, A373FuncaoDados_Tipo, n391FuncaoDados_FuncaoDadosCod, A391FuncaoDados_FuncaoDadosCod, A394FuncaoDados_Ativo, n745FuncaoDados_MelhoraCod, A745FuncaoDados_MelhoraCod, n755FuncaoDados_Contar, A755FuncaoDados_Contar, n1024FuncaoDados_DERImp, A1024FuncaoDados_DERImp, n1025FuncaoDados_RAImp, A1025FuncaoDados_RAImp, n1245FuncaoDados_Observacao, A1245FuncaoDados_Observacao, n1258FuncaoDados_Importada, A1258FuncaoDados_Importada, n1267FuncaoDados_UpdAoImpBsln, A1267FuncaoDados_UpdAoImpBsln});
         A368FuncaoDados_Codigo = P008D18_A368FuncaoDados_Codigo[0];
         pr_default.close(16);
         dsDefault.SmartCacheProvider.SetUpdated("FuncaoDados") ;
         if ( (pr_default.getStatus(16) == 1) )
         {
            context.Gx_err = 1;
            Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
         }
         else
         {
            context.Gx_err = 0;
            Gx_emsg = "";
         }
         /* End Insert */
         AV14Codigo = A368FuncaoDados_Codigo;
         if ( AV93Criar )
         {
            /* Execute user subroutine: 'CRIARRLREDERFNDADOS' */
            S121 ();
            if (returnInSub) return;
         }
      }

      protected void S121( )
      {
         /* 'CRIARRLREDERFNDADOS' Routine */
         AV94RLRs = 0;
         AV95DERs = 0;
         /* Using cursor P008D19 */
         pr_default.execute(17, new Object[] {AV14Codigo});
         while ( (pr_default.getStatus(17) != 101) )
         {
            A172Tabela_Codigo = P008D19_A172Tabela_Codigo[0];
            A368FuncaoDados_Codigo = P008D19_A368FuncaoDados_Codigo[0];
            AV94RLRs = (short)(AV94RLRs+1);
            /* Optimized group. */
            /* Using cursor P008D20 */
            pr_default.execute(18, new Object[] {A172Tabela_Codigo});
            cV95DERs = P008D20_AV95DERs[0];
            pr_default.close(18);
            AV95DERs = (short)(AV95DERs+cV95DERs*1);
            /* End optimized group. */
            pr_default.readNext(17);
         }
         pr_default.close(17);
         if ( AV50SDT_Funcao.gxTpr_Rlr > AV94RLRs )
         {
            AV35i = 1;
            while ( AV35i <= AV50SDT_Funcao.gxTpr_Rlr - AV94RLRs )
            {
               AV35i = (short)(AV35i+1);
            }
         }
      }

      protected void S131( )
      {
         /* 'NEWDADOSPREIMP' Routine */
         /*
            INSERT RECORD ON TABLE FuncaoDadosPreImp

         */
         A1261FuncaoDadosPreImp_FnDadosCodigo = AV14Codigo;
         A1259FuncaoDadosPreImp_Sequencial = 1;
         A1253FuncaoDadosPreImp_Tipo = AV83FuncaoDados_Tipo;
         A1255FuncaoDadosPreImp_RAImp = AV84FuncaoDados_RAImp;
         A1254FuncaoDadosPreImp_DERImp = AV85FuncaoDados_DERImp;
         A1256FuncaoDadosPreImp_Observacao = AV86FuncaoDados_Observacao;
         /* Using cursor P008D21 */
         pr_default.execute(19, new Object[] {A1261FuncaoDadosPreImp_FnDadosCodigo, A1259FuncaoDadosPreImp_Sequencial, A1253FuncaoDadosPreImp_Tipo, A1254FuncaoDadosPreImp_DERImp, A1255FuncaoDadosPreImp_RAImp, A1256FuncaoDadosPreImp_Observacao});
         pr_default.close(19);
         dsDefault.SmartCacheProvider.SetUpdated("FuncaoDadosPreImp") ;
         if ( (pr_default.getStatus(19) == 1) )
         {
            context.Gx_err = 1;
            Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
         }
         else
         {
            context.Gx_err = 0;
            Gx_emsg = "";
         }
         /* End Insert */
      }

      protected void S141( )
      {
         /* 'NEWFUNCAOAPF' Routine */
         /*
            INSERT RECORD ON TABLE FuncoesAPF

         */
         A166FuncaoAPF_Nome = AV50SDT_Funcao.gxTpr_Nome;
         A184FuncaoAPF_Tipo = AV82TipoFuncaoAPF;
         n184FuncaoAPF_Tipo = false;
         A1026FuncaoAPF_RAImp = AV50SDT_Funcao.gxTpr_Rlr;
         n1026FuncaoAPF_RAImp = false;
         A1022FuncaoAPF_DERImp = AV50SDT_Funcao.gxTpr_Der;
         n1022FuncaoAPF_DERImp = false;
         A360FuncaoAPF_SistemaCod = AV76Sistema_Codigo;
         n360FuncaoAPF_SistemaCod = false;
         A1244FuncaoAPF_Observacao = AV50SDT_Funcao.gxTpr_Observacao;
         n1244FuncaoAPF_Observacao = false;
         A414FuncaoAPF_Mensagem = (bool)(Convert.ToBoolean(1));
         n414FuncaoAPF_Mensagem = false;
         A413FuncaoAPF_Acao = (bool)(Convert.ToBoolean(1));
         n413FuncaoAPF_Acao = false;
         A183FuncaoAPF_Ativo = "A";
         A358FuncaoAPF_FunAPFPaiCod = 0;
         n358FuncaoAPF_FunAPFPaiCod = false;
         n358FuncaoAPF_FunAPFPaiCod = true;
         A744FuncaoAPF_MelhoraCod = 0;
         n744FuncaoAPF_MelhoraCod = false;
         n744FuncaoAPF_MelhoraCod = true;
         A359FuncaoAPF_ModuloCod = 0;
         n359FuncaoAPF_ModuloCod = false;
         n359FuncaoAPF_ModuloCod = true;
         A1246FuncaoAPF_Importada = DateTimeUtil.ResetTime( DateTimeUtil.ServerDate( context, "DEFAULT") ) ;
         n1246FuncaoAPF_Importada = false;
         A1268FuncaoAPF_UpdAoImpBsln = true;
         n1268FuncaoAPF_UpdAoImpBsln = false;
         /* Using cursor P008D22 */
         pr_default.execute(20, new Object[] {A166FuncaoAPF_Nome, A183FuncaoAPF_Ativo, n184FuncaoAPF_Tipo, A184FuncaoAPF_Tipo, n360FuncaoAPF_SistemaCod, A360FuncaoAPF_SistemaCod, n359FuncaoAPF_ModuloCod, A359FuncaoAPF_ModuloCod, n358FuncaoAPF_FunAPFPaiCod, A358FuncaoAPF_FunAPFPaiCod, n413FuncaoAPF_Acao, A413FuncaoAPF_Acao, n414FuncaoAPF_Mensagem, A414FuncaoAPF_Mensagem, n744FuncaoAPF_MelhoraCod, A744FuncaoAPF_MelhoraCod, n1022FuncaoAPF_DERImp, A1022FuncaoAPF_DERImp, n1026FuncaoAPF_RAImp, A1026FuncaoAPF_RAImp, n1244FuncaoAPF_Observacao, A1244FuncaoAPF_Observacao, n1246FuncaoAPF_Importada, A1246FuncaoAPF_Importada, n1268FuncaoAPF_UpdAoImpBsln, A1268FuncaoAPF_UpdAoImpBsln});
         A165FuncaoAPF_Codigo = P008D22_A165FuncaoAPF_Codigo[0];
         pr_default.close(20);
         dsDefault.SmartCacheProvider.SetUpdated("FuncoesAPF") ;
         if ( (pr_default.getStatus(20) == 1) )
         {
            context.Gx_err = 1;
            Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
         }
         else
         {
            context.Gx_err = 0;
            Gx_emsg = "";
         }
         /* End Insert */
         AV14Codigo = A165FuncaoAPF_Codigo;
         if ( AV93Criar )
         {
            /* Execute user subroutine: 'CRIARRLREDERFNAPF' */
            S151 ();
            if (returnInSub) return;
         }
      }

      protected void S151( )
      {
         /* 'CRIARRLREDERFNAPF' Routine */
      }

      protected void S161( )
      {
         /* 'NEWAPFPREIMP' Routine */
         /*
            INSERT RECORD ON TABLE FuncaoAPFPreImp

         */
         A1260FuncaoAPFPreImp_FnApfCodigo = AV14Codigo;
         A1257FuncaoAPFPreImp_Sequencial = 1;
         A1248FuncaoAPFPreImp_Tipo = AV87FuncaoAPF_Tipo;
         n1248FuncaoAPFPreImp_Tipo = false;
         A1250FuncaoAPFPreImp_RAImp = AV88FuncaoAPF_RAImp;
         n1250FuncaoAPFPreImp_RAImp = false;
         A1249FuncaoAPFPreImp_DERImp = AV89FuncaoAPF_DERImp;
         n1249FuncaoAPFPreImp_DERImp = false;
         A1251FuncaoAPFPreImp_Observacao = AV90FuncaoAPF_Observacao;
         n1251FuncaoAPFPreImp_Observacao = false;
         /* Using cursor P008D23 */
         pr_default.execute(21, new Object[] {A1260FuncaoAPFPreImp_FnApfCodigo, A1257FuncaoAPFPreImp_Sequencial, n1248FuncaoAPFPreImp_Tipo, A1248FuncaoAPFPreImp_Tipo, n1249FuncaoAPFPreImp_DERImp, A1249FuncaoAPFPreImp_DERImp, n1250FuncaoAPFPreImp_RAImp, A1250FuncaoAPFPreImp_RAImp, n1251FuncaoAPFPreImp_Observacao, A1251FuncaoAPFPreImp_Observacao});
         pr_default.close(21);
         dsDefault.SmartCacheProvider.SetUpdated("FuncaoAPFPreImp") ;
         if ( (pr_default.getStatus(21) == 1) )
         {
            context.Gx_err = 1;
            Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
         }
         else
         {
            context.Gx_err = 0;
            Gx_emsg = "";
         }
         /* End Insert */
      }

      protected void H8D0( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("P�gina", 675, Gx_line+0, 710, Gx_line+14, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("de", 750, Gx_line+0, 764, Gx_line+14, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("{{Pages}}", 767, Gx_line+0, 816, Gx_line+14, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(Gx_page), "ZZZZZ9")), 708, Gx_line+0, 747, Gx_line+15, 1+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+15);
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( Gx_time, "")), 733, Gx_line+9, 826, Gx_line+24, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( Gx_date, "99/99/99"), 675, Gx_line+9, 724, Gx_line+24, 2+256, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV80Sistema_Sigla, "")), 284, Gx_line+30, 535, Gx_line+48, 1+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV66NomeArq, "")), 96, Gx_line+50, 722, Gx_line+68, 1+256, 0, 0, 1) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 14, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Importa��o de Baseline", 306, Gx_line+0, 512, Gx_line+25, 0+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+83);
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
         add_metrics1( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics1( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_InserirSDTBaseline");
         CloseOpenCursors();
         if (IsMain)	waitPrinterEnd();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         AV72WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV51SDT_Baseline = new GxObjectCollection( context, "SDT_Baselines.Funcao", "GxEv3Up14_Meetrika", "SdtSDT_Baselines_Funcao", "GeneXus.Programs");
         AV71WebSession = context.GetSession();
         AV66NomeArq = "";
         AV30EmailText = "";
         AV75TipoExterna = "";
         AV50SDT_Funcao = new SdtSDT_Baselines_Funcao(context);
         AV82TipoFuncaoAPF = "";
         scmdbuf = "";
         P008D2_A1257FuncaoAPFPreImp_Sequencial = new short[1] ;
         P008D2_A1260FuncaoAPFPreImp_FnApfCodigo = new int[1] ;
         P008D2_A184FuncaoAPF_Tipo = new String[] {""} ;
         P008D2_n184FuncaoAPF_Tipo = new bool[] {false} ;
         P008D2_A1248FuncaoAPFPreImp_Tipo = new String[] {""} ;
         P008D2_n1248FuncaoAPFPreImp_Tipo = new bool[] {false} ;
         P008D2_A1026FuncaoAPF_RAImp = new short[1] ;
         P008D2_n1026FuncaoAPF_RAImp = new bool[] {false} ;
         P008D2_A1250FuncaoAPFPreImp_RAImp = new short[1] ;
         P008D2_n1250FuncaoAPFPreImp_RAImp = new bool[] {false} ;
         P008D2_A1022FuncaoAPF_DERImp = new short[1] ;
         P008D2_n1022FuncaoAPF_DERImp = new bool[] {false} ;
         P008D2_A1249FuncaoAPFPreImp_DERImp = new short[1] ;
         P008D2_n1249FuncaoAPFPreImp_DERImp = new bool[] {false} ;
         P008D2_A1244FuncaoAPF_Observacao = new String[] {""} ;
         P008D2_n1244FuncaoAPF_Observacao = new bool[] {false} ;
         P008D2_A1251FuncaoAPFPreImp_Observacao = new String[] {""} ;
         P008D2_n1251FuncaoAPFPreImp_Observacao = new bool[] {false} ;
         P008D2_A1246FuncaoAPF_Importada = new DateTime[] {DateTime.MinValue} ;
         P008D2_n1246FuncaoAPF_Importada = new bool[] {false} ;
         A184FuncaoAPF_Tipo = "";
         A1248FuncaoAPFPreImp_Tipo = "";
         A1244FuncaoAPF_Observacao = "";
         A1251FuncaoAPFPreImp_Observacao = "";
         A1246FuncaoAPF_Importada = (DateTime)(DateTime.MinValue);
         P008D7_A165FuncaoAPF_Codigo = new int[1] ;
         P008D7_A184FuncaoAPF_Tipo = new String[] {""} ;
         P008D7_n184FuncaoAPF_Tipo = new bool[] {false} ;
         P008D7_A1026FuncaoAPF_RAImp = new short[1] ;
         P008D7_n1026FuncaoAPF_RAImp = new bool[] {false} ;
         P008D7_A1022FuncaoAPF_DERImp = new short[1] ;
         P008D7_n1022FuncaoAPF_DERImp = new bool[] {false} ;
         P008D7_A1244FuncaoAPF_Observacao = new String[] {""} ;
         P008D7_n1244FuncaoAPF_Observacao = new bool[] {false} ;
         P008D7_A1246FuncaoAPF_Importada = new DateTime[] {DateTime.MinValue} ;
         P008D7_n1246FuncaoAPF_Importada = new bool[] {false} ;
         AV87FuncaoAPF_Tipo = "";
         AV90FuncaoAPF_Observacao = "";
         AV81TipoFuncaoDados = "";
         P008D10_A1259FuncaoDadosPreImp_Sequencial = new short[1] ;
         P008D10_A1261FuncaoDadosPreImp_FnDadosCodigo = new int[1] ;
         P008D10_A373FuncaoDados_Tipo = new String[] {""} ;
         P008D10_n373FuncaoDados_Tipo = new bool[] {false} ;
         P008D10_A1253FuncaoDadosPreImp_Tipo = new String[] {""} ;
         P008D10_A1025FuncaoDados_RAImp = new short[1] ;
         P008D10_n1025FuncaoDados_RAImp = new bool[] {false} ;
         P008D10_A1255FuncaoDadosPreImp_RAImp = new short[1] ;
         P008D10_A1024FuncaoDados_DERImp = new short[1] ;
         P008D10_n1024FuncaoDados_DERImp = new bool[] {false} ;
         P008D10_A1254FuncaoDadosPreImp_DERImp = new short[1] ;
         P008D10_A1245FuncaoDados_Observacao = new String[] {""} ;
         P008D10_n1245FuncaoDados_Observacao = new bool[] {false} ;
         P008D10_A1256FuncaoDadosPreImp_Observacao = new String[] {""} ;
         P008D10_A1258FuncaoDados_Importada = new DateTime[] {DateTime.MinValue} ;
         P008D10_n1258FuncaoDados_Importada = new bool[] {false} ;
         A373FuncaoDados_Tipo = "";
         A1253FuncaoDadosPreImp_Tipo = "";
         A1245FuncaoDados_Observacao = "";
         A1256FuncaoDadosPreImp_Observacao = "";
         A1258FuncaoDados_Importada = DateTime.MinValue;
         P008D15_A368FuncaoDados_Codigo = new int[1] ;
         P008D15_A373FuncaoDados_Tipo = new String[] {""} ;
         P008D15_n373FuncaoDados_Tipo = new bool[] {false} ;
         P008D15_A1025FuncaoDados_RAImp = new short[1] ;
         P008D15_n1025FuncaoDados_RAImp = new bool[] {false} ;
         P008D15_A1024FuncaoDados_DERImp = new short[1] ;
         P008D15_n1024FuncaoDados_DERImp = new bool[] {false} ;
         P008D15_A1245FuncaoDados_Observacao = new String[] {""} ;
         P008D15_n1245FuncaoDados_Observacao = new bool[] {false} ;
         P008D15_A1258FuncaoDados_Importada = new DateTime[] {DateTime.MinValue} ;
         P008D15_n1258FuncaoDados_Importada = new bool[] {false} ;
         AV83FuncaoDados_Tipo = "";
         AV86FuncaoDados_Observacao = "";
         AV36Linha = "";
         AV67Totais = "";
         A369FuncaoDados_Nome = "";
         A394FuncaoDados_Ativo = "";
         P008D18_A368FuncaoDados_Codigo = new int[1] ;
         Gx_emsg = "";
         P008D19_A172Tabela_Codigo = new int[1] ;
         P008D19_A368FuncaoDados_Codigo = new int[1] ;
         P008D20_AV95DERs = new short[1] ;
         A166FuncaoAPF_Nome = "";
         A183FuncaoAPF_Ativo = "";
         P008D22_A165FuncaoAPF_Codigo = new int[1] ;
         Gx_time = "";
         Gx_date = DateTime.MinValue;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.aprc_inserirsdtbaseline__default(),
            new Object[][] {
                new Object[] {
               P008D2_A1257FuncaoAPFPreImp_Sequencial, P008D2_A1260FuncaoAPFPreImp_FnApfCodigo, P008D2_A184FuncaoAPF_Tipo, P008D2_n184FuncaoAPF_Tipo, P008D2_A1248FuncaoAPFPreImp_Tipo, P008D2_n1248FuncaoAPFPreImp_Tipo, P008D2_A1026FuncaoAPF_RAImp, P008D2_n1026FuncaoAPF_RAImp, P008D2_A1250FuncaoAPFPreImp_RAImp, P008D2_n1250FuncaoAPFPreImp_RAImp,
               P008D2_A1022FuncaoAPF_DERImp, P008D2_n1022FuncaoAPF_DERImp, P008D2_A1249FuncaoAPFPreImp_DERImp, P008D2_n1249FuncaoAPFPreImp_DERImp, P008D2_A1244FuncaoAPF_Observacao, P008D2_n1244FuncaoAPF_Observacao, P008D2_A1251FuncaoAPFPreImp_Observacao, P008D2_n1251FuncaoAPFPreImp_Observacao, P008D2_A1246FuncaoAPF_Importada, P008D2_n1246FuncaoAPF_Importada
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P008D7_A165FuncaoAPF_Codigo, P008D7_A184FuncaoAPF_Tipo, P008D7_A1026FuncaoAPF_RAImp, P008D7_n1026FuncaoAPF_RAImp, P008D7_A1022FuncaoAPF_DERImp, P008D7_n1022FuncaoAPF_DERImp, P008D7_A1244FuncaoAPF_Observacao, P008D7_n1244FuncaoAPF_Observacao, P008D7_A1246FuncaoAPF_Importada, P008D7_n1246FuncaoAPF_Importada
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P008D10_A1259FuncaoDadosPreImp_Sequencial, P008D10_A1261FuncaoDadosPreImp_FnDadosCodigo, P008D10_A373FuncaoDados_Tipo, P008D10_n373FuncaoDados_Tipo, P008D10_A1253FuncaoDadosPreImp_Tipo, P008D10_A1025FuncaoDados_RAImp, P008D10_n1025FuncaoDados_RAImp, P008D10_A1255FuncaoDadosPreImp_RAImp, P008D10_A1024FuncaoDados_DERImp, P008D10_n1024FuncaoDados_DERImp,
               P008D10_A1254FuncaoDadosPreImp_DERImp, P008D10_A1245FuncaoDados_Observacao, P008D10_n1245FuncaoDados_Observacao, P008D10_A1256FuncaoDadosPreImp_Observacao, P008D10_A1258FuncaoDados_Importada, P008D10_n1258FuncaoDados_Importada
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P008D15_A368FuncaoDados_Codigo, P008D15_A373FuncaoDados_Tipo, P008D15_A1025FuncaoDados_RAImp, P008D15_n1025FuncaoDados_RAImp, P008D15_A1024FuncaoDados_DERImp, P008D15_n1024FuncaoDados_DERImp, P008D15_A1245FuncaoDados_Observacao, P008D15_n1245FuncaoDados_Observacao, P008D15_A1258FuncaoDados_Importada, P008D15_n1258FuncaoDados_Importada
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P008D18_A368FuncaoDados_Codigo
               }
               , new Object[] {
               P008D19_A172Tabela_Codigo, P008D19_A368FuncaoDados_Codigo
               }
               , new Object[] {
               P008D20_AV95DERs
               }
               , new Object[] {
               }
               , new Object[] {
               P008D22_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               }
            }
         );
         Gx_date = DateTimeUtil.Today( context);
         Gx_time = context.localUtil.Time( );
         /* GeneXus formulas. */
         Gx_line = 0;
         Gx_date = DateTimeUtil.Today( context);
         Gx_time = context.localUtil.Time( );
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short GxWebError ;
      private short AV35i ;
      private short AV102GXLvl37 ;
      private short A1257FuncaoAPFPreImp_Sequencial ;
      private short A1026FuncaoAPF_RAImp ;
      private short A1250FuncaoAPFPreImp_RAImp ;
      private short A1022FuncaoAPF_DERImp ;
      private short A1249FuncaoAPFPreImp_DERImp ;
      private short AV88FuncaoAPF_RAImp ;
      private short AV89FuncaoAPF_DERImp ;
      private short AV78FncAPF ;
      private short AV104GXLvl89 ;
      private short A1259FuncaoDadosPreImp_Sequencial ;
      private short A1025FuncaoDados_RAImp ;
      private short A1255FuncaoDadosPreImp_RAImp ;
      private short A1024FuncaoDados_DERImp ;
      private short A1254FuncaoDadosPreImp_DERImp ;
      private short AV84FuncaoDados_RAImp ;
      private short AV85FuncaoDados_DERImp ;
      private short AV77FncDados ;
      private short AV94RLRs ;
      private short AV95DERs ;
      private short cV95DERs ;
      private int AV76Sistema_Codigo ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int AV101GXV1 ;
      private int AV14Codigo ;
      private int A1260FuncaoAPFPreImp_FnApfCodigo ;
      private int A165FuncaoAPF_Codigo ;
      private int A1261FuncaoDadosPreImp_FnDadosCodigo ;
      private int A368FuncaoDados_Codigo ;
      private int Gx_OldLine ;
      private int GX_INS59 ;
      private int A370FuncaoDados_SistemaCod ;
      private int A391FuncaoDados_FuncaoDadosCod ;
      private int A745FuncaoDados_MelhoraCod ;
      private int A172Tabela_Codigo ;
      private int GX_INS152 ;
      private int GX_INS36 ;
      private int A360FuncaoAPF_SistemaCod ;
      private int A358FuncaoAPF_FunAPFPaiCod ;
      private int A744FuncaoAPF_MelhoraCod ;
      private int A359FuncaoAPF_ModuloCod ;
      private int GX_INS153 ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String AV80Sistema_Sigla ;
      private String AV91FileName ;
      private String AV92Aba ;
      private String AV66NomeArq ;
      private String AV30EmailText ;
      private String AV75TipoExterna ;
      private String AV82TipoFuncaoAPF ;
      private String scmdbuf ;
      private String A184FuncaoAPF_Tipo ;
      private String A1248FuncaoAPFPreImp_Tipo ;
      private String AV87FuncaoAPF_Tipo ;
      private String AV81TipoFuncaoDados ;
      private String A373FuncaoDados_Tipo ;
      private String A1253FuncaoDadosPreImp_Tipo ;
      private String AV83FuncaoDados_Tipo ;
      private String AV36Linha ;
      private String AV67Totais ;
      private String A394FuncaoDados_Ativo ;
      private String Gx_emsg ;
      private String A183FuncaoAPF_Ativo ;
      private String Gx_time ;
      private DateTime A1246FuncaoAPF_Importada ;
      private DateTime A1258FuncaoDados_Importada ;
      private DateTime Gx_date ;
      private bool entryPointCalled ;
      private bool AV93Criar ;
      private bool returnInSub ;
      private bool n184FuncaoAPF_Tipo ;
      private bool n1248FuncaoAPFPreImp_Tipo ;
      private bool n1026FuncaoAPF_RAImp ;
      private bool n1250FuncaoAPFPreImp_RAImp ;
      private bool n1022FuncaoAPF_DERImp ;
      private bool n1249FuncaoAPFPreImp_DERImp ;
      private bool n1244FuncaoAPF_Observacao ;
      private bool n1251FuncaoAPFPreImp_Observacao ;
      private bool n1246FuncaoAPF_Importada ;
      private bool n373FuncaoDados_Tipo ;
      private bool n1025FuncaoDados_RAImp ;
      private bool n1024FuncaoDados_DERImp ;
      private bool n1245FuncaoDados_Observacao ;
      private bool n1258FuncaoDados_Importada ;
      private bool A755FuncaoDados_Contar ;
      private bool n755FuncaoDados_Contar ;
      private bool n391FuncaoDados_FuncaoDadosCod ;
      private bool n745FuncaoDados_MelhoraCod ;
      private bool A1267FuncaoDados_UpdAoImpBsln ;
      private bool n1267FuncaoDados_UpdAoImpBsln ;
      private bool n360FuncaoAPF_SistemaCod ;
      private bool A414FuncaoAPF_Mensagem ;
      private bool n414FuncaoAPF_Mensagem ;
      private bool A413FuncaoAPF_Acao ;
      private bool n413FuncaoAPF_Acao ;
      private bool n358FuncaoAPF_FunAPFPaiCod ;
      private bool n744FuncaoAPF_MelhoraCod ;
      private bool n359FuncaoAPF_ModuloCod ;
      private bool A1268FuncaoAPF_UpdAoImpBsln ;
      private bool n1268FuncaoAPF_UpdAoImpBsln ;
      private String A1244FuncaoAPF_Observacao ;
      private String A1251FuncaoAPFPreImp_Observacao ;
      private String AV90FuncaoAPF_Observacao ;
      private String A1245FuncaoDados_Observacao ;
      private String A1256FuncaoDadosPreImp_Observacao ;
      private String AV86FuncaoDados_Observacao ;
      private String A369FuncaoDados_Nome ;
      private String A166FuncaoAPF_Nome ;
      private IGxSession AV71WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private bool aP4_Criar ;
      private IDataStoreProvider pr_default ;
      private short[] P008D2_A1257FuncaoAPFPreImp_Sequencial ;
      private int[] P008D2_A1260FuncaoAPFPreImp_FnApfCodigo ;
      private String[] P008D2_A184FuncaoAPF_Tipo ;
      private bool[] P008D2_n184FuncaoAPF_Tipo ;
      private String[] P008D2_A1248FuncaoAPFPreImp_Tipo ;
      private bool[] P008D2_n1248FuncaoAPFPreImp_Tipo ;
      private short[] P008D2_A1026FuncaoAPF_RAImp ;
      private bool[] P008D2_n1026FuncaoAPF_RAImp ;
      private short[] P008D2_A1250FuncaoAPFPreImp_RAImp ;
      private bool[] P008D2_n1250FuncaoAPFPreImp_RAImp ;
      private short[] P008D2_A1022FuncaoAPF_DERImp ;
      private bool[] P008D2_n1022FuncaoAPF_DERImp ;
      private short[] P008D2_A1249FuncaoAPFPreImp_DERImp ;
      private bool[] P008D2_n1249FuncaoAPFPreImp_DERImp ;
      private String[] P008D2_A1244FuncaoAPF_Observacao ;
      private bool[] P008D2_n1244FuncaoAPF_Observacao ;
      private String[] P008D2_A1251FuncaoAPFPreImp_Observacao ;
      private bool[] P008D2_n1251FuncaoAPFPreImp_Observacao ;
      private DateTime[] P008D2_A1246FuncaoAPF_Importada ;
      private bool[] P008D2_n1246FuncaoAPF_Importada ;
      private int[] P008D7_A165FuncaoAPF_Codigo ;
      private String[] P008D7_A184FuncaoAPF_Tipo ;
      private bool[] P008D7_n184FuncaoAPF_Tipo ;
      private short[] P008D7_A1026FuncaoAPF_RAImp ;
      private bool[] P008D7_n1026FuncaoAPF_RAImp ;
      private short[] P008D7_A1022FuncaoAPF_DERImp ;
      private bool[] P008D7_n1022FuncaoAPF_DERImp ;
      private String[] P008D7_A1244FuncaoAPF_Observacao ;
      private bool[] P008D7_n1244FuncaoAPF_Observacao ;
      private DateTime[] P008D7_A1246FuncaoAPF_Importada ;
      private bool[] P008D7_n1246FuncaoAPF_Importada ;
      private short[] P008D10_A1259FuncaoDadosPreImp_Sequencial ;
      private int[] P008D10_A1261FuncaoDadosPreImp_FnDadosCodigo ;
      private String[] P008D10_A373FuncaoDados_Tipo ;
      private bool[] P008D10_n373FuncaoDados_Tipo ;
      private String[] P008D10_A1253FuncaoDadosPreImp_Tipo ;
      private short[] P008D10_A1025FuncaoDados_RAImp ;
      private bool[] P008D10_n1025FuncaoDados_RAImp ;
      private short[] P008D10_A1255FuncaoDadosPreImp_RAImp ;
      private short[] P008D10_A1024FuncaoDados_DERImp ;
      private bool[] P008D10_n1024FuncaoDados_DERImp ;
      private short[] P008D10_A1254FuncaoDadosPreImp_DERImp ;
      private String[] P008D10_A1245FuncaoDados_Observacao ;
      private bool[] P008D10_n1245FuncaoDados_Observacao ;
      private String[] P008D10_A1256FuncaoDadosPreImp_Observacao ;
      private DateTime[] P008D10_A1258FuncaoDados_Importada ;
      private bool[] P008D10_n1258FuncaoDados_Importada ;
      private int[] P008D15_A368FuncaoDados_Codigo ;
      private String[] P008D15_A373FuncaoDados_Tipo ;
      private bool[] P008D15_n373FuncaoDados_Tipo ;
      private short[] P008D15_A1025FuncaoDados_RAImp ;
      private bool[] P008D15_n1025FuncaoDados_RAImp ;
      private short[] P008D15_A1024FuncaoDados_DERImp ;
      private bool[] P008D15_n1024FuncaoDados_DERImp ;
      private String[] P008D15_A1245FuncaoDados_Observacao ;
      private bool[] P008D15_n1245FuncaoDados_Observacao ;
      private DateTime[] P008D15_A1258FuncaoDados_Importada ;
      private bool[] P008D15_n1258FuncaoDados_Importada ;
      private int[] P008D18_A368FuncaoDados_Codigo ;
      private int[] P008D19_A172Tabela_Codigo ;
      private int[] P008D19_A368FuncaoDados_Codigo ;
      private short[] P008D20_AV95DERs ;
      private int[] P008D22_A165FuncaoAPF_Codigo ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Baselines_Funcao ))]
      private IGxCollection AV51SDT_Baseline ;
      private SdtSDT_Baselines_Funcao AV50SDT_Funcao ;
      private wwpbaseobjects.SdtWWPContext AV72WWPContext ;
   }

   public class aprc_inserirsdtbaseline__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new UpdateCursor(def[3])
         ,new UpdateCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new UpdateCursor(def[14])
         ,new UpdateCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new UpdateCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new UpdateCursor(def[21])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP008D2 ;
          prmP008D2 = new Object[] {
          new Object[] {"@AV14Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008D3 ;
          prmP008D3 = new Object[] {
          new Object[] {"@FuncaoAPF_Tipo",SqlDbType.Char,3,0} ,
          new Object[] {"@FuncaoAPF_RAImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoAPF_DERImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoAPF_Observacao",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@FuncaoAPF_Importada",SqlDbType.DateTime,8,5} ,
          new Object[] {"@FuncaoAPFPreImp_FnApfCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008D4 ;
          prmP008D4 = new Object[] {
          new Object[] {"@FuncaoAPFPreImp_Tipo",SqlDbType.Char,3,0} ,
          new Object[] {"@FuncaoAPFPreImp_RAImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoAPFPreImp_DERImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoAPFPreImp_Observacao",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@FuncaoAPFPreImp_FnApfCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFPreImp_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmP008D5 ;
          prmP008D5 = new Object[] {
          new Object[] {"@FuncaoAPF_Tipo",SqlDbType.Char,3,0} ,
          new Object[] {"@FuncaoAPF_RAImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoAPF_DERImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoAPF_Observacao",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@FuncaoAPF_Importada",SqlDbType.DateTime,8,5} ,
          new Object[] {"@FuncaoAPFPreImp_FnApfCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008D6 ;
          prmP008D6 = new Object[] {
          new Object[] {"@FuncaoAPFPreImp_Tipo",SqlDbType.Char,3,0} ,
          new Object[] {"@FuncaoAPFPreImp_RAImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoAPFPreImp_DERImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoAPFPreImp_Observacao",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@FuncaoAPFPreImp_FnApfCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFPreImp_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmP008D7 ;
          prmP008D7 = new Object[] {
          new Object[] {"@AV14Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008D8 ;
          prmP008D8 = new Object[] {
          new Object[] {"@FuncaoAPF_Tipo",SqlDbType.Char,3,0} ,
          new Object[] {"@FuncaoAPF_RAImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoAPF_DERImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoAPF_Observacao",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@FuncaoAPF_Importada",SqlDbType.DateTime,8,5} ,
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008D9 ;
          prmP008D9 = new Object[] {
          new Object[] {"@FuncaoAPF_Tipo",SqlDbType.Char,3,0} ,
          new Object[] {"@FuncaoAPF_RAImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoAPF_DERImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoAPF_Observacao",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@FuncaoAPF_Importada",SqlDbType.DateTime,8,5} ,
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008D10 ;
          prmP008D10 = new Object[] {
          new Object[] {"@AV14Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008D11 ;
          prmP008D11 = new Object[] {
          new Object[] {"@FuncaoDados_Tipo",SqlDbType.Char,3,0} ,
          new Object[] {"@FuncaoDados_RAImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoDados_DERImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoDados_Observacao",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@FuncaoDados_Importada",SqlDbType.DateTime,8,0} ,
          new Object[] {"@FuncaoDadosPreImp_FnDadosCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008D12 ;
          prmP008D12 = new Object[] {
          new Object[] {"@FuncaoDadosPreImp_Tipo",SqlDbType.Char,3,0} ,
          new Object[] {"@FuncaoDadosPreImp_RAImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoDadosPreImp_DERImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoDadosPreImp_Observacao",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@FuncaoDadosPreImp_FnDadosCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoDadosPreImp_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmP008D13 ;
          prmP008D13 = new Object[] {
          new Object[] {"@FuncaoDados_Tipo",SqlDbType.Char,3,0} ,
          new Object[] {"@FuncaoDados_RAImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoDados_DERImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoDados_Observacao",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@FuncaoDados_Importada",SqlDbType.DateTime,8,0} ,
          new Object[] {"@FuncaoDadosPreImp_FnDadosCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008D14 ;
          prmP008D14 = new Object[] {
          new Object[] {"@FuncaoDadosPreImp_Tipo",SqlDbType.Char,3,0} ,
          new Object[] {"@FuncaoDadosPreImp_RAImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoDadosPreImp_DERImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoDadosPreImp_Observacao",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@FuncaoDadosPreImp_FnDadosCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoDadosPreImp_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmP008D15 ;
          prmP008D15 = new Object[] {
          new Object[] {"@AV14Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008D16 ;
          prmP008D16 = new Object[] {
          new Object[] {"@FuncaoDados_Tipo",SqlDbType.Char,3,0} ,
          new Object[] {"@FuncaoDados_RAImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoDados_DERImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoDados_Observacao",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@FuncaoDados_Importada",SqlDbType.DateTime,8,0} ,
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008D17 ;
          prmP008D17 = new Object[] {
          new Object[] {"@FuncaoDados_Tipo",SqlDbType.Char,3,0} ,
          new Object[] {"@FuncaoDados_RAImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoDados_DERImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoDados_Observacao",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@FuncaoDados_Importada",SqlDbType.DateTime,8,0} ,
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008D18 ;
          prmP008D18 = new Object[] {
          new Object[] {"@FuncaoDados_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@FuncaoDados_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoDados_Tipo",SqlDbType.Char,3,0} ,
          new Object[] {"@FuncaoDados_FuncaoDadosCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoDados_Ativo",SqlDbType.Char,1,0} ,
          new Object[] {"@FuncaoDados_MelhoraCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoDados_Contar",SqlDbType.Bit,4,0} ,
          new Object[] {"@FuncaoDados_DERImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoDados_RAImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoDados_Observacao",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@FuncaoDados_Importada",SqlDbType.DateTime,8,0} ,
          new Object[] {"@FuncaoDados_UpdAoImpBsln",SqlDbType.Bit,4,0}
          } ;
          Object[] prmP008D19 ;
          prmP008D19 = new Object[] {
          new Object[] {"@AV14Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008D20 ;
          prmP008D20 = new Object[] {
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008D21 ;
          prmP008D21 = new Object[] {
          new Object[] {"@FuncaoDadosPreImp_FnDadosCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoDadosPreImp_Sequencial",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@FuncaoDadosPreImp_Tipo",SqlDbType.Char,3,0} ,
          new Object[] {"@FuncaoDadosPreImp_DERImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoDadosPreImp_RAImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoDadosPreImp_Observacao",SqlDbType.VarChar,1000,0}
          } ;
          Object[] prmP008D22 ;
          prmP008D22 = new Object[] {
          new Object[] {"@FuncaoAPF_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@FuncaoAPF_Ativo",SqlDbType.Char,1,0} ,
          new Object[] {"@FuncaoAPF_Tipo",SqlDbType.Char,3,0} ,
          new Object[] {"@FuncaoAPF_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPF_ModuloCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPF_FunAPFPaiCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPF_Acao",SqlDbType.Bit,4,0} ,
          new Object[] {"@FuncaoAPF_Mensagem",SqlDbType.Bit,4,0} ,
          new Object[] {"@FuncaoAPF_MelhoraCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPF_DERImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoAPF_RAImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoAPF_Observacao",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@FuncaoAPF_Importada",SqlDbType.DateTime,8,5} ,
          new Object[] {"@FuncaoAPF_UpdAoImpBsln",SqlDbType.Bit,4,0}
          } ;
          Object[] prmP008D23 ;
          prmP008D23 = new Object[] {
          new Object[] {"@FuncaoAPFPreImp_FnApfCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFPreImp_Sequencial",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@FuncaoAPFPreImp_Tipo",SqlDbType.Char,3,0} ,
          new Object[] {"@FuncaoAPFPreImp_DERImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoAPFPreImp_RAImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoAPFPreImp_Observacao",SqlDbType.VarChar,1000,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P008D2", "SELECT TOP 1 T1.[FuncaoAPFPreImp_Sequencial], T1.[FuncaoAPFPreImp_FnApfCodigo] AS FuncaoAPFPreImp_FnApfCodigo, T2.[FuncaoAPF_Tipo], T1.[FuncaoAPFPreImp_Tipo], T2.[FuncaoAPF_RAImp], T1.[FuncaoAPFPreImp_RAImp], T2.[FuncaoAPF_DERImp], T1.[FuncaoAPFPreImp_DERImp], T2.[FuncaoAPF_Observacao], T1.[FuncaoAPFPreImp_Observacao], T2.[FuncaoAPF_Importada] FROM ([FuncaoAPFPreImp] T1 WITH (UPDLOCK) INNER JOIN [FuncoesAPF] T2 WITH (UPDLOCK) ON T2.[FuncaoAPF_Codigo] = T1.[FuncaoAPFPreImp_FnApfCodigo]) WHERE T1.[FuncaoAPFPreImp_FnApfCodigo] = @AV14Codigo and T1.[FuncaoAPFPreImp_Sequencial] = 1 ORDER BY T1.[FuncaoAPFPreImp_FnApfCodigo], T1.[FuncaoAPFPreImp_Sequencial] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008D2,1,0,true,true )
             ,new CursorDef("P008D3", "UPDATE [FuncoesAPF] SET [FuncaoAPF_Tipo]=@FuncaoAPF_Tipo, [FuncaoAPF_RAImp]=@FuncaoAPF_RAImp, [FuncaoAPF_DERImp]=@FuncaoAPF_DERImp, [FuncaoAPF_Observacao]=@FuncaoAPF_Observacao, [FuncaoAPF_Importada]=@FuncaoAPF_Importada  WHERE [FuncaoAPF_Codigo] = @FuncaoAPFPreImp_FnApfCodigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008D3)
             ,new CursorDef("P008D4", "UPDATE [FuncaoAPFPreImp] SET [FuncaoAPFPreImp_Tipo]=@FuncaoAPFPreImp_Tipo, [FuncaoAPFPreImp_RAImp]=@FuncaoAPFPreImp_RAImp, [FuncaoAPFPreImp_DERImp]=@FuncaoAPFPreImp_DERImp, [FuncaoAPFPreImp_Observacao]=@FuncaoAPFPreImp_Observacao  WHERE [FuncaoAPFPreImp_FnApfCodigo] = @FuncaoAPFPreImp_FnApfCodigo AND [FuncaoAPFPreImp_Sequencial] = @FuncaoAPFPreImp_Sequencial", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008D4)
             ,new CursorDef("P008D5", "UPDATE [FuncoesAPF] SET [FuncaoAPF_Tipo]=@FuncaoAPF_Tipo, [FuncaoAPF_RAImp]=@FuncaoAPF_RAImp, [FuncaoAPF_DERImp]=@FuncaoAPF_DERImp, [FuncaoAPF_Observacao]=@FuncaoAPF_Observacao, [FuncaoAPF_Importada]=@FuncaoAPF_Importada  WHERE [FuncaoAPF_Codigo] = @FuncaoAPFPreImp_FnApfCodigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008D5)
             ,new CursorDef("P008D6", "UPDATE [FuncaoAPFPreImp] SET [FuncaoAPFPreImp_Tipo]=@FuncaoAPFPreImp_Tipo, [FuncaoAPFPreImp_RAImp]=@FuncaoAPFPreImp_RAImp, [FuncaoAPFPreImp_DERImp]=@FuncaoAPFPreImp_DERImp, [FuncaoAPFPreImp_Observacao]=@FuncaoAPFPreImp_Observacao  WHERE [FuncaoAPFPreImp_FnApfCodigo] = @FuncaoAPFPreImp_FnApfCodigo AND [FuncaoAPFPreImp_Sequencial] = @FuncaoAPFPreImp_Sequencial", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008D6)
             ,new CursorDef("P008D7", "SELECT TOP 1 [FuncaoAPF_Codigo], [FuncaoAPF_Tipo], [FuncaoAPF_RAImp], [FuncaoAPF_DERImp], [FuncaoAPF_Observacao], [FuncaoAPF_Importada] FROM [FuncoesAPF] WITH (UPDLOCK) WHERE [FuncaoAPF_Codigo] = @AV14Codigo ORDER BY [FuncaoAPF_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008D7,1,0,true,true )
             ,new CursorDef("P008D8", "UPDATE [FuncoesAPF] SET [FuncaoAPF_Tipo]=@FuncaoAPF_Tipo, [FuncaoAPF_RAImp]=@FuncaoAPF_RAImp, [FuncaoAPF_DERImp]=@FuncaoAPF_DERImp, [FuncaoAPF_Observacao]=@FuncaoAPF_Observacao, [FuncaoAPF_Importada]=@FuncaoAPF_Importada  WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008D8)
             ,new CursorDef("P008D9", "UPDATE [FuncoesAPF] SET [FuncaoAPF_Tipo]=@FuncaoAPF_Tipo, [FuncaoAPF_RAImp]=@FuncaoAPF_RAImp, [FuncaoAPF_DERImp]=@FuncaoAPF_DERImp, [FuncaoAPF_Observacao]=@FuncaoAPF_Observacao, [FuncaoAPF_Importada]=@FuncaoAPF_Importada  WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008D9)
             ,new CursorDef("P008D10", "SELECT TOP 1 T1.[FuncaoDadosPreImp_Sequencial], T1.[FuncaoDadosPreImp_FnDadosCodigo] AS FuncaoDadosPreImp_FnDadosCodigo, T2.[FuncaoDados_Tipo], T1.[FuncaoDadosPreImp_Tipo], T2.[FuncaoDados_RAImp], T1.[FuncaoDadosPreImp_RAImp], T2.[FuncaoDados_DERImp], T1.[FuncaoDadosPreImp_DERImp], T2.[FuncaoDados_Observacao], T1.[FuncaoDadosPreImp_Observacao], T2.[FuncaoDados_Importada] FROM ([FuncaoDadosPreImp] T1 WITH (UPDLOCK) INNER JOIN [FuncaoDados] T2 WITH (UPDLOCK) ON T2.[FuncaoDados_Codigo] = T1.[FuncaoDadosPreImp_FnDadosCodigo]) WHERE T1.[FuncaoDadosPreImp_FnDadosCodigo] = @AV14Codigo and T1.[FuncaoDadosPreImp_Sequencial] = 1 ORDER BY T1.[FuncaoDadosPreImp_FnDadosCodigo], T1.[FuncaoDadosPreImp_Sequencial] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008D10,1,0,true,true )
             ,new CursorDef("P008D11", "UPDATE [FuncaoDados] SET [FuncaoDados_Tipo]=@FuncaoDados_Tipo, [FuncaoDados_RAImp]=@FuncaoDados_RAImp, [FuncaoDados_DERImp]=@FuncaoDados_DERImp, [FuncaoDados_Observacao]=@FuncaoDados_Observacao, [FuncaoDados_Importada]=@FuncaoDados_Importada  WHERE [FuncaoDados_Codigo] = @FuncaoDadosPreImp_FnDadosCodigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008D11)
             ,new CursorDef("P008D12", "UPDATE [FuncaoDadosPreImp] SET [FuncaoDadosPreImp_Tipo]=@FuncaoDadosPreImp_Tipo, [FuncaoDadosPreImp_RAImp]=@FuncaoDadosPreImp_RAImp, [FuncaoDadosPreImp_DERImp]=@FuncaoDadosPreImp_DERImp, [FuncaoDadosPreImp_Observacao]=@FuncaoDadosPreImp_Observacao  WHERE [FuncaoDadosPreImp_FnDadosCodigo] = @FuncaoDadosPreImp_FnDadosCodigo AND [FuncaoDadosPreImp_Sequencial] = @FuncaoDadosPreImp_Sequencial", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008D12)
             ,new CursorDef("P008D13", "UPDATE [FuncaoDados] SET [FuncaoDados_Tipo]=@FuncaoDados_Tipo, [FuncaoDados_RAImp]=@FuncaoDados_RAImp, [FuncaoDados_DERImp]=@FuncaoDados_DERImp, [FuncaoDados_Observacao]=@FuncaoDados_Observacao, [FuncaoDados_Importada]=@FuncaoDados_Importada  WHERE [FuncaoDados_Codigo] = @FuncaoDadosPreImp_FnDadosCodigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008D13)
             ,new CursorDef("P008D14", "UPDATE [FuncaoDadosPreImp] SET [FuncaoDadosPreImp_Tipo]=@FuncaoDadosPreImp_Tipo, [FuncaoDadosPreImp_RAImp]=@FuncaoDadosPreImp_RAImp, [FuncaoDadosPreImp_DERImp]=@FuncaoDadosPreImp_DERImp, [FuncaoDadosPreImp_Observacao]=@FuncaoDadosPreImp_Observacao  WHERE [FuncaoDadosPreImp_FnDadosCodigo] = @FuncaoDadosPreImp_FnDadosCodigo AND [FuncaoDadosPreImp_Sequencial] = @FuncaoDadosPreImp_Sequencial", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008D14)
             ,new CursorDef("P008D15", "SELECT TOP 1 [FuncaoDados_Codigo], [FuncaoDados_Tipo], [FuncaoDados_RAImp], [FuncaoDados_DERImp], [FuncaoDados_Observacao], [FuncaoDados_Importada] FROM [FuncaoDados] WITH (UPDLOCK) WHERE [FuncaoDados_Codigo] = @AV14Codigo ORDER BY [FuncaoDados_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008D15,1,0,true,true )
             ,new CursorDef("P008D16", "UPDATE [FuncaoDados] SET [FuncaoDados_Tipo]=@FuncaoDados_Tipo, [FuncaoDados_RAImp]=@FuncaoDados_RAImp, [FuncaoDados_DERImp]=@FuncaoDados_DERImp, [FuncaoDados_Observacao]=@FuncaoDados_Observacao, [FuncaoDados_Importada]=@FuncaoDados_Importada  WHERE [FuncaoDados_Codigo] = @FuncaoDados_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008D16)
             ,new CursorDef("P008D17", "UPDATE [FuncaoDados] SET [FuncaoDados_Tipo]=@FuncaoDados_Tipo, [FuncaoDados_RAImp]=@FuncaoDados_RAImp, [FuncaoDados_DERImp]=@FuncaoDados_DERImp, [FuncaoDados_Observacao]=@FuncaoDados_Observacao, [FuncaoDados_Importada]=@FuncaoDados_Importada  WHERE [FuncaoDados_Codigo] = @FuncaoDados_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008D17)
             ,new CursorDef("P008D18", "INSERT INTO [FuncaoDados]([FuncaoDados_Nome], [FuncaoDados_SistemaCod], [FuncaoDados_Tipo], [FuncaoDados_FuncaoDadosCod], [FuncaoDados_Ativo], [FuncaoDados_MelhoraCod], [FuncaoDados_Contar], [FuncaoDados_DERImp], [FuncaoDados_RAImp], [FuncaoDados_Observacao], [FuncaoDados_Importada], [FuncaoDados_UpdAoImpBsln], [FuncaoDados_Descricao], [FuncaoDados_Tecnica]) VALUES(@FuncaoDados_Nome, @FuncaoDados_SistemaCod, @FuncaoDados_Tipo, @FuncaoDados_FuncaoDadosCod, @FuncaoDados_Ativo, @FuncaoDados_MelhoraCod, @FuncaoDados_Contar, @FuncaoDados_DERImp, @FuncaoDados_RAImp, @FuncaoDados_Observacao, @FuncaoDados_Importada, @FuncaoDados_UpdAoImpBsln, '', convert(int, 0)); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP008D18)
             ,new CursorDef("P008D19", "SELECT [Tabela_Codigo], [FuncaoDados_Codigo] FROM [FuncaoDadosTabela] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @AV14Codigo ORDER BY [FuncaoDados_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008D19,100,0,true,false )
             ,new CursorDef("P008D20", "SELECT COUNT(*) FROM [Atributos] WITH (NOLOCK) WHERE [Atributos_TabelaCod] = @Tabela_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008D20,1,0,true,false )
             ,new CursorDef("P008D21", "INSERT INTO [FuncaoDadosPreImp]([FuncaoDadosPreImp_FnDadosCodigo], [FuncaoDadosPreImp_Sequencial], [FuncaoDadosPreImp_Tipo], [FuncaoDadosPreImp_DERImp], [FuncaoDadosPreImp_RAImp], [FuncaoDadosPreImp_Observacao]) VALUES(@FuncaoDadosPreImp_FnDadosCodigo, @FuncaoDadosPreImp_Sequencial, @FuncaoDadosPreImp_Tipo, @FuncaoDadosPreImp_DERImp, @FuncaoDadosPreImp_RAImp, @FuncaoDadosPreImp_Observacao)", GxErrorMask.GX_NOMASK,prmP008D21)
             ,new CursorDef("P008D22", "INSERT INTO [FuncoesAPF]([FuncaoAPF_Nome], [FuncaoAPF_Ativo], [FuncaoAPF_Tipo], [FuncaoAPF_SistemaCod], [FuncaoAPF_ModuloCod], [FuncaoAPF_FunAPFPaiCod], [FuncaoAPF_Acao], [FuncaoAPF_Mensagem], [FuncaoAPF_MelhoraCod], [FuncaoAPF_DERImp], [FuncaoAPF_RAImp], [FuncaoAPF_Observacao], [FuncaoAPF_Importada], [FuncaoAPF_UpdAoImpBsln], [FuncaoAPF_Descricao], [FuncaoAPF_Link], [FuncaoAPF_Tecnica], [FuncaoAPF_ParecerSE]) VALUES(@FuncaoAPF_Nome, @FuncaoAPF_Ativo, @FuncaoAPF_Tipo, @FuncaoAPF_SistemaCod, @FuncaoAPF_ModuloCod, @FuncaoAPF_FunAPFPaiCod, @FuncaoAPF_Acao, @FuncaoAPF_Mensagem, @FuncaoAPF_MelhoraCod, @FuncaoAPF_DERImp, @FuncaoAPF_RAImp, @FuncaoAPF_Observacao, @FuncaoAPF_Importada, @FuncaoAPF_UpdAoImpBsln, '', '', convert(int, 0), ''); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP008D22)
             ,new CursorDef("P008D23", "INSERT INTO [FuncaoAPFPreImp]([FuncaoAPFPreImp_FnApfCodigo], [FuncaoAPFPreImp_Sequencial], [FuncaoAPFPreImp_Tipo], [FuncaoAPFPreImp_DERImp], [FuncaoAPFPreImp_RAImp], [FuncaoAPFPreImp_Observacao]) VALUES(@FuncaoAPFPreImp_FnApfCodigo, @FuncaoAPFPreImp_Sequencial, @FuncaoAPFPreImp_Tipo, @FuncaoAPFPreImp_DERImp, @FuncaoAPFPreImp_RAImp, @FuncaoAPFPreImp_Observacao)", GxErrorMask.GX_NOMASK,prmP008D23)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((short[]) buf[6])[0] = rslt.getShort(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((short[]) buf[8])[0] = rslt.getShort(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((short[]) buf[10])[0] = rslt.getShort(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((short[]) buf[12])[0] = rslt.getShort(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getLongVarchar(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getLongVarchar(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((DateTime[]) buf[18])[0] = rslt.getGXDateTime(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[8])[0] = rslt.getGXDateTime(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                return;
             case 8 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 3) ;
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((short[]) buf[7])[0] = rslt.getShort(6) ;
                ((short[]) buf[8])[0] = rslt.getShort(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((short[]) buf[10])[0] = rslt.getShort(8) ;
                ((String[]) buf[11])[0] = rslt.getLongVarchar(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getLongVarchar(10) ;
                ((DateTime[]) buf[14])[0] = rslt.getGXDate(11) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(11);
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 18 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(3, (short)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(5, (DateTime)parms[9]);
                }
                stmt.SetParameter(6, (int)parms[10]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(3, (short)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[7]);
                }
                stmt.SetParameter(5, (int)parms[8]);
                stmt.SetParameter(6, (short)parms[9]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(3, (short)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(5, (DateTime)parms[9]);
                }
                stmt.SetParameter(6, (int)parms[10]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(3, (short)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[7]);
                }
                stmt.SetParameter(5, (int)parms[8]);
                stmt.SetParameter(6, (short)parms[9]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(3, (short)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(5, (DateTime)parms[9]);
                }
                stmt.SetParameter(6, (int)parms[10]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(3, (short)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(5, (DateTime)parms[9]);
                }
                stmt.SetParameter(6, (int)parms[10]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(3, (short)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(5, (DateTime)parms[9]);
                }
                stmt.SetParameter(6, (int)parms[10]);
                return;
             case 10 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                stmt.SetParameter(5, (int)parms[4]);
                stmt.SetParameter(6, (short)parms[5]);
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(3, (short)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(5, (DateTime)parms[9]);
                }
                stmt.SetParameter(6, (int)parms[10]);
                return;
             case 12 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                stmt.SetParameter(5, (int)parms[4]);
                stmt.SetParameter(6, (short)parms[5]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(3, (short)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(5, (DateTime)parms[9]);
                }
                stmt.SetParameter(6, (int)parms[10]);
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(3, (short)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(5, (DateTime)parms[9]);
                }
                stmt.SetParameter(6, (int)parms[10]);
                return;
             case 16 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[5]);
                }
                stmt.SetParameter(5, (String)parms[6]);
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(7, (bool)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 8 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(8, (short)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 9 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(9, (short)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 10 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 11 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(11, (DateTime)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 12 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(12, (bool)parms[20]);
                }
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (short)parms[3]);
                stmt.SetParameter(5, (short)parms[4]);
                stmt.SetParameter(6, (String)parms[5]);
                return;
             case 20 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(7, (bool)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(8, (bool)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 10 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(10, (short)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 11 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(11, (short)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 12 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(12, (String)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 13 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(13, (DateTime)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 14 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(14, (bool)parms[25]);
                }
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(4, (short)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(5, (short)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[9]);
                }
                return;
       }
    }

 }

}
