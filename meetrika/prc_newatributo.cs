/*
               File: PRC_NewAtributo
        Description: New Atributo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:18:45.86
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_newatributo : GXProcedure
   {
      public prc_newatributo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_newatributo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( int aP0_Tabela_Codigo ,
                           int aP1_Sistema_Codigo )
      {
         this.AV8Tabela_Codigo = aP0_Tabela_Codigo;
         this.AV14Sistema_Codigo = aP1_Sistema_Codigo;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Tabela_Codigo ,
                                 int aP1_Sistema_Codigo )
      {
         prc_newatributo objprc_newatributo;
         objprc_newatributo = new prc_newatributo();
         objprc_newatributo.AV8Tabela_Codigo = aP0_Tabela_Codigo;
         objprc_newatributo.AV14Sistema_Codigo = aP1_Sistema_Codigo;
         objprc_newatributo.context.SetSubmitInitialConfig(context);
         objprc_newatributo.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_newatributo);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_newatributo)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV12FiltroRecebido = AV13WebSession.Get("FiltroRecebido");
         AV11TrnContextAtt.gxTpr_Attributename = "Atributos_TabelaCod";
         AV11TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV8Tabela_Codigo), 6, 0);
         AV10TrnContext.gxTpr_Attributes.Add(AV11TrnContextAtt, 0);
         AV10TrnContext.gxTpr_Transactionname = "Atributos";
         AV9Session.Set("TrnContext", AV10TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
         context.wjLoc = "atributos.aspx"+ "?" + GXUtil.UrlEncode(StringUtil.RTrim("INS")) + "," + GXUtil.UrlEncode("" +0) + "," + GXUtil.UrlEncode("" +AV14Sistema_Codigo);
         context.wjLocDisableFrm = 1;
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV12FiltroRecebido = "";
         AV13WebSession = context.GetSession();
         AV11TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9Session = context.GetSession();
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8Tabela_Codigo ;
      private int AV14Sistema_Codigo ;
      private String AV12FiltroRecebido ;
      private IGxSession AV13WebSession ;
      private IGxSession AV9Session ;
      private wwpbaseobjects.SdtWWPTransactionContext AV10TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV11TrnContextAtt ;
   }

}
