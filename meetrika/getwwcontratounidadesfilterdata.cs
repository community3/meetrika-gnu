/*
               File: GetWWContratoUnidadesFilterData
        Description: Get WWContrato Unidades Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:16:22.54
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwcontratounidadesfilterdata : GXProcedure
   {
      public getwwcontratounidadesfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwcontratounidadesfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
         return AV27OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwcontratounidadesfilterdata objgetwwcontratounidadesfilterdata;
         objgetwwcontratounidadesfilterdata = new getwwcontratounidadesfilterdata();
         objgetwwcontratounidadesfilterdata.AV18DDOName = aP0_DDOName;
         objgetwwcontratounidadesfilterdata.AV16SearchTxt = aP1_SearchTxt;
         objgetwwcontratounidadesfilterdata.AV17SearchTxtTo = aP2_SearchTxtTo;
         objgetwwcontratounidadesfilterdata.AV22OptionsJson = "" ;
         objgetwwcontratounidadesfilterdata.AV25OptionsDescJson = "" ;
         objgetwwcontratounidadesfilterdata.AV27OptionIndexesJson = "" ;
         objgetwwcontratounidadesfilterdata.context.SetSubmitInitialConfig(context);
         objgetwwcontratounidadesfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwcontratounidadesfilterdata);
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwcontratounidadesfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV21Options = (IGxCollection)(new GxSimpleCollection());
         AV24OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV26OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_CONTRATOUNIDADES_UNDMEDNOM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOUNIDADES_UNDMEDNOMOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_CONTRATOUNIDADES_UNDMEDSIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOUNIDADES_UNDMEDSIGLAOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV22OptionsJson = AV21Options.ToJSonString(false);
         AV25OptionsDescJson = AV24OptionsDesc.ToJSonString(false);
         AV27OptionIndexesJson = AV26OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV29Session.Get("WWContratoUnidadesGridState"), "") == 0 )
         {
            AV31GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWContratoUnidadesGridState"), "");
         }
         else
         {
            AV31GridState.FromXml(AV29Session.Get("WWContratoUnidadesGridState"), "");
         }
         AV36GXV1 = 1;
         while ( AV36GXV1 <= AV31GridState.gxTpr_Filtervalues.Count )
         {
            AV32GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV31GridState.gxTpr_Filtervalues.Item(AV36GXV1));
            if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOUNIDADES_UNDMEDNOM") == 0 )
            {
               AV10TFContratoUnidades_UndMedNom = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOUNIDADES_UNDMEDNOM_SEL") == 0 )
            {
               AV11TFContratoUnidades_UndMedNom_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOUNIDADES_UNDMEDSIGLA") == 0 )
            {
               AV12TFContratoUnidades_UndMedSigla = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOUNIDADES_UNDMEDSIGLA_SEL") == 0 )
            {
               AV13TFContratoUnidades_UndMedSigla_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOUNIDADES_PRODUTIVIDADE") == 0 )
            {
               AV14TFContratoUnidades_Produtividade = NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, ".");
               AV15TFContratoUnidades_Produtividade_To = NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Valueto, ".");
            }
            AV36GXV1 = (int)(AV36GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATOUNIDADES_UNDMEDNOMOPTIONS' Routine */
         AV10TFContratoUnidades_UndMedNom = AV16SearchTxt;
         AV11TFContratoUnidades_UndMedNom_Sel = "";
         AV38WWContratoUnidadesDS_1_Tfcontratounidades_undmednom = AV10TFContratoUnidades_UndMedNom;
         AV39WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel = AV11TFContratoUnidades_UndMedNom_Sel;
         AV40WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla = AV12TFContratoUnidades_UndMedSigla;
         AV41WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel = AV13TFContratoUnidades_UndMedSigla_Sel;
         AV42WWContratoUnidadesDS_5_Tfcontratounidades_produtividade = AV14TFContratoUnidades_Produtividade;
         AV43WWContratoUnidadesDS_6_Tfcontratounidades_produtividade_to = AV15TFContratoUnidades_Produtividade_To;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV39WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel ,
                                              AV38WWContratoUnidadesDS_1_Tfcontratounidades_undmednom ,
                                              AV41WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel ,
                                              AV40WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla ,
                                              AV42WWContratoUnidadesDS_5_Tfcontratounidades_produtividade ,
                                              AV43WWContratoUnidadesDS_6_Tfcontratounidades_produtividade_to ,
                                              A1205ContratoUnidades_UndMedNom ,
                                              A1206ContratoUnidades_UndMedSigla ,
                                              A1208ContratoUnidades_Produtividade },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.DECIMAL, TypeConstants.BOOLEAN
                                              }
         });
         lV38WWContratoUnidadesDS_1_Tfcontratounidades_undmednom = StringUtil.PadR( StringUtil.RTrim( AV38WWContratoUnidadesDS_1_Tfcontratounidades_undmednom), 50, "%");
         lV40WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla = StringUtil.PadR( StringUtil.RTrim( AV40WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla), 15, "%");
         /* Using cursor P00Q72 */
         pr_default.execute(0, new Object[] {lV38WWContratoUnidadesDS_1_Tfcontratounidades_undmednom, AV39WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel, lV40WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla, AV41WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel, AV42WWContratoUnidadesDS_5_Tfcontratounidades_produtividade, AV43WWContratoUnidadesDS_6_Tfcontratounidades_produtividade_to});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKQ72 = false;
            A1204ContratoUnidades_UndMedCod = P00Q72_A1204ContratoUnidades_UndMedCod[0];
            A1208ContratoUnidades_Produtividade = P00Q72_A1208ContratoUnidades_Produtividade[0];
            n1208ContratoUnidades_Produtividade = P00Q72_n1208ContratoUnidades_Produtividade[0];
            A1206ContratoUnidades_UndMedSigla = P00Q72_A1206ContratoUnidades_UndMedSigla[0];
            n1206ContratoUnidades_UndMedSigla = P00Q72_n1206ContratoUnidades_UndMedSigla[0];
            A1205ContratoUnidades_UndMedNom = P00Q72_A1205ContratoUnidades_UndMedNom[0];
            n1205ContratoUnidades_UndMedNom = P00Q72_n1205ContratoUnidades_UndMedNom[0];
            A1207ContratoUnidades_ContratoCod = P00Q72_A1207ContratoUnidades_ContratoCod[0];
            A1206ContratoUnidades_UndMedSigla = P00Q72_A1206ContratoUnidades_UndMedSigla[0];
            n1206ContratoUnidades_UndMedSigla = P00Q72_n1206ContratoUnidades_UndMedSigla[0];
            A1205ContratoUnidades_UndMedNom = P00Q72_A1205ContratoUnidades_UndMedNom[0];
            n1205ContratoUnidades_UndMedNom = P00Q72_n1205ContratoUnidades_UndMedNom[0];
            AV28count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00Q72_A1204ContratoUnidades_UndMedCod[0] == A1204ContratoUnidades_UndMedCod ) )
            {
               BRKQ72 = false;
               A1207ContratoUnidades_ContratoCod = P00Q72_A1207ContratoUnidades_ContratoCod[0];
               AV28count = (long)(AV28count+1);
               BRKQ72 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1205ContratoUnidades_UndMedNom)) )
            {
               AV20Option = A1205ContratoUnidades_UndMedNom;
               AV19InsertIndex = 1;
               while ( ( AV19InsertIndex <= AV21Options.Count ) && ( StringUtil.StrCmp(((String)AV21Options.Item(AV19InsertIndex)), AV20Option) < 0 ) )
               {
                  AV19InsertIndex = (int)(AV19InsertIndex+1);
               }
               AV21Options.Add(AV20Option, AV19InsertIndex);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), AV19InsertIndex);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKQ72 )
            {
               BRKQ72 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATOUNIDADES_UNDMEDSIGLAOPTIONS' Routine */
         AV12TFContratoUnidades_UndMedSigla = AV16SearchTxt;
         AV13TFContratoUnidades_UndMedSigla_Sel = "";
         AV38WWContratoUnidadesDS_1_Tfcontratounidades_undmednom = AV10TFContratoUnidades_UndMedNom;
         AV39WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel = AV11TFContratoUnidades_UndMedNom_Sel;
         AV40WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla = AV12TFContratoUnidades_UndMedSigla;
         AV41WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel = AV13TFContratoUnidades_UndMedSigla_Sel;
         AV42WWContratoUnidadesDS_5_Tfcontratounidades_produtividade = AV14TFContratoUnidades_Produtividade;
         AV43WWContratoUnidadesDS_6_Tfcontratounidades_produtividade_to = AV15TFContratoUnidades_Produtividade_To;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV39WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel ,
                                              AV38WWContratoUnidadesDS_1_Tfcontratounidades_undmednom ,
                                              AV41WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel ,
                                              AV40WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla ,
                                              AV42WWContratoUnidadesDS_5_Tfcontratounidades_produtividade ,
                                              AV43WWContratoUnidadesDS_6_Tfcontratounidades_produtividade_to ,
                                              A1205ContratoUnidades_UndMedNom ,
                                              A1206ContratoUnidades_UndMedSigla ,
                                              A1208ContratoUnidades_Produtividade },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.DECIMAL, TypeConstants.BOOLEAN
                                              }
         });
         lV38WWContratoUnidadesDS_1_Tfcontratounidades_undmednom = StringUtil.PadR( StringUtil.RTrim( AV38WWContratoUnidadesDS_1_Tfcontratounidades_undmednom), 50, "%");
         lV40WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla = StringUtil.PadR( StringUtil.RTrim( AV40WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla), 15, "%");
         /* Using cursor P00Q73 */
         pr_default.execute(1, new Object[] {lV38WWContratoUnidadesDS_1_Tfcontratounidades_undmednom, AV39WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel, lV40WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla, AV41WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel, AV42WWContratoUnidadesDS_5_Tfcontratounidades_produtividade, AV43WWContratoUnidadesDS_6_Tfcontratounidades_produtividade_to});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKQ74 = false;
            A1204ContratoUnidades_UndMedCod = P00Q73_A1204ContratoUnidades_UndMedCod[0];
            A1206ContratoUnidades_UndMedSigla = P00Q73_A1206ContratoUnidades_UndMedSigla[0];
            n1206ContratoUnidades_UndMedSigla = P00Q73_n1206ContratoUnidades_UndMedSigla[0];
            A1208ContratoUnidades_Produtividade = P00Q73_A1208ContratoUnidades_Produtividade[0];
            n1208ContratoUnidades_Produtividade = P00Q73_n1208ContratoUnidades_Produtividade[0];
            A1205ContratoUnidades_UndMedNom = P00Q73_A1205ContratoUnidades_UndMedNom[0];
            n1205ContratoUnidades_UndMedNom = P00Q73_n1205ContratoUnidades_UndMedNom[0];
            A1207ContratoUnidades_ContratoCod = P00Q73_A1207ContratoUnidades_ContratoCod[0];
            A1206ContratoUnidades_UndMedSigla = P00Q73_A1206ContratoUnidades_UndMedSigla[0];
            n1206ContratoUnidades_UndMedSigla = P00Q73_n1206ContratoUnidades_UndMedSigla[0];
            A1205ContratoUnidades_UndMedNom = P00Q73_A1205ContratoUnidades_UndMedNom[0];
            n1205ContratoUnidades_UndMedNom = P00Q73_n1205ContratoUnidades_UndMedNom[0];
            AV28count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00Q73_A1206ContratoUnidades_UndMedSigla[0], A1206ContratoUnidades_UndMedSigla) == 0 ) )
            {
               BRKQ74 = false;
               A1204ContratoUnidades_UndMedCod = P00Q73_A1204ContratoUnidades_UndMedCod[0];
               A1207ContratoUnidades_ContratoCod = P00Q73_A1207ContratoUnidades_ContratoCod[0];
               AV28count = (long)(AV28count+1);
               BRKQ74 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1206ContratoUnidades_UndMedSigla)) )
            {
               AV20Option = A1206ContratoUnidades_UndMedSigla;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKQ74 )
            {
               BRKQ74 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV21Options = new GxSimpleCollection();
         AV24OptionsDesc = new GxSimpleCollection();
         AV26OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV29Session = context.GetSession();
         AV31GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV32GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContratoUnidades_UndMedNom = "";
         AV11TFContratoUnidades_UndMedNom_Sel = "";
         AV12TFContratoUnidades_UndMedSigla = "";
         AV13TFContratoUnidades_UndMedSigla_Sel = "";
         AV38WWContratoUnidadesDS_1_Tfcontratounidades_undmednom = "";
         AV39WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel = "";
         AV40WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla = "";
         AV41WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel = "";
         scmdbuf = "";
         lV38WWContratoUnidadesDS_1_Tfcontratounidades_undmednom = "";
         lV40WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla = "";
         A1205ContratoUnidades_UndMedNom = "";
         A1206ContratoUnidades_UndMedSigla = "";
         P00Q72_A1204ContratoUnidades_UndMedCod = new int[1] ;
         P00Q72_A1208ContratoUnidades_Produtividade = new decimal[1] ;
         P00Q72_n1208ContratoUnidades_Produtividade = new bool[] {false} ;
         P00Q72_A1206ContratoUnidades_UndMedSigla = new String[] {""} ;
         P00Q72_n1206ContratoUnidades_UndMedSigla = new bool[] {false} ;
         P00Q72_A1205ContratoUnidades_UndMedNom = new String[] {""} ;
         P00Q72_n1205ContratoUnidades_UndMedNom = new bool[] {false} ;
         P00Q72_A1207ContratoUnidades_ContratoCod = new int[1] ;
         AV20Option = "";
         P00Q73_A1204ContratoUnidades_UndMedCod = new int[1] ;
         P00Q73_A1206ContratoUnidades_UndMedSigla = new String[] {""} ;
         P00Q73_n1206ContratoUnidades_UndMedSigla = new bool[] {false} ;
         P00Q73_A1208ContratoUnidades_Produtividade = new decimal[1] ;
         P00Q73_n1208ContratoUnidades_Produtividade = new bool[] {false} ;
         P00Q73_A1205ContratoUnidades_UndMedNom = new String[] {""} ;
         P00Q73_n1205ContratoUnidades_UndMedNom = new bool[] {false} ;
         P00Q73_A1207ContratoUnidades_ContratoCod = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwcontratounidadesfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00Q72_A1204ContratoUnidades_UndMedCod, P00Q72_A1208ContratoUnidades_Produtividade, P00Q72_n1208ContratoUnidades_Produtividade, P00Q72_A1206ContratoUnidades_UndMedSigla, P00Q72_n1206ContratoUnidades_UndMedSigla, P00Q72_A1205ContratoUnidades_UndMedNom, P00Q72_n1205ContratoUnidades_UndMedNom, P00Q72_A1207ContratoUnidades_ContratoCod
               }
               , new Object[] {
               P00Q73_A1204ContratoUnidades_UndMedCod, P00Q73_A1206ContratoUnidades_UndMedSigla, P00Q73_n1206ContratoUnidades_UndMedSigla, P00Q73_A1208ContratoUnidades_Produtividade, P00Q73_n1208ContratoUnidades_Produtividade, P00Q73_A1205ContratoUnidades_UndMedNom, P00Q73_n1205ContratoUnidades_UndMedNom, P00Q73_A1207ContratoUnidades_ContratoCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV36GXV1 ;
      private int A1204ContratoUnidades_UndMedCod ;
      private int A1207ContratoUnidades_ContratoCod ;
      private int AV19InsertIndex ;
      private long AV28count ;
      private decimal AV14TFContratoUnidades_Produtividade ;
      private decimal AV15TFContratoUnidades_Produtividade_To ;
      private decimal AV42WWContratoUnidadesDS_5_Tfcontratounidades_produtividade ;
      private decimal AV43WWContratoUnidadesDS_6_Tfcontratounidades_produtividade_to ;
      private decimal A1208ContratoUnidades_Produtividade ;
      private String AV10TFContratoUnidades_UndMedNom ;
      private String AV11TFContratoUnidades_UndMedNom_Sel ;
      private String AV12TFContratoUnidades_UndMedSigla ;
      private String AV13TFContratoUnidades_UndMedSigla_Sel ;
      private String AV38WWContratoUnidadesDS_1_Tfcontratounidades_undmednom ;
      private String AV39WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel ;
      private String AV40WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla ;
      private String AV41WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel ;
      private String scmdbuf ;
      private String lV38WWContratoUnidadesDS_1_Tfcontratounidades_undmednom ;
      private String lV40WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla ;
      private String A1205ContratoUnidades_UndMedNom ;
      private String A1206ContratoUnidades_UndMedSigla ;
      private bool returnInSub ;
      private bool BRKQ72 ;
      private bool n1208ContratoUnidades_Produtividade ;
      private bool n1206ContratoUnidades_UndMedSigla ;
      private bool n1205ContratoUnidades_UndMedNom ;
      private bool BRKQ74 ;
      private String AV27OptionIndexesJson ;
      private String AV22OptionsJson ;
      private String AV25OptionsDescJson ;
      private String AV18DDOName ;
      private String AV16SearchTxt ;
      private String AV17SearchTxtTo ;
      private String AV20Option ;
      private IGxSession AV29Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00Q72_A1204ContratoUnidades_UndMedCod ;
      private decimal[] P00Q72_A1208ContratoUnidades_Produtividade ;
      private bool[] P00Q72_n1208ContratoUnidades_Produtividade ;
      private String[] P00Q72_A1206ContratoUnidades_UndMedSigla ;
      private bool[] P00Q72_n1206ContratoUnidades_UndMedSigla ;
      private String[] P00Q72_A1205ContratoUnidades_UndMedNom ;
      private bool[] P00Q72_n1205ContratoUnidades_UndMedNom ;
      private int[] P00Q72_A1207ContratoUnidades_ContratoCod ;
      private int[] P00Q73_A1204ContratoUnidades_UndMedCod ;
      private String[] P00Q73_A1206ContratoUnidades_UndMedSigla ;
      private bool[] P00Q73_n1206ContratoUnidades_UndMedSigla ;
      private decimal[] P00Q73_A1208ContratoUnidades_Produtividade ;
      private bool[] P00Q73_n1208ContratoUnidades_Produtividade ;
      private String[] P00Q73_A1205ContratoUnidades_UndMedNom ;
      private bool[] P00Q73_n1205ContratoUnidades_UndMedNom ;
      private int[] P00Q73_A1207ContratoUnidades_ContratoCod ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV31GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV32GridStateFilterValue ;
   }

   public class getwwcontratounidadesfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00Q72( IGxContext context ,
                                             String AV39WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel ,
                                             String AV38WWContratoUnidadesDS_1_Tfcontratounidades_undmednom ,
                                             String AV41WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel ,
                                             String AV40WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla ,
                                             decimal AV42WWContratoUnidadesDS_5_Tfcontratounidades_produtividade ,
                                             decimal AV43WWContratoUnidadesDS_6_Tfcontratounidades_produtividade_to ,
                                             String A1205ContratoUnidades_UndMedNom ,
                                             String A1206ContratoUnidades_UndMedSigla ,
                                             decimal A1208ContratoUnidades_Produtividade )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [6] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoUnidades_UndMedCod] AS ContratoUnidades_UndMedCod, T1.[ContratoUnidades_Produtividade], T2.[UnidadeMedicao_Sigla] AS ContratoUnidades_UndMedSigla, T2.[UnidadeMedicao_Nome] AS ContratoUnidades_UndMedNom, T1.[ContratoUnidades_ContratoCod] FROM ([ContratoUnidades] T1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T2 WITH (NOLOCK) ON T2.[UnidadeMedicao_Codigo] = T1.[ContratoUnidades_UndMedCod])";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV39WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38WWContratoUnidadesDS_1_Tfcontratounidades_undmednom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] like @lV38WWContratoUnidadesDS_1_Tfcontratounidades_undmednom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeMedicao_Nome] like @lV38WWContratoUnidadesDS_1_Tfcontratounidades_undmednom)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] = @AV39WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeMedicao_Nome] = @AV39WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV41WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Sigla] like @lV40WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeMedicao_Sigla] like @lV40WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Sigla] = @AV41WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeMedicao_Sigla] = @AV41WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV42WWContratoUnidadesDS_5_Tfcontratounidades_produtividade) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoUnidades_Produtividade] >= @AV42WWContratoUnidadesDS_5_Tfcontratounidades_produtividade)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoUnidades_Produtividade] >= @AV42WWContratoUnidadesDS_5_Tfcontratounidades_produtividade)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV43WWContratoUnidadesDS_6_Tfcontratounidades_produtividade_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoUnidades_Produtividade] <= @AV43WWContratoUnidadesDS_6_Tfcontratounidades_produtividade_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoUnidades_Produtividade] <= @AV43WWContratoUnidadesDS_6_Tfcontratounidades_produtividade_to)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoUnidades_UndMedCod]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00Q73( IGxContext context ,
                                             String AV39WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel ,
                                             String AV38WWContratoUnidadesDS_1_Tfcontratounidades_undmednom ,
                                             String AV41WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel ,
                                             String AV40WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla ,
                                             decimal AV42WWContratoUnidadesDS_5_Tfcontratounidades_produtividade ,
                                             decimal AV43WWContratoUnidadesDS_6_Tfcontratounidades_produtividade_to ,
                                             String A1205ContratoUnidades_UndMedNom ,
                                             String A1206ContratoUnidades_UndMedSigla ,
                                             decimal A1208ContratoUnidades_Produtividade )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [6] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoUnidades_UndMedCod] AS ContratoUnidades_UndMedCod, T2.[UnidadeMedicao_Sigla] AS ContratoUnidades_UndMedSigla, T1.[ContratoUnidades_Produtividade], T2.[UnidadeMedicao_Nome] AS ContratoUnidades_UndMedNom, T1.[ContratoUnidades_ContratoCod] FROM ([ContratoUnidades] T1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T2 WITH (NOLOCK) ON T2.[UnidadeMedicao_Codigo] = T1.[ContratoUnidades_UndMedCod])";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV39WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38WWContratoUnidadesDS_1_Tfcontratounidades_undmednom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] like @lV38WWContratoUnidadesDS_1_Tfcontratounidades_undmednom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeMedicao_Nome] like @lV38WWContratoUnidadesDS_1_Tfcontratounidades_undmednom)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] = @AV39WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeMedicao_Nome] = @AV39WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV41WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Sigla] like @lV40WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeMedicao_Sigla] like @lV40WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Sigla] = @AV41WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeMedicao_Sigla] = @AV41WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV42WWContratoUnidadesDS_5_Tfcontratounidades_produtividade) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoUnidades_Produtividade] >= @AV42WWContratoUnidadesDS_5_Tfcontratounidades_produtividade)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoUnidades_Produtividade] >= @AV42WWContratoUnidadesDS_5_Tfcontratounidades_produtividade)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV43WWContratoUnidadesDS_6_Tfcontratounidades_produtividade_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoUnidades_Produtividade] <= @AV43WWContratoUnidadesDS_6_Tfcontratounidades_produtividade_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoUnidades_Produtividade] <= @AV43WWContratoUnidadesDS_6_Tfcontratounidades_produtividade_to)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T2.[UnidadeMedicao_Sigla]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00Q72(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (decimal)dynConstraints[4] , (decimal)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (decimal)dynConstraints[8] );
               case 1 :
                     return conditional_P00Q73(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (decimal)dynConstraints[4] , (decimal)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (decimal)dynConstraints[8] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00Q72 ;
          prmP00Q72 = new Object[] {
          new Object[] {"@lV38WWContratoUnidadesDS_1_Tfcontratounidades_undmednom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV39WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV40WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV41WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV42WWContratoUnidadesDS_5_Tfcontratounidades_produtividade",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV43WWContratoUnidadesDS_6_Tfcontratounidades_produtividade_to",SqlDbType.Decimal,14,5}
          } ;
          Object[] prmP00Q73 ;
          prmP00Q73 = new Object[] {
          new Object[] {"@lV38WWContratoUnidadesDS_1_Tfcontratounidades_undmednom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV39WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV40WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV41WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV42WWContratoUnidadesDS_5_Tfcontratounidades_produtividade",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV43WWContratoUnidadesDS_6_Tfcontratounidades_produtividade_to",SqlDbType.Decimal,14,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00Q72", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00Q72,100,0,true,false )
             ,new CursorDef("P00Q73", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00Q73,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[11]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[11]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwcontratounidadesfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwcontratounidadesfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwcontratounidadesfilterdata") )
          {
             return  ;
          }
          getwwcontratounidadesfilterdata worker = new getwwcontratounidadesfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
