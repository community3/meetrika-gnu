/*
               File: PromptContratoServicos
        Description: Selecione Contrato Servicos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/15/2020 23:17:5.72
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptcontratoservicos : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptcontratoservicos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptcontratoservicos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutContratoServicos_Codigo )
      {
         this.AV7InOutContratoServicos_Codigo = aP0_InOutContratoServicos_Codigo;
         executePrivate();
         aP0_InOutContratoServicos_Codigo=this.AV7InOutContratoServicos_Codigo;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_95 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_95_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_95_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV12OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
               AV13OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
               AV14DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14DynamicFiltersSelector1", AV14DynamicFiltersSelector1);
               AV16ContratoServicos_Codigo1 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoServicos_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16ContratoServicos_Codigo1), 6, 0)));
               AV32Contrato_Codigo1 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Contrato_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32Contrato_Codigo1), 6, 0)));
               AV33Contrato_Numero1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Contrato_Numero1", AV33Contrato_Numero1);
               AV34Contrato_Numero_To1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Contrato_Numero_To1", AV34Contrato_Numero_To1);
               AV44Servico_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Servico_Nome1", AV44Servico_Nome1);
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV21ContratoServicos_Codigo2 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoServicos_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21ContratoServicos_Codigo2), 6, 0)));
               AV36Contrato_Codigo2 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Contrato_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Contrato_Codigo2), 6, 0)));
               AV37Contrato_Numero2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Contrato_Numero2", AV37Contrato_Numero2);
               AV38Contrato_Numero_To2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Contrato_Numero_To2", AV38Contrato_Numero_To2);
               AV45Servico_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45Servico_Nome2", AV45Servico_Nome2);
               AV24DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
               AV26ContratoServicos_Codigo3 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ContratoServicos_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26ContratoServicos_Codigo3), 6, 0)));
               AV40Contrato_Codigo3 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Contrato_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40Contrato_Codigo3), 6, 0)));
               AV41Contrato_Numero3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Contrato_Numero3", AV41Contrato_Numero3);
               AV42Contrato_Numero_To3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Contrato_Numero_To3", AV42Contrato_Numero_To3);
               AV46Servico_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46Servico_Nome3", AV46Servico_Nome3);
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV23DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
               AV48TFContratoServicos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48TFContratoServicos_Codigo), 6, 0)));
               AV49TFContratoServicos_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContratoServicos_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49TFContratoServicos_Codigo_To), 6, 0)));
               AV52TFContrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52TFContrato_Codigo), 6, 0)));
               AV53TFContrato_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53TFContrato_Codigo_To), 6, 0)));
               AV56TFContrato_Numero = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContrato_Numero", AV56TFContrato_Numero);
               AV57TFContrato_Numero_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFContrato_Numero_Sel", AV57TFContrato_Numero_Sel);
               AV60TFServico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60TFServico_Codigo), 6, 0)));
               AV61TFServico_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFServico_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61TFServico_Codigo_To), 6, 0)));
               AV64TFServico_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFServico_Nome", AV64TFServico_Nome);
               AV65TFServico_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFServico_Nome_Sel", AV65TFServico_Nome_Sel);
               AV50ddo_ContratoServicos_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ddo_ContratoServicos_CodigoTitleControlIdToReplace", AV50ddo_ContratoServicos_CodigoTitleControlIdToReplace);
               AV54ddo_Contrato_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ddo_Contrato_CodigoTitleControlIdToReplace", AV54ddo_Contrato_CodigoTitleControlIdToReplace);
               AV58ddo_Contrato_NumeroTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58ddo_Contrato_NumeroTitleControlIdToReplace", AV58ddo_Contrato_NumeroTitleControlIdToReplace);
               AV62ddo_Servico_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62ddo_Servico_CodigoTitleControlIdToReplace", AV62ddo_Servico_CodigoTitleControlIdToReplace);
               AV66ddo_Servico_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ddo_Servico_NomeTitleControlIdToReplace", AV66ddo_Servico_NomeTitleControlIdToReplace);
               AV74Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV9GridState);
               AV29DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersIgnoreFirst", AV29DynamicFiltersIgnoreFirst);
               AV28DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersRemoving", AV28DynamicFiltersRemoving);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV12OrderedBy, AV13OrderedDsc, AV14DynamicFiltersSelector1, AV16ContratoServicos_Codigo1, AV32Contrato_Codigo1, AV33Contrato_Numero1, AV34Contrato_Numero_To1, AV44Servico_Nome1, AV19DynamicFiltersSelector2, AV21ContratoServicos_Codigo2, AV36Contrato_Codigo2, AV37Contrato_Numero2, AV38Contrato_Numero_To2, AV45Servico_Nome2, AV24DynamicFiltersSelector3, AV26ContratoServicos_Codigo3, AV40Contrato_Codigo3, AV41Contrato_Numero3, AV42Contrato_Numero_To3, AV46Servico_Nome3, AV18DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV48TFContratoServicos_Codigo, AV49TFContratoServicos_Codigo_To, AV52TFContrato_Codigo, AV53TFContrato_Codigo_To, AV56TFContrato_Numero, AV57TFContrato_Numero_Sel, AV60TFServico_Codigo, AV61TFServico_Codigo_To, AV64TFServico_Nome, AV65TFServico_Nome_Sel, AV50ddo_ContratoServicos_CodigoTitleControlIdToReplace, AV54ddo_Contrato_CodigoTitleControlIdToReplace, AV58ddo_Contrato_NumeroTitleControlIdToReplace, AV62ddo_Servico_CodigoTitleControlIdToReplace, AV66ddo_Servico_NomeTitleControlIdToReplace, AV74Pgmname, AV9GridState, AV29DynamicFiltersIgnoreFirst, AV28DynamicFiltersRemoving) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutContratoServicos_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratoServicos_Codigo), 6, 0)));
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PA742( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV74Pgmname = "PromptContratoServicos";
               context.Gx_err = 0;
               WS742( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE742( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020315231764");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptcontratoservicos.aspx") + "?" + UrlEncode("" +AV7InOutContratoServicos_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV13OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV14DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOSERVICOS_CODIGO1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16ContratoServicos_Codigo1), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_CODIGO1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32Contrato_Codigo1), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_NUMERO1", StringUtil.RTrim( AV33Contrato_Numero1));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_NUMERO_TO1", StringUtil.RTrim( AV34Contrato_Numero_To1));
         GxWebStd.gx_hidden_field( context, "GXH_vSERVICO_NOME1", StringUtil.RTrim( AV44Servico_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOSERVICOS_CODIGO2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21ContratoServicos_Codigo2), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_CODIGO2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36Contrato_Codigo2), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_NUMERO2", StringUtil.RTrim( AV37Contrato_Numero2));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_NUMERO_TO2", StringUtil.RTrim( AV38Contrato_Numero_To2));
         GxWebStd.gx_hidden_field( context, "GXH_vSERVICO_NOME2", StringUtil.RTrim( AV45Servico_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV24DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOSERVICOS_CODIGO3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV26ContratoServicos_Codigo3), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_CODIGO3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40Contrato_Codigo3), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_NUMERO3", StringUtil.RTrim( AV41Contrato_Numero3));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_NUMERO_TO3", StringUtil.RTrim( AV42Contrato_Numero_To3));
         GxWebStd.gx_hidden_field( context, "GXH_vSERVICO_NOME3", StringUtil.RTrim( AV46Servico_Nome3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV23DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV48TFContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOS_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV49TFContratoServicos_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV52TFContrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV53TFContrato_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_NUMERO", StringUtil.RTrim( AV56TFContrato_Numero));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_NUMERO_SEL", StringUtil.RTrim( AV57TFContrato_Numero_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV60TFServico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICO_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV61TFServico_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICO_NOME", StringUtil.RTrim( AV64TFServico_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICO_NOME_SEL", StringUtil.RTrim( AV65TFServico_Nome_Sel));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_95", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_95), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV69GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV70GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV67DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV67DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOS_CODIGOTITLEFILTERDATA", AV47ContratoServicos_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOS_CODIGOTITLEFILTERDATA", AV47ContratoServicos_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATO_CODIGOTITLEFILTERDATA", AV51Contrato_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATO_CODIGOTITLEFILTERDATA", AV51Contrato_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATO_NUMEROTITLEFILTERDATA", AV55Contrato_NumeroTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATO_NUMEROTITLEFILTERDATA", AV55Contrato_NumeroTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSERVICO_CODIGOTITLEFILTERDATA", AV59Servico_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSERVICO_CODIGOTITLEFILTERDATA", AV59Servico_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSERVICO_NOMETITLEFILTERDATA", AV63Servico_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSERVICO_NOMETITLEFILTERDATA", AV63Servico_NomeTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV74Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV9GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV9GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV29DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV28DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTCONTRATOSERVICOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOS_CODIGO_Caption", StringUtil.RTrim( Ddo_contratoservicos_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOS_CODIGO_Tooltip", StringUtil.RTrim( Ddo_contratoservicos_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOS_CODIGO_Cls", StringUtil.RTrim( Ddo_contratoservicos_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOS_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicos_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOS_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoservicos_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOS_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicos_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOS_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicos_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOS_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicos_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOS_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicos_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOS_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicos_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOS_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicos_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOS_CODIGO_Filtertype", StringUtil.RTrim( Ddo_contratoservicos_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOS_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicos_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOS_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicos_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOS_CODIGO_Sortasc", StringUtil.RTrim( Ddo_contratoservicos_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOS_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_contratoservicos_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOS_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicos_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOS_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicos_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOS_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicos_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOS_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicos_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Caption", StringUtil.RTrim( Ddo_contrato_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Tooltip", StringUtil.RTrim( Ddo_contrato_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Cls", StringUtil.RTrim( Ddo_contrato_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_contrato_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_contrato_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contrato_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contrato_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_contrato_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_contrato_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_contrato_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_contrato_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Filtertype", StringUtil.RTrim( Ddo_contrato_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_contrato_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_contrato_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Sortasc", StringUtil.RTrim( Ddo_contrato_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_contrato_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_contrato_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_contrato_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_contrato_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_contrato_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Caption", StringUtil.RTrim( Ddo_contrato_numero_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Tooltip", StringUtil.RTrim( Ddo_contrato_numero_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Cls", StringUtil.RTrim( Ddo_contrato_numero_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filteredtext_set", StringUtil.RTrim( Ddo_contrato_numero_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Selectedvalue_set", StringUtil.RTrim( Ddo_contrato_numero_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contrato_numero_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contrato_numero_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includesortasc", StringUtil.BoolToStr( Ddo_contrato_numero_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includesortdsc", StringUtil.BoolToStr( Ddo_contrato_numero_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Sortedstatus", StringUtil.RTrim( Ddo_contrato_numero_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includefilter", StringUtil.BoolToStr( Ddo_contrato_numero_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filtertype", StringUtil.RTrim( Ddo_contrato_numero_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filterisrange", StringUtil.BoolToStr( Ddo_contrato_numero_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includedatalist", StringUtil.BoolToStr( Ddo_contrato_numero_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Datalisttype", StringUtil.RTrim( Ddo_contrato_numero_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Datalistproc", StringUtil.RTrim( Ddo_contrato_numero_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contrato_numero_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Sortasc", StringUtil.RTrim( Ddo_contrato_numero_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Sortdsc", StringUtil.RTrim( Ddo_contrato_numero_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Loadingdata", StringUtil.RTrim( Ddo_contrato_numero_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Cleanfilter", StringUtil.RTrim( Ddo_contrato_numero_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Noresultsfound", StringUtil.RTrim( Ddo_contrato_numero_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Searchbuttontext", StringUtil.RTrim( Ddo_contrato_numero_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Caption", StringUtil.RTrim( Ddo_servico_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Tooltip", StringUtil.RTrim( Ddo_servico_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Cls", StringUtil.RTrim( Ddo_servico_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_servico_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_servico_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_servico_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_servico_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_servico_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_servico_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_servico_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_servico_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Filtertype", StringUtil.RTrim( Ddo_servico_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_servico_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_servico_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Sortasc", StringUtil.RTrim( Ddo_servico_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_servico_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_servico_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_servico_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_servico_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_servico_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Caption", StringUtil.RTrim( Ddo_servico_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Tooltip", StringUtil.RTrim( Ddo_servico_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Cls", StringUtil.RTrim( Ddo_servico_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_servico_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_servico_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_servico_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_servico_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_servico_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_servico_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_servico_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_servico_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Filtertype", StringUtil.RTrim( Ddo_servico_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_servico_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_servico_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Datalisttype", StringUtil.RTrim( Ddo_servico_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Datalistproc", StringUtil.RTrim( Ddo_servico_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_servico_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Sortasc", StringUtil.RTrim( Ddo_servico_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Sortdsc", StringUtil.RTrim( Ddo_servico_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Loadingdata", StringUtil.RTrim( Ddo_servico_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_servico_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_servico_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_servico_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOS_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicos_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOS_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicos_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOS_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoservicos_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_contrato_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_contrato_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_contrato_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Activeeventkey", StringUtil.RTrim( Ddo_contrato_numero_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filteredtext_get", StringUtil.RTrim( Ddo_contrato_numero_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Selectedvalue_get", StringUtil.RTrim( Ddo_contrato_numero_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_servico_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_servico_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_servico_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_servico_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_servico_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_servico_nome_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm742( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptContratoServicos" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Contrato Servicos" ;
      }

      protected void WB740( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_742( true) ;
         }
         else
         {
            wb_table1_2_742( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_742e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_95_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(106, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,106);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_95_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV23DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(107, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,107);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_95_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicos_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV48TFContratoServicos_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV48TFContratoServicos_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,108);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicos_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicos_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_95_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicos_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV49TFContratoServicos_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV49TFContratoServicos_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,109);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicos_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicos_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_95_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV52TFContrato_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV52TFContrato_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,110);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'" + sGXsfl_95_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV53TFContrato_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV53TFContrato_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,111);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_95_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_numero_Internalname, StringUtil.RTrim( AV56TFContrato_Numero), StringUtil.RTrim( context.localUtil.Format( AV56TFContrato_Numero, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,112);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_numero_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_numero_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoServicos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_95_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_numero_sel_Internalname, StringUtil.RTrim( AV57TFContrato_Numero_Sel), StringUtil.RTrim( context.localUtil.Format( AV57TFContrato_Numero_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,113);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_numero_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_numero_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoServicos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_95_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservico_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV60TFServico_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV60TFServico_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,114);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservico_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfservico_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_95_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservico_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV61TFServico_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV61TFServico_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,115);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservico_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfservico_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_95_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservico_nome_Internalname, StringUtil.RTrim( AV64TFServico_Nome), StringUtil.RTrim( context.localUtil.Format( AV64TFServico_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,116);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservico_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfservico_nome_Visible, 1, 0, "text", "", 380, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoServicos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_95_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservico_nome_sel_Internalname, StringUtil.RTrim( AV65TFServico_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV65TFServico_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,117);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservico_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfservico_nome_sel_Visible, 1, 0, "text", "", 380, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoServicos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSERVICOS_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'" + sGXsfl_95_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicos_codigotitlecontrolidtoreplace_Internalname, AV50ddo_ContratoServicos_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,119);\"", 0, edtavDdo_contratoservicos_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoServicos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATO_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'',false,'" + sGXsfl_95_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contrato_codigotitlecontrolidtoreplace_Internalname, AV54ddo_Contrato_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,121);\"", 0, edtavDdo_contrato_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoServicos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATO_NUMEROContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 123,'',false,'" + sGXsfl_95_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname, AV58ddo_Contrato_NumeroTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,123);\"", 0, edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoServicos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SERVICO_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 125,'',false,'" + sGXsfl_95_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_servico_codigotitlecontrolidtoreplace_Internalname, AV62ddo_Servico_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,125);\"", 0, edtavDdo_servico_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoServicos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SERVICO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 127,'',false,'" + sGXsfl_95_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_servico_nometitlecontrolidtoreplace_Internalname, AV66ddo_Servico_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,127);\"", 0, edtavDdo_servico_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoServicos.htm");
         }
         wbLoad = true;
      }

      protected void START742( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Contrato Servicos", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP740( ) ;
      }

      protected void WS742( )
      {
         START742( ) ;
         EVT742( ) ;
      }

      protected void EVT742( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11742 */
                           E11742 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOS_CODIGO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12742 */
                           E12742 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATO_CODIGO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13742 */
                           E13742 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATO_NUMERO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14742 */
                           E14742 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_SERVICO_CODIGO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15742 */
                           E15742 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_SERVICO_NOME.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16742 */
                           E16742 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17742 */
                           E17742 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18742 */
                           E18742 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19742 */
                           E19742 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20742 */
                           E20742 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E21742 */
                           E21742 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E22742 */
                           E22742 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E23742 */
                           E23742 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E24742 */
                           E24742 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E25742 */
                           E25742 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E26742 */
                           E26742 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_95_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_95_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_95_idx), 4, 0)), 4, "0");
                           SubsflControlProps_952( ) ;
                           AV30Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV30Select)) ? AV73Select_GXI : context.convertURL( context.PathToRelativeUrl( AV30Select))));
                           A160ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoServicos_Codigo_Internalname), ",", "."));
                           A74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContrato_Codigo_Internalname), ",", "."));
                           A77Contrato_Numero = cgiGet( edtContrato_Numero_Internalname);
                           A155Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( edtServico_Codigo_Internalname), ",", "."));
                           A608Servico_Nome = StringUtil.Upper( cgiGet( edtServico_Nome_Internalname));
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E27742 */
                                 E27742 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E28742 */
                                 E28742 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E29742 */
                                 E29742 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV12OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV13OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV14DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratoservicos_codigo1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATOSERVICOS_CODIGO1"), ",", ".") != Convert.ToDecimal( AV16ContratoServicos_Codigo1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contrato_codigo1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATO_CODIGO1"), ",", ".") != Convert.ToDecimal( AV32Contrato_Codigo1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contrato_numero1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_NUMERO1"), AV33Contrato_Numero1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contrato_numero_to1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_NUMERO_TO1"), AV34Contrato_Numero_To1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Servico_nome1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICO_NOME1"), AV44Servico_Nome1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratoservicos_codigo2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATOSERVICOS_CODIGO2"), ",", ".") != Convert.ToDecimal( AV21ContratoServicos_Codigo2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contrato_codigo2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATO_CODIGO2"), ",", ".") != Convert.ToDecimal( AV36Contrato_Codigo2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contrato_numero2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_NUMERO2"), AV37Contrato_Numero2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contrato_numero_to2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_NUMERO_TO2"), AV38Contrato_Numero_To2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Servico_nome2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICO_NOME2"), AV45Servico_Nome2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV24DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratoservicos_codigo3 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATOSERVICOS_CODIGO3"), ",", ".") != Convert.ToDecimal( AV26ContratoServicos_Codigo3 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contrato_codigo3 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATO_CODIGO3"), ",", ".") != Convert.ToDecimal( AV40Contrato_Codigo3 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contrato_numero3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_NUMERO3"), AV41Contrato_Numero3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contrato_numero_to3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_NUMERO_TO3"), AV42Contrato_Numero_To3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Servico_nome3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICO_NOME3"), AV46Servico_Nome3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV23DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicos_codigo Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOS_CODIGO"), ",", ".") != Convert.ToDecimal( AV48TFContratoServicos_Codigo )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicos_codigo_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOS_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV49TFContratoServicos_Codigo_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontrato_codigo Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATO_CODIGO"), ",", ".") != Convert.ToDecimal( AV52TFContrato_Codigo )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontrato_codigo_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV53TFContrato_Codigo_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontrato_numero Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO"), AV56TFContrato_Numero) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontrato_numero_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO_SEL"), AV57TFContrato_Numero_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfservico_codigo Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSERVICO_CODIGO"), ",", ".") != Convert.ToDecimal( AV60TFServico_Codigo )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfservico_codigo_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSERVICO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV61TFServico_Codigo_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfservico_nome Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICO_NOME"), AV64TFServico_Nome) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfservico_nome_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICO_NOME_SEL"), AV65TFServico_Nome_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E30742 */
                                       E30742 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE742( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm742( ) ;
            }
         }
      }

      protected void PA742( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV12OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTRATOSERVICOS_CODIGO", "C�digo", 0);
            cmbavDynamicfiltersselector1.addItem("CONTRATO_CODIGO", "C�digo do Contrato", 0);
            cmbavDynamicfiltersselector1.addItem("CONTRATO_NUMERO", "N�merratoo do Contrato", 0);
            cmbavDynamicfiltersselector1.addItem("SERVICO_NOME", "Servi�o", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV14DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV14DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14DynamicFiltersSelector1", AV14DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTRATOSERVICOS_CODIGO", "C�digo", 0);
            cmbavDynamicfiltersselector2.addItem("CONTRATO_CODIGO", "C�digo do Contrato", 0);
            cmbavDynamicfiltersselector2.addItem("CONTRATO_NUMERO", "N�merratoo do Contrato", 0);
            cmbavDynamicfiltersselector2.addItem("SERVICO_NOME", "Servi�o", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CONTRATOSERVICOS_CODIGO", "C�digo", 0);
            cmbavDynamicfiltersselector3.addItem("CONTRATO_CODIGO", "C�digo do Contrato", 0);
            cmbavDynamicfiltersselector3.addItem("CONTRATO_NUMERO", "N�merratoo do Contrato", 0);
            cmbavDynamicfiltersselector3.addItem("SERVICO_NOME", "Servi�o", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV24DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV24DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_952( ) ;
         while ( nGXsfl_95_idx <= nRC_GXsfl_95 )
         {
            sendrow_952( ) ;
            nGXsfl_95_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_95_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_95_idx+1));
            sGXsfl_95_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_95_idx), 4, 0)), 4, "0");
            SubsflControlProps_952( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV12OrderedBy ,
                                       bool AV13OrderedDsc ,
                                       String AV14DynamicFiltersSelector1 ,
                                       int AV16ContratoServicos_Codigo1 ,
                                       int AV32Contrato_Codigo1 ,
                                       String AV33Contrato_Numero1 ,
                                       String AV34Contrato_Numero_To1 ,
                                       String AV44Servico_Nome1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       int AV21ContratoServicos_Codigo2 ,
                                       int AV36Contrato_Codigo2 ,
                                       String AV37Contrato_Numero2 ,
                                       String AV38Contrato_Numero_To2 ,
                                       String AV45Servico_Nome2 ,
                                       String AV24DynamicFiltersSelector3 ,
                                       int AV26ContratoServicos_Codigo3 ,
                                       int AV40Contrato_Codigo3 ,
                                       String AV41Contrato_Numero3 ,
                                       String AV42Contrato_Numero_To3 ,
                                       String AV46Servico_Nome3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV23DynamicFiltersEnabled3 ,
                                       int AV48TFContratoServicos_Codigo ,
                                       int AV49TFContratoServicos_Codigo_To ,
                                       int AV52TFContrato_Codigo ,
                                       int AV53TFContrato_Codigo_To ,
                                       String AV56TFContrato_Numero ,
                                       String AV57TFContrato_Numero_Sel ,
                                       int AV60TFServico_Codigo ,
                                       int AV61TFServico_Codigo_To ,
                                       String AV64TFServico_Nome ,
                                       String AV65TFServico_Nome_Sel ,
                                       String AV50ddo_ContratoServicos_CodigoTitleControlIdToReplace ,
                                       String AV54ddo_Contrato_CodigoTitleControlIdToReplace ,
                                       String AV58ddo_Contrato_NumeroTitleControlIdToReplace ,
                                       String AV62ddo_Servico_CodigoTitleControlIdToReplace ,
                                       String AV66ddo_Servico_NomeTitleControlIdToReplace ,
                                       String AV74Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV9GridState ,
                                       bool AV29DynamicFiltersIgnoreFirst ,
                                       bool AV28DynamicFiltersRemoving )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF742( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A160ContratoServicos_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A160ContratoServicos_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_SERVICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A155Servico_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "SERVICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV12OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV14DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV14DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14DynamicFiltersSelector1", AV14DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV24DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV24DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF742( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV74Pgmname = "PromptContratoServicos";
         context.Gx_err = 0;
      }

      protected void RF742( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 95;
         /* Execute user event: E28742 */
         E28742 ();
         nGXsfl_95_idx = 1;
         sGXsfl_95_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_95_idx), 4, 0)), 4, "0");
         SubsflControlProps_952( ) ;
         nGXsfl_95_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_952( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV14DynamicFiltersSelector1 ,
                                                 AV16ContratoServicos_Codigo1 ,
                                                 AV32Contrato_Codigo1 ,
                                                 AV33Contrato_Numero1 ,
                                                 AV34Contrato_Numero_To1 ,
                                                 AV44Servico_Nome1 ,
                                                 AV18DynamicFiltersEnabled2 ,
                                                 AV19DynamicFiltersSelector2 ,
                                                 AV21ContratoServicos_Codigo2 ,
                                                 AV36Contrato_Codigo2 ,
                                                 AV37Contrato_Numero2 ,
                                                 AV38Contrato_Numero_To2 ,
                                                 AV45Servico_Nome2 ,
                                                 AV23DynamicFiltersEnabled3 ,
                                                 AV24DynamicFiltersSelector3 ,
                                                 AV26ContratoServicos_Codigo3 ,
                                                 AV40Contrato_Codigo3 ,
                                                 AV41Contrato_Numero3 ,
                                                 AV42Contrato_Numero_To3 ,
                                                 AV46Servico_Nome3 ,
                                                 AV48TFContratoServicos_Codigo ,
                                                 AV49TFContratoServicos_Codigo_To ,
                                                 AV52TFContrato_Codigo ,
                                                 AV53TFContrato_Codigo_To ,
                                                 AV57TFContrato_Numero_Sel ,
                                                 AV56TFContrato_Numero ,
                                                 AV60TFServico_Codigo ,
                                                 AV61TFServico_Codigo_To ,
                                                 AV65TFServico_Nome_Sel ,
                                                 AV64TFServico_Nome ,
                                                 A160ContratoServicos_Codigo ,
                                                 A74Contrato_Codigo ,
                                                 A77Contrato_Numero ,
                                                 A608Servico_Nome ,
                                                 A155Servico_Codigo ,
                                                 AV12OrderedBy ,
                                                 AV13OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV44Servico_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV44Servico_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Servico_Nome1", AV44Servico_Nome1);
            lV45Servico_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV45Servico_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45Servico_Nome2", AV45Servico_Nome2);
            lV46Servico_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV46Servico_Nome3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46Servico_Nome3", AV46Servico_Nome3);
            lV56TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV56TFContrato_Numero), 20, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContrato_Numero", AV56TFContrato_Numero);
            lV64TFServico_Nome = StringUtil.PadR( StringUtil.RTrim( AV64TFServico_Nome), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFServico_Nome", AV64TFServico_Nome);
            /* Using cursor H00742 */
            pr_default.execute(0, new Object[] {AV16ContratoServicos_Codigo1, AV32Contrato_Codigo1, AV33Contrato_Numero1, AV34Contrato_Numero_To1, lV44Servico_Nome1, AV21ContratoServicos_Codigo2, AV36Contrato_Codigo2, AV37Contrato_Numero2, AV38Contrato_Numero_To2, lV45Servico_Nome2, AV26ContratoServicos_Codigo3, AV40Contrato_Codigo3, AV41Contrato_Numero3, AV42Contrato_Numero_To3, lV46Servico_Nome3, AV48TFContratoServicos_Codigo, AV49TFContratoServicos_Codigo_To, AV52TFContrato_Codigo, AV53TFContrato_Codigo_To, lV56TFContrato_Numero, AV57TFContrato_Numero_Sel, AV60TFServico_Codigo, AV61TFServico_Codigo_To, lV64TFServico_Nome, AV65TFServico_Nome_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_95_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A608Servico_Nome = H00742_A608Servico_Nome[0];
               A155Servico_Codigo = H00742_A155Servico_Codigo[0];
               A77Contrato_Numero = H00742_A77Contrato_Numero[0];
               A74Contrato_Codigo = H00742_A74Contrato_Codigo[0];
               A160ContratoServicos_Codigo = H00742_A160ContratoServicos_Codigo[0];
               A608Servico_Nome = H00742_A608Servico_Nome[0];
               A77Contrato_Numero = H00742_A77Contrato_Numero[0];
               /* Execute user event: E29742 */
               E29742 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 95;
            WB740( ) ;
         }
         nGXsfl_95_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV14DynamicFiltersSelector1 ,
                                              AV16ContratoServicos_Codigo1 ,
                                              AV32Contrato_Codigo1 ,
                                              AV33Contrato_Numero1 ,
                                              AV34Contrato_Numero_To1 ,
                                              AV44Servico_Nome1 ,
                                              AV18DynamicFiltersEnabled2 ,
                                              AV19DynamicFiltersSelector2 ,
                                              AV21ContratoServicos_Codigo2 ,
                                              AV36Contrato_Codigo2 ,
                                              AV37Contrato_Numero2 ,
                                              AV38Contrato_Numero_To2 ,
                                              AV45Servico_Nome2 ,
                                              AV23DynamicFiltersEnabled3 ,
                                              AV24DynamicFiltersSelector3 ,
                                              AV26ContratoServicos_Codigo3 ,
                                              AV40Contrato_Codigo3 ,
                                              AV41Contrato_Numero3 ,
                                              AV42Contrato_Numero_To3 ,
                                              AV46Servico_Nome3 ,
                                              AV48TFContratoServicos_Codigo ,
                                              AV49TFContratoServicos_Codigo_To ,
                                              AV52TFContrato_Codigo ,
                                              AV53TFContrato_Codigo_To ,
                                              AV57TFContrato_Numero_Sel ,
                                              AV56TFContrato_Numero ,
                                              AV60TFServico_Codigo ,
                                              AV61TFServico_Codigo_To ,
                                              AV65TFServico_Nome_Sel ,
                                              AV64TFServico_Nome ,
                                              A160ContratoServicos_Codigo ,
                                              A74Contrato_Codigo ,
                                              A77Contrato_Numero ,
                                              A608Servico_Nome ,
                                              A155Servico_Codigo ,
                                              AV12OrderedBy ,
                                              AV13OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV44Servico_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV44Servico_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Servico_Nome1", AV44Servico_Nome1);
         lV45Servico_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV45Servico_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45Servico_Nome2", AV45Servico_Nome2);
         lV46Servico_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV46Servico_Nome3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46Servico_Nome3", AV46Servico_Nome3);
         lV56TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV56TFContrato_Numero), 20, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContrato_Numero", AV56TFContrato_Numero);
         lV64TFServico_Nome = StringUtil.PadR( StringUtil.RTrim( AV64TFServico_Nome), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFServico_Nome", AV64TFServico_Nome);
         /* Using cursor H00743 */
         pr_default.execute(1, new Object[] {AV16ContratoServicos_Codigo1, AV32Contrato_Codigo1, AV33Contrato_Numero1, AV34Contrato_Numero_To1, lV44Servico_Nome1, AV21ContratoServicos_Codigo2, AV36Contrato_Codigo2, AV37Contrato_Numero2, AV38Contrato_Numero_To2, lV45Servico_Nome2, AV26ContratoServicos_Codigo3, AV40Contrato_Codigo3, AV41Contrato_Numero3, AV42Contrato_Numero_To3, lV46Servico_Nome3, AV48TFContratoServicos_Codigo, AV49TFContratoServicos_Codigo_To, AV52TFContrato_Codigo, AV53TFContrato_Codigo_To, lV56TFContrato_Numero, AV57TFContrato_Numero_Sel, AV60TFServico_Codigo, AV61TFServico_Codigo_To, lV64TFServico_Nome, AV65TFServico_Nome_Sel});
         GRID_nRecordCount = H00743_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV12OrderedBy, AV13OrderedDsc, AV14DynamicFiltersSelector1, AV16ContratoServicos_Codigo1, AV32Contrato_Codigo1, AV33Contrato_Numero1, AV34Contrato_Numero_To1, AV44Servico_Nome1, AV19DynamicFiltersSelector2, AV21ContratoServicos_Codigo2, AV36Contrato_Codigo2, AV37Contrato_Numero2, AV38Contrato_Numero_To2, AV45Servico_Nome2, AV24DynamicFiltersSelector3, AV26ContratoServicos_Codigo3, AV40Contrato_Codigo3, AV41Contrato_Numero3, AV42Contrato_Numero_To3, AV46Servico_Nome3, AV18DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV48TFContratoServicos_Codigo, AV49TFContratoServicos_Codigo_To, AV52TFContrato_Codigo, AV53TFContrato_Codigo_To, AV56TFContrato_Numero, AV57TFContrato_Numero_Sel, AV60TFServico_Codigo, AV61TFServico_Codigo_To, AV64TFServico_Nome, AV65TFServico_Nome_Sel, AV50ddo_ContratoServicos_CodigoTitleControlIdToReplace, AV54ddo_Contrato_CodigoTitleControlIdToReplace, AV58ddo_Contrato_NumeroTitleControlIdToReplace, AV62ddo_Servico_CodigoTitleControlIdToReplace, AV66ddo_Servico_NomeTitleControlIdToReplace, AV74Pgmname, AV9GridState, AV29DynamicFiltersIgnoreFirst, AV28DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV12OrderedBy, AV13OrderedDsc, AV14DynamicFiltersSelector1, AV16ContratoServicos_Codigo1, AV32Contrato_Codigo1, AV33Contrato_Numero1, AV34Contrato_Numero_To1, AV44Servico_Nome1, AV19DynamicFiltersSelector2, AV21ContratoServicos_Codigo2, AV36Contrato_Codigo2, AV37Contrato_Numero2, AV38Contrato_Numero_To2, AV45Servico_Nome2, AV24DynamicFiltersSelector3, AV26ContratoServicos_Codigo3, AV40Contrato_Codigo3, AV41Contrato_Numero3, AV42Contrato_Numero_To3, AV46Servico_Nome3, AV18DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV48TFContratoServicos_Codigo, AV49TFContratoServicos_Codigo_To, AV52TFContrato_Codigo, AV53TFContrato_Codigo_To, AV56TFContrato_Numero, AV57TFContrato_Numero_Sel, AV60TFServico_Codigo, AV61TFServico_Codigo_To, AV64TFServico_Nome, AV65TFServico_Nome_Sel, AV50ddo_ContratoServicos_CodigoTitleControlIdToReplace, AV54ddo_Contrato_CodigoTitleControlIdToReplace, AV58ddo_Contrato_NumeroTitleControlIdToReplace, AV62ddo_Servico_CodigoTitleControlIdToReplace, AV66ddo_Servico_NomeTitleControlIdToReplace, AV74Pgmname, AV9GridState, AV29DynamicFiltersIgnoreFirst, AV28DynamicFiltersRemoving) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV12OrderedBy, AV13OrderedDsc, AV14DynamicFiltersSelector1, AV16ContratoServicos_Codigo1, AV32Contrato_Codigo1, AV33Contrato_Numero1, AV34Contrato_Numero_To1, AV44Servico_Nome1, AV19DynamicFiltersSelector2, AV21ContratoServicos_Codigo2, AV36Contrato_Codigo2, AV37Contrato_Numero2, AV38Contrato_Numero_To2, AV45Servico_Nome2, AV24DynamicFiltersSelector3, AV26ContratoServicos_Codigo3, AV40Contrato_Codigo3, AV41Contrato_Numero3, AV42Contrato_Numero_To3, AV46Servico_Nome3, AV18DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV48TFContratoServicos_Codigo, AV49TFContratoServicos_Codigo_To, AV52TFContrato_Codigo, AV53TFContrato_Codigo_To, AV56TFContrato_Numero, AV57TFContrato_Numero_Sel, AV60TFServico_Codigo, AV61TFServico_Codigo_To, AV64TFServico_Nome, AV65TFServico_Nome_Sel, AV50ddo_ContratoServicos_CodigoTitleControlIdToReplace, AV54ddo_Contrato_CodigoTitleControlIdToReplace, AV58ddo_Contrato_NumeroTitleControlIdToReplace, AV62ddo_Servico_CodigoTitleControlIdToReplace, AV66ddo_Servico_NomeTitleControlIdToReplace, AV74Pgmname, AV9GridState, AV29DynamicFiltersIgnoreFirst, AV28DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV12OrderedBy, AV13OrderedDsc, AV14DynamicFiltersSelector1, AV16ContratoServicos_Codigo1, AV32Contrato_Codigo1, AV33Contrato_Numero1, AV34Contrato_Numero_To1, AV44Servico_Nome1, AV19DynamicFiltersSelector2, AV21ContratoServicos_Codigo2, AV36Contrato_Codigo2, AV37Contrato_Numero2, AV38Contrato_Numero_To2, AV45Servico_Nome2, AV24DynamicFiltersSelector3, AV26ContratoServicos_Codigo3, AV40Contrato_Codigo3, AV41Contrato_Numero3, AV42Contrato_Numero_To3, AV46Servico_Nome3, AV18DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV48TFContratoServicos_Codigo, AV49TFContratoServicos_Codigo_To, AV52TFContrato_Codigo, AV53TFContrato_Codigo_To, AV56TFContrato_Numero, AV57TFContrato_Numero_Sel, AV60TFServico_Codigo, AV61TFServico_Codigo_To, AV64TFServico_Nome, AV65TFServico_Nome_Sel, AV50ddo_ContratoServicos_CodigoTitleControlIdToReplace, AV54ddo_Contrato_CodigoTitleControlIdToReplace, AV58ddo_Contrato_NumeroTitleControlIdToReplace, AV62ddo_Servico_CodigoTitleControlIdToReplace, AV66ddo_Servico_NomeTitleControlIdToReplace, AV74Pgmname, AV9GridState, AV29DynamicFiltersIgnoreFirst, AV28DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV12OrderedBy, AV13OrderedDsc, AV14DynamicFiltersSelector1, AV16ContratoServicos_Codigo1, AV32Contrato_Codigo1, AV33Contrato_Numero1, AV34Contrato_Numero_To1, AV44Servico_Nome1, AV19DynamicFiltersSelector2, AV21ContratoServicos_Codigo2, AV36Contrato_Codigo2, AV37Contrato_Numero2, AV38Contrato_Numero_To2, AV45Servico_Nome2, AV24DynamicFiltersSelector3, AV26ContratoServicos_Codigo3, AV40Contrato_Codigo3, AV41Contrato_Numero3, AV42Contrato_Numero_To3, AV46Servico_Nome3, AV18DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV48TFContratoServicos_Codigo, AV49TFContratoServicos_Codigo_To, AV52TFContrato_Codigo, AV53TFContrato_Codigo_To, AV56TFContrato_Numero, AV57TFContrato_Numero_Sel, AV60TFServico_Codigo, AV61TFServico_Codigo_To, AV64TFServico_Nome, AV65TFServico_Nome_Sel, AV50ddo_ContratoServicos_CodigoTitleControlIdToReplace, AV54ddo_Contrato_CodigoTitleControlIdToReplace, AV58ddo_Contrato_NumeroTitleControlIdToReplace, AV62ddo_Servico_CodigoTitleControlIdToReplace, AV66ddo_Servico_NomeTitleControlIdToReplace, AV74Pgmname, AV9GridState, AV29DynamicFiltersIgnoreFirst, AV28DynamicFiltersRemoving) ;
         }
         return (int)(0) ;
      }

      protected void STRUP740( )
      {
         /* Before Start, stand alone formulas. */
         AV74Pgmname = "PromptContratoServicos";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E27742 */
         E27742 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV67DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSERVICOS_CODIGOTITLEFILTERDATA"), AV47ContratoServicos_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATO_CODIGOTITLEFILTERDATA"), AV51Contrato_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATO_NUMEROTITLEFILTERDATA"), AV55Contrato_NumeroTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSERVICO_CODIGOTITLEFILTERDATA"), AV59Servico_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSERVICO_NOMETITLEFILTERDATA"), AV63Servico_NomeTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV12OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV14DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14DynamicFiltersSelector1", AV14DynamicFiltersSelector1);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContratoservicos_codigo1_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratoservicos_codigo1_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATOSERVICOS_CODIGO1");
               GX_FocusControl = edtavContratoservicos_codigo1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV16ContratoServicos_Codigo1 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoServicos_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16ContratoServicos_Codigo1), 6, 0)));
            }
            else
            {
               AV16ContratoServicos_Codigo1 = (int)(context.localUtil.CToN( cgiGet( edtavContratoservicos_codigo1_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoServicos_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16ContratoServicos_Codigo1), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContrato_codigo1_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContrato_codigo1_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATO_CODIGO1");
               GX_FocusControl = edtavContrato_codigo1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV32Contrato_Codigo1 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Contrato_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32Contrato_Codigo1), 6, 0)));
            }
            else
            {
               AV32Contrato_Codigo1 = (int)(context.localUtil.CToN( cgiGet( edtavContrato_codigo1_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Contrato_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32Contrato_Codigo1), 6, 0)));
            }
            AV33Contrato_Numero1 = cgiGet( edtavContrato_numero1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Contrato_Numero1", AV33Contrato_Numero1);
            AV34Contrato_Numero_To1 = cgiGet( edtavContrato_numero_to1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Contrato_Numero_To1", AV34Contrato_Numero_To1);
            AV44Servico_Nome1 = StringUtil.Upper( cgiGet( edtavServico_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Servico_Nome1", AV44Servico_Nome1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContratoservicos_codigo2_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratoservicos_codigo2_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATOSERVICOS_CODIGO2");
               GX_FocusControl = edtavContratoservicos_codigo2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV21ContratoServicos_Codigo2 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoServicos_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21ContratoServicos_Codigo2), 6, 0)));
            }
            else
            {
               AV21ContratoServicos_Codigo2 = (int)(context.localUtil.CToN( cgiGet( edtavContratoservicos_codigo2_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoServicos_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21ContratoServicos_Codigo2), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContrato_codigo2_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContrato_codigo2_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATO_CODIGO2");
               GX_FocusControl = edtavContrato_codigo2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36Contrato_Codigo2 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Contrato_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Contrato_Codigo2), 6, 0)));
            }
            else
            {
               AV36Contrato_Codigo2 = (int)(context.localUtil.CToN( cgiGet( edtavContrato_codigo2_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Contrato_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Contrato_Codigo2), 6, 0)));
            }
            AV37Contrato_Numero2 = cgiGet( edtavContrato_numero2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Contrato_Numero2", AV37Contrato_Numero2);
            AV38Contrato_Numero_To2 = cgiGet( edtavContrato_numero_to2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Contrato_Numero_To2", AV38Contrato_Numero_To2);
            AV45Servico_Nome2 = StringUtil.Upper( cgiGet( edtavServico_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45Servico_Nome2", AV45Servico_Nome2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV24DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContratoservicos_codigo3_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratoservicos_codigo3_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATOSERVICOS_CODIGO3");
               GX_FocusControl = edtavContratoservicos_codigo3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV26ContratoServicos_Codigo3 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ContratoServicos_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26ContratoServicos_Codigo3), 6, 0)));
            }
            else
            {
               AV26ContratoServicos_Codigo3 = (int)(context.localUtil.CToN( cgiGet( edtavContratoservicos_codigo3_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ContratoServicos_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26ContratoServicos_Codigo3), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContrato_codigo3_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContrato_codigo3_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATO_CODIGO3");
               GX_FocusControl = edtavContrato_codigo3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV40Contrato_Codigo3 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Contrato_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40Contrato_Codigo3), 6, 0)));
            }
            else
            {
               AV40Contrato_Codigo3 = (int)(context.localUtil.CToN( cgiGet( edtavContrato_codigo3_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Contrato_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40Contrato_Codigo3), 6, 0)));
            }
            AV41Contrato_Numero3 = cgiGet( edtavContrato_numero3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Contrato_Numero3", AV41Contrato_Numero3);
            AV42Contrato_Numero_To3 = cgiGet( edtavContrato_numero_to3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Contrato_Numero_To3", AV42Contrato_Numero_To3);
            AV46Servico_Nome3 = StringUtil.Upper( cgiGet( edtavServico_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46Servico_Nome3", AV46Servico_Nome3);
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV23DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicos_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicos_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOS_CODIGO");
               GX_FocusControl = edtavTfcontratoservicos_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV48TFContratoServicos_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48TFContratoServicos_Codigo), 6, 0)));
            }
            else
            {
               AV48TFContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicos_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48TFContratoServicos_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicos_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicos_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOS_CODIGO_TO");
               GX_FocusControl = edtavTfcontratoservicos_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV49TFContratoServicos_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContratoServicos_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49TFContratoServicos_Codigo_To), 6, 0)));
            }
            else
            {
               AV49TFContratoServicos_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicos_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContratoServicos_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49TFContratoServicos_Codigo_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontrato_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontrato_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATO_CODIGO");
               GX_FocusControl = edtavTfcontrato_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV52TFContrato_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52TFContrato_Codigo), 6, 0)));
            }
            else
            {
               AV52TFContrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfcontrato_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52TFContrato_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontrato_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontrato_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATO_CODIGO_TO");
               GX_FocusControl = edtavTfcontrato_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV53TFContrato_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53TFContrato_Codigo_To), 6, 0)));
            }
            else
            {
               AV53TFContrato_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontrato_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53TFContrato_Codigo_To), 6, 0)));
            }
            AV56TFContrato_Numero = cgiGet( edtavTfcontrato_numero_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContrato_Numero", AV56TFContrato_Numero);
            AV57TFContrato_Numero_Sel = cgiGet( edtavTfcontrato_numero_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFContrato_Numero_Sel", AV57TFContrato_Numero_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfservico_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfservico_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSERVICO_CODIGO");
               GX_FocusControl = edtavTfservico_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV60TFServico_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60TFServico_Codigo), 6, 0)));
            }
            else
            {
               AV60TFServico_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfservico_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60TFServico_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfservico_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfservico_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSERVICO_CODIGO_TO");
               GX_FocusControl = edtavTfservico_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV61TFServico_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFServico_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61TFServico_Codigo_To), 6, 0)));
            }
            else
            {
               AV61TFServico_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfservico_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFServico_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61TFServico_Codigo_To), 6, 0)));
            }
            AV64TFServico_Nome = StringUtil.Upper( cgiGet( edtavTfservico_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFServico_Nome", AV64TFServico_Nome);
            AV65TFServico_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfservico_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFServico_Nome_Sel", AV65TFServico_Nome_Sel);
            AV50ddo_ContratoServicos_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicos_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ddo_ContratoServicos_CodigoTitleControlIdToReplace", AV50ddo_ContratoServicos_CodigoTitleControlIdToReplace);
            AV54ddo_Contrato_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_contrato_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ddo_Contrato_CodigoTitleControlIdToReplace", AV54ddo_Contrato_CodigoTitleControlIdToReplace);
            AV58ddo_Contrato_NumeroTitleControlIdToReplace = cgiGet( edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58ddo_Contrato_NumeroTitleControlIdToReplace", AV58ddo_Contrato_NumeroTitleControlIdToReplace);
            AV62ddo_Servico_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_servico_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62ddo_Servico_CodigoTitleControlIdToReplace", AV62ddo_Servico_CodigoTitleControlIdToReplace);
            AV66ddo_Servico_NomeTitleControlIdToReplace = cgiGet( edtavDdo_servico_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ddo_Servico_NomeTitleControlIdToReplace", AV66ddo_Servico_NomeTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_95 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_95"), ",", "."));
            AV69GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV70GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contratoservicos_codigo_Caption = cgiGet( "DDO_CONTRATOSERVICOS_CODIGO_Caption");
            Ddo_contratoservicos_codigo_Tooltip = cgiGet( "DDO_CONTRATOSERVICOS_CODIGO_Tooltip");
            Ddo_contratoservicos_codigo_Cls = cgiGet( "DDO_CONTRATOSERVICOS_CODIGO_Cls");
            Ddo_contratoservicos_codigo_Filteredtext_set = cgiGet( "DDO_CONTRATOSERVICOS_CODIGO_Filteredtext_set");
            Ddo_contratoservicos_codigo_Filteredtextto_set = cgiGet( "DDO_CONTRATOSERVICOS_CODIGO_Filteredtextto_set");
            Ddo_contratoservicos_codigo_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSERVICOS_CODIGO_Dropdownoptionstype");
            Ddo_contratoservicos_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSERVICOS_CODIGO_Titlecontrolidtoreplace");
            Ddo_contratoservicos_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOS_CODIGO_Includesortasc"));
            Ddo_contratoservicos_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOS_CODIGO_Includesortdsc"));
            Ddo_contratoservicos_codigo_Sortedstatus = cgiGet( "DDO_CONTRATOSERVICOS_CODIGO_Sortedstatus");
            Ddo_contratoservicos_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOS_CODIGO_Includefilter"));
            Ddo_contratoservicos_codigo_Filtertype = cgiGet( "DDO_CONTRATOSERVICOS_CODIGO_Filtertype");
            Ddo_contratoservicos_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOS_CODIGO_Filterisrange"));
            Ddo_contratoservicos_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOS_CODIGO_Includedatalist"));
            Ddo_contratoservicos_codigo_Sortasc = cgiGet( "DDO_CONTRATOSERVICOS_CODIGO_Sortasc");
            Ddo_contratoservicos_codigo_Sortdsc = cgiGet( "DDO_CONTRATOSERVICOS_CODIGO_Sortdsc");
            Ddo_contratoservicos_codigo_Cleanfilter = cgiGet( "DDO_CONTRATOSERVICOS_CODIGO_Cleanfilter");
            Ddo_contratoservicos_codigo_Rangefilterfrom = cgiGet( "DDO_CONTRATOSERVICOS_CODIGO_Rangefilterfrom");
            Ddo_contratoservicos_codigo_Rangefilterto = cgiGet( "DDO_CONTRATOSERVICOS_CODIGO_Rangefilterto");
            Ddo_contratoservicos_codigo_Searchbuttontext = cgiGet( "DDO_CONTRATOSERVICOS_CODIGO_Searchbuttontext");
            Ddo_contrato_codigo_Caption = cgiGet( "DDO_CONTRATO_CODIGO_Caption");
            Ddo_contrato_codigo_Tooltip = cgiGet( "DDO_CONTRATO_CODIGO_Tooltip");
            Ddo_contrato_codigo_Cls = cgiGet( "DDO_CONTRATO_CODIGO_Cls");
            Ddo_contrato_codigo_Filteredtext_set = cgiGet( "DDO_CONTRATO_CODIGO_Filteredtext_set");
            Ddo_contrato_codigo_Filteredtextto_set = cgiGet( "DDO_CONTRATO_CODIGO_Filteredtextto_set");
            Ddo_contrato_codigo_Dropdownoptionstype = cgiGet( "DDO_CONTRATO_CODIGO_Dropdownoptionstype");
            Ddo_contrato_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATO_CODIGO_Titlecontrolidtoreplace");
            Ddo_contrato_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_CODIGO_Includesortasc"));
            Ddo_contrato_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_CODIGO_Includesortdsc"));
            Ddo_contrato_codigo_Sortedstatus = cgiGet( "DDO_CONTRATO_CODIGO_Sortedstatus");
            Ddo_contrato_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_CODIGO_Includefilter"));
            Ddo_contrato_codigo_Filtertype = cgiGet( "DDO_CONTRATO_CODIGO_Filtertype");
            Ddo_contrato_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_CODIGO_Filterisrange"));
            Ddo_contrato_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_CODIGO_Includedatalist"));
            Ddo_contrato_codigo_Sortasc = cgiGet( "DDO_CONTRATO_CODIGO_Sortasc");
            Ddo_contrato_codigo_Sortdsc = cgiGet( "DDO_CONTRATO_CODIGO_Sortdsc");
            Ddo_contrato_codigo_Cleanfilter = cgiGet( "DDO_CONTRATO_CODIGO_Cleanfilter");
            Ddo_contrato_codigo_Rangefilterfrom = cgiGet( "DDO_CONTRATO_CODIGO_Rangefilterfrom");
            Ddo_contrato_codigo_Rangefilterto = cgiGet( "DDO_CONTRATO_CODIGO_Rangefilterto");
            Ddo_contrato_codigo_Searchbuttontext = cgiGet( "DDO_CONTRATO_CODIGO_Searchbuttontext");
            Ddo_contrato_numero_Caption = cgiGet( "DDO_CONTRATO_NUMERO_Caption");
            Ddo_contrato_numero_Tooltip = cgiGet( "DDO_CONTRATO_NUMERO_Tooltip");
            Ddo_contrato_numero_Cls = cgiGet( "DDO_CONTRATO_NUMERO_Cls");
            Ddo_contrato_numero_Filteredtext_set = cgiGet( "DDO_CONTRATO_NUMERO_Filteredtext_set");
            Ddo_contrato_numero_Selectedvalue_set = cgiGet( "DDO_CONTRATO_NUMERO_Selectedvalue_set");
            Ddo_contrato_numero_Dropdownoptionstype = cgiGet( "DDO_CONTRATO_NUMERO_Dropdownoptionstype");
            Ddo_contrato_numero_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATO_NUMERO_Titlecontrolidtoreplace");
            Ddo_contrato_numero_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includesortasc"));
            Ddo_contrato_numero_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includesortdsc"));
            Ddo_contrato_numero_Sortedstatus = cgiGet( "DDO_CONTRATO_NUMERO_Sortedstatus");
            Ddo_contrato_numero_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includefilter"));
            Ddo_contrato_numero_Filtertype = cgiGet( "DDO_CONTRATO_NUMERO_Filtertype");
            Ddo_contrato_numero_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Filterisrange"));
            Ddo_contrato_numero_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includedatalist"));
            Ddo_contrato_numero_Datalisttype = cgiGet( "DDO_CONTRATO_NUMERO_Datalisttype");
            Ddo_contrato_numero_Datalistproc = cgiGet( "DDO_CONTRATO_NUMERO_Datalistproc");
            Ddo_contrato_numero_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATO_NUMERO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contrato_numero_Sortasc = cgiGet( "DDO_CONTRATO_NUMERO_Sortasc");
            Ddo_contrato_numero_Sortdsc = cgiGet( "DDO_CONTRATO_NUMERO_Sortdsc");
            Ddo_contrato_numero_Loadingdata = cgiGet( "DDO_CONTRATO_NUMERO_Loadingdata");
            Ddo_contrato_numero_Cleanfilter = cgiGet( "DDO_CONTRATO_NUMERO_Cleanfilter");
            Ddo_contrato_numero_Noresultsfound = cgiGet( "DDO_CONTRATO_NUMERO_Noresultsfound");
            Ddo_contrato_numero_Searchbuttontext = cgiGet( "DDO_CONTRATO_NUMERO_Searchbuttontext");
            Ddo_servico_codigo_Caption = cgiGet( "DDO_SERVICO_CODIGO_Caption");
            Ddo_servico_codigo_Tooltip = cgiGet( "DDO_SERVICO_CODIGO_Tooltip");
            Ddo_servico_codigo_Cls = cgiGet( "DDO_SERVICO_CODIGO_Cls");
            Ddo_servico_codigo_Filteredtext_set = cgiGet( "DDO_SERVICO_CODIGO_Filteredtext_set");
            Ddo_servico_codigo_Filteredtextto_set = cgiGet( "DDO_SERVICO_CODIGO_Filteredtextto_set");
            Ddo_servico_codigo_Dropdownoptionstype = cgiGet( "DDO_SERVICO_CODIGO_Dropdownoptionstype");
            Ddo_servico_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_SERVICO_CODIGO_Titlecontrolidtoreplace");
            Ddo_servico_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_CODIGO_Includesortasc"));
            Ddo_servico_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_CODIGO_Includesortdsc"));
            Ddo_servico_codigo_Sortedstatus = cgiGet( "DDO_SERVICO_CODIGO_Sortedstatus");
            Ddo_servico_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_CODIGO_Includefilter"));
            Ddo_servico_codigo_Filtertype = cgiGet( "DDO_SERVICO_CODIGO_Filtertype");
            Ddo_servico_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_CODIGO_Filterisrange"));
            Ddo_servico_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_CODIGO_Includedatalist"));
            Ddo_servico_codigo_Sortasc = cgiGet( "DDO_SERVICO_CODIGO_Sortasc");
            Ddo_servico_codigo_Sortdsc = cgiGet( "DDO_SERVICO_CODIGO_Sortdsc");
            Ddo_servico_codigo_Cleanfilter = cgiGet( "DDO_SERVICO_CODIGO_Cleanfilter");
            Ddo_servico_codigo_Rangefilterfrom = cgiGet( "DDO_SERVICO_CODIGO_Rangefilterfrom");
            Ddo_servico_codigo_Rangefilterto = cgiGet( "DDO_SERVICO_CODIGO_Rangefilterto");
            Ddo_servico_codigo_Searchbuttontext = cgiGet( "DDO_SERVICO_CODIGO_Searchbuttontext");
            Ddo_servico_nome_Caption = cgiGet( "DDO_SERVICO_NOME_Caption");
            Ddo_servico_nome_Tooltip = cgiGet( "DDO_SERVICO_NOME_Tooltip");
            Ddo_servico_nome_Cls = cgiGet( "DDO_SERVICO_NOME_Cls");
            Ddo_servico_nome_Filteredtext_set = cgiGet( "DDO_SERVICO_NOME_Filteredtext_set");
            Ddo_servico_nome_Selectedvalue_set = cgiGet( "DDO_SERVICO_NOME_Selectedvalue_set");
            Ddo_servico_nome_Dropdownoptionstype = cgiGet( "DDO_SERVICO_NOME_Dropdownoptionstype");
            Ddo_servico_nome_Titlecontrolidtoreplace = cgiGet( "DDO_SERVICO_NOME_Titlecontrolidtoreplace");
            Ddo_servico_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_NOME_Includesortasc"));
            Ddo_servico_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_NOME_Includesortdsc"));
            Ddo_servico_nome_Sortedstatus = cgiGet( "DDO_SERVICO_NOME_Sortedstatus");
            Ddo_servico_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_NOME_Includefilter"));
            Ddo_servico_nome_Filtertype = cgiGet( "DDO_SERVICO_NOME_Filtertype");
            Ddo_servico_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_NOME_Filterisrange"));
            Ddo_servico_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_NOME_Includedatalist"));
            Ddo_servico_nome_Datalisttype = cgiGet( "DDO_SERVICO_NOME_Datalisttype");
            Ddo_servico_nome_Datalistproc = cgiGet( "DDO_SERVICO_NOME_Datalistproc");
            Ddo_servico_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_SERVICO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_servico_nome_Sortasc = cgiGet( "DDO_SERVICO_NOME_Sortasc");
            Ddo_servico_nome_Sortdsc = cgiGet( "DDO_SERVICO_NOME_Sortdsc");
            Ddo_servico_nome_Loadingdata = cgiGet( "DDO_SERVICO_NOME_Loadingdata");
            Ddo_servico_nome_Cleanfilter = cgiGet( "DDO_SERVICO_NOME_Cleanfilter");
            Ddo_servico_nome_Noresultsfound = cgiGet( "DDO_SERVICO_NOME_Noresultsfound");
            Ddo_servico_nome_Searchbuttontext = cgiGet( "DDO_SERVICO_NOME_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contratoservicos_codigo_Activeeventkey = cgiGet( "DDO_CONTRATOSERVICOS_CODIGO_Activeeventkey");
            Ddo_contratoservicos_codigo_Filteredtext_get = cgiGet( "DDO_CONTRATOSERVICOS_CODIGO_Filteredtext_get");
            Ddo_contratoservicos_codigo_Filteredtextto_get = cgiGet( "DDO_CONTRATOSERVICOS_CODIGO_Filteredtextto_get");
            Ddo_contrato_codigo_Activeeventkey = cgiGet( "DDO_CONTRATO_CODIGO_Activeeventkey");
            Ddo_contrato_codigo_Filteredtext_get = cgiGet( "DDO_CONTRATO_CODIGO_Filteredtext_get");
            Ddo_contrato_codigo_Filteredtextto_get = cgiGet( "DDO_CONTRATO_CODIGO_Filteredtextto_get");
            Ddo_contrato_numero_Activeeventkey = cgiGet( "DDO_CONTRATO_NUMERO_Activeeventkey");
            Ddo_contrato_numero_Filteredtext_get = cgiGet( "DDO_CONTRATO_NUMERO_Filteredtext_get");
            Ddo_contrato_numero_Selectedvalue_get = cgiGet( "DDO_CONTRATO_NUMERO_Selectedvalue_get");
            Ddo_servico_codigo_Activeeventkey = cgiGet( "DDO_SERVICO_CODIGO_Activeeventkey");
            Ddo_servico_codigo_Filteredtext_get = cgiGet( "DDO_SERVICO_CODIGO_Filteredtext_get");
            Ddo_servico_codigo_Filteredtextto_get = cgiGet( "DDO_SERVICO_CODIGO_Filteredtextto_get");
            Ddo_servico_nome_Activeeventkey = cgiGet( "DDO_SERVICO_NOME_Activeeventkey");
            Ddo_servico_nome_Filteredtext_get = cgiGet( "DDO_SERVICO_NOME_Filteredtext_get");
            Ddo_servico_nome_Selectedvalue_get = cgiGet( "DDO_SERVICO_NOME_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV12OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV13OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV14DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATOSERVICOS_CODIGO1"), ",", ".") != Convert.ToDecimal( AV16ContratoServicos_Codigo1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATO_CODIGO1"), ",", ".") != Convert.ToDecimal( AV32Contrato_Codigo1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_NUMERO1"), AV33Contrato_Numero1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_NUMERO_TO1"), AV34Contrato_Numero_To1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICO_NOME1"), AV44Servico_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATOSERVICOS_CODIGO2"), ",", ".") != Convert.ToDecimal( AV21ContratoServicos_Codigo2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATO_CODIGO2"), ",", ".") != Convert.ToDecimal( AV36Contrato_Codigo2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_NUMERO2"), AV37Contrato_Numero2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_NUMERO_TO2"), AV38Contrato_Numero_To2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICO_NOME2"), AV45Servico_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV24DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATOSERVICOS_CODIGO3"), ",", ".") != Convert.ToDecimal( AV26ContratoServicos_Codigo3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATO_CODIGO3"), ",", ".") != Convert.ToDecimal( AV40Contrato_Codigo3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_NUMERO3"), AV41Contrato_Numero3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_NUMERO_TO3"), AV42Contrato_Numero_To3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICO_NOME3"), AV46Servico_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV23DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOS_CODIGO"), ",", ".") != Convert.ToDecimal( AV48TFContratoServicos_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOS_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV49TFContratoServicos_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATO_CODIGO"), ",", ".") != Convert.ToDecimal( AV52TFContrato_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV53TFContrato_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO"), AV56TFContrato_Numero) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO_SEL"), AV57TFContrato_Numero_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSERVICO_CODIGO"), ",", ".") != Convert.ToDecimal( AV60TFServico_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSERVICO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV61TFServico_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICO_NOME"), AV64TFServico_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICO_NOME_SEL"), AV65TFServico_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E27742 */
         E27742 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E27742( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV14DynamicFiltersSelector1 = "CONTRATOSERVICOS_CODIGO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14DynamicFiltersSelector1", AV14DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "CONTRATOSERVICOS_CODIGO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV24DynamicFiltersSelector3 = "CONTRATOSERVICOS_CODIGO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfcontratoservicos_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicos_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicos_codigo_Visible), 5, 0)));
         edtavTfcontratoservicos_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicos_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicos_codigo_to_Visible), 5, 0)));
         edtavTfcontrato_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_codigo_Visible), 5, 0)));
         edtavTfcontrato_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_codigo_to_Visible), 5, 0)));
         edtavTfcontrato_numero_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_numero_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_numero_Visible), 5, 0)));
         edtavTfcontrato_numero_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_numero_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_numero_sel_Visible), 5, 0)));
         edtavTfservico_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservico_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservico_codigo_Visible), 5, 0)));
         edtavTfservico_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservico_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservico_codigo_to_Visible), 5, 0)));
         edtavTfservico_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservico_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservico_nome_Visible), 5, 0)));
         edtavTfservico_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservico_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservico_nome_sel_Visible), 5, 0)));
         Ddo_contratoservicos_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicos_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicos_codigo_Internalname, "TitleControlIdToReplace", Ddo_contratoservicos_codigo_Titlecontrolidtoreplace);
         AV50ddo_ContratoServicos_CodigoTitleControlIdToReplace = Ddo_contratoservicos_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ddo_ContratoServicos_CodigoTitleControlIdToReplace", AV50ddo_ContratoServicos_CodigoTitleControlIdToReplace);
         edtavDdo_contratoservicos_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoservicos_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicos_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contrato_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_Contrato_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "TitleControlIdToReplace", Ddo_contrato_codigo_Titlecontrolidtoreplace);
         AV54ddo_Contrato_CodigoTitleControlIdToReplace = Ddo_contrato_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ddo_Contrato_CodigoTitleControlIdToReplace", AV54ddo_Contrato_CodigoTitleControlIdToReplace);
         edtavDdo_contrato_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contrato_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contrato_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contrato_numero_Titlecontrolidtoreplace = subGrid_Internalname+"_Contrato_Numero";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "TitleControlIdToReplace", Ddo_contrato_numero_Titlecontrolidtoreplace);
         AV58ddo_Contrato_NumeroTitleControlIdToReplace = Ddo_contrato_numero_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58ddo_Contrato_NumeroTitleControlIdToReplace", AV58ddo_Contrato_NumeroTitleControlIdToReplace);
         edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_servico_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_Servico_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_codigo_Internalname, "TitleControlIdToReplace", Ddo_servico_codigo_Titlecontrolidtoreplace);
         AV62ddo_Servico_CodigoTitleControlIdToReplace = Ddo_servico_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62ddo_Servico_CodigoTitleControlIdToReplace", AV62ddo_Servico_CodigoTitleControlIdToReplace);
         edtavDdo_servico_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_servico_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_servico_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_servico_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Servico_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_nome_Internalname, "TitleControlIdToReplace", Ddo_servico_nome_Titlecontrolidtoreplace);
         AV66ddo_Servico_NomeTitleControlIdToReplace = Ddo_servico_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ddo_Servico_NomeTitleControlIdToReplace", AV66ddo_Servico_NomeTitleControlIdToReplace);
         edtavDdo_servico_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_servico_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_servico_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione Contrato Servicos";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "do Servi�o", 0);
         cmbavOrderedby.addItem("2", "Contrato", 0);
         cmbavOrderedby.addItem("3", "N�mero do Contrato", 0);
         cmbavOrderedby.addItem("4", "Servi�o", 0);
         cmbavOrderedby.addItem("5", "Nome", 0);
         if ( AV12OrderedBy < 1 )
         {
            AV12OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV67DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV67DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E28742( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV47ContratoServicos_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV51Contrato_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV55Contrato_NumeroTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV59Servico_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV63Servico_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContratoServicos_Codigo_Titleformat = 2;
         edtContratoServicos_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "do Servi�o", AV50ddo_ContratoServicos_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicos_Codigo_Internalname, "Title", edtContratoServicos_Codigo_Title);
         edtContrato_Codigo_Titleformat = 2;
         edtContrato_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Contrato", AV54ddo_Contrato_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Codigo_Internalname, "Title", edtContrato_Codigo_Title);
         edtContrato_Numero_Titleformat = 2;
         edtContrato_Numero_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "N�mero do Contrato", AV58ddo_Contrato_NumeroTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Numero_Internalname, "Title", edtContrato_Numero_Title);
         edtServico_Codigo_Titleformat = 2;
         edtServico_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Servi�o", AV62ddo_Servico_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_Codigo_Internalname, "Title", edtServico_Codigo_Title);
         edtServico_Nome_Titleformat = 2;
         edtServico_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV66ddo_Servico_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_Nome_Internalname, "Title", edtServico_Nome_Title);
         AV69GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV69GridCurrentPage), 10, 0)));
         AV70GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV70GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV47ContratoServicos_CodigoTitleFilterData", AV47ContratoServicos_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV51Contrato_CodigoTitleFilterData", AV51Contrato_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV55Contrato_NumeroTitleFilterData", AV55Contrato_NumeroTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV59Servico_CodigoTitleFilterData", AV59Servico_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV63Servico_NomeTitleFilterData", AV63Servico_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV9GridState", AV9GridState);
      }

      protected void E11742( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV68PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV68PageToGo) ;
         }
      }

      protected void E12742( )
      {
         /* Ddo_contratoservicos_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicos_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV12OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_contratoservicos_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicos_codigo_Internalname, "SortedStatus", Ddo_contratoservicos_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicos_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV12OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_contratoservicos_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicos_codigo_Internalname, "SortedStatus", Ddo_contratoservicos_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicos_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV48TFContratoServicos_Codigo = (int)(NumberUtil.Val( Ddo_contratoservicos_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48TFContratoServicos_Codigo), 6, 0)));
            AV49TFContratoServicos_Codigo_To = (int)(NumberUtil.Val( Ddo_contratoservicos_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContratoServicos_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49TFContratoServicos_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13742( )
      {
         /* Ddo_contrato_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contrato_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV12OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_contrato_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "SortedStatus", Ddo_contrato_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV12OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_contrato_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "SortedStatus", Ddo_contrato_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV52TFContrato_Codigo = (int)(NumberUtil.Val( Ddo_contrato_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52TFContrato_Codigo), 6, 0)));
            AV53TFContrato_Codigo_To = (int)(NumberUtil.Val( Ddo_contrato_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53TFContrato_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14742( )
      {
         /* Ddo_contrato_numero_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contrato_numero_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV12OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_contrato_numero_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_numero_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV12OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_contrato_numero_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_numero_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV56TFContrato_Numero = Ddo_contrato_numero_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContrato_Numero", AV56TFContrato_Numero);
            AV57TFContrato_Numero_Sel = Ddo_contrato_numero_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFContrato_Numero_Sel", AV57TFContrato_Numero_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15742( )
      {
         /* Ddo_servico_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_servico_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV12OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_servico_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_codigo_Internalname, "SortedStatus", Ddo_servico_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servico_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV12OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_servico_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_codigo_Internalname, "SortedStatus", Ddo_servico_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servico_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV60TFServico_Codigo = (int)(NumberUtil.Val( Ddo_servico_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60TFServico_Codigo), 6, 0)));
            AV61TFServico_Codigo_To = (int)(NumberUtil.Val( Ddo_servico_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFServico_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61TFServico_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E16742( )
      {
         /* Ddo_servico_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_servico_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV12OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_servico_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_nome_Internalname, "SortedStatus", Ddo_servico_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servico_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV12OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_servico_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_nome_Internalname, "SortedStatus", Ddo_servico_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servico_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV64TFServico_Nome = Ddo_servico_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFServico_Nome", AV64TFServico_Nome);
            AV65TFServico_Nome_Sel = Ddo_servico_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFServico_Nome_Sel", AV65TFServico_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E29742( )
      {
         /* Grid_Load Routine */
         AV30Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV30Select);
         AV73Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 95;
         }
         sendrow_952( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_95_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(95, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E30742 */
         E30742 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E30742( )
      {
         /* Enter Routine */
         AV7InOutContratoServicos_Codigo = A160ContratoServicos_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratoServicos_Codigo), 6, 0)));
         context.setWebReturnParms(new Object[] {(int)AV7InOutContratoServicos_Codigo});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E17742( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E22742( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E18742( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV28DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersRemoving", AV28DynamicFiltersRemoving);
         AV29DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersIgnoreFirst", AV29DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV28DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersRemoving", AV28DynamicFiltersRemoving);
         AV29DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersIgnoreFirst", AV29DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV12OrderedBy, AV13OrderedDsc, AV14DynamicFiltersSelector1, AV16ContratoServicos_Codigo1, AV32Contrato_Codigo1, AV33Contrato_Numero1, AV34Contrato_Numero_To1, AV44Servico_Nome1, AV19DynamicFiltersSelector2, AV21ContratoServicos_Codigo2, AV36Contrato_Codigo2, AV37Contrato_Numero2, AV38Contrato_Numero_To2, AV45Servico_Nome2, AV24DynamicFiltersSelector3, AV26ContratoServicos_Codigo3, AV40Contrato_Codigo3, AV41Contrato_Numero3, AV42Contrato_Numero_To3, AV46Servico_Nome3, AV18DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV48TFContratoServicos_Codigo, AV49TFContratoServicos_Codigo_To, AV52TFContrato_Codigo, AV53TFContrato_Codigo_To, AV56TFContrato_Numero, AV57TFContrato_Numero_Sel, AV60TFServico_Codigo, AV61TFServico_Codigo_To, AV64TFServico_Nome, AV65TFServico_Nome_Sel, AV50ddo_ContratoServicos_CodigoTitleControlIdToReplace, AV54ddo_Contrato_CodigoTitleControlIdToReplace, AV58ddo_Contrato_NumeroTitleControlIdToReplace, AV62ddo_Servico_CodigoTitleControlIdToReplace, AV66ddo_Servico_NomeTitleControlIdToReplace, AV74Pgmname, AV9GridState, AV29DynamicFiltersIgnoreFirst, AV28DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV9GridState", AV9GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV14DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E23742( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E24742( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV23DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E19742( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV28DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersRemoving", AV28DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV28DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersRemoving", AV28DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV12OrderedBy, AV13OrderedDsc, AV14DynamicFiltersSelector1, AV16ContratoServicos_Codigo1, AV32Contrato_Codigo1, AV33Contrato_Numero1, AV34Contrato_Numero_To1, AV44Servico_Nome1, AV19DynamicFiltersSelector2, AV21ContratoServicos_Codigo2, AV36Contrato_Codigo2, AV37Contrato_Numero2, AV38Contrato_Numero_To2, AV45Servico_Nome2, AV24DynamicFiltersSelector3, AV26ContratoServicos_Codigo3, AV40Contrato_Codigo3, AV41Contrato_Numero3, AV42Contrato_Numero_To3, AV46Servico_Nome3, AV18DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV48TFContratoServicos_Codigo, AV49TFContratoServicos_Codigo_To, AV52TFContrato_Codigo, AV53TFContrato_Codigo_To, AV56TFContrato_Numero, AV57TFContrato_Numero_Sel, AV60TFServico_Codigo, AV61TFServico_Codigo_To, AV64TFServico_Nome, AV65TFServico_Nome_Sel, AV50ddo_ContratoServicos_CodigoTitleControlIdToReplace, AV54ddo_Contrato_CodigoTitleControlIdToReplace, AV58ddo_Contrato_NumeroTitleControlIdToReplace, AV62ddo_Servico_CodigoTitleControlIdToReplace, AV66ddo_Servico_NomeTitleControlIdToReplace, AV74Pgmname, AV9GridState, AV29DynamicFiltersIgnoreFirst, AV28DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV9GridState", AV9GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV14DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E25742( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E20742( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV28DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersRemoving", AV28DynamicFiltersRemoving);
         AV23DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV28DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersRemoving", AV28DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV12OrderedBy, AV13OrderedDsc, AV14DynamicFiltersSelector1, AV16ContratoServicos_Codigo1, AV32Contrato_Codigo1, AV33Contrato_Numero1, AV34Contrato_Numero_To1, AV44Servico_Nome1, AV19DynamicFiltersSelector2, AV21ContratoServicos_Codigo2, AV36Contrato_Codigo2, AV37Contrato_Numero2, AV38Contrato_Numero_To2, AV45Servico_Nome2, AV24DynamicFiltersSelector3, AV26ContratoServicos_Codigo3, AV40Contrato_Codigo3, AV41Contrato_Numero3, AV42Contrato_Numero_To3, AV46Servico_Nome3, AV18DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV48TFContratoServicos_Codigo, AV49TFContratoServicos_Codigo_To, AV52TFContrato_Codigo, AV53TFContrato_Codigo_To, AV56TFContrato_Numero, AV57TFContrato_Numero_Sel, AV60TFServico_Codigo, AV61TFServico_Codigo_To, AV64TFServico_Nome, AV65TFServico_Nome_Sel, AV50ddo_ContratoServicos_CodigoTitleControlIdToReplace, AV54ddo_Contrato_CodigoTitleControlIdToReplace, AV58ddo_Contrato_NumeroTitleControlIdToReplace, AV62ddo_Servico_CodigoTitleControlIdToReplace, AV66ddo_Servico_NomeTitleControlIdToReplace, AV74Pgmname, AV9GridState, AV29DynamicFiltersIgnoreFirst, AV28DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV9GridState", AV9GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV14DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E26742( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E21742( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV14DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV9GridState", AV9GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contratoservicos_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicos_codigo_Internalname, "SortedStatus", Ddo_contratoservicos_codigo_Sortedstatus);
         Ddo_contrato_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "SortedStatus", Ddo_contrato_codigo_Sortedstatus);
         Ddo_contrato_numero_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
         Ddo_servico_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_codigo_Internalname, "SortedStatus", Ddo_servico_codigo_Sortedstatus);
         Ddo_servico_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_nome_Internalname, "SortedStatus", Ddo_servico_nome_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV12OrderedBy == 1 )
         {
            Ddo_contratoservicos_codigo_Sortedstatus = (AV13OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicos_codigo_Internalname, "SortedStatus", Ddo_contratoservicos_codigo_Sortedstatus);
         }
         else if ( AV12OrderedBy == 2 )
         {
            Ddo_contrato_codigo_Sortedstatus = (AV13OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "SortedStatus", Ddo_contrato_codigo_Sortedstatus);
         }
         else if ( AV12OrderedBy == 3 )
         {
            Ddo_contrato_numero_Sortedstatus = (AV13OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
         }
         else if ( AV12OrderedBy == 4 )
         {
            Ddo_servico_codigo_Sortedstatus = (AV13OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_codigo_Internalname, "SortedStatus", Ddo_servico_codigo_Sortedstatus);
         }
         else if ( AV12OrderedBy == 5 )
         {
            Ddo_servico_nome_Sortedstatus = (AV13OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_nome_Internalname, "SortedStatus", Ddo_servico_nome_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavContratoservicos_codigo1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicos_codigo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicos_codigo1_Visible), 5, 0)));
         edtavContrato_codigo1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_codigo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_codigo1_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontrato_numero1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontrato_numero1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontrato_numero1_Visible), 5, 0)));
         edtavServico_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_nome1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTRATOSERVICOS_CODIGO") == 0 )
         {
            edtavContratoservicos_codigo1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicos_codigo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicos_codigo1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTRATO_CODIGO") == 0 )
         {
            edtavContrato_codigo1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_codigo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_codigo1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTRATO_NUMERO") == 0 )
         {
            tblTablemergeddynamicfilterscontrato_numero1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontrato_numero1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontrato_numero1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "SERVICO_NOME") == 0 )
         {
            edtavServico_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_nome1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavContratoservicos_codigo2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicos_codigo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicos_codigo2_Visible), 5, 0)));
         edtavContrato_codigo2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_codigo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_codigo2_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontrato_numero2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontrato_numero2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontrato_numero2_Visible), 5, 0)));
         edtavServico_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_nome2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOSERVICOS_CODIGO") == 0 )
         {
            edtavContratoservicos_codigo2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicos_codigo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicos_codigo2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATO_CODIGO") == 0 )
         {
            edtavContrato_codigo2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_codigo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_codigo2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATO_NUMERO") == 0 )
         {
            tblTablemergeddynamicfilterscontrato_numero2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontrato_numero2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontrato_numero2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SERVICO_NOME") == 0 )
         {
            edtavServico_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_nome2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavContratoservicos_codigo3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicos_codigo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicos_codigo3_Visible), 5, 0)));
         edtavContrato_codigo3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_codigo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_codigo3_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontrato_numero3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontrato_numero3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontrato_numero3_Visible), 5, 0)));
         edtavServico_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_nome3_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTRATOSERVICOS_CODIGO") == 0 )
         {
            edtavContratoservicos_codigo3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicos_codigo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicos_codigo3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTRATO_CODIGO") == 0 )
         {
            edtavContrato_codigo3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_codigo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_codigo3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTRATO_NUMERO") == 0 )
         {
            tblTablemergeddynamicfilterscontrato_numero3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontrato_numero3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontrato_numero3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "SERVICO_NOME") == 0 )
         {
            edtavServico_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_nome3_Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "CONTRATOSERVICOS_CODIGO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV21ContratoServicos_Codigo2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoServicos_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21ContratoServicos_Codigo2), 6, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
         AV24DynamicFiltersSelector3 = "CONTRATOSERVICOS_CODIGO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
         AV26ContratoServicos_Codigo3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ContratoServicos_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26ContratoServicos_Codigo3), 6, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV48TFContratoServicos_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48TFContratoServicos_Codigo), 6, 0)));
         Ddo_contratoservicos_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicos_codigo_Internalname, "FilteredText_set", Ddo_contratoservicos_codigo_Filteredtext_set);
         AV49TFContratoServicos_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContratoServicos_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49TFContratoServicos_Codigo_To), 6, 0)));
         Ddo_contratoservicos_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicos_codigo_Internalname, "FilteredTextTo_set", Ddo_contratoservicos_codigo_Filteredtextto_set);
         AV52TFContrato_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52TFContrato_Codigo), 6, 0)));
         Ddo_contrato_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "FilteredText_set", Ddo_contrato_codigo_Filteredtext_set);
         AV53TFContrato_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53TFContrato_Codigo_To), 6, 0)));
         Ddo_contrato_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "FilteredTextTo_set", Ddo_contrato_codigo_Filteredtextto_set);
         AV56TFContrato_Numero = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContrato_Numero", AV56TFContrato_Numero);
         Ddo_contrato_numero_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "FilteredText_set", Ddo_contrato_numero_Filteredtext_set);
         AV57TFContrato_Numero_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFContrato_Numero_Sel", AV57TFContrato_Numero_Sel);
         Ddo_contrato_numero_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SelectedValue_set", Ddo_contrato_numero_Selectedvalue_set);
         AV60TFServico_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60TFServico_Codigo), 6, 0)));
         Ddo_servico_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_codigo_Internalname, "FilteredText_set", Ddo_servico_codigo_Filteredtext_set);
         AV61TFServico_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFServico_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61TFServico_Codigo_To), 6, 0)));
         Ddo_servico_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_codigo_Internalname, "FilteredTextTo_set", Ddo_servico_codigo_Filteredtextto_set);
         AV64TFServico_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFServico_Nome", AV64TFServico_Nome);
         Ddo_servico_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_nome_Internalname, "FilteredText_set", Ddo_servico_nome_Filteredtext_set);
         AV65TFServico_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFServico_Nome_Sel", AV65TFServico_Nome_Sel);
         Ddo_servico_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_nome_Internalname, "SelectedValue_set", Ddo_servico_nome_Selectedvalue_set);
         AV14DynamicFiltersSelector1 = "CONTRATOSERVICOS_CODIGO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14DynamicFiltersSelector1", AV14DynamicFiltersSelector1);
         AV16ContratoServicos_Codigo1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoServicos_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16ContratoServicos_Codigo1), 6, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV9GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV9GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV11GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV9GridState.gxTpr_Dynamicfilters.Item(1));
            AV14DynamicFiltersSelector1 = AV11GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14DynamicFiltersSelector1", AV14DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTRATOSERVICOS_CODIGO") == 0 )
            {
               AV16ContratoServicos_Codigo1 = (int)(NumberUtil.Val( AV11GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoServicos_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16ContratoServicos_Codigo1), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTRATO_CODIGO") == 0 )
            {
               AV32Contrato_Codigo1 = (int)(NumberUtil.Val( AV11GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Contrato_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32Contrato_Codigo1), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTRATO_NUMERO") == 0 )
            {
               AV33Contrato_Numero1 = AV11GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Contrato_Numero1", AV33Contrato_Numero1);
               AV34Contrato_Numero_To1 = AV11GridStateDynamicFilter.gxTpr_Valueto;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Contrato_Numero_To1", AV34Contrato_Numero_To1);
            }
            else if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "SERVICO_NOME") == 0 )
            {
               AV44Servico_Nome1 = AV11GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Servico_Nome1", AV44Servico_Nome1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV9GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV11GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV9GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV11GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOSERVICOS_CODIGO") == 0 )
               {
                  AV21ContratoServicos_Codigo2 = (int)(NumberUtil.Val( AV11GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoServicos_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21ContratoServicos_Codigo2), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATO_CODIGO") == 0 )
               {
                  AV36Contrato_Codigo2 = (int)(NumberUtil.Val( AV11GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Contrato_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Contrato_Codigo2), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATO_NUMERO") == 0 )
               {
                  AV37Contrato_Numero2 = AV11GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Contrato_Numero2", AV37Contrato_Numero2);
                  AV38Contrato_Numero_To2 = AV11GridStateDynamicFilter.gxTpr_Valueto;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Contrato_Numero_To2", AV38Contrato_Numero_To2);
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SERVICO_NOME") == 0 )
               {
                  AV45Servico_Nome2 = AV11GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45Servico_Nome2", AV45Servico_Nome2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV9GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV23DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
                  AV11GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV9GridState.gxTpr_Dynamicfilters.Item(3));
                  AV24DynamicFiltersSelector3 = AV11GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTRATOSERVICOS_CODIGO") == 0 )
                  {
                     AV26ContratoServicos_Codigo3 = (int)(NumberUtil.Val( AV11GridStateDynamicFilter.gxTpr_Value, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ContratoServicos_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26ContratoServicos_Codigo3), 6, 0)));
                  }
                  else if ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTRATO_CODIGO") == 0 )
                  {
                     AV40Contrato_Codigo3 = (int)(NumberUtil.Val( AV11GridStateDynamicFilter.gxTpr_Value, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Contrato_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40Contrato_Codigo3), 6, 0)));
                  }
                  else if ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTRATO_NUMERO") == 0 )
                  {
                     AV41Contrato_Numero3 = AV11GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Contrato_Numero3", AV41Contrato_Numero3);
                     AV42Contrato_Numero_To3 = AV11GridStateDynamicFilter.gxTpr_Valueto;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Contrato_Numero_To3", AV42Contrato_Numero_To3);
                  }
                  else if ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "SERVICO_NOME") == 0 )
                  {
                     AV46Servico_Nome3 = AV11GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46Servico_Nome3", AV46Servico_Nome3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV28DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV9GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV9GridState.gxTpr_Orderedby = AV12OrderedBy;
         AV9GridState.gxTpr_Ordereddsc = AV13OrderedDsc;
         AV9GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV48TFContratoServicos_Codigo) && (0==AV49TFContratoServicos_Codigo_To) ) )
         {
            AV10GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV10GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOS_CODIGO";
            AV10GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV48TFContratoServicos_Codigo), 6, 0);
            AV10GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV49TFContratoServicos_Codigo_To), 6, 0);
            AV9GridState.gxTpr_Filtervalues.Add(AV10GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV52TFContrato_Codigo) && (0==AV53TFContrato_Codigo_To) ) )
         {
            AV10GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV10GridStateFilterValue.gxTpr_Name = "TFCONTRATO_CODIGO";
            AV10GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV52TFContrato_Codigo), 6, 0);
            AV10GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV53TFContrato_Codigo_To), 6, 0);
            AV9GridState.gxTpr_Filtervalues.Add(AV10GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56TFContrato_Numero)) )
         {
            AV10GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV10GridStateFilterValue.gxTpr_Name = "TFCONTRATO_NUMERO";
            AV10GridStateFilterValue.gxTpr_Value = AV56TFContrato_Numero;
            AV9GridState.gxTpr_Filtervalues.Add(AV10GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57TFContrato_Numero_Sel)) )
         {
            AV10GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV10GridStateFilterValue.gxTpr_Name = "TFCONTRATO_NUMERO_SEL";
            AV10GridStateFilterValue.gxTpr_Value = AV57TFContrato_Numero_Sel;
            AV9GridState.gxTpr_Filtervalues.Add(AV10GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV60TFServico_Codigo) && (0==AV61TFServico_Codigo_To) ) )
         {
            AV10GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV10GridStateFilterValue.gxTpr_Name = "TFSERVICO_CODIGO";
            AV10GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV60TFServico_Codigo), 6, 0);
            AV10GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV61TFServico_Codigo_To), 6, 0);
            AV9GridState.gxTpr_Filtervalues.Add(AV10GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64TFServico_Nome)) )
         {
            AV10GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV10GridStateFilterValue.gxTpr_Name = "TFSERVICO_NOME";
            AV10GridStateFilterValue.gxTpr_Value = AV64TFServico_Nome;
            AV9GridState.gxTpr_Filtervalues.Add(AV10GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65TFServico_Nome_Sel)) )
         {
            AV10GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV10GridStateFilterValue.gxTpr_Name = "TFSERVICO_NOME_SEL";
            AV10GridStateFilterValue.gxTpr_Value = AV65TFServico_Nome_Sel;
            AV9GridState.gxTpr_Filtervalues.Add(AV10GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV74Pgmname+"GridState",  AV9GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV9GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV29DynamicFiltersIgnoreFirst )
         {
            AV11GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV11GridStateDynamicFilter.gxTpr_Selected = AV14DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTRATOSERVICOS_CODIGO") == 0 ) && ! (0==AV16ContratoServicos_Codigo1) )
            {
               AV11GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV16ContratoServicos_Codigo1), 6, 0);
            }
            else if ( ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTRATO_CODIGO") == 0 ) && ! (0==AV32Contrato_Codigo1) )
            {
               AV11GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV32Contrato_Codigo1), 6, 0);
            }
            else if ( ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTRATO_NUMERO") == 0 ) && ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV33Contrato_Numero1)) && String.IsNullOrEmpty(StringUtil.RTrim( AV34Contrato_Numero_To1)) ) )
            {
               AV11GridStateDynamicFilter.gxTpr_Value = AV33Contrato_Numero1;
               AV11GridStateDynamicFilter.gxTpr_Valueto = AV34Contrato_Numero_To1;
            }
            else if ( ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "SERVICO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV44Servico_Nome1)) )
            {
               AV11GridStateDynamicFilter.gxTpr_Value = AV44Servico_Nome1;
            }
            if ( AV28DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV11GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV11GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV9GridState.gxTpr_Dynamicfilters.Add(AV11GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV11GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV11GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOSERVICOS_CODIGO") == 0 ) && ! (0==AV21ContratoServicos_Codigo2) )
            {
               AV11GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV21ContratoServicos_Codigo2), 6, 0);
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATO_CODIGO") == 0 ) && ! (0==AV36Contrato_Codigo2) )
            {
               AV11GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV36Contrato_Codigo2), 6, 0);
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATO_NUMERO") == 0 ) && ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV37Contrato_Numero2)) && String.IsNullOrEmpty(StringUtil.RTrim( AV38Contrato_Numero_To2)) ) )
            {
               AV11GridStateDynamicFilter.gxTpr_Value = AV37Contrato_Numero2;
               AV11GridStateDynamicFilter.gxTpr_Valueto = AV38Contrato_Numero_To2;
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SERVICO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV45Servico_Nome2)) )
            {
               AV11GridStateDynamicFilter.gxTpr_Value = AV45Servico_Nome2;
            }
            if ( AV28DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV11GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV11GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV9GridState.gxTpr_Dynamicfilters.Add(AV11GridStateDynamicFilter, 0);
            }
         }
         if ( AV23DynamicFiltersEnabled3 )
         {
            AV11GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV11GridStateDynamicFilter.gxTpr_Selected = AV24DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTRATOSERVICOS_CODIGO") == 0 ) && ! (0==AV26ContratoServicos_Codigo3) )
            {
               AV11GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV26ContratoServicos_Codigo3), 6, 0);
            }
            else if ( ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTRATO_CODIGO") == 0 ) && ! (0==AV40Contrato_Codigo3) )
            {
               AV11GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV40Contrato_Codigo3), 6, 0);
            }
            else if ( ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTRATO_NUMERO") == 0 ) && ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV41Contrato_Numero3)) && String.IsNullOrEmpty(StringUtil.RTrim( AV42Contrato_Numero_To3)) ) )
            {
               AV11GridStateDynamicFilter.gxTpr_Value = AV41Contrato_Numero3;
               AV11GridStateDynamicFilter.gxTpr_Valueto = AV42Contrato_Numero_To3;
            }
            else if ( ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "SERVICO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV46Servico_Nome3)) )
            {
               AV11GridStateDynamicFilter.gxTpr_Value = AV46Servico_Nome3;
            }
            if ( AV28DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV11GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV11GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV9GridState.gxTpr_Dynamicfilters.Add(AV11GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_742( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_742( true) ;
         }
         else
         {
            wb_table2_5_742( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_742e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_89_742( true) ;
         }
         else
         {
            wb_table3_89_742( false) ;
         }
         return  ;
      }

      protected void wb_table3_89_742e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_742e( true) ;
         }
         else
         {
            wb_table1_2_742e( false) ;
         }
      }

      protected void wb_table3_89_742( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_92_742( true) ;
         }
         else
         {
            wb_table4_92_742( false) ;
         }
         return  ;
      }

      protected void wb_table4_92_742e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_89_742e( true) ;
         }
         else
         {
            wb_table3_89_742e( false) ;
         }
      }

      protected void wb_table4_92_742( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"95\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicos_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicos_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicos_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContrato_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtContrato_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContrato_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(127), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContrato_Numero_Titleformat == 0 )
               {
                  context.SendWebValue( edtContrato_Numero_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContrato_Numero_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServico_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtServico_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServico_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(380), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServico_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtServico_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServico_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV30Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A160ContratoServicos_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicos_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicos_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContrato_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A77Contrato_Numero));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContrato_Numero_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_Numero_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServico_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServico_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A608Servico_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServico_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServico_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 95 )
         {
            wbEnd = 0;
            nRC_GXsfl_95 = (short)(nGXsfl_95_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_92_742e( true) ;
         }
         else
         {
            wb_table4_92_742e( false) ;
         }
      }

      protected void wb_table2_5_742( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_95_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptContratoServicos.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_95_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV13OrderedDsc), StringUtil.BoolToStr( AV13OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_742( true) ;
         }
         else
         {
            wb_table5_14_742( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_742e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_742e( true) ;
         }
         else
         {
            wb_table2_5_742e( false) ;
         }
      }

      protected void wb_table5_14_742( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_19_742( true) ;
         }
         else
         {
            wb_table6_19_742( false) ;
         }
         return  ;
      }

      protected void wb_table6_19_742e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_742e( true) ;
         }
         else
         {
            wb_table5_14_742e( false) ;
         }
      }

      protected void wb_table6_19_742( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_95_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV14DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_PromptContratoServicos.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV14DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'" + sGXsfl_95_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoservicos_codigo1_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16ContratoServicos_Codigo1), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV16ContratoServicos_Codigo1), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,28);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoservicos_codigo1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratoservicos_codigo1_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'" + sGXsfl_95_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_codigo1_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32Contrato_Codigo1), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV32Contrato_Codigo1), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,29);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_codigo1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContrato_codigo1_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicos.htm");
            wb_table7_30_742( true) ;
         }
         else
         {
            wb_table7_30_742( false) ;
         }
         return  ;
      }

      protected void wb_table7_30_742e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'" + sGXsfl_95_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServico_nome1_Internalname, StringUtil.RTrim( AV44Servico_Nome1), StringUtil.RTrim( context.localUtil.Format( AV44Servico_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,38);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServico_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavServico_nome1_Visible, 1, 0, "text", "", 380, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoServicos.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_95_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", "", true, "HLP_PromptContratoServicos.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_95_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoservicos_codigo2_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21ContratoServicos_Codigo2), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV21ContratoServicos_Codigo2), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoservicos_codigo2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratoservicos_codigo2_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_95_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_codigo2_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36Contrato_Codigo2), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV36Contrato_Codigo2), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,51);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_codigo2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContrato_codigo2_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicos.htm");
            wb_table8_52_742( true) ;
         }
         else
         {
            wb_table8_52_742( false) ;
         }
         return  ;
      }

      protected void wb_table8_52_742e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'" + sGXsfl_95_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServico_nome2_Internalname, StringUtil.RTrim( AV45Servico_Nome2), StringUtil.RTrim( context.localUtil.Format( AV45Servico_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,60);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServico_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavServico_nome2_Visible, 1, 0, "text", "", 380, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoServicos.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'" + sGXsfl_95_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV24DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,68);\"", "", true, "HLP_PromptContratoServicos.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'" + sGXsfl_95_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoservicos_codigo3_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV26ContratoServicos_Codigo3), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV26ContratoServicos_Codigo3), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,72);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoservicos_codigo3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratoservicos_codigo3_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'" + sGXsfl_95_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_codigo3_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40Contrato_Codigo3), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV40Contrato_Codigo3), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,73);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_codigo3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContrato_codigo3_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicos.htm");
            wb_table9_74_742( true) ;
         }
         else
         {
            wb_table9_74_742( false) ;
         }
         return  ;
      }

      protected void wb_table9_74_742e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'" + sGXsfl_95_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServico_nome3_Internalname, StringUtil.RTrim( AV46Servico_Nome3), StringUtil.RTrim( context.localUtil.Format( AV46Servico_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,82);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServico_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavServico_nome3_Visible, 1, 0, "text", "", 380, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_19_742e( true) ;
         }
         else
         {
            wb_table6_19_742e( false) ;
         }
      }

      protected void wb_table9_74_742( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontrato_numero3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontrato_numero3_Internalname, tblTablemergeddynamicfilterscontrato_numero3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'" + sGXsfl_95_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_numero3_Internalname, StringUtil.RTrim( AV41Contrato_Numero3), StringUtil.RTrim( context.localUtil.Format( AV41Contrato_Numero3, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,77);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_numero3_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontrato_numero_rangemiddletext3_Internalname, "to", "", "", lblDynamicfilterscontrato_numero_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'" + sGXsfl_95_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_numero_to3_Internalname, StringUtil.RTrim( AV42Contrato_Numero_To3), StringUtil.RTrim( context.localUtil.Format( AV42Contrato_Numero_To3, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,81);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_numero_to3_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_74_742e( true) ;
         }
         else
         {
            wb_table9_74_742e( false) ;
         }
      }

      protected void wb_table8_52_742( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontrato_numero2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontrato_numero2_Internalname, tblTablemergeddynamicfilterscontrato_numero2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'" + sGXsfl_95_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_numero2_Internalname, StringUtil.RTrim( AV37Contrato_Numero2), StringUtil.RTrim( context.localUtil.Format( AV37Contrato_Numero2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,55);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_numero2_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontrato_numero_rangemiddletext2_Internalname, "to", "", "", lblDynamicfilterscontrato_numero_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_95_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_numero_to2_Internalname, StringUtil.RTrim( AV38Contrato_Numero_To2), StringUtil.RTrim( context.localUtil.Format( AV38Contrato_Numero_To2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_numero_to2_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_52_742e( true) ;
         }
         else
         {
            wb_table8_52_742e( false) ;
         }
      }

      protected void wb_table7_30_742( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontrato_numero1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontrato_numero1_Internalname, tblTablemergeddynamicfilterscontrato_numero1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_95_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_numero1_Internalname, StringUtil.RTrim( AV33Contrato_Numero1), StringUtil.RTrim( context.localUtil.Format( AV33Contrato_Numero1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_numero1_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontrato_numero_rangemiddletext1_Internalname, "to", "", "", lblDynamicfilterscontrato_numero_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_95_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_numero_to1_Internalname, StringUtil.RTrim( AV34Contrato_Numero_To1), StringUtil.RTrim( context.localUtil.Format( AV34Contrato_Numero_To1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_numero_to1_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_30_742e( true) ;
         }
         else
         {
            wb_table7_30_742e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutContratoServicos_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratoServicos_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA742( ) ;
         WS742( ) ;
         WE742( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202031523171274");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptcontratoservicos.js", "?202031523171274");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_952( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_95_idx;
         edtContratoServicos_Codigo_Internalname = "CONTRATOSERVICOS_CODIGO_"+sGXsfl_95_idx;
         edtContrato_Codigo_Internalname = "CONTRATO_CODIGO_"+sGXsfl_95_idx;
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO_"+sGXsfl_95_idx;
         edtServico_Codigo_Internalname = "SERVICO_CODIGO_"+sGXsfl_95_idx;
         edtServico_Nome_Internalname = "SERVICO_NOME_"+sGXsfl_95_idx;
      }

      protected void SubsflControlProps_fel_952( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_95_fel_idx;
         edtContratoServicos_Codigo_Internalname = "CONTRATOSERVICOS_CODIGO_"+sGXsfl_95_fel_idx;
         edtContrato_Codigo_Internalname = "CONTRATO_CODIGO_"+sGXsfl_95_fel_idx;
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO_"+sGXsfl_95_fel_idx;
         edtServico_Codigo_Internalname = "SERVICO_CODIGO_"+sGXsfl_95_fel_idx;
         edtServico_Nome_Internalname = "SERVICO_NOME_"+sGXsfl_95_fel_idx;
      }

      protected void sendrow_952( )
      {
         SubsflControlProps_952( ) ;
         WB740( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_95_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_95_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_95_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 96,'',false,'',95)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV30Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV30Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV73Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV30Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV30Select)) ? AV73Select_GXI : context.PathToRelativeUrl( AV30Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_95_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV30Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicos_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A160ContratoServicos_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A160ContratoServicos_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicos_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)95,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)95,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_Numero_Internalname,StringUtil.RTrim( A77Contrato_Numero),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_Numero_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)127,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)95,(short)1,(short)-1,(short)-1,(bool)true,(String)"NumeroContrato",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServico_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A155Servico_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServico_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)95,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServico_Nome_Internalname,StringUtil.RTrim( A608Servico_Nome),StringUtil.RTrim( context.localUtil.Format( A608Servico_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServico_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)380,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)95,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOS_CODIGO"+"_"+sGXsfl_95_idx, GetSecureSignedToken( sGXsfl_95_idx, context.localUtil.Format( (decimal)(A160ContratoServicos_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_CODIGO"+"_"+sGXsfl_95_idx, GetSecureSignedToken( sGXsfl_95_idx, context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_SERVICO_CODIGO"+"_"+sGXsfl_95_idx, GetSecureSignedToken( sGXsfl_95_idx, context.localUtil.Format( (decimal)(A155Servico_Codigo), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_95_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_95_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_95_idx+1));
            sGXsfl_95_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_95_idx), 4, 0)), 4, "0");
            SubsflControlProps_952( ) ;
         }
         /* End function sendrow_952 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavContratoservicos_codigo1_Internalname = "vCONTRATOSERVICOS_CODIGO1";
         edtavContrato_codigo1_Internalname = "vCONTRATO_CODIGO1";
         edtavContrato_numero1_Internalname = "vCONTRATO_NUMERO1";
         lblDynamicfilterscontrato_numero_rangemiddletext1_Internalname = "DYNAMICFILTERSCONTRATO_NUMERO_RANGEMIDDLETEXT1";
         edtavContrato_numero_to1_Internalname = "vCONTRATO_NUMERO_TO1";
         tblTablemergeddynamicfilterscontrato_numero1_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATO_NUMERO1";
         edtavServico_nome1_Internalname = "vSERVICO_NOME1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavContratoservicos_codigo2_Internalname = "vCONTRATOSERVICOS_CODIGO2";
         edtavContrato_codigo2_Internalname = "vCONTRATO_CODIGO2";
         edtavContrato_numero2_Internalname = "vCONTRATO_NUMERO2";
         lblDynamicfilterscontrato_numero_rangemiddletext2_Internalname = "DYNAMICFILTERSCONTRATO_NUMERO_RANGEMIDDLETEXT2";
         edtavContrato_numero_to2_Internalname = "vCONTRATO_NUMERO_TO2";
         tblTablemergeddynamicfilterscontrato_numero2_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATO_NUMERO2";
         edtavServico_nome2_Internalname = "vSERVICO_NOME2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavContratoservicos_codigo3_Internalname = "vCONTRATOSERVICOS_CODIGO3";
         edtavContrato_codigo3_Internalname = "vCONTRATO_CODIGO3";
         edtavContrato_numero3_Internalname = "vCONTRATO_NUMERO3";
         lblDynamicfilterscontrato_numero_rangemiddletext3_Internalname = "DYNAMICFILTERSCONTRATO_NUMERO_RANGEMIDDLETEXT3";
         edtavContrato_numero_to3_Internalname = "vCONTRATO_NUMERO_TO3";
         tblTablemergeddynamicfilterscontrato_numero3_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATO_NUMERO3";
         edtavServico_nome3_Internalname = "vSERVICO_NOME3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtContratoServicos_Codigo_Internalname = "CONTRATOSERVICOS_CODIGO";
         edtContrato_Codigo_Internalname = "CONTRATO_CODIGO";
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO";
         edtServico_Codigo_Internalname = "SERVICO_CODIGO";
         edtServico_Nome_Internalname = "SERVICO_NOME";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfcontratoservicos_codigo_Internalname = "vTFCONTRATOSERVICOS_CODIGO";
         edtavTfcontratoservicos_codigo_to_Internalname = "vTFCONTRATOSERVICOS_CODIGO_TO";
         edtavTfcontrato_codigo_Internalname = "vTFCONTRATO_CODIGO";
         edtavTfcontrato_codigo_to_Internalname = "vTFCONTRATO_CODIGO_TO";
         edtavTfcontrato_numero_Internalname = "vTFCONTRATO_NUMERO";
         edtavTfcontrato_numero_sel_Internalname = "vTFCONTRATO_NUMERO_SEL";
         edtavTfservico_codigo_Internalname = "vTFSERVICO_CODIGO";
         edtavTfservico_codigo_to_Internalname = "vTFSERVICO_CODIGO_TO";
         edtavTfservico_nome_Internalname = "vTFSERVICO_NOME";
         edtavTfservico_nome_sel_Internalname = "vTFSERVICO_NOME_SEL";
         Ddo_contratoservicos_codigo_Internalname = "DDO_CONTRATOSERVICOS_CODIGO";
         edtavDdo_contratoservicos_codigotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSERVICOS_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_contrato_codigo_Internalname = "DDO_CONTRATO_CODIGO";
         edtavDdo_contrato_codigotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_contrato_numero_Internalname = "DDO_CONTRATO_NUMERO";
         edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE";
         Ddo_servico_codigo_Internalname = "DDO_SERVICO_CODIGO";
         edtavDdo_servico_codigotitlecontrolidtoreplace_Internalname = "vDDO_SERVICO_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_servico_nome_Internalname = "DDO_SERVICO_NOME";
         edtavDdo_servico_nometitlecontrolidtoreplace_Internalname = "vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtServico_Nome_Jsonclick = "";
         edtServico_Codigo_Jsonclick = "";
         edtContrato_Numero_Jsonclick = "";
         edtContrato_Codigo_Jsonclick = "";
         edtContratoServicos_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavContrato_numero_to1_Jsonclick = "";
         edtavContrato_numero1_Jsonclick = "";
         edtavContrato_numero_to2_Jsonclick = "";
         edtavContrato_numero2_Jsonclick = "";
         edtavContrato_numero_to3_Jsonclick = "";
         edtavContrato_numero3_Jsonclick = "";
         edtavServico_nome3_Jsonclick = "";
         edtavContrato_codigo3_Jsonclick = "";
         edtavContratoservicos_codigo3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         edtavServico_nome2_Jsonclick = "";
         edtavContrato_codigo2_Jsonclick = "";
         edtavContratoservicos_codigo2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         edtavServico_nome1_Jsonclick = "";
         edtavContrato_codigo1_Jsonclick = "";
         edtavContratoservicos_codigo1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         edtServico_Nome_Titleformat = 0;
         edtServico_Codigo_Titleformat = 0;
         edtContrato_Numero_Titleformat = 0;
         edtContrato_Codigo_Titleformat = 0;
         edtContratoServicos_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavServico_nome3_Visible = 1;
         tblTablemergeddynamicfilterscontrato_numero3_Visible = 1;
         edtavContrato_codigo3_Visible = 1;
         edtavContratoservicos_codigo3_Visible = 1;
         edtavServico_nome2_Visible = 1;
         tblTablemergeddynamicfilterscontrato_numero2_Visible = 1;
         edtavContrato_codigo2_Visible = 1;
         edtavContratoservicos_codigo2_Visible = 1;
         edtavServico_nome1_Visible = 1;
         tblTablemergeddynamicfilterscontrato_numero1_Visible = 1;
         edtavContrato_codigo1_Visible = 1;
         edtavContratoservicos_codigo1_Visible = 1;
         edtServico_Nome_Title = "Nome";
         edtServico_Codigo_Title = "Servi�o";
         edtContrato_Numero_Title = "N�mero do Contrato";
         edtContrato_Codigo_Title = "Contrato";
         edtContratoServicos_Codigo_Title = "do Servi�o";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_servico_nometitlecontrolidtoreplace_Visible = 1;
         edtavDdo_servico_codigotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contrato_codigotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicos_codigotitlecontrolidtoreplace_Visible = 1;
         edtavTfservico_nome_sel_Jsonclick = "";
         edtavTfservico_nome_sel_Visible = 1;
         edtavTfservico_nome_Jsonclick = "";
         edtavTfservico_nome_Visible = 1;
         edtavTfservico_codigo_to_Jsonclick = "";
         edtavTfservico_codigo_to_Visible = 1;
         edtavTfservico_codigo_Jsonclick = "";
         edtavTfservico_codigo_Visible = 1;
         edtavTfcontrato_numero_sel_Jsonclick = "";
         edtavTfcontrato_numero_sel_Visible = 1;
         edtavTfcontrato_numero_Jsonclick = "";
         edtavTfcontrato_numero_Visible = 1;
         edtavTfcontrato_codigo_to_Jsonclick = "";
         edtavTfcontrato_codigo_to_Visible = 1;
         edtavTfcontrato_codigo_Jsonclick = "";
         edtavTfcontrato_codigo_Visible = 1;
         edtavTfcontratoservicos_codigo_to_Jsonclick = "";
         edtavTfcontratoservicos_codigo_to_Visible = 1;
         edtavTfcontratoservicos_codigo_Jsonclick = "";
         edtavTfcontratoservicos_codigo_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_servico_nome_Searchbuttontext = "Pesquisar";
         Ddo_servico_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_servico_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_servico_nome_Loadingdata = "Carregando dados...";
         Ddo_servico_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_servico_nome_Sortasc = "Ordenar de A � Z";
         Ddo_servico_nome_Datalistupdateminimumcharacters = 0;
         Ddo_servico_nome_Datalistproc = "GetPromptContratoServicosFilterData";
         Ddo_servico_nome_Datalisttype = "Dynamic";
         Ddo_servico_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_servico_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_servico_nome_Filtertype = "Character";
         Ddo_servico_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_servico_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_servico_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_servico_nome_Titlecontrolidtoreplace = "";
         Ddo_servico_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_servico_nome_Cls = "ColumnSettings";
         Ddo_servico_nome_Tooltip = "Op��es";
         Ddo_servico_nome_Caption = "";
         Ddo_servico_codigo_Searchbuttontext = "Pesquisar";
         Ddo_servico_codigo_Rangefilterto = "At�";
         Ddo_servico_codigo_Rangefilterfrom = "Desde";
         Ddo_servico_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_servico_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_servico_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_servico_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_servico_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_servico_codigo_Filtertype = "Numeric";
         Ddo_servico_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_servico_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_servico_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_servico_codigo_Titlecontrolidtoreplace = "";
         Ddo_servico_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_servico_codigo_Cls = "ColumnSettings";
         Ddo_servico_codigo_Tooltip = "Op��es";
         Ddo_servico_codigo_Caption = "";
         Ddo_contrato_numero_Searchbuttontext = "Pesquisar";
         Ddo_contrato_numero_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contrato_numero_Cleanfilter = "Limpar pesquisa";
         Ddo_contrato_numero_Loadingdata = "Carregando dados...";
         Ddo_contrato_numero_Sortdsc = "Ordenar de Z � A";
         Ddo_contrato_numero_Sortasc = "Ordenar de A � Z";
         Ddo_contrato_numero_Datalistupdateminimumcharacters = 0;
         Ddo_contrato_numero_Datalistproc = "GetPromptContratoServicosFilterData";
         Ddo_contrato_numero_Datalisttype = "Dynamic";
         Ddo_contrato_numero_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contrato_numero_Filtertype = "Character";
         Ddo_contrato_numero_Includefilter = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Titlecontrolidtoreplace = "";
         Ddo_contrato_numero_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contrato_numero_Cls = "ColumnSettings";
         Ddo_contrato_numero_Tooltip = "Op��es";
         Ddo_contrato_numero_Caption = "";
         Ddo_contrato_codigo_Searchbuttontext = "Pesquisar";
         Ddo_contrato_codigo_Rangefilterto = "At�";
         Ddo_contrato_codigo_Rangefilterfrom = "Desde";
         Ddo_contrato_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_contrato_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_contrato_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_contrato_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contrato_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contrato_codigo_Filtertype = "Numeric";
         Ddo_contrato_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_contrato_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contrato_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contrato_codigo_Titlecontrolidtoreplace = "";
         Ddo_contrato_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contrato_codigo_Cls = "ColumnSettings";
         Ddo_contrato_codigo_Tooltip = "Op��es";
         Ddo_contrato_codigo_Caption = "";
         Ddo_contratoservicos_codigo_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicos_codigo_Rangefilterto = "At�";
         Ddo_contratoservicos_codigo_Rangefilterfrom = "Desde";
         Ddo_contratoservicos_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicos_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicos_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicos_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoservicos_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoservicos_codigo_Filtertype = "Numeric";
         Ddo_contratoservicos_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicos_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicos_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicos_codigo_Titlecontrolidtoreplace = "";
         Ddo_contratoservicos_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicos_codigo_Cls = "ColumnSettings";
         Ddo_contratoservicos_codigo_Tooltip = "Op��es";
         Ddo_contratoservicos_codigo_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Contrato Servicos";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV50ddo_ContratoServicos_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Servico_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV48TFContratoServicos_Codigo',fld:'vTFCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV49TFContratoServicos_Codigo_To',fld:'vTFCONTRATOSERVICOS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV52TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV53TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV56TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV57TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV60TFServico_Codigo',fld:'vTFSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV61TFServico_Codigo_To',fld:'vTFSERVICO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV64TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV65TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV74Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV9GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoServicos_Codigo1',fld:'vCONTRATOSERVICOS_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV32Contrato_Codigo1',fld:'vCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV33Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV34Contrato_Numero_To1',fld:'vCONTRATO_NUMERO_TO1',pic:'',nv:''},{av:'AV44Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ContratoServicos_Codigo2',fld:'vCONTRATOSERVICOS_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV36Contrato_Codigo2',fld:'vCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV37Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV38Contrato_Numero_To2',fld:'vCONTRATO_NUMERO_TO2',pic:'',nv:''},{av:'AV45Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26ContratoServicos_Codigo3',fld:'vCONTRATOSERVICOS_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV40Contrato_Codigo3',fld:'vCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV41Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV42Contrato_Numero_To3',fld:'vCONTRATO_NUMERO_TO3',pic:'',nv:''},{av:'AV46Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''}],oparms:[{av:'AV47ContratoServicos_CodigoTitleFilterData',fld:'vCONTRATOSERVICOS_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV51Contrato_CodigoTitleFilterData',fld:'vCONTRATO_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV55Contrato_NumeroTitleFilterData',fld:'vCONTRATO_NUMEROTITLEFILTERDATA',pic:'',nv:null},{av:'AV59Servico_CodigoTitleFilterData',fld:'vSERVICO_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV63Servico_NomeTitleFilterData',fld:'vSERVICO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'edtContratoServicos_Codigo_Titleformat',ctrl:'CONTRATOSERVICOS_CODIGO',prop:'Titleformat'},{av:'edtContratoServicos_Codigo_Title',ctrl:'CONTRATOSERVICOS_CODIGO',prop:'Title'},{av:'edtContrato_Codigo_Titleformat',ctrl:'CONTRATO_CODIGO',prop:'Titleformat'},{av:'edtContrato_Codigo_Title',ctrl:'CONTRATO_CODIGO',prop:'Title'},{av:'edtContrato_Numero_Titleformat',ctrl:'CONTRATO_NUMERO',prop:'Titleformat'},{av:'edtContrato_Numero_Title',ctrl:'CONTRATO_NUMERO',prop:'Title'},{av:'edtServico_Codigo_Titleformat',ctrl:'SERVICO_CODIGO',prop:'Titleformat'},{av:'edtServico_Codigo_Title',ctrl:'SERVICO_CODIGO',prop:'Title'},{av:'edtServico_Nome_Titleformat',ctrl:'SERVICO_NOME',prop:'Titleformat'},{av:'edtServico_Nome_Title',ctrl:'SERVICO_NOME',prop:'Title'},{av:'AV69GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV70GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV9GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11742',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoServicos_Codigo1',fld:'vCONTRATOSERVICOS_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV32Contrato_Codigo1',fld:'vCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV33Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV34Contrato_Numero_To1',fld:'vCONTRATO_NUMERO_TO1',pic:'',nv:''},{av:'AV44Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ContratoServicos_Codigo2',fld:'vCONTRATOSERVICOS_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV36Contrato_Codigo2',fld:'vCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV37Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV38Contrato_Numero_To2',fld:'vCONTRATO_NUMERO_TO2',pic:'',nv:''},{av:'AV45Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26ContratoServicos_Codigo3',fld:'vCONTRATOSERVICOS_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV40Contrato_Codigo3',fld:'vCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV41Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV42Contrato_Numero_To3',fld:'vCONTRATO_NUMERO_TO3',pic:'',nv:''},{av:'AV46Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV48TFContratoServicos_Codigo',fld:'vTFCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV49TFContratoServicos_Codigo_To',fld:'vTFCONTRATOSERVICOS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV52TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV53TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV56TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV57TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV60TFServico_Codigo',fld:'vTFSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV61TFServico_Codigo_To',fld:'vTFSERVICO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV64TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV65TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV50ddo_ContratoServicos_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Servico_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV9GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTRATOSERVICOS_CODIGO.ONOPTIONCLICKED","{handler:'E12742',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoServicos_Codigo1',fld:'vCONTRATOSERVICOS_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV32Contrato_Codigo1',fld:'vCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV33Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV34Contrato_Numero_To1',fld:'vCONTRATO_NUMERO_TO1',pic:'',nv:''},{av:'AV44Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ContratoServicos_Codigo2',fld:'vCONTRATOSERVICOS_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV36Contrato_Codigo2',fld:'vCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV37Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV38Contrato_Numero_To2',fld:'vCONTRATO_NUMERO_TO2',pic:'',nv:''},{av:'AV45Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26ContratoServicos_Codigo3',fld:'vCONTRATOSERVICOS_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV40Contrato_Codigo3',fld:'vCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV41Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV42Contrato_Numero_To3',fld:'vCONTRATO_NUMERO_TO3',pic:'',nv:''},{av:'AV46Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV48TFContratoServicos_Codigo',fld:'vTFCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV49TFContratoServicos_Codigo_To',fld:'vTFCONTRATOSERVICOS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV52TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV53TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV56TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV57TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV60TFServico_Codigo',fld:'vTFSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV61TFServico_Codigo_To',fld:'vTFSERVICO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV64TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV65TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV50ddo_ContratoServicos_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Servico_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV9GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratoservicos_codigo_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOS_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_contratoservicos_codigo_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOS_CODIGO',prop:'FilteredText_get'},{av:'Ddo_contratoservicos_codigo_Filteredtextto_get',ctrl:'DDO_CONTRATOSERVICOS_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicos_codigo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOS_CODIGO',prop:'SortedStatus'},{av:'AV48TFContratoServicos_Codigo',fld:'vTFCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV49TFContratoServicos_Codigo_To',fld:'vTFCONTRATOSERVICOS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_servico_codigo_Sortedstatus',ctrl:'DDO_SERVICO_CODIGO',prop:'SortedStatus'},{av:'Ddo_servico_nome_Sortedstatus',ctrl:'DDO_SERVICO_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATO_CODIGO.ONOPTIONCLICKED","{handler:'E13742',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoServicos_Codigo1',fld:'vCONTRATOSERVICOS_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV32Contrato_Codigo1',fld:'vCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV33Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV34Contrato_Numero_To1',fld:'vCONTRATO_NUMERO_TO1',pic:'',nv:''},{av:'AV44Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ContratoServicos_Codigo2',fld:'vCONTRATOSERVICOS_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV36Contrato_Codigo2',fld:'vCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV37Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV38Contrato_Numero_To2',fld:'vCONTRATO_NUMERO_TO2',pic:'',nv:''},{av:'AV45Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26ContratoServicos_Codigo3',fld:'vCONTRATOSERVICOS_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV40Contrato_Codigo3',fld:'vCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV41Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV42Contrato_Numero_To3',fld:'vCONTRATO_NUMERO_TO3',pic:'',nv:''},{av:'AV46Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV48TFContratoServicos_Codigo',fld:'vTFCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV49TFContratoServicos_Codigo_To',fld:'vTFCONTRATOSERVICOS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV52TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV53TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV56TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV57TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV60TFServico_Codigo',fld:'vTFSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV61TFServico_Codigo_To',fld:'vTFSERVICO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV64TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV65TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV50ddo_ContratoServicos_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Servico_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV9GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contrato_codigo_Activeeventkey',ctrl:'DDO_CONTRATO_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_contrato_codigo_Filteredtext_get',ctrl:'DDO_CONTRATO_CODIGO',prop:'FilteredText_get'},{av:'Ddo_contrato_codigo_Filteredtextto_get',ctrl:'DDO_CONTRATO_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'AV52TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV53TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoservicos_codigo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOS_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_servico_codigo_Sortedstatus',ctrl:'DDO_SERVICO_CODIGO',prop:'SortedStatus'},{av:'Ddo_servico_nome_Sortedstatus',ctrl:'DDO_SERVICO_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATO_NUMERO.ONOPTIONCLICKED","{handler:'E14742',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoServicos_Codigo1',fld:'vCONTRATOSERVICOS_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV32Contrato_Codigo1',fld:'vCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV33Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV34Contrato_Numero_To1',fld:'vCONTRATO_NUMERO_TO1',pic:'',nv:''},{av:'AV44Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ContratoServicos_Codigo2',fld:'vCONTRATOSERVICOS_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV36Contrato_Codigo2',fld:'vCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV37Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV38Contrato_Numero_To2',fld:'vCONTRATO_NUMERO_TO2',pic:'',nv:''},{av:'AV45Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26ContratoServicos_Codigo3',fld:'vCONTRATOSERVICOS_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV40Contrato_Codigo3',fld:'vCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV41Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV42Contrato_Numero_To3',fld:'vCONTRATO_NUMERO_TO3',pic:'',nv:''},{av:'AV46Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV48TFContratoServicos_Codigo',fld:'vTFCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV49TFContratoServicos_Codigo_To',fld:'vTFCONTRATOSERVICOS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV52TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV53TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV56TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV57TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV60TFServico_Codigo',fld:'vTFSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV61TFServico_Codigo_To',fld:'vTFSERVICO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV64TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV65TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV50ddo_ContratoServicos_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Servico_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV9GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contrato_numero_Activeeventkey',ctrl:'DDO_CONTRATO_NUMERO',prop:'ActiveEventKey'},{av:'Ddo_contrato_numero_Filteredtext_get',ctrl:'DDO_CONTRATO_NUMERO',prop:'FilteredText_get'},{av:'Ddo_contrato_numero_Selectedvalue_get',ctrl:'DDO_CONTRATO_NUMERO',prop:'SelectedValue_get'}],oparms:[{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'AV56TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV57TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'Ddo_contratoservicos_codigo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOS_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_servico_codigo_Sortedstatus',ctrl:'DDO_SERVICO_CODIGO',prop:'SortedStatus'},{av:'Ddo_servico_nome_Sortedstatus',ctrl:'DDO_SERVICO_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SERVICO_CODIGO.ONOPTIONCLICKED","{handler:'E15742',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoServicos_Codigo1',fld:'vCONTRATOSERVICOS_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV32Contrato_Codigo1',fld:'vCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV33Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV34Contrato_Numero_To1',fld:'vCONTRATO_NUMERO_TO1',pic:'',nv:''},{av:'AV44Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ContratoServicos_Codigo2',fld:'vCONTRATOSERVICOS_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV36Contrato_Codigo2',fld:'vCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV37Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV38Contrato_Numero_To2',fld:'vCONTRATO_NUMERO_TO2',pic:'',nv:''},{av:'AV45Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26ContratoServicos_Codigo3',fld:'vCONTRATOSERVICOS_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV40Contrato_Codigo3',fld:'vCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV41Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV42Contrato_Numero_To3',fld:'vCONTRATO_NUMERO_TO3',pic:'',nv:''},{av:'AV46Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV48TFContratoServicos_Codigo',fld:'vTFCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV49TFContratoServicos_Codigo_To',fld:'vTFCONTRATOSERVICOS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV52TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV53TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV56TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV57TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV60TFServico_Codigo',fld:'vTFSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV61TFServico_Codigo_To',fld:'vTFSERVICO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV64TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV65TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV50ddo_ContratoServicos_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Servico_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV9GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_servico_codigo_Activeeventkey',ctrl:'DDO_SERVICO_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_servico_codigo_Filteredtext_get',ctrl:'DDO_SERVICO_CODIGO',prop:'FilteredText_get'},{av:'Ddo_servico_codigo_Filteredtextto_get',ctrl:'DDO_SERVICO_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_servico_codigo_Sortedstatus',ctrl:'DDO_SERVICO_CODIGO',prop:'SortedStatus'},{av:'AV60TFServico_Codigo',fld:'vTFSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV61TFServico_Codigo_To',fld:'vTFSERVICO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoservicos_codigo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOS_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_servico_nome_Sortedstatus',ctrl:'DDO_SERVICO_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SERVICO_NOME.ONOPTIONCLICKED","{handler:'E16742',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoServicos_Codigo1',fld:'vCONTRATOSERVICOS_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV32Contrato_Codigo1',fld:'vCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV33Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV34Contrato_Numero_To1',fld:'vCONTRATO_NUMERO_TO1',pic:'',nv:''},{av:'AV44Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ContratoServicos_Codigo2',fld:'vCONTRATOSERVICOS_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV36Contrato_Codigo2',fld:'vCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV37Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV38Contrato_Numero_To2',fld:'vCONTRATO_NUMERO_TO2',pic:'',nv:''},{av:'AV45Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26ContratoServicos_Codigo3',fld:'vCONTRATOSERVICOS_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV40Contrato_Codigo3',fld:'vCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV41Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV42Contrato_Numero_To3',fld:'vCONTRATO_NUMERO_TO3',pic:'',nv:''},{av:'AV46Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV48TFContratoServicos_Codigo',fld:'vTFCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV49TFContratoServicos_Codigo_To',fld:'vTFCONTRATOSERVICOS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV52TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV53TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV56TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV57TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV60TFServico_Codigo',fld:'vTFSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV61TFServico_Codigo_To',fld:'vTFSERVICO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV64TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV65TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV50ddo_ContratoServicos_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Servico_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV9GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_servico_nome_Activeeventkey',ctrl:'DDO_SERVICO_NOME',prop:'ActiveEventKey'},{av:'Ddo_servico_nome_Filteredtext_get',ctrl:'DDO_SERVICO_NOME',prop:'FilteredText_get'},{av:'Ddo_servico_nome_Selectedvalue_get',ctrl:'DDO_SERVICO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_servico_nome_Sortedstatus',ctrl:'DDO_SERVICO_NOME',prop:'SortedStatus'},{av:'AV64TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV65TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_contratoservicos_codigo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOS_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_servico_codigo_Sortedstatus',ctrl:'DDO_SERVICO_CODIGO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E29742',iparms:[],oparms:[{av:'AV30Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E30742',iparms:[{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV7InOutContratoServicos_Codigo',fld:'vINOUTCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E17742',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoServicos_Codigo1',fld:'vCONTRATOSERVICOS_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV32Contrato_Codigo1',fld:'vCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV33Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV34Contrato_Numero_To1',fld:'vCONTRATO_NUMERO_TO1',pic:'',nv:''},{av:'AV44Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ContratoServicos_Codigo2',fld:'vCONTRATOSERVICOS_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV36Contrato_Codigo2',fld:'vCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV37Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV38Contrato_Numero_To2',fld:'vCONTRATO_NUMERO_TO2',pic:'',nv:''},{av:'AV45Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26ContratoServicos_Codigo3',fld:'vCONTRATOSERVICOS_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV40Contrato_Codigo3',fld:'vCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV41Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV42Contrato_Numero_To3',fld:'vCONTRATO_NUMERO_TO3',pic:'',nv:''},{av:'AV46Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV48TFContratoServicos_Codigo',fld:'vTFCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV49TFContratoServicos_Codigo_To',fld:'vTFCONTRATOSERVICOS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV52TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV53TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV56TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV57TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV60TFServico_Codigo',fld:'vTFSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV61TFServico_Codigo_To',fld:'vTFSERVICO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV64TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV65TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV50ddo_ContratoServicos_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Servico_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV9GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E22742',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E18742',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoServicos_Codigo1',fld:'vCONTRATOSERVICOS_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV32Contrato_Codigo1',fld:'vCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV33Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV34Contrato_Numero_To1',fld:'vCONTRATO_NUMERO_TO1',pic:'',nv:''},{av:'AV44Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ContratoServicos_Codigo2',fld:'vCONTRATOSERVICOS_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV36Contrato_Codigo2',fld:'vCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV37Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV38Contrato_Numero_To2',fld:'vCONTRATO_NUMERO_TO2',pic:'',nv:''},{av:'AV45Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26ContratoServicos_Codigo3',fld:'vCONTRATOSERVICOS_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV40Contrato_Codigo3',fld:'vCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV41Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV42Contrato_Numero_To3',fld:'vCONTRATO_NUMERO_TO3',pic:'',nv:''},{av:'AV46Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV48TFContratoServicos_Codigo',fld:'vTFCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV49TFContratoServicos_Codigo_To',fld:'vTFCONTRATOSERVICOS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV52TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV53TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV56TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV57TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV60TFServico_Codigo',fld:'vTFSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV61TFServico_Codigo_To',fld:'vTFSERVICO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV64TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV65TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV50ddo_ContratoServicos_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Servico_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV9GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV9GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ContratoServicos_Codigo2',fld:'vCONTRATOSERVICOS_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26ContratoServicos_Codigo3',fld:'vCONTRATOSERVICOS_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoServicos_Codigo1',fld:'vCONTRATOSERVICOS_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV32Contrato_Codigo1',fld:'vCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV33Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV34Contrato_Numero_To1',fld:'vCONTRATO_NUMERO_TO1',pic:'',nv:''},{av:'AV44Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV36Contrato_Codigo2',fld:'vCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV37Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV38Contrato_Numero_To2',fld:'vCONTRATO_NUMERO_TO2',pic:'',nv:''},{av:'AV45Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV40Contrato_Codigo3',fld:'vCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV41Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV42Contrato_Numero_To3',fld:'vCONTRATO_NUMERO_TO3',pic:'',nv:''},{av:'AV46Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'edtavContratoservicos_codigo2_Visible',ctrl:'vCONTRATOSERVICOS_CODIGO2',prop:'Visible'},{av:'edtavContrato_codigo2_Visible',ctrl:'vCONTRATO_CODIGO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_numero2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_NUMERO2',prop:'Visible'},{av:'edtavServico_nome2_Visible',ctrl:'vSERVICO_NOME2',prop:'Visible'},{av:'edtavContratoservicos_codigo3_Visible',ctrl:'vCONTRATOSERVICOS_CODIGO3',prop:'Visible'},{av:'edtavContrato_codigo3_Visible',ctrl:'vCONTRATO_CODIGO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_numero3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_NUMERO3',prop:'Visible'},{av:'edtavServico_nome3_Visible',ctrl:'vSERVICO_NOME3',prop:'Visible'},{av:'edtavContratoservicos_codigo1_Visible',ctrl:'vCONTRATOSERVICOS_CODIGO1',prop:'Visible'},{av:'edtavContrato_codigo1_Visible',ctrl:'vCONTRATO_CODIGO1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_numero1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_NUMERO1',prop:'Visible'},{av:'edtavServico_nome1_Visible',ctrl:'vSERVICO_NOME1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E23742',iparms:[{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavContratoservicos_codigo1_Visible',ctrl:'vCONTRATOSERVICOS_CODIGO1',prop:'Visible'},{av:'edtavContrato_codigo1_Visible',ctrl:'vCONTRATO_CODIGO1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_numero1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_NUMERO1',prop:'Visible'},{av:'edtavServico_nome1_Visible',ctrl:'vSERVICO_NOME1',prop:'Visible'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E24742',iparms:[],oparms:[{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E19742',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoServicos_Codigo1',fld:'vCONTRATOSERVICOS_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV32Contrato_Codigo1',fld:'vCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV33Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV34Contrato_Numero_To1',fld:'vCONTRATO_NUMERO_TO1',pic:'',nv:''},{av:'AV44Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ContratoServicos_Codigo2',fld:'vCONTRATOSERVICOS_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV36Contrato_Codigo2',fld:'vCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV37Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV38Contrato_Numero_To2',fld:'vCONTRATO_NUMERO_TO2',pic:'',nv:''},{av:'AV45Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26ContratoServicos_Codigo3',fld:'vCONTRATOSERVICOS_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV40Contrato_Codigo3',fld:'vCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV41Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV42Contrato_Numero_To3',fld:'vCONTRATO_NUMERO_TO3',pic:'',nv:''},{av:'AV46Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV48TFContratoServicos_Codigo',fld:'vTFCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV49TFContratoServicos_Codigo_To',fld:'vTFCONTRATOSERVICOS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV52TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV53TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV56TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV57TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV60TFServico_Codigo',fld:'vTFSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV61TFServico_Codigo_To',fld:'vTFSERVICO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV64TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV65TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV50ddo_ContratoServicos_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Servico_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV9GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV9GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ContratoServicos_Codigo2',fld:'vCONTRATOSERVICOS_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26ContratoServicos_Codigo3',fld:'vCONTRATOSERVICOS_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoServicos_Codigo1',fld:'vCONTRATOSERVICOS_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV32Contrato_Codigo1',fld:'vCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV33Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV34Contrato_Numero_To1',fld:'vCONTRATO_NUMERO_TO1',pic:'',nv:''},{av:'AV44Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV36Contrato_Codigo2',fld:'vCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV37Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV38Contrato_Numero_To2',fld:'vCONTRATO_NUMERO_TO2',pic:'',nv:''},{av:'AV45Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV40Contrato_Codigo3',fld:'vCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV41Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV42Contrato_Numero_To3',fld:'vCONTRATO_NUMERO_TO3',pic:'',nv:''},{av:'AV46Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'edtavContratoservicos_codigo2_Visible',ctrl:'vCONTRATOSERVICOS_CODIGO2',prop:'Visible'},{av:'edtavContrato_codigo2_Visible',ctrl:'vCONTRATO_CODIGO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_numero2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_NUMERO2',prop:'Visible'},{av:'edtavServico_nome2_Visible',ctrl:'vSERVICO_NOME2',prop:'Visible'},{av:'edtavContratoservicos_codigo3_Visible',ctrl:'vCONTRATOSERVICOS_CODIGO3',prop:'Visible'},{av:'edtavContrato_codigo3_Visible',ctrl:'vCONTRATO_CODIGO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_numero3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_NUMERO3',prop:'Visible'},{av:'edtavServico_nome3_Visible',ctrl:'vSERVICO_NOME3',prop:'Visible'},{av:'edtavContratoservicos_codigo1_Visible',ctrl:'vCONTRATOSERVICOS_CODIGO1',prop:'Visible'},{av:'edtavContrato_codigo1_Visible',ctrl:'vCONTRATO_CODIGO1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_numero1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_NUMERO1',prop:'Visible'},{av:'edtavServico_nome1_Visible',ctrl:'vSERVICO_NOME1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E25742',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavContratoservicos_codigo2_Visible',ctrl:'vCONTRATOSERVICOS_CODIGO2',prop:'Visible'},{av:'edtavContrato_codigo2_Visible',ctrl:'vCONTRATO_CODIGO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_numero2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_NUMERO2',prop:'Visible'},{av:'edtavServico_nome2_Visible',ctrl:'vSERVICO_NOME2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E20742',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoServicos_Codigo1',fld:'vCONTRATOSERVICOS_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV32Contrato_Codigo1',fld:'vCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV33Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV34Contrato_Numero_To1',fld:'vCONTRATO_NUMERO_TO1',pic:'',nv:''},{av:'AV44Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ContratoServicos_Codigo2',fld:'vCONTRATOSERVICOS_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV36Contrato_Codigo2',fld:'vCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV37Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV38Contrato_Numero_To2',fld:'vCONTRATO_NUMERO_TO2',pic:'',nv:''},{av:'AV45Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26ContratoServicos_Codigo3',fld:'vCONTRATOSERVICOS_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV40Contrato_Codigo3',fld:'vCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV41Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV42Contrato_Numero_To3',fld:'vCONTRATO_NUMERO_TO3',pic:'',nv:''},{av:'AV46Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV48TFContratoServicos_Codigo',fld:'vTFCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV49TFContratoServicos_Codigo_To',fld:'vTFCONTRATOSERVICOS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV52TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV53TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV56TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV57TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV60TFServico_Codigo',fld:'vTFSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV61TFServico_Codigo_To',fld:'vTFSERVICO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV64TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV65TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV50ddo_ContratoServicos_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Servico_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV9GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV9GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ContratoServicos_Codigo2',fld:'vCONTRATOSERVICOS_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26ContratoServicos_Codigo3',fld:'vCONTRATOSERVICOS_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoServicos_Codigo1',fld:'vCONTRATOSERVICOS_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV32Contrato_Codigo1',fld:'vCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV33Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV34Contrato_Numero_To1',fld:'vCONTRATO_NUMERO_TO1',pic:'',nv:''},{av:'AV44Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV36Contrato_Codigo2',fld:'vCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV37Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV38Contrato_Numero_To2',fld:'vCONTRATO_NUMERO_TO2',pic:'',nv:''},{av:'AV45Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV40Contrato_Codigo3',fld:'vCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV41Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV42Contrato_Numero_To3',fld:'vCONTRATO_NUMERO_TO3',pic:'',nv:''},{av:'AV46Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'edtavContratoservicos_codigo2_Visible',ctrl:'vCONTRATOSERVICOS_CODIGO2',prop:'Visible'},{av:'edtavContrato_codigo2_Visible',ctrl:'vCONTRATO_CODIGO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_numero2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_NUMERO2',prop:'Visible'},{av:'edtavServico_nome2_Visible',ctrl:'vSERVICO_NOME2',prop:'Visible'},{av:'edtavContratoservicos_codigo3_Visible',ctrl:'vCONTRATOSERVICOS_CODIGO3',prop:'Visible'},{av:'edtavContrato_codigo3_Visible',ctrl:'vCONTRATO_CODIGO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_numero3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_NUMERO3',prop:'Visible'},{av:'edtavServico_nome3_Visible',ctrl:'vSERVICO_NOME3',prop:'Visible'},{av:'edtavContratoservicos_codigo1_Visible',ctrl:'vCONTRATOSERVICOS_CODIGO1',prop:'Visible'},{av:'edtavContrato_codigo1_Visible',ctrl:'vCONTRATO_CODIGO1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_numero1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_NUMERO1',prop:'Visible'},{av:'edtavServico_nome1_Visible',ctrl:'vSERVICO_NOME1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E26742',iparms:[{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavContratoservicos_codigo3_Visible',ctrl:'vCONTRATOSERVICOS_CODIGO3',prop:'Visible'},{av:'edtavContrato_codigo3_Visible',ctrl:'vCONTRATO_CODIGO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_numero3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_NUMERO3',prop:'Visible'},{av:'edtavServico_nome3_Visible',ctrl:'vSERVICO_NOME3',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E21742',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoServicos_Codigo1',fld:'vCONTRATOSERVICOS_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV32Contrato_Codigo1',fld:'vCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV33Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV34Contrato_Numero_To1',fld:'vCONTRATO_NUMERO_TO1',pic:'',nv:''},{av:'AV44Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ContratoServicos_Codigo2',fld:'vCONTRATOSERVICOS_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV36Contrato_Codigo2',fld:'vCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV37Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV38Contrato_Numero_To2',fld:'vCONTRATO_NUMERO_TO2',pic:'',nv:''},{av:'AV45Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26ContratoServicos_Codigo3',fld:'vCONTRATOSERVICOS_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV40Contrato_Codigo3',fld:'vCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV41Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV42Contrato_Numero_To3',fld:'vCONTRATO_NUMERO_TO3',pic:'',nv:''},{av:'AV46Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV48TFContratoServicos_Codigo',fld:'vTFCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV49TFContratoServicos_Codigo_To',fld:'vTFCONTRATOSERVICOS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV52TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV53TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV56TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV57TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV60TFServico_Codigo',fld:'vTFSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV61TFServico_Codigo_To',fld:'vTFSERVICO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV64TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV65TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV50ddo_ContratoServicos_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Servico_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV9GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV48TFContratoServicos_Codigo',fld:'vTFCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoservicos_codigo_Filteredtext_set',ctrl:'DDO_CONTRATOSERVICOS_CODIGO',prop:'FilteredText_set'},{av:'AV49TFContratoServicos_Codigo_To',fld:'vTFCONTRATOSERVICOS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoservicos_codigo_Filteredtextto_set',ctrl:'DDO_CONTRATOSERVICOS_CODIGO',prop:'FilteredTextTo_set'},{av:'AV52TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contrato_codigo_Filteredtext_set',ctrl:'DDO_CONTRATO_CODIGO',prop:'FilteredText_set'},{av:'AV53TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contrato_codigo_Filteredtextto_set',ctrl:'DDO_CONTRATO_CODIGO',prop:'FilteredTextTo_set'},{av:'AV56TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'Ddo_contrato_numero_Filteredtext_set',ctrl:'DDO_CONTRATO_NUMERO',prop:'FilteredText_set'},{av:'AV57TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'Ddo_contrato_numero_Selectedvalue_set',ctrl:'DDO_CONTRATO_NUMERO',prop:'SelectedValue_set'},{av:'AV60TFServico_Codigo',fld:'vTFSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_servico_codigo_Filteredtext_set',ctrl:'DDO_SERVICO_CODIGO',prop:'FilteredText_set'},{av:'AV61TFServico_Codigo_To',fld:'vTFSERVICO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_servico_codigo_Filteredtextto_set',ctrl:'DDO_SERVICO_CODIGO',prop:'FilteredTextTo_set'},{av:'AV64TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'Ddo_servico_nome_Filteredtext_set',ctrl:'DDO_SERVICO_NOME',prop:'FilteredText_set'},{av:'AV65TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_servico_nome_Selectedvalue_set',ctrl:'DDO_SERVICO_NOME',prop:'SelectedValue_set'},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoServicos_Codigo1',fld:'vCONTRATOSERVICOS_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV9GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavContratoservicos_codigo1_Visible',ctrl:'vCONTRATOSERVICOS_CODIGO1',prop:'Visible'},{av:'edtavContrato_codigo1_Visible',ctrl:'vCONTRATO_CODIGO1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_numero1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_NUMERO1',prop:'Visible'},{av:'edtavServico_nome1_Visible',ctrl:'vSERVICO_NOME1',prop:'Visible'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ContratoServicos_Codigo2',fld:'vCONTRATOSERVICOS_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26ContratoServicos_Codigo3',fld:'vCONTRATOSERVICOS_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV32Contrato_Codigo1',fld:'vCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV33Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV34Contrato_Numero_To1',fld:'vCONTRATO_NUMERO_TO1',pic:'',nv:''},{av:'AV44Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV36Contrato_Codigo2',fld:'vCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV37Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV38Contrato_Numero_To2',fld:'vCONTRATO_NUMERO_TO2',pic:'',nv:''},{av:'AV45Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV40Contrato_Codigo3',fld:'vCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV41Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV42Contrato_Numero_To3',fld:'vCONTRATO_NUMERO_TO3',pic:'',nv:''},{av:'AV46Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'edtavContratoservicos_codigo2_Visible',ctrl:'vCONTRATOSERVICOS_CODIGO2',prop:'Visible'},{av:'edtavContrato_codigo2_Visible',ctrl:'vCONTRATO_CODIGO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_numero2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_NUMERO2',prop:'Visible'},{av:'edtavServico_nome2_Visible',ctrl:'vSERVICO_NOME2',prop:'Visible'},{av:'edtavContratoservicos_codigo3_Visible',ctrl:'vCONTRATOSERVICOS_CODIGO3',prop:'Visible'},{av:'edtavContrato_codigo3_Visible',ctrl:'vCONTRATO_CODIGO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_numero3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_NUMERO3',prop:'Visible'},{av:'edtavServico_nome3_Visible',ctrl:'vSERVICO_NOME3',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_contratoservicos_codigo_Activeeventkey = "";
         Ddo_contratoservicos_codigo_Filteredtext_get = "";
         Ddo_contratoservicos_codigo_Filteredtextto_get = "";
         Ddo_contrato_codigo_Activeeventkey = "";
         Ddo_contrato_codigo_Filteredtext_get = "";
         Ddo_contrato_codigo_Filteredtextto_get = "";
         Ddo_contrato_numero_Activeeventkey = "";
         Ddo_contrato_numero_Filteredtext_get = "";
         Ddo_contrato_numero_Selectedvalue_get = "";
         Ddo_servico_codigo_Activeeventkey = "";
         Ddo_servico_codigo_Filteredtext_get = "";
         Ddo_servico_codigo_Filteredtextto_get = "";
         Ddo_servico_nome_Activeeventkey = "";
         Ddo_servico_nome_Filteredtext_get = "";
         Ddo_servico_nome_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV14DynamicFiltersSelector1 = "";
         AV33Contrato_Numero1 = "";
         AV34Contrato_Numero_To1 = "";
         AV44Servico_Nome1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV37Contrato_Numero2 = "";
         AV38Contrato_Numero_To2 = "";
         AV45Servico_Nome2 = "";
         AV24DynamicFiltersSelector3 = "";
         AV41Contrato_Numero3 = "";
         AV42Contrato_Numero_To3 = "";
         AV46Servico_Nome3 = "";
         AV56TFContrato_Numero = "";
         AV57TFContrato_Numero_Sel = "";
         AV64TFServico_Nome = "";
         AV65TFServico_Nome_Sel = "";
         AV50ddo_ContratoServicos_CodigoTitleControlIdToReplace = "";
         AV54ddo_Contrato_CodigoTitleControlIdToReplace = "";
         AV58ddo_Contrato_NumeroTitleControlIdToReplace = "";
         AV62ddo_Servico_CodigoTitleControlIdToReplace = "";
         AV66ddo_Servico_NomeTitleControlIdToReplace = "";
         AV74Pgmname = "";
         AV9GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV67DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV47ContratoServicos_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV51Contrato_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV55Contrato_NumeroTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV59Servico_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV63Servico_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_contratoservicos_codigo_Filteredtext_set = "";
         Ddo_contratoservicos_codigo_Filteredtextto_set = "";
         Ddo_contratoservicos_codigo_Sortedstatus = "";
         Ddo_contrato_codigo_Filteredtext_set = "";
         Ddo_contrato_codigo_Filteredtextto_set = "";
         Ddo_contrato_codigo_Sortedstatus = "";
         Ddo_contrato_numero_Filteredtext_set = "";
         Ddo_contrato_numero_Selectedvalue_set = "";
         Ddo_contrato_numero_Sortedstatus = "";
         Ddo_servico_codigo_Filteredtext_set = "";
         Ddo_servico_codigo_Filteredtextto_set = "";
         Ddo_servico_codigo_Sortedstatus = "";
         Ddo_servico_nome_Filteredtext_set = "";
         Ddo_servico_nome_Selectedvalue_set = "";
         Ddo_servico_nome_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV30Select = "";
         AV73Select_GXI = "";
         A77Contrato_Numero = "";
         A608Servico_Nome = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV44Servico_Nome1 = "";
         lV45Servico_Nome2 = "";
         lV46Servico_Nome3 = "";
         lV56TFContrato_Numero = "";
         lV64TFServico_Nome = "";
         H00742_A608Servico_Nome = new String[] {""} ;
         H00742_A155Servico_Codigo = new int[1] ;
         H00742_A77Contrato_Numero = new String[] {""} ;
         H00742_A74Contrato_Codigo = new int[1] ;
         H00742_A160ContratoServicos_Codigo = new int[1] ;
         H00743_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV11GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV10GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblDynamicfilterscontrato_numero_rangemiddletext3_Jsonclick = "";
         lblDynamicfilterscontrato_numero_rangemiddletext2_Jsonclick = "";
         lblDynamicfilterscontrato_numero_rangemiddletext1_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptcontratoservicos__default(),
            new Object[][] {
                new Object[] {
               H00742_A608Servico_Nome, H00742_A155Servico_Codigo, H00742_A77Contrato_Numero, H00742_A74Contrato_Codigo, H00742_A160ContratoServicos_Codigo
               }
               , new Object[] {
               H00743_AGRID_nRecordCount
               }
            }
         );
         AV74Pgmname = "PromptContratoServicos";
         /* GeneXus formulas. */
         AV74Pgmname = "PromptContratoServicos";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_95 ;
      private short nGXsfl_95_idx=1 ;
      private short AV12OrderedBy ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_95_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContratoServicos_Codigo_Titleformat ;
      private short edtContrato_Codigo_Titleformat ;
      private short edtContrato_Numero_Titleformat ;
      private short edtServico_Codigo_Titleformat ;
      private short edtServico_Nome_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutContratoServicos_Codigo ;
      private int wcpOAV7InOutContratoServicos_Codigo ;
      private int subGrid_Rows ;
      private int AV16ContratoServicos_Codigo1 ;
      private int AV32Contrato_Codigo1 ;
      private int AV21ContratoServicos_Codigo2 ;
      private int AV36Contrato_Codigo2 ;
      private int AV26ContratoServicos_Codigo3 ;
      private int AV40Contrato_Codigo3 ;
      private int AV48TFContratoServicos_Codigo ;
      private int AV49TFContratoServicos_Codigo_To ;
      private int AV52TFContrato_Codigo ;
      private int AV53TFContrato_Codigo_To ;
      private int AV60TFServico_Codigo ;
      private int AV61TFServico_Codigo_To ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_contrato_numero_Datalistupdateminimumcharacters ;
      private int Ddo_servico_nome_Datalistupdateminimumcharacters ;
      private int edtavTfcontratoservicos_codigo_Visible ;
      private int edtavTfcontratoservicos_codigo_to_Visible ;
      private int edtavTfcontrato_codigo_Visible ;
      private int edtavTfcontrato_codigo_to_Visible ;
      private int edtavTfcontrato_numero_Visible ;
      private int edtavTfcontrato_numero_sel_Visible ;
      private int edtavTfservico_codigo_Visible ;
      private int edtavTfservico_codigo_to_Visible ;
      private int edtavTfservico_nome_Visible ;
      private int edtavTfservico_nome_sel_Visible ;
      private int edtavDdo_contratoservicos_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contrato_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_servico_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_servico_nometitlecontrolidtoreplace_Visible ;
      private int A160ContratoServicos_Codigo ;
      private int A74Contrato_Codigo ;
      private int A155Servico_Codigo ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV68PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavContratoservicos_codigo1_Visible ;
      private int edtavContrato_codigo1_Visible ;
      private int tblTablemergeddynamicfilterscontrato_numero1_Visible ;
      private int edtavServico_nome1_Visible ;
      private int edtavContratoservicos_codigo2_Visible ;
      private int edtavContrato_codigo2_Visible ;
      private int tblTablemergeddynamicfilterscontrato_numero2_Visible ;
      private int edtavServico_nome2_Visible ;
      private int edtavContratoservicos_codigo3_Visible ;
      private int edtavContrato_codigo3_Visible ;
      private int tblTablemergeddynamicfilterscontrato_numero3_Visible ;
      private int edtavServico_nome3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV69GridCurrentPage ;
      private long AV70GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contratoservicos_codigo_Activeeventkey ;
      private String Ddo_contratoservicos_codigo_Filteredtext_get ;
      private String Ddo_contratoservicos_codigo_Filteredtextto_get ;
      private String Ddo_contrato_codigo_Activeeventkey ;
      private String Ddo_contrato_codigo_Filteredtext_get ;
      private String Ddo_contrato_codigo_Filteredtextto_get ;
      private String Ddo_contrato_numero_Activeeventkey ;
      private String Ddo_contrato_numero_Filteredtext_get ;
      private String Ddo_contrato_numero_Selectedvalue_get ;
      private String Ddo_servico_codigo_Activeeventkey ;
      private String Ddo_servico_codigo_Filteredtext_get ;
      private String Ddo_servico_codigo_Filteredtextto_get ;
      private String Ddo_servico_nome_Activeeventkey ;
      private String Ddo_servico_nome_Filteredtext_get ;
      private String Ddo_servico_nome_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_95_idx="0001" ;
      private String AV33Contrato_Numero1 ;
      private String AV34Contrato_Numero_To1 ;
      private String AV44Servico_Nome1 ;
      private String AV37Contrato_Numero2 ;
      private String AV38Contrato_Numero_To2 ;
      private String AV45Servico_Nome2 ;
      private String AV41Contrato_Numero3 ;
      private String AV42Contrato_Numero_To3 ;
      private String AV46Servico_Nome3 ;
      private String AV56TFContrato_Numero ;
      private String AV57TFContrato_Numero_Sel ;
      private String AV64TFServico_Nome ;
      private String AV65TFServico_Nome_Sel ;
      private String AV74Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contratoservicos_codigo_Caption ;
      private String Ddo_contratoservicos_codigo_Tooltip ;
      private String Ddo_contratoservicos_codigo_Cls ;
      private String Ddo_contratoservicos_codigo_Filteredtext_set ;
      private String Ddo_contratoservicos_codigo_Filteredtextto_set ;
      private String Ddo_contratoservicos_codigo_Dropdownoptionstype ;
      private String Ddo_contratoservicos_codigo_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicos_codigo_Sortedstatus ;
      private String Ddo_contratoservicos_codigo_Filtertype ;
      private String Ddo_contratoservicos_codigo_Sortasc ;
      private String Ddo_contratoservicos_codigo_Sortdsc ;
      private String Ddo_contratoservicos_codigo_Cleanfilter ;
      private String Ddo_contratoservicos_codigo_Rangefilterfrom ;
      private String Ddo_contratoservicos_codigo_Rangefilterto ;
      private String Ddo_contratoservicos_codigo_Searchbuttontext ;
      private String Ddo_contrato_codigo_Caption ;
      private String Ddo_contrato_codigo_Tooltip ;
      private String Ddo_contrato_codigo_Cls ;
      private String Ddo_contrato_codigo_Filteredtext_set ;
      private String Ddo_contrato_codigo_Filteredtextto_set ;
      private String Ddo_contrato_codigo_Dropdownoptionstype ;
      private String Ddo_contrato_codigo_Titlecontrolidtoreplace ;
      private String Ddo_contrato_codigo_Sortedstatus ;
      private String Ddo_contrato_codigo_Filtertype ;
      private String Ddo_contrato_codigo_Sortasc ;
      private String Ddo_contrato_codigo_Sortdsc ;
      private String Ddo_contrato_codigo_Cleanfilter ;
      private String Ddo_contrato_codigo_Rangefilterfrom ;
      private String Ddo_contrato_codigo_Rangefilterto ;
      private String Ddo_contrato_codigo_Searchbuttontext ;
      private String Ddo_contrato_numero_Caption ;
      private String Ddo_contrato_numero_Tooltip ;
      private String Ddo_contrato_numero_Cls ;
      private String Ddo_contrato_numero_Filteredtext_set ;
      private String Ddo_contrato_numero_Selectedvalue_set ;
      private String Ddo_contrato_numero_Dropdownoptionstype ;
      private String Ddo_contrato_numero_Titlecontrolidtoreplace ;
      private String Ddo_contrato_numero_Sortedstatus ;
      private String Ddo_contrato_numero_Filtertype ;
      private String Ddo_contrato_numero_Datalisttype ;
      private String Ddo_contrato_numero_Datalistproc ;
      private String Ddo_contrato_numero_Sortasc ;
      private String Ddo_contrato_numero_Sortdsc ;
      private String Ddo_contrato_numero_Loadingdata ;
      private String Ddo_contrato_numero_Cleanfilter ;
      private String Ddo_contrato_numero_Noresultsfound ;
      private String Ddo_contrato_numero_Searchbuttontext ;
      private String Ddo_servico_codigo_Caption ;
      private String Ddo_servico_codigo_Tooltip ;
      private String Ddo_servico_codigo_Cls ;
      private String Ddo_servico_codigo_Filteredtext_set ;
      private String Ddo_servico_codigo_Filteredtextto_set ;
      private String Ddo_servico_codigo_Dropdownoptionstype ;
      private String Ddo_servico_codigo_Titlecontrolidtoreplace ;
      private String Ddo_servico_codigo_Sortedstatus ;
      private String Ddo_servico_codigo_Filtertype ;
      private String Ddo_servico_codigo_Sortasc ;
      private String Ddo_servico_codigo_Sortdsc ;
      private String Ddo_servico_codigo_Cleanfilter ;
      private String Ddo_servico_codigo_Rangefilterfrom ;
      private String Ddo_servico_codigo_Rangefilterto ;
      private String Ddo_servico_codigo_Searchbuttontext ;
      private String Ddo_servico_nome_Caption ;
      private String Ddo_servico_nome_Tooltip ;
      private String Ddo_servico_nome_Cls ;
      private String Ddo_servico_nome_Filteredtext_set ;
      private String Ddo_servico_nome_Selectedvalue_set ;
      private String Ddo_servico_nome_Dropdownoptionstype ;
      private String Ddo_servico_nome_Titlecontrolidtoreplace ;
      private String Ddo_servico_nome_Sortedstatus ;
      private String Ddo_servico_nome_Filtertype ;
      private String Ddo_servico_nome_Datalisttype ;
      private String Ddo_servico_nome_Datalistproc ;
      private String Ddo_servico_nome_Sortasc ;
      private String Ddo_servico_nome_Sortdsc ;
      private String Ddo_servico_nome_Loadingdata ;
      private String Ddo_servico_nome_Cleanfilter ;
      private String Ddo_servico_nome_Noresultsfound ;
      private String Ddo_servico_nome_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfcontratoservicos_codigo_Internalname ;
      private String edtavTfcontratoservicos_codigo_Jsonclick ;
      private String edtavTfcontratoservicos_codigo_to_Internalname ;
      private String edtavTfcontratoservicos_codigo_to_Jsonclick ;
      private String edtavTfcontrato_codigo_Internalname ;
      private String edtavTfcontrato_codigo_Jsonclick ;
      private String edtavTfcontrato_codigo_to_Internalname ;
      private String edtavTfcontrato_codigo_to_Jsonclick ;
      private String edtavTfcontrato_numero_Internalname ;
      private String edtavTfcontrato_numero_Jsonclick ;
      private String edtavTfcontrato_numero_sel_Internalname ;
      private String edtavTfcontrato_numero_sel_Jsonclick ;
      private String edtavTfservico_codigo_Internalname ;
      private String edtavTfservico_codigo_Jsonclick ;
      private String edtavTfservico_codigo_to_Internalname ;
      private String edtavTfservico_codigo_to_Jsonclick ;
      private String edtavTfservico_nome_Internalname ;
      private String edtavTfservico_nome_Jsonclick ;
      private String edtavTfservico_nome_sel_Internalname ;
      private String edtavTfservico_nome_sel_Jsonclick ;
      private String edtavDdo_contratoservicos_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contrato_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_servico_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_servico_nometitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtContratoServicos_Codigo_Internalname ;
      private String edtContrato_Codigo_Internalname ;
      private String A77Contrato_Numero ;
      private String edtContrato_Numero_Internalname ;
      private String edtServico_Codigo_Internalname ;
      private String A608Servico_Nome ;
      private String edtServico_Nome_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV44Servico_Nome1 ;
      private String lV45Servico_Nome2 ;
      private String lV46Servico_Nome3 ;
      private String lV56TFContrato_Numero ;
      private String lV64TFServico_Nome ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavContratoservicos_codigo1_Internalname ;
      private String edtavContrato_codigo1_Internalname ;
      private String edtavContrato_numero1_Internalname ;
      private String edtavContrato_numero_to1_Internalname ;
      private String edtavServico_nome1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavContratoservicos_codigo2_Internalname ;
      private String edtavContrato_codigo2_Internalname ;
      private String edtavContrato_numero2_Internalname ;
      private String edtavContrato_numero_to2_Internalname ;
      private String edtavServico_nome2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavContratoservicos_codigo3_Internalname ;
      private String edtavContrato_codigo3_Internalname ;
      private String edtavContrato_numero3_Internalname ;
      private String edtavContrato_numero_to3_Internalname ;
      private String edtavServico_nome3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_contratoservicos_codigo_Internalname ;
      private String Ddo_contrato_codigo_Internalname ;
      private String Ddo_contrato_numero_Internalname ;
      private String Ddo_servico_codigo_Internalname ;
      private String Ddo_servico_nome_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtContratoServicos_Codigo_Title ;
      private String edtContrato_Codigo_Title ;
      private String edtContrato_Numero_Title ;
      private String edtServico_Codigo_Title ;
      private String edtServico_Nome_Title ;
      private String edtavSelect_Tooltiptext ;
      private String tblTablemergeddynamicfilterscontrato_numero1_Internalname ;
      private String tblTablemergeddynamicfilterscontrato_numero2_Internalname ;
      private String tblTablemergeddynamicfilterscontrato_numero3_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavContratoservicos_codigo1_Jsonclick ;
      private String edtavContrato_codigo1_Jsonclick ;
      private String edtavServico_nome1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavContratoservicos_codigo2_Jsonclick ;
      private String edtavContrato_codigo2_Jsonclick ;
      private String edtavServico_nome2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavContratoservicos_codigo3_Jsonclick ;
      private String edtavContrato_codigo3_Jsonclick ;
      private String edtavServico_nome3_Jsonclick ;
      private String edtavContrato_numero3_Jsonclick ;
      private String lblDynamicfilterscontrato_numero_rangemiddletext3_Internalname ;
      private String lblDynamicfilterscontrato_numero_rangemiddletext3_Jsonclick ;
      private String edtavContrato_numero_to3_Jsonclick ;
      private String edtavContrato_numero2_Jsonclick ;
      private String lblDynamicfilterscontrato_numero_rangemiddletext2_Internalname ;
      private String lblDynamicfilterscontrato_numero_rangemiddletext2_Jsonclick ;
      private String edtavContrato_numero_to2_Jsonclick ;
      private String edtavContrato_numero1_Jsonclick ;
      private String lblDynamicfilterscontrato_numero_rangemiddletext1_Internalname ;
      private String lblDynamicfilterscontrato_numero_rangemiddletext1_Jsonclick ;
      private String edtavContrato_numero_to1_Jsonclick ;
      private String sGXsfl_95_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtContratoServicos_Codigo_Jsonclick ;
      private String edtContrato_Codigo_Jsonclick ;
      private String edtContrato_Numero_Jsonclick ;
      private String edtServico_Codigo_Jsonclick ;
      private String edtServico_Nome_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV13OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV23DynamicFiltersEnabled3 ;
      private bool AV29DynamicFiltersIgnoreFirst ;
      private bool AV28DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contratoservicos_codigo_Includesortasc ;
      private bool Ddo_contratoservicos_codigo_Includesortdsc ;
      private bool Ddo_contratoservicos_codigo_Includefilter ;
      private bool Ddo_contratoservicos_codigo_Filterisrange ;
      private bool Ddo_contratoservicos_codigo_Includedatalist ;
      private bool Ddo_contrato_codigo_Includesortasc ;
      private bool Ddo_contrato_codigo_Includesortdsc ;
      private bool Ddo_contrato_codigo_Includefilter ;
      private bool Ddo_contrato_codigo_Filterisrange ;
      private bool Ddo_contrato_codigo_Includedatalist ;
      private bool Ddo_contrato_numero_Includesortasc ;
      private bool Ddo_contrato_numero_Includesortdsc ;
      private bool Ddo_contrato_numero_Includefilter ;
      private bool Ddo_contrato_numero_Filterisrange ;
      private bool Ddo_contrato_numero_Includedatalist ;
      private bool Ddo_servico_codigo_Includesortasc ;
      private bool Ddo_servico_codigo_Includesortdsc ;
      private bool Ddo_servico_codigo_Includefilter ;
      private bool Ddo_servico_codigo_Filterisrange ;
      private bool Ddo_servico_codigo_Includedatalist ;
      private bool Ddo_servico_nome_Includesortasc ;
      private bool Ddo_servico_nome_Includesortdsc ;
      private bool Ddo_servico_nome_Includefilter ;
      private bool Ddo_servico_nome_Filterisrange ;
      private bool Ddo_servico_nome_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV30Select_IsBlob ;
      private String AV14DynamicFiltersSelector1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV24DynamicFiltersSelector3 ;
      private String AV50ddo_ContratoServicos_CodigoTitleControlIdToReplace ;
      private String AV54ddo_Contrato_CodigoTitleControlIdToReplace ;
      private String AV58ddo_Contrato_NumeroTitleControlIdToReplace ;
      private String AV62ddo_Servico_CodigoTitleControlIdToReplace ;
      private String AV66ddo_Servico_NomeTitleControlIdToReplace ;
      private String AV73Select_GXI ;
      private String AV30Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutContratoServicos_Codigo ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private String[] H00742_A608Servico_Nome ;
      private int[] H00742_A155Servico_Codigo ;
      private String[] H00742_A77Contrato_Numero ;
      private int[] H00742_A74Contrato_Codigo ;
      private int[] H00742_A160ContratoServicos_Codigo ;
      private long[] H00743_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV47ContratoServicos_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV51Contrato_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV55Contrato_NumeroTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV59Servico_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV63Servico_NomeTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV9GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV10GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV11GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV67DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptcontratoservicos__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00742( IGxContext context ,
                                             String AV14DynamicFiltersSelector1 ,
                                             int AV16ContratoServicos_Codigo1 ,
                                             int AV32Contrato_Codigo1 ,
                                             String AV33Contrato_Numero1 ,
                                             String AV34Contrato_Numero_To1 ,
                                             String AV44Servico_Nome1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             int AV21ContratoServicos_Codigo2 ,
                                             int AV36Contrato_Codigo2 ,
                                             String AV37Contrato_Numero2 ,
                                             String AV38Contrato_Numero_To2 ,
                                             String AV45Servico_Nome2 ,
                                             bool AV23DynamicFiltersEnabled3 ,
                                             String AV24DynamicFiltersSelector3 ,
                                             int AV26ContratoServicos_Codigo3 ,
                                             int AV40Contrato_Codigo3 ,
                                             String AV41Contrato_Numero3 ,
                                             String AV42Contrato_Numero_To3 ,
                                             String AV46Servico_Nome3 ,
                                             int AV48TFContratoServicos_Codigo ,
                                             int AV49TFContratoServicos_Codigo_To ,
                                             int AV52TFContrato_Codigo ,
                                             int AV53TFContrato_Codigo_To ,
                                             String AV57TFContrato_Numero_Sel ,
                                             String AV56TFContrato_Numero ,
                                             int AV60TFServico_Codigo ,
                                             int AV61TFServico_Codigo_To ,
                                             String AV65TFServico_Nome_Sel ,
                                             String AV64TFServico_Nome ,
                                             int A160ContratoServicos_Codigo ,
                                             int A74Contrato_Codigo ,
                                             String A77Contrato_Numero ,
                                             String A608Servico_Nome ,
                                             int A155Servico_Codigo ,
                                             short AV12OrderedBy ,
                                             bool AV13OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [30] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T2.[Servico_Nome], T1.[Servico_Codigo], T3.[Contrato_Numero], T1.[Contrato_Codigo], T1.[ContratoServicos_Codigo]";
         sFromString = " FROM (([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) INNER JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTRATOSERVICOS_CODIGO") == 0 ) && ( ! (0==AV16ContratoServicos_Codigo1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicos_Codigo] = @AV16ContratoServicos_Codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicos_Codigo] = @AV16ContratoServicos_Codigo1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTRATO_CODIGO") == 0 ) && ( ! (0==AV32Contrato_Codigo1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] >= @AV32Contrato_Codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] >= @AV32Contrato_Codigo1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33Contrato_Numero1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] >= @AV33Contrato_Numero1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] >= @AV33Contrato_Numero1)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34Contrato_Numero_To1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] <= @AV34Contrato_Numero_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] <= @AV34Contrato_Numero_To1)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44Servico_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] like '%' + @lV44Servico_Nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] like '%' + @lV44Servico_Nome1 + '%')";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOSERVICOS_CODIGO") == 0 ) && ( ! (0==AV21ContratoServicos_Codigo2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicos_Codigo] = @AV21ContratoServicos_Codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicos_Codigo] = @AV21ContratoServicos_Codigo2)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATO_CODIGO") == 0 ) && ( ! (0==AV36Contrato_Codigo2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] >= @AV36Contrato_Codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] >= @AV36Contrato_Codigo2)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37Contrato_Numero2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] >= @AV37Contrato_Numero2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] >= @AV37Contrato_Numero2)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38Contrato_Numero_To2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] <= @AV38Contrato_Numero_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] <= @AV38Contrato_Numero_To2)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45Servico_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] like '%' + @lV45Servico_Nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] like '%' + @lV45Servico_Nome2 + '%')";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTRATOSERVICOS_CODIGO") == 0 ) && ( ! (0==AV26ContratoServicos_Codigo3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicos_Codigo] = @AV26ContratoServicos_Codigo3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicos_Codigo] = @AV26ContratoServicos_Codigo3)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTRATO_CODIGO") == 0 ) && ( ! (0==AV40Contrato_Codigo3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] >= @AV40Contrato_Codigo3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] >= @AV40Contrato_Codigo3)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41Contrato_Numero3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] >= @AV41Contrato_Numero3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] >= @AV41Contrato_Numero3)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Contrato_Numero_To3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] <= @AV42Contrato_Numero_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] <= @AV42Contrato_Numero_To3)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46Servico_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] like '%' + @lV46Servico_Nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] like '%' + @lV46Servico_Nome3 + '%')";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( ! (0==AV48TFContratoServicos_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicos_Codigo] >= @AV48TFContratoServicos_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicos_Codigo] >= @AV48TFContratoServicos_Codigo)";
            }
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( ! (0==AV49TFContratoServicos_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicos_Codigo] <= @AV49TFContratoServicos_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicos_Codigo] <= @AV49TFContratoServicos_Codigo_To)";
            }
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( ! (0==AV52TFContrato_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] >= @AV52TFContrato_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] >= @AV52TFContrato_Codigo)";
            }
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( ! (0==AV53TFContrato_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] <= @AV53TFContrato_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] <= @AV53TFContrato_Codigo_To)";
            }
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV57TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] like @lV56TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] like @lV56TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] = @AV57TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] = @AV57TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( ! (0==AV60TFServico_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Codigo] >= @AV60TFServico_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Codigo] >= @AV60TFServico_Codigo)";
            }
         }
         else
         {
            GXv_int2[21] = 1;
         }
         if ( ! (0==AV61TFServico_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Codigo] <= @AV61TFServico_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Codigo] <= @AV61TFServico_Codigo_To)";
            }
         }
         else
         {
            GXv_int2[22] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV65TFServico_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64TFServico_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] like @lV64TFServico_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] like @lV64TFServico_Nome)";
            }
         }
         else
         {
            GXv_int2[23] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65TFServico_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] = @AV65TFServico_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] = @AV65TFServico_Nome_Sel)";
            }
         }
         else
         {
            GXv_int2[24] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV12OrderedBy == 1 ) && ! AV13OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicos_Codigo]";
         }
         else if ( ( AV12OrderedBy == 1 ) && ( AV13OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicos_Codigo] DESC";
         }
         else if ( ( AV12OrderedBy == 2 ) && ! AV13OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo]";
         }
         else if ( ( AV12OrderedBy == 2 ) && ( AV13OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo] DESC";
         }
         else if ( ( AV12OrderedBy == 3 ) && ! AV13OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Contrato_Numero]";
         }
         else if ( ( AV12OrderedBy == 3 ) && ( AV13OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Contrato_Numero] DESC";
         }
         else if ( ( AV12OrderedBy == 4 ) && ! AV13OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Codigo]";
         }
         else if ( ( AV12OrderedBy == 4 ) && ( AV13OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Codigo] DESC";
         }
         else if ( ( AV12OrderedBy == 5 ) && ! AV13OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Servico_Nome]";
         }
         else if ( ( AV12OrderedBy == 5 ) && ( AV13OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Servico_Nome] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicos_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00743( IGxContext context ,
                                             String AV14DynamicFiltersSelector1 ,
                                             int AV16ContratoServicos_Codigo1 ,
                                             int AV32Contrato_Codigo1 ,
                                             String AV33Contrato_Numero1 ,
                                             String AV34Contrato_Numero_To1 ,
                                             String AV44Servico_Nome1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             int AV21ContratoServicos_Codigo2 ,
                                             int AV36Contrato_Codigo2 ,
                                             String AV37Contrato_Numero2 ,
                                             String AV38Contrato_Numero_To2 ,
                                             String AV45Servico_Nome2 ,
                                             bool AV23DynamicFiltersEnabled3 ,
                                             String AV24DynamicFiltersSelector3 ,
                                             int AV26ContratoServicos_Codigo3 ,
                                             int AV40Contrato_Codigo3 ,
                                             String AV41Contrato_Numero3 ,
                                             String AV42Contrato_Numero_To3 ,
                                             String AV46Servico_Nome3 ,
                                             int AV48TFContratoServicos_Codigo ,
                                             int AV49TFContratoServicos_Codigo_To ,
                                             int AV52TFContrato_Codigo ,
                                             int AV53TFContrato_Codigo_To ,
                                             String AV57TFContrato_Numero_Sel ,
                                             String AV56TFContrato_Numero ,
                                             int AV60TFServico_Codigo ,
                                             int AV61TFServico_Codigo_To ,
                                             String AV65TFServico_Nome_Sel ,
                                             String AV64TFServico_Nome ,
                                             int A160ContratoServicos_Codigo ,
                                             int A74Contrato_Codigo ,
                                             String A77Contrato_Numero ,
                                             String A608Servico_Nome ,
                                             int A155Servico_Codigo ,
                                             short AV12OrderedBy ,
                                             bool AV13OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [25] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) INNER JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         if ( ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTRATOSERVICOS_CODIGO") == 0 ) && ( ! (0==AV16ContratoServicos_Codigo1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicos_Codigo] = @AV16ContratoServicos_Codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicos_Codigo] = @AV16ContratoServicos_Codigo1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTRATO_CODIGO") == 0 ) && ( ! (0==AV32Contrato_Codigo1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] >= @AV32Contrato_Codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] >= @AV32Contrato_Codigo1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33Contrato_Numero1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] >= @AV33Contrato_Numero1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] >= @AV33Contrato_Numero1)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34Contrato_Numero_To1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] <= @AV34Contrato_Numero_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] <= @AV34Contrato_Numero_To1)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44Servico_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] like '%' + @lV44Servico_Nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] like '%' + @lV44Servico_Nome1 + '%')";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOSERVICOS_CODIGO") == 0 ) && ( ! (0==AV21ContratoServicos_Codigo2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicos_Codigo] = @AV21ContratoServicos_Codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicos_Codigo] = @AV21ContratoServicos_Codigo2)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATO_CODIGO") == 0 ) && ( ! (0==AV36Contrato_Codigo2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] >= @AV36Contrato_Codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] >= @AV36Contrato_Codigo2)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37Contrato_Numero2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] >= @AV37Contrato_Numero2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] >= @AV37Contrato_Numero2)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38Contrato_Numero_To2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] <= @AV38Contrato_Numero_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] <= @AV38Contrato_Numero_To2)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45Servico_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] like '%' + @lV45Servico_Nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] like '%' + @lV45Servico_Nome2 + '%')";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTRATOSERVICOS_CODIGO") == 0 ) && ( ! (0==AV26ContratoServicos_Codigo3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicos_Codigo] = @AV26ContratoServicos_Codigo3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicos_Codigo] = @AV26ContratoServicos_Codigo3)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTRATO_CODIGO") == 0 ) && ( ! (0==AV40Contrato_Codigo3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] >= @AV40Contrato_Codigo3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] >= @AV40Contrato_Codigo3)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41Contrato_Numero3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] >= @AV41Contrato_Numero3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] >= @AV41Contrato_Numero3)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Contrato_Numero_To3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] <= @AV42Contrato_Numero_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] <= @AV42Contrato_Numero_To3)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46Servico_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] like '%' + @lV46Servico_Nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] like '%' + @lV46Servico_Nome3 + '%')";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( ! (0==AV48TFContratoServicos_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicos_Codigo] >= @AV48TFContratoServicos_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicos_Codigo] >= @AV48TFContratoServicos_Codigo)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( ! (0==AV49TFContratoServicos_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicos_Codigo] <= @AV49TFContratoServicos_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicos_Codigo] <= @AV49TFContratoServicos_Codigo_To)";
            }
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( ! (0==AV52TFContrato_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] >= @AV52TFContrato_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] >= @AV52TFContrato_Codigo)";
            }
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( ! (0==AV53TFContrato_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] <= @AV53TFContrato_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] <= @AV53TFContrato_Codigo_To)";
            }
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV57TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] like @lV56TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] like @lV56TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] = @AV57TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] = @AV57TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int4[20] = 1;
         }
         if ( ! (0==AV60TFServico_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Codigo] >= @AV60TFServico_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Codigo] >= @AV60TFServico_Codigo)";
            }
         }
         else
         {
            GXv_int4[21] = 1;
         }
         if ( ! (0==AV61TFServico_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Codigo] <= @AV61TFServico_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Codigo] <= @AV61TFServico_Codigo_To)";
            }
         }
         else
         {
            GXv_int4[22] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV65TFServico_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64TFServico_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] like @lV64TFServico_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] like @lV64TFServico_Nome)";
            }
         }
         else
         {
            GXv_int4[23] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65TFServico_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] = @AV65TFServico_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] = @AV65TFServico_Nome_Sel)";
            }
         }
         else
         {
            GXv_int4[24] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV12OrderedBy == 1 ) && ! AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV12OrderedBy == 1 ) && ( AV13OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV12OrderedBy == 2 ) && ! AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV12OrderedBy == 2 ) && ( AV13OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV12OrderedBy == 3 ) && ! AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV12OrderedBy == 3 ) && ( AV13OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV12OrderedBy == 4 ) && ! AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV12OrderedBy == 4 ) && ( AV13OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV12OrderedBy == 5 ) && ! AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV12OrderedBy == 5 ) && ( AV13OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00742(context, (String)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (int)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (int)dynConstraints[34] , (short)dynConstraints[35] , (bool)dynConstraints[36] );
               case 1 :
                     return conditional_H00743(context, (String)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (int)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (int)dynConstraints[34] , (short)dynConstraints[35] , (bool)dynConstraints[36] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00742 ;
          prmH00742 = new Object[] {
          new Object[] {"@AV16ContratoServicos_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV32Contrato_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV33Contrato_Numero1",SqlDbType.Char,20,0} ,
          new Object[] {"@AV34Contrato_Numero_To1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV44Servico_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV21ContratoServicos_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV36Contrato_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV37Contrato_Numero2",SqlDbType.Char,20,0} ,
          new Object[] {"@AV38Contrato_Numero_To2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV45Servico_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV26ContratoServicos_Codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV40Contrato_Codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV41Contrato_Numero3",SqlDbType.Char,20,0} ,
          new Object[] {"@AV42Contrato_Numero_To3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV46Servico_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV48TFContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV49TFContratoServicos_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV52TFContrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV53TFContrato_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV56TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV57TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV60TFServico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV61TFServico_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV64TFServico_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV65TFServico_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00743 ;
          prmH00743 = new Object[] {
          new Object[] {"@AV16ContratoServicos_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV32Contrato_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV33Contrato_Numero1",SqlDbType.Char,20,0} ,
          new Object[] {"@AV34Contrato_Numero_To1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV44Servico_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV21ContratoServicos_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV36Contrato_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV37Contrato_Numero2",SqlDbType.Char,20,0} ,
          new Object[] {"@AV38Contrato_Numero_To2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV45Servico_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV26ContratoServicos_Codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV40Contrato_Codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV41Contrato_Numero3",SqlDbType.Char,20,0} ,
          new Object[] {"@AV42Contrato_Numero_To3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV46Servico_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV48TFContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV49TFContratoServicos_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV52TFContrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV53TFContrato_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV56TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV57TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV60TFServico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV61TFServico_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV64TFServico_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV65TFServico_Nome_Sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00742", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00742,11,0,true,false )
             ,new CursorDef("H00743", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00743,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[48]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[51]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[52]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[55]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[56]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[58]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[59]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                return;
       }
    }

 }

}
