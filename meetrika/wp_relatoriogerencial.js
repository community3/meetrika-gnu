/**@preserve  GeneXus C# 10_3_14-114418 on 3/12/2020 21:24:44.34
*/
gx.evt.autoSkip = false;
gx.define('wp_relatoriogerencial', false, function () {
   this.ServerClass =  "wp_relatoriogerencial" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.hasEnterEvent = false;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV10WWPContext=gx.fn.getControlValue("vWWPCONTEXT") ;
      this.AV15CheckRequiredFieldsResult=gx.fn.getControlValue("vCHECKREQUIREDFIELDSRESULT") ;
   };
   this.Validv_Contagemresultado_datadmnin=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTAGEMRESULTADO_DATADMNIN");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contagemresultado_datadmnfim=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTAGEMRESULTADO_DATADMNFIM");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Statusdemanda=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vSTATUSDEMANDA");
         this.AnyError  = 0;
         if ( ! ( ( this.AV9StatusDemanda == "B" ) || ( this.AV9StatusDemanda == "S" ) || ( this.AV9StatusDemanda == "E" ) || ( this.AV9StatusDemanda == "A" ) || ( this.AV9StatusDemanda == "R" ) || ( this.AV9StatusDemanda == "C" ) || ( this.AV9StatusDemanda == "D" ) || ( this.AV9StatusDemanda == "H" ) || ( this.AV9StatusDemanda == "O" ) || ( this.AV9StatusDemanda == "P" ) || ( this.AV9StatusDemanda == "L" ) || ( this.AV9StatusDemanda == "X" ) || ( this.AV9StatusDemanda == "N" ) || ( this.AV9StatusDemanda == "J" ) || ( this.AV9StatusDemanda == "I" ) || ( this.AV9StatusDemanda == "T" ) || ( this.AV9StatusDemanda == "Q" ) || ( this.AV9StatusDemanda == "G" ) || ( this.AV9StatusDemanda == "M" ) || ( this.AV9StatusDemanda == "U" ) || ((''==this.AV9StatusDemanda)) ) )
         {
            try {
               gxballoon.setError("Campo Status Demanda fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Data_contagem=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vDATA_CONTAGEM");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV7Data_Contagem)==0) || new gx.date.gxdate( this.AV7Data_Contagem ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Data_Contagem fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.s112_client=function()
   {
      this.AV15CheckRequiredFieldsResult =  true  ;
      if ( ! (new gx.date.gxdate('').compare(this.AV13ContagemResultado_DataDmnIn)==0) && ! (new gx.date.gxdate('').compare(this.AV14ContagemResultado_DataDmnFim)==0) )
      {
         if ( new gx.date.gxdate( this.AV14ContagemResultado_DataDmnFim ).compare( this.AV13ContagemResultado_DataDmnIn ) < 0 )
         {
            this.addMessage("O período informado para a Data da Demanda não é válido. Verifique!");
            this.AV15CheckRequiredFieldsResult =  false  ;
         }
      }
      else
      {
         this.addMessage("Data da Demanda é obrigatório. Verifique!");
         this.AV15CheckRequiredFieldsResult =  false  ;
      }
   };
   this.e14s51_client=function()
   {
      this.clearMessages();
      this.refreshOutputs([]);
   };
   this.e15s51_client=function()
   {
      this.clearMessages();
      this.refreshOutputs([]);
   };
   this.e12s52_client=function()
   {
      this.executeServerEvent("'DOBNTPDF'", false, null, false, false);
   };
   this.e16s52_client=function()
   {
      this.executeServerEvent("ENTER", true, null, false, false);
   };
   this.e17s52_client=function()
   {
      this.executeServerEvent("CANCEL", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,8,13,14,15,16,17,18,21,22,23,24,25,26,29,30,32,34,35,38,39,40,41,42,43,44,47,48,49,50,51,52,53,56,57,58,59,60,61,62,63,64,65,73];
   this.GXLastCtrlId =73;
   this.DVPANEL_UNNAMEDTABLE1Container = gx.uc.getNew(this, 11, 0, "BootstrapPanel", "DVPANEL_UNNAMEDTABLE1Container", "Dvpanel_unnamedtable1");
   var DVPANEL_UNNAMEDTABLE1Container = this.DVPANEL_UNNAMEDTABLE1Container;
   DVPANEL_UNNAMEDTABLE1Container.setProp("Width", "Width", "100%", "str");
   DVPANEL_UNNAMEDTABLE1Container.setProp("Height", "Height", "100", "str");
   DVPANEL_UNNAMEDTABLE1Container.setProp("AutoWidth", "Autowidth", false, "bool");
   DVPANEL_UNNAMEDTABLE1Container.setProp("AutoHeight", "Autoheight", true, "bool");
   DVPANEL_UNNAMEDTABLE1Container.setProp("Cls", "Cls", "GXUI-DVelop-Panel", "str");
   DVPANEL_UNNAMEDTABLE1Container.setProp("ShowHeader", "Showheader", true, "bool");
   DVPANEL_UNNAMEDTABLE1Container.setProp("Title", "Title", "Relatório Gerencial", "str");
   DVPANEL_UNNAMEDTABLE1Container.setProp("Collapsible", "Collapsible", true, "bool");
   DVPANEL_UNNAMEDTABLE1Container.setProp("Collapsed", "Collapsed", false, "bool");
   DVPANEL_UNNAMEDTABLE1Container.setProp("ShowCollapseIcon", "Showcollapseicon", false, "bool");
   DVPANEL_UNNAMEDTABLE1Container.setProp("IconPosition", "Iconposition", "left", "str");
   DVPANEL_UNNAMEDTABLE1Container.setProp("AutoScroll", "Autoscroll", false, "bool");
   DVPANEL_UNNAMEDTABLE1Container.setProp("Visible", "Visible", true, "bool");
   DVPANEL_UNNAMEDTABLE1Container.setProp("Enabled", "Enabled", true, "boolean");
   DVPANEL_UNNAMEDTABLE1Container.setProp("Class", "Class", "", "char");
   DVPANEL_UNNAMEDTABLE1Container.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(DVPANEL_UNNAMEDTABLE1Container);
   GXValidFnc[2]={fld:"TABLEMAIN",grid:0};
   GXValidFnc[8]={fld:"TABLECONTENT",grid:0};
   GXValidFnc[13]={fld:"LAYOUT_UNNAMEDTABLE1",grid:0};
   GXValidFnc[14]={fld:"",grid:0};
   GXValidFnc[15]={fld:"UNNAMEDTABLE1",grid:0};
   GXValidFnc[16]={fld:"",grid:0};
   GXValidFnc[17]={fld:"",grid:0};
   GXValidFnc[18]={fld:"UNNAMEDTABLE2",grid:0};
   GXValidFnc[21]={fld:"TABLESPLITTEDCONTAGEMRESULTADO_DATADMNIN",grid:0};
   GXValidFnc[22]={fld:"",grid:0};
   GXValidFnc[23]={fld:"",grid:0};
   GXValidFnc[24]={fld:"TEXTBLOCKCONTAGEMRESULTADO_DATADMNIN", format:0,grid:0};
   GXValidFnc[25]={fld:"",grid:0};
   GXValidFnc[26]={fld:"TABLEMERGEDCONTAGEMRESULTADO_DATADMNIN",grid:0};
   GXValidFnc[29]={fld:"",grid:0};
   GXValidFnc[30]={lvl:0,type:"date",len:10,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contagemresultado_datadmnin,isvalid:null,rgrid:[],fld:"vCONTAGEMRESULTADO_DATADMNIN",gxz:"ZV13ContagemResultado_DataDmnIn",gxold:"OV13ContagemResultado_DataDmnIn",gxvar:"AV13ContagemResultado_DataDmnIn",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/9999",dec:0},ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV13ContagemResultado_DataDmnIn=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV13ContagemResultado_DataDmnIn=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vCONTAGEMRESULTADO_DATADMNIN",gx.O.AV13ContagemResultado_DataDmnIn,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV13ContagemResultado_DataDmnIn=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vCONTAGEMRESULTADO_DATADMNIN")},nac:gx.falseFn};
   GXValidFnc[32]={fld:"TXTA", format:0,grid:0};
   GXValidFnc[34]={fld:"",grid:0};
   GXValidFnc[35]={lvl:0,type:"date",len:10,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contagemresultado_datadmnfim,isvalid:null,rgrid:[],fld:"vCONTAGEMRESULTADO_DATADMNFIM",gxz:"ZV14ContagemResultado_DataDmnFim",gxold:"OV14ContagemResultado_DataDmnFim",gxvar:"AV14ContagemResultado_DataDmnFim",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/9999",dec:0},ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV14ContagemResultado_DataDmnFim=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV14ContagemResultado_DataDmnFim=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vCONTAGEMRESULTADO_DATADMNFIM",gx.O.AV14ContagemResultado_DataDmnFim,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV14ContagemResultado_DataDmnFim=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vCONTAGEMRESULTADO_DATADMNFIM")},nac:gx.falseFn};
   GXValidFnc[38]={fld:"UNNAMEDTABLECONTRATADAORIGEM",grid:0};
   GXValidFnc[39]={fld:"",grid:0};
   GXValidFnc[40]={fld:"",grid:0};
   GXValidFnc[41]={fld:"TEXTBLOCKCONTRATADAORIGEM", format:0,grid:0};
   GXValidFnc[42]={fld:"",grid:0};
   GXValidFnc[43]={fld:"",grid:0};
   GXValidFnc[44]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vCONTRATADAORIGEM",gxz:"ZV6ContratadaOrigem",gxold:"OV6ContratadaOrigem",gxvar:"AV6ContratadaOrigem",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"dyncombo",v2v:function(Value){if(Value!==undefined)gx.O.AV6ContratadaOrigem=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV6ContratadaOrigem=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vCONTRATADAORIGEM",gx.O.AV6ContratadaOrigem);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.AV6ContratadaOrigem=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vCONTRATADAORIGEM",'.')},nac:gx.falseFn};
   this.declareDomainHdlr( 44 , function() {
   });
   GXValidFnc[47]={fld:"UNNAMEDTABLESERVICO_CODIGO",grid:0};
   GXValidFnc[48]={fld:"",grid:0};
   GXValidFnc[49]={fld:"",grid:0};
   GXValidFnc[50]={fld:"TEXTBLOCKSERVICO_CODIGO", format:0,grid:0};
   GXValidFnc[51]={fld:"",grid:0};
   GXValidFnc[52]={fld:"",grid:0};
   GXValidFnc[53]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vSERVICO_CODIGO",gxz:"ZV8Servico_Codigo",gxold:"OV8Servico_Codigo",gxvar:"AV8Servico_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"dyncombo",v2v:function(Value){if(Value!==undefined)gx.O.AV8Servico_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV8Servico_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vSERVICO_CODIGO",gx.O.AV8Servico_Codigo)},c2v:function(){if(this.val()!==undefined)gx.O.AV8Servico_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vSERVICO_CODIGO",'.')},nac:gx.falseFn};
   GXValidFnc[56]={fld:"UNNAMEDTABLESTATUSDEMANDA",grid:0};
   GXValidFnc[57]={fld:"",grid:0};
   GXValidFnc[58]={fld:"",grid:0};
   GXValidFnc[59]={fld:"TEXTBLOCKSTATUSDEMANDA", format:0,grid:0};
   GXValidFnc[60]={fld:"",grid:0};
   GXValidFnc[61]={fld:"",grid:0};
   GXValidFnc[62]={lvl:0,type:"char",len:1,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Statusdemanda,isvalid:null,rgrid:[],fld:"vSTATUSDEMANDA",gxz:"ZV9StatusDemanda",gxold:"OV9StatusDemanda",gxvar:"AV9StatusDemanda",ucs:[],op:[62],ip:[62],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV9StatusDemanda=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV9StatusDemanda=Value},v2c:function(){gx.fn.setComboBoxValue("vSTATUSDEMANDA",gx.O.AV9StatusDemanda);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.AV9StatusDemanda=this.val()},val:function(){return gx.fn.getControlValue("vSTATUSDEMANDA")},nac:gx.falseFn};
   this.declareDomainHdlr( 62 , function() {
   });
   GXValidFnc[63]={fld:"",grid:0};
   GXValidFnc[64]={fld:"",grid:0};
   GXValidFnc[65]={fld:"TABLEACTION",grid:0};
   GXValidFnc[73]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Data_contagem,isvalid:null,rgrid:[],fld:"vDATA_CONTAGEM",gxz:"ZV7Data_Contagem",gxold:"OV7Data_Contagem",gxvar:"AV7Data_Contagem",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[73],ip:[73],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV7Data_Contagem=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV7Data_Contagem=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vDATA_CONTAGEM",gx.O.AV7Data_Contagem,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV7Data_Contagem=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vDATA_CONTAGEM")},nac:gx.falseFn};
   this.AV13ContagemResultado_DataDmnIn = gx.date.nullDate() ;
   this.ZV13ContagemResultado_DataDmnIn = gx.date.nullDate() ;
   this.OV13ContagemResultado_DataDmnIn = gx.date.nullDate() ;
   this.AV14ContagemResultado_DataDmnFim = gx.date.nullDate() ;
   this.ZV14ContagemResultado_DataDmnFim = gx.date.nullDate() ;
   this.OV14ContagemResultado_DataDmnFim = gx.date.nullDate() ;
   this.AV6ContratadaOrigem = 0 ;
   this.ZV6ContratadaOrigem = 0 ;
   this.OV6ContratadaOrigem = 0 ;
   this.AV8Servico_Codigo = 0 ;
   this.ZV8Servico_Codigo = 0 ;
   this.OV8Servico_Codigo = 0 ;
   this.AV9StatusDemanda = "" ;
   this.ZV9StatusDemanda = "" ;
   this.OV9StatusDemanda = "" ;
   this.AV7Data_Contagem = gx.date.nullDate() ;
   this.ZV7Data_Contagem = gx.date.nullDate() ;
   this.OV7Data_Contagem = gx.date.nullDate() ;
   this.AV13ContagemResultado_DataDmnIn = gx.date.nullDate() ;
   this.AV14ContagemResultado_DataDmnFim = gx.date.nullDate() ;
   this.AV6ContratadaOrigem = 0 ;
   this.AV8Servico_Codigo = 0 ;
   this.AV9StatusDemanda = "" ;
   this.AV7Data_Contagem = gx.date.nullDate() ;
   this.AV15CheckRequiredFieldsResult = false ;
   this.Events = {"e12s52_client": ["'DOBNTPDF'", true] ,"e16s52_client": ["ENTER", true] ,"e17s52_client": ["CANCEL", true] ,"e14s51_client": ["'DOBNTEXCEL'", false] ,"e15s51_client": ["'DOBNTWORD'", false]};
   this.EvtParms["REFRESH"] = [[],[]];
   this.EvtParms["'DOBNTPDF'"] = [[{av:'AV15CheckRequiredFieldsResult',fld:'vCHECKREQUIREDFIELDSRESULT',pic:'',nv:false},{av:'AV13ContagemResultado_DataDmnIn',fld:'vCONTAGEMRESULTADO_DATADMNIN',pic:'',nv:''},{av:'AV14ContagemResultado_DataDmnFim',fld:'vCONTAGEMRESULTADO_DATADMNFIM',pic:'',nv:''},{av:'AV6ContratadaOrigem',fld:'vCONTRATADAORIGEM',pic:'ZZZZZ9',nv:0},{av:'AV8Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV9StatusDemanda',fld:'vSTATUSDEMANDA',pic:'',nv:''}],[{av:'AV15CheckRequiredFieldsResult',fld:'vCHECKREQUIREDFIELDSRESULT',pic:'',nv:false}]];
   this.EvtParms["'DOBNTEXCEL'"] = [[],[]];
   this.EvtParms["'DOBNTWORD'"] = [[],[]];
   this.setVCMap("AV10WWPContext", "vWWPCONTEXT", 0, "WWPBaseObjects\WWPContext");
   this.setVCMap("AV15CheckRequiredFieldsResult", "vCHECKREQUIREDFIELDSRESULT", 0, "boolean");
   this.InitStandaloneVars( );
});
gx.createParentObj(wp_relatoriogerencial);
