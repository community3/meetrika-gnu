/*
               File: GetWWLoteArquivoAnexoFilterData
        Description: Get WWLote Arquivo Anexo Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:11:59.42
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwlotearquivoanexofilterdata : GXProcedure
   {
      public getwwlotearquivoanexofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwlotearquivoanexofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
         return AV27OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwlotearquivoanexofilterdata objgetwwlotearquivoanexofilterdata;
         objgetwwlotearquivoanexofilterdata = new getwwlotearquivoanexofilterdata();
         objgetwwlotearquivoanexofilterdata.AV18DDOName = aP0_DDOName;
         objgetwwlotearquivoanexofilterdata.AV16SearchTxt = aP1_SearchTxt;
         objgetwwlotearquivoanexofilterdata.AV17SearchTxtTo = aP2_SearchTxtTo;
         objgetwwlotearquivoanexofilterdata.AV22OptionsJson = "" ;
         objgetwwlotearquivoanexofilterdata.AV25OptionsDescJson = "" ;
         objgetwwlotearquivoanexofilterdata.AV27OptionIndexesJson = "" ;
         objgetwwlotearquivoanexofilterdata.context.SetSubmitInitialConfig(context);
         objgetwwlotearquivoanexofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwlotearquivoanexofilterdata);
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwlotearquivoanexofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV21Options = (IGxCollection)(new GxSimpleCollection());
         AV24OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV26OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_LOTEARQUIVOANEXO_NOMEARQ") == 0 )
         {
            /* Execute user subroutine: 'LOADLOTEARQUIVOANEXO_NOMEARQOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_TIPODOCUMENTO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADTIPODOCUMENTO_NOMEOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV22OptionsJson = AV21Options.ToJSonString(false);
         AV25OptionsDescJson = AV24OptionsDesc.ToJSonString(false);
         AV27OptionIndexesJson = AV26OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV29Session.Get("WWLoteArquivoAnexoGridState"), "") == 0 )
         {
            AV31GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWLoteArquivoAnexoGridState"), "");
         }
         else
         {
            AV31GridState.FromXml(AV29Session.Get("WWLoteArquivoAnexoGridState"), "");
         }
         AV50GXV1 = 1;
         while ( AV50GXV1 <= AV31GridState.gxTpr_Filtervalues.Count )
         {
            AV32GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV31GridState.gxTpr_Filtervalues.Item(AV50GXV1));
            if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFLOTEARQUIVOANEXO_NOMEARQ") == 0 )
            {
               AV10TFLoteArquivoAnexo_NomeArq = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFLOTEARQUIVOANEXO_NOMEARQ_SEL") == 0 )
            {
               AV11TFLoteArquivoAnexo_NomeArq_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFTIPODOCUMENTO_NOME") == 0 )
            {
               AV12TFTipoDocumento_Nome = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFTIPODOCUMENTO_NOME_SEL") == 0 )
            {
               AV13TFTipoDocumento_Nome_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFLOTEARQUIVOANEXO_DATA") == 0 )
            {
               AV14TFLoteArquivoAnexo_Data = context.localUtil.CToT( AV32GridStateFilterValue.gxTpr_Value, 2);
               AV15TFLoteArquivoAnexo_Data_To = context.localUtil.CToT( AV32GridStateFilterValue.gxTpr_Valueto, 2);
            }
            AV50GXV1 = (int)(AV50GXV1+1);
         }
         if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(1));
            AV34DynamicFiltersSelector1 = AV33GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "LOTEARQUIVOANEXO_NOMEARQ") == 0 )
            {
               AV35DynamicFiltersOperator1 = AV33GridStateDynamicFilter.gxTpr_Operator;
               AV36LoteArquivoAnexo_NomeArq1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "TIPODOCUMENTO_NOME") == 0 )
            {
               AV35DynamicFiltersOperator1 = AV33GridStateDynamicFilter.gxTpr_Operator;
               AV37TipoDocumento_Nome1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV38DynamicFiltersEnabled2 = true;
               AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(2));
               AV39DynamicFiltersSelector2 = AV33GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "LOTEARQUIVOANEXO_NOMEARQ") == 0 )
               {
                  AV40DynamicFiltersOperator2 = AV33GridStateDynamicFilter.gxTpr_Operator;
                  AV41LoteArquivoAnexo_NomeArq2 = AV33GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "TIPODOCUMENTO_NOME") == 0 )
               {
                  AV40DynamicFiltersOperator2 = AV33GridStateDynamicFilter.gxTpr_Operator;
                  AV42TipoDocumento_Nome2 = AV33GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV43DynamicFiltersEnabled3 = true;
                  AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(3));
                  AV44DynamicFiltersSelector3 = AV33GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV44DynamicFiltersSelector3, "LOTEARQUIVOANEXO_NOMEARQ") == 0 )
                  {
                     AV45DynamicFiltersOperator3 = AV33GridStateDynamicFilter.gxTpr_Operator;
                     AV46LoteArquivoAnexo_NomeArq3 = AV33GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV44DynamicFiltersSelector3, "TIPODOCUMENTO_NOME") == 0 )
                  {
                     AV45DynamicFiltersOperator3 = AV33GridStateDynamicFilter.gxTpr_Operator;
                     AV47TipoDocumento_Nome3 = AV33GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADLOTEARQUIVOANEXO_NOMEARQOPTIONS' Routine */
         AV10TFLoteArquivoAnexo_NomeArq = AV16SearchTxt;
         AV11TFLoteArquivoAnexo_NomeArq_Sel = "";
         AV52WWLoteArquivoAnexoDS_1_Dynamicfiltersselector1 = AV34DynamicFiltersSelector1;
         AV53WWLoteArquivoAnexoDS_2_Dynamicfiltersoperator1 = AV35DynamicFiltersOperator1;
         AV54WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1 = AV36LoteArquivoAnexo_NomeArq1;
         AV55WWLoteArquivoAnexoDS_4_Tipodocumento_nome1 = AV37TipoDocumento_Nome1;
         AV56WWLoteArquivoAnexoDS_5_Dynamicfiltersenabled2 = AV38DynamicFiltersEnabled2;
         AV57WWLoteArquivoAnexoDS_6_Dynamicfiltersselector2 = AV39DynamicFiltersSelector2;
         AV58WWLoteArquivoAnexoDS_7_Dynamicfiltersoperator2 = AV40DynamicFiltersOperator2;
         AV59WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2 = AV41LoteArquivoAnexo_NomeArq2;
         AV60WWLoteArquivoAnexoDS_9_Tipodocumento_nome2 = AV42TipoDocumento_Nome2;
         AV61WWLoteArquivoAnexoDS_10_Dynamicfiltersenabled3 = AV43DynamicFiltersEnabled3;
         AV62WWLoteArquivoAnexoDS_11_Dynamicfiltersselector3 = AV44DynamicFiltersSelector3;
         AV63WWLoteArquivoAnexoDS_12_Dynamicfiltersoperator3 = AV45DynamicFiltersOperator3;
         AV64WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3 = AV46LoteArquivoAnexo_NomeArq3;
         AV65WWLoteArquivoAnexoDS_14_Tipodocumento_nome3 = AV47TipoDocumento_Nome3;
         AV66WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq = AV10TFLoteArquivoAnexo_NomeArq;
         AV67WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel = AV11TFLoteArquivoAnexo_NomeArq_Sel;
         AV68WWLoteArquivoAnexoDS_17_Tftipodocumento_nome = AV12TFTipoDocumento_Nome;
         AV69WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel = AV13TFTipoDocumento_Nome_Sel;
         AV70WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data = AV14TFLoteArquivoAnexo_Data;
         AV71WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to = AV15TFLoteArquivoAnexo_Data_To;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV52WWLoteArquivoAnexoDS_1_Dynamicfiltersselector1 ,
                                              AV53WWLoteArquivoAnexoDS_2_Dynamicfiltersoperator1 ,
                                              AV54WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1 ,
                                              AV55WWLoteArquivoAnexoDS_4_Tipodocumento_nome1 ,
                                              AV56WWLoteArquivoAnexoDS_5_Dynamicfiltersenabled2 ,
                                              AV57WWLoteArquivoAnexoDS_6_Dynamicfiltersselector2 ,
                                              AV58WWLoteArquivoAnexoDS_7_Dynamicfiltersoperator2 ,
                                              AV59WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2 ,
                                              AV60WWLoteArquivoAnexoDS_9_Tipodocumento_nome2 ,
                                              AV61WWLoteArquivoAnexoDS_10_Dynamicfiltersenabled3 ,
                                              AV62WWLoteArquivoAnexoDS_11_Dynamicfiltersselector3 ,
                                              AV63WWLoteArquivoAnexoDS_12_Dynamicfiltersoperator3 ,
                                              AV64WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3 ,
                                              AV65WWLoteArquivoAnexoDS_14_Tipodocumento_nome3 ,
                                              AV67WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel ,
                                              AV66WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq ,
                                              AV69WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel ,
                                              AV68WWLoteArquivoAnexoDS_17_Tftipodocumento_nome ,
                                              AV70WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data ,
                                              AV71WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to ,
                                              A839LoteArquivoAnexo_NomeArq ,
                                              A646TipoDocumento_Nome ,
                                              A836LoteArquivoAnexo_Data },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE
                                              }
         });
         lV54WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1 = StringUtil.PadR( StringUtil.RTrim( AV54WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1), 50, "%");
         lV54WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1 = StringUtil.PadR( StringUtil.RTrim( AV54WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1), 50, "%");
         lV55WWLoteArquivoAnexoDS_4_Tipodocumento_nome1 = StringUtil.PadR( StringUtil.RTrim( AV55WWLoteArquivoAnexoDS_4_Tipodocumento_nome1), 50, "%");
         lV55WWLoteArquivoAnexoDS_4_Tipodocumento_nome1 = StringUtil.PadR( StringUtil.RTrim( AV55WWLoteArquivoAnexoDS_4_Tipodocumento_nome1), 50, "%");
         lV59WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2 = StringUtil.PadR( StringUtil.RTrim( AV59WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2), 50, "%");
         lV59WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2 = StringUtil.PadR( StringUtil.RTrim( AV59WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2), 50, "%");
         lV60WWLoteArquivoAnexoDS_9_Tipodocumento_nome2 = StringUtil.PadR( StringUtil.RTrim( AV60WWLoteArquivoAnexoDS_9_Tipodocumento_nome2), 50, "%");
         lV60WWLoteArquivoAnexoDS_9_Tipodocumento_nome2 = StringUtil.PadR( StringUtil.RTrim( AV60WWLoteArquivoAnexoDS_9_Tipodocumento_nome2), 50, "%");
         lV64WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3 = StringUtil.PadR( StringUtil.RTrim( AV64WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3), 50, "%");
         lV64WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3 = StringUtil.PadR( StringUtil.RTrim( AV64WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3), 50, "%");
         lV65WWLoteArquivoAnexoDS_14_Tipodocumento_nome3 = StringUtil.PadR( StringUtil.RTrim( AV65WWLoteArquivoAnexoDS_14_Tipodocumento_nome3), 50, "%");
         lV65WWLoteArquivoAnexoDS_14_Tipodocumento_nome3 = StringUtil.PadR( StringUtil.RTrim( AV65WWLoteArquivoAnexoDS_14_Tipodocumento_nome3), 50, "%");
         lV66WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq = StringUtil.PadR( StringUtil.RTrim( AV66WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq), 50, "%");
         lV68WWLoteArquivoAnexoDS_17_Tftipodocumento_nome = StringUtil.PadR( StringUtil.RTrim( AV68WWLoteArquivoAnexoDS_17_Tftipodocumento_nome), 50, "%");
         /* Using cursor P00OJ2 */
         pr_default.execute(0, new Object[] {lV54WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1, lV54WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1, lV55WWLoteArquivoAnexoDS_4_Tipodocumento_nome1, lV55WWLoteArquivoAnexoDS_4_Tipodocumento_nome1, lV59WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2, lV59WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2, lV60WWLoteArquivoAnexoDS_9_Tipodocumento_nome2, lV60WWLoteArquivoAnexoDS_9_Tipodocumento_nome2, lV64WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3, lV64WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3, lV65WWLoteArquivoAnexoDS_14_Tipodocumento_nome3, lV65WWLoteArquivoAnexoDS_14_Tipodocumento_nome3, lV66WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq, AV67WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel, lV68WWLoteArquivoAnexoDS_17_Tftipodocumento_nome, AV69WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel, AV70WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data, AV71WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKOJ2 = false;
            A645TipoDocumento_Codigo = P00OJ2_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = P00OJ2_n645TipoDocumento_Codigo[0];
            A839LoteArquivoAnexo_NomeArq = P00OJ2_A839LoteArquivoAnexo_NomeArq[0];
            n839LoteArquivoAnexo_NomeArq = P00OJ2_n839LoteArquivoAnexo_NomeArq[0];
            A836LoteArquivoAnexo_Data = P00OJ2_A836LoteArquivoAnexo_Data[0];
            A646TipoDocumento_Nome = P00OJ2_A646TipoDocumento_Nome[0];
            A841LoteArquivoAnexo_LoteCod = P00OJ2_A841LoteArquivoAnexo_LoteCod[0];
            A646TipoDocumento_Nome = P00OJ2_A646TipoDocumento_Nome[0];
            AV28count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00OJ2_A839LoteArquivoAnexo_NomeArq[0], A839LoteArquivoAnexo_NomeArq) == 0 ) )
            {
               BRKOJ2 = false;
               A836LoteArquivoAnexo_Data = P00OJ2_A836LoteArquivoAnexo_Data[0];
               A841LoteArquivoAnexo_LoteCod = P00OJ2_A841LoteArquivoAnexo_LoteCod[0];
               AV28count = (long)(AV28count+1);
               BRKOJ2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A839LoteArquivoAnexo_NomeArq)) )
            {
               AV20Option = A839LoteArquivoAnexo_NomeArq;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKOJ2 )
            {
               BRKOJ2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADTIPODOCUMENTO_NOMEOPTIONS' Routine */
         AV12TFTipoDocumento_Nome = AV16SearchTxt;
         AV13TFTipoDocumento_Nome_Sel = "";
         AV52WWLoteArquivoAnexoDS_1_Dynamicfiltersselector1 = AV34DynamicFiltersSelector1;
         AV53WWLoteArquivoAnexoDS_2_Dynamicfiltersoperator1 = AV35DynamicFiltersOperator1;
         AV54WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1 = AV36LoteArquivoAnexo_NomeArq1;
         AV55WWLoteArquivoAnexoDS_4_Tipodocumento_nome1 = AV37TipoDocumento_Nome1;
         AV56WWLoteArquivoAnexoDS_5_Dynamicfiltersenabled2 = AV38DynamicFiltersEnabled2;
         AV57WWLoteArquivoAnexoDS_6_Dynamicfiltersselector2 = AV39DynamicFiltersSelector2;
         AV58WWLoteArquivoAnexoDS_7_Dynamicfiltersoperator2 = AV40DynamicFiltersOperator2;
         AV59WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2 = AV41LoteArquivoAnexo_NomeArq2;
         AV60WWLoteArquivoAnexoDS_9_Tipodocumento_nome2 = AV42TipoDocumento_Nome2;
         AV61WWLoteArquivoAnexoDS_10_Dynamicfiltersenabled3 = AV43DynamicFiltersEnabled3;
         AV62WWLoteArquivoAnexoDS_11_Dynamicfiltersselector3 = AV44DynamicFiltersSelector3;
         AV63WWLoteArquivoAnexoDS_12_Dynamicfiltersoperator3 = AV45DynamicFiltersOperator3;
         AV64WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3 = AV46LoteArquivoAnexo_NomeArq3;
         AV65WWLoteArquivoAnexoDS_14_Tipodocumento_nome3 = AV47TipoDocumento_Nome3;
         AV66WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq = AV10TFLoteArquivoAnexo_NomeArq;
         AV67WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel = AV11TFLoteArquivoAnexo_NomeArq_Sel;
         AV68WWLoteArquivoAnexoDS_17_Tftipodocumento_nome = AV12TFTipoDocumento_Nome;
         AV69WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel = AV13TFTipoDocumento_Nome_Sel;
         AV70WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data = AV14TFLoteArquivoAnexo_Data;
         AV71WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to = AV15TFLoteArquivoAnexo_Data_To;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV52WWLoteArquivoAnexoDS_1_Dynamicfiltersselector1 ,
                                              AV53WWLoteArquivoAnexoDS_2_Dynamicfiltersoperator1 ,
                                              AV54WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1 ,
                                              AV55WWLoteArquivoAnexoDS_4_Tipodocumento_nome1 ,
                                              AV56WWLoteArquivoAnexoDS_5_Dynamicfiltersenabled2 ,
                                              AV57WWLoteArquivoAnexoDS_6_Dynamicfiltersselector2 ,
                                              AV58WWLoteArquivoAnexoDS_7_Dynamicfiltersoperator2 ,
                                              AV59WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2 ,
                                              AV60WWLoteArquivoAnexoDS_9_Tipodocumento_nome2 ,
                                              AV61WWLoteArquivoAnexoDS_10_Dynamicfiltersenabled3 ,
                                              AV62WWLoteArquivoAnexoDS_11_Dynamicfiltersselector3 ,
                                              AV63WWLoteArquivoAnexoDS_12_Dynamicfiltersoperator3 ,
                                              AV64WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3 ,
                                              AV65WWLoteArquivoAnexoDS_14_Tipodocumento_nome3 ,
                                              AV67WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel ,
                                              AV66WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq ,
                                              AV69WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel ,
                                              AV68WWLoteArquivoAnexoDS_17_Tftipodocumento_nome ,
                                              AV70WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data ,
                                              AV71WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to ,
                                              A839LoteArquivoAnexo_NomeArq ,
                                              A646TipoDocumento_Nome ,
                                              A836LoteArquivoAnexo_Data },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE
                                              }
         });
         lV54WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1 = StringUtil.PadR( StringUtil.RTrim( AV54WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1), 50, "%");
         lV54WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1 = StringUtil.PadR( StringUtil.RTrim( AV54WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1), 50, "%");
         lV55WWLoteArquivoAnexoDS_4_Tipodocumento_nome1 = StringUtil.PadR( StringUtil.RTrim( AV55WWLoteArquivoAnexoDS_4_Tipodocumento_nome1), 50, "%");
         lV55WWLoteArquivoAnexoDS_4_Tipodocumento_nome1 = StringUtil.PadR( StringUtil.RTrim( AV55WWLoteArquivoAnexoDS_4_Tipodocumento_nome1), 50, "%");
         lV59WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2 = StringUtil.PadR( StringUtil.RTrim( AV59WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2), 50, "%");
         lV59WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2 = StringUtil.PadR( StringUtil.RTrim( AV59WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2), 50, "%");
         lV60WWLoteArquivoAnexoDS_9_Tipodocumento_nome2 = StringUtil.PadR( StringUtil.RTrim( AV60WWLoteArquivoAnexoDS_9_Tipodocumento_nome2), 50, "%");
         lV60WWLoteArquivoAnexoDS_9_Tipodocumento_nome2 = StringUtil.PadR( StringUtil.RTrim( AV60WWLoteArquivoAnexoDS_9_Tipodocumento_nome2), 50, "%");
         lV64WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3 = StringUtil.PadR( StringUtil.RTrim( AV64WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3), 50, "%");
         lV64WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3 = StringUtil.PadR( StringUtil.RTrim( AV64WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3), 50, "%");
         lV65WWLoteArquivoAnexoDS_14_Tipodocumento_nome3 = StringUtil.PadR( StringUtil.RTrim( AV65WWLoteArquivoAnexoDS_14_Tipodocumento_nome3), 50, "%");
         lV65WWLoteArquivoAnexoDS_14_Tipodocumento_nome3 = StringUtil.PadR( StringUtil.RTrim( AV65WWLoteArquivoAnexoDS_14_Tipodocumento_nome3), 50, "%");
         lV66WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq = StringUtil.PadR( StringUtil.RTrim( AV66WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq), 50, "%");
         lV68WWLoteArquivoAnexoDS_17_Tftipodocumento_nome = StringUtil.PadR( StringUtil.RTrim( AV68WWLoteArquivoAnexoDS_17_Tftipodocumento_nome), 50, "%");
         /* Using cursor P00OJ3 */
         pr_default.execute(1, new Object[] {lV54WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1, lV54WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1, lV55WWLoteArquivoAnexoDS_4_Tipodocumento_nome1, lV55WWLoteArquivoAnexoDS_4_Tipodocumento_nome1, lV59WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2, lV59WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2, lV60WWLoteArquivoAnexoDS_9_Tipodocumento_nome2, lV60WWLoteArquivoAnexoDS_9_Tipodocumento_nome2, lV64WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3, lV64WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3, lV65WWLoteArquivoAnexoDS_14_Tipodocumento_nome3, lV65WWLoteArquivoAnexoDS_14_Tipodocumento_nome3, lV66WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq, AV67WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel, lV68WWLoteArquivoAnexoDS_17_Tftipodocumento_nome, AV69WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel, AV70WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data, AV71WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKOJ4 = false;
            A645TipoDocumento_Codigo = P00OJ3_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = P00OJ3_n645TipoDocumento_Codigo[0];
            A836LoteArquivoAnexo_Data = P00OJ3_A836LoteArquivoAnexo_Data[0];
            A646TipoDocumento_Nome = P00OJ3_A646TipoDocumento_Nome[0];
            A839LoteArquivoAnexo_NomeArq = P00OJ3_A839LoteArquivoAnexo_NomeArq[0];
            n839LoteArquivoAnexo_NomeArq = P00OJ3_n839LoteArquivoAnexo_NomeArq[0];
            A841LoteArquivoAnexo_LoteCod = P00OJ3_A841LoteArquivoAnexo_LoteCod[0];
            A646TipoDocumento_Nome = P00OJ3_A646TipoDocumento_Nome[0];
            AV28count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( P00OJ3_A645TipoDocumento_Codigo[0] == A645TipoDocumento_Codigo ) )
            {
               BRKOJ4 = false;
               A836LoteArquivoAnexo_Data = P00OJ3_A836LoteArquivoAnexo_Data[0];
               A841LoteArquivoAnexo_LoteCod = P00OJ3_A841LoteArquivoAnexo_LoteCod[0];
               AV28count = (long)(AV28count+1);
               BRKOJ4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A646TipoDocumento_Nome)) )
            {
               AV20Option = A646TipoDocumento_Nome;
               AV19InsertIndex = 1;
               while ( ( AV19InsertIndex <= AV21Options.Count ) && ( StringUtil.StrCmp(((String)AV21Options.Item(AV19InsertIndex)), AV20Option) < 0 ) )
               {
                  AV19InsertIndex = (int)(AV19InsertIndex+1);
               }
               AV21Options.Add(AV20Option, AV19InsertIndex);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), AV19InsertIndex);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKOJ4 )
            {
               BRKOJ4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV21Options = new GxSimpleCollection();
         AV24OptionsDesc = new GxSimpleCollection();
         AV26OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV29Session = context.GetSession();
         AV31GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV32GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFLoteArquivoAnexo_NomeArq = "";
         AV11TFLoteArquivoAnexo_NomeArq_Sel = "";
         AV12TFTipoDocumento_Nome = "";
         AV13TFTipoDocumento_Nome_Sel = "";
         AV14TFLoteArquivoAnexo_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         AV15TFLoteArquivoAnexo_Data_To = DateTimeUtil.ServerNow( context, "DEFAULT");
         AV33GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV34DynamicFiltersSelector1 = "";
         AV36LoteArquivoAnexo_NomeArq1 = "";
         AV37TipoDocumento_Nome1 = "";
         AV39DynamicFiltersSelector2 = "";
         AV41LoteArquivoAnexo_NomeArq2 = "";
         AV42TipoDocumento_Nome2 = "";
         AV44DynamicFiltersSelector3 = "";
         AV46LoteArquivoAnexo_NomeArq3 = "";
         AV47TipoDocumento_Nome3 = "";
         AV52WWLoteArquivoAnexoDS_1_Dynamicfiltersselector1 = "";
         AV54WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1 = "";
         AV55WWLoteArquivoAnexoDS_4_Tipodocumento_nome1 = "";
         AV57WWLoteArquivoAnexoDS_6_Dynamicfiltersselector2 = "";
         AV59WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2 = "";
         AV60WWLoteArquivoAnexoDS_9_Tipodocumento_nome2 = "";
         AV62WWLoteArquivoAnexoDS_11_Dynamicfiltersselector3 = "";
         AV64WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3 = "";
         AV65WWLoteArquivoAnexoDS_14_Tipodocumento_nome3 = "";
         AV66WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq = "";
         AV67WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel = "";
         AV68WWLoteArquivoAnexoDS_17_Tftipodocumento_nome = "";
         AV69WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel = "";
         AV70WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data = (DateTime)(DateTime.MinValue);
         AV71WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to = (DateTime)(DateTime.MinValue);
         scmdbuf = "";
         lV54WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1 = "";
         lV55WWLoteArquivoAnexoDS_4_Tipodocumento_nome1 = "";
         lV59WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2 = "";
         lV60WWLoteArquivoAnexoDS_9_Tipodocumento_nome2 = "";
         lV64WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3 = "";
         lV65WWLoteArquivoAnexoDS_14_Tipodocumento_nome3 = "";
         lV66WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq = "";
         lV68WWLoteArquivoAnexoDS_17_Tftipodocumento_nome = "";
         A839LoteArquivoAnexo_NomeArq = "";
         A646TipoDocumento_Nome = "";
         A836LoteArquivoAnexo_Data = (DateTime)(DateTime.MinValue);
         P00OJ2_A645TipoDocumento_Codigo = new int[1] ;
         P00OJ2_n645TipoDocumento_Codigo = new bool[] {false} ;
         P00OJ2_A839LoteArquivoAnexo_NomeArq = new String[] {""} ;
         P00OJ2_n839LoteArquivoAnexo_NomeArq = new bool[] {false} ;
         P00OJ2_A836LoteArquivoAnexo_Data = new DateTime[] {DateTime.MinValue} ;
         P00OJ2_A646TipoDocumento_Nome = new String[] {""} ;
         P00OJ2_A841LoteArquivoAnexo_LoteCod = new int[1] ;
         AV20Option = "";
         P00OJ3_A645TipoDocumento_Codigo = new int[1] ;
         P00OJ3_n645TipoDocumento_Codigo = new bool[] {false} ;
         P00OJ3_A836LoteArquivoAnexo_Data = new DateTime[] {DateTime.MinValue} ;
         P00OJ3_A646TipoDocumento_Nome = new String[] {""} ;
         P00OJ3_A839LoteArquivoAnexo_NomeArq = new String[] {""} ;
         P00OJ3_n839LoteArquivoAnexo_NomeArq = new bool[] {false} ;
         P00OJ3_A841LoteArquivoAnexo_LoteCod = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwlotearquivoanexofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00OJ2_A645TipoDocumento_Codigo, P00OJ2_n645TipoDocumento_Codigo, P00OJ2_A839LoteArquivoAnexo_NomeArq, P00OJ2_n839LoteArquivoAnexo_NomeArq, P00OJ2_A836LoteArquivoAnexo_Data, P00OJ2_A646TipoDocumento_Nome, P00OJ2_A841LoteArquivoAnexo_LoteCod
               }
               , new Object[] {
               P00OJ3_A645TipoDocumento_Codigo, P00OJ3_n645TipoDocumento_Codigo, P00OJ3_A836LoteArquivoAnexo_Data, P00OJ3_A646TipoDocumento_Nome, P00OJ3_A839LoteArquivoAnexo_NomeArq, P00OJ3_n839LoteArquivoAnexo_NomeArq, P00OJ3_A841LoteArquivoAnexo_LoteCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV35DynamicFiltersOperator1 ;
      private short AV40DynamicFiltersOperator2 ;
      private short AV45DynamicFiltersOperator3 ;
      private short AV53WWLoteArquivoAnexoDS_2_Dynamicfiltersoperator1 ;
      private short AV58WWLoteArquivoAnexoDS_7_Dynamicfiltersoperator2 ;
      private short AV63WWLoteArquivoAnexoDS_12_Dynamicfiltersoperator3 ;
      private int AV50GXV1 ;
      private int A645TipoDocumento_Codigo ;
      private int A841LoteArquivoAnexo_LoteCod ;
      private int AV19InsertIndex ;
      private long AV28count ;
      private String AV10TFLoteArquivoAnexo_NomeArq ;
      private String AV11TFLoteArquivoAnexo_NomeArq_Sel ;
      private String AV12TFTipoDocumento_Nome ;
      private String AV13TFTipoDocumento_Nome_Sel ;
      private String AV36LoteArquivoAnexo_NomeArq1 ;
      private String AV37TipoDocumento_Nome1 ;
      private String AV41LoteArquivoAnexo_NomeArq2 ;
      private String AV42TipoDocumento_Nome2 ;
      private String AV46LoteArquivoAnexo_NomeArq3 ;
      private String AV47TipoDocumento_Nome3 ;
      private String AV54WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1 ;
      private String AV55WWLoteArquivoAnexoDS_4_Tipodocumento_nome1 ;
      private String AV59WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2 ;
      private String AV60WWLoteArquivoAnexoDS_9_Tipodocumento_nome2 ;
      private String AV64WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3 ;
      private String AV65WWLoteArquivoAnexoDS_14_Tipodocumento_nome3 ;
      private String AV66WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq ;
      private String AV67WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel ;
      private String AV68WWLoteArquivoAnexoDS_17_Tftipodocumento_nome ;
      private String AV69WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel ;
      private String scmdbuf ;
      private String lV54WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1 ;
      private String lV55WWLoteArquivoAnexoDS_4_Tipodocumento_nome1 ;
      private String lV59WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2 ;
      private String lV60WWLoteArquivoAnexoDS_9_Tipodocumento_nome2 ;
      private String lV64WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3 ;
      private String lV65WWLoteArquivoAnexoDS_14_Tipodocumento_nome3 ;
      private String lV66WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq ;
      private String lV68WWLoteArquivoAnexoDS_17_Tftipodocumento_nome ;
      private String A839LoteArquivoAnexo_NomeArq ;
      private String A646TipoDocumento_Nome ;
      private DateTime AV14TFLoteArquivoAnexo_Data ;
      private DateTime AV15TFLoteArquivoAnexo_Data_To ;
      private DateTime AV70WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data ;
      private DateTime AV71WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to ;
      private DateTime A836LoteArquivoAnexo_Data ;
      private bool returnInSub ;
      private bool AV38DynamicFiltersEnabled2 ;
      private bool AV43DynamicFiltersEnabled3 ;
      private bool AV56WWLoteArquivoAnexoDS_5_Dynamicfiltersenabled2 ;
      private bool AV61WWLoteArquivoAnexoDS_10_Dynamicfiltersenabled3 ;
      private bool BRKOJ2 ;
      private bool n645TipoDocumento_Codigo ;
      private bool n839LoteArquivoAnexo_NomeArq ;
      private bool BRKOJ4 ;
      private String AV27OptionIndexesJson ;
      private String AV22OptionsJson ;
      private String AV25OptionsDescJson ;
      private String AV18DDOName ;
      private String AV16SearchTxt ;
      private String AV17SearchTxtTo ;
      private String AV34DynamicFiltersSelector1 ;
      private String AV39DynamicFiltersSelector2 ;
      private String AV44DynamicFiltersSelector3 ;
      private String AV52WWLoteArquivoAnexoDS_1_Dynamicfiltersselector1 ;
      private String AV57WWLoteArquivoAnexoDS_6_Dynamicfiltersselector2 ;
      private String AV62WWLoteArquivoAnexoDS_11_Dynamicfiltersselector3 ;
      private String AV20Option ;
      private IGxSession AV29Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00OJ2_A645TipoDocumento_Codigo ;
      private bool[] P00OJ2_n645TipoDocumento_Codigo ;
      private String[] P00OJ2_A839LoteArquivoAnexo_NomeArq ;
      private bool[] P00OJ2_n839LoteArquivoAnexo_NomeArq ;
      private DateTime[] P00OJ2_A836LoteArquivoAnexo_Data ;
      private String[] P00OJ2_A646TipoDocumento_Nome ;
      private int[] P00OJ2_A841LoteArquivoAnexo_LoteCod ;
      private int[] P00OJ3_A645TipoDocumento_Codigo ;
      private bool[] P00OJ3_n645TipoDocumento_Codigo ;
      private DateTime[] P00OJ3_A836LoteArquivoAnexo_Data ;
      private String[] P00OJ3_A646TipoDocumento_Nome ;
      private String[] P00OJ3_A839LoteArquivoAnexo_NomeArq ;
      private bool[] P00OJ3_n839LoteArquivoAnexo_NomeArq ;
      private int[] P00OJ3_A841LoteArquivoAnexo_LoteCod ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV31GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV32GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV33GridStateDynamicFilter ;
   }

   public class getwwlotearquivoanexofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00OJ2( IGxContext context ,
                                             String AV52WWLoteArquivoAnexoDS_1_Dynamicfiltersselector1 ,
                                             short AV53WWLoteArquivoAnexoDS_2_Dynamicfiltersoperator1 ,
                                             String AV54WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1 ,
                                             String AV55WWLoteArquivoAnexoDS_4_Tipodocumento_nome1 ,
                                             bool AV56WWLoteArquivoAnexoDS_5_Dynamicfiltersenabled2 ,
                                             String AV57WWLoteArquivoAnexoDS_6_Dynamicfiltersselector2 ,
                                             short AV58WWLoteArquivoAnexoDS_7_Dynamicfiltersoperator2 ,
                                             String AV59WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2 ,
                                             String AV60WWLoteArquivoAnexoDS_9_Tipodocumento_nome2 ,
                                             bool AV61WWLoteArquivoAnexoDS_10_Dynamicfiltersenabled3 ,
                                             String AV62WWLoteArquivoAnexoDS_11_Dynamicfiltersselector3 ,
                                             short AV63WWLoteArquivoAnexoDS_12_Dynamicfiltersoperator3 ,
                                             String AV64WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3 ,
                                             String AV65WWLoteArquivoAnexoDS_14_Tipodocumento_nome3 ,
                                             String AV67WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel ,
                                             String AV66WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq ,
                                             String AV69WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel ,
                                             String AV68WWLoteArquivoAnexoDS_17_Tftipodocumento_nome ,
                                             DateTime AV70WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data ,
                                             DateTime AV71WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to ,
                                             String A839LoteArquivoAnexo_NomeArq ,
                                             String A646TipoDocumento_Nome ,
                                             DateTime A836LoteArquivoAnexo_Data )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [18] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[TipoDocumento_Codigo], T1.[LoteArquivoAnexo_NomeArq], T1.[LoteArquivoAnexo_Data], T2.[TipoDocumento_Nome], T1.[LoteArquivoAnexo_LoteCod] FROM ([LoteArquivoAnexo] T1 WITH (NOLOCK) LEFT JOIN [TipoDocumento] T2 WITH (NOLOCK) ON T2.[TipoDocumento_Codigo] = T1.[TipoDocumento_Codigo])";
         if ( ( StringUtil.StrCmp(AV52WWLoteArquivoAnexoDS_1_Dynamicfiltersselector1, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV53WWLoteArquivoAnexoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV54WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV54WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWLoteArquivoAnexoDS_1_Dynamicfiltersselector1, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV53WWLoteArquivoAnexoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV54WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV54WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWLoteArquivoAnexoDS_1_Dynamicfiltersselector1, "TIPODOCUMENTO_NOME") == 0 ) && ( AV53WWLoteArquivoAnexoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWLoteArquivoAnexoDS_4_Tipodocumento_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV55WWLoteArquivoAnexoDS_4_Tipodocumento_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV55WWLoteArquivoAnexoDS_4_Tipodocumento_nome1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWLoteArquivoAnexoDS_1_Dynamicfiltersselector1, "TIPODOCUMENTO_NOME") == 0 ) && ( AV53WWLoteArquivoAnexoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWLoteArquivoAnexoDS_4_Tipodocumento_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like '%' + @lV55WWLoteArquivoAnexoDS_4_Tipodocumento_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like '%' + @lV55WWLoteArquivoAnexoDS_4_Tipodocumento_nome1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV56WWLoteArquivoAnexoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV57WWLoteArquivoAnexoDS_6_Dynamicfiltersselector2, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV58WWLoteArquivoAnexoDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV59WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV59WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV56WWLoteArquivoAnexoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV57WWLoteArquivoAnexoDS_6_Dynamicfiltersselector2, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV58WWLoteArquivoAnexoDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV59WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV59WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV56WWLoteArquivoAnexoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV57WWLoteArquivoAnexoDS_6_Dynamicfiltersselector2, "TIPODOCUMENTO_NOME") == 0 ) && ( AV58WWLoteArquivoAnexoDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWLoteArquivoAnexoDS_9_Tipodocumento_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV60WWLoteArquivoAnexoDS_9_Tipodocumento_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV60WWLoteArquivoAnexoDS_9_Tipodocumento_nome2)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV56WWLoteArquivoAnexoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV57WWLoteArquivoAnexoDS_6_Dynamicfiltersselector2, "TIPODOCUMENTO_NOME") == 0 ) && ( AV58WWLoteArquivoAnexoDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWLoteArquivoAnexoDS_9_Tipodocumento_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like '%' + @lV60WWLoteArquivoAnexoDS_9_Tipodocumento_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like '%' + @lV60WWLoteArquivoAnexoDS_9_Tipodocumento_nome2)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV61WWLoteArquivoAnexoDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV62WWLoteArquivoAnexoDS_11_Dynamicfiltersselector3, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV63WWLoteArquivoAnexoDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV64WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV64WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV61WWLoteArquivoAnexoDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV62WWLoteArquivoAnexoDS_11_Dynamicfiltersselector3, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV63WWLoteArquivoAnexoDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV64WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV64WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV61WWLoteArquivoAnexoDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV62WWLoteArquivoAnexoDS_11_Dynamicfiltersselector3, "TIPODOCUMENTO_NOME") == 0 ) && ( AV63WWLoteArquivoAnexoDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWLoteArquivoAnexoDS_14_Tipodocumento_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV65WWLoteArquivoAnexoDS_14_Tipodocumento_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV65WWLoteArquivoAnexoDS_14_Tipodocumento_nome3)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV61WWLoteArquivoAnexoDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV62WWLoteArquivoAnexoDS_11_Dynamicfiltersselector3, "TIPODOCUMENTO_NOME") == 0 ) && ( AV63WWLoteArquivoAnexoDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWLoteArquivoAnexoDS_14_Tipodocumento_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like '%' + @lV65WWLoteArquivoAnexoDS_14_Tipodocumento_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like '%' + @lV65WWLoteArquivoAnexoDS_14_Tipodocumento_nome3)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV67WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV66WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV66WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] = @AV67WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] = @AV67WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV69WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWLoteArquivoAnexoDS_17_Tftipodocumento_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV68WWLoteArquivoAnexoDS_17_Tftipodocumento_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV68WWLoteArquivoAnexoDS_17_Tftipodocumento_nome)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] = @AV69WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] = @AV69WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV70WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_Data] >= @AV70WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_Data] >= @AV70WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ! (DateTime.MinValue==AV71WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_Data] <= @AV71WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_Data] <= @AV71WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[LoteArquivoAnexo_NomeArq]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00OJ3( IGxContext context ,
                                             String AV52WWLoteArquivoAnexoDS_1_Dynamicfiltersselector1 ,
                                             short AV53WWLoteArquivoAnexoDS_2_Dynamicfiltersoperator1 ,
                                             String AV54WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1 ,
                                             String AV55WWLoteArquivoAnexoDS_4_Tipodocumento_nome1 ,
                                             bool AV56WWLoteArquivoAnexoDS_5_Dynamicfiltersenabled2 ,
                                             String AV57WWLoteArquivoAnexoDS_6_Dynamicfiltersselector2 ,
                                             short AV58WWLoteArquivoAnexoDS_7_Dynamicfiltersoperator2 ,
                                             String AV59WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2 ,
                                             String AV60WWLoteArquivoAnexoDS_9_Tipodocumento_nome2 ,
                                             bool AV61WWLoteArquivoAnexoDS_10_Dynamicfiltersenabled3 ,
                                             String AV62WWLoteArquivoAnexoDS_11_Dynamicfiltersselector3 ,
                                             short AV63WWLoteArquivoAnexoDS_12_Dynamicfiltersoperator3 ,
                                             String AV64WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3 ,
                                             String AV65WWLoteArquivoAnexoDS_14_Tipodocumento_nome3 ,
                                             String AV67WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel ,
                                             String AV66WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq ,
                                             String AV69WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel ,
                                             String AV68WWLoteArquivoAnexoDS_17_Tftipodocumento_nome ,
                                             DateTime AV70WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data ,
                                             DateTime AV71WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to ,
                                             String A839LoteArquivoAnexo_NomeArq ,
                                             String A646TipoDocumento_Nome ,
                                             DateTime A836LoteArquivoAnexo_Data )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [18] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[TipoDocumento_Codigo], T1.[LoteArquivoAnexo_Data], T2.[TipoDocumento_Nome], T1.[LoteArquivoAnexo_NomeArq], T1.[LoteArquivoAnexo_LoteCod] FROM ([LoteArquivoAnexo] T1 WITH (NOLOCK) LEFT JOIN [TipoDocumento] T2 WITH (NOLOCK) ON T2.[TipoDocumento_Codigo] = T1.[TipoDocumento_Codigo])";
         if ( ( StringUtil.StrCmp(AV52WWLoteArquivoAnexoDS_1_Dynamicfiltersselector1, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV53WWLoteArquivoAnexoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV54WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV54WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWLoteArquivoAnexoDS_1_Dynamicfiltersselector1, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV53WWLoteArquivoAnexoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV54WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV54WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWLoteArquivoAnexoDS_1_Dynamicfiltersselector1, "TIPODOCUMENTO_NOME") == 0 ) && ( AV53WWLoteArquivoAnexoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWLoteArquivoAnexoDS_4_Tipodocumento_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV55WWLoteArquivoAnexoDS_4_Tipodocumento_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV55WWLoteArquivoAnexoDS_4_Tipodocumento_nome1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWLoteArquivoAnexoDS_1_Dynamicfiltersselector1, "TIPODOCUMENTO_NOME") == 0 ) && ( AV53WWLoteArquivoAnexoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWLoteArquivoAnexoDS_4_Tipodocumento_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like '%' + @lV55WWLoteArquivoAnexoDS_4_Tipodocumento_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like '%' + @lV55WWLoteArquivoAnexoDS_4_Tipodocumento_nome1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV56WWLoteArquivoAnexoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV57WWLoteArquivoAnexoDS_6_Dynamicfiltersselector2, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV58WWLoteArquivoAnexoDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV59WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV59WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV56WWLoteArquivoAnexoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV57WWLoteArquivoAnexoDS_6_Dynamicfiltersselector2, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV58WWLoteArquivoAnexoDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV59WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV59WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV56WWLoteArquivoAnexoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV57WWLoteArquivoAnexoDS_6_Dynamicfiltersselector2, "TIPODOCUMENTO_NOME") == 0 ) && ( AV58WWLoteArquivoAnexoDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWLoteArquivoAnexoDS_9_Tipodocumento_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV60WWLoteArquivoAnexoDS_9_Tipodocumento_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV60WWLoteArquivoAnexoDS_9_Tipodocumento_nome2)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV56WWLoteArquivoAnexoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV57WWLoteArquivoAnexoDS_6_Dynamicfiltersselector2, "TIPODOCUMENTO_NOME") == 0 ) && ( AV58WWLoteArquivoAnexoDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWLoteArquivoAnexoDS_9_Tipodocumento_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like '%' + @lV60WWLoteArquivoAnexoDS_9_Tipodocumento_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like '%' + @lV60WWLoteArquivoAnexoDS_9_Tipodocumento_nome2)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV61WWLoteArquivoAnexoDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV62WWLoteArquivoAnexoDS_11_Dynamicfiltersselector3, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV63WWLoteArquivoAnexoDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV64WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV64WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV61WWLoteArquivoAnexoDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV62WWLoteArquivoAnexoDS_11_Dynamicfiltersselector3, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV63WWLoteArquivoAnexoDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV64WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV64WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV61WWLoteArquivoAnexoDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV62WWLoteArquivoAnexoDS_11_Dynamicfiltersselector3, "TIPODOCUMENTO_NOME") == 0 ) && ( AV63WWLoteArquivoAnexoDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWLoteArquivoAnexoDS_14_Tipodocumento_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV65WWLoteArquivoAnexoDS_14_Tipodocumento_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV65WWLoteArquivoAnexoDS_14_Tipodocumento_nome3)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV61WWLoteArquivoAnexoDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV62WWLoteArquivoAnexoDS_11_Dynamicfiltersselector3, "TIPODOCUMENTO_NOME") == 0 ) && ( AV63WWLoteArquivoAnexoDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWLoteArquivoAnexoDS_14_Tipodocumento_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like '%' + @lV65WWLoteArquivoAnexoDS_14_Tipodocumento_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like '%' + @lV65WWLoteArquivoAnexoDS_14_Tipodocumento_nome3)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV67WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV66WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV66WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] = @AV67WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] = @AV67WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV69WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWLoteArquivoAnexoDS_17_Tftipodocumento_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV68WWLoteArquivoAnexoDS_17_Tftipodocumento_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV68WWLoteArquivoAnexoDS_17_Tftipodocumento_nome)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] = @AV69WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] = @AV69WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV70WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_Data] >= @AV70WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_Data] >= @AV70WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( ! (DateTime.MinValue==AV71WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_Data] <= @AV71WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_Data] <= @AV71WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[TipoDocumento_Codigo]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00OJ2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (DateTime)dynConstraints[18] , (DateTime)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (DateTime)dynConstraints[22] );
               case 1 :
                     return conditional_P00OJ3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (DateTime)dynConstraints[18] , (DateTime)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (DateTime)dynConstraints[22] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00OJ2 ;
          prmP00OJ2 = new Object[] {
          new Object[] {"@lV54WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV54WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55WWLoteArquivoAnexoDS_4_Tipodocumento_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55WWLoteArquivoAnexoDS_4_Tipodocumento_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV59WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV59WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60WWLoteArquivoAnexoDS_9_Tipodocumento_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60WWLoteArquivoAnexoDS_9_Tipodocumento_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV64WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV64WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV65WWLoteArquivoAnexoDS_14_Tipodocumento_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV65WWLoteArquivoAnexoDS_14_Tipodocumento_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV67WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV68WWLoteArquivoAnexoDS_17_Tftipodocumento_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV69WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV70WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV71WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmP00OJ3 ;
          prmP00OJ3 = new Object[] {
          new Object[] {"@lV54WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV54WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55WWLoteArquivoAnexoDS_4_Tipodocumento_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55WWLoteArquivoAnexoDS_4_Tipodocumento_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV59WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV59WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60WWLoteArquivoAnexoDS_9_Tipodocumento_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60WWLoteArquivoAnexoDS_9_Tipodocumento_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV64WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV64WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV65WWLoteArquivoAnexoDS_14_Tipodocumento_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV65WWLoteArquivoAnexoDS_14_Tipodocumento_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV67WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV68WWLoteArquivoAnexoDS_17_Tftipodocumento_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV69WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV70WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV71WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to",SqlDbType.DateTime,8,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00OJ2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00OJ2,100,0,true,false )
             ,new CursorDef("P00OJ3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00OJ3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[4])[0] = rslt.getGXDateTime(3) ;
                ((String[]) buf[5])[0] = rslt.getString(4, 50) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[35]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[35]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwlotearquivoanexofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwlotearquivoanexofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwlotearquivoanexofilterdata") )
          {
             return  ;
          }
          getwwlotearquivoanexofilterdata worker = new getwwlotearquivoanexofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
