/*
               File: ContratoServicosGeneral
        Description: Contrato Servicos General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/16/2020 0:11:13.62
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratoservicosgeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contratoservicosgeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contratoservicosgeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratoServicos_Codigo )
      {
         this.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbContratoServicos_PrazoTpDias = new GXCombobox();
         cmbContratoServicos_PrazoInicio = new GXCombobox();
         cmbContratoServicos_PrazoTipo = new GXCombobox();
         cmbContratoServicos_PrazoCorrecaoTipo = new GXCombobox();
         cmbContratoServicos_StatusPagFnc = new GXCombobox();
         chkContratoServicos_NaoRequerAtr = new GXCheckbox();
         cmbContratoServicos_LocalExec = new GXCombobox();
         cmbServicoContrato_Faturamento = new GXCombobox();
         cmbContratoServicos_CalculoRmn = new GXCombobox();
         chkContratoServicos_EspelhaAceite = new GXCheckbox();
         cmbContratoServicos_Momento = new GXCombobox();
         cmbContratoServicos_TipoVnc = new GXCombobox();
         chkContratoServicos_SolicitaGestorSistema = new GXCheckbox();
         chkContratoServicos_Ativo = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A160ContratoServicos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A160ContratoServicos_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA752( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV17Pgmname = "ContratoServicosGeneral";
               context.Gx_err = 0;
               edtavServico_percentual_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavServico_percentual_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_percentual_Enabled), 5, 0)));
               /* Using cursor H00753 */
               pr_default.execute(0, new Object[] {A160ContratoServicos_Codigo});
               if ( (pr_default.getStatus(0) != 101) )
               {
                  A911ContratoServicos_Prazos = H00753_A911ContratoServicos_Prazos[0];
                  n911ContratoServicos_Prazos = H00753_n911ContratoServicos_Prazos[0];
               }
               else
               {
                  A911ContratoServicos_Prazos = 0;
                  n911ContratoServicos_Prazos = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A911ContratoServicos_Prazos", StringUtil.LTrim( StringUtil.Str( (decimal)(A911ContratoServicos_Prazos), 4, 0)));
               }
               pr_default.close(0);
               /* Using cursor H00754 */
               pr_default.execute(1, new Object[] {A160ContratoServicos_Codigo});
               if ( (pr_default.getStatus(1) != 101) )
               {
                  A914ContratoServicos_PrazoDias = H00754_A914ContratoServicos_PrazoDias[0];
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A914ContratoServicos_PrazoDias", StringUtil.LTrim( StringUtil.Str( (decimal)(A914ContratoServicos_PrazoDias), 4, 0)));
                  n914ContratoServicos_PrazoDias = H00754_n914ContratoServicos_PrazoDias[0];
                  A913ContratoServicos_PrazoTipo = H00754_A913ContratoServicos_PrazoTipo[0];
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A913ContratoServicos_PrazoTipo", A913ContratoServicos_PrazoTipo);
                  n913ContratoServicos_PrazoTipo = H00754_n913ContratoServicos_PrazoTipo[0];
               }
               else
               {
                  A913ContratoServicos_PrazoTipo = "";
                  n913ContratoServicos_PrazoTipo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A913ContratoServicos_PrazoTipo", A913ContratoServicos_PrazoTipo);
                  A914ContratoServicos_PrazoDias = 0;
                  n914ContratoServicos_PrazoDias = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A914ContratoServicos_PrazoDias", StringUtil.LTrim( StringUtil.Str( (decimal)(A914ContratoServicos_PrazoDias), 4, 0)));
               }
               pr_default.close(1);
               WS752( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contrato Servicos General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203160111378");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratoservicosgeneral.aspx") + "?" + UrlEncode("" +A160ContratoServicos_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA160ContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A160ContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOS_PRAZOS", StringUtil.LTrim( StringUtil.NToC( (decimal)(A911ContratoServicos_Prazos), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICO_PERCENTUAL", StringUtil.LTrim( StringUtil.NToC( A558Servico_Percentual, 7, 3, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICO_SIGLA", StringUtil.RTrim( A605Servico_Sigla));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_SERVICOSIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A826ContratoServicos_ServicoSigla, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_ALIAS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1858ContratoServicos_Alias, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_PRAZOTPDIAS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1454ContratoServicos_PrazoTpDias, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_PRAZOINICIO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1649ContratoServicos_PrazoInicio), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_PRAZOANALISE", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1152ContratoServicos_PrazoAnalise), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_PRAZORESPOSTA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1153ContratoServicos_PrazoResposta), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_PRAZOGARANTIA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1181ContratoServicos_PrazoGarantia), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_PRAZOATENDEGARANTIA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1182ContratoServicos_PrazoAtendeGarantia), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_PRAZOCORRECAOTIPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1225ContratoServicos_PrazoCorrecaoTipo, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_PRAZOCORRECAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1224ContratoServicos_PrazoCorrecao), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_TMPESTANL", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1516ContratoServicos_TmpEstAnl), "ZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_TMPESTEXC", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1501ContratoServicos_TmpEstExc), "ZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_TMPESTCRR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1502ContratoServicos_TmpEstCrr), "ZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_STATUSPAGFNC", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1325ContratoServicos_StatusPagFnc, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_INDICEDIVERGENCIA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1455ContratoServicos_IndiceDivergencia, "ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_NAOREQUERATR", GetSecureSignedToken( sPrefix, A1397ContratoServicos_NaoRequerAtr));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_LIMITEPROPOSTA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1799ContratoServicos_LimiteProposta, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_LOCALEXEC", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A639ContratoServicos_LocalExec, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_FATORCNVUNDCNT", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1341ContratoServicos_FatorCnvUndCnt, "ZZZ9.9999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_QTDCONTRATADA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A555Servico_QtdContratada), "Z,ZZZ,ZZZ,ZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_VLRUNIDADECONTRATADA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A557Servico_VlrUnidadeContratada, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_PRODUTIVIDADE", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1191ContratoServicos_Produtividade, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICOCONTRATO_FATURAMENTO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A607ServicoContrato_Faturamento, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vSERVICO_PERCENTUAL", GetSecureSignedToken( sPrefix, context.localUtil.Format( AV13Servico_Percentual, "ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_CALCULORMN", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2074ContratoServicos_CalculoRmn), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_PERCCNC", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1539ContratoServicos_PercCnc), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_ESPELHAACEITE", GetSecureSignedToken( sPrefix, A1217ContratoServicos_EspelhaAceite));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_MOMENTO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1266ContratoServicos_Momento, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_TIPOVNC", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A868ContratoServicos_TipoVnc, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_QNTUNTCNS", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1340ContratoServicos_QntUntCns, "ZZZ9.9999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_CODIGOFISCAL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1723ContratoServicos_CodigoFiscal, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_SOLICITAGESTORSISTEMA", GetSecureSignedToken( sPrefix, A2094ContratoServicos_SolicitaGestorSistema));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOS_ATIVO", GetSecureSignedToken( sPrefix, A638ContratoServicos_Ativo));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm752( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contratoservicosgeneral.js", "?20203160111384");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContratoServicosGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contrato Servicos General" ;
      }

      protected void WB750( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contratoservicosgeneral.aspx");
            }
            wb_table1_2_752( true) ;
         }
         else
         {
            wb_table1_2_752( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_752e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void START752( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contrato Servicos General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP750( ) ;
            }
         }
      }

      protected void WS752( )
      {
         START752( ) ;
         EVT752( ) ;
      }

      protected void EVT752( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP750( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP750( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11752 */
                                    E11752 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP750( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12752 */
                                    E12752 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP750( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13752 */
                                    E13752 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP750( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14752 */
                                    E14752 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP750( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15752 */
                                    E15752 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDPRAZO'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP750( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E16752 */
                                    E16752 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP750( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP750( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE752( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm752( ) ;
            }
         }
      }

      protected void PA752( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbContratoServicos_PrazoTpDias.Name = "CONTRATOSERVICOS_PRAZOTPDIAS";
            cmbContratoServicos_PrazoTpDias.WebTags = "";
            cmbContratoServicos_PrazoTpDias.addItem("", "(Nenhum)", 0);
            cmbContratoServicos_PrazoTpDias.addItem("U", "�teis", 0);
            cmbContratoServicos_PrazoTpDias.addItem("C", "Corridos", 0);
            if ( cmbContratoServicos_PrazoTpDias.ItemCount > 0 )
            {
               A1454ContratoServicos_PrazoTpDias = cmbContratoServicos_PrazoTpDias.getValidValue(A1454ContratoServicos_PrazoTpDias);
               n1454ContratoServicos_PrazoTpDias = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1454ContratoServicos_PrazoTpDias", A1454ContratoServicos_PrazoTpDias);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_PRAZOTPDIAS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1454ContratoServicos_PrazoTpDias, ""))));
            }
            cmbContratoServicos_PrazoInicio.Name = "CONTRATOSERVICOS_PRAZOINICIO";
            cmbContratoServicos_PrazoInicio.WebTags = "";
            cmbContratoServicos_PrazoInicio.addItem("1", "Dia seguinte", 0);
            cmbContratoServicos_PrazoInicio.addItem("2", "Dia �til seguinte", 0);
            cmbContratoServicos_PrazoInicio.addItem("3", "Imediato", 0);
            if ( cmbContratoServicos_PrazoInicio.ItemCount > 0 )
            {
               A1649ContratoServicos_PrazoInicio = (short)(NumberUtil.Val( cmbContratoServicos_PrazoInicio.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1649ContratoServicos_PrazoInicio), 4, 0))), "."));
               n1649ContratoServicos_PrazoInicio = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1649ContratoServicos_PrazoInicio", StringUtil.LTrim( StringUtil.Str( (decimal)(A1649ContratoServicos_PrazoInicio), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_PRAZOINICIO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1649ContratoServicos_PrazoInicio), "ZZZ9")));
            }
            cmbContratoServicos_PrazoTipo.Name = "CONTRATOSERVICOS_PRAZOTIPO";
            cmbContratoServicos_PrazoTipo.WebTags = "";
            cmbContratoServicos_PrazoTipo.addItem("A", "Acumulado", 0);
            cmbContratoServicos_PrazoTipo.addItem("C", "Complexidade", 0);
            cmbContratoServicos_PrazoTipo.addItem("S", "Solicitado", 0);
            cmbContratoServicos_PrazoTipo.addItem("F", "Fixo", 0);
            cmbContratoServicos_PrazoTipo.addItem("E", "El�stico", 0);
            cmbContratoServicos_PrazoTipo.addItem("V", "Vari�vel", 0);
            cmbContratoServicos_PrazoTipo.addItem("P", "Progress�o Aritm�tica", 0);
            cmbContratoServicos_PrazoTipo.addItem("O", "Combinado", 0);
            if ( cmbContratoServicos_PrazoTipo.ItemCount > 0 )
            {
               A913ContratoServicos_PrazoTipo = cmbContratoServicos_PrazoTipo.getValidValue(A913ContratoServicos_PrazoTipo);
               n913ContratoServicos_PrazoTipo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A913ContratoServicos_PrazoTipo", A913ContratoServicos_PrazoTipo);
            }
            cmbContratoServicos_PrazoCorrecaoTipo.Name = "CONTRATOSERVICOS_PRAZOCORRECAOTIPO";
            cmbContratoServicos_PrazoCorrecaoTipo.WebTags = "";
            cmbContratoServicos_PrazoCorrecaoTipo.addItem("", "(Nenhum)", 0);
            cmbContratoServicos_PrazoCorrecaoTipo.addItem("F", "Fixo", 0);
            cmbContratoServicos_PrazoCorrecaoTipo.addItem("I", "Igual ao prazo de execu��o", 0);
            cmbContratoServicos_PrazoCorrecaoTipo.addItem("P", "Percentual do prazo de execu��o", 0);
            if ( cmbContratoServicos_PrazoCorrecaoTipo.ItemCount > 0 )
            {
               A1225ContratoServicos_PrazoCorrecaoTipo = cmbContratoServicos_PrazoCorrecaoTipo.getValidValue(A1225ContratoServicos_PrazoCorrecaoTipo);
               n1225ContratoServicos_PrazoCorrecaoTipo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1225ContratoServicos_PrazoCorrecaoTipo", A1225ContratoServicos_PrazoCorrecaoTipo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_PRAZOCORRECAOTIPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1225ContratoServicos_PrazoCorrecaoTipo, ""))));
            }
            cmbContratoServicos_StatusPagFnc.Name = "CONTRATOSERVICOS_STATUSPAGFNC";
            cmbContratoServicos_StatusPagFnc.WebTags = "";
            cmbContratoServicos_StatusPagFnc.addItem("", "(Nenhum)", 0);
            cmbContratoServicos_StatusPagFnc.addItem("B", "Stand by", 0);
            cmbContratoServicos_StatusPagFnc.addItem("S", "Solicitada", 0);
            cmbContratoServicos_StatusPagFnc.addItem("E", "Em An�lise", 0);
            cmbContratoServicos_StatusPagFnc.addItem("A", "Em execu��o", 0);
            cmbContratoServicos_StatusPagFnc.addItem("R", "Resolvida", 0);
            cmbContratoServicos_StatusPagFnc.addItem("C", "Conferida", 0);
            cmbContratoServicos_StatusPagFnc.addItem("D", "Rejeitada", 0);
            cmbContratoServicos_StatusPagFnc.addItem("H", "Homologada", 0);
            cmbContratoServicos_StatusPagFnc.addItem("O", "Aceite", 0);
            cmbContratoServicos_StatusPagFnc.addItem("P", "A Pagar", 0);
            cmbContratoServicos_StatusPagFnc.addItem("L", "Liquidada", 0);
            cmbContratoServicos_StatusPagFnc.addItem("X", "Cancelada", 0);
            cmbContratoServicos_StatusPagFnc.addItem("N", "N�o Faturada", 0);
            cmbContratoServicos_StatusPagFnc.addItem("J", "Planejamento", 0);
            cmbContratoServicos_StatusPagFnc.addItem("I", "An�lise Planejamento", 0);
            cmbContratoServicos_StatusPagFnc.addItem("T", "Validacao T�cnica", 0);
            cmbContratoServicos_StatusPagFnc.addItem("Q", "Validacao Qualidade", 0);
            cmbContratoServicos_StatusPagFnc.addItem("G", "Em Homologa��o", 0);
            cmbContratoServicos_StatusPagFnc.addItem("M", "Valida��o Mensura��o", 0);
            cmbContratoServicos_StatusPagFnc.addItem("U", "Rascunho", 0);
            if ( cmbContratoServicos_StatusPagFnc.ItemCount > 0 )
            {
               A1325ContratoServicos_StatusPagFnc = cmbContratoServicos_StatusPagFnc.getValidValue(A1325ContratoServicos_StatusPagFnc);
               n1325ContratoServicos_StatusPagFnc = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1325ContratoServicos_StatusPagFnc", A1325ContratoServicos_StatusPagFnc);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_STATUSPAGFNC", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1325ContratoServicos_StatusPagFnc, ""))));
            }
            chkContratoServicos_NaoRequerAtr.Name = "CONTRATOSERVICOS_NAOREQUERATR";
            chkContratoServicos_NaoRequerAtr.WebTags = "";
            chkContratoServicos_NaoRequerAtr.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkContratoServicos_NaoRequerAtr_Internalname, "TitleCaption", chkContratoServicos_NaoRequerAtr.Caption);
            chkContratoServicos_NaoRequerAtr.CheckedValue = "false";
            cmbContratoServicos_LocalExec.Name = "CONTRATOSERVICOS_LOCALEXEC";
            cmbContratoServicos_LocalExec.WebTags = "";
            cmbContratoServicos_LocalExec.addItem("A", "Contratada", 0);
            cmbContratoServicos_LocalExec.addItem("E", "Contratante", 0);
            cmbContratoServicos_LocalExec.addItem("O", "Opcional", 0);
            if ( cmbContratoServicos_LocalExec.ItemCount > 0 )
            {
               A639ContratoServicos_LocalExec = cmbContratoServicos_LocalExec.getValidValue(A639ContratoServicos_LocalExec);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A639ContratoServicos_LocalExec", A639ContratoServicos_LocalExec);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_LOCALEXEC", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A639ContratoServicos_LocalExec, ""))));
            }
            cmbServicoContrato_Faturamento.Name = "SERVICOCONTRATO_FATURAMENTO";
            cmbServicoContrato_Faturamento.WebTags = "";
            cmbServicoContrato_Faturamento.addItem("B", "Bruto", 0);
            cmbServicoContrato_Faturamento.addItem("L", "Liquido", 0);
            if ( cmbServicoContrato_Faturamento.ItemCount > 0 )
            {
               A607ServicoContrato_Faturamento = cmbServicoContrato_Faturamento.getValidValue(A607ServicoContrato_Faturamento);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A607ServicoContrato_Faturamento", A607ServicoContrato_Faturamento);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICOCONTRATO_FATURAMENTO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A607ServicoContrato_Faturamento, ""))));
            }
            cmbContratoServicos_CalculoRmn.Name = "CONTRATOSERVICOS_CALCULORMN";
            cmbContratoServicos_CalculoRmn.WebTags = "";
            cmbContratoServicos_CalculoRmn.addItem("0", "Configurado", 0);
            cmbContratoServicos_CalculoRmn.addItem("1", "Vari�vel", 0);
            if ( cmbContratoServicos_CalculoRmn.ItemCount > 0 )
            {
               A2074ContratoServicos_CalculoRmn = (short)(NumberUtil.Val( cmbContratoServicos_CalculoRmn.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2074ContratoServicos_CalculoRmn), 4, 0))), "."));
               n2074ContratoServicos_CalculoRmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2074ContratoServicos_CalculoRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2074ContratoServicos_CalculoRmn), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_CALCULORMN", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2074ContratoServicos_CalculoRmn), "ZZZ9")));
            }
            chkContratoServicos_EspelhaAceite.Name = "CONTRATOSERVICOS_ESPELHAACEITE";
            chkContratoServicos_EspelhaAceite.WebTags = "";
            chkContratoServicos_EspelhaAceite.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkContratoServicos_EspelhaAceite_Internalname, "TitleCaption", chkContratoServicos_EspelhaAceite.Caption);
            chkContratoServicos_EspelhaAceite.CheckedValue = "false";
            cmbContratoServicos_Momento.Name = "CONTRATOSERVICOS_MOMENTO";
            cmbContratoServicos_Momento.WebTags = "";
            cmbContratoServicos_Momento.addItem("", "(Nenhum)", 0);
            cmbContratoServicos_Momento.addItem("I", "Inicial", 0);
            cmbContratoServicos_Momento.addItem("F", "Final", 0);
            if ( cmbContratoServicos_Momento.ItemCount > 0 )
            {
               A1266ContratoServicos_Momento = cmbContratoServicos_Momento.getValidValue(A1266ContratoServicos_Momento);
               n1266ContratoServicos_Momento = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1266ContratoServicos_Momento", A1266ContratoServicos_Momento);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_MOMENTO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1266ContratoServicos_Momento, ""))));
            }
            cmbContratoServicos_TipoVnc.Name = "CONTRATOSERVICOS_TIPOVNC";
            cmbContratoServicos_TipoVnc.WebTags = "";
            cmbContratoServicos_TipoVnc.addItem("", "N/A", 0);
            cmbContratoServicos_TipoVnc.addItem("D", "D�bito/Cr�dito", 0);
            cmbContratoServicos_TipoVnc.addItem("C", "Compara��o", 0);
            cmbContratoServicos_TipoVnc.addItem("A", "D�bito/Cr�dito e Compara��o", 0);
            if ( cmbContratoServicos_TipoVnc.ItemCount > 0 )
            {
               A868ContratoServicos_TipoVnc = cmbContratoServicos_TipoVnc.getValidValue(A868ContratoServicos_TipoVnc);
               n868ContratoServicos_TipoVnc = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A868ContratoServicos_TipoVnc", A868ContratoServicos_TipoVnc);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_TIPOVNC", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A868ContratoServicos_TipoVnc, ""))));
            }
            chkContratoServicos_SolicitaGestorSistema.Name = "CONTRATOSERVICOS_SOLICITAGESTORSISTEMA";
            chkContratoServicos_SolicitaGestorSistema.WebTags = "";
            chkContratoServicos_SolicitaGestorSistema.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkContratoServicos_SolicitaGestorSistema_Internalname, "TitleCaption", chkContratoServicos_SolicitaGestorSistema.Caption);
            chkContratoServicos_SolicitaGestorSistema.CheckedValue = "false";
            chkContratoServicos_Ativo.Name = "CONTRATOSERVICOS_ATIVO";
            chkContratoServicos_Ativo.WebTags = "";
            chkContratoServicos_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkContratoServicos_Ativo_Internalname, "TitleCaption", chkContratoServicos_Ativo.Caption);
            chkContratoServicos_Ativo.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbContratoServicos_PrazoTpDias.ItemCount > 0 )
         {
            A1454ContratoServicos_PrazoTpDias = cmbContratoServicos_PrazoTpDias.getValidValue(A1454ContratoServicos_PrazoTpDias);
            n1454ContratoServicos_PrazoTpDias = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1454ContratoServicos_PrazoTpDias", A1454ContratoServicos_PrazoTpDias);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_PRAZOTPDIAS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1454ContratoServicos_PrazoTpDias, ""))));
         }
         if ( cmbContratoServicos_PrazoInicio.ItemCount > 0 )
         {
            A1649ContratoServicos_PrazoInicio = (short)(NumberUtil.Val( cmbContratoServicos_PrazoInicio.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1649ContratoServicos_PrazoInicio), 4, 0))), "."));
            n1649ContratoServicos_PrazoInicio = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1649ContratoServicos_PrazoInicio", StringUtil.LTrim( StringUtil.Str( (decimal)(A1649ContratoServicos_PrazoInicio), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_PRAZOINICIO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1649ContratoServicos_PrazoInicio), "ZZZ9")));
         }
         if ( cmbContratoServicos_PrazoTipo.ItemCount > 0 )
         {
            A913ContratoServicos_PrazoTipo = cmbContratoServicos_PrazoTipo.getValidValue(A913ContratoServicos_PrazoTipo);
            n913ContratoServicos_PrazoTipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A913ContratoServicos_PrazoTipo", A913ContratoServicos_PrazoTipo);
         }
         if ( cmbContratoServicos_PrazoCorrecaoTipo.ItemCount > 0 )
         {
            A1225ContratoServicos_PrazoCorrecaoTipo = cmbContratoServicos_PrazoCorrecaoTipo.getValidValue(A1225ContratoServicos_PrazoCorrecaoTipo);
            n1225ContratoServicos_PrazoCorrecaoTipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1225ContratoServicos_PrazoCorrecaoTipo", A1225ContratoServicos_PrazoCorrecaoTipo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_PRAZOCORRECAOTIPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1225ContratoServicos_PrazoCorrecaoTipo, ""))));
         }
         if ( cmbContratoServicos_StatusPagFnc.ItemCount > 0 )
         {
            A1325ContratoServicos_StatusPagFnc = cmbContratoServicos_StatusPagFnc.getValidValue(A1325ContratoServicos_StatusPagFnc);
            n1325ContratoServicos_StatusPagFnc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1325ContratoServicos_StatusPagFnc", A1325ContratoServicos_StatusPagFnc);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_STATUSPAGFNC", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1325ContratoServicos_StatusPagFnc, ""))));
         }
         if ( cmbContratoServicos_LocalExec.ItemCount > 0 )
         {
            A639ContratoServicos_LocalExec = cmbContratoServicos_LocalExec.getValidValue(A639ContratoServicos_LocalExec);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A639ContratoServicos_LocalExec", A639ContratoServicos_LocalExec);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_LOCALEXEC", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A639ContratoServicos_LocalExec, ""))));
         }
         if ( cmbServicoContrato_Faturamento.ItemCount > 0 )
         {
            A607ServicoContrato_Faturamento = cmbServicoContrato_Faturamento.getValidValue(A607ServicoContrato_Faturamento);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A607ServicoContrato_Faturamento", A607ServicoContrato_Faturamento);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICOCONTRATO_FATURAMENTO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A607ServicoContrato_Faturamento, ""))));
         }
         if ( cmbContratoServicos_CalculoRmn.ItemCount > 0 )
         {
            A2074ContratoServicos_CalculoRmn = (short)(NumberUtil.Val( cmbContratoServicos_CalculoRmn.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2074ContratoServicos_CalculoRmn), 4, 0))), "."));
            n2074ContratoServicos_CalculoRmn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2074ContratoServicos_CalculoRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2074ContratoServicos_CalculoRmn), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_CALCULORMN", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2074ContratoServicos_CalculoRmn), "ZZZ9")));
         }
         if ( cmbContratoServicos_Momento.ItemCount > 0 )
         {
            A1266ContratoServicos_Momento = cmbContratoServicos_Momento.getValidValue(A1266ContratoServicos_Momento);
            n1266ContratoServicos_Momento = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1266ContratoServicos_Momento", A1266ContratoServicos_Momento);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_MOMENTO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1266ContratoServicos_Momento, ""))));
         }
         if ( cmbContratoServicos_TipoVnc.ItemCount > 0 )
         {
            A868ContratoServicos_TipoVnc = cmbContratoServicos_TipoVnc.getValidValue(A868ContratoServicos_TipoVnc);
            n868ContratoServicos_TipoVnc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A868ContratoServicos_TipoVnc", A868ContratoServicos_TipoVnc);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_TIPOVNC", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A868ContratoServicos_TipoVnc, ""))));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF752( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV17Pgmname = "ContratoServicosGeneral";
         context.Gx_err = 0;
         edtavServico_percentual_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavServico_percentual_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_percentual_Enabled), 5, 0)));
      }

      protected void RF752( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00756 */
            pr_default.execute(2, new Object[] {A160ContratoServicos_Codigo});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A155Servico_Codigo = H00756_A155Servico_Codigo[0];
               A39Contratada_Codigo = H00756_A39Contratada_Codigo[0];
               A40Contratada_PessoaCod = H00756_A40Contratada_PessoaCod[0];
               A1212ContratoServicos_UnidadeContratada = H00756_A1212ContratoServicos_UnidadeContratada[0];
               A558Servico_Percentual = H00756_A558Servico_Percentual[0];
               n558Servico_Percentual = H00756_n558Servico_Percentual[0];
               A157ServicoGrupo_Codigo = H00756_A157ServicoGrupo_Codigo[0];
               A74Contrato_Codigo = H00756_A74Contrato_Codigo[0];
               A638ContratoServicos_Ativo = H00756_A638ContratoServicos_Ativo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A638ContratoServicos_Ativo", A638ContratoServicos_Ativo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_ATIVO", GetSecureSignedToken( sPrefix, A638ContratoServicos_Ativo));
               A2094ContratoServicos_SolicitaGestorSistema = H00756_A2094ContratoServicos_SolicitaGestorSistema[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2094ContratoServicos_SolicitaGestorSistema", A2094ContratoServicos_SolicitaGestorSistema);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_SOLICITAGESTORSISTEMA", GetSecureSignedToken( sPrefix, A2094ContratoServicos_SolicitaGestorSistema));
               A1723ContratoServicos_CodigoFiscal = H00756_A1723ContratoServicos_CodigoFiscal[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1723ContratoServicos_CodigoFiscal", A1723ContratoServicos_CodigoFiscal);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_CODIGOFISCAL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1723ContratoServicos_CodigoFiscal, ""))));
               n1723ContratoServicos_CodigoFiscal = H00756_n1723ContratoServicos_CodigoFiscal[0];
               A1340ContratoServicos_QntUntCns = H00756_A1340ContratoServicos_QntUntCns[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1340ContratoServicos_QntUntCns", StringUtil.LTrim( StringUtil.Str( A1340ContratoServicos_QntUntCns, 9, 4)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_QNTUNTCNS", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1340ContratoServicos_QntUntCns, "ZZZ9.9999")));
               n1340ContratoServicos_QntUntCns = H00756_n1340ContratoServicos_QntUntCns[0];
               A868ContratoServicos_TipoVnc = H00756_A868ContratoServicos_TipoVnc[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A868ContratoServicos_TipoVnc", A868ContratoServicos_TipoVnc);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_TIPOVNC", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A868ContratoServicos_TipoVnc, ""))));
               n868ContratoServicos_TipoVnc = H00756_n868ContratoServicos_TipoVnc[0];
               A1266ContratoServicos_Momento = H00756_A1266ContratoServicos_Momento[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1266ContratoServicos_Momento", A1266ContratoServicos_Momento);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_MOMENTO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1266ContratoServicos_Momento, ""))));
               n1266ContratoServicos_Momento = H00756_n1266ContratoServicos_Momento[0];
               A1217ContratoServicos_EspelhaAceite = H00756_A1217ContratoServicos_EspelhaAceite[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1217ContratoServicos_EspelhaAceite", A1217ContratoServicos_EspelhaAceite);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_ESPELHAACEITE", GetSecureSignedToken( sPrefix, A1217ContratoServicos_EspelhaAceite));
               n1217ContratoServicos_EspelhaAceite = H00756_n1217ContratoServicos_EspelhaAceite[0];
               A1539ContratoServicos_PercCnc = H00756_A1539ContratoServicos_PercCnc[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1539ContratoServicos_PercCnc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1539ContratoServicos_PercCnc), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_PERCCNC", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1539ContratoServicos_PercCnc), "ZZZ9")));
               n1539ContratoServicos_PercCnc = H00756_n1539ContratoServicos_PercCnc[0];
               A2074ContratoServicos_CalculoRmn = H00756_A2074ContratoServicos_CalculoRmn[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2074ContratoServicos_CalculoRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2074ContratoServicos_CalculoRmn), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_CALCULORMN", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2074ContratoServicos_CalculoRmn), "ZZZ9")));
               n2074ContratoServicos_CalculoRmn = H00756_n2074ContratoServicos_CalculoRmn[0];
               A607ServicoContrato_Faturamento = H00756_A607ServicoContrato_Faturamento[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A607ServicoContrato_Faturamento", A607ServicoContrato_Faturamento);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICOCONTRATO_FATURAMENTO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A607ServicoContrato_Faturamento, ""))));
               A1191ContratoServicos_Produtividade = H00756_A1191ContratoServicos_Produtividade[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1191ContratoServicos_Produtividade", StringUtil.LTrim( StringUtil.Str( A1191ContratoServicos_Produtividade, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_PRODUTIVIDADE", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1191ContratoServicos_Produtividade, "ZZ,ZZZ,ZZ9.999")));
               n1191ContratoServicos_Produtividade = H00756_n1191ContratoServicos_Produtividade[0];
               A557Servico_VlrUnidadeContratada = H00756_A557Servico_VlrUnidadeContratada[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A557Servico_VlrUnidadeContratada", StringUtil.LTrim( StringUtil.Str( A557Servico_VlrUnidadeContratada, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_VLRUNIDADECONTRATADA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A557Servico_VlrUnidadeContratada, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
               A555Servico_QtdContratada = H00756_A555Servico_QtdContratada[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A555Servico_QtdContratada", StringUtil.LTrim( StringUtil.Str( (decimal)(A555Servico_QtdContratada), 10, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_QTDCONTRATADA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A555Servico_QtdContratada), "Z,ZZZ,ZZZ,ZZ9")));
               n555Servico_QtdContratada = H00756_n555Servico_QtdContratada[0];
               A1341ContratoServicos_FatorCnvUndCnt = H00756_A1341ContratoServicos_FatorCnvUndCnt[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1341ContratoServicos_FatorCnvUndCnt", StringUtil.LTrim( StringUtil.Str( A1341ContratoServicos_FatorCnvUndCnt, 9, 4)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_FATORCNVUNDCNT", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1341ContratoServicos_FatorCnvUndCnt, "ZZZ9.9999")));
               n1341ContratoServicos_FatorCnvUndCnt = H00756_n1341ContratoServicos_FatorCnvUndCnt[0];
               A2132ContratoServicos_UndCntNome = H00756_A2132ContratoServicos_UndCntNome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2132ContratoServicos_UndCntNome", A2132ContratoServicos_UndCntNome);
               n2132ContratoServicos_UndCntNome = H00756_n2132ContratoServicos_UndCntNome[0];
               A639ContratoServicos_LocalExec = H00756_A639ContratoServicos_LocalExec[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A639ContratoServicos_LocalExec", A639ContratoServicos_LocalExec);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_LOCALEXEC", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A639ContratoServicos_LocalExec, ""))));
               A1799ContratoServicos_LimiteProposta = H00756_A1799ContratoServicos_LimiteProposta[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1799ContratoServicos_LimiteProposta", StringUtil.LTrim( StringUtil.Str( A1799ContratoServicos_LimiteProposta, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_LIMITEPROPOSTA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1799ContratoServicos_LimiteProposta, "ZZ,ZZZ,ZZ9.999")));
               n1799ContratoServicos_LimiteProposta = H00756_n1799ContratoServicos_LimiteProposta[0];
               A1397ContratoServicos_NaoRequerAtr = H00756_A1397ContratoServicos_NaoRequerAtr[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1397ContratoServicos_NaoRequerAtr", A1397ContratoServicos_NaoRequerAtr);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_NAOREQUERATR", GetSecureSignedToken( sPrefix, A1397ContratoServicos_NaoRequerAtr));
               n1397ContratoServicos_NaoRequerAtr = H00756_n1397ContratoServicos_NaoRequerAtr[0];
               A1455ContratoServicos_IndiceDivergencia = H00756_A1455ContratoServicos_IndiceDivergencia[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1455ContratoServicos_IndiceDivergencia", StringUtil.LTrim( StringUtil.Str( A1455ContratoServicos_IndiceDivergencia, 6, 2)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_INDICEDIVERGENCIA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1455ContratoServicos_IndiceDivergencia, "ZZ9.99")));
               n1455ContratoServicos_IndiceDivergencia = H00756_n1455ContratoServicos_IndiceDivergencia[0];
               A1325ContratoServicos_StatusPagFnc = H00756_A1325ContratoServicos_StatusPagFnc[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1325ContratoServicos_StatusPagFnc", A1325ContratoServicos_StatusPagFnc);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_STATUSPAGFNC", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1325ContratoServicos_StatusPagFnc, ""))));
               n1325ContratoServicos_StatusPagFnc = H00756_n1325ContratoServicos_StatusPagFnc[0];
               A1502ContratoServicos_TmpEstCrr = H00756_A1502ContratoServicos_TmpEstCrr[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1502ContratoServicos_TmpEstCrr", StringUtil.LTrim( StringUtil.Str( (decimal)(A1502ContratoServicos_TmpEstCrr), 8, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_TMPESTCRR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1502ContratoServicos_TmpEstCrr), "ZZZZZZZ9")));
               n1502ContratoServicos_TmpEstCrr = H00756_n1502ContratoServicos_TmpEstCrr[0];
               A1501ContratoServicos_TmpEstExc = H00756_A1501ContratoServicos_TmpEstExc[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1501ContratoServicos_TmpEstExc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1501ContratoServicos_TmpEstExc), 8, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_TMPESTEXC", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1501ContratoServicos_TmpEstExc), "ZZZZZZZ9")));
               n1501ContratoServicos_TmpEstExc = H00756_n1501ContratoServicos_TmpEstExc[0];
               A1516ContratoServicos_TmpEstAnl = H00756_A1516ContratoServicos_TmpEstAnl[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1516ContratoServicos_TmpEstAnl", StringUtil.LTrim( StringUtil.Str( (decimal)(A1516ContratoServicos_TmpEstAnl), 8, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_TMPESTANL", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1516ContratoServicos_TmpEstAnl), "ZZZZZZZ9")));
               n1516ContratoServicos_TmpEstAnl = H00756_n1516ContratoServicos_TmpEstAnl[0];
               A1224ContratoServicos_PrazoCorrecao = H00756_A1224ContratoServicos_PrazoCorrecao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1224ContratoServicos_PrazoCorrecao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1224ContratoServicos_PrazoCorrecao), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_PRAZOCORRECAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1224ContratoServicos_PrazoCorrecao), "ZZZ9")));
               n1224ContratoServicos_PrazoCorrecao = H00756_n1224ContratoServicos_PrazoCorrecao[0];
               A1225ContratoServicos_PrazoCorrecaoTipo = H00756_A1225ContratoServicos_PrazoCorrecaoTipo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1225ContratoServicos_PrazoCorrecaoTipo", A1225ContratoServicos_PrazoCorrecaoTipo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_PRAZOCORRECAOTIPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1225ContratoServicos_PrazoCorrecaoTipo, ""))));
               n1225ContratoServicos_PrazoCorrecaoTipo = H00756_n1225ContratoServicos_PrazoCorrecaoTipo[0];
               A1182ContratoServicos_PrazoAtendeGarantia = H00756_A1182ContratoServicos_PrazoAtendeGarantia[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1182ContratoServicos_PrazoAtendeGarantia", StringUtil.LTrim( StringUtil.Str( (decimal)(A1182ContratoServicos_PrazoAtendeGarantia), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_PRAZOATENDEGARANTIA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1182ContratoServicos_PrazoAtendeGarantia), "ZZZ9")));
               n1182ContratoServicos_PrazoAtendeGarantia = H00756_n1182ContratoServicos_PrazoAtendeGarantia[0];
               A1181ContratoServicos_PrazoGarantia = H00756_A1181ContratoServicos_PrazoGarantia[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1181ContratoServicos_PrazoGarantia", StringUtil.LTrim( StringUtil.Str( (decimal)(A1181ContratoServicos_PrazoGarantia), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_PRAZOGARANTIA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1181ContratoServicos_PrazoGarantia), "ZZZ9")));
               n1181ContratoServicos_PrazoGarantia = H00756_n1181ContratoServicos_PrazoGarantia[0];
               A1153ContratoServicos_PrazoResposta = H00756_A1153ContratoServicos_PrazoResposta[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1153ContratoServicos_PrazoResposta", StringUtil.LTrim( StringUtil.Str( (decimal)(A1153ContratoServicos_PrazoResposta), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_PRAZORESPOSTA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1153ContratoServicos_PrazoResposta), "ZZZ9")));
               n1153ContratoServicos_PrazoResposta = H00756_n1153ContratoServicos_PrazoResposta[0];
               A1152ContratoServicos_PrazoAnalise = H00756_A1152ContratoServicos_PrazoAnalise[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1152ContratoServicos_PrazoAnalise", StringUtil.LTrim( StringUtil.Str( (decimal)(A1152ContratoServicos_PrazoAnalise), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_PRAZOANALISE", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1152ContratoServicos_PrazoAnalise), "ZZZ9")));
               n1152ContratoServicos_PrazoAnalise = H00756_n1152ContratoServicos_PrazoAnalise[0];
               A1649ContratoServicos_PrazoInicio = H00756_A1649ContratoServicos_PrazoInicio[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1649ContratoServicos_PrazoInicio", StringUtil.LTrim( StringUtil.Str( (decimal)(A1649ContratoServicos_PrazoInicio), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_PRAZOINICIO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1649ContratoServicos_PrazoInicio), "ZZZ9")));
               n1649ContratoServicos_PrazoInicio = H00756_n1649ContratoServicos_PrazoInicio[0];
               A1454ContratoServicos_PrazoTpDias = H00756_A1454ContratoServicos_PrazoTpDias[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1454ContratoServicos_PrazoTpDias", A1454ContratoServicos_PrazoTpDias);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_PRAZOTPDIAS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1454ContratoServicos_PrazoTpDias, ""))));
               n1454ContratoServicos_PrazoTpDias = H00756_n1454ContratoServicos_PrazoTpDias[0];
               A1858ContratoServicos_Alias = H00756_A1858ContratoServicos_Alias[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1858ContratoServicos_Alias", A1858ContratoServicos_Alias);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_ALIAS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1858ContratoServicos_Alias, "@!"))));
               n1858ContratoServicos_Alias = H00756_n1858ContratoServicos_Alias[0];
               A608Servico_Nome = H00756_A608Servico_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A608Servico_Nome", A608Servico_Nome);
               A158ServicoGrupo_Descricao = H00756_A158ServicoGrupo_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A158ServicoGrupo_Descricao", A158ServicoGrupo_Descricao);
               A42Contratada_PessoaCNPJ = H00756_A42Contratada_PessoaCNPJ[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
               n42Contratada_PessoaCNPJ = H00756_n42Contratada_PessoaCNPJ[0];
               A41Contratada_PessoaNom = H00756_A41Contratada_PessoaNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
               n41Contratada_PessoaNom = H00756_n41Contratada_PessoaNom[0];
               A79Contrato_Ano = H00756_A79Contrato_Ano[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
               A78Contrato_NumeroAta = H00756_A78Contrato_NumeroAta[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
               n78Contrato_NumeroAta = H00756_n78Contrato_NumeroAta[0];
               A77Contrato_Numero = H00756_A77Contrato_Numero[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A77Contrato_Numero", A77Contrato_Numero);
               A911ContratoServicos_Prazos = H00756_A911ContratoServicos_Prazos[0];
               n911ContratoServicos_Prazos = H00756_n911ContratoServicos_Prazos[0];
               A605Servico_Sigla = H00756_A605Servico_Sigla[0];
               A157ServicoGrupo_Codigo = H00756_A157ServicoGrupo_Codigo[0];
               A608Servico_Nome = H00756_A608Servico_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A608Servico_Nome", A608Servico_Nome);
               A605Servico_Sigla = H00756_A605Servico_Sigla[0];
               A158ServicoGrupo_Descricao = H00756_A158ServicoGrupo_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A158ServicoGrupo_Descricao", A158ServicoGrupo_Descricao);
               A2132ContratoServicos_UndCntNome = H00756_A2132ContratoServicos_UndCntNome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2132ContratoServicos_UndCntNome", A2132ContratoServicos_UndCntNome);
               n2132ContratoServicos_UndCntNome = H00756_n2132ContratoServicos_UndCntNome[0];
               A39Contratada_Codigo = H00756_A39Contratada_Codigo[0];
               A79Contrato_Ano = H00756_A79Contrato_Ano[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
               A78Contrato_NumeroAta = H00756_A78Contrato_NumeroAta[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
               n78Contrato_NumeroAta = H00756_n78Contrato_NumeroAta[0];
               A77Contrato_Numero = H00756_A77Contrato_Numero[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A77Contrato_Numero", A77Contrato_Numero);
               A40Contratada_PessoaCod = H00756_A40Contratada_PessoaCod[0];
               A42Contratada_PessoaCNPJ = H00756_A42Contratada_PessoaCNPJ[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
               n42Contratada_PessoaCNPJ = H00756_n42Contratada_PessoaCNPJ[0];
               A41Contratada_PessoaNom = H00756_A41Contratada_PessoaNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
               n41Contratada_PessoaNom = H00756_n41Contratada_PessoaNom[0];
               A911ContratoServicos_Prazos = H00756_A911ContratoServicos_Prazos[0];
               n911ContratoServicos_Prazos = H00756_n911ContratoServicos_Prazos[0];
               AV13Servico_Percentual = (decimal)(A558Servico_Percentual*100);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13Servico_Percentual", StringUtil.LTrim( StringUtil.Str( AV13Servico_Percentual, 7, 3)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vSERVICO_PERCENTUAL", GetSecureSignedToken( sPrefix, context.localUtil.Format( AV13Servico_Percentual, "ZZ9.999")));
               A826ContratoServicos_ServicoSigla = A605Servico_Sigla;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A826ContratoServicos_ServicoSigla", A826ContratoServicos_ServicoSigla);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_SERVICOSIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A826ContratoServicos_ServicoSigla, "@!"))));
               /* Execute user event: E12752 */
               E12752 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(2);
            WB750( ) ;
         }
      }

      protected void STRUP750( )
      {
         /* Before Start, stand alone formulas. */
         AV17Pgmname = "ContratoServicosGeneral";
         context.Gx_err = 0;
         edtavServico_percentual_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavServico_percentual_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_percentual_Enabled), 5, 0)));
         /* Using cursor H00758 */
         pr_default.execute(3, new Object[] {A160ContratoServicos_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            A911ContratoServicos_Prazos = H00758_A911ContratoServicos_Prazos[0];
            n911ContratoServicos_Prazos = H00758_n911ContratoServicos_Prazos[0];
         }
         else
         {
            A911ContratoServicos_Prazos = 0;
            n911ContratoServicos_Prazos = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A911ContratoServicos_Prazos", StringUtil.LTrim( StringUtil.Str( (decimal)(A911ContratoServicos_Prazos), 4, 0)));
         }
         pr_default.close(3);
         /* Using cursor H00759 */
         pr_default.execute(4, new Object[] {A160ContratoServicos_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            A914ContratoServicos_PrazoDias = H00759_A914ContratoServicos_PrazoDias[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A914ContratoServicos_PrazoDias", StringUtil.LTrim( StringUtil.Str( (decimal)(A914ContratoServicos_PrazoDias), 4, 0)));
            n914ContratoServicos_PrazoDias = H00759_n914ContratoServicos_PrazoDias[0];
            A913ContratoServicos_PrazoTipo = H00759_A913ContratoServicos_PrazoTipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A913ContratoServicos_PrazoTipo", A913ContratoServicos_PrazoTipo);
            n913ContratoServicos_PrazoTipo = H00759_n913ContratoServicos_PrazoTipo[0];
         }
         else
         {
            A913ContratoServicos_PrazoTipo = "";
            n913ContratoServicos_PrazoTipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A913ContratoServicos_PrazoTipo", A913ContratoServicos_PrazoTipo);
            A914ContratoServicos_PrazoDias = 0;
            n914ContratoServicos_PrazoDias = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A914ContratoServicos_PrazoDias", StringUtil.LTrim( StringUtil.Str( (decimal)(A914ContratoServicos_PrazoDias), 4, 0)));
         }
         pr_default.close(4);
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11752 */
         E11752 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A77Contrato_Numero = cgiGet( edtContrato_Numero_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A77Contrato_Numero", A77Contrato_Numero);
            A78Contrato_NumeroAta = cgiGet( edtContrato_NumeroAta_Internalname);
            n78Contrato_NumeroAta = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
            A79Contrato_Ano = (short)(context.localUtil.CToN( cgiGet( edtContrato_Ano_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
            A41Contratada_PessoaNom = StringUtil.Upper( cgiGet( edtContratada_PessoaNom_Internalname));
            n41Contratada_PessoaNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
            A42Contratada_PessoaCNPJ = cgiGet( edtContratada_PessoaCNPJ_Internalname);
            n42Contratada_PessoaCNPJ = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
            A158ServicoGrupo_Descricao = cgiGet( edtServicoGrupo_Descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A158ServicoGrupo_Descricao", A158ServicoGrupo_Descricao);
            A826ContratoServicos_ServicoSigla = StringUtil.Upper( cgiGet( edtContratoServicos_ServicoSigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A826ContratoServicos_ServicoSigla", A826ContratoServicos_ServicoSigla);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_SERVICOSIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A826ContratoServicos_ServicoSigla, "@!"))));
            A608Servico_Nome = StringUtil.Upper( cgiGet( edtServico_Nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A608Servico_Nome", A608Servico_Nome);
            A1858ContratoServicos_Alias = StringUtil.Upper( cgiGet( edtContratoServicos_Alias_Internalname));
            n1858ContratoServicos_Alias = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1858ContratoServicos_Alias", A1858ContratoServicos_Alias);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_ALIAS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1858ContratoServicos_Alias, "@!"))));
            cmbContratoServicos_PrazoTpDias.CurrentValue = cgiGet( cmbContratoServicos_PrazoTpDias_Internalname);
            A1454ContratoServicos_PrazoTpDias = cgiGet( cmbContratoServicos_PrazoTpDias_Internalname);
            n1454ContratoServicos_PrazoTpDias = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1454ContratoServicos_PrazoTpDias", A1454ContratoServicos_PrazoTpDias);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_PRAZOTPDIAS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1454ContratoServicos_PrazoTpDias, ""))));
            cmbContratoServicos_PrazoInicio.CurrentValue = cgiGet( cmbContratoServicos_PrazoInicio_Internalname);
            A1649ContratoServicos_PrazoInicio = (short)(NumberUtil.Val( cgiGet( cmbContratoServicos_PrazoInicio_Internalname), "."));
            n1649ContratoServicos_PrazoInicio = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1649ContratoServicos_PrazoInicio", StringUtil.LTrim( StringUtil.Str( (decimal)(A1649ContratoServicos_PrazoInicio), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_PRAZOINICIO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1649ContratoServicos_PrazoInicio), "ZZZ9")));
            A1152ContratoServicos_PrazoAnalise = (short)(context.localUtil.CToN( cgiGet( edtContratoServicos_PrazoAnalise_Internalname), ",", "."));
            n1152ContratoServicos_PrazoAnalise = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1152ContratoServicos_PrazoAnalise", StringUtil.LTrim( StringUtil.Str( (decimal)(A1152ContratoServicos_PrazoAnalise), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_PRAZOANALISE", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1152ContratoServicos_PrazoAnalise), "ZZZ9")));
            cmbContratoServicos_PrazoTipo.CurrentValue = cgiGet( cmbContratoServicos_PrazoTipo_Internalname);
            A913ContratoServicos_PrazoTipo = cgiGet( cmbContratoServicos_PrazoTipo_Internalname);
            n913ContratoServicos_PrazoTipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A913ContratoServicos_PrazoTipo", A913ContratoServicos_PrazoTipo);
            A914ContratoServicos_PrazoDias = (short)(context.localUtil.CToN( cgiGet( edtContratoServicos_PrazoDias_Internalname), ",", "."));
            n914ContratoServicos_PrazoDias = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A914ContratoServicos_PrazoDias", StringUtil.LTrim( StringUtil.Str( (decimal)(A914ContratoServicos_PrazoDias), 4, 0)));
            A1153ContratoServicos_PrazoResposta = (short)(context.localUtil.CToN( cgiGet( edtContratoServicos_PrazoResposta_Internalname), ",", "."));
            n1153ContratoServicos_PrazoResposta = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1153ContratoServicos_PrazoResposta", StringUtil.LTrim( StringUtil.Str( (decimal)(A1153ContratoServicos_PrazoResposta), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_PRAZORESPOSTA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1153ContratoServicos_PrazoResposta), "ZZZ9")));
            A1181ContratoServicos_PrazoGarantia = (short)(context.localUtil.CToN( cgiGet( edtContratoServicos_PrazoGarantia_Internalname), ",", "."));
            n1181ContratoServicos_PrazoGarantia = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1181ContratoServicos_PrazoGarantia", StringUtil.LTrim( StringUtil.Str( (decimal)(A1181ContratoServicos_PrazoGarantia), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_PRAZOGARANTIA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1181ContratoServicos_PrazoGarantia), "ZZZ9")));
            A1182ContratoServicos_PrazoAtendeGarantia = (short)(context.localUtil.CToN( cgiGet( edtContratoServicos_PrazoAtendeGarantia_Internalname), ",", "."));
            n1182ContratoServicos_PrazoAtendeGarantia = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1182ContratoServicos_PrazoAtendeGarantia", StringUtil.LTrim( StringUtil.Str( (decimal)(A1182ContratoServicos_PrazoAtendeGarantia), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_PRAZOATENDEGARANTIA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1182ContratoServicos_PrazoAtendeGarantia), "ZZZ9")));
            cmbContratoServicos_PrazoCorrecaoTipo.CurrentValue = cgiGet( cmbContratoServicos_PrazoCorrecaoTipo_Internalname);
            A1225ContratoServicos_PrazoCorrecaoTipo = cgiGet( cmbContratoServicos_PrazoCorrecaoTipo_Internalname);
            n1225ContratoServicos_PrazoCorrecaoTipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1225ContratoServicos_PrazoCorrecaoTipo", A1225ContratoServicos_PrazoCorrecaoTipo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_PRAZOCORRECAOTIPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1225ContratoServicos_PrazoCorrecaoTipo, ""))));
            A1224ContratoServicos_PrazoCorrecao = (short)(context.localUtil.CToN( cgiGet( edtContratoServicos_PrazoCorrecao_Internalname), ",", "."));
            n1224ContratoServicos_PrazoCorrecao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1224ContratoServicos_PrazoCorrecao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1224ContratoServicos_PrazoCorrecao), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_PRAZOCORRECAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1224ContratoServicos_PrazoCorrecao), "ZZZ9")));
            A1516ContratoServicos_TmpEstAnl = (int)(context.localUtil.CToN( cgiGet( edtContratoServicos_TmpEstAnl_Internalname), ",", "."));
            n1516ContratoServicos_TmpEstAnl = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1516ContratoServicos_TmpEstAnl", StringUtil.LTrim( StringUtil.Str( (decimal)(A1516ContratoServicos_TmpEstAnl), 8, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_TMPESTANL", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1516ContratoServicos_TmpEstAnl), "ZZZZZZZ9")));
            A1501ContratoServicos_TmpEstExc = (int)(context.localUtil.CToN( cgiGet( edtContratoServicos_TmpEstExc_Internalname), ",", "."));
            n1501ContratoServicos_TmpEstExc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1501ContratoServicos_TmpEstExc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1501ContratoServicos_TmpEstExc), 8, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_TMPESTEXC", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1501ContratoServicos_TmpEstExc), "ZZZZZZZ9")));
            A1502ContratoServicos_TmpEstCrr = (int)(context.localUtil.CToN( cgiGet( edtContratoServicos_TmpEstCrr_Internalname), ",", "."));
            n1502ContratoServicos_TmpEstCrr = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1502ContratoServicos_TmpEstCrr", StringUtil.LTrim( StringUtil.Str( (decimal)(A1502ContratoServicos_TmpEstCrr), 8, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_TMPESTCRR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1502ContratoServicos_TmpEstCrr), "ZZZZZZZ9")));
            cmbContratoServicos_StatusPagFnc.CurrentValue = cgiGet( cmbContratoServicos_StatusPagFnc_Internalname);
            A1325ContratoServicos_StatusPagFnc = cgiGet( cmbContratoServicos_StatusPagFnc_Internalname);
            n1325ContratoServicos_StatusPagFnc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1325ContratoServicos_StatusPagFnc", A1325ContratoServicos_StatusPagFnc);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_STATUSPAGFNC", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1325ContratoServicos_StatusPagFnc, ""))));
            A1455ContratoServicos_IndiceDivergencia = context.localUtil.CToN( cgiGet( edtContratoServicos_IndiceDivergencia_Internalname), ",", ".");
            n1455ContratoServicos_IndiceDivergencia = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1455ContratoServicos_IndiceDivergencia", StringUtil.LTrim( StringUtil.Str( A1455ContratoServicos_IndiceDivergencia, 6, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_INDICEDIVERGENCIA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1455ContratoServicos_IndiceDivergencia, "ZZ9.99")));
            A1397ContratoServicos_NaoRequerAtr = StringUtil.StrToBool( cgiGet( chkContratoServicos_NaoRequerAtr_Internalname));
            n1397ContratoServicos_NaoRequerAtr = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1397ContratoServicos_NaoRequerAtr", A1397ContratoServicos_NaoRequerAtr);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_NAOREQUERATR", GetSecureSignedToken( sPrefix, A1397ContratoServicos_NaoRequerAtr));
            A1799ContratoServicos_LimiteProposta = context.localUtil.CToN( cgiGet( edtContratoServicos_LimiteProposta_Internalname), ",", ".");
            n1799ContratoServicos_LimiteProposta = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1799ContratoServicos_LimiteProposta", StringUtil.LTrim( StringUtil.Str( A1799ContratoServicos_LimiteProposta, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_LIMITEPROPOSTA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1799ContratoServicos_LimiteProposta, "ZZ,ZZZ,ZZ9.999")));
            cmbContratoServicos_LocalExec.CurrentValue = cgiGet( cmbContratoServicos_LocalExec_Internalname);
            A639ContratoServicos_LocalExec = cgiGet( cmbContratoServicos_LocalExec_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A639ContratoServicos_LocalExec", A639ContratoServicos_LocalExec);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_LOCALEXEC", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A639ContratoServicos_LocalExec, ""))));
            A2132ContratoServicos_UndCntNome = StringUtil.Upper( cgiGet( edtContratoServicos_UndCntNome_Internalname));
            n2132ContratoServicos_UndCntNome = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2132ContratoServicos_UndCntNome", A2132ContratoServicos_UndCntNome);
            A1341ContratoServicos_FatorCnvUndCnt = context.localUtil.CToN( cgiGet( edtContratoServicos_FatorCnvUndCnt_Internalname), ",", ".");
            n1341ContratoServicos_FatorCnvUndCnt = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1341ContratoServicos_FatorCnvUndCnt", StringUtil.LTrim( StringUtil.Str( A1341ContratoServicos_FatorCnvUndCnt, 9, 4)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_FATORCNVUNDCNT", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1341ContratoServicos_FatorCnvUndCnt, "ZZZ9.9999")));
            A555Servico_QtdContratada = (long)(context.localUtil.CToN( cgiGet( edtServico_QtdContratada_Internalname), ",", "."));
            n555Servico_QtdContratada = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A555Servico_QtdContratada", StringUtil.LTrim( StringUtil.Str( (decimal)(A555Servico_QtdContratada), 10, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_QTDCONTRATADA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A555Servico_QtdContratada), "Z,ZZZ,ZZZ,ZZ9")));
            A557Servico_VlrUnidadeContratada = context.localUtil.CToN( cgiGet( edtServico_VlrUnidadeContratada_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A557Servico_VlrUnidadeContratada", StringUtil.LTrim( StringUtil.Str( A557Servico_VlrUnidadeContratada, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_VLRUNIDADECONTRATADA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A557Servico_VlrUnidadeContratada, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            A1191ContratoServicos_Produtividade = context.localUtil.CToN( cgiGet( edtContratoServicos_Produtividade_Internalname), ",", ".");
            n1191ContratoServicos_Produtividade = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1191ContratoServicos_Produtividade", StringUtil.LTrim( StringUtil.Str( A1191ContratoServicos_Produtividade, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_PRODUTIVIDADE", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1191ContratoServicos_Produtividade, "ZZ,ZZZ,ZZ9.999")));
            cmbServicoContrato_Faturamento.CurrentValue = cgiGet( cmbServicoContrato_Faturamento_Internalname);
            A607ServicoContrato_Faturamento = cgiGet( cmbServicoContrato_Faturamento_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A607ServicoContrato_Faturamento", A607ServicoContrato_Faturamento);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICOCONTRATO_FATURAMENTO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A607ServicoContrato_Faturamento, ""))));
            AV13Servico_Percentual = context.localUtil.CToN( cgiGet( edtavServico_percentual_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13Servico_Percentual", StringUtil.LTrim( StringUtil.Str( AV13Servico_Percentual, 7, 3)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vSERVICO_PERCENTUAL", GetSecureSignedToken( sPrefix, context.localUtil.Format( AV13Servico_Percentual, "ZZ9.999")));
            cmbContratoServicos_CalculoRmn.CurrentValue = cgiGet( cmbContratoServicos_CalculoRmn_Internalname);
            A2074ContratoServicos_CalculoRmn = (short)(NumberUtil.Val( cgiGet( cmbContratoServicos_CalculoRmn_Internalname), "."));
            n2074ContratoServicos_CalculoRmn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2074ContratoServicos_CalculoRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2074ContratoServicos_CalculoRmn), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_CALCULORMN", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2074ContratoServicos_CalculoRmn), "ZZZ9")));
            A1539ContratoServicos_PercCnc = (short)(context.localUtil.CToN( cgiGet( edtContratoServicos_PercCnc_Internalname), ",", "."));
            n1539ContratoServicos_PercCnc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1539ContratoServicos_PercCnc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1539ContratoServicos_PercCnc), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_PERCCNC", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1539ContratoServicos_PercCnc), "ZZZ9")));
            A1217ContratoServicos_EspelhaAceite = StringUtil.StrToBool( cgiGet( chkContratoServicos_EspelhaAceite_Internalname));
            n1217ContratoServicos_EspelhaAceite = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1217ContratoServicos_EspelhaAceite", A1217ContratoServicos_EspelhaAceite);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_ESPELHAACEITE", GetSecureSignedToken( sPrefix, A1217ContratoServicos_EspelhaAceite));
            cmbContratoServicos_Momento.CurrentValue = cgiGet( cmbContratoServicos_Momento_Internalname);
            A1266ContratoServicos_Momento = cgiGet( cmbContratoServicos_Momento_Internalname);
            n1266ContratoServicos_Momento = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1266ContratoServicos_Momento", A1266ContratoServicos_Momento);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_MOMENTO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1266ContratoServicos_Momento, ""))));
            cmbContratoServicos_TipoVnc.CurrentValue = cgiGet( cmbContratoServicos_TipoVnc_Internalname);
            A868ContratoServicos_TipoVnc = cgiGet( cmbContratoServicos_TipoVnc_Internalname);
            n868ContratoServicos_TipoVnc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A868ContratoServicos_TipoVnc", A868ContratoServicos_TipoVnc);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_TIPOVNC", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A868ContratoServicos_TipoVnc, ""))));
            A1340ContratoServicos_QntUntCns = context.localUtil.CToN( cgiGet( edtContratoServicos_QntUntCns_Internalname), ",", ".");
            n1340ContratoServicos_QntUntCns = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1340ContratoServicos_QntUntCns", StringUtil.LTrim( StringUtil.Str( A1340ContratoServicos_QntUntCns, 9, 4)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_QNTUNTCNS", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1340ContratoServicos_QntUntCns, "ZZZ9.9999")));
            A1723ContratoServicos_CodigoFiscal = cgiGet( edtContratoServicos_CodigoFiscal_Internalname);
            n1723ContratoServicos_CodigoFiscal = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1723ContratoServicos_CodigoFiscal", A1723ContratoServicos_CodigoFiscal);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_CODIGOFISCAL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1723ContratoServicos_CodigoFiscal, ""))));
            A2094ContratoServicos_SolicitaGestorSistema = StringUtil.StrToBool( cgiGet( chkContratoServicos_SolicitaGestorSistema_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2094ContratoServicos_SolicitaGestorSistema", A2094ContratoServicos_SolicitaGestorSistema);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_SOLICITAGESTORSISTEMA", GetSecureSignedToken( sPrefix, A2094ContratoServicos_SolicitaGestorSistema));
            A638ContratoServicos_Ativo = StringUtil.StrToBool( cgiGet( chkContratoServicos_Ativo_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A638ContratoServicos_Ativo", A638ContratoServicos_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOS_ATIVO", GetSecureSignedToken( sPrefix, A638ContratoServicos_Ativo));
            /* Read saved values. */
            wcpOA160ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA160ContratoServicos_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11752 */
         E11752 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11752( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         bttBtnupdate_Visible = (AV6WWPContext.gxTpr_Update&&AV6WWPContext.gxTpr_Userehgestor ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Visible), 5, 0)));
         bttBtndelete_Visible = (AV6WWPContext.gxTpr_Delete&&AV6WWPContext.gxTpr_Userehgestor ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Visible), 5, 0)));
      }

      protected void nextLoad( )
      {
      }

      protected void E12752( )
      {
         /* Load Routine */
         edtServicoGrupo_Descricao_Link = formatLink("viewservicogrupo.aspx") + "?" + UrlEncode("" +A157ServicoGrupo_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServicoGrupo_Descricao_Internalname, "Link", edtServicoGrupo_Descricao_Link);
         if ( ! ( ( StringUtil.StrCmp(A913ContratoServicos_PrazoTipo, "F") == 0 ) ) )
         {
            edtContratoServicos_PrazoDias_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicos_PrazoDias_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_PrazoDias_Visible), 5, 0)));
            cellContratoservicos_prazodias_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cellContratoservicos_prazodias_cell_Internalname, "Class", cellContratoservicos_prazodias_cell_Class);
            cellContratoservicos_prazodias_righttext_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cellContratoservicos_prazodias_righttext_cell_Internalname, "Class", cellContratoservicos_prazodias_righttext_cell_Class);
         }
         else
         {
            edtContratoServicos_PrazoDias_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicos_PrazoDias_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_PrazoDias_Visible), 5, 0)));
            cellContratoservicos_prazodias_righttext_cell_Class = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cellContratoservicos_prazodias_righttext_cell_Internalname, "Class", cellContratoservicos_prazodias_righttext_cell_Class);
            cellContratoservicos_prazodias_cell_Class = "DataContentCell";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cellContratoservicos_prazodias_cell_Internalname, "Class", cellContratoservicos_prazodias_cell_Class);
         }
         AV14Contrato_Codigo = A74Contrato_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Contrato_Codigo), 6, 0)));
      }

      protected void E13752( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("contratoservicos.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A160ContratoServicos_Codigo) + "," + UrlEncode("" +AV14Contrato_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E14752( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("contratoservicos.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A160ContratoServicos_Codigo) + "," + UrlEncode("" +AV14Contrato_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E15752( )
      {
         /* 'DoFechar' Routine */
         context.wjLoc = formatLink("viewcontrato.aspx") + "?" + UrlEncode("" +A74Contrato_Codigo) + "," + UrlEncode(StringUtil.RTrim("ContratoServicos"));
         context.wjLocDisableFrm = 1;
      }

      protected void E16752( )
      {
         /* 'DoUpdPrazo' Routine */
         context.wjLoc = formatLink("contratoservicosprazo.aspx") + "?" + UrlEncode(StringUtil.RTrim(((0==A911ContratoServicos_Prazos) ? "INS" : "UPD"))) + "," + UrlEncode("" +A160ContratoServicos_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV17Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ContratoServicos";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "ContratoServicos_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ContratoServicos_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_752( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_752( true) ;
         }
         else
         {
            wb_table2_8_752( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_752e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_289_752( true) ;
         }
         else
         {
            wb_table3_289_752( false) ;
         }
         return  ;
      }

      protected void wb_table3_289_752e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_752e( true) ;
         }
         else
         {
            wb_table1_2_752e( false) ;
         }
      }

      protected void wb_table3_289_752( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 292,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, bttBtnupdate_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 294,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtndelete_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 296,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_289_752e( true) ;
         }
         else
         {
            wb_table3_289_752e( false) ;
         }
      }

      protected void wb_table2_8_752( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"6\" >") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup1_Internalname, "Contrato", 1, 0, "px", 0, "px", "Group", "", "HLP_ContratoServicosGeneral.htm");
            wb_table4_12_752( true) ;
         }
         else
         {
            wb_table4_12_752( false) ;
         }
         return  ;
      }

      protected void wb_table4_12_752e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"6\" >") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup2_Internalname, "Servi�o", 1, 0, "px", 0, "px", "Group", "", "HLP_ContratoServicosGeneral.htm");
            wb_table5_40_752( true) ;
         }
         else
         {
            wb_table5_40_752( false) ;
         }
         return  ;
      }

      protected void wb_table5_40_752e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"6\" >") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup3_Internalname, "Prazos", 1, 0, "px", 0, "px", "Group", "", "HLP_ContratoServicosGeneral.htm");
            wb_table6_68_752( true) ;
         }
         else
         {
            wb_table6_68_752( false) ;
         }
         return  ;
      }

      protected void wb_table6_68_752e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"6\" >") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup4_Internalname, "Par�metros", 1, 0, "px", 0, "px", "Group", "", "HLP_ContratoServicosGeneral.htm");
            wb_table7_164_752( true) ;
         }
         else
         {
            wb_table7_164_752( false) ;
         }
         return  ;
      }

      protected void wb_table7_164_752e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_752e( true) ;
         }
         else
         {
            wb_table2_8_752e( false) ;
         }
      }

      protected void wb_table7_164_752( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblParametros_Internalname, tblParametros_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_indicedivergencia_Internalname, "Indice de Aceita��o", "", "", lblTextblockcontratoservicos_indicedivergencia_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table8_169_752( true) ;
         }
         else
         {
            wb_table8_169_752( false) ;
         }
         return  ;
      }

      protected void wb_table8_169_752e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_naorequeratr_Internalname, "N�o requer atribui��o", "", "", lblTextblockcontratoservicos_naorequeratr_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkContratoServicos_NaoRequerAtr_Internalname, StringUtil.BoolToStr( A1397ContratoServicos_NaoRequerAtr), "", "", 1, 0, "true", "", StyleString, ClassString, "", "");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_limiteproposta_Internalname, "Limite das Propostas", "", "", lblTextblockcontratoservicos_limiteproposta_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table9_182_752( true) ;
         }
         else
         {
            wb_table9_182_752( false) ;
         }
         return  ;
      }

      protected void wb_table9_182_752e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_localexec_Internalname, "Local de Execu��o", "", "", lblTextblockcontratoservicos_localexec_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoServicos_LocalExec, cmbContratoServicos_LocalExec_Internalname, StringUtil.RTrim( A639ContratoServicos_LocalExec), 1, cmbContratoServicos_LocalExec_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ContratoServicosGeneral.htm");
            cmbContratoServicos_LocalExec.CurrentValue = StringUtil.RTrim( A639ContratoServicos_LocalExec);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratoServicos_LocalExec_Internalname, "Values", (String)(cmbContratoServicos_LocalExec.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_undcntnome_Internalname, "Unidade Contratada", "", "", lblTextblockcontratoservicos_undcntnome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_UndCntNome_Internalname, StringUtil.RTrim( A2132ContratoServicos_UndCntNome), StringUtil.RTrim( context.localUtil.Format( A2132ContratoServicos_UndCntNome, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_UndCntNome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_fatorcnvundcnt_Internalname, "Fator de convers�o em horas", "", "", lblTextblockcontratoservicos_fatorcnvundcnt_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_FatorCnvUndCnt_Internalname, StringUtil.LTrim( StringUtil.NToC( A1341ContratoServicos_FatorCnvUndCnt, 9, 4, ",", "")), context.localUtil.Format( A1341ContratoServicos_FatorCnvUndCnt, "ZZZ9.9999"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_FatorCnvUndCnt_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 80, "px", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_qtdcontratada_Internalname, "Qtde. Contratada", "", "", lblTextblockservico_qtdcontratada_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServico_QtdContratada_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A555Servico_QtdContratada), 13, 0, ",", "")), context.localUtil.Format( (decimal)(A555Servico_QtdContratada), "Z,ZZZ,ZZZ,ZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_QtdContratada_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 80, "px", 1, "row", 13, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_vlrunidadecontratada_Internalname, "Valor da Unidade", "", "", lblTextblockservico_vlrunidadecontratada_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServico_VlrUnidadeContratada_Internalname, StringUtil.LTrim( StringUtil.NToC( A557Servico_VlrUnidadeContratada, 18, 5, ",", "")), context.localUtil.Format( A557Servico_VlrUnidadeContratada, "ZZZ,ZZZ,ZZZ,ZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_VlrUnidadeContratada_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 90, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_produtividade_Internalname, "Produtividade", "", "", lblTextblockcontratoservicos_produtividade_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table10_213_752( true) ;
         }
         else
         {
            wb_table10_213_752( false) ;
         }
         return  ;
      }

      protected void wb_table10_213_752e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservicocontrato_faturamento_Internalname, "Faturamento", "", "", lblTextblockservicocontrato_faturamento_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbServicoContrato_Faturamento, cmbServicoContrato_Faturamento_Internalname, StringUtil.RTrim( A607ServicoContrato_Faturamento), 1, cmbServicoContrato_Faturamento_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "svchar", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ContratoServicosGeneral.htm");
            cmbServicoContrato_Faturamento.CurrentValue = StringUtil.RTrim( A607ServicoContrato_Faturamento);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServicoContrato_Faturamento_Internalname, "Values", (String)(cmbServicoContrato_Faturamento.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_percentual_Internalname, "Valor do Pagamento", "", "", lblTextblockservico_percentual_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table11_227_752( true) ;
         }
         else
         {
            wb_table11_227_752( false) ;
         }
         return  ;
      }

      protected void wb_table11_227_752e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_calculormn_Internalname, "C�lculo da Remunera��o", "", "", lblTextblockcontratoservicos_calculormn_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoServicos_CalculoRmn, cmbContratoServicos_CalculoRmn_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A2074ContratoServicos_CalculoRmn), 4, 0)), 1, cmbContratoServicos_CalculoRmn_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ContratoServicosGeneral.htm");
            cmbContratoServicos_CalculoRmn.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2074ContratoServicos_CalculoRmn), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratoServicos_CalculoRmn_Internalname, "Values", (String)(cmbContratoServicos_CalculoRmn.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_perccnc_Internalname, "Valor do Cancelamento", "", "", lblTextblockcontratoservicos_perccnc_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table12_241_752( true) ;
         }
         else
         {
            wb_table12_241_752( false) ;
         }
         return  ;
      }

      protected void wb_table12_241_752e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_espelhaaceite_Internalname, "Espelhar Aceite na FS", "", "", lblTextblockcontratoservicos_espelhaaceite_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkContratoServicos_EspelhaAceite_Internalname, StringUtil.BoolToStr( A1217ContratoServicos_EspelhaAceite), "", "", 1, 0, "true", "", StyleString, ClassString, "", "");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_momento_Internalname, "Momento", "", "", lblTextblockcontratoservicos_momento_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoServicos_Momento, cmbContratoServicos_Momento_Internalname, StringUtil.RTrim( A1266ContratoServicos_Momento), 1, cmbContratoServicos_Momento_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ContratoServicosGeneral.htm");
            cmbContratoServicos_Momento.CurrentValue = StringUtil.RTrim( A1266ContratoServicos_Momento);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratoServicos_Momento_Internalname, "Values", (String)(cmbContratoServicos_Momento.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_tipovnc_Internalname, "Tipo de Vinculo", "", "", lblTextblockcontratoservicos_tipovnc_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoServicos_TipoVnc, cmbContratoServicos_TipoVnc_Internalname, StringUtil.RTrim( A868ContratoServicos_TipoVnc), 1, cmbContratoServicos_TipoVnc_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ContratoServicosGeneral.htm");
            cmbContratoServicos_TipoVnc.CurrentValue = StringUtil.RTrim( A868ContratoServicos_TipoVnc);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratoServicos_TipoVnc_Internalname, "Values", (String)(cmbContratoServicos_TipoVnc.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_qntuntcns_Internalname, "Qtde. Unit. de Consumo", "", "", lblTextblockcontratoservicos_qntuntcns_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_QntUntCns_Internalname, StringUtil.LTrim( StringUtil.NToC( A1340ContratoServicos_QntUntCns, 9, 4, ",", "")), context.localUtil.Format( A1340ContratoServicos_QntUntCns, "ZZZ9.9999"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_QntUntCns_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 80, "px", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_codigofiscal_Internalname, "C�digo Fiscal", "", "", lblTextblockcontratoservicos_codigofiscal_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_CodigoFiscal_Internalname, StringUtil.RTrim( A1723ContratoServicos_CodigoFiscal), StringUtil.RTrim( context.localUtil.Format( A1723ContratoServicos_CodigoFiscal, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_CodigoFiscal_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 5, "chr", 1, "row", 5, 0, 0, 0, 1, -1, -1, true, "CodigoFiscal", "left", true, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_solicitagestorsistema_Internalname, "Exibir Gestor Respons�vel pelo Sistema", "", "", lblTextblockcontratoservicos_solicitagestorsistema_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table13_276_752( true) ;
         }
         else
         {
            wb_table13_276_752( false) ;
         }
         return  ;
      }

      protected void wb_table13_276_752e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_ativo_Internalname, "Ativo?", "", "", lblTextblockcontratoservicos_ativo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCell'>") ;
            /* Check box */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkContratoServicos_Ativo_Internalname, StringUtil.BoolToStr( A638ContratoServicos_Ativo), "", "", 1, 0, "true", "", StyleString, ClassString, "", "");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_164_752e( true) ;
         }
         else
         {
            wb_table7_164_752e( false) ;
         }
      }

      protected void wb_table13_276_752( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratoservicos_solicitagestorsistema_Internalname, tblTablemergedcontratoservicos_solicitagestorsistema_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Check box */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkContratoServicos_SolicitaGestorSistema_Internalname, StringUtil.BoolToStr( A2094ContratoServicos_SolicitaGestorSistema), "", "", 1, 0, "true", "", StyleString, ClassString, "", "");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Static images/pictures */
            ClassString = "Image BootstrapTooltipRight";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgContratoservicos_solicitagestorsistema_infoicon_Internalname, context.GetImagePath( "2e4f3057-53d8-4ff3-81df-57002c73064d", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Exibe Gestor Respons�vel pelo Sistema ao cadastrar uma OS", 0, 0, 0, "px", 0, "px", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, false, false, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table13_276_752e( true) ;
         }
         else
         {
            wb_table13_276_752e( false) ;
         }
      }

      protected void wb_table12_241_752( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratoservicos_perccnc_Internalname, tblTablemergedcontratoservicos_perccnc_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_PercCnc_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1539ContratoServicos_PercCnc), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A1539ContratoServicos_PercCnc), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_PercCnc_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 50, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratoservicos_perccnc_righttext_Internalname, "�%", "", "", lblContratoservicos_perccnc_righttext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table12_241_752e( true) ;
         }
         else
         {
            wb_table12_241_752e( false) ;
         }
      }

      protected void wb_table11_227_752( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedservico_percentual_Internalname, tblTablemergedservico_percentual_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavServico_percentual_Internalname, StringUtil.LTrim( StringUtil.NToC( AV13Servico_Percentual, 7, 3, ",", "")), ((edtavServico_percentual_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV13Servico_Percentual, "ZZ9.999")) : context.localUtil.Format( AV13Servico_Percentual, "ZZ9.999")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServico_percentual_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavServico_percentual_Enabled, 0, "text", "", 60, "px", 1, "row", 7, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblServico_percentual_righttext_Internalname, "%", "", "", lblServico_percentual_righttext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_227_752e( true) ;
         }
         else
         {
            wb_table11_227_752e( false) ;
         }
      }

      protected void wb_table10_213_752( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratoservicos_produtividade_Internalname, tblTablemergedcontratoservicos_produtividade_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_Produtividade_Internalname, StringUtil.LTrim( StringUtil.NToC( A1191ContratoServicos_Produtividade, 14, 5, ",", "")), context.localUtil.Format( A1191ContratoServicos_Produtividade, "ZZ,ZZZ,ZZ9.999"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_Produtividade_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 70, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratoservicos_produtividade_righttext_Internalname, "Value", "", "", lblContratoservicos_produtividade_righttext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_213_752e( true) ;
         }
         else
         {
            wb_table10_213_752e( false) ;
         }
      }

      protected void wb_table9_182_752( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratoservicos_limiteproposta_Internalname, tblTablemergedcontratoservicos_limiteproposta_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_LimiteProposta_Internalname, StringUtil.LTrim( StringUtil.NToC( A1799ContratoServicos_LimiteProposta, 14, 5, ",", "")), context.localUtil.Format( A1799ContratoServicos_LimiteProposta, "ZZ,ZZZ,ZZ9.999"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_LimiteProposta_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 60, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratoservicos_limiteproposta_righttext_Internalname, "Value", "", "", lblContratoservicos_limiteproposta_righttext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_182_752e( true) ;
         }
         else
         {
            wb_table9_182_752e( false) ;
         }
      }

      protected void wb_table8_169_752( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratoservicos_indicedivergencia_Internalname, tblTablemergedcontratoservicos_indicedivergencia_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_IndiceDivergencia_Internalname, StringUtil.LTrim( StringUtil.NToC( A1455ContratoServicos_IndiceDivergencia, 6, 2, ",", "")), context.localUtil.Format( A1455ContratoServicos_IndiceDivergencia, "ZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_IndiceDivergencia_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 60, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Percentual", "right", false, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratoservicos_indicedivergencia_righttext_Internalname, "�% (0 aceita tudo)", "", "", lblContratoservicos_indicedivergencia_righttext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_169_752e( true) ;
         }
         else
         {
            wb_table8_169_752e( false) ;
         }
      }

      protected void wb_table6_68_752( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblPrazos_Internalname, tblPrazos_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_prazotpdias_Internalname, "Tipo de dias", "", "", lblTextblockcontratoservicos_prazotpdias_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoServicos_PrazoTpDias, cmbContratoServicos_PrazoTpDias_Internalname, StringUtil.RTrim( A1454ContratoServicos_PrazoTpDias), 1, cmbContratoServicos_PrazoTpDias_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ContratoServicosGeneral.htm");
            cmbContratoServicos_PrazoTpDias.CurrentValue = StringUtil.RTrim( A1454ContratoServicos_PrazoTpDias);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratoServicos_PrazoTpDias_Internalname, "Values", (String)(cmbContratoServicos_PrazoTpDias.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_prazoinicio_Internalname, "Inicio", "", "", lblTextblockcontratoservicos_prazoinicio_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoServicos_PrazoInicio, cmbContratoServicos_PrazoInicio_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1649ContratoServicos_PrazoInicio), 4, 0)), 1, cmbContratoServicos_PrazoInicio_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ContratoServicosGeneral.htm");
            cmbContratoServicos_PrazoInicio.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1649ContratoServicos_PrazoInicio), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratoServicos_PrazoInicio_Internalname, "Values", (String)(cmbContratoServicos_PrazoInicio.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_prazoanalise_Internalname, "An�lise", "", "", lblTextblockcontratoservicos_prazoanalise_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_PrazoAnalise_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1152ContratoServicos_PrazoAnalise), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A1152ContratoServicos_PrazoAnalise), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_PrazoAnalise_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_prazotipo_Internalname, "Execu��o", "", "", lblTextblockcontratoservicos_prazotipo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table14_88_752( true) ;
         }
         else
         {
            wb_table14_88_752( false) ;
         }
         return  ;
      }

      protected void wb_table14_88_752e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_prazoresposta_Internalname, "������������������Resposta", "", "", lblTextblockcontratoservicos_prazoresposta_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_PrazoResposta_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1153ContratoServicos_PrazoResposta), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A1153ContratoServicos_PrazoResposta), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "Tramita��es", "", edtContratoServicos_PrazoResposta_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_prazogarantia_Internalname, "Garantia", "", "", lblTextblockcontratoservicos_prazogarantia_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_PrazoGarantia_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1181ContratoServicos_PrazoGarantia), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A1181ContratoServicos_PrazoGarantia), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_PrazoGarantia_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_prazoatendegarantia_Internalname, "������������Execu��o da Gtia.", "", "", lblTextblockcontratoservicos_prazoatendegarantia_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_PrazoAtendeGarantia_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1182ContratoServicos_PrazoAtendeGarantia), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A1182ContratoServicos_PrazoAtendeGarantia), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_PrazoAtendeGarantia_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_prazocorrecaotipo_Internalname, "Corre��o", "", "", lblTextblockcontratoservicos_prazocorrecaotipo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table15_114_752( true) ;
         }
         else
         {
            wb_table15_114_752( false) ;
         }
         return  ;
      }

      protected void wb_table15_114_752e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_tmpestanl_Internalname, "Estimado de an�lise", "", "", lblTextblockcontratoservicos_tmpestanl_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table16_128_752( true) ;
         }
         else
         {
            wb_table16_128_752( false) ;
         }
         return  ;
      }

      protected void wb_table16_128_752e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_tmpestexc_Internalname, "Estimado de execu��o", "", "", lblTextblockcontratoservicos_tmpestexc_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table17_137_752( true) ;
         }
         else
         {
            wb_table17_137_752( false) ;
         }
         return  ;
      }

      protected void wb_table17_137_752e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_tmpestcrr_Internalname, "Estimado de corre��o", "", "", lblTextblockcontratoservicos_tmpestcrr_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table18_146_752( true) ;
         }
         else
         {
            wb_table18_146_752( false) ;
         }
         return  ;
      }

      protected void wb_table18_146_752e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_statuspagfnc_Internalname, "Pagar em", "", "", lblTextblockcontratoservicos_statuspagfnc_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoServicos_StatusPagFnc, cmbContratoServicos_StatusPagFnc_Internalname, StringUtil.RTrim( A1325ContratoServicos_StatusPagFnc), 1, cmbContratoServicos_StatusPagFnc_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ContratoServicosGeneral.htm");
            cmbContratoServicos_StatusPagFnc.CurrentValue = StringUtil.RTrim( A1325ContratoServicos_StatusPagFnc);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratoServicos_StatusPagFnc_Internalname, "Values", (String)(cmbContratoServicos_StatusPagFnc.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_68_752e( true) ;
         }
         else
         {
            wb_table6_68_752e( false) ;
         }
      }

      protected void wb_table18_146_752( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratoservicos_tmpestcrr_Internalname, tblTablemergedcontratoservicos_tmpestcrr_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_TmpEstCrr_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1502ContratoServicos_TmpEstCrr), 8, 0, ",", "")), context.localUtil.Format( (decimal)(A1502ContratoServicos_TmpEstCrr), "ZZZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_TmpEstCrr_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 70, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratoservicos_tmpestcrr_righttext_Internalname, " �min.����", "", "", lblContratoservicos_tmpestcrr_righttext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table18_146_752e( true) ;
         }
         else
         {
            wb_table18_146_752e( false) ;
         }
      }

      protected void wb_table17_137_752( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratoservicos_tmpestexc_Internalname, tblTablemergedcontratoservicos_tmpestexc_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_TmpEstExc_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1501ContratoServicos_TmpEstExc), 8, 0, ",", "")), context.localUtil.Format( (decimal)(A1501ContratoServicos_TmpEstExc), "ZZZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_TmpEstExc_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 70, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratoservicos_tmpestexc_righttext_Internalname, "�min.", "", "", lblContratoservicos_tmpestexc_righttext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table17_137_752e( true) ;
         }
         else
         {
            wb_table17_137_752e( false) ;
         }
      }

      protected void wb_table16_128_752( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratoservicos_tmpestanl_Internalname, tblTablemergedcontratoservicos_tmpestanl_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_TmpEstAnl_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1516ContratoServicos_TmpEstAnl), 8, 0, ",", "")), context.localUtil.Format( (decimal)(A1516ContratoServicos_TmpEstAnl), "ZZZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_TmpEstAnl_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 70, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratoservicos_tmpestanl_righttext_Internalname, "�min.", "", "", lblContratoservicos_tmpestanl_righttext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table16_128_752e( true) ;
         }
         else
         {
            wb_table16_128_752e( false) ;
         }
      }

      protected void wb_table15_114_752( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratoservicos_prazocorrecaotipo_Internalname, tblTablemergedcontratoservicos_prazocorrecaotipo_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoServicos_PrazoCorrecaoTipo, cmbContratoServicos_PrazoCorrecaoTipo_Internalname, StringUtil.RTrim( A1225ContratoServicos_PrazoCorrecaoTipo), 1, cmbContratoServicos_PrazoCorrecaoTipo_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ContratoServicosGeneral.htm");
            cmbContratoServicos_PrazoCorrecaoTipo.CurrentValue = StringUtil.RTrim( A1225ContratoServicos_PrazoCorrecaoTipo);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratoServicos_PrazoCorrecaoTipo_Internalname, "Values", (String)(cmbContratoServicos_PrazoCorrecaoTipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_prazocorrecao_Internalname, "", "", "", lblTextblockcontratoservicos_prazocorrecao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_PrazoCorrecao_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1224ContratoServicos_PrazoCorrecao), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A1224ContratoServicos_PrazoCorrecao), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_PrazoCorrecao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContratoservicos_prazocorrecao_righttext_cell_Internalname+"\"  class=''>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratoservicos_prazocorrecao_righttext_Internalname, "�-", "", "", lblContratoservicos_prazocorrecao_righttext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table15_114_752e( true) ;
         }
         else
         {
            wb_table15_114_752e( false) ;
         }
      }

      protected void wb_table14_88_752( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratoservicos_prazotipo_Internalname, tblTablemergedcontratoservicos_prazotipo_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoServicos_PrazoTipo, cmbContratoServicos_PrazoTipo_Internalname, StringUtil.RTrim( A913ContratoServicos_PrazoTipo), 1, cmbContratoServicos_PrazoTipo_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ContratoServicosGeneral.htm");
            cmbContratoServicos_PrazoTipo.CurrentValue = StringUtil.RTrim( A913ContratoServicos_PrazoTipo);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratoServicos_PrazoTipo_Internalname, "Values", (String)(cmbContratoServicos_PrazoTipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContratoservicos_prazodias_cell_Internalname+"\"  class='"+cellContratoservicos_prazodias_cell_Class+"'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_PrazoDias_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A914ContratoServicos_PrazoDias), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A914ContratoServicos_PrazoDias), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_PrazoDias_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtContratoServicos_PrazoDias_Visible, 0, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContratoservicos_prazodias_righttext_cell_Internalname+"\"  class='"+cellContratoservicos_prazodias_righttext_cell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratoservicos_prazodias_righttext_Internalname, "�dias", "", "", lblContratoservicos_prazodias_righttext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgUpdprazo_Internalname, context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Alterar prazo de entrega", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgUpdprazo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDPRAZO\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table14_88_752e( true) ;
         }
         else
         {
            wb_table14_88_752e( false) ;
         }
      }

      protected void wb_table5_40_752( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblServico_Internalname, tblServico_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservicogrupo_descricao_Internalname, "Grupo", "", "", lblTextblockservicogrupo_descricao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table19_45_752( true) ;
         }
         else
         {
            wb_table19_45_752( false) ;
         }
         return  ;
      }

      protected void wb_table19_45_752e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_nome_Internalname, "Nome", "", "", lblTextblockservico_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table20_57_752( true) ;
         }
         else
         {
            wb_table20_57_752( false) ;
         }
         return  ;
      }

      protected void wb_table20_57_752e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_40_752e( true) ;
         }
         else
         {
            wb_table5_40_752e( false) ;
         }
      }

      protected void wb_table20_57_752( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedservico_nome_Internalname, tblTablemergedservico_nome_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServico_Nome_Internalname, StringUtil.RTrim( A608Servico_Nome), StringUtil.RTrim( context.localUtil.Format( A608Servico_Nome, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 380, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_alias_Internalname, "Alias", "", "", lblTextblockcontratoservicos_alias_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_Alias_Internalname, StringUtil.RTrim( A1858ContratoServicos_Alias), StringUtil.RTrim( context.localUtil.Format( A1858ContratoServicos_Alias, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_Alias_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table20_57_752e( true) ;
         }
         else
         {
            wb_table20_57_752e( false) ;
         }
      }

      protected void wb_table19_45_752( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedservicogrupo_descricao_Internalname, tblTablemergedservicogrupo_descricao_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServicoGrupo_Descricao_Internalname, A158ServicoGrupo_Descricao, StringUtil.RTrim( context.localUtil.Format( A158ServicoGrupo_Descricao, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtServicoGrupo_Descricao_Link, "", "", "", edtServicoGrupo_Descricao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_servicosigla_Internalname, "Servi�o (Sigla)", "", "", lblTextblockcontratoservicos_servicosigla_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_ServicoSigla_Internalname, StringUtil.RTrim( A826ContratoServicos_ServicoSigla), StringUtil.RTrim( context.localUtil.Format( A826ContratoServicos_ServicoSigla, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_ServicoSigla_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table19_45_752e( true) ;
         }
         else
         {
            wb_table19_45_752e( false) ;
         }
      }

      protected void wb_table4_12_752( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblContrato_Internalname, tblContrato_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_numero_Internalname, "N� do Contrato", "", "", lblTextblockcontrato_numero_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Numero_Internalname, StringUtil.RTrim( A77Contrato_Numero), StringUtil.RTrim( context.localUtil.Format( A77Contrato_Numero, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Numero_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "NumeroContrato", "left", true, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_numeroata_Internalname, "N� da Ata", "", "", lblTextblockcontrato_numeroata_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_NumeroAta_Internalname, StringUtil.RTrim( A78Contrato_NumeroAta), StringUtil.RTrim( context.localUtil.Format( A78Contrato_NumeroAta, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_NumeroAta_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "NumeroAta", "left", true, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_ano_Internalname, "Ano", "", "", lblTextblockcontrato_ano_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Ano_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A79Contrato_Ano), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A79Contrato_Ano), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Ano_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 50, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "Ano", "right", false, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_pessoanom_Internalname, "Contratada", "", "", lblTextblockcontratada_pessoanom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_PessoaNom_Internalname, StringUtil.RTrim( A41Contratada_PessoaNom), StringUtil.RTrim( context.localUtil.Format( A41Contratada_PessoaNom, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_PessoaNom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_pessoacnpj_Internalname, "CNPJ", "", "", lblTextblockcontratada_pessoacnpj_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_PessoaCNPJ_Internalname, A42Contratada_PessoaCNPJ, StringUtil.RTrim( context.localUtil.Format( A42Contratada_PessoaCNPJ, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_PessoaCNPJ_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Docto", "left", true, "HLP_ContratoServicosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_12_752e( true) ;
         }
         else
         {
            wb_table4_12_752e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A160ContratoServicos_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA752( ) ;
         WS752( ) ;
         WE752( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA160ContratoServicos_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA752( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contratoservicosgeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA752( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A160ContratoServicos_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
         }
         wcpOA160ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA160ContratoServicos_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A160ContratoServicos_Codigo != wcpOA160ContratoServicos_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA160ContratoServicos_Codigo = cgiGet( sPrefix+"A160ContratoServicos_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA160ContratoServicos_Codigo) > 0 )
         {
            A160ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA160ContratoServicos_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
         }
         else
         {
            A160ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A160ContratoServicos_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA752( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS752( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS752( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A160ContratoServicos_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A160ContratoServicos_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA160ContratoServicos_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A160ContratoServicos_Codigo_CTRL", StringUtil.RTrim( sCtrlA160ContratoServicos_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE752( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203160111627");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contratoservicosgeneral.js", "?20203160111627");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontrato_numero_Internalname = sPrefix+"TEXTBLOCKCONTRATO_NUMERO";
         edtContrato_Numero_Internalname = sPrefix+"CONTRATO_NUMERO";
         lblTextblockcontrato_numeroata_Internalname = sPrefix+"TEXTBLOCKCONTRATO_NUMEROATA";
         edtContrato_NumeroAta_Internalname = sPrefix+"CONTRATO_NUMEROATA";
         lblTextblockcontrato_ano_Internalname = sPrefix+"TEXTBLOCKCONTRATO_ANO";
         edtContrato_Ano_Internalname = sPrefix+"CONTRATO_ANO";
         lblTextblockcontratada_pessoanom_Internalname = sPrefix+"TEXTBLOCKCONTRATADA_PESSOANOM";
         edtContratada_PessoaNom_Internalname = sPrefix+"CONTRATADA_PESSOANOM";
         lblTextblockcontratada_pessoacnpj_Internalname = sPrefix+"TEXTBLOCKCONTRATADA_PESSOACNPJ";
         edtContratada_PessoaCNPJ_Internalname = sPrefix+"CONTRATADA_PESSOACNPJ";
         tblContrato_Internalname = sPrefix+"CONTRATO";
         grpUnnamedgroup1_Internalname = sPrefix+"UNNAMEDGROUP1";
         lblTextblockservicogrupo_descricao_Internalname = sPrefix+"TEXTBLOCKSERVICOGRUPO_DESCRICAO";
         edtServicoGrupo_Descricao_Internalname = sPrefix+"SERVICOGRUPO_DESCRICAO";
         lblTextblockcontratoservicos_servicosigla_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOS_SERVICOSIGLA";
         edtContratoServicos_ServicoSigla_Internalname = sPrefix+"CONTRATOSERVICOS_SERVICOSIGLA";
         tblTablemergedservicogrupo_descricao_Internalname = sPrefix+"TABLEMERGEDSERVICOGRUPO_DESCRICAO";
         lblTextblockservico_nome_Internalname = sPrefix+"TEXTBLOCKSERVICO_NOME";
         edtServico_Nome_Internalname = sPrefix+"SERVICO_NOME";
         lblTextblockcontratoservicos_alias_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOS_ALIAS";
         edtContratoServicos_Alias_Internalname = sPrefix+"CONTRATOSERVICOS_ALIAS";
         tblTablemergedservico_nome_Internalname = sPrefix+"TABLEMERGEDSERVICO_NOME";
         tblServico_Internalname = sPrefix+"SERVICO";
         grpUnnamedgroup2_Internalname = sPrefix+"UNNAMEDGROUP2";
         lblTextblockcontratoservicos_prazotpdias_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOS_PRAZOTPDIAS";
         cmbContratoServicos_PrazoTpDias_Internalname = sPrefix+"CONTRATOSERVICOS_PRAZOTPDIAS";
         lblTextblockcontratoservicos_prazoinicio_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOS_PRAZOINICIO";
         cmbContratoServicos_PrazoInicio_Internalname = sPrefix+"CONTRATOSERVICOS_PRAZOINICIO";
         lblTextblockcontratoservicos_prazoanalise_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOS_PRAZOANALISE";
         edtContratoServicos_PrazoAnalise_Internalname = sPrefix+"CONTRATOSERVICOS_PRAZOANALISE";
         lblTextblockcontratoservicos_prazotipo_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOS_PRAZOTIPO";
         cmbContratoServicos_PrazoTipo_Internalname = sPrefix+"CONTRATOSERVICOS_PRAZOTIPO";
         edtContratoServicos_PrazoDias_Internalname = sPrefix+"CONTRATOSERVICOS_PRAZODIAS";
         cellContratoservicos_prazodias_cell_Internalname = sPrefix+"CONTRATOSERVICOS_PRAZODIAS_CELL";
         lblContratoservicos_prazodias_righttext_Internalname = sPrefix+"CONTRATOSERVICOS_PRAZODIAS_RIGHTTEXT";
         cellContratoservicos_prazodias_righttext_cell_Internalname = sPrefix+"CONTRATOSERVICOS_PRAZODIAS_RIGHTTEXT_CELL";
         imgUpdprazo_Internalname = sPrefix+"UPDPRAZO";
         tblTablemergedcontratoservicos_prazotipo_Internalname = sPrefix+"TABLEMERGEDCONTRATOSERVICOS_PRAZOTIPO";
         lblTextblockcontratoservicos_prazoresposta_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOS_PRAZORESPOSTA";
         edtContratoServicos_PrazoResposta_Internalname = sPrefix+"CONTRATOSERVICOS_PRAZORESPOSTA";
         lblTextblockcontratoservicos_prazogarantia_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOS_PRAZOGARANTIA";
         edtContratoServicos_PrazoGarantia_Internalname = sPrefix+"CONTRATOSERVICOS_PRAZOGARANTIA";
         lblTextblockcontratoservicos_prazoatendegarantia_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOS_PRAZOATENDEGARANTIA";
         edtContratoServicos_PrazoAtendeGarantia_Internalname = sPrefix+"CONTRATOSERVICOS_PRAZOATENDEGARANTIA";
         lblTextblockcontratoservicos_prazocorrecaotipo_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOS_PRAZOCORRECAOTIPO";
         cmbContratoServicos_PrazoCorrecaoTipo_Internalname = sPrefix+"CONTRATOSERVICOS_PRAZOCORRECAOTIPO";
         lblTextblockcontratoservicos_prazocorrecao_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOS_PRAZOCORRECAO";
         edtContratoServicos_PrazoCorrecao_Internalname = sPrefix+"CONTRATOSERVICOS_PRAZOCORRECAO";
         lblContratoservicos_prazocorrecao_righttext_Internalname = sPrefix+"CONTRATOSERVICOS_PRAZOCORRECAO_RIGHTTEXT";
         cellContratoservicos_prazocorrecao_righttext_cell_Internalname = sPrefix+"CONTRATOSERVICOS_PRAZOCORRECAO_RIGHTTEXT_CELL";
         tblTablemergedcontratoservicos_prazocorrecaotipo_Internalname = sPrefix+"TABLEMERGEDCONTRATOSERVICOS_PRAZOCORRECAOTIPO";
         lblTextblockcontratoservicos_tmpestanl_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOS_TMPESTANL";
         edtContratoServicos_TmpEstAnl_Internalname = sPrefix+"CONTRATOSERVICOS_TMPESTANL";
         lblContratoservicos_tmpestanl_righttext_Internalname = sPrefix+"CONTRATOSERVICOS_TMPESTANL_RIGHTTEXT";
         tblTablemergedcontratoservicos_tmpestanl_Internalname = sPrefix+"TABLEMERGEDCONTRATOSERVICOS_TMPESTANL";
         lblTextblockcontratoservicos_tmpestexc_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOS_TMPESTEXC";
         edtContratoServicos_TmpEstExc_Internalname = sPrefix+"CONTRATOSERVICOS_TMPESTEXC";
         lblContratoservicos_tmpestexc_righttext_Internalname = sPrefix+"CONTRATOSERVICOS_TMPESTEXC_RIGHTTEXT";
         tblTablemergedcontratoservicos_tmpestexc_Internalname = sPrefix+"TABLEMERGEDCONTRATOSERVICOS_TMPESTEXC";
         lblTextblockcontratoservicos_tmpestcrr_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOS_TMPESTCRR";
         edtContratoServicos_TmpEstCrr_Internalname = sPrefix+"CONTRATOSERVICOS_TMPESTCRR";
         lblContratoservicos_tmpestcrr_righttext_Internalname = sPrefix+"CONTRATOSERVICOS_TMPESTCRR_RIGHTTEXT";
         tblTablemergedcontratoservicos_tmpestcrr_Internalname = sPrefix+"TABLEMERGEDCONTRATOSERVICOS_TMPESTCRR";
         lblTextblockcontratoservicos_statuspagfnc_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOS_STATUSPAGFNC";
         cmbContratoServicos_StatusPagFnc_Internalname = sPrefix+"CONTRATOSERVICOS_STATUSPAGFNC";
         tblPrazos_Internalname = sPrefix+"PRAZOS";
         grpUnnamedgroup3_Internalname = sPrefix+"UNNAMEDGROUP3";
         lblTextblockcontratoservicos_indicedivergencia_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOS_INDICEDIVERGENCIA";
         edtContratoServicos_IndiceDivergencia_Internalname = sPrefix+"CONTRATOSERVICOS_INDICEDIVERGENCIA";
         lblContratoservicos_indicedivergencia_righttext_Internalname = sPrefix+"CONTRATOSERVICOS_INDICEDIVERGENCIA_RIGHTTEXT";
         tblTablemergedcontratoservicos_indicedivergencia_Internalname = sPrefix+"TABLEMERGEDCONTRATOSERVICOS_INDICEDIVERGENCIA";
         lblTextblockcontratoservicos_naorequeratr_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOS_NAOREQUERATR";
         chkContratoServicos_NaoRequerAtr_Internalname = sPrefix+"CONTRATOSERVICOS_NAOREQUERATR";
         lblTextblockcontratoservicos_limiteproposta_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOS_LIMITEPROPOSTA";
         edtContratoServicos_LimiteProposta_Internalname = sPrefix+"CONTRATOSERVICOS_LIMITEPROPOSTA";
         lblContratoservicos_limiteproposta_righttext_Internalname = sPrefix+"CONTRATOSERVICOS_LIMITEPROPOSTA_RIGHTTEXT";
         tblTablemergedcontratoservicos_limiteproposta_Internalname = sPrefix+"TABLEMERGEDCONTRATOSERVICOS_LIMITEPROPOSTA";
         lblTextblockcontratoservicos_localexec_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOS_LOCALEXEC";
         cmbContratoServicos_LocalExec_Internalname = sPrefix+"CONTRATOSERVICOS_LOCALEXEC";
         lblTextblockcontratoservicos_undcntnome_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOS_UNDCNTNOME";
         edtContratoServicos_UndCntNome_Internalname = sPrefix+"CONTRATOSERVICOS_UNDCNTNOME";
         lblTextblockcontratoservicos_fatorcnvundcnt_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOS_FATORCNVUNDCNT";
         edtContratoServicos_FatorCnvUndCnt_Internalname = sPrefix+"CONTRATOSERVICOS_FATORCNVUNDCNT";
         lblTextblockservico_qtdcontratada_Internalname = sPrefix+"TEXTBLOCKSERVICO_QTDCONTRATADA";
         edtServico_QtdContratada_Internalname = sPrefix+"SERVICO_QTDCONTRATADA";
         lblTextblockservico_vlrunidadecontratada_Internalname = sPrefix+"TEXTBLOCKSERVICO_VLRUNIDADECONTRATADA";
         edtServico_VlrUnidadeContratada_Internalname = sPrefix+"SERVICO_VLRUNIDADECONTRATADA";
         lblTextblockcontratoservicos_produtividade_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOS_PRODUTIVIDADE";
         edtContratoServicos_Produtividade_Internalname = sPrefix+"CONTRATOSERVICOS_PRODUTIVIDADE";
         lblContratoservicos_produtividade_righttext_Internalname = sPrefix+"CONTRATOSERVICOS_PRODUTIVIDADE_RIGHTTEXT";
         tblTablemergedcontratoservicos_produtividade_Internalname = sPrefix+"TABLEMERGEDCONTRATOSERVICOS_PRODUTIVIDADE";
         lblTextblockservicocontrato_faturamento_Internalname = sPrefix+"TEXTBLOCKSERVICOCONTRATO_FATURAMENTO";
         cmbServicoContrato_Faturamento_Internalname = sPrefix+"SERVICOCONTRATO_FATURAMENTO";
         lblTextblockservico_percentual_Internalname = sPrefix+"TEXTBLOCKSERVICO_PERCENTUAL";
         edtavServico_percentual_Internalname = sPrefix+"vSERVICO_PERCENTUAL";
         lblServico_percentual_righttext_Internalname = sPrefix+"SERVICO_PERCENTUAL_RIGHTTEXT";
         tblTablemergedservico_percentual_Internalname = sPrefix+"TABLEMERGEDSERVICO_PERCENTUAL";
         lblTextblockcontratoservicos_calculormn_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOS_CALCULORMN";
         cmbContratoServicos_CalculoRmn_Internalname = sPrefix+"CONTRATOSERVICOS_CALCULORMN";
         lblTextblockcontratoservicos_perccnc_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOS_PERCCNC";
         edtContratoServicos_PercCnc_Internalname = sPrefix+"CONTRATOSERVICOS_PERCCNC";
         lblContratoservicos_perccnc_righttext_Internalname = sPrefix+"CONTRATOSERVICOS_PERCCNC_RIGHTTEXT";
         tblTablemergedcontratoservicos_perccnc_Internalname = sPrefix+"TABLEMERGEDCONTRATOSERVICOS_PERCCNC";
         lblTextblockcontratoservicos_espelhaaceite_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOS_ESPELHAACEITE";
         chkContratoServicos_EspelhaAceite_Internalname = sPrefix+"CONTRATOSERVICOS_ESPELHAACEITE";
         lblTextblockcontratoservicos_momento_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOS_MOMENTO";
         cmbContratoServicos_Momento_Internalname = sPrefix+"CONTRATOSERVICOS_MOMENTO";
         lblTextblockcontratoservicos_tipovnc_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOS_TIPOVNC";
         cmbContratoServicos_TipoVnc_Internalname = sPrefix+"CONTRATOSERVICOS_TIPOVNC";
         lblTextblockcontratoservicos_qntuntcns_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOS_QNTUNTCNS";
         edtContratoServicos_QntUntCns_Internalname = sPrefix+"CONTRATOSERVICOS_QNTUNTCNS";
         lblTextblockcontratoservicos_codigofiscal_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOS_CODIGOFISCAL";
         edtContratoServicos_CodigoFiscal_Internalname = sPrefix+"CONTRATOSERVICOS_CODIGOFISCAL";
         lblTextblockcontratoservicos_solicitagestorsistema_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOS_SOLICITAGESTORSISTEMA";
         chkContratoServicos_SolicitaGestorSistema_Internalname = sPrefix+"CONTRATOSERVICOS_SOLICITAGESTORSISTEMA";
         imgContratoservicos_solicitagestorsistema_infoicon_Internalname = sPrefix+"CONTRATOSERVICOS_SOLICITAGESTORSISTEMA_INFOICON";
         tblTablemergedcontratoservicos_solicitagestorsistema_Internalname = sPrefix+"TABLEMERGEDCONTRATOSERVICOS_SOLICITAGESTORSISTEMA";
         lblTextblockcontratoservicos_ativo_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOS_ATIVO";
         chkContratoServicos_Ativo_Internalname = sPrefix+"CONTRATOSERVICOS_ATIVO";
         tblParametros_Internalname = sPrefix+"PARAMETROS";
         grpUnnamedgroup4_Internalname = sPrefix+"UNNAMEDGROUP4";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         bttBtnfechar_Internalname = sPrefix+"BTNFECHAR";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtContratada_PessoaCNPJ_Jsonclick = "";
         edtContratada_PessoaNom_Jsonclick = "";
         edtContrato_Ano_Jsonclick = "";
         edtContrato_NumeroAta_Jsonclick = "";
         edtContrato_Numero_Jsonclick = "";
         edtContratoServicos_ServicoSigla_Jsonclick = "";
         edtServicoGrupo_Descricao_Jsonclick = "";
         edtContratoServicos_Alias_Jsonclick = "";
         edtServico_Nome_Jsonclick = "";
         cellContratoservicos_prazodias_righttext_cell_Class = "";
         edtContratoServicos_PrazoDias_Jsonclick = "";
         cellContratoservicos_prazodias_cell_Class = "";
         cmbContratoServicos_PrazoTipo_Jsonclick = "";
         edtContratoServicos_PrazoCorrecao_Jsonclick = "";
         cmbContratoServicos_PrazoCorrecaoTipo_Jsonclick = "";
         edtContratoServicos_TmpEstAnl_Jsonclick = "";
         edtContratoServicos_TmpEstExc_Jsonclick = "";
         edtContratoServicos_TmpEstCrr_Jsonclick = "";
         cmbContratoServicos_StatusPagFnc_Jsonclick = "";
         edtContratoServicos_PrazoAtendeGarantia_Jsonclick = "";
         edtContratoServicos_PrazoGarantia_Jsonclick = "";
         edtContratoServicos_PrazoResposta_Jsonclick = "";
         edtContratoServicos_PrazoAnalise_Jsonclick = "";
         cmbContratoServicos_PrazoInicio_Jsonclick = "";
         cmbContratoServicos_PrazoTpDias_Jsonclick = "";
         edtContratoServicos_IndiceDivergencia_Jsonclick = "";
         edtContratoServicos_LimiteProposta_Jsonclick = "";
         edtContratoServicos_Produtividade_Jsonclick = "";
         edtavServico_percentual_Jsonclick = "";
         edtavServico_percentual_Enabled = 0;
         edtContratoServicos_PercCnc_Jsonclick = "";
         edtContratoServicos_CodigoFiscal_Jsonclick = "";
         edtContratoServicos_QntUntCns_Jsonclick = "";
         cmbContratoServicos_TipoVnc_Jsonclick = "";
         cmbContratoServicos_Momento_Jsonclick = "";
         cmbContratoServicos_CalculoRmn_Jsonclick = "";
         cmbServicoContrato_Faturamento_Jsonclick = "";
         edtServico_VlrUnidadeContratada_Jsonclick = "";
         edtServico_QtdContratada_Jsonclick = "";
         edtContratoServicos_FatorCnvUndCnt_Jsonclick = "";
         edtContratoServicos_UndCntNome_Jsonclick = "";
         cmbContratoServicos_LocalExec_Jsonclick = "";
         bttBtndelete_Visible = 1;
         bttBtnupdate_Visible = 1;
         edtContratoServicos_PrazoDias_Visible = 1;
         edtServicoGrupo_Descricao_Link = "";
         chkContratoServicos_Ativo.Caption = "";
         chkContratoServicos_SolicitaGestorSistema.Caption = "";
         chkContratoServicos_EspelhaAceite.Caption = "";
         chkContratoServicos_NaoRequerAtr.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13752',iparms:[{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV14Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV14Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DODELETE'","{handler:'E14752',iparms:[{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV14Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV14Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DOFECHAR'","{handler:'E15752',iparms:[{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DOUPDPRAZO'","{handler:'E16752',iparms:[{av:'A911ContratoServicos_Prazos',fld:'CONTRATOSERVICOS_PRAZOS',pic:'ZZZ9',nv:0},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV17Pgmname = "";
         scmdbuf = "";
         H00753_A911ContratoServicos_Prazos = new short[1] ;
         H00753_n911ContratoServicos_Prazos = new bool[] {false} ;
         H00754_A914ContratoServicos_PrazoDias = new short[1] ;
         H00754_n914ContratoServicos_PrazoDias = new bool[] {false} ;
         H00754_A913ContratoServicos_PrazoTipo = new String[] {""} ;
         H00754_n913ContratoServicos_PrazoTipo = new bool[] {false} ;
         A913ContratoServicos_PrazoTipo = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A605Servico_Sigla = "";
         A826ContratoServicos_ServicoSigla = "";
         A1858ContratoServicos_Alias = "";
         A1454ContratoServicos_PrazoTpDias = "";
         A1225ContratoServicos_PrazoCorrecaoTipo = "";
         A1325ContratoServicos_StatusPagFnc = "";
         A639ContratoServicos_LocalExec = "";
         A607ServicoContrato_Faturamento = "";
         A1266ContratoServicos_Momento = "";
         A868ContratoServicos_TipoVnc = "";
         A1723ContratoServicos_CodigoFiscal = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         H00756_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         H00756_A155Servico_Codigo = new int[1] ;
         H00756_A39Contratada_Codigo = new int[1] ;
         H00756_A40Contratada_PessoaCod = new int[1] ;
         H00756_A1212ContratoServicos_UnidadeContratada = new int[1] ;
         H00756_A160ContratoServicos_Codigo = new int[1] ;
         H00756_A558Servico_Percentual = new decimal[1] ;
         H00756_n558Servico_Percentual = new bool[] {false} ;
         H00756_A157ServicoGrupo_Codigo = new int[1] ;
         H00756_A74Contrato_Codigo = new int[1] ;
         H00756_A638ContratoServicos_Ativo = new bool[] {false} ;
         H00756_A2094ContratoServicos_SolicitaGestorSistema = new bool[] {false} ;
         H00756_A1723ContratoServicos_CodigoFiscal = new String[] {""} ;
         H00756_n1723ContratoServicos_CodigoFiscal = new bool[] {false} ;
         H00756_A1340ContratoServicos_QntUntCns = new decimal[1] ;
         H00756_n1340ContratoServicos_QntUntCns = new bool[] {false} ;
         H00756_A868ContratoServicos_TipoVnc = new String[] {""} ;
         H00756_n868ContratoServicos_TipoVnc = new bool[] {false} ;
         H00756_A1266ContratoServicos_Momento = new String[] {""} ;
         H00756_n1266ContratoServicos_Momento = new bool[] {false} ;
         H00756_A1217ContratoServicos_EspelhaAceite = new bool[] {false} ;
         H00756_n1217ContratoServicos_EspelhaAceite = new bool[] {false} ;
         H00756_A1539ContratoServicos_PercCnc = new short[1] ;
         H00756_n1539ContratoServicos_PercCnc = new bool[] {false} ;
         H00756_A2074ContratoServicos_CalculoRmn = new short[1] ;
         H00756_n2074ContratoServicos_CalculoRmn = new bool[] {false} ;
         H00756_A607ServicoContrato_Faturamento = new String[] {""} ;
         H00756_A1191ContratoServicos_Produtividade = new decimal[1] ;
         H00756_n1191ContratoServicos_Produtividade = new bool[] {false} ;
         H00756_A557Servico_VlrUnidadeContratada = new decimal[1] ;
         H00756_A555Servico_QtdContratada = new long[1] ;
         H00756_n555Servico_QtdContratada = new bool[] {false} ;
         H00756_A1341ContratoServicos_FatorCnvUndCnt = new decimal[1] ;
         H00756_n1341ContratoServicos_FatorCnvUndCnt = new bool[] {false} ;
         H00756_A2132ContratoServicos_UndCntNome = new String[] {""} ;
         H00756_n2132ContratoServicos_UndCntNome = new bool[] {false} ;
         H00756_A639ContratoServicos_LocalExec = new String[] {""} ;
         H00756_A1799ContratoServicos_LimiteProposta = new decimal[1] ;
         H00756_n1799ContratoServicos_LimiteProposta = new bool[] {false} ;
         H00756_A1397ContratoServicos_NaoRequerAtr = new bool[] {false} ;
         H00756_n1397ContratoServicos_NaoRequerAtr = new bool[] {false} ;
         H00756_A1455ContratoServicos_IndiceDivergencia = new decimal[1] ;
         H00756_n1455ContratoServicos_IndiceDivergencia = new bool[] {false} ;
         H00756_A1325ContratoServicos_StatusPagFnc = new String[] {""} ;
         H00756_n1325ContratoServicos_StatusPagFnc = new bool[] {false} ;
         H00756_A1502ContratoServicos_TmpEstCrr = new int[1] ;
         H00756_n1502ContratoServicos_TmpEstCrr = new bool[] {false} ;
         H00756_A1501ContratoServicos_TmpEstExc = new int[1] ;
         H00756_n1501ContratoServicos_TmpEstExc = new bool[] {false} ;
         H00756_A1516ContratoServicos_TmpEstAnl = new int[1] ;
         H00756_n1516ContratoServicos_TmpEstAnl = new bool[] {false} ;
         H00756_A1224ContratoServicos_PrazoCorrecao = new short[1] ;
         H00756_n1224ContratoServicos_PrazoCorrecao = new bool[] {false} ;
         H00756_A1225ContratoServicos_PrazoCorrecaoTipo = new String[] {""} ;
         H00756_n1225ContratoServicos_PrazoCorrecaoTipo = new bool[] {false} ;
         H00756_A1182ContratoServicos_PrazoAtendeGarantia = new short[1] ;
         H00756_n1182ContratoServicos_PrazoAtendeGarantia = new bool[] {false} ;
         H00756_A1181ContratoServicos_PrazoGarantia = new short[1] ;
         H00756_n1181ContratoServicos_PrazoGarantia = new bool[] {false} ;
         H00756_A1153ContratoServicos_PrazoResposta = new short[1] ;
         H00756_n1153ContratoServicos_PrazoResposta = new bool[] {false} ;
         H00756_A1152ContratoServicos_PrazoAnalise = new short[1] ;
         H00756_n1152ContratoServicos_PrazoAnalise = new bool[] {false} ;
         H00756_A1649ContratoServicos_PrazoInicio = new short[1] ;
         H00756_n1649ContratoServicos_PrazoInicio = new bool[] {false} ;
         H00756_A1454ContratoServicos_PrazoTpDias = new String[] {""} ;
         H00756_n1454ContratoServicos_PrazoTpDias = new bool[] {false} ;
         H00756_A1858ContratoServicos_Alias = new String[] {""} ;
         H00756_n1858ContratoServicos_Alias = new bool[] {false} ;
         H00756_A608Servico_Nome = new String[] {""} ;
         H00756_A158ServicoGrupo_Descricao = new String[] {""} ;
         H00756_A42Contratada_PessoaCNPJ = new String[] {""} ;
         H00756_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         H00756_A41Contratada_PessoaNom = new String[] {""} ;
         H00756_n41Contratada_PessoaNom = new bool[] {false} ;
         H00756_A79Contrato_Ano = new short[1] ;
         H00756_A78Contrato_NumeroAta = new String[] {""} ;
         H00756_n78Contrato_NumeroAta = new bool[] {false} ;
         H00756_A77Contrato_Numero = new String[] {""} ;
         H00756_A911ContratoServicos_Prazos = new short[1] ;
         H00756_n911ContratoServicos_Prazos = new bool[] {false} ;
         H00756_A914ContratoServicos_PrazoDias = new short[1] ;
         H00756_n914ContratoServicos_PrazoDias = new bool[] {false} ;
         H00756_A913ContratoServicos_PrazoTipo = new String[] {""} ;
         H00756_n913ContratoServicos_PrazoTipo = new bool[] {false} ;
         H00756_A605Servico_Sigla = new String[] {""} ;
         A2132ContratoServicos_UndCntNome = "";
         A608Servico_Nome = "";
         A158ServicoGrupo_Descricao = "";
         A42Contratada_PessoaCNPJ = "";
         A41Contratada_PessoaNom = "";
         A78Contrato_NumeroAta = "";
         A77Contrato_Numero = "";
         H00758_A911ContratoServicos_Prazos = new short[1] ;
         H00758_n911ContratoServicos_Prazos = new bool[] {false} ;
         H00759_A914ContratoServicos_PrazoDias = new short[1] ;
         H00759_n914ContratoServicos_PrazoDias = new bool[] {false} ;
         H00759_A913ContratoServicos_PrazoTipo = new String[] {""} ;
         H00759_n913ContratoServicos_PrazoTipo = new bool[] {false} ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         lblTextblockcontratoservicos_indicedivergencia_Jsonclick = "";
         lblTextblockcontratoservicos_naorequeratr_Jsonclick = "";
         lblTextblockcontratoservicos_limiteproposta_Jsonclick = "";
         lblTextblockcontratoservicos_localexec_Jsonclick = "";
         lblTextblockcontratoservicos_undcntnome_Jsonclick = "";
         lblTextblockcontratoservicos_fatorcnvundcnt_Jsonclick = "";
         lblTextblockservico_qtdcontratada_Jsonclick = "";
         lblTextblockservico_vlrunidadecontratada_Jsonclick = "";
         lblTextblockcontratoservicos_produtividade_Jsonclick = "";
         lblTextblockservicocontrato_faturamento_Jsonclick = "";
         lblTextblockservico_percentual_Jsonclick = "";
         lblTextblockcontratoservicos_calculormn_Jsonclick = "";
         lblTextblockcontratoservicos_perccnc_Jsonclick = "";
         lblTextblockcontratoservicos_espelhaaceite_Jsonclick = "";
         lblTextblockcontratoservicos_momento_Jsonclick = "";
         lblTextblockcontratoservicos_tipovnc_Jsonclick = "";
         lblTextblockcontratoservicos_qntuntcns_Jsonclick = "";
         lblTextblockcontratoservicos_codigofiscal_Jsonclick = "";
         lblTextblockcontratoservicos_solicitagestorsistema_Jsonclick = "";
         lblTextblockcontratoservicos_ativo_Jsonclick = "";
         lblContratoservicos_perccnc_righttext_Jsonclick = "";
         lblServico_percentual_righttext_Jsonclick = "";
         lblContratoservicos_produtividade_righttext_Jsonclick = "";
         lblContratoservicos_limiteproposta_righttext_Jsonclick = "";
         lblContratoservicos_indicedivergencia_righttext_Jsonclick = "";
         lblTextblockcontratoservicos_prazotpdias_Jsonclick = "";
         lblTextblockcontratoservicos_prazoinicio_Jsonclick = "";
         lblTextblockcontratoservicos_prazoanalise_Jsonclick = "";
         lblTextblockcontratoservicos_prazotipo_Jsonclick = "";
         lblTextblockcontratoservicos_prazoresposta_Jsonclick = "";
         lblTextblockcontratoservicos_prazogarantia_Jsonclick = "";
         lblTextblockcontratoservicos_prazoatendegarantia_Jsonclick = "";
         lblTextblockcontratoservicos_prazocorrecaotipo_Jsonclick = "";
         lblTextblockcontratoservicos_tmpestanl_Jsonclick = "";
         lblTextblockcontratoservicos_tmpestexc_Jsonclick = "";
         lblTextblockcontratoservicos_tmpestcrr_Jsonclick = "";
         lblTextblockcontratoservicos_statuspagfnc_Jsonclick = "";
         lblContratoservicos_tmpestcrr_righttext_Jsonclick = "";
         lblContratoservicos_tmpestexc_righttext_Jsonclick = "";
         lblContratoservicos_tmpestanl_righttext_Jsonclick = "";
         lblTextblockcontratoservicos_prazocorrecao_Jsonclick = "";
         lblContratoservicos_prazocorrecao_righttext_Jsonclick = "";
         lblContratoservicos_prazodias_righttext_Jsonclick = "";
         imgUpdprazo_Jsonclick = "";
         lblTextblockservicogrupo_descricao_Jsonclick = "";
         lblTextblockservico_nome_Jsonclick = "";
         lblTextblockcontratoservicos_alias_Jsonclick = "";
         lblTextblockcontratoservicos_servicosigla_Jsonclick = "";
         lblTextblockcontrato_numero_Jsonclick = "";
         lblTextblockcontrato_numeroata_Jsonclick = "";
         lblTextblockcontrato_ano_Jsonclick = "";
         lblTextblockcontratada_pessoanom_Jsonclick = "";
         lblTextblockcontratada_pessoacnpj_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA160ContratoServicos_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratoservicosgeneral__default(),
            new Object[][] {
                new Object[] {
               H00753_A911ContratoServicos_Prazos, H00753_n911ContratoServicos_Prazos
               }
               , new Object[] {
               H00754_A914ContratoServicos_PrazoDias, H00754_n914ContratoServicos_PrazoDias, H00754_A913ContratoServicos_PrazoTipo, H00754_n913ContratoServicos_PrazoTipo
               }
               , new Object[] {
               H00756_A903ContratoServicosPrazo_CntSrvCod, H00756_A155Servico_Codigo, H00756_A39Contratada_Codigo, H00756_A40Contratada_PessoaCod, H00756_A1212ContratoServicos_UnidadeContratada, H00756_A160ContratoServicos_Codigo, H00756_A558Servico_Percentual, H00756_n558Servico_Percentual, H00756_A157ServicoGrupo_Codigo, H00756_A74Contrato_Codigo,
               H00756_A638ContratoServicos_Ativo, H00756_A2094ContratoServicos_SolicitaGestorSistema, H00756_A1723ContratoServicos_CodigoFiscal, H00756_n1723ContratoServicos_CodigoFiscal, H00756_A1340ContratoServicos_QntUntCns, H00756_n1340ContratoServicos_QntUntCns, H00756_A868ContratoServicos_TipoVnc, H00756_n868ContratoServicos_TipoVnc, H00756_A1266ContratoServicos_Momento, H00756_n1266ContratoServicos_Momento,
               H00756_A1217ContratoServicos_EspelhaAceite, H00756_n1217ContratoServicos_EspelhaAceite, H00756_A1539ContratoServicos_PercCnc, H00756_n1539ContratoServicos_PercCnc, H00756_A2074ContratoServicos_CalculoRmn, H00756_n2074ContratoServicos_CalculoRmn, H00756_A607ServicoContrato_Faturamento, H00756_A1191ContratoServicos_Produtividade, H00756_n1191ContratoServicos_Produtividade, H00756_A557Servico_VlrUnidadeContratada,
               H00756_A555Servico_QtdContratada, H00756_n555Servico_QtdContratada, H00756_A1341ContratoServicos_FatorCnvUndCnt, H00756_n1341ContratoServicos_FatorCnvUndCnt, H00756_A2132ContratoServicos_UndCntNome, H00756_n2132ContratoServicos_UndCntNome, H00756_A639ContratoServicos_LocalExec, H00756_A1799ContratoServicos_LimiteProposta, H00756_n1799ContratoServicos_LimiteProposta, H00756_A1397ContratoServicos_NaoRequerAtr,
               H00756_n1397ContratoServicos_NaoRequerAtr, H00756_A1455ContratoServicos_IndiceDivergencia, H00756_n1455ContratoServicos_IndiceDivergencia, H00756_A1325ContratoServicos_StatusPagFnc, H00756_n1325ContratoServicos_StatusPagFnc, H00756_A1502ContratoServicos_TmpEstCrr, H00756_n1502ContratoServicos_TmpEstCrr, H00756_A1501ContratoServicos_TmpEstExc, H00756_n1501ContratoServicos_TmpEstExc, H00756_A1516ContratoServicos_TmpEstAnl,
               H00756_n1516ContratoServicos_TmpEstAnl, H00756_A1224ContratoServicos_PrazoCorrecao, H00756_n1224ContratoServicos_PrazoCorrecao, H00756_A1225ContratoServicos_PrazoCorrecaoTipo, H00756_n1225ContratoServicos_PrazoCorrecaoTipo, H00756_A1182ContratoServicos_PrazoAtendeGarantia, H00756_n1182ContratoServicos_PrazoAtendeGarantia, H00756_A1181ContratoServicos_PrazoGarantia, H00756_n1181ContratoServicos_PrazoGarantia, H00756_A1153ContratoServicos_PrazoResposta,
               H00756_n1153ContratoServicos_PrazoResposta, H00756_A1152ContratoServicos_PrazoAnalise, H00756_n1152ContratoServicos_PrazoAnalise, H00756_A1649ContratoServicos_PrazoInicio, H00756_n1649ContratoServicos_PrazoInicio, H00756_A1454ContratoServicos_PrazoTpDias, H00756_n1454ContratoServicos_PrazoTpDias, H00756_A1858ContratoServicos_Alias, H00756_n1858ContratoServicos_Alias, H00756_A608Servico_Nome,
               H00756_A158ServicoGrupo_Descricao, H00756_A42Contratada_PessoaCNPJ, H00756_n42Contratada_PessoaCNPJ, H00756_A41Contratada_PessoaNom, H00756_n41Contratada_PessoaNom, H00756_A79Contrato_Ano, H00756_A78Contrato_NumeroAta, H00756_n78Contrato_NumeroAta, H00756_A77Contrato_Numero, H00756_A911ContratoServicos_Prazos,
               H00756_n911ContratoServicos_Prazos, H00756_A914ContratoServicos_PrazoDias, H00756_n914ContratoServicos_PrazoDias, H00756_A913ContratoServicos_PrazoTipo, H00756_n913ContratoServicos_PrazoTipo, H00756_A605Servico_Sigla
               }
               , new Object[] {
               H00758_A911ContratoServicos_Prazos, H00758_n911ContratoServicos_Prazos
               }
               , new Object[] {
               H00759_A914ContratoServicos_PrazoDias, H00759_n914ContratoServicos_PrazoDias, H00759_A913ContratoServicos_PrazoTipo, H00759_n913ContratoServicos_PrazoTipo
               }
            }
         );
         AV17Pgmname = "ContratoServicosGeneral";
         /* GeneXus formulas. */
         AV17Pgmname = "ContratoServicosGeneral";
         context.Gx_err = 0;
         edtavServico_percentual_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short A911ContratoServicos_Prazos ;
      private short A914ContratoServicos_PrazoDias ;
      private short A1649ContratoServicos_PrazoInicio ;
      private short A1152ContratoServicos_PrazoAnalise ;
      private short A1153ContratoServicos_PrazoResposta ;
      private short A1181ContratoServicos_PrazoGarantia ;
      private short A1182ContratoServicos_PrazoAtendeGarantia ;
      private short A1224ContratoServicos_PrazoCorrecao ;
      private short A2074ContratoServicos_CalculoRmn ;
      private short A1539ContratoServicos_PercCnc ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short A79Contrato_Ano ;
      private short nGXWrapped ;
      private int A160ContratoServicos_Codigo ;
      private int wcpOA160ContratoServicos_Codigo ;
      private int edtavServico_percentual_Enabled ;
      private int AV14Contrato_Codigo ;
      private int A74Contrato_Codigo ;
      private int A1516ContratoServicos_TmpEstAnl ;
      private int A1501ContratoServicos_TmpEstExc ;
      private int A1502ContratoServicos_TmpEstCrr ;
      private int A155Servico_Codigo ;
      private int A39Contratada_Codigo ;
      private int A40Contratada_PessoaCod ;
      private int A1212ContratoServicos_UnidadeContratada ;
      private int A157ServicoGrupo_Codigo ;
      private int bttBtnupdate_Visible ;
      private int bttBtndelete_Visible ;
      private int edtContratoServicos_PrazoDias_Visible ;
      private int AV7ContratoServicos_Codigo ;
      private int idxLst ;
      private long A555Servico_QtdContratada ;
      private decimal A558Servico_Percentual ;
      private decimal A1455ContratoServicos_IndiceDivergencia ;
      private decimal A1799ContratoServicos_LimiteProposta ;
      private decimal A1341ContratoServicos_FatorCnvUndCnt ;
      private decimal A557Servico_VlrUnidadeContratada ;
      private decimal A1191ContratoServicos_Produtividade ;
      private decimal AV13Servico_Percentual ;
      private decimal A1340ContratoServicos_QntUntCns ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV17Pgmname ;
      private String edtavServico_percentual_Internalname ;
      private String scmdbuf ;
      private String A913ContratoServicos_PrazoTipo ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A605Servico_Sigla ;
      private String A826ContratoServicos_ServicoSigla ;
      private String A1858ContratoServicos_Alias ;
      private String A1454ContratoServicos_PrazoTpDias ;
      private String A1225ContratoServicos_PrazoCorrecaoTipo ;
      private String A1325ContratoServicos_StatusPagFnc ;
      private String A639ContratoServicos_LocalExec ;
      private String A1266ContratoServicos_Momento ;
      private String A868ContratoServicos_TipoVnc ;
      private String A1723ContratoServicos_CodigoFiscal ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String chkContratoServicos_NaoRequerAtr_Internalname ;
      private String chkContratoServicos_EspelhaAceite_Internalname ;
      private String chkContratoServicos_SolicitaGestorSistema_Internalname ;
      private String chkContratoServicos_Ativo_Internalname ;
      private String A2132ContratoServicos_UndCntNome ;
      private String A608Servico_Nome ;
      private String A41Contratada_PessoaNom ;
      private String A78Contrato_NumeroAta ;
      private String A77Contrato_Numero ;
      private String edtContrato_Numero_Internalname ;
      private String edtContrato_NumeroAta_Internalname ;
      private String edtContrato_Ano_Internalname ;
      private String edtContratada_PessoaNom_Internalname ;
      private String edtContratada_PessoaCNPJ_Internalname ;
      private String edtServicoGrupo_Descricao_Internalname ;
      private String edtContratoServicos_ServicoSigla_Internalname ;
      private String edtServico_Nome_Internalname ;
      private String edtContratoServicos_Alias_Internalname ;
      private String cmbContratoServicos_PrazoTpDias_Internalname ;
      private String cmbContratoServicos_PrazoInicio_Internalname ;
      private String edtContratoServicos_PrazoAnalise_Internalname ;
      private String cmbContratoServicos_PrazoTipo_Internalname ;
      private String edtContratoServicos_PrazoDias_Internalname ;
      private String edtContratoServicos_PrazoResposta_Internalname ;
      private String edtContratoServicos_PrazoGarantia_Internalname ;
      private String edtContratoServicos_PrazoAtendeGarantia_Internalname ;
      private String cmbContratoServicos_PrazoCorrecaoTipo_Internalname ;
      private String edtContratoServicos_PrazoCorrecao_Internalname ;
      private String edtContratoServicos_TmpEstAnl_Internalname ;
      private String edtContratoServicos_TmpEstExc_Internalname ;
      private String edtContratoServicos_TmpEstCrr_Internalname ;
      private String cmbContratoServicos_StatusPagFnc_Internalname ;
      private String edtContratoServicos_IndiceDivergencia_Internalname ;
      private String edtContratoServicos_LimiteProposta_Internalname ;
      private String cmbContratoServicos_LocalExec_Internalname ;
      private String edtContratoServicos_UndCntNome_Internalname ;
      private String edtContratoServicos_FatorCnvUndCnt_Internalname ;
      private String edtServico_QtdContratada_Internalname ;
      private String edtServico_VlrUnidadeContratada_Internalname ;
      private String edtContratoServicos_Produtividade_Internalname ;
      private String cmbServicoContrato_Faturamento_Internalname ;
      private String cmbContratoServicos_CalculoRmn_Internalname ;
      private String edtContratoServicos_PercCnc_Internalname ;
      private String cmbContratoServicos_Momento_Internalname ;
      private String cmbContratoServicos_TipoVnc_Internalname ;
      private String edtContratoServicos_QntUntCns_Internalname ;
      private String edtContratoServicos_CodigoFiscal_Internalname ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String edtServicoGrupo_Descricao_Link ;
      private String cellContratoservicos_prazodias_cell_Class ;
      private String cellContratoservicos_prazodias_cell_Internalname ;
      private String cellContratoservicos_prazodias_righttext_cell_Class ;
      private String cellContratoservicos_prazodias_righttext_cell_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String grpUnnamedgroup1_Internalname ;
      private String grpUnnamedgroup2_Internalname ;
      private String grpUnnamedgroup3_Internalname ;
      private String grpUnnamedgroup4_Internalname ;
      private String tblParametros_Internalname ;
      private String lblTextblockcontratoservicos_indicedivergencia_Internalname ;
      private String lblTextblockcontratoservicos_indicedivergencia_Jsonclick ;
      private String lblTextblockcontratoservicos_naorequeratr_Internalname ;
      private String lblTextblockcontratoservicos_naorequeratr_Jsonclick ;
      private String lblTextblockcontratoservicos_limiteproposta_Internalname ;
      private String lblTextblockcontratoservicos_limiteproposta_Jsonclick ;
      private String lblTextblockcontratoservicos_localexec_Internalname ;
      private String lblTextblockcontratoservicos_localexec_Jsonclick ;
      private String cmbContratoServicos_LocalExec_Jsonclick ;
      private String lblTextblockcontratoservicos_undcntnome_Internalname ;
      private String lblTextblockcontratoservicos_undcntnome_Jsonclick ;
      private String edtContratoServicos_UndCntNome_Jsonclick ;
      private String lblTextblockcontratoservicos_fatorcnvundcnt_Internalname ;
      private String lblTextblockcontratoservicos_fatorcnvundcnt_Jsonclick ;
      private String edtContratoServicos_FatorCnvUndCnt_Jsonclick ;
      private String lblTextblockservico_qtdcontratada_Internalname ;
      private String lblTextblockservico_qtdcontratada_Jsonclick ;
      private String edtServico_QtdContratada_Jsonclick ;
      private String lblTextblockservico_vlrunidadecontratada_Internalname ;
      private String lblTextblockservico_vlrunidadecontratada_Jsonclick ;
      private String edtServico_VlrUnidadeContratada_Jsonclick ;
      private String lblTextblockcontratoservicos_produtividade_Internalname ;
      private String lblTextblockcontratoservicos_produtividade_Jsonclick ;
      private String lblTextblockservicocontrato_faturamento_Internalname ;
      private String lblTextblockservicocontrato_faturamento_Jsonclick ;
      private String cmbServicoContrato_Faturamento_Jsonclick ;
      private String lblTextblockservico_percentual_Internalname ;
      private String lblTextblockservico_percentual_Jsonclick ;
      private String lblTextblockcontratoservicos_calculormn_Internalname ;
      private String lblTextblockcontratoservicos_calculormn_Jsonclick ;
      private String cmbContratoServicos_CalculoRmn_Jsonclick ;
      private String lblTextblockcontratoservicos_perccnc_Internalname ;
      private String lblTextblockcontratoservicos_perccnc_Jsonclick ;
      private String lblTextblockcontratoservicos_espelhaaceite_Internalname ;
      private String lblTextblockcontratoservicos_espelhaaceite_Jsonclick ;
      private String lblTextblockcontratoservicos_momento_Internalname ;
      private String lblTextblockcontratoservicos_momento_Jsonclick ;
      private String cmbContratoServicos_Momento_Jsonclick ;
      private String lblTextblockcontratoservicos_tipovnc_Internalname ;
      private String lblTextblockcontratoservicos_tipovnc_Jsonclick ;
      private String cmbContratoServicos_TipoVnc_Jsonclick ;
      private String lblTextblockcontratoservicos_qntuntcns_Internalname ;
      private String lblTextblockcontratoservicos_qntuntcns_Jsonclick ;
      private String edtContratoServicos_QntUntCns_Jsonclick ;
      private String lblTextblockcontratoservicos_codigofiscal_Internalname ;
      private String lblTextblockcontratoservicos_codigofiscal_Jsonclick ;
      private String edtContratoServicos_CodigoFiscal_Jsonclick ;
      private String lblTextblockcontratoservicos_solicitagestorsistema_Internalname ;
      private String lblTextblockcontratoservicos_solicitagestorsistema_Jsonclick ;
      private String lblTextblockcontratoservicos_ativo_Internalname ;
      private String lblTextblockcontratoservicos_ativo_Jsonclick ;
      private String tblTablemergedcontratoservicos_solicitagestorsistema_Internalname ;
      private String imgContratoservicos_solicitagestorsistema_infoicon_Internalname ;
      private String tblTablemergedcontratoservicos_perccnc_Internalname ;
      private String edtContratoServicos_PercCnc_Jsonclick ;
      private String lblContratoservicos_perccnc_righttext_Internalname ;
      private String lblContratoservicos_perccnc_righttext_Jsonclick ;
      private String tblTablemergedservico_percentual_Internalname ;
      private String edtavServico_percentual_Jsonclick ;
      private String lblServico_percentual_righttext_Internalname ;
      private String lblServico_percentual_righttext_Jsonclick ;
      private String tblTablemergedcontratoservicos_produtividade_Internalname ;
      private String edtContratoServicos_Produtividade_Jsonclick ;
      private String lblContratoservicos_produtividade_righttext_Internalname ;
      private String lblContratoservicos_produtividade_righttext_Jsonclick ;
      private String tblTablemergedcontratoservicos_limiteproposta_Internalname ;
      private String edtContratoServicos_LimiteProposta_Jsonclick ;
      private String lblContratoservicos_limiteproposta_righttext_Internalname ;
      private String lblContratoservicos_limiteproposta_righttext_Jsonclick ;
      private String tblTablemergedcontratoservicos_indicedivergencia_Internalname ;
      private String edtContratoServicos_IndiceDivergencia_Jsonclick ;
      private String lblContratoservicos_indicedivergencia_righttext_Internalname ;
      private String lblContratoservicos_indicedivergencia_righttext_Jsonclick ;
      private String tblPrazos_Internalname ;
      private String lblTextblockcontratoservicos_prazotpdias_Internalname ;
      private String lblTextblockcontratoservicos_prazotpdias_Jsonclick ;
      private String cmbContratoServicos_PrazoTpDias_Jsonclick ;
      private String lblTextblockcontratoservicos_prazoinicio_Internalname ;
      private String lblTextblockcontratoservicos_prazoinicio_Jsonclick ;
      private String cmbContratoServicos_PrazoInicio_Jsonclick ;
      private String lblTextblockcontratoservicos_prazoanalise_Internalname ;
      private String lblTextblockcontratoservicos_prazoanalise_Jsonclick ;
      private String edtContratoServicos_PrazoAnalise_Jsonclick ;
      private String lblTextblockcontratoservicos_prazotipo_Internalname ;
      private String lblTextblockcontratoservicos_prazotipo_Jsonclick ;
      private String lblTextblockcontratoservicos_prazoresposta_Internalname ;
      private String lblTextblockcontratoservicos_prazoresposta_Jsonclick ;
      private String edtContratoServicos_PrazoResposta_Jsonclick ;
      private String lblTextblockcontratoservicos_prazogarantia_Internalname ;
      private String lblTextblockcontratoservicos_prazogarantia_Jsonclick ;
      private String edtContratoServicos_PrazoGarantia_Jsonclick ;
      private String lblTextblockcontratoservicos_prazoatendegarantia_Internalname ;
      private String lblTextblockcontratoservicos_prazoatendegarantia_Jsonclick ;
      private String edtContratoServicos_PrazoAtendeGarantia_Jsonclick ;
      private String lblTextblockcontratoservicos_prazocorrecaotipo_Internalname ;
      private String lblTextblockcontratoservicos_prazocorrecaotipo_Jsonclick ;
      private String lblTextblockcontratoservicos_tmpestanl_Internalname ;
      private String lblTextblockcontratoservicos_tmpestanl_Jsonclick ;
      private String lblTextblockcontratoservicos_tmpestexc_Internalname ;
      private String lblTextblockcontratoservicos_tmpestexc_Jsonclick ;
      private String lblTextblockcontratoservicos_tmpestcrr_Internalname ;
      private String lblTextblockcontratoservicos_tmpestcrr_Jsonclick ;
      private String lblTextblockcontratoservicos_statuspagfnc_Internalname ;
      private String lblTextblockcontratoservicos_statuspagfnc_Jsonclick ;
      private String cmbContratoServicos_StatusPagFnc_Jsonclick ;
      private String tblTablemergedcontratoservicos_tmpestcrr_Internalname ;
      private String edtContratoServicos_TmpEstCrr_Jsonclick ;
      private String lblContratoservicos_tmpestcrr_righttext_Internalname ;
      private String lblContratoservicos_tmpestcrr_righttext_Jsonclick ;
      private String tblTablemergedcontratoservicos_tmpestexc_Internalname ;
      private String edtContratoServicos_TmpEstExc_Jsonclick ;
      private String lblContratoservicos_tmpestexc_righttext_Internalname ;
      private String lblContratoservicos_tmpestexc_righttext_Jsonclick ;
      private String tblTablemergedcontratoservicos_tmpestanl_Internalname ;
      private String edtContratoServicos_TmpEstAnl_Jsonclick ;
      private String lblContratoservicos_tmpestanl_righttext_Internalname ;
      private String lblContratoservicos_tmpestanl_righttext_Jsonclick ;
      private String tblTablemergedcontratoservicos_prazocorrecaotipo_Internalname ;
      private String cmbContratoServicos_PrazoCorrecaoTipo_Jsonclick ;
      private String lblTextblockcontratoservicos_prazocorrecao_Internalname ;
      private String lblTextblockcontratoservicos_prazocorrecao_Jsonclick ;
      private String edtContratoServicos_PrazoCorrecao_Jsonclick ;
      private String cellContratoservicos_prazocorrecao_righttext_cell_Internalname ;
      private String lblContratoservicos_prazocorrecao_righttext_Internalname ;
      private String lblContratoservicos_prazocorrecao_righttext_Jsonclick ;
      private String tblTablemergedcontratoservicos_prazotipo_Internalname ;
      private String cmbContratoServicos_PrazoTipo_Jsonclick ;
      private String edtContratoServicos_PrazoDias_Jsonclick ;
      private String lblContratoservicos_prazodias_righttext_Internalname ;
      private String lblContratoservicos_prazodias_righttext_Jsonclick ;
      private String imgUpdprazo_Internalname ;
      private String imgUpdprazo_Jsonclick ;
      private String tblServico_Internalname ;
      private String lblTextblockservicogrupo_descricao_Internalname ;
      private String lblTextblockservicogrupo_descricao_Jsonclick ;
      private String lblTextblockservico_nome_Internalname ;
      private String lblTextblockservico_nome_Jsonclick ;
      private String tblTablemergedservico_nome_Internalname ;
      private String edtServico_Nome_Jsonclick ;
      private String lblTextblockcontratoservicos_alias_Internalname ;
      private String lblTextblockcontratoservicos_alias_Jsonclick ;
      private String edtContratoServicos_Alias_Jsonclick ;
      private String tblTablemergedservicogrupo_descricao_Internalname ;
      private String edtServicoGrupo_Descricao_Jsonclick ;
      private String lblTextblockcontratoservicos_servicosigla_Internalname ;
      private String lblTextblockcontratoservicos_servicosigla_Jsonclick ;
      private String edtContratoServicos_ServicoSigla_Jsonclick ;
      private String tblContrato_Internalname ;
      private String lblTextblockcontrato_numero_Internalname ;
      private String lblTextblockcontrato_numero_Jsonclick ;
      private String edtContrato_Numero_Jsonclick ;
      private String lblTextblockcontrato_numeroata_Internalname ;
      private String lblTextblockcontrato_numeroata_Jsonclick ;
      private String edtContrato_NumeroAta_Jsonclick ;
      private String lblTextblockcontrato_ano_Internalname ;
      private String lblTextblockcontrato_ano_Jsonclick ;
      private String edtContrato_Ano_Jsonclick ;
      private String lblTextblockcontratada_pessoanom_Internalname ;
      private String lblTextblockcontratada_pessoanom_Jsonclick ;
      private String edtContratada_PessoaNom_Jsonclick ;
      private String lblTextblockcontratada_pessoacnpj_Internalname ;
      private String lblTextblockcontratada_pessoacnpj_Jsonclick ;
      private String edtContratada_PessoaCNPJ_Jsonclick ;
      private String sCtrlA160ContratoServicos_Codigo ;
      private bool entryPointCalled ;
      private bool n911ContratoServicos_Prazos ;
      private bool n914ContratoServicos_PrazoDias ;
      private bool n913ContratoServicos_PrazoTipo ;
      private bool toggleJsOutput ;
      private bool A1397ContratoServicos_NaoRequerAtr ;
      private bool A1217ContratoServicos_EspelhaAceite ;
      private bool A2094ContratoServicos_SolicitaGestorSistema ;
      private bool A638ContratoServicos_Ativo ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1454ContratoServicos_PrazoTpDias ;
      private bool n1649ContratoServicos_PrazoInicio ;
      private bool n1225ContratoServicos_PrazoCorrecaoTipo ;
      private bool n1325ContratoServicos_StatusPagFnc ;
      private bool n2074ContratoServicos_CalculoRmn ;
      private bool n1266ContratoServicos_Momento ;
      private bool n868ContratoServicos_TipoVnc ;
      private bool n558Servico_Percentual ;
      private bool n1723ContratoServicos_CodigoFiscal ;
      private bool n1340ContratoServicos_QntUntCns ;
      private bool n1217ContratoServicos_EspelhaAceite ;
      private bool n1539ContratoServicos_PercCnc ;
      private bool n1191ContratoServicos_Produtividade ;
      private bool n555Servico_QtdContratada ;
      private bool n1341ContratoServicos_FatorCnvUndCnt ;
      private bool n2132ContratoServicos_UndCntNome ;
      private bool n1799ContratoServicos_LimiteProposta ;
      private bool n1397ContratoServicos_NaoRequerAtr ;
      private bool n1455ContratoServicos_IndiceDivergencia ;
      private bool n1502ContratoServicos_TmpEstCrr ;
      private bool n1501ContratoServicos_TmpEstExc ;
      private bool n1516ContratoServicos_TmpEstAnl ;
      private bool n1224ContratoServicos_PrazoCorrecao ;
      private bool n1182ContratoServicos_PrazoAtendeGarantia ;
      private bool n1181ContratoServicos_PrazoGarantia ;
      private bool n1153ContratoServicos_PrazoResposta ;
      private bool n1152ContratoServicos_PrazoAnalise ;
      private bool n1858ContratoServicos_Alias ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool n41Contratada_PessoaNom ;
      private bool n78Contrato_NumeroAta ;
      private bool returnInSub ;
      private String A607ServicoContrato_Faturamento ;
      private String A158ServicoGrupo_Descricao ;
      private String A42Contratada_PessoaCNPJ ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbContratoServicos_PrazoTpDias ;
      private GXCombobox cmbContratoServicos_PrazoInicio ;
      private GXCombobox cmbContratoServicos_PrazoTipo ;
      private GXCombobox cmbContratoServicos_PrazoCorrecaoTipo ;
      private GXCombobox cmbContratoServicos_StatusPagFnc ;
      private GXCheckbox chkContratoServicos_NaoRequerAtr ;
      private GXCombobox cmbContratoServicos_LocalExec ;
      private GXCombobox cmbServicoContrato_Faturamento ;
      private GXCombobox cmbContratoServicos_CalculoRmn ;
      private GXCheckbox chkContratoServicos_EspelhaAceite ;
      private GXCombobox cmbContratoServicos_Momento ;
      private GXCombobox cmbContratoServicos_TipoVnc ;
      private GXCheckbox chkContratoServicos_SolicitaGestorSistema ;
      private GXCheckbox chkContratoServicos_Ativo ;
      private IDataStoreProvider pr_default ;
      private short[] H00753_A911ContratoServicos_Prazos ;
      private bool[] H00753_n911ContratoServicos_Prazos ;
      private short[] H00754_A914ContratoServicos_PrazoDias ;
      private bool[] H00754_n914ContratoServicos_PrazoDias ;
      private String[] H00754_A913ContratoServicos_PrazoTipo ;
      private bool[] H00754_n913ContratoServicos_PrazoTipo ;
      private int[] H00756_A903ContratoServicosPrazo_CntSrvCod ;
      private int[] H00756_A155Servico_Codigo ;
      private int[] H00756_A39Contratada_Codigo ;
      private int[] H00756_A40Contratada_PessoaCod ;
      private int[] H00756_A1212ContratoServicos_UnidadeContratada ;
      private int[] H00756_A160ContratoServicos_Codigo ;
      private decimal[] H00756_A558Servico_Percentual ;
      private bool[] H00756_n558Servico_Percentual ;
      private int[] H00756_A157ServicoGrupo_Codigo ;
      private int[] H00756_A74Contrato_Codigo ;
      private bool[] H00756_A638ContratoServicos_Ativo ;
      private bool[] H00756_A2094ContratoServicos_SolicitaGestorSistema ;
      private String[] H00756_A1723ContratoServicos_CodigoFiscal ;
      private bool[] H00756_n1723ContratoServicos_CodigoFiscal ;
      private decimal[] H00756_A1340ContratoServicos_QntUntCns ;
      private bool[] H00756_n1340ContratoServicos_QntUntCns ;
      private String[] H00756_A868ContratoServicos_TipoVnc ;
      private bool[] H00756_n868ContratoServicos_TipoVnc ;
      private String[] H00756_A1266ContratoServicos_Momento ;
      private bool[] H00756_n1266ContratoServicos_Momento ;
      private bool[] H00756_A1217ContratoServicos_EspelhaAceite ;
      private bool[] H00756_n1217ContratoServicos_EspelhaAceite ;
      private short[] H00756_A1539ContratoServicos_PercCnc ;
      private bool[] H00756_n1539ContratoServicos_PercCnc ;
      private short[] H00756_A2074ContratoServicos_CalculoRmn ;
      private bool[] H00756_n2074ContratoServicos_CalculoRmn ;
      private String[] H00756_A607ServicoContrato_Faturamento ;
      private decimal[] H00756_A1191ContratoServicos_Produtividade ;
      private bool[] H00756_n1191ContratoServicos_Produtividade ;
      private decimal[] H00756_A557Servico_VlrUnidadeContratada ;
      private long[] H00756_A555Servico_QtdContratada ;
      private bool[] H00756_n555Servico_QtdContratada ;
      private decimal[] H00756_A1341ContratoServicos_FatorCnvUndCnt ;
      private bool[] H00756_n1341ContratoServicos_FatorCnvUndCnt ;
      private String[] H00756_A2132ContratoServicos_UndCntNome ;
      private bool[] H00756_n2132ContratoServicos_UndCntNome ;
      private String[] H00756_A639ContratoServicos_LocalExec ;
      private decimal[] H00756_A1799ContratoServicos_LimiteProposta ;
      private bool[] H00756_n1799ContratoServicos_LimiteProposta ;
      private bool[] H00756_A1397ContratoServicos_NaoRequerAtr ;
      private bool[] H00756_n1397ContratoServicos_NaoRequerAtr ;
      private decimal[] H00756_A1455ContratoServicos_IndiceDivergencia ;
      private bool[] H00756_n1455ContratoServicos_IndiceDivergencia ;
      private String[] H00756_A1325ContratoServicos_StatusPagFnc ;
      private bool[] H00756_n1325ContratoServicos_StatusPagFnc ;
      private int[] H00756_A1502ContratoServicos_TmpEstCrr ;
      private bool[] H00756_n1502ContratoServicos_TmpEstCrr ;
      private int[] H00756_A1501ContratoServicos_TmpEstExc ;
      private bool[] H00756_n1501ContratoServicos_TmpEstExc ;
      private int[] H00756_A1516ContratoServicos_TmpEstAnl ;
      private bool[] H00756_n1516ContratoServicos_TmpEstAnl ;
      private short[] H00756_A1224ContratoServicos_PrazoCorrecao ;
      private bool[] H00756_n1224ContratoServicos_PrazoCorrecao ;
      private String[] H00756_A1225ContratoServicos_PrazoCorrecaoTipo ;
      private bool[] H00756_n1225ContratoServicos_PrazoCorrecaoTipo ;
      private short[] H00756_A1182ContratoServicos_PrazoAtendeGarantia ;
      private bool[] H00756_n1182ContratoServicos_PrazoAtendeGarantia ;
      private short[] H00756_A1181ContratoServicos_PrazoGarantia ;
      private bool[] H00756_n1181ContratoServicos_PrazoGarantia ;
      private short[] H00756_A1153ContratoServicos_PrazoResposta ;
      private bool[] H00756_n1153ContratoServicos_PrazoResposta ;
      private short[] H00756_A1152ContratoServicos_PrazoAnalise ;
      private bool[] H00756_n1152ContratoServicos_PrazoAnalise ;
      private short[] H00756_A1649ContratoServicos_PrazoInicio ;
      private bool[] H00756_n1649ContratoServicos_PrazoInicio ;
      private String[] H00756_A1454ContratoServicos_PrazoTpDias ;
      private bool[] H00756_n1454ContratoServicos_PrazoTpDias ;
      private String[] H00756_A1858ContratoServicos_Alias ;
      private bool[] H00756_n1858ContratoServicos_Alias ;
      private String[] H00756_A608Servico_Nome ;
      private String[] H00756_A158ServicoGrupo_Descricao ;
      private String[] H00756_A42Contratada_PessoaCNPJ ;
      private bool[] H00756_n42Contratada_PessoaCNPJ ;
      private String[] H00756_A41Contratada_PessoaNom ;
      private bool[] H00756_n41Contratada_PessoaNom ;
      private short[] H00756_A79Contrato_Ano ;
      private String[] H00756_A78Contrato_NumeroAta ;
      private bool[] H00756_n78Contrato_NumeroAta ;
      private String[] H00756_A77Contrato_Numero ;
      private short[] H00756_A911ContratoServicos_Prazos ;
      private bool[] H00756_n911ContratoServicos_Prazos ;
      private short[] H00756_A914ContratoServicos_PrazoDias ;
      private bool[] H00756_n914ContratoServicos_PrazoDias ;
      private String[] H00756_A913ContratoServicos_PrazoTipo ;
      private bool[] H00756_n913ContratoServicos_PrazoTipo ;
      private String[] H00756_A605Servico_Sigla ;
      private short[] H00758_A911ContratoServicos_Prazos ;
      private bool[] H00758_n911ContratoServicos_Prazos ;
      private short[] H00759_A914ContratoServicos_PrazoDias ;
      private bool[] H00759_n914ContratoServicos_PrazoDias ;
      private String[] H00759_A913ContratoServicos_PrazoTipo ;
      private bool[] H00759_n913ContratoServicos_PrazoTipo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class contratoservicosgeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00753 ;
          prmH00753 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00754 ;
          prmH00754 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00756 ;
          prmH00756 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferH00756 ;
          cmdBufferH00756=" SELECT T9.[ContratoServicosPrazo_CntSrvCod], T1.[Servico_Codigo], T5.[Contratada_Codigo], T6.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[ContratoServicos_UnidadeContratada] AS ContratoServicos_UnidadeContratada, T1.[ContratoServicos_Codigo], T1.[Servico_Percentual], T2.[ServicoGrupo_Codigo], T1.[Contrato_Codigo], T1.[ContratoServicos_Ativo], T1.[ContratoServicos_SolicitaGestorSistema], T1.[ContratoServicos_CodigoFiscal], T1.[ContratoServicos_QntUntCns], T1.[ContratoServicos_TipoVnc], T1.[ContratoServicos_Momento], T1.[ContratoServicos_EspelhaAceite], T1.[ContratoServicos_PercCnc], T1.[ContratoServicos_CalculoRmn], T1.[ServicoContrato_Faturamento], T1.[ContratoServicos_Produtividade], T1.[Servico_VlrUnidadeContratada], T1.[Servico_QtdContratada], T1.[ContratoServicos_FatorCnvUndCnt], T4.[UnidadeMedicao_Nome] AS ContratoServicos_UndCntNome, T1.[ContratoServicos_LocalExec], T1.[ContratoServicos_LimiteProposta], T1.[ContratoServicos_NaoRequerAtr], T1.[ContratoServicos_IndiceDivergencia], T1.[ContratoServicos_StatusPagFnc], T1.[ContratoServicos_TmpEstCrr], T1.[ContratoServicos_TmpEstExc], T1.[ContratoServicos_TmpEstAnl], T1.[ContratoServicos_PrazoCorrecao], T1.[ContratoServicos_PrazoCorrecaoTipo], T1.[ContratoServicos_PrazoAtendeGarantia], T1.[ContratoServicos_PrazoGarantia], T1.[ContratoServicos_PrazoResposta], T1.[ContratoServicos_PrazoAnalise], T1.[ContratoServicos_PrazoInicio], T1.[ContratoServicos_PrazoTpDias], T1.[ContratoServicos_Alias], T2.[Servico_Nome], T3.[ServicoGrupo_Descricao], T7.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T7.[Pessoa_Nome] AS Contratada_PessoaNom, T5.[Contrato_Ano], T5.[Contrato_NumeroAta], T5.[Contrato_Numero], COALESCE( T8.[ContratoServicos_Prazos], 0) AS ContratoServicos_Prazos, COALESCE( T9.[ContratoServicosPrazo_Dias], 0) AS ContratoServicos_PrazoDias, "
          + " COALESCE( T9.[ContratoServicosPrazo_Tipo], '') AS ContratoServicos_PrazoTipo, T2.[Servico_Sigla] FROM (((((((([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) INNER JOIN [ServicoGrupo] T3 WITH (NOLOCK) ON T3.[ServicoGrupo_Codigo] = T2.[ServicoGrupo_Codigo]) INNER JOIN [UnidadeMedicao] T4 WITH (NOLOCK) ON T4.[UnidadeMedicao_Codigo] = T1.[ContratoServicos_UnidadeContratada]) INNER JOIN [Contrato] T5 WITH (NOLOCK) ON T5.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T6 WITH (NOLOCK) ON T6.[Contratada_Codigo] = T5.[Contratada_Codigo]) INNER JOIN [Pessoa] T7 WITH (NOLOCK) ON T7.[Pessoa_Codigo] = T6.[Contratada_PessoaCod]) LEFT JOIN (SELECT COUNT(*) AS ContratoServicos_Prazos, T11.[ContratoServicos_Codigo] FROM [ContratoServicosPrazo] T10 WITH (NOLOCK),  [ContratoServicos] T11 WITH (NOLOCK) WHERE T10.[ContratoServicosPrazo_CntSrvCod] = T11.[ContratoServicos_Codigo] GROUP BY T11.[ContratoServicos_Codigo] ) T8 ON T8.[ContratoServicos_Codigo] = T1.[ContratoServicos_Codigo]) LEFT JOIN [ContratoServicosPrazo] T9 WITH (NOLOCK) ON T9.[ContratoServicosPrazo_CntSrvCod] = T1.[ContratoServicos_Codigo]) WHERE T1.[ContratoServicos_Codigo] = @ContratoServicos_Codigo ORDER BY T1.[ContratoServicos_Codigo]" ;
          Object[] prmH00758 ;
          prmH00758 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00759 ;
          prmH00759 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00753", "SELECT COALESCE( T1.[ContratoServicos_Prazos], 0) AS ContratoServicos_Prazos FROM (SELECT COUNT(*) AS ContratoServicos_Prazos, T3.[ContratoServicos_Codigo] FROM [ContratoServicosPrazo] T2 WITH (NOLOCK),  [ContratoServicos] T3 WITH (NOLOCK) WHERE T2.[ContratoServicosPrazo_CntSrvCod] = T3.[ContratoServicos_Codigo] GROUP BY T3.[ContratoServicos_Codigo] ) T1 WHERE T1.[ContratoServicos_Codigo] = @ContratoServicos_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00753,1,0,true,true )
             ,new CursorDef("H00754", "SELECT COALESCE( [ContratoServicosPrazo_Dias], 0) AS ContratoServicos_PrazoDias, COALESCE( [ContratoServicosPrazo_Tipo], '') AS ContratoServicos_PrazoTipo FROM [ContratoServicosPrazo] WITH (NOLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicos_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00754,1,0,true,true )
             ,new CursorDef("H00756", cmdBufferH00756,false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00756,1,0,true,true )
             ,new CursorDef("H00758", "SELECT COALESCE( T1.[ContratoServicos_Prazos], 0) AS ContratoServicos_Prazos FROM (SELECT COUNT(*) AS ContratoServicos_Prazos, T3.[ContratoServicos_Codigo] FROM [ContratoServicosPrazo] T2 WITH (NOLOCK),  [ContratoServicos] T3 WITH (NOLOCK) WHERE T2.[ContratoServicosPrazo_CntSrvCod] = T3.[ContratoServicos_Codigo] GROUP BY T3.[ContratoServicos_Codigo] ) T1 WHERE T1.[ContratoServicos_Codigo] = @ContratoServicos_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00758,1,0,true,true )
             ,new CursorDef("H00759", "SELECT COALESCE( [ContratoServicosPrazo_Dias], 0) AS ContratoServicos_PrazoDias, COALESCE( [ContratoServicosPrazo_Tipo], '') AS ContratoServicos_PrazoTipo FROM [ContratoServicosPrazo] WITH (NOLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicos_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00759,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 1 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 20) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(7);
                ((int[]) buf[8])[0] = rslt.getInt(8) ;
                ((int[]) buf[9])[0] = rslt.getInt(9) ;
                ((bool[]) buf[10])[0] = rslt.getBool(10) ;
                ((bool[]) buf[11])[0] = rslt.getBool(11) ;
                ((String[]) buf[12])[0] = rslt.getString(12, 5) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(12);
                ((decimal[]) buf[14])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(13);
                ((String[]) buf[16])[0] = rslt.getString(14, 1) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(14);
                ((String[]) buf[18])[0] = rslt.getString(15, 1) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(15);
                ((bool[]) buf[20])[0] = rslt.getBool(16) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(16);
                ((short[]) buf[22])[0] = rslt.getShort(17) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(17);
                ((short[]) buf[24])[0] = rslt.getShort(18) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(18);
                ((String[]) buf[26])[0] = rslt.getVarchar(19) ;
                ((decimal[]) buf[27])[0] = rslt.getDecimal(20) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(20);
                ((decimal[]) buf[29])[0] = rslt.getDecimal(21) ;
                ((long[]) buf[30])[0] = rslt.getLong(22) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(22);
                ((decimal[]) buf[32])[0] = rslt.getDecimal(23) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(23);
                ((String[]) buf[34])[0] = rslt.getString(24, 50) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(24);
                ((String[]) buf[36])[0] = rslt.getString(25, 1) ;
                ((decimal[]) buf[37])[0] = rslt.getDecimal(26) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(26);
                ((bool[]) buf[39])[0] = rslt.getBool(27) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(27);
                ((decimal[]) buf[41])[0] = rslt.getDecimal(28) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(28);
                ((String[]) buf[43])[0] = rslt.getString(29, 1) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(29);
                ((int[]) buf[45])[0] = rslt.getInt(30) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(30);
                ((int[]) buf[47])[0] = rslt.getInt(31) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(31);
                ((int[]) buf[49])[0] = rslt.getInt(32) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(32);
                ((short[]) buf[51])[0] = rslt.getShort(33) ;
                ((bool[]) buf[52])[0] = rslt.wasNull(33);
                ((String[]) buf[53])[0] = rslt.getString(34, 1) ;
                ((bool[]) buf[54])[0] = rslt.wasNull(34);
                ((short[]) buf[55])[0] = rslt.getShort(35) ;
                ((bool[]) buf[56])[0] = rslt.wasNull(35);
                ((short[]) buf[57])[0] = rslt.getShort(36) ;
                ((bool[]) buf[58])[0] = rslt.wasNull(36);
                ((short[]) buf[59])[0] = rslt.getShort(37) ;
                ((bool[]) buf[60])[0] = rslt.wasNull(37);
                ((short[]) buf[61])[0] = rslt.getShort(38) ;
                ((bool[]) buf[62])[0] = rslt.wasNull(38);
                ((short[]) buf[63])[0] = rslt.getShort(39) ;
                ((bool[]) buf[64])[0] = rslt.wasNull(39);
                ((String[]) buf[65])[0] = rslt.getString(40, 1) ;
                ((bool[]) buf[66])[0] = rslt.wasNull(40);
                ((String[]) buf[67])[0] = rslt.getString(41, 15) ;
                ((bool[]) buf[68])[0] = rslt.wasNull(41);
                ((String[]) buf[69])[0] = rslt.getString(42, 50) ;
                ((String[]) buf[70])[0] = rslt.getVarchar(43) ;
                ((String[]) buf[71])[0] = rslt.getVarchar(44) ;
                ((bool[]) buf[72])[0] = rslt.wasNull(44);
                ((String[]) buf[73])[0] = rslt.getString(45, 100) ;
                ((bool[]) buf[74])[0] = rslt.wasNull(45);
                ((short[]) buf[75])[0] = rslt.getShort(46) ;
                ((String[]) buf[76])[0] = rslt.getString(47, 10) ;
                ((bool[]) buf[77])[0] = rslt.wasNull(47);
                ((String[]) buf[78])[0] = rslt.getString(48, 20) ;
                ((short[]) buf[79])[0] = rslt.getShort(49) ;
                ((bool[]) buf[80])[0] = rslt.wasNull(49);
                ((short[]) buf[81])[0] = rslt.getShort(50) ;
                ((bool[]) buf[82])[0] = rslt.wasNull(50);
                ((String[]) buf[83])[0] = rslt.getString(51, 20) ;
                ((bool[]) buf[84])[0] = rslt.wasNull(51);
                ((String[]) buf[85])[0] = rslt.getString(52, 15) ;
                return;
             case 3 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 20) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
