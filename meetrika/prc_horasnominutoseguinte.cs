/*
               File: PRC_HorasNoMinutoSeguinte
        Description: Horas No Minuto Seguinte
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:25.98
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_horasnominutoseguinte : GXProcedure
   {
      public prc_horasnominutoseguinte( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_horasnominutoseguinte( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( String aP0_Hora ,
                           out String aP1_NovaHora )
      {
         this.AV10Hora = aP0_Hora;
         this.AV11NovaHora = "" ;
         initialize();
         executePrivate();
         aP1_NovaHora=this.AV11NovaHora;
      }

      public String executeUdp( String aP0_Hora )
      {
         this.AV10Hora = aP0_Hora;
         this.AV11NovaHora = "" ;
         initialize();
         executePrivate();
         aP1_NovaHora=this.AV11NovaHora;
         return AV11NovaHora ;
      }

      public void executeSubmit( String aP0_Hora ,
                                 out String aP1_NovaHora )
      {
         prc_horasnominutoseguinte objprc_horasnominutoseguinte;
         objprc_horasnominutoseguinte = new prc_horasnominutoseguinte();
         objprc_horasnominutoseguinte.AV10Hora = aP0_Hora;
         objprc_horasnominutoseguinte.AV11NovaHora = "" ;
         objprc_horasnominutoseguinte.context.SetSubmitInitialConfig(context);
         objprc_horasnominutoseguinte.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_horasnominutoseguinte);
         aP1_NovaHora=this.AV11NovaHora;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_horasnominutoseguinte)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8h = (short)(NumberUtil.Val( StringUtil.Substring( AV10Hora, 1, 2), "."));
         AV9m = (short)(NumberUtil.Val( StringUtil.Substring( AV10Hora, 4, 2), "."));
         AV9m = (short)(AV9m+1);
         if ( AV9m == 60 )
         {
            AV8h = (short)(AV8h+1);
            AV9m = 0;
         }
         AV11NovaHora = StringUtil.PadL( StringUtil.Trim( StringUtil.Str( (decimal)(AV8h), 4, 0)), 2, "0") + ":" + StringUtil.PadL( StringUtil.Trim( StringUtil.Str( (decimal)(AV9m), 4, 0)), 2, "0");
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV8h ;
      private short AV9m ;
      private String AV10Hora ;
      private String AV11NovaHora ;
      private String aP1_NovaHora ;
   }

}
