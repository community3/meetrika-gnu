/*
               File: WP_NovoRequisito
        Description: Novo Requisito T�cnico
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 23:50:20.75
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_novorequisito : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_novorequisito( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_novorequisito( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Requisito_Codigo ,
                           int aP1_Requisito_ReqCod ,
                           int aP2_OSCod )
      {
         this.AV18Requisito_Codigo = aP0_Requisito_Codigo;
         this.AV31Requisito_ReqCod = aP1_Requisito_ReqCod;
         this.AV34OSCod = aP2_OSCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavRequisito_tiporeqcod = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vREQUISITO_TIPOREQCOD") == 0 )
            {
               AV38LinhaNegocio_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38LinhaNegocio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38LinhaNegocio_Codigo), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvREQUISITO_TIPOREQCODQ92( AV38LinhaNegocio_Codigo) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV18Requisito_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Requisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18Requisito_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vREQUISITO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV18Requisito_Codigo), "ZZZZZ9")));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV31Requisito_ReqCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Requisito_ReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31Requisito_ReqCod), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vREQUISITO_REQCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV31Requisito_ReqCod), "ZZZZZ9")));
                  AV34OSCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34OSCod), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vOSCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV34OSCod), "ZZZZZ9")));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAQ92( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTQ92( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202032423502086");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_novorequisito.aspx") + "?" + UrlEncode("" +AV18Requisito_Codigo) + "," + UrlEncode("" +AV31Requisito_ReqCod) + "," + UrlEncode("" +AV34OSCod)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vSOLUCAO", AV8Solucao);
         GxWebStd.gx_hidden_field( context, "vREQUISITO_REQCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31Requisito_ReqCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vREQUISITO", AV14Requisito);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vREQUISITO", AV14Requisito);
         }
         GxWebStd.gx_hidden_field( context, "vREQUISITO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18Requisito_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "vOK", AV30Ok);
         GxWebStd.gx_hidden_field( context, "vOSCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV34OSCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXC1", StringUtil.LTrim( StringUtil.NToC( (decimal)(A40000GXC1), 9, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXC1", StringUtil.LTrim( StringUtil.NToC( (decimal)(A40001GXC1), 9, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vIDENTIFICADOR", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV6Identificador, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vNOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7Nome, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vREQUISITO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV18Requisito_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vREQUISITO_REQCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV31Requisito_ReqCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vOSCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV34OSCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vREQUISITO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV18Requisito_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vREQUISITO_REQCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV31Requisito_ReqCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vOSCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV34OSCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "SOLUCAO_Width", StringUtil.RTrim( Solucao_Width));
         GxWebStd.gx_hidden_field( context, "SOLUCAO_Height", StringUtil.RTrim( Solucao_Height));
         GxWebStd.gx_hidden_field( context, "SOLUCAO_Toolbar", StringUtil.RTrim( Solucao_Toolbar));
         GxWebStd.gx_hidden_field( context, "SOLUCAO_Enabled", StringUtil.BoolToStr( Solucao_Enabled));
         GxWebStd.gx_hidden_field( context, "SOLUCAO_Toolbarexpanded", StringUtil.BoolToStr( Solucao_Toolbarexpanded));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEQ92( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTQ92( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_novorequisito.aspx") + "?" + UrlEncode("" +AV18Requisito_Codigo) + "," + UrlEncode("" +AV31Requisito_ReqCod) + "," + UrlEncode("" +AV34OSCod) ;
      }

      public override String GetPgmname( )
      {
         return "WP_NovoRequisito" ;
      }

      public override String GetPgmdesc( )
      {
         return "Novo Requisito T�cnico" ;
      }

      protected void WBQ90( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            wb_table1_3_Q92( true) ;
         }
         else
         {
            wb_table1_3_Q92( false) ;
         }
         return  ;
      }

      protected void wb_table1_3_Q92e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTQ92( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Novo Requisito T�cnico", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPQ90( ) ;
      }

      protected void WSQ92( )
      {
         STARTQ92( ) ;
         EVTQ92( ) ;
      }

      protected void EVTQ92( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11Q92 */
                              E11Q92 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12Q92 */
                              E12Q92 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'RASCUNHO'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13Q92 */
                              E13Q92 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E14Q92 */
                                    E14Q92 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'FECHAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15Q92 */
                              E15Q92 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16Q92 */
                              E16Q92 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEQ92( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAQ92( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavRequisito_tiporeqcod.Name = "vREQUISITO_TIPOREQCOD";
            dynavRequisito_tiporeqcod.WebTags = "";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavIdentificador_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvREQUISITO_TIPOREQCODQ92( int AV38LinhaNegocio_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvREQUISITO_TIPOREQCOD_dataQ92( AV38LinhaNegocio_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvREQUISITO_TIPOREQCOD_htmlQ92( int AV38LinhaNegocio_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLVvREQUISITO_TIPOREQCOD_dataQ92( AV38LinhaNegocio_Codigo) ;
         gxdynajaxindex = 1;
         dynavRequisito_tiporeqcod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavRequisito_tiporeqcod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavRequisito_tiporeqcod.ItemCount > 0 )
         {
            AV37Requisito_TipoReqCod = (int)(NumberUtil.Val( dynavRequisito_tiporeqcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV37Requisito_TipoReqCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Requisito_TipoReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37Requisito_TipoReqCod), 6, 0)));
         }
      }

      protected void GXDLVvREQUISITO_TIPOREQCOD_dataQ92( int AV38LinhaNegocio_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00Q92 */
         pr_default.execute(0, new Object[] {AV38LinhaNegocio_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00Q92_A2041TipoRequisito_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00Q92_A2042TipoRequisito_Identificador[0]);
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavRequisito_tiporeqcod.ItemCount > 0 )
         {
            AV37Requisito_TipoReqCod = (int)(NumberUtil.Val( dynavRequisito_tiporeqcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV37Requisito_TipoReqCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Requisito_TipoReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37Requisito_TipoReqCod), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFQ92( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavIdentificador_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavIdentificador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavIdentificador_Enabled), 5, 0)));
         edtavNome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNome_Enabled), 5, 0)));
      }

      protected void RFQ92( )
      {
         initialize_formulas( ) ;
         /* Execute user event: E12Q92 */
         E12Q92 ();
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E16Q92 */
            E16Q92 ();
            WBQ90( ) ;
         }
      }

      protected void STRUPQ90( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavIdentificador_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavIdentificador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavIdentificador_Enabled), 5, 0)));
         edtavNome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNome_Enabled), 5, 0)));
         /* Using cursor H00Q94 */
         pr_default.execute(1, new Object[] {AV31Requisito_ReqCod});
         if ( (pr_default.getStatus(1) != 101) )
         {
            A40000GXC1 = H00Q94_A40000GXC1[0];
         }
         else
         {
            A40000GXC1 = 0;
         }
         pr_default.close(1);
         /* Using cursor H00Q96 */
         pr_default.execute(2, new Object[] {AV14Requisito.gxTpr_Requisito_reqcod});
         if ( (pr_default.getStatus(2) != 101) )
         {
            A40001GXC1 = H00Q96_A40001GXC1[0];
         }
         else
         {
            A40001GXC1 = 0;
         }
         pr_default.close(2);
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11Q92 */
         E11Q92 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         GXVvREQUISITO_TIPOREQCOD_htmlQ92( AV38LinhaNegocio_Codigo) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV6Identificador = cgiGet( edtavIdentificador_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Identificador", AV6Identificador);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vIDENTIFICADOR", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV6Identificador, ""))));
            AV7Nome = cgiGet( edtavNome_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Nome", AV7Nome);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vNOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7Nome, ""))));
            AV9Requisito_Titulo = cgiGet( edtavRequisito_titulo_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Requisito_Titulo", AV9Requisito_Titulo);
            AV19Requisito_Agrupador = cgiGet( edtavRequisito_agrupador_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Requisito_Agrupador", AV19Requisito_Agrupador);
            dynavRequisito_tiporeqcod.CurrentValue = cgiGet( dynavRequisito_tiporeqcod_Internalname);
            AV37Requisito_TipoReqCod = (int)(NumberUtil.Val( cgiGet( dynavRequisito_tiporeqcod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Requisito_TipoReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37Requisito_TipoReqCod), 6, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavRequisito_ordem_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavRequisito_ordem_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vREQUISITO_ORDEM");
               GX_FocusControl = edtavRequisito_ordem_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV22Requisito_Ordem = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Requisito_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Requisito_Ordem), 3, 0)));
            }
            else
            {
               AV22Requisito_Ordem = (short)(context.localUtil.CToN( cgiGet( edtavRequisito_ordem_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Requisito_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Requisito_Ordem), 3, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavRequisito_pontuacao_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavRequisito_pontuacao_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vREQUISITO_PONTUACAO");
               GX_FocusControl = edtavRequisito_pontuacao_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV23Requisito_Pontuacao = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Requisito_Pontuacao", StringUtil.LTrim( StringUtil.Str( AV23Requisito_Pontuacao, 14, 5)));
            }
            else
            {
               AV23Requisito_Pontuacao = context.localUtil.CToN( cgiGet( edtavRequisito_pontuacao_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Requisito_Pontuacao", StringUtil.LTrim( StringUtil.Str( AV23Requisito_Pontuacao, 14, 5)));
            }
            AV21Requisito_Restricao = cgiGet( edtavRequisito_restricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Requisito_Restricao", AV21Requisito_Restricao);
            AV33Requisito_ReferenciaTecnica = cgiGet( edtavRequisito_referenciatecnica_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Requisito_ReferenciaTecnica", AV33Requisito_ReferenciaTecnica);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavOrdemmax_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOrdemmax_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vORDEMMAX");
               GX_FocusControl = edtavOrdemmax_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV29OrdemMax = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29OrdemMax", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29OrdemMax), 4, 0)));
            }
            else
            {
               AV29OrdemMax = (short)(context.localUtil.CToN( cgiGet( edtavOrdemmax_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29OrdemMax", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29OrdemMax), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavLinhanegocio_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavLinhanegocio_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vLINHANEGOCIO_CODIGO");
               GX_FocusControl = edtavLinhanegocio_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV38LinhaNegocio_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38LinhaNegocio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38LinhaNegocio_Codigo), 6, 0)));
            }
            else
            {
               AV38LinhaNegocio_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavLinhanegocio_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38LinhaNegocio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38LinhaNegocio_Codigo), 6, 0)));
            }
            /* Read saved values. */
            AV8Solucao = cgiGet( "vSOLUCAO");
            Solucao_Width = cgiGet( "SOLUCAO_Width");
            Solucao_Height = cgiGet( "SOLUCAO_Height");
            Solucao_Toolbar = cgiGet( "SOLUCAO_Toolbar");
            Solucao_Enabled = StringUtil.StrToBool( cgiGet( "SOLUCAO_Enabled"));
            Solucao_Toolbarexpanded = StringUtil.StrToBool( cgiGet( "SOLUCAO_Toolbarexpanded"));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            GXVvREQUISITO_TIPOREQCOD_htmlQ92( AV38LinhaNegocio_Codigo) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11Q92 */
         E11Q92 ();
         if (returnInSub) return;
      }

      protected void E11Q92( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV24WWPContext) ;
         AV10FCKEditorMenuItem.gxTpr_Id = "upload";
         AV10FCKEditorMenuItem.gxTpr_Description = "Inserir imagem local";
         AV10FCKEditorMenuItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "f5b04895-0024-488b-8e3b-b687ca4598ee", "", context.GetTheme( ))));
         AV10FCKEditorMenuItem.gxTpr_Objectinterface = 1;
         AV10FCKEditorMenuItem.gxTpr_Link = formatLink("wp_fileupload.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV13Url));
         AV12FCKEditorMenu.Add(AV10FCKEditorMenuItem, 0);
         this.executeUsercontrolMethod("", false, "SOLUCAOContainer", "SetMenu", "", new Object[] {(IGxCollection)AV12FCKEditorMenu});
         bttBtnrascunho_Visible = ((0==AV18Requisito_Codigo) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnrascunho_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnrascunho_Visible), 5, 0)));
         edtavOrdemmax_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdemmax_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdemmax_Visible), 5, 0)));
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         edtavLinhanegocio_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLinhanegocio_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLinhanegocio_codigo_Visible), 5, 0)));
         if ( AV31Requisito_ReqCod > 0 )
         {
            /* Using cursor H00Q97 */
            pr_default.execute(3, new Object[] {AV31Requisito_ReqCod, AV34OSCod});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A1553ContagemResultado_CntSrvCod = H00Q97_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = H00Q97_n1553ContagemResultado_CntSrvCod[0];
               A601ContagemResultado_Servico = H00Q97_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = H00Q97_n601ContagemResultado_Servico[0];
               A2004ContagemResultadoRequisito_ReqCod = H00Q97_A2004ContagemResultadoRequisito_ReqCod[0];
               A2003ContagemResultadoRequisito_OSCod = H00Q97_A2003ContagemResultadoRequisito_OSCod[0];
               A2001Requisito_Identificador = H00Q97_A2001Requisito_Identificador[0];
               n2001Requisito_Identificador = H00Q97_n2001Requisito_Identificador[0];
               A1927Requisito_Titulo = H00Q97_A1927Requisito_Titulo[0];
               n1927Requisito_Titulo = H00Q97_n1927Requisito_Titulo[0];
               A2050ContagemResultado_SrvLnhNegCod = H00Q97_A2050ContagemResultado_SrvLnhNegCod[0];
               n2050ContagemResultado_SrvLnhNegCod = H00Q97_n2050ContagemResultado_SrvLnhNegCod[0];
               A2001Requisito_Identificador = H00Q97_A2001Requisito_Identificador[0];
               n2001Requisito_Identificador = H00Q97_n2001Requisito_Identificador[0];
               A1927Requisito_Titulo = H00Q97_A1927Requisito_Titulo[0];
               n1927Requisito_Titulo = H00Q97_n1927Requisito_Titulo[0];
               A1553ContagemResultado_CntSrvCod = H00Q97_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = H00Q97_n1553ContagemResultado_CntSrvCod[0];
               A601ContagemResultado_Servico = H00Q97_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = H00Q97_n601ContagemResultado_Servico[0];
               A2050ContagemResultado_SrvLnhNegCod = H00Q97_A2050ContagemResultado_SrvLnhNegCod[0];
               n2050ContagemResultado_SrvLnhNegCod = H00Q97_n2050ContagemResultado_SrvLnhNegCod[0];
               AV6Identificador = A2001Requisito_Identificador;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Identificador", AV6Identificador);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vIDENTIFICADOR", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV6Identificador, ""))));
               AV7Nome = A1927Requisito_Titulo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Nome", AV7Nome);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vNOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7Nome, ""))));
               AV38LinhaNegocio_Codigo = A2050ContagemResultado_SrvLnhNegCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38LinhaNegocio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38LinhaNegocio_Codigo), 6, 0)));
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(3);
            }
            pr_default.close(3);
         }
         else
         {
            /* Using cursor H00Q98 */
            pr_default.execute(4, new Object[] {AV34OSCod});
            while ( (pr_default.getStatus(4) != 101) )
            {
               A1553ContagemResultado_CntSrvCod = H00Q98_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = H00Q98_n1553ContagemResultado_CntSrvCod[0];
               A601ContagemResultado_Servico = H00Q98_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = H00Q98_n601ContagemResultado_Servico[0];
               A456ContagemResultado_Codigo = H00Q98_A456ContagemResultado_Codigo[0];
               A2050ContagemResultado_SrvLnhNegCod = H00Q98_A2050ContagemResultado_SrvLnhNegCod[0];
               n2050ContagemResultado_SrvLnhNegCod = H00Q98_n2050ContagemResultado_SrvLnhNegCod[0];
               A601ContagemResultado_Servico = H00Q98_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = H00Q98_n601ContagemResultado_Servico[0];
               A2050ContagemResultado_SrvLnhNegCod = H00Q98_A2050ContagemResultado_SrvLnhNegCod[0];
               n2050ContagemResultado_SrvLnhNegCod = H00Q98_n2050ContagemResultado_SrvLnhNegCod[0];
               AV38LinhaNegocio_Codigo = A2050ContagemResultado_SrvLnhNegCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38LinhaNegocio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38LinhaNegocio_Codigo), 6, 0)));
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(4);
         }
         if ( AV18Requisito_Codigo > 0 )
         {
            AV14Requisito.Load(AV18Requisito_Codigo);
            AV6Identificador = AV14Requisito.gxTpr_Requisito_identificador;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Identificador", AV6Identificador);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vIDENTIFICADOR", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV6Identificador, ""))));
            AV7Nome = AV14Requisito.gxTpr_Requisito_titulo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Nome", AV7Nome);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vNOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7Nome, ""))));
            AV9Requisito_Titulo = AV14Requisito.gxTpr_Requisito_titulo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Requisito_Titulo", AV9Requisito_Titulo);
            AV8Solucao = AV14Requisito.gxTpr_Requisito_descricao;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Solucao", AV8Solucao);
            AV19Requisito_Agrupador = AV14Requisito.gxTpr_Requisito_agrupador;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Requisito_Agrupador", AV19Requisito_Agrupador);
            AV22Requisito_Ordem = AV14Requisito.gxTpr_Requisito_ordem;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Requisito_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Requisito_Ordem), 3, 0)));
            AV21Requisito_Restricao = AV14Requisito.gxTpr_Requisito_restricao;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Requisito_Restricao", AV21Requisito_Restricao);
            AV23Requisito_Pontuacao = AV14Requisito.gxTpr_Requisito_pontuacao;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Requisito_Pontuacao", StringUtil.LTrim( StringUtil.Str( AV23Requisito_Pontuacao, 14, 5)));
            AV37Requisito_TipoReqCod = AV14Requisito.gxTpr_Requisito_tiporeqcod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Requisito_TipoReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37Requisito_TipoReqCod), 6, 0)));
            AV22Requisito_Ordem = AV14Requisito.gxTpr_Requisito_ordem;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Requisito_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Requisito_Ordem), 3, 0)));
            AV33Requisito_ReferenciaTecnica = AV14Requisito.gxTpr_Requisito_referenciatecnica;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Requisito_ReferenciaTecnica", AV33Requisito_ReferenciaTecnica);
         }
      }

      protected void E12Q92( )
      {
         /* Refresh Routine */
         /* Using cursor H00Q910 */
         pr_default.execute(5, new Object[] {AV31Requisito_ReqCod});
         if ( (pr_default.getStatus(5) != 101) )
         {
            A40000GXC1 = H00Q910_A40000GXC1[0];
         }
         else
         {
            A40000GXC1 = 0;
         }
         pr_default.close(5);
         /* Using cursor H00Q912 */
         pr_default.execute(6, new Object[] {AV14Requisito.gxTpr_Requisito_reqcod});
         if ( (pr_default.getStatus(6) != 101) )
         {
            A40001GXC1 = H00Q912_A40001GXC1[0];
         }
         else
         {
            A40001GXC1 = 0;
         }
         pr_default.close(6);
         if ( AV31Requisito_ReqCod > 0 )
         {
            AV29OrdemMax = (short)(A40000GXC1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29OrdemMax", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29OrdemMax), 4, 0)));
            AV22Requisito_Ordem = (short)(AV29OrdemMax+1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Requisito_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Requisito_Ordem), 3, 0)));
         }
         else
         {
            AV29OrdemMax = (short)(A40001GXC1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29OrdemMax", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29OrdemMax), 4, 0)));
         }
         if ( AV18Requisito_Codigo > 0 )
         {
            GX_FocusControl = bttBtnfechar_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
      }

      protected void E13Q92( )
      {
         /* 'Rascunho' Routine */
         /* Execute user subroutine: 'CHECKDATA' */
         S112 ();
         if (returnInSub) return;
         if ( AV30Ok )
         {
            /* Execute user subroutine: 'SETREQUISITO' */
            S122 ();
            if (returnInSub) return;
            AV14Requisito.gxTpr_Requisito_status = 0;
            AV14Requisito.Save();
            if ( AV14Requisito.Success() )
            {
               context.CommitDataStores( "WP_NovoRequisito");
            }
            else
            {
               context.RollbackDataStores( "WP_NovoRequisito");
               /* Execute user subroutine: 'SHOW ERROR MESSAGES' */
               S132 ();
               if (returnInSub) return;
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV14Requisito", AV14Requisito);
      }

      public void GXEnter( )
      {
         /* Execute user event: E14Q92 */
         E14Q92 ();
         if (returnInSub) return;
      }

      protected void E14Q92( )
      {
         /* Enter Routine */
         /* Execute user subroutine: 'CHECKDATA' */
         S112 ();
         if (returnInSub) return;
         if ( AV30Ok )
         {
            /* Execute user subroutine: 'SETREQUISITO' */
            S122 ();
            if (returnInSub) return;
            AV14Requisito.Save();
            if ( AV14Requisito.Success() )
            {
               if ( (0==AV18Requisito_Codigo) )
               {
                  AV36ContagemResultadoRequisito = new SdtContagemResultadoRequisito(context);
                  AV36ContagemResultadoRequisito.gxTpr_Contagemresultadorequisito_oscod = AV34OSCod;
                  AV36ContagemResultadoRequisito.gxTpr_Contagemresultadorequisito_reqcod = AV14Requisito.gxTpr_Requisito_codigo;
                  AV36ContagemResultadoRequisito.gxTpr_Contagemresultadorequisito_owner = true;
                  AV36ContagemResultadoRequisito.Save();
                  if ( AV36ContagemResultadoRequisito.Success() )
                  {
                     context.CommitDataStores( "WP_NovoRequisito");
                     context.setWebReturnParms(new Object[] {});
                     context.wjLocDisableFrm = 1;
                     context.nUserReturn = 1;
                     returnInSub = true;
                     if (true) return;
                  }
                  else
                  {
                     context.RollbackDataStores( "WP_NovoRequisito");
                     /* Execute user subroutine: 'SHOW ERROR MESSAGES' */
                     S132 ();
                     if (returnInSub) return;
                  }
               }
               else
               {
                  context.CommitDataStores( "WP_NovoRequisito");
                  context.setWebReturnParms(new Object[] {});
                  context.wjLocDisableFrm = 1;
                  context.nUserReturn = 1;
                  returnInSub = true;
                  if (true) return;
               }
            }
            else
            {
               context.RollbackDataStores( "WP_NovoRequisito");
               /* Execute user subroutine: 'SHOW ERROR MESSAGES' */
               S132 ();
               if (returnInSub) return;
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV14Requisito", AV14Requisito);
      }

      protected void E15Q92( )
      {
         /* 'Fechar' Routine */
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void S122( )
      {
         /* 'SETREQUISITO' Routine */
         if ( (0==AV18Requisito_Codigo) )
         {
            AV14Requisito = new SdtRequisito(context);
            AV14Requisito.gxTv_SdtRequisito_Proposta_codigo_SetNull();
            AV14Requisito.gxTpr_Requisito_status = 1;
         }
         if ( AV22Requisito_Ordem > AV29OrdemMax )
         {
            AV22Requisito_Ordem = AV29OrdemMax;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Requisito_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Requisito_Ordem), 3, 0)));
         }
         else
         {
            /* Execute user subroutine: 'ORDEM' */
            S142 ();
            if (returnInSub) return;
         }
         AV14Requisito.gxTpr_Requisito_titulo = AV9Requisito_Titulo;
         AV14Requisito.gxTpr_Requisito_descricao = AV8Solucao;
         AV14Requisito.gxTpr_Requisito_agrupador = AV19Requisito_Agrupador;
         AV14Requisito.gxTpr_Requisito_ordem = AV22Requisito_Ordem;
         AV14Requisito.gxTpr_Requisito_restricao = AV21Requisito_Restricao;
         AV14Requisito.gxTpr_Requisito_pontuacao = AV23Requisito_Pontuacao;
         AV14Requisito.gxTpr_Requisito_tiporeqcod = AV37Requisito_TipoReqCod;
         AV14Requisito.gxTpr_Requisito_referenciatecnica = AV33Requisito_ReferenciaTecnica;
         if ( AV31Requisito_ReqCod > 0 )
         {
            AV14Requisito.gxTpr_Requisito_reqcod = AV31Requisito_ReqCod;
         }
         else
         {
            if ( (0==AV18Requisito_Codigo) )
            {
               AV14Requisito.gxTv_SdtRequisito_Requisito_reqcod_SetNull();
            }
         }
      }

      protected void S132( )
      {
         /* 'SHOW ERROR MESSAGES' Routine */
         AV44GXV2 = 1;
         AV43GXV1 = AV14Requisito.GetMessages();
         while ( AV44GXV2 <= AV43GXV1.Count )
         {
            AV15Message = ((SdtMessages_Message)AV43GXV1.Item(AV44GXV2));
            if ( AV15Message.gxTpr_Type == 1 )
            {
               GX_msglist.addItem(AV15Message.gxTpr_Description);
            }
            AV44GXV2 = (int)(AV44GXV2+1);
         }
      }

      protected void S112( )
      {
         /* 'CHECKDATA' Routine */
         AV30Ok = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Ok", AV30Ok);
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV9Requisito_Titulo)) )
         {
            AV30Ok = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Ok", AV30Ok);
            GX_msglist.addItem("T�tulo � obrigat�rio!");
            GX_FocusControl = edtavRequisito_titulo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( (0==AV37Requisito_TipoReqCod) )
         {
            AV30Ok = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Ok", AV30Ok);
            GX_msglist.addItem("Tipo � obrigat�rio!");
            GX_FocusControl = dynavRequisito_tiporeqcod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV8Solucao)) )
         {
            AV30Ok = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Ok", AV30Ok);
            GX_msglist.addItem("Solu��o � obrigat�ria!");
            GX_FocusControl = Solucao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
      }

      protected void S142( )
      {
         /* 'ORDEM' Routine */
         Gx_mode = AV14Requisito.GetMode();
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            new prc_alterarordem(context ).execute(  AV34OSCod,  AV29OrdemMax,  AV22Requisito_Ordem,  "Req",  "+") ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34OSCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vOSCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV34OSCod), "ZZZZZ9")));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29OrdemMax", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29OrdemMax), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Requisito_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Requisito_Ordem), 3, 0)));
         }
         else if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
         {
            if ( AV22Requisito_Ordem < AV14Requisito.gxTpr_Requisito_ordem )
            {
               new prc_alterarordem(context ).execute(  AV34OSCod,  AV14Requisito.gxTpr_Requisito_ordem,  AV22Requisito_Ordem,  "Req",  "+") ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34OSCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vOSCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV34OSCod), "ZZZZZ9")));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Requisito_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Requisito_Ordem), 3, 0)));
            }
            if ( AV22Requisito_Ordem > AV14Requisito.gxTpr_Requisito_ordem )
            {
               new prc_alterarordem(context ).execute(  AV34OSCod,  AV14Requisito.gxTpr_Requisito_ordem,  AV22Requisito_Ordem,  "Req",  "-") ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34OSCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vOSCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV34OSCod), "ZZZZZ9")));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Requisito_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Requisito_Ordem), 3, 0)));
            }
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E16Q92( )
      {
         /* Load Routine */
      }

      protected void wb_table1_3_Q92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " height: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "classref", 0, "center", "", 5, 5, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td rowspan=\"2\"  style=\""+CSSHelper.Prettify( "width:84px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock12_Internalname, "Necessidade", "", "", lblTextblock12_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_NovoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:111px")+"\">") ;
            context.WriteHtmlText( "&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock3_Internalname, "Identificador", "", "", lblTextblock3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_NovoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"6\" >") ;
            context.WriteHtmlText( "&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock4_Internalname, "Nome", "", "", lblTextblock4_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_NovoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td bordercolor=\"#000000\"  class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavIdentificador_Internalname, AV6Identificador, StringUtil.RTrim( context.localUtil.Format( AV6Identificador, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,13);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavIdentificador_Jsonclick, 0, "Attribute", "", "", "", 1, edtavIdentificador_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_NovoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td bordercolor=\"#000000\" colspan=\"6\"  style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavNome_Internalname, AV7Nome, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,15);\"", 0, 1, edtavNome_Enabled, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "250", -1, "", "", -1, true, "", "HLP_WP_NovoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"8\" >") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock6_Internalname, "Solu��o T�cnica", "", "", lblTextblock6_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_NovoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock5_Internalname, "Titulo", "", "", lblTextblock5_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_NovoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"7\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavRequisito_titulo_Internalname, AV9Requisito_Titulo, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"", 0, 1, 1, 0, 100, "%", 4, "row", StyleString, ClassString, "", "250", 1, "", "", -1, true, "", "HLP_WP_NovoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock7_Internalname, "Agrupador", "", "", lblTextblock7_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_NovoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavRequisito_agrupador_Internalname, AV19Requisito_Agrupador, StringUtil.RTrim( context.localUtil.Format( AV19Requisito_Agrupador, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavRequisito_agrupador_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 25, "chr", 1, "row", 25, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_NovoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock8_Internalname, "Tipo", "", "", lblTextblock8_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_NovoRequisito.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavRequisito_tiporeqcod, dynavRequisito_tiporeqcod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV37Requisito_TipoReqCod), 6, 0)), 1, dynavRequisito_tiporeqcod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_WP_NovoRequisito.htm");
            dynavRequisito_tiporeqcod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV37Requisito_TipoReqCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavRequisito_tiporeqcod_Internalname, "Values", (String)(dynavRequisito_tiporeqcod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock9_Internalname, "Ordem", "", "", lblTextblock9_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_NovoRequisito.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavRequisito_ordem_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22Requisito_Ordem), 3, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV22Requisito_Ordem), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavRequisito_ordem_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_NovoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock11_Internalname, "Pontua��o", "", "", lblTextblock11_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_NovoRequisito.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavRequisito_pontuacao_Internalname, StringUtil.LTrim( StringUtil.NToC( AV23Requisito_Pontuacao, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV23Requisito_Pontuacao, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,37);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavRequisito_pontuacao_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_NovoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:100%")+"\" class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "Descri��o", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_NovoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"7\"  style=\""+CSSHelper.Prettify( "height:100%;width:100%")+"\">") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"SOLUCAOContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock10_Internalname, "Restri��es", "", "", lblTextblock10_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_NovoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"7\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavRequisito_restricao_Internalname, AV21Requisito_Restricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,47);\"", 0, 1, 1, 0, 100, "%", 4, "row", StyleString, ClassString, "", "2097152", 1, "", "", -1, true, "", "HLP_WP_NovoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock13_Internalname, "Ref. T�c.", "", "", lblTextblock13_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_NovoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"7\" >") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavRequisito_referenciatecnica_Internalname, AV33Requisito_ReferenciaTecnica, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,52);\"", 0, 1, 1, 0, 100, "%", 4, "row", StyleString, ClassString, "", "2097152", 1, "", "", -1, true, "", "HLP_WP_NovoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"8\" >") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, "Script", "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_WP_NovoRequisito.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdemmax_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV29OrdemMax), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV29OrdemMax), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,56);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdemmax_Jsonclick, 0, "Attribute", "", "", "", edtavOrdemmax_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_NovoRequisito.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLinhanegocio_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38LinhaNegocio_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV38LinhaNegocio_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,57);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLinhanegocio_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavLinhanegocio_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_WP_NovoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\" colspan=\"8\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'',0)\"";
            ClassString = "ActionButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnrascunho_Internalname, "", "Salvar Rascunho", bttBtnrascunho_Jsonclick, 5, "Salvar Rascunho", "", StyleString, ClassString, bttBtnrascunho_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'RASCUNHO\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_NovoRequisito.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnenter_Internalname, "", "Confirmar", bttBtnenter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_NovoRequisito.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'FECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_NovoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_3_Q92e( true) ;
         }
         else
         {
            wb_table1_3_Q92e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV18Requisito_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Requisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18Requisito_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vREQUISITO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV18Requisito_Codigo), "ZZZZZ9")));
         AV31Requisito_ReqCod = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Requisito_ReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31Requisito_ReqCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vREQUISITO_REQCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV31Requisito_ReqCod), "ZZZZZ9")));
         AV34OSCod = Convert.ToInt32(getParm(obj,2));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34OSCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vOSCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV34OSCod), "ZZZZZ9")));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAQ92( ) ;
         WSQ92( ) ;
         WEQ92( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202032423502171");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_novorequisito.js", "?202032423502171");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblock12_Internalname = "TEXTBLOCK12";
         lblTextblock3_Internalname = "TEXTBLOCK3";
         lblTextblock4_Internalname = "TEXTBLOCK4";
         edtavIdentificador_Internalname = "vIDENTIFICADOR";
         edtavNome_Internalname = "vNOME";
         lblTextblock6_Internalname = "TEXTBLOCK6";
         lblTextblock5_Internalname = "TEXTBLOCK5";
         edtavRequisito_titulo_Internalname = "vREQUISITO_TITULO";
         lblTextblock7_Internalname = "TEXTBLOCK7";
         edtavRequisito_agrupador_Internalname = "vREQUISITO_AGRUPADOR";
         lblTextblock8_Internalname = "TEXTBLOCK8";
         dynavRequisito_tiporeqcod_Internalname = "vREQUISITO_TIPOREQCOD";
         lblTextblock9_Internalname = "TEXTBLOCK9";
         edtavRequisito_ordem_Internalname = "vREQUISITO_ORDEM";
         lblTextblock11_Internalname = "TEXTBLOCK11";
         edtavRequisito_pontuacao_Internalname = "vREQUISITO_PONTUACAO";
         lblTextblock2_Internalname = "TEXTBLOCK2";
         Solucao_Internalname = "SOLUCAO";
         lblTextblock10_Internalname = "TEXTBLOCK10";
         edtavRequisito_restricao_Internalname = "vREQUISITO_RESTRICAO";
         lblTextblock13_Internalname = "TEXTBLOCK13";
         edtavRequisito_referenciatecnica_Internalname = "vREQUISITO_REFERENCIATECNICA";
         lblTbjava_Internalname = "TBJAVA";
         edtavOrdemmax_Internalname = "vORDEMMAX";
         edtavLinhanegocio_codigo_Internalname = "vLINHANEGOCIO_CODIGO";
         bttBtnrascunho_Internalname = "BTNRASCUNHO";
         bttBtnenter_Internalname = "BTNENTER";
         bttBtnfechar_Internalname = "BTNFECHAR";
         tblTable1_Internalname = "TABLE1";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         bttBtnrascunho_Visible = 1;
         edtavLinhanegocio_codigo_Jsonclick = "";
         edtavOrdemmax_Jsonclick = "";
         lblTbjava_Visible = 1;
         Solucao_Enabled = Convert.ToBoolean( 1);
         edtavRequisito_pontuacao_Jsonclick = "";
         edtavRequisito_ordem_Jsonclick = "";
         dynavRequisito_tiporeqcod_Jsonclick = "";
         edtavRequisito_agrupador_Jsonclick = "";
         edtavNome_Enabled = 1;
         edtavIdentificador_Jsonclick = "";
         edtavIdentificador_Enabled = 1;
         edtavLinhanegocio_codigo_Visible = 1;
         edtavOrdemmax_Visible = 1;
         Solucao_Toolbarexpanded = Convert.ToBoolean( 0);
         Solucao_Toolbar = "Default";
         Solucao_Height = "100%";
         Solucao_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Novo Requisito T�cnico";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public void Validv_Linhanegocio_codigo( int GX_Parm1 ,
                                              GXCombobox dynGX_Parm2 )
      {
         AV38LinhaNegocio_Codigo = GX_Parm1;
         dynavRequisito_tiporeqcod = dynGX_Parm2;
         AV37Requisito_TipoReqCod = (int)(NumberUtil.Val( dynavRequisito_tiporeqcod.CurrentValue, "."));
         GXVvREQUISITO_TIPOREQCOD_htmlQ92( AV38LinhaNegocio_Codigo) ;
         dynload_actions( ) ;
         if ( dynavRequisito_tiporeqcod.ItemCount > 0 )
         {
            AV37Requisito_TipoReqCod = (int)(NumberUtil.Val( dynavRequisito_tiporeqcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV37Requisito_TipoReqCod), 6, 0))), "."));
         }
         dynavRequisito_tiporeqcod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV37Requisito_TipoReqCod), 6, 0));
         isValidOutput.Add(dynavRequisito_tiporeqcod);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'AV31Requisito_ReqCod',fld:'vREQUISITO_REQCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV14Requisito',fld:'vREQUISITO',pic:'',nv:null},{av:'AV18Requisito_Codigo',fld:'vREQUISITO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV22Requisito_Ordem',fld:'vREQUISITO_ORDEM',pic:'ZZ9',nv:0},{av:'AV29OrdemMax',fld:'vORDEMMAX',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("'RASCUNHO'","{handler:'E13Q92',iparms:[{av:'AV30Ok',fld:'vOK',pic:'',nv:false},{av:'AV14Requisito',fld:'vREQUISITO',pic:'',nv:null},{av:'AV9Requisito_Titulo',fld:'vREQUISITO_TITULO',pic:'',nv:''},{av:'AV37Requisito_TipoReqCod',fld:'vREQUISITO_TIPOREQCOD',pic:'ZZZZZ9',nv:0},{av:'AV8Solucao',fld:'vSOLUCAO',pic:'',nv:''},{av:'AV18Requisito_Codigo',fld:'vREQUISITO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV22Requisito_Ordem',fld:'vREQUISITO_ORDEM',pic:'ZZ9',nv:0},{av:'AV29OrdemMax',fld:'vORDEMMAX',pic:'ZZZ9',nv:0},{av:'AV19Requisito_Agrupador',fld:'vREQUISITO_AGRUPADOR',pic:'',nv:''},{av:'AV21Requisito_Restricao',fld:'vREQUISITO_RESTRICAO',pic:'',nv:''},{av:'AV23Requisito_Pontuacao',fld:'vREQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV33Requisito_ReferenciaTecnica',fld:'vREQUISITO_REFERENCIATECNICA',pic:'',nv:''},{av:'AV31Requisito_ReqCod',fld:'vREQUISITO_REQCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV34OSCod',fld:'vOSCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV14Requisito',fld:'vREQUISITO',pic:'',nv:null},{av:'AV30Ok',fld:'vOK',pic:'',nv:false},{av:'AV22Requisito_Ordem',fld:'vREQUISITO_ORDEM',pic:'ZZ9',nv:0}]}");
         setEventMetadata("ENTER","{handler:'E14Q92',iparms:[{av:'AV30Ok',fld:'vOK',pic:'',nv:false},{av:'AV14Requisito',fld:'vREQUISITO',pic:'',nv:null},{av:'AV18Requisito_Codigo',fld:'vREQUISITO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV34OSCod',fld:'vOSCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV9Requisito_Titulo',fld:'vREQUISITO_TITULO',pic:'',nv:''},{av:'AV37Requisito_TipoReqCod',fld:'vREQUISITO_TIPOREQCOD',pic:'ZZZZZ9',nv:0},{av:'AV8Solucao',fld:'vSOLUCAO',pic:'',nv:''},{av:'AV22Requisito_Ordem',fld:'vREQUISITO_ORDEM',pic:'ZZ9',nv:0},{av:'AV29OrdemMax',fld:'vORDEMMAX',pic:'ZZZ9',nv:0},{av:'AV19Requisito_Agrupador',fld:'vREQUISITO_AGRUPADOR',pic:'',nv:''},{av:'AV21Requisito_Restricao',fld:'vREQUISITO_RESTRICAO',pic:'',nv:''},{av:'AV23Requisito_Pontuacao',fld:'vREQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV33Requisito_ReferenciaTecnica',fld:'vREQUISITO_REFERENCIATECNICA',pic:'',nv:''},{av:'AV31Requisito_ReqCod',fld:'vREQUISITO_REQCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV30Ok',fld:'vOK',pic:'',nv:false},{av:'AV14Requisito',fld:'vREQUISITO',pic:'',nv:null},{av:'AV22Requisito_Ordem',fld:'vREQUISITO_ORDEM',pic:'ZZ9',nv:0}]}");
         setEventMetadata("'FECHAR'","{handler:'E15Q92',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV8Solucao = "";
         AV14Requisito = new SdtRequisito(context);
         AV6Identificador = "";
         AV7Nome = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00Q92_A2041TipoRequisito_Codigo = new int[1] ;
         H00Q92_A2042TipoRequisito_Identificador = new String[] {""} ;
         H00Q92_A2039TipoRequisito_LinNegCod = new int[1] ;
         H00Q92_n2039TipoRequisito_LinNegCod = new bool[] {false} ;
         H00Q94_A40000GXC1 = new int[1] ;
         H00Q96_A40001GXC1 = new int[1] ;
         AV9Requisito_Titulo = "";
         AV19Requisito_Agrupador = "";
         AV21Requisito_Restricao = "";
         AV33Requisito_ReferenciaTecnica = "";
         AV24WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV10FCKEditorMenuItem = new SdtFckEditorMenu_FckEditorMenuItem(context);
         AV13Url = "";
         AV12FCKEditorMenu = new GxObjectCollection( context, "FckEditorMenu.FckEditorMenuItem", "GxEv3Up14_Meetrika", "SdtFckEditorMenu_FckEditorMenuItem", "GeneXus.Programs");
         H00Q97_A2005ContagemResultadoRequisito_Codigo = new int[1] ;
         H00Q97_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00Q97_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00Q97_A601ContagemResultado_Servico = new int[1] ;
         H00Q97_n601ContagemResultado_Servico = new bool[] {false} ;
         H00Q97_A2004ContagemResultadoRequisito_ReqCod = new int[1] ;
         H00Q97_A2003ContagemResultadoRequisito_OSCod = new int[1] ;
         H00Q97_A2001Requisito_Identificador = new String[] {""} ;
         H00Q97_n2001Requisito_Identificador = new bool[] {false} ;
         H00Q97_A1927Requisito_Titulo = new String[] {""} ;
         H00Q97_n1927Requisito_Titulo = new bool[] {false} ;
         H00Q97_A2050ContagemResultado_SrvLnhNegCod = new int[1] ;
         H00Q97_n2050ContagemResultado_SrvLnhNegCod = new bool[] {false} ;
         A2001Requisito_Identificador = "";
         A1927Requisito_Titulo = "";
         H00Q98_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00Q98_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00Q98_A601ContagemResultado_Servico = new int[1] ;
         H00Q98_n601ContagemResultado_Servico = new bool[] {false} ;
         H00Q98_A456ContagemResultado_Codigo = new int[1] ;
         H00Q98_A2050ContagemResultado_SrvLnhNegCod = new int[1] ;
         H00Q98_n2050ContagemResultado_SrvLnhNegCod = new bool[] {false} ;
         H00Q910_A40000GXC1 = new int[1] ;
         H00Q912_A40001GXC1 = new int[1] ;
         AV36ContagemResultadoRequisito = new SdtContagemResultadoRequisito(context);
         AV43GXV1 = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         AV15Message = new SdtMessages_Message(context);
         Gx_mode = "";
         sStyleString = "";
         lblTextblock12_Jsonclick = "";
         lblTextblock3_Jsonclick = "";
         lblTextblock4_Jsonclick = "";
         TempTags = "";
         lblTextblock6_Jsonclick = "";
         lblTextblock5_Jsonclick = "";
         lblTextblock7_Jsonclick = "";
         lblTextblock8_Jsonclick = "";
         lblTextblock9_Jsonclick = "";
         lblTextblock11_Jsonclick = "";
         lblTextblock2_Jsonclick = "";
         lblTextblock10_Jsonclick = "";
         lblTextblock13_Jsonclick = "";
         lblTbjava_Jsonclick = "";
         bttBtnrascunho_Jsonclick = "";
         bttBtnenter_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_novorequisito__default(),
            new Object[][] {
                new Object[] {
               H00Q92_A2041TipoRequisito_Codigo, H00Q92_A2042TipoRequisito_Identificador, H00Q92_A2039TipoRequisito_LinNegCod, H00Q92_n2039TipoRequisito_LinNegCod
               }
               , new Object[] {
               H00Q94_A40000GXC1
               }
               , new Object[] {
               H00Q96_A40001GXC1
               }
               , new Object[] {
               H00Q97_A2005ContagemResultadoRequisito_Codigo, H00Q97_A1553ContagemResultado_CntSrvCod, H00Q97_n1553ContagemResultado_CntSrvCod, H00Q97_A601ContagemResultado_Servico, H00Q97_n601ContagemResultado_Servico, H00Q97_A2004ContagemResultadoRequisito_ReqCod, H00Q97_A2003ContagemResultadoRequisito_OSCod, H00Q97_A2001Requisito_Identificador, H00Q97_n2001Requisito_Identificador, H00Q97_A1927Requisito_Titulo,
               H00Q97_n1927Requisito_Titulo, H00Q97_A2050ContagemResultado_SrvLnhNegCod, H00Q97_n2050ContagemResultado_SrvLnhNegCod
               }
               , new Object[] {
               H00Q98_A1553ContagemResultado_CntSrvCod, H00Q98_n1553ContagemResultado_CntSrvCod, H00Q98_A601ContagemResultado_Servico, H00Q98_n601ContagemResultado_Servico, H00Q98_A456ContagemResultado_Codigo, H00Q98_A2050ContagemResultado_SrvLnhNegCod, H00Q98_n2050ContagemResultado_SrvLnhNegCod
               }
               , new Object[] {
               H00Q910_A40000GXC1
               }
               , new Object[] {
               H00Q912_A40001GXC1
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavIdentificador_Enabled = 0;
         edtavNome_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV22Requisito_Ordem ;
      private short AV29OrdemMax ;
      private short nGXWrapped ;
      private short wbTemp ;
      private int AV18Requisito_Codigo ;
      private int AV31Requisito_ReqCod ;
      private int AV34OSCod ;
      private int wcpOAV18Requisito_Codigo ;
      private int wcpOAV31Requisito_ReqCod ;
      private int wcpOAV34OSCod ;
      private int AV38LinhaNegocio_Codigo ;
      private int A40000GXC1 ;
      private int A40001GXC1 ;
      private int gxdynajaxindex ;
      private int AV37Requisito_TipoReqCod ;
      private int edtavIdentificador_Enabled ;
      private int edtavNome_Enabled ;
      private int bttBtnrascunho_Visible ;
      private int edtavOrdemmax_Visible ;
      private int lblTbjava_Visible ;
      private int edtavLinhanegocio_codigo_Visible ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A601ContagemResultado_Servico ;
      private int A2004ContagemResultadoRequisito_ReqCod ;
      private int A2003ContagemResultadoRequisito_OSCod ;
      private int A2050ContagemResultado_SrvLnhNegCod ;
      private int A456ContagemResultado_Codigo ;
      private int AV44GXV2 ;
      private int idxLst ;
      private decimal AV23Requisito_Pontuacao ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Solucao_Width ;
      private String Solucao_Height ;
      private String Solucao_Toolbar ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String ClassString ;
      private String StyleString ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavIdentificador_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String edtavNome_Internalname ;
      private String edtavRequisito_titulo_Internalname ;
      private String edtavRequisito_agrupador_Internalname ;
      private String dynavRequisito_tiporeqcod_Internalname ;
      private String edtavRequisito_ordem_Internalname ;
      private String edtavRequisito_pontuacao_Internalname ;
      private String edtavRequisito_restricao_Internalname ;
      private String edtavRequisito_referenciatecnica_Internalname ;
      private String edtavOrdemmax_Internalname ;
      private String edtavLinhanegocio_codigo_Internalname ;
      private String AV13Url ;
      private String bttBtnrascunho_Internalname ;
      private String lblTbjava_Internalname ;
      private String bttBtnfechar_Internalname ;
      private String Solucao_Internalname ;
      private String Gx_mode ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String lblTextblock12_Internalname ;
      private String lblTextblock12_Jsonclick ;
      private String lblTextblock3_Internalname ;
      private String lblTextblock3_Jsonclick ;
      private String lblTextblock4_Internalname ;
      private String lblTextblock4_Jsonclick ;
      private String TempTags ;
      private String edtavIdentificador_Jsonclick ;
      private String lblTextblock6_Internalname ;
      private String lblTextblock6_Jsonclick ;
      private String lblTextblock5_Internalname ;
      private String lblTextblock5_Jsonclick ;
      private String lblTextblock7_Internalname ;
      private String lblTextblock7_Jsonclick ;
      private String edtavRequisito_agrupador_Jsonclick ;
      private String lblTextblock8_Internalname ;
      private String lblTextblock8_Jsonclick ;
      private String dynavRequisito_tiporeqcod_Jsonclick ;
      private String lblTextblock9_Internalname ;
      private String lblTextblock9_Jsonclick ;
      private String edtavRequisito_ordem_Jsonclick ;
      private String lblTextblock11_Internalname ;
      private String lblTextblock11_Jsonclick ;
      private String edtavRequisito_pontuacao_Jsonclick ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String lblTextblock10_Internalname ;
      private String lblTextblock10_Jsonclick ;
      private String lblTextblock13_Internalname ;
      private String lblTextblock13_Jsonclick ;
      private String lblTbjava_Jsonclick ;
      private String edtavOrdemmax_Jsonclick ;
      private String edtavLinhanegocio_codigo_Jsonclick ;
      private String bttBtnrascunho_Jsonclick ;
      private String bttBtnenter_Internalname ;
      private String bttBtnenter_Jsonclick ;
      private String bttBtnfechar_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool AV30Ok ;
      private bool Solucao_Enabled ;
      private bool Solucao_Toolbarexpanded ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n2001Requisito_Identificador ;
      private bool n1927Requisito_Titulo ;
      private bool n2050ContagemResultado_SrvLnhNegCod ;
      private String AV8Solucao ;
      private String AV21Requisito_Restricao ;
      private String AV33Requisito_ReferenciaTecnica ;
      private String AV6Identificador ;
      private String AV7Nome ;
      private String AV9Requisito_Titulo ;
      private String AV19Requisito_Agrupador ;
      private String A2001Requisito_Identificador ;
      private String A1927Requisito_Titulo ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavRequisito_tiporeqcod ;
      private IDataStoreProvider pr_default ;
      private int[] H00Q92_A2041TipoRequisito_Codigo ;
      private String[] H00Q92_A2042TipoRequisito_Identificador ;
      private int[] H00Q92_A2039TipoRequisito_LinNegCod ;
      private bool[] H00Q92_n2039TipoRequisito_LinNegCod ;
      private int[] H00Q94_A40000GXC1 ;
      private int[] H00Q96_A40001GXC1 ;
      private int[] H00Q97_A2005ContagemResultadoRequisito_Codigo ;
      private int[] H00Q97_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00Q97_n1553ContagemResultado_CntSrvCod ;
      private int[] H00Q97_A601ContagemResultado_Servico ;
      private bool[] H00Q97_n601ContagemResultado_Servico ;
      private int[] H00Q97_A2004ContagemResultadoRequisito_ReqCod ;
      private int[] H00Q97_A2003ContagemResultadoRequisito_OSCod ;
      private String[] H00Q97_A2001Requisito_Identificador ;
      private bool[] H00Q97_n2001Requisito_Identificador ;
      private String[] H00Q97_A1927Requisito_Titulo ;
      private bool[] H00Q97_n1927Requisito_Titulo ;
      private int[] H00Q97_A2050ContagemResultado_SrvLnhNegCod ;
      private bool[] H00Q97_n2050ContagemResultado_SrvLnhNegCod ;
      private int[] H00Q98_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00Q98_n1553ContagemResultado_CntSrvCod ;
      private int[] H00Q98_A601ContagemResultado_Servico ;
      private bool[] H00Q98_n601ContagemResultado_Servico ;
      private int[] H00Q98_A456ContagemResultado_Codigo ;
      private int[] H00Q98_A2050ContagemResultado_SrvLnhNegCod ;
      private bool[] H00Q98_n2050ContagemResultado_SrvLnhNegCod ;
      private int[] H00Q910_A40000GXC1 ;
      private int[] H00Q912_A40001GXC1 ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( SdtFckEditorMenu_FckEditorMenuItem ))]
      private IGxCollection AV12FCKEditorMenu ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV43GXV1 ;
      private GXWebForm Form ;
      private SdtFckEditorMenu_FckEditorMenuItem AV10FCKEditorMenuItem ;
      private SdtContagemResultadoRequisito AV36ContagemResultadoRequisito ;
      private SdtMessages_Message AV15Message ;
      private SdtRequisito AV14Requisito ;
      private wwpbaseobjects.SdtWWPContext AV24WWPContext ;
   }

   public class wp_novorequisito__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00Q92 ;
          prmH00Q92 = new Object[] {
          new Object[] {"@AV38LinhaNegocio_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00Q94 ;
          prmH00Q94 = new Object[] {
          new Object[] {"@AV31Requisito_ReqCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00Q96 ;
          prmH00Q96 = new Object[] {
          new Object[] {"@AV14Requ_1Requisito_reqcod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00Q97 ;
          prmH00Q97 = new Object[] {
          new Object[] {"@AV31Requisito_ReqCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV34OSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00Q98 ;
          prmH00Q98 = new Object[] {
          new Object[] {"@AV34OSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00Q910 ;
          prmH00Q910 = new Object[] {
          new Object[] {"@AV31Requisito_ReqCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00Q912 ;
          prmH00Q912 = new Object[] {
          new Object[] {"@AV14Requ_1Requisito_reqcod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00Q92", "SELECT [TipoRequisito_Codigo], [TipoRequisito_Identificador], [TipoRequisito_LinNegCod] FROM [TipoRequisito] WITH (NOLOCK) WHERE [TipoRequisito_LinNegCod] = @AV38LinhaNegocio_Codigo or (@AV38LinhaNegocio_Codigo = convert(int, 0)) ORDER BY [TipoRequisito_Identificador] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00Q92,0,0,true,false )
             ,new CursorDef("H00Q94", "SELECT COALESCE( T1.[GXC1], 0) AS GXC1 FROM (SELECT COUNT(*) AS GXC1 FROM [Requisito] WITH (NOLOCK) WHERE [Requisito_ReqCod] = @AV31Requisito_ReqCod ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00Q94,1,0,true,false )
             ,new CursorDef("H00Q96", "SELECT COALESCE( T1.[GXC1], 0) AS GXC2 FROM (SELECT COUNT(*) AS GXC1 FROM [Requisito] WITH (NOLOCK) WHERE [Requisito_ReqCod] = @AV14Requ_1Requisito_reqcod ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00Q96,1,0,true,false )
             ,new CursorDef("H00Q97", "SELECT TOP 1 T1.[ContagemResultadoRequisito_Codigo], T3.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T4.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultadoRequisito_ReqCod] AS ContagemResultadoRequisito_ReqCod, T1.[ContagemResultadoRequisito_OSCod] AS ContagemResultadoRequisito_OSCod, T2.[Requisito_Identificador], T2.[Requisito_Titulo], T5.[Servico_LinNegCod] AS ContagemResultado_SrvLnhNegCod FROM (((([ContagemResultadoRequisito] T1 WITH (NOLOCK) INNER JOIN [Requisito] T2 WITH (NOLOCK) ON T2.[Requisito_Codigo] = T1.[ContagemResultadoRequisito_ReqCod]) INNER JOIN [ContagemResultado] T3 WITH (NOLOCK) ON T3.[ContagemResultado_Codigo] = T1.[ContagemResultadoRequisito_OSCod]) LEFT JOIN [ContratoServicos] T4 WITH (NOLOCK) ON T4.[ContratoServicos_Codigo] = T3.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T5 WITH (NOLOCK) ON T5.[Servico_Codigo] = T4.[Servico_Codigo]) WHERE (T1.[ContagemResultadoRequisito_ReqCod] = @AV31Requisito_ReqCod) AND (T1.[ContagemResultadoRequisito_OSCod] = @AV34OSCod) ORDER BY T1.[ContagemResultadoRequisito_ReqCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00Q97,1,0,false,true )
             ,new CursorDef("H00Q98", "SELECT TOP 1 T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_Codigo], T3.[Servico_LinNegCod] AS ContagemResultado_SrvLnhNegCod FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) WHERE T1.[ContagemResultado_Codigo] = @AV34OSCod ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00Q98,1,0,false,true )
             ,new CursorDef("H00Q910", "SELECT COALESCE( T1.[GXC1], 0) AS GXC1 FROM (SELECT COUNT(*) AS GXC1 FROM [Requisito] WITH (NOLOCK) WHERE [Requisito_ReqCod] = @AV31Requisito_ReqCod ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00Q910,1,0,true,false )
             ,new CursorDef("H00Q912", "SELECT COALESCE( T1.[GXC1], 0) AS GXC2 FROM (SELECT COUNT(*) AS GXC1 FROM [Requisito] WITH (NOLOCK) WHERE [Requisito_ReqCod] = @AV14Requ_1Requisito_reqcod ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00Q912,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
