/*
               File: PRC_GetHmlgComPnd
        Description: Contratante homologa com pendÍncias
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:9.18
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_gethmlgcompnd : GXProcedure
   {
      public prc_gethmlgcompnd( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_gethmlgcompnd( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_AreaTrabalho_Codigo ,
                           out bool aP1_HmlgComPnd )
      {
         this.A5AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV8HmlgComPnd = false ;
         initialize();
         executePrivate();
         aP0_AreaTrabalho_Codigo=this.A5AreaTrabalho_Codigo;
         aP1_HmlgComPnd=this.AV8HmlgComPnd;
      }

      public bool executeUdp( ref int aP0_AreaTrabalho_Codigo )
      {
         this.A5AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV8HmlgComPnd = false ;
         initialize();
         executePrivate();
         aP0_AreaTrabalho_Codigo=this.A5AreaTrabalho_Codigo;
         aP1_HmlgComPnd=this.AV8HmlgComPnd;
         return AV8HmlgComPnd ;
      }

      public void executeSubmit( ref int aP0_AreaTrabalho_Codigo ,
                                 out bool aP1_HmlgComPnd )
      {
         prc_gethmlgcompnd objprc_gethmlgcompnd;
         objprc_gethmlgcompnd = new prc_gethmlgcompnd();
         objprc_gethmlgcompnd.A5AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         objprc_gethmlgcompnd.AV8HmlgComPnd = false ;
         objprc_gethmlgcompnd.context.SetSubmitInitialConfig(context);
         objprc_gethmlgcompnd.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_gethmlgcompnd);
         aP0_AreaTrabalho_Codigo=this.A5AreaTrabalho_Codigo;
         aP1_HmlgComPnd=this.AV8HmlgComPnd;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_gethmlgcompnd)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00DL2 */
         pr_default.execute(0, new Object[] {A5AreaTrabalho_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A29Contratante_Codigo = P00DL2_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P00DL2_n29Contratante_Codigo[0];
            A1803Contratante_OSHmlgComPnd = P00DL2_A1803Contratante_OSHmlgComPnd[0];
            n1803Contratante_OSHmlgComPnd = P00DL2_n1803Contratante_OSHmlgComPnd[0];
            A1803Contratante_OSHmlgComPnd = P00DL2_A1803Contratante_OSHmlgComPnd[0];
            n1803Contratante_OSHmlgComPnd = P00DL2_n1803Contratante_OSHmlgComPnd[0];
            AV8HmlgComPnd = A1803Contratante_OSHmlgComPnd;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00DL2_A29Contratante_Codigo = new int[1] ;
         P00DL2_n29Contratante_Codigo = new bool[] {false} ;
         P00DL2_A5AreaTrabalho_Codigo = new int[1] ;
         P00DL2_A1803Contratante_OSHmlgComPnd = new bool[] {false} ;
         P00DL2_n1803Contratante_OSHmlgComPnd = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_gethmlgcompnd__default(),
            new Object[][] {
                new Object[] {
               P00DL2_A29Contratante_Codigo, P00DL2_n29Contratante_Codigo, P00DL2_A5AreaTrabalho_Codigo, P00DL2_A1803Contratante_OSHmlgComPnd, P00DL2_n1803Contratante_OSHmlgComPnd
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A5AreaTrabalho_Codigo ;
      private int A29Contratante_Codigo ;
      private String scmdbuf ;
      private bool AV8HmlgComPnd ;
      private bool n29Contratante_Codigo ;
      private bool A1803Contratante_OSHmlgComPnd ;
      private bool n1803Contratante_OSHmlgComPnd ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_AreaTrabalho_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00DL2_A29Contratante_Codigo ;
      private bool[] P00DL2_n29Contratante_Codigo ;
      private int[] P00DL2_A5AreaTrabalho_Codigo ;
      private bool[] P00DL2_A1803Contratante_OSHmlgComPnd ;
      private bool[] P00DL2_n1803Contratante_OSHmlgComPnd ;
      private bool aP1_HmlgComPnd ;
   }

   public class prc_gethmlgcompnd__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00DL2 ;
          prmP00DL2 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00DL2", "SELECT TOP 1 T1.[Contratante_Codigo], T1.[AreaTrabalho_Codigo], T2.[Contratante_OSHmlgComPnd] FROM ([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) WHERE T1.[AreaTrabalho_Codigo] = @AreaTrabalho_Codigo ORDER BY T1.[AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00DL2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
