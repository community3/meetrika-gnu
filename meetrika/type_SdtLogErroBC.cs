/*
               File: type_SdtLogErroBC
        Description: Log Erro BC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:29:39.60
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "LogErroBC" )]
   [XmlType(TypeName =  "LogErroBC" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtLogErroBC : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtLogErroBC( )
      {
         /* Constructor for serialization */
         gxTv_SdtLogErroBC_Logerrobcmodulo = "";
         gxTv_SdtLogErroBC_Logerrobcdata = DateTime.MinValue;
         gxTv_SdtLogErroBC_Logerrobcnomebc = "";
         gxTv_SdtLogErroBC_Logerrobcjsonbc = "";
         gxTv_SdtLogErroBC_Logerrobcjasonmessage = "";
         gxTv_SdtLogErroBC_Mode = "";
         gxTv_SdtLogErroBC_Logerrobcmodulo_Z = "";
         gxTv_SdtLogErroBC_Logerrobcdata_Z = DateTime.MinValue;
         gxTv_SdtLogErroBC_Logerrobcnomebc_Z = "";
      }

      public SdtLogErroBC( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( long AV1668LogErroBCID )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(long)AV1668LogErroBCID});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"LogErroBCID", typeof(long)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "LogErroBC");
         metadata.Set("BT", "LogErroBC");
         metadata.Set("PK", "[ \"LogErroBCID\" ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Logerrobcid_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Logerrobcmodulo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Logerrobcdata_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Logerrobcnomebc_Z" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtLogErroBC deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtLogErroBC)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtLogErroBC obj ;
         obj = this;
         obj.gxTpr_Logerrobcid = deserialized.gxTpr_Logerrobcid;
         obj.gxTpr_Logerrobcmodulo = deserialized.gxTpr_Logerrobcmodulo;
         obj.gxTpr_Logerrobcdata = deserialized.gxTpr_Logerrobcdata;
         obj.gxTpr_Logerrobcnomebc = deserialized.gxTpr_Logerrobcnomebc;
         obj.gxTpr_Logerrobcjsonbc = deserialized.gxTpr_Logerrobcjsonbc;
         obj.gxTpr_Logerrobcjasonmessage = deserialized.gxTpr_Logerrobcjasonmessage;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Logerrobcid_Z = deserialized.gxTpr_Logerrobcid_Z;
         obj.gxTpr_Logerrobcmodulo_Z = deserialized.gxTpr_Logerrobcmodulo_Z;
         obj.gxTpr_Logerrobcdata_Z = deserialized.gxTpr_Logerrobcdata_Z;
         obj.gxTpr_Logerrobcnomebc_Z = deserialized.gxTpr_Logerrobcnomebc_Z;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "LogErroBCID") )
               {
                  gxTv_SdtLogErroBC_Logerrobcid = (long)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "LogErroBCModulo") )
               {
                  gxTv_SdtLogErroBC_Logerrobcmodulo = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "LogErroBCData") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtLogErroBC_Logerrobcdata = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtLogErroBC_Logerrobcdata = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "LogErroBCNomeBC") )
               {
                  gxTv_SdtLogErroBC_Logerrobcnomebc = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "LogErroBCJsonBC") )
               {
                  gxTv_SdtLogErroBC_Logerrobcjsonbc = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "LogErroBCJasonMessage") )
               {
                  gxTv_SdtLogErroBC_Logerrobcjasonmessage = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtLogErroBC_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtLogErroBC_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "LogErroBCID_Z") )
               {
                  gxTv_SdtLogErroBC_Logerrobcid_Z = (long)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "LogErroBCModulo_Z") )
               {
                  gxTv_SdtLogErroBC_Logerrobcmodulo_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "LogErroBCData_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtLogErroBC_Logerrobcdata_Z = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtLogErroBC_Logerrobcdata_Z = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "LogErroBCNomeBC_Z") )
               {
                  gxTv_SdtLogErroBC_Logerrobcnomebc_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "LogErroBC";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("LogErroBCID", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtLogErroBC_Logerrobcid), 18, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("LogErroBCModulo", StringUtil.RTrim( gxTv_SdtLogErroBC_Logerrobcmodulo));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( (DateTime.MinValue==gxTv_SdtLogErroBC_Logerrobcdata) )
         {
            oWriter.WriteStartElement("LogErroBCData");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtLogErroBC_Logerrobcdata)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtLogErroBC_Logerrobcdata)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtLogErroBC_Logerrobcdata)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("LogErroBCData", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteElement("LogErroBCNomeBC", StringUtil.RTrim( gxTv_SdtLogErroBC_Logerrobcnomebc));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("LogErroBCJsonBC", StringUtil.RTrim( gxTv_SdtLogErroBC_Logerrobcjsonbc));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("LogErroBCJasonMessage", StringUtil.RTrim( gxTv_SdtLogErroBC_Logerrobcjasonmessage));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtLogErroBC_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtLogErroBC_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("LogErroBCID_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtLogErroBC_Logerrobcid_Z), 18, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("LogErroBCModulo_Z", StringUtil.RTrim( gxTv_SdtLogErroBC_Logerrobcmodulo_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            if ( (DateTime.MinValue==gxTv_SdtLogErroBC_Logerrobcdata_Z) )
            {
               oWriter.WriteStartElement("LogErroBCData_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtLogErroBC_Logerrobcdata_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtLogErroBC_Logerrobcdata_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtLogErroBC_Logerrobcdata_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("LogErroBCData_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
               }
            }
            oWriter.WriteElement("LogErroBCNomeBC_Z", StringUtil.RTrim( gxTv_SdtLogErroBC_Logerrobcnomebc_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("LogErroBCID", StringUtil.LTrim( StringUtil.Str( (decimal)(gxTv_SdtLogErroBC_Logerrobcid), 18, 0)), false);
         AddObjectProperty("LogErroBCModulo", gxTv_SdtLogErroBC_Logerrobcmodulo, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtLogErroBC_Logerrobcdata)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtLogErroBC_Logerrobcdata)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtLogErroBC_Logerrobcdata)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("LogErroBCData", sDateCnv, false);
         AddObjectProperty("LogErroBCNomeBC", gxTv_SdtLogErroBC_Logerrobcnomebc, false);
         AddObjectProperty("LogErroBCJsonBC", gxTv_SdtLogErroBC_Logerrobcjsonbc, false);
         AddObjectProperty("LogErroBCJasonMessage", gxTv_SdtLogErroBC_Logerrobcjasonmessage, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtLogErroBC_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtLogErroBC_Initialized, false);
            AddObjectProperty("LogErroBCID_Z", StringUtil.LTrim( StringUtil.Str( (decimal)(gxTv_SdtLogErroBC_Logerrobcid_Z), 18, 0)), false);
            AddObjectProperty("LogErroBCModulo_Z", gxTv_SdtLogErroBC_Logerrobcmodulo_Z, false);
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtLogErroBC_Logerrobcdata_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtLogErroBC_Logerrobcdata_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtLogErroBC_Logerrobcdata_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("LogErroBCData_Z", sDateCnv, false);
            AddObjectProperty("LogErroBCNomeBC_Z", gxTv_SdtLogErroBC_Logerrobcnomebc_Z, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "LogErroBCID" )]
      [  XmlElement( ElementName = "LogErroBCID"   )]
      public long gxTpr_Logerrobcid
      {
         get {
            return gxTv_SdtLogErroBC_Logerrobcid ;
         }

         set {
            if ( gxTv_SdtLogErroBC_Logerrobcid != value )
            {
               gxTv_SdtLogErroBC_Mode = "INS";
               this.gxTv_SdtLogErroBC_Logerrobcid_Z_SetNull( );
               this.gxTv_SdtLogErroBC_Logerrobcmodulo_Z_SetNull( );
               this.gxTv_SdtLogErroBC_Logerrobcdata_Z_SetNull( );
               this.gxTv_SdtLogErroBC_Logerrobcnomebc_Z_SetNull( );
            }
            gxTv_SdtLogErroBC_Logerrobcid = (long)(value);
         }

      }

      [  SoapElement( ElementName = "LogErroBCModulo" )]
      [  XmlElement( ElementName = "LogErroBCModulo"   )]
      public String gxTpr_Logerrobcmodulo
      {
         get {
            return gxTv_SdtLogErroBC_Logerrobcmodulo ;
         }

         set {
            gxTv_SdtLogErroBC_Logerrobcmodulo = (String)(value);
         }

      }

      [  SoapElement( ElementName = "LogErroBCData" )]
      [  XmlElement( ElementName = "LogErroBCData"  , IsNullable=true )]
      public string gxTpr_Logerrobcdata_Nullable
      {
         get {
            if ( gxTv_SdtLogErroBC_Logerrobcdata == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtLogErroBC_Logerrobcdata).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtLogErroBC_Logerrobcdata = DateTime.MinValue;
            else
               gxTv_SdtLogErroBC_Logerrobcdata = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Logerrobcdata
      {
         get {
            return gxTv_SdtLogErroBC_Logerrobcdata ;
         }

         set {
            gxTv_SdtLogErroBC_Logerrobcdata = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "LogErroBCNomeBC" )]
      [  XmlElement( ElementName = "LogErroBCNomeBC"   )]
      public String gxTpr_Logerrobcnomebc
      {
         get {
            return gxTv_SdtLogErroBC_Logerrobcnomebc ;
         }

         set {
            gxTv_SdtLogErroBC_Logerrobcnomebc = (String)(value);
         }

      }

      [  SoapElement( ElementName = "LogErroBCJsonBC" )]
      [  XmlElement( ElementName = "LogErroBCJsonBC"   )]
      public String gxTpr_Logerrobcjsonbc
      {
         get {
            return gxTv_SdtLogErroBC_Logerrobcjsonbc ;
         }

         set {
            gxTv_SdtLogErroBC_Logerrobcjsonbc = (String)(value);
         }

      }

      [  SoapElement( ElementName = "LogErroBCJasonMessage" )]
      [  XmlElement( ElementName = "LogErroBCJasonMessage"   )]
      public String gxTpr_Logerrobcjasonmessage
      {
         get {
            return gxTv_SdtLogErroBC_Logerrobcjasonmessage ;
         }

         set {
            gxTv_SdtLogErroBC_Logerrobcjasonmessage = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtLogErroBC_Mode ;
         }

         set {
            gxTv_SdtLogErroBC_Mode = (String)(value);
         }

      }

      public void gxTv_SdtLogErroBC_Mode_SetNull( )
      {
         gxTv_SdtLogErroBC_Mode = "";
         return  ;
      }

      public bool gxTv_SdtLogErroBC_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtLogErroBC_Initialized ;
         }

         set {
            gxTv_SdtLogErroBC_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtLogErroBC_Initialized_SetNull( )
      {
         gxTv_SdtLogErroBC_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtLogErroBC_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "LogErroBCID_Z" )]
      [  XmlElement( ElementName = "LogErroBCID_Z"   )]
      public long gxTpr_Logerrobcid_Z
      {
         get {
            return gxTv_SdtLogErroBC_Logerrobcid_Z ;
         }

         set {
            gxTv_SdtLogErroBC_Logerrobcid_Z = (long)(value);
         }

      }

      public void gxTv_SdtLogErroBC_Logerrobcid_Z_SetNull( )
      {
         gxTv_SdtLogErroBC_Logerrobcid_Z = 0;
         return  ;
      }

      public bool gxTv_SdtLogErroBC_Logerrobcid_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "LogErroBCModulo_Z" )]
      [  XmlElement( ElementName = "LogErroBCModulo_Z"   )]
      public String gxTpr_Logerrobcmodulo_Z
      {
         get {
            return gxTv_SdtLogErroBC_Logerrobcmodulo_Z ;
         }

         set {
            gxTv_SdtLogErroBC_Logerrobcmodulo_Z = (String)(value);
         }

      }

      public void gxTv_SdtLogErroBC_Logerrobcmodulo_Z_SetNull( )
      {
         gxTv_SdtLogErroBC_Logerrobcmodulo_Z = "";
         return  ;
      }

      public bool gxTv_SdtLogErroBC_Logerrobcmodulo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "LogErroBCData_Z" )]
      [  XmlElement( ElementName = "LogErroBCData_Z"  , IsNullable=true )]
      public string gxTpr_Logerrobcdata_Z_Nullable
      {
         get {
            if ( gxTv_SdtLogErroBC_Logerrobcdata_Z == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtLogErroBC_Logerrobcdata_Z).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtLogErroBC_Logerrobcdata_Z = DateTime.MinValue;
            else
               gxTv_SdtLogErroBC_Logerrobcdata_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Logerrobcdata_Z
      {
         get {
            return gxTv_SdtLogErroBC_Logerrobcdata_Z ;
         }

         set {
            gxTv_SdtLogErroBC_Logerrobcdata_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtLogErroBC_Logerrobcdata_Z_SetNull( )
      {
         gxTv_SdtLogErroBC_Logerrobcdata_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtLogErroBC_Logerrobcdata_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "LogErroBCNomeBC_Z" )]
      [  XmlElement( ElementName = "LogErroBCNomeBC_Z"   )]
      public String gxTpr_Logerrobcnomebc_Z
      {
         get {
            return gxTv_SdtLogErroBC_Logerrobcnomebc_Z ;
         }

         set {
            gxTv_SdtLogErroBC_Logerrobcnomebc_Z = (String)(value);
         }

      }

      public void gxTv_SdtLogErroBC_Logerrobcnomebc_Z_SetNull( )
      {
         gxTv_SdtLogErroBC_Logerrobcnomebc_Z = "";
         return  ;
      }

      public bool gxTv_SdtLogErroBC_Logerrobcnomebc_Z_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtLogErroBC_Logerrobcmodulo = "";
         gxTv_SdtLogErroBC_Logerrobcdata = DateTime.MinValue;
         gxTv_SdtLogErroBC_Logerrobcnomebc = "";
         gxTv_SdtLogErroBC_Logerrobcjsonbc = "";
         gxTv_SdtLogErroBC_Logerrobcjasonmessage = "";
         gxTv_SdtLogErroBC_Mode = "";
         gxTv_SdtLogErroBC_Logerrobcmodulo_Z = "";
         gxTv_SdtLogErroBC_Logerrobcdata_Z = DateTime.MinValue;
         gxTv_SdtLogErroBC_Logerrobcnomebc_Z = "";
         sTagName = "";
         sDateCnv = "";
         sNumToPad = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "logerrobc", "GeneXus.Programs.logerrobc_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtLogErroBC_Initialized ;
      private short readOk ;
      private short nOutParmCount ;
      private long gxTv_SdtLogErroBC_Logerrobcid ;
      private long gxTv_SdtLogErroBC_Logerrobcid_Z ;
      private String gxTv_SdtLogErroBC_Mode ;
      private String sTagName ;
      private String sDateCnv ;
      private String sNumToPad ;
      private DateTime gxTv_SdtLogErroBC_Logerrobcdata ;
      private DateTime gxTv_SdtLogErroBC_Logerrobcdata_Z ;
      private String gxTv_SdtLogErroBC_Logerrobcjsonbc ;
      private String gxTv_SdtLogErroBC_Logerrobcjasonmessage ;
      private String gxTv_SdtLogErroBC_Logerrobcmodulo ;
      private String gxTv_SdtLogErroBC_Logerrobcnomebc ;
      private String gxTv_SdtLogErroBC_Logerrobcmodulo_Z ;
      private String gxTv_SdtLogErroBC_Logerrobcnomebc_Z ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"LogErroBC", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtLogErroBC_RESTInterface : GxGenericCollectionItem<SdtLogErroBC>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtLogErroBC_RESTInterface( ) : base()
      {
      }

      public SdtLogErroBC_RESTInterface( SdtLogErroBC psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "LogErroBCID" , Order = 0 )]
      [GxSeudo()]
      public String gxTpr_Logerrobcid
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Logerrobcid), 18, 0)) ;
         }

         set {
            sdt.gxTpr_Logerrobcid = (long)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "LogErroBCModulo" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Logerrobcmodulo
      {
         get {
            return sdt.gxTpr_Logerrobcmodulo ;
         }

         set {
            sdt.gxTpr_Logerrobcmodulo = (String)(value);
         }

      }

      [DataMember( Name = "LogErroBCData" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Logerrobcdata
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Logerrobcdata) ;
         }

         set {
            sdt.gxTpr_Logerrobcdata = DateTimeUtil.CToD2( (String)(value));
         }

      }

      [DataMember( Name = "LogErroBCNomeBC" , Order = 3 )]
      [GxSeudo()]
      public String gxTpr_Logerrobcnomebc
      {
         get {
            return sdt.gxTpr_Logerrobcnomebc ;
         }

         set {
            sdt.gxTpr_Logerrobcnomebc = (String)(value);
         }

      }

      [DataMember( Name = "LogErroBCJsonBC" , Order = 4 )]
      public String gxTpr_Logerrobcjsonbc
      {
         get {
            return sdt.gxTpr_Logerrobcjsonbc ;
         }

         set {
            sdt.gxTpr_Logerrobcjsonbc = (String)(value);
         }

      }

      [DataMember( Name = "LogErroBCJasonMessage" , Order = 5 )]
      public String gxTpr_Logerrobcjasonmessage
      {
         get {
            return sdt.gxTpr_Logerrobcjasonmessage ;
         }

         set {
            sdt.gxTpr_Logerrobcjasonmessage = (String)(value);
         }

      }

      public SdtLogErroBC sdt
      {
         get {
            return (SdtLogErroBC)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtLogErroBC() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 12 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
