/*
               File: WP_AssociarUsuarioContratadas
        Description: Contratadas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:12:22.19
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_associarusuariocontratadas : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public wp_associarusuariocontratadas( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_associarusuariocontratadas( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Usuario_Codigo )
      {
         this.AV22Usuario_Codigo = aP0_Usuario_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavContratadas = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV22Usuario_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Usuario_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSUARIO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV22Usuario_Codigo), "ZZZZZ9")));
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PARI2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               edtavUsuario_nome_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_nome_Enabled), 5, 0)));
               WSRI2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WERI2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( "Contratadas") ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203119122228");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery-1.4.2.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.core.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.widget.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.mouse.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.selectable.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.corner.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery-1.4.2.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.core.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.widget.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.mouse.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.selectable.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.corner.js", "");
         context.AddJavascriptSource("jQueryUI/jqSelectRender.js", "");
         context.AddJavascriptSource("jQueryUI/jqSelectRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_associarusuariocontratadas.aspx") + "?" + UrlEncode("" +AV22Usuario_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vJQNAOASSOCIADOS", AV17jqNaoAssociados);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vJQNAOASSOCIADOS", AV17jqNaoAssociados);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vJQASSOCIADOS", AV15jqAssociados);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vJQASSOCIADOS", AV15jqAssociados);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCODIGOS", AV9Codigos);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCODIGOS", AV9Codigos);
         }
         GxWebStd.gx_hidden_field( context, "CONTRATADA_PESSOACNPJ", A42Contratada_PessoaCNPJ);
         GxWebStd.gx_hidden_field( context, "CONTRATADA_AREATRABALHODES", A53Contratada_AreaTrabalhoDes);
         GxWebStd.gx_hidden_field( context, "CONTRATADA_SIGLA", StringUtil.RTrim( A438Contratada_Sigla));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCNPJS", AV25CNPJs);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCNPJS", AV25CNPJs);
         }
         GxWebStd.gx_hidden_field( context, "CONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "AREATRABALHO_ATIVO", A72AreaTrabalho_Ativo);
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATADA_ATIVO", A43Contratada_Ativo);
         GxWebStd.gx_hidden_field( context, "AREATRABALHO_DESCRICAO", A6AreaTrabalho_Descricao);
         GxWebStd.gx_hidden_field( context, "AREATRABALHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A5AreaTrabalho_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAREAS", AV8Areas);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAREAS", AV8Areas);
         }
         GxWebStd.gx_hidden_field( context, "CONTRATADA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vUSUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22Usuario_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMENUPERFIL", AV6MenuPerfil);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMENUPERFIL", AV6MenuPerfil);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vALL", AV7All);
         GxWebStd.gx_hidden_field( context, "gxhash_vUSUARIO_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV23Usuario_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vUSUARIO_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV23Usuario_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vUSUARIO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV22Usuario_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vUSUARIO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV22Usuario_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Width", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqnaoassociados_Width), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Height", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqnaoassociados_Height), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Rounding", StringUtil.RTrim( Jqnaoassociados_Rounding));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Theme", StringUtil.RTrim( Jqnaoassociados_Theme));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Unselectedicon", StringUtil.RTrim( Jqnaoassociados_Unselectedicon));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Selectedcolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqnaoassociados_Selectedcolor), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Itemwidth", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqnaoassociados_Itemwidth), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Itemheight", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqnaoassociados_Itemheight), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Width", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqassociados_Width), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Height", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqassociados_Height), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Rounding", StringUtil.RTrim( Jqassociados_Rounding));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Theme", StringUtil.RTrim( Jqassociados_Theme));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Unselectedicon", StringUtil.RTrim( Jqassociados_Unselectedicon));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Selectedcolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqassociados_Selectedcolor), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Itemwidth", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqassociados_Itemwidth), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Itemheight", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqassociados_Itemheight), 9, 0, ".", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormRI2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "WP_AssociarUsuarioContratadas" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contratadas" ;
      }

      protected void WBRI0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            wb_table1_2_RI2( true) ;
         }
         else
         {
            wb_table1_2_RI2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_RI2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTRI2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Contratadas", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPRI0( ) ;
      }

      protected void WSRI2( )
      {
         STARTRI2( ) ;
         EVTRI2( ) ;
      }

      protected void EVTRI2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11RI2 */
                           E11RI2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12RI2 */
                           E12RI2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VCONTRATADAS.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13RI2 */
                           E13RI2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( ! wbErr )
                           {
                              Rfr0gs = false;
                              if ( ! Rfr0gs )
                              {
                                 /* Execute user event: E14RI2 */
                                 E14RI2 ();
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DISASSOCIATE SELECTED'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15RI2 */
                           E15RI2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ASSOCIATE SELECTED'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16RI2 */
                           E16RI2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ASSOCIATE ALL'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17RI2 */
                           E17RI2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DISASSOCIATE ALL'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18RI2 */
                           E18RI2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19RI2 */
                           E19RI2 ();
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WERI2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormRI2( ) ;
            }
         }
      }

      protected void PARI2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavContratadas.Name = "vCONTRATADAS";
            cmbavContratadas.WebTags = "";
            if ( cmbavContratadas.ItemCount > 0 )
            {
               AV11Contratadas = (int)(NumberUtil.Val( cmbavContratadas.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV11Contratadas), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Contratadas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Contratadas), 6, 0)));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavUsuario_nome_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavContratadas.ItemCount > 0 )
         {
            AV11Contratadas = (int)(NumberUtil.Val( cmbavContratadas.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV11Contratadas), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Contratadas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Contratadas), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFRI2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavUsuario_nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_nome_Enabled), 5, 0)));
      }

      protected void RFRI2( )
      {
         initialize_formulas( ) ;
         /* Execute user event: E12RI2 */
         E12RI2 ();
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E19RI2 */
            E19RI2 ();
            WBRI0( ) ;
         }
      }

      protected void STRUPRI0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavUsuario_nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_nome_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11RI2 */
         E11RI2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vJQNAOASSOCIADOS"), AV17jqNaoAssociados);
            ajax_req_read_hidden_sdt(cgiGet( "vJQASSOCIADOS"), AV15jqAssociados);
            /* Read variables values. */
            AV23Usuario_Nome = StringUtil.Upper( cgiGet( edtavUsuario_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Usuario_Nome", AV23Usuario_Nome);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSUARIO_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV23Usuario_Nome, "@!"))));
            cmbavContratadas.CurrentValue = cgiGet( cmbavContratadas_Internalname);
            AV11Contratadas = (int)(NumberUtil.Val( cgiGet( cmbavContratadas_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Contratadas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Contratadas), 6, 0)));
            /* Read saved values. */
            Jqnaoassociados_Width = (int)(context.localUtil.CToN( cgiGet( "JQNAOASSOCIADOS_Width"), ",", "."));
            Jqnaoassociados_Height = (int)(context.localUtil.CToN( cgiGet( "JQNAOASSOCIADOS_Height"), ",", "."));
            Jqnaoassociados_Rounding = cgiGet( "JQNAOASSOCIADOS_Rounding");
            Jqnaoassociados_Theme = cgiGet( "JQNAOASSOCIADOS_Theme");
            Jqnaoassociados_Unselectedicon = cgiGet( "JQNAOASSOCIADOS_Unselectedicon");
            Jqnaoassociados_Selectedcolor = (int)(context.localUtil.CToN( cgiGet( "JQNAOASSOCIADOS_Selectedcolor"), ",", "."));
            Jqnaoassociados_Itemwidth = (int)(context.localUtil.CToN( cgiGet( "JQNAOASSOCIADOS_Itemwidth"), ",", "."));
            Jqnaoassociados_Itemheight = (int)(context.localUtil.CToN( cgiGet( "JQNAOASSOCIADOS_Itemheight"), ",", "."));
            Jqassociados_Width = (int)(context.localUtil.CToN( cgiGet( "JQASSOCIADOS_Width"), ",", "."));
            Jqassociados_Height = (int)(context.localUtil.CToN( cgiGet( "JQASSOCIADOS_Height"), ",", "."));
            Jqassociados_Rounding = cgiGet( "JQASSOCIADOS_Rounding");
            Jqassociados_Theme = cgiGet( "JQASSOCIADOS_Theme");
            Jqassociados_Unselectedicon = cgiGet( "JQASSOCIADOS_Unselectedicon");
            Jqassociados_Selectedcolor = (int)(context.localUtil.CToN( cgiGet( "JQASSOCIADOS_Selectedcolor"), ",", "."));
            Jqassociados_Itemwidth = (int)(context.localUtil.CToN( cgiGet( "JQASSOCIADOS_Itemwidth"), ",", "."));
            Jqassociados_Itemheight = (int)(context.localUtil.CToN( cgiGet( "JQASSOCIADOS_Itemheight"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11RI2 */
         E11RI2 ();
         if (returnInSub) return;
      }

      protected void E11RI2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV24WWPContext) ;
         if ( StringUtil.StrCmp(AV12HTTPRequest.Method, "GET") == 0 )
         {
            AV29GXLvl7 = 0;
            /* Using cursor H00RI2 */
            pr_default.execute(0, new Object[] {AV22Usuario_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A57Usuario_PessoaCod = H00RI2_A57Usuario_PessoaCod[0];
               A1Usuario_Codigo = H00RI2_A1Usuario_Codigo[0];
               A58Usuario_PessoaNom = H00RI2_A58Usuario_PessoaNom[0];
               n58Usuario_PessoaNom = H00RI2_n58Usuario_PessoaNom[0];
               A58Usuario_PessoaNom = H00RI2_A58Usuario_PessoaNom[0];
               n58Usuario_PessoaNom = H00RI2_n58Usuario_PessoaNom[0];
               AV29GXLvl7 = 1;
               AV23Usuario_Nome = A58Usuario_PessoaNom;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Usuario_Nome", AV23Usuario_Nome);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSUARIO_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV23Usuario_Nome, "@!"))));
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            if ( AV29GXLvl7 == 0 )
            {
               GX_msglist.addItem("Usu�rio n�o encontrado.");
            }
            /* Using cursor H00RI3 */
            pr_default.execute(1, new Object[] {AV22Usuario_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               BRKRI4 = false;
               A66ContratadaUsuario_ContratadaCod = H00RI3_A66ContratadaUsuario_ContratadaCod[0];
               A67ContratadaUsuario_ContratadaPessoaCod = H00RI3_A67ContratadaUsuario_ContratadaPessoaCod[0];
               n67ContratadaUsuario_ContratadaPessoaCod = H00RI3_n67ContratadaUsuario_ContratadaPessoaCod[0];
               A69ContratadaUsuario_UsuarioCod = H00RI3_A69ContratadaUsuario_UsuarioCod[0];
               A348ContratadaUsuario_ContratadaPessoaCNPJ = H00RI3_A348ContratadaUsuario_ContratadaPessoaCNPJ[0];
               n348ContratadaUsuario_ContratadaPessoaCNPJ = H00RI3_n348ContratadaUsuario_ContratadaPessoaCNPJ[0];
               A438Contratada_Sigla = H00RI3_A438Contratada_Sigla[0];
               n438Contratada_Sigla = H00RI3_n438Contratada_Sigla[0];
               A67ContratadaUsuario_ContratadaPessoaCod = H00RI3_A67ContratadaUsuario_ContratadaPessoaCod[0];
               n67ContratadaUsuario_ContratadaPessoaCod = H00RI3_n67ContratadaUsuario_ContratadaPessoaCod[0];
               A438Contratada_Sigla = H00RI3_A438Contratada_Sigla[0];
               n438Contratada_Sigla = H00RI3_n438Contratada_Sigla[0];
               A348ContratadaUsuario_ContratadaPessoaCNPJ = H00RI3_A348ContratadaUsuario_ContratadaPessoaCNPJ[0];
               n348ContratadaUsuario_ContratadaPessoaCNPJ = H00RI3_n348ContratadaUsuario_ContratadaPessoaCNPJ[0];
               AV26CNPJ = "";
               while ( (pr_default.getStatus(1) != 101) && ( H00RI3_A69ContratadaUsuario_UsuarioCod[0] == A69ContratadaUsuario_UsuarioCod ) && ( StringUtil.StrCmp(H00RI3_A348ContratadaUsuario_ContratadaPessoaCNPJ[0], A348ContratadaUsuario_ContratadaPessoaCNPJ) == 0 ) )
               {
                  BRKRI4 = false;
                  A66ContratadaUsuario_ContratadaCod = H00RI3_A66ContratadaUsuario_ContratadaCod[0];
                  A67ContratadaUsuario_ContratadaPessoaCod = H00RI3_A67ContratadaUsuario_ContratadaPessoaCod[0];
                  n67ContratadaUsuario_ContratadaPessoaCod = H00RI3_n67ContratadaUsuario_ContratadaPessoaCod[0];
                  A438Contratada_Sigla = H00RI3_A438Contratada_Sigla[0];
                  n438Contratada_Sigla = H00RI3_n438Contratada_Sigla[0];
                  A67ContratadaUsuario_ContratadaPessoaCod = H00RI3_A67ContratadaUsuario_ContratadaPessoaCod[0];
                  n67ContratadaUsuario_ContratadaPessoaCod = H00RI3_n67ContratadaUsuario_ContratadaPessoaCod[0];
                  A438Contratada_Sigla = H00RI3_A438Contratada_Sigla[0];
                  n438Contratada_Sigla = H00RI3_n438Contratada_Sigla[0];
                  if ( StringUtil.StrCmp(AV26CNPJ, A348ContratadaUsuario_ContratadaPessoaCNPJ) != 0 )
                  {
                     AV26CNPJ = A348ContratadaUsuario_ContratadaPessoaCNPJ;
                     cmbavContratadas.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(cmbavContratadas.ItemCount+1), 6, 0)), StringUtil.Trim( A438Contratada_Sigla)+" de outras �reas", (short)(cmbavContratadas.ItemCount+1));
                     AV25CNPJs.Add(A348ContratadaUsuario_ContratadaPessoaCNPJ, 0);
                  }
                  BRKRI4 = true;
                  pr_default.readNext(1);
               }
               if ( ! BRKRI4 )
               {
                  BRKRI4 = true;
                  pr_default.readNext(1);
               }
            }
            pr_default.close(1);
            cmbavContratadas.addItem("999999", " desta ou outras �reas", (short)(cmbavContratadas.ItemCount+1));
            if ( cmbavContratadas.ItemCount >= 2 )
            {
               AV11Contratadas = 1;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Contratadas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Contratadas), 6, 0)));
            }
            else
            {
               AV11Contratadas = 999999;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Contratadas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Contratadas), 6, 0)));
            }
            /* Using cursor H00RI4 */
            pr_default.execute(2, new Object[] {AV22Usuario_Codigo});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A69ContratadaUsuario_UsuarioCod = H00RI4_A69ContratadaUsuario_UsuarioCod[0];
               A29Contratante_Codigo = H00RI4_A29Contratante_Codigo[0];
               n29Contratante_Codigo = H00RI4_n29Contratante_Codigo[0];
               A1228ContratadaUsuario_AreaTrabalhoCod = H00RI4_A1228ContratadaUsuario_AreaTrabalhoCod[0];
               n1228ContratadaUsuario_AreaTrabalhoCod = H00RI4_n1228ContratadaUsuario_AreaTrabalhoCod[0];
               A66ContratadaUsuario_ContratadaCod = H00RI4_A66ContratadaUsuario_ContratadaCod[0];
               A438Contratada_Sigla = H00RI4_A438Contratada_Sigla[0];
               n438Contratada_Sigla = H00RI4_n438Contratada_Sigla[0];
               A1297ContratadaUsuario_AreaTrabalhoDes = H00RI4_A1297ContratadaUsuario_AreaTrabalhoDes[0];
               n1297ContratadaUsuario_AreaTrabalhoDes = H00RI4_n1297ContratadaUsuario_AreaTrabalhoDes[0];
               A1228ContratadaUsuario_AreaTrabalhoCod = H00RI4_A1228ContratadaUsuario_AreaTrabalhoCod[0];
               n1228ContratadaUsuario_AreaTrabalhoCod = H00RI4_n1228ContratadaUsuario_AreaTrabalhoCod[0];
               A438Contratada_Sigla = H00RI4_A438Contratada_Sigla[0];
               n438Contratada_Sigla = H00RI4_n438Contratada_Sigla[0];
               A29Contratante_Codigo = H00RI4_A29Contratante_Codigo[0];
               n29Contratante_Codigo = H00RI4_n29Contratante_Codigo[0];
               A1297ContratadaUsuario_AreaTrabalhoDes = H00RI4_A1297ContratadaUsuario_AreaTrabalhoDes[0];
               n1297ContratadaUsuario_AreaTrabalhoDes = H00RI4_n1297ContratadaUsuario_AreaTrabalhoDes[0];
               AV33GXLvl39 = 0;
               /* Using cursor H00RI6 */
               pr_default.execute(3, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo, AV22Usuario_Codigo, n1228ContratadaUsuario_AreaTrabalhoCod, A1228ContratadaUsuario_AreaTrabalhoCod});
               while ( (pr_default.getStatus(3) != 101) )
               {
                  A63ContratanteUsuario_ContratanteCod = H00RI6_A63ContratanteUsuario_ContratanteCod[0];
                  A60ContratanteUsuario_UsuarioCod = H00RI6_A60ContratanteUsuario_UsuarioCod[0];
                  A1020ContratanteUsuario_AreaTrabalhoCod = H00RI6_A1020ContratanteUsuario_AreaTrabalhoCod[0];
                  n1020ContratanteUsuario_AreaTrabalhoCod = H00RI6_n1020ContratanteUsuario_AreaTrabalhoCod[0];
                  A1020ContratanteUsuario_AreaTrabalhoCod = H00RI6_A1020ContratanteUsuario_AreaTrabalhoCod[0];
                  n1020ContratanteUsuario_AreaTrabalhoCod = H00RI6_n1020ContratanteUsuario_AreaTrabalhoCod[0];
                  AV33GXLvl39 = 1;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(3);
               if ( AV33GXLvl39 == 0 )
               {
                  AV8Areas.Add(A1228ContratadaUsuario_AreaTrabalhoCod, 0);
                  AV9Codigos.Add(A66ContratadaUsuario_ContratadaCod, 0);
                  AV16jqItem = new SdtjqSelectData_Item(context);
                  AV16jqItem.gxTpr_Id = StringUtil.Str( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0);
                  AV16jqItem.gxTpr_Descr = A1297ContratadaUsuario_AreaTrabalhoDes+" ("+StringUtil.Trim( A438Contratada_Sigla)+")";
                  AV16jqItem.gxTpr_Selected = false;
                  AV15jqAssociados.Add(AV16jqItem, 0);
               }
               pr_default.readNext(2);
            }
            pr_default.close(2);
            /* Execute user subroutine: 'CARREGARNAOASSOCIADOS' */
            S112 ();
            if (returnInSub) return;
         }
      }

      protected void E12RI2( )
      {
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV24WWPContext) ;
         imgImageassociateselected_Visible = (AV24WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImageassociateselected_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImageassociateselected_Visible), 5, 0)));
         imgImageassociateall_Visible = (AV24WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImageassociateall_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImageassociateall_Visible), 5, 0)));
         imgImagedisassociateselected_Visible = (AV24WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImagedisassociateselected_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImagedisassociateselected_Visible), 5, 0)));
         imgImagedisassociateall_Visible = (AV24WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImagedisassociateall_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImagedisassociateall_Visible), 5, 0)));
         bttBtn_confirm_Visible = (AV24WWPContext.gxTpr_Insert||AV24WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_confirm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_confirm_Visible), 5, 0)));
      }

      protected void E13RI2( )
      {
         /* Contratadas_Click Routine */
         /* Execute user subroutine: 'CARREGARNAOASSOCIADOS' */
         S112 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV17jqNaoAssociados", AV17jqNaoAssociados);
      }

      public void GXEnter( )
      {
         /* Execute user event: E14RI2 */
         E14RI2 ();
         if (returnInSub) return;
      }

      protected void E14RI2( )
      {
         /* Enter Routine */
         AV21Success = true;
         AV34GXV1 = 1;
         while ( AV34GXV1 <= AV15jqAssociados.Count )
         {
            AV16jqItem = ((SdtjqSelectData_Item)AV15jqAssociados.Item(AV34GXV1));
            if ( AV21Success )
            {
               AV10Contratada = (int)(NumberUtil.Val( AV16jqItem.gxTpr_Id, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Contratada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Contratada), 6, 0)));
               AV35GXLvl85 = 0;
               /* Using cursor H00RI7 */
               pr_default.execute(4, new Object[] {AV10Contratada, AV22Usuario_Codigo});
               while ( (pr_default.getStatus(4) != 101) )
               {
                  A69ContratadaUsuario_UsuarioCod = H00RI7_A69ContratadaUsuario_UsuarioCod[0];
                  A66ContratadaUsuario_ContratadaCod = H00RI7_A66ContratadaUsuario_ContratadaCod[0];
                  AV35GXLvl85 = 1;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(4);
               if ( AV35GXLvl85 == 0 )
               {
                  new prc_importarusuario(context ).execute(  0,  AV10Contratada,  AV22Usuario_Codigo) ;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Contratada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Contratada), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Usuario_Codigo), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSUARIO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV22Usuario_Codigo), "ZZZZZ9")));
               }
            }
            else
            {
               if (true) break;
            }
            AV34GXV1 = (int)(AV34GXV1+1);
         }
         AV36GXV2 = 1;
         while ( AV36GXV2 <= AV17jqNaoAssociados.Count )
         {
            AV16jqItem = ((SdtjqSelectData_Item)AV17jqNaoAssociados.Item(AV36GXV2));
            if ( AV21Success )
            {
               AV10Contratada = (int)(NumberUtil.Val( AV16jqItem.gxTpr_Id, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Contratada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Contratada), 6, 0)));
               AV5ContratadaUsuario.Load(AV10Contratada, AV22Usuario_Codigo);
               if ( AV5ContratadaUsuario.Success() )
               {
                  AV5ContratadaUsuario.Delete();
                  AV21Success = AV5ContratadaUsuario.Success();
               }
            }
            else
            {
               if (true) break;
            }
            AV36GXV2 = (int)(AV36GXV2+1);
         }
         if ( AV21Success )
         {
            context.CommitDataStores( "WP_AssociarUsuarioContratadas");
            context.setWebReturnParms(new Object[] {});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         else
         {
            context.RollbackDataStores( "WP_AssociarUsuarioContratadas");
            /* Execute user subroutine: 'SHOW ERROR MESSAGES' */
            S122 ();
            if (returnInSub) return;
         }
      }

      protected void E15RI2( )
      {
         /* 'Disassociate Selected' Routine */
         bttBtn_cancel_Caption = "Cancelar";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_cancel_Internalname, "Caption", bttBtn_cancel_Caption);
         AV7All = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7All", AV7All);
         /* Execute user subroutine: 'DISASSOCIATESELECTED' */
         S132 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV15jqAssociados", AV15jqAssociados);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV17jqNaoAssociados", AV17jqNaoAssociados);
      }

      protected void E16RI2( )
      {
         /* 'Associate selected' Routine */
         bttBtn_cancel_Caption = "Cancelar";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_cancel_Internalname, "Caption", bttBtn_cancel_Caption);
         AV7All = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7All", AV7All);
         /* Execute user subroutine: 'ASSOCIATESELECTED' */
         S142 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV17jqNaoAssociados", AV17jqNaoAssociados);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV15jqAssociados", AV15jqAssociados);
      }

      protected void E17RI2( )
      {
         /* 'Associate All' Routine */
         bttBtn_cancel_Caption = "Cancelar";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_cancel_Internalname, "Caption", bttBtn_cancel_Caption);
         AV7All = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7All", AV7All);
         /* Execute user subroutine: 'ASSOCIATESELECTED' */
         S142 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV17jqNaoAssociados", AV17jqNaoAssociados);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV15jqAssociados", AV15jqAssociados);
      }

      protected void E18RI2( )
      {
         /* 'Disassociate All' Routine */
         bttBtn_cancel_Caption = "Cancelar";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_cancel_Internalname, "Caption", bttBtn_cancel_Caption);
         AV7All = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7All", AV7All);
         /* Execute user subroutine: 'DISASSOCIATESELECTED' */
         S132 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV15jqAssociados", AV15jqAssociados);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV17jqNaoAssociados", AV17jqNaoAssociados);
      }

      protected void S122( )
      {
         /* 'SHOW ERROR MESSAGES' Routine */
         AV38GXV4 = 1;
         AV37GXV3 = AV6MenuPerfil.GetMessages();
         while ( AV38GXV4 <= AV37GXV3.Count )
         {
            AV20Message = ((SdtMessages_Message)AV37GXV3.Item(AV38GXV4));
            if ( AV20Message.gxTpr_Type == 1 )
            {
               GX_msglist.addItem(AV20Message.gxTpr_Description);
            }
            AV38GXV4 = (int)(AV38GXV4+1);
         }
      }

      protected void S142( )
      {
         /* 'ASSOCIATESELECTED' Routine */
         AV13i = 1;
         while ( AV13i <= AV17jqNaoAssociados.Count )
         {
            if ( ((SdtjqSelectData_Item)AV17jqNaoAssociados.Item(AV13i)).gxTpr_Selected || AV7All )
            {
               ((SdtjqSelectData_Item)AV17jqNaoAssociados.Item(AV13i)).gxTpr_Selected = false;
               AV15jqAssociados.Add(((SdtjqSelectData_Item)AV17jqNaoAssociados.Item(AV13i)), 0);
               AV17jqNaoAssociados.RemoveItem(AV13i);
               AV13i = (int)(AV13i-1);
            }
            AV13i = (int)(AV13i+1);
         }
         /* Execute user subroutine: 'TITLES' */
         S152 ();
         if (returnInSub) return;
      }

      protected void S132( )
      {
         /* 'DISASSOCIATESELECTED' Routine */
         AV13i = 1;
         while ( AV13i <= AV15jqAssociados.Count )
         {
            if ( ((SdtjqSelectData_Item)AV15jqAssociados.Item(AV13i)).gxTpr_Selected || AV7All )
            {
               ((SdtjqSelectData_Item)AV15jqAssociados.Item(AV13i)).gxTpr_Selected = false;
               AV17jqNaoAssociados.Add(((SdtjqSelectData_Item)AV15jqAssociados.Item(AV13i)), 0);
               AV15jqAssociados.RemoveItem(AV13i);
               AV13i = (int)(AV13i-1);
            }
            AV13i = (int)(AV13i+1);
         }
         /* Execute user subroutine: 'TITLES' */
         S152 ();
         if (returnInSub) return;
      }

      protected void S152( )
      {
         /* 'TITLES' Routine */
         lblNotassociatedrecordstitle_Caption = "N�o Associados ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV17jqNaoAssociados.Count), 9, 0))+")";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblNotassociatedrecordstitle_Internalname, "Caption", lblNotassociatedrecordstitle_Caption);
         lblAssociatedrecordstitle_Caption = "Associados ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV15jqAssociados.Count), 9, 0))+")";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblAssociatedrecordstitle_Internalname, "Caption", lblAssociatedrecordstitle_Caption);
      }

      protected void S112( )
      {
         /* 'CARREGARNAOASSOCIADOS' Routine */
         AV17jqNaoAssociados.Clear();
         if ( AV11Contratadas < 999999 )
         {
            AV10Contratada = (int)(AV9Codigos.GetNumeric(AV11Contratadas));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Contratada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Contratada), 6, 0)));
            pr_default.dynParam(5, new Object[]{ new Object[]{
                                                 A39Contratada_Codigo ,
                                                 AV9Codigos ,
                                                 A42Contratada_PessoaCNPJ ,
                                                 AV11Contratadas ,
                                                 A72AreaTrabalho_Ativo ,
                                                 A43Contratada_Ativo },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                                 }
            });
            /* Using cursor H00RI8 */
            pr_default.execute(5);
            while ( (pr_default.getStatus(5) != 101) )
            {
               A40Contratada_PessoaCod = H00RI8_A40Contratada_PessoaCod[0];
               A52Contratada_AreaTrabalhoCod = H00RI8_A52Contratada_AreaTrabalhoCod[0];
               A43Contratada_Ativo = H00RI8_A43Contratada_Ativo[0];
               A72AreaTrabalho_Ativo = H00RI8_A72AreaTrabalho_Ativo[0];
               n72AreaTrabalho_Ativo = H00RI8_n72AreaTrabalho_Ativo[0];
               A39Contratada_Codigo = H00RI8_A39Contratada_Codigo[0];
               A42Contratada_PessoaCNPJ = H00RI8_A42Contratada_PessoaCNPJ[0];
               n42Contratada_PessoaCNPJ = H00RI8_n42Contratada_PessoaCNPJ[0];
               A438Contratada_Sigla = H00RI8_A438Contratada_Sigla[0];
               n438Contratada_Sigla = H00RI8_n438Contratada_Sigla[0];
               A53Contratada_AreaTrabalhoDes = H00RI8_A53Contratada_AreaTrabalhoDes[0];
               n53Contratada_AreaTrabalhoDes = H00RI8_n53Contratada_AreaTrabalhoDes[0];
               A42Contratada_PessoaCNPJ = H00RI8_A42Contratada_PessoaCNPJ[0];
               n42Contratada_PessoaCNPJ = H00RI8_n42Contratada_PessoaCNPJ[0];
               A72AreaTrabalho_Ativo = H00RI8_A72AreaTrabalho_Ativo[0];
               n72AreaTrabalho_Ativo = H00RI8_n72AreaTrabalho_Ativo[0];
               A53Contratada_AreaTrabalhoDes = H00RI8_A53Contratada_AreaTrabalhoDes[0];
               n53Contratada_AreaTrabalhoDes = H00RI8_n53Contratada_AreaTrabalhoDes[0];
               if ( StringUtil.StrCmp(A42Contratada_PessoaCNPJ, AV25CNPJs.GetString(AV11Contratadas)) == 0 )
               {
                  AV16jqItem = new SdtjqSelectData_Item(context);
                  AV16jqItem.gxTpr_Id = StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0);
                  AV16jqItem.gxTpr_Descr = A53Contratada_AreaTrabalhoDes+" ("+StringUtil.Trim( A438Contratada_Sigla)+")";
                  AV16jqItem.gxTpr_Selected = false;
                  AV17jqNaoAssociados.Add(AV16jqItem, 0);
               }
               pr_default.readNext(5);
            }
            pr_default.close(5);
         }
         else
         {
            pr_default.dynParam(6, new Object[]{ new Object[]{
                                                 A5AreaTrabalho_Codigo ,
                                                 AV8Areas ,
                                                 A72AreaTrabalho_Ativo },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                                 }
            });
            /* Using cursor H00RI9 */
            pr_default.execute(6);
            while ( (pr_default.getStatus(6) != 101) )
            {
               A5AreaTrabalho_Codigo = H00RI9_A5AreaTrabalho_Codigo[0];
               A72AreaTrabalho_Ativo = H00RI9_A72AreaTrabalho_Ativo[0];
               n72AreaTrabalho_Ativo = H00RI9_n72AreaTrabalho_Ativo[0];
               A6AreaTrabalho_Descricao = H00RI9_A6AreaTrabalho_Descricao[0];
               /* Using cursor H00RI10 */
               pr_default.execute(7, new Object[] {A5AreaTrabalho_Codigo});
               while ( (pr_default.getStatus(7) != 101) )
               {
                  BRKRI11 = false;
                  A40Contratada_PessoaCod = H00RI10_A40Contratada_PessoaCod[0];
                  A52Contratada_AreaTrabalhoCod = H00RI10_A52Contratada_AreaTrabalhoCod[0];
                  A42Contratada_PessoaCNPJ = H00RI10_A42Contratada_PessoaCNPJ[0];
                  n42Contratada_PessoaCNPJ = H00RI10_n42Contratada_PessoaCNPJ[0];
                  A39Contratada_Codigo = H00RI10_A39Contratada_Codigo[0];
                  A53Contratada_AreaTrabalhoDes = H00RI10_A53Contratada_AreaTrabalhoDes[0];
                  n53Contratada_AreaTrabalhoDes = H00RI10_n53Contratada_AreaTrabalhoDes[0];
                  A438Contratada_Sigla = H00RI10_A438Contratada_Sigla[0];
                  n438Contratada_Sigla = H00RI10_n438Contratada_Sigla[0];
                  A43Contratada_Ativo = H00RI10_A43Contratada_Ativo[0];
                  A42Contratada_PessoaCNPJ = H00RI10_A42Contratada_PessoaCNPJ[0];
                  n42Contratada_PessoaCNPJ = H00RI10_n42Contratada_PessoaCNPJ[0];
                  A53Contratada_AreaTrabalhoDes = H00RI10_A53Contratada_AreaTrabalhoDes[0];
                  n53Contratada_AreaTrabalhoDes = H00RI10_n53Contratada_AreaTrabalhoDes[0];
                  AV26CNPJ = "";
                  while ( (pr_default.getStatus(7) != 101) && ( H00RI10_A52Contratada_AreaTrabalhoCod[0] == A52Contratada_AreaTrabalhoCod ) && ( StringUtil.StrCmp(H00RI10_A42Contratada_PessoaCNPJ[0], A42Contratada_PessoaCNPJ) == 0 ) )
                  {
                     BRKRI11 = false;
                     A40Contratada_PessoaCod = H00RI10_A40Contratada_PessoaCod[0];
                     A39Contratada_Codigo = H00RI10_A39Contratada_Codigo[0];
                     A53Contratada_AreaTrabalhoDes = H00RI10_A53Contratada_AreaTrabalhoDes[0];
                     n53Contratada_AreaTrabalhoDes = H00RI10_n53Contratada_AreaTrabalhoDes[0];
                     A438Contratada_Sigla = H00RI10_A438Contratada_Sigla[0];
                     n438Contratada_Sigla = H00RI10_n438Contratada_Sigla[0];
                     A43Contratada_Ativo = H00RI10_A43Contratada_Ativo[0];
                     A53Contratada_AreaTrabalhoDes = H00RI10_A53Contratada_AreaTrabalhoDes[0];
                     n53Contratada_AreaTrabalhoDes = H00RI10_n53Contratada_AreaTrabalhoDes[0];
                     if ( StringUtil.StrCmp(AV26CNPJ, A42Contratada_PessoaCNPJ) != 0 )
                     {
                        AV26CNPJ = A42Contratada_PessoaCNPJ;
                        AV16jqItem = new SdtjqSelectData_Item(context);
                        AV16jqItem.gxTpr_Id = StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0);
                        AV16jqItem.gxTpr_Descr = StringUtil.Trim( A53Contratada_AreaTrabalhoDes)+" ("+StringUtil.Trim( A438Contratada_Sigla)+")";
                        AV16jqItem.gxTpr_Selected = false;
                        AV17jqNaoAssociados.Add(AV16jqItem, 0);
                     }
                     BRKRI11 = true;
                     pr_default.readNext(7);
                  }
                  if ( ! BRKRI11 )
                  {
                     BRKRI11 = true;
                     pr_default.readNext(7);
                  }
               }
               pr_default.close(7);
               pr_default.readNext(6);
            }
            pr_default.close(6);
         }
         /* Execute user subroutine: 'TITLES' */
         S152 ();
         if (returnInSub) return;
      }

      protected void nextLoad( )
      {
      }

      protected void E19RI2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_RI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TextBlockTitleCell'>") ;
            wb_table2_8_RI2( true) ;
         }
         else
         {
            wb_table2_8_RI2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_RI2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "� contratada: ") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContratadas, cmbavContratadas_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV11Contratadas), 6, 0)), 1, cmbavContratadas_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVCONTRATADAS.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,14);\"", "", true, "HLP_WP_AssociarUsuarioContratadas.htm");
            cmbavContratadas.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV11Contratadas), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratadas_Internalname, "Values", (String)(cmbavContratadas.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_17_RI2( true) ;
         }
         else
         {
            wb_table3_17_RI2( false) ;
         }
         return  ;
      }

      protected void wb_table3_17_RI2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_49_RI2( true) ;
         }
         else
         {
            wb_table4_49_RI2( false) ;
         }
         return  ;
      }

      protected void wb_table4_49_RI2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_RI2e( true) ;
         }
         else
         {
            wb_table1_2_RI2e( false) ;
         }
      }

      protected void wb_table4_49_RI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_confirm_Internalname, "", "Confirmar", bttBtn_confirm_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_confirm_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AssociarUsuarioContratadas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", bttBtn_cancel_Caption, bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AssociarUsuarioContratadas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_49_RI2e( true) ;
         }
         else
         {
            wb_table4_49_RI2e( false) ;
         }
      }

      protected void wb_table3_17_RI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefullcontent_Internalname, tblTablefullcontent_Internalname, "", "TableContent", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableAttributesCell'>") ;
            wb_table5_20_RI2( true) ;
         }
         else
         {
            wb_table5_20_RI2( false) ;
         }
         return  ;
      }

      protected void wb_table5_20_RI2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_17_RI2e( true) ;
         }
         else
         {
            wb_table3_17_RI2e( false) ;
         }
      }

      protected void wb_table5_20_RI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableAssociation", 0, "", "", 4, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblNotassociatedrecordstitle_Internalname, lblNotassociatedrecordstitle_Caption, "", "", lblNotassociatedrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociarUsuarioContratadas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblCenterrecordstitle_Internalname, " ", "", "", lblCenterrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociarUsuarioContratadas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAssociatedrecordstitle_Internalname, lblAssociatedrecordstitle_Caption, "", "", lblAssociatedrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociarUsuarioContratadas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"JQNAOASSOCIADOSContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            wb_table6_32_RI2( true) ;
         }
         else
         {
            wb_table6_32_RI2( false) ;
         }
         return  ;
      }

      protected void wb_table6_32_RI2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"JQASSOCIADOSContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_20_RI2e( true) ;
         }
         else
         {
            wb_table5_20_RI2e( false) ;
         }
      }

      protected void wb_table6_32_RI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImageassociateall_Internalname, context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImageassociateall_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImageassociateall_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ASSOCIATE ALL\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarUsuarioContratadas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImageassociateselected_Internalname, context.GetImagePath( "56a5f17b-0bc3-48b5-b303-afa6e0585b6d", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImageassociateselected_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImageassociateselected_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ASSOCIATE SELECTED\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarUsuarioContratadas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImagedisassociateselected_Internalname, context.GetImagePath( "a3800d0c-bf04-4575-bc01-11fe5d7b3525", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImagedisassociateselected_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImagedisassociateselected_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DISASSOCIATE SELECTED\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarUsuarioContratadas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImagedisassociateall_Internalname, context.GetImagePath( "c619e28f-4b32-4ff9-baaf-b3063fe4f782", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImagedisassociateall_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImagedisassociateall_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DISASSOCIATE ALL\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarUsuarioContratadas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_32_RI2e( true) ;
         }
         else
         {
            wb_table6_32_RI2e( false) ;
         }
      }

      protected void wb_table2_8_RI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedassociationtitle_Internalname, tblTablemergedassociationtitle_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAssociationtitle_Internalname, "Associar ao Usu�rio :: ", "", "", lblAssociationtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_AssociarUsuarioContratadas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_nome_Internalname, StringUtil.RTrim( AV23Usuario_Nome), StringUtil.RTrim( context.localUtil.Format( AV23Usuario_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,13);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_nome_Jsonclick, 0, "AttributeTitleWWP", "", "", "", 1, edtavUsuario_nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_AssociarUsuarioContratadas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_RI2e( true) ;
         }
         else
         {
            wb_table2_8_RI2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV22Usuario_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Usuario_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSUARIO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV22Usuario_Codigo), "ZZZZZ9")));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PARI2( ) ;
         WSRI2( ) ;
         WERI2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("jQueryUI/css/demos.css", "?1349420");
         AddStyleSheetFile("jQueryUI/css/demos.css", "?1349420");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203119122317");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_associarusuariocontratadas.js", "?20203119122317");
         context.AddJavascriptSource("Shared/jquery/jquery-1.4.2.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.core.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.widget.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.mouse.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.selectable.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.corner.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery-1.4.2.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.core.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.widget.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.mouse.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.selectable.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.corner.js", "");
         context.AddJavascriptSource("jQueryUI/jqSelectRender.js", "");
         context.AddJavascriptSource("jQueryUI/jqSelectRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblAssociationtitle_Internalname = "ASSOCIATIONTITLE";
         edtavUsuario_nome_Internalname = "vUSUARIO_NOME";
         tblTablemergedassociationtitle_Internalname = "TABLEMERGEDASSOCIATIONTITLE";
         cmbavContratadas_Internalname = "vCONTRATADAS";
         lblNotassociatedrecordstitle_Internalname = "NOTASSOCIATEDRECORDSTITLE";
         lblCenterrecordstitle_Internalname = "CENTERRECORDSTITLE";
         lblAssociatedrecordstitle_Internalname = "ASSOCIATEDRECORDSTITLE";
         Jqnaoassociados_Internalname = "JQNAOASSOCIADOS";
         imgImageassociateall_Internalname = "IMAGEASSOCIATEALL";
         imgImageassociateselected_Internalname = "IMAGEASSOCIATESELECTED";
         imgImagedisassociateselected_Internalname = "IMAGEDISASSOCIATESELECTED";
         imgImagedisassociateall_Internalname = "IMAGEDISASSOCIATEALL";
         tblTable1_Internalname = "TABLE1";
         Jqassociados_Internalname = "JQASSOCIADOS";
         tblTablecontent_Internalname = "TABLECONTENT";
         tblTablefullcontent_Internalname = "TABLEFULLCONTENT";
         bttBtn_confirm_Internalname = "BTN_CONFIRM";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavUsuario_nome_Jsonclick = "";
         edtavUsuario_nome_Enabled = 1;
         imgImagedisassociateall_Visible = 1;
         imgImagedisassociateselected_Visible = 1;
         imgImageassociateselected_Visible = 1;
         imgImageassociateall_Visible = 1;
         bttBtn_confirm_Visible = 1;
         cmbavContratadas_Jsonclick = "";
         lblAssociatedrecordstitle_Caption = "Associadas";
         lblNotassociatedrecordstitle_Caption = "N�o Associadas";
         bttBtn_cancel_Caption = "Fechar";
         Jqassociados_Itemheight = 17;
         Jqassociados_Itemwidth = 270;
         Jqassociados_Selectedcolor = (int)(0xE0E0E0);
         Jqassociados_Unselectedicon = "ui-icon-minus";
         Jqassociados_Theme = "base";
         Jqassociados_Rounding = "bevelfold bl";
         Jqassociados_Height = 300;
         Jqassociados_Width = 400;
         Jqnaoassociados_Itemheight = 17;
         Jqnaoassociados_Itemwidth = 270;
         Jqnaoassociados_Selectedcolor = (int)(0xE0E0E0);
         Jqnaoassociados_Unselectedicon = "ui-icon-minus";
         Jqnaoassociados_Theme = "base";
         Jqnaoassociados_Rounding = "bevelfold bl";
         Jqnaoassociados_Height = 300;
         Jqnaoassociados_Width = 400;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[{av:'imgImageassociateselected_Visible',ctrl:'IMAGEASSOCIATESELECTED',prop:'Visible'},{av:'imgImageassociateall_Visible',ctrl:'IMAGEASSOCIATEALL',prop:'Visible'},{av:'imgImagedisassociateselected_Visible',ctrl:'IMAGEDISASSOCIATESELECTED',prop:'Visible'},{av:'imgImagedisassociateall_Visible',ctrl:'IMAGEDISASSOCIATEALL',prop:'Visible'},{ctrl:'BTN_CONFIRM',prop:'Visible'}]}");
         setEventMetadata("VCONTRATADAS.CLICK","{handler:'E13RI2',iparms:[{av:'AV11Contratadas',fld:'vCONTRATADAS',pic:'ZZZZZ9',nv:0},{av:'AV9Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'A42Contratada_PessoaCNPJ',fld:'CONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'A53Contratada_AreaTrabalhoDes',fld:'CONTRATADA_AREATRABALHODES',pic:'@!',nv:''},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'AV25CNPJs',fld:'vCNPJS',pic:'',nv:null},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO',pic:'',nv:false},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO',pic:'',nv:false},{av:'A6AreaTrabalho_Descricao',fld:'AREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8Areas',fld:'vAREAS',pic:'',nv:null},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'AV15jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null}],oparms:[{av:'AV17jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'AV10Contratada',fld:'vCONTRATADA',pic:'ZZZZZ9',nv:0},{av:'lblNotassociatedrecordstitle_Caption',ctrl:'NOTASSOCIATEDRECORDSTITLE',prop:'Caption'},{av:'lblAssociatedrecordstitle_Caption',ctrl:'ASSOCIATEDRECORDSTITLE',prop:'Caption'}]}");
         setEventMetadata("ENTER","{handler:'E14RI2',iparms:[{av:'AV15jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV22Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV17jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'AV6MenuPerfil',fld:'vMENUPERFIL',pic:'',nv:null}],oparms:[{av:'AV10Contratada',fld:'vCONTRATADA',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DISASSOCIATE SELECTED'","{handler:'E15RI2',iparms:[{av:'AV15jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'AV7All',fld:'vALL',pic:'',nv:false},{av:'AV17jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null}],oparms:[{ctrl:'BTN_CANCEL',prop:'Caption'},{av:'AV7All',fld:'vALL',pic:'',nv:false},{av:'AV15jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'AV17jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'lblNotassociatedrecordstitle_Caption',ctrl:'NOTASSOCIATEDRECORDSTITLE',prop:'Caption'},{av:'lblAssociatedrecordstitle_Caption',ctrl:'ASSOCIATEDRECORDSTITLE',prop:'Caption'}]}");
         setEventMetadata("'ASSOCIATE SELECTED'","{handler:'E16RI2',iparms:[{av:'AV17jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'AV7All',fld:'vALL',pic:'',nv:false},{av:'AV15jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null}],oparms:[{ctrl:'BTN_CANCEL',prop:'Caption'},{av:'AV7All',fld:'vALL',pic:'',nv:false},{av:'AV17jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'AV15jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'lblNotassociatedrecordstitle_Caption',ctrl:'NOTASSOCIATEDRECORDSTITLE',prop:'Caption'},{av:'lblAssociatedrecordstitle_Caption',ctrl:'ASSOCIATEDRECORDSTITLE',prop:'Caption'}]}");
         setEventMetadata("'ASSOCIATE ALL'","{handler:'E17RI2',iparms:[{av:'AV17jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'AV7All',fld:'vALL',pic:'',nv:false},{av:'AV15jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null}],oparms:[{ctrl:'BTN_CANCEL',prop:'Caption'},{av:'AV7All',fld:'vALL',pic:'',nv:false},{av:'AV17jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'AV15jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'lblNotassociatedrecordstitle_Caption',ctrl:'NOTASSOCIATEDRECORDSTITLE',prop:'Caption'},{av:'lblAssociatedrecordstitle_Caption',ctrl:'ASSOCIATEDRECORDSTITLE',prop:'Caption'}]}");
         setEventMetadata("'DISASSOCIATE ALL'","{handler:'E18RI2',iparms:[{av:'AV15jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'AV7All',fld:'vALL',pic:'',nv:false},{av:'AV17jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null}],oparms:[{ctrl:'BTN_CANCEL',prop:'Caption'},{av:'AV7All',fld:'vALL',pic:'',nv:false},{av:'AV15jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'AV17jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'lblNotassociatedrecordstitle_Caption',ctrl:'NOTASSOCIATEDRECORDSTITLE',prop:'Caption'},{av:'lblAssociatedrecordstitle_Caption',ctrl:'ASSOCIATEDRECORDSTITLE',prop:'Caption'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV17jqNaoAssociados = new GxObjectCollection( context, "jqSelectData.Item", "GxEv3Up14_Meetrika", "SdtjqSelectData_Item", "GeneXus.Programs");
         AV15jqAssociados = new GxObjectCollection( context, "jqSelectData.Item", "GxEv3Up14_Meetrika", "SdtjqSelectData_Item", "GeneXus.Programs");
         AV9Codigos = new GxSimpleCollection();
         A42Contratada_PessoaCNPJ = "";
         A53Contratada_AreaTrabalhoDes = "";
         A438Contratada_Sigla = "";
         AV25CNPJs = new GxSimpleCollection();
         A6AreaTrabalho_Descricao = "";
         AV8Areas = new GxSimpleCollection();
         AV6MenuPerfil = new SdtMenuPerfil(context);
         AV23Usuario_Nome = "";
         GXKey = "";
         GX_FocusControl = "";
         sPrefix = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV24WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV12HTTPRequest = new GxHttpRequest( context);
         scmdbuf = "";
         H00RI2_A57Usuario_PessoaCod = new int[1] ;
         H00RI2_A1Usuario_Codigo = new int[1] ;
         H00RI2_A58Usuario_PessoaNom = new String[] {""} ;
         H00RI2_n58Usuario_PessoaNom = new bool[] {false} ;
         A58Usuario_PessoaNom = "";
         H00RI3_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00RI3_A67ContratadaUsuario_ContratadaPessoaCod = new int[1] ;
         H00RI3_n67ContratadaUsuario_ContratadaPessoaCod = new bool[] {false} ;
         H00RI3_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00RI3_A348ContratadaUsuario_ContratadaPessoaCNPJ = new String[] {""} ;
         H00RI3_n348ContratadaUsuario_ContratadaPessoaCNPJ = new bool[] {false} ;
         H00RI3_A438Contratada_Sigla = new String[] {""} ;
         H00RI3_n438Contratada_Sigla = new bool[] {false} ;
         A348ContratadaUsuario_ContratadaPessoaCNPJ = "";
         AV26CNPJ = "";
         H00RI4_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00RI4_A29Contratante_Codigo = new int[1] ;
         H00RI4_n29Contratante_Codigo = new bool[] {false} ;
         H00RI4_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         H00RI4_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         H00RI4_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00RI4_A438Contratada_Sigla = new String[] {""} ;
         H00RI4_n438Contratada_Sigla = new bool[] {false} ;
         H00RI4_A1297ContratadaUsuario_AreaTrabalhoDes = new String[] {""} ;
         H00RI4_n1297ContratadaUsuario_AreaTrabalhoDes = new bool[] {false} ;
         A1297ContratadaUsuario_AreaTrabalhoDes = "";
         H00RI6_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         H00RI6_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         H00RI6_A1020ContratanteUsuario_AreaTrabalhoCod = new int[1] ;
         H00RI6_n1020ContratanteUsuario_AreaTrabalhoCod = new bool[] {false} ;
         AV16jqItem = new SdtjqSelectData_Item(context);
         H00RI7_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00RI7_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         AV5ContratadaUsuario = new SdtContratadaUsuario(context);
         AV37GXV3 = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         AV20Message = new SdtMessages_Message(context);
         H00RI8_A40Contratada_PessoaCod = new int[1] ;
         H00RI8_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00RI8_A43Contratada_Ativo = new bool[] {false} ;
         H00RI8_A72AreaTrabalho_Ativo = new bool[] {false} ;
         H00RI8_n72AreaTrabalho_Ativo = new bool[] {false} ;
         H00RI8_A39Contratada_Codigo = new int[1] ;
         H00RI8_A42Contratada_PessoaCNPJ = new String[] {""} ;
         H00RI8_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         H00RI8_A438Contratada_Sigla = new String[] {""} ;
         H00RI8_n438Contratada_Sigla = new bool[] {false} ;
         H00RI8_A53Contratada_AreaTrabalhoDes = new String[] {""} ;
         H00RI8_n53Contratada_AreaTrabalhoDes = new bool[] {false} ;
         H00RI9_A5AreaTrabalho_Codigo = new int[1] ;
         H00RI9_A72AreaTrabalho_Ativo = new bool[] {false} ;
         H00RI9_n72AreaTrabalho_Ativo = new bool[] {false} ;
         H00RI9_A6AreaTrabalho_Descricao = new String[] {""} ;
         H00RI10_A40Contratada_PessoaCod = new int[1] ;
         H00RI10_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00RI10_A42Contratada_PessoaCNPJ = new String[] {""} ;
         H00RI10_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         H00RI10_A39Contratada_Codigo = new int[1] ;
         H00RI10_A53Contratada_AreaTrabalhoDes = new String[] {""} ;
         H00RI10_n53Contratada_AreaTrabalhoDes = new bool[] {false} ;
         H00RI10_A438Contratada_Sigla = new String[] {""} ;
         H00RI10_n438Contratada_Sigla = new bool[] {false} ;
         H00RI10_A43Contratada_Ativo = new bool[] {false} ;
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_confirm_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         lblNotassociatedrecordstitle_Jsonclick = "";
         lblCenterrecordstitle_Jsonclick = "";
         lblAssociatedrecordstitle_Jsonclick = "";
         imgImageassociateall_Jsonclick = "";
         imgImageassociateselected_Jsonclick = "";
         imgImagedisassociateselected_Jsonclick = "";
         imgImagedisassociateall_Jsonclick = "";
         lblAssociationtitle_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_associarusuariocontratadas__default(),
            new Object[][] {
                new Object[] {
               H00RI2_A57Usuario_PessoaCod, H00RI2_A1Usuario_Codigo, H00RI2_A58Usuario_PessoaNom, H00RI2_n58Usuario_PessoaNom
               }
               , new Object[] {
               H00RI3_A66ContratadaUsuario_ContratadaCod, H00RI3_A67ContratadaUsuario_ContratadaPessoaCod, H00RI3_n67ContratadaUsuario_ContratadaPessoaCod, H00RI3_A69ContratadaUsuario_UsuarioCod, H00RI3_A348ContratadaUsuario_ContratadaPessoaCNPJ, H00RI3_n348ContratadaUsuario_ContratadaPessoaCNPJ, H00RI3_A438Contratada_Sigla, H00RI3_n438Contratada_Sigla
               }
               , new Object[] {
               H00RI4_A69ContratadaUsuario_UsuarioCod, H00RI4_A29Contratante_Codigo, H00RI4_n29Contratante_Codigo, H00RI4_A1228ContratadaUsuario_AreaTrabalhoCod, H00RI4_n1228ContratadaUsuario_AreaTrabalhoCod, H00RI4_A66ContratadaUsuario_ContratadaCod, H00RI4_A438Contratada_Sigla, H00RI4_n438Contratada_Sigla, H00RI4_A1297ContratadaUsuario_AreaTrabalhoDes, H00RI4_n1297ContratadaUsuario_AreaTrabalhoDes
               }
               , new Object[] {
               H00RI6_A63ContratanteUsuario_ContratanteCod, H00RI6_A60ContratanteUsuario_UsuarioCod, H00RI6_A1020ContratanteUsuario_AreaTrabalhoCod, H00RI6_n1020ContratanteUsuario_AreaTrabalhoCod
               }
               , new Object[] {
               H00RI7_A69ContratadaUsuario_UsuarioCod, H00RI7_A66ContratadaUsuario_ContratadaCod
               }
               , new Object[] {
               H00RI8_A40Contratada_PessoaCod, H00RI8_A52Contratada_AreaTrabalhoCod, H00RI8_A43Contratada_Ativo, H00RI8_A72AreaTrabalho_Ativo, H00RI8_n72AreaTrabalho_Ativo, H00RI8_A39Contratada_Codigo, H00RI8_A42Contratada_PessoaCNPJ, H00RI8_n42Contratada_PessoaCNPJ, H00RI8_A438Contratada_Sigla, H00RI8_A53Contratada_AreaTrabalhoDes,
               H00RI8_n53Contratada_AreaTrabalhoDes
               }
               , new Object[] {
               H00RI9_A5AreaTrabalho_Codigo, H00RI9_A72AreaTrabalho_Ativo, H00RI9_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               H00RI10_A40Contratada_PessoaCod, H00RI10_A52Contratada_AreaTrabalhoCod, H00RI10_A42Contratada_PessoaCNPJ, H00RI10_n42Contratada_PessoaCNPJ, H00RI10_A39Contratada_Codigo, H00RI10_A53Contratada_AreaTrabalhoDes, H00RI10_n53Contratada_AreaTrabalhoDes, H00RI10_A438Contratada_Sigla, H00RI10_A43Contratada_Ativo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavUsuario_nome_Enabled = 0;
      }

      private short nRcdExists_8 ;
      private short nIsMod_8 ;
      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV29GXLvl7 ;
      private short AV33GXLvl39 ;
      private short AV35GXLvl85 ;
      private short nGXWrapped ;
      private int AV22Usuario_Codigo ;
      private int wcpOAV22Usuario_Codigo ;
      private int edtavUsuario_nome_Enabled ;
      private int A39Contratada_Codigo ;
      private int A5AreaTrabalho_Codigo ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int Jqnaoassociados_Width ;
      private int Jqnaoassociados_Height ;
      private int Jqnaoassociados_Selectedcolor ;
      private int Jqnaoassociados_Itemwidth ;
      private int Jqnaoassociados_Itemheight ;
      private int Jqassociados_Width ;
      private int Jqassociados_Height ;
      private int Jqassociados_Selectedcolor ;
      private int Jqassociados_Itemwidth ;
      private int Jqassociados_Itemheight ;
      private int AV11Contratadas ;
      private int A57Usuario_PessoaCod ;
      private int A1Usuario_Codigo ;
      private int A67ContratadaUsuario_ContratadaPessoaCod ;
      private int A29Contratante_Codigo ;
      private int A1228ContratadaUsuario_AreaTrabalhoCod ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int A1020ContratanteUsuario_AreaTrabalhoCod ;
      private int imgImageassociateselected_Visible ;
      private int imgImageassociateall_Visible ;
      private int imgImagedisassociateselected_Visible ;
      private int imgImagedisassociateall_Visible ;
      private int bttBtn_confirm_Visible ;
      private int AV34GXV1 ;
      private int AV10Contratada ;
      private int AV36GXV2 ;
      private int AV38GXV4 ;
      private int AV13i ;
      private int A40Contratada_PessoaCod ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String edtavUsuario_nome_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A438Contratada_Sigla ;
      private String AV23Usuario_Nome ;
      private String Jqnaoassociados_Rounding ;
      private String Jqnaoassociados_Theme ;
      private String Jqnaoassociados_Unselectedicon ;
      private String Jqassociados_Rounding ;
      private String Jqassociados_Theme ;
      private String Jqassociados_Unselectedicon ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String cmbavContratadas_Internalname ;
      private String scmdbuf ;
      private String A58Usuario_PessoaNom ;
      private String AV26CNPJ ;
      private String imgImageassociateselected_Internalname ;
      private String imgImageassociateall_Internalname ;
      private String imgImagedisassociateselected_Internalname ;
      private String imgImagedisassociateall_Internalname ;
      private String bttBtn_confirm_Internalname ;
      private String bttBtn_cancel_Caption ;
      private String bttBtn_cancel_Internalname ;
      private String lblNotassociatedrecordstitle_Caption ;
      private String lblNotassociatedrecordstitle_Internalname ;
      private String lblAssociatedrecordstitle_Caption ;
      private String lblAssociatedrecordstitle_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String cmbavContratadas_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String bttBtn_confirm_Jsonclick ;
      private String bttBtn_cancel_Jsonclick ;
      private String tblTablefullcontent_Internalname ;
      private String tblTablecontent_Internalname ;
      private String lblNotassociatedrecordstitle_Jsonclick ;
      private String lblCenterrecordstitle_Internalname ;
      private String lblCenterrecordstitle_Jsonclick ;
      private String lblAssociatedrecordstitle_Jsonclick ;
      private String tblTable1_Internalname ;
      private String imgImageassociateall_Jsonclick ;
      private String imgImageassociateselected_Jsonclick ;
      private String imgImagedisassociateselected_Jsonclick ;
      private String imgImagedisassociateall_Jsonclick ;
      private String tblTablemergedassociationtitle_Internalname ;
      private String lblAssociationtitle_Internalname ;
      private String lblAssociationtitle_Jsonclick ;
      private String edtavUsuario_nome_Jsonclick ;
      private String Jqnaoassociados_Internalname ;
      private String Jqassociados_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool A72AreaTrabalho_Ativo ;
      private bool A43Contratada_Ativo ;
      private bool AV7All ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool n58Usuario_PessoaNom ;
      private bool BRKRI4 ;
      private bool n67ContratadaUsuario_ContratadaPessoaCod ;
      private bool n348ContratadaUsuario_ContratadaPessoaCNPJ ;
      private bool n438Contratada_Sigla ;
      private bool n29Contratante_Codigo ;
      private bool n1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool n1297ContratadaUsuario_AreaTrabalhoDes ;
      private bool n1020ContratanteUsuario_AreaTrabalhoCod ;
      private bool AV21Success ;
      private bool n72AreaTrabalho_Ativo ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool n53Contratada_AreaTrabalhoDes ;
      private bool BRKRI11 ;
      private String A42Contratada_PessoaCNPJ ;
      private String A53Contratada_AreaTrabalhoDes ;
      private String A6AreaTrabalho_Descricao ;
      private String A348ContratadaUsuario_ContratadaPessoaCNPJ ;
      private String A1297ContratadaUsuario_AreaTrabalhoDes ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavContratadas ;
      private IDataStoreProvider pr_default ;
      private int[] H00RI2_A57Usuario_PessoaCod ;
      private int[] H00RI2_A1Usuario_Codigo ;
      private String[] H00RI2_A58Usuario_PessoaNom ;
      private bool[] H00RI2_n58Usuario_PessoaNom ;
      private int[] H00RI3_A66ContratadaUsuario_ContratadaCod ;
      private int[] H00RI3_A67ContratadaUsuario_ContratadaPessoaCod ;
      private bool[] H00RI3_n67ContratadaUsuario_ContratadaPessoaCod ;
      private int[] H00RI3_A69ContratadaUsuario_UsuarioCod ;
      private String[] H00RI3_A348ContratadaUsuario_ContratadaPessoaCNPJ ;
      private bool[] H00RI3_n348ContratadaUsuario_ContratadaPessoaCNPJ ;
      private String[] H00RI3_A438Contratada_Sigla ;
      private bool[] H00RI3_n438Contratada_Sigla ;
      private int[] H00RI4_A69ContratadaUsuario_UsuarioCod ;
      private int[] H00RI4_A29Contratante_Codigo ;
      private bool[] H00RI4_n29Contratante_Codigo ;
      private int[] H00RI4_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] H00RI4_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private int[] H00RI4_A66ContratadaUsuario_ContratadaCod ;
      private String[] H00RI4_A438Contratada_Sigla ;
      private bool[] H00RI4_n438Contratada_Sigla ;
      private String[] H00RI4_A1297ContratadaUsuario_AreaTrabalhoDes ;
      private bool[] H00RI4_n1297ContratadaUsuario_AreaTrabalhoDes ;
      private int[] H00RI6_A63ContratanteUsuario_ContratanteCod ;
      private int[] H00RI6_A60ContratanteUsuario_UsuarioCod ;
      private int[] H00RI6_A1020ContratanteUsuario_AreaTrabalhoCod ;
      private bool[] H00RI6_n1020ContratanteUsuario_AreaTrabalhoCod ;
      private int[] H00RI7_A69ContratadaUsuario_UsuarioCod ;
      private int[] H00RI7_A66ContratadaUsuario_ContratadaCod ;
      private int[] H00RI8_A40Contratada_PessoaCod ;
      private int[] H00RI8_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00RI8_A43Contratada_Ativo ;
      private bool[] H00RI8_A72AreaTrabalho_Ativo ;
      private bool[] H00RI8_n72AreaTrabalho_Ativo ;
      private int[] H00RI8_A39Contratada_Codigo ;
      private String[] H00RI8_A42Contratada_PessoaCNPJ ;
      private bool[] H00RI8_n42Contratada_PessoaCNPJ ;
      private String[] H00RI8_A438Contratada_Sigla ;
      private bool[] H00RI8_n438Contratada_Sigla ;
      private String[] H00RI8_A53Contratada_AreaTrabalhoDes ;
      private bool[] H00RI8_n53Contratada_AreaTrabalhoDes ;
      private int[] H00RI9_A5AreaTrabalho_Codigo ;
      private bool[] H00RI9_A72AreaTrabalho_Ativo ;
      private bool[] H00RI9_n72AreaTrabalho_Ativo ;
      private String[] H00RI9_A6AreaTrabalho_Descricao ;
      private int[] H00RI10_A40Contratada_PessoaCod ;
      private int[] H00RI10_A52Contratada_AreaTrabalhoCod ;
      private String[] H00RI10_A42Contratada_PessoaCNPJ ;
      private bool[] H00RI10_n42Contratada_PessoaCNPJ ;
      private int[] H00RI10_A39Contratada_Codigo ;
      private String[] H00RI10_A53Contratada_AreaTrabalhoDes ;
      private bool[] H00RI10_n53Contratada_AreaTrabalhoDes ;
      private String[] H00RI10_A438Contratada_Sigla ;
      private bool[] H00RI10_n438Contratada_Sigla ;
      private bool[] H00RI10_A43Contratada_Ativo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV12HTTPRequest ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV9Codigos ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV8Areas ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV25CNPJs ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV37GXV3 ;
      [ObjectCollection(ItemType=typeof( SdtjqSelectData_Item ))]
      private IGxCollection AV17jqNaoAssociados ;
      [ObjectCollection(ItemType=typeof( SdtjqSelectData_Item ))]
      private IGxCollection AV15jqAssociados ;
      private SdtContratadaUsuario AV5ContratadaUsuario ;
      private SdtMessages_Message AV20Message ;
      private SdtMenuPerfil AV6MenuPerfil ;
      private SdtjqSelectData_Item AV16jqItem ;
      private wwpbaseobjects.SdtWWPContext AV24WWPContext ;
   }

   public class wp_associarusuariocontratadas__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00RI8( IGxContext context ,
                                             int A39Contratada_Codigo ,
                                             IGxCollection AV9Codigos ,
                                             String A42Contratada_PessoaCNPJ ,
                                             int AV11Contratadas ,
                                             bool A72AreaTrabalho_Ativo ,
                                             bool A43Contratada_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object1 ;
         GXv_Object1 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T1.[Contratada_Ativo], T3.[AreaTrabalho_Ativo], T1.[Contratada_Codigo], T2.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T1.[Contratada_Sigla], T3.[AreaTrabalho_Descricao] AS Contratada_AreaTrabalhoDes FROM (([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) INNER JOIN [AreaTrabalho] T3 WITH (NOLOCK) ON T3.[AreaTrabalho_Codigo] = T1.[Contratada_AreaTrabalhoCod])";
         scmdbuf = scmdbuf + " WHERE (Not " + new GxDbmsUtils( new GxSqlServer()).ValueList(AV9Codigos, "T1.[Contratada_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and (T3.[AreaTrabalho_Ativo] = 1)";
         scmdbuf = scmdbuf + " and (T1.[Contratada_Ativo] = 1)";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T2.[Pessoa_Docto], T3.[AreaTrabalho_Descricao], T1.[Contratada_Sigla]";
         GXv_Object1[0] = scmdbuf;
         return GXv_Object1 ;
      }

      protected Object[] conditional_H00RI9( IGxContext context ,
                                             int A5AreaTrabalho_Codigo ,
                                             IGxCollection AV8Areas ,
                                             bool A72AreaTrabalho_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Ativo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE (Not " + new GxDbmsUtils( new GxSqlServer()).ValueList(AV8Areas, "[AreaTrabalho_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and ([AreaTrabalho_Ativo] = 1)";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [AreaTrabalho_Descricao]";
         GXv_Object3[0] = scmdbuf;
         return GXv_Object3 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 5 :
                     return conditional_H00RI8(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (int)dynConstraints[3] , (bool)dynConstraints[4] , (bool)dynConstraints[5] );
               case 6 :
                     return conditional_H00RI9(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (bool)dynConstraints[2] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00RI2 ;
          prmH00RI2 = new Object[] {
          new Object[] {"@AV22Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00RI3 ;
          prmH00RI3 = new Object[] {
          new Object[] {"@AV22Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00RI4 ;
          prmH00RI4 = new Object[] {
          new Object[] {"@AV22Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00RI6 ;
          prmH00RI6 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV22Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratadaUsuario_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00RI7 ;
          prmH00RI7 = new Object[] {
          new Object[] {"@AV10Contratada",SqlDbType.Int,6,0} ,
          new Object[] {"@AV22Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00RI10 ;
          prmH00RI10 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00RI8 ;
          prmH00RI8 = new Object[] {
          } ;
          Object[] prmH00RI9 ;
          prmH00RI9 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("H00RI2", "SELECT TOP 1 T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Usuario_Codigo], T2.[Pessoa_Nome] AS Usuario_PessoaNom FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) WHERE T1.[Usuario_Codigo] = @AV22Usuario_Codigo ORDER BY T1.[Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00RI2,1,0,false,true )
             ,new CursorDef("H00RI3", "SELECT T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T2.[Contratada_PessoaCod] AS ContratadaUsuario_ContratadaPessoaCod, T1.[ContratadaUsuario_UsuarioCod], T3.[Pessoa_Docto] AS ContratadaUsuario_ContratadaPe, T2.[Contratada_Sigla] FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratada_PessoaCod]) WHERE T1.[ContratadaUsuario_UsuarioCod] = @AV22Usuario_Codigo ORDER BY T1.[ContratadaUsuario_UsuarioCod], T3.[Pessoa_Docto], T2.[Contratada_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00RI3,100,0,true,false )
             ,new CursorDef("H00RI4", "SELECT T1.[ContratadaUsuario_UsuarioCod], T3.[Contratante_Codigo], T2.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod, T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T2.[Contratada_Sigla], T3.[AreaTrabalho_Descricao] AS ContratadaUsuario_AreaTrabalhoDes FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) LEFT JOIN [AreaTrabalho] T3 WITH (NOLOCK) ON T3.[AreaTrabalho_Codigo] = T2.[Contratada_AreaTrabalhoCod]) WHERE T1.[ContratadaUsuario_UsuarioCod] = @AV22Usuario_Codigo ORDER BY T1.[ContratadaUsuario_UsuarioCod], T3.[AreaTrabalho_Descricao], T2.[Contratada_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00RI4,100,0,true,false )
             ,new CursorDef("H00RI6", "SELECT TOP 1 T1.[ContratanteUsuario_ContratanteCod], T1.[ContratanteUsuario_UsuarioCod], COALESCE( T2.[ContratanteUsuario_AreaTrabalhoCod], 0) AS ContratanteUsuario_AreaTrabalhoCod FROM ([ContratanteUsuario] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN(T3.[AreaTrabalho_Codigo]) AS ContratanteUsuario_AreaTrabalhoCod, T4.[ContratanteUsuario_ContratanteCod], T4.[ContratanteUsuario_UsuarioCod] FROM [AreaTrabalho] T3 WITH (NOLOCK),  [ContratanteUsuario] T4 WITH (NOLOCK) WHERE T3.[Contratante_Codigo] = T4.[ContratanteUsuario_ContratanteCod] GROUP BY T4.[ContratanteUsuario_ContratanteCod], T4.[ContratanteUsuario_UsuarioCod] ) T2 ON T2.[ContratanteUsuario_ContratanteCod] = T1.[ContratanteUsuario_ContratanteCod] AND T2.[ContratanteUsuario_UsuarioCod] = T1.[ContratanteUsuario_UsuarioCod]) WHERE (T1.[ContratanteUsuario_ContratanteCod] = @Contratante_Codigo and T1.[ContratanteUsuario_UsuarioCod] = @AV22Usuario_Codigo) AND (COALESCE( T2.[ContratanteUsuario_AreaTrabalhoCod], 0) = @ContratadaUsuario_AreaTrabalhoCod) ORDER BY T1.[ContratanteUsuario_ContratanteCod], T1.[ContratanteUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00RI6,1,0,false,true )
             ,new CursorDef("H00RI7", "SELECT TOP 1 [ContratadaUsuario_UsuarioCod], [ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod FROM [ContratadaUsuario] WITH (NOLOCK) WHERE [ContratadaUsuario_ContratadaCod] = @AV10Contratada and [ContratadaUsuario_UsuarioCod] = @AV22Usuario_Codigo ORDER BY [ContratadaUsuario_ContratadaCod], [ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00RI7,1,0,false,true )
             ,new CursorDef("H00RI8", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00RI8,100,0,false,false )
             ,new CursorDef("H00RI9", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00RI9,100,0,true,false )
             ,new CursorDef("H00RI10", "SELECT T1.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T2.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T1.[Contratada_Codigo], T3.[AreaTrabalho_Descricao] AS Contratada_AreaTrabalhoDes, T1.[Contratada_Sigla], T1.[Contratada_Ativo] FROM (([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) INNER JOIN [AreaTrabalho] T3 WITH (NOLOCK) ON T3.[AreaTrabalho_Codigo] = T1.[Contratada_AreaTrabalhoCod]) WHERE (T1.[Contratada_AreaTrabalhoCod] = @AreaTrabalho_Codigo) AND (T1.[Contratada_Ativo] = 1) ORDER BY T1.[Contratada_AreaTrabalhoCod], T2.[Pessoa_Docto], T1.[Contratada_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00RI10,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((String[]) buf[6])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getString(7, 15) ;
                ((String[]) buf[9])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[8])[0] = rslt.getBool(7) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[4]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
