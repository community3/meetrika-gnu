/*
               File: GetWWPaisFilterData
        Description: Get WWPais Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:14.40
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwpaisfilterdata : GXProcedure
   {
      public getwwpaisfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwpaisfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
         return AV25OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwpaisfilterdata objgetwwpaisfilterdata;
         objgetwwpaisfilterdata = new getwwpaisfilterdata();
         objgetwwpaisfilterdata.AV16DDOName = aP0_DDOName;
         objgetwwpaisfilterdata.AV14SearchTxt = aP1_SearchTxt;
         objgetwwpaisfilterdata.AV15SearchTxtTo = aP2_SearchTxtTo;
         objgetwwpaisfilterdata.AV20OptionsJson = "" ;
         objgetwwpaisfilterdata.AV23OptionsDescJson = "" ;
         objgetwwpaisfilterdata.AV25OptionIndexesJson = "" ;
         objgetwwpaisfilterdata.context.SetSubmitInitialConfig(context);
         objgetwwpaisfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwpaisfilterdata);
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwpaisfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV19Options = (IGxCollection)(new GxSimpleCollection());
         AV22OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV24OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV16DDOName), "DDO_PAIS_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADPAIS_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV20OptionsJson = AV19Options.ToJSonString(false);
         AV23OptionsDescJson = AV22OptionsDesc.ToJSonString(false);
         AV25OptionIndexesJson = AV24OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV27Session.Get("WWPaisGridState"), "") == 0 )
         {
            AV29GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWPaisGridState"), "");
         }
         else
         {
            AV29GridState.FromXml(AV27Session.Get("WWPaisGridState"), "");
         }
         AV45GXV1 = 1;
         while ( AV45GXV1 <= AV29GridState.gxTpr_Filtervalues.Count )
         {
            AV30GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV29GridState.gxTpr_Filtervalues.Item(AV45GXV1));
            if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFPAIS_CODIGO") == 0 )
            {
               AV10TFPais_Codigo = (int)(NumberUtil.Val( AV30GridStateFilterValue.gxTpr_Value, "."));
               AV11TFPais_Codigo_To = (int)(NumberUtil.Val( AV30GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFPAIS_NOME") == 0 )
            {
               AV12TFPais_Nome = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFPAIS_NOME_SEL") == 0 )
            {
               AV13TFPais_Nome_Sel = AV30GridStateFilterValue.gxTpr_Value;
            }
            AV45GXV1 = (int)(AV45GXV1+1);
         }
         if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(1));
            AV32DynamicFiltersSelector1 = AV31GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "PAIS_NOME") == 0 )
            {
               AV33DynamicFiltersOperator1 = AV31GridStateDynamicFilter.gxTpr_Operator;
               AV34Pais_Nome1 = AV31GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV35DynamicFiltersEnabled2 = true;
               AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(2));
               AV36DynamicFiltersSelector2 = AV31GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV36DynamicFiltersSelector2, "PAIS_NOME") == 0 )
               {
                  AV37DynamicFiltersOperator2 = AV31GridStateDynamicFilter.gxTpr_Operator;
                  AV38Pais_Nome2 = AV31GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV39DynamicFiltersEnabled3 = true;
                  AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(3));
                  AV40DynamicFiltersSelector3 = AV31GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "PAIS_NOME") == 0 )
                  {
                     AV41DynamicFiltersOperator3 = AV31GridStateDynamicFilter.gxTpr_Operator;
                     AV42Pais_Nome3 = AV31GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADPAIS_NOMEOPTIONS' Routine */
         AV12TFPais_Nome = AV14SearchTxt;
         AV13TFPais_Nome_Sel = "";
         AV47WWPaisDS_1_Dynamicfiltersselector1 = AV32DynamicFiltersSelector1;
         AV48WWPaisDS_2_Dynamicfiltersoperator1 = AV33DynamicFiltersOperator1;
         AV49WWPaisDS_3_Pais_nome1 = AV34Pais_Nome1;
         AV50WWPaisDS_4_Dynamicfiltersenabled2 = AV35DynamicFiltersEnabled2;
         AV51WWPaisDS_5_Dynamicfiltersselector2 = AV36DynamicFiltersSelector2;
         AV52WWPaisDS_6_Dynamicfiltersoperator2 = AV37DynamicFiltersOperator2;
         AV53WWPaisDS_7_Pais_nome2 = AV38Pais_Nome2;
         AV54WWPaisDS_8_Dynamicfiltersenabled3 = AV39DynamicFiltersEnabled3;
         AV55WWPaisDS_9_Dynamicfiltersselector3 = AV40DynamicFiltersSelector3;
         AV56WWPaisDS_10_Dynamicfiltersoperator3 = AV41DynamicFiltersOperator3;
         AV57WWPaisDS_11_Pais_nome3 = AV42Pais_Nome3;
         AV58WWPaisDS_12_Tfpais_codigo = AV10TFPais_Codigo;
         AV59WWPaisDS_13_Tfpais_codigo_to = AV11TFPais_Codigo_To;
         AV60WWPaisDS_14_Tfpais_nome = AV12TFPais_Nome;
         AV61WWPaisDS_15_Tfpais_nome_sel = AV13TFPais_Nome_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV47WWPaisDS_1_Dynamicfiltersselector1 ,
                                              AV48WWPaisDS_2_Dynamicfiltersoperator1 ,
                                              AV49WWPaisDS_3_Pais_nome1 ,
                                              AV50WWPaisDS_4_Dynamicfiltersenabled2 ,
                                              AV51WWPaisDS_5_Dynamicfiltersselector2 ,
                                              AV52WWPaisDS_6_Dynamicfiltersoperator2 ,
                                              AV53WWPaisDS_7_Pais_nome2 ,
                                              AV54WWPaisDS_8_Dynamicfiltersenabled3 ,
                                              AV55WWPaisDS_9_Dynamicfiltersselector3 ,
                                              AV56WWPaisDS_10_Dynamicfiltersoperator3 ,
                                              AV57WWPaisDS_11_Pais_nome3 ,
                                              AV58WWPaisDS_12_Tfpais_codigo ,
                                              AV59WWPaisDS_13_Tfpais_codigo_to ,
                                              AV61WWPaisDS_15_Tfpais_nome_sel ,
                                              AV60WWPaisDS_14_Tfpais_nome ,
                                              A22Pais_Nome ,
                                              A21Pais_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT
                                              }
         });
         lV49WWPaisDS_3_Pais_nome1 = StringUtil.PadR( StringUtil.RTrim( AV49WWPaisDS_3_Pais_nome1), 50, "%");
         lV49WWPaisDS_3_Pais_nome1 = StringUtil.PadR( StringUtil.RTrim( AV49WWPaisDS_3_Pais_nome1), 50, "%");
         lV53WWPaisDS_7_Pais_nome2 = StringUtil.PadR( StringUtil.RTrim( AV53WWPaisDS_7_Pais_nome2), 50, "%");
         lV53WWPaisDS_7_Pais_nome2 = StringUtil.PadR( StringUtil.RTrim( AV53WWPaisDS_7_Pais_nome2), 50, "%");
         lV57WWPaisDS_11_Pais_nome3 = StringUtil.PadR( StringUtil.RTrim( AV57WWPaisDS_11_Pais_nome3), 50, "%");
         lV57WWPaisDS_11_Pais_nome3 = StringUtil.PadR( StringUtil.RTrim( AV57WWPaisDS_11_Pais_nome3), 50, "%");
         lV60WWPaisDS_14_Tfpais_nome = StringUtil.PadR( StringUtil.RTrim( AV60WWPaisDS_14_Tfpais_nome), 50, "%");
         /* Using cursor P00EH2 */
         pr_default.execute(0, new Object[] {lV49WWPaisDS_3_Pais_nome1, lV49WWPaisDS_3_Pais_nome1, lV53WWPaisDS_7_Pais_nome2, lV53WWPaisDS_7_Pais_nome2, lV57WWPaisDS_11_Pais_nome3, lV57WWPaisDS_11_Pais_nome3, AV58WWPaisDS_12_Tfpais_codigo, AV59WWPaisDS_13_Tfpais_codigo_to, lV60WWPaisDS_14_Tfpais_nome, AV61WWPaisDS_15_Tfpais_nome_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKEH2 = false;
            A22Pais_Nome = P00EH2_A22Pais_Nome[0];
            A21Pais_Codigo = P00EH2_A21Pais_Codigo[0];
            AV26count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00EH2_A22Pais_Nome[0], A22Pais_Nome) == 0 ) )
            {
               BRKEH2 = false;
               A21Pais_Codigo = P00EH2_A21Pais_Codigo[0];
               AV26count = (long)(AV26count+1);
               BRKEH2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A22Pais_Nome)) )
            {
               AV18Option = A22Pais_Nome;
               AV19Options.Add(AV18Option, 0);
               AV24OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV26count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV19Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKEH2 )
            {
               BRKEH2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV19Options = new GxSimpleCollection();
         AV22OptionsDesc = new GxSimpleCollection();
         AV24OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV27Session = context.GetSession();
         AV29GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV30GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12TFPais_Nome = "";
         AV13TFPais_Nome_Sel = "";
         AV31GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV32DynamicFiltersSelector1 = "";
         AV34Pais_Nome1 = "";
         AV36DynamicFiltersSelector2 = "";
         AV38Pais_Nome2 = "";
         AV40DynamicFiltersSelector3 = "";
         AV42Pais_Nome3 = "";
         AV47WWPaisDS_1_Dynamicfiltersselector1 = "";
         AV49WWPaisDS_3_Pais_nome1 = "";
         AV51WWPaisDS_5_Dynamicfiltersselector2 = "";
         AV53WWPaisDS_7_Pais_nome2 = "";
         AV55WWPaisDS_9_Dynamicfiltersselector3 = "";
         AV57WWPaisDS_11_Pais_nome3 = "";
         AV60WWPaisDS_14_Tfpais_nome = "";
         AV61WWPaisDS_15_Tfpais_nome_sel = "";
         scmdbuf = "";
         lV49WWPaisDS_3_Pais_nome1 = "";
         lV53WWPaisDS_7_Pais_nome2 = "";
         lV57WWPaisDS_11_Pais_nome3 = "";
         lV60WWPaisDS_14_Tfpais_nome = "";
         A22Pais_Nome = "";
         P00EH2_A22Pais_Nome = new String[] {""} ;
         P00EH2_A21Pais_Codigo = new int[1] ;
         AV18Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwpaisfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00EH2_A22Pais_Nome, P00EH2_A21Pais_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV33DynamicFiltersOperator1 ;
      private short AV37DynamicFiltersOperator2 ;
      private short AV41DynamicFiltersOperator3 ;
      private short AV48WWPaisDS_2_Dynamicfiltersoperator1 ;
      private short AV52WWPaisDS_6_Dynamicfiltersoperator2 ;
      private short AV56WWPaisDS_10_Dynamicfiltersoperator3 ;
      private int AV45GXV1 ;
      private int AV10TFPais_Codigo ;
      private int AV11TFPais_Codigo_To ;
      private int AV58WWPaisDS_12_Tfpais_codigo ;
      private int AV59WWPaisDS_13_Tfpais_codigo_to ;
      private int A21Pais_Codigo ;
      private long AV26count ;
      private String AV12TFPais_Nome ;
      private String AV13TFPais_Nome_Sel ;
      private String AV34Pais_Nome1 ;
      private String AV38Pais_Nome2 ;
      private String AV42Pais_Nome3 ;
      private String AV49WWPaisDS_3_Pais_nome1 ;
      private String AV53WWPaisDS_7_Pais_nome2 ;
      private String AV57WWPaisDS_11_Pais_nome3 ;
      private String AV60WWPaisDS_14_Tfpais_nome ;
      private String AV61WWPaisDS_15_Tfpais_nome_sel ;
      private String scmdbuf ;
      private String lV49WWPaisDS_3_Pais_nome1 ;
      private String lV53WWPaisDS_7_Pais_nome2 ;
      private String lV57WWPaisDS_11_Pais_nome3 ;
      private String lV60WWPaisDS_14_Tfpais_nome ;
      private String A22Pais_Nome ;
      private bool returnInSub ;
      private bool AV35DynamicFiltersEnabled2 ;
      private bool AV39DynamicFiltersEnabled3 ;
      private bool AV50WWPaisDS_4_Dynamicfiltersenabled2 ;
      private bool AV54WWPaisDS_8_Dynamicfiltersenabled3 ;
      private bool BRKEH2 ;
      private String AV25OptionIndexesJson ;
      private String AV20OptionsJson ;
      private String AV23OptionsDescJson ;
      private String AV16DDOName ;
      private String AV14SearchTxt ;
      private String AV15SearchTxtTo ;
      private String AV32DynamicFiltersSelector1 ;
      private String AV36DynamicFiltersSelector2 ;
      private String AV40DynamicFiltersSelector3 ;
      private String AV47WWPaisDS_1_Dynamicfiltersselector1 ;
      private String AV51WWPaisDS_5_Dynamicfiltersselector2 ;
      private String AV55WWPaisDS_9_Dynamicfiltersselector3 ;
      private String AV18Option ;
      private IGxSession AV27Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00EH2_A22Pais_Nome ;
      private int[] P00EH2_A21Pais_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV19Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV29GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV30GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV31GridStateDynamicFilter ;
   }

   public class getwwpaisfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00EH2( IGxContext context ,
                                             String AV47WWPaisDS_1_Dynamicfiltersselector1 ,
                                             short AV48WWPaisDS_2_Dynamicfiltersoperator1 ,
                                             String AV49WWPaisDS_3_Pais_nome1 ,
                                             bool AV50WWPaisDS_4_Dynamicfiltersenabled2 ,
                                             String AV51WWPaisDS_5_Dynamicfiltersselector2 ,
                                             short AV52WWPaisDS_6_Dynamicfiltersoperator2 ,
                                             String AV53WWPaisDS_7_Pais_nome2 ,
                                             bool AV54WWPaisDS_8_Dynamicfiltersenabled3 ,
                                             String AV55WWPaisDS_9_Dynamicfiltersselector3 ,
                                             short AV56WWPaisDS_10_Dynamicfiltersoperator3 ,
                                             String AV57WWPaisDS_11_Pais_nome3 ,
                                             int AV58WWPaisDS_12_Tfpais_codigo ,
                                             int AV59WWPaisDS_13_Tfpais_codigo_to ,
                                             String AV61WWPaisDS_15_Tfpais_nome_sel ,
                                             String AV60WWPaisDS_14_Tfpais_nome ,
                                             String A22Pais_Nome ,
                                             int A21Pais_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [10] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [Pais_Nome], [Pais_Codigo] FROM [Pais] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV47WWPaisDS_1_Dynamicfiltersselector1, "PAIS_NOME") == 0 ) && ( AV48WWPaisDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWPaisDS_3_Pais_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Pais_Nome] like @lV49WWPaisDS_3_Pais_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Pais_Nome] like @lV49WWPaisDS_3_Pais_nome1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV47WWPaisDS_1_Dynamicfiltersselector1, "PAIS_NOME") == 0 ) && ( AV48WWPaisDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWPaisDS_3_Pais_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Pais_Nome] like '%' + @lV49WWPaisDS_3_Pais_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Pais_Nome] like '%' + @lV49WWPaisDS_3_Pais_nome1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV50WWPaisDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV51WWPaisDS_5_Dynamicfiltersselector2, "PAIS_NOME") == 0 ) && ( AV52WWPaisDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWPaisDS_7_Pais_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Pais_Nome] like @lV53WWPaisDS_7_Pais_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Pais_Nome] like @lV53WWPaisDS_7_Pais_nome2)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV50WWPaisDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV51WWPaisDS_5_Dynamicfiltersselector2, "PAIS_NOME") == 0 ) && ( AV52WWPaisDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWPaisDS_7_Pais_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Pais_Nome] like '%' + @lV53WWPaisDS_7_Pais_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Pais_Nome] like '%' + @lV53WWPaisDS_7_Pais_nome2)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV54WWPaisDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV55WWPaisDS_9_Dynamicfiltersselector3, "PAIS_NOME") == 0 ) && ( AV56WWPaisDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWPaisDS_11_Pais_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Pais_Nome] like @lV57WWPaisDS_11_Pais_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Pais_Nome] like @lV57WWPaisDS_11_Pais_nome3)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV54WWPaisDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV55WWPaisDS_9_Dynamicfiltersselector3, "PAIS_NOME") == 0 ) && ( AV56WWPaisDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWPaisDS_11_Pais_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Pais_Nome] like '%' + @lV57WWPaisDS_11_Pais_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Pais_Nome] like '%' + @lV57WWPaisDS_11_Pais_nome3)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! (0==AV58WWPaisDS_12_Tfpais_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Pais_Codigo] >= @AV58WWPaisDS_12_Tfpais_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Pais_Codigo] >= @AV58WWPaisDS_12_Tfpais_codigo)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! (0==AV59WWPaisDS_13_Tfpais_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Pais_Codigo] <= @AV59WWPaisDS_13_Tfpais_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([Pais_Codigo] <= @AV59WWPaisDS_13_Tfpais_codigo_to)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV61WWPaisDS_15_Tfpais_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWPaisDS_14_Tfpais_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Pais_Nome] like @lV60WWPaisDS_14_Tfpais_nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([Pais_Nome] like @lV60WWPaisDS_14_Tfpais_nome)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWPaisDS_15_Tfpais_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Pais_Nome] = @AV61WWPaisDS_15_Tfpais_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Pais_Nome] = @AV61WWPaisDS_15_Tfpais_nome_sel)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [Pais_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00EH2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00EH2 ;
          prmP00EH2 = new Object[] {
          new Object[] {"@lV49WWPaisDS_3_Pais_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV49WWPaisDS_3_Pais_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53WWPaisDS_7_Pais_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53WWPaisDS_7_Pais_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV57WWPaisDS_11_Pais_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV57WWPaisDS_11_Pais_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV58WWPaisDS_12_Tfpais_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV59WWPaisDS_13_Tfpais_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV60WWPaisDS_14_Tfpais_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV61WWPaisDS_15_Tfpais_nome_sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00EH2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00EH2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwpaisfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwpaisfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwpaisfilterdata") )
          {
             return  ;
          }
          getwwpaisfilterdata worker = new getwwpaisfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
