/*
               File: ContratoServicosPrazoContratoServicosPrazoRegraWC
        Description: Contrato Servicos Prazo Contrato Servicos Prazo Regra WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:6:46.37
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratoservicosprazocontratoservicosprazoregrawc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contratoservicosprazocontratoservicosprazoregrawc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contratoservicosprazocontratoservicosprazoregrawc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratoServicosPrazo_CntSrvCod )
      {
         this.AV7ContratoServicosPrazo_CntSrvCod = aP0_ContratoServicosPrazo_CntSrvCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7ContratoServicosPrazo_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicosPrazo_CntSrvCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7ContratoServicosPrazo_CntSrvCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_11 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_11_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_11_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
                  AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
                  AV17TFContratoServicosPrazoRegra_Sequencial = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFContratoServicosPrazoRegra_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17TFContratoServicosPrazoRegra_Sequencial), 4, 0)));
                  AV18TFContratoServicosPrazoRegra_Sequencial_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFContratoServicosPrazoRegra_Sequencial_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18TFContratoServicosPrazoRegra_Sequencial_To), 4, 0)));
                  AV21TFContratoServicosPrazoRegra_Inicio = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFContratoServicosPrazoRegra_Inicio", StringUtil.LTrim( StringUtil.Str( AV21TFContratoServicosPrazoRegra_Inicio, 14, 5)));
                  AV22TFContratoServicosPrazoRegra_Inicio_To = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFContratoServicosPrazoRegra_Inicio_To", StringUtil.LTrim( StringUtil.Str( AV22TFContratoServicosPrazoRegra_Inicio_To, 14, 5)));
                  AV25TFContratoServicosPrazoRegra_Fim = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TFContratoServicosPrazoRegra_Fim", StringUtil.LTrim( StringUtil.Str( AV25TFContratoServicosPrazoRegra_Fim, 14, 5)));
                  AV26TFContratoServicosPrazoRegra_Fim_To = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFContratoServicosPrazoRegra_Fim_To", StringUtil.LTrim( StringUtil.Str( AV26TFContratoServicosPrazoRegra_Fim_To, 14, 5)));
                  AV29TFContratoServicosPrazoRegra_Dias = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFContratoServicosPrazoRegra_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29TFContratoServicosPrazoRegra_Dias), 3, 0)));
                  AV30TFContratoServicosPrazoRegra_Dias_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFContratoServicosPrazoRegra_Dias_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30TFContratoServicosPrazoRegra_Dias_To), 3, 0)));
                  AV7ContratoServicosPrazo_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicosPrazo_CntSrvCod), 6, 0)));
                  AV19ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace", AV19ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace);
                  AV23ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace", AV23ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace);
                  AV27ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace", AV27ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace);
                  AV31ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace", AV31ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace);
                  AV38Pgmname = GetNextPar( );
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV17TFContratoServicosPrazoRegra_Sequencial, AV18TFContratoServicosPrazoRegra_Sequencial_To, AV21TFContratoServicosPrazoRegra_Inicio, AV22TFContratoServicosPrazoRegra_Inicio_To, AV25TFContratoServicosPrazoRegra_Fim, AV26TFContratoServicosPrazoRegra_Fim_To, AV29TFContratoServicosPrazoRegra_Dias, AV30TFContratoServicosPrazoRegra_Dias_To, AV7ContratoServicosPrazo_CntSrvCod, AV19ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace, AV23ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace, AV27ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace, AV31ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace, AV38Pgmname, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAG52( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV38Pgmname = "ContratoServicosPrazoContratoServicosPrazoRegraWC";
               context.Gx_err = 0;
               WSG52( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contrato Servicos Prazo Contrato Servicos Prazo Regra WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311764649");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratoservicosprazocontratoservicosprazoregrawc.aspx") + "?" + UrlEncode("" +AV7ContratoServicosPrazo_CntSrvCod)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17TFContratoServicosPrazoRegra_Sequencial), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18TFContratoServicosPrazoRegra_Sequencial_To), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSPRAZOREGRA_INICIO", StringUtil.LTrim( StringUtil.NToC( AV21TFContratoServicosPrazoRegra_Inicio, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSPRAZOREGRA_INICIO_TO", StringUtil.LTrim( StringUtil.NToC( AV22TFContratoServicosPrazoRegra_Inicio_To, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSPRAZOREGRA_FIM", StringUtil.LTrim( StringUtil.NToC( AV25TFContratoServicosPrazoRegra_Fim, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSPRAZOREGRA_FIM_TO", StringUtil.LTrim( StringUtil.NToC( AV26TFContratoServicosPrazoRegra_Fim_To, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSPRAZOREGRA_DIAS", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV29TFContratoServicosPrazoRegra_Dias), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSPRAZOREGRA_DIAS_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV30TFContratoServicosPrazoRegra_Dias_To), 3, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_11", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_11), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV34GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV32DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV32DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOSERVICOSPRAZOREGRA_SEQUENCIALTITLEFILTERDATA", AV16ContratoServicosPrazoRegra_SequencialTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOSERVICOSPRAZOREGRA_SEQUENCIALTITLEFILTERDATA", AV16ContratoServicosPrazoRegra_SequencialTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOSERVICOSPRAZOREGRA_INICIOTITLEFILTERDATA", AV20ContratoServicosPrazoRegra_InicioTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOSERVICOSPRAZOREGRA_INICIOTITLEFILTERDATA", AV20ContratoServicosPrazoRegra_InicioTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOSERVICOSPRAZOREGRA_FIMTITLEFILTERDATA", AV24ContratoServicosPrazoRegra_FimTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOSERVICOSPRAZOREGRA_FIMTITLEFILTERDATA", AV24ContratoServicosPrazoRegra_FimTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOSERVICOSPRAZOREGRA_DIASTITLEFILTERDATA", AV28ContratoServicosPrazoRegra_DiasTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOSERVICOSPRAZOREGRA_DIASTITLEFILTERDATA", AV28ContratoServicosPrazoRegra_DiasTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7ContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7ContratoServicosPrazo_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTRATOSERVICOSPRAZO_CNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratoServicosPrazo_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV38Pgmname));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Caption", StringUtil.RTrim( Ddo_contratoservicosprazoregra_sequencial_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Tooltip", StringUtil.RTrim( Ddo_contratoservicosprazoregra_sequencial_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Cls", StringUtil.RTrim( Ddo_contratoservicosprazoregra_sequencial_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicosprazoregra_sequencial_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoservicosprazoregra_sequencial_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosprazoregra_sequencial_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosprazoregra_sequencial_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_sequencial_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_sequencial_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosprazoregra_sequencial_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_sequencial_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Filtertype", StringUtil.RTrim( Ddo_contratoservicosprazoregra_sequencial_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_sequencial_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_sequencial_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Sortasc", StringUtil.RTrim( Ddo_contratoservicosprazoregra_sequencial_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosprazoregra_sequencial_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosprazoregra_sequencial_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicosprazoregra_sequencial_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicosprazoregra_sequencial_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosprazoregra_sequencial_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Caption", StringUtil.RTrim( Ddo_contratoservicosprazoregra_inicio_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Tooltip", StringUtil.RTrim( Ddo_contratoservicosprazoregra_inicio_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Cls", StringUtil.RTrim( Ddo_contratoservicosprazoregra_inicio_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicosprazoregra_inicio_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoservicosprazoregra_inicio_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosprazoregra_inicio_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosprazoregra_inicio_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_inicio_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_inicio_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosprazoregra_inicio_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_inicio_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Filtertype", StringUtil.RTrim( Ddo_contratoservicosprazoregra_inicio_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_inicio_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_inicio_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Sortasc", StringUtil.RTrim( Ddo_contratoservicosprazoregra_inicio_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosprazoregra_inicio_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosprazoregra_inicio_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicosprazoregra_inicio_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicosprazoregra_inicio_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosprazoregra_inicio_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Caption", StringUtil.RTrim( Ddo_contratoservicosprazoregra_fim_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Tooltip", StringUtil.RTrim( Ddo_contratoservicosprazoregra_fim_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Cls", StringUtil.RTrim( Ddo_contratoservicosprazoregra_fim_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicosprazoregra_fim_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoservicosprazoregra_fim_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosprazoregra_fim_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosprazoregra_fim_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_fim_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_fim_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosprazoregra_fim_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_fim_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Filtertype", StringUtil.RTrim( Ddo_contratoservicosprazoregra_fim_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_fim_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_fim_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Sortasc", StringUtil.RTrim( Ddo_contratoservicosprazoregra_fim_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosprazoregra_fim_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosprazoregra_fim_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicosprazoregra_fim_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicosprazoregra_fim_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosprazoregra_fim_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Caption", StringUtil.RTrim( Ddo_contratoservicosprazoregra_dias_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Tooltip", StringUtil.RTrim( Ddo_contratoservicosprazoregra_dias_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Cls", StringUtil.RTrim( Ddo_contratoservicosprazoregra_dias_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicosprazoregra_dias_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoservicosprazoregra_dias_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosprazoregra_dias_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosprazoregra_dias_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_dias_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_dias_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosprazoregra_dias_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_dias_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Filtertype", StringUtil.RTrim( Ddo_contratoservicosprazoregra_dias_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_dias_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_dias_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Sortasc", StringUtil.RTrim( Ddo_contratoservicosprazoregra_dias_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosprazoregra_dias_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosprazoregra_dias_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicosprazoregra_dias_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicosprazoregra_dias_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosprazoregra_dias_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosprazoregra_sequencial_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicosprazoregra_sequencial_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoservicosprazoregra_sequencial_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosprazoregra_inicio_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicosprazoregra_inicio_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoservicosprazoregra_inicio_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosprazoregra_fim_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicosprazoregra_fim_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoservicosprazoregra_fim_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosprazoregra_dias_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicosprazoregra_dias_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoservicosprazoregra_dias_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormG52( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contratoservicosprazocontratoservicosprazoregrawc.js", "?2020311764759");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContratoServicosPrazoContratoServicosPrazoRegraWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contrato Servicos Prazo Contrato Servicos Prazo Regra WC" ;
      }

      protected void WBG50( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contratoservicosprazocontratoservicosprazoregrawc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            wb_table1_2_G52( true) ;
         }
         else
         {
            wb_table1_2_G52( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_G52e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosPrazo_CntSrvCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A903ContratoServicosPrazo_CntSrvCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A903ContratoServicosPrazo_CntSrvCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosPrazo_CntSrvCod_Jsonclick, 0, "Attribute", "", "", "", edtContratoServicosPrazo_CntSrvCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoServicosPrazoContratoServicosPrazoRegraWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrderedby_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV13OrderedBy), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,21);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrderedby_Jsonclick, 0, "Attribute", "", "", "", edtavOrderedby_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosPrazoContratoServicosPrazoRegraWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,22);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_ContratoServicosPrazoContratoServicosPrazoRegraWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosprazoregra_sequencial_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17TFContratoServicosPrazoRegra_Sequencial), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV17TFContratoServicosPrazoRegra_Sequencial), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,23);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosprazoregra_sequencial_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosprazoregra_sequencial_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosPrazoContratoServicosPrazoRegraWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosprazoregra_sequencial_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18TFContratoServicosPrazoRegra_Sequencial_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV18TFContratoServicosPrazoRegra_Sequencial_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,24);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosprazoregra_sequencial_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosprazoregra_sequencial_to_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosPrazoContratoServicosPrazoRegraWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosprazoregra_inicio_Internalname, StringUtil.LTrim( StringUtil.NToC( AV21TFContratoServicosPrazoRegra_Inicio, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV21TFContratoServicosPrazoRegra_Inicio, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,25);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosprazoregra_inicio_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosprazoregra_inicio_Visible, 1, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosPrazoContratoServicosPrazoRegraWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosprazoregra_inicio_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV22TFContratoServicosPrazoRegra_Inicio_To, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV22TFContratoServicosPrazoRegra_Inicio_To, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,26);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosprazoregra_inicio_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosprazoregra_inicio_to_Visible, 1, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosPrazoContratoServicosPrazoRegraWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosprazoregra_fim_Internalname, StringUtil.LTrim( StringUtil.NToC( AV25TFContratoServicosPrazoRegra_Fim, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV25TFContratoServicosPrazoRegra_Fim, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,27);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosprazoregra_fim_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosprazoregra_fim_Visible, 1, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosPrazoContratoServicosPrazoRegraWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosprazoregra_fim_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV26TFContratoServicosPrazoRegra_Fim_To, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV26TFContratoServicosPrazoRegra_Fim_To, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,28);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosprazoregra_fim_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosprazoregra_fim_to_Visible, 1, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosPrazoContratoServicosPrazoRegraWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosprazoregra_dias_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV29TFContratoServicosPrazoRegra_Dias), 3, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV29TFContratoServicosPrazoRegra_Dias), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,29);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosprazoregra_dias_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosprazoregra_dias_Visible, 1, 0, "text", "", 30, "px", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosPrazoContratoServicosPrazoRegraWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosprazoregra_dias_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV30TFContratoServicosPrazoRegra_Dias_To), 3, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV30TFContratoServicosPrazoRegra_Dias_To), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,30);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosprazoregra_dias_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosprazoregra_dias_to_Visible, 1, 0, "text", "", 30, "px", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosPrazoContratoServicosPrazoRegraWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIALContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosprazoregra_sequencialtitlecontrolidtoreplace_Internalname, AV19ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,32);\"", 0, edtavDdo_contratoservicosprazoregra_sequencialtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoServicosPrazoContratoServicosPrazoRegraWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosprazoregra_iniciotitlecontrolidtoreplace_Internalname, AV23ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,34);\"", 0, edtavDdo_contratoservicosprazoregra_iniciotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoServicosPrazoContratoServicosPrazoRegraWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosprazoregra_fimtitlecontrolidtoreplace_Internalname, AV27ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,36);\"", 0, edtavDdo_contratoservicosprazoregra_fimtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoServicosPrazoContratoServicosPrazoRegraWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIASContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosprazoregra_diastitlecontrolidtoreplace_Internalname, AV31ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", 0, edtavDdo_contratoservicosprazoregra_diastitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoServicosPrazoContratoServicosPrazoRegraWC.htm");
         }
         wbLoad = true;
      }

      protected void STARTG52( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contrato Servicos Prazo Contrato Servicos Prazo Regra WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPG50( ) ;
            }
         }
      }

      protected void WSG52( )
      {
         STARTG52( ) ;
         EVTG52( ) ;
      }

      protected void EVTG52( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPG50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPG50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11G52 */
                                    E11G52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPG50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12G52 */
                                    E12G52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPG50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13G52 */
                                    E13G52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPG50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14G52 */
                                    E14G52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPG50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15G52 */
                                    E15G52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPG50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPG50( ) ;
                              }
                              nGXsfl_11_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
                              SubsflControlProps_112( ) ;
                              A906ContratoServicosPrazoRegra_Sequencial = (short)(context.localUtil.CToN( cgiGet( edtContratoServicosPrazoRegra_Sequencial_Internalname), ",", "."));
                              A907ContratoServicosPrazoRegra_Inicio = context.localUtil.CToN( cgiGet( edtContratoServicosPrazoRegra_Inicio_Internalname), ",", ".");
                              A908ContratoServicosPrazoRegra_Fim = context.localUtil.CToN( cgiGet( edtContratoServicosPrazoRegra_Fim_Internalname), ",", ".");
                              A909ContratoServicosPrazoRegra_Dias = (short)(context.localUtil.CToN( cgiGet( edtContratoServicosPrazoRegra_Dias_Internalname), ",", "."));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E16G52 */
                                          E16G52 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E17G52 */
                                          E17G52 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E18G52 */
                                          E18G52 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicosprazoregra_sequencial Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL"), ",", ".") != Convert.ToDecimal( AV17TFContratoServicosPrazoRegra_Sequencial )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicosprazoregra_sequencial_to Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_TO"), ",", ".") != Convert.ToDecimal( AV18TFContratoServicosPrazoRegra_Sequencial_To )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicosprazoregra_inicio Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSPRAZOREGRA_INICIO"), ",", ".") != AV21TFContratoServicosPrazoRegra_Inicio )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicosprazoregra_inicio_to Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSPRAZOREGRA_INICIO_TO"), ",", ".") != AV22TFContratoServicosPrazoRegra_Inicio_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicosprazoregra_fim Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSPRAZOREGRA_FIM"), ",", ".") != AV25TFContratoServicosPrazoRegra_Fim )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicosprazoregra_fim_to Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSPRAZOREGRA_FIM_TO"), ",", ".") != AV26TFContratoServicosPrazoRegra_Fim_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicosprazoregra_dias Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSPRAZOREGRA_DIAS"), ",", ".") != Convert.ToDecimal( AV29TFContratoServicosPrazoRegra_Dias )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicosprazoregra_dias_to Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSPRAZOREGRA_DIAS_TO"), ",", ".") != Convert.ToDecimal( AV30TFContratoServicosPrazoRegra_Dias_To )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPG50( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEG52( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormG52( ) ;
            }
         }
      }

      protected void PAG52( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_112( ) ;
         while ( nGXsfl_11_idx <= nRC_GXsfl_11 )
         {
            sendrow_112( ) ;
            nGXsfl_11_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_11_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_11_idx+1));
            sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
            SubsflControlProps_112( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       short AV17TFContratoServicosPrazoRegra_Sequencial ,
                                       short AV18TFContratoServicosPrazoRegra_Sequencial_To ,
                                       decimal AV21TFContratoServicosPrazoRegra_Inicio ,
                                       decimal AV22TFContratoServicosPrazoRegra_Inicio_To ,
                                       decimal AV25TFContratoServicosPrazoRegra_Fim ,
                                       decimal AV26TFContratoServicosPrazoRegra_Fim_To ,
                                       short AV29TFContratoServicosPrazoRegra_Dias ,
                                       short AV30TFContratoServicosPrazoRegra_Dias_To ,
                                       int AV7ContratoServicosPrazo_CntSrvCod ,
                                       String AV19ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace ,
                                       String AV23ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace ,
                                       String AV27ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace ,
                                       String AV31ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace ,
                                       String AV38Pgmname ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFG52( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A906ContratoServicosPrazoRegra_Sequencial), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL", StringUtil.LTrim( StringUtil.NToC( (decimal)(A906ContratoServicosPrazoRegra_Sequencial), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSPRAZOREGRA_INICIO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A907ContratoServicosPrazoRegra_Inicio, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOSPRAZOREGRA_INICIO", StringUtil.LTrim( StringUtil.NToC( A907ContratoServicosPrazoRegra_Inicio, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSPRAZOREGRA_FIM", GetSecureSignedToken( sPrefix, context.localUtil.Format( A908ContratoServicosPrazoRegra_Fim, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOSPRAZOREGRA_FIM", StringUtil.LTrim( StringUtil.NToC( A908ContratoServicosPrazoRegra_Fim, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSPRAZOREGRA_DIAS", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A909ContratoServicosPrazoRegra_Dias), "ZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOSPRAZOREGRA_DIAS", StringUtil.LTrim( StringUtil.NToC( (decimal)(A909ContratoServicosPrazoRegra_Dias), 3, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFG52( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV38Pgmname = "ContratoServicosPrazoContratoServicosPrazoRegraWC";
         context.Gx_err = 0;
      }

      protected void RFG52( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 11;
         /* Execute user event: E17G52 */
         E17G52 ();
         nGXsfl_11_idx = 1;
         sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
         SubsflControlProps_112( ) ;
         nGXsfl_11_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_112( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV17TFContratoServicosPrazoRegra_Sequencial ,
                                                 AV18TFContratoServicosPrazoRegra_Sequencial_To ,
                                                 AV21TFContratoServicosPrazoRegra_Inicio ,
                                                 AV22TFContratoServicosPrazoRegra_Inicio_To ,
                                                 AV25TFContratoServicosPrazoRegra_Fim ,
                                                 AV26TFContratoServicosPrazoRegra_Fim_To ,
                                                 AV29TFContratoServicosPrazoRegra_Dias ,
                                                 AV30TFContratoServicosPrazoRegra_Dias_To ,
                                                 A906ContratoServicosPrazoRegra_Sequencial ,
                                                 A907ContratoServicosPrazoRegra_Inicio ,
                                                 A908ContratoServicosPrazoRegra_Fim ,
                                                 A909ContratoServicosPrazoRegra_Dias ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A903ContratoServicosPrazo_CntSrvCod ,
                                                 AV7ContratoServicosPrazo_CntSrvCod },
                                                 new int[] {
                                                 TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DECIMAL,
                                                 TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            /* Using cursor H00G52 */
            pr_default.execute(0, new Object[] {AV7ContratoServicosPrazo_CntSrvCod, AV17TFContratoServicosPrazoRegra_Sequencial, AV18TFContratoServicosPrazoRegra_Sequencial_To, AV21TFContratoServicosPrazoRegra_Inicio, AV22TFContratoServicosPrazoRegra_Inicio_To, AV25TFContratoServicosPrazoRegra_Fim, AV26TFContratoServicosPrazoRegra_Fim_To, AV29TFContratoServicosPrazoRegra_Dias, AV30TFContratoServicosPrazoRegra_Dias_To, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_11_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A903ContratoServicosPrazo_CntSrvCod = H00G52_A903ContratoServicosPrazo_CntSrvCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A903ContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A903ContratoServicosPrazo_CntSrvCod), 6, 0)));
               A909ContratoServicosPrazoRegra_Dias = H00G52_A909ContratoServicosPrazoRegra_Dias[0];
               A908ContratoServicosPrazoRegra_Fim = H00G52_A908ContratoServicosPrazoRegra_Fim[0];
               A907ContratoServicosPrazoRegra_Inicio = H00G52_A907ContratoServicosPrazoRegra_Inicio[0];
               A906ContratoServicosPrazoRegra_Sequencial = H00G52_A906ContratoServicosPrazoRegra_Sequencial[0];
               /* Execute user event: E18G52 */
               E18G52 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 11;
            WBG50( ) ;
         }
         nGXsfl_11_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV17TFContratoServicosPrazoRegra_Sequencial ,
                                              AV18TFContratoServicosPrazoRegra_Sequencial_To ,
                                              AV21TFContratoServicosPrazoRegra_Inicio ,
                                              AV22TFContratoServicosPrazoRegra_Inicio_To ,
                                              AV25TFContratoServicosPrazoRegra_Fim ,
                                              AV26TFContratoServicosPrazoRegra_Fim_To ,
                                              AV29TFContratoServicosPrazoRegra_Dias ,
                                              AV30TFContratoServicosPrazoRegra_Dias_To ,
                                              A906ContratoServicosPrazoRegra_Sequencial ,
                                              A907ContratoServicosPrazoRegra_Inicio ,
                                              A908ContratoServicosPrazoRegra_Fim ,
                                              A909ContratoServicosPrazoRegra_Dias ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              A903ContratoServicosPrazo_CntSrvCod ,
                                              AV7ContratoServicosPrazo_CntSrvCod },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         /* Using cursor H00G53 */
         pr_default.execute(1, new Object[] {AV7ContratoServicosPrazo_CntSrvCod, AV17TFContratoServicosPrazoRegra_Sequencial, AV18TFContratoServicosPrazoRegra_Sequencial_To, AV21TFContratoServicosPrazoRegra_Inicio, AV22TFContratoServicosPrazoRegra_Inicio_To, AV25TFContratoServicosPrazoRegra_Fim, AV26TFContratoServicosPrazoRegra_Fim_To, AV29TFContratoServicosPrazoRegra_Dias, AV30TFContratoServicosPrazoRegra_Dias_To});
         GRID_nRecordCount = H00G53_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV17TFContratoServicosPrazoRegra_Sequencial, AV18TFContratoServicosPrazoRegra_Sequencial_To, AV21TFContratoServicosPrazoRegra_Inicio, AV22TFContratoServicosPrazoRegra_Inicio_To, AV25TFContratoServicosPrazoRegra_Fim, AV26TFContratoServicosPrazoRegra_Fim_To, AV29TFContratoServicosPrazoRegra_Dias, AV30TFContratoServicosPrazoRegra_Dias_To, AV7ContratoServicosPrazo_CntSrvCod, AV19ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace, AV23ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace, AV27ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace, AV31ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace, AV38Pgmname, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV17TFContratoServicosPrazoRegra_Sequencial, AV18TFContratoServicosPrazoRegra_Sequencial_To, AV21TFContratoServicosPrazoRegra_Inicio, AV22TFContratoServicosPrazoRegra_Inicio_To, AV25TFContratoServicosPrazoRegra_Fim, AV26TFContratoServicosPrazoRegra_Fim_To, AV29TFContratoServicosPrazoRegra_Dias, AV30TFContratoServicosPrazoRegra_Dias_To, AV7ContratoServicosPrazo_CntSrvCod, AV19ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace, AV23ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace, AV27ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace, AV31ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace, AV38Pgmname, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV17TFContratoServicosPrazoRegra_Sequencial, AV18TFContratoServicosPrazoRegra_Sequencial_To, AV21TFContratoServicosPrazoRegra_Inicio, AV22TFContratoServicosPrazoRegra_Inicio_To, AV25TFContratoServicosPrazoRegra_Fim, AV26TFContratoServicosPrazoRegra_Fim_To, AV29TFContratoServicosPrazoRegra_Dias, AV30TFContratoServicosPrazoRegra_Dias_To, AV7ContratoServicosPrazo_CntSrvCod, AV19ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace, AV23ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace, AV27ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace, AV31ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace, AV38Pgmname, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV17TFContratoServicosPrazoRegra_Sequencial, AV18TFContratoServicosPrazoRegra_Sequencial_To, AV21TFContratoServicosPrazoRegra_Inicio, AV22TFContratoServicosPrazoRegra_Inicio_To, AV25TFContratoServicosPrazoRegra_Fim, AV26TFContratoServicosPrazoRegra_Fim_To, AV29TFContratoServicosPrazoRegra_Dias, AV30TFContratoServicosPrazoRegra_Dias_To, AV7ContratoServicosPrazo_CntSrvCod, AV19ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace, AV23ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace, AV27ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace, AV31ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace, AV38Pgmname, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV17TFContratoServicosPrazoRegra_Sequencial, AV18TFContratoServicosPrazoRegra_Sequencial_To, AV21TFContratoServicosPrazoRegra_Inicio, AV22TFContratoServicosPrazoRegra_Inicio_To, AV25TFContratoServicosPrazoRegra_Fim, AV26TFContratoServicosPrazoRegra_Fim_To, AV29TFContratoServicosPrazoRegra_Dias, AV30TFContratoServicosPrazoRegra_Dias_To, AV7ContratoServicosPrazo_CntSrvCod, AV19ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace, AV23ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace, AV27ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace, AV31ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace, AV38Pgmname, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPG50( )
      {
         /* Before Start, stand alone formulas. */
         AV38Pgmname = "ContratoServicosPrazoContratoServicosPrazoRegraWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E16G52 */
         E16G52 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV32DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOSERVICOSPRAZOREGRA_SEQUENCIALTITLEFILTERDATA"), AV16ContratoServicosPrazoRegra_SequencialTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOSERVICOSPRAZOREGRA_INICIOTITLEFILTERDATA"), AV20ContratoServicosPrazoRegra_InicioTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOSERVICOSPRAZOREGRA_FIMTITLEFILTERDATA"), AV24ContratoServicosPrazoRegra_FimTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOSERVICOSPRAZOREGRA_DIASTITLEFILTERDATA"), AV28ContratoServicosPrazoRegra_DiasTitleFilterData);
            /* Read variables values. */
            A903ContratoServicosPrazo_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosPrazo_CntSrvCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A903ContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A903ContratoServicosPrazo_CntSrvCod), 6, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vORDEREDBY");
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV13OrderedBy = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            else
            {
               AV13OrderedBy = (short)(context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_sequencial_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_sequencial_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL");
               GX_FocusControl = edtavTfcontratoservicosprazoregra_sequencial_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV17TFContratoServicosPrazoRegra_Sequencial = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFContratoServicosPrazoRegra_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17TFContratoServicosPrazoRegra_Sequencial), 4, 0)));
            }
            else
            {
               AV17TFContratoServicosPrazoRegra_Sequencial = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_sequencial_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFContratoServicosPrazoRegra_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17TFContratoServicosPrazoRegra_Sequencial), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_sequencial_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_sequencial_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_TO");
               GX_FocusControl = edtavTfcontratoservicosprazoregra_sequencial_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV18TFContratoServicosPrazoRegra_Sequencial_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFContratoServicosPrazoRegra_Sequencial_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18TFContratoServicosPrazoRegra_Sequencial_To), 4, 0)));
            }
            else
            {
               AV18TFContratoServicosPrazoRegra_Sequencial_To = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_sequencial_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFContratoServicosPrazoRegra_Sequencial_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18TFContratoServicosPrazoRegra_Sequencial_To), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_inicio_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_inicio_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSPRAZOREGRA_INICIO");
               GX_FocusControl = edtavTfcontratoservicosprazoregra_inicio_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV21TFContratoServicosPrazoRegra_Inicio = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFContratoServicosPrazoRegra_Inicio", StringUtil.LTrim( StringUtil.Str( AV21TFContratoServicosPrazoRegra_Inicio, 14, 5)));
            }
            else
            {
               AV21TFContratoServicosPrazoRegra_Inicio = context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_inicio_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFContratoServicosPrazoRegra_Inicio", StringUtil.LTrim( StringUtil.Str( AV21TFContratoServicosPrazoRegra_Inicio, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_inicio_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_inicio_to_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSPRAZOREGRA_INICIO_TO");
               GX_FocusControl = edtavTfcontratoservicosprazoregra_inicio_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV22TFContratoServicosPrazoRegra_Inicio_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFContratoServicosPrazoRegra_Inicio_To", StringUtil.LTrim( StringUtil.Str( AV22TFContratoServicosPrazoRegra_Inicio_To, 14, 5)));
            }
            else
            {
               AV22TFContratoServicosPrazoRegra_Inicio_To = context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_inicio_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFContratoServicosPrazoRegra_Inicio_To", StringUtil.LTrim( StringUtil.Str( AV22TFContratoServicosPrazoRegra_Inicio_To, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_fim_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_fim_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSPRAZOREGRA_FIM");
               GX_FocusControl = edtavTfcontratoservicosprazoregra_fim_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV25TFContratoServicosPrazoRegra_Fim = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TFContratoServicosPrazoRegra_Fim", StringUtil.LTrim( StringUtil.Str( AV25TFContratoServicosPrazoRegra_Fim, 14, 5)));
            }
            else
            {
               AV25TFContratoServicosPrazoRegra_Fim = context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_fim_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TFContratoServicosPrazoRegra_Fim", StringUtil.LTrim( StringUtil.Str( AV25TFContratoServicosPrazoRegra_Fim, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_fim_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_fim_to_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSPRAZOREGRA_FIM_TO");
               GX_FocusControl = edtavTfcontratoservicosprazoregra_fim_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV26TFContratoServicosPrazoRegra_Fim_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFContratoServicosPrazoRegra_Fim_To", StringUtil.LTrim( StringUtil.Str( AV26TFContratoServicosPrazoRegra_Fim_To, 14, 5)));
            }
            else
            {
               AV26TFContratoServicosPrazoRegra_Fim_To = context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_fim_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFContratoServicosPrazoRegra_Fim_To", StringUtil.LTrim( StringUtil.Str( AV26TFContratoServicosPrazoRegra_Fim_To, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_dias_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_dias_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSPRAZOREGRA_DIAS");
               GX_FocusControl = edtavTfcontratoservicosprazoregra_dias_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV29TFContratoServicosPrazoRegra_Dias = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFContratoServicosPrazoRegra_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29TFContratoServicosPrazoRegra_Dias), 3, 0)));
            }
            else
            {
               AV29TFContratoServicosPrazoRegra_Dias = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_dias_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFContratoServicosPrazoRegra_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29TFContratoServicosPrazoRegra_Dias), 3, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_dias_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_dias_to_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSPRAZOREGRA_DIAS_TO");
               GX_FocusControl = edtavTfcontratoservicosprazoregra_dias_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV30TFContratoServicosPrazoRegra_Dias_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFContratoServicosPrazoRegra_Dias_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30TFContratoServicosPrazoRegra_Dias_To), 3, 0)));
            }
            else
            {
               AV30TFContratoServicosPrazoRegra_Dias_To = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_dias_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFContratoServicosPrazoRegra_Dias_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30TFContratoServicosPrazoRegra_Dias_To), 3, 0)));
            }
            AV19ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosprazoregra_sequencialtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace", AV19ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace);
            AV23ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosprazoregra_iniciotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace", AV23ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace);
            AV27ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosprazoregra_fimtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace", AV27ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace);
            AV31ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosprazoregra_diastitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace", AV31ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_11 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_11"), ",", "."));
            AV34GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV35GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7ContratoServicosPrazo_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ContratoServicosPrazo_CntSrvCod"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contratoservicosprazoregra_sequencial_Caption = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Caption");
            Ddo_contratoservicosprazoregra_sequencial_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Tooltip");
            Ddo_contratoservicosprazoregra_sequencial_Cls = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Cls");
            Ddo_contratoservicosprazoregra_sequencial_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Filteredtext_set");
            Ddo_contratoservicosprazoregra_sequencial_Filteredtextto_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Filteredtextto_set");
            Ddo_contratoservicosprazoregra_sequencial_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Dropdownoptionstype");
            Ddo_contratoservicosprazoregra_sequencial_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Titlecontrolidtoreplace");
            Ddo_contratoservicosprazoregra_sequencial_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Includesortasc"));
            Ddo_contratoservicosprazoregra_sequencial_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Includesortdsc"));
            Ddo_contratoservicosprazoregra_sequencial_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Sortedstatus");
            Ddo_contratoservicosprazoregra_sequencial_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Includefilter"));
            Ddo_contratoservicosprazoregra_sequencial_Filtertype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Filtertype");
            Ddo_contratoservicosprazoregra_sequencial_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Filterisrange"));
            Ddo_contratoservicosprazoregra_sequencial_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Includedatalist"));
            Ddo_contratoservicosprazoregra_sequencial_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Sortasc");
            Ddo_contratoservicosprazoregra_sequencial_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Sortdsc");
            Ddo_contratoservicosprazoregra_sequencial_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Cleanfilter");
            Ddo_contratoservicosprazoregra_sequencial_Rangefilterfrom = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Rangefilterfrom");
            Ddo_contratoservicosprazoregra_sequencial_Rangefilterto = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Rangefilterto");
            Ddo_contratoservicosprazoregra_sequencial_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Searchbuttontext");
            Ddo_contratoservicosprazoregra_inicio_Caption = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Caption");
            Ddo_contratoservicosprazoregra_inicio_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Tooltip");
            Ddo_contratoservicosprazoregra_inicio_Cls = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Cls");
            Ddo_contratoservicosprazoregra_inicio_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Filteredtext_set");
            Ddo_contratoservicosprazoregra_inicio_Filteredtextto_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Filteredtextto_set");
            Ddo_contratoservicosprazoregra_inicio_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Dropdownoptionstype");
            Ddo_contratoservicosprazoregra_inicio_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Titlecontrolidtoreplace");
            Ddo_contratoservicosprazoregra_inicio_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Includesortasc"));
            Ddo_contratoservicosprazoregra_inicio_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Includesortdsc"));
            Ddo_contratoservicosprazoregra_inicio_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Sortedstatus");
            Ddo_contratoservicosprazoregra_inicio_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Includefilter"));
            Ddo_contratoservicosprazoregra_inicio_Filtertype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Filtertype");
            Ddo_contratoservicosprazoregra_inicio_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Filterisrange"));
            Ddo_contratoservicosprazoregra_inicio_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Includedatalist"));
            Ddo_contratoservicosprazoregra_inicio_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Sortasc");
            Ddo_contratoservicosprazoregra_inicio_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Sortdsc");
            Ddo_contratoservicosprazoregra_inicio_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Cleanfilter");
            Ddo_contratoservicosprazoregra_inicio_Rangefilterfrom = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Rangefilterfrom");
            Ddo_contratoservicosprazoregra_inicio_Rangefilterto = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Rangefilterto");
            Ddo_contratoservicosprazoregra_inicio_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Searchbuttontext");
            Ddo_contratoservicosprazoregra_fim_Caption = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Caption");
            Ddo_contratoservicosprazoregra_fim_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Tooltip");
            Ddo_contratoservicosprazoregra_fim_Cls = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Cls");
            Ddo_contratoservicosprazoregra_fim_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Filteredtext_set");
            Ddo_contratoservicosprazoregra_fim_Filteredtextto_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Filteredtextto_set");
            Ddo_contratoservicosprazoregra_fim_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Dropdownoptionstype");
            Ddo_contratoservicosprazoregra_fim_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Titlecontrolidtoreplace");
            Ddo_contratoservicosprazoregra_fim_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Includesortasc"));
            Ddo_contratoservicosprazoregra_fim_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Includesortdsc"));
            Ddo_contratoservicosprazoregra_fim_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Sortedstatus");
            Ddo_contratoservicosprazoregra_fim_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Includefilter"));
            Ddo_contratoservicosprazoregra_fim_Filtertype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Filtertype");
            Ddo_contratoservicosprazoregra_fim_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Filterisrange"));
            Ddo_contratoservicosprazoregra_fim_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Includedatalist"));
            Ddo_contratoservicosprazoregra_fim_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Sortasc");
            Ddo_contratoservicosprazoregra_fim_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Sortdsc");
            Ddo_contratoservicosprazoregra_fim_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Cleanfilter");
            Ddo_contratoservicosprazoregra_fim_Rangefilterfrom = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Rangefilterfrom");
            Ddo_contratoservicosprazoregra_fim_Rangefilterto = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Rangefilterto");
            Ddo_contratoservicosprazoregra_fim_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Searchbuttontext");
            Ddo_contratoservicosprazoregra_dias_Caption = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Caption");
            Ddo_contratoservicosprazoregra_dias_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Tooltip");
            Ddo_contratoservicosprazoregra_dias_Cls = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Cls");
            Ddo_contratoservicosprazoregra_dias_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Filteredtext_set");
            Ddo_contratoservicosprazoregra_dias_Filteredtextto_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Filteredtextto_set");
            Ddo_contratoservicosprazoregra_dias_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Dropdownoptionstype");
            Ddo_contratoservicosprazoregra_dias_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Titlecontrolidtoreplace");
            Ddo_contratoservicosprazoregra_dias_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Includesortasc"));
            Ddo_contratoservicosprazoregra_dias_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Includesortdsc"));
            Ddo_contratoservicosprazoregra_dias_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Sortedstatus");
            Ddo_contratoservicosprazoregra_dias_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Includefilter"));
            Ddo_contratoservicosprazoregra_dias_Filtertype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Filtertype");
            Ddo_contratoservicosprazoregra_dias_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Filterisrange"));
            Ddo_contratoservicosprazoregra_dias_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Includedatalist"));
            Ddo_contratoservicosprazoregra_dias_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Sortasc");
            Ddo_contratoservicosprazoregra_dias_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Sortdsc");
            Ddo_contratoservicosprazoregra_dias_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Cleanfilter");
            Ddo_contratoservicosprazoregra_dias_Rangefilterfrom = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Rangefilterfrom");
            Ddo_contratoservicosprazoregra_dias_Rangefilterto = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Rangefilterto");
            Ddo_contratoservicosprazoregra_dias_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contratoservicosprazoregra_sequencial_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Activeeventkey");
            Ddo_contratoservicosprazoregra_sequencial_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Filteredtext_get");
            Ddo_contratoservicosprazoregra_sequencial_Filteredtextto_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Filteredtextto_get");
            Ddo_contratoservicosprazoregra_inicio_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Activeeventkey");
            Ddo_contratoservicosprazoregra_inicio_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Filteredtext_get");
            Ddo_contratoservicosprazoregra_inicio_Filteredtextto_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Filteredtextto_get");
            Ddo_contratoservicosprazoregra_fim_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Activeeventkey");
            Ddo_contratoservicosprazoregra_fim_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Filteredtext_get");
            Ddo_contratoservicosprazoregra_fim_Filteredtextto_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Filteredtextto_get");
            Ddo_contratoservicosprazoregra_dias_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Activeeventkey");
            Ddo_contratoservicosprazoregra_dias_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Filteredtext_get");
            Ddo_contratoservicosprazoregra_dias_Filteredtextto_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL"), ",", ".") != Convert.ToDecimal( AV17TFContratoServicosPrazoRegra_Sequencial )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_TO"), ",", ".") != Convert.ToDecimal( AV18TFContratoServicosPrazoRegra_Sequencial_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSPRAZOREGRA_INICIO"), ",", ".") != AV21TFContratoServicosPrazoRegra_Inicio )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSPRAZOREGRA_INICIO_TO"), ",", ".") != AV22TFContratoServicosPrazoRegra_Inicio_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSPRAZOREGRA_FIM"), ",", ".") != AV25TFContratoServicosPrazoRegra_Fim )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSPRAZOREGRA_FIM_TO"), ",", ".") != AV26TFContratoServicosPrazoRegra_Fim_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSPRAZOREGRA_DIAS"), ",", ".") != Convert.ToDecimal( AV29TFContratoServicosPrazoRegra_Dias )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSPRAZOREGRA_DIAS_TO"), ",", ".") != Convert.ToDecimal( AV30TFContratoServicosPrazoRegra_Dias_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E16G52 */
         E16G52 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E16G52( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTfcontratoservicosprazoregra_sequencial_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicosprazoregra_sequencial_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosprazoregra_sequencial_Visible), 5, 0)));
         edtavTfcontratoservicosprazoregra_sequencial_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicosprazoregra_sequencial_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosprazoregra_sequencial_to_Visible), 5, 0)));
         edtavTfcontratoservicosprazoregra_inicio_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicosprazoregra_inicio_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosprazoregra_inicio_Visible), 5, 0)));
         edtavTfcontratoservicosprazoregra_inicio_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicosprazoregra_inicio_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosprazoregra_inicio_to_Visible), 5, 0)));
         edtavTfcontratoservicosprazoregra_fim_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicosprazoregra_fim_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosprazoregra_fim_Visible), 5, 0)));
         edtavTfcontratoservicosprazoregra_fim_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicosprazoregra_fim_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosprazoregra_fim_to_Visible), 5, 0)));
         edtavTfcontratoservicosprazoregra_dias_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicosprazoregra_dias_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosprazoregra_dias_Visible), 5, 0)));
         edtavTfcontratoservicosprazoregra_dias_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicosprazoregra_dias_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosprazoregra_dias_to_Visible), 5, 0)));
         Ddo_contratoservicosprazoregra_sequencial_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosPrazoRegra_Sequencial";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosprazoregra_sequencial_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosprazoregra_sequencial_Titlecontrolidtoreplace);
         AV19ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace = Ddo_contratoservicosprazoregra_sequencial_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace", AV19ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace);
         edtavDdo_contratoservicosprazoregra_sequencialtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratoservicosprazoregra_sequencialtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosprazoregra_sequencialtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicosprazoregra_inicio_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosPrazoRegra_Inicio";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosprazoregra_inicio_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosprazoregra_inicio_Titlecontrolidtoreplace);
         AV23ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace = Ddo_contratoservicosprazoregra_inicio_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace", AV23ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace);
         edtavDdo_contratoservicosprazoregra_iniciotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratoservicosprazoregra_iniciotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosprazoregra_iniciotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicosprazoregra_fim_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosPrazoRegra_Fim";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosprazoregra_fim_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosprazoregra_fim_Titlecontrolidtoreplace);
         AV27ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace = Ddo_contratoservicosprazoregra_fim_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace", AV27ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace);
         edtavDdo_contratoservicosprazoregra_fimtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratoservicosprazoregra_fimtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosprazoregra_fimtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicosprazoregra_dias_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosPrazoRegra_Dias";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosprazoregra_dias_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosprazoregra_dias_Titlecontrolidtoreplace);
         AV31ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace = Ddo_contratoservicosprazoregra_dias_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace", AV31ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace);
         edtavDdo_contratoservicosprazoregra_diastitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratoservicosprazoregra_diastitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosprazoregra_diastitlecontrolidtoreplace_Visible), 5, 0)));
         edtContratoServicosPrazo_CntSrvCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicosPrazo_CntSrvCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrazo_CntSrvCod_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrderedby_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrderedby_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrderedby_Visible), 5, 0)));
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV32DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV32DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E17G52( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV16ContratoServicosPrazoRegra_SequencialTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV20ContratoServicosPrazoRegra_InicioTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV24ContratoServicosPrazoRegra_FimTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV28ContratoServicosPrazoRegra_DiasTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContratoServicosPrazoRegra_Sequencial_Titleformat = 2;
         edtContratoServicosPrazoRegra_Sequencial_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Sequencial", AV19ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicosPrazoRegra_Sequencial_Internalname, "Title", edtContratoServicosPrazoRegra_Sequencial_Title);
         edtContratoServicosPrazoRegra_Inicio_Titleformat = 2;
         edtContratoServicosPrazoRegra_Inicio_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Inicio", AV23ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicosPrazoRegra_Inicio_Internalname, "Title", edtContratoServicosPrazoRegra_Inicio_Title);
         edtContratoServicosPrazoRegra_Fim_Titleformat = 2;
         edtContratoServicosPrazoRegra_Fim_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Fim", AV27ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicosPrazoRegra_Fim_Internalname, "Title", edtContratoServicosPrazoRegra_Fim_Title);
         edtContratoServicosPrazoRegra_Dias_Titleformat = 2;
         edtContratoServicosPrazoRegra_Dias_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Dias", AV31ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicosPrazoRegra_Dias_Internalname, "Title", edtContratoServicosPrazoRegra_Dias_Title);
         AV34GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34GridCurrentPage), 10, 0)));
         AV35GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV16ContratoServicosPrazoRegra_SequencialTitleFilterData", AV16ContratoServicosPrazoRegra_SequencialTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV20ContratoServicosPrazoRegra_InicioTitleFilterData", AV20ContratoServicosPrazoRegra_InicioTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV24ContratoServicosPrazoRegra_FimTitleFilterData", AV24ContratoServicosPrazoRegra_FimTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV28ContratoServicosPrazoRegra_DiasTitleFilterData", AV28ContratoServicosPrazoRegra_DiasTitleFilterData);
      }

      protected void E11G52( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV33PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV33PageToGo) ;
         }
      }

      protected void E12G52( )
      {
         /* Ddo_contratoservicosprazoregra_sequencial_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosprazoregra_sequencial_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosprazoregra_sequencial_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosprazoregra_sequencial_Internalname, "SortedStatus", Ddo_contratoservicosprazoregra_sequencial_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosprazoregra_sequencial_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosprazoregra_sequencial_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosprazoregra_sequencial_Internalname, "SortedStatus", Ddo_contratoservicosprazoregra_sequencial_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosprazoregra_sequencial_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV17TFContratoServicosPrazoRegra_Sequencial = (short)(NumberUtil.Val( Ddo_contratoservicosprazoregra_sequencial_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFContratoServicosPrazoRegra_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17TFContratoServicosPrazoRegra_Sequencial), 4, 0)));
            AV18TFContratoServicosPrazoRegra_Sequencial_To = (short)(NumberUtil.Val( Ddo_contratoservicosprazoregra_sequencial_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFContratoServicosPrazoRegra_Sequencial_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18TFContratoServicosPrazoRegra_Sequencial_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E13G52( )
      {
         /* Ddo_contratoservicosprazoregra_inicio_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosprazoregra_inicio_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosprazoregra_inicio_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosprazoregra_inicio_Internalname, "SortedStatus", Ddo_contratoservicosprazoregra_inicio_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosprazoregra_inicio_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosprazoregra_inicio_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosprazoregra_inicio_Internalname, "SortedStatus", Ddo_contratoservicosprazoregra_inicio_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosprazoregra_inicio_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV21TFContratoServicosPrazoRegra_Inicio = NumberUtil.Val( Ddo_contratoservicosprazoregra_inicio_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFContratoServicosPrazoRegra_Inicio", StringUtil.LTrim( StringUtil.Str( AV21TFContratoServicosPrazoRegra_Inicio, 14, 5)));
            AV22TFContratoServicosPrazoRegra_Inicio_To = NumberUtil.Val( Ddo_contratoservicosprazoregra_inicio_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFContratoServicosPrazoRegra_Inicio_To", StringUtil.LTrim( StringUtil.Str( AV22TFContratoServicosPrazoRegra_Inicio_To, 14, 5)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E14G52( )
      {
         /* Ddo_contratoservicosprazoregra_fim_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosprazoregra_fim_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosprazoregra_fim_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosprazoregra_fim_Internalname, "SortedStatus", Ddo_contratoservicosprazoregra_fim_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosprazoregra_fim_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosprazoregra_fim_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosprazoregra_fim_Internalname, "SortedStatus", Ddo_contratoservicosprazoregra_fim_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosprazoregra_fim_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV25TFContratoServicosPrazoRegra_Fim = NumberUtil.Val( Ddo_contratoservicosprazoregra_fim_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TFContratoServicosPrazoRegra_Fim", StringUtil.LTrim( StringUtil.Str( AV25TFContratoServicosPrazoRegra_Fim, 14, 5)));
            AV26TFContratoServicosPrazoRegra_Fim_To = NumberUtil.Val( Ddo_contratoservicosprazoregra_fim_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFContratoServicosPrazoRegra_Fim_To", StringUtil.LTrim( StringUtil.Str( AV26TFContratoServicosPrazoRegra_Fim_To, 14, 5)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E15G52( )
      {
         /* Ddo_contratoservicosprazoregra_dias_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosprazoregra_dias_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosprazoregra_dias_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosprazoregra_dias_Internalname, "SortedStatus", Ddo_contratoservicosprazoregra_dias_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosprazoregra_dias_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosprazoregra_dias_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosprazoregra_dias_Internalname, "SortedStatus", Ddo_contratoservicosprazoregra_dias_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosprazoregra_dias_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV29TFContratoServicosPrazoRegra_Dias = (short)(NumberUtil.Val( Ddo_contratoservicosprazoregra_dias_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFContratoServicosPrazoRegra_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29TFContratoServicosPrazoRegra_Dias), 3, 0)));
            AV30TFContratoServicosPrazoRegra_Dias_To = (short)(NumberUtil.Val( Ddo_contratoservicosprazoregra_dias_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFContratoServicosPrazoRegra_Dias_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30TFContratoServicosPrazoRegra_Dias_To), 3, 0)));
            subgrid_firstpage( ) ;
         }
      }

      private void E18G52( )
      {
         /* Grid_Load Routine */
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 11;
         }
         sendrow_112( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_11_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(11, GridRow);
         }
      }

      protected void S152( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contratoservicosprazoregra_sequencial_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosprazoregra_sequencial_Internalname, "SortedStatus", Ddo_contratoservicosprazoregra_sequencial_Sortedstatus);
         Ddo_contratoservicosprazoregra_inicio_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosprazoregra_inicio_Internalname, "SortedStatus", Ddo_contratoservicosprazoregra_inicio_Sortedstatus);
         Ddo_contratoservicosprazoregra_fim_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosprazoregra_fim_Internalname, "SortedStatus", Ddo_contratoservicosprazoregra_fim_Sortedstatus);
         Ddo_contratoservicosprazoregra_dias_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosprazoregra_dias_Internalname, "SortedStatus", Ddo_contratoservicosprazoregra_dias_Sortedstatus);
      }

      protected void S132( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_contratoservicosprazoregra_sequencial_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosprazoregra_sequencial_Internalname, "SortedStatus", Ddo_contratoservicosprazoregra_sequencial_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_contratoservicosprazoregra_inicio_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosprazoregra_inicio_Internalname, "SortedStatus", Ddo_contratoservicosprazoregra_inicio_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_contratoservicosprazoregra_fim_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosprazoregra_fim_Internalname, "SortedStatus", Ddo_contratoservicosprazoregra_fim_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_contratoservicosprazoregra_dias_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosprazoregra_dias_Internalname, "SortedStatus", Ddo_contratoservicosprazoregra_dias_Sortedstatus);
         }
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV15Session.Get(AV38Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV38Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV15Session.Get(AV38Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV39GXV1 = 1;
         while ( AV39GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV39GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL") == 0 )
            {
               AV17TFContratoServicosPrazoRegra_Sequencial = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFContratoServicosPrazoRegra_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17TFContratoServicosPrazoRegra_Sequencial), 4, 0)));
               AV18TFContratoServicosPrazoRegra_Sequencial_To = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFContratoServicosPrazoRegra_Sequencial_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18TFContratoServicosPrazoRegra_Sequencial_To), 4, 0)));
               if ( ! (0==AV17TFContratoServicosPrazoRegra_Sequencial) )
               {
                  Ddo_contratoservicosprazoregra_sequencial_Filteredtext_set = StringUtil.Str( (decimal)(AV17TFContratoServicosPrazoRegra_Sequencial), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosprazoregra_sequencial_Internalname, "FilteredText_set", Ddo_contratoservicosprazoregra_sequencial_Filteredtext_set);
               }
               if ( ! (0==AV18TFContratoServicosPrazoRegra_Sequencial_To) )
               {
                  Ddo_contratoservicosprazoregra_sequencial_Filteredtextto_set = StringUtil.Str( (decimal)(AV18TFContratoServicosPrazoRegra_Sequencial_To), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosprazoregra_sequencial_Internalname, "FilteredTextTo_set", Ddo_contratoservicosprazoregra_sequencial_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSPRAZOREGRA_INICIO") == 0 )
            {
               AV21TFContratoServicosPrazoRegra_Inicio = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFContratoServicosPrazoRegra_Inicio", StringUtil.LTrim( StringUtil.Str( AV21TFContratoServicosPrazoRegra_Inicio, 14, 5)));
               AV22TFContratoServicosPrazoRegra_Inicio_To = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFContratoServicosPrazoRegra_Inicio_To", StringUtil.LTrim( StringUtil.Str( AV22TFContratoServicosPrazoRegra_Inicio_To, 14, 5)));
               if ( ! (Convert.ToDecimal(0)==AV21TFContratoServicosPrazoRegra_Inicio) )
               {
                  Ddo_contratoservicosprazoregra_inicio_Filteredtext_set = StringUtil.Str( AV21TFContratoServicosPrazoRegra_Inicio, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosprazoregra_inicio_Internalname, "FilteredText_set", Ddo_contratoservicosprazoregra_inicio_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV22TFContratoServicosPrazoRegra_Inicio_To) )
               {
                  Ddo_contratoservicosprazoregra_inicio_Filteredtextto_set = StringUtil.Str( AV22TFContratoServicosPrazoRegra_Inicio_To, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosprazoregra_inicio_Internalname, "FilteredTextTo_set", Ddo_contratoservicosprazoregra_inicio_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSPRAZOREGRA_FIM") == 0 )
            {
               AV25TFContratoServicosPrazoRegra_Fim = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TFContratoServicosPrazoRegra_Fim", StringUtil.LTrim( StringUtil.Str( AV25TFContratoServicosPrazoRegra_Fim, 14, 5)));
               AV26TFContratoServicosPrazoRegra_Fim_To = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFContratoServicosPrazoRegra_Fim_To", StringUtil.LTrim( StringUtil.Str( AV26TFContratoServicosPrazoRegra_Fim_To, 14, 5)));
               if ( ! (Convert.ToDecimal(0)==AV25TFContratoServicosPrazoRegra_Fim) )
               {
                  Ddo_contratoservicosprazoregra_fim_Filteredtext_set = StringUtil.Str( AV25TFContratoServicosPrazoRegra_Fim, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosprazoregra_fim_Internalname, "FilteredText_set", Ddo_contratoservicosprazoregra_fim_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV26TFContratoServicosPrazoRegra_Fim_To) )
               {
                  Ddo_contratoservicosprazoregra_fim_Filteredtextto_set = StringUtil.Str( AV26TFContratoServicosPrazoRegra_Fim_To, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosprazoregra_fim_Internalname, "FilteredTextTo_set", Ddo_contratoservicosprazoregra_fim_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSPRAZOREGRA_DIAS") == 0 )
            {
               AV29TFContratoServicosPrazoRegra_Dias = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFContratoServicosPrazoRegra_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29TFContratoServicosPrazoRegra_Dias), 3, 0)));
               AV30TFContratoServicosPrazoRegra_Dias_To = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFContratoServicosPrazoRegra_Dias_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30TFContratoServicosPrazoRegra_Dias_To), 3, 0)));
               if ( ! (0==AV29TFContratoServicosPrazoRegra_Dias) )
               {
                  Ddo_contratoservicosprazoregra_dias_Filteredtext_set = StringUtil.Str( (decimal)(AV29TFContratoServicosPrazoRegra_Dias), 3, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosprazoregra_dias_Internalname, "FilteredText_set", Ddo_contratoservicosprazoregra_dias_Filteredtext_set);
               }
               if ( ! (0==AV30TFContratoServicosPrazoRegra_Dias_To) )
               {
                  Ddo_contratoservicosprazoregra_dias_Filteredtextto_set = StringUtil.Str( (decimal)(AV30TFContratoServicosPrazoRegra_Dias_To), 3, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosprazoregra_dias_Internalname, "FilteredTextTo_set", Ddo_contratoservicosprazoregra_dias_Filteredtextto_set);
               }
            }
            AV39GXV1 = (int)(AV39GXV1+1);
         }
      }

      protected void S142( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV15Session.Get(AV38Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV17TFContratoServicosPrazoRegra_Sequencial) && (0==AV18TFContratoServicosPrazoRegra_Sequencial_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV17TFContratoServicosPrazoRegra_Sequencial), 4, 0);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV18TFContratoServicosPrazoRegra_Sequencial_To), 4, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV21TFContratoServicosPrazoRegra_Inicio) && (Convert.ToDecimal(0)==AV22TFContratoServicosPrazoRegra_Inicio_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSPRAZOREGRA_INICIO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV21TFContratoServicosPrazoRegra_Inicio, 14, 5);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV22TFContratoServicosPrazoRegra_Inicio_To, 14, 5);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV25TFContratoServicosPrazoRegra_Fim) && (Convert.ToDecimal(0)==AV26TFContratoServicosPrazoRegra_Fim_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSPRAZOREGRA_FIM";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV25TFContratoServicosPrazoRegra_Fim, 14, 5);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV26TFContratoServicosPrazoRegra_Fim_To, 14, 5);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV29TFContratoServicosPrazoRegra_Dias) && (0==AV30TFContratoServicosPrazoRegra_Dias_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSPRAZOREGRA_DIAS";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV29TFContratoServicosPrazoRegra_Dias), 3, 0);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV30TFContratoServicosPrazoRegra_Dias_To), 3, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV38Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV38Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "ContratoServicosPrazo";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "ContratoServicosPrazo_CntSrvCod";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ContratoServicosPrazo_CntSrvCod), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV15Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_G52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_G52( true) ;
         }
         else
         {
            wb_table2_8_G52( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_G52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_G52e( true) ;
         }
         else
         {
            wb_table1_2_G52e( false) ;
         }
      }

      protected void wb_table2_8_G52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"11\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosPrazoRegra_Sequencial_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosPrazoRegra_Sequencial_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosPrazoRegra_Sequencial_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(94), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosPrazoRegra_Inicio_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosPrazoRegra_Inicio_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosPrazoRegra_Inicio_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(94), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosPrazoRegra_Fim_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosPrazoRegra_Fim_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosPrazoRegra_Fim_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(28), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosPrazoRegra_Dias_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosPrazoRegra_Dias_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosPrazoRegra_Dias_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A906ContratoServicosPrazoRegra_Sequencial), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosPrazoRegra_Sequencial_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoRegra_Sequencial_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A907ContratoServicosPrazoRegra_Inicio, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosPrazoRegra_Inicio_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoRegra_Inicio_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A908ContratoServicosPrazoRegra_Fim, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosPrazoRegra_Fim_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoRegra_Fim_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A909ContratoServicosPrazoRegra_Dias), 3, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosPrazoRegra_Dias_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoRegra_Dias_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 11 )
         {
            wbEnd = 0;
            nRC_GXsfl_11 = (short)(nGXsfl_11_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_G52e( true) ;
         }
         else
         {
            wb_table2_8_G52e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7ContratoServicosPrazo_CntSrvCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicosPrazo_CntSrvCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAG52( ) ;
         WSG52( ) ;
         WEG52( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7ContratoServicosPrazo_CntSrvCod = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAG52( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contratoservicosprazocontratoservicosprazoregrawc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAG52( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7ContratoServicosPrazo_CntSrvCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicosPrazo_CntSrvCod), 6, 0)));
         }
         wcpOAV7ContratoServicosPrazo_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ContratoServicosPrazo_CntSrvCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7ContratoServicosPrazo_CntSrvCod != wcpOAV7ContratoServicosPrazo_CntSrvCod ) ) )
         {
            setjustcreated();
         }
         wcpOAV7ContratoServicosPrazo_CntSrvCod = AV7ContratoServicosPrazo_CntSrvCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7ContratoServicosPrazo_CntSrvCod = cgiGet( sPrefix+"AV7ContratoServicosPrazo_CntSrvCod_CTRL");
         if ( StringUtil.Len( sCtrlAV7ContratoServicosPrazo_CntSrvCod) > 0 )
         {
            AV7ContratoServicosPrazo_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7ContratoServicosPrazo_CntSrvCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicosPrazo_CntSrvCod), 6, 0)));
         }
         else
         {
            AV7ContratoServicosPrazo_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7ContratoServicosPrazo_CntSrvCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAG52( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSG52( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSG52( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7ContratoServicosPrazo_CntSrvCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratoServicosPrazo_CntSrvCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7ContratoServicosPrazo_CntSrvCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7ContratoServicosPrazo_CntSrvCod_CTRL", StringUtil.RTrim( sCtrlAV7ContratoServicosPrazo_CntSrvCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEG52( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311765048");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contratoservicosprazocontratoservicosprazoregrawc.js", "?2020311765048");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_112( )
      {
         edtContratoServicosPrazoRegra_Sequencial_Internalname = sPrefix+"CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_"+sGXsfl_11_idx;
         edtContratoServicosPrazoRegra_Inicio_Internalname = sPrefix+"CONTRATOSERVICOSPRAZOREGRA_INICIO_"+sGXsfl_11_idx;
         edtContratoServicosPrazoRegra_Fim_Internalname = sPrefix+"CONTRATOSERVICOSPRAZOREGRA_FIM_"+sGXsfl_11_idx;
         edtContratoServicosPrazoRegra_Dias_Internalname = sPrefix+"CONTRATOSERVICOSPRAZOREGRA_DIAS_"+sGXsfl_11_idx;
      }

      protected void SubsflControlProps_fel_112( )
      {
         edtContratoServicosPrazoRegra_Sequencial_Internalname = sPrefix+"CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_"+sGXsfl_11_fel_idx;
         edtContratoServicosPrazoRegra_Inicio_Internalname = sPrefix+"CONTRATOSERVICOSPRAZOREGRA_INICIO_"+sGXsfl_11_fel_idx;
         edtContratoServicosPrazoRegra_Fim_Internalname = sPrefix+"CONTRATOSERVICOSPRAZOREGRA_FIM_"+sGXsfl_11_fel_idx;
         edtContratoServicosPrazoRegra_Dias_Internalname = sPrefix+"CONTRATOSERVICOSPRAZOREGRA_DIAS_"+sGXsfl_11_fel_idx;
      }

      protected void sendrow_112( )
      {
         SubsflControlProps_112( ) ;
         WBG50( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_11_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_11_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_11_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosPrazoRegra_Sequencial_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A906ContratoServicosPrazoRegra_Sequencial), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A906ContratoServicosPrazoRegra_Sequencial), "ZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosPrazoRegra_Sequencial_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)11,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosPrazoRegra_Inicio_Internalname,StringUtil.LTrim( StringUtil.NToC( A907ContratoServicosPrazoRegra_Inicio, 14, 5, ",", "")),context.localUtil.Format( A907ContratoServicosPrazoRegra_Inicio, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosPrazoRegra_Inicio_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)94,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)11,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosPrazoRegra_Fim_Internalname,StringUtil.LTrim( StringUtil.NToC( A908ContratoServicosPrazoRegra_Fim, 14, 5, ",", "")),context.localUtil.Format( A908ContratoServicosPrazoRegra_Fim, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosPrazoRegra_Fim_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)94,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)11,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosPrazoRegra_Dias_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A909ContratoServicosPrazoRegra_Dias), 3, 0, ",", "")),context.localUtil.Format( (decimal)(A909ContratoServicosPrazoRegra_Dias), "ZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosPrazoRegra_Dias_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)28,(String)"px",(short)17,(String)"px",(short)3,(short)0,(short)0,(short)11,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL"+"_"+sGXsfl_11_idx, GetSecureSignedToken( sPrefix+sGXsfl_11_idx, context.localUtil.Format( (decimal)(A906ContratoServicosPrazoRegra_Sequencial), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSPRAZOREGRA_INICIO"+"_"+sGXsfl_11_idx, GetSecureSignedToken( sPrefix+sGXsfl_11_idx, context.localUtil.Format( A907ContratoServicosPrazoRegra_Inicio, "ZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSPRAZOREGRA_FIM"+"_"+sGXsfl_11_idx, GetSecureSignedToken( sPrefix+sGXsfl_11_idx, context.localUtil.Format( A908ContratoServicosPrazoRegra_Fim, "ZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSPRAZOREGRA_DIAS"+"_"+sGXsfl_11_idx, GetSecureSignedToken( sPrefix+sGXsfl_11_idx, context.localUtil.Format( (decimal)(A909ContratoServicosPrazoRegra_Dias), "ZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_11_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_11_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_11_idx+1));
            sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
            SubsflControlProps_112( ) ;
         }
         /* End function sendrow_112 */
      }

      protected void init_default_properties( )
      {
         edtContratoServicosPrazoRegra_Sequencial_Internalname = sPrefix+"CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL";
         edtContratoServicosPrazoRegra_Inicio_Internalname = sPrefix+"CONTRATOSERVICOSPRAZOREGRA_INICIO";
         edtContratoServicosPrazoRegra_Fim_Internalname = sPrefix+"CONTRATOSERVICOSPRAZOREGRA_FIM";
         edtContratoServicosPrazoRegra_Dias_Internalname = sPrefix+"CONTRATOSERVICOSPRAZOREGRA_DIAS";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         edtContratoServicosPrazo_CntSrvCod_Internalname = sPrefix+"CONTRATOSERVICOSPRAZO_CNTSRVCOD";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         edtavTfcontratoservicosprazoregra_sequencial_Internalname = sPrefix+"vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL";
         edtavTfcontratoservicosprazoregra_sequencial_to_Internalname = sPrefix+"vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_TO";
         edtavTfcontratoservicosprazoregra_inicio_Internalname = sPrefix+"vTFCONTRATOSERVICOSPRAZOREGRA_INICIO";
         edtavTfcontratoservicosprazoregra_inicio_to_Internalname = sPrefix+"vTFCONTRATOSERVICOSPRAZOREGRA_INICIO_TO";
         edtavTfcontratoservicosprazoregra_fim_Internalname = sPrefix+"vTFCONTRATOSERVICOSPRAZOREGRA_FIM";
         edtavTfcontratoservicosprazoregra_fim_to_Internalname = sPrefix+"vTFCONTRATOSERVICOSPRAZOREGRA_FIM_TO";
         edtavTfcontratoservicosprazoregra_dias_Internalname = sPrefix+"vTFCONTRATOSERVICOSPRAZOREGRA_DIAS";
         edtavTfcontratoservicosprazoregra_dias_to_Internalname = sPrefix+"vTFCONTRATOSERVICOSPRAZOREGRA_DIAS_TO";
         Ddo_contratoservicosprazoregra_sequencial_Internalname = sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL";
         edtavDdo_contratoservicosprazoregra_sequencialtitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIALTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicosprazoregra_inicio_Internalname = sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO";
         edtavDdo_contratoservicosprazoregra_iniciotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOSERVICOSPRAZOREGRA_INICIOTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicosprazoregra_fim_Internalname = sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIM";
         edtavDdo_contratoservicosprazoregra_fimtitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOSERVICOSPRAZOREGRA_FIMTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicosprazoregra_dias_Internalname = sPrefix+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS";
         edtavDdo_contratoservicosprazoregra_diastitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOSERVICOSPRAZOREGRA_DIASTITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtContratoServicosPrazoRegra_Dias_Jsonclick = "";
         edtContratoServicosPrazoRegra_Fim_Jsonclick = "";
         edtContratoServicosPrazoRegra_Inicio_Jsonclick = "";
         edtContratoServicosPrazoRegra_Sequencial_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtContratoServicosPrazoRegra_Dias_Titleformat = 0;
         edtContratoServicosPrazoRegra_Fim_Titleformat = 0;
         edtContratoServicosPrazoRegra_Inicio_Titleformat = 0;
         edtContratoServicosPrazoRegra_Sequencial_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtContratoServicosPrazoRegra_Dias_Title = "Dias";
         edtContratoServicosPrazoRegra_Fim_Title = "Fim";
         edtContratoServicosPrazoRegra_Inicio_Title = "Inicio";
         edtContratoServicosPrazoRegra_Sequencial_Title = "Sequencial";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_contratoservicosprazoregra_diastitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicosprazoregra_fimtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicosprazoregra_iniciotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicosprazoregra_sequencialtitlecontrolidtoreplace_Visible = 1;
         edtavTfcontratoservicosprazoregra_dias_to_Jsonclick = "";
         edtavTfcontratoservicosprazoregra_dias_to_Visible = 1;
         edtavTfcontratoservicosprazoregra_dias_Jsonclick = "";
         edtavTfcontratoservicosprazoregra_dias_Visible = 1;
         edtavTfcontratoservicosprazoregra_fim_to_Jsonclick = "";
         edtavTfcontratoservicosprazoregra_fim_to_Visible = 1;
         edtavTfcontratoservicosprazoregra_fim_Jsonclick = "";
         edtavTfcontratoservicosprazoregra_fim_Visible = 1;
         edtavTfcontratoservicosprazoregra_inicio_to_Jsonclick = "";
         edtavTfcontratoservicosprazoregra_inicio_to_Visible = 1;
         edtavTfcontratoservicosprazoregra_inicio_Jsonclick = "";
         edtavTfcontratoservicosprazoregra_inicio_Visible = 1;
         edtavTfcontratoservicosprazoregra_sequencial_to_Jsonclick = "";
         edtavTfcontratoservicosprazoregra_sequencial_to_Visible = 1;
         edtavTfcontratoservicosprazoregra_sequencial_Jsonclick = "";
         edtavTfcontratoservicosprazoregra_sequencial_Visible = 1;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         edtavOrderedby_Jsonclick = "";
         edtavOrderedby_Visible = 1;
         edtContratoServicosPrazo_CntSrvCod_Jsonclick = "";
         edtContratoServicosPrazo_CntSrvCod_Visible = 1;
         Ddo_contratoservicosprazoregra_dias_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosprazoregra_dias_Rangefilterto = "At�";
         Ddo_contratoservicosprazoregra_dias_Rangefilterfrom = "Desde";
         Ddo_contratoservicosprazoregra_dias_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosprazoregra_dias_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosprazoregra_dias_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosprazoregra_dias_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoservicosprazoregra_dias_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazoregra_dias_Filtertype = "Numeric";
         Ddo_contratoservicosprazoregra_dias_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazoregra_dias_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazoregra_dias_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazoregra_dias_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosprazoregra_dias_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosprazoregra_dias_Cls = "ColumnSettings";
         Ddo_contratoservicosprazoregra_dias_Tooltip = "Op��es";
         Ddo_contratoservicosprazoregra_dias_Caption = "";
         Ddo_contratoservicosprazoregra_fim_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosprazoregra_fim_Rangefilterto = "At�";
         Ddo_contratoservicosprazoregra_fim_Rangefilterfrom = "Desde";
         Ddo_contratoservicosprazoregra_fim_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosprazoregra_fim_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosprazoregra_fim_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosprazoregra_fim_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoservicosprazoregra_fim_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazoregra_fim_Filtertype = "Numeric";
         Ddo_contratoservicosprazoregra_fim_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazoregra_fim_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazoregra_fim_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazoregra_fim_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosprazoregra_fim_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosprazoregra_fim_Cls = "ColumnSettings";
         Ddo_contratoservicosprazoregra_fim_Tooltip = "Op��es";
         Ddo_contratoservicosprazoregra_fim_Caption = "";
         Ddo_contratoservicosprazoregra_inicio_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosprazoregra_inicio_Rangefilterto = "At�";
         Ddo_contratoservicosprazoregra_inicio_Rangefilterfrom = "Desde";
         Ddo_contratoservicosprazoregra_inicio_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosprazoregra_inicio_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosprazoregra_inicio_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosprazoregra_inicio_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoservicosprazoregra_inicio_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazoregra_inicio_Filtertype = "Numeric";
         Ddo_contratoservicosprazoregra_inicio_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazoregra_inicio_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazoregra_inicio_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazoregra_inicio_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosprazoregra_inicio_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosprazoregra_inicio_Cls = "ColumnSettings";
         Ddo_contratoservicosprazoregra_inicio_Tooltip = "Op��es";
         Ddo_contratoservicosprazoregra_inicio_Caption = "";
         Ddo_contratoservicosprazoregra_sequencial_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosprazoregra_sequencial_Rangefilterto = "At�";
         Ddo_contratoservicosprazoregra_sequencial_Rangefilterfrom = "Desde";
         Ddo_contratoservicosprazoregra_sequencial_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosprazoregra_sequencial_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosprazoregra_sequencial_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosprazoregra_sequencial_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoservicosprazoregra_sequencial_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazoregra_sequencial_Filtertype = "Numeric";
         Ddo_contratoservicosprazoregra_sequencial_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazoregra_sequencial_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazoregra_sequencial_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazoregra_sequencial_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosprazoregra_sequencial_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosprazoregra_sequencial_Cls = "ColumnSettings";
         Ddo_contratoservicosprazoregra_sequencial_Tooltip = "Op��es";
         Ddo_contratoservicosprazoregra_sequencial_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV7ContratoServicosPrazo_CntSrvCod',fld:'vCONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV19ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV23ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFContratoServicosPrazoRegra_Sequencial',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',pic:'ZZZ9',nv:0},{av:'AV18TFContratoServicosPrazoRegra_Sequencial_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_TO',pic:'ZZZ9',nv:0},{av:'AV21TFContratoServicosPrazoRegra_Inicio',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV22TFContratoServicosPrazoRegra_Inicio_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV25TFContratoServicosPrazoRegra_Fim',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV26TFContratoServicosPrazoRegra_Fim_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV29TFContratoServicosPrazoRegra_Dias',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS',pic:'ZZ9',nv:0},{av:'AV30TFContratoServicosPrazoRegra_Dias_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS_TO',pic:'ZZ9',nv:0}],oparms:[{av:'AV16ContratoServicosPrazoRegra_SequencialTitleFilterData',fld:'vCONTRATOSERVICOSPRAZOREGRA_SEQUENCIALTITLEFILTERDATA',pic:'',nv:null},{av:'AV20ContratoServicosPrazoRegra_InicioTitleFilterData',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIOTITLEFILTERDATA',pic:'',nv:null},{av:'AV24ContratoServicosPrazoRegra_FimTitleFilterData',fld:'vCONTRATOSERVICOSPRAZOREGRA_FIMTITLEFILTERDATA',pic:'',nv:null},{av:'AV28ContratoServicosPrazoRegra_DiasTitleFilterData',fld:'vCONTRATOSERVICOSPRAZOREGRA_DIASTITLEFILTERDATA',pic:'',nv:null},{av:'edtContratoServicosPrazoRegra_Sequencial_Titleformat',ctrl:'CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',prop:'Titleformat'},{av:'edtContratoServicosPrazoRegra_Sequencial_Title',ctrl:'CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',prop:'Title'},{av:'edtContratoServicosPrazoRegra_Inicio_Titleformat',ctrl:'CONTRATOSERVICOSPRAZOREGRA_INICIO',prop:'Titleformat'},{av:'edtContratoServicosPrazoRegra_Inicio_Title',ctrl:'CONTRATOSERVICOSPRAZOREGRA_INICIO',prop:'Title'},{av:'edtContratoServicosPrazoRegra_Fim_Titleformat',ctrl:'CONTRATOSERVICOSPRAZOREGRA_FIM',prop:'Titleformat'},{av:'edtContratoServicosPrazoRegra_Fim_Title',ctrl:'CONTRATOSERVICOSPRAZOREGRA_FIM',prop:'Title'},{av:'edtContratoServicosPrazoRegra_Dias_Titleformat',ctrl:'CONTRATOSERVICOSPRAZOREGRA_DIAS',prop:'Titleformat'},{av:'edtContratoServicosPrazoRegra_Dias_Title',ctrl:'CONTRATOSERVICOSPRAZOREGRA_DIAS',prop:'Title'},{av:'AV34GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV35GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11G52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFContratoServicosPrazoRegra_Sequencial',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',pic:'ZZZ9',nv:0},{av:'AV18TFContratoServicosPrazoRegra_Sequencial_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_TO',pic:'ZZZ9',nv:0},{av:'AV21TFContratoServicosPrazoRegra_Inicio',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV22TFContratoServicosPrazoRegra_Inicio_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV25TFContratoServicosPrazoRegra_Fim',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV26TFContratoServicosPrazoRegra_Fim_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV29TFContratoServicosPrazoRegra_Dias',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS',pic:'ZZ9',nv:0},{av:'AV30TFContratoServicosPrazoRegra_Dias_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS_TO',pic:'ZZ9',nv:0},{av:'AV7ContratoServicosPrazo_CntSrvCod',fld:'vCONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV19ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV23ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL.ONOPTIONCLICKED","{handler:'E12G52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFContratoServicosPrazoRegra_Sequencial',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',pic:'ZZZ9',nv:0},{av:'AV18TFContratoServicosPrazoRegra_Sequencial_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_TO',pic:'ZZZ9',nv:0},{av:'AV21TFContratoServicosPrazoRegra_Inicio',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV22TFContratoServicosPrazoRegra_Inicio_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV25TFContratoServicosPrazoRegra_Fim',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV26TFContratoServicosPrazoRegra_Fim_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV29TFContratoServicosPrazoRegra_Dias',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS',pic:'ZZ9',nv:0},{av:'AV30TFContratoServicosPrazoRegra_Dias_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS_TO',pic:'ZZ9',nv:0},{av:'AV7ContratoServicosPrazo_CntSrvCod',fld:'vCONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV19ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV23ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_contratoservicosprazoregra_sequencial_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosprazoregra_sequencial_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',prop:'FilteredText_get'},{av:'Ddo_contratoservicosprazoregra_sequencial_Filteredtextto_get',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosprazoregra_sequencial_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',prop:'SortedStatus'},{av:'AV17TFContratoServicosPrazoRegra_Sequencial',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',pic:'ZZZ9',nv:0},{av:'AV18TFContratoServicosPrazoRegra_Sequencial_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_TO',pic:'ZZZ9',nv:0},{av:'Ddo_contratoservicosprazoregra_inicio_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO',prop:'SortedStatus'},{av:'Ddo_contratoservicosprazoregra_fim_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_FIM',prop:'SortedStatus'},{av:'Ddo_contratoservicosprazoregra_dias_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO.ONOPTIONCLICKED","{handler:'E13G52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFContratoServicosPrazoRegra_Sequencial',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',pic:'ZZZ9',nv:0},{av:'AV18TFContratoServicosPrazoRegra_Sequencial_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_TO',pic:'ZZZ9',nv:0},{av:'AV21TFContratoServicosPrazoRegra_Inicio',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV22TFContratoServicosPrazoRegra_Inicio_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV25TFContratoServicosPrazoRegra_Fim',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV26TFContratoServicosPrazoRegra_Fim_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV29TFContratoServicosPrazoRegra_Dias',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS',pic:'ZZ9',nv:0},{av:'AV30TFContratoServicosPrazoRegra_Dias_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS_TO',pic:'ZZ9',nv:0},{av:'AV7ContratoServicosPrazo_CntSrvCod',fld:'vCONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV19ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV23ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_contratoservicosprazoregra_inicio_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosprazoregra_inicio_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO',prop:'FilteredText_get'},{av:'Ddo_contratoservicosprazoregra_inicio_Filteredtextto_get',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosprazoregra_inicio_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO',prop:'SortedStatus'},{av:'AV21TFContratoServicosPrazoRegra_Inicio',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV22TFContratoServicosPrazoRegra_Inicio_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_contratoservicosprazoregra_sequencial_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',prop:'SortedStatus'},{av:'Ddo_contratoservicosprazoregra_fim_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_FIM',prop:'SortedStatus'},{av:'Ddo_contratoservicosprazoregra_dias_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSPRAZOREGRA_FIM.ONOPTIONCLICKED","{handler:'E14G52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFContratoServicosPrazoRegra_Sequencial',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',pic:'ZZZ9',nv:0},{av:'AV18TFContratoServicosPrazoRegra_Sequencial_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_TO',pic:'ZZZ9',nv:0},{av:'AV21TFContratoServicosPrazoRegra_Inicio',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV22TFContratoServicosPrazoRegra_Inicio_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV25TFContratoServicosPrazoRegra_Fim',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV26TFContratoServicosPrazoRegra_Fim_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV29TFContratoServicosPrazoRegra_Dias',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS',pic:'ZZ9',nv:0},{av:'AV30TFContratoServicosPrazoRegra_Dias_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS_TO',pic:'ZZ9',nv:0},{av:'AV7ContratoServicosPrazo_CntSrvCod',fld:'vCONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV19ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV23ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_contratoservicosprazoregra_fim_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_FIM',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosprazoregra_fim_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_FIM',prop:'FilteredText_get'},{av:'Ddo_contratoservicosprazoregra_fim_Filteredtextto_get',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_FIM',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosprazoregra_fim_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_FIM',prop:'SortedStatus'},{av:'AV25TFContratoServicosPrazoRegra_Fim',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV26TFContratoServicosPrazoRegra_Fim_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_contratoservicosprazoregra_sequencial_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',prop:'SortedStatus'},{av:'Ddo_contratoservicosprazoregra_inicio_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO',prop:'SortedStatus'},{av:'Ddo_contratoservicosprazoregra_dias_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS.ONOPTIONCLICKED","{handler:'E15G52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFContratoServicosPrazoRegra_Sequencial',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',pic:'ZZZ9',nv:0},{av:'AV18TFContratoServicosPrazoRegra_Sequencial_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_TO',pic:'ZZZ9',nv:0},{av:'AV21TFContratoServicosPrazoRegra_Inicio',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV22TFContratoServicosPrazoRegra_Inicio_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV25TFContratoServicosPrazoRegra_Fim',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV26TFContratoServicosPrazoRegra_Fim_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV29TFContratoServicosPrazoRegra_Dias',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS',pic:'ZZ9',nv:0},{av:'AV30TFContratoServicosPrazoRegra_Dias_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS_TO',pic:'ZZ9',nv:0},{av:'AV7ContratoServicosPrazo_CntSrvCod',fld:'vCONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV19ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV23ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_contratoservicosprazoregra_dias_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosprazoregra_dias_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS',prop:'FilteredText_get'},{av:'Ddo_contratoservicosprazoregra_dias_Filteredtextto_get',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosprazoregra_dias_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS',prop:'SortedStatus'},{av:'AV29TFContratoServicosPrazoRegra_Dias',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS',pic:'ZZ9',nv:0},{av:'AV30TFContratoServicosPrazoRegra_Dias_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS_TO',pic:'ZZ9',nv:0},{av:'Ddo_contratoservicosprazoregra_sequencial_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',prop:'SortedStatus'},{av:'Ddo_contratoservicosprazoregra_inicio_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO',prop:'SortedStatus'},{av:'Ddo_contratoservicosprazoregra_fim_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_FIM',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E18G52',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_contratoservicosprazoregra_sequencial_Activeeventkey = "";
         Ddo_contratoservicosprazoregra_sequencial_Filteredtext_get = "";
         Ddo_contratoservicosprazoregra_sequencial_Filteredtextto_get = "";
         Ddo_contratoservicosprazoregra_inicio_Activeeventkey = "";
         Ddo_contratoservicosprazoregra_inicio_Filteredtext_get = "";
         Ddo_contratoservicosprazoregra_inicio_Filteredtextto_get = "";
         Ddo_contratoservicosprazoregra_fim_Activeeventkey = "";
         Ddo_contratoservicosprazoregra_fim_Filteredtext_get = "";
         Ddo_contratoservicosprazoregra_fim_Filteredtextto_get = "";
         Ddo_contratoservicosprazoregra_dias_Activeeventkey = "";
         Ddo_contratoservicosprazoregra_dias_Filteredtext_get = "";
         Ddo_contratoservicosprazoregra_dias_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV19ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace = "";
         AV23ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace = "";
         AV27ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace = "";
         AV31ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace = "";
         AV38Pgmname = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV32DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV16ContratoServicosPrazoRegra_SequencialTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV20ContratoServicosPrazoRegra_InicioTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV24ContratoServicosPrazoRegra_FimTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV28ContratoServicosPrazoRegra_DiasTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_contratoservicosprazoregra_sequencial_Filteredtext_set = "";
         Ddo_contratoservicosprazoregra_sequencial_Filteredtextto_set = "";
         Ddo_contratoservicosprazoregra_sequencial_Sortedstatus = "";
         Ddo_contratoservicosprazoregra_inicio_Filteredtext_set = "";
         Ddo_contratoservicosprazoregra_inicio_Filteredtextto_set = "";
         Ddo_contratoservicosprazoregra_inicio_Sortedstatus = "";
         Ddo_contratoservicosprazoregra_fim_Filteredtext_set = "";
         Ddo_contratoservicosprazoregra_fim_Filteredtextto_set = "";
         Ddo_contratoservicosprazoregra_fim_Sortedstatus = "";
         Ddo_contratoservicosprazoregra_dias_Filteredtext_set = "";
         Ddo_contratoservicosprazoregra_dias_Filteredtextto_set = "";
         Ddo_contratoservicosprazoregra_dias_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         H00G52_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         H00G52_A909ContratoServicosPrazoRegra_Dias = new short[1] ;
         H00G52_A908ContratoServicosPrazoRegra_Fim = new decimal[1] ;
         H00G52_A907ContratoServicosPrazoRegra_Inicio = new decimal[1] ;
         H00G52_A906ContratoServicosPrazoRegra_Sequencial = new short[1] ;
         H00G53_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV15Session = context.GetSession();
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7ContratoServicosPrazo_CntSrvCod = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratoservicosprazocontratoservicosprazoregrawc__default(),
            new Object[][] {
                new Object[] {
               H00G52_A903ContratoServicosPrazo_CntSrvCod, H00G52_A909ContratoServicosPrazoRegra_Dias, H00G52_A908ContratoServicosPrazoRegra_Fim, H00G52_A907ContratoServicosPrazoRegra_Inicio, H00G52_A906ContratoServicosPrazoRegra_Sequencial
               }
               , new Object[] {
               H00G53_AGRID_nRecordCount
               }
            }
         );
         AV38Pgmname = "ContratoServicosPrazoContratoServicosPrazoRegraWC";
         /* GeneXus formulas. */
         AV38Pgmname = "ContratoServicosPrazoContratoServicosPrazoRegraWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_11 ;
      private short nGXsfl_11_idx=1 ;
      private short AV13OrderedBy ;
      private short AV17TFContratoServicosPrazoRegra_Sequencial ;
      private short AV18TFContratoServicosPrazoRegra_Sequencial_To ;
      private short AV29TFContratoServicosPrazoRegra_Dias ;
      private short AV30TFContratoServicosPrazoRegra_Dias_To ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short A906ContratoServicosPrazoRegra_Sequencial ;
      private short A909ContratoServicosPrazoRegra_Dias ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_11_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContratoServicosPrazoRegra_Sequencial_Titleformat ;
      private short edtContratoServicosPrazoRegra_Inicio_Titleformat ;
      private short edtContratoServicosPrazoRegra_Fim_Titleformat ;
      private short edtContratoServicosPrazoRegra_Dias_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7ContratoServicosPrazo_CntSrvCod ;
      private int wcpOAV7ContratoServicosPrazo_CntSrvCod ;
      private int subGrid_Rows ;
      private int Gridpaginationbar_Pagestoshow ;
      private int A903ContratoServicosPrazo_CntSrvCod ;
      private int edtContratoServicosPrazo_CntSrvCod_Visible ;
      private int edtavOrderedby_Visible ;
      private int edtavOrdereddsc_Visible ;
      private int edtavTfcontratoservicosprazoregra_sequencial_Visible ;
      private int edtavTfcontratoservicosprazoregra_sequencial_to_Visible ;
      private int edtavTfcontratoservicosprazoregra_inicio_Visible ;
      private int edtavTfcontratoservicosprazoregra_inicio_to_Visible ;
      private int edtavTfcontratoservicosprazoregra_fim_Visible ;
      private int edtavTfcontratoservicosprazoregra_fim_to_Visible ;
      private int edtavTfcontratoservicosprazoregra_dias_Visible ;
      private int edtavTfcontratoservicosprazoregra_dias_to_Visible ;
      private int edtavDdo_contratoservicosprazoregra_sequencialtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicosprazoregra_iniciotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicosprazoregra_fimtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicosprazoregra_diastitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV33PageToGo ;
      private int AV39GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV34GridCurrentPage ;
      private long AV35GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV21TFContratoServicosPrazoRegra_Inicio ;
      private decimal AV22TFContratoServicosPrazoRegra_Inicio_To ;
      private decimal AV25TFContratoServicosPrazoRegra_Fim ;
      private decimal AV26TFContratoServicosPrazoRegra_Fim_To ;
      private decimal A907ContratoServicosPrazoRegra_Inicio ;
      private decimal A908ContratoServicosPrazoRegra_Fim ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contratoservicosprazoregra_sequencial_Activeeventkey ;
      private String Ddo_contratoservicosprazoregra_sequencial_Filteredtext_get ;
      private String Ddo_contratoservicosprazoregra_sequencial_Filteredtextto_get ;
      private String Ddo_contratoservicosprazoregra_inicio_Activeeventkey ;
      private String Ddo_contratoservicosprazoregra_inicio_Filteredtext_get ;
      private String Ddo_contratoservicosprazoregra_inicio_Filteredtextto_get ;
      private String Ddo_contratoservicosprazoregra_fim_Activeeventkey ;
      private String Ddo_contratoservicosprazoregra_fim_Filteredtext_get ;
      private String Ddo_contratoservicosprazoregra_fim_Filteredtextto_get ;
      private String Ddo_contratoservicosprazoregra_dias_Activeeventkey ;
      private String Ddo_contratoservicosprazoregra_dias_Filteredtext_get ;
      private String Ddo_contratoservicosprazoregra_dias_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_11_idx="0001" ;
      private String AV38Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contratoservicosprazoregra_sequencial_Caption ;
      private String Ddo_contratoservicosprazoregra_sequencial_Tooltip ;
      private String Ddo_contratoservicosprazoregra_sequencial_Cls ;
      private String Ddo_contratoservicosprazoregra_sequencial_Filteredtext_set ;
      private String Ddo_contratoservicosprazoregra_sequencial_Filteredtextto_set ;
      private String Ddo_contratoservicosprazoregra_sequencial_Dropdownoptionstype ;
      private String Ddo_contratoservicosprazoregra_sequencial_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosprazoregra_sequencial_Sortedstatus ;
      private String Ddo_contratoservicosprazoregra_sequencial_Filtertype ;
      private String Ddo_contratoservicosprazoregra_sequencial_Sortasc ;
      private String Ddo_contratoservicosprazoregra_sequencial_Sortdsc ;
      private String Ddo_contratoservicosprazoregra_sequencial_Cleanfilter ;
      private String Ddo_contratoservicosprazoregra_sequencial_Rangefilterfrom ;
      private String Ddo_contratoservicosprazoregra_sequencial_Rangefilterto ;
      private String Ddo_contratoservicosprazoregra_sequencial_Searchbuttontext ;
      private String Ddo_contratoservicosprazoregra_inicio_Caption ;
      private String Ddo_contratoservicosprazoregra_inicio_Tooltip ;
      private String Ddo_contratoservicosprazoregra_inicio_Cls ;
      private String Ddo_contratoservicosprazoregra_inicio_Filteredtext_set ;
      private String Ddo_contratoservicosprazoregra_inicio_Filteredtextto_set ;
      private String Ddo_contratoservicosprazoregra_inicio_Dropdownoptionstype ;
      private String Ddo_contratoservicosprazoregra_inicio_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosprazoregra_inicio_Sortedstatus ;
      private String Ddo_contratoservicosprazoregra_inicio_Filtertype ;
      private String Ddo_contratoservicosprazoregra_inicio_Sortasc ;
      private String Ddo_contratoservicosprazoregra_inicio_Sortdsc ;
      private String Ddo_contratoservicosprazoregra_inicio_Cleanfilter ;
      private String Ddo_contratoservicosprazoregra_inicio_Rangefilterfrom ;
      private String Ddo_contratoservicosprazoregra_inicio_Rangefilterto ;
      private String Ddo_contratoservicosprazoregra_inicio_Searchbuttontext ;
      private String Ddo_contratoservicosprazoregra_fim_Caption ;
      private String Ddo_contratoservicosprazoregra_fim_Tooltip ;
      private String Ddo_contratoservicosprazoregra_fim_Cls ;
      private String Ddo_contratoservicosprazoregra_fim_Filteredtext_set ;
      private String Ddo_contratoservicosprazoregra_fim_Filteredtextto_set ;
      private String Ddo_contratoservicosprazoregra_fim_Dropdownoptionstype ;
      private String Ddo_contratoservicosprazoregra_fim_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosprazoregra_fim_Sortedstatus ;
      private String Ddo_contratoservicosprazoregra_fim_Filtertype ;
      private String Ddo_contratoservicosprazoregra_fim_Sortasc ;
      private String Ddo_contratoservicosprazoregra_fim_Sortdsc ;
      private String Ddo_contratoservicosprazoregra_fim_Cleanfilter ;
      private String Ddo_contratoservicosprazoregra_fim_Rangefilterfrom ;
      private String Ddo_contratoservicosprazoregra_fim_Rangefilterto ;
      private String Ddo_contratoservicosprazoregra_fim_Searchbuttontext ;
      private String Ddo_contratoservicosprazoregra_dias_Caption ;
      private String Ddo_contratoservicosprazoregra_dias_Tooltip ;
      private String Ddo_contratoservicosprazoregra_dias_Cls ;
      private String Ddo_contratoservicosprazoregra_dias_Filteredtext_set ;
      private String Ddo_contratoservicosprazoregra_dias_Filteredtextto_set ;
      private String Ddo_contratoservicosprazoregra_dias_Dropdownoptionstype ;
      private String Ddo_contratoservicosprazoregra_dias_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosprazoregra_dias_Sortedstatus ;
      private String Ddo_contratoservicosprazoregra_dias_Filtertype ;
      private String Ddo_contratoservicosprazoregra_dias_Sortasc ;
      private String Ddo_contratoservicosprazoregra_dias_Sortdsc ;
      private String Ddo_contratoservicosprazoregra_dias_Cleanfilter ;
      private String Ddo_contratoservicosprazoregra_dias_Rangefilterfrom ;
      private String Ddo_contratoservicosprazoregra_dias_Rangefilterto ;
      private String Ddo_contratoservicosprazoregra_dias_Searchbuttontext ;
      private String GX_FocusControl ;
      private String edtContratoServicosPrazo_CntSrvCod_Internalname ;
      private String edtContratoServicosPrazo_CntSrvCod_Jsonclick ;
      private String TempTags ;
      private String edtavOrderedby_Internalname ;
      private String edtavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String edtavTfcontratoservicosprazoregra_sequencial_Internalname ;
      private String edtavTfcontratoservicosprazoregra_sequencial_Jsonclick ;
      private String edtavTfcontratoservicosprazoregra_sequencial_to_Internalname ;
      private String edtavTfcontratoservicosprazoregra_sequencial_to_Jsonclick ;
      private String edtavTfcontratoservicosprazoregra_inicio_Internalname ;
      private String edtavTfcontratoservicosprazoregra_inicio_Jsonclick ;
      private String edtavTfcontratoservicosprazoregra_inicio_to_Internalname ;
      private String edtavTfcontratoservicosprazoregra_inicio_to_Jsonclick ;
      private String edtavTfcontratoservicosprazoregra_fim_Internalname ;
      private String edtavTfcontratoservicosprazoregra_fim_Jsonclick ;
      private String edtavTfcontratoservicosprazoregra_fim_to_Internalname ;
      private String edtavTfcontratoservicosprazoregra_fim_to_Jsonclick ;
      private String edtavTfcontratoservicosprazoregra_dias_Internalname ;
      private String edtavTfcontratoservicosprazoregra_dias_Jsonclick ;
      private String edtavTfcontratoservicosprazoregra_dias_to_Internalname ;
      private String edtavTfcontratoservicosprazoregra_dias_to_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_contratoservicosprazoregra_sequencialtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicosprazoregra_iniciotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicosprazoregra_fimtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicosprazoregra_diastitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtContratoServicosPrazoRegra_Sequencial_Internalname ;
      private String edtContratoServicosPrazoRegra_Inicio_Internalname ;
      private String edtContratoServicosPrazoRegra_Fim_Internalname ;
      private String edtContratoServicosPrazoRegra_Dias_Internalname ;
      private String scmdbuf ;
      private String subGrid_Internalname ;
      private String Ddo_contratoservicosprazoregra_sequencial_Internalname ;
      private String Ddo_contratoservicosprazoregra_inicio_Internalname ;
      private String Ddo_contratoservicosprazoregra_fim_Internalname ;
      private String Ddo_contratoservicosprazoregra_dias_Internalname ;
      private String edtContratoServicosPrazoRegra_Sequencial_Title ;
      private String edtContratoServicosPrazoRegra_Inicio_Title ;
      private String edtContratoServicosPrazoRegra_Fim_Title ;
      private String edtContratoServicosPrazoRegra_Dias_Title ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String sCtrlAV7ContratoServicosPrazo_CntSrvCod ;
      private String sGXsfl_11_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContratoServicosPrazoRegra_Sequencial_Jsonclick ;
      private String edtContratoServicosPrazoRegra_Inicio_Jsonclick ;
      private String edtContratoServicosPrazoRegra_Fim_Jsonclick ;
      private String edtContratoServicosPrazoRegra_Dias_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contratoservicosprazoregra_sequencial_Includesortasc ;
      private bool Ddo_contratoservicosprazoregra_sequencial_Includesortdsc ;
      private bool Ddo_contratoservicosprazoregra_sequencial_Includefilter ;
      private bool Ddo_contratoservicosprazoregra_sequencial_Filterisrange ;
      private bool Ddo_contratoservicosprazoregra_sequencial_Includedatalist ;
      private bool Ddo_contratoservicosprazoregra_inicio_Includesortasc ;
      private bool Ddo_contratoservicosprazoregra_inicio_Includesortdsc ;
      private bool Ddo_contratoservicosprazoregra_inicio_Includefilter ;
      private bool Ddo_contratoservicosprazoregra_inicio_Filterisrange ;
      private bool Ddo_contratoservicosprazoregra_inicio_Includedatalist ;
      private bool Ddo_contratoservicosprazoregra_fim_Includesortasc ;
      private bool Ddo_contratoservicosprazoregra_fim_Includesortdsc ;
      private bool Ddo_contratoservicosprazoregra_fim_Includefilter ;
      private bool Ddo_contratoservicosprazoregra_fim_Filterisrange ;
      private bool Ddo_contratoservicosprazoregra_fim_Includedatalist ;
      private bool Ddo_contratoservicosprazoregra_dias_Includesortasc ;
      private bool Ddo_contratoservicosprazoregra_dias_Includesortdsc ;
      private bool Ddo_contratoservicosprazoregra_dias_Includefilter ;
      private bool Ddo_contratoservicosprazoregra_dias_Filterisrange ;
      private bool Ddo_contratoservicosprazoregra_dias_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private String AV19ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace ;
      private String AV23ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace ;
      private String AV27ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace ;
      private String AV31ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace ;
      private IGxSession AV15Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00G52_A903ContratoServicosPrazo_CntSrvCod ;
      private short[] H00G52_A909ContratoServicosPrazoRegra_Dias ;
      private decimal[] H00G52_A908ContratoServicosPrazoRegra_Fim ;
      private decimal[] H00G52_A907ContratoServicosPrazoRegra_Inicio ;
      private short[] H00G52_A906ContratoServicosPrazoRegra_Sequencial ;
      private long[] H00G53_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV16ContratoServicosPrazoRegra_SequencialTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV20ContratoServicosPrazoRegra_InicioTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV24ContratoServicosPrazoRegra_FimTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV28ContratoServicosPrazoRegra_DiasTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV32DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class contratoservicosprazocontratoservicosprazoregrawc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00G52( IGxContext context ,
                                             short AV17TFContratoServicosPrazoRegra_Sequencial ,
                                             short AV18TFContratoServicosPrazoRegra_Sequencial_To ,
                                             decimal AV21TFContratoServicosPrazoRegra_Inicio ,
                                             decimal AV22TFContratoServicosPrazoRegra_Inicio_To ,
                                             decimal AV25TFContratoServicosPrazoRegra_Fim ,
                                             decimal AV26TFContratoServicosPrazoRegra_Fim_To ,
                                             short AV29TFContratoServicosPrazoRegra_Dias ,
                                             short AV30TFContratoServicosPrazoRegra_Dias_To ,
                                             short A906ContratoServicosPrazoRegra_Sequencial ,
                                             decimal A907ContratoServicosPrazoRegra_Inicio ,
                                             decimal A908ContratoServicosPrazoRegra_Fim ,
                                             short A909ContratoServicosPrazoRegra_Dias ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A903ContratoServicosPrazo_CntSrvCod ,
                                             int AV7ContratoServicosPrazo_CntSrvCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [14] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [ContratoServicosPrazo_CntSrvCod], [ContratoServicosPrazoRegra_Dias], [ContratoServicosPrazoRegra_Fim], [ContratoServicosPrazoRegra_Inicio], [ContratoServicosPrazoRegra_Sequencial]";
         sFromString = " FROM [ContratoServicosPrazoContratoServicosPrazoRegra] WITH (NOLOCK)";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE ([ContratoServicosPrazo_CntSrvCod] = @AV7ContratoServicosPrazo_CntSrvCod)";
         if ( ! (0==AV17TFContratoServicosPrazoRegra_Sequencial) )
         {
            sWhereString = sWhereString + " and ([ContratoServicosPrazoRegra_Sequencial] >= @AV17TFContratoServicosPrazoRegra_Sequencial)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ! (0==AV18TFContratoServicosPrazoRegra_Sequencial_To) )
         {
            sWhereString = sWhereString + " and ([ContratoServicosPrazoRegra_Sequencial] <= @AV18TFContratoServicosPrazoRegra_Sequencial_To)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV21TFContratoServicosPrazoRegra_Inicio) )
         {
            sWhereString = sWhereString + " and ([ContratoServicosPrazoRegra_Inicio] >= @AV21TFContratoServicosPrazoRegra_Inicio)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV22TFContratoServicosPrazoRegra_Inicio_To) )
         {
            sWhereString = sWhereString + " and ([ContratoServicosPrazoRegra_Inicio] <= @AV22TFContratoServicosPrazoRegra_Inicio_To)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV25TFContratoServicosPrazoRegra_Fim) )
         {
            sWhereString = sWhereString + " and ([ContratoServicosPrazoRegra_Fim] >= @AV25TFContratoServicosPrazoRegra_Fim)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV26TFContratoServicosPrazoRegra_Fim_To) )
         {
            sWhereString = sWhereString + " and ([ContratoServicosPrazoRegra_Fim] <= @AV26TFContratoServicosPrazoRegra_Fim_To)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! (0==AV29TFContratoServicosPrazoRegra_Dias) )
         {
            sWhereString = sWhereString + " and ([ContratoServicosPrazoRegra_Dias] >= @AV29TFContratoServicosPrazoRegra_Dias)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! (0==AV30TFContratoServicosPrazoRegra_Dias_To) )
         {
            sWhereString = sWhereString + " and ([ContratoServicosPrazoRegra_Dias] <= @AV30TFContratoServicosPrazoRegra_Dias_To)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosPrazo_CntSrvCod], [ContratoServicosPrazoRegra_Sequencial]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosPrazo_CntSrvCod] DESC, [ContratoServicosPrazoRegra_Sequencial] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosPrazo_CntSrvCod], [ContratoServicosPrazoRegra_Inicio]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosPrazo_CntSrvCod] DESC, [ContratoServicosPrazoRegra_Inicio] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosPrazo_CntSrvCod], [ContratoServicosPrazoRegra_Fim]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosPrazo_CntSrvCod] DESC, [ContratoServicosPrazoRegra_Fim] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosPrazo_CntSrvCod], [ContratoServicosPrazoRegra_Dias]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosPrazo_CntSrvCod] DESC, [ContratoServicosPrazoRegra_Dias] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosPrazo_CntSrvCod], [ContratoServicosPrazoRegra_Sequencial]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00G53( IGxContext context ,
                                             short AV17TFContratoServicosPrazoRegra_Sequencial ,
                                             short AV18TFContratoServicosPrazoRegra_Sequencial_To ,
                                             decimal AV21TFContratoServicosPrazoRegra_Inicio ,
                                             decimal AV22TFContratoServicosPrazoRegra_Inicio_To ,
                                             decimal AV25TFContratoServicosPrazoRegra_Fim ,
                                             decimal AV26TFContratoServicosPrazoRegra_Fim_To ,
                                             short AV29TFContratoServicosPrazoRegra_Dias ,
                                             short AV30TFContratoServicosPrazoRegra_Dias_To ,
                                             short A906ContratoServicosPrazoRegra_Sequencial ,
                                             decimal A907ContratoServicosPrazoRegra_Inicio ,
                                             decimal A908ContratoServicosPrazoRegra_Fim ,
                                             short A909ContratoServicosPrazoRegra_Dias ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A903ContratoServicosPrazo_CntSrvCod ,
                                             int AV7ContratoServicosPrazo_CntSrvCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [9] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [ContratoServicosPrazoContratoServicosPrazoRegra] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([ContratoServicosPrazo_CntSrvCod] = @AV7ContratoServicosPrazo_CntSrvCod)";
         if ( ! (0==AV17TFContratoServicosPrazoRegra_Sequencial) )
         {
            sWhereString = sWhereString + " and ([ContratoServicosPrazoRegra_Sequencial] >= @AV17TFContratoServicosPrazoRegra_Sequencial)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ! (0==AV18TFContratoServicosPrazoRegra_Sequencial_To) )
         {
            sWhereString = sWhereString + " and ([ContratoServicosPrazoRegra_Sequencial] <= @AV18TFContratoServicosPrazoRegra_Sequencial_To)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV21TFContratoServicosPrazoRegra_Inicio) )
         {
            sWhereString = sWhereString + " and ([ContratoServicosPrazoRegra_Inicio] >= @AV21TFContratoServicosPrazoRegra_Inicio)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV22TFContratoServicosPrazoRegra_Inicio_To) )
         {
            sWhereString = sWhereString + " and ([ContratoServicosPrazoRegra_Inicio] <= @AV22TFContratoServicosPrazoRegra_Inicio_To)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV25TFContratoServicosPrazoRegra_Fim) )
         {
            sWhereString = sWhereString + " and ([ContratoServicosPrazoRegra_Fim] >= @AV25TFContratoServicosPrazoRegra_Fim)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV26TFContratoServicosPrazoRegra_Fim_To) )
         {
            sWhereString = sWhereString + " and ([ContratoServicosPrazoRegra_Fim] <= @AV26TFContratoServicosPrazoRegra_Fim_To)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! (0==AV29TFContratoServicosPrazoRegra_Dias) )
         {
            sWhereString = sWhereString + " and ([ContratoServicosPrazoRegra_Dias] >= @AV29TFContratoServicosPrazoRegra_Dias)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! (0==AV30TFContratoServicosPrazoRegra_Dias_To) )
         {
            sWhereString = sWhereString + " and ([ContratoServicosPrazoRegra_Dias] <= @AV30TFContratoServicosPrazoRegra_Dias_To)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00G52(context, (short)dynConstraints[0] , (short)dynConstraints[1] , (decimal)dynConstraints[2] , (decimal)dynConstraints[3] , (decimal)dynConstraints[4] , (decimal)dynConstraints[5] , (short)dynConstraints[6] , (short)dynConstraints[7] , (short)dynConstraints[8] , (decimal)dynConstraints[9] , (decimal)dynConstraints[10] , (short)dynConstraints[11] , (short)dynConstraints[12] , (bool)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] );
               case 1 :
                     return conditional_H00G53(context, (short)dynConstraints[0] , (short)dynConstraints[1] , (decimal)dynConstraints[2] , (decimal)dynConstraints[3] , (decimal)dynConstraints[4] , (decimal)dynConstraints[5] , (short)dynConstraints[6] , (short)dynConstraints[7] , (short)dynConstraints[8] , (decimal)dynConstraints[9] , (decimal)dynConstraints[10] , (short)dynConstraints[11] , (short)dynConstraints[12] , (bool)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00G52 ;
          prmH00G52 = new Object[] {
          new Object[] {"@AV7ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFContratoServicosPrazoRegra_Sequencial",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV18TFContratoServicosPrazoRegra_Sequencial_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV21TFContratoServicosPrazoRegra_Inicio",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV22TFContratoServicosPrazoRegra_Inicio_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV25TFContratoServicosPrazoRegra_Fim",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV26TFContratoServicosPrazoRegra_Fim_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV29TFContratoServicosPrazoRegra_Dias",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV30TFContratoServicosPrazoRegra_Dias_To",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00G53 ;
          prmH00G53 = new Object[] {
          new Object[] {"@AV7ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFContratoServicosPrazoRegra_Sequencial",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV18TFContratoServicosPrazoRegra_Sequencial_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV21TFContratoServicosPrazoRegra_Inicio",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV22TFContratoServicosPrazoRegra_Inicio_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV25TFContratoServicosPrazoRegra_Fim",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV26TFContratoServicosPrazoRegra_Fim_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV29TFContratoServicosPrazoRegra_Dias",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV30TFContratoServicosPrazoRegra_Dias_To",SqlDbType.SmallInt,3,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00G52", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00G52,11,0,true,false )
             ,new CursorDef("H00G53", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00G53,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[15]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[16]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[17]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[18]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[19]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[20]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[21]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[22]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[10]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[11]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[12]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[13]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[14]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[15]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[16]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[17]);
                }
                return;
       }
    }

 }

}
