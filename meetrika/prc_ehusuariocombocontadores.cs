/*
               File: PRC_EhUsuarioComboContadores
        Description: Eh Usuario Combo Contadores
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:16.10
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_ehusuariocombocontadores : GXProcedure
   {
      public prc_ehusuariocombocontadores( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_ehusuariocombocontadores( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_UserId ,
                           int aP1_Contratada_Codigo ,
                           int aP2_Contratante_Codigo ,
                           bool aP3_EhAdministradorGAM ,
                           int aP4_AreaTrabalho_Codigo ,
                           out bool aP5_Flag )
      {
         this.AV14UserId = aP0_UserId;
         this.AV9Contratada_Codigo = aP1_Contratada_Codigo;
         this.AV10Contratante_Codigo = aP2_Contratante_Codigo;
         this.AV11EhAdministradorGAM = aP3_EhAdministradorGAM;
         this.AV13AreaTrabalho_Codigo = aP4_AreaTrabalho_Codigo;
         this.AV12Flag = false ;
         initialize();
         executePrivate();
         aP5_Flag=this.AV12Flag;
      }

      public bool executeUdp( int aP0_UserId ,
                              int aP1_Contratada_Codigo ,
                              int aP2_Contratante_Codigo ,
                              bool aP3_EhAdministradorGAM ,
                              int aP4_AreaTrabalho_Codigo )
      {
         this.AV14UserId = aP0_UserId;
         this.AV9Contratada_Codigo = aP1_Contratada_Codigo;
         this.AV10Contratante_Codigo = aP2_Contratante_Codigo;
         this.AV11EhAdministradorGAM = aP3_EhAdministradorGAM;
         this.AV13AreaTrabalho_Codigo = aP4_AreaTrabalho_Codigo;
         this.AV12Flag = false ;
         initialize();
         executePrivate();
         aP5_Flag=this.AV12Flag;
         return AV12Flag ;
      }

      public void executeSubmit( int aP0_UserId ,
                                 int aP1_Contratada_Codigo ,
                                 int aP2_Contratante_Codigo ,
                                 bool aP3_EhAdministradorGAM ,
                                 int aP4_AreaTrabalho_Codigo ,
                                 out bool aP5_Flag )
      {
         prc_ehusuariocombocontadores objprc_ehusuariocombocontadores;
         objprc_ehusuariocombocontadores = new prc_ehusuariocombocontadores();
         objprc_ehusuariocombocontadores.AV14UserId = aP0_UserId;
         objprc_ehusuariocombocontadores.AV9Contratada_Codigo = aP1_Contratada_Codigo;
         objprc_ehusuariocombocontadores.AV10Contratante_Codigo = aP2_Contratante_Codigo;
         objprc_ehusuariocombocontadores.AV11EhAdministradorGAM = aP3_EhAdministradorGAM;
         objprc_ehusuariocombocontadores.AV13AreaTrabalho_Codigo = aP4_AreaTrabalho_Codigo;
         objprc_ehusuariocombocontadores.AV12Flag = false ;
         objprc_ehusuariocombocontadores.context.SetSubmitInitialConfig(context);
         objprc_ehusuariocombocontadores.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_ehusuariocombocontadores);
         aP5_Flag=this.AV12Flag;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_ehusuariocombocontadores)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV12Flag = false;
         if ( AV10Contratante_Codigo > 0 )
         {
            AV17GXLvl5 = 0;
            /* Using cursor P009T2 */
            pr_default.execute(0, new Object[] {AV10Contratante_Codigo, AV14UserId});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A60ContratanteUsuario_UsuarioCod = P009T2_A60ContratanteUsuario_UsuarioCod[0];
               A63ContratanteUsuario_ContratanteCod = P009T2_A63ContratanteUsuario_ContratanteCod[0];
               AV17GXLvl5 = 1;
               AV12Flag = true;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            if ( AV17GXLvl5 == 0 )
            {
               /* Using cursor P009T3 */
               pr_default.execute(1, new Object[] {AV14UserId});
               while ( (pr_default.getStatus(1) != 101) )
               {
                  A292Usuario_EhContratante = P009T3_A292Usuario_EhContratante[0];
                  A54Usuario_Ativo = P009T3_A54Usuario_Ativo[0];
                  A1Usuario_Codigo = P009T3_A1Usuario_Codigo[0];
                  AV12Flag = true;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(1);
            }
         }
         else if ( AV9Contratada_Codigo > 0 )
         {
            /* Using cursor P009T4 */
            pr_default.execute(2, new Object[] {AV9Contratada_Codigo, AV14UserId, AV13AreaTrabalho_Codigo});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A66ContratadaUsuario_ContratadaCod = P009T4_A66ContratadaUsuario_ContratadaCod[0];
               A1228ContratadaUsuario_AreaTrabalhoCod = P009T4_A1228ContratadaUsuario_AreaTrabalhoCod[0];
               n1228ContratadaUsuario_AreaTrabalhoCod = P009T4_n1228ContratadaUsuario_AreaTrabalhoCod[0];
               A69ContratadaUsuario_UsuarioCod = P009T4_A69ContratadaUsuario_UsuarioCod[0];
               A1228ContratadaUsuario_AreaTrabalhoCod = P009T4_A1228ContratadaUsuario_AreaTrabalhoCod[0];
               n1228ContratadaUsuario_AreaTrabalhoCod = P009T4_n1228ContratadaUsuario_AreaTrabalhoCod[0];
               AV12Flag = true;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(2);
         }
         else if ( AV11EhAdministradorGAM )
         {
            /* Using cursor P009T5 */
            pr_default.execute(3, new Object[] {AV14UserId, AV13AreaTrabalho_Codigo});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A66ContratadaUsuario_ContratadaCod = P009T5_A66ContratadaUsuario_ContratadaCod[0];
               A516Contratada_TipoFabrica = P009T5_A516Contratada_TipoFabrica[0];
               n516Contratada_TipoFabrica = P009T5_n516Contratada_TipoFabrica[0];
               A1228ContratadaUsuario_AreaTrabalhoCod = P009T5_A1228ContratadaUsuario_AreaTrabalhoCod[0];
               n1228ContratadaUsuario_AreaTrabalhoCod = P009T5_n1228ContratadaUsuario_AreaTrabalhoCod[0];
               A69ContratadaUsuario_UsuarioCod = P009T5_A69ContratadaUsuario_UsuarioCod[0];
               A516Contratada_TipoFabrica = P009T5_A516Contratada_TipoFabrica[0];
               n516Contratada_TipoFabrica = P009T5_n516Contratada_TipoFabrica[0];
               A1228ContratadaUsuario_AreaTrabalhoCod = P009T5_A1228ContratadaUsuario_AreaTrabalhoCod[0];
               n1228ContratadaUsuario_AreaTrabalhoCod = P009T5_n1228ContratadaUsuario_AreaTrabalhoCod[0];
               AV12Flag = true;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(3);
            }
            pr_default.close(3);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P009T2_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         P009T2_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         P009T3_A292Usuario_EhContratante = new bool[] {false} ;
         P009T3_A54Usuario_Ativo = new bool[] {false} ;
         P009T3_A1Usuario_Codigo = new int[1] ;
         P009T4_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P009T4_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         P009T4_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         P009T4_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P009T5_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P009T5_A516Contratada_TipoFabrica = new String[] {""} ;
         P009T5_n516Contratada_TipoFabrica = new bool[] {false} ;
         P009T5_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         P009T5_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         P009T5_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         A516Contratada_TipoFabrica = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_ehusuariocombocontadores__default(),
            new Object[][] {
                new Object[] {
               P009T2_A60ContratanteUsuario_UsuarioCod, P009T2_A63ContratanteUsuario_ContratanteCod
               }
               , new Object[] {
               P009T3_A292Usuario_EhContratante, P009T3_A54Usuario_Ativo, P009T3_A1Usuario_Codigo
               }
               , new Object[] {
               P009T4_A66ContratadaUsuario_ContratadaCod, P009T4_A1228ContratadaUsuario_AreaTrabalhoCod, P009T4_n1228ContratadaUsuario_AreaTrabalhoCod, P009T4_A69ContratadaUsuario_UsuarioCod
               }
               , new Object[] {
               P009T5_A66ContratadaUsuario_ContratadaCod, P009T5_A516Contratada_TipoFabrica, P009T5_n516Contratada_TipoFabrica, P009T5_A1228ContratadaUsuario_AreaTrabalhoCod, P009T5_n1228ContratadaUsuario_AreaTrabalhoCod, P009T5_A69ContratadaUsuario_UsuarioCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV17GXLvl5 ;
      private int AV14UserId ;
      private int AV9Contratada_Codigo ;
      private int AV10Contratante_Codigo ;
      private int AV13AreaTrabalho_Codigo ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A1Usuario_Codigo ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A1228ContratadaUsuario_AreaTrabalhoCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private String scmdbuf ;
      private String A516Contratada_TipoFabrica ;
      private bool AV11EhAdministradorGAM ;
      private bool AV12Flag ;
      private bool A292Usuario_EhContratante ;
      private bool A54Usuario_Ativo ;
      private bool n1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool n516Contratada_TipoFabrica ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P009T2_A60ContratanteUsuario_UsuarioCod ;
      private int[] P009T2_A63ContratanteUsuario_ContratanteCod ;
      private bool[] P009T3_A292Usuario_EhContratante ;
      private bool[] P009T3_A54Usuario_Ativo ;
      private int[] P009T3_A1Usuario_Codigo ;
      private int[] P009T4_A66ContratadaUsuario_ContratadaCod ;
      private int[] P009T4_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] P009T4_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private int[] P009T4_A69ContratadaUsuario_UsuarioCod ;
      private int[] P009T5_A66ContratadaUsuario_ContratadaCod ;
      private String[] P009T5_A516Contratada_TipoFabrica ;
      private bool[] P009T5_n516Contratada_TipoFabrica ;
      private int[] P009T5_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] P009T5_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private int[] P009T5_A69ContratadaUsuario_UsuarioCod ;
      private bool aP5_Flag ;
   }

   public class prc_ehusuariocombocontadores__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP009T2 ;
          prmP009T2 = new Object[] {
          new Object[] {"@AV10Contratante_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV14UserId",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009T3 ;
          prmP009T3 = new Object[] {
          new Object[] {"@AV14UserId",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009T4 ;
          prmP009T4 = new Object[] {
          new Object[] {"@AV9Contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV14UserId",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009T5 ;
          prmP009T5 = new Object[] {
          new Object[] {"@AV14UserId",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P009T2", "SELECT TOP 1 [ContratanteUsuario_UsuarioCod], [ContratanteUsuario_ContratanteCod] FROM [ContratanteUsuario] WITH (NOLOCK) WHERE [ContratanteUsuario_ContratanteCod] = @AV10Contratante_Codigo and [ContratanteUsuario_UsuarioCod] = @AV14UserId ORDER BY [ContratanteUsuario_ContratanteCod], [ContratanteUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009T2,1,0,false,true )
             ,new CursorDef("P009T3", "SELECT TOP 1 [Usuario_EhContratante], [Usuario_Ativo], [Usuario_Codigo] FROM [Usuario] WITH (NOLOCK) WHERE ([Usuario_Codigo] = @AV14UserId) AND ([Usuario_Ativo] = 1) AND ([Usuario_EhContratante] = 1) ORDER BY [Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009T3,1,0,false,true )
             ,new CursorDef("P009T4", "SELECT TOP 1 T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T2.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod, T1.[ContratadaUsuario_UsuarioCod] FROM ([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) WHERE (T1.[ContratadaUsuario_ContratadaCod] = @AV9Contratada_Codigo and T1.[ContratadaUsuario_UsuarioCod] = @AV14UserId) AND (T2.[Contratada_AreaTrabalhoCod] = @AV13AreaTrabalho_Codigo) ORDER BY T1.[ContratadaUsuario_ContratadaCod], T1.[ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009T4,1,0,false,true )
             ,new CursorDef("P009T5", "SELECT TOP 1 T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T2.[Contratada_TipoFabrica], T2.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod, T1.[ContratadaUsuario_UsuarioCod] FROM ([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) WHERE (T1.[ContratadaUsuario_UsuarioCod] = @AV14UserId) AND (T2.[Contratada_AreaTrabalhoCod] = @AV13AreaTrabalho_Codigo) AND (T2.[Contratada_TipoFabrica] = 'M') ORDER BY T1.[ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009T5,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
