/*
               File: Modulo
        Description: Modulo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:19:21.59
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class modulo : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_11") == 0 )
         {
            A127Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_11( A127Sistema_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Modulo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Modulo_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODULO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Modulo_Codigo), "ZZZZZ9")));
               AV15Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Sistema_Codigo), 6, 0)));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Modulo", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtModulo_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public modulo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public modulo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Modulo_Codigo ,
                           ref int aP2_Sistema_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Modulo_Codigo = aP1_Modulo_Codigo;
         this.AV15Sistema_Codigo = aP2_Sistema_Codigo;
         executePrivate();
         aP2_Sistema_Codigo=this.AV15Sistema_Codigo;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_0R28( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_0R28e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtModulo_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A146Modulo_Codigo), 6, 0, ",", "")), ((edtModulo_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A146Modulo_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A146Modulo_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtModulo_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtModulo_Codigo_Visible, edtModulo_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Modulo.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_0R28( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_0R28( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_0R28e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_30_0R28( true) ;
         }
         return  ;
      }

      protected void wb_table3_30_0R28e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_0R28e( true) ;
         }
         else
         {
            wb_table1_2_0R28e( false) ;
         }
      }

      protected void wb_table3_30_0R28( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Modulo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Modulo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Modulo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            ClassString = "ActionButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtncopiarcolarmodulos_Internalname, "", "Copiar e Colar", bttBtncopiarcolarmodulos_Jsonclick, 5, "Copiar e Colar", "", StyleString, ClassString, bttBtncopiarcolarmodulos_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOCOPIARCOLARMODULOS\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_Modulo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_30_0R28e( true) ;
         }
         else
         {
            wb_table3_30_0R28e( false) ;
         }
      }

      protected void wb_table2_5_0R28( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_0R28( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_0R28e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_0R28e( true) ;
         }
         else
         {
            wb_table2_5_0R28e( false) ;
         }
      }

      protected void wb_table4_13_0R28( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmodulo_nome_Internalname, "Nome", "", "", lblTextblockmodulo_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Modulo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtModulo_Nome_Internalname, StringUtil.RTrim( A143Modulo_Nome), StringUtil.RTrim( context.localUtil.Format( A143Modulo_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtModulo_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtModulo_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_Modulo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmodulo_sigla_Internalname, "Sigla", "", "", lblTextblockmodulo_sigla_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Modulo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtModulo_Sigla_Internalname, StringUtil.RTrim( A145Modulo_Sigla), StringUtil.RTrim( context.localUtil.Format( A145Modulo_Sigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,22);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtModulo_Sigla_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtModulo_Sigla_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_Modulo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmodulo_descricao_Internalname, "Descri��o", "", "", lblTextblockmodulo_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Modulo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtModulo_Descricao_Internalname, A144Modulo_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,27);\"", 0, 1, edtModulo_Descricao_Enabled, 0, 80, "chr", 2, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "DescricaoLonga", "HLP_Modulo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_0R28e( true) ;
         }
         else
         {
            wb_table4_13_0R28e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E110R2 */
         E110R2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A143Modulo_Nome = StringUtil.Upper( cgiGet( edtModulo_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A143Modulo_Nome", A143Modulo_Nome);
               A145Modulo_Sigla = StringUtil.Upper( cgiGet( edtModulo_Sigla_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A145Modulo_Sigla", A145Modulo_Sigla);
               A144Modulo_Descricao = cgiGet( edtModulo_Descricao_Internalname);
               n144Modulo_Descricao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A144Modulo_Descricao", A144Modulo_Descricao);
               n144Modulo_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A144Modulo_Descricao)) ? true : false);
               A146Modulo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtModulo_Codigo_Internalname), ",", "."));
               n146Modulo_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A146Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0)));
               /* Read saved values. */
               Z146Modulo_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z146Modulo_Codigo"), ",", "."));
               Z143Modulo_Nome = cgiGet( "Z143Modulo_Nome");
               Z145Modulo_Sigla = cgiGet( "Z145Modulo_Sigla");
               Z127Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z127Sistema_Codigo"), ",", "."));
               A127Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z127Sistema_Codigo"), ",", "."));
               O143Modulo_Nome = cgiGet( "O143Modulo_Nome");
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N127Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( "N127Sistema_Codigo"), ",", "."));
               AV7Modulo_Codigo = (int)(context.localUtil.CToN( cgiGet( "vMODULO_CODIGO"), ",", "."));
               AV11Insert_Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_SISTEMA_CODIGO"), ",", "."));
               AV15Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( "vSISTEMA_CODIGO"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A127Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( "SISTEMA_CODIGO"), ",", "."));
               A416Sistema_Nome = cgiGet( "SISTEMA_NOME");
               AV17Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Modulo";
               A146Modulo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtModulo_Codigo_Internalname), ",", "."));
               n146Modulo_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A146Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A146Modulo_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A146Modulo_Codigo != Z146Modulo_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("modulo:[SecurityCheckFailed value for]"+"Modulo_Codigo:"+context.localUtil.Format( (decimal)(A146Modulo_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("modulo:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("modulo:[SecurityCheckFailed value for]"+"Sistema_Codigo:"+context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A146Modulo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n146Modulo_Codigo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A146Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode28 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode28;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound28 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_0R0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "MODULO_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtModulo_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E110R2 */
                           E110R2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E120R2 */
                           E120R2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCOPIARCOLARMODULOS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E130R2 */
                           E130R2 ();
                           nKeyPressed = 3;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E120R2 */
            E120R2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll0R28( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes0R28( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_0R0( )
      {
         BeforeValidate0R28( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls0R28( ) ;
            }
            else
            {
               CheckExtendedTable0R28( ) ;
               CloseExtendedTableCursors0R28( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption0R0( )
      {
      }

      protected void E110R2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV17Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV18GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18GXV1), 8, 0)));
            while ( AV18GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV18GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Sistema_Codigo") == 0 )
               {
                  AV11Insert_Sistema_Codigo = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Sistema_Codigo), 6, 0)));
               }
               AV18GXV1 = (int)(AV18GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18GXV1), 8, 0)));
            }
         }
         edtModulo_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtModulo_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtModulo_Codigo_Visible), 5, 0)));
         AV14Tabela_ModuloCod = A146Modulo_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Tabela_ModuloCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Tabela_ModuloCod), 6, 0)));
      }

      protected void E120R2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwmodulo.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {(int)AV15Sistema_Codigo});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E130R2( )
      {
         /* 'DoCopiarColarModulos' Routine */
         context.wjLoc = formatLink("wp_copiarcolarmodulos.aspx") ;
         context.wjLocDisableFrm = 1;
      }

      protected void ZM0R28( short GX_JID )
      {
         if ( ( GX_JID == 10 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z143Modulo_Nome = T000R3_A143Modulo_Nome[0];
               Z145Modulo_Sigla = T000R3_A145Modulo_Sigla[0];
               Z127Sistema_Codigo = T000R3_A127Sistema_Codigo[0];
            }
            else
            {
               Z143Modulo_Nome = A143Modulo_Nome;
               Z145Modulo_Sigla = A145Modulo_Sigla;
               Z127Sistema_Codigo = A127Sistema_Codigo;
            }
         }
         if ( GX_JID == -10 )
         {
            Z146Modulo_Codigo = A146Modulo_Codigo;
            Z143Modulo_Nome = A143Modulo_Nome;
            Z145Modulo_Sigla = A145Modulo_Sigla;
            Z144Modulo_Descricao = A144Modulo_Descricao;
            Z127Sistema_Codigo = A127Sistema_Codigo;
            Z416Sistema_Nome = A416Sistema_Nome;
         }
      }

      protected void standaloneNotModal( )
      {
         edtModulo_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtModulo_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtModulo_Codigo_Enabled), 5, 0)));
         AV17Pgmname = "Modulo";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Pgmname", AV17Pgmname);
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         edtModulo_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtModulo_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtModulo_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7Modulo_Codigo) )
         {
            A146Modulo_Codigo = AV7Modulo_Codigo;
            n146Modulo_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A146Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Sistema_Codigo) )
         {
            A127Sistema_Codigo = AV11Insert_Sistema_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A127Sistema_Codigo) && ( Gx_BScreen == 0 ) )
            {
               A127Sistema_Codigo = AV15Sistema_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
            }
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T000R4 */
            pr_default.execute(2, new Object[] {A127Sistema_Codigo});
            A416Sistema_Nome = T000R4_A416Sistema_Nome[0];
            pr_default.close(2);
            Dvpanel_tableattributes_Title = "M�dulo do Sistema: "+A416Sistema_Nome;
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvpanel_tableattributes_Internalname, "Title", Dvpanel_tableattributes_Title);
         }
      }

      protected void Load0R28( )
      {
         /* Using cursor T000R5 */
         pr_default.execute(3, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound28 = 1;
            A143Modulo_Nome = T000R5_A143Modulo_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A143Modulo_Nome", A143Modulo_Nome);
            A145Modulo_Sigla = T000R5_A145Modulo_Sigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A145Modulo_Sigla", A145Modulo_Sigla);
            A144Modulo_Descricao = T000R5_A144Modulo_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A144Modulo_Descricao", A144Modulo_Descricao);
            n144Modulo_Descricao = T000R5_n144Modulo_Descricao[0];
            A416Sistema_Nome = T000R5_A416Sistema_Nome[0];
            A127Sistema_Codigo = T000R5_A127Sistema_Codigo[0];
            ZM0R28( -10) ;
         }
         pr_default.close(3);
         OnLoadActions0R28( ) ;
      }

      protected void OnLoadActions0R28( )
      {
         Dvpanel_tableattributes_Title = "M�dulo do Sistema: "+A416Sistema_Nome;
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvpanel_tableattributes_Internalname, "Title", Dvpanel_tableattributes_Title);
      }

      protected void CheckExtendedTable0R28( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A143Modulo_Nome)) )
         {
            GX_msglist.addItem("M�dulo � obrigat�rio.", 1, "MODULO_NOME");
            AnyError = 1;
            GX_FocusControl = edtModulo_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T000R4 */
         pr_default.execute(2, new Object[] {A127Sistema_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Sistema'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A416Sistema_Nome = T000R4_A416Sistema_Nome[0];
         pr_default.close(2);
         Dvpanel_tableattributes_Title = "M�dulo do Sistema: "+A416Sistema_Nome;
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvpanel_tableattributes_Internalname, "Title", Dvpanel_tableattributes_Title);
      }

      protected void CloseExtendedTableCursors0R28( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_11( int A127Sistema_Codigo )
      {
         /* Using cursor T000R6 */
         pr_default.execute(4, new Object[] {A127Sistema_Codigo});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Sistema'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A416Sistema_Nome = T000R6_A416Sistema_Nome[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A416Sistema_Nome)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(4) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(4);
      }

      protected void GetKey0R28( )
      {
         /* Using cursor T000R7 */
         pr_default.execute(5, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound28 = 1;
         }
         else
         {
            RcdFound28 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T000R3 */
         pr_default.execute(1, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM0R28( 10) ;
            RcdFound28 = 1;
            A146Modulo_Codigo = T000R3_A146Modulo_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A146Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0)));
            n146Modulo_Codigo = T000R3_n146Modulo_Codigo[0];
            A143Modulo_Nome = T000R3_A143Modulo_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A143Modulo_Nome", A143Modulo_Nome);
            A145Modulo_Sigla = T000R3_A145Modulo_Sigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A145Modulo_Sigla", A145Modulo_Sigla);
            A144Modulo_Descricao = T000R3_A144Modulo_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A144Modulo_Descricao", A144Modulo_Descricao);
            n144Modulo_Descricao = T000R3_n144Modulo_Descricao[0];
            A127Sistema_Codigo = T000R3_A127Sistema_Codigo[0];
            O143Modulo_Nome = A143Modulo_Nome;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A143Modulo_Nome", A143Modulo_Nome);
            Z146Modulo_Codigo = A146Modulo_Codigo;
            sMode28 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load0R28( ) ;
            if ( AnyError == 1 )
            {
               RcdFound28 = 0;
               InitializeNonKey0R28( ) ;
            }
            Gx_mode = sMode28;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound28 = 0;
            InitializeNonKey0R28( ) ;
            sMode28 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode28;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey0R28( ) ;
         if ( RcdFound28 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound28 = 0;
         /* Using cursor T000R8 */
         pr_default.execute(6, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( T000R8_A146Modulo_Codigo[0] < A146Modulo_Codigo ) ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( T000R8_A146Modulo_Codigo[0] > A146Modulo_Codigo ) ) )
            {
               A146Modulo_Codigo = T000R8_A146Modulo_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A146Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0)));
               n146Modulo_Codigo = T000R8_n146Modulo_Codigo[0];
               RcdFound28 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound28 = 0;
         /* Using cursor T000R9 */
         pr_default.execute(7, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T000R9_A146Modulo_Codigo[0] > A146Modulo_Codigo ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T000R9_A146Modulo_Codigo[0] < A146Modulo_Codigo ) ) )
            {
               A146Modulo_Codigo = T000R9_A146Modulo_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A146Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0)));
               n146Modulo_Codigo = T000R9_n146Modulo_Codigo[0];
               RcdFound28 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey0R28( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtModulo_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert0R28( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound28 == 1 )
            {
               if ( A146Modulo_Codigo != Z146Modulo_Codigo )
               {
                  A146Modulo_Codigo = Z146Modulo_Codigo;
                  n146Modulo_Codigo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A146Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "MODULO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtModulo_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtModulo_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update0R28( ) ;
                  GX_FocusControl = edtModulo_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A146Modulo_Codigo != Z146Modulo_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtModulo_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert0R28( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "MODULO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtModulo_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtModulo_Nome_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert0R28( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A146Modulo_Codigo != Z146Modulo_Codigo )
         {
            A146Modulo_Codigo = Z146Modulo_Codigo;
            n146Modulo_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A146Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "MODULO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtModulo_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtModulo_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency0R28( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T000R2 */
            pr_default.execute(0, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Modulo"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z143Modulo_Nome, T000R2_A143Modulo_Nome[0]) != 0 ) || ( StringUtil.StrCmp(Z145Modulo_Sigla, T000R2_A145Modulo_Sigla[0]) != 0 ) || ( Z127Sistema_Codigo != T000R2_A127Sistema_Codigo[0] ) )
            {
               if ( StringUtil.StrCmp(Z143Modulo_Nome, T000R2_A143Modulo_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("modulo:[seudo value changed for attri]"+"Modulo_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z143Modulo_Nome);
                  GXUtil.WriteLogRaw("Current: ",T000R2_A143Modulo_Nome[0]);
               }
               if ( StringUtil.StrCmp(Z145Modulo_Sigla, T000R2_A145Modulo_Sigla[0]) != 0 )
               {
                  GXUtil.WriteLog("modulo:[seudo value changed for attri]"+"Modulo_Sigla");
                  GXUtil.WriteLogRaw("Old: ",Z145Modulo_Sigla);
                  GXUtil.WriteLogRaw("Current: ",T000R2_A145Modulo_Sigla[0]);
               }
               if ( Z127Sistema_Codigo != T000R2_A127Sistema_Codigo[0] )
               {
                  GXUtil.WriteLog("modulo:[seudo value changed for attri]"+"Sistema_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z127Sistema_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T000R2_A127Sistema_Codigo[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Modulo"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0R28( )
      {
         BeforeValidate0R28( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0R28( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0R28( 0) ;
            CheckOptimisticConcurrency0R28( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0R28( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0R28( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000R10 */
                     pr_default.execute(8, new Object[] {A143Modulo_Nome, A145Modulo_Sigla, n144Modulo_Descricao, A144Modulo_Descricao, A127Sistema_Codigo});
                     A146Modulo_Codigo = T000R10_A146Modulo_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A146Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0)));
                     n146Modulo_Codigo = T000R10_n146Modulo_Codigo[0];
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("Modulo") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption0R0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0R28( ) ;
            }
            EndLevel0R28( ) ;
         }
         CloseExtendedTableCursors0R28( ) ;
      }

      protected void Update0R28( )
      {
         BeforeValidate0R28( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0R28( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0R28( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0R28( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0R28( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000R11 */
                     pr_default.execute(9, new Object[] {A143Modulo_Nome, A145Modulo_Sigla, n144Modulo_Descricao, A144Modulo_Descricao, A127Sistema_Codigo, n146Modulo_Codigo, A146Modulo_Codigo});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("Modulo") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Modulo"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0R28( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0R28( ) ;
         }
         CloseExtendedTableCursors0R28( ) ;
      }

      protected void DeferredUpdate0R28( )
      {
      }

      protected void delete( )
      {
         BeforeValidate0R28( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0R28( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0R28( ) ;
            AfterConfirm0R28( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0R28( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000R12 */
                  pr_default.execute(10, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("Modulo") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode28 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel0R28( ) ;
         Gx_mode = sMode28;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls0R28( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T000R13 */
            pr_default.execute(11, new Object[] {A127Sistema_Codigo});
            A416Sistema_Nome = T000R13_A416Sistema_Nome[0];
            pr_default.close(11);
            Dvpanel_tableattributes_Title = "M�dulo do Sistema: "+A416Sistema_Nome;
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvpanel_tableattributes_Internalname, "Title", Dvpanel_tableattributes_Title);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T000R14 */
            pr_default.execute(12, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
            if ( (pr_default.getStatus(12) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Fun��es APF - An�lise de Ponto de Fun��o"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(12);
            /* Using cursor T000R15 */
            pr_default.execute(13, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
            if ( (pr_default.getStatus(13) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Tabela"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(13);
            /* Using cursor T000R16 */
            pr_default.execute(14, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
            if ( (pr_default.getStatus(14) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Resultado das Contagens"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(14);
            /* Using cursor T000R17 */
            pr_default.execute(15, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
            if ( (pr_default.getStatus(15) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Modulo Funcoes"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(15);
         }
      }

      protected void EndLevel0R28( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0R28( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(11);
            context.CommitDataStores( "Modulo");
            if ( AnyError == 0 )
            {
               ConfirmValues0R0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(11);
            context.RollbackDataStores( "Modulo");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart0R28( )
      {
         /* Scan By routine */
         /* Using cursor T000R18 */
         pr_default.execute(16);
         RcdFound28 = 0;
         if ( (pr_default.getStatus(16) != 101) )
         {
            RcdFound28 = 1;
            A146Modulo_Codigo = T000R18_A146Modulo_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A146Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0)));
            n146Modulo_Codigo = T000R18_n146Modulo_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext0R28( )
      {
         /* Scan next routine */
         pr_default.readNext(16);
         RcdFound28 = 0;
         if ( (pr_default.getStatus(16) != 101) )
         {
            RcdFound28 = 1;
            A146Modulo_Codigo = T000R18_A146Modulo_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A146Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0)));
            n146Modulo_Codigo = T000R18_n146Modulo_Codigo[0];
         }
      }

      protected void ScanEnd0R28( )
      {
         pr_default.close(16);
      }

      protected void AfterConfirm0R28( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0R28( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0R28( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0R28( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0R28( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0R28( )
      {
         /* Before Validate Rules */
         if ( ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  || ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  ) && ( StringUtil.StrCmp(A143Modulo_Nome, O143Modulo_Nome) != 0 ) && new prc_existemodulo(context).executeUdp( ref  A127Sistema_Codigo,  A143Modulo_Nome,  A145Modulo_Sigla) )
         {
            GX_msglist.addItem("Esse nome ou sigla do M�dulo j� foi cadastrado!", 1, "MODULO_NOME");
            AnyError = 1;
            GX_FocusControl = edtModulo_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void DisableAttributes0R28( )
      {
         edtModulo_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtModulo_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtModulo_Nome_Enabled), 5, 0)));
         edtModulo_Sigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtModulo_Sigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtModulo_Sigla_Enabled), 5, 0)));
         edtModulo_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtModulo_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtModulo_Descricao_Enabled), 5, 0)));
         edtModulo_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtModulo_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtModulo_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues0R0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203120192265");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("modulo.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Modulo_Codigo) + "," + UrlEncode("" +AV15Sistema_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z146Modulo_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z146Modulo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z143Modulo_Nome", StringUtil.RTrim( Z143Modulo_Nome));
         GxWebStd.gx_hidden_field( context, "Z145Modulo_Sigla", StringUtil.RTrim( Z145Modulo_Sigla));
         GxWebStd.gx_hidden_field( context, "Z127Sistema_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z127Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "O143Modulo_Nome", StringUtil.RTrim( O143Modulo_Nome));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N127Sistema_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vMODULO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Modulo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_SISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vSISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SISTEMA_NOME", A416Sistema_Nome);
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV17Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODULO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Modulo_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Modulo";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A146Modulo_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("modulo:[SendSecurityCheck value for]"+"Modulo_Codigo:"+context.localUtil.Format( (decimal)(A146Modulo_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("modulo:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("modulo:[SendSecurityCheck value for]"+"Sistema_Codigo:"+context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("modulo.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Modulo_Codigo) + "," + UrlEncode("" +AV15Sistema_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "Modulo" ;
      }

      public override String GetPgmdesc( )
      {
         return "Modulo" ;
      }

      protected void InitializeNonKey0R28( )
      {
         A143Modulo_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A143Modulo_Nome", A143Modulo_Nome);
         A145Modulo_Sigla = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A145Modulo_Sigla", A145Modulo_Sigla);
         A144Modulo_Descricao = "";
         n144Modulo_Descricao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A144Modulo_Descricao", A144Modulo_Descricao);
         n144Modulo_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A144Modulo_Descricao)) ? true : false);
         A416Sistema_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A416Sistema_Nome", A416Sistema_Nome);
         A127Sistema_Codigo = AV15Sistema_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
         O143Modulo_Nome = A143Modulo_Nome;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A143Modulo_Nome", A143Modulo_Nome);
         Z143Modulo_Nome = "";
         Z145Modulo_Sigla = "";
         Z127Sistema_Codigo = 0;
      }

      protected void InitAll0R28( )
      {
         A146Modulo_Codigo = 0;
         n146Modulo_Codigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A146Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0)));
         InitializeNonKey0R28( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A127Sistema_Codigo = i127Sistema_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203120192289");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("modulo.js", "?20203120192290");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockmodulo_nome_Internalname = "TEXTBLOCKMODULO_NOME";
         edtModulo_Nome_Internalname = "MODULO_NOME";
         lblTextblockmodulo_sigla_Internalname = "TEXTBLOCKMODULO_SIGLA";
         edtModulo_Sigla_Internalname = "MODULO_SIGLA";
         lblTextblockmodulo_descricao_Internalname = "TEXTBLOCKMODULO_DESCRICAO";
         edtModulo_Descricao_Internalname = "MODULO_DESCRICAO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         bttBtncopiarcolarmodulos_Internalname = "BTNCOPIARCOLARMODULOS";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtModulo_Codigo_Internalname = "MODULO_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Modulo";
         Dvpanel_tableattributes_Title = "M�dulo";
         edtModulo_Descricao_Enabled = 1;
         edtModulo_Sigla_Jsonclick = "";
         edtModulo_Sigla_Enabled = 1;
         edtModulo_Nome_Jsonclick = "";
         edtModulo_Nome_Enabled = 1;
         bttBtncopiarcolarmodulos_Visible = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtModulo_Codigo_Jsonclick = "";
         edtModulo_Codigo_Enabled = 0;
         edtModulo_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7Modulo_Codigo',fld:'vMODULO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV15Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E120R2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         setEventMetadata("'DOCOPIARCOLARMODULOS'","{handler:'E130R2',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(11);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z143Modulo_Nome = "";
         Z145Modulo_Sigla = "";
         O143Modulo_Nome = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         bttBtncopiarcolarmodulos_Jsonclick = "";
         lblTextblockmodulo_nome_Jsonclick = "";
         A143Modulo_Nome = "";
         lblTextblockmodulo_sigla_Jsonclick = "";
         A145Modulo_Sigla = "";
         lblTextblockmodulo_descricao_Jsonclick = "";
         A144Modulo_Descricao = "";
         A416Sistema_Nome = "";
         AV17Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode28 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z144Modulo_Descricao = "";
         Z416Sistema_Nome = "";
         T000R4_A416Sistema_Nome = new String[] {""} ;
         T000R5_A146Modulo_Codigo = new int[1] ;
         T000R5_n146Modulo_Codigo = new bool[] {false} ;
         T000R5_A143Modulo_Nome = new String[] {""} ;
         T000R5_A145Modulo_Sigla = new String[] {""} ;
         T000R5_A144Modulo_Descricao = new String[] {""} ;
         T000R5_n144Modulo_Descricao = new bool[] {false} ;
         T000R5_A416Sistema_Nome = new String[] {""} ;
         T000R5_A127Sistema_Codigo = new int[1] ;
         T000R6_A416Sistema_Nome = new String[] {""} ;
         T000R7_A146Modulo_Codigo = new int[1] ;
         T000R7_n146Modulo_Codigo = new bool[] {false} ;
         T000R3_A146Modulo_Codigo = new int[1] ;
         T000R3_n146Modulo_Codigo = new bool[] {false} ;
         T000R3_A143Modulo_Nome = new String[] {""} ;
         T000R3_A145Modulo_Sigla = new String[] {""} ;
         T000R3_A144Modulo_Descricao = new String[] {""} ;
         T000R3_n144Modulo_Descricao = new bool[] {false} ;
         T000R3_A127Sistema_Codigo = new int[1] ;
         T000R8_A146Modulo_Codigo = new int[1] ;
         T000R8_n146Modulo_Codigo = new bool[] {false} ;
         T000R9_A146Modulo_Codigo = new int[1] ;
         T000R9_n146Modulo_Codigo = new bool[] {false} ;
         T000R2_A146Modulo_Codigo = new int[1] ;
         T000R2_n146Modulo_Codigo = new bool[] {false} ;
         T000R2_A143Modulo_Nome = new String[] {""} ;
         T000R2_A145Modulo_Sigla = new String[] {""} ;
         T000R2_A144Modulo_Descricao = new String[] {""} ;
         T000R2_n144Modulo_Descricao = new bool[] {false} ;
         T000R2_A127Sistema_Codigo = new int[1] ;
         T000R10_A146Modulo_Codigo = new int[1] ;
         T000R10_n146Modulo_Codigo = new bool[] {false} ;
         T000R13_A416Sistema_Nome = new String[] {""} ;
         T000R14_A165FuncaoAPF_Codigo = new int[1] ;
         T000R15_A172Tabela_Codigo = new int[1] ;
         T000R16_A456ContagemResultado_Codigo = new int[1] ;
         T000R17_A146Modulo_Codigo = new int[1] ;
         T000R17_n146Modulo_Codigo = new bool[] {false} ;
         T000R17_A161FuncaoUsuario_Codigo = new int[1] ;
         T000R18_A146Modulo_Codigo = new int[1] ;
         T000R18_n146Modulo_Codigo = new bool[] {false} ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.modulo__default(),
            new Object[][] {
                new Object[] {
               T000R2_A146Modulo_Codigo, T000R2_A143Modulo_Nome, T000R2_A145Modulo_Sigla, T000R2_A144Modulo_Descricao, T000R2_n144Modulo_Descricao, T000R2_A127Sistema_Codigo
               }
               , new Object[] {
               T000R3_A146Modulo_Codigo, T000R3_A143Modulo_Nome, T000R3_A145Modulo_Sigla, T000R3_A144Modulo_Descricao, T000R3_n144Modulo_Descricao, T000R3_A127Sistema_Codigo
               }
               , new Object[] {
               T000R4_A416Sistema_Nome
               }
               , new Object[] {
               T000R5_A146Modulo_Codigo, T000R5_A143Modulo_Nome, T000R5_A145Modulo_Sigla, T000R5_A144Modulo_Descricao, T000R5_n144Modulo_Descricao, T000R5_A416Sistema_Nome, T000R5_A127Sistema_Codigo
               }
               , new Object[] {
               T000R6_A416Sistema_Nome
               }
               , new Object[] {
               T000R7_A146Modulo_Codigo
               }
               , new Object[] {
               T000R8_A146Modulo_Codigo
               }
               , new Object[] {
               T000R9_A146Modulo_Codigo
               }
               , new Object[] {
               T000R10_A146Modulo_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000R13_A416Sistema_Nome
               }
               , new Object[] {
               T000R14_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               T000R15_A172Tabela_Codigo
               }
               , new Object[] {
               T000R16_A456ContagemResultado_Codigo
               }
               , new Object[] {
               T000R17_A146Modulo_Codigo, T000R17_A161FuncaoUsuario_Codigo
               }
               , new Object[] {
               T000R18_A146Modulo_Codigo
               }
            }
         );
         AV17Pgmname = "Modulo";
         Z127Sistema_Codigo = 0;
         N127Sistema_Codigo = 0;
         i127Sistema_Codigo = 0;
         A127Sistema_Codigo = 0;
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short Gx_BScreen ;
      private short RcdFound28 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private int wcpOAV7Modulo_Codigo ;
      private int wcpOAV15Sistema_Codigo ;
      private int Z146Modulo_Codigo ;
      private int Z127Sistema_Codigo ;
      private int N127Sistema_Codigo ;
      private int A127Sistema_Codigo ;
      private int AV7Modulo_Codigo ;
      private int AV15Sistema_Codigo ;
      private int trnEnded ;
      private int A146Modulo_Codigo ;
      private int edtModulo_Codigo_Enabled ;
      private int edtModulo_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int bttBtncopiarcolarmodulos_Visible ;
      private int edtModulo_Nome_Enabled ;
      private int edtModulo_Sigla_Enabled ;
      private int edtModulo_Descricao_Enabled ;
      private int AV11Insert_Sistema_Codigo ;
      private int AV18GXV1 ;
      private int AV14Tabela_ModuloCod ;
      private int i127Sistema_Codigo ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z143Modulo_Nome ;
      private String Z145Modulo_Sigla ;
      private String O143Modulo_Nome ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtModulo_Nome_Internalname ;
      private String edtModulo_Codigo_Internalname ;
      private String edtModulo_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String bttBtncopiarcolarmodulos_Internalname ;
      private String bttBtncopiarcolarmodulos_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockmodulo_nome_Internalname ;
      private String lblTextblockmodulo_nome_Jsonclick ;
      private String A143Modulo_Nome ;
      private String edtModulo_Nome_Jsonclick ;
      private String lblTextblockmodulo_sigla_Internalname ;
      private String lblTextblockmodulo_sigla_Jsonclick ;
      private String edtModulo_Sigla_Internalname ;
      private String A145Modulo_Sigla ;
      private String edtModulo_Sigla_Jsonclick ;
      private String lblTextblockmodulo_descricao_Internalname ;
      private String lblTextblockmodulo_descricao_Jsonclick ;
      private String edtModulo_Descricao_Internalname ;
      private String AV17Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode28 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Dvpanel_tableattributes_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n144Modulo_Descricao ;
      private bool n146Modulo_Codigo ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private String A144Modulo_Descricao ;
      private String Z144Modulo_Descricao ;
      private String A416Sistema_Nome ;
      private String Z416Sistema_Nome ;
      private IGxSession AV10WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP2_Sistema_Codigo ;
      private IDataStoreProvider pr_default ;
      private String[] T000R4_A416Sistema_Nome ;
      private int[] T000R5_A146Modulo_Codigo ;
      private bool[] T000R5_n146Modulo_Codigo ;
      private String[] T000R5_A143Modulo_Nome ;
      private String[] T000R5_A145Modulo_Sigla ;
      private String[] T000R5_A144Modulo_Descricao ;
      private bool[] T000R5_n144Modulo_Descricao ;
      private String[] T000R5_A416Sistema_Nome ;
      private int[] T000R5_A127Sistema_Codigo ;
      private String[] T000R6_A416Sistema_Nome ;
      private int[] T000R7_A146Modulo_Codigo ;
      private bool[] T000R7_n146Modulo_Codigo ;
      private int[] T000R3_A146Modulo_Codigo ;
      private bool[] T000R3_n146Modulo_Codigo ;
      private String[] T000R3_A143Modulo_Nome ;
      private String[] T000R3_A145Modulo_Sigla ;
      private String[] T000R3_A144Modulo_Descricao ;
      private bool[] T000R3_n144Modulo_Descricao ;
      private int[] T000R3_A127Sistema_Codigo ;
      private int[] T000R8_A146Modulo_Codigo ;
      private bool[] T000R8_n146Modulo_Codigo ;
      private int[] T000R9_A146Modulo_Codigo ;
      private bool[] T000R9_n146Modulo_Codigo ;
      private int[] T000R2_A146Modulo_Codigo ;
      private bool[] T000R2_n146Modulo_Codigo ;
      private String[] T000R2_A143Modulo_Nome ;
      private String[] T000R2_A145Modulo_Sigla ;
      private String[] T000R2_A144Modulo_Descricao ;
      private bool[] T000R2_n144Modulo_Descricao ;
      private int[] T000R2_A127Sistema_Codigo ;
      private int[] T000R10_A146Modulo_Codigo ;
      private bool[] T000R10_n146Modulo_Codigo ;
      private String[] T000R13_A416Sistema_Nome ;
      private int[] T000R14_A165FuncaoAPF_Codigo ;
      private int[] T000R15_A172Tabela_Codigo ;
      private int[] T000R16_A456ContagemResultado_Codigo ;
      private int[] T000R17_A146Modulo_Codigo ;
      private bool[] T000R17_n146Modulo_Codigo ;
      private int[] T000R17_A161FuncaoUsuario_Codigo ;
      private int[] T000R18_A146Modulo_Codigo ;
      private bool[] T000R18_n146Modulo_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class modulo__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT000R5 ;
          prmT000R5 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000R4 ;
          prmT000R4 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000R6 ;
          prmT000R6 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000R7 ;
          prmT000R7 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000R3 ;
          prmT000R3 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000R8 ;
          prmT000R8 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000R9 ;
          prmT000R9 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000R2 ;
          prmT000R2 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000R10 ;
          prmT000R10 = new Object[] {
          new Object[] {"@Modulo_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Modulo_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@Modulo_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000R11 ;
          prmT000R11 = new Object[] {
          new Object[] {"@Modulo_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Modulo_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@Modulo_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000R12 ;
          prmT000R12 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000R13 ;
          prmT000R13 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000R14 ;
          prmT000R14 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000R15 ;
          prmT000R15 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000R16 ;
          prmT000R16 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000R17 ;
          prmT000R17 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000R18 ;
          prmT000R18 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("T000R2", "SELECT [Modulo_Codigo], [Modulo_Nome], [Modulo_Sigla], [Modulo_Descricao], [Sistema_Codigo] FROM [Modulo] WITH (UPDLOCK) WHERE [Modulo_Codigo] = @Modulo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000R2,1,0,true,false )
             ,new CursorDef("T000R3", "SELECT [Modulo_Codigo], [Modulo_Nome], [Modulo_Sigla], [Modulo_Descricao], [Sistema_Codigo] FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Modulo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000R3,1,0,true,false )
             ,new CursorDef("T000R4", "SELECT [Sistema_Nome] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000R4,1,0,true,false )
             ,new CursorDef("T000R5", "SELECT TM1.[Modulo_Codigo], TM1.[Modulo_Nome], TM1.[Modulo_Sigla], TM1.[Modulo_Descricao], T2.[Sistema_Nome], TM1.[Sistema_Codigo] FROM ([Modulo] TM1 WITH (NOLOCK) INNER JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = TM1.[Sistema_Codigo]) WHERE TM1.[Modulo_Codigo] = @Modulo_Codigo ORDER BY TM1.[Modulo_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000R5,100,0,true,false )
             ,new CursorDef("T000R6", "SELECT [Sistema_Nome] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000R6,1,0,true,false )
             ,new CursorDef("T000R7", "SELECT [Modulo_Codigo] FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Modulo_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000R7,1,0,true,false )
             ,new CursorDef("T000R8", "SELECT TOP 1 [Modulo_Codigo] FROM [Modulo] WITH (NOLOCK) WHERE ( [Modulo_Codigo] > @Modulo_Codigo) ORDER BY [Modulo_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000R8,1,0,true,true )
             ,new CursorDef("T000R9", "SELECT TOP 1 [Modulo_Codigo] FROM [Modulo] WITH (NOLOCK) WHERE ( [Modulo_Codigo] < @Modulo_Codigo) ORDER BY [Modulo_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000R9,1,0,true,true )
             ,new CursorDef("T000R10", "INSERT INTO [Modulo]([Modulo_Nome], [Modulo_Sigla], [Modulo_Descricao], [Sistema_Codigo]) VALUES(@Modulo_Nome, @Modulo_Sigla, @Modulo_Descricao, @Sistema_Codigo); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000R10)
             ,new CursorDef("T000R11", "UPDATE [Modulo] SET [Modulo_Nome]=@Modulo_Nome, [Modulo_Sigla]=@Modulo_Sigla, [Modulo_Descricao]=@Modulo_Descricao, [Sistema_Codigo]=@Sistema_Codigo  WHERE [Modulo_Codigo] = @Modulo_Codigo", GxErrorMask.GX_NOMASK,prmT000R11)
             ,new CursorDef("T000R12", "DELETE FROM [Modulo]  WHERE [Modulo_Codigo] = @Modulo_Codigo", GxErrorMask.GX_NOMASK,prmT000R12)
             ,new CursorDef("T000R13", "SELECT [Sistema_Nome] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000R13,1,0,true,false )
             ,new CursorDef("T000R14", "SELECT TOP 1 [FuncaoAPF_Codigo] FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_ModuloCod] = @Modulo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000R14,1,0,true,true )
             ,new CursorDef("T000R15", "SELECT TOP 1 [Tabela_Codigo] FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_ModuloCod] = @Modulo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000R15,1,0,true,true )
             ,new CursorDef("T000R16", "SELECT TOP 1 [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Modulo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000R16,1,0,true,true )
             ,new CursorDef("T000R17", "SELECT TOP 1 [Modulo_Codigo], [FuncaoUsuario_Codigo] FROM [ModuloFuncoes1] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Modulo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000R17,1,0,true,true )
             ,new CursorDef("T000R18", "SELECT [Modulo_Codigo] FROM [Modulo] WITH (NOLOCK) ORDER BY [Modulo_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000R18,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 8 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                stmt.SetParameter(4, (int)parms[4]);
                return;
             case 9 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                stmt.SetParameter(4, (int)parms[4]);
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[6]);
                }
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
