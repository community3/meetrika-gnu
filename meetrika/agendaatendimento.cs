/*
               File: AgendaAtendimento
        Description: Agenda Atendimento
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:10:41.41
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class agendaatendimento : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_12") == 0 )
         {
            A1183AgendaAtendimento_CntSrcCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1183AgendaAtendimento_CntSrcCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1183AgendaAtendimento_CntSrcCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_12( A1183AgendaAtendimento_CntSrcCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_13") == 0 )
         {
            A1209AgendaAtendimento_CodDmn = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1209AgendaAtendimento_CodDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A1209AgendaAtendimento_CodDmn), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_13( A1209AgendaAtendimento_CodDmn) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7AgendaAtendimento_CntSrcCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7AgendaAtendimento_CntSrcCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7AgendaAtendimento_CntSrcCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vAGENDAATENDIMENTO_CNTSRCCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7AgendaAtendimento_CntSrcCod), "ZZZZZ9")));
               AV8AgendaAtendimento_Data = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8AgendaAtendimento_Data", context.localUtil.Format(AV8AgendaAtendimento_Data, "99/99/99"));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vAGENDAATENDIMENTO_DATA", GetSecureSignedToken( "", AV8AgendaAtendimento_Data));
               AV12AgendaAtendimento_CodDmn = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12AgendaAtendimento_CodDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12AgendaAtendimento_CodDmn), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vAGENDAATENDIMENTO_CODDMN", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV12AgendaAtendimento_CodDmn), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Agenda Atendimento", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtAgendaAtendimento_Data_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public agendaatendimento( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public agendaatendimento( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_AgendaAtendimento_CntSrcCod ,
                           DateTime aP2_AgendaAtendimento_Data ,
                           int aP3_AgendaAtendimento_CodDmn )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7AgendaAtendimento_CntSrcCod = aP1_AgendaAtendimento_CntSrcCod;
         this.AV8AgendaAtendimento_Data = aP2_AgendaAtendimento_Data;
         this.AV12AgendaAtendimento_CodDmn = aP3_AgendaAtendimento_CodDmn;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_3A142( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_3A142e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtAgendaAtendimento_CntSrcCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1183AgendaAtendimento_CntSrcCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A1183AgendaAtendimento_CntSrcCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAgendaAtendimento_CntSrcCod_Jsonclick, 0, "Attribute", "", "", "", edtAgendaAtendimento_CntSrcCod_Visible, edtAgendaAtendimento_CntSrcCod_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_AgendaAtendimento.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_3A142( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_3A142( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_3A142e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_31_3A142( true) ;
         }
         return  ;
      }

      protected void wb_table3_31_3A142e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_3A142e( true) ;
         }
         else
         {
            wb_table1_2_3A142e( false) ;
         }
      }

      protected void wb_table3_31_3A142( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_AgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_AgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_AgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_31_3A142e( true) ;
         }
         else
         {
            wb_table3_31_3A142e( false) ;
         }
      }

      protected void wb_table2_5_3A142( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_3A142( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_3A142e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_3A142e( true) ;
         }
         else
         {
            wb_table2_5_3A142e( false) ;
         }
      }

      protected void wb_table4_13_3A142( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockagendaatendimento_data_Internalname, "Data", "", "", lblTextblockagendaatendimento_data_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtAgendaAtendimento_Data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtAgendaAtendimento_Data_Internalname, context.localUtil.Format(A1184AgendaAtendimento_Data, "99/99/99"), context.localUtil.Format( A1184AgendaAtendimento_Data, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAgendaAtendimento_Data_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtAgendaAtendimento_Data_Enabled, 1, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_AgendaAtendimento.htm");
            GxWebStd.gx_bitmap( context, edtAgendaAtendimento_Data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtAgendaAtendimento_Data_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_AgendaAtendimento.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockagendaatendimento_coddmn_Internalname, "Codigo", "", "", lblTextblockagendaatendimento_coddmn_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtAgendaAtendimento_CodDmn_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1209AgendaAtendimento_CodDmn), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A1209AgendaAtendimento_CodDmn), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,23);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAgendaAtendimento_CodDmn_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtAgendaAtendimento_CodDmn_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_AgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockagendaatendimento_qtdund_Internalname, "Unidades", "", "", lblTextblockagendaatendimento_qtdund_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtAgendaAtendimento_QtdUnd_Internalname, StringUtil.LTrim( StringUtil.NToC( A1186AgendaAtendimento_QtdUnd, 14, 5, ",", "")), ((edtAgendaAtendimento_QtdUnd_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1186AgendaAtendimento_QtdUnd, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A1186AgendaAtendimento_QtdUnd, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,28);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAgendaAtendimento_QtdUnd_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtAgendaAtendimento_QtdUnd_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_AgendaAtendimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_3A142e( true) ;
         }
         else
         {
            wb_table4_13_3A142e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E113A2 */
         E113A2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( context.localUtil.VCDate( cgiGet( edtAgendaAtendimento_Data_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data"}), 1, "AGENDAATENDIMENTO_DATA");
                  AnyError = 1;
                  GX_FocusControl = edtAgendaAtendimento_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1184AgendaAtendimento_Data = DateTime.MinValue;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1184AgendaAtendimento_Data", context.localUtil.Format(A1184AgendaAtendimento_Data, "99/99/99"));
               }
               else
               {
                  A1184AgendaAtendimento_Data = context.localUtil.CToD( cgiGet( edtAgendaAtendimento_Data_Internalname), 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1184AgendaAtendimento_Data", context.localUtil.Format(A1184AgendaAtendimento_Data, "99/99/99"));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtAgendaAtendimento_CodDmn_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtAgendaAtendimento_CodDmn_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "AGENDAATENDIMENTO_CODDMN");
                  AnyError = 1;
                  GX_FocusControl = edtAgendaAtendimento_CodDmn_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1209AgendaAtendimento_CodDmn = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1209AgendaAtendimento_CodDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A1209AgendaAtendimento_CodDmn), 6, 0)));
               }
               else
               {
                  A1209AgendaAtendimento_CodDmn = (int)(context.localUtil.CToN( cgiGet( edtAgendaAtendimento_CodDmn_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1209AgendaAtendimento_CodDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A1209AgendaAtendimento_CodDmn), 6, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtAgendaAtendimento_QtdUnd_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtAgendaAtendimento_QtdUnd_Internalname), ",", ".") > 99999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "AGENDAATENDIMENTO_QTDUND");
                  AnyError = 1;
                  GX_FocusControl = edtAgendaAtendimento_QtdUnd_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1186AgendaAtendimento_QtdUnd = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1186AgendaAtendimento_QtdUnd", StringUtil.LTrim( StringUtil.Str( A1186AgendaAtendimento_QtdUnd, 14, 5)));
               }
               else
               {
                  A1186AgendaAtendimento_QtdUnd = context.localUtil.CToN( cgiGet( edtAgendaAtendimento_QtdUnd_Internalname), ",", ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1186AgendaAtendimento_QtdUnd", StringUtil.LTrim( StringUtil.Str( A1186AgendaAtendimento_QtdUnd, 14, 5)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtAgendaAtendimento_CntSrcCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtAgendaAtendimento_CntSrcCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "AGENDAATENDIMENTO_CNTSRCCOD");
                  AnyError = 1;
                  GX_FocusControl = edtAgendaAtendimento_CntSrcCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1183AgendaAtendimento_CntSrcCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1183AgendaAtendimento_CntSrcCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1183AgendaAtendimento_CntSrcCod), 6, 0)));
               }
               else
               {
                  A1183AgendaAtendimento_CntSrcCod = (int)(context.localUtil.CToN( cgiGet( edtAgendaAtendimento_CntSrcCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1183AgendaAtendimento_CntSrcCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1183AgendaAtendimento_CntSrcCod), 6, 0)));
               }
               /* Read saved values. */
               Z1183AgendaAtendimento_CntSrcCod = (int)(context.localUtil.CToN( cgiGet( "Z1183AgendaAtendimento_CntSrcCod"), ",", "."));
               Z1184AgendaAtendimento_Data = context.localUtil.CToD( cgiGet( "Z1184AgendaAtendimento_Data"), 0);
               Z1209AgendaAtendimento_CodDmn = (int)(context.localUtil.CToN( cgiGet( "Z1209AgendaAtendimento_CodDmn"), ",", "."));
               Z1186AgendaAtendimento_QtdUnd = context.localUtil.CToN( cgiGet( "Z1186AgendaAtendimento_QtdUnd"), ",", ".");
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               AV7AgendaAtendimento_CntSrcCod = (int)(context.localUtil.CToN( cgiGet( "vAGENDAATENDIMENTO_CNTSRCCOD"), ",", "."));
               AV8AgendaAtendimento_Data = context.localUtil.CToD( cgiGet( "vAGENDAATENDIMENTO_DATA"), 0);
               AV12AgendaAtendimento_CodDmn = (int)(context.localUtil.CToN( cgiGet( "vAGENDAATENDIMENTO_CODDMN"), ",", "."));
               A1194AgendaAtendimento_UnidadeContratada = (int)(context.localUtil.CToN( cgiGet( "AGENDAATENDIMENTO_UNIDADECONTRATADA"), ",", "."));
               n1194AgendaAtendimento_UnidadeContratada = false;
               A457ContagemResultado_Demanda = cgiGet( "CONTAGEMRESULTADO_DEMANDA");
               n457ContagemResultado_Demanda = false;
               A493ContagemResultado_DemandaFM = cgiGet( "CONTAGEMRESULTADO_DEMANDAFM");
               n493ContagemResultado_DemandaFM = false;
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "AgendaAtendimento";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A1183AgendaAtendimento_CntSrcCod != Z1183AgendaAtendimento_CntSrcCod ) || ( A1184AgendaAtendimento_Data != Z1184AgendaAtendimento_Data ) || ( A1209AgendaAtendimento_CodDmn != Z1209AgendaAtendimento_CodDmn ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("agendaatendimento:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A1183AgendaAtendimento_CntSrcCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1183AgendaAtendimento_CntSrcCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1183AgendaAtendimento_CntSrcCod), 6, 0)));
                  A1184AgendaAtendimento_Data = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1184AgendaAtendimento_Data", context.localUtil.Format(A1184AgendaAtendimento_Data, "99/99/99"));
                  A1209AgendaAtendimento_CodDmn = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1209AgendaAtendimento_CodDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A1209AgendaAtendimento_CodDmn), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode142 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode142;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound142 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_3A0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "AGENDAATENDIMENTO_CNTSRCCOD");
                        AnyError = 1;
                        GX_FocusControl = edtAgendaAtendimento_CntSrcCod_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E113A2 */
                           E113A2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E123A2 */
                           E123A2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E123A2 */
            E123A2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll3A142( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes3A142( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_3A0( )
      {
         BeforeValidate3A142( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls3A142( ) ;
            }
            else
            {
               CheckExtendedTable3A142( ) ;
               CloseExtendedTableCursors3A142( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption3A0( )
      {
      }

      protected void E113A2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         AV10TrnContext.FromXml(AV11WebSession.Get("TrnContext"), "");
         edtAgendaAtendimento_CntSrcCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAgendaAtendimento_CntSrcCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAgendaAtendimento_CntSrcCod_Visible), 5, 0)));
      }

      protected void E123A2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV10TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwagendaatendimento.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM3A142( short GX_JID )
      {
         if ( ( GX_JID == 11 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1186AgendaAtendimento_QtdUnd = T003A3_A1186AgendaAtendimento_QtdUnd[0];
            }
            else
            {
               Z1186AgendaAtendimento_QtdUnd = A1186AgendaAtendimento_QtdUnd;
            }
         }
         if ( GX_JID == -11 )
         {
            Z1184AgendaAtendimento_Data = A1184AgendaAtendimento_Data;
            Z1186AgendaAtendimento_QtdUnd = A1186AgendaAtendimento_QtdUnd;
            Z1183AgendaAtendimento_CntSrcCod = A1183AgendaAtendimento_CntSrcCod;
            Z1209AgendaAtendimento_CodDmn = A1209AgendaAtendimento_CodDmn;
            Z1194AgendaAtendimento_UnidadeContratada = A1194AgendaAtendimento_UnidadeContratada;
            Z457ContagemResultado_Demanda = A457ContagemResultado_Demanda;
            Z493ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
         }
      }

      protected void standaloneNotModal( )
      {
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7AgendaAtendimento_CntSrcCod) )
         {
            A1183AgendaAtendimento_CntSrcCod = AV7AgendaAtendimento_CntSrcCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1183AgendaAtendimento_CntSrcCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1183AgendaAtendimento_CntSrcCod), 6, 0)));
         }
         if ( ! (0==AV7AgendaAtendimento_CntSrcCod) )
         {
            edtAgendaAtendimento_CntSrcCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAgendaAtendimento_CntSrcCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAgendaAtendimento_CntSrcCod_Enabled), 5, 0)));
         }
         else
         {
            edtAgendaAtendimento_CntSrcCod_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAgendaAtendimento_CntSrcCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAgendaAtendimento_CntSrcCod_Enabled), 5, 0)));
         }
         if ( ! (0==AV7AgendaAtendimento_CntSrcCod) )
         {
            edtAgendaAtendimento_CntSrcCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAgendaAtendimento_CntSrcCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAgendaAtendimento_CntSrcCod_Enabled), 5, 0)));
         }
         if ( ! (DateTime.MinValue==AV8AgendaAtendimento_Data) )
         {
            A1184AgendaAtendimento_Data = AV8AgendaAtendimento_Data;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1184AgendaAtendimento_Data", context.localUtil.Format(A1184AgendaAtendimento_Data, "99/99/99"));
         }
         if ( ! (DateTime.MinValue==AV8AgendaAtendimento_Data) )
         {
            edtAgendaAtendimento_Data_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAgendaAtendimento_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAgendaAtendimento_Data_Enabled), 5, 0)));
         }
         else
         {
            edtAgendaAtendimento_Data_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAgendaAtendimento_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAgendaAtendimento_Data_Enabled), 5, 0)));
         }
         if ( ! (DateTime.MinValue==AV8AgendaAtendimento_Data) )
         {
            edtAgendaAtendimento_Data_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAgendaAtendimento_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAgendaAtendimento_Data_Enabled), 5, 0)));
         }
         if ( ! (0==AV12AgendaAtendimento_CodDmn) )
         {
            A1209AgendaAtendimento_CodDmn = AV12AgendaAtendimento_CodDmn;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1209AgendaAtendimento_CodDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A1209AgendaAtendimento_CodDmn), 6, 0)));
         }
         if ( ! (0==AV12AgendaAtendimento_CodDmn) )
         {
            edtAgendaAtendimento_CodDmn_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAgendaAtendimento_CodDmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAgendaAtendimento_CodDmn_Enabled), 5, 0)));
         }
         else
         {
            edtAgendaAtendimento_CodDmn_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAgendaAtendimento_CodDmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAgendaAtendimento_CodDmn_Enabled), 5, 0)));
         }
         if ( ! (0==AV12AgendaAtendimento_CodDmn) )
         {
            edtAgendaAtendimento_CodDmn_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAgendaAtendimento_CodDmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAgendaAtendimento_CodDmn_Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T003A4 */
            pr_default.execute(2, new Object[] {A1183AgendaAtendimento_CntSrcCod});
            A1194AgendaAtendimento_UnidadeContratada = T003A4_A1194AgendaAtendimento_UnidadeContratada[0];
            n1194AgendaAtendimento_UnidadeContratada = T003A4_n1194AgendaAtendimento_UnidadeContratada[0];
            pr_default.close(2);
            /* Using cursor T003A5 */
            pr_default.execute(3, new Object[] {A1209AgendaAtendimento_CodDmn});
            A457ContagemResultado_Demanda = T003A5_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = T003A5_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = T003A5_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = T003A5_n493ContagemResultado_DemandaFM[0];
            pr_default.close(3);
         }
      }

      protected void Load3A142( )
      {
         /* Using cursor T003A6 */
         pr_default.execute(4, new Object[] {A1183AgendaAtendimento_CntSrcCod, A1184AgendaAtendimento_Data, A1209AgendaAtendimento_CodDmn});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound142 = 1;
            A1186AgendaAtendimento_QtdUnd = T003A6_A1186AgendaAtendimento_QtdUnd[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1186AgendaAtendimento_QtdUnd", StringUtil.LTrim( StringUtil.Str( A1186AgendaAtendimento_QtdUnd, 14, 5)));
            A457ContagemResultado_Demanda = T003A6_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = T003A6_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = T003A6_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = T003A6_n493ContagemResultado_DemandaFM[0];
            A1194AgendaAtendimento_UnidadeContratada = T003A6_A1194AgendaAtendimento_UnidadeContratada[0];
            n1194AgendaAtendimento_UnidadeContratada = T003A6_n1194AgendaAtendimento_UnidadeContratada[0];
            ZM3A142( -11) ;
         }
         pr_default.close(4);
         OnLoadActions3A142( ) ;
      }

      protected void OnLoadActions3A142( )
      {
      }

      protected void CheckExtendedTable3A142( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T003A4 */
         pr_default.execute(2, new Object[] {A1183AgendaAtendimento_CntSrcCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Agenda Atendimento_Contrato Servicos'.", "ForeignKeyNotFound", 1, "AGENDAATENDIMENTO_CNTSRCCOD");
            AnyError = 1;
            GX_FocusControl = edtAgendaAtendimento_CntSrcCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1194AgendaAtendimento_UnidadeContratada = T003A4_A1194AgendaAtendimento_UnidadeContratada[0];
         n1194AgendaAtendimento_UnidadeContratada = T003A4_n1194AgendaAtendimento_UnidadeContratada[0];
         pr_default.close(2);
         if ( ! ( (DateTime.MinValue==A1184AgendaAtendimento_Data) || ( A1184AgendaAtendimento_Data >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Data fora do intervalo", "OutOfRange", 1, "AGENDAATENDIMENTO_DATA");
            AnyError = 1;
            GX_FocusControl = edtAgendaAtendimento_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T003A5 */
         pr_default.execute(3, new Object[] {A1209AgendaAtendimento_CodDmn});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Agenda Atendimento_Contagem Resultado'.", "ForeignKeyNotFound", 1, "AGENDAATENDIMENTO_CODDMN");
            AnyError = 1;
            GX_FocusControl = edtAgendaAtendimento_CodDmn_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A457ContagemResultado_Demanda = T003A5_A457ContagemResultado_Demanda[0];
         n457ContagemResultado_Demanda = T003A5_n457ContagemResultado_Demanda[0];
         A493ContagemResultado_DemandaFM = T003A5_A493ContagemResultado_DemandaFM[0];
         n493ContagemResultado_DemandaFM = T003A5_n493ContagemResultado_DemandaFM[0];
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors3A142( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_12( int A1183AgendaAtendimento_CntSrcCod )
      {
         /* Using cursor T003A7 */
         pr_default.execute(5, new Object[] {A1183AgendaAtendimento_CntSrcCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Agenda Atendimento_Contrato Servicos'.", "ForeignKeyNotFound", 1, "AGENDAATENDIMENTO_CNTSRCCOD");
            AnyError = 1;
            GX_FocusControl = edtAgendaAtendimento_CntSrcCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1194AgendaAtendimento_UnidadeContratada = T003A7_A1194AgendaAtendimento_UnidadeContratada[0];
         n1194AgendaAtendimento_UnidadeContratada = T003A7_n1194AgendaAtendimento_UnidadeContratada[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1194AgendaAtendimento_UnidadeContratada), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void gxLoad_13( int A1209AgendaAtendimento_CodDmn )
      {
         /* Using cursor T003A8 */
         pr_default.execute(6, new Object[] {A1209AgendaAtendimento_CodDmn});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Agenda Atendimento_Contagem Resultado'.", "ForeignKeyNotFound", 1, "AGENDAATENDIMENTO_CODDMN");
            AnyError = 1;
            GX_FocusControl = edtAgendaAtendimento_CodDmn_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A457ContagemResultado_Demanda = T003A8_A457ContagemResultado_Demanda[0];
         n457ContagemResultado_Demanda = T003A8_n457ContagemResultado_Demanda[0];
         A493ContagemResultado_DemandaFM = T003A8_A493ContagemResultado_DemandaFM[0];
         n493ContagemResultado_DemandaFM = T003A8_n493ContagemResultado_DemandaFM[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A457ContagemResultado_Demanda)+"\""+","+"\""+GXUtil.EncodeJSConstant( A493ContagemResultado_DemandaFM)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void GetKey3A142( )
      {
         /* Using cursor T003A9 */
         pr_default.execute(7, new Object[] {A1183AgendaAtendimento_CntSrcCod, A1184AgendaAtendimento_Data, A1209AgendaAtendimento_CodDmn});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound142 = 1;
         }
         else
         {
            RcdFound142 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T003A3 */
         pr_default.execute(1, new Object[] {A1183AgendaAtendimento_CntSrcCod, A1184AgendaAtendimento_Data, A1209AgendaAtendimento_CodDmn});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM3A142( 11) ;
            RcdFound142 = 1;
            A1184AgendaAtendimento_Data = T003A3_A1184AgendaAtendimento_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1184AgendaAtendimento_Data", context.localUtil.Format(A1184AgendaAtendimento_Data, "99/99/99"));
            A1186AgendaAtendimento_QtdUnd = T003A3_A1186AgendaAtendimento_QtdUnd[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1186AgendaAtendimento_QtdUnd", StringUtil.LTrim( StringUtil.Str( A1186AgendaAtendimento_QtdUnd, 14, 5)));
            A1183AgendaAtendimento_CntSrcCod = T003A3_A1183AgendaAtendimento_CntSrcCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1183AgendaAtendimento_CntSrcCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1183AgendaAtendimento_CntSrcCod), 6, 0)));
            A1209AgendaAtendimento_CodDmn = T003A3_A1209AgendaAtendimento_CodDmn[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1209AgendaAtendimento_CodDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A1209AgendaAtendimento_CodDmn), 6, 0)));
            Z1183AgendaAtendimento_CntSrcCod = A1183AgendaAtendimento_CntSrcCod;
            Z1184AgendaAtendimento_Data = A1184AgendaAtendimento_Data;
            Z1209AgendaAtendimento_CodDmn = A1209AgendaAtendimento_CodDmn;
            sMode142 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load3A142( ) ;
            if ( AnyError == 1 )
            {
               RcdFound142 = 0;
               InitializeNonKey3A142( ) ;
            }
            Gx_mode = sMode142;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound142 = 0;
            InitializeNonKey3A142( ) ;
            sMode142 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode142;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey3A142( ) ;
         if ( RcdFound142 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound142 = 0;
         /* Using cursor T003A10 */
         pr_default.execute(8, new Object[] {A1183AgendaAtendimento_CntSrcCod, A1184AgendaAtendimento_Data, A1209AgendaAtendimento_CodDmn});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T003A10_A1183AgendaAtendimento_CntSrcCod[0] < A1183AgendaAtendimento_CntSrcCod ) || ( T003A10_A1183AgendaAtendimento_CntSrcCod[0] == A1183AgendaAtendimento_CntSrcCod ) && ( T003A10_A1184AgendaAtendimento_Data[0] < A1184AgendaAtendimento_Data ) || ( T003A10_A1184AgendaAtendimento_Data[0] == A1184AgendaAtendimento_Data ) && ( T003A10_A1183AgendaAtendimento_CntSrcCod[0] == A1183AgendaAtendimento_CntSrcCod ) && ( T003A10_A1209AgendaAtendimento_CodDmn[0] < A1209AgendaAtendimento_CodDmn ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T003A10_A1183AgendaAtendimento_CntSrcCod[0] > A1183AgendaAtendimento_CntSrcCod ) || ( T003A10_A1183AgendaAtendimento_CntSrcCod[0] == A1183AgendaAtendimento_CntSrcCod ) && ( T003A10_A1184AgendaAtendimento_Data[0] > A1184AgendaAtendimento_Data ) || ( T003A10_A1184AgendaAtendimento_Data[0] == A1184AgendaAtendimento_Data ) && ( T003A10_A1183AgendaAtendimento_CntSrcCod[0] == A1183AgendaAtendimento_CntSrcCod ) && ( T003A10_A1209AgendaAtendimento_CodDmn[0] > A1209AgendaAtendimento_CodDmn ) ) )
            {
               A1183AgendaAtendimento_CntSrcCod = T003A10_A1183AgendaAtendimento_CntSrcCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1183AgendaAtendimento_CntSrcCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1183AgendaAtendimento_CntSrcCod), 6, 0)));
               A1184AgendaAtendimento_Data = T003A10_A1184AgendaAtendimento_Data[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1184AgendaAtendimento_Data", context.localUtil.Format(A1184AgendaAtendimento_Data, "99/99/99"));
               A1209AgendaAtendimento_CodDmn = T003A10_A1209AgendaAtendimento_CodDmn[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1209AgendaAtendimento_CodDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A1209AgendaAtendimento_CodDmn), 6, 0)));
               RcdFound142 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void move_previous( )
      {
         RcdFound142 = 0;
         /* Using cursor T003A11 */
         pr_default.execute(9, new Object[] {A1183AgendaAtendimento_CntSrcCod, A1184AgendaAtendimento_Data, A1209AgendaAtendimento_CodDmn});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T003A11_A1183AgendaAtendimento_CntSrcCod[0] > A1183AgendaAtendimento_CntSrcCod ) || ( T003A11_A1183AgendaAtendimento_CntSrcCod[0] == A1183AgendaAtendimento_CntSrcCod ) && ( T003A11_A1184AgendaAtendimento_Data[0] > A1184AgendaAtendimento_Data ) || ( T003A11_A1184AgendaAtendimento_Data[0] == A1184AgendaAtendimento_Data ) && ( T003A11_A1183AgendaAtendimento_CntSrcCod[0] == A1183AgendaAtendimento_CntSrcCod ) && ( T003A11_A1209AgendaAtendimento_CodDmn[0] > A1209AgendaAtendimento_CodDmn ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T003A11_A1183AgendaAtendimento_CntSrcCod[0] < A1183AgendaAtendimento_CntSrcCod ) || ( T003A11_A1183AgendaAtendimento_CntSrcCod[0] == A1183AgendaAtendimento_CntSrcCod ) && ( T003A11_A1184AgendaAtendimento_Data[0] < A1184AgendaAtendimento_Data ) || ( T003A11_A1184AgendaAtendimento_Data[0] == A1184AgendaAtendimento_Data ) && ( T003A11_A1183AgendaAtendimento_CntSrcCod[0] == A1183AgendaAtendimento_CntSrcCod ) && ( T003A11_A1209AgendaAtendimento_CodDmn[0] < A1209AgendaAtendimento_CodDmn ) ) )
            {
               A1183AgendaAtendimento_CntSrcCod = T003A11_A1183AgendaAtendimento_CntSrcCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1183AgendaAtendimento_CntSrcCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1183AgendaAtendimento_CntSrcCod), 6, 0)));
               A1184AgendaAtendimento_Data = T003A11_A1184AgendaAtendimento_Data[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1184AgendaAtendimento_Data", context.localUtil.Format(A1184AgendaAtendimento_Data, "99/99/99"));
               A1209AgendaAtendimento_CodDmn = T003A11_A1209AgendaAtendimento_CodDmn[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1209AgendaAtendimento_CodDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A1209AgendaAtendimento_CodDmn), 6, 0)));
               RcdFound142 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey3A142( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtAgendaAtendimento_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert3A142( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound142 == 1 )
            {
               if ( ( A1183AgendaAtendimento_CntSrcCod != Z1183AgendaAtendimento_CntSrcCod ) || ( A1184AgendaAtendimento_Data != Z1184AgendaAtendimento_Data ) || ( A1209AgendaAtendimento_CodDmn != Z1209AgendaAtendimento_CodDmn ) )
               {
                  A1183AgendaAtendimento_CntSrcCod = Z1183AgendaAtendimento_CntSrcCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1183AgendaAtendimento_CntSrcCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1183AgendaAtendimento_CntSrcCod), 6, 0)));
                  A1184AgendaAtendimento_Data = Z1184AgendaAtendimento_Data;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1184AgendaAtendimento_Data", context.localUtil.Format(A1184AgendaAtendimento_Data, "99/99/99"));
                  A1209AgendaAtendimento_CodDmn = Z1209AgendaAtendimento_CodDmn;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1209AgendaAtendimento_CodDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A1209AgendaAtendimento_CodDmn), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "AGENDAATENDIMENTO_CNTSRCCOD");
                  AnyError = 1;
                  GX_FocusControl = edtAgendaAtendimento_CntSrcCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtAgendaAtendimento_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update3A142( ) ;
                  GX_FocusControl = edtAgendaAtendimento_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A1183AgendaAtendimento_CntSrcCod != Z1183AgendaAtendimento_CntSrcCod ) || ( A1184AgendaAtendimento_Data != Z1184AgendaAtendimento_Data ) || ( A1209AgendaAtendimento_CodDmn != Z1209AgendaAtendimento_CodDmn ) )
               {
                  /* Insert record */
                  GX_FocusControl = edtAgendaAtendimento_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert3A142( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "AGENDAATENDIMENTO_CNTSRCCOD");
                     AnyError = 1;
                     GX_FocusControl = edtAgendaAtendimento_CntSrcCod_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtAgendaAtendimento_Data_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert3A142( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( ( A1183AgendaAtendimento_CntSrcCod != Z1183AgendaAtendimento_CntSrcCod ) || ( A1184AgendaAtendimento_Data != Z1184AgendaAtendimento_Data ) || ( A1209AgendaAtendimento_CodDmn != Z1209AgendaAtendimento_CodDmn ) )
         {
            A1183AgendaAtendimento_CntSrcCod = Z1183AgendaAtendimento_CntSrcCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1183AgendaAtendimento_CntSrcCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1183AgendaAtendimento_CntSrcCod), 6, 0)));
            A1184AgendaAtendimento_Data = Z1184AgendaAtendimento_Data;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1184AgendaAtendimento_Data", context.localUtil.Format(A1184AgendaAtendimento_Data, "99/99/99"));
            A1209AgendaAtendimento_CodDmn = Z1209AgendaAtendimento_CodDmn;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1209AgendaAtendimento_CodDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A1209AgendaAtendimento_CodDmn), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "AGENDAATENDIMENTO_CNTSRCCOD");
            AnyError = 1;
            GX_FocusControl = edtAgendaAtendimento_CntSrcCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtAgendaAtendimento_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency3A142( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T003A2 */
            pr_default.execute(0, new Object[] {A1183AgendaAtendimento_CntSrcCod, A1184AgendaAtendimento_Data, A1209AgendaAtendimento_CodDmn});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"AgendaAtendimento"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z1186AgendaAtendimento_QtdUnd != T003A2_A1186AgendaAtendimento_QtdUnd[0] ) )
            {
               if ( Z1186AgendaAtendimento_QtdUnd != T003A2_A1186AgendaAtendimento_QtdUnd[0] )
               {
                  GXUtil.WriteLog("agendaatendimento:[seudo value changed for attri]"+"AgendaAtendimento_QtdUnd");
                  GXUtil.WriteLogRaw("Old: ",Z1186AgendaAtendimento_QtdUnd);
                  GXUtil.WriteLogRaw("Current: ",T003A2_A1186AgendaAtendimento_QtdUnd[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"AgendaAtendimento"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert3A142( )
      {
         BeforeValidate3A142( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3A142( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM3A142( 0) ;
            CheckOptimisticConcurrency3A142( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3A142( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert3A142( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003A12 */
                     pr_default.execute(10, new Object[] {A1184AgendaAtendimento_Data, A1186AgendaAtendimento_QtdUnd, A1183AgendaAtendimento_CntSrcCod, A1209AgendaAtendimento_CodDmn});
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("AgendaAtendimento") ;
                     if ( (pr_default.getStatus(10) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption3A0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load3A142( ) ;
            }
            EndLevel3A142( ) ;
         }
         CloseExtendedTableCursors3A142( ) ;
      }

      protected void Update3A142( )
      {
         BeforeValidate3A142( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3A142( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3A142( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3A142( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate3A142( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003A13 */
                     pr_default.execute(11, new Object[] {A1186AgendaAtendimento_QtdUnd, A1183AgendaAtendimento_CntSrcCod, A1184AgendaAtendimento_Data, A1209AgendaAtendimento_CodDmn});
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("AgendaAtendimento") ;
                     if ( (pr_default.getStatus(11) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"AgendaAtendimento"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate3A142( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel3A142( ) ;
         }
         CloseExtendedTableCursors3A142( ) ;
      }

      protected void DeferredUpdate3A142( )
      {
      }

      protected void delete( )
      {
         BeforeValidate3A142( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3A142( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls3A142( ) ;
            AfterConfirm3A142( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete3A142( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T003A14 */
                  pr_default.execute(12, new Object[] {A1183AgendaAtendimento_CntSrcCod, A1184AgendaAtendimento_Data, A1209AgendaAtendimento_CodDmn});
                  pr_default.close(12);
                  dsDefault.SmartCacheProvider.SetUpdated("AgendaAtendimento") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode142 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel3A142( ) ;
         Gx_mode = sMode142;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls3A142( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T003A15 */
            pr_default.execute(13, new Object[] {A1183AgendaAtendimento_CntSrcCod});
            A1194AgendaAtendimento_UnidadeContratada = T003A15_A1194AgendaAtendimento_UnidadeContratada[0];
            n1194AgendaAtendimento_UnidadeContratada = T003A15_n1194AgendaAtendimento_UnidadeContratada[0];
            pr_default.close(13);
            /* Using cursor T003A16 */
            pr_default.execute(14, new Object[] {A1209AgendaAtendimento_CodDmn});
            A457ContagemResultado_Demanda = T003A16_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = T003A16_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = T003A16_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = T003A16_n493ContagemResultado_DemandaFM[0];
            pr_default.close(14);
         }
      }

      protected void EndLevel3A142( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete3A142( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(13);
            pr_default.close(14);
            context.CommitDataStores( "AgendaAtendimento");
            if ( AnyError == 0 )
            {
               ConfirmValues3A0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(13);
            pr_default.close(14);
            context.RollbackDataStores( "AgendaAtendimento");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart3A142( )
      {
         /* Scan By routine */
         /* Using cursor T003A17 */
         pr_default.execute(15);
         RcdFound142 = 0;
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound142 = 1;
            A1183AgendaAtendimento_CntSrcCod = T003A17_A1183AgendaAtendimento_CntSrcCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1183AgendaAtendimento_CntSrcCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1183AgendaAtendimento_CntSrcCod), 6, 0)));
            A1184AgendaAtendimento_Data = T003A17_A1184AgendaAtendimento_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1184AgendaAtendimento_Data", context.localUtil.Format(A1184AgendaAtendimento_Data, "99/99/99"));
            A1209AgendaAtendimento_CodDmn = T003A17_A1209AgendaAtendimento_CodDmn[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1209AgendaAtendimento_CodDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A1209AgendaAtendimento_CodDmn), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext3A142( )
      {
         /* Scan next routine */
         pr_default.readNext(15);
         RcdFound142 = 0;
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound142 = 1;
            A1183AgendaAtendimento_CntSrcCod = T003A17_A1183AgendaAtendimento_CntSrcCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1183AgendaAtendimento_CntSrcCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1183AgendaAtendimento_CntSrcCod), 6, 0)));
            A1184AgendaAtendimento_Data = T003A17_A1184AgendaAtendimento_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1184AgendaAtendimento_Data", context.localUtil.Format(A1184AgendaAtendimento_Data, "99/99/99"));
            A1209AgendaAtendimento_CodDmn = T003A17_A1209AgendaAtendimento_CodDmn[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1209AgendaAtendimento_CodDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A1209AgendaAtendimento_CodDmn), 6, 0)));
         }
      }

      protected void ScanEnd3A142( )
      {
         pr_default.close(15);
      }

      protected void AfterConfirm3A142( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert3A142( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate3A142( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete3A142( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete3A142( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate3A142( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes3A142( )
      {
         edtAgendaAtendimento_Data_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAgendaAtendimento_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAgendaAtendimento_Data_Enabled), 5, 0)));
         edtAgendaAtendimento_CodDmn_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAgendaAtendimento_CodDmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAgendaAtendimento_CodDmn_Enabled), 5, 0)));
         edtAgendaAtendimento_QtdUnd_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAgendaAtendimento_QtdUnd_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAgendaAtendimento_QtdUnd_Enabled), 5, 0)));
         edtAgendaAtendimento_CntSrcCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAgendaAtendimento_CntSrcCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAgendaAtendimento_CntSrcCod_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues3A0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202031221104271");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("agendaatendimento.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7AgendaAtendimento_CntSrcCod) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV8AgendaAtendimento_Data)) + "," + UrlEncode("" +AV12AgendaAtendimento_CodDmn)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1183AgendaAtendimento_CntSrcCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1183AgendaAtendimento_CntSrcCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1184AgendaAtendimento_Data", context.localUtil.DToC( Z1184AgendaAtendimento_Data, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z1209AgendaAtendimento_CodDmn", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1209AgendaAtendimento_CodDmn), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1186AgendaAtendimento_QtdUnd", StringUtil.LTrim( StringUtil.NToC( Z1186AgendaAtendimento_QtdUnd, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV10TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV10TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vAGENDAATENDIMENTO_CNTSRCCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7AgendaAtendimento_CntSrcCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vAGENDAATENDIMENTO_DATA", context.localUtil.DToC( AV8AgendaAtendimento_Data, 0, "/"));
         GxWebStd.gx_hidden_field( context, "vAGENDAATENDIMENTO_CODDMN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12AgendaAtendimento_CodDmn), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "AGENDAATENDIMENTO_UNIDADECONTRATADA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1194AgendaAtendimento_UnidadeContratada), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DEMANDA", A457ContagemResultado_Demanda);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DEMANDAFM", A493ContagemResultado_DemandaFM);
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vAGENDAATENDIMENTO_CNTSRCCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7AgendaAtendimento_CntSrcCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vAGENDAATENDIMENTO_DATA", GetSecureSignedToken( "", AV8AgendaAtendimento_Data));
         GxWebStd.gx_hidden_field( context, "gxhash_vAGENDAATENDIMENTO_CODDMN", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV12AgendaAtendimento_CodDmn), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "AgendaAtendimento";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("agendaatendimento:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("agendaatendimento.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7AgendaAtendimento_CntSrcCod) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV8AgendaAtendimento_Data)) + "," + UrlEncode("" +AV12AgendaAtendimento_CodDmn) ;
      }

      public override String GetPgmname( )
      {
         return "AgendaAtendimento" ;
      }

      public override String GetPgmdesc( )
      {
         return "Agenda Atendimento" ;
      }

      protected void InitializeNonKey3A142( )
      {
         A1186AgendaAtendimento_QtdUnd = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1186AgendaAtendimento_QtdUnd", StringUtil.LTrim( StringUtil.Str( A1186AgendaAtendimento_QtdUnd, 14, 5)));
         A457ContagemResultado_Demanda = "";
         n457ContagemResultado_Demanda = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A457ContagemResultado_Demanda", A457ContagemResultado_Demanda);
         A493ContagemResultado_DemandaFM = "";
         n493ContagemResultado_DemandaFM = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A493ContagemResultado_DemandaFM", A493ContagemResultado_DemandaFM);
         A1194AgendaAtendimento_UnidadeContratada = 0;
         n1194AgendaAtendimento_UnidadeContratada = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1194AgendaAtendimento_UnidadeContratada", StringUtil.LTrim( StringUtil.Str( (decimal)(A1194AgendaAtendimento_UnidadeContratada), 6, 0)));
         Z1186AgendaAtendimento_QtdUnd = 0;
      }

      protected void InitAll3A142( )
      {
         A1183AgendaAtendimento_CntSrcCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1183AgendaAtendimento_CntSrcCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1183AgendaAtendimento_CntSrcCod), 6, 0)));
         A1184AgendaAtendimento_Data = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1184AgendaAtendimento_Data", context.localUtil.Format(A1184AgendaAtendimento_Data, "99/99/99"));
         A1209AgendaAtendimento_CodDmn = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1209AgendaAtendimento_CodDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A1209AgendaAtendimento_CodDmn), 6, 0)));
         InitializeNonKey3A142( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202031221104288");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("agendaatendimento.js", "?202031221104288");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockagendaatendimento_data_Internalname = "TEXTBLOCKAGENDAATENDIMENTO_DATA";
         edtAgendaAtendimento_Data_Internalname = "AGENDAATENDIMENTO_DATA";
         lblTextblockagendaatendimento_coddmn_Internalname = "TEXTBLOCKAGENDAATENDIMENTO_CODDMN";
         edtAgendaAtendimento_CodDmn_Internalname = "AGENDAATENDIMENTO_CODDMN";
         lblTextblockagendaatendimento_qtdund_Internalname = "TEXTBLOCKAGENDAATENDIMENTO_QTDUND";
         edtAgendaAtendimento_QtdUnd_Internalname = "AGENDAATENDIMENTO_QTDUND";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtAgendaAtendimento_CntSrcCod_Internalname = "AGENDAATENDIMENTO_CNTSRCCOD";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Agenda de Atendimento";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Agenda Atendimento";
         edtAgendaAtendimento_QtdUnd_Jsonclick = "";
         edtAgendaAtendimento_QtdUnd_Enabled = 1;
         edtAgendaAtendimento_CodDmn_Jsonclick = "";
         edtAgendaAtendimento_CodDmn_Enabled = 1;
         edtAgendaAtendimento_Data_Jsonclick = "";
         edtAgendaAtendimento_Data_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtAgendaAtendimento_CntSrcCod_Jsonclick = "";
         edtAgendaAtendimento_CntSrcCod_Enabled = 1;
         edtAgendaAtendimento_CntSrcCod_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public void Valid_Agendaatendimento_coddmn( int GX_Parm1 ,
                                                  String GX_Parm2 ,
                                                  String GX_Parm3 )
      {
         A1209AgendaAtendimento_CodDmn = GX_Parm1;
         A457ContagemResultado_Demanda = GX_Parm2;
         n457ContagemResultado_Demanda = false;
         A493ContagemResultado_DemandaFM = GX_Parm3;
         n493ContagemResultado_DemandaFM = false;
         /* Using cursor T003A16 */
         pr_default.execute(14, new Object[] {A1209AgendaAtendimento_CodDmn});
         if ( (pr_default.getStatus(14) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Agenda Atendimento_Contagem Resultado'.", "ForeignKeyNotFound", 1, "AGENDAATENDIMENTO_CODDMN");
            AnyError = 1;
            GX_FocusControl = edtAgendaAtendimento_CodDmn_Internalname;
         }
         A457ContagemResultado_Demanda = T003A16_A457ContagemResultado_Demanda[0];
         n457ContagemResultado_Demanda = T003A16_n457ContagemResultado_Demanda[0];
         A493ContagemResultado_DemandaFM = T003A16_A493ContagemResultado_DemandaFM[0];
         n493ContagemResultado_DemandaFM = T003A16_n493ContagemResultado_DemandaFM[0];
         pr_default.close(14);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A457ContagemResultado_Demanda = "";
            n457ContagemResultado_Demanda = false;
            A493ContagemResultado_DemandaFM = "";
            n493ContagemResultado_DemandaFM = false;
         }
         isValidOutput.Add(A457ContagemResultado_Demanda);
         isValidOutput.Add(A493ContagemResultado_DemandaFM);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Agendaatendimento_cntsrccod( int GX_Parm1 ,
                                                     int GX_Parm2 )
      {
         A1183AgendaAtendimento_CntSrcCod = GX_Parm1;
         A1194AgendaAtendimento_UnidadeContratada = GX_Parm2;
         n1194AgendaAtendimento_UnidadeContratada = false;
         /* Using cursor T003A15 */
         pr_default.execute(13, new Object[] {A1183AgendaAtendimento_CntSrcCod});
         if ( (pr_default.getStatus(13) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Agenda Atendimento_Contrato Servicos'.", "ForeignKeyNotFound", 1, "AGENDAATENDIMENTO_CNTSRCCOD");
            AnyError = 1;
            GX_FocusControl = edtAgendaAtendimento_CntSrcCod_Internalname;
         }
         A1194AgendaAtendimento_UnidadeContratada = T003A15_A1194AgendaAtendimento_UnidadeContratada[0];
         n1194AgendaAtendimento_UnidadeContratada = T003A15_n1194AgendaAtendimento_UnidadeContratada[0];
         pr_default.close(13);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1194AgendaAtendimento_UnidadeContratada = 0;
            n1194AgendaAtendimento_UnidadeContratada = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1194AgendaAtendimento_UnidadeContratada), 6, 0, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7AgendaAtendimento_CntSrcCod',fld:'vAGENDAATENDIMENTO_CNTSRCCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV8AgendaAtendimento_Data',fld:'vAGENDAATENDIMENTO_DATA',pic:'',hsh:true,nv:''},{av:'AV12AgendaAtendimento_CodDmn',fld:'vAGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E123A2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV10TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(13);
         pr_default.close(14);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         wcpOAV8AgendaAtendimento_Data = DateTime.MinValue;
         Z1184AgendaAtendimento_Data = DateTime.MinValue;
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         TempTags = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockagendaatendimento_data_Jsonclick = "";
         A1184AgendaAtendimento_Data = DateTime.MinValue;
         lblTextblockagendaatendimento_coddmn_Jsonclick = "";
         lblTextblockagendaatendimento_qtdund_Jsonclick = "";
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode142 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV10TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11WebSession = context.GetSession();
         Z457ContagemResultado_Demanda = "";
         Z493ContagemResultado_DemandaFM = "";
         T003A4_A1194AgendaAtendimento_UnidadeContratada = new int[1] ;
         T003A4_n1194AgendaAtendimento_UnidadeContratada = new bool[] {false} ;
         T003A5_A457ContagemResultado_Demanda = new String[] {""} ;
         T003A5_n457ContagemResultado_Demanda = new bool[] {false} ;
         T003A5_A493ContagemResultado_DemandaFM = new String[] {""} ;
         T003A5_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         T003A6_A1184AgendaAtendimento_Data = new DateTime[] {DateTime.MinValue} ;
         T003A6_A1186AgendaAtendimento_QtdUnd = new decimal[1] ;
         T003A6_A457ContagemResultado_Demanda = new String[] {""} ;
         T003A6_n457ContagemResultado_Demanda = new bool[] {false} ;
         T003A6_A493ContagemResultado_DemandaFM = new String[] {""} ;
         T003A6_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         T003A6_A1183AgendaAtendimento_CntSrcCod = new int[1] ;
         T003A6_A1209AgendaAtendimento_CodDmn = new int[1] ;
         T003A6_A1194AgendaAtendimento_UnidadeContratada = new int[1] ;
         T003A6_n1194AgendaAtendimento_UnidadeContratada = new bool[] {false} ;
         T003A7_A1194AgendaAtendimento_UnidadeContratada = new int[1] ;
         T003A7_n1194AgendaAtendimento_UnidadeContratada = new bool[] {false} ;
         T003A8_A457ContagemResultado_Demanda = new String[] {""} ;
         T003A8_n457ContagemResultado_Demanda = new bool[] {false} ;
         T003A8_A493ContagemResultado_DemandaFM = new String[] {""} ;
         T003A8_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         T003A9_A1183AgendaAtendimento_CntSrcCod = new int[1] ;
         T003A9_A1184AgendaAtendimento_Data = new DateTime[] {DateTime.MinValue} ;
         T003A9_A1209AgendaAtendimento_CodDmn = new int[1] ;
         T003A3_A1184AgendaAtendimento_Data = new DateTime[] {DateTime.MinValue} ;
         T003A3_A1186AgendaAtendimento_QtdUnd = new decimal[1] ;
         T003A3_A1183AgendaAtendimento_CntSrcCod = new int[1] ;
         T003A3_A1209AgendaAtendimento_CodDmn = new int[1] ;
         T003A10_A1183AgendaAtendimento_CntSrcCod = new int[1] ;
         T003A10_A1184AgendaAtendimento_Data = new DateTime[] {DateTime.MinValue} ;
         T003A10_A1209AgendaAtendimento_CodDmn = new int[1] ;
         T003A11_A1183AgendaAtendimento_CntSrcCod = new int[1] ;
         T003A11_A1184AgendaAtendimento_Data = new DateTime[] {DateTime.MinValue} ;
         T003A11_A1209AgendaAtendimento_CodDmn = new int[1] ;
         T003A2_A1184AgendaAtendimento_Data = new DateTime[] {DateTime.MinValue} ;
         T003A2_A1186AgendaAtendimento_QtdUnd = new decimal[1] ;
         T003A2_A1183AgendaAtendimento_CntSrcCod = new int[1] ;
         T003A2_A1209AgendaAtendimento_CodDmn = new int[1] ;
         T003A15_A1194AgendaAtendimento_UnidadeContratada = new int[1] ;
         T003A15_n1194AgendaAtendimento_UnidadeContratada = new bool[] {false} ;
         T003A16_A457ContagemResultado_Demanda = new String[] {""} ;
         T003A16_n457ContagemResultado_Demanda = new bool[] {false} ;
         T003A16_A493ContagemResultado_DemandaFM = new String[] {""} ;
         T003A16_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         T003A17_A1183AgendaAtendimento_CntSrcCod = new int[1] ;
         T003A17_A1184AgendaAtendimento_Data = new DateTime[] {DateTime.MinValue} ;
         T003A17_A1209AgendaAtendimento_CodDmn = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.agendaatendimento__default(),
            new Object[][] {
                new Object[] {
               T003A2_A1184AgendaAtendimento_Data, T003A2_A1186AgendaAtendimento_QtdUnd, T003A2_A1183AgendaAtendimento_CntSrcCod, T003A2_A1209AgendaAtendimento_CodDmn
               }
               , new Object[] {
               T003A3_A1184AgendaAtendimento_Data, T003A3_A1186AgendaAtendimento_QtdUnd, T003A3_A1183AgendaAtendimento_CntSrcCod, T003A3_A1209AgendaAtendimento_CodDmn
               }
               , new Object[] {
               T003A4_A1194AgendaAtendimento_UnidadeContratada, T003A4_n1194AgendaAtendimento_UnidadeContratada
               }
               , new Object[] {
               T003A5_A457ContagemResultado_Demanda, T003A5_n457ContagemResultado_Demanda, T003A5_A493ContagemResultado_DemandaFM, T003A5_n493ContagemResultado_DemandaFM
               }
               , new Object[] {
               T003A6_A1184AgendaAtendimento_Data, T003A6_A1186AgendaAtendimento_QtdUnd, T003A6_A457ContagemResultado_Demanda, T003A6_n457ContagemResultado_Demanda, T003A6_A493ContagemResultado_DemandaFM, T003A6_n493ContagemResultado_DemandaFM, T003A6_A1183AgendaAtendimento_CntSrcCod, T003A6_A1209AgendaAtendimento_CodDmn, T003A6_A1194AgendaAtendimento_UnidadeContratada, T003A6_n1194AgendaAtendimento_UnidadeContratada
               }
               , new Object[] {
               T003A7_A1194AgendaAtendimento_UnidadeContratada, T003A7_n1194AgendaAtendimento_UnidadeContratada
               }
               , new Object[] {
               T003A8_A457ContagemResultado_Demanda, T003A8_n457ContagemResultado_Demanda, T003A8_A493ContagemResultado_DemandaFM, T003A8_n493ContagemResultado_DemandaFM
               }
               , new Object[] {
               T003A9_A1183AgendaAtendimento_CntSrcCod, T003A9_A1184AgendaAtendimento_Data, T003A9_A1209AgendaAtendimento_CodDmn
               }
               , new Object[] {
               T003A10_A1183AgendaAtendimento_CntSrcCod, T003A10_A1184AgendaAtendimento_Data, T003A10_A1209AgendaAtendimento_CodDmn
               }
               , new Object[] {
               T003A11_A1183AgendaAtendimento_CntSrcCod, T003A11_A1184AgendaAtendimento_Data, T003A11_A1209AgendaAtendimento_CodDmn
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T003A15_A1194AgendaAtendimento_UnidadeContratada, T003A15_n1194AgendaAtendimento_UnidadeContratada
               }
               , new Object[] {
               T003A16_A457ContagemResultado_Demanda, T003A16_n457ContagemResultado_Demanda, T003A16_A493ContagemResultado_DemandaFM, T003A16_n493ContagemResultado_DemandaFM
               }
               , new Object[] {
               T003A17_A1183AgendaAtendimento_CntSrcCod, T003A17_A1184AgendaAtendimento_Data, T003A17_A1209AgendaAtendimento_CodDmn
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short RcdFound142 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7AgendaAtendimento_CntSrcCod ;
      private int wcpOAV12AgendaAtendimento_CodDmn ;
      private int Z1183AgendaAtendimento_CntSrcCod ;
      private int Z1209AgendaAtendimento_CodDmn ;
      private int A1183AgendaAtendimento_CntSrcCod ;
      private int A1209AgendaAtendimento_CodDmn ;
      private int AV7AgendaAtendimento_CntSrcCod ;
      private int AV12AgendaAtendimento_CodDmn ;
      private int trnEnded ;
      private int edtAgendaAtendimento_CntSrcCod_Visible ;
      private int edtAgendaAtendimento_CntSrcCod_Enabled ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtAgendaAtendimento_Data_Enabled ;
      private int edtAgendaAtendimento_CodDmn_Enabled ;
      private int edtAgendaAtendimento_QtdUnd_Enabled ;
      private int A1194AgendaAtendimento_UnidadeContratada ;
      private int Z1194AgendaAtendimento_UnidadeContratada ;
      private int idxLst ;
      private decimal Z1186AgendaAtendimento_QtdUnd ;
      private decimal A1186AgendaAtendimento_QtdUnd ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtAgendaAtendimento_Data_Internalname ;
      private String TempTags ;
      private String edtAgendaAtendimento_CntSrcCod_Internalname ;
      private String edtAgendaAtendimento_CntSrcCod_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockagendaatendimento_data_Internalname ;
      private String lblTextblockagendaatendimento_data_Jsonclick ;
      private String edtAgendaAtendimento_Data_Jsonclick ;
      private String lblTextblockagendaatendimento_coddmn_Internalname ;
      private String lblTextblockagendaatendimento_coddmn_Jsonclick ;
      private String edtAgendaAtendimento_CodDmn_Internalname ;
      private String edtAgendaAtendimento_CodDmn_Jsonclick ;
      private String lblTextblockagendaatendimento_qtdund_Internalname ;
      private String lblTextblockagendaatendimento_qtdund_Jsonclick ;
      private String edtAgendaAtendimento_QtdUnd_Internalname ;
      private String edtAgendaAtendimento_QtdUnd_Jsonclick ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode142 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private DateTime wcpOAV8AgendaAtendimento_Data ;
      private DateTime Z1184AgendaAtendimento_Data ;
      private DateTime AV8AgendaAtendimento_Data ;
      private DateTime A1184AgendaAtendimento_Data ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n1194AgendaAtendimento_UnidadeContratada ;
      private bool n457ContagemResultado_Demanda ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String Z457ContagemResultado_Demanda ;
      private String Z493ContagemResultado_DemandaFM ;
      private IGxSession AV11WebSession ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] T003A4_A1194AgendaAtendimento_UnidadeContratada ;
      private bool[] T003A4_n1194AgendaAtendimento_UnidadeContratada ;
      private String[] T003A5_A457ContagemResultado_Demanda ;
      private bool[] T003A5_n457ContagemResultado_Demanda ;
      private String[] T003A5_A493ContagemResultado_DemandaFM ;
      private bool[] T003A5_n493ContagemResultado_DemandaFM ;
      private DateTime[] T003A6_A1184AgendaAtendimento_Data ;
      private decimal[] T003A6_A1186AgendaAtendimento_QtdUnd ;
      private String[] T003A6_A457ContagemResultado_Demanda ;
      private bool[] T003A6_n457ContagemResultado_Demanda ;
      private String[] T003A6_A493ContagemResultado_DemandaFM ;
      private bool[] T003A6_n493ContagemResultado_DemandaFM ;
      private int[] T003A6_A1183AgendaAtendimento_CntSrcCod ;
      private int[] T003A6_A1209AgendaAtendimento_CodDmn ;
      private int[] T003A6_A1194AgendaAtendimento_UnidadeContratada ;
      private bool[] T003A6_n1194AgendaAtendimento_UnidadeContratada ;
      private int[] T003A7_A1194AgendaAtendimento_UnidadeContratada ;
      private bool[] T003A7_n1194AgendaAtendimento_UnidadeContratada ;
      private String[] T003A8_A457ContagemResultado_Demanda ;
      private bool[] T003A8_n457ContagemResultado_Demanda ;
      private String[] T003A8_A493ContagemResultado_DemandaFM ;
      private bool[] T003A8_n493ContagemResultado_DemandaFM ;
      private int[] T003A9_A1183AgendaAtendimento_CntSrcCod ;
      private DateTime[] T003A9_A1184AgendaAtendimento_Data ;
      private int[] T003A9_A1209AgendaAtendimento_CodDmn ;
      private DateTime[] T003A3_A1184AgendaAtendimento_Data ;
      private decimal[] T003A3_A1186AgendaAtendimento_QtdUnd ;
      private int[] T003A3_A1183AgendaAtendimento_CntSrcCod ;
      private int[] T003A3_A1209AgendaAtendimento_CodDmn ;
      private int[] T003A10_A1183AgendaAtendimento_CntSrcCod ;
      private DateTime[] T003A10_A1184AgendaAtendimento_Data ;
      private int[] T003A10_A1209AgendaAtendimento_CodDmn ;
      private int[] T003A11_A1183AgendaAtendimento_CntSrcCod ;
      private DateTime[] T003A11_A1184AgendaAtendimento_Data ;
      private int[] T003A11_A1209AgendaAtendimento_CodDmn ;
      private DateTime[] T003A2_A1184AgendaAtendimento_Data ;
      private decimal[] T003A2_A1186AgendaAtendimento_QtdUnd ;
      private int[] T003A2_A1183AgendaAtendimento_CntSrcCod ;
      private int[] T003A2_A1209AgendaAtendimento_CodDmn ;
      private int[] T003A15_A1194AgendaAtendimento_UnidadeContratada ;
      private bool[] T003A15_n1194AgendaAtendimento_UnidadeContratada ;
      private String[] T003A16_A457ContagemResultado_Demanda ;
      private bool[] T003A16_n457ContagemResultado_Demanda ;
      private String[] T003A16_A493ContagemResultado_DemandaFM ;
      private bool[] T003A16_n493ContagemResultado_DemandaFM ;
      private int[] T003A17_A1183AgendaAtendimento_CntSrcCod ;
      private DateTime[] T003A17_A1184AgendaAtendimento_Data ;
      private int[] T003A17_A1209AgendaAtendimento_CodDmn ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV10TrnContext ;
   }

   public class agendaatendimento__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT003A6 ;
          prmT003A6 = new Object[] {
          new Object[] {"@AgendaAtendimento_CntSrcCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AgendaAtendimento_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AgendaAtendimento_CodDmn",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003A4 ;
          prmT003A4 = new Object[] {
          new Object[] {"@AgendaAtendimento_CntSrcCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003A5 ;
          prmT003A5 = new Object[] {
          new Object[] {"@AgendaAtendimento_CodDmn",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003A7 ;
          prmT003A7 = new Object[] {
          new Object[] {"@AgendaAtendimento_CntSrcCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003A8 ;
          prmT003A8 = new Object[] {
          new Object[] {"@AgendaAtendimento_CodDmn",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003A9 ;
          prmT003A9 = new Object[] {
          new Object[] {"@AgendaAtendimento_CntSrcCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AgendaAtendimento_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AgendaAtendimento_CodDmn",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003A3 ;
          prmT003A3 = new Object[] {
          new Object[] {"@AgendaAtendimento_CntSrcCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AgendaAtendimento_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AgendaAtendimento_CodDmn",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003A10 ;
          prmT003A10 = new Object[] {
          new Object[] {"@AgendaAtendimento_CntSrcCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AgendaAtendimento_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AgendaAtendimento_CodDmn",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003A11 ;
          prmT003A11 = new Object[] {
          new Object[] {"@AgendaAtendimento_CntSrcCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AgendaAtendimento_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AgendaAtendimento_CodDmn",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003A2 ;
          prmT003A2 = new Object[] {
          new Object[] {"@AgendaAtendimento_CntSrcCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AgendaAtendimento_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AgendaAtendimento_CodDmn",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003A12 ;
          prmT003A12 = new Object[] {
          new Object[] {"@AgendaAtendimento_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AgendaAtendimento_QtdUnd",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AgendaAtendimento_CntSrcCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AgendaAtendimento_CodDmn",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003A13 ;
          prmT003A13 = new Object[] {
          new Object[] {"@AgendaAtendimento_QtdUnd",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AgendaAtendimento_CntSrcCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AgendaAtendimento_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AgendaAtendimento_CodDmn",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003A14 ;
          prmT003A14 = new Object[] {
          new Object[] {"@AgendaAtendimento_CntSrcCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AgendaAtendimento_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AgendaAtendimento_CodDmn",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003A17 ;
          prmT003A17 = new Object[] {
          } ;
          Object[] prmT003A16 ;
          prmT003A16 = new Object[] {
          new Object[] {"@AgendaAtendimento_CodDmn",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003A15 ;
          prmT003A15 = new Object[] {
          new Object[] {"@AgendaAtendimento_CntSrcCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T003A2", "SELECT [AgendaAtendimento_Data], [AgendaAtendimento_QtdUnd], [AgendaAtendimento_CntSrcCod] AS AgendaAtendimento_CntSrcCod, [AgendaAtendimento_CodDmn] AS AgendaAtendimento_CodDmn FROM [AgendaAtendimento] WITH (UPDLOCK) WHERE [AgendaAtendimento_CntSrcCod] = @AgendaAtendimento_CntSrcCod AND [AgendaAtendimento_Data] = @AgendaAtendimento_Data AND [AgendaAtendimento_CodDmn] = @AgendaAtendimento_CodDmn ",true, GxErrorMask.GX_NOMASK, false, this,prmT003A2,1,0,true,false )
             ,new CursorDef("T003A3", "SELECT [AgendaAtendimento_Data], [AgendaAtendimento_QtdUnd], [AgendaAtendimento_CntSrcCod] AS AgendaAtendimento_CntSrcCod, [AgendaAtendimento_CodDmn] AS AgendaAtendimento_CodDmn FROM [AgendaAtendimento] WITH (NOLOCK) WHERE [AgendaAtendimento_CntSrcCod] = @AgendaAtendimento_CntSrcCod AND [AgendaAtendimento_Data] = @AgendaAtendimento_Data AND [AgendaAtendimento_CodDmn] = @AgendaAtendimento_CodDmn ",true, GxErrorMask.GX_NOMASK, false, this,prmT003A3,1,0,true,false )
             ,new CursorDef("T003A4", "SELECT [ContratoServicos_UnidadeContratada] AS AgendaAtendimento_UnidadeContratada FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @AgendaAtendimento_CntSrcCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003A4,1,0,true,false )
             ,new CursorDef("T003A5", "SELECT [ContagemResultado_Demanda], [ContagemResultado_DemandaFM] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @AgendaAtendimento_CodDmn ",true, GxErrorMask.GX_NOMASK, false, this,prmT003A5,1,0,true,false )
             ,new CursorDef("T003A6", "SELECT TM1.[AgendaAtendimento_Data], TM1.[AgendaAtendimento_QtdUnd], T3.[ContagemResultado_Demanda], T3.[ContagemResultado_DemandaFM], TM1.[AgendaAtendimento_CntSrcCod] AS AgendaAtendimento_CntSrcCod, TM1.[AgendaAtendimento_CodDmn] AS AgendaAtendimento_CodDmn, T2.[ContratoServicos_UnidadeContratada] AS AgendaAtendimento_UnidadeContratada FROM (([AgendaAtendimento] TM1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = TM1.[AgendaAtendimento_CntSrcCod]) INNER JOIN [ContagemResultado] T3 WITH (NOLOCK) ON T3.[ContagemResultado_Codigo] = TM1.[AgendaAtendimento_CodDmn]) WHERE TM1.[AgendaAtendimento_CntSrcCod] = @AgendaAtendimento_CntSrcCod and TM1.[AgendaAtendimento_Data] = @AgendaAtendimento_Data and TM1.[AgendaAtendimento_CodDmn] = @AgendaAtendimento_CodDmn ORDER BY TM1.[AgendaAtendimento_CntSrcCod], TM1.[AgendaAtendimento_Data], TM1.[AgendaAtendimento_CodDmn]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003A6,100,0,true,false )
             ,new CursorDef("T003A7", "SELECT [ContratoServicos_UnidadeContratada] AS AgendaAtendimento_UnidadeContratada FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @AgendaAtendimento_CntSrcCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003A7,1,0,true,false )
             ,new CursorDef("T003A8", "SELECT [ContagemResultado_Demanda], [ContagemResultado_DemandaFM] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @AgendaAtendimento_CodDmn ",true, GxErrorMask.GX_NOMASK, false, this,prmT003A8,1,0,true,false )
             ,new CursorDef("T003A9", "SELECT [AgendaAtendimento_CntSrcCod] AS AgendaAtendimento_CntSrcCod, [AgendaAtendimento_Data], [AgendaAtendimento_CodDmn] AS AgendaAtendimento_CodDmn FROM [AgendaAtendimento] WITH (NOLOCK) WHERE [AgendaAtendimento_CntSrcCod] = @AgendaAtendimento_CntSrcCod AND [AgendaAtendimento_Data] = @AgendaAtendimento_Data AND [AgendaAtendimento_CodDmn] = @AgendaAtendimento_CodDmn  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003A9,1,0,true,false )
             ,new CursorDef("T003A10", "SELECT TOP 1 [AgendaAtendimento_CntSrcCod] AS AgendaAtendimento_CntSrcCod, [AgendaAtendimento_Data], [AgendaAtendimento_CodDmn] AS AgendaAtendimento_CodDmn FROM [AgendaAtendimento] WITH (NOLOCK) WHERE ( [AgendaAtendimento_CntSrcCod] > @AgendaAtendimento_CntSrcCod or [AgendaAtendimento_CntSrcCod] = @AgendaAtendimento_CntSrcCod and [AgendaAtendimento_Data] > @AgendaAtendimento_Data or [AgendaAtendimento_Data] = @AgendaAtendimento_Data and [AgendaAtendimento_CntSrcCod] = @AgendaAtendimento_CntSrcCod and [AgendaAtendimento_CodDmn] > @AgendaAtendimento_CodDmn) ORDER BY [AgendaAtendimento_CntSrcCod], [AgendaAtendimento_Data], [AgendaAtendimento_CodDmn]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003A10,1,0,true,true )
             ,new CursorDef("T003A11", "SELECT TOP 1 [AgendaAtendimento_CntSrcCod] AS AgendaAtendimento_CntSrcCod, [AgendaAtendimento_Data], [AgendaAtendimento_CodDmn] AS AgendaAtendimento_CodDmn FROM [AgendaAtendimento] WITH (NOLOCK) WHERE ( [AgendaAtendimento_CntSrcCod] < @AgendaAtendimento_CntSrcCod or [AgendaAtendimento_CntSrcCod] = @AgendaAtendimento_CntSrcCod and [AgendaAtendimento_Data] < @AgendaAtendimento_Data or [AgendaAtendimento_Data] = @AgendaAtendimento_Data and [AgendaAtendimento_CntSrcCod] = @AgendaAtendimento_CntSrcCod and [AgendaAtendimento_CodDmn] < @AgendaAtendimento_CodDmn) ORDER BY [AgendaAtendimento_CntSrcCod] DESC, [AgendaAtendimento_Data] DESC, [AgendaAtendimento_CodDmn] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003A11,1,0,true,true )
             ,new CursorDef("T003A12", "INSERT INTO [AgendaAtendimento]([AgendaAtendimento_Data], [AgendaAtendimento_QtdUnd], [AgendaAtendimento_CntSrcCod], [AgendaAtendimento_CodDmn]) VALUES(@AgendaAtendimento_Data, @AgendaAtendimento_QtdUnd, @AgendaAtendimento_CntSrcCod, @AgendaAtendimento_CodDmn)", GxErrorMask.GX_NOMASK,prmT003A12)
             ,new CursorDef("T003A13", "UPDATE [AgendaAtendimento] SET [AgendaAtendimento_QtdUnd]=@AgendaAtendimento_QtdUnd  WHERE [AgendaAtendimento_CntSrcCod] = @AgendaAtendimento_CntSrcCod AND [AgendaAtendimento_Data] = @AgendaAtendimento_Data AND [AgendaAtendimento_CodDmn] = @AgendaAtendimento_CodDmn", GxErrorMask.GX_NOMASK,prmT003A13)
             ,new CursorDef("T003A14", "DELETE FROM [AgendaAtendimento]  WHERE [AgendaAtendimento_CntSrcCod] = @AgendaAtendimento_CntSrcCod AND [AgendaAtendimento_Data] = @AgendaAtendimento_Data AND [AgendaAtendimento_CodDmn] = @AgendaAtendimento_CodDmn", GxErrorMask.GX_NOMASK,prmT003A14)
             ,new CursorDef("T003A15", "SELECT [ContratoServicos_UnidadeContratada] AS AgendaAtendimento_UnidadeContratada FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @AgendaAtendimento_CntSrcCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003A15,1,0,true,false )
             ,new CursorDef("T003A16", "SELECT [ContagemResultado_Demanda], [ContagemResultado_DemandaFM] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @AgendaAtendimento_CodDmn ",true, GxErrorMask.GX_NOMASK, false, this,prmT003A16,1,0,true,false )
             ,new CursorDef("T003A17", "SELECT [AgendaAtendimento_CntSrcCod] AS AgendaAtendimento_CntSrcCod, [AgendaAtendimento_Data], [AgendaAtendimento_CodDmn] AS AgendaAtendimento_CodDmn FROM [AgendaAtendimento] WITH (NOLOCK) ORDER BY [AgendaAtendimento_CntSrcCod], [AgendaAtendimento_Data], [AgendaAtendimento_CodDmn]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003A17,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 4 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 14 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 10 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (decimal)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                return;
             case 11 :
                stmt.SetParameter(1, (decimal)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
