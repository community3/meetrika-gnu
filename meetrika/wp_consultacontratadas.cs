/*
               File: WP_ConsultaContratadas
        Description: Consulta Contratadas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 23:43:11.79
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_consultacontratadas : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_consultacontratadas( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_consultacontratadas( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavAreatrabalho_codigo = new GXCombobox();
         dynavFiltro_contratadacodigo = new GXCombobox();
         cmbavFiltro_ano = new GXCombobox();
         cmbavMes = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vFILTRO_CONTRATADACODIGO") == 0 )
            {
               AV9AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9AreaTrabalho_Codigo), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvFILTRO_CONTRATADACODIGOCD2( AV9AreaTrabalho_Codigo) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid1") == 0 )
            {
               nRC_GXsfl_26 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_26_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_26_idx = GetNextPar( );
               edtavQtdedmn_Tooltiptext = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtdedmn_Internalname, "Tooltiptext", edtavQtdedmn_Tooltiptext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid1_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid1") == 0 )
            {
               edtavQtdedmn_Tooltiptext = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtdedmn_Internalname, "Tooltiptext", edtavQtdedmn_Tooltiptext);
               AV22Filtro_Ano = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Filtro_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Filtro_Ano), 4, 0)));
               A39Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
               AV53Filtro_ContratadaCodigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53Filtro_ContratadaCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53Filtro_ContratadaCodigo), 6, 0)));
               A92Contrato_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A92Contrato_Ativo", A92Contrato_Ativo);
               A116Contrato_ValorUnidadeContratacao = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A116Contrato_ValorUnidadeContratacao", StringUtil.LTrim( StringUtil.Str( A116Contrato_ValorUnidadeContratacao, 18, 5)));
               A490ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n490ContagemResultado_ContratadaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A490ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0)));
               A473ContagemResultado_DataCnt = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A473ContagemResultado_DataCnt", context.localUtil.Format(A473ContagemResultado_DataCnt, "99/99/99"));
               A484ContagemResultado_StatusDmn = GetNextPar( );
               n484ContagemResultado_StatusDmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A484ContagemResultado_StatusDmn", A484ContagemResultado_StatusDmn);
               A517ContagemResultado_Ultima = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A517ContagemResultado_Ultima", A517ContagemResultado_Ultima);
               A500ContagemResultado_ContratadaPessoaNom = GetNextPar( );
               n500ContagemResultado_ContratadaPessoaNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A500ContagemResultado_ContratadaPessoaNom", A500ContagemResultado_ContratadaPessoaNom);
               A470ContagemResultado_ContadorFMCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A470ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A470ContagemResultado_ContadorFMCod), 6, 0)));
               A474ContagemResultado_ContadorFMNom = GetNextPar( );
               n474ContagemResultado_ContadorFMNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A474ContagemResultado_ContadorFMNom", A474ContagemResultado_ContadorFMNom);
               A458ContagemResultado_PFBFS = NumberUtil.Val( GetNextPar( ), ".");
               n458ContagemResultado_PFBFS = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A458ContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( A458ContagemResultado_PFBFS, 14, 5)));
               A460ContagemResultado_PFBFM = NumberUtil.Val( GetNextPar( ), ".");
               n460ContagemResultado_PFBFM = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A460ContagemResultado_PFBFM", StringUtil.LTrim( StringUtil.Str( A460ContagemResultado_PFBFM, 14, 5)));
               AV20CUPF = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20CUPF", StringUtil.LTrim( StringUtil.Str( AV20CUPF, 14, 2)));
               AV48tQtdeDmn = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48tQtdeDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48tQtdeDmn), 8, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTQTDEDMN", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV48tQtdeDmn), "ZZZZZZZ9")));
               AV47tPFBFS = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47tPFBFS", StringUtil.LTrim( StringUtil.Str( AV47tPFBFS, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTPFBFS", GetSecureSignedToken( "", context.localUtil.Format( AV47tPFBFS, "ZZ,ZZZ,ZZ9.999")));
               AV46tPFBFM = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46tPFBFM", StringUtil.LTrim( StringUtil.Str( AV46tPFBFM, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTPFBFM", GetSecureSignedToken( "", context.localUtil.Format( AV46tPFBFM, "ZZ,ZZZ,ZZ9.999")));
               AV42tCTPF = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42tCTPF", StringUtil.LTrim( StringUtil.Str( AV42tCTPF, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTCTPF", GetSecureSignedToken( "", context.localUtil.Format( AV42tCTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
               AV49tRTPF = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49tRTPF", StringUtil.LTrim( StringUtil.Str( AV49tRTPF, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTRTPF", GetSecureSignedToken( "", context.localUtil.Format( AV49tRTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
               AV44tErros = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44tErros", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44tErros), 8, 0)));
               AV45tPendencias = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45tPendencias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45tPendencias), 8, 0)));
               AV41tCanceladas = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41tCanceladas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41tCanceladas), 8, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid1_refresh( AV22Filtro_Ano, A39Contratada_Codigo, AV53Filtro_ContratadaCodigo, A92Contrato_Ativo, A116Contrato_ValorUnidadeContratacao, A490ContagemResultado_ContratadaCod, A473ContagemResultado_DataCnt, A484ContagemResultado_StatusDmn, A517ContagemResultado_Ultima, A500ContagemResultado_ContratadaPessoaNom, A470ContagemResultado_ContadorFMCod, A474ContagemResultado_ContadorFMNom, A458ContagemResultado_PFBFS, A460ContagemResultado_PFBFM, AV20CUPF, AV48tQtdeDmn, AV47tPFBFS, AV46tPFBFM, AV42tCTPF, AV49tRTPF, AV44tErros, AV45tPendencias, AV41tCanceladas) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "WP_ConsultaContratadas";
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV47tPFBFS, "ZZ,ZZZ,ZZ9.999");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV46tPFBFM, "ZZ,ZZZ,ZZ9.999");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV48tQtdeDmn), "ZZZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV42tCTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV49tRTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99");
               GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
               GXUtil.WriteLog("wp_consultacontratadas:[SendSecurityCheck value for]"+"tPFBFS:"+context.localUtil.Format( AV47tPFBFS, "ZZ,ZZZ,ZZ9.999"));
               GXUtil.WriteLog("wp_consultacontratadas:[SendSecurityCheck value for]"+"tPFBFM:"+context.localUtil.Format( AV46tPFBFM, "ZZ,ZZZ,ZZ9.999"));
               GXUtil.WriteLog("wp_consultacontratadas:[SendSecurityCheck value for]"+"tQtdeDmn:"+context.localUtil.Format( (decimal)(AV48tQtdeDmn), "ZZZZZZZ9"));
               GXUtil.WriteLog("wp_consultacontratadas:[SendSecurityCheck value for]"+"tCTPF:"+context.localUtil.Format( AV42tCTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99"));
               GXUtil.WriteLog("wp_consultacontratadas:[SendSecurityCheck value for]"+"tRTPF:"+context.localUtil.Format( AV49tRTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99"));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PACD2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTCD2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202032423431195");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_consultacontratadas.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_26", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_26), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATO_ATIVO", A92Contrato_Ativo);
         GxWebStd.gx_hidden_field( context, "CONTRATO_VALORUNIDADECONTRATACAO", StringUtil.LTrim( StringUtil.NToC( A116Contrato_ValorUnidadeContratacao, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DATACNT", context.localUtil.DToC( A473ContagemResultado_DataCnt, 0, "/"));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_STATUSDMN", StringUtil.RTrim( A484ContagemResultado_StatusDmn));
         GxWebStd.gx_boolean_hidden_field( context, "CONTAGEMRESULTADO_ULTIMA", A517ContagemResultado_Ultima);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTRATADAPESSOANOM", StringUtil.RTrim( A500ContagemResultado_ContratadaPessoaNom));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTADORFMCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A470ContagemResultado_ContadorFMCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTADORFMNOM", StringUtil.RTrim( A474ContagemResultado_ContadorFMNom));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFBFS", StringUtil.LTrim( StringUtil.NToC( A458ContagemResultado_PFBFS, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFBFM", StringUtil.LTrim( StringUtil.NToC( A460ContagemResultado_PFBFM, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCUPF", StringUtil.LTrim( StringUtil.NToC( AV20CUPF, 14, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "vTERROS", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44tErros), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vTPENDENCIAS", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV45tPendencias), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vTCANCELADAS", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV41tCanceladas), 8, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_FILTROCONSCONTADORFM", AV52SDT_FiltroConsContadorFM);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_FILTROCONSCONTADORFM", AV52SDT_FiltroConsContadorFM);
         }
         GxWebStd.gx_hidden_field( context, "vFILTRO_MES", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV55Filtro_Mes), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vTPFBFS", GetSecureSignedToken( "", context.localUtil.Format( AV47tPFBFS, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vTPFBFM", GetSecureSignedToken( "", context.localUtil.Format( AV46tPFBFM, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vTQTDEDMN", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV48tQtdeDmn), "ZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vTCTPF", GetSecureSignedToken( "", context.localUtil.Format( AV42tCTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "gxhash_vTRTPF", GetSecureSignedToken( "", context.localUtil.Format( AV49tRTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "gxhash_vTPFBFS", GetSecureSignedToken( "", context.localUtil.Format( AV47tPFBFS, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vTPFBFM", GetSecureSignedToken( "", context.localUtil.Format( AV46tPFBFM, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vTQTDEDMN", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV48tQtdeDmn), "ZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vTCTPF", GetSecureSignedToken( "", context.localUtil.Format( AV42tCTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "gxhash_vTRTPF", GetSecureSignedToken( "", context.localUtil.Format( AV49tRTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "vQTDEDMN_Tooltiptext", StringUtil.RTrim( edtavQtdedmn_Tooltiptext));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "WP_ConsultaContratadas";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV47tPFBFS, "ZZ,ZZZ,ZZ9.999");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV46tPFBFM, "ZZ,ZZZ,ZZ9.999");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV48tQtdeDmn), "ZZZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV42tCTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV49tRTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("wp_consultacontratadas:[SendSecurityCheck value for]"+"tPFBFS:"+context.localUtil.Format( AV47tPFBFS, "ZZ,ZZZ,ZZ9.999"));
         GXUtil.WriteLog("wp_consultacontratadas:[SendSecurityCheck value for]"+"tPFBFM:"+context.localUtil.Format( AV46tPFBFM, "ZZ,ZZZ,ZZ9.999"));
         GXUtil.WriteLog("wp_consultacontratadas:[SendSecurityCheck value for]"+"tQtdeDmn:"+context.localUtil.Format( (decimal)(AV48tQtdeDmn), "ZZZZZZZ9"));
         GXUtil.WriteLog("wp_consultacontratadas:[SendSecurityCheck value for]"+"tCTPF:"+context.localUtil.Format( AV42tCTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99"));
         GXUtil.WriteLog("wp_consultacontratadas:[SendSecurityCheck value for]"+"tRTPF:"+context.localUtil.Format( AV49tRTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WECD2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTCD2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_consultacontratadas.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WP_ConsultaContratadas" ;
      }

      public override String GetPgmdesc( )
      {
         return "Consulta Contratadas" ;
      }

      protected void WBCD0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_CD2( true) ;
         }
         else
         {
            wb_table1_2_CD2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_CD2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table2_35_CD2( true) ;
         }
         else
         {
            wb_table2_35_CD2( false) ;
         }
         return  ;
      }

      protected void wb_table2_35_CD2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTCD2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Consulta Contratadas", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPCD0( ) ;
      }

      protected void WSCD2( )
      {
         STARTCD2( ) ;
         EVTCD2( ) ;
      }

      protected void EVTCD2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11CD2 */
                              E11CD2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 4), "LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 14), "VQTDEDMN.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 14), "VQTDEDMN.CLICK") == 0 ) )
                           {
                              nGXsfl_26_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_26_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_26_idx), 4, 0)), 4, "0");
                              SubsflControlProps_262( ) ;
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_contadorfmcod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_contadorfmcod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADO_CONTADORFMCOD");
                                 GX_FocusControl = edtavContagemresultado_contadorfmcod_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV51ContagemResultado_ContadorFMCod = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_contadorfmcod_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV51ContagemResultado_ContadorFMCod), 6, 0)));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_CONTADORFMCOD"+"_"+sGXsfl_26_idx, GetSecureSignedToken( sGXsfl_26_idx, context.localUtil.Format( (decimal)(AV51ContagemResultado_ContadorFMCod), "ZZZZZ9")));
                              }
                              else
                              {
                                 AV51ContagemResultado_ContadorFMCod = (int)(context.localUtil.CToN( cgiGet( edtavContagemresultado_contadorfmcod_Internalname), ",", "."));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_contadorfmcod_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV51ContagemResultado_ContadorFMCod), 6, 0)));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_CONTADORFMCOD"+"_"+sGXsfl_26_idx, GetSecureSignedToken( sGXsfl_26_idx, context.localUtil.Format( (decimal)(AV51ContagemResultado_ContadorFMCod), "ZZZZZ9")));
                              }
                              AV56Contratada_PessoaNom = StringUtil.Upper( cgiGet( edtavContratada_pessoanom_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContratada_pessoanom_Internalname, AV56Contratada_PessoaNom);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATADA_PESSOANOM"+"_"+sGXsfl_26_idx, GetSecureSignedToken( sGXsfl_26_idx, StringUtil.RTrim( context.localUtil.Format( AV56Contratada_PessoaNom, "@!"))));
                              cmbavMes.Name = cmbavMes_Internalname;
                              cmbavMes.CurrentValue = cgiGet( cmbavMes_Internalname);
                              AV29Mes = (long)(NumberUtil.Val( cgiGet( cmbavMes_Internalname), "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavMes_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV29Mes), 10, 0)));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMES"+"_"+sGXsfl_26_idx, GetSecureSignedToken( sGXsfl_26_idx, context.localUtil.Format( (decimal)(AV29Mes), "ZZZZZZZZZ9")));
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavPfbfs_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPfbfs_Internalname), ",", ".") > 99999999.99999m ) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPFBFS");
                                 GX_FocusControl = edtavPfbfs_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV37PFBFS = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfbfs_Internalname, StringUtil.LTrim( StringUtil.Str( AV37PFBFS, 14, 5)));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFBFS"+"_"+sGXsfl_26_idx, GetSecureSignedToken( sGXsfl_26_idx, context.localUtil.Format( AV37PFBFS, "ZZ,ZZZ,ZZ9.999")));
                              }
                              else
                              {
                                 AV37PFBFS = context.localUtil.CToN( cgiGet( edtavPfbfs_Internalname), ",", ".");
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfbfs_Internalname, StringUtil.LTrim( StringUtil.Str( AV37PFBFS, 14, 5)));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFBFS"+"_"+sGXsfl_26_idx, GetSecureSignedToken( sGXsfl_26_idx, context.localUtil.Format( AV37PFBFS, "ZZ,ZZZ,ZZ9.999")));
                              }
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavPfbfm_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPfbfm_Internalname), ",", ".") > 99999999.99999m ) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPFBFM");
                                 GX_FocusControl = edtavPfbfm_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV36PFBFM = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfbfm_Internalname, StringUtil.LTrim( StringUtil.Str( AV36PFBFM, 14, 5)));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFBFM"+"_"+sGXsfl_26_idx, GetSecureSignedToken( sGXsfl_26_idx, context.localUtil.Format( AV36PFBFM, "ZZ,ZZZ,ZZ9.999")));
                              }
                              else
                              {
                                 AV36PFBFM = context.localUtil.CToN( cgiGet( edtavPfbfm_Internalname), ",", ".");
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfbfm_Internalname, StringUtil.LTrim( StringUtil.Str( AV36PFBFM, 14, 5)));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFBFM"+"_"+sGXsfl_26_idx, GetSecureSignedToken( sGXsfl_26_idx, context.localUtil.Format( AV36PFBFM, "ZZ,ZZZ,ZZ9.999")));
                              }
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavQtdedmn_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavQtdedmn_Internalname), ",", ".") > Convert.ToDecimal( 99999999 )) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vQTDEDMN");
                                 GX_FocusControl = edtavQtdedmn_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV38QtdeDmn = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtdedmn_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV38QtdeDmn), 8, 0)));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vQTDEDMN"+"_"+sGXsfl_26_idx, GetSecureSignedToken( sGXsfl_26_idx, context.localUtil.Format( (decimal)(AV38QtdeDmn), "ZZZZZZZ9")));
                              }
                              else
                              {
                                 AV38QtdeDmn = (int)(context.localUtil.CToN( cgiGet( edtavQtdedmn_Internalname), ",", "."));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtdedmn_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV38QtdeDmn), 8, 0)));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vQTDEDMN"+"_"+sGXsfl_26_idx, GetSecureSignedToken( sGXsfl_26_idx, context.localUtil.Format( (decimal)(AV38QtdeDmn), "ZZZZZZZ9")));
                              }
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavCtpf_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavCtpf_Internalname), ",", ".") > 999999999999.99999m ) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCTPF");
                                 GX_FocusControl = edtavCtpf_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV19CTPF = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCtpf_Internalname, StringUtil.LTrim( StringUtil.Str( AV19CTPF, 18, 5)));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCTPF"+"_"+sGXsfl_26_idx, GetSecureSignedToken( sGXsfl_26_idx, context.localUtil.Format( AV19CTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
                              }
                              else
                              {
                                 AV19CTPF = context.localUtil.CToN( cgiGet( edtavCtpf_Internalname), ",", ".");
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCtpf_Internalname, StringUtil.LTrim( StringUtil.Str( AV19CTPF, 18, 5)));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCTPF"+"_"+sGXsfl_26_idx, GetSecureSignedToken( sGXsfl_26_idx, context.localUtil.Format( AV19CTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
                              }
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavRtpf_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavRtpf_Internalname), ",", ".") > 999999999999.99999m ) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vRTPF");
                                 GX_FocusControl = edtavRtpf_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV40RTPF = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRtpf_Internalname, StringUtil.LTrim( StringUtil.Str( AV40RTPF, 18, 5)));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vRTPF"+"_"+sGXsfl_26_idx, GetSecureSignedToken( sGXsfl_26_idx, context.localUtil.Format( AV40RTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
                              }
                              else
                              {
                                 AV40RTPF = context.localUtil.CToN( cgiGet( edtavRtpf_Internalname), ",", ".");
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRtpf_Internalname, StringUtil.LTrim( StringUtil.Str( AV40RTPF, 18, 5)));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vRTPF"+"_"+sGXsfl_26_idx, GetSecureSignedToken( sGXsfl_26_idx, context.localUtil.Format( AV40RTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
                              }
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E12CD2 */
                                    E12CD2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E13CD2 */
                                    E13CD2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VQTDEDMN.CLICK") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E14CD2 */
                                    E14CD2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WECD2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PACD2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavAreatrabalho_codigo.Name = "vAREATRABALHO_CODIGO";
            dynavAreatrabalho_codigo.WebTags = "";
            dynavAreatrabalho_codigo.removeAllItems();
            /* Using cursor H00CD2 */
            pr_default.execute(0);
            while ( (pr_default.getStatus(0) != 101) )
            {
               dynavAreatrabalho_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(H00CD2_A5AreaTrabalho_Codigo[0]), 6, 0)), H00CD2_A6AreaTrabalho_Descricao[0], 0);
               pr_default.readNext(0);
            }
            pr_default.close(0);
            if ( dynavAreatrabalho_codigo.ItemCount > 0 )
            {
               AV9AreaTrabalho_Codigo = (int)(NumberUtil.Val( dynavAreatrabalho_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9AreaTrabalho_Codigo), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9AreaTrabalho_Codigo), 6, 0)));
            }
            dynavFiltro_contratadacodigo.Name = "vFILTRO_CONTRATADACODIGO";
            dynavFiltro_contratadacodigo.WebTags = "";
            cmbavFiltro_ano.Name = "vFILTRO_ANO";
            cmbavFiltro_ano.WebTags = "";
            cmbavFiltro_ano.addItem("2014", "2014", 0);
            cmbavFiltro_ano.addItem("2015", "2015", 0);
            if ( cmbavFiltro_ano.ItemCount > 0 )
            {
               AV22Filtro_Ano = (short)(NumberUtil.Val( cmbavFiltro_ano.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22Filtro_Ano), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Filtro_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Filtro_Ano), 4, 0)));
            }
            GXCCtl = "vMES_" + sGXsfl_26_idx;
            cmbavMes.Name = GXCCtl;
            cmbavMes.WebTags = "";
            cmbavMes.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 10, 0)), "", 0);
            cmbavMes.addItem("1", "Janeiro", 0);
            cmbavMes.addItem("2", "Fevereiro", 0);
            cmbavMes.addItem("3", "Mar�o", 0);
            cmbavMes.addItem("4", "Abril", 0);
            cmbavMes.addItem("5", "Maio", 0);
            cmbavMes.addItem("6", "Junho", 0);
            cmbavMes.addItem("7", "Julho", 0);
            cmbavMes.addItem("8", "Agosto", 0);
            cmbavMes.addItem("9", "Setembro", 0);
            cmbavMes.addItem("10", "Outubro", 0);
            cmbavMes.addItem("11", "Novembro", 0);
            cmbavMes.addItem("12", "Dezembro", 0);
            if ( cmbavMes.ItemCount > 0 )
            {
               AV29Mes = (long)(NumberUtil.Val( cmbavMes.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV29Mes), 10, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavMes_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV29Mes), 10, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMES"+"_"+sGXsfl_26_idx, GetSecureSignedToken( sGXsfl_26_idx, context.localUtil.Format( (decimal)(AV29Mes), "ZZZZZZZZZ9")));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = dynavAreatrabalho_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvAREATRABALHO_CODIGOCD1( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvAREATRABALHO_CODIGO_dataCD1( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvAREATRABALHO_CODIGO_htmlCD1( )
      {
         int gxdynajaxvalue ;
         GXDLVvAREATRABALHO_CODIGO_dataCD1( ) ;
         gxdynajaxindex = 1;
         dynavAreatrabalho_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavAreatrabalho_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavAreatrabalho_codigo.ItemCount > 0 )
         {
            AV9AreaTrabalho_Codigo = (int)(NumberUtil.Val( dynavAreatrabalho_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9AreaTrabalho_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9AreaTrabalho_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvAREATRABALHO_CODIGO_dataCD1( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor H00CD3 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00CD3_A5AreaTrabalho_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00CD3_A6AreaTrabalho_Descricao[0]);
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void GXDLVvFILTRO_CONTRATADACODIGOCD2( int AV9AreaTrabalho_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvFILTRO_CONTRATADACODIGO_dataCD2( AV9AreaTrabalho_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvFILTRO_CONTRATADACODIGO_htmlCD2( int AV9AreaTrabalho_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLVvFILTRO_CONTRATADACODIGO_dataCD2( AV9AreaTrabalho_Codigo) ;
         gxdynajaxindex = 1;
         dynavFiltro_contratadacodigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavFiltro_contratadacodigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavFiltro_contratadacodigo.ItemCount > 0 )
         {
            AV53Filtro_ContratadaCodigo = (int)(NumberUtil.Val( dynavFiltro_contratadacodigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV53Filtro_ContratadaCodigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53Filtro_ContratadaCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53Filtro_ContratadaCodigo), 6, 0)));
         }
      }

      protected void GXDLVvFILTRO_CONTRATADACODIGO_dataCD2( int AV9AreaTrabalho_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor H00CD4 */
         pr_default.execute(2, new Object[] {AV9AreaTrabalho_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00CD4_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00CD4_A41Contratada_PessoaNom[0]));
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void gxnrGrid1_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_262( ) ;
         while ( nGXsfl_26_idx <= nRC_GXsfl_26 )
         {
            sendrow_262( ) ;
            nGXsfl_26_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_26_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_26_idx+1));
            sGXsfl_26_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_26_idx), 4, 0)), 4, "0");
            SubsflControlProps_262( ) ;
         }
         context.GX_webresponse.AddString(Grid1Container.ToJavascriptSource());
         /* End function gxnrGrid1_newrow */
      }

      protected void gxgrGrid1_refresh( short AV22Filtro_Ano ,
                                        int A39Contratada_Codigo ,
                                        int AV53Filtro_ContratadaCodigo ,
                                        bool A92Contrato_Ativo ,
                                        decimal A116Contrato_ValorUnidadeContratacao ,
                                        int A490ContagemResultado_ContratadaCod ,
                                        DateTime A473ContagemResultado_DataCnt ,
                                        String A484ContagemResultado_StatusDmn ,
                                        bool A517ContagemResultado_Ultima ,
                                        String A500ContagemResultado_ContratadaPessoaNom ,
                                        int A470ContagemResultado_ContadorFMCod ,
                                        String A474ContagemResultado_ContadorFMNom ,
                                        decimal A458ContagemResultado_PFBFS ,
                                        decimal A460ContagemResultado_PFBFM ,
                                        decimal AV20CUPF ,
                                        int AV48tQtdeDmn ,
                                        decimal AV47tPFBFS ,
                                        decimal AV46tPFBFM ,
                                        decimal AV42tCTPF ,
                                        decimal AV49tRTPF ,
                                        int AV44tErros ,
                                        int AV45tPendencias ,
                                        int AV41tCanceladas )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRID1_nCurrentRecord = 0;
         RFCD2( ) ;
         /* End function gxgrGrid1_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_CONTADORFMCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV51ContagemResultado_ContadorFMCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_CONTADORFMCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV51ContagemResultado_ContadorFMCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATADA_PESSOANOM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV56Contratada_PessoaNom, "@!"))));
         GxWebStd.gx_hidden_field( context, "vCONTRATADA_PESSOANOM", StringUtil.RTrim( AV56Contratada_PessoaNom));
         GxWebStd.gx_hidden_field( context, "gxhash_vMES", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV29Mes), "ZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "vMES", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV29Mes), 10, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFBFS", GetSecureSignedToken( "", context.localUtil.Format( AV37PFBFS, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "vPFBFS", StringUtil.LTrim( StringUtil.NToC( AV37PFBFS, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFBFM", GetSecureSignedToken( "", context.localUtil.Format( AV36PFBFM, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "vPFBFM", StringUtil.LTrim( StringUtil.NToC( AV36PFBFM, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vQTDEDMN", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV38QtdeDmn), "ZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "vQTDEDMN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38QtdeDmn), 8, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCTPF", GetSecureSignedToken( "", context.localUtil.Format( AV19CTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "vCTPF", StringUtil.LTrim( StringUtil.NToC( AV19CTPF, 18, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vRTPF", GetSecureSignedToken( "", context.localUtil.Format( AV40RTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "vRTPF", StringUtil.LTrim( StringUtil.NToC( AV40RTPF, 18, 5, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavAreatrabalho_codigo.ItemCount > 0 )
         {
            AV9AreaTrabalho_Codigo = (int)(NumberUtil.Val( dynavAreatrabalho_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9AreaTrabalho_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9AreaTrabalho_Codigo), 6, 0)));
         }
         if ( dynavFiltro_contratadacodigo.ItemCount > 0 )
         {
            AV53Filtro_ContratadaCodigo = (int)(NumberUtil.Val( dynavFiltro_contratadacodigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV53Filtro_ContratadaCodigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53Filtro_ContratadaCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53Filtro_ContratadaCodigo), 6, 0)));
         }
         if ( cmbavFiltro_ano.ItemCount > 0 )
         {
            AV22Filtro_Ano = (short)(NumberUtil.Val( cmbavFiltro_ano.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22Filtro_Ano), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Filtro_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Filtro_Ano), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFCD2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         edtavContagemresultado_contadorfmcod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_contadorfmcod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_contadorfmcod_Enabled), 5, 0)));
         edtavContratada_pessoanom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_pessoanom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoanom_Enabled), 5, 0)));
         cmbavMes.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavMes_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavMes.Enabled), 5, 0)));
         edtavPfbfs_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbfs_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfs_Enabled), 5, 0)));
         edtavPfbfm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbfm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfm_Enabled), 5, 0)));
         edtavQtdedmn_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtdedmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtdedmn_Enabled), 5, 0)));
         edtavCtpf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtpf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtpf_Enabled), 5, 0)));
         edtavRtpf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRtpf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRtpf_Enabled), 5, 0)));
         edtavTpfbfs_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTpfbfs_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTpfbfs_Enabled), 5, 0)));
         edtavTpfbfm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTpfbfm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTpfbfm_Enabled), 5, 0)));
         edtavTqtdedmn_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTqtdedmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTqtdedmn_Enabled), 5, 0)));
         edtavTctpf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTctpf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTctpf_Enabled), 5, 0)));
         edtavTrtpf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTrtpf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTrtpf_Enabled), 5, 0)));
      }

      protected void RFCD2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            Grid1Container.ClearRows();
         }
         wbStart = 26;
         /* Execute user event: E11CD2 */
         E11CD2 ();
         nGXsfl_26_idx = 1;
         sGXsfl_26_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_26_idx), 4, 0)), 4, "0");
         SubsflControlProps_262( ) ;
         nGXsfl_26_Refreshing = 1;
         Grid1Container.AddObjectProperty("GridName", "Grid1");
         Grid1Container.AddObjectProperty("CmpContext", "");
         Grid1Container.AddObjectProperty("InMasterPage", "false");
         Grid1Container.AddObjectProperty("Class", "WorkWith");
         Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
         Grid1Container.AddObjectProperty("Titleforecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Titleforecolor), 9, 0, ".", "")));
         Grid1Container.PageSize = subGrid1_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_262( ) ;
            /* Execute user event: E13CD2 */
            E13CD2 ();
            wbEnd = 26;
            WBCD0( ) ;
         }
         nGXsfl_26_Refreshing = 0;
      }

      protected int subGrid1_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUPCD0( )
      {
         /* Before Start, stand alone formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         edtavContagemresultado_contadorfmcod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_contadorfmcod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_contadorfmcod_Enabled), 5, 0)));
         edtavContratada_pessoanom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_pessoanom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoanom_Enabled), 5, 0)));
         cmbavMes.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavMes_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavMes.Enabled), 5, 0)));
         edtavPfbfs_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbfs_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfs_Enabled), 5, 0)));
         edtavPfbfm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbfm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfm_Enabled), 5, 0)));
         edtavQtdedmn_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtdedmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtdedmn_Enabled), 5, 0)));
         edtavCtpf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtpf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtpf_Enabled), 5, 0)));
         edtavRtpf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRtpf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRtpf_Enabled), 5, 0)));
         edtavTpfbfs_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTpfbfs_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTpfbfs_Enabled), 5, 0)));
         edtavTpfbfm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTpfbfm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTpfbfm_Enabled), 5, 0)));
         edtavTqtdedmn_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTqtdedmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTqtdedmn_Enabled), 5, 0)));
         edtavTctpf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTctpf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTctpf_Enabled), 5, 0)));
         edtavTrtpf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTrtpf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTrtpf_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E12CD2 */
         E12CD2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         GXVvFILTRO_CONTRATADACODIGO_htmlCD2( AV9AreaTrabalho_Codigo) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            dynavAreatrabalho_codigo.Name = dynavAreatrabalho_codigo_Internalname;
            dynavAreatrabalho_codigo.CurrentValue = cgiGet( dynavAreatrabalho_codigo_Internalname);
            AV9AreaTrabalho_Codigo = (int)(NumberUtil.Val( cgiGet( dynavAreatrabalho_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9AreaTrabalho_Codigo), 6, 0)));
            dynavFiltro_contratadacodigo.Name = dynavFiltro_contratadacodigo_Internalname;
            dynavFiltro_contratadacodigo.CurrentValue = cgiGet( dynavFiltro_contratadacodigo_Internalname);
            AV53Filtro_ContratadaCodigo = (int)(NumberUtil.Val( cgiGet( dynavFiltro_contratadacodigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53Filtro_ContratadaCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53Filtro_ContratadaCodigo), 6, 0)));
            cmbavFiltro_ano.Name = cmbavFiltro_ano_Internalname;
            cmbavFiltro_ano.CurrentValue = cgiGet( cmbavFiltro_ano_Internalname);
            AV22Filtro_Ano = (short)(NumberUtil.Val( cgiGet( cmbavFiltro_ano_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Filtro_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Filtro_Ano), 4, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTpfbfs_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTpfbfs_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTPFBFS");
               GX_FocusControl = edtavTpfbfs_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV47tPFBFS = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47tPFBFS", StringUtil.LTrim( StringUtil.Str( AV47tPFBFS, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTPFBFS", GetSecureSignedToken( "", context.localUtil.Format( AV47tPFBFS, "ZZ,ZZZ,ZZ9.999")));
            }
            else
            {
               AV47tPFBFS = context.localUtil.CToN( cgiGet( edtavTpfbfs_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47tPFBFS", StringUtil.LTrim( StringUtil.Str( AV47tPFBFS, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTPFBFS", GetSecureSignedToken( "", context.localUtil.Format( AV47tPFBFS, "ZZ,ZZZ,ZZ9.999")));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTpfbfm_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTpfbfm_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTPFBFM");
               GX_FocusControl = edtavTpfbfm_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV46tPFBFM = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46tPFBFM", StringUtil.LTrim( StringUtil.Str( AV46tPFBFM, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTPFBFM", GetSecureSignedToken( "", context.localUtil.Format( AV46tPFBFM, "ZZ,ZZZ,ZZ9.999")));
            }
            else
            {
               AV46tPFBFM = context.localUtil.CToN( cgiGet( edtavTpfbfm_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46tPFBFM", StringUtil.LTrim( StringUtil.Str( AV46tPFBFM, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTPFBFM", GetSecureSignedToken( "", context.localUtil.Format( AV46tPFBFM, "ZZ,ZZZ,ZZ9.999")));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTqtdedmn_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTqtdedmn_Internalname), ",", ".") > Convert.ToDecimal( 99999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTQTDEDMN");
               GX_FocusControl = edtavTqtdedmn_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV48tQtdeDmn = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48tQtdeDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48tQtdeDmn), 8, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTQTDEDMN", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV48tQtdeDmn), "ZZZZZZZ9")));
            }
            else
            {
               AV48tQtdeDmn = (int)(context.localUtil.CToN( cgiGet( edtavTqtdedmn_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48tQtdeDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48tQtdeDmn), 8, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTQTDEDMN", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV48tQtdeDmn), "ZZZZZZZ9")));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTctpf_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTctpf_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTCTPF");
               GX_FocusControl = edtavTctpf_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV42tCTPF = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42tCTPF", StringUtil.LTrim( StringUtil.Str( AV42tCTPF, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTCTPF", GetSecureSignedToken( "", context.localUtil.Format( AV42tCTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            }
            else
            {
               AV42tCTPF = context.localUtil.CToN( cgiGet( edtavTctpf_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42tCTPF", StringUtil.LTrim( StringUtil.Str( AV42tCTPF, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTCTPF", GetSecureSignedToken( "", context.localUtil.Format( AV42tCTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTrtpf_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTrtpf_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTRTPF");
               GX_FocusControl = edtavTrtpf_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV49tRTPF = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49tRTPF", StringUtil.LTrim( StringUtil.Str( AV49tRTPF, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTRTPF", GetSecureSignedToken( "", context.localUtil.Format( AV49tRTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            }
            else
            {
               AV49tRTPF = context.localUtil.CToN( cgiGet( edtavTrtpf_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49tRTPF", StringUtil.LTrim( StringUtil.Str( AV49tRTPF, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTRTPF", GetSecureSignedToken( "", context.localUtil.Format( AV49tRTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            }
            /* Read saved values. */
            nRC_GXsfl_26 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_26"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = "hsh" + "WP_ConsultaContratadas";
            AV47tPFBFS = context.localUtil.CToN( cgiGet( edtavTpfbfs_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47tPFBFS", StringUtil.LTrim( StringUtil.Str( AV47tPFBFS, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTPFBFS", GetSecureSignedToken( "", context.localUtil.Format( AV47tPFBFS, "ZZ,ZZZ,ZZ9.999")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV47tPFBFS, "ZZ,ZZZ,ZZ9.999");
            AV46tPFBFM = context.localUtil.CToN( cgiGet( edtavTpfbfm_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46tPFBFM", StringUtil.LTrim( StringUtil.Str( AV46tPFBFM, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTPFBFM", GetSecureSignedToken( "", context.localUtil.Format( AV46tPFBFM, "ZZ,ZZZ,ZZ9.999")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV46tPFBFM, "ZZ,ZZZ,ZZ9.999");
            AV48tQtdeDmn = (int)(context.localUtil.CToN( cgiGet( edtavTqtdedmn_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48tQtdeDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48tQtdeDmn), 8, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTQTDEDMN", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV48tQtdeDmn), "ZZZZZZZ9")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV48tQtdeDmn), "ZZZZZZZ9");
            AV42tCTPF = context.localUtil.CToN( cgiGet( edtavTctpf_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42tCTPF", StringUtil.LTrim( StringUtil.Str( AV42tCTPF, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTCTPF", GetSecureSignedToken( "", context.localUtil.Format( AV42tCTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV42tCTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99");
            AV49tRTPF = context.localUtil.CToN( cgiGet( edtavTrtpf_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49tRTPF", StringUtil.LTrim( StringUtil.Str( AV49tRTPF, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTRTPF", GetSecureSignedToken( "", context.localUtil.Format( AV49tRTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV49tRTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99");
            hsh = cgiGet( "hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("wp_consultacontratadas:[SecurityCheckFailed value for]"+"tPFBFS:"+context.localUtil.Format( AV47tPFBFS, "ZZ,ZZZ,ZZ9.999"));
               GXUtil.WriteLog("wp_consultacontratadas:[SecurityCheckFailed value for]"+"tPFBFM:"+context.localUtil.Format( AV46tPFBFM, "ZZ,ZZZ,ZZ9.999"));
               GXUtil.WriteLog("wp_consultacontratadas:[SecurityCheckFailed value for]"+"tQtdeDmn:"+context.localUtil.Format( (decimal)(AV48tQtdeDmn), "ZZZZZZZ9"));
               GXUtil.WriteLog("wp_consultacontratadas:[SecurityCheckFailed value for]"+"tCTPF:"+context.localUtil.Format( AV42tCTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99"));
               GXUtil.WriteLog("wp_consultacontratadas:[SecurityCheckFailed value for]"+"tRTPF:"+context.localUtil.Format( AV49tRTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E12CD2 */
         E12CD2 ();
         if (returnInSub) return;
      }

      protected void E12CD2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV50WWPContext) ;
         AV9AreaTrabalho_Codigo = AV50WWPContext.gxTpr_Areatrabalho_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9AreaTrabalho_Codigo), 6, 0)));
         AV22Filtro_Ano = (short)(DateTimeUtil.Year( Gx_date));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Filtro_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Filtro_Ano), 4, 0)));
         if ( DateTimeUtil.Year( Gx_date) > 2015 )
         {
            AV24i = 2016;
            while ( AV24i <= DateTimeUtil.Year( Gx_date) )
            {
               cmbavFiltro_ano.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV24i), 4, 0)), StringUtil.Str( (decimal)(AV24i), 4, 0), 0);
               AV24i = (short)(AV24i+1);
            }
         }
         imgExport_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgExport_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgExport_Visible), 5, 0)));
         imgExportreport_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgExportreport_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgExportreport_Visible), 5, 0)));
         edtavQtdedmn_Tooltiptext = "Visualizar demandas";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtdedmn_Internalname, "Tooltiptext", edtavQtdedmn_Tooltiptext);
      }

      protected void E11CD2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV8Ano = AV22Filtro_Ano;
         AV48tQtdeDmn = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48tQtdeDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48tQtdeDmn), 8, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTQTDEDMN", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV48tQtdeDmn), "ZZZZZZZ9")));
         AV47tPFBFS = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47tPFBFS", StringUtil.LTrim( StringUtil.Str( AV47tPFBFS, 14, 5)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTPFBFS", GetSecureSignedToken( "", context.localUtil.Format( AV47tPFBFS, "ZZ,ZZZ,ZZ9.999")));
         AV46tPFBFM = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46tPFBFM", StringUtil.LTrim( StringUtil.Str( AV46tPFBFM, 14, 5)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTPFBFM", GetSecureSignedToken( "", context.localUtil.Format( AV46tPFBFM, "ZZ,ZZZ,ZZ9.999")));
         AV42tCTPF = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42tCTPF", StringUtil.LTrim( StringUtil.Str( AV42tCTPF, 18, 5)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTCTPF", GetSecureSignedToken( "", context.localUtil.Format( AV42tCTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         AV49tRTPF = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49tRTPF", StringUtil.LTrim( StringUtil.Str( AV49tRTPF, 18, 5)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTRTPF", GetSecureSignedToken( "", context.localUtil.Format( AV49tRTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         AV45tPendencias = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45tPendencias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45tPendencias), 8, 0)));
         AV41tCanceladas = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41tCanceladas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41tCanceladas), 8, 0)));
         AV44tErros = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44tErros", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44tErros), 8, 0)));
         /* Using cursor H00CD5 */
         pr_default.execute(3, new Object[] {AV53Filtro_ContratadaCodigo});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A92Contrato_Ativo = H00CD5_A92Contrato_Ativo[0];
            A39Contratada_Codigo = H00CD5_A39Contratada_Codigo[0];
            A116Contrato_ValorUnidadeContratacao = H00CD5_A116Contrato_ValorUnidadeContratacao[0];
            AV20CUPF = A116Contrato_ValorUnidadeContratacao;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20CUPF", StringUtil.LTrim( StringUtil.Str( AV20CUPF, 14, 2)));
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      private void E13CD2( )
      {
         /* Load Routine */
         pr_default.dynParam(4, new Object[]{ new Object[]{
                                              AV53Filtro_ContratadaCodigo ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A473ContagemResultado_DataCnt ,
                                              AV22Filtro_Ano ,
                                              A484ContagemResultado_StatusDmn ,
                                              A517ContagemResultado_Ultima },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor H00CD6 */
         pr_default.execute(4, new Object[] {AV22Filtro_Ano, AV53Filtro_ContratadaCodigo});
         while ( (pr_default.getStatus(4) != 101) )
         {
            BRKCD4 = false;
            A456ContagemResultado_Codigo = H00CD6_A456ContagemResultado_Codigo[0];
            A499ContagemResultado_ContratadaPessoaCod = H00CD6_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = H00CD6_n499ContagemResultado_ContratadaPessoaCod[0];
            A479ContagemResultado_CrFMPessoaCod = H00CD6_A479ContagemResultado_CrFMPessoaCod[0];
            n479ContagemResultado_CrFMPessoaCod = H00CD6_n479ContagemResultado_CrFMPessoaCod[0];
            A517ContagemResultado_Ultima = H00CD6_A517ContagemResultado_Ultima[0];
            A484ContagemResultado_StatusDmn = H00CD6_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00CD6_n484ContagemResultado_StatusDmn[0];
            A490ContagemResultado_ContratadaCod = H00CD6_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00CD6_n490ContagemResultado_ContratadaCod[0];
            A473ContagemResultado_DataCnt = H00CD6_A473ContagemResultado_DataCnt[0];
            A458ContagemResultado_PFBFS = H00CD6_A458ContagemResultado_PFBFS[0];
            n458ContagemResultado_PFBFS = H00CD6_n458ContagemResultado_PFBFS[0];
            A460ContagemResultado_PFBFM = H00CD6_A460ContagemResultado_PFBFM[0];
            n460ContagemResultado_PFBFM = H00CD6_n460ContagemResultado_PFBFM[0];
            A52Contratada_AreaTrabalhoCod = H00CD6_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00CD6_n52Contratada_AreaTrabalhoCod[0];
            A43Contratada_Ativo = H00CD6_A43Contratada_Ativo[0];
            n43Contratada_Ativo = H00CD6_n43Contratada_Ativo[0];
            A500ContagemResultado_ContratadaPessoaNom = H00CD6_A500ContagemResultado_ContratadaPessoaNom[0];
            n500ContagemResultado_ContratadaPessoaNom = H00CD6_n500ContagemResultado_ContratadaPessoaNom[0];
            A470ContagemResultado_ContadorFMCod = H00CD6_A470ContagemResultado_ContadorFMCod[0];
            A474ContagemResultado_ContadorFMNom = H00CD6_A474ContagemResultado_ContadorFMNom[0];
            n474ContagemResultado_ContadorFMNom = H00CD6_n474ContagemResultado_ContadorFMNom[0];
            A484ContagemResultado_StatusDmn = H00CD6_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00CD6_n484ContagemResultado_StatusDmn[0];
            A490ContagemResultado_ContratadaCod = H00CD6_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00CD6_n490ContagemResultado_ContratadaCod[0];
            A499ContagemResultado_ContratadaPessoaCod = H00CD6_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = H00CD6_n499ContagemResultado_ContratadaPessoaCod[0];
            A52Contratada_AreaTrabalhoCod = H00CD6_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00CD6_n52Contratada_AreaTrabalhoCod[0];
            A43Contratada_Ativo = H00CD6_A43Contratada_Ativo[0];
            n43Contratada_Ativo = H00CD6_n43Contratada_Ativo[0];
            A500ContagemResultado_ContratadaPessoaNom = H00CD6_A500ContagemResultado_ContratadaPessoaNom[0];
            n500ContagemResultado_ContratadaPessoaNom = H00CD6_n500ContagemResultado_ContratadaPessoaNom[0];
            A479ContagemResultado_CrFMPessoaCod = H00CD6_A479ContagemResultado_CrFMPessoaCod[0];
            n479ContagemResultado_CrFMPessoaCod = H00CD6_n479ContagemResultado_CrFMPessoaCod[0];
            A474ContagemResultado_ContadorFMNom = H00CD6_A474ContagemResultado_ContadorFMNom[0];
            n474ContagemResultado_ContadorFMNom = H00CD6_n474ContagemResultado_ContadorFMNom[0];
            AV56Contratada_PessoaNom = A500ContagemResultado_ContratadaPessoaNom;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContratada_pessoanom_Internalname, AV56Contratada_PessoaNom);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATADA_PESSOANOM"+"_"+sGXsfl_26_idx, GetSecureSignedToken( sGXsfl_26_idx, StringUtil.RTrim( context.localUtil.Format( AV56Contratada_PessoaNom, "@!"))));
            AV51ContagemResultado_ContadorFMCod = A470ContagemResultado_ContadorFMCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_contadorfmcod_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV51ContagemResultado_ContadorFMCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_CONTADORFMCOD"+"_"+sGXsfl_26_idx, GetSecureSignedToken( sGXsfl_26_idx, context.localUtil.Format( (decimal)(AV51ContagemResultado_ContadorFMCod), "ZZZZZ9")));
            AV12ContagemResultado_ContadorFMNom = A474ContagemResultado_ContadorFMNom;
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV33mQtdeDmn[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV31mPFBFM[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV32mPFBFS[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV26mCTPF[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV34mRTPF[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV28mErros[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV30mPendencias[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV25mCanceladas[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            while ( (pr_default.getStatus(4) != 101) && ( H00CD6_A490ContagemResultado_ContratadaCod[0] == A490ContagemResultado_ContratadaCod ) && ( H00CD6_A473ContagemResultado_DataCnt[0] == A473ContagemResultado_DataCnt ) && ( StringUtil.StrCmp(H00CD6_A484ContagemResultado_StatusDmn[0], A484ContagemResultado_StatusDmn) == 0 ) && ( H00CD6_A517ContagemResultado_Ultima[0] == A517ContagemResultado_Ultima ) )
            {
               BRKCD4 = false;
               A456ContagemResultado_Codigo = H00CD6_A456ContagemResultado_Codigo[0];
               A458ContagemResultado_PFBFS = H00CD6_A458ContagemResultado_PFBFS[0];
               n458ContagemResultado_PFBFS = H00CD6_n458ContagemResultado_PFBFS[0];
               A460ContagemResultado_PFBFM = H00CD6_A460ContagemResultado_PFBFM[0];
               n460ContagemResultado_PFBFM = H00CD6_n460ContagemResultado_PFBFM[0];
               A470ContagemResultado_ContadorFMCod = H00CD6_A470ContagemResultado_ContadorFMCod[0];
               AV24i = (short)(DateTimeUtil.Month( A473ContagemResultado_DataCnt));
               AV33mQtdeDmn[AV24i-1] = (int)(AV33mQtdeDmn[AV24i-1]+1);
               AV32mPFBFS[AV24i-1] = (decimal)(AV32mPFBFS[AV24i-1]+A458ContagemResultado_PFBFS);
               AV31mPFBFM[AV24i-1] = (decimal)(AV31mPFBFM[AV24i-1]+A460ContagemResultado_PFBFM);
               AV26mCTPF[AV24i-1] = (decimal)(AV26mCTPF[AV24i-1]+(A460ContagemResultado_PFBFM*AV20CUPF));
               AV34mRTPF[AV24i-1] = (decimal)(AV34mRTPF[AV24i-1]+(A460ContagemResultado_PFBFM*AV20CUPF));
               BRKCD4 = true;
               pr_default.readNext(4);
            }
            AV24i = 1;
            while ( AV24i <= 12 )
            {
               AV29Mes = AV24i;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavMes_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV29Mes), 10, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMES"+"_"+sGXsfl_26_idx, GetSecureSignedToken( sGXsfl_26_idx, context.localUtil.Format( (decimal)(AV29Mes), "ZZZZZZZZZ9")));
               AV38QtdeDmn = AV33mQtdeDmn[AV24i-1];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtdedmn_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV38QtdeDmn), 8, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vQTDEDMN"+"_"+sGXsfl_26_idx, GetSecureSignedToken( sGXsfl_26_idx, context.localUtil.Format( (decimal)(AV38QtdeDmn), "ZZZZZZZ9")));
               AV37PFBFS = AV32mPFBFS[AV24i-1];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfbfs_Internalname, StringUtil.LTrim( StringUtil.Str( AV37PFBFS, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFBFS"+"_"+sGXsfl_26_idx, GetSecureSignedToken( sGXsfl_26_idx, context.localUtil.Format( AV37PFBFS, "ZZ,ZZZ,ZZ9.999")));
               AV36PFBFM = AV31mPFBFM[AV24i-1];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfbfm_Internalname, StringUtil.LTrim( StringUtil.Str( AV36PFBFM, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFBFM"+"_"+sGXsfl_26_idx, GetSecureSignedToken( sGXsfl_26_idx, context.localUtil.Format( AV36PFBFM, "ZZ,ZZZ,ZZ9.999")));
               AV19CTPF = AV26mCTPF[AV24i-1];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCtpf_Internalname, StringUtil.LTrim( StringUtil.Str( AV19CTPF, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCTPF"+"_"+sGXsfl_26_idx, GetSecureSignedToken( sGXsfl_26_idx, context.localUtil.Format( AV19CTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
               AV40RTPF = AV34mRTPF[AV24i-1];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRtpf_Internalname, StringUtil.LTrim( StringUtil.Str( AV40RTPF, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vRTPF"+"_"+sGXsfl_26_idx, GetSecureSignedToken( sGXsfl_26_idx, context.localUtil.Format( AV40RTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
               AV21Erros = AV28mErros[AV24i-1];
               AV35Pendencias = AV30mPendencias[AV24i-1];
               AV10Canceladas = AV25mCanceladas[AV24i-1];
               sendrow_262( ) ;
               if ( isFullAjaxMode( ) && ( nGXsfl_26_Refreshing == 0 ) )
               {
                  context.DoAjaxLoad(26, Grid1Row);
               }
               AV12ContagemResultado_ContadorFMNom = "";
               AV48tQtdeDmn = (int)(AV48tQtdeDmn+(AV33mQtdeDmn[AV24i-1]));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48tQtdeDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48tQtdeDmn), 8, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTQTDEDMN", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV48tQtdeDmn), "ZZZZZZZ9")));
               AV47tPFBFS = (decimal)(AV47tPFBFS+(AV32mPFBFS[AV24i-1]));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47tPFBFS", StringUtil.LTrim( StringUtil.Str( AV47tPFBFS, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTPFBFS", GetSecureSignedToken( "", context.localUtil.Format( AV47tPFBFS, "ZZ,ZZZ,ZZ9.999")));
               AV46tPFBFM = (decimal)(AV46tPFBFM+(AV31mPFBFM[AV24i-1]));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46tPFBFM", StringUtil.LTrim( StringUtil.Str( AV46tPFBFM, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTPFBFM", GetSecureSignedToken( "", context.localUtil.Format( AV46tPFBFM, "ZZ,ZZZ,ZZ9.999")));
               AV42tCTPF = (decimal)(AV42tCTPF+(AV26mCTPF[AV24i-1]));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42tCTPF", StringUtil.LTrim( StringUtil.Str( AV42tCTPF, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTCTPF", GetSecureSignedToken( "", context.localUtil.Format( AV42tCTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
               AV49tRTPF = (decimal)(AV49tRTPF+(AV34mRTPF[AV24i-1]));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49tRTPF", StringUtil.LTrim( StringUtil.Str( AV49tRTPF, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTRTPF", GetSecureSignedToken( "", context.localUtil.Format( AV49tRTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
               AV44tErros = (int)(AV44tErros+(AV28mErros[AV24i-1]));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44tErros", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44tErros), 8, 0)));
               AV45tPendencias = (int)(AV45tPendencias+(AV30mPendencias[AV24i-1]));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45tPendencias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45tPendencias), 8, 0)));
               AV41tCanceladas = (int)(AV41tCanceladas+(AV25mCanceladas[AV24i-1]));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41tCanceladas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41tCanceladas), 8, 0)));
               AV24i = (short)(AV24i+1);
            }
            if ( ! BRKCD4 )
            {
               BRKCD4 = true;
               pr_default.readNext(4);
            }
         }
         pr_default.close(4);
      }

      protected void E14CD2( )
      {
         /* Qtdedmn_Click Routine */
         AV52SDT_FiltroConsContadorFM.gxTpr_Areatrabalho_codigo = AV9AreaTrabalho_Codigo;
         AV52SDT_FiltroConsContadorFM.gxTpr_Contagemresultado_contadorfmcod = AV51ContagemResultado_ContadorFMCod;
         AV52SDT_FiltroConsContadorFM.gxTpr_Mes = AV55Filtro_Mes;
         AV52SDT_FiltroConsContadorFM.gxTpr_Ano = AV22Filtro_Ano;
         AV52SDT_FiltroConsContadorFM.gxTpr_Comerro = false;
         AV52SDT_FiltroConsContadorFM.gxTpr_Abertas = false;
         AV52SDT_FiltroConsContadorFM.gxTpr_Solicitadas = false;
         AV6WebSession.Set("FiltroConsultaContador", AV52SDT_FiltroConsContadorFM.ToXml(false, true, "SDT_FiltroConsContadorFM", "GxEv3Up14_Meetrika"));
         context.wjLoc = formatLink("wwcontagemresultado.aspx") ;
         context.wjLocDisableFrm = 1;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV52SDT_FiltroConsContadorFM", AV52SDT_FiltroConsContadorFM);
      }

      protected void wb_table2_35_CD2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"right\" colspan=\"2\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            wb_table3_38_CD2( true) ;
         }
         else
         {
            wb_table3_38_CD2( false) ;
         }
         return  ;
      }

      protected void wb_table3_38_CD2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_35_CD2e( true) ;
         }
         else
         {
            wb_table2_35_CD2e( false) ;
         }
      }

      protected void wb_table3_38_CD2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable3_Internalname, tblTable3_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pflfstotal_Internalname, "PFB FS: ", "", "", lblTextblockcontagemresultado_pflfstotal_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaContratadas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'" + sGXsfl_26_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTpfbfs_Internalname, StringUtil.LTrim( StringUtil.NToC( AV47tPFBFS, 14, 5, ",", "")), ((edtavTpfbfs_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV47tPFBFS, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV47tPFBFS, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,43);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTpfbfs_Jsonclick, 0, "BootstrapAttribute", "font-family:'Trebuchet MS'; font-size:10.0pt; font-weight:bold; font-style:normal;", "", "", 1, edtavTpfbfs_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_WP_ConsultaContratadas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pfbfmtotal_Internalname, " - PFB FM: ", "", "", lblTextblockcontagemresultado_pfbfmtotal_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaContratadas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'" + sGXsfl_26_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTpfbfm_Internalname, StringUtil.LTrim( StringUtil.NToC( AV46tPFBFM, 14, 5, ",", "")), ((edtavTpfbfm_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV46tPFBFM, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV46tPFBFM, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,47);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTpfbfm_Jsonclick, 0, "BootstrapAttribute", "font-family:'Trebuchet MS'; font-size:10.0pt; font-weight:bold; font-style:normal;", "", "", 1, edtavTpfbfm_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_WP_ConsultaContratadas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pflfmtotal_Internalname, " - Dmn: ", "", "", lblTextblockcontagemresultado_pflfmtotal_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaContratadas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_26_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTqtdedmn_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV48tQtdeDmn), 8, 0, ",", "")), ((edtavTqtdedmn_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV48tQtdeDmn), "ZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV48tQtdeDmn), "ZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,51);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTqtdedmn_Jsonclick, 0, "BootstrapAttribute", "font-family:'Trebuchet MS'; font-size:10.0pt; font-weight:bold; font-style:normal;", "", "", 1, edtavTqtdedmn_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_ConsultaContratadas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pftotal_Internalname, " - R$: ", "", "", lblTextblockcontagemresultado_pftotal_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaContratadas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'" + sGXsfl_26_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTctpf_Internalname, StringUtil.LTrim( StringUtil.NToC( AV42tCTPF, 18, 5, ",", "")), ((edtavTctpf_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV42tCTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( AV42tCTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,55);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTctpf_Jsonclick, 0, "BootstrapAttribute", "font-family:'Trebuchet MS'; font-size:10.0pt; font-weight:bold; font-style:normal;", "", "", 1, edtavTctpf_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_WP_ConsultaContratadas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pftotalpagar_Internalname, "R$: ", "", "", lblTextblockcontagemresultado_pftotalpagar_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaContratadas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_26_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTrtpf_Internalname, StringUtil.LTrim( StringUtil.NToC( AV49tRTPF, 18, 5, ",", "")), ((edtavTrtpf_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV49tRTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( AV49tRTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTrtpf_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavTrtpf_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_WP_ConsultaContratadas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_38_CD2e( true) ;
         }
         else
         {
            wb_table3_38_CD2e( false) ;
         }
      }

      protected void wb_table1_2_CD2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable4_Internalname, tblTable4_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:220px")+"\" class='GridHeaderCell'>") ;
            context.WriteHtmlText( "&nbsp;&nbsp; ") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemresultadotitle_Internalname, "Consulta de Contratadas", "", "", lblContagemresultadotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_ConsultaContratadas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_7_CD2( true) ;
         }
         else
         {
            wb_table4_7_CD2( false) ;
         }
         return  ;
      }

      protected void wb_table4_7_CD2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            context.WriteHtmlText( "&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "�rea de Trabalho:", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaContratadas.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'" + sGXsfl_26_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavAreatrabalho_codigo, dynavAreatrabalho_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV9AreaTrabalho_Codigo), 6, 0)), 1, dynavAreatrabalho_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_WP_ConsultaContratadas.htm");
            dynavAreatrabalho_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9AreaTrabalho_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavAreatrabalho_codigo_Internalname, "Values", (String)(dynavAreatrabalho_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock4_Internalname, "Contratada:", "", "", lblTextblock4_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaContratadas.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_26_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavFiltro_contratadacodigo, dynavFiltro_contratadacodigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV53Filtro_ContratadaCodigo), 6, 0)), 1, dynavFiltro_contratadacodigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WP_ConsultaContratadas.htm");
            dynavFiltro_contratadacodigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV53Filtro_ContratadaCodigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavFiltro_contratadacodigo_Internalname, "Values", (String)(dynavFiltro_contratadacodigo.ToJavascriptSource()));
            context.WriteHtmlText( "&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock3_Internalname, "Ano:", "", "", lblTextblock3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaContratadas.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'" + sGXsfl_26_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFiltro_ano, cmbavFiltro_ano_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV22Filtro_Ano), 4, 0)), 1, cmbavFiltro_ano_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,22);\"", "", true, "HLP_WP_ConsultaContratadas.htm");
            cmbavFiltro_ano.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22Filtro_Ano), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFiltro_ano_Internalname, "Values", (String)(cmbavFiltro_ano.ToJavascriptSource()));
            context.WriteHtmlText( "&nbsp;&nbsp;") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton1_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(26), 2, 0)+","+"null"+");", "Atualizar", bttButton1_Jsonclick, 5, "Atualizar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EREFRESH."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_ConsultaContratadas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            /*  Grid Control  */
            Grid1Container.SetWrapped(nGXWrapped);
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"Grid1Container"+"DivS\" data-gxgridid=\"26\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid1_Internalname, subGrid1_Internalname, "", "WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid1_Backcolorstyle == 0 )
               {
                  subGrid1_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid1_Class) > 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"Title";
                  }
               }
               else
               {
                  subGrid1_Titlebackstyle = 1;
                  if ( subGrid1_Backcolorstyle == 1 )
                  {
                     subGrid1_Titlebackcolor = subGrid1_Allbackcolor;
                     if ( StringUtil.Len( subGrid1_Class) > 0 )
                     {
                        subGrid1_Linesclass = subGrid1_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid1_Class) > 0 )
                     {
                        subGrid1_Linesclass = subGrid1_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+"display:none;"+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Contador FM") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Contratada") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Mes") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "PFBFS") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "PFBFM") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Qtde Dmn") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "CT/PF") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "RT/PF") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               Grid1Container.AddObjectProperty("GridName", "Grid1");
            }
            else
            {
               Grid1Container.AddObjectProperty("GridName", "Grid1");
               Grid1Container.AddObjectProperty("Class", "WorkWith");
               Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Titleforecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Titleforecolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("CmpContext", "");
               Grid1Container.AddObjectProperty("InMasterPage", "false");
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV51ContagemResultado_ContadorFMCod), 6, 0, ".", "")));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContagemresultado_contadorfmcod_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.RTrim( AV56Contratada_PessoaNom));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContratada_pessoanom_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV29Mes), 10, 0, ".", "")));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavMes.Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV37PFBFS, 14, 5, ".", "")));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavPfbfs_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV36PFBFM, 14, 5, ".", "")));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavPfbfm_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38QtdeDmn), 8, 0, ".", "")));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavQtdedmn_Enabled), 5, 0, ".", "")));
               Grid1Column.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavQtdedmn_Tooltiptext));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV19CTPF, 18, 5, ".", "")));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtpf_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV40RTPF, 18, 5, ".", "")));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavRtpf_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Container.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowselection), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Selectioncolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowhovering), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Hoveringcolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowcollapsing), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 26 )
         {
            wbEnd = 0;
            nRC_GXsfl_26 = (short)(nGXsfl_26_idx-1);
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"Grid1Container"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid1", Grid1Container);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid1ContainerData", Grid1Container.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid1ContainerData"+"V", Grid1Container.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Grid1ContainerData"+"V"+"\" value='"+Grid1Container.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_CD2e( true) ;
         }
         else
         {
            wb_table1_2_CD2e( false) ;
         }
      }

      protected void wb_table4_7_CD2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgExport_Internalname, context.GetImagePath( "da69a816-fd11-445b-8aaf-1a2f7f1acc93", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgExport_Visible, 1, "", "Exportar Excel!", 0, 0, 0, "px", 0, "px", 0, 0, 7, imgExport_Jsonclick, "'"+""+"'"+",false,"+"'"+"e15cd1_client"+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_ConsultaContratadas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgExportreport_Internalname, context.GetImagePath( "776fb79c-a0a1-4302-b5e5-d773dbe1a297", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgExportreport_Visible, 1, "", "Oculto", 0, 0, 0, "px", 0, "px", 0, 0, 7, imgExportreport_Jsonclick, "'"+""+"'"+",false,"+"'"+"e16cd1_client"+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_ConsultaContratadas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgExportgraphicdmn_Internalname, context.GetImagePath( "776fb79c-a0a1-4302-b5e5-d773dbe1a297", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Grafico anual", 0, 0, 0, "px", 0, "px", 0, 0, 7, imgExportgraphicdmn_Jsonclick, "'"+""+"'"+",false,"+"'"+"e17cd1_client"+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_ConsultaContratadas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_7_CD2e( true) ;
         }
         else
         {
            wb_table4_7_CD2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PACD2( ) ;
         WSCD2( ) ;
         WECD2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202032423431329");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_consultacontratadas.js", "?202032423431330");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_262( )
      {
         edtavContagemresultado_contadorfmcod_Internalname = "vCONTAGEMRESULTADO_CONTADORFMCOD_"+sGXsfl_26_idx;
         edtavContratada_pessoanom_Internalname = "vCONTRATADA_PESSOANOM_"+sGXsfl_26_idx;
         cmbavMes_Internalname = "vMES_"+sGXsfl_26_idx;
         edtavPfbfs_Internalname = "vPFBFS_"+sGXsfl_26_idx;
         edtavPfbfm_Internalname = "vPFBFM_"+sGXsfl_26_idx;
         edtavQtdedmn_Internalname = "vQTDEDMN_"+sGXsfl_26_idx;
         edtavCtpf_Internalname = "vCTPF_"+sGXsfl_26_idx;
         edtavRtpf_Internalname = "vRTPF_"+sGXsfl_26_idx;
      }

      protected void SubsflControlProps_fel_262( )
      {
         edtavContagemresultado_contadorfmcod_Internalname = "vCONTAGEMRESULTADO_CONTADORFMCOD_"+sGXsfl_26_fel_idx;
         edtavContratada_pessoanom_Internalname = "vCONTRATADA_PESSOANOM_"+sGXsfl_26_fel_idx;
         cmbavMes_Internalname = "vMES_"+sGXsfl_26_fel_idx;
         edtavPfbfs_Internalname = "vPFBFS_"+sGXsfl_26_fel_idx;
         edtavPfbfm_Internalname = "vPFBFM_"+sGXsfl_26_fel_idx;
         edtavQtdedmn_Internalname = "vQTDEDMN_"+sGXsfl_26_fel_idx;
         edtavCtpf_Internalname = "vCTPF_"+sGXsfl_26_fel_idx;
         edtavRtpf_Internalname = "vRTPF_"+sGXsfl_26_fel_idx;
      }

      protected void sendrow_262( )
      {
         SubsflControlProps_262( ) ;
         WBCD0( ) ;
         Grid1Row = GXWebRow.GetNew(context,Grid1Container);
         if ( subGrid1_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid1_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Odd";
            }
         }
         else if ( subGrid1_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid1_Backstyle = 0;
            subGrid1_Backcolor = subGrid1_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Uniform";
            }
         }
         else if ( subGrid1_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid1_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Odd";
            }
            subGrid1_Backcolor = (int)(0x0);
         }
         else if ( subGrid1_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid1_Backstyle = 1;
            if ( ((int)((nGXsfl_26_idx) % (2))) == 0 )
            {
               subGrid1_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Even";
               }
            }
            else
            {
               subGrid1_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Odd";
               }
            }
         }
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+subGrid1_Linesclass+"\" style=\""+""+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_26_idx+"\">") ;
         }
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavContagemresultado_contadorfmcod_Enabled!=0)&&(edtavContagemresultado_contadorfmcod_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 27,'',false,'"+sGXsfl_26_idx+"',26)\"" : " ");
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavContagemresultado_contadorfmcod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV51ContagemResultado_ContadorFMCod), 6, 0, ",", "")),((edtavContagemresultado_contadorfmcod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV51ContagemResultado_ContadorFMCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV51ContagemResultado_ContadorFMCod), "ZZZZZ9")),TempTags+((edtavContagemresultado_contadorfmcod_Enabled!=0)&&(edtavContagemresultado_contadorfmcod_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavContagemresultado_contadorfmcod_Enabled!=0)&&(edtavContagemresultado_contadorfmcod_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,27);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavContagemresultado_contadorfmcod_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavContagemresultado_contadorfmcod_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)26,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavContratada_pessoanom_Enabled!=0)&&(edtavContratada_pessoanom_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 28,'',false,'"+sGXsfl_26_idx+"',26)\"" : " ");
         ROClassString = "BootstrapAttribute100";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavContratada_pessoanom_Internalname,StringUtil.RTrim( AV56Contratada_PessoaNom),StringUtil.RTrim( context.localUtil.Format( AV56Contratada_PessoaNom, "@!")),TempTags+((edtavContratada_pessoanom_Enabled!=0)&&(edtavContratada_pessoanom_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavContratada_pessoanom_Enabled!=0)&&(edtavContratada_pessoanom_Visible!=0) ? " onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,28);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavContratada_pessoanom_Jsonclick,(short)0,(String)"BootstrapAttribute100",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavContratada_pessoanom_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)26,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         TempTags = " " + ((cmbavMes.Enabled!=0)&&(cmbavMes.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 29,'',false,'"+sGXsfl_26_idx+"',26)\"" : " ");
         if ( ( nGXsfl_26_idx == 1 ) && isAjaxCallMode( ) )
         {
            GXCCtl = "vMES_" + sGXsfl_26_idx;
            cmbavMes.Name = GXCCtl;
            cmbavMes.WebTags = "";
            cmbavMes.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 10, 0)), "", 0);
            cmbavMes.addItem("1", "Janeiro", 0);
            cmbavMes.addItem("2", "Fevereiro", 0);
            cmbavMes.addItem("3", "Mar�o", 0);
            cmbavMes.addItem("4", "Abril", 0);
            cmbavMes.addItem("5", "Maio", 0);
            cmbavMes.addItem("6", "Junho", 0);
            cmbavMes.addItem("7", "Julho", 0);
            cmbavMes.addItem("8", "Agosto", 0);
            cmbavMes.addItem("9", "Setembro", 0);
            cmbavMes.addItem("10", "Outubro", 0);
            cmbavMes.addItem("11", "Novembro", 0);
            cmbavMes.addItem("12", "Dezembro", 0);
            if ( cmbavMes.ItemCount > 0 )
            {
               AV29Mes = (long)(NumberUtil.Val( cmbavMes.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV29Mes), 10, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavMes_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV29Mes), 10, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMES"+"_"+sGXsfl_26_idx, GetSecureSignedToken( sGXsfl_26_idx, context.localUtil.Format( (decimal)(AV29Mes), "ZZZZZZZZZ9")));
            }
         }
         /* ComboBox */
         Grid1Row.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavMes,(String)cmbavMes_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(AV29Mes), 10, 0)),(short)1,(String)cmbavMes_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,cmbavMes.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",TempTags+((cmbavMes.Enabled!=0)&&(cmbavMes.Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((cmbavMes.Enabled!=0)&&(cmbavMes.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,29);\"" : " "),(String)"",(bool)true});
         cmbavMes.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV29Mes), 10, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavMes_Internalname, "Values", (String)(cmbavMes.ToJavascriptSource()));
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavPfbfs_Enabled!=0)&&(edtavPfbfs_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 30,'',false,'"+sGXsfl_26_idx+"',26)\"" : " ");
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPfbfs_Internalname,StringUtil.LTrim( StringUtil.NToC( AV37PFBFS, 14, 5, ",", "")),((edtavPfbfs_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV37PFBFS, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV37PFBFS, "ZZ,ZZZ,ZZ9.999")),TempTags+((edtavPfbfs_Enabled!=0)&&(edtavPfbfs_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavPfbfs_Enabled!=0)&&(edtavPfbfs_Visible!=0) ? " onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,30);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavPfbfs_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavPfbfs_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)26,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavPfbfm_Enabled!=0)&&(edtavPfbfm_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 31,'',false,'"+sGXsfl_26_idx+"',26)\"" : " ");
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPfbfm_Internalname,StringUtil.LTrim( StringUtil.NToC( AV36PFBFM, 14, 5, ",", "")),((edtavPfbfm_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV36PFBFM, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV36PFBFM, "ZZ,ZZZ,ZZ9.999")),TempTags+((edtavPfbfm_Enabled!=0)&&(edtavPfbfm_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavPfbfm_Enabled!=0)&&(edtavPfbfm_Visible!=0) ? " onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,31);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavPfbfm_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavPfbfm_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)26,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavQtdedmn_Enabled!=0)&&(edtavQtdedmn_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 32,'',false,'"+sGXsfl_26_idx+"',26)\"" : " ");
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavQtdedmn_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38QtdeDmn), 8, 0, ",", "")),((edtavQtdedmn_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV38QtdeDmn), "ZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV38QtdeDmn), "ZZZZZZZ9")),TempTags+((edtavQtdedmn_Enabled!=0)&&(edtavQtdedmn_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavQtdedmn_Enabled!=0)&&(edtavQtdedmn_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,32);\"" : " "),"'"+""+"'"+",false,"+"'"+"EVQTDEDMN.CLICK."+sGXsfl_26_idx+"'",(String)"",(String)"",(String)edtavQtdedmn_Tooltiptext,(String)"",(String)edtavQtdedmn_Jsonclick,(short)5,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavQtdedmn_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)26,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavCtpf_Enabled!=0)&&(edtavCtpf_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 33,'',false,'"+sGXsfl_26_idx+"',26)\"" : " ");
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtpf_Internalname,StringUtil.LTrim( StringUtil.NToC( AV19CTPF, 18, 5, ",", "")),((edtavCtpf_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV19CTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( AV19CTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")),TempTags+((edtavCtpf_Enabled!=0)&&(edtavCtpf_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCtpf_Enabled!=0)&&(edtavCtpf_Visible!=0) ? " onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,33);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"Custo Total Ponto de Fun��o",(String)"",(String)edtavCtpf_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavCtpf_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)26,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavRtpf_Enabled!=0)&&(edtavRtpf_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 34,'',false,'"+sGXsfl_26_idx+"',26)\"" : " ");
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavRtpf_Internalname,StringUtil.LTrim( StringUtil.NToC( AV40RTPF, 18, 5, ",", "")),((edtavRtpf_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV40RTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( AV40RTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")),TempTags+((edtavRtpf_Enabled!=0)&&(edtavRtpf_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavRtpf_Enabled!=0)&&(edtavRtpf_Visible!=0) ? " onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,34);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"Receita Total Ponto de Fun��o",(String)"",(String)edtavRtpf_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavRtpf_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)26,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_CONTADORFMCOD"+"_"+sGXsfl_26_idx, GetSecureSignedToken( sGXsfl_26_idx, context.localUtil.Format( (decimal)(AV51ContagemResultado_ContadorFMCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATADA_PESSOANOM"+"_"+sGXsfl_26_idx, GetSecureSignedToken( sGXsfl_26_idx, StringUtil.RTrim( context.localUtil.Format( AV56Contratada_PessoaNom, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vMES"+"_"+sGXsfl_26_idx, GetSecureSignedToken( sGXsfl_26_idx, context.localUtil.Format( (decimal)(AV29Mes), "ZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFBFS"+"_"+sGXsfl_26_idx, GetSecureSignedToken( sGXsfl_26_idx, context.localUtil.Format( AV37PFBFS, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFBFM"+"_"+sGXsfl_26_idx, GetSecureSignedToken( sGXsfl_26_idx, context.localUtil.Format( AV36PFBFM, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vQTDEDMN"+"_"+sGXsfl_26_idx, GetSecureSignedToken( sGXsfl_26_idx, context.localUtil.Format( (decimal)(AV38QtdeDmn), "ZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCTPF"+"_"+sGXsfl_26_idx, GetSecureSignedToken( sGXsfl_26_idx, context.localUtil.Format( AV19CTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "gxhash_vRTPF"+"_"+sGXsfl_26_idx, GetSecureSignedToken( sGXsfl_26_idx, context.localUtil.Format( AV40RTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         Grid1Container.AddRow(Grid1Row);
         nGXsfl_26_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_26_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_26_idx+1));
         sGXsfl_26_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_26_idx), 4, 0)), 4, "0");
         SubsflControlProps_262( ) ;
         /* End function sendrow_262 */
      }

      protected void init_default_properties( )
      {
         lblContagemresultadotitle_Internalname = "CONTAGEMRESULTADOTITLE";
         imgExport_Internalname = "EXPORT";
         imgExportreport_Internalname = "EXPORTREPORT";
         imgExportgraphicdmn_Internalname = "EXPORTGRAPHICDMN";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblTextblock2_Internalname = "TEXTBLOCK2";
         dynavAreatrabalho_codigo_Internalname = "vAREATRABALHO_CODIGO";
         lblTextblock4_Internalname = "TEXTBLOCK4";
         dynavFiltro_contratadacodigo_Internalname = "vFILTRO_CONTRATADACODIGO";
         lblTextblock3_Internalname = "TEXTBLOCK3";
         cmbavFiltro_ano_Internalname = "vFILTRO_ANO";
         bttButton1_Internalname = "BUTTON1";
         edtavContagemresultado_contadorfmcod_Internalname = "vCONTAGEMRESULTADO_CONTADORFMCOD";
         edtavContratada_pessoanom_Internalname = "vCONTRATADA_PESSOANOM";
         cmbavMes_Internalname = "vMES";
         edtavPfbfs_Internalname = "vPFBFS";
         edtavPfbfm_Internalname = "vPFBFM";
         edtavQtdedmn_Internalname = "vQTDEDMN";
         edtavCtpf_Internalname = "vCTPF";
         edtavRtpf_Internalname = "vRTPF";
         tblTable4_Internalname = "TABLE4";
         lblTextblockcontagemresultado_pflfstotal_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFLFSTOTAL";
         edtavTpfbfs_Internalname = "vTPFBFS";
         lblTextblockcontagemresultado_pfbfmtotal_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFBFMTOTAL";
         edtavTpfbfm_Internalname = "vTPFBFM";
         lblTextblockcontagemresultado_pflfmtotal_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFLFMTOTAL";
         edtavTqtdedmn_Internalname = "vTQTDEDMN";
         lblTextblockcontagemresultado_pftotal_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFTOTAL";
         edtavTctpf_Internalname = "vTCTPF";
         lblTextblockcontagemresultado_pftotalpagar_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFTOTALPAGAR";
         edtavTrtpf_Internalname = "vTRTPF";
         tblTable3_Internalname = "TABLE3";
         tblTable1_Internalname = "TABLE1";
         Form.Internalname = "FORM";
         subGrid1_Internalname = "GRID1";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavRtpf_Jsonclick = "";
         edtavRtpf_Visible = -1;
         edtavCtpf_Jsonclick = "";
         edtavCtpf_Visible = -1;
         edtavQtdedmn_Jsonclick = "";
         edtavQtdedmn_Visible = -1;
         edtavPfbfm_Jsonclick = "";
         edtavPfbfm_Visible = -1;
         edtavPfbfs_Jsonclick = "";
         edtavPfbfs_Visible = -1;
         cmbavMes_Jsonclick = "";
         cmbavMes.Visible = -1;
         edtavContratada_pessoanom_Jsonclick = "";
         edtavContratada_pessoanom_Visible = -1;
         edtavContagemresultado_contadorfmcod_Jsonclick = "";
         edtavContagemresultado_contadorfmcod_Visible = 0;
         imgExportreport_Visible = 1;
         imgExport_Visible = 1;
         subGrid1_Allowcollapsing = 0;
         subGrid1_Allowselection = 0;
         edtavRtpf_Enabled = 1;
         edtavCtpf_Enabled = 1;
         edtavQtdedmn_Enabled = 1;
         edtavPfbfm_Enabled = 1;
         edtavPfbfs_Enabled = 1;
         cmbavMes.Enabled = 1;
         edtavContratada_pessoanom_Enabled = 1;
         edtavContagemresultado_contadorfmcod_Enabled = 1;
         subGrid1_Class = "WorkWith";
         cmbavFiltro_ano_Jsonclick = "";
         dynavFiltro_contratadacodigo_Jsonclick = "";
         dynavAreatrabalho_codigo_Jsonclick = "";
         edtavTrtpf_Jsonclick = "";
         edtavTrtpf_Enabled = 1;
         edtavTctpf_Jsonclick = "";
         edtavTctpf_Enabled = 1;
         edtavTqtdedmn_Jsonclick = "";
         edtavTqtdedmn_Enabled = 1;
         edtavTpfbfm_Jsonclick = "";
         edtavTpfbfm_Enabled = 1;
         edtavTpfbfs_Jsonclick = "";
         edtavTpfbfs_Enabled = 1;
         subGrid1_Titleforecolor = (int)(0x000000);
         subGrid1_Backcolorstyle = 0;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Consulta Contratadas";
         edtavQtdedmn_Tooltiptext = "";
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public void Validv_Areatrabalho_codigo( GXCombobox dynGX_Parm1 ,
                                              GXCombobox dynGX_Parm2 )
      {
         dynavAreatrabalho_codigo = dynGX_Parm1;
         AV9AreaTrabalho_Codigo = (int)(NumberUtil.Val( dynavAreatrabalho_codigo.CurrentValue, "."));
         dynavFiltro_contratadacodigo = dynGX_Parm2;
         AV53Filtro_ContratadaCodigo = (int)(NumberUtil.Val( dynavFiltro_contratadacodigo.CurrentValue, "."));
         GXVvFILTRO_CONTRATADACODIGO_htmlCD2( AV9AreaTrabalho_Codigo) ;
         dynload_actions( ) ;
         if ( dynavFiltro_contratadacodigo.ItemCount > 0 )
         {
            AV53Filtro_ContratadaCodigo = (int)(NumberUtil.Val( dynavFiltro_contratadacodigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV53Filtro_ContratadaCodigo), 6, 0))), "."));
         }
         dynavFiltro_contratadacodigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV53Filtro_ContratadaCodigo), 6, 0));
         isValidOutput.Add(dynavFiltro_contratadacodigo);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID1_nFirstRecordOnPage',nv:0},{av:'GRID1_nEOF',nv:0},{av:'edtavQtdedmn_Tooltiptext',ctrl:'vQTDEDMN',prop:'Tooltiptext'},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A473ContagemResultado_DataCnt',fld:'CONTAGEMRESULTADO_DATACNT',pic:'',nv:''},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A517ContagemResultado_Ultima',fld:'CONTAGEMRESULTADO_ULTIMA',pic:'',nv:false},{av:'A500ContagemResultado_ContratadaPessoaNom',fld:'CONTAGEMRESULTADO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'A470ContagemResultado_ContadorFMCod',fld:'CONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'A474ContagemResultado_ContadorFMNom',fld:'CONTAGEMRESULTADO_CONTADORFMNOM',pic:'@!',nv:''},{av:'A458ContagemResultado_PFBFS',fld:'CONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A460ContagemResultado_PFBFM',fld:'CONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV20CUPF',fld:'vCUPF',pic:'ZZZZZZZZZZZ.ZZ',nv:0.0},{av:'AV48tQtdeDmn',fld:'vTQTDEDMN',pic:'ZZZZZZZ9',hsh:true,nv:0},{av:'AV47tPFBFS',fld:'vTPFBFS',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'AV46tPFBFM',fld:'vTPFBFM',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'AV42tCTPF',fld:'vTCTPF',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',hsh:true,nv:0.0},{av:'AV49tRTPF',fld:'vTRTPF',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',hsh:true,nv:0.0},{av:'AV44tErros',fld:'vTERROS',pic:'ZZZZZZZ9',nv:0},{av:'AV45tPendencias',fld:'vTPENDENCIAS',pic:'ZZZZZZZ9',nv:0},{av:'AV41tCanceladas',fld:'vTCANCELADAS',pic:'ZZZZZZZ9',nv:0},{av:'AV22Filtro_Ano',fld:'vFILTRO_ANO',pic:'ZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV53Filtro_ContratadaCodigo',fld:'vFILTRO_CONTRATADACODIGO',pic:'ZZZZZ9',nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',nv:false},{av:'A116Contrato_ValorUnidadeContratacao',fld:'CONTRATO_VALORUNIDADECONTRATACAO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0}],oparms:[{av:'AV48tQtdeDmn',fld:'vTQTDEDMN',pic:'ZZZZZZZ9',hsh:true,nv:0},{av:'AV47tPFBFS',fld:'vTPFBFS',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'AV46tPFBFM',fld:'vTPFBFM',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'AV42tCTPF',fld:'vTCTPF',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',hsh:true,nv:0.0},{av:'AV49tRTPF',fld:'vTRTPF',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',hsh:true,nv:0.0},{av:'AV45tPendencias',fld:'vTPENDENCIAS',pic:'ZZZZZZZ9',nv:0},{av:'AV41tCanceladas',fld:'vTCANCELADAS',pic:'ZZZZZZZ9',nv:0},{av:'AV44tErros',fld:'vTERROS',pic:'ZZZZZZZ9',nv:0},{av:'AV20CUPF',fld:'vCUPF',pic:'ZZZZZZZZZZZ.ZZ',nv:0.0}]}");
         setEventMetadata("'DOGRAFICOSDMN'","{handler:'E17CD1',iparms:[{av:'AV53Filtro_ContratadaCodigo',fld:'vFILTRO_CONTRATADACODIGO',pic:'ZZZZZ9',nv:0},{av:'AV22Filtro_Ano',fld:'vFILTRO_ANO',pic:'ZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("VQTDEDMN.CLICK","{handler:'E14CD2',iparms:[{av:'AV9AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV52SDT_FiltroConsContadorFM',fld:'vSDT_FILTROCONSCONTADORFM',pic:'',nv:null},{av:'AV51ContagemResultado_ContadorFMCod',fld:'vCONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV55Filtro_Mes',fld:'vFILTRO_MES',pic:'ZZZZZZZZZ9',nv:0},{av:'AV22Filtro_Ano',fld:'vFILTRO_ANO',pic:'ZZZ9',nv:0}],oparms:[{av:'AV52SDT_FiltroConsContadorFM',fld:'vSDT_FILTROCONSCONTADORFM',pic:'',nv:null}]}");
         setEventMetadata("'DOEXPORT'","{handler:'E15CD1',iparms:[],oparms:[]}");
         setEventMetadata("'DOEXPORTREPORT'","{handler:'E16CD1',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A484ContagemResultado_StatusDmn = "";
         A500ContagemResultado_ContratadaPessoaNom = "";
         A474ContagemResultado_ContadorFMNom = "";
         GXKey = "";
         forbiddenHiddens = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV52SDT_FiltroConsContadorFM = new SdtSDT_FiltroConsContadorFM(context);
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV56Contratada_PessoaNom = "";
         scmdbuf = "";
         H00CD2_A5AreaTrabalho_Codigo = new int[1] ;
         H00CD2_A6AreaTrabalho_Descricao = new String[] {""} ;
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         H00CD3_A5AreaTrabalho_Codigo = new int[1] ;
         H00CD3_A6AreaTrabalho_Descricao = new String[] {""} ;
         H00CD4_A40Contratada_PessoaCod = new int[1] ;
         H00CD4_A39Contratada_Codigo = new int[1] ;
         H00CD4_A41Contratada_PessoaNom = new String[] {""} ;
         H00CD4_n41Contratada_PessoaNom = new bool[] {false} ;
         H00CD4_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00CD4_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00CD4_A43Contratada_Ativo = new bool[] {false} ;
         H00CD4_n43Contratada_Ativo = new bool[] {false} ;
         Grid1Container = new GXWebGrid( context);
         Gx_date = DateTime.MinValue;
         hsh = "";
         AV50WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         H00CD5_A74Contrato_Codigo = new int[1] ;
         H00CD5_A92Contrato_Ativo = new bool[] {false} ;
         H00CD5_A39Contratada_Codigo = new int[1] ;
         H00CD5_A116Contrato_ValorUnidadeContratacao = new decimal[1] ;
         H00CD6_A511ContagemResultado_HoraCnt = new String[] {""} ;
         H00CD6_A456ContagemResultado_Codigo = new int[1] ;
         H00CD6_A499ContagemResultado_ContratadaPessoaCod = new int[1] ;
         H00CD6_n499ContagemResultado_ContratadaPessoaCod = new bool[] {false} ;
         H00CD6_A479ContagemResultado_CrFMPessoaCod = new int[1] ;
         H00CD6_n479ContagemResultado_CrFMPessoaCod = new bool[] {false} ;
         H00CD6_A517ContagemResultado_Ultima = new bool[] {false} ;
         H00CD6_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00CD6_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00CD6_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00CD6_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00CD6_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         H00CD6_A458ContagemResultado_PFBFS = new decimal[1] ;
         H00CD6_n458ContagemResultado_PFBFS = new bool[] {false} ;
         H00CD6_A460ContagemResultado_PFBFM = new decimal[1] ;
         H00CD6_n460ContagemResultado_PFBFM = new bool[] {false} ;
         H00CD6_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00CD6_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00CD6_A43Contratada_Ativo = new bool[] {false} ;
         H00CD6_n43Contratada_Ativo = new bool[] {false} ;
         H00CD6_A500ContagemResultado_ContratadaPessoaNom = new String[] {""} ;
         H00CD6_n500ContagemResultado_ContratadaPessoaNom = new bool[] {false} ;
         H00CD6_A470ContagemResultado_ContadorFMCod = new int[1] ;
         H00CD6_A474ContagemResultado_ContadorFMNom = new String[] {""} ;
         H00CD6_n474ContagemResultado_ContadorFMNom = new bool[] {false} ;
         AV12ContagemResultado_ContadorFMNom = "";
         AV33mQtdeDmn = new int [12] ;
         AV31mPFBFM = new decimal [12] ;
         AV32mPFBFS = new decimal [12] ;
         AV26mCTPF = new decimal [12] ;
         AV34mRTPF = new decimal [12] ;
         AV28mErros = new int [12] ;
         AV30mPendencias = new int [12] ;
         AV25mCanceladas = new int [12] ;
         Grid1Row = new GXWebRow();
         AV6WebSession = context.GetSession();
         sStyleString = "";
         lblTextblockcontagemresultado_pflfstotal_Jsonclick = "";
         TempTags = "";
         lblTextblockcontagemresultado_pfbfmtotal_Jsonclick = "";
         lblTextblockcontagemresultado_pflfmtotal_Jsonclick = "";
         lblTextblockcontagemresultado_pftotal_Jsonclick = "";
         lblTextblockcontagemresultado_pftotalpagar_Jsonclick = "";
         lblContagemresultadotitle_Jsonclick = "";
         lblTextblock2_Jsonclick = "";
         lblTextblock4_Jsonclick = "";
         lblTextblock3_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         bttButton1_Jsonclick = "";
         subGrid1_Linesclass = "";
         Grid1Column = new GXWebColumn();
         imgExport_Jsonclick = "";
         imgExportreport_Jsonclick = "";
         imgExportgraphicdmn_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_consultacontratadas__default(),
            new Object[][] {
                new Object[] {
               H00CD2_A5AreaTrabalho_Codigo, H00CD2_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               H00CD3_A5AreaTrabalho_Codigo, H00CD3_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               H00CD4_A40Contratada_PessoaCod, H00CD4_A39Contratada_Codigo, H00CD4_A41Contratada_PessoaNom, H00CD4_n41Contratada_PessoaNom, H00CD4_A52Contratada_AreaTrabalhoCod, H00CD4_A43Contratada_Ativo
               }
               , new Object[] {
               H00CD5_A74Contrato_Codigo, H00CD5_A92Contrato_Ativo, H00CD5_A39Contratada_Codigo, H00CD5_A116Contrato_ValorUnidadeContratacao
               }
               , new Object[] {
               H00CD6_A511ContagemResultado_HoraCnt, H00CD6_A456ContagemResultado_Codigo, H00CD6_A499ContagemResultado_ContratadaPessoaCod, H00CD6_n499ContagemResultado_ContratadaPessoaCod, H00CD6_A479ContagemResultado_CrFMPessoaCod, H00CD6_n479ContagemResultado_CrFMPessoaCod, H00CD6_A517ContagemResultado_Ultima, H00CD6_A484ContagemResultado_StatusDmn, H00CD6_n484ContagemResultado_StatusDmn, H00CD6_A490ContagemResultado_ContratadaCod,
               H00CD6_n490ContagemResultado_ContratadaCod, H00CD6_A473ContagemResultado_DataCnt, H00CD6_A458ContagemResultado_PFBFS, H00CD6_n458ContagemResultado_PFBFS, H00CD6_A460ContagemResultado_PFBFM, H00CD6_n460ContagemResultado_PFBFM, H00CD6_A52Contratada_AreaTrabalhoCod, H00CD6_n52Contratada_AreaTrabalhoCod, H00CD6_A43Contratada_Ativo, H00CD6_n43Contratada_Ativo,
               H00CD6_A500ContagemResultado_ContratadaPessoaNom, H00CD6_n500ContagemResultado_ContratadaPessoaNom, H00CD6_A470ContagemResultado_ContadorFMCod, H00CD6_A474ContagemResultado_ContadorFMNom, H00CD6_n474ContagemResultado_ContadorFMNom
               }
            }
         );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         edtavContagemresultado_contadorfmcod_Enabled = 0;
         edtavContratada_pessoanom_Enabled = 0;
         cmbavMes.Enabled = 0;
         edtavPfbfs_Enabled = 0;
         edtavPfbfm_Enabled = 0;
         edtavQtdedmn_Enabled = 0;
         edtavCtpf_Enabled = 0;
         edtavRtpf_Enabled = 0;
         edtavTpfbfs_Enabled = 0;
         edtavTpfbfm_Enabled = 0;
         edtavTqtdedmn_Enabled = 0;
         edtavTctpf_Enabled = 0;
         edtavTrtpf_Enabled = 0;
      }

      private short nRcdExists_4 ;
      private short nIsMod_4 ;
      private short nRcdExists_5 ;
      private short nIsMod_5 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_26 ;
      private short nGXsfl_26_idx=1 ;
      private short AV22Filtro_Ano ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_26_Refreshing=0 ;
      private short subGrid1_Backcolorstyle ;
      private short AV24i ;
      private short AV8Ano ;
      private short GRID1_nEOF ;
      private short subGrid1_Titlebackstyle ;
      private short subGrid1_Allowselection ;
      private short subGrid1_Allowhovering ;
      private short subGrid1_Allowcollapsing ;
      private short subGrid1_Collapsed ;
      private short nGXWrapped ;
      private short subGrid1_Backstyle ;
      private short wbTemp ;
      private int AV9AreaTrabalho_Codigo ;
      private int A39Contratada_Codigo ;
      private int AV53Filtro_ContratadaCodigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A470ContagemResultado_ContadorFMCod ;
      private int AV48tQtdeDmn ;
      private int AV44tErros ;
      private int AV45tPendencias ;
      private int AV41tCanceladas ;
      private int AV51ContagemResultado_ContadorFMCod ;
      private int AV38QtdeDmn ;
      private int gxdynajaxindex ;
      private int subGrid1_Islastpage ;
      private int edtavContagemresultado_contadorfmcod_Enabled ;
      private int edtavContratada_pessoanom_Enabled ;
      private int edtavPfbfs_Enabled ;
      private int edtavPfbfm_Enabled ;
      private int edtavQtdedmn_Enabled ;
      private int edtavCtpf_Enabled ;
      private int edtavRtpf_Enabled ;
      private int edtavTpfbfs_Enabled ;
      private int edtavTpfbfm_Enabled ;
      private int edtavTqtdedmn_Enabled ;
      private int edtavTctpf_Enabled ;
      private int edtavTrtpf_Enabled ;
      private int subGrid1_Titleforecolor ;
      private int imgExport_Visible ;
      private int imgExportreport_Visible ;
      private int A456ContagemResultado_Codigo ;
      private int A499ContagemResultado_ContratadaPessoaCod ;
      private int A479ContagemResultado_CrFMPessoaCod ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int GX_I ;
      private int [] AV33mQtdeDmn ;
      private int [] AV28mErros ;
      private int [] AV30mPendencias ;
      private int [] AV25mCanceladas ;
      private int AV21Erros ;
      private int AV35Pendencias ;
      private int AV10Canceladas ;
      private int subGrid1_Titlebackcolor ;
      private int subGrid1_Allbackcolor ;
      private int subGrid1_Selectioncolor ;
      private int subGrid1_Hoveringcolor ;
      private int idxLst ;
      private int subGrid1_Backcolor ;
      private int edtavContagemresultado_contadorfmcod_Visible ;
      private int edtavContratada_pessoanom_Visible ;
      private int edtavPfbfs_Visible ;
      private int edtavPfbfm_Visible ;
      private int edtavQtdedmn_Visible ;
      private int edtavCtpf_Visible ;
      private int edtavRtpf_Visible ;
      private long AV55Filtro_Mes ;
      private long AV29Mes ;
      private long GRID1_nCurrentRecord ;
      private long GRID1_nFirstRecordOnPage ;
      private decimal A116Contrato_ValorUnidadeContratacao ;
      private decimal A458ContagemResultado_PFBFS ;
      private decimal A460ContagemResultado_PFBFM ;
      private decimal AV20CUPF ;
      private decimal AV47tPFBFS ;
      private decimal AV46tPFBFM ;
      private decimal AV42tCTPF ;
      private decimal AV49tRTPF ;
      private decimal AV37PFBFS ;
      private decimal AV36PFBFM ;
      private decimal AV19CTPF ;
      private decimal AV40RTPF ;
      private decimal [] AV31mPFBFM ;
      private decimal [] AV32mPFBFS ;
      private decimal [] AV26mCTPF ;
      private decimal [] AV34mRTPF ;
      private String edtavQtdedmn_Tooltiptext ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_26_idx="0001" ;
      private String edtavQtdedmn_Internalname ;
      private String A484ContagemResultado_StatusDmn ;
      private String A500ContagemResultado_ContratadaPessoaNom ;
      private String A474ContagemResultado_ContadorFMNom ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavContagemresultado_contadorfmcod_Internalname ;
      private String AV56Contratada_PessoaNom ;
      private String edtavContratada_pessoanom_Internalname ;
      private String cmbavMes_Internalname ;
      private String edtavPfbfs_Internalname ;
      private String edtavPfbfm_Internalname ;
      private String edtavCtpf_Internalname ;
      private String edtavRtpf_Internalname ;
      private String scmdbuf ;
      private String GXCCtl ;
      private String dynavAreatrabalho_codigo_Internalname ;
      private String gxwrpcisep ;
      private String edtavTpfbfs_Internalname ;
      private String edtavTpfbfm_Internalname ;
      private String edtavTqtdedmn_Internalname ;
      private String edtavTctpf_Internalname ;
      private String edtavTrtpf_Internalname ;
      private String dynavFiltro_contratadacodigo_Internalname ;
      private String cmbavFiltro_ano_Internalname ;
      private String hsh ;
      private String imgExport_Internalname ;
      private String imgExportreport_Internalname ;
      private String AV12ContagemResultado_ContadorFMNom ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String tblTable3_Internalname ;
      private String lblTextblockcontagemresultado_pflfstotal_Internalname ;
      private String lblTextblockcontagemresultado_pflfstotal_Jsonclick ;
      private String TempTags ;
      private String edtavTpfbfs_Jsonclick ;
      private String lblTextblockcontagemresultado_pfbfmtotal_Internalname ;
      private String lblTextblockcontagemresultado_pfbfmtotal_Jsonclick ;
      private String edtavTpfbfm_Jsonclick ;
      private String lblTextblockcontagemresultado_pflfmtotal_Internalname ;
      private String lblTextblockcontagemresultado_pflfmtotal_Jsonclick ;
      private String edtavTqtdedmn_Jsonclick ;
      private String lblTextblockcontagemresultado_pftotal_Internalname ;
      private String lblTextblockcontagemresultado_pftotal_Jsonclick ;
      private String edtavTctpf_Jsonclick ;
      private String lblTextblockcontagemresultado_pftotalpagar_Internalname ;
      private String lblTextblockcontagemresultado_pftotalpagar_Jsonclick ;
      private String edtavTrtpf_Jsonclick ;
      private String tblTable4_Internalname ;
      private String lblContagemresultadotitle_Internalname ;
      private String lblContagemresultadotitle_Jsonclick ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String dynavAreatrabalho_codigo_Jsonclick ;
      private String lblTextblock4_Internalname ;
      private String lblTextblock4_Jsonclick ;
      private String dynavFiltro_contratadacodigo_Jsonclick ;
      private String lblTextblock3_Internalname ;
      private String lblTextblock3_Jsonclick ;
      private String cmbavFiltro_ano_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String bttButton1_Internalname ;
      private String bttButton1_Jsonclick ;
      private String subGrid1_Internalname ;
      private String subGrid1_Class ;
      private String subGrid1_Linesclass ;
      private String tblTableactions_Internalname ;
      private String imgExport_Jsonclick ;
      private String imgExportreport_Jsonclick ;
      private String imgExportgraphicdmn_Internalname ;
      private String imgExportgraphicdmn_Jsonclick ;
      private String sGXsfl_26_fel_idx="0001" ;
      private String ROClassString ;
      private String edtavContagemresultado_contadorfmcod_Jsonclick ;
      private String edtavContratada_pessoanom_Jsonclick ;
      private String cmbavMes_Jsonclick ;
      private String edtavPfbfs_Jsonclick ;
      private String edtavPfbfm_Jsonclick ;
      private String edtavQtdedmn_Jsonclick ;
      private String edtavCtpf_Jsonclick ;
      private String edtavRtpf_Jsonclick ;
      private DateTime A473ContagemResultado_DataCnt ;
      private DateTime Gx_date ;
      private bool entryPointCalled ;
      private bool A92Contrato_Ativo ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool A517ContagemResultado_Ultima ;
      private bool n500ContagemResultado_ContratadaPessoaNom ;
      private bool n474ContagemResultado_ContadorFMNom ;
      private bool n458ContagemResultado_PFBFS ;
      private bool n460ContagemResultado_PFBFM ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool BRKCD4 ;
      private bool n499ContagemResultado_ContratadaPessoaCod ;
      private bool n479ContagemResultado_CrFMPessoaCod ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool A43Contratada_Ativo ;
      private bool n43Contratada_Ativo ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private GXWebGrid Grid1Container ;
      private GXWebRow Grid1Row ;
      private GXWebColumn Grid1Column ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavAreatrabalho_codigo ;
      private GXCombobox dynavFiltro_contratadacodigo ;
      private GXCombobox cmbavFiltro_ano ;
      private GXCombobox cmbavMes ;
      private IDataStoreProvider pr_default ;
      private int[] H00CD2_A5AreaTrabalho_Codigo ;
      private String[] H00CD2_A6AreaTrabalho_Descricao ;
      private int[] H00CD3_A5AreaTrabalho_Codigo ;
      private String[] H00CD3_A6AreaTrabalho_Descricao ;
      private int[] H00CD4_A40Contratada_PessoaCod ;
      private int[] H00CD4_A39Contratada_Codigo ;
      private String[] H00CD4_A41Contratada_PessoaNom ;
      private bool[] H00CD4_n41Contratada_PessoaNom ;
      private int[] H00CD4_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00CD4_n52Contratada_AreaTrabalhoCod ;
      private bool[] H00CD4_A43Contratada_Ativo ;
      private bool[] H00CD4_n43Contratada_Ativo ;
      private int[] H00CD5_A74Contrato_Codigo ;
      private bool[] H00CD5_A92Contrato_Ativo ;
      private int[] H00CD5_A39Contratada_Codigo ;
      private decimal[] H00CD5_A116Contrato_ValorUnidadeContratacao ;
      private String[] H00CD6_A511ContagemResultado_HoraCnt ;
      private int[] H00CD6_A456ContagemResultado_Codigo ;
      private int[] H00CD6_A499ContagemResultado_ContratadaPessoaCod ;
      private bool[] H00CD6_n499ContagemResultado_ContratadaPessoaCod ;
      private int[] H00CD6_A479ContagemResultado_CrFMPessoaCod ;
      private bool[] H00CD6_n479ContagemResultado_CrFMPessoaCod ;
      private bool[] H00CD6_A517ContagemResultado_Ultima ;
      private String[] H00CD6_A484ContagemResultado_StatusDmn ;
      private bool[] H00CD6_n484ContagemResultado_StatusDmn ;
      private int[] H00CD6_A490ContagemResultado_ContratadaCod ;
      private bool[] H00CD6_n490ContagemResultado_ContratadaCod ;
      private DateTime[] H00CD6_A473ContagemResultado_DataCnt ;
      private decimal[] H00CD6_A458ContagemResultado_PFBFS ;
      private bool[] H00CD6_n458ContagemResultado_PFBFS ;
      private decimal[] H00CD6_A460ContagemResultado_PFBFM ;
      private bool[] H00CD6_n460ContagemResultado_PFBFM ;
      private int[] H00CD6_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00CD6_n52Contratada_AreaTrabalhoCod ;
      private bool[] H00CD6_A43Contratada_Ativo ;
      private bool[] H00CD6_n43Contratada_Ativo ;
      private String[] H00CD6_A500ContagemResultado_ContratadaPessoaNom ;
      private bool[] H00CD6_n500ContagemResultado_ContratadaPessoaNom ;
      private int[] H00CD6_A470ContagemResultado_ContadorFMCod ;
      private String[] H00CD6_A474ContagemResultado_ContadorFMNom ;
      private bool[] H00CD6_n474ContagemResultado_ContadorFMNom ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IGxSession AV6WebSession ;
      private GXWebForm Form ;
      private SdtSDT_FiltroConsContadorFM AV52SDT_FiltroConsContadorFM ;
      private wwpbaseobjects.SdtWWPContext AV50WWPContext ;
   }

   public class wp_consultacontratadas__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00CD6( IGxContext context ,
                                             int AV53Filtro_ContratadaCodigo ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             DateTime A473ContagemResultado_DataCnt ,
                                             short AV22Filtro_Ano ,
                                             String A484ContagemResultado_StatusDmn ,
                                             bool A517ContagemResultado_Ultima )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [2] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_HoraCnt], T1.[ContagemResultado_Codigo], T3.[Contratada_PessoaCod] AS ContagemResultado_ContratadaPessoaCod, T5.[Usuario_PessoaCod] AS ContagemResultado_CrFMPessoaCod, T1.[ContagemResultado_Ultima], T2.[ContagemResultado_StatusDmn], T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_PFBFS], T1.[ContagemResultado_PFBFM], T3.[Contratada_AreaTrabalhoCod], T3.[Contratada_Ativo], T4.[Pessoa_Nome] AS ContagemResultado_ContratadaPessoaNom, T1.[ContagemResultado_ContadorFMCod] AS ContagemResultado_ContadorFMCod, T6.[Pessoa_Nome] AS ContagemResultado_ContadorFMNo FROM ((((([ContagemResultadoContagens] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod]) INNER JOIN [Usuario] T5 WITH (NOLOCK) ON T5.[Usuario_Codigo] = T1.[ContagemResultado_ContadorFMCod]) LEFT JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T5.[Usuario_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (YEAR(T1.[ContagemResultado_DataCnt]) = @AV22Filtro_Ano)";
         scmdbuf = scmdbuf + " and (Not T2.[ContagemResultado_StatusDmn] = 'A')";
         scmdbuf = scmdbuf + " and (Not T2.[ContagemResultado_StatusDmn] = 'X')";
         scmdbuf = scmdbuf + " and (Not T2.[ContagemResultado_StatusDmn] = 'D')";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_Ultima] = 1)";
         if ( ! (0==AV53Filtro_ContratadaCodigo) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV53Filtro_ContratadaCodigo)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T2.[ContagemResultado_ContratadaCod], T1.[ContagemResultado_DataCnt], T2.[ContagemResultado_StatusDmn], T1.[ContagemResultado_Ultima]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 4 :
                     return conditional_H00CD6(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00CD2 ;
          prmH00CD2 = new Object[] {
          } ;
          Object[] prmH00CD3 ;
          prmH00CD3 = new Object[] {
          } ;
          Object[] prmH00CD4 ;
          prmH00CD4 = new Object[] {
          new Object[] {"@AV9AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CD5 ;
          prmH00CD5 = new Object[] {
          new Object[] {"@AV53Filtro_ContratadaCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CD6 ;
          prmH00CD6 = new Object[] {
          new Object[] {"@AV22Filtro_Ano",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV53Filtro_ContratadaCodigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00CD2", "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) ORDER BY [AreaTrabalho_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CD2,0,0,true,false )
             ,new CursorDef("H00CD3", "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) ORDER BY [AreaTrabalho_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CD3,0,0,true,false )
             ,new CursorDef("H00CD4", "SELECT T2.[Pessoa_Codigo] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_AreaTrabalhoCod], T1.[Contratada_Ativo] FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE (T1.[Contratada_Ativo] = 1) AND (T1.[Contratada_AreaTrabalhoCod] = @AV9AreaTrabalho_Codigo) ORDER BY T2.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CD4,0,0,true,false )
             ,new CursorDef("H00CD5", "SELECT TOP 1 [Contrato_Codigo], [Contrato_Ativo], [Contratada_Codigo], [Contrato_ValorUnidadeContratacao] FROM [Contrato] WITH (NOLOCK) WHERE ([Contratada_Codigo] = @AV53Filtro_ContratadaCodigo) AND ([Contrato_Ativo] = 1) ORDER BY [Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CD5,1,0,false,true )
             ,new CursorDef("H00CD6", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CD6,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 5) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((String[]) buf[7])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((int[]) buf[9])[0] = rslt.getInt(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[11])[0] = rslt.getGXDate(8) ;
                ((decimal[]) buf[12])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((decimal[]) buf[14])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((int[]) buf[16])[0] = rslt.getInt(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((bool[]) buf[18])[0] = rslt.getBool(12) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((String[]) buf[20])[0] = rslt.getString(13, 100) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                ((int[]) buf[22])[0] = rslt.getInt(14) ;
                ((String[]) buf[23])[0] = rslt.getString(15, 100) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(15);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                return;
       }
    }

 }

}
