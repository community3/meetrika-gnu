/*
               File: WP_Agendamento
        Description: Agendamento
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:23:28.67
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_agendamento : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public wp_agendamento( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_agendamento( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_OriContagemResultado_Codigo )
      {
         this.AV13OriContagemResultado_Codigo = aP0_OriContagemResultado_Codigo;
         executePrivate();
         aP0_OriContagemResultado_Codigo=this.AV13OriContagemResultado_Codigo;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV13OriContagemResultado_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OriContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OriContagemResultado_Codigo), 6, 0)));
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAMN2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               edtavContagemresultado_owner_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_owner_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_owner_Enabled), 5, 0)));
               edtavContagemresultado_ownernome_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_ownernome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_ownernome_Enabled), 5, 0)));
               edtavContagemresultado_responsavel_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_responsavel_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_responsavel_Enabled), 5, 0)));
               edtavContagemresultado_responsavelnome_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_responsavelnome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_responsavelnome_Enabled), 5, 0)));
               WSMN2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEMN2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( "Agendamento") ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202031221233028");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_agendamento.aspx") + "?" + UrlEncode("" +AV13OriContagemResultado_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_OBSERVACAO", AV12ContagemResultado_Observacao);
         GxWebStd.gx_boolean_hidden_field( context, "vCHECKREQUIREDFIELDSRESULT", AV17CheckRequiredFieldsResult);
         GxWebStd.gx_hidden_field( context, "vORICONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OriContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_OWNER", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8ContagemResultado_Owner), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_OWNERNOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV15ContagemResultado_OwnerNome, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_RESPONSAVEL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9ContagemResultado_Responsavel), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_RESPONSAVELNOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV16ContagemResultado_ResponsavelNome, "@!"))));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_OBSERVACAO_Enabled", StringUtil.BoolToStr( Contagemresultado_observacao_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "WP_Agendamento";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV8ContagemResultado_Owner), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV9ContagemResultado_Responsavel), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("wp_agendamento:[SendSecurityCheck value for]"+"ContagemResultado_Owner:"+context.localUtil.Format( (decimal)(AV8ContagemResultado_Owner), "ZZZZZ9"));
         GXUtil.WriteLog("wp_agendamento:[SendSecurityCheck value for]"+"ContagemResultado_Responsavel:"+context.localUtil.Format( (decimal)(AV9ContagemResultado_Responsavel), "ZZZZZ9"));
      }

      protected void RenderHtmlCloseFormMN2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "WP_Agendamento" ;
      }

      public override String GetPgmdesc( )
      {
         return "Agendamento" ;
      }

      protected void WBMN0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            wb_table1_2_MN2( true) ;
         }
         else
         {
            wb_table1_2_MN2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_MN2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTMN2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Agendamento", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPMN0( ) ;
      }

      protected void WSMN2( )
      {
         STARTMN2( ) ;
         EVTMN2( ) ;
      }

      protected void EVTMN2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11MN2 */
                           E11MN2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( ! wbErr )
                           {
                              Rfr0gs = false;
                              if ( ! Rfr0gs )
                              {
                                 /* Execute user event: E12MN2 */
                                 E12MN2 ();
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13MN2 */
                           E13MN2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14MN2 */
                           E14MN2 ();
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEMN2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormMN2( ) ;
            }
         }
      }

      protected void PAMN2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavContagemresultado_owner_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFMN2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavContagemresultado_owner_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_owner_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_owner_Enabled), 5, 0)));
         edtavContagemresultado_ownernome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_ownernome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_ownernome_Enabled), 5, 0)));
         edtavContagemresultado_responsavel_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_responsavel_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_responsavel_Enabled), 5, 0)));
         edtavContagemresultado_responsavelnome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_responsavelnome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_responsavelnome_Enabled), 5, 0)));
      }

      protected void RFMN2( )
      {
         initialize_formulas( ) ;
         /* Execute user event: E13MN2 */
         E13MN2 ();
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E14MN2 */
            E14MN2 ();
            WBMN0( ) ;
         }
      }

      protected void STRUPMN0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavContagemresultado_owner_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_owner_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_owner_Enabled), 5, 0)));
         edtavContagemresultado_ownernome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_ownernome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_ownernome_Enabled), 5, 0)));
         edtavContagemresultado_responsavel_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_responsavel_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_responsavel_Enabled), 5, 0)));
         edtavContagemresultado_responsavelnome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_responsavelnome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_responsavelnome_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11MN2 */
         E11MN2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_owner_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_owner_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADO_OWNER");
               GX_FocusControl = edtavContagemresultado_owner_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV8ContagemResultado_Owner = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContagemResultado_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8ContagemResultado_Owner), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_OWNER", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8ContagemResultado_Owner), "ZZZZZ9")));
            }
            else
            {
               AV8ContagemResultado_Owner = (int)(context.localUtil.CToN( cgiGet( edtavContagemresultado_owner_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContagemResultado_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8ContagemResultado_Owner), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_OWNER", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8ContagemResultado_Owner), "ZZZZZ9")));
            }
            AV15ContagemResultado_OwnerNome = StringUtil.Upper( cgiGet( edtavContagemresultado_ownernome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ContagemResultado_OwnerNome", AV15ContagemResultado_OwnerNome);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_OWNERNOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV15ContagemResultado_OwnerNome, "@!"))));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_responsavel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_responsavel_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADO_RESPONSAVEL");
               GX_FocusControl = edtavContagemresultado_responsavel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV9ContagemResultado_Responsavel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ContagemResultado_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9ContagemResultado_Responsavel), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_RESPONSAVEL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9ContagemResultado_Responsavel), "ZZZZZ9")));
            }
            else
            {
               AV9ContagemResultado_Responsavel = (int)(context.localUtil.CToN( cgiGet( edtavContagemresultado_responsavel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ContagemResultado_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9ContagemResultado_Responsavel), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_RESPONSAVEL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9ContagemResultado_Responsavel), "ZZZZZ9")));
            }
            AV16ContagemResultado_ResponsavelNome = StringUtil.Upper( cgiGet( edtavContagemresultado_responsavelnome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContagemResultado_ResponsavelNome", AV16ContagemResultado_ResponsavelNome);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_RESPONSAVELNOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV16ContagemResultado_ResponsavelNome, "@!"))));
            AV10ContagemResultado_Descricao = cgiGet( edtavContagemresultado_descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10ContagemResultado_Descricao", AV10ContagemResultado_Descricao);
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_dataprevista_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado_Data Prevista"}), 1, "vCONTAGEMRESULTADO_DATAPREVISTA");
               GX_FocusControl = edtavContagemresultado_dataprevista_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV11ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11ContagemResultado_DataPrevista", context.localUtil.TToC( AV11ContagemResultado_DataPrevista, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV11ContagemResultado_DataPrevista = context.localUtil.CToT( cgiGet( edtavContagemresultado_dataprevista_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11ContagemResultado_DataPrevista", context.localUtil.TToC( AV11ContagemResultado_DataPrevista, 8, 5, 0, 3, "/", ":", " "));
            }
            /* Read saved values. */
            AV12ContagemResultado_Observacao = cgiGet( "vCONTAGEMRESULTADO_OBSERVACAO");
            Contagemresultado_observacao_Enabled = StringUtil.StrToBool( cgiGet( "CONTAGEMRESULTADO_OBSERVACAO_Enabled"));
            Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
            Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
            Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
            Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
            Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
            Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
            Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
            Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
            Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
            Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = "hsh" + "WP_Agendamento";
            AV8ContagemResultado_Owner = (int)(context.localUtil.CToN( cgiGet( edtavContagemresultado_owner_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContagemResultado_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8ContagemResultado_Owner), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_OWNER", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8ContagemResultado_Owner), "ZZZZZ9")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV8ContagemResultado_Owner), "ZZZZZ9");
            AV9ContagemResultado_Responsavel = (int)(context.localUtil.CToN( cgiGet( edtavContagemresultado_responsavel_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ContagemResultado_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9ContagemResultado_Responsavel), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_RESPONSAVEL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9ContagemResultado_Responsavel), "ZZZZZ9")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV9ContagemResultado_Responsavel), "ZZZZZ9");
            hsh = cgiGet( "hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("wp_agendamento:[SecurityCheckFailed value for]"+"ContagemResultado_Owner:"+context.localUtil.Format( (decimal)(AV8ContagemResultado_Owner), "ZZZZZ9"));
               GXUtil.WriteLog("wp_agendamento:[SecurityCheckFailed value for]"+"ContagemResultado_Responsavel:"+context.localUtil.Format( (decimal)(AV9ContagemResultado_Responsavel), "ZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11MN2 */
         E11MN2 ();
         if (returnInSub) return;
      }

      protected void E11MN2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV14WWPContext) ;
         AV8ContagemResultado_Owner = AV14WWPContext.gxTpr_Userid;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContagemResultado_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8ContagemResultado_Owner), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_OWNER", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8ContagemResultado_Owner), "ZZZZZ9")));
         AV15ContagemResultado_OwnerNome = AV14WWPContext.gxTpr_Username;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ContagemResultado_OwnerNome", AV15ContagemResultado_OwnerNome);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_OWNERNOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV15ContagemResultado_OwnerNome, "@!"))));
         AV18ContagemResultado.Load(AV13OriContagemResultado_Codigo);
         if ( AV18ContagemResultado.Success() )
         {
            AV9ContagemResultado_Responsavel = AV18ContagemResultado.gxTpr_Contagemresultado_responsavel;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ContagemResultado_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9ContagemResultado_Responsavel), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_RESPONSAVEL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9ContagemResultado_Responsavel), "ZZZZZ9")));
            AV20Usuario.Load(AV9ContagemResultado_Responsavel);
            if ( AV20Usuario.Success() )
            {
               AV16ContagemResultado_ResponsavelNome = AV20Usuario.gxTpr_Usuario_nome;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContagemResultado_ResponsavelNome", AV16ContagemResultado_ResponsavelNome);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_RESPONSAVELNOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV16ContagemResultado_ResponsavelNome, "@!"))));
            }
         }
         else
         {
            AV6Messages = AV18ContagemResultado.GetMessages();
            /* Execute user subroutine: 'SHOW MESSAGES' */
            S112 ();
            if (returnInSub) return;
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E12MN2 */
         E12MN2 ();
         if (returnInSub) return;
      }

      protected void E12MN2( )
      {
         /* Enter Routine */
         /* Execute user subroutine: 'CHECKREQUIREDFIELDS' */
         S122 ();
         if (returnInSub) return;
         if ( AV17CheckRequiredFieldsResult )
         {
            AV18ContagemResultado.Load(AV13OriContagemResultado_Codigo);
            if ( AV18ContagemResultado.Success() )
            {
               AV19ContagemResultado_ContratadaCod = AV18ContagemResultado.gxTpr_Contagemresultado_contratadacod;
            }
            if ( new prc_contagemresultadoagenda_gravar(context).executeUdp(  AV8ContagemResultado_Owner,  AV13OriContagemResultado_Codigo,  AV9ContagemResultado_Responsavel,  AV10ContagemResultado_Descricao,  AV12ContagemResultado_Observacao) )
            {
               context.CommitDataStores( "WP_Agendamento");
               GX_msglist.addItem("Agendamento criado com sucesso!");
               bttBtnenter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Visible), 5, 0)));
            }
            else
            {
               GX_msglist.addItem("Erro ao gravar o agendamento.");
            }
         }
      }

      protected void S122( )
      {
         /* 'CHECKREQUIREDFIELDS' Routine */
         AV17CheckRequiredFieldsResult = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17CheckRequiredFieldsResult", AV17CheckRequiredFieldsResult);
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV10ContagemResultado_Descricao)) )
         {
            GX_msglist.addItem("Assunto � obrigat�rio.");
            AV17CheckRequiredFieldsResult = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17CheckRequiredFieldsResult", AV17CheckRequiredFieldsResult);
         }
         if ( (DateTime.MinValue==AV11ContagemResultado_DataPrevista) )
         {
            GX_msglist.addItem("Data Prevista � obrigat�rio.");
            AV17CheckRequiredFieldsResult = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17CheckRequiredFieldsResult", AV17CheckRequiredFieldsResult);
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV12ContagemResultado_Observacao)) )
         {
            GX_msglist.addItem("Pauta � obrigat�rio.");
            AV17CheckRequiredFieldsResult = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17CheckRequiredFieldsResult", AV17CheckRequiredFieldsResult);
         }
      }

      protected void S112( )
      {
         /* 'SHOW MESSAGES' Routine */
         AV23GXV1 = 1;
         while ( AV23GXV1 <= AV6Messages.Count )
         {
            AV5Message = ((SdtMessages_Message)AV6Messages.Item(AV23GXV1));
            GX_msglist.addItem(AV5Message.gxTpr_Description);
            AV23GXV1 = (int)(AV23GXV1+1);
         }
      }

      protected void E13MN2( )
      {
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV7Context) ;
      }

      protected void nextLoad( )
      {
      }

      protected void E14MN2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_MN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktitle_Internalname, "Agendamento", "", "", lblTextblocktitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_Agendamento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_MN2( true) ;
         }
         else
         {
            wb_table2_8_MN2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_MN2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_54_MN2( true) ;
         }
         else
         {
            wb_table3_54_MN2( false) ;
         }
         return  ;
      }

      protected void wb_table3_54_MN2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_MN2e( true) ;
         }
         else
         {
            wb_table1_2_MN2e( false) ;
         }
      }

      protected void wb_table3_54_MN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnenter_Internalname, "", "Confirmar", bttBtnenter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtnenter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_Agendamento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_Agendamento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_54_MN2e( true) ;
         }
         else
         {
            wb_table3_54_MN2e( false) ;
         }
      }

      protected void wb_table2_8_MN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_16_MN2( true) ;
         }
         else
         {
            wb_table4_16_MN2( false) ;
         }
         return  ;
      }

      protected void wb_table4_16_MN2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_MN2e( true) ;
         }
         else
         {
            wb_table2_8_MN2e( false) ;
         }
      }

      protected void wb_table4_16_MN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_owner_Internalname, "Requisitante", "", "", lblTextblockcontagemresultado_owner_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_Agendamento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table5_21_MN2( true) ;
         }
         else
         {
            wb_table5_21_MN2( false) ;
         }
         return  ;
      }

      protected void wb_table5_21_MN2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_responsavel_Internalname, "Requisitado", "", "", lblTextblockcontagemresultado_responsavel_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_Agendamento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table6_31_MN2( true) ;
         }
         else
         {
            wb_table6_31_MN2( false) ;
         }
         return  ;
      }

      protected void wb_table6_31_MN2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_descricao_Internalname, "Assunto", "", "", lblTextblockcontagemresultado_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_Agendamento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_descricao_Internalname, AV10ContagemResultado_Descricao, StringUtil.RTrim( context.localUtil.Format( AV10ContagemResultado_Descricao, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,41);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_descricao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 100, "%", 1, "row", 500, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_Agendamento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_dataprevista_Internalname, "Data Prevista", "", "", lblTextblockcontagemresultado_dataprevista_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_Agendamento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_dataprevista_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_dataprevista_Internalname, context.localUtil.TToC( AV11ContagemResultado_DataPrevista, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV11ContagemResultado_DataPrevista, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,46);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_dataprevista_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_Agendamento.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_dataprevista_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_Agendamento.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_observacao_Internalname, "Pauta", "", "", lblTextblockcontagemresultado_observacao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_Agendamento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"CONTAGEMRESULTADO_OBSERVACAOContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_16_MN2e( true) ;
         }
         else
         {
            wb_table4_16_MN2e( false) ;
         }
      }

      protected void wb_table6_31_MN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontagemresultado_responsavel_Internalname, tblTablemergedcontagemresultado_responsavel_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_responsavel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9ContagemResultado_Responsavel), 6, 0, ",", "")), ((edtavContagemresultado_responsavel_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV9ContagemResultado_Responsavel), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV9ContagemResultado_Responsavel), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_responsavel_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavContagemresultado_responsavel_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_Agendamento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_responsavelnome_Internalname, StringUtil.RTrim( AV16ContagemResultado_ResponsavelNome), StringUtil.RTrim( context.localUtil.Format( AV16ContagemResultado_ResponsavelNome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,36);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_responsavelnome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavContagemresultado_responsavelnome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_Agendamento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_31_MN2e( true) ;
         }
         else
         {
            wb_table6_31_MN2e( false) ;
         }
      }

      protected void wb_table5_21_MN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontagemresultado_owner_Internalname, tblTablemergedcontagemresultado_owner_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_owner_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8ContagemResultado_Owner), 6, 0, ",", "")), ((edtavContagemresultado_owner_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV8ContagemResultado_Owner), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV8ContagemResultado_Owner), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,24);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_owner_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavContagemresultado_owner_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_Agendamento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_ownernome_Internalname, StringUtil.RTrim( AV15ContagemResultado_OwnerNome), StringUtil.RTrim( context.localUtil.Format( AV15ContagemResultado_OwnerNome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,26);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_ownernome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavContagemresultado_ownernome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_Agendamento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_21_MN2e( true) ;
         }
         else
         {
            wb_table5_21_MN2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV13OriContagemResultado_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OriContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OriContagemResultado_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAMN2( ) ;
         WSMN2( ) ;
         WEMN2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202031221233076");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_agendamento.js", "?202031221233076");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblocktitle_Internalname = "TEXTBLOCKTITLE";
         lblTextblockcontagemresultado_owner_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_OWNER";
         edtavContagemresultado_owner_Internalname = "vCONTAGEMRESULTADO_OWNER";
         edtavContagemresultado_ownernome_Internalname = "vCONTAGEMRESULTADO_OWNERNOME";
         tblTablemergedcontagemresultado_owner_Internalname = "TABLEMERGEDCONTAGEMRESULTADO_OWNER";
         lblTextblockcontagemresultado_responsavel_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_RESPONSAVEL";
         edtavContagemresultado_responsavel_Internalname = "vCONTAGEMRESULTADO_RESPONSAVEL";
         edtavContagemresultado_responsavelnome_Internalname = "vCONTAGEMRESULTADO_RESPONSAVELNOME";
         tblTablemergedcontagemresultado_responsavel_Internalname = "TABLEMERGEDCONTAGEMRESULTADO_RESPONSAVEL";
         lblTextblockcontagemresultado_descricao_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_DESCRICAO";
         edtavContagemresultado_descricao_Internalname = "vCONTAGEMRESULTADO_DESCRICAO";
         lblTextblockcontagemresultado_dataprevista_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_DATAPREVISTA";
         edtavContagemresultado_dataprevista_Internalname = "vCONTAGEMRESULTADO_DATAPREVISTA";
         lblTextblockcontagemresultado_observacao_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_OBSERVACAO";
         Contagemresultado_observacao_Internalname = "CONTAGEMRESULTADO_OBSERVACAO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtnenter_Internalname = "BTNENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavContagemresultado_ownernome_Jsonclick = "";
         edtavContagemresultado_ownernome_Enabled = 1;
         edtavContagemresultado_owner_Jsonclick = "";
         edtavContagemresultado_owner_Enabled = 1;
         edtavContagemresultado_responsavelnome_Jsonclick = "";
         edtavContagemresultado_responsavelnome_Enabled = 1;
         edtavContagemresultado_responsavel_Jsonclick = "";
         edtavContagemresultado_responsavel_Enabled = 1;
         Contagemresultado_observacao_Enabled = Convert.ToBoolean( 1);
         edtavContagemresultado_dataprevista_Jsonclick = "";
         edtavContagemresultado_descricao_Jsonclick = "";
         bttBtnenter_Visible = 1;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Dados Gerais";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("ENTER","{handler:'E12MN2',iparms:[{av:'AV17CheckRequiredFieldsResult',fld:'vCHECKREQUIREDFIELDSRESULT',pic:'',nv:false},{av:'AV13OriContagemResultado_Codigo',fld:'vORICONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8ContagemResultado_Owner',fld:'vCONTAGEMRESULTADO_OWNER',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV9ContagemResultado_Responsavel',fld:'vCONTAGEMRESULTADO_RESPONSAVEL',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV10ContagemResultado_Descricao',fld:'vCONTAGEMRESULTADO_DESCRICAO',pic:'',nv:''},{av:'AV12ContagemResultado_Observacao',fld:'vCONTAGEMRESULTADO_OBSERVACAO',pic:'',nv:''},{av:'AV11ContagemResultado_DataPrevista',fld:'vCONTAGEMRESULTADO_DATAPREVISTA',pic:'99/99/99 99:99',nv:''}],oparms:[{ctrl:'BTNENTER',prop:'Visible'},{av:'AV17CheckRequiredFieldsResult',fld:'vCHECKREQUIREDFIELDSRESULT',pic:'',nv:false}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV12ContagemResultado_Observacao = "";
         AV15ContagemResultado_OwnerNome = "";
         AV16ContagemResultado_ResponsavelNome = "";
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         sPrefix = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV10ContagemResultado_Descricao = "";
         AV11ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         hsh = "";
         AV14WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV18ContagemResultado = new SdtContagemResultado(context);
         AV20Usuario = new SdtUsuario(context);
         AV6Messages = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         AV5Message = new SdtMessages_Message(context);
         AV7Context = new wwpbaseobjects.SdtWWPContext(context);
         sStyleString = "";
         lblTextblocktitle_Jsonclick = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtnenter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         lblTextblockcontagemresultado_owner_Jsonclick = "";
         lblTextblockcontagemresultado_responsavel_Jsonclick = "";
         lblTextblockcontagemresultado_descricao_Jsonclick = "";
         lblTextblockcontagemresultado_dataprevista_Jsonclick = "";
         lblTextblockcontagemresultado_observacao_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_agendamento__default(),
            new Object[][] {
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavContagemresultado_owner_Enabled = 0;
         edtavContagemresultado_ownernome_Enabled = 0;
         edtavContagemresultado_responsavel_Enabled = 0;
         edtavContagemresultado_responsavelnome_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int AV13OriContagemResultado_Codigo ;
      private int wcpOAV13OriContagemResultado_Codigo ;
      private int edtavContagemresultado_owner_Enabled ;
      private int edtavContagemresultado_ownernome_Enabled ;
      private int edtavContagemresultado_responsavel_Enabled ;
      private int edtavContagemresultado_responsavelnome_Enabled ;
      private int AV8ContagemResultado_Owner ;
      private int AV9ContagemResultado_Responsavel ;
      private int AV19ContagemResultado_ContratadaCod ;
      private int bttBtnenter_Visible ;
      private int AV23GXV1 ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String edtavContagemresultado_owner_Internalname ;
      private String edtavContagemresultado_ownernome_Internalname ;
      private String edtavContagemresultado_responsavel_Internalname ;
      private String edtavContagemresultado_responsavelnome_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV15ContagemResultado_OwnerNome ;
      private String AV16ContagemResultado_ResponsavelNome ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavContagemresultado_descricao_Internalname ;
      private String edtavContagemresultado_dataprevista_Internalname ;
      private String hsh ;
      private String bttBtnenter_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String lblTextblocktitle_Internalname ;
      private String lblTextblocktitle_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtnenter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcontagemresultado_owner_Internalname ;
      private String lblTextblockcontagemresultado_owner_Jsonclick ;
      private String lblTextblockcontagemresultado_responsavel_Internalname ;
      private String lblTextblockcontagemresultado_responsavel_Jsonclick ;
      private String lblTextblockcontagemresultado_descricao_Internalname ;
      private String lblTextblockcontagemresultado_descricao_Jsonclick ;
      private String edtavContagemresultado_descricao_Jsonclick ;
      private String lblTextblockcontagemresultado_dataprevista_Internalname ;
      private String lblTextblockcontagemresultado_dataprevista_Jsonclick ;
      private String edtavContagemresultado_dataprevista_Jsonclick ;
      private String lblTextblockcontagemresultado_observacao_Internalname ;
      private String lblTextblockcontagemresultado_observacao_Jsonclick ;
      private String tblTablemergedcontagemresultado_responsavel_Internalname ;
      private String edtavContagemresultado_responsavel_Jsonclick ;
      private String edtavContagemresultado_responsavelnome_Jsonclick ;
      private String tblTablemergedcontagemresultado_owner_Internalname ;
      private String edtavContagemresultado_owner_Jsonclick ;
      private String edtavContagemresultado_ownernome_Jsonclick ;
      private String Contagemresultado_observacao_Internalname ;
      private String Dvpanel_tableattributes_Internalname ;
      private DateTime AV11ContagemResultado_DataPrevista ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool AV17CheckRequiredFieldsResult ;
      private bool Contagemresultado_observacao_Enabled ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private String AV12ContagemResultado_Observacao ;
      private String AV10ContagemResultado_Descricao ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_OriContagemResultado_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IDataStoreProvider pr_default ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV6Messages ;
      private SdtMessages_Message AV5Message ;
      private wwpbaseobjects.SdtWWPContext AV14WWPContext ;
      private wwpbaseobjects.SdtWWPContext AV7Context ;
      private SdtContagemResultado AV18ContagemResultado ;
      private SdtUsuario AV20Usuario ;
   }

   public class wp_agendamento__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
