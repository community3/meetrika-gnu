/*
               File: PRC_FUPF
        Description: Pontos de Fun��o da Fun��o de Usu�rio
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:43.20
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_fupf : GXProcedure
   {
      public prc_fupf( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_fupf( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_FuncaoUsuario_Codigo ,
                           ref short aP1_FuncaoUsuario_PF )
      {
         this.A161FuncaoUsuario_Codigo = aP0_FuncaoUsuario_Codigo;
         this.AV8FuncaoUsuario_PF = aP1_FuncaoUsuario_PF;
         initialize();
         executePrivate();
         aP0_FuncaoUsuario_Codigo=this.A161FuncaoUsuario_Codigo;
         aP1_FuncaoUsuario_PF=this.AV8FuncaoUsuario_PF;
      }

      public short executeUdp( ref int aP0_FuncaoUsuario_Codigo )
      {
         this.A161FuncaoUsuario_Codigo = aP0_FuncaoUsuario_Codigo;
         this.AV8FuncaoUsuario_PF = aP1_FuncaoUsuario_PF;
         initialize();
         executePrivate();
         aP0_FuncaoUsuario_Codigo=this.A161FuncaoUsuario_Codigo;
         aP1_FuncaoUsuario_PF=this.AV8FuncaoUsuario_PF;
         return AV8FuncaoUsuario_PF ;
      }

      public void executeSubmit( ref int aP0_FuncaoUsuario_Codigo ,
                                 ref short aP1_FuncaoUsuario_PF )
      {
         prc_fupf objprc_fupf;
         objprc_fupf = new prc_fupf();
         objprc_fupf.A161FuncaoUsuario_Codigo = aP0_FuncaoUsuario_Codigo;
         objprc_fupf.AV8FuncaoUsuario_PF = aP1_FuncaoUsuario_PF;
         objprc_fupf.context.SetSubmitInitialConfig(context);
         objprc_fupf.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_fupf);
         aP0_FuncaoUsuario_Codigo=this.A161FuncaoUsuario_Codigo;
         aP1_FuncaoUsuario_PF=this.AV8FuncaoUsuario_PF;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_fupf)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8FuncaoUsuario_PF = 0;
         /* Using cursor P00242 */
         pr_default.execute(0, new Object[] {A161FuncaoUsuario_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A165FuncaoAPF_Codigo = P00242_A165FuncaoAPF_Codigo[0];
            GXt_decimal1 = A386FuncaoAPF_PF;
            new prc_fapfpf(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_decimal1) ;
            A386FuncaoAPF_PF = GXt_decimal1;
            /* Using cursor P00243 */
            pr_default.execute(1, new Object[] {A165FuncaoAPF_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A183FuncaoAPF_Ativo = P00243_A183FuncaoAPF_Ativo[0];
               AV8FuncaoUsuario_PF = (short)(AV8FuncaoUsuario_PF+A386FuncaoAPF_PF);
               /* Using cursor P00244 */
               pr_default.execute(2, new Object[] {A165FuncaoAPF_Codigo});
               while ( (pr_default.getStatus(2) != 101) )
               {
                  A378FuncaoAPFAtributos_FuncaoDadosCod = P00244_A378FuncaoAPFAtributos_FuncaoDadosCod[0];
                  n378FuncaoAPFAtributos_FuncaoDadosCod = P00244_n378FuncaoAPFAtributos_FuncaoDadosCod[0];
                  A755FuncaoDados_Contar = P00244_A755FuncaoDados_Contar[0];
                  n755FuncaoDados_Contar = P00244_n755FuncaoDados_Contar[0];
                  A755FuncaoDados_Contar = P00244_A755FuncaoDados_Contar[0];
                  n755FuncaoDados_Contar = P00244_n755FuncaoDados_Contar[0];
                  /* Using cursor P00245 */
                  pr_default.execute(3, new Object[] {n378FuncaoAPFAtributos_FuncaoDadosCod, A378FuncaoAPFAtributos_FuncaoDadosCod});
                  while ( (pr_default.getStatus(3) != 101) )
                  {
                     A368FuncaoDados_Codigo = P00245_A368FuncaoDados_Codigo[0];
                     A394FuncaoDados_Ativo = P00245_A394FuncaoDados_Ativo[0];
                     AV8FuncaoUsuario_PF = (short)(AV8FuncaoUsuario_PF+A377FuncaoDados_PF);
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                     /* Exiting from a For First loop. */
                     if (true) break;
                  }
                  pr_default.close(3);
                  pr_default.readNext(2);
               }
               pr_default.close(2);
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00242_A161FuncaoUsuario_Codigo = new int[1] ;
         P00242_A165FuncaoAPF_Codigo = new int[1] ;
         P00243_A165FuncaoAPF_Codigo = new int[1] ;
         P00243_A183FuncaoAPF_Ativo = new String[] {""} ;
         A183FuncaoAPF_Ativo = "";
         P00244_A165FuncaoAPF_Codigo = new int[1] ;
         P00244_A378FuncaoAPFAtributos_FuncaoDadosCod = new int[1] ;
         P00244_n378FuncaoAPFAtributos_FuncaoDadosCod = new bool[] {false} ;
         P00244_A755FuncaoDados_Contar = new bool[] {false} ;
         P00244_n755FuncaoDados_Contar = new bool[] {false} ;
         P00245_A368FuncaoDados_Codigo = new int[1] ;
         P00245_A394FuncaoDados_Ativo = new String[] {""} ;
         A394FuncaoDados_Ativo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_fupf__default(),
            new Object[][] {
                new Object[] {
               P00242_A161FuncaoUsuario_Codigo, P00242_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               P00243_A165FuncaoAPF_Codigo, P00243_A183FuncaoAPF_Ativo
               }
               , new Object[] {
               P00244_A165FuncaoAPF_Codigo, P00244_A378FuncaoAPFAtributos_FuncaoDadosCod, P00244_n378FuncaoAPFAtributos_FuncaoDadosCod, P00244_A755FuncaoDados_Contar, P00244_n755FuncaoDados_Contar
               }
               , new Object[] {
               P00245_A368FuncaoDados_Codigo, P00245_A394FuncaoDados_Ativo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV8FuncaoUsuario_PF ;
      private int A161FuncaoUsuario_Codigo ;
      private int A165FuncaoAPF_Codigo ;
      private int A378FuncaoAPFAtributos_FuncaoDadosCod ;
      private int A368FuncaoDados_Codigo ;
      private decimal A386FuncaoAPF_PF ;
      private decimal GXt_decimal1 ;
      private decimal A377FuncaoDados_PF ;
      private String scmdbuf ;
      private String A183FuncaoAPF_Ativo ;
      private String A394FuncaoDados_Ativo ;
      private bool n378FuncaoAPFAtributos_FuncaoDadosCod ;
      private bool A755FuncaoDados_Contar ;
      private bool n755FuncaoDados_Contar ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_FuncaoUsuario_Codigo ;
      private short aP1_FuncaoUsuario_PF ;
      private IDataStoreProvider pr_default ;
      private int[] P00242_A161FuncaoUsuario_Codigo ;
      private int[] P00242_A165FuncaoAPF_Codigo ;
      private int[] P00243_A165FuncaoAPF_Codigo ;
      private String[] P00243_A183FuncaoAPF_Ativo ;
      private int[] P00244_A165FuncaoAPF_Codigo ;
      private int[] P00244_A378FuncaoAPFAtributos_FuncaoDadosCod ;
      private bool[] P00244_n378FuncaoAPFAtributos_FuncaoDadosCod ;
      private bool[] P00244_A755FuncaoDados_Contar ;
      private bool[] P00244_n755FuncaoDados_Contar ;
      private int[] P00245_A368FuncaoDados_Codigo ;
      private String[] P00245_A394FuncaoDados_Ativo ;
   }

   public class prc_fupf__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00242 ;
          prmP00242 = new Object[] {
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00243 ;
          prmP00243 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00244 ;
          prmP00244 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00245 ;
          prmP00245 = new Object[] {
          new Object[] {"@FuncaoAPFAtributos_FuncaoDadosCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00242", "SELECT [FuncaoUsuario_Codigo], [FuncaoAPF_Codigo] FROM [FuncoesUsuarioFuncoesAPF] WITH (NOLOCK) WHERE [FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo ORDER BY [FuncaoUsuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00242,100,0,true,false )
             ,new CursorDef("P00243", "SELECT [FuncaoAPF_Codigo], [FuncaoAPF_Ativo] FROM [FuncoesAPF] WITH (NOLOCK) WHERE ([FuncaoAPF_Codigo] = @FuncaoAPF_Codigo) AND ([FuncaoAPF_Ativo] = 'A') ORDER BY [FuncaoAPF_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00243,1,0,true,true )
             ,new CursorDef("P00244", "SELECT DISTINCT NULL AS [FuncaoAPF_Codigo], [FuncaoAPFAtributos_FuncaoDadosCod], NULL AS [FuncaoDados_Contar] FROM ( SELECT TOP(100) PERCENT T1.[FuncaoAPF_Codigo], T1.[FuncaoAPFAtributos_FuncaoDadosCod] AS FuncaoAPFAtributos_FuncaoDadosCod, T2.[FuncaoDados_Contar] FROM ([FuncoesAPFAtributos] T1 WITH (NOLOCK) LEFT JOIN [FuncaoDados] T2 WITH (NOLOCK) ON T2.[FuncaoDados_Codigo] = T1.[FuncaoAPFAtributos_FuncaoDadosCod]) WHERE T1.[FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ORDER BY T1.[FuncaoAPF_Codigo]) DistinctT ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00244,100,0,true,false )
             ,new CursorDef("P00245", "SELECT TOP 1 [FuncaoDados_Codigo], [FuncaoDados_Ativo] FROM [FuncaoDados] WITH (NOLOCK) WHERE ([FuncaoDados_Codigo] = @FuncaoAPFAtributos_FuncaoDadosCod) AND ([FuncaoDados_Ativo] = 'A') ORDER BY [FuncaoDados_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00245,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
