/*
               File: PRC_NewRecTRF
        Description: New Registro de Esfor�o TRF
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:5:24.45
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_newrectrf : GXProcedure
   {
      public prc_newrectrf( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_newrectrf( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_AreaTrabalho_Codigo ,
                           ref String aP1_ContagemResultado_Demanda ,
                           ref String aP2_ContagemResultado_DemandaFM ,
                           ref int aP3_ContagemResultado_ContratadaCod ,
                           ref int aP4_Servico_Codigo ,
                           ref int aP5_ContagemResultado_SistemaCod ,
                           ref DateTime aP6_ContagemResultado_DataDmn ,
                           ref String aP7_ContagemResultado_Descricao ,
                           ref int aP8_ContagemResultado_NaoCnfDmnCod ,
                           ref short aP9_ContagemResultadoContagens_Esforco ,
                           ref decimal aP10_ContagemResultado_PFBFM ,
                           ref decimal aP11_ContagemResultado_PFLFM ,
                           ref decimal aP12_ContagemResultado_PFBFS ,
                           ref decimal aP13_ContagemResultado_PFLFS ,
                           ref int aP14_ContagemResultado_ContadorFMCod ,
                           ref int aP15_ContagemResultado_ContadorFSCod ,
                           ref decimal aP16_ContagemResultado_ValorPF ,
                           ref bool aP17_ContagemResultado_EhValidacao ,
                           ref int aP18_DataEmNumero ,
                           ref int aP19_ContagemResultado_CntSrvCod )
      {
         this.AV21AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV18ContagemResultado_Demanda = aP1_ContagemResultado_Demanda;
         this.AV11ContagemResultado_DemandaFM = aP2_ContagemResultado_DemandaFM;
         this.A490ContagemResultado_ContratadaCod = aP3_ContagemResultado_ContratadaCod;
         this.AV22Servico_Codigo = aP4_Servico_Codigo;
         this.A489ContagemResultado_SistemaCod = aP5_ContagemResultado_SistemaCod;
         this.AV10ContagemResultado_DataDmn = aP6_ContagemResultado_DataDmn;
         this.AV12ContagemResultado_Descricao = aP7_ContagemResultado_Descricao;
         this.AV14ContagemResultado_NaoCnfDmnCod = aP8_ContagemResultado_NaoCnfDmnCod;
         this.A482ContagemResultadoContagens_Esforco = aP9_ContagemResultadoContagens_Esforco;
         this.AV15ContagemResultado_PFBFM = aP10_ContagemResultado_PFBFM;
         this.A461ContagemResultado_PFLFM = aP11_ContagemResultado_PFLFM;
         this.A458ContagemResultado_PFBFS = aP12_ContagemResultado_PFBFS;
         this.A459ContagemResultado_PFLFS = aP13_ContagemResultado_PFLFS;
         this.A470ContagemResultado_ContadorFMCod = aP14_ContagemResultado_ContadorFMCod;
         this.A454ContagemResultado_ContadorFSCod = aP15_ContagemResultado_ContadorFSCod;
         this.A512ContagemResultado_ValorPF = aP16_ContagemResultado_ValorPF;
         this.A485ContagemResultado_EhValidacao = aP17_ContagemResultado_EhValidacao;
         this.AV16DataEmNumero = aP18_DataEmNumero;
         this.A1553ContagemResultado_CntSrvCod = aP19_ContagemResultado_CntSrvCod;
         initialize();
         executePrivate();
         aP0_AreaTrabalho_Codigo=this.AV21AreaTrabalho_Codigo;
         aP1_ContagemResultado_Demanda=this.AV18ContagemResultado_Demanda;
         aP2_ContagemResultado_DemandaFM=this.AV11ContagemResultado_DemandaFM;
         aP3_ContagemResultado_ContratadaCod=this.A490ContagemResultado_ContratadaCod;
         aP4_Servico_Codigo=this.AV22Servico_Codigo;
         aP5_ContagemResultado_SistemaCod=this.A489ContagemResultado_SistemaCod;
         aP6_ContagemResultado_DataDmn=this.AV10ContagemResultado_DataDmn;
         aP7_ContagemResultado_Descricao=this.AV12ContagemResultado_Descricao;
         aP8_ContagemResultado_NaoCnfDmnCod=this.AV14ContagemResultado_NaoCnfDmnCod;
         aP9_ContagemResultadoContagens_Esforco=this.A482ContagemResultadoContagens_Esforco;
         aP10_ContagemResultado_PFBFM=this.AV15ContagemResultado_PFBFM;
         aP11_ContagemResultado_PFLFM=this.A461ContagemResultado_PFLFM;
         aP12_ContagemResultado_PFBFS=this.A458ContagemResultado_PFBFS;
         aP13_ContagemResultado_PFLFS=this.A459ContagemResultado_PFLFS;
         aP14_ContagemResultado_ContadorFMCod=this.A470ContagemResultado_ContadorFMCod;
         aP15_ContagemResultado_ContadorFSCod=this.A454ContagemResultado_ContadorFSCod;
         aP16_ContagemResultado_ValorPF=this.A512ContagemResultado_ValorPF;
         aP17_ContagemResultado_EhValidacao=this.A485ContagemResultado_EhValidacao;
         aP18_DataEmNumero=this.AV16DataEmNumero;
         aP19_ContagemResultado_CntSrvCod=this.A1553ContagemResultado_CntSrvCod;
      }

      public int executeUdp( ref int aP0_AreaTrabalho_Codigo ,
                             ref String aP1_ContagemResultado_Demanda ,
                             ref String aP2_ContagemResultado_DemandaFM ,
                             ref int aP3_ContagemResultado_ContratadaCod ,
                             ref int aP4_Servico_Codigo ,
                             ref int aP5_ContagemResultado_SistemaCod ,
                             ref DateTime aP6_ContagemResultado_DataDmn ,
                             ref String aP7_ContagemResultado_Descricao ,
                             ref int aP8_ContagemResultado_NaoCnfDmnCod ,
                             ref short aP9_ContagemResultadoContagens_Esforco ,
                             ref decimal aP10_ContagemResultado_PFBFM ,
                             ref decimal aP11_ContagemResultado_PFLFM ,
                             ref decimal aP12_ContagemResultado_PFBFS ,
                             ref decimal aP13_ContagemResultado_PFLFS ,
                             ref int aP14_ContagemResultado_ContadorFMCod ,
                             ref int aP15_ContagemResultado_ContadorFSCod ,
                             ref decimal aP16_ContagemResultado_ValorPF ,
                             ref bool aP17_ContagemResultado_EhValidacao ,
                             ref int aP18_DataEmNumero )
      {
         this.AV21AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV18ContagemResultado_Demanda = aP1_ContagemResultado_Demanda;
         this.AV11ContagemResultado_DemandaFM = aP2_ContagemResultado_DemandaFM;
         this.A490ContagemResultado_ContratadaCod = aP3_ContagemResultado_ContratadaCod;
         this.AV22Servico_Codigo = aP4_Servico_Codigo;
         this.A489ContagemResultado_SistemaCod = aP5_ContagemResultado_SistemaCod;
         this.AV10ContagemResultado_DataDmn = aP6_ContagemResultado_DataDmn;
         this.AV12ContagemResultado_Descricao = aP7_ContagemResultado_Descricao;
         this.AV14ContagemResultado_NaoCnfDmnCod = aP8_ContagemResultado_NaoCnfDmnCod;
         this.A482ContagemResultadoContagens_Esforco = aP9_ContagemResultadoContagens_Esforco;
         this.AV15ContagemResultado_PFBFM = aP10_ContagemResultado_PFBFM;
         this.A461ContagemResultado_PFLFM = aP11_ContagemResultado_PFLFM;
         this.A458ContagemResultado_PFBFS = aP12_ContagemResultado_PFBFS;
         this.A459ContagemResultado_PFLFS = aP13_ContagemResultado_PFLFS;
         this.A470ContagemResultado_ContadorFMCod = aP14_ContagemResultado_ContadorFMCod;
         this.A454ContagemResultado_ContadorFSCod = aP15_ContagemResultado_ContadorFSCod;
         this.A512ContagemResultado_ValorPF = aP16_ContagemResultado_ValorPF;
         this.A485ContagemResultado_EhValidacao = aP17_ContagemResultado_EhValidacao;
         this.AV16DataEmNumero = aP18_DataEmNumero;
         this.A1553ContagemResultado_CntSrvCod = aP19_ContagemResultado_CntSrvCod;
         initialize();
         executePrivate();
         aP0_AreaTrabalho_Codigo=this.AV21AreaTrabalho_Codigo;
         aP1_ContagemResultado_Demanda=this.AV18ContagemResultado_Demanda;
         aP2_ContagemResultado_DemandaFM=this.AV11ContagemResultado_DemandaFM;
         aP3_ContagemResultado_ContratadaCod=this.A490ContagemResultado_ContratadaCod;
         aP4_Servico_Codigo=this.AV22Servico_Codigo;
         aP5_ContagemResultado_SistemaCod=this.A489ContagemResultado_SistemaCod;
         aP6_ContagemResultado_DataDmn=this.AV10ContagemResultado_DataDmn;
         aP7_ContagemResultado_Descricao=this.AV12ContagemResultado_Descricao;
         aP8_ContagemResultado_NaoCnfDmnCod=this.AV14ContagemResultado_NaoCnfDmnCod;
         aP9_ContagemResultadoContagens_Esforco=this.A482ContagemResultadoContagens_Esforco;
         aP10_ContagemResultado_PFBFM=this.AV15ContagemResultado_PFBFM;
         aP11_ContagemResultado_PFLFM=this.A461ContagemResultado_PFLFM;
         aP12_ContagemResultado_PFBFS=this.A458ContagemResultado_PFBFS;
         aP13_ContagemResultado_PFLFS=this.A459ContagemResultado_PFLFS;
         aP14_ContagemResultado_ContadorFMCod=this.A470ContagemResultado_ContadorFMCod;
         aP15_ContagemResultado_ContadorFSCod=this.A454ContagemResultado_ContadorFSCod;
         aP16_ContagemResultado_ValorPF=this.A512ContagemResultado_ValorPF;
         aP17_ContagemResultado_EhValidacao=this.A485ContagemResultado_EhValidacao;
         aP18_DataEmNumero=this.AV16DataEmNumero;
         aP19_ContagemResultado_CntSrvCod=this.A1553ContagemResultado_CntSrvCod;
         return A1553ContagemResultado_CntSrvCod ;
      }

      public void executeSubmit( ref int aP0_AreaTrabalho_Codigo ,
                                 ref String aP1_ContagemResultado_Demanda ,
                                 ref String aP2_ContagemResultado_DemandaFM ,
                                 ref int aP3_ContagemResultado_ContratadaCod ,
                                 ref int aP4_Servico_Codigo ,
                                 ref int aP5_ContagemResultado_SistemaCod ,
                                 ref DateTime aP6_ContagemResultado_DataDmn ,
                                 ref String aP7_ContagemResultado_Descricao ,
                                 ref int aP8_ContagemResultado_NaoCnfDmnCod ,
                                 ref short aP9_ContagemResultadoContagens_Esforco ,
                                 ref decimal aP10_ContagemResultado_PFBFM ,
                                 ref decimal aP11_ContagemResultado_PFLFM ,
                                 ref decimal aP12_ContagemResultado_PFBFS ,
                                 ref decimal aP13_ContagemResultado_PFLFS ,
                                 ref int aP14_ContagemResultado_ContadorFMCod ,
                                 ref int aP15_ContagemResultado_ContadorFSCod ,
                                 ref decimal aP16_ContagemResultado_ValorPF ,
                                 ref bool aP17_ContagemResultado_EhValidacao ,
                                 ref int aP18_DataEmNumero ,
                                 ref int aP19_ContagemResultado_CntSrvCod )
      {
         prc_newrectrf objprc_newrectrf;
         objprc_newrectrf = new prc_newrectrf();
         objprc_newrectrf.AV21AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         objprc_newrectrf.AV18ContagemResultado_Demanda = aP1_ContagemResultado_Demanda;
         objprc_newrectrf.AV11ContagemResultado_DemandaFM = aP2_ContagemResultado_DemandaFM;
         objprc_newrectrf.A490ContagemResultado_ContratadaCod = aP3_ContagemResultado_ContratadaCod;
         objprc_newrectrf.AV22Servico_Codigo = aP4_Servico_Codigo;
         objprc_newrectrf.A489ContagemResultado_SistemaCod = aP5_ContagemResultado_SistemaCod;
         objprc_newrectrf.AV10ContagemResultado_DataDmn = aP6_ContagemResultado_DataDmn;
         objprc_newrectrf.AV12ContagemResultado_Descricao = aP7_ContagemResultado_Descricao;
         objprc_newrectrf.AV14ContagemResultado_NaoCnfDmnCod = aP8_ContagemResultado_NaoCnfDmnCod;
         objprc_newrectrf.A482ContagemResultadoContagens_Esforco = aP9_ContagemResultadoContagens_Esforco;
         objprc_newrectrf.AV15ContagemResultado_PFBFM = aP10_ContagemResultado_PFBFM;
         objprc_newrectrf.A461ContagemResultado_PFLFM = aP11_ContagemResultado_PFLFM;
         objprc_newrectrf.A458ContagemResultado_PFBFS = aP12_ContagemResultado_PFBFS;
         objprc_newrectrf.A459ContagemResultado_PFLFS = aP13_ContagemResultado_PFLFS;
         objprc_newrectrf.A470ContagemResultado_ContadorFMCod = aP14_ContagemResultado_ContadorFMCod;
         objprc_newrectrf.A454ContagemResultado_ContadorFSCod = aP15_ContagemResultado_ContadorFSCod;
         objprc_newrectrf.A512ContagemResultado_ValorPF = aP16_ContagemResultado_ValorPF;
         objprc_newrectrf.A485ContagemResultado_EhValidacao = aP17_ContagemResultado_EhValidacao;
         objprc_newrectrf.AV16DataEmNumero = aP18_DataEmNumero;
         objprc_newrectrf.A1553ContagemResultado_CntSrvCod = aP19_ContagemResultado_CntSrvCod;
         objprc_newrectrf.context.SetSubmitInitialConfig(context);
         objprc_newrectrf.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_newrectrf);
         aP0_AreaTrabalho_Codigo=this.AV21AreaTrabalho_Codigo;
         aP1_ContagemResultado_Demanda=this.AV18ContagemResultado_Demanda;
         aP2_ContagemResultado_DemandaFM=this.AV11ContagemResultado_DemandaFM;
         aP3_ContagemResultado_ContratadaCod=this.A490ContagemResultado_ContratadaCod;
         aP4_Servico_Codigo=this.AV22Servico_Codigo;
         aP5_ContagemResultado_SistemaCod=this.A489ContagemResultado_SistemaCod;
         aP6_ContagemResultado_DataDmn=this.AV10ContagemResultado_DataDmn;
         aP7_ContagemResultado_Descricao=this.AV12ContagemResultado_Descricao;
         aP8_ContagemResultado_NaoCnfDmnCod=this.AV14ContagemResultado_NaoCnfDmnCod;
         aP9_ContagemResultadoContagens_Esforco=this.A482ContagemResultadoContagens_Esforco;
         aP10_ContagemResultado_PFBFM=this.AV15ContagemResultado_PFBFM;
         aP11_ContagemResultado_PFLFM=this.A461ContagemResultado_PFLFM;
         aP12_ContagemResultado_PFBFS=this.A458ContagemResultado_PFBFS;
         aP13_ContagemResultado_PFLFS=this.A459ContagemResultado_PFLFS;
         aP14_ContagemResultado_ContadorFMCod=this.A470ContagemResultado_ContadorFMCod;
         aP15_ContagemResultado_ContadorFSCod=this.A454ContagemResultado_ContadorFSCod;
         aP16_ContagemResultado_ValorPF=this.A512ContagemResultado_ValorPF;
         aP17_ContagemResultado_EhValidacao=this.A485ContagemResultado_EhValidacao;
         aP18_DataEmNumero=this.AV16DataEmNumero;
         aP19_ContagemResultado_CntSrvCod=this.A1553ContagemResultado_CntSrvCod;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_newrectrf)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         new prc_getparmindicedivergencia(context ).execute( ref  A1553ContagemResultado_CntSrvCod, out  AV17IndiceDivergencia, out  AV8CalculoDivergencia, out  AV20ContagemResultado_ValorPF) ;
         GXt_decimal1 = AV13ContagemResultado_Divergencia;
         new prc_calculardivergencia(context ).execute(  AV8CalculoDivergencia,  A458ContagemResultado_PFBFS,  AV15ContagemResultado_PFBFM,  A459ContagemResultado_PFLFS,  A461ContagemResultado_PFLFM, out  GXt_decimal1) ;
         AV13ContagemResultado_Divergencia = GXt_decimal1;
         if ( AV13ContagemResultado_Divergencia > AV17IndiceDivergencia )
         {
            AV23StatusDmn = "A";
         }
         else
         {
            AV23StatusDmn = "R";
         }
         AV19Flag = true;
         AV26GXLvl12 = 0;
         /* Using cursor P00382 */
         pr_default.execute(0, new Object[] {AV18ContagemResultado_Demanda, n454ContagemResultado_ContadorFSCod, A454ContagemResultado_ContadorFSCod, n485ContagemResultado_EhValidacao, A485ContagemResultado_EhValidacao, n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod, n489ContagemResultado_SistemaCod, A489ContagemResultado_SistemaCod, n512ContagemResultado_ValorPF, A512ContagemResultado_ValorPF, n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A457ContagemResultado_Demanda = P00382_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00382_n457ContagemResultado_Demanda[0];
            A456ContagemResultado_Codigo = P00382_A456ContagemResultado_Codigo[0];
            AV26GXLvl12 = 1;
            AV19Flag = false;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         if ( AV26GXLvl12 == 0 )
         {
            /*
               INSERT RECORD ON TABLE ContagemResultado

            */
            A457ContagemResultado_Demanda = AV18ContagemResultado_Demanda;
            n457ContagemResultado_Demanda = false;
            A471ContagemResultado_DataDmn = AV10ContagemResultado_DataDmn;
            A493ContagemResultado_DemandaFM = AV11ContagemResultado_DemandaFM;
            n493ContagemResultado_DemandaFM = false;
            A146Modulo_Codigo = 0;
            n146Modulo_Codigo = false;
            n146Modulo_Codigo = true;
            A468ContagemResultado_NaoCnfDmnCod = 0;
            n468ContagemResultado_NaoCnfDmnCod = false;
            n468ContagemResultado_NaoCnfDmnCod = true;
            A494ContagemResultado_Descricao = AV12ContagemResultado_Descricao;
            n494ContagemResultado_Descricao = false;
            A484ContagemResultado_StatusDmn = AV23StatusDmn;
            n484ContagemResultado_StatusDmn = false;
            A508ContagemResultado_Owner = A470ContagemResultado_ContadorFMCod;
            if ( (0==AV14ContagemResultado_NaoCnfDmnCod) )
            {
               A468ContagemResultado_NaoCnfDmnCod = 0;
               n468ContagemResultado_NaoCnfDmnCod = false;
               n468ContagemResultado_NaoCnfDmnCod = true;
            }
            else
            {
               A468ContagemResultado_NaoCnfDmnCod = AV14ContagemResultado_NaoCnfDmnCod;
               n468ContagemResultado_NaoCnfDmnCod = false;
            }
            A890ContagemResultado_Responsavel = 0;
            n890ContagemResultado_Responsavel = false;
            n890ContagemResultado_Responsavel = true;
            A1043ContagemResultado_LiqLogCod = 0;
            n1043ContagemResultado_LiqLogCod = false;
            n1043ContagemResultado_LiqLogCod = true;
            A598ContagemResultado_Baseline = false;
            n598ContagemResultado_Baseline = false;
            A1350ContagemResultado_DataCadastro = DateTimeUtil.ServerNow( context, "DEFAULT");
            n1350ContagemResultado_DataCadastro = false;
            A1452ContagemResultado_SS = 0;
            n1452ContagemResultado_SS = false;
            A1515ContagemResultado_Evento = 1;
            n1515ContagemResultado_Evento = false;
            A1583ContagemResultado_TipoRegistro = 1;
            /* Using cursor P00383 */
            pr_default.execute(1, new Object[] {A471ContagemResultado_DataDmn, n454ContagemResultado_ContadorFSCod, A454ContagemResultado_ContadorFSCod, n457ContagemResultado_Demanda, A457ContagemResultado_Demanda, n468ContagemResultado_NaoCnfDmnCod, A468ContagemResultado_NaoCnfDmnCod, n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n485ContagemResultado_EhValidacao, A485ContagemResultado_EhValidacao, n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod, n493ContagemResultado_DemandaFM, A493ContagemResultado_DemandaFM, n494ContagemResultado_Descricao, A494ContagemResultado_Descricao, n489ContagemResultado_SistemaCod, A489ContagemResultado_SistemaCod, n146Modulo_Codigo, A146Modulo_Codigo, A508ContagemResultado_Owner, n512ContagemResultado_ValorPF, A512ContagemResultado_ValorPF, n598ContagemResultado_Baseline, A598ContagemResultado_Baseline, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, n1043ContagemResultado_LiqLogCod, A1043ContagemResultado_LiqLogCod, n1350ContagemResultado_DataCadastro, A1350ContagemResultado_DataCadastro, n1452ContagemResultado_SS, A1452ContagemResultado_SS, n1515ContagemResultado_Evento, A1515ContagemResultado_Evento, n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod, A1583ContagemResultado_TipoRegistro});
            A456ContagemResultado_Codigo = P00383_A456ContagemResultado_Codigo[0];
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            if ( (pr_default.getStatus(1) == 1) )
            {
               context.Gx_err = 1;
               Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
            }
            else
            {
               context.Gx_err = 0;
               Gx_emsg = "";
            }
            /* End Insert */
            context.CommitDataStores( "PRC_NewRecTRF");
            AV9ContagemResultado_Codigo = A456ContagemResultado_Codigo;
         }
         if ( AV19Flag )
         {
            /*
               INSERT RECORD ON TABLE ContagemResultadoContagens

            */
            A456ContagemResultado_Codigo = AV9ContagemResultado_Codigo;
            A473ContagemResultado_DataCnt = DateTimeUtil.DAdd(DateTimeUtil.DAdd(context.localUtil.CToD( "01/01/1900", 2),+(AV16DataEmNumero)),-((int)(2)));
            A511ContagemResultado_HoraCnt = "00:00";
            A460ContagemResultado_PFBFM = AV15ContagemResultado_PFBFM;
            n460ContagemResultado_PFBFM = false;
            A462ContagemResultado_Divergencia = AV13ContagemResultado_Divergencia;
            if ( StringUtil.StrCmp(AV23StatusDmn, "A") == 0 )
            {
               A483ContagemResultado_StatusCnt = 7;
            }
            else
            {
               A483ContagemResultado_StatusCnt = 5;
            }
            A469ContagemResultado_NaoCnfCntCod = 0;
            n469ContagemResultado_NaoCnfCntCod = false;
            n469ContagemResultado_NaoCnfCntCod = true;
            A517ContagemResultado_Ultima = true;
            /* Using cursor P00384 */
            pr_default.execute(2, new Object[] {A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt, n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, n460ContagemResultado_PFBFM, A460ContagemResultado_PFBFM, n461ContagemResultado_PFLFM, A461ContagemResultado_PFLFM, A462ContagemResultado_Divergencia, A470ContagemResultado_ContadorFMCod, n469ContagemResultado_NaoCnfCntCod, A469ContagemResultado_NaoCnfCntCod, A483ContagemResultado_StatusCnt, A482ContagemResultadoContagens_Esforco, A517ContagemResultado_Ultima});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
            if ( (pr_default.getStatus(2) == 1) )
            {
               context.Gx_err = 1;
               Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
            }
            else
            {
               context.Gx_err = 0;
               Gx_emsg = "";
            }
            /* End Insert */
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_NewRecTRF");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV8CalculoDivergencia = "";
         AV23StatusDmn = "";
         scmdbuf = "";
         P00382_A454ContagemResultado_ContadorFSCod = new int[1] ;
         P00382_n454ContagemResultado_ContadorFSCod = new bool[] {false} ;
         P00382_A485ContagemResultado_EhValidacao = new bool[] {false} ;
         P00382_n485ContagemResultado_EhValidacao = new bool[] {false} ;
         P00382_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00382_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00382_A489ContagemResultado_SistemaCod = new int[1] ;
         P00382_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00382_A512ContagemResultado_ValorPF = new decimal[1] ;
         P00382_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P00382_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00382_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00382_A457ContagemResultado_Demanda = new String[] {""} ;
         P00382_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00382_A456ContagemResultado_Codigo = new int[1] ;
         A457ContagemResultado_Demanda = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A493ContagemResultado_DemandaFM = "";
         A494ContagemResultado_Descricao = "";
         A484ContagemResultado_StatusDmn = "";
         A1350ContagemResultado_DataCadastro = (DateTime)(DateTime.MinValue);
         P00383_A456ContagemResultado_Codigo = new int[1] ;
         Gx_emsg = "";
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A511ContagemResultado_HoraCnt = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_newrectrf__default(),
            new Object[][] {
                new Object[] {
               P00382_A454ContagemResultado_ContadorFSCod, P00382_n454ContagemResultado_ContadorFSCod, P00382_A485ContagemResultado_EhValidacao, P00382_n485ContagemResultado_EhValidacao, P00382_A490ContagemResultado_ContratadaCod, P00382_n490ContagemResultado_ContratadaCod, P00382_A489ContagemResultado_SistemaCod, P00382_n489ContagemResultado_SistemaCod, P00382_A512ContagemResultado_ValorPF, P00382_n512ContagemResultado_ValorPF,
               P00382_A1553ContagemResultado_CntSrvCod, P00382_n1553ContagemResultado_CntSrvCod, P00382_A457ContagemResultado_Demanda, P00382_n457ContagemResultado_Demanda, P00382_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P00383_A456ContagemResultado_Codigo
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A482ContagemResultadoContagens_Esforco ;
      private short AV26GXLvl12 ;
      private short A1515ContagemResultado_Evento ;
      private short A1583ContagemResultado_TipoRegistro ;
      private short A483ContagemResultado_StatusCnt ;
      private int AV21AreaTrabalho_Codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int AV22Servico_Codigo ;
      private int A489ContagemResultado_SistemaCod ;
      private int AV14ContagemResultado_NaoCnfDmnCod ;
      private int A470ContagemResultado_ContadorFMCod ;
      private int A454ContagemResultado_ContadorFSCod ;
      private int AV16DataEmNumero ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A456ContagemResultado_Codigo ;
      private int GX_INS69 ;
      private int A146Modulo_Codigo ;
      private int A468ContagemResultado_NaoCnfDmnCod ;
      private int A508ContagemResultado_Owner ;
      private int A890ContagemResultado_Responsavel ;
      private int A1043ContagemResultado_LiqLogCod ;
      private int A1452ContagemResultado_SS ;
      private int AV9ContagemResultado_Codigo ;
      private int GX_INS72 ;
      private int A469ContagemResultado_NaoCnfCntCod ;
      private decimal AV15ContagemResultado_PFBFM ;
      private decimal A461ContagemResultado_PFLFM ;
      private decimal A458ContagemResultado_PFBFS ;
      private decimal A459ContagemResultado_PFLFS ;
      private decimal A512ContagemResultado_ValorPF ;
      private decimal AV17IndiceDivergencia ;
      private decimal AV20ContagemResultado_ValorPF ;
      private decimal AV13ContagemResultado_Divergencia ;
      private decimal GXt_decimal1 ;
      private decimal A460ContagemResultado_PFBFM ;
      private decimal A462ContagemResultado_Divergencia ;
      private String AV8CalculoDivergencia ;
      private String AV23StatusDmn ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private String Gx_emsg ;
      private String A511ContagemResultado_HoraCnt ;
      private DateTime A1350ContagemResultado_DataCadastro ;
      private DateTime AV10ContagemResultado_DataDmn ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime A473ContagemResultado_DataCnt ;
      private bool A485ContagemResultado_EhValidacao ;
      private bool AV19Flag ;
      private bool n454ContagemResultado_ContadorFSCod ;
      private bool n485ContagemResultado_EhValidacao ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n512ContagemResultado_ValorPF ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n457ContagemResultado_Demanda ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n146Modulo_Codigo ;
      private bool n468ContagemResultado_NaoCnfDmnCod ;
      private bool n494ContagemResultado_Descricao ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n1043ContagemResultado_LiqLogCod ;
      private bool A598ContagemResultado_Baseline ;
      private bool n598ContagemResultado_Baseline ;
      private bool n1350ContagemResultado_DataCadastro ;
      private bool n1452ContagemResultado_SS ;
      private bool n1515ContagemResultado_Evento ;
      private bool n460ContagemResultado_PFBFM ;
      private bool n469ContagemResultado_NaoCnfCntCod ;
      private bool A517ContagemResultado_Ultima ;
      private bool n458ContagemResultado_PFBFS ;
      private bool n459ContagemResultado_PFLFS ;
      private bool n461ContagemResultado_PFLFM ;
      private String AV18ContagemResultado_Demanda ;
      private String AV11ContagemResultado_DemandaFM ;
      private String AV12ContagemResultado_Descricao ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String A494ContagemResultado_Descricao ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_AreaTrabalho_Codigo ;
      private String aP1_ContagemResultado_Demanda ;
      private String aP2_ContagemResultado_DemandaFM ;
      private int aP3_ContagemResultado_ContratadaCod ;
      private int aP4_Servico_Codigo ;
      private int aP5_ContagemResultado_SistemaCod ;
      private DateTime aP6_ContagemResultado_DataDmn ;
      private String aP7_ContagemResultado_Descricao ;
      private int aP8_ContagemResultado_NaoCnfDmnCod ;
      private short aP9_ContagemResultadoContagens_Esforco ;
      private decimal aP10_ContagemResultado_PFBFM ;
      private decimal aP11_ContagemResultado_PFLFM ;
      private decimal aP12_ContagemResultado_PFBFS ;
      private decimal aP13_ContagemResultado_PFLFS ;
      private int aP14_ContagemResultado_ContadorFMCod ;
      private int aP15_ContagemResultado_ContadorFSCod ;
      private decimal aP16_ContagemResultado_ValorPF ;
      private bool aP17_ContagemResultado_EhValidacao ;
      private int aP18_DataEmNumero ;
      private int aP19_ContagemResultado_CntSrvCod ;
      private IDataStoreProvider pr_default ;
      private int[] P00382_A454ContagemResultado_ContadorFSCod ;
      private bool[] P00382_n454ContagemResultado_ContadorFSCod ;
      private bool[] P00382_A485ContagemResultado_EhValidacao ;
      private bool[] P00382_n485ContagemResultado_EhValidacao ;
      private int[] P00382_A490ContagemResultado_ContratadaCod ;
      private bool[] P00382_n490ContagemResultado_ContratadaCod ;
      private int[] P00382_A489ContagemResultado_SistemaCod ;
      private bool[] P00382_n489ContagemResultado_SistemaCod ;
      private decimal[] P00382_A512ContagemResultado_ValorPF ;
      private bool[] P00382_n512ContagemResultado_ValorPF ;
      private int[] P00382_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00382_n1553ContagemResultado_CntSrvCod ;
      private String[] P00382_A457ContagemResultado_Demanda ;
      private bool[] P00382_n457ContagemResultado_Demanda ;
      private int[] P00382_A456ContagemResultado_Codigo ;
      private int[] P00383_A456ContagemResultado_Codigo ;
   }

   public class prc_newrectrf__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new UpdateCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00382 ;
          prmP00382 = new Object[] {
          new Object[] {"@AV18ContagemResultado_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@ContagemResultado_ContadorFSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_EhValidacao",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_ValorPF",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00383 ;
          prmP00383 = new Object[] {
          new Object[] {"@ContagemResultado_DataDmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_ContadorFSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@ContagemResultado_NaoCnfDmnCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_EhValidacao",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DemandaFM",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ContagemResultado_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Owner",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_ValorPF",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_Baseline",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_LiqLogCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCadastro",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_SS",SqlDbType.Int,8,0} ,
          new Object[] {"@ContagemResultado_Evento",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_TipoRegistro",SqlDbType.SmallInt,4,0}
          } ;
          String cmdBufferP00383 ;
          cmdBufferP00383=" INSERT INTO [ContagemResultado]([ContagemResultado_DataDmn], [ContagemResultado_ContadorFSCod], [ContagemResultado_Demanda], [ContagemResultado_NaoCnfDmnCod], [ContagemResultado_StatusDmn], [ContagemResultado_EhValidacao], [ContagemResultado_ContratadaCod], [ContagemResultado_DemandaFM], [ContagemResultado_Descricao], [ContagemResultado_SistemaCod], [Modulo_Codigo], [ContagemResultado_Owner], [ContagemResultado_ValorPF], [ContagemResultado_Baseline], [ContagemResultado_Responsavel], [ContagemResultado_LiqLogCod], [ContagemResultado_DataCadastro], [ContagemResultado_SS], [ContagemResultado_Evento], [ContagemResultado_CntSrvCod], [ContagemResultado_TipoRegistro], [ContagemResultado_DataEntrega], [ContagemResultado_Link], [ContagemResultado_Observacao], [ContagemResultado_Evidencia], [ContagemResultado_LoteAceiteCod], [ContagemResultado_OSVinculada], [ContagemResultado_PFBFSImp], [ContagemResultado_PFLFSImp], [ContagemResultado_ContratadaOrigemCod], [ContagemResultado_HoraEntrega], [ContagemResultado_FncUsrCod], [ContagemResultado_Agrupador], [ContagemResultado_GlsData], [ContagemResultado_GlsDescricao], [ContagemResultado_GlsValor], [ContagemResultado_GlsUser], [ContagemResultado_OSManual], [ContagemResultado_PBFinal], [ContagemResultado_PLFinal], [ContagemResultado_Custo], [ContagemResultado_PrazoInicialDias], [ContagemResultado_PrazoMaisDias], [ContagemResultado_DataHomologacao], [ContagemResultado_DataExecucao], [ContagemResultado_DataPrevista], [ContagemResultado_RdmnIssueId], [ContagemResultado_RdmnProjectId], [ContagemResultado_RdmnUpdated], [ContagemResultado_CntSrvPrrCod], [ContagemResultado_CntSrvPrrPrz], [ContagemResultado_CntSrvPrrCst], [ContagemResultado_TemDpnHmlg], [ContagemResultado_TmpEstExc], [ContagemResultado_TmpEstCrr], [ContagemResultado_InicioExc], [ContagemResultado_FimExc], "
          + " [ContagemResultado_InicioCrr], [ContagemResultado_FimCrr], [ContagemResultado_TmpEstAnl], [ContagemResultado_InicioAnl], [ContagemResultado_FimAnl], [ContagemResultado_ProjetoCod], [ContagemResultado_VlrAceite], [ContagemResultado_UOOwner], [ContagemResultado_Referencia], [ContagemResultado_Restricoes], [ContagemResultado_PrioridadePrevista], [ContagemResultado_ServicoSS], [ContagemResultado_Combinada], [ContagemResultado_Entrega], [ContagemResultado_DataInicio], [ContagemResultado_SemCusto], [ContagemResultado_VlrCnc], [ContagemResultado_PFCnc], [ContagemResultado_DataPrvPgm], [ContagemResultado_DataEntregaReal], [ContagemResultado_QuantidadeSolicitada]) VALUES(@ContagemResultado_DataDmn, @ContagemResultado_ContadorFSCod, @ContagemResultado_Demanda, @ContagemResultado_NaoCnfDmnCod, @ContagemResultado_StatusDmn, @ContagemResultado_EhValidacao, @ContagemResultado_ContratadaCod, @ContagemResultado_DemandaFM, @ContagemResultado_Descricao, @ContagemResultado_SistemaCod, @Modulo_Codigo, @ContagemResultado_Owner, @ContagemResultado_ValorPF, @ContagemResultado_Baseline, @ContagemResultado_Responsavel, @ContagemResultado_LiqLogCod, @ContagemResultado_DataCadastro, @ContagemResultado_SS, @ContagemResultado_Evento, @ContagemResultado_CntSrvCod, @ContagemResultado_TipoRegistro, convert( DATETIME, '17530101', 112 ), '', '', '', convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert(int, 0), '', convert( DATETIME, '17530101', 112 ), '', convert(int, 0), convert(int, 0), convert(bit, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0), convert(int,"
          + " 0), convert( DATETIME, '17530101', 112 ), convert(int, 0), convert(int, 0), convert(int, 0), convert(bit, 0), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0), convert(int, 0), convert(int, 0), '', '', '', convert(int, 0), convert(bit, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert(bit, 0), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0)); SELECT SCOPE_IDENTITY()" ;
          Object[] prmP00384 ;
          prmP00384 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0} ,
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFBFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_NaoCnfCntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultadoContagens_Esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00382", "SELECT TOP 1 [ContagemResultado_ContadorFSCod], [ContagemResultado_EhValidacao], [ContagemResultado_ContratadaCod], [ContagemResultado_SistemaCod], [ContagemResultado_ValorPF], [ContagemResultado_CntSrvCod], [ContagemResultado_Demanda], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE ([ContagemResultado_Demanda] = @AV18ContagemResultado_Demanda) AND ([ContagemResultado_ContadorFSCod] = @ContagemResultado_ContadorFSCod) AND ([ContagemResultado_EhValidacao] = @ContagemResultado_EhValidacao) AND ([ContagemResultado_ContratadaCod] = @ContagemResultado_ContratadaCod) AND ([ContagemResultado_SistemaCod] = @ContagemResultado_SistemaCod) AND ([ContagemResultado_ValorPF] = @ContagemResultado_ValorPF) AND ([ContagemResultado_CntSrvCod] = @ContagemResultado_CntSrvCod) ORDER BY [ContagemResultado_Demanda] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00382,1,0,false,true )
             ,new CursorDef("P00383", cmdBufferP00383, GxErrorMask.GX_NOMASK,prmP00383)
             ,new CursorDef("P00384", "INSERT INTO [ContagemResultadoContagens]([ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt], [ContagemResultado_PFBFS], [ContagemResultado_PFLFS], [ContagemResultado_PFBFM], [ContagemResultado_PFLFM], [ContagemResultado_Divergencia], [ContagemResultado_ContadorFMCod], [ContagemResultado_NaoCnfCntCod], [ContagemResultado_StatusCnt], [ContagemResultadoContagens_Esforco], [ContagemResultado_Ultima], [ContagemResultado_TimeCnt], [ContagemResultado_ParecerTcn], [ContagemResultado_Deflator], [ContagemResultado_CstUntPrd], [ContagemResultado_Planilha], [ContagemResultado_NomePla], [ContagemResultado_TipoPla], [ContagemResultadoContagens_Prazo], [ContagemResultado_NvlCnt]) VALUES(@ContagemResultado_Codigo, @ContagemResultado_DataCnt, @ContagemResultado_HoraCnt, @ContagemResultado_PFBFS, @ContagemResultado_PFLFS, @ContagemResultado_PFBFM, @ContagemResultado_PFLFM, @ContagemResultado_Divergencia, @ContagemResultado_ContadorFMCod, @ContagemResultado_NaoCnfCntCod, @ContagemResultado_StatusCnt, @ContagemResultadoContagens_Esforco, @ContagemResultado_Ultima, convert( DATETIME, '17530101', 112 ), '', convert(int, 0), convert(int, 0), CONVERT(varbinary(1), ''), '', '', convert( DATETIME, '17530101', 112 ), convert(int, 0))", GxErrorMask.GX_NOMASK,prmP00384)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(3, (bool)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(6, (decimal)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[12]);
                }
                return;
             case 1 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(6, (bool)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 8 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 9 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 10 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(10, (int)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 11 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(11, (int)parms[20]);
                }
                stmt.SetParameter(12, (int)parms[21]);
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 13 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(13, (decimal)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 14 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(14, (bool)parms[25]);
                }
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 15 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(15, (int)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 16 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(16, (int)parms[29]);
                }
                if ( (bool)parms[30] )
                {
                   stmt.setNull( 17 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(17, (DateTime)parms[31]);
                }
                if ( (bool)parms[32] )
                {
                   stmt.setNull( 18 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(18, (int)parms[33]);
                }
                if ( (bool)parms[34] )
                {
                   stmt.setNull( 19 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(19, (short)parms[35]);
                }
                if ( (bool)parms[36] )
                {
                   stmt.setNull( 20 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(20, (int)parms[37]);
                }
                stmt.SetParameter(21, (short)parms[38]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(6, (decimal)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(7, (decimal)parms[10]);
                }
                stmt.SetParameter(8, (decimal)parms[11]);
                stmt.SetParameter(9, (int)parms[12]);
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 10 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(10, (int)parms[14]);
                }
                stmt.SetParameter(11, (short)parms[15]);
                stmt.SetParameter(12, (short)parms[16]);
                stmt.SetParameter(13, (bool)parms[17]);
                return;
       }
    }

 }

}
