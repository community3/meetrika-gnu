/*
               File: WP_ProjetoMelhoriaItens
        Description: Projeto Melhoria Itens
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:45:21.64
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_projetomelhoriaitens : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_projetomelhoriaitens( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_projetomelhoriaitens( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Projeto_Codigo ,
                           int aP1_AreaTrabalho_Codigo ,
                           String aP2_ParmProjeto_Sigla ,
                           String aP3_Sistema_Tipo )
      {
         this.AV20Projeto_Codigo = aP0_Projeto_Codigo;
         this.AV9AreaTrabalho_Codigo = aP1_AreaTrabalho_Codigo;
         this.AV28ParmProjeto_Sigla = aP2_ParmProjeto_Sigla;
         this.AV25Sistema_Tipo = aP3_Sistema_Tipo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavSistema_codigo = new GXCombobox();
         cmbavSistema_tecnica = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vSISTEMA_CODIGO") == 0 )
            {
               AV9AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9AreaTrabalho_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vAREATRABALHO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9AreaTrabalho_Codigo), "ZZZZZ9")));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvSISTEMA_CODIGODT2( AV9AreaTrabalho_Codigo) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Griditnstm") == 0 )
            {
               nRC_GXsfl_44 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_44_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_44_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGriditnstm_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Griditnstm") == 0 )
            {
               subGriditnstm_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGriditnstm_refresh( subGriditnstm_Rows) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Griditnprj") == 0 )
            {
               nRC_GXsfl_55 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_55_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_55_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGriditnprj_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Griditnprj") == 0 )
            {
               subGriditnprj_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGriditnprj_refresh( subGriditnprj_Rows) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV20Projeto_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Projeto_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPROJETO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV20Projeto_Codigo), "ZZZZZ9")));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV9AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9AreaTrabalho_Codigo), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vAREATRABALHO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9AreaTrabalho_Codigo), "ZZZZZ9")));
                  AV28ParmProjeto_Sigla = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ParmProjeto_Sigla", AV28ParmProjeto_Sigla);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPARMPROJETO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV28ParmProjeto_Sigla, "@!"))));
                  AV25Sistema_Tipo = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Sistema_Tipo", AV25Sistema_Tipo);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PADT2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTDT2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117452172");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_projetomelhoriaitens.aspx") + "?" + UrlEncode("" +AV20Projeto_Codigo) + "," + UrlEncode("" +AV9AreaTrabalho_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV28ParmProjeto_Sigla)) + "," + UrlEncode(StringUtil.RTrim(AV25Sistema_Tipo))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "Sdt_itnprj", AV14Sdt_ItnPrj);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("Sdt_itnprj", AV14Sdt_ItnPrj);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "Sdt_itnstm", AV15Sdt_ItnStm);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("Sdt_itnstm", AV15Sdt_ItnStm);
         }
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_44", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_44), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_55", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_55), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A370FuncaoDados_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_ATIVO", StringUtil.RTrim( A394FuncaoDados_Ativo));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_ITEM", AV13Sdt_Item);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_ITEM", AV13Sdt_Item);
         }
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_NOME", A369FuncaoDados_Nome);
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_TIPO", StringUtil.RTrim( A373FuncaoDados_Tipo));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_ATIVO", StringUtil.RTrim( A183FuncaoAPF_Ativo));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_NOME", A166FuncaoAPF_Nome);
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_TIPO", StringUtil.RTrim( A184FuncaoAPF_Tipo));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_ITNSTM", AV15Sdt_ItnStm);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_ITNSTM", AV15Sdt_ItnStm);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_ITNPRJ", AV14Sdt_ItnPrj);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_ITNPRJ", AV14Sdt_ItnPrj);
         }
         GxWebStd.gx_hidden_field( context, "SISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SISTEMA_NOME", A416Sistema_Nome);
         GxWebStd.gx_hidden_field( context, "vPROJETO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20Projeto_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPARMPROJETO_SIGLA", StringUtil.RTrim( AV28ParmProjeto_Sigla));
         GxWebStd.gx_hidden_field( context, "vSISTEMA_TIPO", StringUtil.RTrim( AV25Sistema_Tipo));
         GxWebStd.gx_hidden_field( context, "GRIDITNSTM_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDITNSTM_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRIDITNPRJ_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDITNPRJ_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRIDITNSTM_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDITNSTM_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRIDITNPRJ_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDITNPRJ_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vAREATRABALHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9AreaTrabalho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_PF", StringUtil.LTrim( StringUtil.NToC( A386FuncaoAPF_PF, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A368FuncaoDados_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "FUNCAODADOS_CONTAR", A755FuncaoDados_Contar);
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_PF", StringUtil.LTrim( StringUtil.NToC( A377FuncaoDados_PF, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPROJETO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV5Projeto_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vPROJETO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV5Projeto_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vPROJETO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV5Projeto_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vPROJETO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV20Projeto_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vAREATRABALHO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9AreaTrabalho_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPARMPROJETO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV28ParmProjeto_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vPROJETO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV20Projeto_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vAREATRABALHO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9AreaTrabalho_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPARMPROJETO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV28ParmProjeto_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, "GRIDITNSTM_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGriditnstm_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDITNPRJ_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGriditnprj_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TBLSTM_Width", StringUtil.RTrim( Dvpanel_tblstm_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TBLSTM_Cls", StringUtil.RTrim( Dvpanel_tblstm_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TBLSTM_Title", StringUtil.RTrim( Dvpanel_tblstm_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TBLSTM_Collapsible", StringUtil.BoolToStr( Dvpanel_tblstm_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TBLSTM_Collapsed", StringUtil.BoolToStr( Dvpanel_tblstm_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TBLSTM_Autowidth", StringUtil.BoolToStr( Dvpanel_tblstm_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TBLSTM_Autoheight", StringUtil.BoolToStr( Dvpanel_tblstm_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TBLSTM_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tblstm_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TBLSTM_Iconposition", StringUtil.RTrim( Dvpanel_tblstm_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TBLSTM_Autoscroll", StringUtil.BoolToStr( Dvpanel_tblstm_Autoscroll));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TBLPRJ_Width", StringUtil.RTrim( Dvpanel_tblprj_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TBLPRJ_Cls", StringUtil.RTrim( Dvpanel_tblprj_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TBLPRJ_Title", StringUtil.RTrim( Dvpanel_tblprj_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TBLPRJ_Collapsible", StringUtil.BoolToStr( Dvpanel_tblprj_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TBLPRJ_Collapsed", StringUtil.BoolToStr( Dvpanel_tblprj_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TBLPRJ_Autowidth", StringUtil.BoolToStr( Dvpanel_tblprj_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TBLPRJ_Autoheight", StringUtil.BoolToStr( Dvpanel_tblprj_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TBLPRJ_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tblprj_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TBLPRJ_Iconposition", StringUtil.RTrim( Dvpanel_tblprj_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TBLPRJ_Autoscroll", StringUtil.BoolToStr( Dvpanel_tblprj_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEDT2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTDT2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_projetomelhoriaitens.aspx") + "?" + UrlEncode("" +AV20Projeto_Codigo) + "," + UrlEncode("" +AV9AreaTrabalho_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV28ParmProjeto_Sigla)) + "," + UrlEncode(StringUtil.RTrim(AV25Sistema_Tipo)) ;
      }

      public override String GetPgmname( )
      {
         return "WP_ProjetoMelhoriaItens" ;
      }

      public override String GetPgmdesc( )
      {
         return "Projeto Melhoria Itens" ;
      }

      protected void WBDT0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_DT2( true) ;
         }
         else
         {
            wb_table1_2_DT2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_DT2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTDT2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Projeto Melhoria Itens", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPDT0( ) ;
      }

      protected void WSDT2( )
      {
         STARTDT2( ) ;
         EVTDT2( ) ;
      }

      protected void EVTDT2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VSISTEMA_CODIGO.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11DT2 */
                              E11DT2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'CONTINUAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12DT2 */
                              E12DT2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDITNSTMPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              sEvt = cgiGet( "GRIDITNSTMPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgriditnstm_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgriditnstm_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgriditnstm_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgriditnstm_lastpage( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDITNPRJPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              sEvt = cgiGet( "GRIDITNPRJPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgriditnprj_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgriditnprj_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgriditnprj_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgriditnprj_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 17), "PRJITEMNOME.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 15), "GRIDITNPRJ.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 17), "PRJITEMNOME.CLICK") == 0 ) )
                           {
                              nGXsfl_55_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_55_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_55_idx), 4, 0)), 4, "0");
                              SubsflControlProps_556( ) ;
                              AV36GXV2 = (short)(nGXsfl_55_idx+GRIDITNPRJ_nFirstRecordOnPage);
                              if ( ( AV14Sdt_ItnPrj.Count >= AV36GXV2 ) && ( AV36GXV2 > 0 ) )
                              {
                                 AV14Sdt_ItnPrj.CurrentItem = ((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV36GXV2));
                              }
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "PRJITEMNOME.CLICK") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E13DT2 */
                                    E13DT2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRIDITNPRJ.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E14DT6 */
                                    E14DT6 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                           else if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 17), "CTLITEMNOME.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 15), "GRIDITNSTM.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 17), "CTLITEMNOME.CLICK") == 0 ) )
                           {
                              nGXsfl_44_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_44_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_44_idx), 4, 0)), 4, "0");
                              SubsflControlProps_442( ) ;
                              AV31GXV1 = (short)(nGXsfl_44_idx+GRIDITNSTM_nFirstRecordOnPage);
                              if ( ( AV15Sdt_ItnStm.Count >= AV31GXV1 ) && ( AV31GXV1 > 0 ) )
                              {
                                 AV15Sdt_ItnStm.CurrentItem = ((SdtSDT_ItensConagem_Item)AV15Sdt_ItnStm.Item(AV31GXV1));
                              }
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E15DT2 */
                                    E15DT2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CTLITEMNOME.CLICK") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E16DT2 */
                                    E16DT2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRIDITNSTM.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E17DT2 */
                                    E17DT2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEDT2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PADT2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavSistema_codigo.Name = "vSISTEMA_CODIGO";
            dynavSistema_codigo.WebTags = "";
            cmbavSistema_tecnica.Name = "vSISTEMA_TECNICA";
            cmbavSistema_tecnica.WebTags = "";
            cmbavSistema_tecnica.addItem("", "(Nenhum)", 0);
            cmbavSistema_tecnica.addItem("I", "Indicativa", 0);
            cmbavSistema_tecnica.addItem("E", "Estimada", 0);
            cmbavSistema_tecnica.addItem("D", "Detalhada", 0);
            if ( cmbavSistema_tecnica.ItemCount > 0 )
            {
               AV26Sistema_Tecnica = cmbavSistema_tecnica.getValidValue(AV26Sistema_Tecnica);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Sistema_Tecnica", AV26Sistema_Tecnica);
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavProjeto_sigla_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvSISTEMA_CODIGODT2( int AV9AreaTrabalho_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvSISTEMA_CODIGO_dataDT2( AV9AreaTrabalho_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvSISTEMA_CODIGO_htmlDT2( int AV9AreaTrabalho_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLVvSISTEMA_CODIGO_dataDT2( AV9AreaTrabalho_Codigo) ;
         gxdynajaxindex = 1;
         dynavSistema_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavSistema_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavSistema_codigo.ItemCount > 0 )
         {
            AV6Sistema_Codigo = (int)(NumberUtil.Val( dynavSistema_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV6Sistema_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Sistema_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvSISTEMA_CODIGO_dataDT2( int AV9AreaTrabalho_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhum");
         /* Using cursor H00DT2 */
         pr_default.execute(0, new Object[] {AV9AreaTrabalho_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00DT2_A127Sistema_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00DT2_A416Sistema_Nome[0]);
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void gxnrGriditnstm_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_442( ) ;
         while ( nGXsfl_44_idx <= nRC_GXsfl_44 )
         {
            sendrow_442( ) ;
            nGXsfl_44_idx = (short)(((subGriditnstm_Islastpage==1)&&(nGXsfl_44_idx+1>subGriditnstm_Recordsperpage( )) ? 1 : nGXsfl_44_idx+1));
            sGXsfl_44_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_44_idx), 4, 0)), 4, "0");
            SubsflControlProps_442( ) ;
         }
         context.GX_webresponse.AddString(GriditnstmContainer.ToJavascriptSource());
         /* End function gxnrGriditnstm_newrow */
      }

      protected void gxnrGriditnprj_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_556( ) ;
         while ( nGXsfl_55_idx <= nRC_GXsfl_55 )
         {
            sendrow_556( ) ;
            nGXsfl_55_idx = (short)(((subGriditnprj_Islastpage==1)&&(nGXsfl_55_idx+1>subGriditnprj_Recordsperpage( )) ? 1 : nGXsfl_55_idx+1));
            sGXsfl_55_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_55_idx), 4, 0)), 4, "0");
            SubsflControlProps_556( ) ;
         }
         context.GX_webresponse.AddString(GriditnprjContainer.ToJavascriptSource());
         /* End function gxnrGriditnprj_newrow */
      }

      protected void gxgrGriditnstm_refresh( int subGriditnstm_Rows )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRIDITNSTM_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGriditnstm_Rows), 6, 0, ".", "")));
         GRIDITNSTM_nCurrentRecord = 0;
         RFDT2( ) ;
         /* End function gxgrGriditnstm_refresh */
      }

      protected void gxgrGriditnprj_refresh( int subGriditnprj_Rows )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRIDITNPRJ_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGriditnprj_Rows), 6, 0, ".", "")));
         GRIDITNPRJ_nCurrentRecord = 0;
         RFDT6( ) ;
         /* End function gxgrGriditnprj_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavSistema_codigo.ItemCount > 0 )
         {
            AV6Sistema_Codigo = (int)(NumberUtil.Val( dynavSistema_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV6Sistema_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Sistema_Codigo), 6, 0)));
         }
         if ( cmbavSistema_tecnica.ItemCount > 0 )
         {
            AV26Sistema_Tecnica = cmbavSistema_tecnica.getValidValue(AV26Sistema_Tecnica);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Sistema_Tecnica", AV26Sistema_Tecnica);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFDT2( ) ;
         RFDT6( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavProjeto_sigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProjeto_sigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_sigla_Enabled), 5, 0)));
         edtavCtlitemcodigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlitemcodigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlitemcodigo_Enabled), 5, 0)));
         edtavCtlitemtipo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlitemtipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlitemtipo_Enabled), 5, 0)));
         edtavCtlitemnome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlitemnome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlitemnome_Enabled), 5, 0)));
         edtavCtlitemtipofn_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlitemtipofn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlitemtipofn_Enabled), 5, 0)));
         edtavPrjitemcodigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPrjitemcodigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPrjitemcodigo_Enabled), 5, 0)));
         edtavPrjitemtipo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPrjitemtipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPrjitemtipo_Enabled), 5, 0)));
         edtavPrjitemnome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPrjitemnome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPrjitemnome_Enabled), 5, 0)));
         edtavPrjitemtipofn_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPrjitemtipofn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPrjitemtipofn_Enabled), 5, 0)));
      }

      protected void RFDT2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GriditnstmContainer.ClearRows();
         }
         wbStart = 44;
         nGXsfl_44_idx = 1;
         sGXsfl_44_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_44_idx), 4, 0)), 4, "0");
         SubsflControlProps_442( ) ;
         nGXsfl_44_Refreshing = 1;
         GriditnstmContainer.AddObjectProperty("GridName", "Griditnstm");
         GriditnstmContainer.AddObjectProperty("CmpContext", "");
         GriditnstmContainer.AddObjectProperty("InMasterPage", "false");
         GriditnstmContainer.AddObjectProperty("Class", "WorkWith");
         GriditnstmContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GriditnstmContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GriditnstmContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGriditnstm_Backcolorstyle), 1, 0, ".", "")));
         GriditnstmContainer.PageSize = subGriditnstm_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_442( ) ;
            /* Execute user event: E17DT2 */
            E17DT2 ();
            if ( ( GRIDITNSTM_nCurrentRecord > 0 ) && ( GRIDITNSTM_nGridOutOfScope == 0 ) && ( nGXsfl_44_idx == 1 ) )
            {
               GRIDITNSTM_nCurrentRecord = 0;
               GRIDITNSTM_nGridOutOfScope = 1;
               subgriditnstm_firstpage( ) ;
               /* Execute user event: E17DT2 */
               E17DT2 ();
            }
            wbEnd = 44;
            WBDT0( ) ;
         }
         nGXsfl_44_Refreshing = 0;
      }

      protected void RFDT6( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GriditnprjContainer.ClearRows();
         }
         wbStart = 55;
         nGXsfl_55_idx = 1;
         sGXsfl_55_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_55_idx), 4, 0)), 4, "0");
         SubsflControlProps_556( ) ;
         nGXsfl_55_Refreshing = 1;
         GriditnprjContainer.AddObjectProperty("GridName", "Griditnprj");
         GriditnprjContainer.AddObjectProperty("CmpContext", "");
         GriditnprjContainer.AddObjectProperty("InMasterPage", "false");
         GriditnprjContainer.AddObjectProperty("Class", "WorkWith");
         GriditnprjContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GriditnprjContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GriditnprjContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGriditnprj_Backcolorstyle), 1, 0, ".", "")));
         GriditnprjContainer.PageSize = subGriditnprj_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_556( ) ;
            /* Execute user event: E14DT6 */
            E14DT6 ();
            if ( ( GRIDITNPRJ_nCurrentRecord > 0 ) && ( GRIDITNPRJ_nGridOutOfScope == 0 ) && ( nGXsfl_55_idx == 1 ) )
            {
               GRIDITNPRJ_nCurrentRecord = 0;
               GRIDITNPRJ_nGridOutOfScope = 1;
               subgriditnprj_firstpage( ) ;
               /* Execute user event: E14DT6 */
               E14DT6 ();
            }
            wbEnd = 55;
            WBDT0( ) ;
         }
         nGXsfl_55_Refreshing = 0;
      }

      protected int subGriditnstm_Pagecount( )
      {
         GRIDITNSTM_nRecordCount = subGriditnstm_Recordcount( );
         if ( ((int)((GRIDITNSTM_nRecordCount) % (subGriditnstm_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRIDITNSTM_nRecordCount/ (decimal)(subGriditnstm_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRIDITNSTM_nRecordCount/ (decimal)(subGriditnstm_Recordsperpage( ))))+1) ;
      }

      protected int subGriditnstm_Recordcount( )
      {
         return AV15Sdt_ItnStm.Count ;
      }

      protected int subGriditnstm_Recordsperpage( )
      {
         if ( subGriditnstm_Rows > 0 )
         {
            return subGriditnstm_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGriditnstm_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRIDITNSTM_nFirstRecordOnPage/ (decimal)(subGriditnstm_Recordsperpage( ))))+1) ;
      }

      protected short subgriditnstm_firstpage( )
      {
         GRIDITNSTM_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRIDITNSTM_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDITNSTM_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGriditnstm_refresh( subGriditnstm_Rows) ;
         }
         return 0 ;
      }

      protected short subgriditnstm_nextpage( )
      {
         GRIDITNSTM_nRecordCount = subGriditnstm_Recordcount( );
         if ( ( GRIDITNSTM_nRecordCount >= subGriditnstm_Recordsperpage( ) ) && ( GRIDITNSTM_nEOF == 0 ) )
         {
            GRIDITNSTM_nFirstRecordOnPage = (long)(GRIDITNSTM_nFirstRecordOnPage+subGriditnstm_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRIDITNSTM_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDITNSTM_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGriditnstm_refresh( subGriditnstm_Rows) ;
         }
         return (short)(((GRIDITNSTM_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgriditnstm_previouspage( )
      {
         if ( GRIDITNSTM_nFirstRecordOnPage >= subGriditnstm_Recordsperpage( ) )
         {
            GRIDITNSTM_nFirstRecordOnPage = (long)(GRIDITNSTM_nFirstRecordOnPage-subGriditnstm_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRIDITNSTM_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDITNSTM_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGriditnstm_refresh( subGriditnstm_Rows) ;
         }
         return 0 ;
      }

      protected short subgriditnstm_lastpage( )
      {
         GRIDITNSTM_nRecordCount = subGriditnstm_Recordcount( );
         if ( GRIDITNSTM_nRecordCount > subGriditnstm_Recordsperpage( ) )
         {
            if ( ((int)((GRIDITNSTM_nRecordCount) % (subGriditnstm_Recordsperpage( )))) == 0 )
            {
               GRIDITNSTM_nFirstRecordOnPage = (long)(GRIDITNSTM_nRecordCount-subGriditnstm_Recordsperpage( ));
            }
            else
            {
               GRIDITNSTM_nFirstRecordOnPage = (long)(GRIDITNSTM_nRecordCount-((int)((GRIDITNSTM_nRecordCount) % (subGriditnstm_Recordsperpage( )))));
            }
         }
         else
         {
            GRIDITNSTM_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRIDITNSTM_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDITNSTM_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGriditnstm_refresh( subGriditnstm_Rows) ;
         }
         return 0 ;
      }

      protected int subgriditnstm_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRIDITNSTM_nFirstRecordOnPage = (long)(subGriditnstm_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRIDITNSTM_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRIDITNSTM_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDITNSTM_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGriditnstm_refresh( subGriditnstm_Rows) ;
         }
         return (int)(0) ;
      }

      protected int subGriditnprj_Pagecount( )
      {
         GRIDITNPRJ_nRecordCount = subGriditnprj_Recordcount( );
         if ( ((int)((GRIDITNPRJ_nRecordCount) % (subGriditnprj_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRIDITNPRJ_nRecordCount/ (decimal)(subGriditnprj_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRIDITNPRJ_nRecordCount/ (decimal)(subGriditnprj_Recordsperpage( ))))+1) ;
      }

      protected int subGriditnprj_Recordcount( )
      {
         return AV14Sdt_ItnPrj.Count ;
      }

      protected int subGriditnprj_Recordsperpage( )
      {
         if ( subGriditnprj_Rows > 0 )
         {
            return subGriditnprj_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGriditnprj_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRIDITNPRJ_nFirstRecordOnPage/ (decimal)(subGriditnprj_Recordsperpage( ))))+1) ;
      }

      protected short subgriditnprj_firstpage( )
      {
         GRIDITNPRJ_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRIDITNPRJ_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDITNPRJ_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGriditnprj_refresh( subGriditnprj_Rows) ;
         }
         return 0 ;
      }

      protected short subgriditnprj_nextpage( )
      {
         GRIDITNPRJ_nRecordCount = subGriditnprj_Recordcount( );
         if ( ( GRIDITNPRJ_nRecordCount >= subGriditnprj_Recordsperpage( ) ) && ( GRIDITNPRJ_nEOF == 0 ) )
         {
            GRIDITNPRJ_nFirstRecordOnPage = (long)(GRIDITNPRJ_nFirstRecordOnPage+subGriditnprj_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRIDITNPRJ_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDITNPRJ_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGriditnprj_refresh( subGriditnprj_Rows) ;
         }
         return (short)(((GRIDITNPRJ_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgriditnprj_previouspage( )
      {
         if ( GRIDITNPRJ_nFirstRecordOnPage >= subGriditnprj_Recordsperpage( ) )
         {
            GRIDITNPRJ_nFirstRecordOnPage = (long)(GRIDITNPRJ_nFirstRecordOnPage-subGriditnprj_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRIDITNPRJ_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDITNPRJ_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGriditnprj_refresh( subGriditnprj_Rows) ;
         }
         return 0 ;
      }

      protected short subgriditnprj_lastpage( )
      {
         GRIDITNPRJ_nRecordCount = subGriditnprj_Recordcount( );
         if ( GRIDITNPRJ_nRecordCount > subGriditnprj_Recordsperpage( ) )
         {
            if ( ((int)((GRIDITNPRJ_nRecordCount) % (subGriditnprj_Recordsperpage( )))) == 0 )
            {
               GRIDITNPRJ_nFirstRecordOnPage = (long)(GRIDITNPRJ_nRecordCount-subGriditnprj_Recordsperpage( ));
            }
            else
            {
               GRIDITNPRJ_nFirstRecordOnPage = (long)(GRIDITNPRJ_nRecordCount-((int)((GRIDITNPRJ_nRecordCount) % (subGriditnprj_Recordsperpage( )))));
            }
         }
         else
         {
            GRIDITNPRJ_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRIDITNPRJ_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDITNPRJ_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGriditnprj_refresh( subGriditnprj_Rows) ;
         }
         return 0 ;
      }

      protected int subgriditnprj_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRIDITNPRJ_nFirstRecordOnPage = (long)(subGriditnprj_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRIDITNPRJ_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRIDITNPRJ_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDITNPRJ_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGriditnprj_refresh( subGriditnprj_Rows) ;
         }
         return (int)(0) ;
      }

      protected void STRUPDT0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavProjeto_sigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProjeto_sigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_sigla_Enabled), 5, 0)));
         edtavCtlitemcodigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlitemcodigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlitemcodigo_Enabled), 5, 0)));
         edtavCtlitemtipo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlitemtipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlitemtipo_Enabled), 5, 0)));
         edtavCtlitemnome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlitemnome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlitemnome_Enabled), 5, 0)));
         edtavCtlitemtipofn_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlitemtipofn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlitemtipofn_Enabled), 5, 0)));
         edtavPrjitemcodigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPrjitemcodigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPrjitemcodigo_Enabled), 5, 0)));
         edtavPrjitemtipo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPrjitemtipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPrjitemtipo_Enabled), 5, 0)));
         edtavPrjitemnome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPrjitemnome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPrjitemnome_Enabled), 5, 0)));
         edtavPrjitemtipofn_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPrjitemtipofn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPrjitemtipofn_Enabled), 5, 0)));
         GXVvSISTEMA_CODIGO_htmlDT2( AV9AreaTrabalho_Codigo) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E15DT2 */
         E15DT2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "Sdt_itnprj"), AV14Sdt_ItnPrj);
            ajax_req_read_hidden_sdt(cgiGet( "Sdt_itnstm"), AV15Sdt_ItnStm);
            ajax_req_read_hidden_sdt(cgiGet( "vSDT_ITNSTM"), AV15Sdt_ItnStm);
            /* Read variables values. */
            AV5Projeto_Sigla = StringUtil.Upper( cgiGet( edtavProjeto_sigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Projeto_Sigla", AV5Projeto_Sigla);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPROJETO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV5Projeto_Sigla, "@!"))));
            dynavSistema_codigo.Name = dynavSistema_codigo_Internalname;
            dynavSistema_codigo.CurrentValue = cgiGet( dynavSistema_codigo_Internalname);
            AV6Sistema_Codigo = (int)(NumberUtil.Val( cgiGet( dynavSistema_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Sistema_Codigo), 6, 0)));
            cmbavSistema_tecnica.Name = cmbavSistema_tecnica_Internalname;
            cmbavSistema_tecnica.CurrentValue = cgiGet( cmbavSistema_tecnica_Internalname);
            AV26Sistema_Tecnica = cgiGet( cmbavSistema_tecnica_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Sistema_Tecnica", AV26Sistema_Tecnica);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavSistema_fatorajuste_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavSistema_fatorajuste_Internalname), ",", ".") > 999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSISTEMA_FATORAJUSTE");
               GX_FocusControl = edtavSistema_fatorajuste_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV23Sistema_FatorAjuste = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Sistema_FatorAjuste", StringUtil.LTrim( StringUtil.Str( AV23Sistema_FatorAjuste, 6, 2)));
            }
            else
            {
               AV23Sistema_FatorAjuste = context.localUtil.CToN( cgiGet( edtavSistema_fatorajuste_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Sistema_FatorAjuste", StringUtil.LTrim( StringUtil.Str( AV23Sistema_FatorAjuste, 6, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavSistema_prazo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavSistema_prazo_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSISTEMA_PRAZO");
               GX_FocusControl = edtavSistema_prazo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV24Sistema_Prazo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Sistema_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Sistema_Prazo), 4, 0)));
            }
            else
            {
               AV24Sistema_Prazo = (short)(context.localUtil.CToN( cgiGet( edtavSistema_prazo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Sistema_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Sistema_Prazo), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavSistema_custo_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavSistema_custo_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSISTEMA_CUSTO");
               GX_FocusControl = edtavSistema_custo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV21Sistema_Custo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Sistema_Custo", StringUtil.LTrim( StringUtil.Str( AV21Sistema_Custo, 18, 5)));
            }
            else
            {
               AV21Sistema_Custo = context.localUtil.CToN( cgiGet( edtavSistema_custo_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Sistema_Custo", StringUtil.LTrim( StringUtil.Str( AV21Sistema_Custo, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavSistema_esforco_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavSistema_esforco_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSISTEMA_ESFORCO");
               GX_FocusControl = edtavSistema_esforco_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV22Sistema_Esforco = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Sistema_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Sistema_Esforco), 4, 0)));
            }
            else
            {
               AV22Sistema_Esforco = (short)(context.localUtil.CToN( cgiGet( edtavSistema_esforco_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Sistema_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Sistema_Esforco), 4, 0)));
            }
            /* Read saved values. */
            nRC_GXsfl_44 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_44"), ",", "."));
            nRC_GXsfl_55 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_55"), ",", "."));
            GRIDITNSTM_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRIDITNSTM_nFirstRecordOnPage"), ",", "."));
            GRIDITNPRJ_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRIDITNPRJ_nFirstRecordOnPage"), ",", "."));
            GRIDITNSTM_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRIDITNSTM_nEOF"), ",", "."));
            GRIDITNPRJ_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRIDITNPRJ_nEOF"), ",", "."));
            subGriditnstm_Rows = (int)(context.localUtil.CToN( cgiGet( "GRIDITNSTM_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRIDITNSTM_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGriditnstm_Rows), 6, 0, ".", "")));
            subGriditnprj_Rows = (int)(context.localUtil.CToN( cgiGet( "GRIDITNPRJ_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRIDITNPRJ_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGriditnprj_Rows), 6, 0, ".", "")));
            Dvpanel_tblstm_Width = cgiGet( "DVPANEL_TBLSTM_Width");
            Dvpanel_tblstm_Cls = cgiGet( "DVPANEL_TBLSTM_Cls");
            Dvpanel_tblstm_Title = cgiGet( "DVPANEL_TBLSTM_Title");
            Dvpanel_tblstm_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TBLSTM_Collapsible"));
            Dvpanel_tblstm_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TBLSTM_Collapsed"));
            Dvpanel_tblstm_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TBLSTM_Autowidth"));
            Dvpanel_tblstm_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TBLSTM_Autoheight"));
            Dvpanel_tblstm_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TBLSTM_Showcollapseicon"));
            Dvpanel_tblstm_Iconposition = cgiGet( "DVPANEL_TBLSTM_Iconposition");
            Dvpanel_tblstm_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TBLSTM_Autoscroll"));
            Dvpanel_tblprj_Width = cgiGet( "DVPANEL_TBLPRJ_Width");
            Dvpanel_tblprj_Cls = cgiGet( "DVPANEL_TBLPRJ_Cls");
            Dvpanel_tblprj_Title = cgiGet( "DVPANEL_TBLPRJ_Title");
            Dvpanel_tblprj_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TBLPRJ_Collapsible"));
            Dvpanel_tblprj_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TBLPRJ_Collapsed"));
            Dvpanel_tblprj_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TBLPRJ_Autowidth"));
            Dvpanel_tblprj_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TBLPRJ_Autoheight"));
            Dvpanel_tblprj_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TBLPRJ_Showcollapseicon"));
            Dvpanel_tblprj_Iconposition = cgiGet( "DVPANEL_TBLPRJ_Iconposition");
            Dvpanel_tblprj_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TBLPRJ_Autoscroll"));
            nRC_GXsfl_44 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_44"), ",", "."));
            nGXsfl_44_fel_idx = 0;
            while ( nGXsfl_44_fel_idx < nRC_GXsfl_44 )
            {
               nGXsfl_44_fel_idx = (short)(((subGriditnstm_Islastpage==1)&&(nGXsfl_44_fel_idx+1>subGriditnstm_Recordsperpage( )) ? 1 : nGXsfl_44_fel_idx+1));
               sGXsfl_44_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_44_fel_idx), 4, 0)), 4, "0");
               SubsflControlProps_fel_442( ) ;
               AV31GXV1 = (short)(nGXsfl_44_fel_idx+GRIDITNSTM_nFirstRecordOnPage);
               if ( ( AV15Sdt_ItnStm.Count >= AV31GXV1 ) && ( AV31GXV1 > 0 ) )
               {
                  AV15Sdt_ItnStm.CurrentItem = ((SdtSDT_ItensConagem_Item)AV15Sdt_ItnStm.Item(AV31GXV1));
               }
            }
            if ( nGXsfl_44_fel_idx == 0 )
            {
               nGXsfl_44_idx = 1;
               sGXsfl_44_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_44_idx), 4, 0)), 4, "0");
               SubsflControlProps_442( ) ;
            }
            nGXsfl_44_fel_idx = 1;
            nRC_GXsfl_55 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_55"), ",", "."));
            nGXsfl_55_fel_idx = 0;
            while ( nGXsfl_55_fel_idx < nRC_GXsfl_55 )
            {
               nGXsfl_55_fel_idx = (short)(((subGriditnprj_Islastpage==1)&&(nGXsfl_55_fel_idx+1>subGriditnprj_Recordsperpage( )) ? 1 : nGXsfl_55_fel_idx+1));
               sGXsfl_55_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_55_fel_idx), 4, 0)), 4, "0");
               SubsflControlProps_fel_556( ) ;
               AV36GXV2 = (short)(nGXsfl_55_fel_idx+GRIDITNPRJ_nFirstRecordOnPage);
               if ( ( AV14Sdt_ItnPrj.Count >= AV36GXV2 ) && ( AV36GXV2 > 0 ) )
               {
                  AV14Sdt_ItnPrj.CurrentItem = ((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV36GXV2));
               }
            }
            if ( nGXsfl_55_fel_idx == 0 )
            {
               nGXsfl_55_idx = 1;
               sGXsfl_55_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_55_idx), 4, 0)), 4, "0");
               SubsflControlProps_556( ) ;
            }
            nGXsfl_55_fel_idx = 1;
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            /* Check if conditions changed and reset current page numbers */
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E15DT2 */
         E15DT2 ();
         if (returnInSub) return;
      }

      protected void E15DT2( )
      {
         /* Start Routine */
         subGriditnprj_Rows = 15;
         GxWebStd.gx_hidden_field( context, "GRIDITNPRJ_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGriditnprj_Rows), 6, 0, ".", "")));
         subGriditnstm_Rows = 15;
         GxWebStd.gx_hidden_field( context, "GRIDITNSTM_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGriditnstm_Rows), 6, 0, ".", "")));
         AV5Projeto_Sigla = AV28ParmProjeto_Sigla;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Projeto_Sigla", AV5Projeto_Sigla);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPROJETO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV5Projeto_Sigla, "@!"))));
         GXt_decimal1 = AV23Sistema_FatorAjuste;
         new prc_fapadrao(context ).execute( out  GXt_decimal1) ;
         AV23Sistema_FatorAjuste = GXt_decimal1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Sistema_FatorAjuste", StringUtil.LTrim( StringUtil.Str( AV23Sistema_FatorAjuste, 6, 2)));
      }

      protected void E11DT2( )
      {
         /* Sistema_codigo_Click Routine */
         AV15Sdt_ItnStm.Clear();
         gx_BV44 = true;
         /* Using cursor H00DT3 */
         pr_default.execute(1, new Object[] {AV6Sistema_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A394FuncaoDados_Ativo = H00DT3_A394FuncaoDados_Ativo[0];
            A370FuncaoDados_SistemaCod = H00DT3_A370FuncaoDados_SistemaCod[0];
            A369FuncaoDados_Nome = H00DT3_A369FuncaoDados_Nome[0];
            A373FuncaoDados_Tipo = H00DT3_A373FuncaoDados_Tipo[0];
            A755FuncaoDados_Contar = H00DT3_A755FuncaoDados_Contar[0];
            n755FuncaoDados_Contar = H00DT3_n755FuncaoDados_Contar[0];
            A368FuncaoDados_Codigo = H00DT3_A368FuncaoDados_Codigo[0];
            if ( A755FuncaoDados_Contar )
            {
               GXt_int2 = (short)(A377FuncaoDados_PF);
               new prc_fdpf(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_int2) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
               A377FuncaoDados_PF = (decimal)(GXt_int2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A377FuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( A377FuncaoDados_PF, 14, 5)));
            }
            else
            {
               A377FuncaoDados_PF = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A377FuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( A377FuncaoDados_PF, 14, 5)));
            }
            AV13Sdt_Item.gxTpr_Itemcodigo = A368FuncaoDados_Codigo;
            AV13Sdt_Item.gxTpr_Itemnome = A369FuncaoDados_Nome;
            AV13Sdt_Item.gxTpr_Itempfb = A377FuncaoDados_PF;
            AV13Sdt_Item.gxTpr_Itemtipofn = A373FuncaoDados_Tipo;
            AV13Sdt_Item.gxTpr_Itemtipo = "D";
            AV15Sdt_ItnStm.Add(AV13Sdt_Item, 0);
            gx_BV44 = true;
            AV13Sdt_Item = new SdtSDT_ItensConagem_Item(context);
            pr_default.readNext(1);
         }
         pr_default.close(1);
         /* Using cursor H00DT4 */
         pr_default.execute(2, new Object[] {AV6Sistema_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A183FuncaoAPF_Ativo = H00DT4_A183FuncaoAPF_Ativo[0];
            A360FuncaoAPF_SistemaCod = H00DT4_A360FuncaoAPF_SistemaCod[0];
            n360FuncaoAPF_SistemaCod = H00DT4_n360FuncaoAPF_SistemaCod[0];
            A166FuncaoAPF_Nome = H00DT4_A166FuncaoAPF_Nome[0];
            A184FuncaoAPF_Tipo = H00DT4_A184FuncaoAPF_Tipo[0];
            A165FuncaoAPF_Codigo = H00DT4_A165FuncaoAPF_Codigo[0];
            GXt_decimal1 = A386FuncaoAPF_PF;
            new prc_fapfpf(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_decimal1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            A386FuncaoAPF_PF = GXt_decimal1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A386FuncaoAPF_PF", StringUtil.LTrim( StringUtil.Str( A386FuncaoAPF_PF, 14, 5)));
            AV13Sdt_Item.gxTpr_Itemcodigo = A165FuncaoAPF_Codigo;
            AV13Sdt_Item.gxTpr_Itemnome = A166FuncaoAPF_Nome;
            AV13Sdt_Item.gxTpr_Itempfb = A386FuncaoAPF_PF;
            AV13Sdt_Item.gxTpr_Itemtipofn = A184FuncaoAPF_Tipo;
            AV13Sdt_Item.gxTpr_Itemtipo = "T";
            AV15Sdt_ItnStm.Add(AV13Sdt_Item, 0);
            gx_BV44 = true;
            AV13Sdt_Item = new SdtSDT_ItensConagem_Item(context);
            pr_default.readNext(2);
         }
         pr_default.close(2);
         AV15Sdt_ItnStm.Sort("ItemNome");
         gx_BV44 = true;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV15Sdt_ItnStm", AV15Sdt_ItnStm);
         nGXsfl_44_bak_idx = nGXsfl_44_idx;
         gxgrGriditnstm_refresh( subGriditnstm_Rows) ;
         nGXsfl_44_idx = nGXsfl_44_bak_idx;
         sGXsfl_44_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_44_idx), 4, 0)), 4, "0");
         SubsflControlProps_442( ) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV13Sdt_Item", AV13Sdt_Item);
      }

      protected void E16DT2( )
      {
         AV31GXV1 = (short)(nGXsfl_44_idx+GRIDITNSTM_nFirstRecordOnPage);
         if ( AV15Sdt_ItnStm.Count >= AV31GXV1 )
         {
            AV15Sdt_ItnStm.CurrentItem = ((SdtSDT_ItensConagem_Item)AV15Sdt_ItnStm.Item(AV31GXV1));
         }
         AV36GXV2 = (short)(nGXsfl_55_idx+GRIDITNPRJ_nFirstRecordOnPage);
         if ( AV14Sdt_ItnPrj.Count >= AV36GXV2 )
         {
            AV14Sdt_ItnPrj.CurrentItem = ((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV36GXV2));
         }
         /* Ctlitemnome_Click Routine */
         AV13Sdt_Item = new SdtSDT_ItensConagem_Item(context);
         AV13Sdt_Item.gxTpr_Itemcodigo = ((SdtSDT_ItensConagem_Item)(AV15Sdt_ItnStm.CurrentItem)).gxTpr_Itemcodigo;
         AV13Sdt_Item.gxTpr_Itemnome = ((SdtSDT_ItensConagem_Item)(AV15Sdt_ItnStm.CurrentItem)).gxTpr_Itemnome;
         AV13Sdt_Item.gxTpr_Itemtipo = ((SdtSDT_ItensConagem_Item)(AV15Sdt_ItnStm.CurrentItem)).gxTpr_Itemtipo;
         AV13Sdt_Item.gxTpr_Itempfb = ((SdtSDT_ItensConagem_Item)(AV15Sdt_ItnStm.CurrentItem)).gxTpr_Itempfb;
         AV13Sdt_Item.gxTpr_Itemtipofn = ((SdtSDT_ItensConagem_Item)(AV15Sdt_ItnStm.CurrentItem)).gxTpr_Itemtipofn;
         AV14Sdt_ItnPrj.Add(AV13Sdt_Item, 0);
         gx_BV55 = true;
         AV14Sdt_ItnPrj.Sort("ItemNome");
         gx_BV55 = true;
         AV17i = (short)(AV15Sdt_ItnStm.IndexOf(((SdtSDT_ItensConagem_Item)(AV15Sdt_ItnStm.CurrentItem))));
         AV15Sdt_ItnStm.RemoveItem(AV17i);
         gx_BV44 = true;
         gxgrGriditnstm_refresh( subGriditnstm_Rows) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV13Sdt_Item", AV13Sdt_Item);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV14Sdt_ItnPrj", AV14Sdt_ItnPrj);
         nGXsfl_55_bak_idx = nGXsfl_55_idx;
         gxgrGriditnprj_refresh( subGriditnprj_Rows) ;
         nGXsfl_55_idx = nGXsfl_55_bak_idx;
         sGXsfl_55_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_55_idx), 4, 0)), 4, "0");
         SubsflControlProps_556( ) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV15Sdt_ItnStm", AV15Sdt_ItnStm);
         nGXsfl_44_bak_idx = nGXsfl_44_idx;
         gxgrGriditnstm_refresh( subGriditnstm_Rows) ;
         nGXsfl_44_idx = nGXsfl_44_bak_idx;
         sGXsfl_44_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_44_idx), 4, 0)), 4, "0");
         SubsflControlProps_442( ) ;
      }

      protected void E13DT2( )
      {
         AV36GXV2 = (short)(nGXsfl_55_idx+GRIDITNPRJ_nFirstRecordOnPage);
         if ( AV14Sdt_ItnPrj.Count >= AV36GXV2 )
         {
            AV14Sdt_ItnPrj.CurrentItem = ((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV36GXV2));
         }
         AV31GXV1 = (short)(nGXsfl_44_idx+GRIDITNSTM_nFirstRecordOnPage);
         if ( AV15Sdt_ItnStm.Count >= AV31GXV1 )
         {
            AV15Sdt_ItnStm.CurrentItem = ((SdtSDT_ItensConagem_Item)AV15Sdt_ItnStm.Item(AV31GXV1));
         }
         /* Prjitemnome_Click Routine */
         AV13Sdt_Item = new SdtSDT_ItensConagem_Item(context);
         AV13Sdt_Item.gxTpr_Itemcodigo = ((SdtSDT_ItensConagem_Item)(AV14Sdt_ItnPrj.CurrentItem)).gxTpr_Itemcodigo;
         AV13Sdt_Item.gxTpr_Itemnome = ((SdtSDT_ItensConagem_Item)(AV14Sdt_ItnPrj.CurrentItem)).gxTpr_Itemnome;
         AV13Sdt_Item.gxTpr_Itemtipo = ((SdtSDT_ItensConagem_Item)(AV14Sdt_ItnPrj.CurrentItem)).gxTpr_Itemtipo;
         AV13Sdt_Item.gxTpr_Itempfb = ((SdtSDT_ItensConagem_Item)(AV14Sdt_ItnPrj.CurrentItem)).gxTpr_Itempfb;
         AV13Sdt_Item.gxTpr_Itemtipofn = ((SdtSDT_ItensConagem_Item)(AV14Sdt_ItnPrj.CurrentItem)).gxTpr_Itemtipofn;
         AV15Sdt_ItnStm.Add(AV13Sdt_Item, 0);
         gx_BV44 = true;
         AV15Sdt_ItnStm.Sort("ItemNome");
         gx_BV44 = true;
         AV17i = (short)(AV14Sdt_ItnPrj.IndexOf(((SdtSDT_ItensConagem_Item)(AV14Sdt_ItnPrj.CurrentItem))));
         AV14Sdt_ItnPrj.RemoveItem(AV17i);
         gx_BV55 = true;
         gxgrGriditnprj_refresh( subGriditnprj_Rows) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV13Sdt_Item", AV13Sdt_Item);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV15Sdt_ItnStm", AV15Sdt_ItnStm);
         nGXsfl_44_bak_idx = nGXsfl_44_idx;
         gxgrGriditnstm_refresh( subGriditnstm_Rows) ;
         nGXsfl_44_idx = nGXsfl_44_bak_idx;
         sGXsfl_44_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_44_idx), 4, 0)), 4, "0");
         SubsflControlProps_442( ) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV14Sdt_ItnPrj", AV14Sdt_ItnPrj);
         nGXsfl_55_bak_idx = nGXsfl_55_idx;
         gxgrGriditnprj_refresh( subGriditnprj_Rows) ;
         nGXsfl_55_idx = nGXsfl_55_bak_idx;
         sGXsfl_55_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_55_idx), 4, 0)), 4, "0");
         SubsflControlProps_556( ) ;
      }

      protected void E12DT2( )
      {
         AV36GXV2 = (short)(nGXsfl_55_idx+GRIDITNPRJ_nFirstRecordOnPage);
         if ( AV14Sdt_ItnPrj.Count >= AV36GXV2 )
         {
            AV14Sdt_ItnPrj.CurrentItem = ((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV36GXV2));
         }
         /* 'Continuar' Routine */
         /* Using cursor H00DT5 */
         pr_default.execute(3, new Object[] {AV6Sistema_Codigo});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A127Sistema_Codigo = H00DT5_A127Sistema_Codigo[0];
            A416Sistema_Nome = H00DT5_A416Sistema_Nome[0];
            AV19Sistema_Nome = A416Sistema_Nome;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(3);
         AV18WebSession.Set("ItensProjeto", AV14Sdt_ItnPrj.ToXml(false, true, "SDT_ItensConagem", "GxEv3Up14_Meetrika"));
         context.wjLoc = formatLink("wp_projetomelhoriaobs.aspx") + "?" + UrlEncode("" +AV20Projeto_Codigo) + "," + UrlEncode("" +AV6Sistema_Codigo) + "," + UrlEncode("" +AV9AreaTrabalho_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV28ParmProjeto_Sigla)) + "," + UrlEncode(StringUtil.RTrim(AV19Sistema_Nome)) + "," + UrlEncode(StringUtil.Str(AV23Sistema_FatorAjuste,6,2)) + "," + UrlEncode("" +AV24Sistema_Prazo) + "," + UrlEncode(StringUtil.Str(AV21Sistema_Custo,18,5)) + "," + UrlEncode("" +AV22Sistema_Esforco) + "," + UrlEncode(StringUtil.RTrim(AV25Sistema_Tipo)) + "," + UrlEncode(StringUtil.RTrim(AV26Sistema_Tecnica));
         context.wjLocDisableFrm = 1;
         cmbavSistema_tecnica.CurrentValue = StringUtil.RTrim( AV26Sistema_Tecnica);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_tecnica_Internalname, "Values", cmbavSistema_tecnica.ToJavascriptSource());
      }

      private void E17DT2( )
      {
         /* Griditnstm_Load Routine */
         AV31GXV1 = 1;
         while ( AV31GXV1 <= AV15Sdt_ItnStm.Count )
         {
            AV15Sdt_ItnStm.CurrentItem = ((SdtSDT_ItensConagem_Item)AV15Sdt_ItnStm.Item(AV31GXV1));
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 44;
            }
            if ( ( subGriditnstm_Islastpage == 1 ) || ( subGriditnstm_Rows == 0 ) || ( ( GRIDITNSTM_nCurrentRecord >= GRIDITNSTM_nFirstRecordOnPage ) && ( GRIDITNSTM_nCurrentRecord < GRIDITNSTM_nFirstRecordOnPage + subGriditnstm_Recordsperpage( ) ) ) )
            {
               sendrow_442( ) ;
               GRIDITNSTM_nEOF = 0;
               GxWebStd.gx_hidden_field( context, "GRIDITNSTM_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDITNSTM_nEOF), 1, 0, ".", "")));
               if ( GRIDITNSTM_nCurrentRecord + 1 >= subGriditnstm_Recordcount( ) )
               {
                  GRIDITNSTM_nEOF = 1;
                  GxWebStd.gx_hidden_field( context, "GRIDITNSTM_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDITNSTM_nEOF), 1, 0, ".", "")));
               }
            }
            GRIDITNSTM_nCurrentRecord = (long)(GRIDITNSTM_nCurrentRecord+1);
            if ( isFullAjaxMode( ) && ( nGXsfl_44_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(44, GriditnstmRow);
            }
            AV31GXV1 = (short)(AV31GXV1+1);
         }
      }

      private void E14DT6( )
      {
         /* Griditnprj_Load Routine */
         AV36GXV2 = 1;
         while ( AV36GXV2 <= AV14Sdt_ItnPrj.Count )
         {
            AV14Sdt_ItnPrj.CurrentItem = ((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV36GXV2));
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 55;
            }
            if ( ( subGriditnprj_Islastpage == 1 ) || ( subGriditnprj_Rows == 0 ) || ( ( GRIDITNPRJ_nCurrentRecord >= GRIDITNPRJ_nFirstRecordOnPage ) && ( GRIDITNPRJ_nCurrentRecord < GRIDITNPRJ_nFirstRecordOnPage + subGriditnprj_Recordsperpage( ) ) ) )
            {
               sendrow_556( ) ;
               GRIDITNPRJ_nEOF = 0;
               GxWebStd.gx_hidden_field( context, "GRIDITNPRJ_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDITNPRJ_nEOF), 1, 0, ".", "")));
               if ( GRIDITNPRJ_nCurrentRecord + 1 >= subGriditnprj_Recordcount( ) )
               {
                  GRIDITNPRJ_nEOF = 1;
                  GxWebStd.gx_hidden_field( context, "GRIDITNPRJ_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDITNPRJ_nEOF), 1, 0, ".", "")));
               }
            }
            GRIDITNPRJ_nCurrentRecord = (long)(GRIDITNPRJ_nCurrentRecord+1);
            if ( isFullAjaxMode( ) && ( nGXsfl_55_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(55, GriditnprjRow);
            }
            AV36GXV2 = (short)(AV36GXV2+1);
         }
      }

      protected void wb_table1_2_DT2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain2_Internalname, tblTablemain2_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_DT2( true) ;
         }
         else
         {
            wb_table2_5_DT2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_DT2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table3_16_DT2( true) ;
         }
         else
         {
            wb_table3_16_DT2( false) ;
         }
         return  ;
      }

      protected void wb_table3_16_DT2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttContinuar_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(44), 2, 0)+","+"null"+");", "Continuar", bttContinuar_Jsonclick, 5, "Incluir as melhorias de cada item ", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'CONTINUAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_ProjetoMelhoriaItens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_DT2e( true) ;
         }
         else
         {
            wb_table1_2_DT2e( false) ;
         }
      }

      protected void wb_table3_16_DT2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            wb_table4_19_DT2( true) ;
         }
         else
         {
            wb_table4_19_DT2( false) ;
         }
         return  ;
      }

      protected void wb_table4_19_DT2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock5_Internalname, "T�cnica:", "", "", lblTextblock5_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ProjetoMelhoriaItens.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'" + sGXsfl_44_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavSistema_tecnica, cmbavSistema_tecnica_Internalname, StringUtil.RTrim( AV26Sistema_Tecnica), 1, cmbavSistema_tecnica_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "", true, "HLP_WP_ProjetoMelhoriaItens.htm");
            cmbavSistema_tecnica.CurrentValue = StringUtil.RTrim( AV26Sistema_Tecnica);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_tecnica_Internalname, "Values", (String)(cmbavSistema_tecnica.ToJavascriptSource()));
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;&nbsp; ") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "Fator de Ajuste:", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ProjetoMelhoriaItens.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'" + sGXsfl_44_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSistema_fatorajuste_Internalname, StringUtil.LTrim( StringUtil.NToC( AV23Sistema_FatorAjuste, 6, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV23Sistema_FatorAjuste, "ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,30);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSistema_fatorajuste_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 60, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_ProjetoMelhoriaItens.htm");
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;&nbsp; ") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "Prazo:", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ProjetoMelhoriaItens.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'" + sGXsfl_44_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSistema_prazo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24Sistema_Prazo), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV24Sistema_Prazo), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,32);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSistema_prazo_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_ProjetoMelhoriaItens.htm");
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock3_Internalname, "C:usto:", "", "", lblTextblock3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ProjetoMelhoriaItens.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'" + sGXsfl_44_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSistema_custo_Internalname, StringUtil.LTrim( StringUtil.NToC( AV21Sistema_Custo, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV21Sistema_Custo, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSistema_custo_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 90, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_ProjetoMelhoriaItens.htm");
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;&nbsp; ") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock4_Internalname, "Esfor�o:", "", "", lblTextblock4_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ProjetoMelhoriaItens.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'" + sGXsfl_44_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSistema_esforco_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22Sistema_Esforco), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV22Sistema_Esforco), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,36);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSistema_esforco_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_ProjetoMelhoriaItens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TBLSTMContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TBLSTMContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table5_41_DT2( true) ;
         }
         else
         {
            wb_table5_41_DT2( false) ;
         }
         return  ;
      }

      protected void wb_table5_41_DT2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TBLPRJContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TBLPRJContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table6_52_DT2( true) ;
         }
         else
         {
            wb_table6_52_DT2( false) ;
         }
         return  ;
      }

      protected void wb_table6_52_DT2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_16_DT2e( true) ;
         }
         else
         {
            wb_table3_16_DT2e( false) ;
         }
      }

      protected void wb_table6_52_DT2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTblprj_Internalname, tblTblprj_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /*  Grid Control  */
            GriditnprjContainer.SetWrapped(nGXWrapped);
            if ( GriditnprjContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GriditnprjContainer"+"DivS\" data-gxgridid=\"55\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGriditnprj_Internalname, subGriditnprj_Internalname, "", "WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGriditnprj_Backcolorstyle == 0 )
               {
                  subGriditnprj_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGriditnprj_Class) > 0 )
                  {
                     subGriditnprj_Linesclass = subGriditnprj_Class+"Title";
                  }
               }
               else
               {
                  subGriditnprj_Titlebackstyle = 1;
                  if ( subGriditnprj_Backcolorstyle == 1 )
                  {
                     subGriditnprj_Titlebackcolor = subGriditnprj_Allbackcolor;
                     if ( StringUtil.Len( subGriditnprj_Class) > 0 )
                     {
                        subGriditnprj_Linesclass = subGriditnprj_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGriditnprj_Class) > 0 )
                     {
                        subGriditnprj_Linesclass = subGriditnprj_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGriditnprj_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGriditnprj_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Dados/Trn") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGriditnprj_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Nome") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGriditnprj_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Tipo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GriditnprjContainer.AddObjectProperty("GridName", "Griditnprj");
            }
            else
            {
               GriditnprjContainer.AddObjectProperty("GridName", "Griditnprj");
               GriditnprjContainer.AddObjectProperty("Class", "WorkWith");
               GriditnprjContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GriditnprjContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GriditnprjContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGriditnprj_Backcolorstyle), 1, 0, ".", "")));
               GriditnprjContainer.AddObjectProperty("CmpContext", "");
               GriditnprjContainer.AddObjectProperty("InMasterPage", "false");
               GriditnprjColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GriditnprjColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavPrjitemcodigo_Enabled), 5, 0, ".", "")));
               GriditnprjContainer.AddColumnProperties(GriditnprjColumn);
               GriditnprjColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GriditnprjColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavPrjitemtipo_Enabled), 5, 0, ".", "")));
               GriditnprjContainer.AddColumnProperties(GriditnprjColumn);
               GriditnprjColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GriditnprjColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavPrjitemnome_Enabled), 5, 0, ".", "")));
               GriditnprjContainer.AddColumnProperties(GriditnprjColumn);
               GriditnprjColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GriditnprjColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavPrjitemtipofn_Enabled), 5, 0, ".", "")));
               GriditnprjContainer.AddColumnProperties(GriditnprjColumn);
               GriditnprjContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGriditnprj_Allowselection), 1, 0, ".", "")));
               GriditnprjContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGriditnprj_Selectioncolor), 9, 0, ".", "")));
               GriditnprjContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGriditnprj_Allowhovering), 1, 0, ".", "")));
               GriditnprjContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGriditnprj_Hoveringcolor), 9, 0, ".", "")));
               GriditnprjContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGriditnprj_Allowcollapsing), 1, 0, ".", "")));
               GriditnprjContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGriditnprj_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 55 )
         {
            wbEnd = 0;
            nRC_GXsfl_55 = (short)(nGXsfl_55_idx-1);
            if ( GriditnprjContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GriditnprjContainer.AddObjectProperty("GRIDITNPRJ_nEOF", GRIDITNPRJ_nEOF);
               GriditnprjContainer.AddObjectProperty("GRIDITNPRJ_nFirstRecordOnPage", GRIDITNPRJ_nFirstRecordOnPage);
               AV36GXV2 = nGXsfl_55_idx;
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GriditnprjContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Griditnprj", GriditnprjContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GriditnprjContainerData", GriditnprjContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GriditnprjContainerData"+"V", GriditnprjContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GriditnprjContainerData"+"V"+"\" value='"+GriditnprjContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_52_DT2e( true) ;
         }
         else
         {
            wb_table6_52_DT2e( false) ;
         }
      }

      protected void wb_table5_41_DT2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTblstm_Internalname, tblTblstm_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /*  Grid Control  */
            GriditnstmContainer.SetWrapped(nGXWrapped);
            if ( GriditnstmContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GriditnstmContainer"+"DivS\" data-gxgridid=\"44\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGriditnstm_Internalname, subGriditnstm_Internalname, "", "WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGriditnstm_Backcolorstyle == 0 )
               {
                  subGriditnstm_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGriditnstm_Class) > 0 )
                  {
                     subGriditnstm_Linesclass = subGriditnstm_Class+"Title";
                  }
               }
               else
               {
                  subGriditnstm_Titlebackstyle = 1;
                  if ( subGriditnstm_Backcolorstyle == 1 )
                  {
                     subGriditnstm_Titlebackcolor = subGriditnstm_Allbackcolor;
                     if ( StringUtil.Len( subGriditnstm_Class) > 0 )
                     {
                        subGriditnstm_Linesclass = subGriditnstm_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGriditnstm_Class) > 0 )
                     {
                        subGriditnstm_Linesclass = subGriditnstm_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGriditnstm_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGriditnstm_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Dados/Trn") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGriditnstm_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Nome") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGriditnstm_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Tipo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GriditnstmContainer.AddObjectProperty("GridName", "Griditnstm");
            }
            else
            {
               GriditnstmContainer.AddObjectProperty("GridName", "Griditnstm");
               GriditnstmContainer.AddObjectProperty("Class", "WorkWith");
               GriditnstmContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GriditnstmContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GriditnstmContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGriditnstm_Backcolorstyle), 1, 0, ".", "")));
               GriditnstmContainer.AddObjectProperty("CmpContext", "");
               GriditnstmContainer.AddObjectProperty("InMasterPage", "false");
               GriditnstmColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GriditnstmColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlitemcodigo_Enabled), 5, 0, ".", "")));
               GriditnstmContainer.AddColumnProperties(GriditnstmColumn);
               GriditnstmColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GriditnstmColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlitemtipo_Enabled), 5, 0, ".", "")));
               GriditnstmContainer.AddColumnProperties(GriditnstmColumn);
               GriditnstmColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GriditnstmColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlitemnome_Enabled), 5, 0, ".", "")));
               GriditnstmContainer.AddColumnProperties(GriditnstmColumn);
               GriditnstmColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GriditnstmColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlitemtipofn_Enabled), 5, 0, ".", "")));
               GriditnstmContainer.AddColumnProperties(GriditnstmColumn);
               GriditnstmContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGriditnstm_Allowselection), 1, 0, ".", "")));
               GriditnstmContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGriditnstm_Selectioncolor), 9, 0, ".", "")));
               GriditnstmContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGriditnstm_Allowhovering), 1, 0, ".", "")));
               GriditnstmContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGriditnstm_Hoveringcolor), 9, 0, ".", "")));
               GriditnstmContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGriditnstm_Allowcollapsing), 1, 0, ".", "")));
               GriditnstmContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGriditnstm_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 44 )
         {
            wbEnd = 0;
            nRC_GXsfl_44 = (short)(nGXsfl_44_idx-1);
            if ( GriditnstmContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GriditnstmContainer.AddObjectProperty("GRIDITNSTM_nEOF", GRIDITNSTM_nEOF);
               GriditnstmContainer.AddObjectProperty("GRIDITNSTM_nFirstRecordOnPage", GRIDITNSTM_nFirstRecordOnPage);
               AV31GXV1 = nGXsfl_44_idx;
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GriditnstmContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Griditnstm", GriditnstmContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GriditnstmContainerData", GriditnstmContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GriditnstmContainerData"+"V", GriditnstmContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GriditnstmContainerData"+"V"+"\" value='"+GriditnstmContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_41_DT2e( true) ;
         }
         else
         {
            wb_table5_41_DT2e( false) ;
         }
      }

      protected void wb_table4_19_DT2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Table", 0, "", "", 5, 5, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_codigo2_Internalname, "Sistema:", "", "", lblTextblocksistema_codigo2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ProjetoMelhoriaItens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_44_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavSistema_codigo, dynavSistema_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV6Sistema_Codigo), 6, 0)), 1, dynavSistema_codigo_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVSISTEMA_CODIGO.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_WP_ProjetoMelhoriaItens.htm");
            dynavSistema_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV6Sistema_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSistema_codigo_Internalname, "Values", (String)(dynavSistema_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_19_DT2e( true) ;
         }
         else
         {
            wb_table4_19_DT2e( false) ;
         }
      }

      protected void wb_table2_5_DT2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedtextblocktitle2_Internalname, tblTablemergedtextblocktitle2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktitle2_Internalname, "Projeto de Melhoria :: ", "", "", lblTextblocktitle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_ProjetoMelhoriaItens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_44_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavProjeto_sigla_Internalname, StringUtil.RTrim( AV5Projeto_Sigla), StringUtil.RTrim( context.localUtil.Format( AV5Projeto_Sigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,10);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavProjeto_sigla_Jsonclick, 0, "AttributeTitleWWP", "", "", "", 1, edtavProjeto_sigla_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_ProjetoMelhoriaItens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_DT2e( true) ;
         }
         else
         {
            wb_table2_5_DT2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV20Projeto_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Projeto_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPROJETO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV20Projeto_Codigo), "ZZZZZ9")));
         AV9AreaTrabalho_Codigo = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9AreaTrabalho_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vAREATRABALHO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9AreaTrabalho_Codigo), "ZZZZZ9")));
         AV28ParmProjeto_Sigla = (String)getParm(obj,2);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ParmProjeto_Sigla", AV28ParmProjeto_Sigla);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPARMPROJETO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV28ParmProjeto_Sigla, "@!"))));
         AV25Sistema_Tipo = (String)getParm(obj,3);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Sistema_Tipo", AV25Sistema_Tipo);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PADT2( ) ;
         WSDT2( ) ;
         WEDT2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117452325");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_projetomelhoriaitens.js", "?20203117452326");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_442( )
      {
         edtavCtlitemcodigo_Internalname = "CTLITEMCODIGO_"+sGXsfl_44_idx;
         edtavCtlitemtipo_Internalname = "CTLITEMTIPO_"+sGXsfl_44_idx;
         edtavCtlitemnome_Internalname = "CTLITEMNOME_"+sGXsfl_44_idx;
         edtavCtlitemtipofn_Internalname = "CTLITEMTIPOFN_"+sGXsfl_44_idx;
      }

      protected void SubsflControlProps_fel_442( )
      {
         edtavCtlitemcodigo_Internalname = "CTLITEMCODIGO_"+sGXsfl_44_fel_idx;
         edtavCtlitemtipo_Internalname = "CTLITEMTIPO_"+sGXsfl_44_fel_idx;
         edtavCtlitemnome_Internalname = "CTLITEMNOME_"+sGXsfl_44_fel_idx;
         edtavCtlitemtipofn_Internalname = "CTLITEMTIPOFN_"+sGXsfl_44_fel_idx;
      }

      protected void sendrow_442( )
      {
         SubsflControlProps_442( ) ;
         WBDT0( ) ;
         if ( ( subGriditnstm_Rows * 1 == 0 ) || ( nGXsfl_44_idx <= subGriditnstm_Recordsperpage( ) * 1 ) )
         {
            GriditnstmRow = GXWebRow.GetNew(context,GriditnstmContainer);
            if ( subGriditnstm_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGriditnstm_Backstyle = 0;
               if ( StringUtil.StrCmp(subGriditnstm_Class, "") != 0 )
               {
                  subGriditnstm_Linesclass = subGriditnstm_Class+"Odd";
               }
            }
            else if ( subGriditnstm_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGriditnstm_Backstyle = 0;
               subGriditnstm_Backcolor = subGriditnstm_Allbackcolor;
               if ( StringUtil.StrCmp(subGriditnstm_Class, "") != 0 )
               {
                  subGriditnstm_Linesclass = subGriditnstm_Class+"Uniform";
               }
            }
            else if ( subGriditnstm_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGriditnstm_Backstyle = 1;
               if ( StringUtil.StrCmp(subGriditnstm_Class, "") != 0 )
               {
                  subGriditnstm_Linesclass = subGriditnstm_Class+"Odd";
               }
               subGriditnstm_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGriditnstm_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGriditnstm_Backstyle = 1;
               if ( ((int)((nGXsfl_44_idx) % (2))) == 0 )
               {
                  subGriditnstm_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGriditnstm_Class, "") != 0 )
                  {
                     subGriditnstm_Linesclass = subGriditnstm_Class+"Even";
                  }
               }
               else
               {
                  subGriditnstm_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGriditnstm_Class, "") != 0 )
                  {
                     subGriditnstm_Linesclass = subGriditnstm_Class+"Odd";
                  }
               }
            }
            if ( GriditnstmContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGriditnstm_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_44_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GriditnstmContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GriditnstmRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlitemcodigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(((SdtSDT_ItensConagem_Item)AV15Sdt_ItnStm.Item(AV31GXV1)).gxTpr_Itemcodigo), 6, 0, ",", "")),((edtavCtlitemcodigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(((SdtSDT_ItensConagem_Item)AV15Sdt_ItnStm.Item(AV31GXV1)).gxTpr_Itemcodigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(((SdtSDT_ItensConagem_Item)AV15Sdt_ItnStm.Item(AV31GXV1)).gxTpr_Itemcodigo), "ZZZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlitemcodigo_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavCtlitemcodigo_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)44,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GriditnstmContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GriditnstmRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlitemtipo_Internalname,StringUtil.RTrim( ((SdtSDT_ItensConagem_Item)AV15Sdt_ItnStm.Item(AV31GXV1)).gxTpr_Itemtipo),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlitemtipo_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavCtlitemtipo_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)1,(short)0,(short)0,(short)44,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GriditnstmContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GriditnstmRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlitemnome_Internalname,StringUtil.RTrim( ((SdtSDT_ItensConagem_Item)AV15Sdt_ItnStm.Item(AV31GXV1)).gxTpr_Itemnome),StringUtil.RTrim( context.localUtil.Format( ((SdtSDT_ItensConagem_Item)AV15Sdt_ItnStm.Item(AV31GXV1)).gxTpr_Itemnome, "@!")),(String)"","'"+""+"'"+",false,"+"'"+"ECTLITEMNOME.CLICK."+sGXsfl_44_idx+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlitemnome_Jsonclick,(short)5,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavCtlitemnome_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)44,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GriditnstmContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GriditnstmRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlitemtipofn_Internalname,StringUtil.RTrim( ((SdtSDT_ItensConagem_Item)AV15Sdt_ItnStm.Item(AV31GXV1)).gxTpr_Itemtipofn),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlitemtipofn_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavCtlitemtipofn_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)3,(short)0,(short)0,(short)44,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            GriditnstmContainer.AddRow(GriditnstmRow);
            nGXsfl_44_idx = (short)(((subGriditnstm_Islastpage==1)&&(nGXsfl_44_idx+1>subGriditnstm_Recordsperpage( )) ? 1 : nGXsfl_44_idx+1));
            sGXsfl_44_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_44_idx), 4, 0)), 4, "0");
            SubsflControlProps_442( ) ;
         }
         /* End function sendrow_442 */
      }

      protected void SubsflControlProps_556( )
      {
         edtavPrjitemcodigo_Internalname = "PRJITEMCODIGO_"+sGXsfl_55_idx;
         edtavPrjitemtipo_Internalname = "PRJITEMTIPO_"+sGXsfl_55_idx;
         edtavPrjitemnome_Internalname = "PRJITEMNOME_"+sGXsfl_55_idx;
         edtavPrjitemtipofn_Internalname = "PRJITEMTIPOFN_"+sGXsfl_55_idx;
      }

      protected void SubsflControlProps_fel_556( )
      {
         edtavPrjitemcodigo_Internalname = "PRJITEMCODIGO_"+sGXsfl_55_fel_idx;
         edtavPrjitemtipo_Internalname = "PRJITEMTIPO_"+sGXsfl_55_fel_idx;
         edtavPrjitemnome_Internalname = "PRJITEMNOME_"+sGXsfl_55_fel_idx;
         edtavPrjitemtipofn_Internalname = "PRJITEMTIPOFN_"+sGXsfl_55_fel_idx;
      }

      protected void sendrow_556( )
      {
         SubsflControlProps_556( ) ;
         WBDT0( ) ;
         if ( ( subGriditnprj_Rows * 1 == 0 ) || ( nGXsfl_55_idx <= subGriditnprj_Recordsperpage( ) * 1 ) )
         {
            GriditnprjRow = GXWebRow.GetNew(context,GriditnprjContainer);
            if ( subGriditnprj_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGriditnprj_Backstyle = 0;
               if ( StringUtil.StrCmp(subGriditnprj_Class, "") != 0 )
               {
                  subGriditnprj_Linesclass = subGriditnprj_Class+"Odd";
               }
            }
            else if ( subGriditnprj_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGriditnprj_Backstyle = 0;
               subGriditnprj_Backcolor = subGriditnprj_Allbackcolor;
               if ( StringUtil.StrCmp(subGriditnprj_Class, "") != 0 )
               {
                  subGriditnprj_Linesclass = subGriditnprj_Class+"Uniform";
               }
            }
            else if ( subGriditnprj_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGriditnprj_Backstyle = 1;
               if ( StringUtil.StrCmp(subGriditnprj_Class, "") != 0 )
               {
                  subGriditnprj_Linesclass = subGriditnprj_Class+"Odd";
               }
               subGriditnprj_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGriditnprj_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGriditnprj_Backstyle = 1;
               if ( ((int)((nGXsfl_55_idx) % (2))) == 0 )
               {
                  subGriditnprj_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGriditnprj_Class, "") != 0 )
                  {
                     subGriditnprj_Linesclass = subGriditnprj_Class+"Even";
                  }
               }
               else
               {
                  subGriditnprj_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGriditnprj_Class, "") != 0 )
                  {
                     subGriditnprj_Linesclass = subGriditnprj_Class+"Odd";
                  }
               }
            }
            if ( GriditnprjContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGriditnprj_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_55_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GriditnprjContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GriditnprjRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPrjitemcodigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV36GXV2)).gxTpr_Itemcodigo), 6, 0, ",", "")),((edtavPrjitemcodigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV36GXV2)).gxTpr_Itemcodigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV36GXV2)).gxTpr_Itemcodigo), "ZZZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavPrjitemcodigo_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavPrjitemcodigo_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)55,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GriditnprjContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GriditnprjRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPrjitemtipo_Internalname,StringUtil.RTrim( ((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV36GXV2)).gxTpr_Itemtipo),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavPrjitemtipo_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavPrjitemtipo_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)1,(short)0,(short)0,(short)55,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GriditnprjContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GriditnprjRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPrjitemnome_Internalname,StringUtil.RTrim( ((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV36GXV2)).gxTpr_Itemnome),StringUtil.RTrim( context.localUtil.Format( ((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV36GXV2)).gxTpr_Itemnome, "@!")),(String)"","'"+""+"'"+",false,"+"'"+"EPRJITEMNOME.CLICK."+sGXsfl_55_idx+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavPrjitemnome_Jsonclick,(short)5,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavPrjitemnome_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)55,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GriditnprjContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GriditnprjRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPrjitemtipofn_Internalname,StringUtil.RTrim( ((SdtSDT_ItensConagem_Item)AV14Sdt_ItnPrj.Item(AV36GXV2)).gxTpr_Itemtipofn),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavPrjitemtipofn_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavPrjitemtipofn_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)3,(short)0,(short)0,(short)55,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            GriditnprjContainer.AddRow(GriditnprjRow);
            nGXsfl_55_idx = (short)(((subGriditnprj_Islastpage==1)&&(nGXsfl_55_idx+1>subGriditnprj_Recordsperpage( )) ? 1 : nGXsfl_55_idx+1));
            sGXsfl_55_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_55_idx), 4, 0)), 4, "0");
            SubsflControlProps_556( ) ;
         }
         /* End function sendrow_556 */
      }

      protected void init_default_properties( )
      {
         lblTextblocktitle2_Internalname = "TEXTBLOCKTITLE2";
         edtavProjeto_sigla_Internalname = "vPROJETO_SIGLA";
         tblTablemergedtextblocktitle2_Internalname = "TABLEMERGEDTEXTBLOCKTITLE2";
         lblTextblocksistema_codigo2_Internalname = "TEXTBLOCKSISTEMA_CODIGO2";
         dynavSistema_codigo_Internalname = "vSISTEMA_CODIGO";
         tblTable2_Internalname = "TABLE2";
         lblTextblock5_Internalname = "TEXTBLOCK5";
         cmbavSistema_tecnica_Internalname = "vSISTEMA_TECNICA";
         lblTextblock1_Internalname = "TEXTBLOCK1";
         edtavSistema_fatorajuste_Internalname = "vSISTEMA_FATORAJUSTE";
         lblTextblock2_Internalname = "TEXTBLOCK2";
         edtavSistema_prazo_Internalname = "vSISTEMA_PRAZO";
         lblTextblock3_Internalname = "TEXTBLOCK3";
         edtavSistema_custo_Internalname = "vSISTEMA_CUSTO";
         lblTextblock4_Internalname = "TEXTBLOCK4";
         edtavSistema_esforco_Internalname = "vSISTEMA_ESFORCO";
         edtavCtlitemcodigo_Internalname = "CTLITEMCODIGO";
         edtavCtlitemtipo_Internalname = "CTLITEMTIPO";
         edtavCtlitemnome_Internalname = "CTLITEMNOME";
         edtavCtlitemtipofn_Internalname = "CTLITEMTIPOFN";
         tblTblstm_Internalname = "TBLSTM";
         Dvpanel_tblstm_Internalname = "DVPANEL_TBLSTM";
         edtavPrjitemcodigo_Internalname = "PRJITEMCODIGO";
         edtavPrjitemtipo_Internalname = "PRJITEMTIPO";
         edtavPrjitemnome_Internalname = "PRJITEMNOME";
         edtavPrjitemtipofn_Internalname = "PRJITEMTIPOFN";
         tblTblprj_Internalname = "TBLPRJ";
         Dvpanel_tblprj_Internalname = "DVPANEL_TBLPRJ";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttContinuar_Internalname = "CONTINUAR";
         tblTablemain2_Internalname = "TABLEMAIN2";
         Form.Internalname = "FORM";
         subGriditnstm_Internalname = "GRIDITNSTM";
         subGriditnprj_Internalname = "GRIDITNPRJ";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavPrjitemtipofn_Jsonclick = "";
         edtavPrjitemnome_Jsonclick = "";
         edtavPrjitemtipo_Jsonclick = "";
         edtavPrjitemcodigo_Jsonclick = "";
         edtavCtlitemtipofn_Jsonclick = "";
         edtavCtlitemnome_Jsonclick = "";
         edtavCtlitemtipo_Jsonclick = "";
         edtavCtlitemcodigo_Jsonclick = "";
         edtavProjeto_sigla_Jsonclick = "";
         edtavProjeto_sigla_Enabled = 1;
         dynavSistema_codigo_Jsonclick = "";
         subGriditnstm_Allowcollapsing = 0;
         subGriditnstm_Allowselection = 0;
         edtavCtlitemtipofn_Enabled = 0;
         edtavCtlitemnome_Enabled = 0;
         edtavCtlitemtipo_Enabled = 0;
         edtavCtlitemcodigo_Enabled = 0;
         subGriditnstm_Class = "WorkWith";
         subGriditnprj_Allowcollapsing = 0;
         subGriditnprj_Allowselection = 0;
         edtavPrjitemtipofn_Enabled = 0;
         edtavPrjitemnome_Enabled = 0;
         edtavPrjitemtipo_Enabled = 0;
         edtavPrjitemcodigo_Enabled = 0;
         subGriditnprj_Class = "WorkWith";
         edtavSistema_esforco_Jsonclick = "";
         edtavSistema_custo_Jsonclick = "";
         edtavSistema_prazo_Jsonclick = "";
         edtavSistema_fatorajuste_Jsonclick = "";
         cmbavSistema_tecnica_Jsonclick = "";
         subGriditnprj_Backcolorstyle = 3;
         subGriditnstm_Backcolorstyle = 3;
         edtavPrjitemtipofn_Enabled = -1;
         edtavPrjitemnome_Enabled = -1;
         edtavPrjitemtipo_Enabled = -1;
         edtavPrjitemcodigo_Enabled = -1;
         edtavCtlitemtipofn_Enabled = -1;
         edtavCtlitemnome_Enabled = -1;
         edtavCtlitemtipo_Enabled = -1;
         edtavCtlitemcodigo_Enabled = -1;
         Dvpanel_tblprj_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tblprj_Iconposition = "left";
         Dvpanel_tblprj_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tblprj_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tblprj_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tblprj_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tblprj_Collapsible = Convert.ToBoolean( -1);
         Dvpanel_tblprj_Title = "Itens do Projeto";
         Dvpanel_tblprj_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tblprj_Width = "100%";
         Dvpanel_tblstm_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tblstm_Iconposition = "left";
         Dvpanel_tblstm_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tblstm_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tblstm_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tblstm_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tblstm_Collapsible = Convert.ToBoolean( -1);
         Dvpanel_tblstm_Title = "Itens do Sistema";
         Dvpanel_tblstm_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tblstm_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Projeto Melhoria Itens";
         subGriditnprj_Rows = 0;
         subGriditnstm_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRIDITNSTM_nFirstRecordOnPage',nv:0},{av:'GRIDITNSTM_nEOF',nv:0},{av:'AV15Sdt_ItnStm',fld:'vSDT_ITNSTM',grid:44,pic:'',nv:null},{av:'subGriditnstm_Rows',nv:0},{av:'GRIDITNPRJ_nFirstRecordOnPage',nv:0},{av:'GRIDITNPRJ_nEOF',nv:0},{av:'AV14Sdt_ItnPrj',fld:'vSDT_ITNPRJ',grid:55,pic:'',nv:null},{av:'subGriditnprj_Rows',nv:0}],oparms:[]}");
         setEventMetadata("VSISTEMA_CODIGO.CLICK","{handler:'E11DT2',iparms:[{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV6Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A394FuncaoDados_Ativo',fld:'FUNCAODADOS_ATIVO',pic:'',nv:''},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV13Sdt_Item',fld:'vSDT_ITEM',pic:'',nv:null},{av:'A369FuncaoDados_Nome',fld:'FUNCAODADOS_NOME',pic:'',nv:''},{av:'A377FuncaoDados_PF',fld:'FUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A373FuncaoDados_Tipo',fld:'FUNCAODADOS_TIPO',pic:'',nv:''},{av:'A360FuncaoAPF_SistemaCod',fld:'FUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A183FuncaoAPF_Ativo',fld:'FUNCAOAPF_ATIVO',pic:'',nv:''},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A166FuncaoAPF_Nome',fld:'FUNCAOAPF_NOME',pic:'',nv:''},{av:'A386FuncaoAPF_PF',fld:'FUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A184FuncaoAPF_Tipo',fld:'FUNCAOAPF_TIPO',pic:'',nv:''},{av:'AV15Sdt_ItnStm',fld:'vSDT_ITNSTM',grid:44,pic:'',nv:null},{av:'GRIDITNSTM_nFirstRecordOnPage',nv:0},{av:'GRIDITNSTM_nEOF',nv:0},{av:'subGriditnstm_Rows',nv:0}],oparms:[{av:'AV15Sdt_ItnStm',fld:'vSDT_ITNSTM',grid:44,pic:'',nv:null},{av:'AV13Sdt_Item',fld:'vSDT_ITEM',pic:'',nv:null}]}");
         setEventMetadata("CTLITEMNOME.CLICK","{handler:'E16DT2',iparms:[{av:'GRIDITNSTM_nFirstRecordOnPage',nv:0},{av:'GRIDITNSTM_nEOF',nv:0},{av:'AV15Sdt_ItnStm',fld:'vSDT_ITNSTM',grid:44,pic:'',nv:null},{av:'subGriditnstm_Rows',nv:0},{av:'AV14Sdt_ItnPrj',fld:'vSDT_ITNPRJ',grid:55,pic:'',nv:null},{av:'GRIDITNPRJ_nFirstRecordOnPage',nv:0},{av:'GRIDITNPRJ_nEOF',nv:0},{av:'subGriditnprj_Rows',nv:0}],oparms:[{av:'AV13Sdt_Item',fld:'vSDT_ITEM',pic:'',nv:null},{av:'AV14Sdt_ItnPrj',fld:'vSDT_ITNPRJ',grid:55,pic:'',nv:null},{av:'AV15Sdt_ItnStm',fld:'vSDT_ITNSTM',grid:44,pic:'',nv:null}]}");
         setEventMetadata("PRJITEMNOME.CLICK","{handler:'E13DT2',iparms:[{av:'GRIDITNPRJ_nFirstRecordOnPage',nv:0},{av:'GRIDITNPRJ_nEOF',nv:0},{av:'AV14Sdt_ItnPrj',fld:'vSDT_ITNPRJ',grid:55,pic:'',nv:null},{av:'subGriditnprj_Rows',nv:0},{av:'AV15Sdt_ItnStm',fld:'vSDT_ITNSTM',grid:44,pic:'',nv:null},{av:'GRIDITNSTM_nFirstRecordOnPage',nv:0},{av:'GRIDITNSTM_nEOF',nv:0},{av:'subGriditnstm_Rows',nv:0}],oparms:[{av:'AV13Sdt_Item',fld:'vSDT_ITEM',pic:'',nv:null},{av:'AV15Sdt_ItnStm',fld:'vSDT_ITNSTM',grid:44,pic:'',nv:null},{av:'AV14Sdt_ItnPrj',fld:'vSDT_ITNPRJ',grid:55,pic:'',nv:null}]}");
         setEventMetadata("'CONTINUAR'","{handler:'E12DT2',iparms:[{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A416Sistema_Nome',fld:'SISTEMA_NOME',pic:'@!',nv:''},{av:'AV14Sdt_ItnPrj',fld:'vSDT_ITNPRJ',grid:55,pic:'',nv:null},{av:'AV20Projeto_Codigo',fld:'vPROJETO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV9AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV28ParmProjeto_Sigla',fld:'vPARMPROJETO_SIGLA',pic:'@!',hsh:true,nv:''},{av:'AV23Sistema_FatorAjuste',fld:'vSISTEMA_FATORAJUSTE',pic:'ZZ9.99',nv:0.0},{av:'AV24Sistema_Prazo',fld:'vSISTEMA_PRAZO',pic:'ZZZ9',nv:0},{av:'AV21Sistema_Custo',fld:'vSISTEMA_CUSTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV22Sistema_Esforco',fld:'vSISTEMA_ESFORCO',pic:'ZZZ9',nv:0},{av:'AV25Sistema_Tipo',fld:'vSISTEMA_TIPO',pic:'',nv:''},{av:'AV26Sistema_Tecnica',fld:'vSISTEMA_TECNICA',pic:'',nv:''}],oparms:[{av:'AV26Sistema_Tecnica',fld:'vSISTEMA_TECNICA',pic:'',nv:''},{av:'AV25Sistema_Tipo',fld:'vSISTEMA_TIPO',pic:'',nv:''},{av:'AV22Sistema_Esforco',fld:'vSISTEMA_ESFORCO',pic:'ZZZ9',nv:0},{av:'AV21Sistema_Custo',fld:'vSISTEMA_CUSTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV24Sistema_Prazo',fld:'vSISTEMA_PRAZO',pic:'ZZZ9',nv:0},{av:'AV23Sistema_FatorAjuste',fld:'vSISTEMA_FATORAJUSTE',pic:'ZZ9.99',nv:0.0}]}");
         setEventMetadata("GRIDITNSTM_FIRSTPAGE","{handler:'subgriditnstm_firstpage',iparms:[{av:'GRIDITNSTM_nFirstRecordOnPage',nv:0},{av:'GRIDITNSTM_nEOF',nv:0},{av:'subGriditnstm_Rows',nv:0},{av:'AV15Sdt_ItnStm',fld:'vSDT_ITNSTM',grid:44,pic:'',nv:null}],oparms:[]}");
         setEventMetadata("GRIDITNSTM_PREVPAGE","{handler:'subgriditnstm_previouspage',iparms:[{av:'GRIDITNSTM_nFirstRecordOnPage',nv:0},{av:'GRIDITNSTM_nEOF',nv:0},{av:'subGriditnstm_Rows',nv:0},{av:'AV15Sdt_ItnStm',fld:'vSDT_ITNSTM',grid:44,pic:'',nv:null}],oparms:[]}");
         setEventMetadata("GRIDITNSTM_NEXTPAGE","{handler:'subgriditnstm_nextpage',iparms:[{av:'GRIDITNSTM_nFirstRecordOnPage',nv:0},{av:'GRIDITNSTM_nEOF',nv:0},{av:'subGriditnstm_Rows',nv:0},{av:'AV15Sdt_ItnStm',fld:'vSDT_ITNSTM',grid:44,pic:'',nv:null}],oparms:[]}");
         setEventMetadata("GRIDITNSTM_LASTPAGE","{handler:'subgriditnstm_lastpage',iparms:[{av:'GRIDITNSTM_nFirstRecordOnPage',nv:0},{av:'GRIDITNSTM_nEOF',nv:0},{av:'subGriditnstm_Rows',nv:0},{av:'AV15Sdt_ItnStm',fld:'vSDT_ITNSTM',grid:44,pic:'',nv:null}],oparms:[]}");
         setEventMetadata("GRIDITNPRJ_FIRSTPAGE","{handler:'subgriditnprj_firstpage',iparms:[{av:'GRIDITNPRJ_nFirstRecordOnPage',nv:0},{av:'GRIDITNPRJ_nEOF',nv:0},{av:'subGriditnprj_Rows',nv:0},{av:'AV14Sdt_ItnPrj',fld:'vSDT_ITNPRJ',grid:55,pic:'',nv:null}],oparms:[]}");
         setEventMetadata("GRIDITNPRJ_PREVPAGE","{handler:'subgriditnprj_previouspage',iparms:[{av:'GRIDITNPRJ_nFirstRecordOnPage',nv:0},{av:'GRIDITNPRJ_nEOF',nv:0},{av:'subGriditnprj_Rows',nv:0},{av:'AV14Sdt_ItnPrj',fld:'vSDT_ITNPRJ',grid:55,pic:'',nv:null}],oparms:[]}");
         setEventMetadata("GRIDITNPRJ_NEXTPAGE","{handler:'subgriditnprj_nextpage',iparms:[{av:'GRIDITNPRJ_nFirstRecordOnPage',nv:0},{av:'GRIDITNPRJ_nEOF',nv:0},{av:'subGriditnprj_Rows',nv:0},{av:'AV14Sdt_ItnPrj',fld:'vSDT_ITNPRJ',grid:55,pic:'',nv:null}],oparms:[]}");
         setEventMetadata("GRIDITNPRJ_LASTPAGE","{handler:'subgriditnprj_lastpage',iparms:[{av:'GRIDITNPRJ_nFirstRecordOnPage',nv:0},{av:'GRIDITNPRJ_nEOF',nv:0},{av:'subGriditnprj_Rows',nv:0},{av:'AV14Sdt_ItnPrj',fld:'vSDT_ITNPRJ',grid:55,pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV28ParmProjeto_Sigla = "";
         wcpOAV25Sistema_Tipo = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV14Sdt_ItnPrj = new GxObjectCollection( context, "SDT_ItensConagem.Item", "GxEv3Up14_Meetrika", "SdtSDT_ItensConagem_Item", "GeneXus.Programs");
         AV15Sdt_ItnStm = new GxObjectCollection( context, "SDT_ItensConagem.Item", "GxEv3Up14_Meetrika", "SdtSDT_ItensConagem_Item", "GeneXus.Programs");
         A394FuncaoDados_Ativo = "";
         AV13Sdt_Item = new SdtSDT_ItensConagem_Item(context);
         A369FuncaoDados_Nome = "";
         A373FuncaoDados_Tipo = "";
         A183FuncaoAPF_Ativo = "";
         A166FuncaoAPF_Nome = "";
         A184FuncaoAPF_Tipo = "";
         A416Sistema_Nome = "";
         AV5Projeto_Sigla = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV26Sistema_Tecnica = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00DT2_A127Sistema_Codigo = new int[1] ;
         H00DT2_A416Sistema_Nome = new String[] {""} ;
         H00DT2_A135Sistema_AreaTrabalhoCod = new int[1] ;
         H00DT2_A699Sistema_Tipo = new String[] {""} ;
         H00DT2_n699Sistema_Tipo = new bool[] {false} ;
         GriditnstmContainer = new GXWebGrid( context);
         GriditnprjContainer = new GXWebGrid( context);
         H00DT3_A394FuncaoDados_Ativo = new String[] {""} ;
         H00DT3_A370FuncaoDados_SistemaCod = new int[1] ;
         H00DT3_A369FuncaoDados_Nome = new String[] {""} ;
         H00DT3_A373FuncaoDados_Tipo = new String[] {""} ;
         H00DT3_A755FuncaoDados_Contar = new bool[] {false} ;
         H00DT3_n755FuncaoDados_Contar = new bool[] {false} ;
         H00DT3_A368FuncaoDados_Codigo = new int[1] ;
         H00DT4_A183FuncaoAPF_Ativo = new String[] {""} ;
         H00DT4_A360FuncaoAPF_SistemaCod = new int[1] ;
         H00DT4_n360FuncaoAPF_SistemaCod = new bool[] {false} ;
         H00DT4_A166FuncaoAPF_Nome = new String[] {""} ;
         H00DT4_A184FuncaoAPF_Tipo = new String[] {""} ;
         H00DT4_A165FuncaoAPF_Codigo = new int[1] ;
         H00DT5_A127Sistema_Codigo = new int[1] ;
         H00DT5_A416Sistema_Nome = new String[] {""} ;
         AV19Sistema_Nome = "";
         AV18WebSession = context.GetSession();
         GriditnstmRow = new GXWebRow();
         GriditnprjRow = new GXWebRow();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttContinuar_Jsonclick = "";
         lblTextblock5_Jsonclick = "";
         lblTextblock1_Jsonclick = "";
         lblTextblock2_Jsonclick = "";
         lblTextblock3_Jsonclick = "";
         lblTextblock4_Jsonclick = "";
         subGriditnprj_Linesclass = "";
         GriditnprjColumn = new GXWebColumn();
         subGriditnstm_Linesclass = "";
         GriditnstmColumn = new GXWebColumn();
         lblTextblocksistema_codigo2_Jsonclick = "";
         lblTextblocktitle2_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_projetomelhoriaitens__default(),
            new Object[][] {
                new Object[] {
               H00DT2_A127Sistema_Codigo, H00DT2_A416Sistema_Nome, H00DT2_A135Sistema_AreaTrabalhoCod, H00DT2_A699Sistema_Tipo, H00DT2_n699Sistema_Tipo
               }
               , new Object[] {
               H00DT3_A394FuncaoDados_Ativo, H00DT3_A370FuncaoDados_SistemaCod, H00DT3_A369FuncaoDados_Nome, H00DT3_A373FuncaoDados_Tipo, H00DT3_A755FuncaoDados_Contar, H00DT3_n755FuncaoDados_Contar, H00DT3_A368FuncaoDados_Codigo
               }
               , new Object[] {
               H00DT4_A183FuncaoAPF_Ativo, H00DT4_A360FuncaoAPF_SistemaCod, H00DT4_n360FuncaoAPF_SistemaCod, H00DT4_A166FuncaoAPF_Nome, H00DT4_A184FuncaoAPF_Tipo, H00DT4_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               H00DT5_A127Sistema_Codigo, H00DT5_A416Sistema_Nome
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavProjeto_sigla_Enabled = 0;
         edtavCtlitemcodigo_Enabled = 0;
         edtavCtlitemtipo_Enabled = 0;
         edtavCtlitemnome_Enabled = 0;
         edtavCtlitemtipofn_Enabled = 0;
         edtavPrjitemcodigo_Enabled = 0;
         edtavPrjitemtipo_Enabled = 0;
         edtavPrjitemnome_Enabled = 0;
         edtavPrjitemtipofn_Enabled = 0;
      }

      private short nRcdExists_5 ;
      private short nIsMod_5 ;
      private short nRcdExists_4 ;
      private short nIsMod_4 ;
      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_44 ;
      private short nGXsfl_44_idx=1 ;
      private short nRC_GXsfl_55 ;
      private short nGXsfl_55_idx=1 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRIDITNSTM_nEOF ;
      private short GRIDITNPRJ_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short AV36GXV2 ;
      private short AV31GXV1 ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_44_Refreshing=0 ;
      private short subGriditnstm_Backcolorstyle ;
      private short nGXsfl_55_Refreshing=0 ;
      private short subGriditnprj_Backcolorstyle ;
      private short AV24Sistema_Prazo ;
      private short AV22Sistema_Esforco ;
      private short nGXsfl_44_fel_idx=1 ;
      private short nGXsfl_55_fel_idx=1 ;
      private short GXt_int2 ;
      private short nGXsfl_44_bak_idx=1 ;
      private short AV17i ;
      private short nGXsfl_55_bak_idx=1 ;
      private short subGriditnprj_Titlebackstyle ;
      private short subGriditnprj_Allowselection ;
      private short subGriditnprj_Allowhovering ;
      private short subGriditnprj_Allowcollapsing ;
      private short subGriditnprj_Collapsed ;
      private short subGriditnstm_Titlebackstyle ;
      private short subGriditnstm_Allowselection ;
      private short subGriditnstm_Allowhovering ;
      private short subGriditnstm_Allowcollapsing ;
      private short subGriditnstm_Collapsed ;
      private short nGXWrapped ;
      private short subGriditnstm_Backstyle ;
      private short subGriditnprj_Backstyle ;
      private int AV20Projeto_Codigo ;
      private int AV9AreaTrabalho_Codigo ;
      private int wcpOAV20Projeto_Codigo ;
      private int wcpOAV9AreaTrabalho_Codigo ;
      private int subGriditnstm_Rows ;
      private int subGriditnprj_Rows ;
      private int A370FuncaoDados_SistemaCod ;
      private int A360FuncaoAPF_SistemaCod ;
      private int A127Sistema_Codigo ;
      private int A165FuncaoAPF_Codigo ;
      private int A368FuncaoDados_Codigo ;
      private int gxdynajaxindex ;
      private int AV6Sistema_Codigo ;
      private int subGriditnstm_Islastpage ;
      private int subGriditnprj_Islastpage ;
      private int edtavProjeto_sigla_Enabled ;
      private int edtavCtlitemcodigo_Enabled ;
      private int edtavCtlitemtipo_Enabled ;
      private int edtavCtlitemnome_Enabled ;
      private int edtavCtlitemtipofn_Enabled ;
      private int edtavPrjitemcodigo_Enabled ;
      private int edtavPrjitemtipo_Enabled ;
      private int edtavPrjitemnome_Enabled ;
      private int edtavPrjitemtipofn_Enabled ;
      private int GRIDITNSTM_nGridOutOfScope ;
      private int GRIDITNPRJ_nGridOutOfScope ;
      private int subGriditnprj_Titlebackcolor ;
      private int subGriditnprj_Allbackcolor ;
      private int subGriditnprj_Selectioncolor ;
      private int subGriditnprj_Hoveringcolor ;
      private int subGriditnstm_Titlebackcolor ;
      private int subGriditnstm_Allbackcolor ;
      private int subGriditnstm_Selectioncolor ;
      private int subGriditnstm_Hoveringcolor ;
      private int idxLst ;
      private int subGriditnstm_Backcolor ;
      private int subGriditnprj_Backcolor ;
      private long GRIDITNSTM_nFirstRecordOnPage ;
      private long GRIDITNPRJ_nFirstRecordOnPage ;
      private long GRIDITNSTM_nCurrentRecord ;
      private long GRIDITNPRJ_nCurrentRecord ;
      private long GRIDITNSTM_nRecordCount ;
      private long GRIDITNPRJ_nRecordCount ;
      private decimal A386FuncaoAPF_PF ;
      private decimal A377FuncaoDados_PF ;
      private decimal AV23Sistema_FatorAjuste ;
      private decimal AV21Sistema_Custo ;
      private decimal GXt_decimal1 ;
      private String AV28ParmProjeto_Sigla ;
      private String AV25Sistema_Tipo ;
      private String wcpOAV28ParmProjeto_Sigla ;
      private String wcpOAV25Sistema_Tipo ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_44_idx="0001" ;
      private String GXKey ;
      private String sGXsfl_55_idx="0001" ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A394FuncaoDados_Ativo ;
      private String A373FuncaoDados_Tipo ;
      private String A183FuncaoAPF_Ativo ;
      private String A184FuncaoAPF_Tipo ;
      private String AV5Projeto_Sigla ;
      private String Dvpanel_tblstm_Width ;
      private String Dvpanel_tblstm_Cls ;
      private String Dvpanel_tblstm_Title ;
      private String Dvpanel_tblstm_Iconposition ;
      private String Dvpanel_tblprj_Width ;
      private String Dvpanel_tblprj_Cls ;
      private String Dvpanel_tblprj_Title ;
      private String Dvpanel_tblprj_Iconposition ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String AV26Sistema_Tecnica ;
      private String edtavProjeto_sigla_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String edtavCtlitemcodigo_Internalname ;
      private String edtavCtlitemtipo_Internalname ;
      private String edtavCtlitemnome_Internalname ;
      private String edtavCtlitemtipofn_Internalname ;
      private String edtavPrjitemcodigo_Internalname ;
      private String edtavPrjitemtipo_Internalname ;
      private String edtavPrjitemnome_Internalname ;
      private String edtavPrjitemtipofn_Internalname ;
      private String dynavSistema_codigo_Internalname ;
      private String cmbavSistema_tecnica_Internalname ;
      private String edtavSistema_fatorajuste_Internalname ;
      private String edtavSistema_prazo_Internalname ;
      private String edtavSistema_custo_Internalname ;
      private String edtavSistema_esforco_Internalname ;
      private String sGXsfl_44_fel_idx="0001" ;
      private String sGXsfl_55_fel_idx="0001" ;
      private String sStyleString ;
      private String tblTablemain2_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttContinuar_Internalname ;
      private String bttContinuar_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String lblTextblock5_Internalname ;
      private String lblTextblock5_Jsonclick ;
      private String cmbavSistema_tecnica_Jsonclick ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String edtavSistema_fatorajuste_Jsonclick ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String edtavSistema_prazo_Jsonclick ;
      private String lblTextblock3_Internalname ;
      private String lblTextblock3_Jsonclick ;
      private String edtavSistema_custo_Jsonclick ;
      private String lblTextblock4_Internalname ;
      private String lblTextblock4_Jsonclick ;
      private String edtavSistema_esforco_Jsonclick ;
      private String tblTblprj_Internalname ;
      private String subGriditnprj_Internalname ;
      private String subGriditnprj_Class ;
      private String subGriditnprj_Linesclass ;
      private String tblTblstm_Internalname ;
      private String subGriditnstm_Internalname ;
      private String subGriditnstm_Class ;
      private String subGriditnstm_Linesclass ;
      private String tblTable2_Internalname ;
      private String lblTextblocksistema_codigo2_Internalname ;
      private String lblTextblocksistema_codigo2_Jsonclick ;
      private String dynavSistema_codigo_Jsonclick ;
      private String tblTablemergedtextblocktitle2_Internalname ;
      private String lblTextblocktitle2_Internalname ;
      private String lblTextblocktitle2_Jsonclick ;
      private String edtavProjeto_sigla_Jsonclick ;
      private String ROClassString ;
      private String edtavCtlitemcodigo_Jsonclick ;
      private String edtavCtlitemtipo_Jsonclick ;
      private String edtavCtlitemnome_Jsonclick ;
      private String edtavCtlitemtipofn_Jsonclick ;
      private String edtavPrjitemcodigo_Jsonclick ;
      private String edtavPrjitemtipo_Jsonclick ;
      private String edtavPrjitemnome_Jsonclick ;
      private String edtavPrjitemtipofn_Jsonclick ;
      private String Dvpanel_tblstm_Internalname ;
      private String Dvpanel_tblprj_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool A755FuncaoDados_Contar ;
      private bool Dvpanel_tblstm_Collapsible ;
      private bool Dvpanel_tblstm_Collapsed ;
      private bool Dvpanel_tblstm_Autowidth ;
      private bool Dvpanel_tblstm_Autoheight ;
      private bool Dvpanel_tblstm_Showcollapseicon ;
      private bool Dvpanel_tblstm_Autoscroll ;
      private bool Dvpanel_tblprj_Collapsible ;
      private bool Dvpanel_tblprj_Collapsed ;
      private bool Dvpanel_tblprj_Autowidth ;
      private bool Dvpanel_tblprj_Autoheight ;
      private bool Dvpanel_tblprj_Showcollapseicon ;
      private bool Dvpanel_tblprj_Autoscroll ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_BV44 ;
      private bool n755FuncaoDados_Contar ;
      private bool n360FuncaoAPF_SistemaCod ;
      private bool gx_BV55 ;
      private String A369FuncaoDados_Nome ;
      private String A166FuncaoAPF_Nome ;
      private String A416Sistema_Nome ;
      private String AV19Sistema_Nome ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GriditnstmContainer ;
      private GXWebGrid GriditnprjContainer ;
      private GXWebRow GriditnstmRow ;
      private GXWebRow GriditnprjRow ;
      private GXWebColumn GriditnprjColumn ;
      private GXWebColumn GriditnstmColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavSistema_codigo ;
      private GXCombobox cmbavSistema_tecnica ;
      private IDataStoreProvider pr_default ;
      private int[] H00DT2_A127Sistema_Codigo ;
      private String[] H00DT2_A416Sistema_Nome ;
      private int[] H00DT2_A135Sistema_AreaTrabalhoCod ;
      private String[] H00DT2_A699Sistema_Tipo ;
      private bool[] H00DT2_n699Sistema_Tipo ;
      private String[] H00DT3_A394FuncaoDados_Ativo ;
      private int[] H00DT3_A370FuncaoDados_SistemaCod ;
      private String[] H00DT3_A369FuncaoDados_Nome ;
      private String[] H00DT3_A373FuncaoDados_Tipo ;
      private bool[] H00DT3_A755FuncaoDados_Contar ;
      private bool[] H00DT3_n755FuncaoDados_Contar ;
      private int[] H00DT3_A368FuncaoDados_Codigo ;
      private String[] H00DT4_A183FuncaoAPF_Ativo ;
      private int[] H00DT4_A360FuncaoAPF_SistemaCod ;
      private bool[] H00DT4_n360FuncaoAPF_SistemaCod ;
      private String[] H00DT4_A166FuncaoAPF_Nome ;
      private String[] H00DT4_A184FuncaoAPF_Tipo ;
      private int[] H00DT4_A165FuncaoAPF_Codigo ;
      private int[] H00DT5_A127Sistema_Codigo ;
      private String[] H00DT5_A416Sistema_Nome ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IGxSession AV18WebSession ;
      [ObjectCollection(ItemType=typeof( SdtSDT_ItensConagem_Item ))]
      private IGxCollection AV14Sdt_ItnPrj ;
      [ObjectCollection(ItemType=typeof( SdtSDT_ItensConagem_Item ))]
      private IGxCollection AV15Sdt_ItnStm ;
      private GXWebForm Form ;
      private SdtSDT_ItensConagem_Item AV13Sdt_Item ;
   }

   public class wp_projetomelhoriaitens__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00DT2 ;
          prmH00DT2 = new Object[] {
          new Object[] {"@AV9AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00DT3 ;
          prmH00DT3 = new Object[] {
          new Object[] {"@AV6Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00DT4 ;
          prmH00DT4 = new Object[] {
          new Object[] {"@AV6Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00DT5 ;
          prmH00DT5 = new Object[] {
          new Object[] {"@AV6Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00DT2", "SELECT [Sistema_Codigo], [Sistema_Nome], [Sistema_AreaTrabalhoCod], [Sistema_Tipo] FROM [Sistema] WITH (NOLOCK) WHERE ([Sistema_AreaTrabalhoCod] = @AV9AreaTrabalho_Codigo) AND ([Sistema_Tipo] = 'A') ORDER BY [Sistema_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DT2,0,0,true,false )
             ,new CursorDef("H00DT3", "SELECT [FuncaoDados_Ativo], [FuncaoDados_SistemaCod], [FuncaoDados_Nome], [FuncaoDados_Tipo], [FuncaoDados_Contar], [FuncaoDados_Codigo] FROM [FuncaoDados] WITH (NOLOCK) WHERE ([FuncaoDados_SistemaCod] = @AV6Sistema_Codigo) AND ([FuncaoDados_Ativo] = 'A') ORDER BY [FuncaoDados_SistemaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DT3,100,0,true,false )
             ,new CursorDef("H00DT4", "SELECT [FuncaoAPF_Ativo], [FuncaoAPF_SistemaCod], [FuncaoAPF_Nome], [FuncaoAPF_Tipo], [FuncaoAPF_Codigo] FROM [FuncoesAPF] WITH (NOLOCK) WHERE ([FuncaoAPF_SistemaCod] = @AV6Sistema_Codigo) AND ([FuncaoAPF_Ativo] = 'A') ORDER BY [FuncaoAPF_SistemaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DT4,100,0,true,false )
             ,new CursorDef("H00DT5", "SELECT TOP 1 [Sistema_Codigo], [Sistema_Nome] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @AV6Sistema_Codigo ORDER BY [Sistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DT5,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 3) ;
                ((bool[]) buf[4])[0] = rslt.getBool(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 3) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
