/*
               File: PRC_SSPrestadoraUnica
        Description: Prestadora Unica
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:49.55
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_ssprestadoraunica : GXProcedure
   {
      public prc_ssprestadoraunica( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_ssprestadoraunica( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Contratante_Codigo ,
                           out bool aP1_SSPrestadoraUnica )
      {
         this.A29Contratante_Codigo = aP0_Contratante_Codigo;
         this.AV9SSPrestadoraUnica = false ;
         initialize();
         executePrivate();
         aP0_Contratante_Codigo=this.A29Contratante_Codigo;
         aP1_SSPrestadoraUnica=this.AV9SSPrestadoraUnica;
      }

      public bool executeUdp( ref int aP0_Contratante_Codigo )
      {
         this.A29Contratante_Codigo = aP0_Contratante_Codigo;
         this.AV9SSPrestadoraUnica = false ;
         initialize();
         executePrivate();
         aP0_Contratante_Codigo=this.A29Contratante_Codigo;
         aP1_SSPrestadoraUnica=this.AV9SSPrestadoraUnica;
         return AV9SSPrestadoraUnica ;
      }

      public void executeSubmit( ref int aP0_Contratante_Codigo ,
                                 out bool aP1_SSPrestadoraUnica )
      {
         prc_ssprestadoraunica objprc_ssprestadoraunica;
         objprc_ssprestadoraunica = new prc_ssprestadoraunica();
         objprc_ssprestadoraunica.A29Contratante_Codigo = aP0_Contratante_Codigo;
         objprc_ssprestadoraunica.AV9SSPrestadoraUnica = false ;
         objprc_ssprestadoraunica.context.SetSubmitInitialConfig(context);
         objprc_ssprestadoraunica.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_ssprestadoraunica);
         aP0_Contratante_Codigo=this.A29Contratante_Codigo;
         aP1_SSPrestadoraUnica=this.AV9SSPrestadoraUnica;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_ssprestadoraunica)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00CG2 */
         pr_default.execute(0, new Object[] {A29Contratante_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1733Contratante_SSPrestadoraUnica = P00CG2_A1733Contratante_SSPrestadoraUnica[0];
            n1733Contratante_SSPrestadoraUnica = P00CG2_n1733Contratante_SSPrestadoraUnica[0];
            AV9SSPrestadoraUnica = A1733Contratante_SSPrestadoraUnica;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00CG2_A29Contratante_Codigo = new int[1] ;
         P00CG2_A1733Contratante_SSPrestadoraUnica = new bool[] {false} ;
         P00CG2_n1733Contratante_SSPrestadoraUnica = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_ssprestadoraunica__default(),
            new Object[][] {
                new Object[] {
               P00CG2_A29Contratante_Codigo, P00CG2_A1733Contratante_SSPrestadoraUnica, P00CG2_n1733Contratante_SSPrestadoraUnica
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A29Contratante_Codigo ;
      private String scmdbuf ;
      private bool AV9SSPrestadoraUnica ;
      private bool A1733Contratante_SSPrestadoraUnica ;
      private bool n1733Contratante_SSPrestadoraUnica ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Contratante_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00CG2_A29Contratante_Codigo ;
      private bool[] P00CG2_A1733Contratante_SSPrestadoraUnica ;
      private bool[] P00CG2_n1733Contratante_SSPrestadoraUnica ;
      private bool aP1_SSPrestadoraUnica ;
   }

   public class prc_ssprestadoraunica__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00CG2 ;
          prmP00CG2 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00CG2", "SELECT TOP 1 [Contratante_Codigo], [Contratante_SSPrestadoraUnica] FROM [Contratante] WITH (NOLOCK) WHERE [Contratante_Codigo] = @Contratante_Codigo ORDER BY [Contratante_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00CG2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
