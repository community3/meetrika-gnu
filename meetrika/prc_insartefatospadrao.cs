/*
               File: PRC_InsArtefatosPadrao
        Description: Inserir Artefatos Padrao
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:1.41
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_insartefatospadrao : GXProcedure
   {
      public prc_insartefatospadrao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_insartefatospadrao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContratoServicos_Codigo ,
                           ref int aP1_Servico_Codigo )
      {
         this.AV8ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         this.A155Servico_Codigo = aP1_Servico_Codigo;
         initialize();
         executePrivate();
         aP0_ContratoServicos_Codigo=this.AV8ContratoServicos_Codigo;
         aP1_Servico_Codigo=this.A155Servico_Codigo;
      }

      public int executeUdp( ref int aP0_ContratoServicos_Codigo )
      {
         this.AV8ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         this.A155Servico_Codigo = aP1_Servico_Codigo;
         initialize();
         executePrivate();
         aP0_ContratoServicos_Codigo=this.AV8ContratoServicos_Codigo;
         aP1_Servico_Codigo=this.A155Servico_Codigo;
         return A155Servico_Codigo ;
      }

      public void executeSubmit( ref int aP0_ContratoServicos_Codigo ,
                                 ref int aP1_Servico_Codigo )
      {
         prc_insartefatospadrao objprc_insartefatospadrao;
         objprc_insartefatospadrao = new prc_insartefatospadrao();
         objprc_insartefatospadrao.AV8ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         objprc_insartefatospadrao.A155Servico_Codigo = aP1_Servico_Codigo;
         objprc_insartefatospadrao.context.SetSubmitInitialConfig(context);
         objprc_insartefatospadrao.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_insartefatospadrao);
         aP0_ContratoServicos_Codigo=this.AV8ContratoServicos_Codigo;
         aP1_Servico_Codigo=this.A155Servico_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_insartefatospadrao)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00CV2 */
         pr_default.execute(0, new Object[] {A155Servico_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1766ServicoArtefato_ArtefatoCod = P00CV2_A1766ServicoArtefato_ArtefatoCod[0];
            /*
               INSERT RECORD ON TABLE ContratoServicosArtefatos

            */
            W1749Artefatos_Codigo = A1749Artefatos_Codigo;
            A160ContratoServicos_Codigo = AV8ContratoServicos_Codigo;
            A1749Artefatos_Codigo = A1766ServicoArtefato_ArtefatoCod;
            /* Using cursor P00CV3 */
            pr_default.execute(1, new Object[] {A160ContratoServicos_Codigo, A1749Artefatos_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosArtefatos") ;
            if ( (pr_default.getStatus(1) == 1) )
            {
               context.Gx_err = 1;
               Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
            }
            else
            {
               context.Gx_err = 0;
               Gx_emsg = "";
            }
            A1749Artefatos_Codigo = W1749Artefatos_Codigo;
            /* End Insert */
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_InsArtefatosPadrao");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00CV2_A155Servico_Codigo = new int[1] ;
         P00CV2_A1766ServicoArtefato_ArtefatoCod = new int[1] ;
         Gx_emsg = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_insartefatospadrao__default(),
            new Object[][] {
                new Object[] {
               P00CV2_A155Servico_Codigo, P00CV2_A1766ServicoArtefato_ArtefatoCod
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8ContratoServicos_Codigo ;
      private int A155Servico_Codigo ;
      private int A1766ServicoArtefato_ArtefatoCod ;
      private int GX_INS194 ;
      private int W1749Artefatos_Codigo ;
      private int A1749Artefatos_Codigo ;
      private int A160ContratoServicos_Codigo ;
      private String scmdbuf ;
      private String Gx_emsg ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContratoServicos_Codigo ;
      private int aP1_Servico_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00CV2_A155Servico_Codigo ;
      private int[] P00CV2_A1766ServicoArtefato_ArtefatoCod ;
   }

   public class prc_insartefatospadrao__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00CV2 ;
          prmP00CV2 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00CV3 ;
          prmP00CV3 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00CV2", "SELECT [Servico_Codigo], [ServicoArtefato_ArtefatoCod] FROM [ServicoArtefatos] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ORDER BY [Servico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00CV2,100,0,true,false )
             ,new CursorDef("P00CV3", "INSERT INTO [ContratoServicosArtefatos]([ContratoServicos_Codigo], [Artefatos_Codigo], [ContratoServicosArtefato_Obrigatorio]) VALUES(@ContratoServicos_Codigo, @Artefatos_Codigo, convert(bit, 0))", GxErrorMask.GX_NOMASK,prmP00CV3)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
