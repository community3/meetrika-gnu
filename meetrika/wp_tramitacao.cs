/*
               File: WP_Tramitacao
        Description: Tramitac�o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 22:6:13.68
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_tramitacao : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public wp_tramitacao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_tramitacao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Codigo ,
                           bool aP1_PodeAtribuirDmn ,
                           bool aP2_ParaContratante ,
                           ref String aP3_Destino_Nome )
      {
         this.AV9Codigo = aP0_Codigo;
         this.AV11PodeAtribuirDmn = aP1_PodeAtribuirDmn;
         this.AV38ParaContratante = aP2_ParaContratante;
         this.AV10Destino_Nome = aP3_Destino_Nome;
         executePrivate();
         aP3_Destino_Nome=this.AV10Destino_Nome;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavUsuario_codigo = new GXCombobox();
         dynavContratofs = new GXCombobox();
         cmbavServicogrupo_codigo = new GXCombobox();
         cmbavNewcntsrvcod = new GXCombobox();
         cmbavCriarospara_codigo = new GXCombobox();
         cmbavContrato = new GXCombobox();
         dynavNaoconformidade_codigo = new GXCombobox();
         radavDeferido = new GXRadio();
         cmbavStatuscontagem = new GXCombobox();
         chkavRemetenteinterno = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTRATOFS") == 0 )
            {
               AV5Usuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Usuario_Codigo), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTRATOFSIZ2( AV5Usuario_Codigo) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vNAOCONFORMIDADE_CODIGO") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV53WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvNAOCONFORMIDADE_CODIGOIZ2( AV53WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV9Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV11PodeAtribuirDmn = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11PodeAtribuirDmn", AV11PodeAtribuirDmn);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPODEATRIBUIRDMN", GetSecureSignedToken( "", AV11PodeAtribuirDmn));
                  AV38ParaContratante = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ParaContratante", AV38ParaContratante);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPARACONTRATANTE", GetSecureSignedToken( "", AV38ParaContratante));
                  AV10Destino_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Destino_Nome", AV10Destino_Nome);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAIZ2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV191Pgmname = "WP_Tramitacao";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV191Pgmname", AV191Pgmname);
               context.Gx_err = 0;
               edtavDescricao_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDescricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDescricao_Enabled), 5, 0)));
               edtavPfb_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfb_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfb_Enabled), 5, 0)));
               edtavPfl_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfl_Enabled), 5, 0)));
               edtavPfbof_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbof_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbof_Enabled), 5, 0)));
               edtavPflof_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPflof_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPflof_Enabled), 5, 0)));
               cmbavStatuscontagem.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavStatuscontagem_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavStatuscontagem.Enabled), 5, 0)));
               edtavPfbfmultima_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbfmultima_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfmultima_Enabled), 5, 0)));
               edtavPfbfsultima_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbfsultima_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfsultima_Enabled), 5, 0)));
               edtavPflfmultima_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPflfmultima_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPflfmultima_Enabled), 5, 0)));
               edtavPflfsultima_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPflfsultima_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPflfsultima_Enabled), 5, 0)));
               edtavOldpfb_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOldpfb_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOldpfb_Enabled), 5, 0)));
               edtavOldpfl_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOldpfl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOldpfl_Enabled), 5, 0)));
               WSIZ2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEIZ2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203262261446");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_tramitacao.aspx") + "?" + UrlEncode("" +AV9Codigo) + "," + UrlEncode(StringUtil.BoolToStr(AV11PodeAtribuirDmn)) + "," + UrlEncode(StringUtil.BoolToStr(AV38ParaContratante)) + "," + UrlEncode(StringUtil.RTrim(AV10Destino_Nome))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV191Pgmname));
         GxWebStd.gx_boolean_hidden_field( context, "vPODEATRIBUIRDMN", AV11PodeAtribuirDmn);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_ARTEFATOS", AV145Sdt_Artefatos);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_ARTEFATOS", AV145Sdt_Artefatos);
         }
         GxWebStd.gx_hidden_field( context, "vPRESTADORA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV27Prestadora_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vSTATUSDEMANDA", StringUtil.RTrim( AV16StatusDemanda));
         GxWebStd.gx_hidden_field( context, "vUSERID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12UserId), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCALLER", StringUtil.RTrim( AV114Caller));
         GxWebStd.gx_boolean_hidden_field( context, "vEMDIVERGENCIA", AV97EmDivergencia);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSELECIONADAS", AV20Selecionadas);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSELECIONADAS", AV20Selecionadas);
         }
         GxWebStd.gx_hidden_field( context, "vORIGEM_SIGLA", StringUtil.RTrim( AV110Origem_Sigla));
         GxWebStd.gx_boolean_hidden_field( context, "vUSUARIODAPRESTADORA", AV61UsuarioDaPrestadora);
         GxWebStd.gx_boolean_hidden_field( context, "vUSEREHCONTRATANTE", AV8UserEhContratante);
         GxWebStd.gx_boolean_hidden_field( context, "vEHSOLICITACAOSS", AV154EhSolicitacaoSS);
         GxWebStd.gx_hidden_field( context, "vPRAZOINICIAL", context.localUtil.TToC( AV50PrazoInicial, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "vTIPODIAS", StringUtil.RTrim( AV107TipoDias));
         GxWebStd.gx_hidden_field( context, "CONTRATOGESTOR_CONTRATADAAREACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOGESTOR_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATO_ATIVO", A92Contrato_Ativo);
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATADA_ATIVO", A43Contratada_Ativo);
         GxWebStd.gx_hidden_field( context, "CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOGESTOR_CONTRATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1078ContratoGestor_ContratoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICOGRUPO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A157ServicoGrupo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATOSERVICOS_ATIVO", A638ContratoServicos_Ativo);
         GxWebStd.gx_boolean_hidden_field( context, "SERVICO_ATIVO", A632Servico_Ativo);
         GxWebStd.gx_hidden_field( context, "SERVICOGRUPO_DESCRICAO", A158ServicoGrupo_Descricao);
         GxWebStd.gx_hidden_field( context, "CONTRATO_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A75Contrato_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOAUXILIAR_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1825ContratoAuxiliar_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOAUXILIAR_CONTRATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1824ContratoAuxiliar_ContratoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_ALIAS", StringUtil.RTrim( A1858ContratoServicos_Alias));
         GxWebStd.gx_hidden_field( context, "vCODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATOSERVICOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV116ContratoServicos_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vREQUISITOS", AV179Requisitos);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vREQUISITOS", AV179Requisitos);
         }
         GxWebStd.gx_hidden_field( context, "vDESTINO_NOME", StringUtil.RTrim( AV10Destino_Nome));
         GxWebStd.gx_hidden_field( context, "vATRIBUIDOA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15AtribuidoA_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "LOGRESPONSAVEL_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1797LogResponsavel_Codigo), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "USUARIO_DEFERIAS", A1908Usuario_DeFerias);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CNTCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1603ContagemResultado_CntCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPRAZOENTREGA", context.localUtil.TToC( AV45PrazoEntrega, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "vNOVOSTATUS", StringUtil.RTrim( AV43NovoStatus));
         GxWebStd.gx_hidden_field( context, "vPRAZOANTERIOR", context.localUtil.DToC( AV51PrazoAnterior, 0, "/"));
         GxWebStd.gx_hidden_field( context, "vCONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vDIASANALISE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV93DiasAnalise), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPRAZOEXECUCAO", context.localUtil.TToC( AV18PrazoExecucao, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "vPRAZOATUAL", context.localUtil.TToC( AV186PrazoAtual, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "vPRAZOANALISE", context.localUtil.TToC( AV75PrazoAnalise, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "vPRAZOINICIALDIAS", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV73PrazoInicialDias), 4, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "vOSENTREGUE", AV184OSEntregue);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOREQUISITO_OSCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2003ContagemResultadoRequisito_OSCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOREQUISITO_REQCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2004ContagemResultadoRequisito_ReqCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vOSVINCULADA", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV59OsVinculada), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vSTATUSDEMANDAVNC", StringUtil.RTrim( AV98StatusDemandaVnc));
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATOGESTOR_USUARIOEHCONTRATANTE", A1135ContratoGestor_UsuarioEhContratante);
         GxWebStd.gx_hidden_field( context, "vCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV165Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vRESPONSAVEL_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV65Responsavel_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "LOGRESPONSAVEL_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A891LogResponsavel_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATADA_USAOSISTEMA", A1481Contratada_UsaOSistema);
         GxWebStd.gx_boolean_hidden_field( context, "LOGRESPONSAVEL_USUARIOEHCONTRATANTE", A1148LogResponsavel_UsuarioEhContratante);
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV187ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_PREPOSTOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1013Contrato_PrepostoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vSERVICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22Servico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATOORIGEM_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV115ContratoOrigem_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATADAORIGEM_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV66ContratadaOrigem_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "LOGRESPONSAVEL_ACAO", StringUtil.RTrim( A894LogResponsavel_Acao));
         GxWebStd.gx_boolean_hidden_field( context, "vUSAOSISTEMA", AV166UsaOSistema);
         GxWebStd.gx_boolean_hidden_field( context, "vOK", AV113Ok);
         GxWebStd.gx_boolean_hidden_field( context, "vNOVACONTAGEM", AV69NovaContagem);
         GxWebStd.gx_boolean_hidden_field( context, "vEMREUNIAO", AV72EmReuniao);
         GxWebStd.gx_hidden_field( context, "vRESULTADO", StringUtil.RTrim( AV68Resultado));
         GxWebStd.gx_hidden_field( context, "vSELECIONADA", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21Selecionada), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CNTPRPCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1604ContagemResultado_CntPrpCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_SERVICO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A601ContagemResultado_Servico), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_STATUSDMN", StringUtil.RTrim( A484ContagemResultado_StatusDmn));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DATADMN", context.localUtil.DToC( A471ContagemResultado_DataDmn, 0, "/"));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PRAZOINICIALDIAS", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1227ContagemResultado_PrazoInicialDias), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PRAZOMAISDIAS", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1237ContagemResultado_PrazoMaisDias), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_HORAENTREGA", context.localUtil.TToC( A912ContagemResultado_HoraEntrega, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DATAENTREGA", context.localUtil.DToC( A472ContagemResultado_DataEntrega, 0, "/"));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DATAPREVISTA", context.localUtil.TToC( A1351ContagemResultado_DataPrevista, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PRZTPDIAS", StringUtil.RTrim( A1611ContagemResultado_PrzTpDias));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFBFSULTIMA", StringUtil.LTrim( StringUtil.NToC( A684ContagemResultado_PFBFSUltima, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_STATUSULTCNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(A531ContagemResultado_StatusUltCnt), 2, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFBFSIMP", StringUtil.LTrim( StringUtil.NToC( A798ContagemResultado_PFBFSImp, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "LOGRESPONSAVEL_STATUS", StringUtil.RTrim( A1130LogResponsavel_Status));
         GxWebStd.gx_hidden_field( context, "LOGRESPONSAVEL_NOVOSTATUS", StringUtil.RTrim( A1234LogResponsavel_NovoStatus));
         GxWebStd.gx_hidden_field( context, "LOGRESPONSAVEL_PRAZO", context.localUtil.TToC( A1177LogResponsavel_Prazo, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "vUNIDADES", StringUtil.LTrim( StringUtil.NToC( AV29Unidades, 14, 5, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "vANALISEIMPLICITO", AV74AnaliseImplicito);
         GxWebStd.gx_boolean_hidden_field( context, "vEXECUCAOIMPLICITA", AV94ExecucaoImplicita);
         GxWebStd.gx_hidden_field( context, "vHORAENTREGA", context.localUtil.TToC( AV102HoraEntrega, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "vAREATRABALHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31AreaTrabalho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PRZRSP", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1618ContagemResultado_PrzRsp), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_PESSOANOM", StringUtil.RTrim( A41Contratada_PessoaNom));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_CNTPADRAO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1953Contratada_CntPadrao), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_NUMERO", StringUtil.RTrim( A77Contrato_Numero));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A160ContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "ARTEFATOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1749Artefatos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "ARTEFATOS_DESCRICAO", A1751Artefatos_Descricao);
         GxWebStd.gx_boolean_hidden_field( context, "vPARACONTRATANTE", AV38ParaContratante);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV53WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV53WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "LOGRESPONSAVEL_DEMANDACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A892LogResponsavel_DemandaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "LOGRESPONSAVEL_OWNER", StringUtil.LTrim( StringUtil.NToC( (decimal)(A896LogResponsavel_Owner), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "LOGRESPONSAVEL_OWNEREHCONTRATANTE", A1149LogResponsavel_OwnerEhContratante);
         GxWebStd.gx_hidden_field( context, "CONTRATO_DATAVIGENCIATERMINO", context.localUtil.DToC( A83Contrato_DataVigenciaTermino, 0, "/"));
         GxWebStd.gx_hidden_field( context, "CONTRATO_DATAFIMTA", context.localUtil.DToC( A843Contrato_DataFimTA, 0, "/"));
         GxWebStd.gx_hidden_field( context, "CONTRATO_DATATERMINO", context.localUtil.DToC( A1869Contrato_DataTermino, 0, "/"));
         GxWebStd.gx_hidden_field( context, "gxhash_vDESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV140Descricao, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFB", GetSecureSignedToken( "", context.localUtil.Format( AV57PFB, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFL", GetSecureSignedToken( "", context.localUtil.Format( AV58PFL, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFBOF", GetSecureSignedToken( "", context.localUtil.Format( AV70PFBOF, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFLOF", GetSecureSignedToken( "", context.localUtil.Format( AV71PFLOF, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vSTATUSCONTAGEM", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV42StatusContagem), "Z9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFBFMULTIMA", GetSecureSignedToken( "", context.localUtil.Format( AV81PFBFMUltima, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFBFSULTIMA", GetSecureSignedToken( "", context.localUtil.Format( AV83PFBFSUltima, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFLFMULTIMA", GetSecureSignedToken( "", context.localUtil.Format( AV82PFLFMUltima, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFLFSULTIMA", GetSecureSignedToken( "", context.localUtil.Format( AV80PFLFSUltima, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vDESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV140Descricao, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFB", GetSecureSignedToken( "", context.localUtil.Format( AV57PFB, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFL", GetSecureSignedToken( "", context.localUtil.Format( AV58PFL, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFBOF", GetSecureSignedToken( "", context.localUtil.Format( AV70PFBOF, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFLOF", GetSecureSignedToken( "", context.localUtil.Format( AV71PFLOF, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vSTATUSCONTAGEM", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV42StatusContagem), "Z9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFBFMULTIMA", GetSecureSignedToken( "", context.localUtil.Format( AV81PFBFMUltima, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFBFSULTIMA", GetSecureSignedToken( "", context.localUtil.Format( AV83PFBFSUltima, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFLFMULTIMA", GetSecureSignedToken( "", context.localUtil.Format( AV82PFLFMUltima, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFLFSULTIMA", GetSecureSignedToken( "", context.localUtil.Format( AV80PFLFSUltima, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vDESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV140Descricao, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFB", GetSecureSignedToken( "", context.localUtil.Format( AV57PFB, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFL", GetSecureSignedToken( "", context.localUtil.Format( AV58PFL, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFBOF", GetSecureSignedToken( "", context.localUtil.Format( AV70PFBOF, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFLOF", GetSecureSignedToken( "", context.localUtil.Format( AV71PFLOF, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vSTATUSCONTAGEM", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV42StatusContagem), "Z9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFBFMULTIMA", GetSecureSignedToken( "", context.localUtil.Format( AV81PFBFMUltima, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFBFSULTIMA", GetSecureSignedToken( "", context.localUtil.Format( AV83PFBFSUltima, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFLFMULTIMA", GetSecureSignedToken( "", context.localUtil.Format( AV82PFLFMUltima, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFLFSULTIMA", GetSecureSignedToken( "", context.localUtil.Format( AV80PFLFSUltima, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vDESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV140Descricao, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFB", GetSecureSignedToken( "", context.localUtil.Format( AV57PFB, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFL", GetSecureSignedToken( "", context.localUtil.Format( AV58PFL, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFBOF", GetSecureSignedToken( "", context.localUtil.Format( AV70PFBOF, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFLOF", GetSecureSignedToken( "", context.localUtil.Format( AV71PFLOF, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vSTATUSCONTAGEM", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV42StatusContagem), "Z9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFBFMULTIMA", GetSecureSignedToken( "", context.localUtil.Format( AV81PFBFMUltima, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFBFSULTIMA", GetSecureSignedToken( "", context.localUtil.Format( AV83PFBFSUltima, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFLFMULTIMA", GetSecureSignedToken( "", context.localUtil.Format( AV82PFLFMUltima, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFLFSULTIMA", GetSecureSignedToken( "", context.localUtil.Format( AV80PFLFSUltima, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPODEATRIBUIRDMN", GetSecureSignedToken( "", AV11PodeAtribuirDmn));
         GxWebStd.gx_hidden_field( context, "gxhash_vPARACONTRATANTE", GetSecureSignedToken( "", AV38ParaContratante));
         GxWebStd.gx_hidden_field( context, "gxhash_vPODEATRIBUIRDMN", GetSecureSignedToken( "", AV11PodeAtribuirDmn));
         GxWebStd.gx_hidden_field( context, "gxhash_vPARACONTRATANTE", GetSecureSignedToken( "", AV38ParaContratante));
         GxWebStd.gx_hidden_field( context, "vUSUARIO_CODIGO_Text", StringUtil.RTrim( cmbavUsuario_codigo.Description));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "WP_Tramitacao";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( AV140Descricao, ""));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV57PFB, "ZZ,ZZZ,ZZ9.999");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV58PFL, "ZZ,ZZZ,ZZ9.999");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV70PFBOF, "ZZ,ZZZ,ZZ9.999");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV71PFLOF, "ZZ,ZZZ,ZZ9.999");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV81PFBFMUltima, "ZZ,ZZZ,ZZ9.999");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV83PFBFSUltima, "ZZ,ZZZ,ZZ9.999");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV82PFLFMUltima, "ZZ,ZZZ,ZZ9.999");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV80PFLFSUltima, "ZZ,ZZZ,ZZ9.999");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("wp_tramitacao:[SendSecurityCheck value for]"+"Descricao:"+StringUtil.RTrim( context.localUtil.Format( AV140Descricao, "")));
         GXUtil.WriteLog("wp_tramitacao:[SendSecurityCheck value for]"+"PFB:"+context.localUtil.Format( AV57PFB, "ZZ,ZZZ,ZZ9.999"));
         GXUtil.WriteLog("wp_tramitacao:[SendSecurityCheck value for]"+"PFL:"+context.localUtil.Format( AV58PFL, "ZZ,ZZZ,ZZ9.999"));
         GXUtil.WriteLog("wp_tramitacao:[SendSecurityCheck value for]"+"PFBOF:"+context.localUtil.Format( AV70PFBOF, "ZZ,ZZZ,ZZ9.999"));
         GXUtil.WriteLog("wp_tramitacao:[SendSecurityCheck value for]"+"PFLOF:"+context.localUtil.Format( AV71PFLOF, "ZZ,ZZZ,ZZ9.999"));
         GXUtil.WriteLog("wp_tramitacao:[SendSecurityCheck value for]"+"PFBFMUltima:"+context.localUtil.Format( AV81PFBFMUltima, "ZZ,ZZZ,ZZ9.999"));
         GXUtil.WriteLog("wp_tramitacao:[SendSecurityCheck value for]"+"PFBFSUltima:"+context.localUtil.Format( AV83PFBFSUltima, "ZZ,ZZZ,ZZ9.999"));
         GXUtil.WriteLog("wp_tramitacao:[SendSecurityCheck value for]"+"PFLFMUltima:"+context.localUtil.Format( AV82PFLFMUltima, "ZZ,ZZZ,ZZ9.999"));
         GXUtil.WriteLog("wp_tramitacao:[SendSecurityCheck value for]"+"PFLFSUltima:"+context.localUtil.Format( AV80PFLFSUltima, "ZZ,ZZZ,ZZ9.999"));
      }

      protected void RenderHtmlCloseFormIZ2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         if ( ! ( WebComp_Webcomp2 == null ) )
         {
            WebComp_Webcomp2.componentjscripts();
         }
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "WP_Tramitacao" ;
      }

      public override String GetPgmdesc( )
      {
         return "Tramitac�o" ;
      }

      protected void WBIZ0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            wb_table1_2_IZ2( true) ;
         }
         else
         {
            wb_table1_2_IZ2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_IZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table2_86_IZ2( true) ;
         }
         else
         {
            wb_table2_86_IZ2( false) ;
         }
         return  ;
      }

      protected void wb_table2_86_IZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_WP_Tramitacao.htm");
         }
         wbLoad = true;
      }

      protected void STARTIZ2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Tramitac�o", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPIZ0( ) ;
      }

      protected void WSIZ2( )
      {
         STARTIZ2( ) ;
         EVTIZ2( ) ;
      }

      protected void EVTIZ2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11IZ2 */
                           E11IZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12IZ2 */
                           E12IZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VUSUARIO_CODIGO.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13IZ2 */
                           E13IZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( ! wbErr )
                           {
                              Rfr0gs = false;
                              if ( ! Rfr0gs )
                              {
                                 /* Execute user event: E14IZ2 */
                                 E14IZ2 ();
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VPRAZORESPOSTA.ISVALID") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15IZ2 */
                           E15IZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VSERVICOGRUPO_CODIGO.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16IZ2 */
                           E16IZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VNEWCNTSRVCOD.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17IZ2 */
                           E17IZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOARTEFATOS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18IZ2 */
                           E18IZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VCRIAROSPARA_CODIGO.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19IZ2 */
                           E19IZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VCONTRATO.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20IZ2 */
                           E20IZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E21IZ2 */
                           E21IZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E22IZ2 */
                           E22IZ2 ();
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  else if ( StringUtil.StrCmp(sEvtType, "W") == 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 4);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-4));
                     nCmpId = (short)(NumberUtil.Val( sEvtType, "."));
                     if ( nCmpId == 77 )
                     {
                        WebComp_Webcomp2 = getWebComponent(GetType(), "GeneXus.Programs", "wc_contagemresultadoevidencias", new Object[] {context} );
                        WebComp_Webcomp2.ComponentInit();
                        WebComp_Webcomp2.Name = "WC_ContagemResultadoEvidencias";
                        WebComp_Webcomp2_Component = "WC_ContagemResultadoEvidencias";
                        WebComp_Webcomp2.componentprocess("W0077", "", sEvt);
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEIZ2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormIZ2( ) ;
            }
         }
      }

      protected void PAIZ2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavUsuario_codigo.Name = "vUSUARIO_CODIGO";
            cmbavUsuario_codigo.WebTags = "";
            if ( cmbavUsuario_codigo.ItemCount > 0 )
            {
               AV5Usuario_Codigo = (int)(NumberUtil.Val( cmbavUsuario_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV5Usuario_Codigo), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Usuario_Codigo), 6, 0)));
            }
            dynavContratofs.Name = "vCONTRATOFS";
            dynavContratofs.WebTags = "";
            cmbavServicogrupo_codigo.Name = "vSERVICOGRUPO_CODIGO";
            cmbavServicogrupo_codigo.WebTags = "";
            cmbavServicogrupo_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "(Nenhum)", 0);
            if ( cmbavServicogrupo_codigo.ItemCount > 0 )
            {
               AV177ServicoGrupo_Codigo = (int)(NumberUtil.Val( cmbavServicogrupo_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV177ServicoGrupo_Codigo), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV177ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV177ServicoGrupo_Codigo), 6, 0)));
            }
            cmbavNewcntsrvcod.Name = "vNEWCNTSRVCOD";
            cmbavNewcntsrvcod.WebTags = "";
            cmbavNewcntsrvcod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "(Nenhum)", 0);
            if ( cmbavNewcntsrvcod.ItemCount > 0 )
            {
               AV182NewCntSrvCod = (int)(NumberUtil.Val( cmbavNewcntsrvcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV182NewCntSrvCod), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV182NewCntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV182NewCntSrvCod), 6, 0)));
            }
            cmbavCriarospara_codigo.Name = "vCRIAROSPARA_CODIGO";
            cmbavCriarospara_codigo.WebTags = "";
            cmbavCriarospara_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "(Nenhuma)", 0);
            if ( cmbavCriarospara_codigo.ItemCount > 0 )
            {
               AV168CriarOSPara_Codigo = (int)(NumberUtil.Val( cmbavCriarospara_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV168CriarOSPara_Codigo), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV168CriarOSPara_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV168CriarOSPara_Codigo), 6, 0)));
            }
            cmbavContrato.Name = "vCONTRATO";
            cmbavContrato.WebTags = "";
            cmbavContrato.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "(Nenhum)", 0);
            if ( cmbavContrato.ItemCount > 0 )
            {
               AV174Contrato = (int)(NumberUtil.Val( cmbavContrato.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV174Contrato), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV174Contrato", StringUtil.LTrim( StringUtil.Str( (decimal)(AV174Contrato), 6, 0)));
            }
            dynavNaoconformidade_codigo.Name = "vNAOCONFORMIDADE_CODIGO";
            dynavNaoconformidade_codigo.WebTags = "";
            radavDeferido.Name = "vDEFERIDO";
            radavDeferido.WebTags = "";
            radavDeferido.addItem("R", "Recalcular prazo de entrega ap�s o retorno (Padr�o)", 0);
            radavDeferido.addItem("E", "Extender prazo de entrega (Deferido)", 0);
            radavDeferido.addItem("M", "Manter o prazo de entrega inicial (Indeferido)", 0);
            cmbavStatuscontagem.Name = "vSTATUSCONTAGEM";
            cmbavStatuscontagem.WebTags = "";
            cmbavStatuscontagem.addItem("1", "Inicial", 0);
            cmbavStatuscontagem.addItem("2", "Auditoria", 0);
            cmbavStatuscontagem.addItem("3", "Negociac�o", 0);
            cmbavStatuscontagem.addItem("4", "N�o Aprovada", 0);
            cmbavStatuscontagem.addItem("5", "Aprovada", 0);
            cmbavStatuscontagem.addItem("6", "Pend�ncias", 0);
            cmbavStatuscontagem.addItem("7", "Diverg�ncia", 0);
            if ( cmbavStatuscontagem.ItemCount > 0 )
            {
               AV42StatusContagem = (short)(NumberUtil.Val( cmbavStatuscontagem.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV42StatusContagem), 2, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42StatusContagem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42StatusContagem), 2, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSTATUSCONTAGEM", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV42StatusContagem), "Z9")));
            }
            chkavRemetenteinterno.Name = "vREMETENTEINTERNO";
            chkavRemetenteinterno.WebTags = "";
            chkavRemetenteinterno.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavRemetenteinterno_Internalname, "TitleCaption", chkavRemetenteinterno.Caption);
            chkavRemetenteinterno.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavUsuario_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         GXVvCONTRATOFS_htmlIZ2( AV5Usuario_Codigo) ;
         /* End function dynload_actions */
      }

      protected void GXDLVvCONTRATOFSIZ2( int AV5Usuario_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTRATOFS_dataIZ2( AV5Usuario_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTRATOFS_htmlIZ2( int AV5Usuario_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTRATOFS_dataIZ2( AV5Usuario_Codigo) ;
         gxdynajaxindex = 1;
         dynavContratofs.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContratofs.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContratofs.ItemCount > 0 )
         {
            AV188ContratoFS = (int)(NumberUtil.Val( dynavContratofs.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV188ContratoFS), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV188ContratoFS", StringUtil.LTrim( StringUtil.Str( (decimal)(AV188ContratoFS), 6, 0)));
         }
      }

      protected void GXDLVvCONTRATOFS_dataIZ2( int AV5Usuario_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00IZ4 */
         pr_default.execute(0, new Object[] {AV5Usuario_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00IZ4_A74Contrato_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00IZ4_A77Contrato_Numero[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXDLVvNAOCONFORMIDADE_CODIGOIZ2( wwpbaseobjects.SdtWWPContext AV53WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvNAOCONFORMIDADE_CODIGO_dataIZ2( AV53WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvNAOCONFORMIDADE_CODIGO_htmlIZ2( wwpbaseobjects.SdtWWPContext AV53WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvNAOCONFORMIDADE_CODIGO_dataIZ2( AV53WWPContext) ;
         gxdynajaxindex = 1;
         dynavNaoconformidade_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavNaoconformidade_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavNaoconformidade_codigo.ItemCount > 0 )
         {
            AV183NaoConformidade_Codigo = (int)(NumberUtil.Val( dynavNaoconformidade_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV183NaoConformidade_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV183NaoConformidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV183NaoConformidade_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvNAOCONFORMIDADE_CODIGO_dataIZ2( wwpbaseobjects.SdtWWPContext AV53WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhuma)");
         /* Using cursor H00IZ5 */
         pr_default.execute(1, new Object[] {AV53WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00IZ5_A426NaoConformidade_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00IZ5_A427NaoConformidade_Nome[0]));
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavUsuario_codigo.ItemCount > 0 )
         {
            AV5Usuario_Codigo = (int)(NumberUtil.Val( cmbavUsuario_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV5Usuario_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Usuario_Codigo), 6, 0)));
         }
         if ( dynavContratofs.ItemCount > 0 )
         {
            AV188ContratoFS = (int)(NumberUtil.Val( dynavContratofs.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV188ContratoFS), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV188ContratoFS", StringUtil.LTrim( StringUtil.Str( (decimal)(AV188ContratoFS), 6, 0)));
         }
         if ( cmbavServicogrupo_codigo.ItemCount > 0 )
         {
            AV177ServicoGrupo_Codigo = (int)(NumberUtil.Val( cmbavServicogrupo_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV177ServicoGrupo_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV177ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV177ServicoGrupo_Codigo), 6, 0)));
         }
         if ( cmbavNewcntsrvcod.ItemCount > 0 )
         {
            AV182NewCntSrvCod = (int)(NumberUtil.Val( cmbavNewcntsrvcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV182NewCntSrvCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV182NewCntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV182NewCntSrvCod), 6, 0)));
         }
         if ( cmbavCriarospara_codigo.ItemCount > 0 )
         {
            AV168CriarOSPara_Codigo = (int)(NumberUtil.Val( cmbavCriarospara_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV168CriarOSPara_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV168CriarOSPara_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV168CriarOSPara_Codigo), 6, 0)));
         }
         if ( cmbavContrato.ItemCount > 0 )
         {
            AV174Contrato = (int)(NumberUtil.Val( cmbavContrato.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV174Contrato), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV174Contrato", StringUtil.LTrim( StringUtil.Str( (decimal)(AV174Contrato), 6, 0)));
         }
         if ( dynavNaoconformidade_codigo.ItemCount > 0 )
         {
            AV183NaoConformidade_Codigo = (int)(NumberUtil.Val( dynavNaoconformidade_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV183NaoConformidade_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV183NaoConformidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV183NaoConformidade_Codigo), 6, 0)));
         }
         if ( cmbavStatuscontagem.ItemCount > 0 )
         {
            AV42StatusContagem = (short)(NumberUtil.Val( cmbavStatuscontagem.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV42StatusContagem), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42StatusContagem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42StatusContagem), 2, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSTATUSCONTAGEM", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV42StatusContagem), "Z9")));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFIZ2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV191Pgmname = "WP_Tramitacao";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV191Pgmname", AV191Pgmname);
         context.Gx_err = 0;
         edtavDescricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDescricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDescricao_Enabled), 5, 0)));
         edtavPfb_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfb_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfb_Enabled), 5, 0)));
         edtavPfl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfl_Enabled), 5, 0)));
         edtavPfbof_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbof_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbof_Enabled), 5, 0)));
         edtavPflof_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPflof_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPflof_Enabled), 5, 0)));
         cmbavStatuscontagem.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavStatuscontagem_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavStatuscontagem.Enabled), 5, 0)));
         edtavPfbfmultima_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbfmultima_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfmultima_Enabled), 5, 0)));
         edtavPfbfsultima_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbfsultima_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfsultima_Enabled), 5, 0)));
         edtavPflfmultima_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPflfmultima_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPflfmultima_Enabled), 5, 0)));
         edtavPflfsultima_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPflfsultima_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPflfsultima_Enabled), 5, 0)));
         edtavOldpfb_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOldpfb_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOldpfb_Enabled), 5, 0)));
         edtavOldpfl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOldpfl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOldpfl_Enabled), 5, 0)));
      }

      protected void RFIZ2( )
      {
         initialize_formulas( ) ;
         /* Execute user event: E12IZ2 */
         E12IZ2 ();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            if ( StringUtil.StrCmp(WebComp_Webcomp2_Component, "") == 0 )
            {
               WebComp_Webcomp2 = getWebComponent(GetType(), "GeneXus.Programs", "wc_contagemresultadoevidencias", new Object[] {context} );
               WebComp_Webcomp2.ComponentInit();
               WebComp_Webcomp2.Name = "WC_ContagemResultadoEvidencias";
               WebComp_Webcomp2_Component = "WC_ContagemResultadoEvidencias";
            }
            WebComp_Webcomp2.setjustcreated();
            WebComp_Webcomp2.componentprepare(new Object[] {(String)"W0077",(String)""});
            WebComp_Webcomp2.componentbind(new Object[] {});
            if ( isFullAjaxMode( ) )
            {
               context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpW0077"+"");
               WebComp_Webcomp2.componentdraw();
               context.httpAjaxContext.ajax_rspEndCmp();
            }
            if ( 1 != 0 )
            {
               WebComp_Webcomp2.componentstart();
            }
         }
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E22IZ2 */
            E22IZ2 ();
            WBIZ0( ) ;
         }
      }

      protected void STRUPIZ0( )
      {
         /* Before Start, stand alone formulas. */
         AV191Pgmname = "WP_Tramitacao";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV191Pgmname", AV191Pgmname);
         context.Gx_err = 0;
         edtavDescricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDescricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDescricao_Enabled), 5, 0)));
         edtavPfb_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfb_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfb_Enabled), 5, 0)));
         edtavPfl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfl_Enabled), 5, 0)));
         edtavPfbof_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbof_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbof_Enabled), 5, 0)));
         edtavPflof_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPflof_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPflof_Enabled), 5, 0)));
         cmbavStatuscontagem.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavStatuscontagem_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavStatuscontagem.Enabled), 5, 0)));
         edtavPfbfmultima_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbfmultima_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfmultima_Enabled), 5, 0)));
         edtavPfbfsultima_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbfsultima_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfsultima_Enabled), 5, 0)));
         edtavPflfmultima_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPflfmultima_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPflfmultima_Enabled), 5, 0)));
         edtavPflfsultima_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPflfsultima_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPflfsultima_Enabled), 5, 0)));
         edtavOldpfb_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOldpfb_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOldpfb_Enabled), 5, 0)));
         edtavOldpfl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOldpfl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOldpfl_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11IZ2 */
         E11IZ2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         GXVvNAOCONFORMIDADE_CODIGO_htmlIZ2( AV53WWPContext) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbavUsuario_codigo.CurrentValue = cgiGet( cmbavUsuario_codigo_Internalname);
            AV5Usuario_Codigo = (int)(NumberUtil.Val( cgiGet( cmbavUsuario_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Usuario_Codigo), 6, 0)));
            if ( context.localUtil.VCDateTime( cgiGet( edtavPrazoresposta_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Prazo Resposta"}), 1, "vPRAZORESPOSTA");
               GX_FocusControl = edtavPrazoresposta_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV39PrazoResposta = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39PrazoResposta", context.localUtil.Format(AV39PrazoResposta, "99/99/99"));
            }
            else
            {
               AV39PrazoResposta = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavPrazoresposta_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39PrazoResposta", context.localUtil.Format(AV39PrazoResposta, "99/99/99"));
            }
            dynavContratofs.CurrentValue = cgiGet( dynavContratofs_Internalname);
            AV188ContratoFS = (int)(NumberUtil.Val( cgiGet( dynavContratofs_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV188ContratoFS", StringUtil.LTrim( StringUtil.Str( (decimal)(AV188ContratoFS), 6, 0)));
            cmbavServicogrupo_codigo.CurrentValue = cgiGet( cmbavServicogrupo_codigo_Internalname);
            AV177ServicoGrupo_Codigo = (int)(NumberUtil.Val( cgiGet( cmbavServicogrupo_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV177ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV177ServicoGrupo_Codigo), 6, 0)));
            cmbavNewcntsrvcod.CurrentValue = cgiGet( cmbavNewcntsrvcod_Internalname);
            AV182NewCntSrvCod = (int)(NumberUtil.Val( cgiGet( cmbavNewcntsrvcod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV182NewCntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV182NewCntSrvCod), 6, 0)));
            cmbavCriarospara_codigo.CurrentValue = cgiGet( cmbavCriarospara_codigo_Internalname);
            AV168CriarOSPara_Codigo = (int)(NumberUtil.Val( cgiGet( cmbavCriarospara_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV168CriarOSPara_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV168CriarOSPara_Codigo), 6, 0)));
            cmbavContrato.CurrentValue = cgiGet( cmbavContrato_Internalname);
            AV174Contrato = (int)(NumberUtil.Val( cgiGet( cmbavContrato_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV174Contrato", StringUtil.LTrim( StringUtil.Str( (decimal)(AV174Contrato), 6, 0)));
            dynavNaoconformidade_codigo.CurrentValue = cgiGet( dynavNaoconformidade_codigo_Internalname);
            AV183NaoConformidade_Codigo = (int)(NumberUtil.Val( cgiGet( dynavNaoconformidade_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV183NaoConformidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV183NaoConformidade_Codigo), 6, 0)));
            AV140Descricao = cgiGet( edtavDescricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV140Descricao", AV140Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vDESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV140Descricao, ""))));
            AV6Observacao = cgiGet( edtavObservacao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Observacao", AV6Observacao);
            AV46Deferido = cgiGet( radavDeferido_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46Deferido", AV46Deferido);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavNewpfb_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavNewpfb_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vNEWPFB");
               GX_FocusControl = edtavNewpfb_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV63NewPFB = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63NewPFB", StringUtil.LTrim( StringUtil.Str( AV63NewPFB, 14, 5)));
            }
            else
            {
               AV63NewPFB = context.localUtil.CToN( cgiGet( edtavNewpfb_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63NewPFB", StringUtil.LTrim( StringUtil.Str( AV63NewPFB, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavNewpfl_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavNewpfl_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vNEWPFL");
               GX_FocusControl = edtavNewpfl_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV64NewPFL = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64NewPFL", StringUtil.LTrim( StringUtil.Str( AV64NewPFL, 14, 5)));
            }
            else
            {
               AV64NewPFL = context.localUtil.CToN( cgiGet( edtavNewpfl_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64NewPFL", StringUtil.LTrim( StringUtil.Str( AV64NewPFL, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavPfb_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPfb_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPFB");
               GX_FocusControl = edtavPfb_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV57PFB = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57PFB", StringUtil.LTrim( StringUtil.Str( AV57PFB, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFB", GetSecureSignedToken( "", context.localUtil.Format( AV57PFB, "ZZ,ZZZ,ZZ9.999")));
            }
            else
            {
               AV57PFB = context.localUtil.CToN( cgiGet( edtavPfb_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57PFB", StringUtil.LTrim( StringUtil.Str( AV57PFB, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFB", GetSecureSignedToken( "", context.localUtil.Format( AV57PFB, "ZZ,ZZZ,ZZ9.999")));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavPfl_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPfl_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPFL");
               GX_FocusControl = edtavPfl_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV58PFL = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58PFL", StringUtil.LTrim( StringUtil.Str( AV58PFL, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFL", GetSecureSignedToken( "", context.localUtil.Format( AV58PFL, "ZZ,ZZZ,ZZ9.999")));
            }
            else
            {
               AV58PFL = context.localUtil.CToN( cgiGet( edtavPfl_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58PFL", StringUtil.LTrim( StringUtil.Str( AV58PFL, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFL", GetSecureSignedToken( "", context.localUtil.Format( AV58PFL, "ZZ,ZZZ,ZZ9.999")));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavPfbof_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPfbof_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPFBOF");
               GX_FocusControl = edtavPfbof_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV70PFBOF = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70PFBOF", StringUtil.LTrim( StringUtil.Str( AV70PFBOF, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFBOF", GetSecureSignedToken( "", context.localUtil.Format( AV70PFBOF, "ZZ,ZZZ,ZZ9.999")));
            }
            else
            {
               AV70PFBOF = context.localUtil.CToN( cgiGet( edtavPfbof_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70PFBOF", StringUtil.LTrim( StringUtil.Str( AV70PFBOF, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFBOF", GetSecureSignedToken( "", context.localUtil.Format( AV70PFBOF, "ZZ,ZZZ,ZZ9.999")));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavPflof_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPflof_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPFLOF");
               GX_FocusControl = edtavPflof_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV71PFLOF = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71PFLOF", StringUtil.LTrim( StringUtil.Str( AV71PFLOF, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFLOF", GetSecureSignedToken( "", context.localUtil.Format( AV71PFLOF, "ZZ,ZZZ,ZZ9.999")));
            }
            else
            {
               AV71PFLOF = context.localUtil.CToN( cgiGet( edtavPflof_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71PFLOF", StringUtil.LTrim( StringUtil.Str( AV71PFLOF, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFLOF", GetSecureSignedToken( "", context.localUtil.Format( AV71PFLOF, "ZZ,ZZZ,ZZ9.999")));
            }
            cmbavStatuscontagem.CurrentValue = cgiGet( cmbavStatuscontagem_Internalname);
            AV42StatusContagem = (short)(NumberUtil.Val( cgiGet( cmbavStatuscontagem_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42StatusContagem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42StatusContagem), 2, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSTATUSCONTAGEM", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV42StatusContagem), "Z9")));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavPfbfmultima_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPfbfmultima_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPFBFMULTIMA");
               GX_FocusControl = edtavPfbfmultima_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV81PFBFMUltima = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81PFBFMUltima", StringUtil.LTrim( StringUtil.Str( AV81PFBFMUltima, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFBFMULTIMA", GetSecureSignedToken( "", context.localUtil.Format( AV81PFBFMUltima, "ZZ,ZZZ,ZZ9.999")));
            }
            else
            {
               AV81PFBFMUltima = context.localUtil.CToN( cgiGet( edtavPfbfmultima_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81PFBFMUltima", StringUtil.LTrim( StringUtil.Str( AV81PFBFMUltima, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFBFMULTIMA", GetSecureSignedToken( "", context.localUtil.Format( AV81PFBFMUltima, "ZZ,ZZZ,ZZ9.999")));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavPfbfsultima_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPfbfsultima_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPFBFSULTIMA");
               GX_FocusControl = edtavPfbfsultima_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV83PFBFSUltima = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83PFBFSUltima", StringUtil.LTrim( StringUtil.Str( AV83PFBFSUltima, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFBFSULTIMA", GetSecureSignedToken( "", context.localUtil.Format( AV83PFBFSUltima, "ZZ,ZZZ,ZZ9.999")));
            }
            else
            {
               AV83PFBFSUltima = context.localUtil.CToN( cgiGet( edtavPfbfsultima_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83PFBFSUltima", StringUtil.LTrim( StringUtil.Str( AV83PFBFSUltima, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFBFSULTIMA", GetSecureSignedToken( "", context.localUtil.Format( AV83PFBFSUltima, "ZZ,ZZZ,ZZ9.999")));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavPflfmultima_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPflfmultima_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPFLFMULTIMA");
               GX_FocusControl = edtavPflfmultima_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV82PFLFMUltima = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82PFLFMUltima", StringUtil.LTrim( StringUtil.Str( AV82PFLFMUltima, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFLFMULTIMA", GetSecureSignedToken( "", context.localUtil.Format( AV82PFLFMUltima, "ZZ,ZZZ,ZZ9.999")));
            }
            else
            {
               AV82PFLFMUltima = context.localUtil.CToN( cgiGet( edtavPflfmultima_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82PFLFMUltima", StringUtil.LTrim( StringUtil.Str( AV82PFLFMUltima, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFLFMULTIMA", GetSecureSignedToken( "", context.localUtil.Format( AV82PFLFMUltima, "ZZ,ZZZ,ZZ9.999")));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavPflfsultima_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPflfsultima_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPFLFSULTIMA");
               GX_FocusControl = edtavPflfsultima_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV80PFLFSUltima = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80PFLFSUltima", StringUtil.LTrim( StringUtil.Str( AV80PFLFSUltima, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFLFSULTIMA", GetSecureSignedToken( "", context.localUtil.Format( AV80PFLFSUltima, "ZZ,ZZZ,ZZ9.999")));
            }
            else
            {
               AV80PFLFSUltima = context.localUtil.CToN( cgiGet( edtavPflfsultima_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80PFLFSUltima", StringUtil.LTrim( StringUtil.Str( AV80PFLFSUltima, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFLFSULTIMA", GetSecureSignedToken( "", context.localUtil.Format( AV80PFLFSUltima, "ZZ,ZZZ,ZZ9.999")));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavOldpfb_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOldpfb_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vOLDPFB");
               GX_FocusControl = edtavOldpfb_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV104OldPFB = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV104OldPFB", StringUtil.LTrim( StringUtil.Str( AV104OldPFB, 14, 5)));
            }
            else
            {
               AV104OldPFB = context.localUtil.CToN( cgiGet( edtavOldpfb_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV104OldPFB", StringUtil.LTrim( StringUtil.Str( AV104OldPFB, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavOldpfl_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOldpfl_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vOLDPFL");
               GX_FocusControl = edtavOldpfl_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV105OldPFL = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105OldPFL", StringUtil.LTrim( StringUtil.Str( AV105OldPFL, 14, 5)));
            }
            else
            {
               AV105OldPFL = context.localUtil.CToN( cgiGet( edtavOldpfl_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105OldPFL", StringUtil.LTrim( StringUtil.Str( AV105OldPFL, 14, 5)));
            }
            AV151RemetenteInterno = StringUtil.StrToBool( cgiGet( chkavRemetenteinterno_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV151RemetenteInterno", AV151RemetenteInterno);
            /* Read saved values. */
            AV45PrazoEntrega = context.localUtil.CToT( cgiGet( "vPRAZOENTREGA"), 0);
            AV50PrazoInicial = context.localUtil.CToT( cgiGet( "vPRAZOINICIAL"), 0);
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = "hsh" + "WP_Tramitacao";
            AV140Descricao = cgiGet( edtavDescricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV140Descricao", AV140Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vDESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV140Descricao, ""))));
            forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( AV140Descricao, ""));
            AV57PFB = context.localUtil.CToN( cgiGet( edtavPfb_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57PFB", StringUtil.LTrim( StringUtil.Str( AV57PFB, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFB", GetSecureSignedToken( "", context.localUtil.Format( AV57PFB, "ZZ,ZZZ,ZZ9.999")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV57PFB, "ZZ,ZZZ,ZZ9.999");
            AV58PFL = context.localUtil.CToN( cgiGet( edtavPfl_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58PFL", StringUtil.LTrim( StringUtil.Str( AV58PFL, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFL", GetSecureSignedToken( "", context.localUtil.Format( AV58PFL, "ZZ,ZZZ,ZZ9.999")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV58PFL, "ZZ,ZZZ,ZZ9.999");
            AV70PFBOF = context.localUtil.CToN( cgiGet( edtavPfbof_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70PFBOF", StringUtil.LTrim( StringUtil.Str( AV70PFBOF, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFBOF", GetSecureSignedToken( "", context.localUtil.Format( AV70PFBOF, "ZZ,ZZZ,ZZ9.999")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV70PFBOF, "ZZ,ZZZ,ZZ9.999");
            AV71PFLOF = context.localUtil.CToN( cgiGet( edtavPflof_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71PFLOF", StringUtil.LTrim( StringUtil.Str( AV71PFLOF, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFLOF", GetSecureSignedToken( "", context.localUtil.Format( AV71PFLOF, "ZZ,ZZZ,ZZ9.999")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV71PFLOF, "ZZ,ZZZ,ZZ9.999");
            AV81PFBFMUltima = context.localUtil.CToN( cgiGet( edtavPfbfmultima_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81PFBFMUltima", StringUtil.LTrim( StringUtil.Str( AV81PFBFMUltima, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFBFMULTIMA", GetSecureSignedToken( "", context.localUtil.Format( AV81PFBFMUltima, "ZZ,ZZZ,ZZ9.999")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV81PFBFMUltima, "ZZ,ZZZ,ZZ9.999");
            AV83PFBFSUltima = context.localUtil.CToN( cgiGet( edtavPfbfsultima_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83PFBFSUltima", StringUtil.LTrim( StringUtil.Str( AV83PFBFSUltima, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFBFSULTIMA", GetSecureSignedToken( "", context.localUtil.Format( AV83PFBFSUltima, "ZZ,ZZZ,ZZ9.999")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV83PFBFSUltima, "ZZ,ZZZ,ZZ9.999");
            AV82PFLFMUltima = context.localUtil.CToN( cgiGet( edtavPflfmultima_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82PFLFMUltima", StringUtil.LTrim( StringUtil.Str( AV82PFLFMUltima, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFLFMULTIMA", GetSecureSignedToken( "", context.localUtil.Format( AV82PFLFMUltima, "ZZ,ZZZ,ZZ9.999")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV82PFLFMUltima, "ZZ,ZZZ,ZZ9.999");
            AV80PFLFSUltima = context.localUtil.CToN( cgiGet( edtavPflfsultima_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80PFLFSUltima", StringUtil.LTrim( StringUtil.Str( AV80PFLFSUltima, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFLFSULTIMA", GetSecureSignedToken( "", context.localUtil.Format( AV80PFLFSUltima, "ZZ,ZZZ,ZZ9.999")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV80PFLFSUltima, "ZZ,ZZZ,ZZ9.999");
            hsh = cgiGet( "hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("wp_tramitacao:[SecurityCheckFailed value for]"+"Descricao:"+StringUtil.RTrim( context.localUtil.Format( AV140Descricao, "")));
               GXUtil.WriteLog("wp_tramitacao:[SecurityCheckFailed value for]"+"PFB:"+context.localUtil.Format( AV57PFB, "ZZ,ZZZ,ZZ9.999"));
               GXUtil.WriteLog("wp_tramitacao:[SecurityCheckFailed value for]"+"PFL:"+context.localUtil.Format( AV58PFL, "ZZ,ZZZ,ZZ9.999"));
               GXUtil.WriteLog("wp_tramitacao:[SecurityCheckFailed value for]"+"PFBOF:"+context.localUtil.Format( AV70PFBOF, "ZZ,ZZZ,ZZ9.999"));
               GXUtil.WriteLog("wp_tramitacao:[SecurityCheckFailed value for]"+"PFLOF:"+context.localUtil.Format( AV71PFLOF, "ZZ,ZZZ,ZZ9.999"));
               GXUtil.WriteLog("wp_tramitacao:[SecurityCheckFailed value for]"+"PFBFMUltima:"+context.localUtil.Format( AV81PFBFMUltima, "ZZ,ZZZ,ZZ9.999"));
               GXUtil.WriteLog("wp_tramitacao:[SecurityCheckFailed value for]"+"PFBFSUltima:"+context.localUtil.Format( AV83PFBFSUltima, "ZZ,ZZZ,ZZ9.999"));
               GXUtil.WriteLog("wp_tramitacao:[SecurityCheckFailed value for]"+"PFLFMUltima:"+context.localUtil.Format( AV82PFLFMUltima, "ZZ,ZZZ,ZZ9.999"));
               GXUtil.WriteLog("wp_tramitacao:[SecurityCheckFailed value for]"+"PFLFSUltima:"+context.localUtil.Format( AV80PFLFSUltima, "ZZ,ZZZ,ZZ9.999"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
            GXVvNAOCONFORMIDADE_CODIGO_htmlIZ2( AV53WWPContext) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11IZ2 */
         E11IZ2 ();
         if (returnInSub) return;
      }

      protected void E11IZ2( )
      {
         /* Start Routine */
         Form.Meta.addItem("Versao", "1.5 - Data: 26/03/2020 00:01", 0) ;
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV53WWPContext) ;
         AV31AreaTrabalho_Codigo = AV53WWPContext.gxTpr_Areatrabalho_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31AreaTrabalho_Codigo), 6, 0)));
         AV24Contratada_Codigo = AV53WWPContext.gxTpr_Contratada_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Contratada_Codigo), 6, 0)));
         AV67Contratada_Sigla = AV53WWPContext.gxTpr_Contratada_pessoanom;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67Contratada_Sigla", AV67Contratada_Sigla);
         AV40Contratante_Codigo = AV53WWPContext.gxTpr_Contratante_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40Contratante_Codigo), 6, 0)));
         AV8UserEhContratante = AV53WWPContext.gxTpr_Userehcontratante;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8UserEhContratante", AV8UserEhContratante);
         AV12UserId = AV53WWPContext.gxTpr_Userid;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12UserId), 6, 0)));
         AV114Caller = AV30WebSession.Get("Caller");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV114Caller", AV114Caller);
         new geralog(context ).execute( ref  AV191Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV191Pgmname", AV191Pgmname);
         GXt_char1 = "----------------- Event Start -----------------------";
         new geralog(context ).execute( ref  GXt_char1) ;
         GXt_char2 = "&AreaTrabalho_Codigo = " + context.localUtil.Format( (decimal)(AV31AreaTrabalho_Codigo), "ZZZZZ9") + " - " + AV53WWPContext.gxTpr_Areatrabalho_descricao;
         new geralog(context ).execute( ref  GXt_char2) ;
         GXt_char3 = "&Contratada_Codigo = " + context.localUtil.Format( (decimal)(AV24Contratada_Codigo), "ZZZZZ9") + " - " + context.localUtil.Format( (decimal)(AV53WWPContext.gxTpr_Contratada_pessoacod), "ZZZZZ9") + " - " + AV53WWPContext.gxTpr_Contratada_pessoanom;
         new geralog(context ).execute( ref  GXt_char3) ;
         GXt_char4 = "&Contratada_Sigla = " + AV67Contratada_Sigla;
         new geralog(context ).execute( ref  GXt_char4) ;
         GXt_char5 = "&Contratante_Codigo = " + context.localUtil.Format( (decimal)(AV40Contratante_Codigo), "ZZZZZ9") + " - " + AV53WWPContext.gxTpr_Contratante_nomefantasia;
         new geralog(context ).execute( ref  GXt_char5) ;
         GXt_char6 = "&UserEhContratante = " + StringUtil.BoolToStr( AV8UserEhContratante);
         new geralog(context ).execute( ref  GXt_char6) ;
         GXt_char7 = "&UserId = " + context.localUtil.Format( (decimal)(AV12UserId), "ZZZZZ9");
         new geralog(context ).execute( ref  GXt_char7) ;
         GXt_char8 = "&Caller =  " + AV114Caller;
         new geralog(context ).execute( ref  GXt_char8) ;
         GXt_char9 = "&&PodeAtribuirDmn = " + StringUtil.BoolToStr( AV11PodeAtribuirDmn);
         new geralog(context ).execute( ref  GXt_char9) ;
         GXt_char10 = "----------------- Event Start -----------------------";
         new geralog(context ).execute( ref  GXt_char10) ;
         new geralog(context ).execute( ref  AV191Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV191Pgmname", AV191Pgmname);
         AV30WebSession.Remove("Caller");
         AV20Selecionadas.FromXml(AV30WebSession.Get("Codigos"), "Collection");
         GXt_boolean11 = AV152RetornaSS;
         GXt_int12 = AV53WWPContext.gxTpr_Contratante_codigo;
         new prc_retornass(context ).execute( ref  GXt_int12, out  GXt_boolean11) ;
         AV53WWPContext.gxTpr_Contratante_codigo = GXt_int12;
         AV152RetornaSS = GXt_boolean11;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV152RetornaSS", AV152RetornaSS);
         /* Using cursor H00IZ7 */
         pr_default.execute(2, new Object[] {AV9Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A146Modulo_Codigo = H00IZ7_A146Modulo_Codigo[0];
            n146Modulo_Codigo = H00IZ7_n146Modulo_Codigo[0];
            A468ContagemResultado_NaoCnfDmnCod = H00IZ7_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = H00IZ7_n468ContagemResultado_NaoCnfDmnCod[0];
            A1553ContagemResultado_CntSrvCod = H00IZ7_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = H00IZ7_n1553ContagemResultado_CntSrvCod[0];
            A456ContagemResultado_Codigo = H00IZ7_A456ContagemResultado_Codigo[0];
            A127Sistema_Codigo = H00IZ7_A127Sistema_Codigo[0];
            A489ContagemResultado_SistemaCod = H00IZ7_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = H00IZ7_n489ContagemResultado_SistemaCod[0];
            A1831Sistema_Responsavel = H00IZ7_A1831Sistema_Responsavel[0];
            n1831Sistema_Responsavel = H00IZ7_n1831Sistema_Responsavel[0];
            A428NaoConformidade_AreaTrabalhoCod = H00IZ7_A428NaoConformidade_AreaTrabalhoCod[0];
            n428NaoConformidade_AreaTrabalhoCod = H00IZ7_n428NaoConformidade_AreaTrabalhoCod[0];
            A429NaoConformidade_Tipo = H00IZ7_A429NaoConformidade_Tipo[0];
            n429NaoConformidade_Tipo = H00IZ7_n429NaoConformidade_Tipo[0];
            A92Contrato_Ativo = H00IZ7_A92Contrato_Ativo[0];
            n92Contrato_Ativo = H00IZ7_n92Contrato_Ativo[0];
            A457ContagemResultado_Demanda = H00IZ7_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = H00IZ7_n457ContagemResultado_Demanda[0];
            A484ContagemResultado_StatusDmn = H00IZ7_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00IZ7_n484ContagemResultado_StatusDmn[0];
            A1593ContagemResultado_CntSrvTpVnc = H00IZ7_A1593ContagemResultado_CntSrvTpVnc[0];
            n1593ContagemResultado_CntSrvTpVnc = H00IZ7_n1593ContagemResultado_CntSrvTpVnc[0];
            A602ContagemResultado_OSVinculada = H00IZ7_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = H00IZ7_n602ContagemResultado_OSVinculada[0];
            A890ContagemResultado_Responsavel = H00IZ7_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = H00IZ7_n890ContagemResultado_Responsavel[0];
            A601ContagemResultado_Servico = H00IZ7_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00IZ7_n601ContagemResultado_Servico[0];
            A803ContagemResultado_ContratadaSigla = H00IZ7_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = H00IZ7_n803ContagemResultado_ContratadaSigla[0];
            A490ContagemResultado_ContratadaCod = H00IZ7_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00IZ7_n490ContagemResultado_ContratadaCod[0];
            A805ContagemResultado_ContratadaOrigemCod = H00IZ7_A805ContagemResultado_ContratadaOrigemCod[0];
            n805ContagemResultado_ContratadaOrigemCod = H00IZ7_n805ContagemResultado_ContratadaOrigemCod[0];
            A1627ContagemResultado_CntSrvVncCod = H00IZ7_A1627ContagemResultado_CntSrvVncCod[0];
            n1627ContagemResultado_CntSrvVncCod = H00IZ7_n1627ContagemResultado_CntSrvVncCod[0];
            A1622ContagemResultado_CntVncCod = H00IZ7_A1622ContagemResultado_CntVncCod[0];
            n1622ContagemResultado_CntVncCod = H00IZ7_n1622ContagemResultado_CntVncCod[0];
            A866ContagemResultado_ContratadaOrigemSigla = H00IZ7_A866ContagemResultado_ContratadaOrigemSigla[0];
            n866ContagemResultado_ContratadaOrigemSigla = H00IZ7_n866ContagemResultado_ContratadaOrigemSigla[0];
            A1326ContagemResultado_ContratadaTipoFab = H00IZ7_A1326ContagemResultado_ContratadaTipoFab[0];
            n1326ContagemResultado_ContratadaTipoFab = H00IZ7_n1326ContagemResultado_ContratadaTipoFab[0];
            A1611ContagemResultado_PrzTpDias = H00IZ7_A1611ContagemResultado_PrzTpDias[0];
            n1611ContagemResultado_PrzTpDias = H00IZ7_n1611ContagemResultado_PrzTpDias[0];
            A1636ContagemResultado_ServicoSS = H00IZ7_A1636ContagemResultado_ServicoSS[0];
            n1636ContagemResultado_ServicoSS = H00IZ7_n1636ContagemResultado_ServicoSS[0];
            A1452ContagemResultado_SS = H00IZ7_A1452ContagemResultado_SS[0];
            n1452ContagemResultado_SS = H00IZ7_n1452ContagemResultado_SS[0];
            A1603ContagemResultado_CntCod = H00IZ7_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = H00IZ7_n1603ContagemResultado_CntCod[0];
            A508ContagemResultado_Owner = H00IZ7_A508ContagemResultado_Owner[0];
            A494ContagemResultado_Descricao = H00IZ7_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = H00IZ7_n494ContagemResultado_Descricao[0];
            A531ContagemResultado_StatusUltCnt = H00IZ7_A531ContagemResultado_StatusUltCnt[0];
            A684ContagemResultado_PFBFSUltima = H00IZ7_A684ContagemResultado_PFBFSUltima[0];
            A685ContagemResultado_PFLFSUltima = H00IZ7_A685ContagemResultado_PFLFSUltima[0];
            A682ContagemResultado_PFBFMUltima = H00IZ7_A682ContagemResultado_PFBFMUltima[0];
            A683ContagemResultado_PFLFMUltima = H00IZ7_A683ContagemResultado_PFLFMUltima[0];
            A83Contrato_DataVigenciaTermino = H00IZ7_A83Contrato_DataVigenciaTermino[0];
            n83Contrato_DataVigenciaTermino = H00IZ7_n83Contrato_DataVigenciaTermino[0];
            A1829Sistema_GpoObjCtrlCod = H00IZ7_A1829Sistema_GpoObjCtrlCod[0];
            n1829Sistema_GpoObjCtrlCod = H00IZ7_n1829Sistema_GpoObjCtrlCod[0];
            A127Sistema_Codigo = H00IZ7_A127Sistema_Codigo[0];
            A1831Sistema_Responsavel = H00IZ7_A1831Sistema_Responsavel[0];
            n1831Sistema_Responsavel = H00IZ7_n1831Sistema_Responsavel[0];
            A1829Sistema_GpoObjCtrlCod = H00IZ7_A1829Sistema_GpoObjCtrlCod[0];
            n1829Sistema_GpoObjCtrlCod = H00IZ7_n1829Sistema_GpoObjCtrlCod[0];
            A428NaoConformidade_AreaTrabalhoCod = H00IZ7_A428NaoConformidade_AreaTrabalhoCod[0];
            n428NaoConformidade_AreaTrabalhoCod = H00IZ7_n428NaoConformidade_AreaTrabalhoCod[0];
            A429NaoConformidade_Tipo = H00IZ7_A429NaoConformidade_Tipo[0];
            n429NaoConformidade_Tipo = H00IZ7_n429NaoConformidade_Tipo[0];
            A1593ContagemResultado_CntSrvTpVnc = H00IZ7_A1593ContagemResultado_CntSrvTpVnc[0];
            n1593ContagemResultado_CntSrvTpVnc = H00IZ7_n1593ContagemResultado_CntSrvTpVnc[0];
            A601ContagemResultado_Servico = H00IZ7_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00IZ7_n601ContagemResultado_Servico[0];
            A1611ContagemResultado_PrzTpDias = H00IZ7_A1611ContagemResultado_PrzTpDias[0];
            n1611ContagemResultado_PrzTpDias = H00IZ7_n1611ContagemResultado_PrzTpDias[0];
            A1603ContagemResultado_CntCod = H00IZ7_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = H00IZ7_n1603ContagemResultado_CntCod[0];
            A92Contrato_Ativo = H00IZ7_A92Contrato_Ativo[0];
            n92Contrato_Ativo = H00IZ7_n92Contrato_Ativo[0];
            A83Contrato_DataVigenciaTermino = H00IZ7_A83Contrato_DataVigenciaTermino[0];
            n83Contrato_DataVigenciaTermino = H00IZ7_n83Contrato_DataVigenciaTermino[0];
            A531ContagemResultado_StatusUltCnt = H00IZ7_A531ContagemResultado_StatusUltCnt[0];
            A684ContagemResultado_PFBFSUltima = H00IZ7_A684ContagemResultado_PFBFSUltima[0];
            A685ContagemResultado_PFLFSUltima = H00IZ7_A685ContagemResultado_PFLFSUltima[0];
            A682ContagemResultado_PFBFMUltima = H00IZ7_A682ContagemResultado_PFBFMUltima[0];
            A683ContagemResultado_PFLFMUltima = H00IZ7_A683ContagemResultado_PFLFMUltima[0];
            A1627ContagemResultado_CntSrvVncCod = H00IZ7_A1627ContagemResultado_CntSrvVncCod[0];
            n1627ContagemResultado_CntSrvVncCod = H00IZ7_n1627ContagemResultado_CntSrvVncCod[0];
            A1622ContagemResultado_CntVncCod = H00IZ7_A1622ContagemResultado_CntVncCod[0];
            n1622ContagemResultado_CntVncCod = H00IZ7_n1622ContagemResultado_CntVncCod[0];
            A803ContagemResultado_ContratadaSigla = H00IZ7_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = H00IZ7_n803ContagemResultado_ContratadaSigla[0];
            A1326ContagemResultado_ContratadaTipoFab = H00IZ7_A1326ContagemResultado_ContratadaTipoFab[0];
            n1326ContagemResultado_ContratadaTipoFab = H00IZ7_n1326ContagemResultado_ContratadaTipoFab[0];
            A866ContagemResultado_ContratadaOrigemSigla = H00IZ7_A866ContagemResultado_ContratadaOrigemSigla[0];
            n866ContagemResultado_ContratadaOrigemSigla = H00IZ7_n866ContagemResultado_ContratadaOrigemSigla[0];
            GXt_int12 = A1830Sistema_GpoObjCtrlRsp;
            GXt_int13 = (short)(A1829Sistema_GpoObjCtrlCod);
            new prc_getgpoobjctrl_responsavel(context ).execute( ref  GXt_int13, out  GXt_int12) ;
            A1829Sistema_GpoObjCtrlCod = GXt_int13;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1829Sistema_GpoObjCtrlCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1829Sistema_GpoObjCtrlCod), 6, 0)));
            A1830Sistema_GpoObjCtrlRsp = GXt_int12;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1830Sistema_GpoObjCtrlRsp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1830Sistema_GpoObjCtrlRsp), 6, 0)));
            AV76Demanda = A457ContagemResultado_Demanda;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76Demanda", AV76Demanda);
            AV16StatusDemanda = A484ContagemResultado_StatusDmn;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16StatusDemanda", AV16StatusDemanda);
            AV42StatusContagem = A531ContagemResultado_StatusUltCnt;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42StatusContagem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42StatusContagem), 2, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSTATUSCONTAGEM", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV42StatusContagem), "Z9")));
            AV97EmDivergencia = (bool)(((A531ContagemResultado_StatusUltCnt==7)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97EmDivergencia", AV97EmDivergencia);
            if ( AV97EmDivergencia )
            {
               if ( ( StringUtil.StrCmp(A1593ContagemResultado_CntSrvTpVnc, "C") == 0 ) || ( StringUtil.StrCmp(A1593ContagemResultado_CntSrvTpVnc, "A") == 0 ) )
               {
                  AV59OsVinculada = A602ContagemResultado_OSVinculada;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59OsVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59OsVinculada), 6, 0)));
                  AV78TemVnc = (bool)(((A602ContagemResultado_OSVinculada>0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TemVnc", AV78TemVnc);
               }
            }
            else
            {
               AV59OsVinculada = A602ContagemResultado_OSVinculada;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59OsVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59OsVinculada), 6, 0)));
            }
            AV47ResponsavelAtual = A890ContagemResultado_Responsavel;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ResponsavelAtual", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47ResponsavelAtual), 6, 0)));
            new geralog(context ).execute( ref  AV191Pgmname) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV191Pgmname", AV191Pgmname);
            GXt_char10 = "ContagemResultado_Codigo " + context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9");
            new geralog(context ).execute( ref  GXt_char10) ;
            GXt_char9 = "&ResponsavelAtual " + context.localUtil.Format( (decimal)(AV47ResponsavelAtual), "ZZZZZ9");
            new geralog(context ).execute( ref  GXt_char9) ;
            new geralog(context ).execute( ref  AV191Pgmname) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV191Pgmname", AV191Pgmname);
            AV22Servico_Codigo = A601ContagemResultado_Servico;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Servico_Codigo), 6, 0)));
            AV67Contratada_Sigla = A803ContagemResultado_ContratadaSigla;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67Contratada_Sigla", AV67Contratada_Sigla);
            AV143Sistema_Codigo = A489ContagemResultado_SistemaCod;
            AV27Prestadora_Codigo = A490ContagemResultado_ContratadaCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Prestadora_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27Prestadora_Codigo), 6, 0)));
            AV142Prestadora_Sigla = A803ContagemResultado_ContratadaSigla;
            AV66ContratadaOrigem_Codigo = A805ContagemResultado_ContratadaOrigemCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ContratadaOrigem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66ContratadaOrigem_Codigo), 6, 0)));
            AV130CntSrvCodOrigem_Codigo = A1627ContagemResultado_CntSrvVncCod;
            AV115ContratoOrigem_Codigo = A1622ContagemResultado_CntVncCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV115ContratoOrigem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV115ContratoOrigem_Codigo), 6, 0)));
            AV110Origem_Sigla = A866ContagemResultado_ContratadaOrigemSigla + " ";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV110Origem_Sigla", AV110Origem_Sigla);
            AV61UsuarioDaPrestadora = (bool)(((A490ContagemResultado_ContratadaCod==AV53WWPContext.gxTpr_Contratada_codigo)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61UsuarioDaPrestadora", AV61UsuarioDaPrestadora);
            AV77TipoFabrica = A1326ContagemResultado_ContratadaTipoFab;
            AV83PFBFSUltima = A684ContagemResultado_PFBFSUltima;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83PFBFSUltima", StringUtil.LTrim( StringUtil.Str( AV83PFBFSUltima, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFBFSULTIMA", GetSecureSignedToken( "", context.localUtil.Format( AV83PFBFSUltima, "ZZ,ZZZ,ZZ9.999")));
            AV80PFLFSUltima = A685ContagemResultado_PFLFSUltima;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80PFLFSUltima", StringUtil.LTrim( StringUtil.Str( AV80PFLFSUltima, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFLFSULTIMA", GetSecureSignedToken( "", context.localUtil.Format( AV80PFLFSUltima, "ZZ,ZZZ,ZZ9.999")));
            AV81PFBFMUltima = A682ContagemResultado_PFBFMUltima;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81PFBFMUltima", StringUtil.LTrim( StringUtil.Str( AV81PFBFMUltima, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFBFMULTIMA", GetSecureSignedToken( "", context.localUtil.Format( AV81PFBFMUltima, "ZZ,ZZZ,ZZ9.999")));
            AV82PFLFMUltima = A683ContagemResultado_PFLFMUltima;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82PFLFMUltima", StringUtil.LTrim( StringUtil.Str( AV82PFLFMUltima, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFLFMULTIMA", GetSecureSignedToken( "", context.localUtil.Format( AV82PFLFMUltima, "ZZ,ZZZ,ZZ9.999")));
            AV107TipoDias = A1611ContagemResultado_PrzTpDias;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV107TipoDias", AV107TipoDias);
            AV154EhSolicitacaoSS = (bool)((A1636ContagemResultado_ServicoSS>0));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV154EhSolicitacaoSS", AV154EhSolicitacaoSS);
            AV120ServicoSS_Codigo = A1636ContagemResultado_ServicoSS;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV120ServicoSS_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV120ServicoSS_Codigo), 6, 0)));
            AV123ContagemResultado_SS = A1452ContagemResultado_SS;
            AV131ContagemResultado_CntCod = A1603ContagemResultado_CntCod;
            AV128Owner_Codigo = A508ContagemResultado_Owner;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV128Owner_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV128Owner_Codigo), 6, 0)));
            AV140Descricao = A494ContagemResultado_Descricao;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV140Descricao", AV140Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vDESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV140Descricao, ""))));
            AV165Contrato_Codigo = A1603ContagemResultado_CntCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV165Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV165Contrato_Codigo), 6, 0)));
            /* Using cursor H00IZ8 */
            pr_default.execute(3, new Object[] {n489ContagemResultado_SistemaCod, A489ContagemResultado_SistemaCod});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A127Sistema_Codigo = H00IZ8_A127Sistema_Codigo[0];
               if ( A1831Sistema_Responsavel > 0 )
               {
                  AV148Servico_Responsavel = A1831Sistema_Responsavel;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV148Servico_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV148Servico_Responsavel), 6, 0)));
               }
               else
               {
                  if ( A1830Sistema_GpoObjCtrlRsp > 0 )
                  {
                     AV148Servico_Responsavel = A1830Sistema_GpoObjCtrlRsp;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV148Servico_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV148Servico_Responsavel), 6, 0)));
                  }
               }
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(3);
            if ( (0==AV148Servico_Responsavel) )
            {
               if ( A1636ContagemResultado_ServicoSS > 0 )
               {
                  GXt_int12 = AV148Servico_Responsavel;
                  new prc_getservico_responsavel(context ).execute( ref  A1636ContagemResultado_ServicoSS, out  GXt_int12) ;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1636ContagemResultado_ServicoSS", StringUtil.LTrim( StringUtil.Str( (decimal)(A1636ContagemResultado_ServicoSS), 6, 0)));
                  AV148Servico_Responsavel = GXt_int12;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV148Servico_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV148Servico_Responsavel), 6, 0)));
               }
               else
               {
                  GXt_int12 = AV148Servico_Responsavel;
                  new prc_getservico_responsavel(context ).execute( ref  A601ContagemResultado_Servico, out  GXt_int12) ;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A601ContagemResultado_Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(A601ContagemResultado_Servico), 6, 0)));
                  AV148Servico_Responsavel = GXt_int12;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV148Servico_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV148Servico_Responsavel), 6, 0)));
               }
            }
            AV103QtdNaoAcatado = 0;
            AV150i = 0;
            /* Using cursor H00IZ9 */
            pr_default.execute(4, new Object[] {A456ContagemResultado_Codigo});
            while ( (pr_default.getStatus(4) != 101) )
            {
               A1219LogResponsavel_OwnerPessoaCod = H00IZ9_A1219LogResponsavel_OwnerPessoaCod[0];
               n1219LogResponsavel_OwnerPessoaCod = H00IZ9_n1219LogResponsavel_OwnerPessoaCod[0];
               A1553ContagemResultado_CntSrvCod = H00IZ9_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = H00IZ9_n1553ContagemResultado_CntSrvCod[0];
               A1603ContagemResultado_CntCod = H00IZ9_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = H00IZ9_n1603ContagemResultado_CntCod[0];
               A39Contratada_Codigo = H00IZ9_A39Contratada_Codigo[0];
               n39Contratada_Codigo = H00IZ9_n39Contratada_Codigo[0];
               A1222LogResponsavel_OwnerPessoaNom = H00IZ9_A1222LogResponsavel_OwnerPessoaNom[0];
               n1222LogResponsavel_OwnerPessoaNom = H00IZ9_n1222LogResponsavel_OwnerPessoaNom[0];
               A1797LogResponsavel_Codigo = H00IZ9_A1797LogResponsavel_Codigo[0];
               A896LogResponsavel_Owner = H00IZ9_A896LogResponsavel_Owner[0];
               A892LogResponsavel_DemandaCod = H00IZ9_A892LogResponsavel_DemandaCod[0];
               n892LogResponsavel_DemandaCod = H00IZ9_n892LogResponsavel_DemandaCod[0];
               A1219LogResponsavel_OwnerPessoaCod = H00IZ9_A1219LogResponsavel_OwnerPessoaCod[0];
               n1219LogResponsavel_OwnerPessoaCod = H00IZ9_n1219LogResponsavel_OwnerPessoaCod[0];
               A1222LogResponsavel_OwnerPessoaNom = H00IZ9_A1222LogResponsavel_OwnerPessoaNom[0];
               n1222LogResponsavel_OwnerPessoaNom = H00IZ9_n1222LogResponsavel_OwnerPessoaNom[0];
               A1553ContagemResultado_CntSrvCod = H00IZ9_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = H00IZ9_n1553ContagemResultado_CntSrvCod[0];
               A1603ContagemResultado_CntCod = H00IZ9_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = H00IZ9_n1603ContagemResultado_CntCod[0];
               A39Contratada_Codigo = H00IZ9_A39Contratada_Codigo[0];
               n39Contratada_Codigo = H00IZ9_n39Contratada_Codigo[0];
               GXt_boolean11 = A1149LogResponsavel_OwnerEhContratante;
               new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A896LogResponsavel_Owner, out  GXt_boolean11) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A892LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A892LogResponsavel_DemandaCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A896LogResponsavel_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A896LogResponsavel_Owner), 6, 0)));
               A1149LogResponsavel_OwnerEhContratante = GXt_boolean11;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1149LogResponsavel_OwnerEhContratante", A1149LogResponsavel_OwnerEhContratante);
               AV153Remetente = A896LogResponsavel_Owner;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV153Remetente", StringUtil.LTrim( StringUtil.Str( (decimal)(AV153Remetente), 6, 0)));
               AV150i = (short)(StringUtil.StringSearch( A1222LogResponsavel_OwnerPessoaNom, " ", 1)+1);
               if ( AV150i > 0 )
               {
                  AV150i = (short)(StringUtil.StringSearch( A1222LogResponsavel_OwnerPessoaNom, " ", AV150i)-1);
               }
               else
               {
                  AV150i = (short)(StringUtil.Len( StringUtil.Trim( A1222LogResponsavel_OwnerPessoaNom)));
               }
               AV149Remetente_Nome = "Remetente (" + StringUtil.Substring( A1222LogResponsavel_OwnerPessoaNom, 1, AV150i) + ")";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV149Remetente_Nome", AV149Remetente_Nome);
               if ( ! A1149LogResponsavel_OwnerEhContratante )
               {
                  AV150i = 0;
                  /* Optimized group. */
                  /* Using cursor H00IZ10 */
                  pr_default.execute(5, new Object[] {AV53WWPContext.gxTpr_Contratada_codigo, A896LogResponsavel_Owner, AV53WWPContext.gxTpr_Userid});
                  cV150i = H00IZ10_AV150i[0];
                  pr_default.close(5);
                  AV150i = (short)(AV150i+cV150i*1);
                  /* End optimized group. */
                  AV151RemetenteInterno = (bool)(((AV150i==2)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV151RemetenteInterno", AV151RemetenteInterno);
               }
               AV151RemetenteInterno = (bool)(AV151RemetenteInterno||(A1149LogResponsavel_OwnerEhContratante&&AV53WWPContext.gxTpr_Userehcontratante));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV151RemetenteInterno", AV151RemetenteInterno);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(4);
            }
            pr_default.close(4);
            if ( ( StringUtil.StrCmp(AV16StatusDemanda, "D") == 0 ) && AV53WWPContext.gxTpr_Userehcontratante )
            {
               AV21Selecionada = A456ContagemResultado_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Selecionada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Selecionada), 6, 0)));
               /* Execute user subroutine: 'STATUSANTESREJEICAO' */
               S113 ();
               if ( returnInSub )
               {
                  pr_default.close(2);
                  returnInSub = true;
                  if (true) return;
               }
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(2);
         if ( ! ( StringUtil.StrCmp(AV114Caller, "Gestao") == 0 ) )
         {
            if ( AV97EmDivergencia )
            {
               /* Execute user subroutine: 'DADOSOSVINCULADA' */
               S122 ();
               if (returnInSub) return;
               if ( AV78TemVnc )
               {
                  if ( StringUtil.StrCmp(AV77TipoFabrica, "M") == 0 )
                  {
                     AV57PFB = AV81PFBFMUltima;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57PFB", StringUtil.LTrim( StringUtil.Str( AV57PFB, 14, 5)));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFB", GetSecureSignedToken( "", context.localUtil.Format( AV57PFB, "ZZ,ZZZ,ZZ9.999")));
                     AV58PFL = AV82PFLFMUltima;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58PFL", StringUtil.LTrim( StringUtil.Str( AV58PFL, 14, 5)));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFL", GetSecureSignedToken( "", context.localUtil.Format( AV58PFL, "ZZ,ZZZ,ZZ9.999")));
                  }
                  else
                  {
                     AV57PFB = AV83PFBFSUltima;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57PFB", StringUtil.LTrim( StringUtil.Str( AV57PFB, 14, 5)));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFB", GetSecureSignedToken( "", context.localUtil.Format( AV57PFB, "ZZ,ZZZ,ZZ9.999")));
                     AV58PFL = AV80PFLFSUltima;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58PFL", StringUtil.LTrim( StringUtil.Str( AV58PFL, 14, 5)));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFL", GetSecureSignedToken( "", context.localUtil.Format( AV58PFL, "ZZ,ZZZ,ZZ9.999")));
                  }
               }
               else
               {
                  if ( AV61UsuarioDaPrestadora )
                  {
                     AV57PFB = AV81PFBFMUltima;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57PFB", StringUtil.LTrim( StringUtil.Str( AV57PFB, 14, 5)));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFB", GetSecureSignedToken( "", context.localUtil.Format( AV57PFB, "ZZ,ZZZ,ZZ9.999")));
                     AV58PFL = AV82PFLFMUltima;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58PFL", StringUtil.LTrim( StringUtil.Str( AV58PFL, 14, 5)));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFL", GetSecureSignedToken( "", context.localUtil.Format( AV58PFL, "ZZ,ZZZ,ZZ9.999")));
                     AV70PFBOF = AV83PFBFSUltima;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70PFBOF", StringUtil.LTrim( StringUtil.Str( AV70PFBOF, 14, 5)));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFBOF", GetSecureSignedToken( "", context.localUtil.Format( AV70PFBOF, "ZZ,ZZZ,ZZ9.999")));
                     AV71PFLOF = AV80PFLFSUltima;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71PFLOF", StringUtil.LTrim( StringUtil.Str( AV71PFLOF, 14, 5)));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFLOF", GetSecureSignedToken( "", context.localUtil.Format( AV71PFLOF, "ZZ,ZZZ,ZZ9.999")));
                  }
                  else
                  {
                     AV57PFB = AV83PFBFSUltima;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57PFB", StringUtil.LTrim( StringUtil.Str( AV57PFB, 14, 5)));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFB", GetSecureSignedToken( "", context.localUtil.Format( AV57PFB, "ZZ,ZZZ,ZZ9.999")));
                     AV58PFL = AV80PFLFSUltima;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58PFL", StringUtil.LTrim( StringUtil.Str( AV58PFL, 14, 5)));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFL", GetSecureSignedToken( "", context.localUtil.Format( AV58PFL, "ZZ,ZZZ,ZZ9.999")));
                     AV70PFBOF = AV81PFBFMUltima;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70PFBOF", StringUtil.LTrim( StringUtil.Str( AV70PFBOF, 14, 5)));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFBOF", GetSecureSignedToken( "", context.localUtil.Format( AV70PFBOF, "ZZ,ZZZ,ZZ9.999")));
                     AV71PFLOF = AV82PFLFMUltima;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71PFLOF", StringUtil.LTrim( StringUtil.Str( AV71PFLOF, 14, 5)));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFLOF", GetSecureSignedToken( "", context.localUtil.Format( AV71PFLOF, "ZZ,ZZZ,ZZ9.999")));
                  }
               }
               if ( StringUtil.StrCmp(AV16StatusDemanda, "A") == 0 )
               {
                  AV63NewPFB = AV57PFB;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63NewPFB", StringUtil.LTrim( StringUtil.Str( AV63NewPFB, 14, 5)));
                  AV64NewPFL = AV58PFL;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64NewPFL", StringUtil.LTrim( StringUtil.Str( AV64NewPFL, 14, 5)));
               }
               else
               {
                  AV63NewPFB = AV70PFBOF;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63NewPFB", StringUtil.LTrim( StringUtil.Str( AV63NewPFB, 14, 5)));
                  AV64NewPFL = AV71PFLOF;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64NewPFL", StringUtil.LTrim( StringUtil.Str( AV64NewPFL, 14, 5)));
               }
               AV104OldPFB = AV63NewPFB;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV104OldPFB", StringUtil.LTrim( StringUtil.Str( AV104OldPFB, 14, 5)));
               AV105OldPFL = AV64NewPFL;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105OldPFL", StringUtil.LTrim( StringUtil.Str( AV105OldPFL, 14, 5)));
               radavDeferido.removeItem("M");
               radavDeferido.addItem("M", "Manter o prazo de entrega inicial "+context.localUtil.DToC( DateTimeUtil.ResetTime( AV50PrazoInicial), 2, "/")+" (Indeferido)", 1);
               Form.Caption = "Tratamento da OS "+AV76Demanda+" ("+AV67Contratada_Sigla+")";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
               lblTbdestino_Caption = "A��o";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbdestino_Internalname, "Caption", lblTbdestino_Caption);
               /* Optimized group. */
               /* Using cursor H00IZ11 */
               pr_default.execute(6, new Object[] {AV9Codigo});
               cV103QtdNaoAcatado = H00IZ11_AV103QtdNaoAcatado[0];
               pr_default.close(6);
               AV103QtdNaoAcatado = (short)(AV103QtdNaoAcatado+cV103QtdNaoAcatado*1);
               /* End optimized group. */
            }
            else
            {
               if ( AV154EhSolicitacaoSS )
               {
                  Form.Caption = "Encaminhar SS "+StringUtil.Trim( StringUtil.Str( (decimal)(AV123ContagemResultado_SS), 8, 0));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
                  lblTbobservacoes_Caption = "Desc. do Respons�vel";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbobservacoes_Internalname, "Caption", lblTbobservacoes_Caption);
               }
               else
               {
                  Form.Caption = "Tramita��o da OS "+AV76Demanda+" ("+AV67Contratada_Sigla+")";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
                  /* Using cursor H00IZ12 */
                  pr_default.execute(7, new Object[] {AV9Codigo});
                  while ( (pr_default.getStatus(7) != 101) )
                  {
                     A758CheckList_Codigo = H00IZ12_A758CheckList_Codigo[0];
                     A1230CheckList_De = H00IZ12_A1230CheckList_De[0];
                     n1230CheckList_De = H00IZ12_n1230CheckList_De[0];
                     A762ContagemResultadoChckLst_Cumpre = H00IZ12_A762ContagemResultadoChckLst_Cumpre[0];
                     A1868ContagemResultadoChckLst_OSCod = H00IZ12_A1868ContagemResultadoChckLst_OSCod[0];
                     A1230CheckList_De = H00IZ12_A1230CheckList_De[0];
                     n1230CheckList_De = H00IZ12_n1230CheckList_De[0];
                     AV41AnalisePendencia = true;
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                     pr_default.readNext(7);
                  }
                  pr_default.close(7);
                  /* Using cursor H00IZ13 */
                  pr_default.execute(8, new Object[] {AV9Codigo});
                  while ( (pr_default.getStatus(8) != 101) )
                  {
                     A892LogResponsavel_DemandaCod = H00IZ13_A892LogResponsavel_DemandaCod[0];
                     n892LogResponsavel_DemandaCod = H00IZ13_n892LogResponsavel_DemandaCod[0];
                     A1177LogResponsavel_Prazo = H00IZ13_A1177LogResponsavel_Prazo[0];
                     n1177LogResponsavel_Prazo = H00IZ13_n1177LogResponsavel_Prazo[0];
                     A896LogResponsavel_Owner = H00IZ13_A896LogResponsavel_Owner[0];
                     A1797LogResponsavel_Codigo = H00IZ13_A1797LogResponsavel_Codigo[0];
                     AV50PrazoInicial = A1177LogResponsavel_Prazo;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50PrazoInicial", context.localUtil.TToC( AV50PrazoInicial, 8, 5, 0, 3, "/", ":", " "));
                     AV49Emissor = A896LogResponsavel_Owner;
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                     pr_default.readNext(8);
                  }
                  pr_default.close(8);
               }
            }
         }
         AV39PrazoResposta = DateTimeUtil.ResetTime(AV50PrazoInicial);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39PrazoResposta", context.localUtil.Format(AV39PrazoResposta, "99/99/99"));
         edtavPrazoresposta_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPrazoresposta_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPrazoresposta_Visible), 5, 0)));
         tblTblinvisible_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblinvisible_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblinvisible_Visible), 5, 0)));
         tblTblnaoacata_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblnaoacata_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblnaoacata_Visible), 5, 0)));
         lblTbnaocnf_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbnaocnf_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbnaocnf_Visible), 5, 0)));
         dynavNaoconformidade_codigo.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavNaoconformidade_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavNaoconformidade_codigo.Visible), 5, 0)));
         tblTblanexos_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblanexos_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblanexos_Visible), 5, 0)));
         lblTbprazo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbprazo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbprazo_Visible), 5, 0)));
         radavDeferido.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, radavDeferido_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(radavDeferido.Visible), 5, 0)));
         chkavRemetenteinterno.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavRemetenteinterno_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavRemetenteinterno.Visible), 5, 0)));
         bttBtnenter_Visible = (AV53WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Visible), 5, 0)));
         bttBtnartefatos_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnartefatos_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnartefatos_Visible), 5, 0)));
         lblTextblockcontrato_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontrato_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcontrato_Visible), 5, 0)));
         cmbavContrato.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContrato_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContrato.Visible), 5, 0)));
         tblTblcriaros_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcriaros_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcriaros_Visible), 5, 0)));
         lblTbcontratofs_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbcontratofs_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbcontratofs_Visible), 5, 0)));
         dynavContratofs.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratofs_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContratofs.Visible), 5, 0)));
         lblTbdescricao_Visible = (((StringUtil.Len( AV140Descricao)>0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbdescricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbdescricao_Visible), 5, 0)));
         edtavDescricao_Visible = (((StringUtil.Len( AV140Descricao)>0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDescricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDescricao_Visible), 5, 0)));
         AV46Deferido = "R";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46Deferido", AV46Deferido);
         AV150i = (short)(AV53WWPContext.gxTpr_Screen_width*0.30m);
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         lblTbjava_Caption = "<script type='text/javascript'> document.body.style.overflow = 'hidden';"+"document.getElementById(\"vOBSERVACAO\").style.width = \""+StringUtil.Trim( StringUtil.Str( (decimal)(AV150i), 4, 0))+"px\";"+"document.getElementById(\"vDESCRICAO\").style.wordWrap = \"break-word\";"+"document.getElementById(\"vDESCRICAO\").style.width = \""+StringUtil.Trim( StringUtil.Str( (decimal)(AV150i), 4, 0))+"px\"; </script>";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
         if ( StringUtil.StrCmp(AV114Caller, "Gestao") == 0 )
         {
         }
         else if ( AV97EmDivergencia )
         {
            if ( AV61UsuarioDaPrestadora )
            {
               GXt_boolean11 = AV101TemGestorOrigem;
               new prc_temgestor(context ).execute( ref  AV115ContratoOrigem_Codigo, out  GXt_boolean11) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV115ContratoOrigem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV115ContratoOrigem_Codigo), 6, 0)));
               AV101TemGestorOrigem = GXt_boolean11;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101TemGestorOrigem", AV101TemGestorOrigem);
            }
            if ( ( StringUtil.StrCmp(AV16StatusDemanda, "B") == 0 ) || AV53WWPContext.gxTpr_Userehcontratante || ( AV61UsuarioDaPrestadora && ( (0==AV59OsVinculada) || ! AV101TemGestorOrigem ) ) )
            {
               if ( ( AV103QtdNaoAcatado /  ( decimal )( 2 ) == Convert.ToDecimal( NumberUtil.Int( (long)(AV103QtdNaoAcatado/ (decimal)(2))) )) )
               {
                  if ( (0==AV66ContratadaOrigem_Codigo) )
                  {
                     AV63NewPFB = AV57PFB;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63NewPFB", StringUtil.LTrim( StringUtil.Str( AV63NewPFB, 14, 5)));
                     AV64NewPFL = AV58PFL;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64NewPFL", StringUtil.LTrim( StringUtil.Str( AV64NewPFL, 14, 5)));
                     /* Execute user subroutine: 'CARREGATRATAMENTOABERTAS' */
                     S132 ();
                     if (returnInSub) return;
                  }
                  else
                  {
                     AV63NewPFB = AV70PFBOF;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63NewPFB", StringUtil.LTrim( StringUtil.Str( AV63NewPFB, 14, 5)));
                     AV64NewPFL = AV71PFLOF;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64NewPFL", StringUtil.LTrim( StringUtil.Str( AV64NewPFL, 14, 5)));
                     /* Execute user subroutine: 'CARREGATRATAMENTOSTANDBY' */
                     S142 ();
                     if (returnInSub) return;
                  }
               }
               else
               {
                  AV63NewPFB = AV57PFB;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63NewPFB", StringUtil.LTrim( StringUtil.Str( AV63NewPFB, 14, 5)));
                  AV64NewPFL = AV58PFL;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64NewPFL", StringUtil.LTrim( StringUtil.Str( AV64NewPFL, 14, 5)));
                  /* Execute user subroutine: 'CARREGATRATAMENTOABERTAS' */
                  S132 ();
                  if (returnInSub) return;
               }
               AV104OldPFB = AV63NewPFB;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV104OldPFB", StringUtil.LTrim( StringUtil.Str( AV104OldPFB, 14, 5)));
               AV105OldPFL = AV64NewPFL;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105OldPFL", StringUtil.LTrim( StringUtil.Str( AV105OldPFL, 14, 5)));
            }
            else if ( StringUtil.StrCmp(AV16StatusDemanda, "A") == 0 )
            {
               /* Execute user subroutine: 'CARREGATRATAMENTOABERTAS' */
               S132 ();
               if (returnInSub) return;
            }
         }
         else if ( AV154EhSolicitacaoSS && ( ( AV53WWPContext.gxTpr_Userid != AV128Owner_Codigo ) || ( AV53WWPContext.gxTpr_Userid == AV148Servico_Responsavel ) ) )
         {
            /* Execute user subroutine: 'CARREGADESTINOS' */
            S152 ();
            if (returnInSub) return;
         }
         else
         {
            /* Execute user subroutine: 'CARREGAUSUARIOS' */
            S162 ();
            if (returnInSub) return;
         }
         AV30WebSession.Remove("ArquivosEvd");
      }

      protected void E12IZ2( )
      {
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV53WWPContext) ;
         AV31AreaTrabalho_Codigo = AV53WWPContext.gxTpr_Areatrabalho_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31AreaTrabalho_Codigo), 6, 0)));
         AV24Contratada_Codigo = AV53WWPContext.gxTpr_Contratada_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Contratada_Codigo), 6, 0)));
         AV67Contratada_Sigla = AV53WWPContext.gxTpr_Contratada_pessoanom;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67Contratada_Sigla", AV67Contratada_Sigla);
         AV40Contratante_Codigo = AV53WWPContext.gxTpr_Contratante_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40Contratante_Codigo), 6, 0)));
         AV8UserEhContratante = AV53WWPContext.gxTpr_Userehcontratante;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8UserEhContratante", AV8UserEhContratante);
         AV12UserId = AV53WWPContext.gxTpr_Userid;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12UserId), 6, 0)));
         AV114Caller = AV30WebSession.Get("Caller");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV114Caller", AV114Caller);
         new geralog(context ).execute( ref  AV191Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV191Pgmname", AV191Pgmname);
         GXt_char10 = "-----------------Event Refresh -----------------------";
         new geralog(context ).execute( ref  GXt_char10) ;
         GXt_char9 = "&AreaTrabalho_Codigo = " + context.localUtil.Format( (decimal)(AV31AreaTrabalho_Codigo), "ZZZZZ9") + " - " + AV53WWPContext.gxTpr_Areatrabalho_descricao;
         new geralog(context ).execute( ref  GXt_char9) ;
         GXt_char8 = "&Contratada_Codigo = " + context.localUtil.Format( (decimal)(AV24Contratada_Codigo), "ZZZZZ9") + " - " + context.localUtil.Format( (decimal)(AV53WWPContext.gxTpr_Contratada_pessoacod), "ZZZZZ9") + " - " + AV53WWPContext.gxTpr_Contratada_pessoanom;
         new geralog(context ).execute( ref  GXt_char8) ;
         GXt_char7 = "&Contratada_Sigla = " + AV67Contratada_Sigla;
         new geralog(context ).execute( ref  GXt_char7) ;
         GXt_char6 = "&Contratante_Codigo = " + context.localUtil.Format( (decimal)(AV40Contratante_Codigo), "ZZZZZ9") + " - " + AV53WWPContext.gxTpr_Contratante_nomefantasia;
         new geralog(context ).execute( ref  GXt_char6) ;
         GXt_char5 = "&UserEhContratante = " + StringUtil.BoolToStr( AV8UserEhContratante);
         new geralog(context ).execute( ref  GXt_char5) ;
         GXt_char4 = "&UserId = " + context.localUtil.Format( (decimal)(AV12UserId), "ZZZZZ9");
         new geralog(context ).execute( ref  GXt_char4) ;
         GXt_char3 = "&Caller =  " + AV114Caller;
         new geralog(context ).execute( ref  GXt_char3) ;
         GXt_char2 = "&&PodeAtribuirDmn = " + StringUtil.BoolToStr( AV11PodeAtribuirDmn);
         new geralog(context ).execute( ref  GXt_char2) ;
         GXt_char1 = "----------------- Event Refresh -----------------------";
         new geralog(context ).execute( ref  GXt_char1) ;
         new geralog(context ).execute( ref  AV191Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV191Pgmname", AV191Pgmname);
         bttBtnartefatos_Caption = "Artefatos ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV145Sdt_Artefatos.Count), 9, 0))+")";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnartefatos_Internalname, "Caption", bttBtnartefatos_Caption);
         bttBtnenter_Visible = (AV53WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Visible), 5, 0)));
         AV145Sdt_Artefatos.FromXml(AV30WebSession.Get("Artefatos"), "SDT_ArtefatosCollection");
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV53WWPContext", AV53WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV145Sdt_Artefatos", AV145Sdt_Artefatos);
      }

      protected void S132( )
      {
         /* 'CARREGATRATAMENTOABERTAS' Routine */
         new geralog(context ).execute( ref  AV191Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV191Pgmname", AV191Pgmname);
         GXt_char10 = "-----------------Sub CarregaTratamentoAbertas -----------------------";
         new geralog(context ).execute( ref  GXt_char10) ;
         GXt_char9 = "&AreaTrabalho_Codigo = " + context.localUtil.Format( (decimal)(AV31AreaTrabalho_Codigo), "ZZZZZ9") + " - " + AV53WWPContext.gxTpr_Areatrabalho_descricao;
         new geralog(context ).execute( ref  GXt_char9) ;
         GXt_char8 = "&Contratada_Codigo = " + context.localUtil.Format( (decimal)(AV24Contratada_Codigo), "ZZZZZ9") + " - " + context.localUtil.Format( (decimal)(AV53WWPContext.gxTpr_Contratada_pessoacod), "ZZZZZ9") + " - " + AV53WWPContext.gxTpr_Contratada_pessoanom;
         new geralog(context ).execute( ref  GXt_char8) ;
         GXt_char7 = "&Contratada_Sigla = " + AV67Contratada_Sigla;
         new geralog(context ).execute( ref  GXt_char7) ;
         GXt_char6 = "&Contratante_Codigo = " + context.localUtil.Format( (decimal)(AV40Contratante_Codigo), "ZZZZZ9") + " - " + AV53WWPContext.gxTpr_Contratante_nomefantasia;
         new geralog(context ).execute( ref  GXt_char6) ;
         GXt_char5 = "&UserEhContratante = " + StringUtil.BoolToStr( AV8UserEhContratante);
         new geralog(context ).execute( ref  GXt_char5) ;
         GXt_char4 = "&UserId = " + context.localUtil.Format( (decimal)(AV12UserId), "ZZZZZ9");
         new geralog(context ).execute( ref  GXt_char4) ;
         GXt_char3 = "&Caller =  " + AV114Caller;
         new geralog(context ).execute( ref  GXt_char3) ;
         GXt_char2 = "&&PodeAtribuirDmn = " + StringUtil.BoolToStr( AV11PodeAtribuirDmn);
         new geralog(context ).execute( ref  GXt_char2) ;
         GXt_char1 = "----------------- Sub CarregaTratamentoAbertas -----------------------";
         new geralog(context ).execute( ref  GXt_char1) ;
         new geralog(context ).execute( ref  AV191Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV191Pgmname", AV191Pgmname);
         cmbavUsuario_codigo.addItem("0", "(Nenhuma)", 0);
         cmbavUsuario_codigo.addItem("800001", "Acatar Bruto: "+StringUtil.Trim( StringUtil.Str( AV70PFBOF, 14, 3))+" Liquido: "+StringUtil.Trim( StringUtil.Str( AV71PFLOF, 14, 3)), 0);
         cmbavUsuario_codigo.addItem("800002", "N�o Acatar Bruto: "+StringUtil.Trim( StringUtil.Str( AV70PFBOF, 14, 3))+" Liquido: "+StringUtil.Trim( StringUtil.Str( AV71PFLOF, 14, 3)), 0);
         if ( StringUtil.StrCmp(AV16StatusDemanda, "A") == 0 )
         {
            cmbavUsuario_codigo.addItem("800003", "Solicitar reuni�o para tratar a diverg�ncia", 0);
         }
         if ( AV11PodeAtribuirDmn )
         {
            GXt_boolean11 = AV155NotPreferencial;
            GXt_int12 = AV53WWPContext.gxTpr_Contratada_codigo;
            new prc_atendimentopreferencial(context ).execute( ref  GXt_int12, out  GXt_boolean11) ;
            AV53WWPContext.gxTpr_Contratada_codigo = GXt_int12;
            AV155NotPreferencial = (bool)(!GXt_boolean11);
            /* Using cursor H00IZ14 */
            pr_default.execute(9, new Object[] {AV24Contratada_Codigo, AV47ResponsavelAtual});
            while ( (pr_default.getStatus(9) != 101) )
            {
               A70ContratadaUsuario_UsuarioPessoaCod = H00IZ14_A70ContratadaUsuario_UsuarioPessoaCod[0];
               n70ContratadaUsuario_UsuarioPessoaCod = H00IZ14_n70ContratadaUsuario_UsuarioPessoaCod[0];
               A66ContratadaUsuario_ContratadaCod = H00IZ14_A66ContratadaUsuario_ContratadaCod[0];
               A69ContratadaUsuario_UsuarioCod = H00IZ14_A69ContratadaUsuario_UsuarioCod[0];
               A71ContratadaUsuario_UsuarioPessoaNom = H00IZ14_A71ContratadaUsuario_UsuarioPessoaNom[0];
               n71ContratadaUsuario_UsuarioPessoaNom = H00IZ14_n71ContratadaUsuario_UsuarioPessoaNom[0];
               A1394ContratadaUsuario_UsuarioAtivo = H00IZ14_A1394ContratadaUsuario_UsuarioAtivo[0];
               n1394ContratadaUsuario_UsuarioAtivo = H00IZ14_n1394ContratadaUsuario_UsuarioAtivo[0];
               A1908Usuario_DeFerias = H00IZ14_A1908Usuario_DeFerias[0];
               n1908Usuario_DeFerias = H00IZ14_n1908Usuario_DeFerias[0];
               A70ContratadaUsuario_UsuarioPessoaCod = H00IZ14_A70ContratadaUsuario_UsuarioPessoaCod[0];
               n70ContratadaUsuario_UsuarioPessoaCod = H00IZ14_n70ContratadaUsuario_UsuarioPessoaCod[0];
               A1394ContratadaUsuario_UsuarioAtivo = H00IZ14_A1394ContratadaUsuario_UsuarioAtivo[0];
               n1394ContratadaUsuario_UsuarioAtivo = H00IZ14_n1394ContratadaUsuario_UsuarioAtivo[0];
               A1908Usuario_DeFerias = H00IZ14_A1908Usuario_DeFerias[0];
               n1908Usuario_DeFerias = H00IZ14_n1908Usuario_DeFerias[0];
               A71ContratadaUsuario_UsuarioPessoaNom = H00IZ14_A71ContratadaUsuario_UsuarioPessoaNom[0];
               n71ContratadaUsuario_UsuarioPessoaNom = H00IZ14_n71ContratadaUsuario_UsuarioPessoaNom[0];
               /* Using cursor H00IZ15 */
               pr_default.execute(10, new Object[] {A69ContratadaUsuario_UsuarioCod, AV22Servico_Codigo});
               while ( (pr_default.getStatus(10) != 101) )
               {
                  A828UsuarioServicos_UsuarioCod = H00IZ15_A828UsuarioServicos_UsuarioCod[0];
                  A829UsuarioServicos_ServicoCod = H00IZ15_A829UsuarioServicos_ServicoCod[0];
                  if ( AV155NotPreferencial || new prc_disponivel(context).executeUdp(  A69ContratadaUsuario_UsuarioCod) )
                  {
                     cmbavUsuario_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0)), A71ContratadaUsuario_UsuarioPessoaNom, 0);
                     GXt_char10 = "Carregou aqui 05";
                     new geralog(context ).execute( ref  GXt_char10) ;
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                  }
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(10);
               pr_default.readNext(9);
            }
            pr_default.close(9);
         }
      }

      protected void S142( )
      {
         /* 'CARREGATRATAMENTOSTANDBY' Routine */
         new geralog(context ).execute( ref  AV191Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV191Pgmname", AV191Pgmname);
         GXt_char9 = "----------------- SUB CarregaTratamentoStandby -----------------------";
         new geralog(context ).execute( ref  GXt_char9) ;
         GXt_char8 = "&AreaTrabalho_Codigo = " + context.localUtil.Format( (decimal)(AV31AreaTrabalho_Codigo), "ZZZZZ9") + " - " + AV53WWPContext.gxTpr_Areatrabalho_descricao;
         new geralog(context ).execute( ref  GXt_char8) ;
         GXt_char7 = "&Contratada_Codigo = " + context.localUtil.Format( (decimal)(AV24Contratada_Codigo), "ZZZZZ9") + " - " + context.localUtil.Format( (decimal)(AV53WWPContext.gxTpr_Contratada_pessoacod), "ZZZZZ9") + " - " + AV53WWPContext.gxTpr_Contratada_pessoanom;
         new geralog(context ).execute( ref  GXt_char7) ;
         GXt_char6 = "&Contratada_Sigla = " + AV67Contratada_Sigla;
         new geralog(context ).execute( ref  GXt_char6) ;
         GXt_char5 = "&Contratante_Codigo = " + context.localUtil.Format( (decimal)(AV40Contratante_Codigo), "ZZZZZ9") + " - " + AV53WWPContext.gxTpr_Contratante_nomefantasia;
         new geralog(context ).execute( ref  GXt_char5) ;
         GXt_char4 = "&UserEhContratante = " + StringUtil.BoolToStr( AV8UserEhContratante);
         new geralog(context ).execute( ref  GXt_char4) ;
         GXt_char3 = "&UserId = " + context.localUtil.Format( (decimal)(AV12UserId), "ZZZZZ9");
         new geralog(context ).execute( ref  GXt_char3) ;
         GXt_char2 = "&Caller =  " + AV114Caller;
         new geralog(context ).execute( ref  GXt_char2) ;
         GXt_char1 = "&&PodeAtribuirDmn = " + StringUtil.BoolToStr( AV11PodeAtribuirDmn);
         new geralog(context ).execute( ref  GXt_char1) ;
         GXt_char14 = "----------------- SUb CarregaTratamentoStandby -----------------------";
         new geralog(context ).execute( ref  GXt_char14) ;
         new geralog(context ).execute( ref  AV191Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV191Pgmname", AV191Pgmname);
         AV201GXLvl372 = 0;
         /* Using cursor H00IZ16 */
         pr_default.execute(11, new Object[] {AV76Demanda, AV27Prestadora_Codigo});
         while ( (pr_default.getStatus(11) != 101) )
         {
            A1118Contagem_ContratadaCod = H00IZ16_A1118Contagem_ContratadaCod[0];
            n1118Contagem_ContratadaCod = H00IZ16_n1118Contagem_ContratadaCod[0];
            A945Contagem_Demanda = H00IZ16_A945Contagem_Demanda[0];
            n945Contagem_Demanda = H00IZ16_n945Contagem_Demanda[0];
            AV201GXLvl372 = 1;
            cmbavUsuario_codigo.addItem("0", "(Tratamento da diverg�ncia no m�dulo de Mensura��o)", 0);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(11);
         if ( AV201GXLvl372 == 0 )
         {
            cmbavUsuario_codigo.addItem("0", "(Nenhuma)", 0);
            GXt_int12 = (AV61UsuarioDaPrestadora ? AV66ContratadaOrigem_Codigo : AV27Prestadora_Codigo);
            GXt_int15 = 0;
            if ( AV53WWPContext.gxTpr_Userehcontratante || ! new prc_usaosistema(context).executeUdp( ref  GXt_int12, ref  GXt_int15) )
            {
               if ( AV20Selecionadas.Count == 1 )
               {
                  if ( AV53WWPContext.gxTpr_Userehcontratante )
                  {
                     cmbavUsuario_codigo.addItem("800004", "A "+AV110Origem_Sigla+" Acatou Bruto: "+StringUtil.Trim( StringUtil.Str( AV70PFBOF, 14, 3))+" Liquido: "+StringUtil.Trim( StringUtil.Str( AV71PFLOF, 14, 3)), 0);
                     cmbavUsuario_codigo.addItem("800005", "A "+AV110Origem_Sigla+" N�o Acatou Bruto: "+StringUtil.Trim( StringUtil.Str( AV70PFBOF, 14, 3))+" Liquido: "+StringUtil.Trim( StringUtil.Str( AV71PFLOF, 14, 3)), 0);
                  }
                  else
                  {
                     cmbavUsuario_codigo.addItem("800004", "A "+AV110Origem_Sigla+" Acatou Bruto: "+StringUtil.Trim( StringUtil.Str( AV57PFB, 14, 3))+" Liquido: "+StringUtil.Trim( StringUtil.Str( AV58PFL, 14, 3)), 0);
                     cmbavUsuario_codigo.addItem("800005", "A "+AV110Origem_Sigla+" N�o Acatou Bruto: "+StringUtil.Trim( StringUtil.Str( AV57PFB, 14, 3))+" Liquido: "+StringUtil.Trim( StringUtil.Str( AV58PFL, 14, 3)), 0);
                  }
               }
               else
               {
                  cmbavUsuario_codigo.addItem("800004", "A "+AV110Origem_Sigla+" Acatou todas as "+StringUtil.Trim( StringUtil.Str( (decimal)(AV20Selecionadas.Count), 9, 0))+" OS", 0);
                  cmbavUsuario_codigo.addItem("800005", "A "+AV110Origem_Sigla+" N�o Acatou nenhuma das "+StringUtil.Trim( StringUtil.Str( (decimal)(AV20Selecionadas.Count), 9, 0))+" OS", 0);
               }
            }
            cmbavUsuario_codigo.addItem("800003", "Solicitar reuni�o para tratar a diverg�ncia", 0);
         }
         if ( AV11PodeAtribuirDmn )
         {
            pr_default.dynParam(12, new Object[]{ new Object[]{
                                                 AV11PodeAtribuirDmn ,
                                                 A69ContratadaUsuario_UsuarioCod ,
                                                 AV12UserId ,
                                                 AV47ResponsavelAtual ,
                                                 A1908Usuario_DeFerias ,
                                                 A1394ContratadaUsuario_UsuarioAtivo ,
                                                 AV24Contratada_Codigo ,
                                                 A66ContratadaUsuario_ContratadaCod },
                                                 new int[] {
                                                 TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            /* Using cursor H00IZ17 */
            pr_default.execute(12, new Object[] {AV24Contratada_Codigo, AV47ResponsavelAtual, AV12UserId});
            while ( (pr_default.getStatus(12) != 101) )
            {
               A70ContratadaUsuario_UsuarioPessoaCod = H00IZ17_A70ContratadaUsuario_UsuarioPessoaCod[0];
               n70ContratadaUsuario_UsuarioPessoaCod = H00IZ17_n70ContratadaUsuario_UsuarioPessoaCod[0];
               A69ContratadaUsuario_UsuarioCod = H00IZ17_A69ContratadaUsuario_UsuarioCod[0];
               A71ContratadaUsuario_UsuarioPessoaNom = H00IZ17_A71ContratadaUsuario_UsuarioPessoaNom[0];
               n71ContratadaUsuario_UsuarioPessoaNom = H00IZ17_n71ContratadaUsuario_UsuarioPessoaNom[0];
               A1394ContratadaUsuario_UsuarioAtivo = H00IZ17_A1394ContratadaUsuario_UsuarioAtivo[0];
               n1394ContratadaUsuario_UsuarioAtivo = H00IZ17_n1394ContratadaUsuario_UsuarioAtivo[0];
               A1908Usuario_DeFerias = H00IZ17_A1908Usuario_DeFerias[0];
               n1908Usuario_DeFerias = H00IZ17_n1908Usuario_DeFerias[0];
               A66ContratadaUsuario_ContratadaCod = H00IZ17_A66ContratadaUsuario_ContratadaCod[0];
               A70ContratadaUsuario_UsuarioPessoaCod = H00IZ17_A70ContratadaUsuario_UsuarioPessoaCod[0];
               n70ContratadaUsuario_UsuarioPessoaCod = H00IZ17_n70ContratadaUsuario_UsuarioPessoaCod[0];
               A1394ContratadaUsuario_UsuarioAtivo = H00IZ17_A1394ContratadaUsuario_UsuarioAtivo[0];
               n1394ContratadaUsuario_UsuarioAtivo = H00IZ17_n1394ContratadaUsuario_UsuarioAtivo[0];
               A1908Usuario_DeFerias = H00IZ17_A1908Usuario_DeFerias[0];
               n1908Usuario_DeFerias = H00IZ17_n1908Usuario_DeFerias[0];
               A71ContratadaUsuario_UsuarioPessoaNom = H00IZ17_A71ContratadaUsuario_UsuarioPessoaNom[0];
               n71ContratadaUsuario_UsuarioPessoaNom = H00IZ17_n71ContratadaUsuario_UsuarioPessoaNom[0];
               /* Using cursor H00IZ18 */
               pr_default.execute(13, new Object[] {A69ContratadaUsuario_UsuarioCod, AV22Servico_Codigo});
               while ( (pr_default.getStatus(13) != 101) )
               {
                  A828UsuarioServicos_UsuarioCod = H00IZ18_A828UsuarioServicos_UsuarioCod[0];
                  A829UsuarioServicos_ServicoCod = H00IZ18_A829UsuarioServicos_ServicoCod[0];
                  cmbavUsuario_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0)), A71ContratadaUsuario_UsuarioPessoaNom, 0);
                  GXt_char14 = "Carregou aqqui 01";
                  new geralog(context ).execute( ref  GXt_char14) ;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(13);
               pr_default.readNext(12);
            }
            pr_default.close(12);
         }
         new geralog(context ).execute( ref  AV191Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV191Pgmname", AV191Pgmname);
         new geralog(context ).execute( ref  AV191Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV191Pgmname", AV191Pgmname);
      }

      protected void S152( )
      {
         /* 'CARREGADESTINOS' Routine */
         cmbavUsuario_codigo.removeAllItems();
         cmbavUsuario_codigo.addItem("0", "(Nenhum)", 0);
         if ( AV152RetornaSS )
         {
            /* Execute user subroutine: 'DADOSOWNER' */
            S332 ();
            if (returnInSub) return;
            if ( ( AV128Owner_Codigo != AV153Remetente ) && AV151RemetenteInterno )
            {
               cmbavUsuario_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(400000+AV153Remetente), 6, 0)), AV149Remetente_Nome, 0);
               AV5Usuario_Codigo = (int)(400000+AV153Remetente);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Usuario_Codigo), 6, 0)));
            }
            if ( AV53WWPContext.gxTpr_Userid != AV128Owner_Codigo )
            {
               cmbavUsuario_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(600000+AV128Owner_Codigo), 6, 0)), "Solicitante "+AV129Owner_Nome, 0);
               AV5Usuario_Codigo = (int)(600000+AV128Owner_Codigo);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Usuario_Codigo), 6, 0)));
            }
         }
         else
         {
            if ( AV151RemetenteInterno )
            {
               cmbavUsuario_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(400000+AV153Remetente), 6, 0)), AV149Remetente_Nome, 0);
               AV5Usuario_Codigo = (int)(400000+AV153Remetente);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Usuario_Codigo), 6, 0)));
            }
         }
         cmbavUsuario_codigo.addItem("700000", "Criar uma nova OS", 0);
         if ( cmbavUsuario_codigo.ItemCount > 2 )
         {
            AV5Usuario_Codigo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Usuario_Codigo), 6, 0)));
         }
         else
         {
            AV5Usuario_Codigo = 700000;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Usuario_Codigo), 6, 0)));
            tblTblcriaros_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcriaros_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcriaros_Visible), 5, 0)));
            bttBtnenter_Caption = "Criar OS";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Caption", bttBtnenter_Caption);
            /* Execute user subroutine: 'CARREGAGRUPOSDESERVICOS' */
            S172 ();
            if (returnInSub) return;
         }
      }

      protected void S162( )
      {
         /* 'CARREGAUSUARIOS' Routine */
         new geralog(context ).execute( ref  AV191Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV191Pgmname", AV191Pgmname);
         GXt_char14 = "----------------- SUB CarregaUsuarios -----------------------";
         new geralog(context ).execute( ref  GXt_char14) ;
         GXt_char10 = "&AreaTrabalho_Codigo = " + context.localUtil.Format( (decimal)(AV31AreaTrabalho_Codigo), "ZZZZZ9") + " - " + AV53WWPContext.gxTpr_Areatrabalho_descricao;
         new geralog(context ).execute( ref  GXt_char10) ;
         GXt_char9 = "&Contratada_Codigo = " + context.localUtil.Format( (decimal)(AV24Contratada_Codigo), "ZZZZZ9") + " - " + context.localUtil.Format( (decimal)(AV53WWPContext.gxTpr_Contratada_pessoacod), "ZZZZZ9") + " - " + AV53WWPContext.gxTpr_Contratada_pessoanom;
         new geralog(context ).execute( ref  GXt_char9) ;
         GXt_char8 = "&Contratada_Sigla = " + AV67Contratada_Sigla;
         new geralog(context ).execute( ref  GXt_char8) ;
         GXt_char7 = "&Contratante_Codigo = " + context.localUtil.Format( (decimal)(AV40Contratante_Codigo), "ZZZZZ9") + " - " + AV53WWPContext.gxTpr_Contratante_nomefantasia;
         new geralog(context ).execute( ref  GXt_char7) ;
         GXt_char6 = "&UserEhContratante = " + StringUtil.BoolToStr( AV8UserEhContratante);
         new geralog(context ).execute( ref  GXt_char6) ;
         GXt_char5 = "&UserId = " + context.localUtil.Format( (decimal)(AV12UserId), "ZZZZZ9");
         new geralog(context ).execute( ref  GXt_char5) ;
         GXt_char4 = "&Caller =  " + AV114Caller;
         new geralog(context ).execute( ref  GXt_char4) ;
         GXt_char3 = "&&PodeAtribuirDmn = " + StringUtil.BoolToStr( AV11PodeAtribuirDmn);
         new geralog(context ).execute( ref  GXt_char3) ;
         GXt_char2 = "----------------- SUb CarregaUsuarios -----------------------";
         new geralog(context ).execute( ref  GXt_char2) ;
         new geralog(context ).execute( ref  AV191Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV191Pgmname", AV191Pgmname);
         cmbavUsuario_codigo.removeAllItems();
         cmbavUsuario_codigo.addItem("0", "(Nenhum)", 0);
         if ( ( AV53WWPContext.gxTpr_Userid != AV153Remetente ) && AV151RemetenteInterno )
         {
            cmbavUsuario_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(400000+AV153Remetente), 6, 0)), AV149Remetente_Nome, 0);
            AV5Usuario_Codigo = (int)(400000+AV153Remetente);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Usuario_Codigo), 6, 0)));
         }
         if ( AV8UserEhContratante )
         {
            if ( AV154EhSolicitacaoSS )
            {
               if ( AV53WWPContext.gxTpr_Userid == AV128Owner_Codigo )
               {
                  /* Using cursor H00IZ19 */
                  pr_default.execute(14, new Object[] {AV120ServicoSS_Codigo});
                  while ( (pr_default.getStatus(14) != 101) )
                  {
                     A155Servico_Codigo = H00IZ19_A155Servico_Codigo[0];
                     GXt_int15 = A1551Servico_Responsavel;
                     new prc_getservico_responsavel(context ).execute( ref  A155Servico_Codigo, out  GXt_int15) ;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
                     A1551Servico_Responsavel = GXt_int15;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1551Servico_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1551Servico_Responsavel), 6, 0)));
                     if ( A1551Servico_Responsavel != AV53WWPContext.gxTpr_Userid )
                     {
                        /* Using cursor H00IZ20 */
                        pr_default.execute(15, new Object[] {A1551Servico_Responsavel});
                        while ( (pr_default.getStatus(15) != 101) )
                        {
                           A57Usuario_PessoaCod = H00IZ20_A57Usuario_PessoaCod[0];
                           A1Usuario_Codigo = H00IZ20_A1Usuario_Codigo[0];
                           A58Usuario_PessoaNom = H00IZ20_A58Usuario_PessoaNom[0];
                           n58Usuario_PessoaNom = H00IZ20_n58Usuario_PessoaNom[0];
                           A58Usuario_PessoaNom = H00IZ20_A58Usuario_PessoaNom[0];
                           n58Usuario_PessoaNom = H00IZ20_n58Usuario_PessoaNom[0];
                           cmbavUsuario_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(500000+A1Usuario_Codigo), 6, 0)), "Respons�vel do servi�o: "+A58Usuario_PessoaNom, 0);
                           AV5Usuario_Codigo = (int)(500000+A1Usuario_Codigo);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Usuario_Codigo), 6, 0)));
                           /* Exit For each command. Update data (if necessary), close cursors & exit. */
                           if (true) break;
                           /* Exiting from a For First loop. */
                           if (true) break;
                        }
                        pr_default.close(15);
                     }
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                     /* Exiting from a For First loop. */
                     if (true) break;
                  }
                  pr_default.close(14);
               }
            }
            else
            {
               if ( ! ( StringUtil.StrCmp(AV114Caller, "Gestao") == 0 ) )
               {
                  /* Using cursor H00IZ21 */
                  pr_default.execute(16, new Object[] {AV27Prestadora_Codigo, AV66ContratadaOrigem_Codigo});
                  while ( (pr_default.getStatus(16) != 101) )
                  {
                     A1078ContratoGestor_ContratoCod = H00IZ21_A1078ContratoGestor_ContratoCod[0];
                     A1136ContratoGestor_ContratadaCod = H00IZ21_A1136ContratoGestor_ContratadaCod[0];
                     n1136ContratoGestor_ContratadaCod = H00IZ21_n1136ContratoGestor_ContratadaCod[0];
                     A1223ContratoGestor_ContratadaSigla = H00IZ21_A1223ContratoGestor_ContratadaSigla[0];
                     n1223ContratoGestor_ContratadaSigla = H00IZ21_n1223ContratoGestor_ContratadaSigla[0];
                     A1136ContratoGestor_ContratadaCod = H00IZ21_A1136ContratoGestor_ContratadaCod[0];
                     n1136ContratoGestor_ContratadaCod = H00IZ21_n1136ContratoGestor_ContratadaCod[0];
                     A1223ContratoGestor_ContratadaSigla = H00IZ21_A1223ContratoGestor_ContratadaSigla[0];
                     n1223ContratoGestor_ContratadaSigla = H00IZ21_n1223ContratoGestor_ContratadaSigla[0];
                     cmbavUsuario_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(900000+A1136ContratoGestor_ContratadaCod), 6, 0)), A1223ContratoGestor_ContratadaSigla, 0);
                     AV5Usuario_Codigo = (int)(900000+A1136ContratoGestor_ContratadaCod);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Usuario_Codigo), 6, 0)));
                     GXt_char14 = "Carregou aqui 02";
                     new geralog(context ).execute( ref  GXt_char14) ;
                     pr_default.readNext(16);
                  }
                  pr_default.close(16);
               }
               pr_default.dynParam(17, new Object[]{ new Object[]{
                                                    AV11PodeAtribuirDmn ,
                                                    A60ContratanteUsuario_UsuarioCod ,
                                                    AV12UserId ,
                                                    AV47ResponsavelAtual ,
                                                    A1908Usuario_DeFerias ,
                                                    A54Usuario_Ativo ,
                                                    A63ContratanteUsuario_ContratanteCod ,
                                                    AV40Contratante_Codigo },
                                                    new int[] {
                                                    TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                    }
               });
               /* Using cursor H00IZ22 */
               pr_default.execute(17, new Object[] {AV47ResponsavelAtual, AV40Contratante_Codigo, AV12UserId});
               while ( (pr_default.getStatus(17) != 101) )
               {
                  A61ContratanteUsuario_UsuarioPessoaCod = H00IZ22_A61ContratanteUsuario_UsuarioPessoaCod[0];
                  n61ContratanteUsuario_UsuarioPessoaCod = H00IZ22_n61ContratanteUsuario_UsuarioPessoaCod[0];
                  A54Usuario_Ativo = H00IZ22_A54Usuario_Ativo[0];
                  n54Usuario_Ativo = H00IZ22_n54Usuario_Ativo[0];
                  A1908Usuario_DeFerias = H00IZ22_A1908Usuario_DeFerias[0];
                  n1908Usuario_DeFerias = H00IZ22_n1908Usuario_DeFerias[0];
                  A60ContratanteUsuario_UsuarioCod = H00IZ22_A60ContratanteUsuario_UsuarioCod[0];
                  A63ContratanteUsuario_ContratanteCod = H00IZ22_A63ContratanteUsuario_ContratanteCod[0];
                  A62ContratanteUsuario_UsuarioPessoaNom = H00IZ22_A62ContratanteUsuario_UsuarioPessoaNom[0];
                  n62ContratanteUsuario_UsuarioPessoaNom = H00IZ22_n62ContratanteUsuario_UsuarioPessoaNom[0];
                  A61ContratanteUsuario_UsuarioPessoaCod = H00IZ22_A61ContratanteUsuario_UsuarioPessoaCod[0];
                  n61ContratanteUsuario_UsuarioPessoaCod = H00IZ22_n61ContratanteUsuario_UsuarioPessoaCod[0];
                  A54Usuario_Ativo = H00IZ22_A54Usuario_Ativo[0];
                  n54Usuario_Ativo = H00IZ22_n54Usuario_Ativo[0];
                  A1908Usuario_DeFerias = H00IZ22_A1908Usuario_DeFerias[0];
                  n1908Usuario_DeFerias = H00IZ22_n1908Usuario_DeFerias[0];
                  A62ContratanteUsuario_UsuarioPessoaNom = H00IZ22_A62ContratanteUsuario_UsuarioPessoaNom[0];
                  n62ContratanteUsuario_UsuarioPessoaNom = H00IZ22_n62ContratanteUsuario_UsuarioPessoaNom[0];
                  cmbavUsuario_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0)), A62ContratanteUsuario_UsuarioPessoaNom, 0);
                  AV5Usuario_Codigo = A60ContratanteUsuario_UsuarioCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Usuario_Codigo), 6, 0)));
                  GXt_char14 = "Carregou aqui 03";
                  new geralog(context ).execute( ref  GXt_char14) ;
                  pr_default.readNext(17);
               }
               pr_default.close(17);
            }
         }
         else
         {
            if ( ! ( StringUtil.StrCmp(AV114Caller, "Gestao") == 0 ) && ! AV97EmDivergencia )
            {
               pr_default.dynParam(18, new Object[]{ new Object[]{
                                                    AV154EhSolicitacaoSS ,
                                                    A1136ContratoGestor_ContratadaCod ,
                                                    AV27Prestadora_Codigo ,
                                                    A1079ContratoGestor_UsuarioCod ,
                                                    AV128Owner_Codigo ,
                                                    A1135ContratoGestor_UsuarioEhContratante },
                                                    new int[] {
                                                    TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                                    }
               });
               /* Using cursor H00IZ23 */
               pr_default.execute(18, new Object[] {AV27Prestadora_Codigo, AV128Owner_Codigo});
               while ( (pr_default.getStatus(18) != 101) )
               {
                  A1078ContratoGestor_ContratoCod = H00IZ23_A1078ContratoGestor_ContratoCod[0];
                  A1136ContratoGestor_ContratadaCod = H00IZ23_A1136ContratoGestor_ContratadaCod[0];
                  n1136ContratoGestor_ContratadaCod = H00IZ23_n1136ContratoGestor_ContratadaCod[0];
                  A1447ContratoGestor_ContratadaAreaDes = H00IZ23_A1447ContratoGestor_ContratadaAreaDes[0];
                  n1447ContratoGestor_ContratadaAreaDes = H00IZ23_n1447ContratoGestor_ContratadaAreaDes[0];
                  A1079ContratoGestor_UsuarioCod = H00IZ23_A1079ContratoGestor_UsuarioCod[0];
                  A1446ContratoGestor_ContratadaAreaCod = H00IZ23_A1446ContratoGestor_ContratadaAreaCod[0];
                  n1446ContratoGestor_ContratadaAreaCod = H00IZ23_n1446ContratoGestor_ContratadaAreaCod[0];
                  A1136ContratoGestor_ContratadaCod = H00IZ23_A1136ContratoGestor_ContratadaCod[0];
                  n1136ContratoGestor_ContratadaCod = H00IZ23_n1136ContratoGestor_ContratadaCod[0];
                  A1446ContratoGestor_ContratadaAreaCod = H00IZ23_A1446ContratoGestor_ContratadaAreaCod[0];
                  n1446ContratoGestor_ContratadaAreaCod = H00IZ23_n1446ContratoGestor_ContratadaAreaCod[0];
                  A1447ContratoGestor_ContratadaAreaDes = H00IZ23_A1447ContratoGestor_ContratadaAreaDes[0];
                  n1447ContratoGestor_ContratadaAreaDes = H00IZ23_n1447ContratoGestor_ContratadaAreaDes[0];
                  GXt_boolean11 = A1135ContratoGestor_UsuarioEhContratante;
                  new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean11) ;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1446ContratoGestor_ContratadaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
                  A1135ContratoGestor_UsuarioEhContratante = GXt_boolean11;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1135ContratoGestor_UsuarioEhContratante", A1135ContratoGestor_UsuarioEhContratante);
                  if ( A1135ContratoGestor_UsuarioEhContratante )
                  {
                     cmbavUsuario_codigo.addItem("900000", A1447ContratoGestor_ContratadaAreaDes, 0);
                     AV5Usuario_Codigo = 900000;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Usuario_Codigo), 6, 0)));
                     GXt_char14 = "Carregou aqui 03";
                     new geralog(context ).execute( ref  GXt_char14) ;
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                  }
                  pr_default.readNext(18);
               }
               pr_default.close(18);
            }
            if ( AV38ParaContratante )
            {
               AV5Usuario_Codigo = 900000;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Usuario_Codigo), 6, 0)));
               cmbavUsuario_codigo.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUsuario_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavUsuario_codigo.Enabled), 5, 0)));
            }
            else
            {
               pr_default.dynParam(19, new Object[]{ new Object[]{
                                                    AV114Caller ,
                                                    AV11PodeAtribuirDmn ,
                                                    A66ContratadaUsuario_ContratadaCod ,
                                                    AV24Contratada_Codigo ,
                                                    AV27Prestadora_Codigo ,
                                                    A69ContratadaUsuario_UsuarioCod ,
                                                    AV12UserId ,
                                                    AV47ResponsavelAtual ,
                                                    A1908Usuario_DeFerias ,
                                                    A1394ContratadaUsuario_UsuarioAtivo },
                                                    new int[] {
                                                    TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN,
                                                    TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                                    }
               });
               /* Using cursor H00IZ24 */
               pr_default.execute(19, new Object[] {AV47ResponsavelAtual, AV24Contratada_Codigo, AV27Prestadora_Codigo, AV12UserId});
               while ( (pr_default.getStatus(19) != 101) )
               {
                  A70ContratadaUsuario_UsuarioPessoaCod = H00IZ24_A70ContratadaUsuario_UsuarioPessoaCod[0];
                  n70ContratadaUsuario_UsuarioPessoaCod = H00IZ24_n70ContratadaUsuario_UsuarioPessoaCod[0];
                  A69ContratadaUsuario_UsuarioCod = H00IZ24_A69ContratadaUsuario_UsuarioCod[0];
                  A71ContratadaUsuario_UsuarioPessoaNom = H00IZ24_A71ContratadaUsuario_UsuarioPessoaNom[0];
                  n71ContratadaUsuario_UsuarioPessoaNom = H00IZ24_n71ContratadaUsuario_UsuarioPessoaNom[0];
                  A1394ContratadaUsuario_UsuarioAtivo = H00IZ24_A1394ContratadaUsuario_UsuarioAtivo[0];
                  n1394ContratadaUsuario_UsuarioAtivo = H00IZ24_n1394ContratadaUsuario_UsuarioAtivo[0];
                  A1908Usuario_DeFerias = H00IZ24_A1908Usuario_DeFerias[0];
                  n1908Usuario_DeFerias = H00IZ24_n1908Usuario_DeFerias[0];
                  A66ContratadaUsuario_ContratadaCod = H00IZ24_A66ContratadaUsuario_ContratadaCod[0];
                  A70ContratadaUsuario_UsuarioPessoaCod = H00IZ24_A70ContratadaUsuario_UsuarioPessoaCod[0];
                  n70ContratadaUsuario_UsuarioPessoaCod = H00IZ24_n70ContratadaUsuario_UsuarioPessoaCod[0];
                  A1394ContratadaUsuario_UsuarioAtivo = H00IZ24_A1394ContratadaUsuario_UsuarioAtivo[0];
                  n1394ContratadaUsuario_UsuarioAtivo = H00IZ24_n1394ContratadaUsuario_UsuarioAtivo[0];
                  A1908Usuario_DeFerias = H00IZ24_A1908Usuario_DeFerias[0];
                  n1908Usuario_DeFerias = H00IZ24_n1908Usuario_DeFerias[0];
                  A71ContratadaUsuario_UsuarioPessoaNom = H00IZ24_A71ContratadaUsuario_UsuarioPessoaNom[0];
                  n71ContratadaUsuario_UsuarioPessoaNom = H00IZ24_n71ContratadaUsuario_UsuarioPessoaNom[0];
                  /* Using cursor H00IZ25 */
                  pr_default.execute(20, new Object[] {A69ContratadaUsuario_UsuarioCod, AV22Servico_Codigo});
                  while ( (pr_default.getStatus(20) != 101) )
                  {
                     A828UsuarioServicos_UsuarioCod = H00IZ25_A828UsuarioServicos_UsuarioCod[0];
                     A829UsuarioServicos_ServicoCod = H00IZ25_A829UsuarioServicos_ServicoCod[0];
                     cmbavUsuario_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0)), A71ContratadaUsuario_UsuarioPessoaNom, 0);
                     AV5Usuario_Codigo = A69ContratadaUsuario_UsuarioCod;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Usuario_Codigo), 6, 0)));
                     GXt_char14 = "Carregou aqui 04";
                     new geralog(context ).execute( ref  GXt_char14) ;
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                     /* Exiting from a For First loop. */
                     if (true) break;
                  }
                  pr_default.close(20);
                  pr_default.readNext(19);
               }
               pr_default.close(19);
            }
         }
         if ( cmbavUsuario_codigo.ItemCount > 2 )
         {
            AV5Usuario_Codigo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Usuario_Codigo), 6, 0)));
         }
      }

      protected void E13IZ2( )
      {
         /* Usuario_codigo_Click Routine */
         tblTblcriaros_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcriaros_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcriaros_Visible), 5, 0)));
         if ( AV5Usuario_Codigo == 900000 )
         {
            lblTbnaocnf_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbnaocnf_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbnaocnf_Visible), 5, 0)));
            dynavNaoconformidade_codigo.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavNaoconformidade_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavNaoconformidade_codigo.Visible), 5, 0)));
         }
         else if ( ( AV27Prestadora_Codigo == ( AV5Usuario_Codigo - 900000 ) ) && ( StringUtil.StrCmp(AV16StatusDemanda, "D") != 0 ) )
         {
            lblTbnaocnf_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbnaocnf_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbnaocnf_Visible), 5, 0)));
            dynavNaoconformidade_codigo.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavNaoconformidade_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavNaoconformidade_codigo.Visible), 5, 0)));
         }
         else if ( AV5Usuario_Codigo > 900000 )
         {
            lblTbnaocnf_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbnaocnf_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbnaocnf_Visible), 5, 0)));
            dynavNaoconformidade_codigo.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavNaoconformidade_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavNaoconformidade_codigo.Visible), 5, 0)));
         }
         else
         {
            lblTbnaocnf_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbnaocnf_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbnaocnf_Visible), 5, 0)));
            dynavNaoconformidade_codigo.Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavNaoconformidade_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavNaoconformidade_codigo.Visible), 5, 0)));
            AV183NaoConformidade_Codigo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV183NaoConformidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV183NaoConformidade_Codigo), 6, 0)));
         }
         if ( AV5Usuario_Codigo >= 900000 )
         {
            bttBtnenter_Caption = "Encaminhar";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Caption", bttBtnenter_Caption);
         }
         else if ( AV5Usuario_Codigo > 800000 )
         {
            bttBtnenter_Caption = "Confirmar";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Caption", bttBtnenter_Caption);
         }
         else if ( AV5Usuario_Codigo == 700000 )
         {
            tblTblcriaros_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcriaros_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcriaros_Visible), 5, 0)));
            bttBtnenter_Caption = "Criar OS";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Caption", bttBtnenter_Caption);
            /* Execute user subroutine: 'CARREGAGRUPOSDESERVICOS' */
            S172 ();
            if (returnInSub) return;
         }
         else if ( AV5Usuario_Codigo > 600000 )
         {
            bttBtnenter_Caption = "Encaminhar";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Caption", bttBtnenter_Caption);
         }
         else if ( AV5Usuario_Codigo > 500000 )
         {
            bttBtnenter_Caption = "Encaminhar";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Caption", bttBtnenter_Caption);
         }
         else if ( AV5Usuario_Codigo > 400000 )
         {
            bttBtnenter_Caption = "Encaminhar";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Caption", bttBtnenter_Caption);
         }
         else if ( AV5Usuario_Codigo == AV12UserId )
         {
            bttBtnenter_Caption = "Capturar";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Caption", bttBtnenter_Caption);
         }
         else if ( (0==AV5Usuario_Codigo) )
         {
            bttBtnenter_Caption = "Confirmar";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Caption", bttBtnenter_Caption);
         }
         else if ( AV11PodeAtribuirDmn )
         {
            bttBtnenter_Caption = "Atribuir";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Caption", bttBtnenter_Caption);
         }
         else
         {
            bttBtnenter_Caption = "Confirmar";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Caption", bttBtnenter_Caption);
         }
         if ( ! ( StringUtil.StrCmp(AV114Caller, "Gestao") == 0 ) )
         {
            if ( AV97EmDivergencia )
            {
               tblTblnaoacata_Visible = (((AV5Usuario_Codigo==800002)||((AV5Usuario_Codigo==800005)&&(AV20Selecionadas.Count==1))) ? 1 : 0);
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblnaoacata_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblnaoacata_Visible), 5, 0)));
               tblTblanexos_Visible = ((AV5Usuario_Codigo>0) ? 1 : 0);
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblanexos_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblanexos_Visible), 5, 0)));
               if ( AV5Usuario_Codigo == 800005 )
               {
                  lblTbdescpfb_Caption = "Valor Bruto da "+AV110Origem_Sigla+":";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbdescpfb_Internalname, "Caption", lblTbdescpfb_Caption);
                  lblTbdescpfl_Caption = "Liquido:";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbdescpfl_Internalname, "Caption", lblTbdescpfl_Caption);
                  if ( AV61UsuarioDaPrestadora || AV53WWPContext.gxTpr_Userehcontratante )
                  {
                     AV63NewPFB = AV83PFBFSUltima;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63NewPFB", StringUtil.LTrim( StringUtil.Str( AV63NewPFB, 14, 5)));
                     AV64NewPFL = AV80PFLFSUltima;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64NewPFL", StringUtil.LTrim( StringUtil.Str( AV64NewPFL, 14, 5)));
                  }
                  else
                  {
                     AV63NewPFB = AV81PFBFMUltima;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63NewPFB", StringUtil.LTrim( StringUtil.Str( AV63NewPFB, 14, 5)));
                     AV64NewPFL = AV82PFLFMUltima;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64NewPFL", StringUtil.LTrim( StringUtil.Str( AV64NewPFL, 14, 5)));
                  }
                  AV104OldPFB = AV63NewPFB;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV104OldPFB", StringUtil.LTrim( StringUtil.Str( AV104OldPFB, 14, 5)));
                  AV105OldPFL = AV64NewPFL;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105OldPFL", StringUtil.LTrim( StringUtil.Str( AV105OldPFL, 14, 5)));
               }
            }
            else
            {
               if ( AV8UserEhContratante )
               {
                  if ( AV154EhSolicitacaoSS )
                  {
                     cmbavNewcntsrvcod.removeAllItems();
                     cmbavNewcntsrvcod.addItem("0", "(Nenhum)", 0);
                     AV182NewCntSrvCod = 0;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV182NewCntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV182NewCntSrvCod), 6, 0)));
                     if ( ( AV5Usuario_Codigo > 600000 ) && ( AV5Usuario_Codigo < 700000 ) )
                     {
                        lblTbservico_Visible = 0;
                        context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbservico_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbservico_Visible), 5, 0)));
                        cmbavNewcntsrvcod.Visible = 0;
                        context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavNewcntsrvcod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavNewcntsrvcod.Visible), 5, 0)));
                     }
                  }
                  else
                  {
                     radavDeferido.Visible = (((AV27Prestadora_Codigo==AV5Usuario_Codigo-900000))&&(StringUtil.StrCmp(AV16StatusDemanda, "D")==0) ? 1 : 0);
                     context.httpAjaxContext.ajax_rsp_assign_prop("", false, radavDeferido_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(radavDeferido.Visible), 5, 0)));
                     if ( AV5Usuario_Codigo > 900000 )
                     {
                        if ( AV27Prestadora_Codigo == AV5Usuario_Codigo - 900000 )
                        {
                           lblTbprazo_Visible = (((StringUtil.StrCmp(AV46Deferido, "E")==0)) ? 1 : 0);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbprazo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbprazo_Visible), 5, 0)));
                           lblTbprazo_Caption = "Existem pend�ncias, novo prazo:";
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbprazo_Internalname, "Caption", lblTbprazo_Caption);
                           lblTbcontratofs_Visible = 0;
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbcontratofs_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbcontratofs_Visible), 5, 0)));
                           edtavPrazoresposta_Visible = (((StringUtil.StrCmp(AV46Deferido, "E")==0)) ? 1 : 0);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPrazoresposta_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPrazoresposta_Visible), 5, 0)));
                           AV39PrazoResposta = DateTimeUtil.ResetTime(AV50PrazoInicial);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39PrazoResposta", context.localUtil.Format(AV39PrazoResposta, "99/99/99"));
                           dynavContratofs.Visible = 0;
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratofs_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContratofs.Visible), 5, 0)));
                        }
                        else
                        {
                           lblTbprazo_Visible = 1;
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbprazo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbprazo_Visible), 5, 0)));
                           lblTbcontratofs_Visible = 1;
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbcontratofs_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbcontratofs_Visible), 5, 0)));
                           lblTbprazo_Caption = "Prazo de resposta:";
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbprazo_Internalname, "Caption", lblTbprazo_Caption);
                           edtavPrazoresposta_Visible = 1;
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPrazoresposta_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPrazoresposta_Visible), 5, 0)));
                           dynavContratofs.Visible = 1;
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratofs_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContratofs.Visible), 5, 0)));
                           AV39PrazoResposta = DateTimeUtil.ServerDate( context, "DEFAULT");
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39PrazoResposta", context.localUtil.Format(AV39PrazoResposta, "99/99/99"));
                           GXt_dtime16 = DateTimeUtil.ResetTime( AV39PrazoResposta ) ;
                           GXt_dtime17 = DateTimeUtil.ResetTime( AV39PrazoResposta ) ;
                           new prc_adddiasuteis(context ).execute(  GXt_dtime17,  1,  AV107TipoDias, out  GXt_dtime16) ;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39PrazoResposta", context.localUtil.Format(AV39PrazoResposta, "99/99/99"));
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV107TipoDias", AV107TipoDias);
                           AV39PrazoResposta = DateTimeUtil.ResetTime(GXt_dtime16);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39PrazoResposta", context.localUtil.Format(AV39PrazoResposta, "99/99/99"));
                           /* Execute user subroutine: 'GETCONTRATOFS' */
                           S182 ();
                           if (returnInSub) return;
                        }
                     }
                     else
                     {
                        lblTbprazo_Visible = 0;
                        context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbprazo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbprazo_Visible), 5, 0)));
                        lblTbcontratofs_Visible = 0;
                        context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbcontratofs_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbcontratofs_Visible), 5, 0)));
                        edtavPrazoresposta_Visible = 0;
                        context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPrazoresposta_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPrazoresposta_Visible), 5, 0)));
                        dynavContratofs.Visible = 0;
                        context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratofs_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContratofs.Visible), 5, 0)));
                        AV46Deferido = "M";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46Deferido", AV46Deferido);
                     }
                  }
               }
               else
               {
                  if ( AV5Usuario_Codigo > 900000 )
                  {
                     GX_msglist.addItem("Tramita��o para outra Contratada n�o permitida!");
                  }
               }
            }
         }
         dynavNaoconformidade_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV183NaoConformidade_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavNaoconformidade_codigo_Internalname, "Values", dynavNaoconformidade_codigo.ToJavascriptSource());
         cmbavNewcntsrvcod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV182NewCntSrvCod), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavNewcntsrvcod_Internalname, "Values", cmbavNewcntsrvcod.ToJavascriptSource());
         radavDeferido.CurrentValue = StringUtil.RTrim( AV46Deferido);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, radavDeferido_Internalname, "Values", radavDeferido.ToJavascriptSource());
         cmbavServicogrupo_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV177ServicoGrupo_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServicogrupo_codigo_Internalname, "Values", cmbavServicogrupo_codigo.ToJavascriptSource());
         dynavContratofs.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV188ContratoFS), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratofs_Internalname, "Values", dynavContratofs.ToJavascriptSource());
      }

      public void GXEnter( )
      {
         /* Execute user event: E14IZ2 */
         E14IZ2 ();
         if (returnInSub) return;
      }

      protected void E14IZ2( )
      {
         /* Enter Routine */
         new geralog(context ).execute( ref  AV191Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV191Pgmname", AV191Pgmname);
         GXt_char14 = "ENTER - &Usuario_Codigo = " + context.localUtil.Format( (decimal)(AV5Usuario_Codigo), "ZZZZZ9") + " - Status Demanda - " + StringUtil.RTrim( context.localUtil.Format( AV16StatusDemanda, "")) + "-" + gxdomainstatusdemanda.getDescription(context,AV16StatusDemanda);
         new geralog(context ).execute( ref  GXt_char14) ;
         this_Displaymode = 0;
         GX_msglist.addItem(StringUtil.StringReplace( StringUtil.BoolToStr( AV97EmDivergencia), StringUtil.BoolToStr( AV97EmDivergencia), ""));
         if ( AV5Usuario_Codigo == 700000 )
         {
            if ( (0==AV182NewCntSrvCod) )
            {
               GX_msglist.addItem("Servi�o � obrigat�rio!");
               GX_FocusControl = cmbavNewcntsrvcod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               context.DoAjaxSetFocus(GX_FocusControl);
            }
            else if ( (0==AV168CriarOSPara_Codigo) )
            {
               GX_msglist.addItem("Prestadora � obrigat�ria!");
               GX_FocusControl = cmbavCriarospara_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               context.DoAjaxSetFocus(GX_FocusControl);
            }
            else if ( (0==AV174Contrato) )
            {
               GX_msglist.addItem("Contrato � obrigat�rio!");
               GX_FocusControl = cmbavContrato_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               context.DoAjaxSetFocus(GX_FocusControl);
            }
            else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV6Observacao)) )
            {
               GX_msglist.addItem("Descri��o do Respons�vel � obrigat�ria!");
               GX_FocusControl = edtavObservacao_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               context.DoAjaxSetFocus(GX_FocusControl);
            }
            else
            {
               AV15AtribuidoA_Codigo = AV168CriarOSPara_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15AtribuidoA_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15AtribuidoA_Codigo), 6, 0)));
               AV30WebSession.Set("Codigo", StringUtil.Str( (decimal)(AV9Codigo), 6, 0));
               AV30WebSession.Set("Contratada", StringUtil.Str( (decimal)(AV15AtribuidoA_Codigo), 6, 0));
               AV30WebSession.Set("Servico", StringUtil.Str( (decimal)(AV116ContratoServicos_Codigo), 6, 0));
               AV30WebSession.Set("Descricao", "Cliente:"+StringUtil.Chr( 10)+StringUtil.Substring( AV140Descricao, StringUtil.StringSearch( AV140Descricao, "�", 1)+1, 500)+StringUtil.Chr( 13)+"Respons�vel:"+StringUtil.Chr( 10)+StringUtil.Trim( AV6Observacao));
               /* Execute user subroutine: 'SETREQUISITOS' */
               S192 ();
               if (returnInSub) return;
               AV30WebSession.Set("Requisitos", AV179Requisitos.ToXml(false, true, "Collection", ""));
               context.PopUp(formatLink("wp_os.aspx") , new Object[] {});
               context.setWebReturnParms(new Object[] {(String)AV10Destino_Nome});
               context.wjLocDisableFrm = 1;
               context.nUserReturn = 1;
               returnInSub = true;
               if (true) return;
            }
         }
         else if ( ( AV5Usuario_Codigo >= 800000 ) && ( AV5Usuario_Codigo < 900000 ) )
         {
            /* Execute user subroutine: 'TRATARDIVERGENCIA' */
            S202 ();
            if (returnInSub) return;
         }
         else
         {
            AV56SemUsuarioContratante = false;
            if ( AV5Usuario_Codigo == 900000 )
            {
               AV211GXV1 = 1;
               while ( AV211GXV1 <= AV20Selecionadas.Count )
               {
                  AV21Selecionada = (int)(AV20Selecionadas.GetNumeric(AV211GXV1));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Selecionada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Selecionada), 6, 0)));
                  /* Execute user subroutine: 'ULTIMOATORDACONTRATANTE' */
                  S352 ();
                  if (returnInSub) return;
                  if ( AV56SemUsuarioContratante )
                  {
                     if (true) break;
                  }
                  AV211GXV1 = (int)(AV211GXV1+1);
               }
            }
            else
            {
               AV15AtribuidoA_Codigo = AV5Usuario_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15AtribuidoA_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15AtribuidoA_Codigo), 6, 0)));
            }
            if ( (0==AV5Usuario_Codigo) )
            {
               GX_msglist.addItem("Selecione o destino da A��o!");
            }
            else if ( ! AV8UserEhContratante && ( AV5Usuario_Codigo > 900000 ) )
            {
               GX_msglist.addItem("Tramita��o para outra Contratada n�o permitida!");
            }
            else if ( AV56SemUsuarioContratante )
            {
               GX_msglist.addItem("Sem usu�rio da Contratante no Contrato da demanda selecionada!");
            }
            else if ( (0==AV188ContratoFS) && ( AV5Usuario_Codigo > 900000 ) && ( AV27Prestadora_Codigo != ( AV5Usuario_Codigo - 900000 ) ) )
            {
               GX_msglist.addItem("Contrato da FS � obrigat�rio!");
            }
            else if ( ( AV5Usuario_Codigo >= 900000 ) && String.IsNullOrEmpty(StringUtil.RTrim( AV6Observacao)) )
            {
               GX_msglist.addItem("Campo observa��es � obrigat�rio!");
            }
            else if ( ( StringUtil.StrCmp(AV46Deferido, "E") == 0 ) && ( AV39PrazoResposta == DateTimeUtil.ResetTime( AV50PrazoInicial) ) )
            {
               GX_msglist.addItem("Informe o novo prazo de entrega!");
               GX_FocusControl = edtavPrazoresposta_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               context.DoAjaxSetFocus(GX_FocusControl);
            }
            else if ( ( StringUtil.StrCmp(AV46Deferido, "E") == 0 ) && ( AV27Prestadora_Codigo == AV5Usuario_Codigo - 900000 ) && ( AV39PrazoResposta < AV50PrazoInicial ) )
            {
               GX_msglist.addItem("O novo prazo de entrega � menor ao prazo definido inicialmente!");
               GX_FocusControl = edtavPrazoresposta_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               context.DoAjaxSetFocus(GX_FocusControl);
            }
            else if ( ( StringUtil.StrCmp(AV46Deferido, "E") == 0 ) && ( AV5Usuario_Codigo > 900000 ) && ( AV39PrazoResposta < DateTimeUtil.ServerDate( context, "DEFAULT") ) )
            {
               GX_msglist.addItem("Prazo de resposta j� expirou!");
               GX_FocusControl = edtavPrazoresposta_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               context.DoAjaxSetFocus(GX_FocusControl);
            }
            else
            {
               if ( AV15AtribuidoA_Codigo > 900000 )
               {
                  AV212GXLvl778 = 0;
                  /* Using cursor H00IZ26 */
                  pr_default.execute(21, new Object[] {AV9Codigo});
                  while ( (pr_default.getStatus(21) != 101) )
                  {
                     A891LogResponsavel_UsuarioCod = H00IZ26_A891LogResponsavel_UsuarioCod[0];
                     n891LogResponsavel_UsuarioCod = H00IZ26_n891LogResponsavel_UsuarioCod[0];
                     A468ContagemResultado_NaoCnfDmnCod = H00IZ26_A468ContagemResultado_NaoCnfDmnCod[0];
                     n468ContagemResultado_NaoCnfDmnCod = H00IZ26_n468ContagemResultado_NaoCnfDmnCod[0];
                     A1553ContagemResultado_CntSrvCod = H00IZ26_A1553ContagemResultado_CntSrvCod[0];
                     n1553ContagemResultado_CntSrvCod = H00IZ26_n1553ContagemResultado_CntSrvCod[0];
                     A1603ContagemResultado_CntCod = H00IZ26_A1603ContagemResultado_CntCod[0];
                     n1603ContagemResultado_CntCod = H00IZ26_n1603ContagemResultado_CntCod[0];
                     A1908Usuario_DeFerias = H00IZ26_A1908Usuario_DeFerias[0];
                     n1908Usuario_DeFerias = H00IZ26_n1908Usuario_DeFerias[0];
                     A490ContagemResultado_ContratadaCod = H00IZ26_A490ContagemResultado_ContratadaCod[0];
                     n490ContagemResultado_ContratadaCod = H00IZ26_n490ContagemResultado_ContratadaCod[0];
                     A39Contratada_Codigo = H00IZ26_A39Contratada_Codigo[0];
                     n39Contratada_Codigo = H00IZ26_n39Contratada_Codigo[0];
                     A428NaoConformidade_AreaTrabalhoCod = H00IZ26_A428NaoConformidade_AreaTrabalhoCod[0];
                     n428NaoConformidade_AreaTrabalhoCod = H00IZ26_n428NaoConformidade_AreaTrabalhoCod[0];
                     A429NaoConformidade_Tipo = H00IZ26_A429NaoConformidade_Tipo[0];
                     n429NaoConformidade_Tipo = H00IZ26_n429NaoConformidade_Tipo[0];
                     A92Contrato_Ativo = H00IZ26_A92Contrato_Ativo[0];
                     n92Contrato_Ativo = H00IZ26_n92Contrato_Ativo[0];
                     A1797LogResponsavel_Codigo = H00IZ26_A1797LogResponsavel_Codigo[0];
                     A896LogResponsavel_Owner = H00IZ26_A896LogResponsavel_Owner[0];
                     A892LogResponsavel_DemandaCod = H00IZ26_A892LogResponsavel_DemandaCod[0];
                     n892LogResponsavel_DemandaCod = H00IZ26_n892LogResponsavel_DemandaCod[0];
                     A83Contrato_DataVigenciaTermino = H00IZ26_A83Contrato_DataVigenciaTermino[0];
                     n83Contrato_DataVigenciaTermino = H00IZ26_n83Contrato_DataVigenciaTermino[0];
                     A1908Usuario_DeFerias = H00IZ26_A1908Usuario_DeFerias[0];
                     n1908Usuario_DeFerias = H00IZ26_n1908Usuario_DeFerias[0];
                     A468ContagemResultado_NaoCnfDmnCod = H00IZ26_A468ContagemResultado_NaoCnfDmnCod[0];
                     n468ContagemResultado_NaoCnfDmnCod = H00IZ26_n468ContagemResultado_NaoCnfDmnCod[0];
                     A1553ContagemResultado_CntSrvCod = H00IZ26_A1553ContagemResultado_CntSrvCod[0];
                     n1553ContagemResultado_CntSrvCod = H00IZ26_n1553ContagemResultado_CntSrvCod[0];
                     A490ContagemResultado_ContratadaCod = H00IZ26_A490ContagemResultado_ContratadaCod[0];
                     n490ContagemResultado_ContratadaCod = H00IZ26_n490ContagemResultado_ContratadaCod[0];
                     A428NaoConformidade_AreaTrabalhoCod = H00IZ26_A428NaoConformidade_AreaTrabalhoCod[0];
                     n428NaoConformidade_AreaTrabalhoCod = H00IZ26_n428NaoConformidade_AreaTrabalhoCod[0];
                     A429NaoConformidade_Tipo = H00IZ26_A429NaoConformidade_Tipo[0];
                     n429NaoConformidade_Tipo = H00IZ26_n429NaoConformidade_Tipo[0];
                     A1603ContagemResultado_CntCod = H00IZ26_A1603ContagemResultado_CntCod[0];
                     n1603ContagemResultado_CntCod = H00IZ26_n1603ContagemResultado_CntCod[0];
                     A39Contratada_Codigo = H00IZ26_A39Contratada_Codigo[0];
                     n39Contratada_Codigo = H00IZ26_n39Contratada_Codigo[0];
                     A92Contrato_Ativo = H00IZ26_A92Contrato_Ativo[0];
                     n92Contrato_Ativo = H00IZ26_n92Contrato_Ativo[0];
                     A83Contrato_DataVigenciaTermino = H00IZ26_A83Contrato_DataVigenciaTermino[0];
                     n83Contrato_DataVigenciaTermino = H00IZ26_n83Contrato_DataVigenciaTermino[0];
                     GXt_boolean11 = A1149LogResponsavel_OwnerEhContratante;
                     new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A896LogResponsavel_Owner, out  GXt_boolean11) ;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A892LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A892LogResponsavel_DemandaCod), 6, 0)));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A896LogResponsavel_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A896LogResponsavel_Owner), 6, 0)));
                     A1149LogResponsavel_OwnerEhContratante = GXt_boolean11;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1149LogResponsavel_OwnerEhContratante", A1149LogResponsavel_OwnerEhContratante);
                     if ( ! A1149LogResponsavel_OwnerEhContratante )
                     {
                        AV212GXLvl778 = 1;
                        /* Using cursor H00IZ27 */
                        pr_default.execute(22, new Object[] {A896LogResponsavel_Owner, AV5Usuario_Codigo});
                        while ( (pr_default.getStatus(22) != 101) )
                        {
                           A69ContratadaUsuario_UsuarioCod = H00IZ27_A69ContratadaUsuario_UsuarioCod[0];
                           A66ContratadaUsuario_ContratadaCod = H00IZ27_A66ContratadaUsuario_ContratadaCod[0];
                           if ( A1908Usuario_DeFerias )
                           {
                              if ( AV5Usuario_Codigo - 900000 == A490ContagemResultado_ContratadaCod )
                              {
                                 /* Using cursor H00IZ28 */
                                 pr_default.execute(23, new Object[] {n1603ContagemResultado_CntCod, A1603ContagemResultado_CntCod});
                                 while ( (pr_default.getStatus(23) != 101) )
                                 {
                                    A1078ContratoGestor_ContratoCod = H00IZ28_A1078ContratoGestor_ContratoCod[0];
                                    A1079ContratoGestor_UsuarioCod = H00IZ28_A1079ContratoGestor_UsuarioCod[0];
                                    AV15AtribuidoA_Codigo = A1079ContratoGestor_UsuarioCod;
                                    context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15AtribuidoA_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15AtribuidoA_Codigo), 6, 0)));
                                    /* Exit For each command. Update data (if necessary), close cursors & exit. */
                                    if (true) break;
                                    pr_default.readNext(23);
                                 }
                                 pr_default.close(23);
                              }
                              else
                              {
                                 AV15AtribuidoA_Codigo = A896LogResponsavel_Owner;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15AtribuidoA_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15AtribuidoA_Codigo), 6, 0)));
                              }
                           }
                           else
                           {
                              AV15AtribuidoA_Codigo = A896LogResponsavel_Owner;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15AtribuidoA_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15AtribuidoA_Codigo), 6, 0)));
                           }
                           /* Exit For each command. Update data (if necessary), close cursors & exit. */
                           if (true) break;
                           pr_default.readNext(22);
                        }
                        pr_default.close(22);
                        if ( AV15AtribuidoA_Codigo < 900000 )
                        {
                           /* Exit For each command. Update data (if necessary), close cursors & exit. */
                           if (true) break;
                        }
                     }
                     pr_default.readNext(21);
                  }
                  pr_default.close(21);
                  if ( AV212GXLvl778 == 0 )
                  {
                     /* Execute user subroutine: 'SEMTRAMITACOESANTERIORES' */
                     S212 ();
                     if (returnInSub) return;
                  }
                  if ( AV15AtribuidoA_Codigo > 900000 )
                  {
                     /* Execute user subroutine: 'SEMTRAMITACOESANTERIORES' */
                     S212 ();
                     if (returnInSub) return;
                  }
               }
               if ( ( AV15AtribuidoA_Codigo > 900000 ) || (0==AV15AtribuidoA_Codigo) )
               {
                  GX_msglist.addItem("N�o existe preposto/gestor no Contrato do destino escolhido. OS N�O encaminhada!");
               }
               else
               {
                  AV106RetornoParaPrestadora = (bool)((AV27Prestadora_Codigo==(AV5Usuario_Codigo-900000)));
                  AV147Observ = StringUtil.Trim( AV6Observacao);
                  if ( AV53WWPContext.gxTpr_Userehcontratante && AV106RetornoParaPrestadora )
                  {
                     if ( ( StringUtil.StrCmp(AV46Deferido, "R") == 0 ) && ( AV45PrazoEntrega > AV50PrazoInicial ) )
                     {
                        AV147Observ = AV147Observ + StringUtil.Trim( AV147Observ) + " (Prazo recalculado)";
                     }
                     else if ( StringUtil.StrCmp(AV46Deferido, "E") == 0 )
                     {
                        AV147Observ = AV147Observ + StringUtil.Trim( AV147Observ) + " (Prazo extendido)";
                     }
                     else if ( StringUtil.StrCmp(AV46Deferido, "M") == 0 )
                     {
                        AV147Observ = AV147Observ + StringUtil.Trim( AV147Observ) + " (Prazo indeferido)";
                     }
                  }
                  AV215GXV2 = 1;
                  while ( AV215GXV2 <= AV20Selecionadas.Count )
                  {
                     AV21Selecionada = (int)(AV20Selecionadas.GetNumeric(AV215GXV2));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Selecionada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Selecionada), 6, 0)));
                     /* Execute user subroutine: 'DADOSDADEMANDA' */
                     S222 ();
                     if (returnInSub) return;
                     GXt_char14 = "For &Selecionada in &Selecionadas  - &Usuario_Codigo = " + context.localUtil.Format( (decimal)(AV5Usuario_Codigo), "ZZZZZ9") + " - &StatusDemanda - " + StringUtil.RTrim( context.localUtil.Format( AV16StatusDemanda, ""));
                     new geralog(context ).execute( ref  GXt_char14) ;
                     GXt_char10 = "For &Selecionada in &Selecionadas  - &Usuario_Codigo = " + context.localUtil.Format( (decimal)(AV5Usuario_Codigo), "ZZZZZ9") + " - &NovoStatus - " + StringUtil.RTrim( context.localUtil.Format( AV43NovoStatus, ""));
                     new geralog(context ).execute( ref  GXt_char10) ;
                     AV74AnaliseImplicito = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74AnaliseImplicito", AV74AnaliseImplicito);
                     AV94ExecucaoImplicita = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV94ExecucaoImplicita", AV94ExecucaoImplicita);
                     AV180PrazoPrevisto = (DateTime)(DateTime.MinValue);
                     if ( AV15AtribuidoA_Codigo == AV12UserId )
                     {
                        AV17Acao = "C";
                        if ( ! ( StringUtil.StrCmp(AV114Caller, "Gestao") == 0 ) && ! AV8UserEhContratante )
                        {
                           if ( StringUtil.StrCmp(AV16StatusDemanda, "S") == 0 )
                           {
                              AV43NovoStatus = "E";
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43NovoStatus", AV43NovoStatus);
                           }
                           else
                           {
                              AV43NovoStatus = "A";
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43NovoStatus", AV43NovoStatus);
                              AV45PrazoEntrega = DateTimeUtil.ResetTime( AV51PrazoAnterior ) ;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45PrazoEntrega", context.localUtil.TToC( AV45PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
                           }
                        }
                     }
                     else
                     {
                        GXt_char14 = "For &Selecionada in &Selecionadas  ELSE ELSE :::&Deferido::::" + AV46Deferido;
                        new geralog(context ).execute( ref  GXt_char14) ;
                        GXt_char10 = "For &Selecionada in &Selecionadas  ELSE ELSE :::&RetornoParaPrestadora::::" + StringUtil.BoolToStr( AV106RetornoParaPrestadora);
                        new geralog(context ).execute( ref  GXt_char10) ;
                        if ( AV5Usuario_Codigo >= 900000 )
                        {
                           if ( AV106RetornoParaPrestadora )
                           {
                              if ( StringUtil.StrCmp(AV16StatusDemanda, "D") == 0 )
                              {
                                 /* Execute user subroutine: 'STATUSANTESREJEICAO' */
                                 S113 ();
                                 if (returnInSub) return;
                              }
                              if ( StringUtil.StrCmp(AV46Deferido, "M") == 0 )
                              {
                                 AV45PrazoEntrega = AV50PrazoInicial;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45PrazoEntrega", context.localUtil.TToC( AV45PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
                              }
                              else
                              {
                                 GXt_char14 = "For &Selecionada in &Selecionadas  &PrazoResposta > &PrazoInicial :::&::::";
                                 new geralog(context ).execute( ref  GXt_char14) ;
                                 GXt_char10 = "&PrazoResposta = " + context.localUtil.Format( AV39PrazoResposta, "99/99/99");
                                 new geralog(context ).execute( ref  GXt_char10) ;
                                 GXt_char9 = "&PrazoInicial = " + context.localUtil.Format( AV50PrazoInicial, "99/99/99 99:99");
                                 new geralog(context ).execute( ref  GXt_char9) ;
                                 if ( AV39PrazoResposta > AV50PrazoInicial )
                                 {
                                    GXt_char14 = "1 - If &PrazoResposta > &PrazoInicial";
                                    new geralog(context ).execute( ref  GXt_char14) ;
                                    /* Execute user subroutine: 'EXTENDEPRAZOENTREGA' */
                                    S232 ();
                                    if (returnInSub) return;
                                 }
                                 else
                                 {
                                    GXt_char10 = "&NovoStatus = " + StringUtil.RTrim( context.localUtil.Format( AV43NovoStatus, "")) + "-" + gxdomainstatusdemanda.getDescription(context,AV43NovoStatus);
                                    new geralog(context ).execute( ref  GXt_char10) ;
                                    if ( ( StringUtil.StrCmp(AV43NovoStatus, "S") == 0 ) || ( StringUtil.StrCmp(AV43NovoStatus, "A") == 0 ) )
                                    {
                                       GXt_char14 = "2 - If &NovoStatus = StatusDemanda.Solicitada or &NovoStatus = StatusDemanda.Aberta";
                                       new geralog(context ).execute( ref  GXt_char14) ;
                                       /* Execute user subroutine: 'NOVOPRAZOENTREGA' */
                                       S242 ();
                                       if (returnInSub) return;
                                    }
                                 }
                              }
                           }
                           else
                           {
                              /* Execute user subroutine: 'ENCAMINHAOUDEVOLVE' */
                              S252 ();
                              if (returnInSub) return;
                           }
                           AV17Acao = "E";
                           if ( ( AV27Prestadora_Codigo == AV24Contratada_Codigo ) && ( AV5Usuario_Codigo == 900000 ) )
                           {
                              AV43NovoStatus = "D";
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43NovoStatus", AV43NovoStatus);
                              AV17Acao = "R";
                              if ( StringUtil.StrCmp(AV16StatusDemanda, "S") == 0 )
                              {
                                 if ( AV93DiasAnalise > 0 )
                                 {
                                    AV74AnaliseImplicito = true;
                                    context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74AnaliseImplicito", AV74AnaliseImplicito);
                                 }
                                 else
                                 {
                                    AV94ExecucaoImplicita = true;
                                    context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV94ExecucaoImplicita", AV94ExecucaoImplicita);
                                 }
                              }
                           }
                        }
                        else
                        {
                           if ( ( AV5Usuario_Codigo > 500000 ) && ( AV5Usuario_Codigo <= 600000 ) )
                           {
                              AV15AtribuidoA_Codigo = (int)(AV5Usuario_Codigo-500000);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15AtribuidoA_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15AtribuidoA_Codigo), 6, 0)));
                              AV17Acao = "E";
                              AV45PrazoEntrega = AV18PrazoExecucao;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45PrazoEntrega", context.localUtil.TToC( AV45PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
                              AV43NovoStatus = "S";
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43NovoStatus", AV43NovoStatus);
                           }
                           else if ( StringUtil.StrCmp(AV16StatusDemanda, "D") == 0 )
                           {
                              if ( ( AV5Usuario_Codigo > 400000 ) && ( AV5Usuario_Codigo <= 500000 ) )
                              {
                                 AV15AtribuidoA_Codigo = (int)(AV5Usuario_Codigo-400000);
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15AtribuidoA_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15AtribuidoA_Codigo), 6, 0)));
                              }
                              AV17Acao = "E";
                              AV45PrazoEntrega = AV18PrazoExecucao;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45PrazoEntrega", context.localUtil.TToC( AV45PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
                              /* Execute user subroutine: 'STATUSANTESREJEICAO' */
                              S113 ();
                              if (returnInSub) return;
                           }
                           else if ( ( AV5Usuario_Codigo > 400000 ) && ( AV5Usuario_Codigo <= 500000 ) )
                           {
                              AV15AtribuidoA_Codigo = (int)(AV5Usuario_Codigo-400000);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15AtribuidoA_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15AtribuidoA_Codigo), 6, 0)));
                              AV17Acao = "R";
                              AV45PrazoEntrega = AV18PrazoExecucao;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45PrazoEntrega", context.localUtil.TToC( AV45PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
                              AV16StatusDemanda = "D";
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16StatusDemanda", AV16StatusDemanda);
                           }
                           else
                           {
                              AV17Acao = "A";
                              AV45PrazoEntrega = AV186PrazoAtual;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45PrazoEntrega", context.localUtil.TToC( AV45PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
                              if ( ! ( StringUtil.StrCmp(AV114Caller, "Gestao") == 0 ) && ! AV8UserEhContratante )
                              {
                                 if ( StringUtil.StrCmp(AV16StatusDemanda, "S") == 0 )
                                 {
                                    AV45PrazoEntrega = AV75PrazoAnalise;
                                    context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45PrazoEntrega", context.localUtil.TToC( AV45PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
                                    AV43NovoStatus = "E";
                                    context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43NovoStatus", AV43NovoStatus);
                                 }
                                 else if ( StringUtil.StrCmp(AV16StatusDemanda, "D") == 0 )
                                 {
                                    AV43NovoStatus = "A";
                                    context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43NovoStatus", AV43NovoStatus);
                                 }
                              }
                           }
                        }
                     }
                     GXt_char14 = "3 - If &Acao = " + AV17Acao;
                     new geralog(context ).execute( ref  GXt_char14) ;
                     GXt_char10 = "3 - If &Acao = " + AV17Acao;
                     new geralog(context ).execute( ref  GXt_char10) ;
                     GXt_char9 = "3 - If &Acao = " + AV17Acao;
                     new geralog(context ).execute( ref  GXt_char9) ;
                     if ( ! ( StringUtil.StrCmp(AV114Caller, "Gestao") == 0 ) )
                     {
                        if ( StringUtil.StrCmp(AV43NovoStatus, "E") == 0 )
                        {
                           if ( StringUtil.StrCmp(AV17Acao, "E") == 0 )
                           {
                              if ( AV106RetornoParaPrestadora )
                              {
                                 new prc_setinicioanl(context ).execute( ref  AV21Selecionada) ;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Selecionada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Selecionada), 6, 0)));
                                 if ( StringUtil.StrCmp(AV46Deferido, "M") == 0 )
                                 {
                                    AV45PrazoEntrega = DateTimeUtil.ResetTime( AV51PrazoAnterior ) ;
                                    context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45PrazoEntrega", context.localUtil.TToC( AV45PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
                                 }
                                 else if ( StringUtil.StrCmp(AV46Deferido, "E") == 0 )
                                 {
                                    GXt_dtime17 = AV45PrazoEntrega;
                                    new prc_adddiasuteis(context ).execute(  AV45PrazoEntrega,  AV93DiasAnalise,  AV107TipoDias, out  GXt_dtime17) ;
                                    context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45PrazoEntrega", context.localUtil.TToC( AV45PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
                                    context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV93DiasAnalise", StringUtil.LTrim( StringUtil.Str( (decimal)(AV93DiasAnalise), 4, 0)));
                                    context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV107TipoDias", AV107TipoDias);
                                    AV45PrazoEntrega = GXt_dtime17;
                                    context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45PrazoEntrega", context.localUtil.TToC( AV45PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
                                    GXt_dtime16 = DateTimeUtil.ResetTime( AV39PrazoResposta ) ;
                                    new prc_savenewdataprevista(context ).execute( ref  AV21Selecionada, ref  GXt_dtime16) ;
                                    AV39PrazoResposta = DateTimeUtil.ResetTime((DateTime)(GXt_dtime16));
                                    context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Selecionada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Selecionada), 6, 0)));
                                    context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39PrazoResposta", context.localUtil.Format(AV39PrazoResposta, "99/99/99"));
                                 }
                                 else
                                 {
                                    AV45PrazoEntrega = DateTimeUtil.ResetTime( DateTimeUtil.ServerDate( context, "DEFAULT") ) ;
                                    context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45PrazoEntrega", context.localUtil.TToC( AV45PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
                                    AV180PrazoPrevisto = DateTimeUtil.ResetTime( DateTimeUtil.ServerDate( context, "DEFAULT") ) ;
                                    GXt_char14 = "12 - Gera as novas data = &PrazoEntrega = " + context.localUtil.Format( AV45PrazoEntrega, "99/99/99 99:99");
                                    new geralog(context ).execute( ref  GXt_char14) ;
                                    GXt_char10 = "13 - Gera as novas data = &PrazoPrevisto = " + context.localUtil.Format( AV180PrazoPrevisto, "99/99/99 99:99");
                                    new geralog(context ).execute( ref  GXt_char10) ;
                                    GXt_dtime17 = AV45PrazoEntrega;
                                    new prc_adddiasuteis(context ).execute(  AV45PrazoEntrega,  AV93DiasAnalise,  AV107TipoDias, out  GXt_dtime17) ;
                                    context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45PrazoEntrega", context.localUtil.TToC( AV45PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
                                    context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV93DiasAnalise", StringUtil.LTrim( StringUtil.Str( (decimal)(AV93DiasAnalise), 4, 0)));
                                    context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV107TipoDias", AV107TipoDias);
                                    AV45PrazoEntrega = GXt_dtime17;
                                    context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45PrazoEntrega", context.localUtil.TToC( AV45PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
                                    GXt_dtime17 = AV180PrazoPrevisto;
                                    new prc_adddiasuteis(context ).execute(  AV180PrazoPrevisto,  AV73PrazoInicialDias,  AV107TipoDias, out  GXt_dtime17) ;
                                    context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73PrazoInicialDias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV73PrazoInicialDias), 4, 0)));
                                    context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV107TipoDias", AV107TipoDias);
                                    AV180PrazoPrevisto = GXt_dtime17;
                                    GXt_char14 = "14 - Gera as novas data = &PrazoEntrega = " + context.localUtil.Format( AV45PrazoEntrega, "99/99/99 99:99");
                                    new geralog(context ).execute( ref  GXt_char14) ;
                                    GXt_char10 = "15 - Gera as novas data = &PrazoPrevisto = " + context.localUtil.Format( AV180PrazoPrevisto, "99/99/99 99:99");
                                    new geralog(context ).execute( ref  GXt_char10) ;
                                    new prc_savenewdataprevista(context ).execute( ref  AV21Selecionada, ref  AV180PrazoPrevisto) ;
                                    context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Selecionada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Selecionada), 6, 0)));
                                 }
                              }
                           }
                        }
                        else if ( StringUtil.StrCmp(AV43NovoStatus, "A") == 0 )
                        {
                           if ( StringUtil.StrCmp(AV17Acao, "E") == 0 )
                           {
                              if ( AV106RetornoParaPrestadora )
                              {
                                 new prc_setinicioexc(context ).execute( ref  AV21Selecionada) ;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Selecionada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Selecionada), 6, 0)));
                                 if ( StringUtil.StrCmp(AV46Deferido, "M") == 0 )
                                 {
                                    AV45PrazoEntrega = DateTimeUtil.ResetTime( AV51PrazoAnterior ) ;
                                    context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45PrazoEntrega", context.localUtil.TToC( AV45PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
                                 }
                                 else if ( StringUtil.StrCmp(AV46Deferido, "E") == 0 )
                                 {
                                    AV45PrazoEntrega = DateTimeUtil.ResetTime( AV39PrazoResposta ) ;
                                    context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45PrazoEntrega", context.localUtil.TToC( AV45PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
                                 }
                                 else
                                 {
                                    AV45PrazoEntrega = DateTimeUtil.ResetTime( DateTimeUtil.ServerDate( context, "DEFAULT") ) ;
                                    context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45PrazoEntrega", context.localUtil.TToC( AV45PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
                                    GXt_char14 = "14 - Gera as novas data = &PrazoEntrega = " + context.localUtil.Format( AV45PrazoEntrega, "99/99/99 99:99");
                                    new geralog(context ).execute( ref  GXt_char14) ;
                                    GXt_char10 = "15 - Gera as novas data = &PrazoPrevisto = " + context.localUtil.Format( AV180PrazoPrevisto, "99/99/99 99:99");
                                    new geralog(context ).execute( ref  GXt_char10) ;
                                    GXt_dtime17 = AV45PrazoEntrega;
                                    new prc_adddiasuteis(context ).execute(  AV45PrazoEntrega,  AV73PrazoInicialDias,  AV107TipoDias, out  GXt_dtime17) ;
                                    context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45PrazoEntrega", context.localUtil.TToC( AV45PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
                                    context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73PrazoInicialDias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV73PrazoInicialDias), 4, 0)));
                                    context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV107TipoDias", AV107TipoDias);
                                    AV45PrazoEntrega = GXt_dtime17;
                                    context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45PrazoEntrega", context.localUtil.TToC( AV45PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
                                    GXt_char14 = "16 - Gera as novas data = &PrazoEntrega = " + context.localUtil.Format( AV45PrazoEntrega, "99/99/99 99:99");
                                    new geralog(context ).execute( ref  GXt_char14) ;
                                    GXt_char10 = "17 - Gera as novas data = &PrazoPrevisto = " + context.localUtil.Format( AV180PrazoPrevisto, "99/99/99 99:99");
                                    new geralog(context ).execute( ref  GXt_char10) ;
                                 }
                              }
                           }
                        }
                        GXt_char9 = " - 18 - &Usuario_Codigo = " + context.localUtil.Format( (decimal)(AV5Usuario_Codigo), "ZZZZZ9");
                        new geralog(context ).execute( ref  GXt_char9) ;
                        GXt_char8 = " - 19 - &StatusDemanda = " + StringUtil.RTrim( context.localUtil.Format( AV16StatusDemanda, "")) + "-" + gxdomainstatusdemanda.getDescription(context,AV16StatusDemanda);
                        new geralog(context ).execute( ref  GXt_char8) ;
                        GXt_char7 = " 20  - If &Acao = " + AV17Acao;
                        new geralog(context ).execute( ref  GXt_char7) ;
                        GXt_char6 = "21 - data = &PrazoEntrega = " + context.localUtil.Format( AV45PrazoEntrega, "99/99/99 99:99");
                        new geralog(context ).execute( ref  GXt_char6) ;
                        GXt_char5 = "22 - data = &PrazoPrevisto = " + context.localUtil.Format( AV180PrazoPrevisto, "99/99/99 99:99");
                        new geralog(context ).execute( ref  GXt_char5) ;
                        if ( AV5Usuario_Codigo == 900000 )
                        {
                           if ( StringUtil.StrCmp(AV16StatusDemanda, "E") == 0 )
                           {
                              new prc_setfimanl(context ).execute( ref  AV21Selecionada) ;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Selecionada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Selecionada), 6, 0)));
                           }
                           else if ( StringUtil.StrCmp(AV16StatusDemanda, "A") == 0 )
                           {
                              new prc_setfimexc(context ).execute( ref  AV21Selecionada) ;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Selecionada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Selecionada), 6, 0)));
                           }
                           new prc_encerrarcicloexecucao(context ).execute(  AV21Selecionada,  DateTimeUtil.ServerNow( context, "DEFAULT")) ;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Selecionada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Selecionada), 6, 0)));
                        }
                        else
                        {
                           if ( AV106RetornoParaPrestadora && ! ( StringUtil.StrCmp(AV17Acao, "C") == 0 ) )
                           {
                              if ( (DateTime.MinValue==AV180PrazoPrevisto) )
                              {
                                 AV180PrazoPrevisto = AV45PrazoEntrega;
                              }
                              GXt_char14 = "23 - data = &PrazoPrevisto = " + context.localUtil.Format( AV180PrazoPrevisto, "99/99/99 99:99");
                              new geralog(context ).execute( ref  GXt_char14) ;
                              new prc_novocicloexecucao(context ).execute(  AV21Selecionada,  DateTimeUtil.ServerNow( context, "DEFAULT"),  AV180PrazoPrevisto,  AV107TipoDias,  true) ;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Selecionada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Selecionada), 6, 0)));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV107TipoDias", AV107TipoDias);
                           }
                        }
                     }
                     GXt_char10 = "24 - &Usuario_Codigo = " + context.localUtil.Format( (decimal)(AV5Usuario_Codigo), "ZZZZZ9");
                     new geralog(context ).execute( ref  GXt_char10) ;
                     GXt_char9 = "25 - AddHorasEntrega";
                     new geralog(context ).execute( ref  GXt_char9) ;
                     GXt_char8 = "26 - &NovoStatus = " + StringUtil.RTrim( context.localUtil.Format( AV43NovoStatus, "")) + "-" + gxdomainstatusdemanda.getDescription(context,AV43NovoStatus);
                     new geralog(context ).execute( ref  GXt_char8) ;
                     GXt_char7 = "27 - If &Acao = " + AV17Acao;
                     new geralog(context ).execute( ref  GXt_char7) ;
                     /* Execute user subroutine: 'ADDHORASENTREGA' */
                     S262 ();
                     if (returnInSub) return;
                     if ( ( StringUtil.StrCmp(AV17Acao, "C") == 0 ) && ( StringUtil.StrCmp(AV43NovoStatus, "E") == 0 ) )
                     {
                        new prc_setinicioanl(context ).execute( ref  AV21Selecionada) ;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Selecionada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Selecionada), 6, 0)));
                     }
                     if ( ( AV5Usuario_Codigo > 600000 ) && ( AV5Usuario_Codigo < 700000 ) )
                     {
                        AV15AtribuidoA_Codigo = (int)(AV5Usuario_Codigo-600000);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15AtribuidoA_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15AtribuidoA_Codigo), 6, 0)));
                        AV43NovoStatus = "D";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43NovoStatus", AV43NovoStatus);
                        AV17Acao = "R";
                     }
                     else
                     {
                        GXt_char14 = "28 - Entrou aqui!";
                        new geralog(context ).execute( ref  GXt_char14) ;
                        AV181UpdDataPrevista = false;
                        if ( ( StringUtil.StrCmp(AV17Acao, "C") == 0 ) || ( AV106RetornoParaPrestadora && ( ( StringUtil.StrCmp(AV46Deferido, "E") == 0 ) || ( StringUtil.StrCmp(AV46Deferido, "R") != 0 ) ) ) )
                        {
                           AV181UpdDataPrevista = (bool)(((StringUtil.StrCmp(AV43NovoStatus, "E")!=0)));
                        }
                        GXt_char14 = "29 - Entrou aqui!";
                        new geralog(context ).execute( ref  GXt_char14) ;
                        new prc_savenewdataentrega(context ).execute(  AV21Selecionada,  AV45PrazoEntrega,  AV181UpdDataPrevista) ;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Selecionada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Selecionada), 6, 0)));
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45PrazoEntrega", context.localUtil.TToC( AV45PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
                        GXt_char10 = "30 - Entrou aqui! - PRC_SaveNewDataEntrega";
                        new geralog(context ).execute( ref  GXt_char10) ;
                        if ( ! ( StringUtil.StrCmp(AV114Caller, "Gestao") == 0 ) )
                        {
                           if ( AV74AnaliseImplicito )
                           {
                              new prc_preenchechklst(context ).execute(  2,  "N") ;
                              new prc_updchecklistanalise(context ).execute(  AV21Selecionada,  AV12UserId) ;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Selecionada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Selecionada), 6, 0)));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12UserId), 6, 0)));
                              new prc_inslogresponsavel(context ).execute( ref  AV21Selecionada,  AV12UserId,  "C",  "D",  AV12UserId,  0,  AV16StatusDemanda,  "E",  "",  AV75PrazoAnalise,  false) ;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Selecionada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Selecionada), 6, 0)));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12UserId), 6, 0)));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12UserId), 6, 0)));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16StatusDemanda", AV16StatusDemanda);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75PrazoAnalise", context.localUtil.TToC( AV75PrazoAnalise, 8, 5, 0, 3, "/", ":", " "));
                              AV92dateTime = DateTimeUtil.ServerNow( context, "DEFAULT");
                              AV16StatusDemanda = "E";
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16StatusDemanda", AV16StatusDemanda);
                           }
                           if ( AV94ExecucaoImplicita )
                           {
                              new prc_inslogresponsavel(context ).execute( ref  AV21Selecionada,  AV12UserId,  "C",  "D",  AV12UserId,  0,  AV16StatusDemanda,  "A",  "",  AV45PrazoEntrega,  false) ;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Selecionada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Selecionada), 6, 0)));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12UserId), 6, 0)));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12UserId), 6, 0)));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16StatusDemanda", AV16StatusDemanda);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45PrazoEntrega", context.localUtil.TToC( AV45PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
                              AV16StatusDemanda = "A";
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16StatusDemanda", AV16StatusDemanda);
                           }
                        }
                     }
                     new prc_asignardemanda(context ).execute( ref  AV21Selecionada,  AV15AtribuidoA_Codigo,  AV17Acao,  AV43NovoStatus) ;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Selecionada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Selecionada), 6, 0)));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15AtribuidoA_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15AtribuidoA_Codigo), 6, 0)));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43NovoStatus", AV43NovoStatus);
                     new prc_inslogresponsavel(context ).execute( ref  AV21Selecionada,  AV15AtribuidoA_Codigo,  AV17Acao,  "D",  AV12UserId,  0,  AV16StatusDemanda,  AV43NovoStatus,  AV147Observ,  AV45PrazoEntrega,  true) ;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Selecionada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Selecionada), 6, 0)));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15AtribuidoA_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15AtribuidoA_Codigo), 6, 0)));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12UserId), 6, 0)));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16StatusDemanda", AV16StatusDemanda);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43NovoStatus", AV43NovoStatus);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45PrazoEntrega", context.localUtil.TToC( AV45PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
                     if ( AV183NaoConformidade_Codigo > 0 )
                     {
                        AV185LogResponsavel_Codigo = (long)(NumberUtil.Val( AV30WebSession.Get("LogRspCodigo"), "."));
                        AV30WebSession.Remove("LogRspCodigo");
                        new prc_newosnaocnf(context ).execute( ref  AV9Codigo, ref  AV183NaoConformidade_Codigo, ref  AV184OSEntregue,  AV185LogResponsavel_Codigo) ;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Codigo), 6, 0)));
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV183NaoConformidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV183NaoConformidade_Codigo), 6, 0)));
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV184OSEntregue", AV184OSEntregue);
                     }
                     AV215GXV2 = (int)(AV215GXV2+1);
                  }
                  context.CommitDataStores( "WP_Tramitacao");
                  AV10Destino_Nome = cmbavUsuario_codigo.Description;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Destino_Nome", AV10Destino_Nome);
                  /* Execute user subroutine: 'FECHAR' */
                  S272 ();
                  if (returnInSub) return;
               }
            }
         }
         cmbavUsuario_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV5Usuario_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUsuario_codigo_Internalname, "Values", cmbavUsuario_codigo.ToJavascriptSource());
         radavDeferido.CurrentValue = StringUtil.RTrim( AV46Deferido);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, radavDeferido_Internalname, "Values", radavDeferido.ToJavascriptSource());
         dynavNaoconformidade_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV183NaoConformidade_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavNaoconformidade_codigo_Internalname, "Values", dynavNaoconformidade_codigo.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV179Requisitos", AV179Requisitos);
      }

      protected void E15IZ2( )
      {
         /* Prazoresposta_Isvalid Routine */
         GXt_dtime17 = DateTimeUtil.ResetTime( AV39PrazoResposta ) ;
         if ( new prc_ehferiado(context).executeUdp( ref  GXt_dtime17) )
         {
            GX_msglist.addItem("Prazo de resposta � feriado!");
            GX_FocusControl = edtavPrazoresposta_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( (DateTime.MinValue==AV39PrazoResposta) )
         {
            GX_msglist.addItem("Prazo de resposta � obrigat�rio!");
            GX_FocusControl = edtavPrazoresposta_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( AV39PrazoResposta < AV50PrazoInicial )
         {
            GX_msglist.addItem("O novo prazo de entrega � menor ao prazo definido inicialmente!");
         }
         else if ( ( AV5Usuario_Codigo > 900000 ) && ( StringUtil.StrCmp(AV46Deferido, "E") == 0 ) && ( AV39PrazoResposta < DateTimeUtil.ServerDate( context, "DEFAULT") ) )
         {
            GX_msglist.addItem("Prazo de resposta j� expirou!");
            GX_FocusControl = edtavPrazoresposta_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( DateTimeUtil.Dow( AV39PrazoResposta) == 1 )
         {
            GX_msglist.addItem("Prazo de resposta � domingo!");
            GX_FocusControl = edtavPrazoresposta_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( DateTimeUtil.Dow( AV39PrazoResposta) == 7 )
         {
            GX_msglist.addItem("Prazo de resposta � s�bado!");
            GX_FocusControl = edtavPrazoresposta_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else
         {
            AV45PrazoEntrega = DateTimeUtil.ResetTime( AV39PrazoResposta ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45PrazoEntrega", context.localUtil.TToC( AV45PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
         }
      }

      protected void E16IZ2( )
      {
         /* Servicogrupo_codigo_Click Routine */
         /* Execute user subroutine: 'CARREGASERVICOS' */
         S282 ();
         if (returnInSub) return;
         cmbavNewcntsrvcod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV182NewCntSrvCod), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavNewcntsrvcod_Internalname, "Values", cmbavNewcntsrvcod.ToJavascriptSource());
      }

      protected void E17IZ2( )
      {
         /* Newcntsrvcod_Click Routine */
         if ( AV182NewCntSrvCod > 0 )
         {
            /* Execute user subroutine: 'CARREGAPRESTADORAS' */
            S292 ();
            if (returnInSub) return;
         }
         cmbavCriarospara_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV168CriarOSPara_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavCriarospara_codigo_Internalname, "Values", cmbavCriarospara_codigo.ToJavascriptSource());
         cmbavContrato.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV174Contrato), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContrato_Internalname, "Values", cmbavContrato.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV145Sdt_Artefatos", AV145Sdt_Artefatos);
      }

      protected void E18IZ2( )
      {
         /* 'DoArtefatos' Routine */
         bttBtnenter_Visible = (AV53WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Visible), 5, 0)));
         /* Execute user subroutine: 'SOLICITACAOARTEFATOS' */
         S302 ();
         if (returnInSub) return;
      }

      protected void E19IZ2( )
      {
         /* Criarospara_codigo_Click Routine */
         /* Execute user subroutine: 'CARREGACONTRATOS' */
         S312 ();
         if (returnInSub) return;
         cmbavContrato.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV174Contrato), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContrato_Internalname, "Values", cmbavContrato.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV145Sdt_Artefatos", AV145Sdt_Artefatos);
      }

      protected void E20IZ2( )
      {
         /* Contrato_Click Routine */
         /* Execute user subroutine: 'TEMARTEFATOS' */
         S322 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV145Sdt_Artefatos", AV145Sdt_Artefatos);
      }

      protected void S222( )
      {
         /* 'DADOSDADEMANDA' Routine */
         GXt_char14 = " 000 DadosDaDemanda";
         new geralog(context ).execute( ref  GXt_char14) ;
         GXt_char10 = " 000 DadosDaDemanda";
         new geralog(context ).execute( ref  GXt_char10) ;
         /* Using cursor H00IZ31 */
         pr_default.execute(24, new Object[] {AV21Selecionada});
         while ( (pr_default.getStatus(24) != 101) )
         {
            A456ContagemResultado_Codigo = H00IZ31_A456ContagemResultado_Codigo[0];
            A490ContagemResultado_ContratadaCod = H00IZ31_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00IZ31_n490ContagemResultado_ContratadaCod[0];
            A601ContagemResultado_Servico = H00IZ31_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00IZ31_n601ContagemResultado_Servico[0];
            A484ContagemResultado_StatusDmn = H00IZ31_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00IZ31_n484ContagemResultado_StatusDmn[0];
            A471ContagemResultado_DataDmn = H00IZ31_A471ContagemResultado_DataDmn[0];
            A1237ContagemResultado_PrazoMaisDias = H00IZ31_A1237ContagemResultado_PrazoMaisDias[0];
            n1237ContagemResultado_PrazoMaisDias = H00IZ31_n1237ContagemResultado_PrazoMaisDias[0];
            A1227ContagemResultado_PrazoInicialDias = H00IZ31_A1227ContagemResultado_PrazoInicialDias[0];
            n1227ContagemResultado_PrazoInicialDias = H00IZ31_n1227ContagemResultado_PrazoInicialDias[0];
            A912ContagemResultado_HoraEntrega = H00IZ31_A912ContagemResultado_HoraEntrega[0];
            n912ContagemResultado_HoraEntrega = H00IZ31_n912ContagemResultado_HoraEntrega[0];
            A472ContagemResultado_DataEntrega = H00IZ31_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = H00IZ31_n472ContagemResultado_DataEntrega[0];
            A1351ContagemResultado_DataPrevista = H00IZ31_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = H00IZ31_n1351ContagemResultado_DataPrevista[0];
            A1611ContagemResultado_PrzTpDias = H00IZ31_A1611ContagemResultado_PrzTpDias[0];
            n1611ContagemResultado_PrzTpDias = H00IZ31_n1611ContagemResultado_PrzTpDias[0];
            A1553ContagemResultado_CntSrvCod = H00IZ31_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = H00IZ31_n1553ContagemResultado_CntSrvCod[0];
            A798ContagemResultado_PFBFSImp = H00IZ31_A798ContagemResultado_PFBFSImp[0];
            n798ContagemResultado_PFBFSImp = H00IZ31_n798ContagemResultado_PFBFSImp[0];
            A531ContagemResultado_StatusUltCnt = H00IZ31_A531ContagemResultado_StatusUltCnt[0];
            A684ContagemResultado_PFBFSUltima = H00IZ31_A684ContagemResultado_PFBFSUltima[0];
            A40000ContagemResultado_ContadorFMCod = H00IZ31_A40000ContagemResultado_ContadorFMCod[0];
            n40000ContagemResultado_ContadorFMCod = H00IZ31_n40000ContagemResultado_ContadorFMCod[0];
            A531ContagemResultado_StatusUltCnt = H00IZ31_A531ContagemResultado_StatusUltCnt[0];
            A684ContagemResultado_PFBFSUltima = H00IZ31_A684ContagemResultado_PFBFSUltima[0];
            A40000ContagemResultado_ContadorFMCod = H00IZ31_A40000ContagemResultado_ContadorFMCod[0];
            n40000ContagemResultado_ContadorFMCod = H00IZ31_n40000ContagemResultado_ContadorFMCod[0];
            A601ContagemResultado_Servico = H00IZ31_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00IZ31_n601ContagemResultado_Servico[0];
            A1611ContagemResultado_PrzTpDias = H00IZ31_A1611ContagemResultado_PrzTpDias[0];
            n1611ContagemResultado_PrzTpDias = H00IZ31_n1611ContagemResultado_PrzTpDias[0];
            AV27Prestadora_Codigo = A490ContagemResultado_ContratadaCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Prestadora_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27Prestadora_Codigo), 6, 0)));
            AV22Servico_Codigo = A601ContagemResultado_Servico;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Servico_Codigo), 6, 0)));
            AV16StatusDemanda = A484ContagemResultado_StatusDmn;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16StatusDemanda", AV16StatusDemanda);
            AV43NovoStatus = A484ContagemResultado_StatusDmn;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43NovoStatus", AV43NovoStatus);
            AV44DataSolicitacao = DateTimeUtil.ResetTime( A471ContagemResultado_DataDmn ) ;
            AV73PrazoInicialDias = (short)(A1227ContagemResultado_PrazoInicialDias+A1237ContagemResultado_PrazoMaisDias);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73PrazoInicialDias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV73PrazoInicialDias), 4, 0)));
            AV102HoraEntrega = A912ContagemResultado_HoraEntrega;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102HoraEntrega", context.localUtil.TToC( AV102HoraEntrega, 0, 5, 0, 3, "/", ":", " "));
            AV186PrazoAtual = DateTimeUtil.ResetTime( A472ContagemResultado_DataEntrega ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV186PrazoAtual", context.localUtil.TToC( AV186PrazoAtual, 8, 5, 0, 3, "/", ":", " "));
            AV186PrazoAtual = DateTimeUtil.TAdd( AV186PrazoAtual, 3600*(DateTimeUtil.Hour( AV102HoraEntrega)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV186PrazoAtual", context.localUtil.TToC( AV186PrazoAtual, 8, 5, 0, 3, "/", ":", " "));
            AV186PrazoAtual = DateTimeUtil.TAdd( AV186PrazoAtual, 60*(DateTimeUtil.Minute( AV102HoraEntrega)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV186PrazoAtual", context.localUtil.TToC( AV186PrazoAtual, 8, 5, 0, 3, "/", ":", " "));
            AV18PrazoExecucao = A1351ContagemResultado_DataPrevista;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18PrazoExecucao", context.localUtil.TToC( AV18PrazoExecucao, 8, 5, 0, 3, "/", ":", " "));
            AV18PrazoExecucao = DateTimeUtil.TAdd( AV18PrazoExecucao, 3600*(DateTimeUtil.Hour( AV102HoraEntrega)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18PrazoExecucao", context.localUtil.TToC( AV18PrazoExecucao, 8, 5, 0, 3, "/", ":", " "));
            AV18PrazoExecucao = DateTimeUtil.TAdd( AV18PrazoExecucao, 60*(DateTimeUtil.Minute( AV102HoraEntrega)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18PrazoExecucao", context.localUtil.TToC( AV18PrazoExecucao, 8, 5, 0, 3, "/", ":", " "));
            AV75PrazoAnalise = DateTimeUtil.ResetTime( A471ContagemResultado_DataDmn ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75PrazoAnalise", context.localUtil.TToC( AV75PrazoAnalise, 8, 5, 0, 3, "/", ":", " "));
            AV107TipoDias = A1611ContagemResultado_PrzTpDias;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV107TipoDias", AV107TipoDias);
            AV116ContratoServicos_Codigo = A1553ContagemResultado_CntSrvCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV116ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV116ContratoServicos_Codigo), 6, 0)));
            AV184OSEntregue = (bool)(((A40000ContagemResultado_ContadorFMCod>0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV184OSEntregue", AV184OSEntregue);
            if ( ( A684ContagemResultado_PFBFSUltima > Convert.ToDecimal( 0 )) && ( A531ContagemResultado_StatusUltCnt == 5 ) )
            {
               AV29Unidades = A684ContagemResultado_PFBFSUltima;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Unidades", StringUtil.LTrim( StringUtil.Str( AV29Unidades, 14, 5)));
            }
            else
            {
               AV29Unidades = A798ContagemResultado_PFBFSImp;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Unidades", StringUtil.LTrim( StringUtil.Str( AV29Unidades, 14, 5)));
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(24);
         GXt_char14 = " 000 DadosDaDemanda - ContagemResultado_Codigo = " + context.localUtil.Format( (decimal)(AV21Selecionada), "ZZZZZ9");
         new geralog(context ).execute( ref  GXt_char14) ;
         GXt_char10 = " 000 DadosDaDemanda - ANTES DAS PRC - &DiasAnalise  = PRC_DiasParaAnalise.Udp(&ContratoServicos_Codigo,&Unidades,0)";
         new geralog(context ).execute( ref  GXt_char10) ;
         GXt_char9 = " 000 DadosDaDemanda - ANTES DAS PRC - &PrazoAnalise = PRC_AddDiasUteis.Udp(&PrazoAnalise, &DiasAnalise, &TipoDias)";
         new geralog(context ).execute( ref  GXt_char9) ;
         GXt_char8 = " 000 DadosDaDemanda - &DiasAnalise  = " + context.localUtil.Format( (decimal)(AV93DiasAnalise), "ZZZ9");
         new geralog(context ).execute( ref  GXt_char8) ;
         GXt_char7 = " 000 DadosDaDemanda - &PrazoAnalise = " + context.localUtil.Format( AV75PrazoAnalise, "99/99/99 99:99");
         new geralog(context ).execute( ref  GXt_char7) ;
         GXt_int13 = AV93DiasAnalise;
         new prc_diasparaanalise(context ).execute( ref  AV116ContratoServicos_Codigo,  AV29Unidades,  0, out  GXt_int13) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV116ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV116ContratoServicos_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Unidades", StringUtil.LTrim( StringUtil.Str( AV29Unidades, 14, 5)));
         AV93DiasAnalise = GXt_int13;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV93DiasAnalise", StringUtil.LTrim( StringUtil.Str( (decimal)(AV93DiasAnalise), 4, 0)));
         GXt_dtime17 = AV75PrazoAnalise;
         new prc_adddiasuteis(context ).execute(  AV75PrazoAnalise,  AV93DiasAnalise,  AV107TipoDias, out  GXt_dtime17) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75PrazoAnalise", context.localUtil.TToC( AV75PrazoAnalise, 8, 5, 0, 3, "/", ":", " "));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV93DiasAnalise", StringUtil.LTrim( StringUtil.Str( (decimal)(AV93DiasAnalise), 4, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV107TipoDias", AV107TipoDias);
         AV75PrazoAnalise = GXt_dtime17;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75PrazoAnalise", context.localUtil.TToC( AV75PrazoAnalise, 8, 5, 0, 3, "/", ":", " "));
         GXt_char14 = " 000 DadosDaDemanda - DEPOISIIIIIIIIIIIIIIIIIIIIIIIIIIII";
         new geralog(context ).execute( ref  GXt_char14) ;
         GXt_char10 = " 000 DadosDaDemanda - &DiasAnalise = " + context.localUtil.Format( (decimal)(AV93DiasAnalise), "ZZZ9");
         new geralog(context ).execute( ref  GXt_char10) ;
         GXt_char9 = " 000 DadosDaDemanda - &PrazoAnalise = " + context.localUtil.Format( AV75PrazoAnalise, "99/99/99 99:99");
         new geralog(context ).execute( ref  GXt_char9) ;
         GXt_char8 = " 000 DadosDaDemanda";
         new geralog(context ).execute( ref  GXt_char8) ;
         GXt_char7 = " 000 DadosDaDemanda";
         new geralog(context ).execute( ref  GXt_char7) ;
         GXt_char6 = " 000 DadosDaDemanda";
         new geralog(context ).execute( ref  GXt_char6) ;
         GXt_char5 = " 000 DadosDaDemanda";
         new geralog(context ).execute( ref  GXt_char5) ;
      }

      protected void S113( )
      {
         /* 'STATUSANTESREJEICAO' Routine */
         AV217GXLvl1220 = 0;
         /* Using cursor H00IZ32 */
         pr_default.execute(25, new Object[] {AV21Selecionada});
         while ( (pr_default.getStatus(25) != 101) )
         {
            A1130LogResponsavel_Status = H00IZ32_A1130LogResponsavel_Status[0];
            n1130LogResponsavel_Status = H00IZ32_n1130LogResponsavel_Status[0];
            A894LogResponsavel_Acao = H00IZ32_A894LogResponsavel_Acao[0];
            A1797LogResponsavel_Codigo = H00IZ32_A1797LogResponsavel_Codigo[0];
            A891LogResponsavel_UsuarioCod = H00IZ32_A891LogResponsavel_UsuarioCod[0];
            n891LogResponsavel_UsuarioCod = H00IZ32_n891LogResponsavel_UsuarioCod[0];
            A892LogResponsavel_DemandaCod = H00IZ32_A892LogResponsavel_DemandaCod[0];
            n892LogResponsavel_DemandaCod = H00IZ32_n892LogResponsavel_DemandaCod[0];
            GXt_boolean11 = A1148LogResponsavel_UsuarioEhContratante;
            new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A891LogResponsavel_UsuarioCod, out  GXt_boolean11) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A892LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A892LogResponsavel_DemandaCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A891LogResponsavel_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A891LogResponsavel_UsuarioCod), 6, 0)));
            A1148LogResponsavel_UsuarioEhContratante = GXt_boolean11;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1148LogResponsavel_UsuarioEhContratante", A1148LogResponsavel_UsuarioEhContratante);
            if ( A1148LogResponsavel_UsuarioEhContratante )
            {
               AV217GXLvl1220 = 1;
               AV43NovoStatus = A1130LogResponsavel_Status;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43NovoStatus", AV43NovoStatus);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            pr_default.readNext(25);
         }
         pr_default.close(25);
         if ( AV217GXLvl1220 == 0 )
         {
            AV43NovoStatus = "E";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43NovoStatus", AV43NovoStatus);
         }
         /* Using cursor H00IZ33 */
         pr_default.execute(26, new Object[] {AV21Selecionada, AV43NovoStatus});
         while ( (pr_default.getStatus(26) != 101) )
         {
            A1234LogResponsavel_NovoStatus = H00IZ33_A1234LogResponsavel_NovoStatus[0];
            n1234LogResponsavel_NovoStatus = H00IZ33_n1234LogResponsavel_NovoStatus[0];
            A892LogResponsavel_DemandaCod = H00IZ33_A892LogResponsavel_DemandaCod[0];
            n892LogResponsavel_DemandaCod = H00IZ33_n892LogResponsavel_DemandaCod[0];
            A1177LogResponsavel_Prazo = H00IZ33_A1177LogResponsavel_Prazo[0];
            n1177LogResponsavel_Prazo = H00IZ33_n1177LogResponsavel_Prazo[0];
            A1797LogResponsavel_Codigo = H00IZ33_A1797LogResponsavel_Codigo[0];
            AV51PrazoAnterior = DateTimeUtil.ResetTime(A1177LogResponsavel_Prazo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51PrazoAnterior", context.localUtil.Format(AV51PrazoAnterior, "99/99/99"));
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(26);
         }
         pr_default.close(26);
      }

      protected void S252( )
      {
         /* 'ENCAMINHAOUDEVOLVE' Routine */
         if ( ( AV5Usuario_Codigo == 900000 ) && ( AV27Prestadora_Codigo == AV24Contratada_Codigo ) )
         {
            /* Execute user subroutine: 'GETPRAZORESPOSTA' */
            S342 ();
            if (returnInSub) return;
         }
         else
         {
            AV219GXLvl1246 = 0;
            /* Using cursor H00IZ34 */
            pr_default.execute(27, new Object[] {AV21Selecionada});
            while ( (pr_default.getStatus(27) != 101) )
            {
               A894LogResponsavel_Acao = H00IZ34_A894LogResponsavel_Acao[0];
               A1177LogResponsavel_Prazo = H00IZ34_A1177LogResponsavel_Prazo[0];
               n1177LogResponsavel_Prazo = H00IZ34_n1177LogResponsavel_Prazo[0];
               A1797LogResponsavel_Codigo = H00IZ34_A1797LogResponsavel_Codigo[0];
               A896LogResponsavel_Owner = H00IZ34_A896LogResponsavel_Owner[0];
               A891LogResponsavel_UsuarioCod = H00IZ34_A891LogResponsavel_UsuarioCod[0];
               n891LogResponsavel_UsuarioCod = H00IZ34_n891LogResponsavel_UsuarioCod[0];
               A892LogResponsavel_DemandaCod = H00IZ34_A892LogResponsavel_DemandaCod[0];
               n892LogResponsavel_DemandaCod = H00IZ34_n892LogResponsavel_DemandaCod[0];
               GXt_boolean11 = A1149LogResponsavel_OwnerEhContratante;
               new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A896LogResponsavel_Owner, out  GXt_boolean11) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A892LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A892LogResponsavel_DemandaCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A896LogResponsavel_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A896LogResponsavel_Owner), 6, 0)));
               A1149LogResponsavel_OwnerEhContratante = GXt_boolean11;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1149LogResponsavel_OwnerEhContratante", A1149LogResponsavel_OwnerEhContratante);
               GXt_boolean11 = A1148LogResponsavel_UsuarioEhContratante;
               new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A891LogResponsavel_UsuarioCod, out  GXt_boolean11) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A892LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A892LogResponsavel_DemandaCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A891LogResponsavel_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A891LogResponsavel_UsuarioCod), 6, 0)));
               A1148LogResponsavel_UsuarioEhContratante = GXt_boolean11;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1148LogResponsavel_UsuarioEhContratante", A1148LogResponsavel_UsuarioEhContratante);
               AV219GXLvl1246 = 1;
               if ( ( ( A896LogResponsavel_Owner == AV15AtribuidoA_Codigo ) || ( AV8UserEhContratante && A1149LogResponsavel_OwnerEhContratante ) ) && ( ( A891LogResponsavel_UsuarioCod == AV12UserId ) || ( AV8UserEhContratante && A1148LogResponsavel_UsuarioEhContratante ) ) )
               {
                  AV45PrazoEntrega = A1177LogResponsavel_Prazo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45PrazoEntrega", context.localUtil.TToC( AV45PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
               }
               else
               {
                  /* Execute user subroutine: 'GETPRAZORESPOSTA' */
                  S342 ();
                  if ( returnInSub )
                  {
                     pr_default.close(27);
                     returnInSub = true;
                     if (true) return;
                  }
               }
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(27);
            }
            pr_default.close(27);
            if ( AV219GXLvl1246 == 0 )
            {
               /* Execute user subroutine: 'GETPRAZORESPOSTA' */
               S342 ();
               if (returnInSub) return;
            }
         }
      }

      protected void S342( )
      {
         /* 'GETPRAZORESPOSTA' Routine */
         if ( ! AV8UserEhContratante )
         {
            /* Using cursor H00IZ35 */
            pr_default.execute(28, new Object[] {AV21Selecionada});
            while ( (pr_default.getStatus(28) != 101) )
            {
               A1553ContagemResultado_CntSrvCod = H00IZ35_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = H00IZ35_n1553ContagemResultado_CntSrvCod[0];
               A456ContagemResultado_Codigo = H00IZ35_A456ContagemResultado_Codigo[0];
               A1618ContagemResultado_PrzRsp = H00IZ35_A1618ContagemResultado_PrzRsp[0];
               n1618ContagemResultado_PrzRsp = H00IZ35_n1618ContagemResultado_PrzRsp[0];
               A1611ContagemResultado_PrzTpDias = H00IZ35_A1611ContagemResultado_PrzTpDias[0];
               n1611ContagemResultado_PrzTpDias = H00IZ35_n1611ContagemResultado_PrzTpDias[0];
               A1618ContagemResultado_PrzRsp = H00IZ35_A1618ContagemResultado_PrzRsp[0];
               n1618ContagemResultado_PrzRsp = H00IZ35_n1618ContagemResultado_PrzRsp[0];
               A1611ContagemResultado_PrzTpDias = H00IZ35_A1611ContagemResultado_PrzTpDias[0];
               n1611ContagemResultado_PrzTpDias = H00IZ35_n1611ContagemResultado_PrzTpDias[0];
               AV26Dias = A1618ContagemResultado_PrzRsp;
               AV107TipoDias = A1611ContagemResultado_PrzTpDias;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV107TipoDias", AV107TipoDias);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(28);
            if ( (0==AV26Dias) )
            {
               AV26Dias = 5;
            }
            AV45PrazoEntrega = DateTimeUtil.ResetTime( DateTimeUtil.ServerDate( context, "DEFAULT") ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45PrazoEntrega", context.localUtil.TToC( AV45PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
            GXt_dtime17 = AV45PrazoEntrega;
            new prc_adddiasuteis(context ).execute(  AV45PrazoEntrega,  AV26Dias,  AV107TipoDias, out  GXt_dtime17) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45PrazoEntrega", context.localUtil.TToC( AV45PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV107TipoDias", AV107TipoDias);
            AV45PrazoEntrega = GXt_dtime17;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45PrazoEntrega", context.localUtil.TToC( AV45PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
         }
      }

      protected void S242( )
      {
         /* 'NOVOPRAZOENTREGA' Routine */
         AV45PrazoEntrega = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45PrazoEntrega", context.localUtil.TToC( AV45PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
         GXt_int13 = AV26Dias;
         new prc_diasparaentrega(context ).execute( ref  AV116ContratoServicos_Codigo,  AV29Unidades,  0, out  GXt_int13) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV116ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV116ContratoServicos_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Unidades", StringUtil.LTrim( StringUtil.Str( AV29Unidades, 14, 5)));
         AV26Dias = GXt_int13;
         if ( AV26Dias > 0 )
         {
            AV45PrazoEntrega = DateTimeUtil.ResetTime( DateTimeUtil.ServerDate( context, "DEFAULT") ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45PrazoEntrega", context.localUtil.TToC( AV45PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
            GXt_dtime17 = AV45PrazoEntrega;
            new prc_adddiasuteis(context ).execute(  AV45PrazoEntrega,  AV26Dias,  AV107TipoDias, out  GXt_dtime17) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45PrazoEntrega", context.localUtil.TToC( AV45PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV107TipoDias", AV107TipoDias);
            AV45PrazoEntrega = GXt_dtime17;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45PrazoEntrega", context.localUtil.TToC( AV45PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
         }
         if ( (DateTime.MinValue==AV45PrazoEntrega) )
         {
            GXt_dtime17 = AV45PrazoEntrega;
            new prc_primeiroprazoentregadadmn(context ).execute( ref  AV21Selecionada, out  GXt_dtime17) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Selecionada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Selecionada), 6, 0)));
            AV45PrazoEntrega = GXt_dtime17;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45PrazoEntrega", context.localUtil.TToC( AV45PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
         }
         GXt_char14 = "NovoPrazoEntrega = " + context.localUtil.Format( AV45PrazoEntrega, "99/99/99 99:99");
         new geralog(context ).execute( ref  GXt_char14) ;
      }

      protected void S262( )
      {
         /* 'ADDHORASENTREGA' Routine */
         if ( ! (DateTime.MinValue==AV45PrazoEntrega) || AV74AnaliseImplicito || AV94ExecucaoImplicita )
         {
            if ( DateTimeUtil.Hour( AV102HoraEntrega) + DateTimeUtil.Minute( AV102HoraEntrega) + DateTimeUtil.Second( AV102HoraEntrega) == 0 )
            {
               new prc_gethorasentrega(context ).execute(  AV31AreaTrabalho_Codigo,  AV9Codigo,  0, out  AV32Hours, out  AV33Minutes) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31AreaTrabalho_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Codigo), 6, 0)));
            }
            else
            {
               AV32Hours = (short)(DateTimeUtil.Hour( AV102HoraEntrega));
               AV33Minutes = (short)(DateTimeUtil.Minute( AV102HoraEntrega));
            }
            if ( ! (DateTime.MinValue==AV45PrazoEntrega) )
            {
               if ( ( DateTimeUtil.Hour( AV45PrazoEntrega) + DateTimeUtil.Minute( AV45PrazoEntrega) + DateTimeUtil.Second( AV45PrazoEntrega) ) == 0 )
               {
                  AV45PrazoEntrega = DateTimeUtil.TAdd( AV45PrazoEntrega, 3600*(AV32Hours));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45PrazoEntrega", context.localUtil.TToC( AV45PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
                  AV45PrazoEntrega = DateTimeUtil.TAdd( AV45PrazoEntrega, 60*(AV33Minutes));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45PrazoEntrega", context.localUtil.TToC( AV45PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
               }
            }
            if ( AV74AnaliseImplicito )
            {
               if ( ( DateTimeUtil.Hour( AV75PrazoAnalise) + DateTimeUtil.Minute( AV75PrazoAnalise) + DateTimeUtil.Second( AV75PrazoAnalise) ) == 0 )
               {
                  AV75PrazoAnalise = DateTimeUtil.TAdd( AV75PrazoAnalise, 3600*(AV32Hours));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75PrazoAnalise", context.localUtil.TToC( AV75PrazoAnalise, 8, 5, 0, 3, "/", ":", " "));
                  AV75PrazoAnalise = DateTimeUtil.TAdd( AV75PrazoAnalise, 60*(AV33Minutes));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75PrazoAnalise", context.localUtil.TToC( AV75PrazoAnalise, 8, 5, 0, 3, "/", ":", " "));
               }
            }
         }
      }

      protected void S352( )
      {
         /* 'ULTIMOATORDACONTRATANTE' Routine */
         AV221GXLvl1327 = 0;
         /* Using cursor H00IZ36 */
         pr_default.execute(29, new Object[] {AV21Selecionada});
         while ( (pr_default.getStatus(29) != 101) )
         {
            A1797LogResponsavel_Codigo = H00IZ36_A1797LogResponsavel_Codigo[0];
            A896LogResponsavel_Owner = H00IZ36_A896LogResponsavel_Owner[0];
            A892LogResponsavel_DemandaCod = H00IZ36_A892LogResponsavel_DemandaCod[0];
            n892LogResponsavel_DemandaCod = H00IZ36_n892LogResponsavel_DemandaCod[0];
            GXt_boolean11 = A1149LogResponsavel_OwnerEhContratante;
            new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A896LogResponsavel_Owner, out  GXt_boolean11) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A892LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A892LogResponsavel_DemandaCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A896LogResponsavel_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A896LogResponsavel_Owner), 6, 0)));
            A1149LogResponsavel_OwnerEhContratante = GXt_boolean11;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1149LogResponsavel_OwnerEhContratante", A1149LogResponsavel_OwnerEhContratante);
            if ( A1149LogResponsavel_OwnerEhContratante )
            {
               AV221GXLvl1327 = 1;
               AV15AtribuidoA_Codigo = A896LogResponsavel_Owner;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15AtribuidoA_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15AtribuidoA_Codigo), 6, 0)));
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            pr_default.readNext(29);
         }
         pr_default.close(29);
         if ( AV221GXLvl1327 == 0 )
         {
            /* Using cursor H00IZ37 */
            pr_default.execute(30, new Object[] {AV21Selecionada});
            while ( (pr_default.getStatus(30) != 101) )
            {
               A468ContagemResultado_NaoCnfDmnCod = H00IZ37_A468ContagemResultado_NaoCnfDmnCod[0];
               n468ContagemResultado_NaoCnfDmnCod = H00IZ37_n468ContagemResultado_NaoCnfDmnCod[0];
               A1553ContagemResultado_CntSrvCod = H00IZ37_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = H00IZ37_n1553ContagemResultado_CntSrvCod[0];
               A1603ContagemResultado_CntCod = H00IZ37_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = H00IZ37_n1603ContagemResultado_CntCod[0];
               A39Contratada_Codigo = H00IZ37_A39Contratada_Codigo[0];
               n39Contratada_Codigo = H00IZ37_n39Contratada_Codigo[0];
               A428NaoConformidade_AreaTrabalhoCod = H00IZ37_A428NaoConformidade_AreaTrabalhoCod[0];
               n428NaoConformidade_AreaTrabalhoCod = H00IZ37_n428NaoConformidade_AreaTrabalhoCod[0];
               A429NaoConformidade_Tipo = H00IZ37_A429NaoConformidade_Tipo[0];
               n429NaoConformidade_Tipo = H00IZ37_n429NaoConformidade_Tipo[0];
               A92Contrato_Ativo = H00IZ37_A92Contrato_Ativo[0];
               n92Contrato_Ativo = H00IZ37_n92Contrato_Ativo[0];
               A456ContagemResultado_Codigo = H00IZ37_A456ContagemResultado_Codigo[0];
               A83Contrato_DataVigenciaTermino = H00IZ37_A83Contrato_DataVigenciaTermino[0];
               n83Contrato_DataVigenciaTermino = H00IZ37_n83Contrato_DataVigenciaTermino[0];
               A428NaoConformidade_AreaTrabalhoCod = H00IZ37_A428NaoConformidade_AreaTrabalhoCod[0];
               n428NaoConformidade_AreaTrabalhoCod = H00IZ37_n428NaoConformidade_AreaTrabalhoCod[0];
               A429NaoConformidade_Tipo = H00IZ37_A429NaoConformidade_Tipo[0];
               n429NaoConformidade_Tipo = H00IZ37_n429NaoConformidade_Tipo[0];
               A1603ContagemResultado_CntCod = H00IZ37_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = H00IZ37_n1603ContagemResultado_CntCod[0];
               A39Contratada_Codigo = H00IZ37_A39Contratada_Codigo[0];
               n39Contratada_Codigo = H00IZ37_n39Contratada_Codigo[0];
               A92Contrato_Ativo = H00IZ37_A92Contrato_Ativo[0];
               n92Contrato_Ativo = H00IZ37_n92Contrato_Ativo[0];
               A83Contrato_DataVigenciaTermino = H00IZ37_A83Contrato_DataVigenciaTermino[0];
               n83Contrato_DataVigenciaTermino = H00IZ37_n83Contrato_DataVigenciaTermino[0];
               AV223GXLvl1336 = 0;
               /* Using cursor H00IZ38 */
               pr_default.execute(31, new Object[] {n1603ContagemResultado_CntCod, A1603ContagemResultado_CntCod});
               while ( (pr_default.getStatus(31) != 101) )
               {
                  A1078ContratoGestor_ContratoCod = H00IZ38_A1078ContratoGestor_ContratoCod[0];
                  A1079ContratoGestor_UsuarioCod = H00IZ38_A1079ContratoGestor_UsuarioCod[0];
                  A1446ContratoGestor_ContratadaAreaCod = H00IZ38_A1446ContratoGestor_ContratadaAreaCod[0];
                  n1446ContratoGestor_ContratadaAreaCod = H00IZ38_n1446ContratoGestor_ContratadaAreaCod[0];
                  A1446ContratoGestor_ContratadaAreaCod = H00IZ38_A1446ContratoGestor_ContratadaAreaCod[0];
                  n1446ContratoGestor_ContratadaAreaCod = H00IZ38_n1446ContratoGestor_ContratadaAreaCod[0];
                  GXt_boolean11 = A1135ContratoGestor_UsuarioEhContratante;
                  new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean11) ;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1446ContratoGestor_ContratadaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
                  A1135ContratoGestor_UsuarioEhContratante = GXt_boolean11;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1135ContratoGestor_UsuarioEhContratante", A1135ContratoGestor_UsuarioEhContratante);
                  if ( A1135ContratoGestor_UsuarioEhContratante )
                  {
                     AV223GXLvl1336 = 1;
                     AV15AtribuidoA_Codigo = A1079ContratoGestor_UsuarioCod;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15AtribuidoA_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15AtribuidoA_Codigo), 6, 0)));
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                  }
                  pr_default.readNext(31);
               }
               pr_default.close(31);
               if ( AV223GXLvl1336 == 0 )
               {
                  AV56SemUsuarioContratante = true;
               }
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(30);
         }
      }

      protected void S212( )
      {
         /* 'SEMTRAMITACOESANTERIORES' Routine */
         if ( AV27Prestadora_Codigo == AV5Usuario_Codigo - 900000 )
         {
            /* Using cursor H00IZ39 */
            pr_default.execute(32, new Object[] {AV21Selecionada});
            while ( (pr_default.getStatus(32) != 101) )
            {
               A468ContagemResultado_NaoCnfDmnCod = H00IZ39_A468ContagemResultado_NaoCnfDmnCod[0];
               n468ContagemResultado_NaoCnfDmnCod = H00IZ39_n468ContagemResultado_NaoCnfDmnCod[0];
               A1553ContagemResultado_CntSrvCod = H00IZ39_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = H00IZ39_n1553ContagemResultado_CntSrvCod[0];
               A1603ContagemResultado_CntCod = H00IZ39_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = H00IZ39_n1603ContagemResultado_CntCod[0];
               A39Contratada_Codigo = H00IZ39_A39Contratada_Codigo[0];
               n39Contratada_Codigo = H00IZ39_n39Contratada_Codigo[0];
               A428NaoConformidade_AreaTrabalhoCod = H00IZ39_A428NaoConformidade_AreaTrabalhoCod[0];
               n428NaoConformidade_AreaTrabalhoCod = H00IZ39_n428NaoConformidade_AreaTrabalhoCod[0];
               A429NaoConformidade_Tipo = H00IZ39_A429NaoConformidade_Tipo[0];
               n429NaoConformidade_Tipo = H00IZ39_n429NaoConformidade_Tipo[0];
               A92Contrato_Ativo = H00IZ39_A92Contrato_Ativo[0];
               n92Contrato_Ativo = H00IZ39_n92Contrato_Ativo[0];
               A456ContagemResultado_Codigo = H00IZ39_A456ContagemResultado_Codigo[0];
               A1604ContagemResultado_CntPrpCod = H00IZ39_A1604ContagemResultado_CntPrpCod[0];
               n1604ContagemResultado_CntPrpCod = H00IZ39_n1604ContagemResultado_CntPrpCod[0];
               A83Contrato_DataVigenciaTermino = H00IZ39_A83Contrato_DataVigenciaTermino[0];
               n83Contrato_DataVigenciaTermino = H00IZ39_n83Contrato_DataVigenciaTermino[0];
               A428NaoConformidade_AreaTrabalhoCod = H00IZ39_A428NaoConformidade_AreaTrabalhoCod[0];
               n428NaoConformidade_AreaTrabalhoCod = H00IZ39_n428NaoConformidade_AreaTrabalhoCod[0];
               A429NaoConformidade_Tipo = H00IZ39_A429NaoConformidade_Tipo[0];
               n429NaoConformidade_Tipo = H00IZ39_n429NaoConformidade_Tipo[0];
               A1603ContagemResultado_CntCod = H00IZ39_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = H00IZ39_n1603ContagemResultado_CntCod[0];
               A39Contratada_Codigo = H00IZ39_A39Contratada_Codigo[0];
               n39Contratada_Codigo = H00IZ39_n39Contratada_Codigo[0];
               A92Contrato_Ativo = H00IZ39_A92Contrato_Ativo[0];
               n92Contrato_Ativo = H00IZ39_n92Contrato_Ativo[0];
               A1604ContagemResultado_CntPrpCod = H00IZ39_A1604ContagemResultado_CntPrpCod[0];
               n1604ContagemResultado_CntPrpCod = H00IZ39_n1604ContagemResultado_CntPrpCod[0];
               A83Contrato_DataVigenciaTermino = H00IZ39_A83Contrato_DataVigenciaTermino[0];
               n83Contrato_DataVigenciaTermino = H00IZ39_n83Contrato_DataVigenciaTermino[0];
               AV225GXLvl1355 = 0;
               /* Using cursor H00IZ40 */
               pr_default.execute(33, new Object[] {n1603ContagemResultado_CntCod, A1603ContagemResultado_CntCod});
               while ( (pr_default.getStatus(33) != 101) )
               {
                  A1078ContratoGestor_ContratoCod = H00IZ40_A1078ContratoGestor_ContratoCod[0];
                  A1079ContratoGestor_UsuarioCod = H00IZ40_A1079ContratoGestor_UsuarioCod[0];
                  A1446ContratoGestor_ContratadaAreaCod = H00IZ40_A1446ContratoGestor_ContratadaAreaCod[0];
                  n1446ContratoGestor_ContratadaAreaCod = H00IZ40_n1446ContratoGestor_ContratadaAreaCod[0];
                  A1446ContratoGestor_ContratadaAreaCod = H00IZ40_A1446ContratoGestor_ContratadaAreaCod[0];
                  n1446ContratoGestor_ContratadaAreaCod = H00IZ40_n1446ContratoGestor_ContratadaAreaCod[0];
                  GXt_boolean11 = A1135ContratoGestor_UsuarioEhContratante;
                  new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean11) ;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1446ContratoGestor_ContratadaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
                  A1135ContratoGestor_UsuarioEhContratante = GXt_boolean11;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1135ContratoGestor_UsuarioEhContratante", A1135ContratoGestor_UsuarioEhContratante);
                  if ( ! A1135ContratoGestor_UsuarioEhContratante )
                  {
                     AV225GXLvl1355 = 1;
                     AV15AtribuidoA_Codigo = A1079ContratoGestor_UsuarioCod;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15AtribuidoA_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15AtribuidoA_Codigo), 6, 0)));
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                  }
                  pr_default.readNext(33);
               }
               pr_default.close(33);
               if ( AV225GXLvl1355 == 0 )
               {
                  AV15AtribuidoA_Codigo = A1604ContagemResultado_CntPrpCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15AtribuidoA_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15AtribuidoA_Codigo), 6, 0)));
               }
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(32);
         }
         else
         {
            /* Using cursor H00IZ43 */
            pr_default.execute(34, new Object[] {AV188ContratoFS});
            while ( (pr_default.getStatus(34) != 101) )
            {
               A74Contrato_Codigo = H00IZ43_A74Contrato_Codigo[0];
               n74Contrato_Codigo = H00IZ43_n74Contrato_Codigo[0];
               A39Contratada_Codigo = H00IZ43_A39Contratada_Codigo[0];
               n39Contratada_Codigo = H00IZ43_n39Contratada_Codigo[0];
               A92Contrato_Ativo = H00IZ43_A92Contrato_Ativo[0];
               n92Contrato_Ativo = H00IZ43_n92Contrato_Ativo[0];
               A1013Contrato_PrepostoCod = H00IZ43_A1013Contrato_PrepostoCod[0];
               n1013Contrato_PrepostoCod = H00IZ43_n1013Contrato_PrepostoCod[0];
               A843Contrato_DataFimTA = H00IZ43_A843Contrato_DataFimTA[0];
               n843Contrato_DataFimTA = H00IZ43_n843Contrato_DataFimTA[0];
               A83Contrato_DataVigenciaTermino = H00IZ43_A83Contrato_DataVigenciaTermino[0];
               n83Contrato_DataVigenciaTermino = H00IZ43_n83Contrato_DataVigenciaTermino[0];
               A843Contrato_DataFimTA = H00IZ43_A843Contrato_DataFimTA[0];
               n843Contrato_DataFimTA = H00IZ43_n843Contrato_DataFimTA[0];
               A1869Contrato_DataTermino = ((A83Contrato_DataVigenciaTermino>A843Contrato_DataFimTA) ? A83Contrato_DataVigenciaTermino : A843Contrato_DataFimTA);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1869Contrato_DataTermino", context.localUtil.Format(A1869Contrato_DataTermino, "99/99/99"));
               AV227GXLvl1368 = 0;
               /* Using cursor H00IZ44 */
               pr_default.execute(35, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
               while ( (pr_default.getStatus(35) != 101) )
               {
                  A1078ContratoGestor_ContratoCod = H00IZ44_A1078ContratoGestor_ContratoCod[0];
                  A1079ContratoGestor_UsuarioCod = H00IZ44_A1079ContratoGestor_UsuarioCod[0];
                  A1446ContratoGestor_ContratadaAreaCod = H00IZ44_A1446ContratoGestor_ContratadaAreaCod[0];
                  n1446ContratoGestor_ContratadaAreaCod = H00IZ44_n1446ContratoGestor_ContratadaAreaCod[0];
                  A1446ContratoGestor_ContratadaAreaCod = H00IZ44_A1446ContratoGestor_ContratadaAreaCod[0];
                  n1446ContratoGestor_ContratadaAreaCod = H00IZ44_n1446ContratoGestor_ContratadaAreaCod[0];
                  GXt_boolean11 = A1135ContratoGestor_UsuarioEhContratante;
                  new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean11) ;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1446ContratoGestor_ContratadaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
                  A1135ContratoGestor_UsuarioEhContratante = GXt_boolean11;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1135ContratoGestor_UsuarioEhContratante", A1135ContratoGestor_UsuarioEhContratante);
                  if ( ! A1135ContratoGestor_UsuarioEhContratante )
                  {
                     AV227GXLvl1368 = 1;
                     AV15AtribuidoA_Codigo = A1079ContratoGestor_UsuarioCod;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15AtribuidoA_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15AtribuidoA_Codigo), 6, 0)));
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                  }
                  pr_default.readNext(35);
               }
               pr_default.close(35);
               if ( AV227GXLvl1368 == 0 )
               {
                  AV15AtribuidoA_Codigo = A1013Contrato_PrepostoCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15AtribuidoA_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15AtribuidoA_Codigo), 6, 0)));
               }
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(34);
         }
      }

      protected void S232( )
      {
         /* 'EXTENDEPRAZOENTREGA' Routine */
         GXt_int13 = AV26Dias;
         new prc_diasuteisentre(context ).execute(  DateTimeUtil.ResetTime( AV50PrazoInicial),  AV39PrazoResposta,  AV107TipoDias, out  GXt_int13) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39PrazoResposta", context.localUtil.Format(AV39PrazoResposta, "99/99/99"));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV107TipoDias", AV107TipoDias);
         AV26Dias = GXt_int13;
         new prc_updprazomaisdias(context ).execute( ref  AV21Selecionada,  AV26Dias) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Selecionada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Selecionada), 6, 0)));
         AV45PrazoEntrega = DateTimeUtil.ResetTime( AV39PrazoResposta ) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45PrazoEntrega", context.localUtil.TToC( AV45PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
      }

      protected void E21IZ2( )
      {
         /* 'DoFechar' Routine */
         AV30WebSession.Remove("Codigos");
         context.setWebReturnParms(new Object[] {(String)AV10Destino_Nome});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void S272( )
      {
         /* 'FECHAR' Routine */
         lblTbjava_Caption = "<script language=\"javascript\" type=\"text/javascript\">parent.location.replace(parent.location.href); </script>";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
      }

      protected void S122( )
      {
         /* 'DADOSOSVINCULADA' Routine */
         AV78TemVnc = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TemVnc", AV78TemVnc);
         if ( AV59OsVinculada > 0 )
         {
            AV228GXLvl1406 = 0;
            /* Using cursor H00IZ46 */
            pr_default.execute(36, new Object[] {AV59OsVinculada, AV27Prestadora_Codigo});
            while ( (pr_default.getStatus(36) != 101) )
            {
               A1636ContagemResultado_ServicoSS = H00IZ46_A1636ContagemResultado_ServicoSS[0];
               n1636ContagemResultado_ServicoSS = H00IZ46_n1636ContagemResultado_ServicoSS[0];
               A484ContagemResultado_StatusDmn = H00IZ46_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = H00IZ46_n484ContagemResultado_StatusDmn[0];
               A490ContagemResultado_ContratadaCod = H00IZ46_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = H00IZ46_n490ContagemResultado_ContratadaCod[0];
               A456ContagemResultado_Codigo = H00IZ46_A456ContagemResultado_Codigo[0];
               A1326ContagemResultado_ContratadaTipoFab = H00IZ46_A1326ContagemResultado_ContratadaTipoFab[0];
               n1326ContagemResultado_ContratadaTipoFab = H00IZ46_n1326ContagemResultado_ContratadaTipoFab[0];
               A531ContagemResultado_StatusUltCnt = H00IZ46_A531ContagemResultado_StatusUltCnt[0];
               A682ContagemResultado_PFBFMUltima = H00IZ46_A682ContagemResultado_PFBFMUltima[0];
               A683ContagemResultado_PFLFMUltima = H00IZ46_A683ContagemResultado_PFLFMUltima[0];
               A684ContagemResultado_PFBFSUltima = H00IZ46_A684ContagemResultado_PFBFSUltima[0];
               A685ContagemResultado_PFLFSUltima = H00IZ46_A685ContagemResultado_PFLFSUltima[0];
               A1326ContagemResultado_ContratadaTipoFab = H00IZ46_A1326ContagemResultado_ContratadaTipoFab[0];
               n1326ContagemResultado_ContratadaTipoFab = H00IZ46_n1326ContagemResultado_ContratadaTipoFab[0];
               A531ContagemResultado_StatusUltCnt = H00IZ46_A531ContagemResultado_StatusUltCnt[0];
               A682ContagemResultado_PFBFMUltima = H00IZ46_A682ContagemResultado_PFBFMUltima[0];
               A683ContagemResultado_PFLFMUltima = H00IZ46_A683ContagemResultado_PFLFMUltima[0];
               A684ContagemResultado_PFBFSUltima = H00IZ46_A684ContagemResultado_PFBFSUltima[0];
               A685ContagemResultado_PFLFSUltima = H00IZ46_A685ContagemResultado_PFLFSUltima[0];
               AV228GXLvl1406 = 1;
               AV98StatusDemandaVnc = A484ContagemResultado_StatusDmn;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV98StatusDemandaVnc", AV98StatusDemandaVnc);
               if ( StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "M") == 0 )
               {
                  AV70PFBOF = A682ContagemResultado_PFBFMUltima;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70PFBOF", StringUtil.LTrim( StringUtil.Str( AV70PFBOF, 14, 5)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFBOF", GetSecureSignedToken( "", context.localUtil.Format( AV70PFBOF, "ZZ,ZZZ,ZZ9.999")));
                  AV71PFLOF = A683ContagemResultado_PFLFMUltima;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71PFLOF", StringUtil.LTrim( StringUtil.Str( AV71PFLOF, 14, 5)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFLOF", GetSecureSignedToken( "", context.localUtil.Format( AV71PFLOF, "ZZ,ZZZ,ZZ9.999")));
               }
               else
               {
                  AV70PFBOF = A684ContagemResultado_PFBFSUltima;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70PFBOF", StringUtil.LTrim( StringUtil.Str( AV70PFBOF, 14, 5)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFBOF", GetSecureSignedToken( "", context.localUtil.Format( AV70PFBOF, "ZZ,ZZZ,ZZ9.999")));
                  AV71PFLOF = A685ContagemResultado_PFLFSUltima;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71PFLOF", StringUtil.LTrim( StringUtil.Str( AV71PFLOF, 14, 5)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFLOF", GetSecureSignedToken( "", context.localUtil.Format( AV71PFLOF, "ZZ,ZZZ,ZZ9.999")));
               }
               AV78TemVnc = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TemVnc", AV78TemVnc);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(36);
            if ( AV228GXLvl1406 == 0 )
            {
               AV59OsVinculada = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59OsVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59OsVinculada), 6, 0)));
            }
         }
         else
         {
            AV229GXLvl1432 = 0;
            /* Using cursor H00IZ48 */
            pr_default.execute(37, new Object[] {AV9Codigo, AV27Prestadora_Codigo});
            while ( (pr_default.getStatus(37) != 101) )
            {
               A1553ContagemResultado_CntSrvCod = H00IZ48_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = H00IZ48_n1553ContagemResultado_CntSrvCod[0];
               A1636ContagemResultado_ServicoSS = H00IZ48_A1636ContagemResultado_ServicoSS[0];
               n1636ContagemResultado_ServicoSS = H00IZ48_n1636ContagemResultado_ServicoSS[0];
               A484ContagemResultado_StatusDmn = H00IZ48_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = H00IZ48_n484ContagemResultado_StatusDmn[0];
               A1593ContagemResultado_CntSrvTpVnc = H00IZ48_A1593ContagemResultado_CntSrvTpVnc[0];
               n1593ContagemResultado_CntSrvTpVnc = H00IZ48_n1593ContagemResultado_CntSrvTpVnc[0];
               A490ContagemResultado_ContratadaCod = H00IZ48_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = H00IZ48_n490ContagemResultado_ContratadaCod[0];
               A602ContagemResultado_OSVinculada = H00IZ48_A602ContagemResultado_OSVinculada[0];
               n602ContagemResultado_OSVinculada = H00IZ48_n602ContagemResultado_OSVinculada[0];
               A1603ContagemResultado_CntCod = H00IZ48_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = H00IZ48_n1603ContagemResultado_CntCod[0];
               A803ContagemResultado_ContratadaSigla = H00IZ48_A803ContagemResultado_ContratadaSigla[0];
               n803ContagemResultado_ContratadaSigla = H00IZ48_n803ContagemResultado_ContratadaSigla[0];
               A456ContagemResultado_Codigo = H00IZ48_A456ContagemResultado_Codigo[0];
               A1326ContagemResultado_ContratadaTipoFab = H00IZ48_A1326ContagemResultado_ContratadaTipoFab[0];
               n1326ContagemResultado_ContratadaTipoFab = H00IZ48_n1326ContagemResultado_ContratadaTipoFab[0];
               A531ContagemResultado_StatusUltCnt = H00IZ48_A531ContagemResultado_StatusUltCnt[0];
               A682ContagemResultado_PFBFMUltima = H00IZ48_A682ContagemResultado_PFBFMUltima[0];
               A683ContagemResultado_PFLFMUltima = H00IZ48_A683ContagemResultado_PFLFMUltima[0];
               A684ContagemResultado_PFBFSUltima = H00IZ48_A684ContagemResultado_PFBFSUltima[0];
               A685ContagemResultado_PFLFSUltima = H00IZ48_A685ContagemResultado_PFLFSUltima[0];
               A1593ContagemResultado_CntSrvTpVnc = H00IZ48_A1593ContagemResultado_CntSrvTpVnc[0];
               n1593ContagemResultado_CntSrvTpVnc = H00IZ48_n1593ContagemResultado_CntSrvTpVnc[0];
               A1603ContagemResultado_CntCod = H00IZ48_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = H00IZ48_n1603ContagemResultado_CntCod[0];
               A803ContagemResultado_ContratadaSigla = H00IZ48_A803ContagemResultado_ContratadaSigla[0];
               n803ContagemResultado_ContratadaSigla = H00IZ48_n803ContagemResultado_ContratadaSigla[0];
               A1326ContagemResultado_ContratadaTipoFab = H00IZ48_A1326ContagemResultado_ContratadaTipoFab[0];
               n1326ContagemResultado_ContratadaTipoFab = H00IZ48_n1326ContagemResultado_ContratadaTipoFab[0];
               A531ContagemResultado_StatusUltCnt = H00IZ48_A531ContagemResultado_StatusUltCnt[0];
               A682ContagemResultado_PFBFMUltima = H00IZ48_A682ContagemResultado_PFBFMUltima[0];
               A683ContagemResultado_PFLFMUltima = H00IZ48_A683ContagemResultado_PFLFMUltima[0];
               A684ContagemResultado_PFBFSUltima = H00IZ48_A684ContagemResultado_PFBFSUltima[0];
               A685ContagemResultado_PFLFSUltima = H00IZ48_A685ContagemResultado_PFLFSUltima[0];
               AV229GXLvl1432 = 1;
               AV66ContratadaOrigem_Codigo = A490ContagemResultado_ContratadaCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ContratadaOrigem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66ContratadaOrigem_Codigo), 6, 0)));
               AV115ContratoOrigem_Codigo = A1603ContagemResultado_CntCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV115ContratoOrigem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV115ContratoOrigem_Codigo), 6, 0)));
               AV110Origem_Sigla = A803ContagemResultado_ContratadaSigla + " ";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV110Origem_Sigla", AV110Origem_Sigla);
               AV59OsVinculada = A456ContagemResultado_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59OsVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59OsVinculada), 6, 0)));
               AV98StatusDemandaVnc = A484ContagemResultado_StatusDmn;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV98StatusDemandaVnc", AV98StatusDemandaVnc);
               if ( StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "M") == 0 )
               {
                  AV70PFBOF = A682ContagemResultado_PFBFMUltima;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70PFBOF", StringUtil.LTrim( StringUtil.Str( AV70PFBOF, 14, 5)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFBOF", GetSecureSignedToken( "", context.localUtil.Format( AV70PFBOF, "ZZ,ZZZ,ZZ9.999")));
                  AV71PFLOF = A683ContagemResultado_PFLFMUltima;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71PFLOF", StringUtil.LTrim( StringUtil.Str( AV71PFLOF, 14, 5)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFLOF", GetSecureSignedToken( "", context.localUtil.Format( AV71PFLOF, "ZZ,ZZZ,ZZ9.999")));
               }
               else
               {
                  AV70PFBOF = A684ContagemResultado_PFBFSUltima;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70PFBOF", StringUtil.LTrim( StringUtil.Str( AV70PFBOF, 14, 5)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFBOF", GetSecureSignedToken( "", context.localUtil.Format( AV70PFBOF, "ZZ,ZZZ,ZZ9.999")));
                  AV71PFLOF = A685ContagemResultado_PFLFSUltima;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71PFLOF", StringUtil.LTrim( StringUtil.Str( AV71PFLOF, 14, 5)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFLOF", GetSecureSignedToken( "", context.localUtil.Format( AV71PFLOF, "ZZ,ZZZ,ZZ9.999")));
               }
               AV78TemVnc = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TemVnc", AV78TemVnc);
               pr_default.readNext(37);
            }
            pr_default.close(37);
            if ( AV229GXLvl1432 == 0 )
            {
               AV110Origem_Sigla = "F�BRICA";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV110Origem_Sigla", AV110Origem_Sigla);
            }
         }
      }

      protected void S202( )
      {
         /* 'TRATARDIVERGENCIA' Routine */
         AV87Sdt_ContagemResultadoEvidencias.FromXml(AV30WebSession.Get("ArquivosEvd"), "");
         if ( (0==AV5Usuario_Codigo) )
         {
            GX_msglist.addItem("Selecione a a��o desejada!");
         }
         else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV6Observacao)) )
         {
            GX_msglist.addItem("Campo Observa��es � obrigat�rio!");
         }
         else if ( ( ( AV63NewPFB != AV104OldPFB ) || ( AV64NewPFL != AV105OldPFL ) ) && (0==AV87Sdt_ContagemResultadoEvidencias.Count) )
         {
            GX_msglist.addItem("Arquivo anexo � obrigat�rio!");
         }
         else if ( AV5Usuario_Codigo == 800001 )
         {
            new prc_acata(context ).execute(  AV9Codigo,  AV59OsVinculada,  AV12UserId,  AV70PFBOF,  AV71PFLOF,  AV6Observacao) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59OsVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59OsVinculada), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12UserId), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70PFBOF", StringUtil.LTrim( StringUtil.Str( AV70PFBOF, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFBOF", GetSecureSignedToken( "", context.localUtil.Format( AV70PFBOF, "ZZ,ZZZ,ZZ9.999")));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71PFLOF", StringUtil.LTrim( StringUtil.Str( AV71PFLOF, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFLOF", GetSecureSignedToken( "", context.localUtil.Format( AV71PFLOF, "ZZ,ZZZ,ZZ9.999")));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Observacao", AV6Observacao);
            new prc_newevidenciademanda(context ).execute( ref  AV9Codigo) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Codigo), 6, 0)));
            AV10Destino_Nome = "Acatado";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Destino_Nome", AV10Destino_Nome);
            /* Execute user subroutine: 'FECHAR' */
            S272 ();
            if (returnInSub) return;
         }
         else if ( AV5Usuario_Codigo == 800002 )
         {
            AV10Destino_Nome = "N�o acatado";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Destino_Nome", AV10Destino_Nome);
            if ( AV53WWPContext.gxTpr_Userehcontratante )
            {
               AV69NovaContagem = (bool)(((AV70PFBOF!=AV63NewPFB)||(AV71PFLOF!=AV64NewPFL)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69NovaContagem", AV69NovaContagem);
            }
            else
            {
               AV69NovaContagem = (bool)(((AV57PFB!=AV63NewPFB)||(AV58PFL!=AV64NewPFL)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69NovaContagem", AV69NovaContagem);
            }
            AV72EmReuniao = (bool)(((StringUtil.StrCmp(AV16StatusDemanda, "B")==0))&&((StringUtil.StrCmp(AV98StatusDemandaVnc, "B")==0)||(0==AV66ContratadaOrigem_Codigo)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72EmReuniao", AV72EmReuniao);
            GXt_boolean11 = AV166UsaOSistema;
            GXt_int15 = (AV61UsuarioDaPrestadora ? AV66ContratadaOrigem_Codigo : AV27Prestadora_Codigo);
            GXt_int12 = 0;
            new prc_usaosistema(context ).execute( ref  GXt_int15, ref  GXt_int12, out  GXt_boolean11) ;
            AV166UsaOSistema = GXt_boolean11;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV166UsaOSistema", AV166UsaOSistema);
            if ( AV61UsuarioDaPrestadora )
            {
               if ( AV66ContratadaOrigem_Codigo > 0 )
               {
                  /* Using cursor H00IZ49 */
                  pr_default.execute(38, new Object[] {AV187ContagemResultado_Codigo});
                  while ( (pr_default.getStatus(38) != 101) )
                  {
                     A468ContagemResultado_NaoCnfDmnCod = H00IZ49_A468ContagemResultado_NaoCnfDmnCod[0];
                     n468ContagemResultado_NaoCnfDmnCod = H00IZ49_n468ContagemResultado_NaoCnfDmnCod[0];
                     A490ContagemResultado_ContratadaCod = H00IZ49_A490ContagemResultado_ContratadaCod[0];
                     n490ContagemResultado_ContratadaCod = H00IZ49_n490ContagemResultado_ContratadaCod[0];
                     A1553ContagemResultado_CntSrvCod = H00IZ49_A1553ContagemResultado_CntSrvCod[0];
                     n1553ContagemResultado_CntSrvCod = H00IZ49_n1553ContagemResultado_CntSrvCod[0];
                     A1603ContagemResultado_CntCod = H00IZ49_A1603ContagemResultado_CntCod[0];
                     n1603ContagemResultado_CntCod = H00IZ49_n1603ContagemResultado_CntCod[0];
                     A1481Contratada_UsaOSistema = H00IZ49_A1481Contratada_UsaOSistema[0];
                     n1481Contratada_UsaOSistema = H00IZ49_n1481Contratada_UsaOSistema[0];
                     A39Contratada_Codigo = H00IZ49_A39Contratada_Codigo[0];
                     n39Contratada_Codigo = H00IZ49_n39Contratada_Codigo[0];
                     A428NaoConformidade_AreaTrabalhoCod = H00IZ49_A428NaoConformidade_AreaTrabalhoCod[0];
                     n428NaoConformidade_AreaTrabalhoCod = H00IZ49_n428NaoConformidade_AreaTrabalhoCod[0];
                     A429NaoConformidade_Tipo = H00IZ49_A429NaoConformidade_Tipo[0];
                     n429NaoConformidade_Tipo = H00IZ49_n429NaoConformidade_Tipo[0];
                     A92Contrato_Ativo = H00IZ49_A92Contrato_Ativo[0];
                     n92Contrato_Ativo = H00IZ49_n92Contrato_Ativo[0];
                     A1797LogResponsavel_Codigo = H00IZ49_A1797LogResponsavel_Codigo[0];
                     A896LogResponsavel_Owner = H00IZ49_A896LogResponsavel_Owner[0];
                     A891LogResponsavel_UsuarioCod = H00IZ49_A891LogResponsavel_UsuarioCod[0];
                     n891LogResponsavel_UsuarioCod = H00IZ49_n891LogResponsavel_UsuarioCod[0];
                     A892LogResponsavel_DemandaCod = H00IZ49_A892LogResponsavel_DemandaCod[0];
                     n892LogResponsavel_DemandaCod = H00IZ49_n892LogResponsavel_DemandaCod[0];
                     A83Contrato_DataVigenciaTermino = H00IZ49_A83Contrato_DataVigenciaTermino[0];
                     n83Contrato_DataVigenciaTermino = H00IZ49_n83Contrato_DataVigenciaTermino[0];
                     A468ContagemResultado_NaoCnfDmnCod = H00IZ49_A468ContagemResultado_NaoCnfDmnCod[0];
                     n468ContagemResultado_NaoCnfDmnCod = H00IZ49_n468ContagemResultado_NaoCnfDmnCod[0];
                     A490ContagemResultado_ContratadaCod = H00IZ49_A490ContagemResultado_ContratadaCod[0];
                     n490ContagemResultado_ContratadaCod = H00IZ49_n490ContagemResultado_ContratadaCod[0];
                     A1553ContagemResultado_CntSrvCod = H00IZ49_A1553ContagemResultado_CntSrvCod[0];
                     n1553ContagemResultado_CntSrvCod = H00IZ49_n1553ContagemResultado_CntSrvCod[0];
                     A428NaoConformidade_AreaTrabalhoCod = H00IZ49_A428NaoConformidade_AreaTrabalhoCod[0];
                     n428NaoConformidade_AreaTrabalhoCod = H00IZ49_n428NaoConformidade_AreaTrabalhoCod[0];
                     A429NaoConformidade_Tipo = H00IZ49_A429NaoConformidade_Tipo[0];
                     n429NaoConformidade_Tipo = H00IZ49_n429NaoConformidade_Tipo[0];
                     A1481Contratada_UsaOSistema = H00IZ49_A1481Contratada_UsaOSistema[0];
                     n1481Contratada_UsaOSistema = H00IZ49_n1481Contratada_UsaOSistema[0];
                     A1603ContagemResultado_CntCod = H00IZ49_A1603ContagemResultado_CntCod[0];
                     n1603ContagemResultado_CntCod = H00IZ49_n1603ContagemResultado_CntCod[0];
                     A39Contratada_Codigo = H00IZ49_A39Contratada_Codigo[0];
                     n39Contratada_Codigo = H00IZ49_n39Contratada_Codigo[0];
                     A92Contrato_Ativo = H00IZ49_A92Contrato_Ativo[0];
                     n92Contrato_Ativo = H00IZ49_n92Contrato_Ativo[0];
                     A83Contrato_DataVigenciaTermino = H00IZ49_A83Contrato_DataVigenciaTermino[0];
                     n83Contrato_DataVigenciaTermino = H00IZ49_n83Contrato_DataVigenciaTermino[0];
                     GXt_boolean11 = A1149LogResponsavel_OwnerEhContratante;
                     new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A896LogResponsavel_Owner, out  GXt_boolean11) ;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A892LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A892LogResponsavel_DemandaCod), 6, 0)));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A896LogResponsavel_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A896LogResponsavel_Owner), 6, 0)));
                     A1149LogResponsavel_OwnerEhContratante = GXt_boolean11;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1149LogResponsavel_OwnerEhContratante", A1149LogResponsavel_OwnerEhContratante);
                     if ( ! A1149LogResponsavel_OwnerEhContratante )
                     {
                        GXt_boolean11 = A1148LogResponsavel_UsuarioEhContratante;
                        new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A891LogResponsavel_UsuarioCod, out  GXt_boolean11) ;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A892LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A892LogResponsavel_DemandaCod), 6, 0)));
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A891LogResponsavel_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A891LogResponsavel_UsuarioCod), 6, 0)));
                        A1148LogResponsavel_UsuarioEhContratante = GXt_boolean11;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1148LogResponsavel_UsuarioEhContratante", A1148LogResponsavel_UsuarioEhContratante);
                        if ( ! A1148LogResponsavel_UsuarioEhContratante )
                        {
                           /* Using cursor H00IZ50 */
                           pr_default.execute(39, new Object[] {AV66ContratadaOrigem_Codigo, n1481Contratada_UsaOSistema, A1481Contratada_UsaOSistema, A896LogResponsavel_Owner, n891LogResponsavel_UsuarioCod, A891LogResponsavel_UsuarioCod});
                           while ( (pr_default.getStatus(39) != 101) )
                           {
                              A69ContratadaUsuario_UsuarioCod = H00IZ50_A69ContratadaUsuario_UsuarioCod[0];
                              A66ContratadaUsuario_ContratadaCod = H00IZ50_A66ContratadaUsuario_ContratadaCod[0];
                              AV65Responsavel_Codigo = A69ContratadaUsuario_UsuarioCod;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65Responsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65Responsavel_Codigo), 6, 0)));
                              /* Exit For each command. Update data (if necessary), close cursors & exit. */
                              if (true) break;
                              pr_default.readNext(39);
                           }
                           pr_default.close(39);
                           if ( AV65Responsavel_Codigo > 0 )
                           {
                              /* Exit For each command. Update data (if necessary), close cursors & exit. */
                              if (true) break;
                           }
                        }
                     }
                     pr_default.readNext(38);
                  }
                  pr_default.close(38);
                  if ( (0==AV65Responsavel_Codigo) )
                  {
                     /* Using cursor H00IZ51 */
                     pr_default.execute(40, new Object[] {AV66ContratadaOrigem_Codigo});
                     while ( (pr_default.getStatus(40) != 101) )
                     {
                        A1481Contratada_UsaOSistema = H00IZ51_A1481Contratada_UsaOSistema[0];
                        n1481Contratada_UsaOSistema = H00IZ51_n1481Contratada_UsaOSistema[0];
                        A66ContratadaUsuario_ContratadaCod = H00IZ51_A66ContratadaUsuario_ContratadaCod[0];
                        A69ContratadaUsuario_UsuarioCod = H00IZ51_A69ContratadaUsuario_UsuarioCod[0];
                        A1481Contratada_UsaOSistema = H00IZ51_A1481Contratada_UsaOSistema[0];
                        n1481Contratada_UsaOSistema = H00IZ51_n1481Contratada_UsaOSistema[0];
                        AV65Responsavel_Codigo = A69ContratadaUsuario_UsuarioCod;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65Responsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65Responsavel_Codigo), 6, 0)));
                        /* Exit For each command. Update data (if necessary), close cursors & exit. */
                        if (true) break;
                        pr_default.readNext(40);
                     }
                     pr_default.close(40);
                  }
               }
               if ( (0==AV66ContratadaOrigem_Codigo) || (0==AV65Responsavel_Codigo) )
               {
                  AV233GXLvl1520 = 0;
                  /* Using cursor H00IZ52 */
                  pr_default.execute(41, new Object[] {AV9Codigo});
                  while ( (pr_default.getStatus(41) != 101) )
                  {
                     A1797LogResponsavel_Codigo = H00IZ52_A1797LogResponsavel_Codigo[0];
                     A896LogResponsavel_Owner = H00IZ52_A896LogResponsavel_Owner[0];
                     A892LogResponsavel_DemandaCod = H00IZ52_A892LogResponsavel_DemandaCod[0];
                     n892LogResponsavel_DemandaCod = H00IZ52_n892LogResponsavel_DemandaCod[0];
                     GXt_boolean11 = A1149LogResponsavel_OwnerEhContratante;
                     new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A896LogResponsavel_Owner, out  GXt_boolean11) ;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A892LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A892LogResponsavel_DemandaCod), 6, 0)));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A896LogResponsavel_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A896LogResponsavel_Owner), 6, 0)));
                     A1149LogResponsavel_OwnerEhContratante = GXt_boolean11;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1149LogResponsavel_OwnerEhContratante", A1149LogResponsavel_OwnerEhContratante);
                     if ( A1149LogResponsavel_OwnerEhContratante )
                     {
                        AV233GXLvl1520 = 1;
                        AV65Responsavel_Codigo = A896LogResponsavel_Owner;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65Responsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65Responsavel_Codigo), 6, 0)));
                        /* Exit For each command. Update data (if necessary), close cursors & exit. */
                        if (true) break;
                     }
                     pr_default.readNext(41);
                  }
                  pr_default.close(41);
                  if ( AV233GXLvl1520 == 0 )
                  {
                     /* Using cursor H00IZ53 */
                     pr_default.execute(42, new Object[] {AV165Contrato_Codigo});
                     while ( (pr_default.getStatus(42) != 101) )
                     {
                        A1078ContratoGestor_ContratoCod = H00IZ53_A1078ContratoGestor_ContratoCod[0];
                        A1079ContratoGestor_UsuarioCod = H00IZ53_A1079ContratoGestor_UsuarioCod[0];
                        A1446ContratoGestor_ContratadaAreaCod = H00IZ53_A1446ContratoGestor_ContratadaAreaCod[0];
                        n1446ContratoGestor_ContratadaAreaCod = H00IZ53_n1446ContratoGestor_ContratadaAreaCod[0];
                        A1446ContratoGestor_ContratadaAreaCod = H00IZ53_A1446ContratoGestor_ContratadaAreaCod[0];
                        n1446ContratoGestor_ContratadaAreaCod = H00IZ53_n1446ContratoGestor_ContratadaAreaCod[0];
                        GXt_boolean11 = A1135ContratoGestor_UsuarioEhContratante;
                        new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean11) ;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1446ContratoGestor_ContratadaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0)));
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
                        A1135ContratoGestor_UsuarioEhContratante = GXt_boolean11;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1135ContratoGestor_UsuarioEhContratante", A1135ContratoGestor_UsuarioEhContratante);
                        if ( A1135ContratoGestor_UsuarioEhContratante )
                        {
                           AV65Responsavel_Codigo = A1079ContratoGestor_UsuarioCod;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65Responsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65Responsavel_Codigo), 6, 0)));
                           /* Exit For each command. Update data (if necessary), close cursors & exit. */
                           if (true) break;
                        }
                        pr_default.readNext(42);
                     }
                     pr_default.close(42);
                  }
               }
               if ( (0==AV65Responsavel_Codigo) )
               {
                  GX_msglist.addItem("N�o existe usu�rio ou representante da "+AV110Origem_Sigla+" para retorno da OS!");
               }
               else
               {
                  AV113Ok = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV113Ok", AV113Ok);
               }
            }
            else if ( AV166UsaOSistema )
            {
               AV235GXLvl1541 = 0;
               /* Using cursor H00IZ54 */
               pr_default.execute(43, new Object[] {AV9Codigo});
               while ( (pr_default.getStatus(43) != 101) )
               {
                  A468ContagemResultado_NaoCnfDmnCod = H00IZ54_A468ContagemResultado_NaoCnfDmnCod[0];
                  n468ContagemResultado_NaoCnfDmnCod = H00IZ54_n468ContagemResultado_NaoCnfDmnCod[0];
                  A1553ContagemResultado_CntSrvCod = H00IZ54_A1553ContagemResultado_CntSrvCod[0];
                  n1553ContagemResultado_CntSrvCod = H00IZ54_n1553ContagemResultado_CntSrvCod[0];
                  A1603ContagemResultado_CntCod = H00IZ54_A1603ContagemResultado_CntCod[0];
                  n1603ContagemResultado_CntCod = H00IZ54_n1603ContagemResultado_CntCod[0];
                  A39Contratada_Codigo = H00IZ54_A39Contratada_Codigo[0];
                  n39Contratada_Codigo = H00IZ54_n39Contratada_Codigo[0];
                  A428NaoConformidade_AreaTrabalhoCod = H00IZ54_A428NaoConformidade_AreaTrabalhoCod[0];
                  n428NaoConformidade_AreaTrabalhoCod = H00IZ54_n428NaoConformidade_AreaTrabalhoCod[0];
                  A429NaoConformidade_Tipo = H00IZ54_A429NaoConformidade_Tipo[0];
                  n429NaoConformidade_Tipo = H00IZ54_n429NaoConformidade_Tipo[0];
                  A92Contrato_Ativo = H00IZ54_A92Contrato_Ativo[0];
                  n92Contrato_Ativo = H00IZ54_n92Contrato_Ativo[0];
                  A894LogResponsavel_Acao = H00IZ54_A894LogResponsavel_Acao[0];
                  A1797LogResponsavel_Codigo = H00IZ54_A1797LogResponsavel_Codigo[0];
                  A896LogResponsavel_Owner = H00IZ54_A896LogResponsavel_Owner[0];
                  A892LogResponsavel_DemandaCod = H00IZ54_A892LogResponsavel_DemandaCod[0];
                  n892LogResponsavel_DemandaCod = H00IZ54_n892LogResponsavel_DemandaCod[0];
                  A83Contrato_DataVigenciaTermino = H00IZ54_A83Contrato_DataVigenciaTermino[0];
                  n83Contrato_DataVigenciaTermino = H00IZ54_n83Contrato_DataVigenciaTermino[0];
                  A468ContagemResultado_NaoCnfDmnCod = H00IZ54_A468ContagemResultado_NaoCnfDmnCod[0];
                  n468ContagemResultado_NaoCnfDmnCod = H00IZ54_n468ContagemResultado_NaoCnfDmnCod[0];
                  A1553ContagemResultado_CntSrvCod = H00IZ54_A1553ContagemResultado_CntSrvCod[0];
                  n1553ContagemResultado_CntSrvCod = H00IZ54_n1553ContagemResultado_CntSrvCod[0];
                  A428NaoConformidade_AreaTrabalhoCod = H00IZ54_A428NaoConformidade_AreaTrabalhoCod[0];
                  n428NaoConformidade_AreaTrabalhoCod = H00IZ54_n428NaoConformidade_AreaTrabalhoCod[0];
                  A429NaoConformidade_Tipo = H00IZ54_A429NaoConformidade_Tipo[0];
                  n429NaoConformidade_Tipo = H00IZ54_n429NaoConformidade_Tipo[0];
                  A1603ContagemResultado_CntCod = H00IZ54_A1603ContagemResultado_CntCod[0];
                  n1603ContagemResultado_CntCod = H00IZ54_n1603ContagemResultado_CntCod[0];
                  A39Contratada_Codigo = H00IZ54_A39Contratada_Codigo[0];
                  n39Contratada_Codigo = H00IZ54_n39Contratada_Codigo[0];
                  A92Contrato_Ativo = H00IZ54_A92Contrato_Ativo[0];
                  n92Contrato_Ativo = H00IZ54_n92Contrato_Ativo[0];
                  A83Contrato_DataVigenciaTermino = H00IZ54_A83Contrato_DataVigenciaTermino[0];
                  n83Contrato_DataVigenciaTermino = H00IZ54_n83Contrato_DataVigenciaTermino[0];
                  GXt_boolean11 = A1149LogResponsavel_OwnerEhContratante;
                  new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A896LogResponsavel_Owner, out  GXt_boolean11) ;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A892LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A892LogResponsavel_DemandaCod), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A896LogResponsavel_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A896LogResponsavel_Owner), 6, 0)));
                  A1149LogResponsavel_OwnerEhContratante = GXt_boolean11;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1149LogResponsavel_OwnerEhContratante", A1149LogResponsavel_OwnerEhContratante);
                  if ( ! A1149LogResponsavel_OwnerEhContratante )
                  {
                     AV235GXLvl1541 = 1;
                     pr_default.dynParam(44, new Object[]{ new Object[]{
                                                          AV61UsuarioDaPrestadora ,
                                                          A66ContratadaUsuario_ContratadaCod ,
                                                          AV66ContratadaOrigem_Codigo ,
                                                          AV27Prestadora_Codigo ,
                                                          A896LogResponsavel_Owner ,
                                                          A69ContratadaUsuario_UsuarioCod },
                                                          new int[] {
                                                          TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT
                                                          }
                     });
                     /* Using cursor H00IZ55 */
                     pr_default.execute(44, new Object[] {A896LogResponsavel_Owner, AV66ContratadaOrigem_Codigo, AV27Prestadora_Codigo});
                     while ( (pr_default.getStatus(44) != 101) )
                     {
                        A69ContratadaUsuario_UsuarioCod = H00IZ55_A69ContratadaUsuario_UsuarioCod[0];
                        A66ContratadaUsuario_ContratadaCod = H00IZ55_A66ContratadaUsuario_ContratadaCod[0];
                        AV65Responsavel_Codigo = A896LogResponsavel_Owner;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65Responsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65Responsavel_Codigo), 6, 0)));
                        /* Exit For each command. Update data (if necessary), close cursors & exit. */
                        if (true) break;
                        pr_default.readNext(44);
                     }
                     pr_default.close(44);
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                  }
                  pr_default.readNext(43);
               }
               pr_default.close(43);
               if ( AV235GXLvl1541 == 0 )
               {
                  pr_default.dynParam(45, new Object[]{ new Object[]{
                                                       AV61UsuarioDaPrestadora ,
                                                       A74Contrato_Codigo ,
                                                       AV115ContratoOrigem_Codigo ,
                                                       A39Contratada_Codigo ,
                                                       AV27Prestadora_Codigo ,
                                                       A155Servico_Codigo ,
                                                       AV22Servico_Codigo },
                                                       new int[] {
                                                       TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT
                                                       }
                  });
                  /* Using cursor H00IZ58 */
                  pr_default.execute(45, new Object[] {AV115ContratoOrigem_Codigo, AV27Prestadora_Codigo, AV22Servico_Codigo});
                  while ( (pr_default.getStatus(45) != 101) )
                  {
                     A74Contrato_Codigo = H00IZ58_A74Contrato_Codigo[0];
                     n74Contrato_Codigo = H00IZ58_n74Contrato_Codigo[0];
                     A1013Contrato_PrepostoCod = H00IZ58_A1013Contrato_PrepostoCod[0];
                     n1013Contrato_PrepostoCod = H00IZ58_n1013Contrato_PrepostoCod[0];
                     A39Contratada_Codigo = H00IZ58_A39Contratada_Codigo[0];
                     n39Contratada_Codigo = H00IZ58_n39Contratada_Codigo[0];
                     A92Contrato_Ativo = H00IZ58_A92Contrato_Ativo[0];
                     n92Contrato_Ativo = H00IZ58_n92Contrato_Ativo[0];
                     A155Servico_Codigo = H00IZ58_A155Servico_Codigo[0];
                     A843Contrato_DataFimTA = H00IZ58_A843Contrato_DataFimTA[0];
                     n843Contrato_DataFimTA = H00IZ58_n843Contrato_DataFimTA[0];
                     A83Contrato_DataVigenciaTermino = H00IZ58_A83Contrato_DataVigenciaTermino[0];
                     n83Contrato_DataVigenciaTermino = H00IZ58_n83Contrato_DataVigenciaTermino[0];
                     A1013Contrato_PrepostoCod = H00IZ58_A1013Contrato_PrepostoCod[0];
                     n1013Contrato_PrepostoCod = H00IZ58_n1013Contrato_PrepostoCod[0];
                     A39Contratada_Codigo = H00IZ58_A39Contratada_Codigo[0];
                     n39Contratada_Codigo = H00IZ58_n39Contratada_Codigo[0];
                     A92Contrato_Ativo = H00IZ58_A92Contrato_Ativo[0];
                     n92Contrato_Ativo = H00IZ58_n92Contrato_Ativo[0];
                     A83Contrato_DataVigenciaTermino = H00IZ58_A83Contrato_DataVigenciaTermino[0];
                     n83Contrato_DataVigenciaTermino = H00IZ58_n83Contrato_DataVigenciaTermino[0];
                     A843Contrato_DataFimTA = H00IZ58_A843Contrato_DataFimTA[0];
                     n843Contrato_DataFimTA = H00IZ58_n843Contrato_DataFimTA[0];
                     A1869Contrato_DataTermino = ((A83Contrato_DataVigenciaTermino>A843Contrato_DataFimTA) ? A83Contrato_DataVigenciaTermino : A843Contrato_DataFimTA);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1869Contrato_DataTermino", context.localUtil.Format(A1869Contrato_DataTermino, "99/99/99"));
                     AV65Responsavel_Codigo = A1013Contrato_PrepostoCod;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65Responsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65Responsavel_Codigo), 6, 0)));
                     pr_default.dynParam(46, new Object[]{ new Object[]{
                                                          A1013Contrato_PrepostoCod ,
                                                          A1078ContratoGestor_ContratoCod ,
                                                          A74Contrato_Codigo },
                                                          new int[] {
                                                          TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                                          }
                     });
                     /* Using cursor H00IZ59 */
                     pr_default.execute(46, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
                     while ( (pr_default.getStatus(46) != 101) )
                     {
                        A1078ContratoGestor_ContratoCod = H00IZ59_A1078ContratoGestor_ContratoCod[0];
                        A1079ContratoGestor_UsuarioCod = H00IZ59_A1079ContratoGestor_UsuarioCod[0];
                        AV65Responsavel_Codigo = A1079ContratoGestor_UsuarioCod;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65Responsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65Responsavel_Codigo), 6, 0)));
                        /* Exit For each command. Update data (if necessary), close cursors & exit. */
                        if (true) break;
                        pr_default.readNext(46);
                     }
                     pr_default.close(46);
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                     pr_default.readNext(45);
                  }
                  pr_default.close(45);
               }
               if ( (0==AV65Responsavel_Codigo) )
               {
                  GX_msglist.addItem("N�o existe usu�rio ou representante da "+AV110Origem_Sigla+" para retorno da OS!");
               }
               else
               {
                  AV113Ok = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV113Ok", AV113Ok);
               }
            }
            else
            {
               AV113Ok = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV113Ok", AV113Ok);
            }
            if ( AV113Ok )
            {
               new prc_naoacata(context ).execute(  AV9Codigo,  AV59OsVinculada,  AV12UserId,  AV63NewPFB,  AV64NewPFL,  AV65Responsavel_Codigo,  AV6Observacao,  AV69NovaContagem,  AV72EmReuniao,  false) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59OsVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59OsVinculada), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12UserId), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63NewPFB", StringUtil.LTrim( StringUtil.Str( AV63NewPFB, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64NewPFL", StringUtil.LTrim( StringUtil.Str( AV64NewPFL, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65Responsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65Responsavel_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Observacao", AV6Observacao);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69NovaContagem", AV69NovaContagem);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72EmReuniao", AV72EmReuniao);
               new prc_newevidenciademanda(context ).execute( ref  AV9Codigo) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Codigo), 6, 0)));
               /* Execute user subroutine: 'FECHAR' */
               S272 ();
               if (returnInSub) return;
            }
         }
         else if ( AV5Usuario_Codigo == 800003 )
         {
            cmbavUsuario_codigo.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUsuario_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavUsuario_codigo.Enabled), 5, 0)));
            bttBtnenter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Visible), 5, 0)));
            AV10Destino_Nome = "Reuni�o solicitada";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Destino_Nome", AV10Destino_Nome);
            new prc_reuniaodivergencia(context ).execute(  AV9Codigo,  AV59OsVinculada,  AV53WWPContext.gxTpr_Userid,  AV6Observacao,  AV53WWPContext.gxTpr_Username, ref  AV68Resultado) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59OsVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59OsVinculada), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Observacao", AV6Observacao);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68Resultado", AV68Resultado);
            GX_msglist.addItem(AV68Resultado);
         }
         else if ( AV5Usuario_Codigo == 800004 )
         {
            if ( AV20Selecionadas.Count == 1 )
            {
               if ( AV53WWPContext.gxTpr_Userehcontratante )
               {
                  new prc_acata(context ).execute(  AV9Codigo,  AV59OsVinculada,  AV12UserId,  AV70PFBOF,  AV71PFLOF,  AV6Observacao) ;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Codigo), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59OsVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59OsVinculada), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12UserId), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70PFBOF", StringUtil.LTrim( StringUtil.Str( AV70PFBOF, 14, 5)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFBOF", GetSecureSignedToken( "", context.localUtil.Format( AV70PFBOF, "ZZ,ZZZ,ZZ9.999")));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71PFLOF", StringUtil.LTrim( StringUtil.Str( AV71PFLOF, 14, 5)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFLOF", GetSecureSignedToken( "", context.localUtil.Format( AV71PFLOF, "ZZ,ZZZ,ZZ9.999")));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Observacao", AV6Observacao);
               }
               else
               {
                  new prc_acata(context ).execute(  AV9Codigo,  AV59OsVinculada,  AV12UserId,  AV57PFB,  AV58PFL,  AV6Observacao) ;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Codigo), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59OsVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59OsVinculada), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12UserId), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57PFB", StringUtil.LTrim( StringUtil.Str( AV57PFB, 14, 5)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFB", GetSecureSignedToken( "", context.localUtil.Format( AV57PFB, "ZZ,ZZZ,ZZ9.999")));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58PFL", StringUtil.LTrim( StringUtil.Str( AV58PFL, 14, 5)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFL", GetSecureSignedToken( "", context.localUtil.Format( AV58PFL, "ZZ,ZZZ,ZZ9.999")));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Observacao", AV6Observacao);
               }
            }
            else
            {
               new prc_acatou(context ).execute(  AV12UserId,  AV6Observacao) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12UserId), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Observacao", AV6Observacao);
            }
            new prc_newevidenciademanda(context ).execute( ref  AV9Codigo) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Codigo), 6, 0)));
            AV10Destino_Nome = "Acatou";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Destino_Nome", AV10Destino_Nome);
            /* Execute user subroutine: 'FECHAR' */
            S272 ();
            if (returnInSub) return;
         }
         else if ( AV5Usuario_Codigo == 800005 )
         {
            AV69NovaContagem = (bool)(((AV20Selecionadas.Count==1))&&((AV70PFBOF!=AV63NewPFB)||(AV71PFLOF!=AV64NewPFL)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69NovaContagem", AV69NovaContagem);
            new prc_naoacatou(context ).execute(  AV63NewPFB,  AV64NewPFL,  AV69NovaContagem,  AV6Observacao) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63NewPFB", StringUtil.LTrim( StringUtil.Str( AV63NewPFB, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64NewPFL", StringUtil.LTrim( StringUtil.Str( AV64NewPFL, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69NovaContagem", AV69NovaContagem);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Observacao", AV6Observacao);
            new prc_newevidenciademanda(context ).execute( ref  AV9Codigo) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Codigo), 6, 0)));
            AV10Destino_Nome = "N�o Acatou";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Destino_Nome", AV10Destino_Nome);
            /* Execute user subroutine: 'FECHAR' */
            S272 ();
            if (returnInSub) return;
         }
         else
         {
            /* Execute user subroutine: 'DADOSDADEMANDA' */
            S222 ();
            if (returnInSub) return;
            AV45PrazoEntrega = AV18PrazoExecucao;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45PrazoEntrega", context.localUtil.TToC( AV45PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
            AV17Acao = "A";
            /* Execute user subroutine: 'ADDHORASENTREGA' */
            S262 ();
            if (returnInSub) return;
            new prc_asignardemanda(context ).execute( ref  AV9Codigo,  AV5Usuario_Codigo,  AV17Acao,  AV16StatusDemanda) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Usuario_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16StatusDemanda", AV16StatusDemanda);
            new prc_inslogresponsavel(context ).execute( ref  AV9Codigo,  AV5Usuario_Codigo,  AV17Acao,  "D",  AV12UserId,  0,  AV16StatusDemanda,  AV43NovoStatus,  AV6Observacao,  AV45PrazoEntrega,  true) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Usuario_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12UserId), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16StatusDemanda", AV16StatusDemanda);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43NovoStatus", AV43NovoStatus);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Observacao", AV6Observacao);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45PrazoEntrega", context.localUtil.TToC( AV45PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
            AV10Destino_Nome = cmbavUsuario_codigo.Description;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Destino_Nome", AV10Destino_Nome);
            /* Execute user subroutine: 'FECHAR' */
            S272 ();
            if (returnInSub) return;
         }
      }

      protected void S172( )
      {
         /* 'CARREGAGRUPOSDESERVICOS' Routine */
         AV172Servicos.Clear();
         AV171SDT_Servicos.Clear();
         /* Using cursor H00IZ60 */
         pr_default.execute(47, new Object[] {AV53WWPContext.gxTpr_Areatrabalho_codigo, AV53WWPContext.gxTpr_Userid});
         while ( (pr_default.getStatus(47) != 101) )
         {
            A1136ContratoGestor_ContratadaCod = H00IZ60_A1136ContratoGestor_ContratadaCod[0];
            n1136ContratoGestor_ContratadaCod = H00IZ60_n1136ContratoGestor_ContratadaCod[0];
            A52Contratada_AreaTrabalhoCod = H00IZ60_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00IZ60_n52Contratada_AreaTrabalhoCod[0];
            A1595Contratada_AreaTrbSrvPdr = H00IZ60_A1595Contratada_AreaTrbSrvPdr[0];
            n1595Contratada_AreaTrbSrvPdr = H00IZ60_n1595Contratada_AreaTrbSrvPdr[0];
            A1446ContratoGestor_ContratadaAreaCod = H00IZ60_A1446ContratoGestor_ContratadaAreaCod[0];
            n1446ContratoGestor_ContratadaAreaCod = H00IZ60_n1446ContratoGestor_ContratadaAreaCod[0];
            A1079ContratoGestor_UsuarioCod = H00IZ60_A1079ContratoGestor_UsuarioCod[0];
            A632Servico_Ativo = H00IZ60_A632Servico_Ativo[0];
            n632Servico_Ativo = H00IZ60_n632Servico_Ativo[0];
            A157ServicoGrupo_Codigo = H00IZ60_A157ServicoGrupo_Codigo[0];
            n157ServicoGrupo_Codigo = H00IZ60_n157ServicoGrupo_Codigo[0];
            A1078ContratoGestor_ContratoCod = H00IZ60_A1078ContratoGestor_ContratoCod[0];
            A158ServicoGrupo_Descricao = H00IZ60_A158ServicoGrupo_Descricao[0];
            A92Contrato_Ativo = H00IZ60_A92Contrato_Ativo[0];
            n92Contrato_Ativo = H00IZ60_n92Contrato_Ativo[0];
            A43Contratada_Ativo = H00IZ60_A43Contratada_Ativo[0];
            n43Contratada_Ativo = H00IZ60_n43Contratada_Ativo[0];
            A83Contrato_DataVigenciaTermino = H00IZ60_A83Contrato_DataVigenciaTermino[0];
            n83Contrato_DataVigenciaTermino = H00IZ60_n83Contrato_DataVigenciaTermino[0];
            A1136ContratoGestor_ContratadaCod = H00IZ60_A1136ContratoGestor_ContratadaCod[0];
            n1136ContratoGestor_ContratadaCod = H00IZ60_n1136ContratoGestor_ContratadaCod[0];
            A1446ContratoGestor_ContratadaAreaCod = H00IZ60_A1446ContratoGestor_ContratadaAreaCod[0];
            n1446ContratoGestor_ContratadaAreaCod = H00IZ60_n1446ContratoGestor_ContratadaAreaCod[0];
            A92Contrato_Ativo = H00IZ60_A92Contrato_Ativo[0];
            n92Contrato_Ativo = H00IZ60_n92Contrato_Ativo[0];
            A83Contrato_DataVigenciaTermino = H00IZ60_A83Contrato_DataVigenciaTermino[0];
            n83Contrato_DataVigenciaTermino = H00IZ60_n83Contrato_DataVigenciaTermino[0];
            A52Contratada_AreaTrabalhoCod = H00IZ60_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00IZ60_n52Contratada_AreaTrabalhoCod[0];
            A43Contratada_Ativo = H00IZ60_A43Contratada_Ativo[0];
            n43Contratada_Ativo = H00IZ60_n43Contratada_Ativo[0];
            A1595Contratada_AreaTrbSrvPdr = H00IZ60_A1595Contratada_AreaTrbSrvPdr[0];
            n1595Contratada_AreaTrbSrvPdr = H00IZ60_n1595Contratada_AreaTrbSrvPdr[0];
            A632Servico_Ativo = H00IZ60_A632Servico_Ativo[0];
            n632Servico_Ativo = H00IZ60_n632Servico_Ativo[0];
            A157ServicoGrupo_Codigo = H00IZ60_A157ServicoGrupo_Codigo[0];
            n157ServicoGrupo_Codigo = H00IZ60_n157ServicoGrupo_Codigo[0];
            A158ServicoGrupo_Descricao = H00IZ60_A158ServicoGrupo_Descricao[0];
            pr_default.dynParam(48, new Object[]{ new Object[]{
                                                 A157ServicoGrupo_Codigo ,
                                                 AV172Servicos ,
                                                 A638ContratoServicos_Ativo ,
                                                 A632Servico_Ativo ,
                                                 A1078ContratoGestor_ContratoCod ,
                                                 A74Contrato_Codigo },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                                 }
            });
            /* Using cursor H00IZ61 */
            pr_default.execute(48, new Object[] {A1078ContratoGestor_ContratoCod, n632Servico_Ativo, A632Servico_Ativo});
            while ( (pr_default.getStatus(48) != 101) )
            {
               A74Contrato_Codigo = H00IZ61_A74Contrato_Codigo[0];
               n74Contrato_Codigo = H00IZ61_n74Contrato_Codigo[0];
               A638ContratoServicos_Ativo = H00IZ61_A638ContratoServicos_Ativo[0];
               if ( ! (AV172Servicos.IndexOf(A157ServicoGrupo_Codigo)>0) )
               {
                  AV172Servicos.Add(A157ServicoGrupo_Codigo, 0);
                  AV170SDT_Servico = new SdtSDT_Codigos(context);
                  AV170SDT_Servico.gxTpr_Codigo = A157ServicoGrupo_Codigo;
                  AV170SDT_Servico.gxTpr_Descricao = A158ServicoGrupo_Descricao;
                  AV171SDT_Servicos.Add(AV170SDT_Servico, 0);
               }
               pr_default.readNext(48);
            }
            pr_default.close(48);
            pr_default.readNext(47);
         }
         pr_default.close(47);
         /* Using cursor H00IZ62 */
         pr_default.execute(49, new Object[] {AV53WWPContext.gxTpr_Areatrabalho_codigo, AV53WWPContext.gxTpr_Userid});
         while ( (pr_default.getStatus(49) != 101) )
         {
            A52Contratada_AreaTrabalhoCod = H00IZ62_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00IZ62_n52Contratada_AreaTrabalhoCod[0];
            A1595Contratada_AreaTrbSrvPdr = H00IZ62_A1595Contratada_AreaTrbSrvPdr[0];
            n1595Contratada_AreaTrbSrvPdr = H00IZ62_n1595Contratada_AreaTrbSrvPdr[0];
            A75Contrato_AreaTrabalhoCod = H00IZ62_A75Contrato_AreaTrabalhoCod[0];
            n75Contrato_AreaTrabalhoCod = H00IZ62_n75Contrato_AreaTrabalhoCod[0];
            A1825ContratoAuxiliar_UsuarioCod = H00IZ62_A1825ContratoAuxiliar_UsuarioCod[0];
            A632Servico_Ativo = H00IZ62_A632Servico_Ativo[0];
            n632Servico_Ativo = H00IZ62_n632Servico_Ativo[0];
            A157ServicoGrupo_Codigo = H00IZ62_A157ServicoGrupo_Codigo[0];
            n157ServicoGrupo_Codigo = H00IZ62_n157ServicoGrupo_Codigo[0];
            A1824ContratoAuxiliar_ContratoCod = H00IZ62_A1824ContratoAuxiliar_ContratoCod[0];
            A158ServicoGrupo_Descricao = H00IZ62_A158ServicoGrupo_Descricao[0];
            A39Contratada_Codigo = H00IZ62_A39Contratada_Codigo[0];
            n39Contratada_Codigo = H00IZ62_n39Contratada_Codigo[0];
            A92Contrato_Ativo = H00IZ62_A92Contrato_Ativo[0];
            n92Contrato_Ativo = H00IZ62_n92Contrato_Ativo[0];
            A43Contratada_Ativo = H00IZ62_A43Contratada_Ativo[0];
            n43Contratada_Ativo = H00IZ62_n43Contratada_Ativo[0];
            A83Contrato_DataVigenciaTermino = H00IZ62_A83Contrato_DataVigenciaTermino[0];
            n83Contrato_DataVigenciaTermino = H00IZ62_n83Contrato_DataVigenciaTermino[0];
            A75Contrato_AreaTrabalhoCod = H00IZ62_A75Contrato_AreaTrabalhoCod[0];
            n75Contrato_AreaTrabalhoCod = H00IZ62_n75Contrato_AreaTrabalhoCod[0];
            A39Contratada_Codigo = H00IZ62_A39Contratada_Codigo[0];
            n39Contratada_Codigo = H00IZ62_n39Contratada_Codigo[0];
            A92Contrato_Ativo = H00IZ62_A92Contrato_Ativo[0];
            n92Contrato_Ativo = H00IZ62_n92Contrato_Ativo[0];
            A83Contrato_DataVigenciaTermino = H00IZ62_A83Contrato_DataVigenciaTermino[0];
            n83Contrato_DataVigenciaTermino = H00IZ62_n83Contrato_DataVigenciaTermino[0];
            A52Contratada_AreaTrabalhoCod = H00IZ62_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00IZ62_n52Contratada_AreaTrabalhoCod[0];
            A43Contratada_Ativo = H00IZ62_A43Contratada_Ativo[0];
            n43Contratada_Ativo = H00IZ62_n43Contratada_Ativo[0];
            A1595Contratada_AreaTrbSrvPdr = H00IZ62_A1595Contratada_AreaTrbSrvPdr[0];
            n1595Contratada_AreaTrbSrvPdr = H00IZ62_n1595Contratada_AreaTrbSrvPdr[0];
            A632Servico_Ativo = H00IZ62_A632Servico_Ativo[0];
            n632Servico_Ativo = H00IZ62_n632Servico_Ativo[0];
            A157ServicoGrupo_Codigo = H00IZ62_A157ServicoGrupo_Codigo[0];
            n157ServicoGrupo_Codigo = H00IZ62_n157ServicoGrupo_Codigo[0];
            A158ServicoGrupo_Descricao = H00IZ62_A158ServicoGrupo_Descricao[0];
            pr_default.dynParam(50, new Object[]{ new Object[]{
                                                 A157ServicoGrupo_Codigo ,
                                                 AV172Servicos ,
                                                 A638ContratoServicos_Ativo ,
                                                 A632Servico_Ativo ,
                                                 A1824ContratoAuxiliar_ContratoCod ,
                                                 A74Contrato_Codigo },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                                 }
            });
            /* Using cursor H00IZ63 */
            pr_default.execute(50, new Object[] {A1824ContratoAuxiliar_ContratoCod, n632Servico_Ativo, A632Servico_Ativo});
            while ( (pr_default.getStatus(50) != 101) )
            {
               A74Contrato_Codigo = H00IZ63_A74Contrato_Codigo[0];
               n74Contrato_Codigo = H00IZ63_n74Contrato_Codigo[0];
               A638ContratoServicos_Ativo = H00IZ63_A638ContratoServicos_Ativo[0];
               A155Servico_Codigo = H00IZ63_A155Servico_Codigo[0];
               if ( ! (AV172Servicos.IndexOf(A157ServicoGrupo_Codigo)>0) )
               {
                  AV172Servicos.Add(A155Servico_Codigo, 0);
                  AV170SDT_Servico = new SdtSDT_Codigos(context);
                  AV170SDT_Servico.gxTpr_Codigo = A157ServicoGrupo_Codigo;
                  AV170SDT_Servico.gxTpr_Descricao = A158ServicoGrupo_Descricao;
                  AV171SDT_Servicos.Add(AV170SDT_Servico, 0);
               }
               pr_default.readNext(50);
            }
            pr_default.close(50);
            pr_default.readNext(49);
         }
         pr_default.close(49);
         AV171SDT_Servicos.Sort("Descricao");
         cmbavServicogrupo_codigo.removeAllItems();
         cmbavServicogrupo_codigo.addItem("0", "Todos", 0);
         AV243GXV3 = 1;
         while ( AV243GXV3 <= AV171SDT_Servicos.Count )
         {
            AV170SDT_Servico = ((SdtSDT_Codigos)AV171SDT_Servicos.Item(AV243GXV3));
            cmbavServicogrupo_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV170SDT_Servico.gxTpr_Codigo), 6, 0)), AV170SDT_Servico.gxTpr_Descricao, 0);
            AV177ServicoGrupo_Codigo = AV170SDT_Servico.gxTpr_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV177ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV177ServicoGrupo_Codigo), 6, 0)));
            AV243GXV3 = (int)(AV243GXV3+1);
         }
         if ( AV171SDT_Servicos.Count > 2 )
         {
            AV177ServicoGrupo_Codigo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV177ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV177ServicoGrupo_Codigo), 6, 0)));
         }
         /* Execute user subroutine: 'CARREGASERVICOS' */
         S282 ();
         if (returnInSub) return;
      }

      protected void S282( )
      {
         /* 'CARREGASERVICOS' Routine */
         AV172Servicos.Clear();
         AV171SDT_Servicos.Clear();
         /* Using cursor H00IZ64 */
         pr_default.execute(51, new Object[] {AV53WWPContext.gxTpr_Areatrabalho_codigo, AV53WWPContext.gxTpr_Userid});
         while ( (pr_default.getStatus(51) != 101) )
         {
            A1136ContratoGestor_ContratadaCod = H00IZ64_A1136ContratoGestor_ContratadaCod[0];
            n1136ContratoGestor_ContratadaCod = H00IZ64_n1136ContratoGestor_ContratadaCod[0];
            A52Contratada_AreaTrabalhoCod = H00IZ64_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00IZ64_n52Contratada_AreaTrabalhoCod[0];
            A1595Contratada_AreaTrbSrvPdr = H00IZ64_A1595Contratada_AreaTrbSrvPdr[0];
            n1595Contratada_AreaTrbSrvPdr = H00IZ64_n1595Contratada_AreaTrbSrvPdr[0];
            A1446ContratoGestor_ContratadaAreaCod = H00IZ64_A1446ContratoGestor_ContratadaAreaCod[0];
            n1446ContratoGestor_ContratadaAreaCod = H00IZ64_n1446ContratoGestor_ContratadaAreaCod[0];
            A1079ContratoGestor_UsuarioCod = H00IZ64_A1079ContratoGestor_UsuarioCod[0];
            A632Servico_Ativo = H00IZ64_A632Servico_Ativo[0];
            n632Servico_Ativo = H00IZ64_n632Servico_Ativo[0];
            A157ServicoGrupo_Codigo = H00IZ64_A157ServicoGrupo_Codigo[0];
            n157ServicoGrupo_Codigo = H00IZ64_n157ServicoGrupo_Codigo[0];
            A1078ContratoGestor_ContratoCod = H00IZ64_A1078ContratoGestor_ContratoCod[0];
            A92Contrato_Ativo = H00IZ64_A92Contrato_Ativo[0];
            n92Contrato_Ativo = H00IZ64_n92Contrato_Ativo[0];
            A43Contratada_Ativo = H00IZ64_A43Contratada_Ativo[0];
            n43Contratada_Ativo = H00IZ64_n43Contratada_Ativo[0];
            A83Contrato_DataVigenciaTermino = H00IZ64_A83Contrato_DataVigenciaTermino[0];
            n83Contrato_DataVigenciaTermino = H00IZ64_n83Contrato_DataVigenciaTermino[0];
            A1136ContratoGestor_ContratadaCod = H00IZ64_A1136ContratoGestor_ContratadaCod[0];
            n1136ContratoGestor_ContratadaCod = H00IZ64_n1136ContratoGestor_ContratadaCod[0];
            A1446ContratoGestor_ContratadaAreaCod = H00IZ64_A1446ContratoGestor_ContratadaAreaCod[0];
            n1446ContratoGestor_ContratadaAreaCod = H00IZ64_n1446ContratoGestor_ContratadaAreaCod[0];
            A92Contrato_Ativo = H00IZ64_A92Contrato_Ativo[0];
            n92Contrato_Ativo = H00IZ64_n92Contrato_Ativo[0];
            A83Contrato_DataVigenciaTermino = H00IZ64_A83Contrato_DataVigenciaTermino[0];
            n83Contrato_DataVigenciaTermino = H00IZ64_n83Contrato_DataVigenciaTermino[0];
            A52Contratada_AreaTrabalhoCod = H00IZ64_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00IZ64_n52Contratada_AreaTrabalhoCod[0];
            A43Contratada_Ativo = H00IZ64_A43Contratada_Ativo[0];
            n43Contratada_Ativo = H00IZ64_n43Contratada_Ativo[0];
            A1595Contratada_AreaTrbSrvPdr = H00IZ64_A1595Contratada_AreaTrbSrvPdr[0];
            n1595Contratada_AreaTrbSrvPdr = H00IZ64_n1595Contratada_AreaTrbSrvPdr[0];
            A632Servico_Ativo = H00IZ64_A632Servico_Ativo[0];
            n632Servico_Ativo = H00IZ64_n632Servico_Ativo[0];
            A157ServicoGrupo_Codigo = H00IZ64_A157ServicoGrupo_Codigo[0];
            n157ServicoGrupo_Codigo = H00IZ64_n157ServicoGrupo_Codigo[0];
            pr_default.dynParam(52, new Object[]{ new Object[]{
                                                 A155Servico_Codigo ,
                                                 AV172Servicos ,
                                                 AV177ServicoGrupo_Codigo ,
                                                 A157ServicoGrupo_Codigo ,
                                                 A638ContratoServicos_Ativo ,
                                                 A632Servico_Ativo ,
                                                 A1078ContratoGestor_ContratoCod ,
                                                 A74Contrato_Codigo },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                                 }
            });
            /* Using cursor H00IZ65 */
            pr_default.execute(52, new Object[] {A1078ContratoGestor_ContratoCod, n632Servico_Ativo, A632Servico_Ativo});
            while ( (pr_default.getStatus(52) != 101) )
            {
               A74Contrato_Codigo = H00IZ65_A74Contrato_Codigo[0];
               n74Contrato_Codigo = H00IZ65_n74Contrato_Codigo[0];
               A638ContratoServicos_Ativo = H00IZ65_A638ContratoServicos_Ativo[0];
               A155Servico_Codigo = H00IZ65_A155Servico_Codigo[0];
               A1858ContratoServicos_Alias = H00IZ65_A1858ContratoServicos_Alias[0];
               n1858ContratoServicos_Alias = H00IZ65_n1858ContratoServicos_Alias[0];
               AV172Servicos.Add(A155Servico_Codigo, 0);
               AV170SDT_Servico = new SdtSDT_Codigos(context);
               AV170SDT_Servico.gxTpr_Codigo = A155Servico_Codigo;
               AV170SDT_Servico.gxTpr_Descricao = A1858ContratoServicos_Alias;
               AV171SDT_Servicos.Add(AV170SDT_Servico, 0);
               pr_default.readNext(52);
            }
            pr_default.close(52);
            pr_default.readNext(51);
         }
         pr_default.close(51);
         /* Using cursor H00IZ66 */
         pr_default.execute(53, new Object[] {AV53WWPContext.gxTpr_Areatrabalho_codigo, AV53WWPContext.gxTpr_Userid});
         while ( (pr_default.getStatus(53) != 101) )
         {
            A52Contratada_AreaTrabalhoCod = H00IZ66_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00IZ66_n52Contratada_AreaTrabalhoCod[0];
            A1595Contratada_AreaTrbSrvPdr = H00IZ66_A1595Contratada_AreaTrbSrvPdr[0];
            n1595Contratada_AreaTrbSrvPdr = H00IZ66_n1595Contratada_AreaTrbSrvPdr[0];
            A75Contrato_AreaTrabalhoCod = H00IZ66_A75Contrato_AreaTrabalhoCod[0];
            n75Contrato_AreaTrabalhoCod = H00IZ66_n75Contrato_AreaTrabalhoCod[0];
            A1825ContratoAuxiliar_UsuarioCod = H00IZ66_A1825ContratoAuxiliar_UsuarioCod[0];
            A632Servico_Ativo = H00IZ66_A632Servico_Ativo[0];
            n632Servico_Ativo = H00IZ66_n632Servico_Ativo[0];
            A157ServicoGrupo_Codigo = H00IZ66_A157ServicoGrupo_Codigo[0];
            n157ServicoGrupo_Codigo = H00IZ66_n157ServicoGrupo_Codigo[0];
            A1824ContratoAuxiliar_ContratoCod = H00IZ66_A1824ContratoAuxiliar_ContratoCod[0];
            A39Contratada_Codigo = H00IZ66_A39Contratada_Codigo[0];
            n39Contratada_Codigo = H00IZ66_n39Contratada_Codigo[0];
            A92Contrato_Ativo = H00IZ66_A92Contrato_Ativo[0];
            n92Contrato_Ativo = H00IZ66_n92Contrato_Ativo[0];
            A43Contratada_Ativo = H00IZ66_A43Contratada_Ativo[0];
            n43Contratada_Ativo = H00IZ66_n43Contratada_Ativo[0];
            A83Contrato_DataVigenciaTermino = H00IZ66_A83Contrato_DataVigenciaTermino[0];
            n83Contrato_DataVigenciaTermino = H00IZ66_n83Contrato_DataVigenciaTermino[0];
            A75Contrato_AreaTrabalhoCod = H00IZ66_A75Contrato_AreaTrabalhoCod[0];
            n75Contrato_AreaTrabalhoCod = H00IZ66_n75Contrato_AreaTrabalhoCod[0];
            A39Contratada_Codigo = H00IZ66_A39Contratada_Codigo[0];
            n39Contratada_Codigo = H00IZ66_n39Contratada_Codigo[0];
            A92Contrato_Ativo = H00IZ66_A92Contrato_Ativo[0];
            n92Contrato_Ativo = H00IZ66_n92Contrato_Ativo[0];
            A83Contrato_DataVigenciaTermino = H00IZ66_A83Contrato_DataVigenciaTermino[0];
            n83Contrato_DataVigenciaTermino = H00IZ66_n83Contrato_DataVigenciaTermino[0];
            A52Contratada_AreaTrabalhoCod = H00IZ66_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00IZ66_n52Contratada_AreaTrabalhoCod[0];
            A43Contratada_Ativo = H00IZ66_A43Contratada_Ativo[0];
            n43Contratada_Ativo = H00IZ66_n43Contratada_Ativo[0];
            A1595Contratada_AreaTrbSrvPdr = H00IZ66_A1595Contratada_AreaTrbSrvPdr[0];
            n1595Contratada_AreaTrbSrvPdr = H00IZ66_n1595Contratada_AreaTrbSrvPdr[0];
            A632Servico_Ativo = H00IZ66_A632Servico_Ativo[0];
            n632Servico_Ativo = H00IZ66_n632Servico_Ativo[0];
            A157ServicoGrupo_Codigo = H00IZ66_A157ServicoGrupo_Codigo[0];
            n157ServicoGrupo_Codigo = H00IZ66_n157ServicoGrupo_Codigo[0];
            pr_default.dynParam(54, new Object[]{ new Object[]{
                                                 A155Servico_Codigo ,
                                                 AV172Servicos ,
                                                 AV177ServicoGrupo_Codigo ,
                                                 A157ServicoGrupo_Codigo ,
                                                 A638ContratoServicos_Ativo ,
                                                 A632Servico_Ativo ,
                                                 A1824ContratoAuxiliar_ContratoCod ,
                                                 A74Contrato_Codigo },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                                 }
            });
            /* Using cursor H00IZ67 */
            pr_default.execute(54, new Object[] {A1824ContratoAuxiliar_ContratoCod, n632Servico_Ativo, A632Servico_Ativo});
            while ( (pr_default.getStatus(54) != 101) )
            {
               A74Contrato_Codigo = H00IZ67_A74Contrato_Codigo[0];
               n74Contrato_Codigo = H00IZ67_n74Contrato_Codigo[0];
               A638ContratoServicos_Ativo = H00IZ67_A638ContratoServicos_Ativo[0];
               A155Servico_Codigo = H00IZ67_A155Servico_Codigo[0];
               A1858ContratoServicos_Alias = H00IZ67_A1858ContratoServicos_Alias[0];
               n1858ContratoServicos_Alias = H00IZ67_n1858ContratoServicos_Alias[0];
               AV172Servicos.Add(A155Servico_Codigo, 0);
               AV170SDT_Servico = new SdtSDT_Codigos(context);
               AV170SDT_Servico.gxTpr_Codigo = A155Servico_Codigo;
               AV170SDT_Servico.gxTpr_Descricao = A1858ContratoServicos_Alias;
               AV171SDT_Servicos.Add(AV170SDT_Servico, 0);
               pr_default.readNext(54);
            }
            pr_default.close(54);
            pr_default.readNext(53);
         }
         pr_default.close(53);
         AV171SDT_Servicos.Sort("Descricao");
         cmbavNewcntsrvcod.removeAllItems();
         cmbavNewcntsrvcod.addItem("0", "(Nenhum)", 0);
         AV182NewCntSrvCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV182NewCntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV182NewCntSrvCod), 6, 0)));
         AV248GXV4 = 1;
         while ( AV248GXV4 <= AV171SDT_Servicos.Count )
         {
            AV170SDT_Servico = ((SdtSDT_Codigos)AV171SDT_Servicos.Item(AV248GXV4));
            cmbavNewcntsrvcod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV170SDT_Servico.gxTpr_Codigo), 6, 0)), AV170SDT_Servico.gxTpr_Descricao, 0);
            AV182NewCntSrvCod = AV170SDT_Servico.gxTpr_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV182NewCntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV182NewCntSrvCod), 6, 0)));
            AV248GXV4 = (int)(AV248GXV4+1);
         }
         if ( AV171SDT_Servicos.Count > 2 )
         {
            AV182NewCntSrvCod = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV182NewCntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV182NewCntSrvCod), 6, 0)));
         }
      }

      protected void S292( )
      {
         /* 'CARREGAPRESTADORAS' Routine */
         cmbavCriarospara_codigo.removeAllItems();
         cmbavCriarospara_codigo.addItem("0", "(Nenhum)", 0);
         /* Using cursor H00IZ70 */
         pr_default.execute(55, new Object[] {AV53WWPContext.gxTpr_Areatrabalho_codigo, AV182NewCntSrvCod});
         while ( (pr_default.getStatus(55) != 101) )
         {
            A74Contrato_Codigo = H00IZ70_A74Contrato_Codigo[0];
            n74Contrato_Codigo = H00IZ70_n74Contrato_Codigo[0];
            A40Contratada_PessoaCod = H00IZ70_A40Contratada_PessoaCod[0];
            A75Contrato_AreaTrabalhoCod = H00IZ70_A75Contrato_AreaTrabalhoCod[0];
            n75Contrato_AreaTrabalhoCod = H00IZ70_n75Contrato_AreaTrabalhoCod[0];
            A155Servico_Codigo = H00IZ70_A155Servico_Codigo[0];
            A43Contratada_Ativo = H00IZ70_A43Contratada_Ativo[0];
            n43Contratada_Ativo = H00IZ70_n43Contratada_Ativo[0];
            A92Contrato_Ativo = H00IZ70_A92Contrato_Ativo[0];
            n92Contrato_Ativo = H00IZ70_n92Contrato_Ativo[0];
            A638ContratoServicos_Ativo = H00IZ70_A638ContratoServicos_Ativo[0];
            A83Contrato_DataVigenciaTermino = H00IZ70_A83Contrato_DataVigenciaTermino[0];
            n83Contrato_DataVigenciaTermino = H00IZ70_n83Contrato_DataVigenciaTermino[0];
            A39Contratada_Codigo = H00IZ70_A39Contratada_Codigo[0];
            n39Contratada_Codigo = H00IZ70_n39Contratada_Codigo[0];
            A41Contratada_PessoaNom = H00IZ70_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = H00IZ70_n41Contratada_PessoaNom[0];
            A843Contrato_DataFimTA = H00IZ70_A843Contrato_DataFimTA[0];
            n843Contrato_DataFimTA = H00IZ70_n843Contrato_DataFimTA[0];
            A75Contrato_AreaTrabalhoCod = H00IZ70_A75Contrato_AreaTrabalhoCod[0];
            n75Contrato_AreaTrabalhoCod = H00IZ70_n75Contrato_AreaTrabalhoCod[0];
            A92Contrato_Ativo = H00IZ70_A92Contrato_Ativo[0];
            n92Contrato_Ativo = H00IZ70_n92Contrato_Ativo[0];
            A83Contrato_DataVigenciaTermino = H00IZ70_A83Contrato_DataVigenciaTermino[0];
            n83Contrato_DataVigenciaTermino = H00IZ70_n83Contrato_DataVigenciaTermino[0];
            A39Contratada_Codigo = H00IZ70_A39Contratada_Codigo[0];
            n39Contratada_Codigo = H00IZ70_n39Contratada_Codigo[0];
            A40Contratada_PessoaCod = H00IZ70_A40Contratada_PessoaCod[0];
            A43Contratada_Ativo = H00IZ70_A43Contratada_Ativo[0];
            n43Contratada_Ativo = H00IZ70_n43Contratada_Ativo[0];
            A41Contratada_PessoaNom = H00IZ70_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = H00IZ70_n41Contratada_PessoaNom[0];
            A843Contrato_DataFimTA = H00IZ70_A843Contrato_DataFimTA[0];
            n843Contrato_DataFimTA = H00IZ70_n843Contrato_DataFimTA[0];
            if ( ( A843Contrato_DataFimTA >= DateTimeUtil.ServerDate( context, "DEFAULT") ) || ( A83Contrato_DataVigenciaTermino >= DateTimeUtil.ServerDate( context, "DEFAULT") ) )
            {
               cmbavCriarospara_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)), A41Contratada_PessoaNom, 0);
               AV168CriarOSPara_Codigo = A39Contratada_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV168CriarOSPara_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV168CriarOSPara_Codigo), 6, 0)));
            }
            pr_default.readNext(55);
         }
         pr_default.close(55);
         if ( cmbavCriarospara_codigo.ItemCount > 2 )
         {
            AV168CriarOSPara_Codigo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV168CriarOSPara_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV168CriarOSPara_Codigo), 6, 0)));
         }
         else
         {
            /* Execute user subroutine: 'CARREGACONTRATOS' */
            S312 ();
            if (returnInSub) return;
         }
      }

      protected void S332( )
      {
         /* 'DADOSOWNER' Routine */
         /* Using cursor H00IZ71 */
         pr_default.execute(56, new Object[] {AV128Owner_Codigo});
         while ( (pr_default.getStatus(56) != 101) )
         {
            A57Usuario_PessoaCod = H00IZ71_A57Usuario_PessoaCod[0];
            A1Usuario_Codigo = H00IZ71_A1Usuario_Codigo[0];
            A58Usuario_PessoaNom = H00IZ71_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = H00IZ71_n58Usuario_PessoaNom[0];
            A58Usuario_PessoaNom = H00IZ71_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = H00IZ71_n58Usuario_PessoaNom[0];
            AV129Owner_Nome = A58Usuario_PessoaNom;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV129Owner_Nome", AV129Owner_Nome);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(56);
      }

      protected void S322( )
      {
         /* 'TEMARTEFATOS' Routine */
         AV145Sdt_Artefatos.Clear();
         /* Using cursor H00IZ74 */
         pr_default.execute(57, new Object[] {AV174Contrato, AV182NewCntSrvCod});
         while ( (pr_default.getStatus(57) != 101) )
         {
            A160ContratoServicos_Codigo = H00IZ74_A160ContratoServicos_Codigo[0];
            A39Contratada_Codigo = H00IZ74_A39Contratada_Codigo[0];
            n39Contratada_Codigo = H00IZ74_n39Contratada_Codigo[0];
            A92Contrato_Ativo = H00IZ74_A92Contrato_Ativo[0];
            n92Contrato_Ativo = H00IZ74_n92Contrato_Ativo[0];
            A155Servico_Codigo = H00IZ74_A155Servico_Codigo[0];
            A74Contrato_Codigo = H00IZ74_A74Contrato_Codigo[0];
            n74Contrato_Codigo = H00IZ74_n74Contrato_Codigo[0];
            A843Contrato_DataFimTA = H00IZ74_A843Contrato_DataFimTA[0];
            n843Contrato_DataFimTA = H00IZ74_n843Contrato_DataFimTA[0];
            A83Contrato_DataVigenciaTermino = H00IZ74_A83Contrato_DataVigenciaTermino[0];
            n83Contrato_DataVigenciaTermino = H00IZ74_n83Contrato_DataVigenciaTermino[0];
            A39Contratada_Codigo = H00IZ74_A39Contratada_Codigo[0];
            n39Contratada_Codigo = H00IZ74_n39Contratada_Codigo[0];
            A92Contrato_Ativo = H00IZ74_A92Contrato_Ativo[0];
            n92Contrato_Ativo = H00IZ74_n92Contrato_Ativo[0];
            A83Contrato_DataVigenciaTermino = H00IZ74_A83Contrato_DataVigenciaTermino[0];
            n83Contrato_DataVigenciaTermino = H00IZ74_n83Contrato_DataVigenciaTermino[0];
            A843Contrato_DataFimTA = H00IZ74_A843Contrato_DataFimTA[0];
            n843Contrato_DataFimTA = H00IZ74_n843Contrato_DataFimTA[0];
            A1869Contrato_DataTermino = ((A83Contrato_DataVigenciaTermino>A843Contrato_DataFimTA) ? A83Contrato_DataVigenciaTermino : A843Contrato_DataFimTA);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1869Contrato_DataTermino", context.localUtil.Format(A1869Contrato_DataTermino, "99/99/99"));
            AV116ContratoServicos_Codigo = A160ContratoServicos_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV116ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV116ContratoServicos_Codigo), 6, 0)));
            /* Using cursor H00IZ75 */
            pr_default.execute(58, new Object[] {A160ContratoServicos_Codigo});
            while ( (pr_default.getStatus(58) != 101) )
            {
               A1749Artefatos_Codigo = H00IZ75_A1749Artefatos_Codigo[0];
               A1751Artefatos_Descricao = H00IZ75_A1751Artefatos_Descricao[0];
               A1751Artefatos_Descricao = H00IZ75_A1751Artefatos_Descricao[0];
               AV146sdt_Artefato = new SdtSDT_Artefatos(context);
               AV146sdt_Artefato.gxTpr_Artefatos_codigo = A1749Artefatos_Codigo;
               AV146sdt_Artefato.gxTpr_Artefatos_descricao = A1751Artefatos_Descricao;
               AV145Sdt_Artefatos.Add(AV146sdt_Artefato, 0);
               pr_default.readNext(58);
            }
            pr_default.close(58);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(57);
         }
         pr_default.close(57);
         AV144ServicoTemArtefatos = (bool)(((AV145Sdt_Artefatos.Count>0)));
         bttBtnartefatos_Visible = (AV144ServicoTemArtefatos ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnartefatos_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnartefatos_Visible), 5, 0)));
         bttBtnenter_Visible = (AV53WWPContext.gxTpr_Update&&!AV144ServicoTemArtefatos ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Visible), 5, 0)));
         AV30WebSession.Set("Artefatos", AV145Sdt_Artefatos.ToXml(false, true, "SDT_ArtefatosCollection", "GxEv3Up14_Meetrika"));
      }

      protected void S302( )
      {
         /* 'SOLICITACAOARTEFATOS' Routine */
         context.PopUp(formatLink("wp_solicitacaoartefatos.aspx") + "?" + UrlEncode("" +AV182NewCntSrvCod), new Object[] {});
         context.DoAjaxRefresh();
      }

      protected void S192( )
      {
         /* 'SETREQUISITOS' Routine */
         /* Using cursor H00IZ76 */
         pr_default.execute(59, new Object[] {AV9Codigo});
         while ( (pr_default.getStatus(59) != 101) )
         {
            A2003ContagemResultadoRequisito_OSCod = H00IZ76_A2003ContagemResultadoRequisito_OSCod[0];
            A2004ContagemResultadoRequisito_ReqCod = H00IZ76_A2004ContagemResultadoRequisito_ReqCod[0];
            AV179Requisitos.Add(A2004ContagemResultadoRequisito_ReqCod, 0);
            pr_default.readNext(59);
         }
         pr_default.close(59);
      }

      protected void S312( )
      {
         /* 'CARREGACONTRATOS' Routine */
         cmbavContrato.removeAllItems();
         cmbavContrato.addItem("0", "(Nenhum)", 0);
         /* Using cursor H00IZ79 */
         pr_default.execute(60, new Object[] {AV168CriarOSPara_Codigo});
         while ( (pr_default.getStatus(60) != 101) )
         {
            A77Contrato_Numero = H00IZ79_A77Contrato_Numero[0];
            A74Contrato_Codigo = H00IZ79_A74Contrato_Codigo[0];
            n74Contrato_Codigo = H00IZ79_n74Contrato_Codigo[0];
            A39Contratada_Codigo = H00IZ79_A39Contratada_Codigo[0];
            n39Contratada_Codigo = H00IZ79_n39Contratada_Codigo[0];
            A92Contrato_Ativo = H00IZ79_A92Contrato_Ativo[0];
            n92Contrato_Ativo = H00IZ79_n92Contrato_Ativo[0];
            A1953Contratada_CntPadrao = H00IZ79_A1953Contratada_CntPadrao[0];
            n1953Contratada_CntPadrao = H00IZ79_n1953Contratada_CntPadrao[0];
            A843Contrato_DataFimTA = H00IZ79_A843Contrato_DataFimTA[0];
            n843Contrato_DataFimTA = H00IZ79_n843Contrato_DataFimTA[0];
            A83Contrato_DataVigenciaTermino = H00IZ79_A83Contrato_DataVigenciaTermino[0];
            n83Contrato_DataVigenciaTermino = H00IZ79_n83Contrato_DataVigenciaTermino[0];
            A843Contrato_DataFimTA = H00IZ79_A843Contrato_DataFimTA[0];
            n843Contrato_DataFimTA = H00IZ79_n843Contrato_DataFimTA[0];
            A1953Contratada_CntPadrao = H00IZ79_A1953Contratada_CntPadrao[0];
            n1953Contratada_CntPadrao = H00IZ79_n1953Contratada_CntPadrao[0];
            if ( ( A843Contrato_DataFimTA >= DateTimeUtil.ServerDate( context, "DEFAULT") ) || ( A83Contrato_DataVigenciaTermino >= DateTimeUtil.ServerDate( context, "DEFAULT") ) )
            {
               A1869Contrato_DataTermino = ((A83Contrato_DataVigenciaTermino>A843Contrato_DataFimTA) ? A83Contrato_DataVigenciaTermino : A843Contrato_DataFimTA);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1869Contrato_DataTermino", context.localUtil.Format(A1869Contrato_DataTermino, "99/99/99"));
               AV178Contrato_Padrao = A1953Contratada_CntPadrao;
               /* Using cursor H00IZ80 */
               pr_default.execute(61, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo, AV182NewCntSrvCod});
               while ( (pr_default.getStatus(61) != 101) )
               {
                  A638ContratoServicos_Ativo = H00IZ80_A638ContratoServicos_Ativo[0];
                  A155Servico_Codigo = H00IZ80_A155Servico_Codigo[0];
                  A160ContratoServicos_Codigo = H00IZ80_A160ContratoServicos_Codigo[0];
                  cmbavContrato.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)), A77Contrato_Numero, 0);
                  AV174Contrato = A74Contrato_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV174Contrato", StringUtil.LTrim( StringUtil.Str( (decimal)(AV174Contrato), 6, 0)));
                  AV116ContratoServicos_Codigo = A160ContratoServicos_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV116ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV116ContratoServicos_Codigo), 6, 0)));
                  pr_default.readNext(61);
               }
               pr_default.close(61);
            }
            pr_default.readNext(60);
         }
         pr_default.close(60);
         if ( cmbavContrato.ItemCount > 2 )
         {
            AV174Contrato = AV178Contrato_Padrao;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV174Contrato", StringUtil.LTrim( StringUtil.Str( (decimal)(AV174Contrato), 6, 0)));
            AV116ContratoServicos_Codigo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV116ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV116ContratoServicos_Codigo), 6, 0)));
            lblTextblockcontrato_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontrato_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcontrato_Visible), 5, 0)));
            cmbavContrato.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContrato_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContrato.Visible), 5, 0)));
         }
         else
         {
            lblTextblockcontrato_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontrato_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcontrato_Visible), 5, 0)));
            cmbavContrato.Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContrato_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContrato.Visible), 5, 0)));
            /* Execute user subroutine: 'TEMARTEFATOS' */
            S322 ();
            if (returnInSub) return;
         }
      }

      protected void S182( )
      {
         /* 'GETCONTRATOFS' Routine */
         AV163Count = 0;
         /* Using cursor H00IZ83 */
         pr_default.execute(62, new Object[] {AV5Usuario_Codigo});
         while ( (pr_default.getStatus(62) != 101) )
         {
            A92Contrato_Ativo = H00IZ83_A92Contrato_Ativo[0];
            n92Contrato_Ativo = H00IZ83_n92Contrato_Ativo[0];
            A83Contrato_DataVigenciaTermino = H00IZ83_A83Contrato_DataVigenciaTermino[0];
            n83Contrato_DataVigenciaTermino = H00IZ83_n83Contrato_DataVigenciaTermino[0];
            A39Contratada_Codigo = H00IZ83_A39Contratada_Codigo[0];
            n39Contratada_Codigo = H00IZ83_n39Contratada_Codigo[0];
            A74Contrato_Codigo = H00IZ83_A74Contrato_Codigo[0];
            n74Contrato_Codigo = H00IZ83_n74Contrato_Codigo[0];
            A843Contrato_DataFimTA = H00IZ83_A843Contrato_DataFimTA[0];
            n843Contrato_DataFimTA = H00IZ83_n843Contrato_DataFimTA[0];
            A843Contrato_DataFimTA = H00IZ83_A843Contrato_DataFimTA[0];
            n843Contrato_DataFimTA = H00IZ83_n843Contrato_DataFimTA[0];
            if ( ( A843Contrato_DataFimTA >= DateTimeUtil.ServerDate( context, "DEFAULT") ) || ( A83Contrato_DataVigenciaTermino >= DateTimeUtil.ServerDate( context, "DEFAULT") ) )
            {
               AV163Count = (short)(AV163Count+1);
               AV188ContratoFS = A74Contrato_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV188ContratoFS", StringUtil.LTrim( StringUtil.Str( (decimal)(AV188ContratoFS), 6, 0)));
            }
            pr_default.readNext(62);
         }
         pr_default.close(62);
         if ( AV163Count > 1 )
         {
            AV188ContratoFS = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV188ContratoFS", StringUtil.LTrim( StringUtil.Str( (decimal)(AV188ContratoFS), 6, 0)));
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E22IZ2( )
      {
         /* Load Routine */
      }

      protected void wb_table2_86_IZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblinvisible_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTblinvisible_Internalname, tblTblinvisible_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPfb_Internalname, StringUtil.LTrim( StringUtil.NToC( AV57PFB, 14, 5, ",", "")), ((edtavPfb_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV57PFB, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV57PFB, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,89);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPfb_Jsonclick, 0, "Attribute", "", "", "", 1, edtavPfb_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_WP_Tramitacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPfl_Internalname, StringUtil.LTrim( StringUtil.NToC( AV58PFL, 14, 5, ",", "")), ((edtavPfl_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV58PFL, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV58PFL, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,90);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPfl_Jsonclick, 0, "Attribute", "", "", "", 1, edtavPfl_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_WP_Tramitacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPfbof_Internalname, StringUtil.LTrim( StringUtil.NToC( AV70PFBOF, 14, 5, ",", "")), ((edtavPfbof_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV70PFBOF, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV70PFBOF, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,91);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPfbof_Jsonclick, 0, "Attribute", "", "", "", 1, edtavPfbof_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_WP_Tramitacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPflof_Internalname, StringUtil.LTrim( StringUtil.NToC( AV71PFLOF, 14, 5, ",", "")), ((edtavPflof_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV71PFLOF, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV71PFLOF, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,92);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPflof_Jsonclick, 0, "Attribute", "", "", "", 1, edtavPflof_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_WP_Tramitacao.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavStatuscontagem, cmbavStatuscontagem_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV42StatusContagem), 2, 0)), 1, cmbavStatuscontagem_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbavStatuscontagem.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,93);\"", "", true, "HLP_WP_Tramitacao.htm");
            cmbavStatuscontagem.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV42StatusContagem), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavStatuscontagem_Internalname, "Values", (String)(cmbavStatuscontagem.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPfbfmultima_Internalname, StringUtil.LTrim( StringUtil.NToC( AV81PFBFMUltima, 14, 5, ",", "")), ((edtavPfbfmultima_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV81PFBFMUltima, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV81PFBFMUltima, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,94);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPfbfmultima_Jsonclick, 0, "Attribute", "", "", "", 1, edtavPfbfmultima_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_WP_Tramitacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPfbfsultima_Internalname, StringUtil.LTrim( StringUtil.NToC( AV83PFBFSUltima, 14, 5, ",", "")), ((edtavPfbfsultima_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV83PFBFSUltima, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV83PFBFSUltima, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,95);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPfbfsultima_Jsonclick, 0, "Attribute", "", "", "", 1, edtavPfbfsultima_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_WP_Tramitacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPflfmultima_Internalname, StringUtil.LTrim( StringUtil.NToC( AV82PFLFMUltima, 14, 5, ",", "")), ((edtavPflfmultima_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV82PFLFMUltima, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV82PFLFMUltima, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,96);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPflfmultima_Jsonclick, 0, "Attribute", "", "", "", 1, edtavPflfmultima_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_WP_Tramitacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPflfsultima_Internalname, StringUtil.LTrim( StringUtil.NToC( AV80PFLFSUltima, 14, 5, ",", "")), ((edtavPflfsultima_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV80PFLFSUltima, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV80PFLFSUltima, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,97);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPflfsultima_Jsonclick, 0, "Attribute", "", "", "", 1, edtavPflfsultima_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_WP_Tramitacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOldpfb_Internalname, StringUtil.LTrim( StringUtil.NToC( AV104OldPFB, 14, 5, ",", "")), ((edtavOldpfb_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV104OldPFB, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV104OldPFB, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,98);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOldpfb_Jsonclick, 0, "Attribute", "", "", "", 1, edtavOldpfb_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_WP_Tramitacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOldpfl_Internalname, StringUtil.LTrim( StringUtil.NToC( AV105OldPFL, 14, 5, ",", "")), ((edtavOldpfl_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV105OldPFL, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV105OldPFL, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,99);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOldpfl_Jsonclick, 0, "Attribute", "", "", "", 1, edtavOldpfl_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_WP_Tramitacao.htm");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavRemetenteinterno_Internalname, StringUtil.BoolToStr( AV151RemetenteInterno), "", "", chkavRemetenteinterno.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(100, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,100);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_86_IZ2e( true) ;
         }
         else
         {
            wb_table2_86_IZ2e( false) ;
         }
      }

      protected void wb_table1_2_IZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 5, 5, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"5\" >") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbdestino_Internalname, lblTbdestino_Caption, "", "", lblTbdestino_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Tramitacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  style=\""+CSSHelper.Prettify( "width:80px")+"\" class='RequiredDataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavUsuario_codigo, cmbavUsuario_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV5Usuario_Codigo), 6, 0)), 1, cmbavUsuario_codigo_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVUSUARIO_CODIGO.CLICK."+"'", "int", "", 1, cmbavUsuario_codigo.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_WP_Tramitacao.htm");
            cmbavUsuario_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV5Usuario_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUsuario_codigo_Internalname, "Values", (String)(cmbavUsuario_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:28px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbprazo_Internalname, lblTbprazo_Caption, "", "", lblTbprazo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbprazo_Visible, 1, 0, "HLP_WP_Tramitacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class=''>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavPrazoresposta_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavPrazoresposta_Internalname, context.localUtil.Format(AV39PrazoResposta, "99/99/99"), context.localUtil.Format( AV39PrazoResposta, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,15);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPrazoresposta_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", edtavPrazoresposta_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_Tramitacao.htm");
            GxWebStd.gx_bitmap( context, edtavPrazoresposta_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavPrazoresposta_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_Tramitacao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:24px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbcontratofs_Internalname, "Contrato:", "", "", lblTbcontratofs_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbcontratofs_Visible, 1, 0, "HLP_WP_Tramitacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class=''>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContratofs, dynavContratofs_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV188ContratoFS), 6, 0)), 1, dynavContratofs_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavContratofs.Visible, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WP_Tramitacao.htm");
            dynavContratofs.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV188ContratoFS), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratofs_Internalname, "Values", (String)(dynavContratofs.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"6\" >") ;
            wb_table3_23_IZ2( true) ;
         }
         else
         {
            wb_table3_23_IZ2( false) ;
         }
         return  ;
      }

      protected void wb_table3_23_IZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbnaocnf_Internalname, "N�o conformidade:", "", "", lblTbnaocnf_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbnaocnf_Visible, 1, 0, "HLP_WP_Tramitacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\" >") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavNaoconformidade_codigo, dynavNaoconformidade_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV183NaoConformidade_Codigo), 6, 0)), 1, dynavNaoconformidade_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavNaoconformidade_codigo.Visible, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", "", true, "HLP_WP_Tramitacao.htm");
            dynavNaoconformidade_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV183NaoConformidade_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavNaoconformidade_codigo_Internalname, "Values", (String)(dynavNaoconformidade_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:middle")+"\">") ;
            context.WriteHtmlText( "<p></p>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbdescricao_Internalname, "Desc. do Cliente:", "", "", lblTbdescricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbdescricao_Visible, 1, 0, "HLP_WP_Tramitacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"4\"  class=''>") ;
            context.WriteHtmlText( "<p>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDescricao_Internalname, AV140Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,51);\"", 0, edtavDescricao_Visible, edtavDescricao_Enabled, 0, 100, "%", 5, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "", "HLP_WP_Tramitacao.htm");
            context.WriteHtmlText( "</p>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbobservacoes_Internalname, lblTbobservacoes_Caption, "", "", lblTbobservacoes_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Tramitacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"4\"  class='RequiredDataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavObservacao_Internalname, AV6Observacao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", 0, 1, 1, 0, 80, "chr", 5, "row", StyleString, ClassString, "", "2097152", 1, "", "", -1, true, "", "HLP_WP_Tramitacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            context.WriteHtmlText( "<td data-align=\"center\" colspan=\"5\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;vertical-align:top;height:0px")+"\">") ;
            /* Radio button */
            ClassString = "";
            StyleString = "font-family:'Arial'; font-size:10.0pt; font-weight:bold; font-style:normal;";
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            GxWebStd.gx_radio_ctrl( context, radavDeferido, radavDeferido_Internalname, StringUtil.RTrim( AV46Deferido), "", radavDeferido.Visible, 1, 1, 1, StyleString, ClassString, "", 0, radavDeferido_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", TempTags+" onclick=\"gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", "HLP_WP_Tramitacao.htm");
            wb_table4_60_IZ2( true) ;
         }
         else
         {
            wb_table4_60_IZ2( false) ;
         }
         return  ;
      }

      protected void wb_table4_60_IZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table5_71_IZ2( true) ;
         }
         else
         {
            wb_table5_71_IZ2( false) ;
         }
         return  ;
      }

      protected void wb_table5_71_IZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\" colspan=\"5\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;vertical-align:bottom")+"\">") ;
            wb_table6_80_IZ2( true) ;
         }
         else
         {
            wb_table6_80_IZ2( false) ;
         }
         return  ;
      }

      protected void wb_table6_80_IZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_IZ2e( true) ;
         }
         else
         {
            wb_table1_2_IZ2e( false) ;
         }
      }

      protected void wb_table6_80_IZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'',0)\"";
            ClassString = "ActionButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnartefatos_Internalname, "", bttBtnartefatos_Caption, bttBtnartefatos_Jsonclick, 5, "Artefatos", "", StyleString, ClassString, bttBtnartefatos_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOARTEFATOS\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_Tramitacao.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnenter_Internalname, "", bttBtnenter_Caption, bttBtnenter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtnenter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_Tramitacao.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_Tramitacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_80_IZ2e( true) ;
         }
         else
         {
            wb_table6_80_IZ2e( false) ;
         }
      }

      protected void wb_table5_71_IZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblanexos_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTblanexos_Internalname, tblTblanexos_Internalname, "", "Table", 0, "", "", 0, 5, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbdestino5_Internalname, "Anexos:", "", "", lblTbdestino5_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Tramitacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            if ( ! isFullAjaxMode( ) )
            {
               /* WebComponent */
               context.WriteHtmlText( "<div") ;
               GxWebStd.ClassAttribute( context, "gxwebcomponent");
               context.WriteHtmlText( " id=\""+"gxHTMLWrpW0077"+""+"\""+"") ;
               context.WriteHtmlText( ">") ;
               if ( ! context.isAjaxRequest( ) )
               {
                  context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpW0077"+"");
               }
               WebComp_Webcomp2.componentdraw();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.httpAjaxContext.ajax_rspEndCmp();
               }
               context.WriteHtmlText( "</div>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_71_IZ2e( true) ;
         }
         else
         {
            wb_table5_71_IZ2e( false) ;
         }
      }

      protected void wb_table4_60_IZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblnaoacata_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTblnaoacata_Internalname, tblTblnaoacata_Internalname, "", "Table", 0, "", "", 0, 5, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbdescpfb_Internalname, lblTbdescpfb_Caption, "", "", lblTbdescpfb_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Tramitacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavNewpfb_Internalname, StringUtil.LTrim( StringUtil.NToC( AV63NewPFB, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV63NewPFB, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,66);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavNewpfb_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_WP_Tramitacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbdescpfl_Internalname, lblTbdescpfl_Caption, "", "", lblTbdescpfl_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Tramitacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavNewpfl_Internalname, StringUtil.LTrim( StringUtil.NToC( AV64NewPFL, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV64NewPFL, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,70);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavNewpfl_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_WP_Tramitacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_60_IZ2e( true) ;
         }
         else
         {
            wb_table4_60_IZ2e( false) ;
         }
      }

      protected void wb_table3_23_IZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblcriaros_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            sStyleString = sStyleString + "border:" + StringUtil.Str( (decimal)(5), 3, 0) + "px solid;";
            GxWebStd.gx_table_start( context, tblTblcriaros_Internalname, tblTblcriaros_Internalname, "", "Table", 5, "", "", 5, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbservico5_Internalname, "Grupo de Servi�os:", "", "", lblTbservico5_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Tramitacao.htm");
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;&nbsp;") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavServicogrupo_codigo, cmbavServicogrupo_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV177ServicoGrupo_Codigo), 6, 0)), 1, cmbavServicogrupo_codigo_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVSERVICOGRUPO_CODIGO.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "", true, "HLP_WP_Tramitacao.htm");
            cmbavServicogrupo_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV177ServicoGrupo_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServicogrupo_codigo_Internalname, "Values", (String)(cmbavServicogrupo_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "&nbsp; ") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbservico_Internalname, "Servi�o:", "", "", lblTbservico_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbservico_Visible, 1, 0, "HLP_WP_Tramitacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavNewcntsrvcod, cmbavNewcntsrvcod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV182NewCntSrvCod), 6, 0)), 1, cmbavNewcntsrvcod_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVNEWCNTSRVCOD.CLICK."+"'", "int", "", cmbavNewcntsrvcod.Visible, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,32);\"", "", true, "HLP_WP_Tramitacao.htm");
            cmbavNewcntsrvcod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV182NewCntSrvCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavNewcntsrvcod_Internalname, "Values", (String)(cmbavNewcntsrvcod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbservico4_Internalname, "Prestadora:", "", "", lblTbservico4_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Tramitacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavCriarospara_codigo, cmbavCriarospara_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV168CriarOSPara_Codigo), 6, 0)), 1, cmbavCriarospara_codigo_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVCRIAROSPARA_CODIGO.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"", "", true, "HLP_WP_Tramitacao.htm");
            cmbavCriarospara_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV168CriarOSPara_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavCriarospara_codigo_Internalname, "Values", (String)(cmbavCriarospara_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "&nbsp; ") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_Internalname, "Contrato:", "", "", lblTextblockcontrato_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTextblockcontrato_Visible, 1, 0, "HLP_WP_Tramitacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContrato, cmbavContrato_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV174Contrato), 6, 0)), 1, cmbavContrato_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVCONTRATO.CLICK."+"'", "int", "", cmbavContrato.Visible, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,41);\"", "", true, "HLP_WP_Tramitacao.htm");
            cmbavContrato.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV174Contrato), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContrato_Internalname, "Values", (String)(cmbavContrato.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_23_IZ2e( true) ;
         }
         else
         {
            wb_table3_23_IZ2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV9Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Codigo), 6, 0)));
         AV11PodeAtribuirDmn = (bool)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11PodeAtribuirDmn", AV11PodeAtribuirDmn);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPODEATRIBUIRDMN", GetSecureSignedToken( "", AV11PodeAtribuirDmn));
         AV38ParaContratante = (bool)getParm(obj,2);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ParaContratante", AV38ParaContratante);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPARACONTRATANTE", GetSecureSignedToken( "", AV38ParaContratante));
         AV10Destino_Nome = (String)getParm(obj,3);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Destino_Nome", AV10Destino_Nome);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAIZ2( ) ;
         WSIZ2( ) ;
         WEIZ2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         if ( StringUtil.StrCmp(WebComp_Webcomp2_Component, "") == 0 )
         {
            WebComp_Webcomp2 = getWebComponent(GetType(), "GeneXus.Programs", "wc_contagemresultadoevidencias", new Object[] {context} );
            WebComp_Webcomp2.ComponentInit();
            WebComp_Webcomp2.Name = "WC_ContagemResultadoEvidencias";
            WebComp_Webcomp2_Component = "WC_ContagemResultadoEvidencias";
         }
         if ( ! ( WebComp_Webcomp2 == null ) )
         {
            WebComp_Webcomp2.componentthemes();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203262263316");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_tramitacao.js", "?20203262263317");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTbdestino_Internalname = "TBDESTINO";
         cmbavUsuario_codigo_Internalname = "vUSUARIO_CODIGO";
         lblTbprazo_Internalname = "TBPRAZO";
         edtavPrazoresposta_Internalname = "vPRAZORESPOSTA";
         lblTbcontratofs_Internalname = "TBCONTRATOFS";
         dynavContratofs_Internalname = "vCONTRATOFS";
         lblTbservico5_Internalname = "TBSERVICO5";
         cmbavServicogrupo_codigo_Internalname = "vSERVICOGRUPO_CODIGO";
         lblTbservico_Internalname = "TBSERVICO";
         cmbavNewcntsrvcod_Internalname = "vNEWCNTSRVCOD";
         lblTbservico4_Internalname = "TBSERVICO4";
         cmbavCriarospara_codigo_Internalname = "vCRIAROSPARA_CODIGO";
         lblTextblockcontrato_Internalname = "TEXTBLOCKCONTRATO";
         cmbavContrato_Internalname = "vCONTRATO";
         tblTblcriaros_Internalname = "TBLCRIAROS";
         lblTbnaocnf_Internalname = "TBNAOCNF";
         dynavNaoconformidade_codigo_Internalname = "vNAOCONFORMIDADE_CODIGO";
         lblTbdescricao_Internalname = "TBDESCRICAO";
         edtavDescricao_Internalname = "vDESCRICAO";
         lblTbobservacoes_Internalname = "TBOBSERVACOES";
         edtavObservacao_Internalname = "vOBSERVACAO";
         radavDeferido_Internalname = "vDEFERIDO";
         lblTbdescpfb_Internalname = "TBDESCPFB";
         edtavNewpfb_Internalname = "vNEWPFB";
         lblTbdescpfl_Internalname = "TBDESCPFL";
         edtavNewpfl_Internalname = "vNEWPFL";
         tblTblnaoacata_Internalname = "TBLNAOACATA";
         lblTbdestino5_Internalname = "TBDESTINO5";
         tblTblanexos_Internalname = "TBLANEXOS";
         bttBtnartefatos_Internalname = "BTNARTEFATOS";
         bttBtnenter_Internalname = "BTNENTER";
         bttBtnfechar_Internalname = "BTNFECHAR";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTable1_Internalname = "TABLE1";
         edtavPfb_Internalname = "vPFB";
         edtavPfl_Internalname = "vPFL";
         edtavPfbof_Internalname = "vPFBOF";
         edtavPflof_Internalname = "vPFLOF";
         cmbavStatuscontagem_Internalname = "vSTATUSCONTAGEM";
         edtavPfbfmultima_Internalname = "vPFBFMULTIMA";
         edtavPfbfsultima_Internalname = "vPFBFSULTIMA";
         edtavPflfmultima_Internalname = "vPFLFMULTIMA";
         edtavPflfsultima_Internalname = "vPFLFSULTIMA";
         edtavOldpfb_Internalname = "vOLDPFB";
         edtavOldpfl_Internalname = "vOLDPFL";
         chkavRemetenteinterno_Internalname = "vREMETENTEINTERNO";
         tblTblinvisible_Internalname = "TBLINVISIBLE";
         lblTbjava_Internalname = "TBJAVA";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbavContrato_Jsonclick = "";
         lblTextblockcontrato_Visible = 1;
         cmbavCriarospara_codigo_Jsonclick = "";
         cmbavNewcntsrvcod_Jsonclick = "";
         lblTbservico_Visible = 1;
         cmbavServicogrupo_codigo_Jsonclick = "";
         edtavNewpfl_Jsonclick = "";
         edtavNewpfb_Jsonclick = "";
         bttBtnenter_Visible = 1;
         bttBtnartefatos_Visible = 1;
         radavDeferido_Jsonclick = "";
         radavDeferido.BackColor = (int)(0xFFFFFF);
         edtavDescricao_Enabled = 1;
         lblTbdescricao_Visible = 1;
         dynavNaoconformidade_codigo_Jsonclick = "";
         lblTbnaocnf_Visible = 1;
         dynavContratofs_Jsonclick = "";
         lblTbcontratofs_Visible = 1;
         edtavPrazoresposta_Jsonclick = "";
         lblTbprazo_Visible = 1;
         cmbavUsuario_codigo_Jsonclick = "";
         edtavOldpfl_Jsonclick = "";
         edtavOldpfl_Enabled = 1;
         edtavOldpfb_Jsonclick = "";
         edtavOldpfb_Enabled = 1;
         edtavPflfsultima_Jsonclick = "";
         edtavPflfsultima_Enabled = 1;
         edtavPflfmultima_Jsonclick = "";
         edtavPflfmultima_Enabled = 1;
         edtavPfbfsultima_Jsonclick = "";
         edtavPfbfsultima_Enabled = 1;
         edtavPfbfmultima_Jsonclick = "";
         edtavPfbfmultima_Enabled = 1;
         cmbavStatuscontagem_Jsonclick = "";
         cmbavStatuscontagem.Enabled = 1;
         edtavPflof_Jsonclick = "";
         edtavPflof_Enabled = 1;
         edtavPfbof_Jsonclick = "";
         edtavPfbof_Enabled = 1;
         edtavPfl_Jsonclick = "";
         edtavPfl_Enabled = 1;
         edtavPfb_Jsonclick = "";
         edtavPfb_Enabled = 1;
         lblTbprazo_Caption = "Prazo de resposta:";
         cmbavNewcntsrvcod.Visible = 1;
         lblTbdescpfl_Caption = "Liquido:";
         lblTbdescpfb_Caption = "Valor Bruto:";
         cmbavUsuario_codigo.Enabled = 1;
         bttBtnenter_Caption = "Confirmar";
         bttBtnartefatos_Caption = "Artefatos";
         edtavDescricao_Visible = 1;
         dynavContratofs.Visible = 1;
         tblTblcriaros_Visible = 1;
         cmbavContrato.Visible = 1;
         chkavRemetenteinterno.Visible = 1;
         radavDeferido.Visible = 1;
         tblTblanexos_Visible = 1;
         dynavNaoconformidade_codigo.Visible = 1;
         tblTblnaoacata_Visible = 1;
         tblTblinvisible_Visible = 1;
         edtavPrazoresposta_Visible = 1;
         lblTbobservacoes_Caption = "Observa��es";
         lblTbdestino_Caption = "Destino:";
         chkavRemetenteinterno.Caption = "";
         lblTbjava_Caption = "tbJava";
         lblTbjava_Visible = 1;
         cmbavUsuario_codigo.Description = "";
         Form.Caption = "Tramitac�o";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public void Validv_Usuario_codigo( GXCombobox cmbGX_Parm1 ,
                                         GXCombobox dynGX_Parm2 )
      {
         cmbavUsuario_codigo = cmbGX_Parm1;
         AV5Usuario_Codigo = (int)(NumberUtil.Val( cmbavUsuario_codigo.CurrentValue, "."));
         dynavContratofs = dynGX_Parm2;
         AV188ContratoFS = (int)(NumberUtil.Val( dynavContratofs.CurrentValue, "."));
         GXVvCONTRATOFS_htmlIZ2( AV5Usuario_Codigo) ;
         dynload_actions( ) ;
         if ( dynavContratofs.ItemCount > 0 )
         {
            AV188ContratoFS = (int)(NumberUtil.Val( dynavContratofs.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV188ContratoFS), 6, 0))), "."));
         }
         dynavContratofs.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV188ContratoFS), 6, 0));
         isValidOutput.Add(dynavContratofs);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'AV191Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11PodeAtribuirDmn',fld:'vPODEATRIBUIRDMN',pic:'',hsh:true,nv:false},{av:'AV145Sdt_Artefatos',fld:'vSDT_ARTEFATOS',pic:'',nv:null}],oparms:[{av:'AV53WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV31AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV24Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV67Contratada_Sigla',fld:'vCONTRATADA_SIGLA',pic:'@!',nv:''},{av:'AV40Contratante_Codigo',fld:'vCONTRATANTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8UserEhContratante',fld:'vUSEREHCONTRATANTE',pic:'',nv:false},{av:'AV12UserId',fld:'vUSERID',pic:'ZZZZZ9',nv:0},{av:'AV114Caller',fld:'vCALLER',pic:'@!',nv:''},{av:'AV191Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11PodeAtribuirDmn',fld:'vPODEATRIBUIRDMN',pic:'',hsh:true,nv:false},{ctrl:'BTNARTEFATOS',prop:'Caption'},{ctrl:'BTNENTER',prop:'Visible'},{av:'AV145Sdt_Artefatos',fld:'vSDT_ARTEFATOS',pic:'',nv:null}]}");
         setEventMetadata("VUSUARIO_CODIGO.CLICK","{handler:'E13IZ2',iparms:[{av:'AV5Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV27Prestadora_Codigo',fld:'vPRESTADORA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV16StatusDemanda',fld:'vSTATUSDEMANDA',pic:'',nv:''},{av:'AV12UserId',fld:'vUSERID',pic:'ZZZZZ9',nv:0},{av:'AV11PodeAtribuirDmn',fld:'vPODEATRIBUIRDMN',pic:'',hsh:true,nv:false},{av:'AV114Caller',fld:'vCALLER',pic:'@!',nv:''},{av:'AV97EmDivergencia',fld:'vEMDIVERGENCIA',pic:'',nv:false},{av:'AV20Selecionadas',fld:'vSELECIONADAS',pic:'',nv:null},{av:'AV110Origem_Sigla',fld:'vORIGEM_SIGLA',pic:'@!',nv:''},{av:'AV61UsuarioDaPrestadora',fld:'vUSUARIODAPRESTADORA',pic:'',nv:false},{av:'AV53WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV83PFBFSUltima',fld:'vPFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'AV80PFLFSUltima',fld:'vPFLFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'AV81PFBFMUltima',fld:'vPFBFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'AV82PFLFMUltima',fld:'vPFLFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'AV8UserEhContratante',fld:'vUSEREHCONTRATANTE',pic:'',nv:false},{av:'AV154EhSolicitacaoSS',fld:'vEHSOLICITACAOSS',pic:'',nv:false},{av:'AV46Deferido',fld:'vDEFERIDO',pic:'',nv:''},{av:'AV50PrazoInicial',fld:'vPRAZOINICIAL',pic:'99/99/99 99:99',nv:''},{av:'AV107TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'A1446ContratoGestor_ContratadaAreaCod',fld:'CONTRATOGESTOR_CONTRATADAAREACOD',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A843Contrato_DataFimTA',fld:'CONTRATO_DATAFIMTA',pic:'',nv:''},{av:'A83Contrato_DataVigenciaTermino',fld:'CONTRATO_DATAVIGENCIATERMINO',pic:'',nv:''},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',nv:false},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO',pic:'',nv:false},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1078ContratoGestor_ContratoCod',fld:'CONTRATOGESTOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A638ContratoServicos_Ativo',fld:'CONTRATOSERVICOS_ATIVO',pic:'',nv:false},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',nv:false},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A75Contrato_AreaTrabalhoCod',fld:'CONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A1825ContratoAuxiliar_UsuarioCod',fld:'CONTRATOAUXILIAR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1824ContratoAuxiliar_ContratoCod',fld:'CONTRATOAUXILIAR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV177ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1858ContratoServicos_Alias',fld:'CONTRATOSERVICOS_ALIAS',pic:'@!',nv:''}],oparms:[{av:'tblTblcriaros_Visible',ctrl:'TBLCRIAROS',prop:'Visible'},{av:'AV183NaoConformidade_Codigo',fld:'vNAOCONFORMIDADE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'lblTbnaocnf_Visible',ctrl:'TBNAOCNF',prop:'Visible'},{av:'dynavNaoconformidade_codigo'},{ctrl:'BTNENTER',prop:'Caption'},{av:'tblTblnaoacata_Visible',ctrl:'TBLNAOACATA',prop:'Visible'},{av:'tblTblanexos_Visible',ctrl:'TBLANEXOS',prop:'Visible'},{av:'lblTbdescpfb_Caption',ctrl:'TBDESCPFB',prop:'Caption'},{av:'lblTbdescpfl_Caption',ctrl:'TBDESCPFL',prop:'Caption'},{av:'AV63NewPFB',fld:'vNEWPFB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64NewPFL',fld:'vNEWPFL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV104OldPFB',fld:'vOLDPFB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV105OldPFL',fld:'vOLDPFL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV182NewCntSrvCod',fld:'vNEWCNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'lblTbservico_Visible',ctrl:'TBSERVICO',prop:'Visible'},{av:'cmbavNewcntsrvcod'},{av:'radavDeferido'},{av:'lblTbprazo_Caption',ctrl:'TBPRAZO',prop:'Caption'},{av:'AV39PrazoResposta',fld:'vPRAZORESPOSTA',pic:'',nv:''},{av:'AV46Deferido',fld:'vDEFERIDO',pic:'',nv:''},{av:'lblTbprazo_Visible',ctrl:'TBPRAZO',prop:'Visible'},{av:'lblTbcontratofs_Visible',ctrl:'TBCONTRATOFS',prop:'Visible'},{av:'edtavPrazoresposta_Visible',ctrl:'vPRAZORESPOSTA',prop:'Visible'},{av:'dynavContratofs'},{av:'AV177ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV188ContratoFS',fld:'vCONTRATOFS',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("ENTER","{handler:'E14IZ2',iparms:[{av:'AV191Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV5Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV16StatusDemanda',fld:'vSTATUSDEMANDA',pic:'',nv:''},{av:'AV97EmDivergencia',fld:'vEMDIVERGENCIA',pic:'',nv:false},{av:'AV182NewCntSrvCod',fld:'vNEWCNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV168CriarOSPara_Codigo',fld:'vCRIAROSPARA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV174Contrato',fld:'vCONTRATO',pic:'ZZZZZ9',nv:0},{av:'AV6Observacao',fld:'vOBSERVACAO',pic:'',nv:''},{av:'AV9Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV116ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV140Descricao',fld:'vDESCRICAO',pic:'',hsh:true,nv:''},{av:'AV179Requisitos',fld:'vREQUISITOS',pic:'',nv:null},{av:'AV10Destino_Nome',fld:'vDESTINO_NOME',pic:'@!',nv:''},{av:'AV20Selecionadas',fld:'vSELECIONADAS',pic:'',nv:null},{av:'AV15AtribuidoA_Codigo',fld:'vATRIBUIDOA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A892LogResponsavel_DemandaCod',fld:'LOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'A1797LogResponsavel_Codigo',fld:'LOGRESPONSAVEL_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'A1149LogResponsavel_OwnerEhContratante',fld:'LOGRESPONSAVEL_OWNEREHCONTRATANTE',pic:'',nv:false},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A1908Usuario_DeFerias',fld:'USUARIO_DEFERIAS',pic:'',nv:false},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A1078ContratoGestor_ContratoCod',fld:'CONTRATOGESTOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'A1603ContagemResultado_CntCod',fld:'CONTAGEMRESULTADO_CNTCOD',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV27Prestadora_Codigo',fld:'vPRESTADORA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV46Deferido',fld:'vDEFERIDO',pic:'',nv:''},{av:'AV45PrazoEntrega',fld:'vPRAZOENTREGA',pic:'99/99/99 99:99',nv:''},{av:'AV50PrazoInicial',fld:'vPRAZOINICIAL',pic:'99/99/99 99:99',nv:''},{av:'AV43NovoStatus',fld:'vNOVOSTATUS',pic:'',nv:''},{av:'AV12UserId',fld:'vUSERID',pic:'ZZZZZ9',nv:0},{av:'AV114Caller',fld:'vCALLER',pic:'@!',nv:''},{av:'AV8UserEhContratante',fld:'vUSEREHCONTRATANTE',pic:'',nv:false},{av:'AV51PrazoAnterior',fld:'vPRAZOANTERIOR',pic:'',nv:''},{av:'AV39PrazoResposta',fld:'vPRAZORESPOSTA',pic:'',nv:''},{av:'AV24Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV93DiasAnalise',fld:'vDIASANALISE',pic:'ZZZ9',nv:0},{av:'AV18PrazoExecucao',fld:'vPRAZOEXECUCAO',pic:'99/99/99 99:99',nv:''},{av:'AV186PrazoAtual',fld:'vPRAZOATUAL',pic:'99/99/99 99:99',nv:''},{av:'AV75PrazoAnalise',fld:'vPRAZOANALISE',pic:'99/99/99 99:99',nv:''},{av:'AV107TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'AV73PrazoInicialDias',fld:'vPRAZOINICIALDIAS',pic:'ZZZ9',nv:0},{av:'AV183NaoConformidade_Codigo',fld:'vNAOCONFORMIDADE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV184OSEntregue',fld:'vOSENTREGUE',pic:'',nv:false},{av:'A2003ContagemResultadoRequisito_OSCod',fld:'CONTAGEMRESULTADOREQUISITO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'A2004ContagemResultadoRequisito_ReqCod',fld:'CONTAGEMRESULTADOREQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'AV105OldPFL',fld:'vOLDPFL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV104OldPFB',fld:'vOLDPFB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59OsVinculada',fld:'vOSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV98StatusDemandaVnc',fld:'vSTATUSDEMANDAVNC',pic:'',nv:''},{av:'AV58PFL',fld:'vPFL',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'AV57PFB',fld:'vPFB',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'AV64NewPFL',fld:'vNEWPFL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV71PFLOF',fld:'vPFLOF',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'AV63NewPFB',fld:'vNEWPFB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV70PFBOF',fld:'vPFBOF',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'AV53WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1135ContratoGestor_UsuarioEhContratante',fld:'CONTRATOGESTOR_USUARIOEHCONTRATANTE',pic:'',nv:false},{av:'AV165Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV65Responsavel_Codigo',fld:'vRESPONSAVEL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1481Contratada_UsaOSistema',fld:'CONTRATADA_USAOSISTEMA',pic:'',nv:false},{av:'A1148LogResponsavel_UsuarioEhContratante',fld:'LOGRESPONSAVEL_USUARIOEHCONTRATANTE',pic:'',nv:false},{av:'AV187ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV110Origem_Sigla',fld:'vORIGEM_SIGLA',pic:'@!',nv:''},{av:'A1013Contrato_PrepostoCod',fld:'CONTRATO_PREPOSTOCOD',pic:'ZZZZZ9',nv:0},{av:'AV22Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV115ContratoOrigem_Codigo',fld:'vCONTRATOORIGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV66ContratadaOrigem_Codigo',fld:'vCONTRATADAORIGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV61UsuarioDaPrestadora',fld:'vUSUARIODAPRESTADORA',pic:'',nv:false},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',nv:''},{av:'AV166UsaOSistema',fld:'vUSAOSISTEMA',pic:'',nv:false},{av:'AV113Ok',fld:'vOK',pic:'',nv:false},{av:'AV69NovaContagem',fld:'vNOVACONTAGEM',pic:'',nv:false},{av:'AV72EmReuniao',fld:'vEMREUNIAO',pic:'',nv:false},{av:'AV68Resultado',fld:'vRESULTADO',pic:'',nv:''},{av:'cmbavUsuario_codigo'},{av:'AV21Selecionada',fld:'vSELECIONADA',pic:'ZZZZZ9',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1604ContagemResultado_CntPrpCod',fld:'CONTAGEMRESULTADO_CNTPRPCOD',pic:'ZZZZZ9',nv:0},{av:'AV188ContratoFS',fld:'vCONTRATOFS',pic:'ZZZZZ9',nv:0},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'A1227ContagemResultado_PrazoInicialDias',fld:'CONTAGEMRESULTADO_PRAZOINICIALDIAS',pic:'ZZZ9',nv:0},{av:'A1237ContagemResultado_PrazoMaisDias',fld:'CONTAGEMRESULTADO_PRAZOMAISDIAS',pic:'ZZZ9',nv:0},{av:'A912ContagemResultado_HoraEntrega',fld:'CONTAGEMRESULTADO_HORAENTREGA',pic:'99:99',nv:''},{av:'A472ContagemResultado_DataEntrega',fld:'CONTAGEMRESULTADO_DATAENTREGA',pic:'',nv:''},{av:'A1351ContagemResultado_DataPrevista',fld:'CONTAGEMRESULTADO_DATAPREVISTA',pic:'99/99/99 99:99',nv:''},{av:'A1611ContagemResultado_PrzTpDias',fld:'CONTAGEMRESULTADO_PRZTPDIAS',pic:'',nv:''},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A684ContagemResultado_PFBFSUltima',fld:'CONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A531ContagemResultado_StatusUltCnt',fld:'CONTAGEMRESULTADO_STATUSULTCNT',pic:'Z9',nv:0},{av:'A798ContagemResultado_PFBFSImp',fld:'CONTAGEMRESULTADO_PFBFSIMP',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1130LogResponsavel_Status',fld:'LOGRESPONSAVEL_STATUS',pic:'',nv:''},{av:'A1234LogResponsavel_NovoStatus',fld:'LOGRESPONSAVEL_NOVOSTATUS',pic:'',nv:''},{av:'A1177LogResponsavel_Prazo',fld:'LOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV29Unidades',fld:'vUNIDADES',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV74AnaliseImplicito',fld:'vANALISEIMPLICITO',pic:'',nv:false},{av:'AV94ExecucaoImplicita',fld:'vEXECUCAOIMPLICITA',pic:'',nv:false},{av:'AV102HoraEntrega',fld:'vHORAENTREGA',pic:'99:99',nv:''},{av:'AV31AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1618ContagemResultado_PrzRsp',fld:'CONTAGEMRESULTADO_PRZRSP',pic:'ZZZ9',nv:0}],oparms:[{av:'AV191Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV16StatusDemanda',fld:'vSTATUSDEMANDA',pic:'',nv:''},{av:'AV5Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{ctrl:'ERRVIEWER',prop:'Displaymode'},{av:'AV15AtribuidoA_Codigo',fld:'vATRIBUIDOA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV21Selecionada',fld:'vSELECIONADA',pic:'ZZZZZ9',nv:0},{av:'AV43NovoStatus',fld:'vNOVOSTATUS',pic:'',nv:''},{av:'AV74AnaliseImplicito',fld:'vANALISEIMPLICITO',pic:'',nv:false},{av:'AV94ExecucaoImplicita',fld:'vEXECUCAOIMPLICITA',pic:'',nv:false},{av:'AV45PrazoEntrega',fld:'vPRAZOENTREGA',pic:'99/99/99 99:99',nv:''},{av:'AV46Deferido',fld:'vDEFERIDO',pic:'',nv:''},{av:'AV39PrazoResposta',fld:'vPRAZORESPOSTA',pic:'',nv:''},{av:'AV50PrazoInicial',fld:'vPRAZOINICIAL',pic:'99/99/99 99:99',nv:''},{av:'AV184OSEntregue',fld:'vOSENTREGUE',pic:'',nv:false},{av:'AV183NaoConformidade_Codigo',fld:'vNAOCONFORMIDADE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV9Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV10Destino_Nome',fld:'vDESTINO_NOME',pic:'@!',nv:''},{av:'AV179Requisitos',fld:'vREQUISITOS',pic:'',nv:null},{av:'AV65Responsavel_Codigo',fld:'vRESPONSAVEL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV113Ok',fld:'vOK',pic:'',nv:false},{av:'AV69NovaContagem',fld:'vNOVACONTAGEM',pic:'',nv:false},{av:'AV72EmReuniao',fld:'vEMREUNIAO',pic:'',nv:false},{av:'AV166UsaOSistema',fld:'vUSAOSISTEMA',pic:'',nv:false},{av:'cmbavUsuario_codigo'},{ctrl:'BTNENTER',prop:'Visible'},{av:'AV68Resultado',fld:'vRESULTADO',pic:'',nv:''},{av:'AV27Prestadora_Codigo',fld:'vPRESTADORA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV22Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV73PrazoInicialDias',fld:'vPRAZOINICIALDIAS',pic:'ZZZ9',nv:0},{av:'AV102HoraEntrega',fld:'vHORAENTREGA',pic:'99:99',nv:''},{av:'AV186PrazoAtual',fld:'vPRAZOATUAL',pic:'99/99/99 99:99',nv:''},{av:'AV18PrazoExecucao',fld:'vPRAZOEXECUCAO',pic:'99/99/99 99:99',nv:''},{av:'AV75PrazoAnalise',fld:'vPRAZOANALISE',pic:'99/99/99 99:99',nv:''},{av:'AV107TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'AV116ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV29Unidades',fld:'vUNIDADES',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV93DiasAnalise',fld:'vDIASANALISE',pic:'ZZZ9',nv:0},{av:'AV51PrazoAnterior',fld:'vPRAZOANTERIOR',pic:'',nv:''},{av:'lblTbjava_Caption',ctrl:'TBJAVA',prop:'Caption'}]}");
         setEventMetadata("VPRAZORESPOSTA.ISVALID","{handler:'E15IZ2',iparms:[{av:'AV50PrazoInicial',fld:'vPRAZOINICIAL',pic:'99/99/99 99:99',nv:''},{av:'AV46Deferido',fld:'vDEFERIDO',pic:'',nv:''},{av:'AV5Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39PrazoResposta',fld:'vPRAZORESPOSTA',pic:'',nv:''}],oparms:[{av:'AV45PrazoEntrega',fld:'vPRAZOENTREGA',pic:'99/99/99 99:99',nv:''}]}");
         setEventMetadata("VSERVICOGRUPO_CODIGO.CLICK","{handler:'E16IZ2',iparms:[{av:'A1446ContratoGestor_ContratadaAreaCod',fld:'CONTRATOGESTOR_CONTRATADAAREACOD',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV53WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A843Contrato_DataFimTA',fld:'CONTRATO_DATAFIMTA',pic:'',nv:''},{av:'A83Contrato_DataVigenciaTermino',fld:'CONTRATO_DATAVIGENCIATERMINO',pic:'',nv:''},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',nv:false},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO',pic:'',nv:false},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1078ContratoGestor_ContratoCod',fld:'CONTRATOGESTOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV177ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A638ContratoServicos_Ativo',fld:'CONTRATOSERVICOS_ATIVO',pic:'',nv:false},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',nv:false},{av:'A1858ContratoServicos_Alias',fld:'CONTRATOSERVICOS_ALIAS',pic:'@!',nv:''},{av:'A75Contrato_AreaTrabalhoCod',fld:'CONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A1825ContratoAuxiliar_UsuarioCod',fld:'CONTRATOAUXILIAR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1824ContratoAuxiliar_ContratoCod',fld:'CONTRATOAUXILIAR_CONTRATOCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV182NewCntSrvCod',fld:'vNEWCNTSRVCOD',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VNEWCNTSRVCOD.CLICK","{handler:'E17IZ2',iparms:[{av:'AV182NewCntSrvCod',fld:'vNEWCNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A41Contratada_PessoaNom',fld:'CONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'A75Contrato_AreaTrabalhoCod',fld:'CONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV53WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A843Contrato_DataFimTA',fld:'CONTRATO_DATAFIMTA',pic:'',nv:''},{av:'A83Contrato_DataVigenciaTermino',fld:'CONTRATO_DATAVIGENCIATERMINO',pic:'',nv:''},{av:'A638ContratoServicos_Ativo',fld:'CONTRATOSERVICOS_ATIVO',pic:'',nv:false},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',nv:false},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO',pic:'',nv:false},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV168CriarOSPara_Codigo',fld:'vCRIAROSPARA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1953Contratada_CntPadrao',fld:'CONTRATADA_CNTPADRAO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A77Contrato_Numero',fld:'CONTRATO_NUMERO',pic:'',nv:''},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV174Contrato',fld:'vCONTRATO',pic:'ZZZZZ9',nv:0},{av:'A1749Artefatos_Codigo',fld:'ARTEFATOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1751Artefatos_Descricao',fld:'ARTEFATOS_DESCRICAO',pic:'@!',nv:''}],oparms:[{av:'AV168CriarOSPara_Codigo',fld:'vCRIAROSPARA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV174Contrato',fld:'vCONTRATO',pic:'ZZZZZ9',nv:0},{av:'AV116ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'lblTextblockcontrato_Visible',ctrl:'TEXTBLOCKCONTRATO',prop:'Visible'},{av:'cmbavContrato'},{av:'AV145Sdt_Artefatos',fld:'vSDT_ARTEFATOS',pic:'',nv:null},{ctrl:'BTNARTEFATOS',prop:'Visible'},{ctrl:'BTNENTER',prop:'Visible'}]}");
         setEventMetadata("'DOARTEFATOS'","{handler:'E18IZ2',iparms:[{av:'AV53WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV182NewCntSrvCod',fld:'vNEWCNTSRVCOD',pic:'ZZZZZ9',nv:0}],oparms:[{ctrl:'BTNENTER',prop:'Visible'}]}");
         setEventMetadata("VCRIAROSPARA_CODIGO.CLICK","{handler:'E19IZ2',iparms:[{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV168CriarOSPara_Codigo',fld:'vCRIAROSPARA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A843Contrato_DataFimTA',fld:'CONTRATO_DATAFIMTA',pic:'',nv:''},{av:'A83Contrato_DataVigenciaTermino',fld:'CONTRATO_DATAVIGENCIATERMINO',pic:'',nv:''},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',nv:false},{av:'A1953Contratada_CntPadrao',fld:'CONTRATADA_CNTPADRAO',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV182NewCntSrvCod',fld:'vNEWCNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A638ContratoServicos_Ativo',fld:'CONTRATOSERVICOS_ATIVO',pic:'',nv:false},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A77Contrato_Numero',fld:'CONTRATO_NUMERO',pic:'',nv:''},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV174Contrato',fld:'vCONTRATO',pic:'ZZZZZ9',nv:0},{av:'A1749Artefatos_Codigo',fld:'ARTEFATOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1751Artefatos_Descricao',fld:'ARTEFATOS_DESCRICAO',pic:'@!',nv:''},{av:'AV53WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],oparms:[{av:'AV174Contrato',fld:'vCONTRATO',pic:'ZZZZZ9',nv:0},{av:'AV116ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'lblTextblockcontrato_Visible',ctrl:'TEXTBLOCKCONTRATO',prop:'Visible'},{av:'cmbavContrato'},{av:'AV145Sdt_Artefatos',fld:'vSDT_ARTEFATOS',pic:'',nv:null},{ctrl:'BTNARTEFATOS',prop:'Visible'},{ctrl:'BTNENTER',prop:'Visible'}]}");
         setEventMetadata("VCONTRATO.CLICK","{handler:'E20IZ2',iparms:[{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV174Contrato',fld:'vCONTRATO',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV182NewCntSrvCod',fld:'vNEWCNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1749Artefatos_Codigo',fld:'ARTEFATOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1751Artefatos_Descricao',fld:'ARTEFATOS_DESCRICAO',pic:'@!',nv:''},{av:'AV53WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],oparms:[{av:'AV145Sdt_Artefatos',fld:'vSDT_ARTEFATOS',pic:'',nv:null},{av:'AV116ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{ctrl:'BTNARTEFATOS',prop:'Visible'},{ctrl:'BTNENTER',prop:'Visible'}]}");
         setEventMetadata("'DOFECHAR'","{handler:'E21IZ2',iparms:[{av:'AV10Destino_Nome',fld:'vDESTINO_NOME',pic:'@!',nv:''}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV10Destino_Nome = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV53WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV191Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV145Sdt_Artefatos = new GxObjectCollection( context, "SDT_Artefatos", "GxEv3Up14_Meetrika", "SdtSDT_Artefatos", "GeneXus.Programs");
         AV16StatusDemanda = "";
         AV114Caller = "";
         AV20Selecionadas = new GxSimpleCollection();
         AV110Origem_Sigla = "";
         AV50PrazoInicial = (DateTime)(DateTime.MinValue);
         AV107TipoDias = "";
         A158ServicoGrupo_Descricao = "";
         A1858ContratoServicos_Alias = "";
         AV179Requisitos = new GxSimpleCollection();
         AV45PrazoEntrega = (DateTime)(DateTime.MinValue);
         AV43NovoStatus = "";
         AV51PrazoAnterior = DateTime.MinValue;
         AV18PrazoExecucao = (DateTime)(DateTime.MinValue);
         AV186PrazoAtual = (DateTime)(DateTime.MinValue);
         AV75PrazoAnalise = (DateTime)(DateTime.MinValue);
         AV98StatusDemandaVnc = "";
         A894LogResponsavel_Acao = "";
         AV68Resultado = "";
         A484ContagemResultado_StatusDmn = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         A1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         A1611ContagemResultado_PrzTpDias = "";
         A1130LogResponsavel_Status = "";
         A1234LogResponsavel_NovoStatus = "";
         A1177LogResponsavel_Prazo = (DateTime)(DateTime.MinValue);
         AV102HoraEntrega = (DateTime)(DateTime.MinValue);
         A41Contratada_PessoaNom = "";
         A77Contrato_Numero = "";
         A1751Artefatos_Descricao = "";
         A83Contrato_DataVigenciaTermino = DateTime.MinValue;
         A843Contrato_DataFimTA = DateTime.MinValue;
         A1869Contrato_DataTermino = DateTime.MinValue;
         AV140Descricao = "";
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         sPrefix = "";
         lblTbjava_Jsonclick = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         WebComp_Webcomp2_Component = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00IZ4_A74Contrato_Codigo = new int[1] ;
         H00IZ4_n74Contrato_Codigo = new bool[] {false} ;
         H00IZ4_A77Contrato_Numero = new String[] {""} ;
         H00IZ4_A39Contratada_Codigo = new int[1] ;
         H00IZ4_n39Contratada_Codigo = new bool[] {false} ;
         H00IZ4_A92Contrato_Ativo = new bool[] {false} ;
         H00IZ4_n92Contrato_Ativo = new bool[] {false} ;
         H00IZ4_A1869Contrato_DataTermino = new DateTime[] {DateTime.MinValue} ;
         H00IZ5_A426NaoConformidade_Codigo = new int[1] ;
         H00IZ5_A427NaoConformidade_Nome = new String[] {""} ;
         H00IZ5_A429NaoConformidade_Tipo = new short[1] ;
         H00IZ5_n429NaoConformidade_Tipo = new bool[] {false} ;
         H00IZ5_A428NaoConformidade_AreaTrabalhoCod = new int[1] ;
         H00IZ5_n428NaoConformidade_AreaTrabalhoCod = new bool[] {false} ;
         AV39PrazoResposta = DateTime.MinValue;
         AV6Observacao = "";
         AV46Deferido = "";
         hsh = "";
         AV67Contratada_Sigla = "";
         AV30WebSession = context.GetSession();
         H00IZ7_A146Modulo_Codigo = new int[1] ;
         H00IZ7_n146Modulo_Codigo = new bool[] {false} ;
         H00IZ7_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         H00IZ7_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         H00IZ7_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00IZ7_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00IZ7_A456ContagemResultado_Codigo = new int[1] ;
         H00IZ7_A127Sistema_Codigo = new int[1] ;
         H00IZ7_A489ContagemResultado_SistemaCod = new int[1] ;
         H00IZ7_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         H00IZ7_A1831Sistema_Responsavel = new int[1] ;
         H00IZ7_n1831Sistema_Responsavel = new bool[] {false} ;
         H00IZ7_A428NaoConformidade_AreaTrabalhoCod = new int[1] ;
         H00IZ7_n428NaoConformidade_AreaTrabalhoCod = new bool[] {false} ;
         H00IZ7_A429NaoConformidade_Tipo = new short[1] ;
         H00IZ7_n429NaoConformidade_Tipo = new bool[] {false} ;
         H00IZ7_A92Contrato_Ativo = new bool[] {false} ;
         H00IZ7_n92Contrato_Ativo = new bool[] {false} ;
         H00IZ7_A457ContagemResultado_Demanda = new String[] {""} ;
         H00IZ7_n457ContagemResultado_Demanda = new bool[] {false} ;
         H00IZ7_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00IZ7_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00IZ7_A1593ContagemResultado_CntSrvTpVnc = new String[] {""} ;
         H00IZ7_n1593ContagemResultado_CntSrvTpVnc = new bool[] {false} ;
         H00IZ7_A602ContagemResultado_OSVinculada = new int[1] ;
         H00IZ7_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         H00IZ7_A890ContagemResultado_Responsavel = new int[1] ;
         H00IZ7_n890ContagemResultado_Responsavel = new bool[] {false} ;
         H00IZ7_A601ContagemResultado_Servico = new int[1] ;
         H00IZ7_n601ContagemResultado_Servico = new bool[] {false} ;
         H00IZ7_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         H00IZ7_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         H00IZ7_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00IZ7_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00IZ7_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         H00IZ7_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         H00IZ7_A1627ContagemResultado_CntSrvVncCod = new int[1] ;
         H00IZ7_n1627ContagemResultado_CntSrvVncCod = new bool[] {false} ;
         H00IZ7_A1622ContagemResultado_CntVncCod = new int[1] ;
         H00IZ7_n1622ContagemResultado_CntVncCod = new bool[] {false} ;
         H00IZ7_A866ContagemResultado_ContratadaOrigemSigla = new String[] {""} ;
         H00IZ7_n866ContagemResultado_ContratadaOrigemSigla = new bool[] {false} ;
         H00IZ7_A1326ContagemResultado_ContratadaTipoFab = new String[] {""} ;
         H00IZ7_n1326ContagemResultado_ContratadaTipoFab = new bool[] {false} ;
         H00IZ7_A1611ContagemResultado_PrzTpDias = new String[] {""} ;
         H00IZ7_n1611ContagemResultado_PrzTpDias = new bool[] {false} ;
         H00IZ7_A1636ContagemResultado_ServicoSS = new int[1] ;
         H00IZ7_n1636ContagemResultado_ServicoSS = new bool[] {false} ;
         H00IZ7_A1452ContagemResultado_SS = new int[1] ;
         H00IZ7_n1452ContagemResultado_SS = new bool[] {false} ;
         H00IZ7_A1603ContagemResultado_CntCod = new int[1] ;
         H00IZ7_n1603ContagemResultado_CntCod = new bool[] {false} ;
         H00IZ7_A508ContagemResultado_Owner = new int[1] ;
         H00IZ7_A494ContagemResultado_Descricao = new String[] {""} ;
         H00IZ7_n494ContagemResultado_Descricao = new bool[] {false} ;
         H00IZ7_A531ContagemResultado_StatusUltCnt = new short[1] ;
         H00IZ7_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         H00IZ7_A685ContagemResultado_PFLFSUltima = new decimal[1] ;
         H00IZ7_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         H00IZ7_A683ContagemResultado_PFLFMUltima = new decimal[1] ;
         H00IZ7_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         H00IZ7_n83Contrato_DataVigenciaTermino = new bool[] {false} ;
         H00IZ7_A1829Sistema_GpoObjCtrlCod = new int[1] ;
         H00IZ7_n1829Sistema_GpoObjCtrlCod = new bool[] {false} ;
         A457ContagemResultado_Demanda = "";
         A1593ContagemResultado_CntSrvTpVnc = "";
         A803ContagemResultado_ContratadaSigla = "";
         A866ContagemResultado_ContratadaOrigemSigla = "";
         A1326ContagemResultado_ContratadaTipoFab = "";
         A494ContagemResultado_Descricao = "";
         AV76Demanda = "";
         AV142Prestadora_Sigla = "";
         AV77TipoFabrica = "S";
         AV123ContagemResultado_SS = 0;
         H00IZ8_A127Sistema_Codigo = new int[1] ;
         H00IZ9_A1219LogResponsavel_OwnerPessoaCod = new int[1] ;
         H00IZ9_n1219LogResponsavel_OwnerPessoaCod = new bool[] {false} ;
         H00IZ9_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00IZ9_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00IZ9_A1603ContagemResultado_CntCod = new int[1] ;
         H00IZ9_n1603ContagemResultado_CntCod = new bool[] {false} ;
         H00IZ9_A39Contratada_Codigo = new int[1] ;
         H00IZ9_n39Contratada_Codigo = new bool[] {false} ;
         H00IZ9_A1222LogResponsavel_OwnerPessoaNom = new String[] {""} ;
         H00IZ9_n1222LogResponsavel_OwnerPessoaNom = new bool[] {false} ;
         H00IZ9_A1797LogResponsavel_Codigo = new long[1] ;
         H00IZ9_A896LogResponsavel_Owner = new int[1] ;
         H00IZ9_A892LogResponsavel_DemandaCod = new int[1] ;
         H00IZ9_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         A1222LogResponsavel_OwnerPessoaNom = "";
         AV149Remetente_Nome = "";
         H00IZ10_AV150i = new short[1] ;
         H00IZ11_AV103QtdNaoAcatado = new short[1] ;
         H00IZ12_A761ContagemResultadoChckLst_Codigo = new short[1] ;
         H00IZ12_A758CheckList_Codigo = new int[1] ;
         H00IZ12_A1230CheckList_De = new short[1] ;
         H00IZ12_n1230CheckList_De = new bool[] {false} ;
         H00IZ12_A762ContagemResultadoChckLst_Cumpre = new String[] {""} ;
         H00IZ12_A1868ContagemResultadoChckLst_OSCod = new int[1] ;
         A762ContagemResultadoChckLst_Cumpre = "";
         H00IZ13_A892LogResponsavel_DemandaCod = new int[1] ;
         H00IZ13_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         H00IZ13_A1177LogResponsavel_Prazo = new DateTime[] {DateTime.MinValue} ;
         H00IZ13_n1177LogResponsavel_Prazo = new bool[] {false} ;
         H00IZ13_A896LogResponsavel_Owner = new int[1] ;
         H00IZ13_A1797LogResponsavel_Codigo = new long[1] ;
         H00IZ14_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         H00IZ14_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00IZ14_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00IZ14_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00IZ14_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00IZ14_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         H00IZ14_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         H00IZ14_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         H00IZ14_A1908Usuario_DeFerias = new bool[] {false} ;
         H00IZ14_n1908Usuario_DeFerias = new bool[] {false} ;
         A71ContratadaUsuario_UsuarioPessoaNom = "";
         H00IZ15_A828UsuarioServicos_UsuarioCod = new int[1] ;
         H00IZ15_A829UsuarioServicos_ServicoCod = new int[1] ;
         GXt_char1 = "";
         H00IZ16_A192Contagem_Codigo = new int[1] ;
         H00IZ16_A1118Contagem_ContratadaCod = new int[1] ;
         H00IZ16_n1118Contagem_ContratadaCod = new bool[] {false} ;
         H00IZ16_A945Contagem_Demanda = new String[] {""} ;
         H00IZ16_n945Contagem_Demanda = new bool[] {false} ;
         A945Contagem_Demanda = "";
         H00IZ17_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         H00IZ17_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00IZ17_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00IZ17_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00IZ17_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         H00IZ17_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         H00IZ17_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         H00IZ17_A1908Usuario_DeFerias = new bool[] {false} ;
         H00IZ17_n1908Usuario_DeFerias = new bool[] {false} ;
         H00IZ17_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00IZ18_A828UsuarioServicos_UsuarioCod = new int[1] ;
         H00IZ18_A829UsuarioServicos_ServicoCod = new int[1] ;
         AV129Owner_Nome = "";
         GXt_char4 = "";
         GXt_char3 = "";
         GXt_char2 = "";
         H00IZ19_A155Servico_Codigo = new int[1] ;
         H00IZ20_A57Usuario_PessoaCod = new int[1] ;
         H00IZ20_A1Usuario_Codigo = new int[1] ;
         H00IZ20_A58Usuario_PessoaNom = new String[] {""} ;
         H00IZ20_n58Usuario_PessoaNom = new bool[] {false} ;
         A58Usuario_PessoaNom = "";
         H00IZ21_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00IZ21_A1136ContratoGestor_ContratadaCod = new int[1] ;
         H00IZ21_n1136ContratoGestor_ContratadaCod = new bool[] {false} ;
         H00IZ21_A1223ContratoGestor_ContratadaSigla = new String[] {""} ;
         H00IZ21_n1223ContratoGestor_ContratadaSigla = new bool[] {false} ;
         A1223ContratoGestor_ContratadaSigla = "";
         H00IZ22_A61ContratanteUsuario_UsuarioPessoaCod = new int[1] ;
         H00IZ22_n61ContratanteUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00IZ22_A54Usuario_Ativo = new bool[] {false} ;
         H00IZ22_n54Usuario_Ativo = new bool[] {false} ;
         H00IZ22_A1908Usuario_DeFerias = new bool[] {false} ;
         H00IZ22_n1908Usuario_DeFerias = new bool[] {false} ;
         H00IZ22_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         H00IZ22_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         H00IZ22_A62ContratanteUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00IZ22_n62ContratanteUsuario_UsuarioPessoaNom = new bool[] {false} ;
         A62ContratanteUsuario_UsuarioPessoaNom = "";
         H00IZ23_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00IZ23_A1136ContratoGestor_ContratadaCod = new int[1] ;
         H00IZ23_n1136ContratoGestor_ContratadaCod = new bool[] {false} ;
         H00IZ23_A1447ContratoGestor_ContratadaAreaDes = new String[] {""} ;
         H00IZ23_n1447ContratoGestor_ContratadaAreaDes = new bool[] {false} ;
         H00IZ23_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H00IZ23_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         H00IZ23_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         A1447ContratoGestor_ContratadaAreaDes = "";
         H00IZ24_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         H00IZ24_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00IZ24_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00IZ24_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00IZ24_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         H00IZ24_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         H00IZ24_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         H00IZ24_A1908Usuario_DeFerias = new bool[] {false} ;
         H00IZ24_n1908Usuario_DeFerias = new bool[] {false} ;
         H00IZ24_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00IZ25_A828UsuarioServicos_UsuarioCod = new int[1] ;
         H00IZ25_A829UsuarioServicos_ServicoCod = new int[1] ;
         H00IZ26_A891LogResponsavel_UsuarioCod = new int[1] ;
         H00IZ26_n891LogResponsavel_UsuarioCod = new bool[] {false} ;
         H00IZ26_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         H00IZ26_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         H00IZ26_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00IZ26_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00IZ26_A1603ContagemResultado_CntCod = new int[1] ;
         H00IZ26_n1603ContagemResultado_CntCod = new bool[] {false} ;
         H00IZ26_A1908Usuario_DeFerias = new bool[] {false} ;
         H00IZ26_n1908Usuario_DeFerias = new bool[] {false} ;
         H00IZ26_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00IZ26_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00IZ26_A39Contratada_Codigo = new int[1] ;
         H00IZ26_n39Contratada_Codigo = new bool[] {false} ;
         H00IZ26_A428NaoConformidade_AreaTrabalhoCod = new int[1] ;
         H00IZ26_n428NaoConformidade_AreaTrabalhoCod = new bool[] {false} ;
         H00IZ26_A429NaoConformidade_Tipo = new short[1] ;
         H00IZ26_n429NaoConformidade_Tipo = new bool[] {false} ;
         H00IZ26_A92Contrato_Ativo = new bool[] {false} ;
         H00IZ26_n92Contrato_Ativo = new bool[] {false} ;
         H00IZ26_A1797LogResponsavel_Codigo = new long[1] ;
         H00IZ26_A896LogResponsavel_Owner = new int[1] ;
         H00IZ26_A892LogResponsavel_DemandaCod = new int[1] ;
         H00IZ26_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         H00IZ26_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         H00IZ26_n83Contrato_DataVigenciaTermino = new bool[] {false} ;
         H00IZ27_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00IZ27_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00IZ28_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00IZ28_A1079ContratoGestor_UsuarioCod = new int[1] ;
         AV147Observ = "";
         AV180PrazoPrevisto = (DateTime)(DateTime.MinValue);
         AV17Acao = "";
         GXt_dtime16 = (DateTime)(DateTime.MinValue);
         AV92dateTime = (DateTime)(DateTime.MinValue);
         H00IZ31_A456ContagemResultado_Codigo = new int[1] ;
         H00IZ31_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00IZ31_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00IZ31_A601ContagemResultado_Servico = new int[1] ;
         H00IZ31_n601ContagemResultado_Servico = new bool[] {false} ;
         H00IZ31_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00IZ31_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00IZ31_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         H00IZ31_A1237ContagemResultado_PrazoMaisDias = new short[1] ;
         H00IZ31_n1237ContagemResultado_PrazoMaisDias = new bool[] {false} ;
         H00IZ31_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         H00IZ31_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         H00IZ31_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         H00IZ31_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         H00IZ31_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         H00IZ31_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         H00IZ31_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         H00IZ31_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         H00IZ31_A1611ContagemResultado_PrzTpDias = new String[] {""} ;
         H00IZ31_n1611ContagemResultado_PrzTpDias = new bool[] {false} ;
         H00IZ31_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00IZ31_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00IZ31_A798ContagemResultado_PFBFSImp = new decimal[1] ;
         H00IZ31_n798ContagemResultado_PFBFSImp = new bool[] {false} ;
         H00IZ31_A531ContagemResultado_StatusUltCnt = new short[1] ;
         H00IZ31_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         H00IZ31_A40000ContagemResultado_ContadorFMCod = new int[1] ;
         H00IZ31_n40000ContagemResultado_ContadorFMCod = new bool[] {false} ;
         AV44DataSolicitacao = (DateTime)(DateTime.MinValue);
         GXt_char10 = "";
         GXt_char9 = "";
         GXt_char8 = "";
         GXt_char7 = "";
         GXt_char6 = "";
         GXt_char5 = "";
         H00IZ32_A1130LogResponsavel_Status = new String[] {""} ;
         H00IZ32_n1130LogResponsavel_Status = new bool[] {false} ;
         H00IZ32_A894LogResponsavel_Acao = new String[] {""} ;
         H00IZ32_A1797LogResponsavel_Codigo = new long[1] ;
         H00IZ32_A891LogResponsavel_UsuarioCod = new int[1] ;
         H00IZ32_n891LogResponsavel_UsuarioCod = new bool[] {false} ;
         H00IZ32_A892LogResponsavel_DemandaCod = new int[1] ;
         H00IZ32_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         H00IZ33_A1234LogResponsavel_NovoStatus = new String[] {""} ;
         H00IZ33_n1234LogResponsavel_NovoStatus = new bool[] {false} ;
         H00IZ33_A892LogResponsavel_DemandaCod = new int[1] ;
         H00IZ33_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         H00IZ33_A1177LogResponsavel_Prazo = new DateTime[] {DateTime.MinValue} ;
         H00IZ33_n1177LogResponsavel_Prazo = new bool[] {false} ;
         H00IZ33_A1797LogResponsavel_Codigo = new long[1] ;
         H00IZ34_A894LogResponsavel_Acao = new String[] {""} ;
         H00IZ34_A1177LogResponsavel_Prazo = new DateTime[] {DateTime.MinValue} ;
         H00IZ34_n1177LogResponsavel_Prazo = new bool[] {false} ;
         H00IZ34_A1797LogResponsavel_Codigo = new long[1] ;
         H00IZ34_A896LogResponsavel_Owner = new int[1] ;
         H00IZ34_A891LogResponsavel_UsuarioCod = new int[1] ;
         H00IZ34_n891LogResponsavel_UsuarioCod = new bool[] {false} ;
         H00IZ34_A892LogResponsavel_DemandaCod = new int[1] ;
         H00IZ34_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         H00IZ35_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00IZ35_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00IZ35_A456ContagemResultado_Codigo = new int[1] ;
         H00IZ35_A1618ContagemResultado_PrzRsp = new short[1] ;
         H00IZ35_n1618ContagemResultado_PrzRsp = new bool[] {false} ;
         H00IZ35_A1611ContagemResultado_PrzTpDias = new String[] {""} ;
         H00IZ35_n1611ContagemResultado_PrzTpDias = new bool[] {false} ;
         GXt_dtime17 = (DateTime)(DateTime.MinValue);
         GXt_char14 = "";
         H00IZ36_A1797LogResponsavel_Codigo = new long[1] ;
         H00IZ36_A896LogResponsavel_Owner = new int[1] ;
         H00IZ36_A892LogResponsavel_DemandaCod = new int[1] ;
         H00IZ36_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         H00IZ37_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         H00IZ37_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         H00IZ37_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00IZ37_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00IZ37_A1603ContagemResultado_CntCod = new int[1] ;
         H00IZ37_n1603ContagemResultado_CntCod = new bool[] {false} ;
         H00IZ37_A39Contratada_Codigo = new int[1] ;
         H00IZ37_n39Contratada_Codigo = new bool[] {false} ;
         H00IZ37_A428NaoConformidade_AreaTrabalhoCod = new int[1] ;
         H00IZ37_n428NaoConformidade_AreaTrabalhoCod = new bool[] {false} ;
         H00IZ37_A429NaoConformidade_Tipo = new short[1] ;
         H00IZ37_n429NaoConformidade_Tipo = new bool[] {false} ;
         H00IZ37_A92Contrato_Ativo = new bool[] {false} ;
         H00IZ37_n92Contrato_Ativo = new bool[] {false} ;
         H00IZ37_A456ContagemResultado_Codigo = new int[1] ;
         H00IZ37_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         H00IZ37_n83Contrato_DataVigenciaTermino = new bool[] {false} ;
         H00IZ38_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00IZ38_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H00IZ38_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         H00IZ38_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         H00IZ39_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         H00IZ39_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         H00IZ39_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00IZ39_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00IZ39_A1603ContagemResultado_CntCod = new int[1] ;
         H00IZ39_n1603ContagemResultado_CntCod = new bool[] {false} ;
         H00IZ39_A39Contratada_Codigo = new int[1] ;
         H00IZ39_n39Contratada_Codigo = new bool[] {false} ;
         H00IZ39_A428NaoConformidade_AreaTrabalhoCod = new int[1] ;
         H00IZ39_n428NaoConformidade_AreaTrabalhoCod = new bool[] {false} ;
         H00IZ39_A429NaoConformidade_Tipo = new short[1] ;
         H00IZ39_n429NaoConformidade_Tipo = new bool[] {false} ;
         H00IZ39_A92Contrato_Ativo = new bool[] {false} ;
         H00IZ39_n92Contrato_Ativo = new bool[] {false} ;
         H00IZ39_A456ContagemResultado_Codigo = new int[1] ;
         H00IZ39_A1604ContagemResultado_CntPrpCod = new int[1] ;
         H00IZ39_n1604ContagemResultado_CntPrpCod = new bool[] {false} ;
         H00IZ39_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         H00IZ39_n83Contrato_DataVigenciaTermino = new bool[] {false} ;
         H00IZ40_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00IZ40_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H00IZ40_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         H00IZ40_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         H00IZ43_A74Contrato_Codigo = new int[1] ;
         H00IZ43_n74Contrato_Codigo = new bool[] {false} ;
         H00IZ43_A39Contratada_Codigo = new int[1] ;
         H00IZ43_n39Contratada_Codigo = new bool[] {false} ;
         H00IZ43_A92Contrato_Ativo = new bool[] {false} ;
         H00IZ43_n92Contrato_Ativo = new bool[] {false} ;
         H00IZ43_A1013Contrato_PrepostoCod = new int[1] ;
         H00IZ43_n1013Contrato_PrepostoCod = new bool[] {false} ;
         H00IZ43_A843Contrato_DataFimTA = new DateTime[] {DateTime.MinValue} ;
         H00IZ43_n843Contrato_DataFimTA = new bool[] {false} ;
         H00IZ43_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         H00IZ43_n83Contrato_DataVigenciaTermino = new bool[] {false} ;
         H00IZ44_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00IZ44_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H00IZ44_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         H00IZ44_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         H00IZ46_A1636ContagemResultado_ServicoSS = new int[1] ;
         H00IZ46_n1636ContagemResultado_ServicoSS = new bool[] {false} ;
         H00IZ46_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00IZ46_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00IZ46_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00IZ46_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00IZ46_A456ContagemResultado_Codigo = new int[1] ;
         H00IZ46_A1326ContagemResultado_ContratadaTipoFab = new String[] {""} ;
         H00IZ46_n1326ContagemResultado_ContratadaTipoFab = new bool[] {false} ;
         H00IZ46_A531ContagemResultado_StatusUltCnt = new short[1] ;
         H00IZ46_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         H00IZ46_A683ContagemResultado_PFLFMUltima = new decimal[1] ;
         H00IZ46_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         H00IZ46_A685ContagemResultado_PFLFSUltima = new decimal[1] ;
         H00IZ48_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00IZ48_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00IZ48_A1636ContagemResultado_ServicoSS = new int[1] ;
         H00IZ48_n1636ContagemResultado_ServicoSS = new bool[] {false} ;
         H00IZ48_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00IZ48_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00IZ48_A1593ContagemResultado_CntSrvTpVnc = new String[] {""} ;
         H00IZ48_n1593ContagemResultado_CntSrvTpVnc = new bool[] {false} ;
         H00IZ48_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00IZ48_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00IZ48_A602ContagemResultado_OSVinculada = new int[1] ;
         H00IZ48_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         H00IZ48_A1603ContagemResultado_CntCod = new int[1] ;
         H00IZ48_n1603ContagemResultado_CntCod = new bool[] {false} ;
         H00IZ48_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         H00IZ48_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         H00IZ48_A456ContagemResultado_Codigo = new int[1] ;
         H00IZ48_A1326ContagemResultado_ContratadaTipoFab = new String[] {""} ;
         H00IZ48_n1326ContagemResultado_ContratadaTipoFab = new bool[] {false} ;
         H00IZ48_A531ContagemResultado_StatusUltCnt = new short[1] ;
         H00IZ48_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         H00IZ48_A683ContagemResultado_PFLFMUltima = new decimal[1] ;
         H00IZ48_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         H00IZ48_A685ContagemResultado_PFLFSUltima = new decimal[1] ;
         AV87Sdt_ContagemResultadoEvidencias = new GxObjectCollection( context, "SDT_ContagemResultadoEvidencias.Arquivo", "GxEv3Up14_Meetrika", "SdtSDT_ContagemResultadoEvidencias_Arquivo", "GeneXus.Programs");
         H00IZ49_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         H00IZ49_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         H00IZ49_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00IZ49_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00IZ49_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00IZ49_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00IZ49_A1603ContagemResultado_CntCod = new int[1] ;
         H00IZ49_n1603ContagemResultado_CntCod = new bool[] {false} ;
         H00IZ49_A1481Contratada_UsaOSistema = new bool[] {false} ;
         H00IZ49_n1481Contratada_UsaOSistema = new bool[] {false} ;
         H00IZ49_A39Contratada_Codigo = new int[1] ;
         H00IZ49_n39Contratada_Codigo = new bool[] {false} ;
         H00IZ49_A428NaoConformidade_AreaTrabalhoCod = new int[1] ;
         H00IZ49_n428NaoConformidade_AreaTrabalhoCod = new bool[] {false} ;
         H00IZ49_A429NaoConformidade_Tipo = new short[1] ;
         H00IZ49_n429NaoConformidade_Tipo = new bool[] {false} ;
         H00IZ49_A92Contrato_Ativo = new bool[] {false} ;
         H00IZ49_n92Contrato_Ativo = new bool[] {false} ;
         H00IZ49_A1797LogResponsavel_Codigo = new long[1] ;
         H00IZ49_A896LogResponsavel_Owner = new int[1] ;
         H00IZ49_A891LogResponsavel_UsuarioCod = new int[1] ;
         H00IZ49_n891LogResponsavel_UsuarioCod = new bool[] {false} ;
         H00IZ49_A892LogResponsavel_DemandaCod = new int[1] ;
         H00IZ49_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         H00IZ49_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         H00IZ49_n83Contrato_DataVigenciaTermino = new bool[] {false} ;
         H00IZ50_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00IZ50_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00IZ51_A1481Contratada_UsaOSistema = new bool[] {false} ;
         H00IZ51_n1481Contratada_UsaOSistema = new bool[] {false} ;
         H00IZ51_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00IZ51_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00IZ52_A1797LogResponsavel_Codigo = new long[1] ;
         H00IZ52_A896LogResponsavel_Owner = new int[1] ;
         H00IZ52_A892LogResponsavel_DemandaCod = new int[1] ;
         H00IZ52_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         H00IZ53_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00IZ53_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H00IZ53_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         H00IZ53_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         H00IZ54_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         H00IZ54_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         H00IZ54_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00IZ54_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00IZ54_A1603ContagemResultado_CntCod = new int[1] ;
         H00IZ54_n1603ContagemResultado_CntCod = new bool[] {false} ;
         H00IZ54_A39Contratada_Codigo = new int[1] ;
         H00IZ54_n39Contratada_Codigo = new bool[] {false} ;
         H00IZ54_A428NaoConformidade_AreaTrabalhoCod = new int[1] ;
         H00IZ54_n428NaoConformidade_AreaTrabalhoCod = new bool[] {false} ;
         H00IZ54_A429NaoConformidade_Tipo = new short[1] ;
         H00IZ54_n429NaoConformidade_Tipo = new bool[] {false} ;
         H00IZ54_A92Contrato_Ativo = new bool[] {false} ;
         H00IZ54_n92Contrato_Ativo = new bool[] {false} ;
         H00IZ54_A894LogResponsavel_Acao = new String[] {""} ;
         H00IZ54_A1797LogResponsavel_Codigo = new long[1] ;
         H00IZ54_A896LogResponsavel_Owner = new int[1] ;
         H00IZ54_A892LogResponsavel_DemandaCod = new int[1] ;
         H00IZ54_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         H00IZ54_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         H00IZ54_n83Contrato_DataVigenciaTermino = new bool[] {false} ;
         H00IZ55_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00IZ55_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00IZ58_A160ContratoServicos_Codigo = new int[1] ;
         H00IZ58_A74Contrato_Codigo = new int[1] ;
         H00IZ58_n74Contrato_Codigo = new bool[] {false} ;
         H00IZ58_A1013Contrato_PrepostoCod = new int[1] ;
         H00IZ58_n1013Contrato_PrepostoCod = new bool[] {false} ;
         H00IZ58_A39Contratada_Codigo = new int[1] ;
         H00IZ58_n39Contratada_Codigo = new bool[] {false} ;
         H00IZ58_A92Contrato_Ativo = new bool[] {false} ;
         H00IZ58_n92Contrato_Ativo = new bool[] {false} ;
         H00IZ58_A155Servico_Codigo = new int[1] ;
         H00IZ58_A843Contrato_DataFimTA = new DateTime[] {DateTime.MinValue} ;
         H00IZ58_n843Contrato_DataFimTA = new bool[] {false} ;
         H00IZ58_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         H00IZ58_n83Contrato_DataVigenciaTermino = new bool[] {false} ;
         H00IZ59_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00IZ59_A1079ContratoGestor_UsuarioCod = new int[1] ;
         AV172Servicos = new GxSimpleCollection();
         AV171SDT_Servicos = new GxObjectCollection( context, "SDT_Codigos", "GxEv3Up14_Meetrika", "SdtSDT_Codigos", "GeneXus.Programs");
         H00IZ60_A1136ContratoGestor_ContratadaCod = new int[1] ;
         H00IZ60_n1136ContratoGestor_ContratadaCod = new bool[] {false} ;
         H00IZ60_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00IZ60_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00IZ60_A1595Contratada_AreaTrbSrvPdr = new int[1] ;
         H00IZ60_n1595Contratada_AreaTrbSrvPdr = new bool[] {false} ;
         H00IZ60_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         H00IZ60_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         H00IZ60_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H00IZ60_A632Servico_Ativo = new bool[] {false} ;
         H00IZ60_n632Servico_Ativo = new bool[] {false} ;
         H00IZ60_A157ServicoGrupo_Codigo = new int[1] ;
         H00IZ60_n157ServicoGrupo_Codigo = new bool[] {false} ;
         H00IZ60_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00IZ60_A158ServicoGrupo_Descricao = new String[] {""} ;
         H00IZ60_A92Contrato_Ativo = new bool[] {false} ;
         H00IZ60_n92Contrato_Ativo = new bool[] {false} ;
         H00IZ60_A43Contratada_Ativo = new bool[] {false} ;
         H00IZ60_n43Contratada_Ativo = new bool[] {false} ;
         H00IZ60_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         H00IZ60_n83Contrato_DataVigenciaTermino = new bool[] {false} ;
         H00IZ61_A160ContratoServicos_Codigo = new int[1] ;
         H00IZ61_A74Contrato_Codigo = new int[1] ;
         H00IZ61_n74Contrato_Codigo = new bool[] {false} ;
         H00IZ61_A638ContratoServicos_Ativo = new bool[] {false} ;
         AV170SDT_Servico = new SdtSDT_Codigos(context);
         H00IZ62_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00IZ62_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00IZ62_A1595Contratada_AreaTrbSrvPdr = new int[1] ;
         H00IZ62_n1595Contratada_AreaTrbSrvPdr = new bool[] {false} ;
         H00IZ62_A75Contrato_AreaTrabalhoCod = new int[1] ;
         H00IZ62_n75Contrato_AreaTrabalhoCod = new bool[] {false} ;
         H00IZ62_A1825ContratoAuxiliar_UsuarioCod = new int[1] ;
         H00IZ62_A632Servico_Ativo = new bool[] {false} ;
         H00IZ62_n632Servico_Ativo = new bool[] {false} ;
         H00IZ62_A157ServicoGrupo_Codigo = new int[1] ;
         H00IZ62_n157ServicoGrupo_Codigo = new bool[] {false} ;
         H00IZ62_A1824ContratoAuxiliar_ContratoCod = new int[1] ;
         H00IZ62_A158ServicoGrupo_Descricao = new String[] {""} ;
         H00IZ62_A39Contratada_Codigo = new int[1] ;
         H00IZ62_n39Contratada_Codigo = new bool[] {false} ;
         H00IZ62_A92Contrato_Ativo = new bool[] {false} ;
         H00IZ62_n92Contrato_Ativo = new bool[] {false} ;
         H00IZ62_A43Contratada_Ativo = new bool[] {false} ;
         H00IZ62_n43Contratada_Ativo = new bool[] {false} ;
         H00IZ62_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         H00IZ62_n83Contrato_DataVigenciaTermino = new bool[] {false} ;
         H00IZ63_A160ContratoServicos_Codigo = new int[1] ;
         H00IZ63_A74Contrato_Codigo = new int[1] ;
         H00IZ63_n74Contrato_Codigo = new bool[] {false} ;
         H00IZ63_A638ContratoServicos_Ativo = new bool[] {false} ;
         H00IZ63_A155Servico_Codigo = new int[1] ;
         H00IZ64_A1136ContratoGestor_ContratadaCod = new int[1] ;
         H00IZ64_n1136ContratoGestor_ContratadaCod = new bool[] {false} ;
         H00IZ64_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00IZ64_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00IZ64_A1595Contratada_AreaTrbSrvPdr = new int[1] ;
         H00IZ64_n1595Contratada_AreaTrbSrvPdr = new bool[] {false} ;
         H00IZ64_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         H00IZ64_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         H00IZ64_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H00IZ64_A632Servico_Ativo = new bool[] {false} ;
         H00IZ64_n632Servico_Ativo = new bool[] {false} ;
         H00IZ64_A157ServicoGrupo_Codigo = new int[1] ;
         H00IZ64_n157ServicoGrupo_Codigo = new bool[] {false} ;
         H00IZ64_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00IZ64_A92Contrato_Ativo = new bool[] {false} ;
         H00IZ64_n92Contrato_Ativo = new bool[] {false} ;
         H00IZ64_A43Contratada_Ativo = new bool[] {false} ;
         H00IZ64_n43Contratada_Ativo = new bool[] {false} ;
         H00IZ64_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         H00IZ64_n83Contrato_DataVigenciaTermino = new bool[] {false} ;
         H00IZ65_A160ContratoServicos_Codigo = new int[1] ;
         H00IZ65_A74Contrato_Codigo = new int[1] ;
         H00IZ65_n74Contrato_Codigo = new bool[] {false} ;
         H00IZ65_A638ContratoServicos_Ativo = new bool[] {false} ;
         H00IZ65_A155Servico_Codigo = new int[1] ;
         H00IZ65_A1858ContratoServicos_Alias = new String[] {""} ;
         H00IZ65_n1858ContratoServicos_Alias = new bool[] {false} ;
         H00IZ66_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00IZ66_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00IZ66_A1595Contratada_AreaTrbSrvPdr = new int[1] ;
         H00IZ66_n1595Contratada_AreaTrbSrvPdr = new bool[] {false} ;
         H00IZ66_A75Contrato_AreaTrabalhoCod = new int[1] ;
         H00IZ66_n75Contrato_AreaTrabalhoCod = new bool[] {false} ;
         H00IZ66_A1825ContratoAuxiliar_UsuarioCod = new int[1] ;
         H00IZ66_A632Servico_Ativo = new bool[] {false} ;
         H00IZ66_n632Servico_Ativo = new bool[] {false} ;
         H00IZ66_A157ServicoGrupo_Codigo = new int[1] ;
         H00IZ66_n157ServicoGrupo_Codigo = new bool[] {false} ;
         H00IZ66_A1824ContratoAuxiliar_ContratoCod = new int[1] ;
         H00IZ66_A39Contratada_Codigo = new int[1] ;
         H00IZ66_n39Contratada_Codigo = new bool[] {false} ;
         H00IZ66_A92Contrato_Ativo = new bool[] {false} ;
         H00IZ66_n92Contrato_Ativo = new bool[] {false} ;
         H00IZ66_A43Contratada_Ativo = new bool[] {false} ;
         H00IZ66_n43Contratada_Ativo = new bool[] {false} ;
         H00IZ66_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         H00IZ66_n83Contrato_DataVigenciaTermino = new bool[] {false} ;
         H00IZ67_A160ContratoServicos_Codigo = new int[1] ;
         H00IZ67_A74Contrato_Codigo = new int[1] ;
         H00IZ67_n74Contrato_Codigo = new bool[] {false} ;
         H00IZ67_A638ContratoServicos_Ativo = new bool[] {false} ;
         H00IZ67_A155Servico_Codigo = new int[1] ;
         H00IZ67_A1858ContratoServicos_Alias = new String[] {""} ;
         H00IZ67_n1858ContratoServicos_Alias = new bool[] {false} ;
         H00IZ70_A160ContratoServicos_Codigo = new int[1] ;
         H00IZ70_A74Contrato_Codigo = new int[1] ;
         H00IZ70_n74Contrato_Codigo = new bool[] {false} ;
         H00IZ70_A40Contratada_PessoaCod = new int[1] ;
         H00IZ70_A75Contrato_AreaTrabalhoCod = new int[1] ;
         H00IZ70_n75Contrato_AreaTrabalhoCod = new bool[] {false} ;
         H00IZ70_A155Servico_Codigo = new int[1] ;
         H00IZ70_A43Contratada_Ativo = new bool[] {false} ;
         H00IZ70_n43Contratada_Ativo = new bool[] {false} ;
         H00IZ70_A92Contrato_Ativo = new bool[] {false} ;
         H00IZ70_n92Contrato_Ativo = new bool[] {false} ;
         H00IZ70_A638ContratoServicos_Ativo = new bool[] {false} ;
         H00IZ70_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         H00IZ70_n83Contrato_DataVigenciaTermino = new bool[] {false} ;
         H00IZ70_A39Contratada_Codigo = new int[1] ;
         H00IZ70_n39Contratada_Codigo = new bool[] {false} ;
         H00IZ70_A41Contratada_PessoaNom = new String[] {""} ;
         H00IZ70_n41Contratada_PessoaNom = new bool[] {false} ;
         H00IZ70_A843Contrato_DataFimTA = new DateTime[] {DateTime.MinValue} ;
         H00IZ70_n843Contrato_DataFimTA = new bool[] {false} ;
         H00IZ71_A57Usuario_PessoaCod = new int[1] ;
         H00IZ71_A1Usuario_Codigo = new int[1] ;
         H00IZ71_A58Usuario_PessoaNom = new String[] {""} ;
         H00IZ71_n58Usuario_PessoaNom = new bool[] {false} ;
         H00IZ74_A160ContratoServicos_Codigo = new int[1] ;
         H00IZ74_A39Contratada_Codigo = new int[1] ;
         H00IZ74_n39Contratada_Codigo = new bool[] {false} ;
         H00IZ74_A92Contrato_Ativo = new bool[] {false} ;
         H00IZ74_n92Contrato_Ativo = new bool[] {false} ;
         H00IZ74_A155Servico_Codigo = new int[1] ;
         H00IZ74_A74Contrato_Codigo = new int[1] ;
         H00IZ74_n74Contrato_Codigo = new bool[] {false} ;
         H00IZ74_A843Contrato_DataFimTA = new DateTime[] {DateTime.MinValue} ;
         H00IZ74_n843Contrato_DataFimTA = new bool[] {false} ;
         H00IZ74_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         H00IZ74_n83Contrato_DataVigenciaTermino = new bool[] {false} ;
         H00IZ75_A160ContratoServicos_Codigo = new int[1] ;
         H00IZ75_A1749Artefatos_Codigo = new int[1] ;
         H00IZ75_A1751Artefatos_Descricao = new String[] {""} ;
         AV146sdt_Artefato = new SdtSDT_Artefatos(context);
         H00IZ76_A2005ContagemResultadoRequisito_Codigo = new int[1] ;
         H00IZ76_A2003ContagemResultadoRequisito_OSCod = new int[1] ;
         H00IZ76_A2004ContagemResultadoRequisito_ReqCod = new int[1] ;
         H00IZ79_A77Contrato_Numero = new String[] {""} ;
         H00IZ79_A74Contrato_Codigo = new int[1] ;
         H00IZ79_n74Contrato_Codigo = new bool[] {false} ;
         H00IZ79_A39Contratada_Codigo = new int[1] ;
         H00IZ79_n39Contratada_Codigo = new bool[] {false} ;
         H00IZ79_A92Contrato_Ativo = new bool[] {false} ;
         H00IZ79_n92Contrato_Ativo = new bool[] {false} ;
         H00IZ79_A1953Contratada_CntPadrao = new int[1] ;
         H00IZ79_n1953Contratada_CntPadrao = new bool[] {false} ;
         H00IZ79_A843Contrato_DataFimTA = new DateTime[] {DateTime.MinValue} ;
         H00IZ79_n843Contrato_DataFimTA = new bool[] {false} ;
         H00IZ79_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         H00IZ79_n83Contrato_DataVigenciaTermino = new bool[] {false} ;
         H00IZ80_A74Contrato_Codigo = new int[1] ;
         H00IZ80_n74Contrato_Codigo = new bool[] {false} ;
         H00IZ80_A638ContratoServicos_Ativo = new bool[] {false} ;
         H00IZ80_A155Servico_Codigo = new int[1] ;
         H00IZ80_A160ContratoServicos_Codigo = new int[1] ;
         H00IZ83_A92Contrato_Ativo = new bool[] {false} ;
         H00IZ83_n92Contrato_Ativo = new bool[] {false} ;
         H00IZ83_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         H00IZ83_n83Contrato_DataVigenciaTermino = new bool[] {false} ;
         H00IZ83_A39Contratada_Codigo = new int[1] ;
         H00IZ83_n39Contratada_Codigo = new bool[] {false} ;
         H00IZ83_A74Contrato_Codigo = new int[1] ;
         H00IZ83_n74Contrato_Codigo = new bool[] {false} ;
         H00IZ83_A843Contrato_DataFimTA = new DateTime[] {DateTime.MinValue} ;
         H00IZ83_n843Contrato_DataFimTA = new bool[] {false} ;
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         lblTbdestino_Jsonclick = "";
         lblTbprazo_Jsonclick = "";
         lblTbcontratofs_Jsonclick = "";
         lblTbnaocnf_Jsonclick = "";
         lblTbdescricao_Jsonclick = "";
         lblTbobservacoes_Jsonclick = "";
         bttBtnartefatos_Jsonclick = "";
         bttBtnenter_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         lblTbdestino5_Jsonclick = "";
         lblTbdescpfb_Jsonclick = "";
         lblTbdescpfl_Jsonclick = "";
         lblTbservico5_Jsonclick = "";
         lblTbservico_Jsonclick = "";
         lblTbservico4_Jsonclick = "";
         lblTextblockcontrato_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_tramitacao__default(),
            new Object[][] {
                new Object[] {
               H00IZ4_A74Contrato_Codigo, H00IZ4_A77Contrato_Numero, H00IZ4_A39Contratada_Codigo, H00IZ4_A92Contrato_Ativo, H00IZ4_A1869Contrato_DataTermino
               }
               , new Object[] {
               H00IZ5_A426NaoConformidade_Codigo, H00IZ5_A427NaoConformidade_Nome, H00IZ5_A429NaoConformidade_Tipo, H00IZ5_A428NaoConformidade_AreaTrabalhoCod
               }
               , new Object[] {
               H00IZ7_A146Modulo_Codigo, H00IZ7_n146Modulo_Codigo, H00IZ7_A468ContagemResultado_NaoCnfDmnCod, H00IZ7_n468ContagemResultado_NaoCnfDmnCod, H00IZ7_A1553ContagemResultado_CntSrvCod, H00IZ7_n1553ContagemResultado_CntSrvCod, H00IZ7_A456ContagemResultado_Codigo, H00IZ7_A127Sistema_Codigo, H00IZ7_A489ContagemResultado_SistemaCod, H00IZ7_n489ContagemResultado_SistemaCod,
               H00IZ7_A1831Sistema_Responsavel, H00IZ7_n1831Sistema_Responsavel, H00IZ7_A428NaoConformidade_AreaTrabalhoCod, H00IZ7_n428NaoConformidade_AreaTrabalhoCod, H00IZ7_A429NaoConformidade_Tipo, H00IZ7_n429NaoConformidade_Tipo, H00IZ7_A92Contrato_Ativo, H00IZ7_n92Contrato_Ativo, H00IZ7_A457ContagemResultado_Demanda, H00IZ7_n457ContagemResultado_Demanda,
               H00IZ7_A484ContagemResultado_StatusDmn, H00IZ7_n484ContagemResultado_StatusDmn, H00IZ7_A1593ContagemResultado_CntSrvTpVnc, H00IZ7_n1593ContagemResultado_CntSrvTpVnc, H00IZ7_A602ContagemResultado_OSVinculada, H00IZ7_n602ContagemResultado_OSVinculada, H00IZ7_A890ContagemResultado_Responsavel, H00IZ7_n890ContagemResultado_Responsavel, H00IZ7_A601ContagemResultado_Servico, H00IZ7_n601ContagemResultado_Servico,
               H00IZ7_A803ContagemResultado_ContratadaSigla, H00IZ7_n803ContagemResultado_ContratadaSigla, H00IZ7_A490ContagemResultado_ContratadaCod, H00IZ7_n490ContagemResultado_ContratadaCod, H00IZ7_A805ContagemResultado_ContratadaOrigemCod, H00IZ7_n805ContagemResultado_ContratadaOrigemCod, H00IZ7_A1627ContagemResultado_CntSrvVncCod, H00IZ7_n1627ContagemResultado_CntSrvVncCod, H00IZ7_A1622ContagemResultado_CntVncCod, H00IZ7_n1622ContagemResultado_CntVncCod,
               H00IZ7_A866ContagemResultado_ContratadaOrigemSigla, H00IZ7_n866ContagemResultado_ContratadaOrigemSigla, H00IZ7_A1326ContagemResultado_ContratadaTipoFab, H00IZ7_n1326ContagemResultado_ContratadaTipoFab, H00IZ7_A1611ContagemResultado_PrzTpDias, H00IZ7_n1611ContagemResultado_PrzTpDias, H00IZ7_A1636ContagemResultado_ServicoSS, H00IZ7_n1636ContagemResultado_ServicoSS, H00IZ7_A1452ContagemResultado_SS, H00IZ7_n1452ContagemResultado_SS,
               H00IZ7_A1603ContagemResultado_CntCod, H00IZ7_n1603ContagemResultado_CntCod, H00IZ7_A508ContagemResultado_Owner, H00IZ7_A494ContagemResultado_Descricao, H00IZ7_n494ContagemResultado_Descricao, H00IZ7_A531ContagemResultado_StatusUltCnt, H00IZ7_A684ContagemResultado_PFBFSUltima, H00IZ7_A685ContagemResultado_PFLFSUltima, H00IZ7_A682ContagemResultado_PFBFMUltima, H00IZ7_A683ContagemResultado_PFLFMUltima,
               H00IZ7_A83Contrato_DataVigenciaTermino, H00IZ7_n83Contrato_DataVigenciaTermino, H00IZ7_A1829Sistema_GpoObjCtrlCod, H00IZ7_n1829Sistema_GpoObjCtrlCod
               }
               , new Object[] {
               H00IZ8_A127Sistema_Codigo
               }
               , new Object[] {
               H00IZ9_A1219LogResponsavel_OwnerPessoaCod, H00IZ9_n1219LogResponsavel_OwnerPessoaCod, H00IZ9_A1553ContagemResultado_CntSrvCod, H00IZ9_n1553ContagemResultado_CntSrvCod, H00IZ9_A1603ContagemResultado_CntCod, H00IZ9_n1603ContagemResultado_CntCod, H00IZ9_A39Contratada_Codigo, H00IZ9_n39Contratada_Codigo, H00IZ9_A1222LogResponsavel_OwnerPessoaNom, H00IZ9_n1222LogResponsavel_OwnerPessoaNom,
               H00IZ9_A1797LogResponsavel_Codigo, H00IZ9_A896LogResponsavel_Owner, H00IZ9_A892LogResponsavel_DemandaCod, H00IZ9_n892LogResponsavel_DemandaCod
               }
               , new Object[] {
               H00IZ10_AV150i
               }
               , new Object[] {
               H00IZ11_AV103QtdNaoAcatado
               }
               , new Object[] {
               H00IZ12_A761ContagemResultadoChckLst_Codigo, H00IZ12_A758CheckList_Codigo, H00IZ12_A1230CheckList_De, H00IZ12_n1230CheckList_De, H00IZ12_A762ContagemResultadoChckLst_Cumpre, H00IZ12_A1868ContagemResultadoChckLst_OSCod
               }
               , new Object[] {
               H00IZ13_A892LogResponsavel_DemandaCod, H00IZ13_n892LogResponsavel_DemandaCod, H00IZ13_A1177LogResponsavel_Prazo, H00IZ13_n1177LogResponsavel_Prazo, H00IZ13_A896LogResponsavel_Owner, H00IZ13_A1797LogResponsavel_Codigo
               }
               , new Object[] {
               H00IZ14_A70ContratadaUsuario_UsuarioPessoaCod, H00IZ14_n70ContratadaUsuario_UsuarioPessoaCod, H00IZ14_A66ContratadaUsuario_ContratadaCod, H00IZ14_A69ContratadaUsuario_UsuarioCod, H00IZ14_A71ContratadaUsuario_UsuarioPessoaNom, H00IZ14_n71ContratadaUsuario_UsuarioPessoaNom, H00IZ14_A1394ContratadaUsuario_UsuarioAtivo, H00IZ14_n1394ContratadaUsuario_UsuarioAtivo, H00IZ14_A1908Usuario_DeFerias, H00IZ14_n1908Usuario_DeFerias
               }
               , new Object[] {
               H00IZ15_A828UsuarioServicos_UsuarioCod, H00IZ15_A829UsuarioServicos_ServicoCod
               }
               , new Object[] {
               H00IZ16_A192Contagem_Codigo, H00IZ16_A1118Contagem_ContratadaCod, H00IZ16_n1118Contagem_ContratadaCod, H00IZ16_A945Contagem_Demanda, H00IZ16_n945Contagem_Demanda
               }
               , new Object[] {
               H00IZ17_A70ContratadaUsuario_UsuarioPessoaCod, H00IZ17_n70ContratadaUsuario_UsuarioPessoaCod, H00IZ17_A69ContratadaUsuario_UsuarioCod, H00IZ17_A71ContratadaUsuario_UsuarioPessoaNom, H00IZ17_n71ContratadaUsuario_UsuarioPessoaNom, H00IZ17_A1394ContratadaUsuario_UsuarioAtivo, H00IZ17_n1394ContratadaUsuario_UsuarioAtivo, H00IZ17_A1908Usuario_DeFerias, H00IZ17_n1908Usuario_DeFerias, H00IZ17_A66ContratadaUsuario_ContratadaCod
               }
               , new Object[] {
               H00IZ18_A828UsuarioServicos_UsuarioCod, H00IZ18_A829UsuarioServicos_ServicoCod
               }
               , new Object[] {
               H00IZ19_A155Servico_Codigo
               }
               , new Object[] {
               H00IZ20_A57Usuario_PessoaCod, H00IZ20_A1Usuario_Codigo, H00IZ20_A58Usuario_PessoaNom, H00IZ20_n58Usuario_PessoaNom
               }
               , new Object[] {
               H00IZ21_A1078ContratoGestor_ContratoCod, H00IZ21_A1136ContratoGestor_ContratadaCod, H00IZ21_n1136ContratoGestor_ContratadaCod, H00IZ21_A1223ContratoGestor_ContratadaSigla, H00IZ21_n1223ContratoGestor_ContratadaSigla
               }
               , new Object[] {
               H00IZ22_A61ContratanteUsuario_UsuarioPessoaCod, H00IZ22_n61ContratanteUsuario_UsuarioPessoaCod, H00IZ22_A54Usuario_Ativo, H00IZ22_n54Usuario_Ativo, H00IZ22_A1908Usuario_DeFerias, H00IZ22_n1908Usuario_DeFerias, H00IZ22_A60ContratanteUsuario_UsuarioCod, H00IZ22_A63ContratanteUsuario_ContratanteCod, H00IZ22_A62ContratanteUsuario_UsuarioPessoaNom, H00IZ22_n62ContratanteUsuario_UsuarioPessoaNom
               }
               , new Object[] {
               H00IZ23_A1078ContratoGestor_ContratoCod, H00IZ23_A1136ContratoGestor_ContratadaCod, H00IZ23_n1136ContratoGestor_ContratadaCod, H00IZ23_A1447ContratoGestor_ContratadaAreaDes, H00IZ23_n1447ContratoGestor_ContratadaAreaDes, H00IZ23_A1079ContratoGestor_UsuarioCod, H00IZ23_A1446ContratoGestor_ContratadaAreaCod, H00IZ23_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               H00IZ24_A70ContratadaUsuario_UsuarioPessoaCod, H00IZ24_n70ContratadaUsuario_UsuarioPessoaCod, H00IZ24_A69ContratadaUsuario_UsuarioCod, H00IZ24_A71ContratadaUsuario_UsuarioPessoaNom, H00IZ24_n71ContratadaUsuario_UsuarioPessoaNom, H00IZ24_A1394ContratadaUsuario_UsuarioAtivo, H00IZ24_n1394ContratadaUsuario_UsuarioAtivo, H00IZ24_A1908Usuario_DeFerias, H00IZ24_n1908Usuario_DeFerias, H00IZ24_A66ContratadaUsuario_ContratadaCod
               }
               , new Object[] {
               H00IZ25_A828UsuarioServicos_UsuarioCod, H00IZ25_A829UsuarioServicos_ServicoCod
               }
               , new Object[] {
               H00IZ26_A891LogResponsavel_UsuarioCod, H00IZ26_n891LogResponsavel_UsuarioCod, H00IZ26_A468ContagemResultado_NaoCnfDmnCod, H00IZ26_n468ContagemResultado_NaoCnfDmnCod, H00IZ26_A1553ContagemResultado_CntSrvCod, H00IZ26_n1553ContagemResultado_CntSrvCod, H00IZ26_A1603ContagemResultado_CntCod, H00IZ26_n1603ContagemResultado_CntCod, H00IZ26_A1908Usuario_DeFerias, H00IZ26_n1908Usuario_DeFerias,
               H00IZ26_A490ContagemResultado_ContratadaCod, H00IZ26_n490ContagemResultado_ContratadaCod, H00IZ26_A39Contratada_Codigo, H00IZ26_n39Contratada_Codigo, H00IZ26_A428NaoConformidade_AreaTrabalhoCod, H00IZ26_n428NaoConformidade_AreaTrabalhoCod, H00IZ26_A429NaoConformidade_Tipo, H00IZ26_n429NaoConformidade_Tipo, H00IZ26_A92Contrato_Ativo, H00IZ26_n92Contrato_Ativo,
               H00IZ26_A1797LogResponsavel_Codigo, H00IZ26_A896LogResponsavel_Owner, H00IZ26_A892LogResponsavel_DemandaCod, H00IZ26_n892LogResponsavel_DemandaCod, H00IZ26_A83Contrato_DataVigenciaTermino, H00IZ26_n83Contrato_DataVigenciaTermino
               }
               , new Object[] {
               H00IZ27_A69ContratadaUsuario_UsuarioCod, H00IZ27_A66ContratadaUsuario_ContratadaCod
               }
               , new Object[] {
               H00IZ28_A1078ContratoGestor_ContratoCod, H00IZ28_A1079ContratoGestor_UsuarioCod
               }
               , new Object[] {
               H00IZ31_A456ContagemResultado_Codigo, H00IZ31_A490ContagemResultado_ContratadaCod, H00IZ31_n490ContagemResultado_ContratadaCod, H00IZ31_A601ContagemResultado_Servico, H00IZ31_n601ContagemResultado_Servico, H00IZ31_A484ContagemResultado_StatusDmn, H00IZ31_n484ContagemResultado_StatusDmn, H00IZ31_A471ContagemResultado_DataDmn, H00IZ31_A1237ContagemResultado_PrazoMaisDias, H00IZ31_n1237ContagemResultado_PrazoMaisDias,
               H00IZ31_A1227ContagemResultado_PrazoInicialDias, H00IZ31_n1227ContagemResultado_PrazoInicialDias, H00IZ31_A912ContagemResultado_HoraEntrega, H00IZ31_n912ContagemResultado_HoraEntrega, H00IZ31_A472ContagemResultado_DataEntrega, H00IZ31_n472ContagemResultado_DataEntrega, H00IZ31_A1351ContagemResultado_DataPrevista, H00IZ31_n1351ContagemResultado_DataPrevista, H00IZ31_A1611ContagemResultado_PrzTpDias, H00IZ31_n1611ContagemResultado_PrzTpDias,
               H00IZ31_A1553ContagemResultado_CntSrvCod, H00IZ31_n1553ContagemResultado_CntSrvCod, H00IZ31_A798ContagemResultado_PFBFSImp, H00IZ31_n798ContagemResultado_PFBFSImp, H00IZ31_A531ContagemResultado_StatusUltCnt, H00IZ31_A684ContagemResultado_PFBFSUltima, H00IZ31_A40000ContagemResultado_ContadorFMCod, H00IZ31_n40000ContagemResultado_ContadorFMCod
               }
               , new Object[] {
               H00IZ32_A1130LogResponsavel_Status, H00IZ32_n1130LogResponsavel_Status, H00IZ32_A894LogResponsavel_Acao, H00IZ32_A1797LogResponsavel_Codigo, H00IZ32_A891LogResponsavel_UsuarioCod, H00IZ32_n891LogResponsavel_UsuarioCod, H00IZ32_A892LogResponsavel_DemandaCod, H00IZ32_n892LogResponsavel_DemandaCod
               }
               , new Object[] {
               H00IZ33_A1234LogResponsavel_NovoStatus, H00IZ33_n1234LogResponsavel_NovoStatus, H00IZ33_A892LogResponsavel_DemandaCod, H00IZ33_n892LogResponsavel_DemandaCod, H00IZ33_A1177LogResponsavel_Prazo, H00IZ33_n1177LogResponsavel_Prazo, H00IZ33_A1797LogResponsavel_Codigo
               }
               , new Object[] {
               H00IZ34_A894LogResponsavel_Acao, H00IZ34_A1177LogResponsavel_Prazo, H00IZ34_n1177LogResponsavel_Prazo, H00IZ34_A1797LogResponsavel_Codigo, H00IZ34_A896LogResponsavel_Owner, H00IZ34_A891LogResponsavel_UsuarioCod, H00IZ34_n891LogResponsavel_UsuarioCod, H00IZ34_A892LogResponsavel_DemandaCod, H00IZ34_n892LogResponsavel_DemandaCod
               }
               , new Object[] {
               H00IZ35_A1553ContagemResultado_CntSrvCod, H00IZ35_n1553ContagemResultado_CntSrvCod, H00IZ35_A456ContagemResultado_Codigo, H00IZ35_A1618ContagemResultado_PrzRsp, H00IZ35_n1618ContagemResultado_PrzRsp, H00IZ35_A1611ContagemResultado_PrzTpDias, H00IZ35_n1611ContagemResultado_PrzTpDias
               }
               , new Object[] {
               H00IZ36_A1797LogResponsavel_Codigo, H00IZ36_A896LogResponsavel_Owner, H00IZ36_A892LogResponsavel_DemandaCod, H00IZ36_n892LogResponsavel_DemandaCod
               }
               , new Object[] {
               H00IZ37_A468ContagemResultado_NaoCnfDmnCod, H00IZ37_n468ContagemResultado_NaoCnfDmnCod, H00IZ37_A1553ContagemResultado_CntSrvCod, H00IZ37_n1553ContagemResultado_CntSrvCod, H00IZ37_A1603ContagemResultado_CntCod, H00IZ37_n1603ContagemResultado_CntCod, H00IZ37_A39Contratada_Codigo, H00IZ37_n39Contratada_Codigo, H00IZ37_A428NaoConformidade_AreaTrabalhoCod, H00IZ37_n428NaoConformidade_AreaTrabalhoCod,
               H00IZ37_A429NaoConformidade_Tipo, H00IZ37_n429NaoConformidade_Tipo, H00IZ37_A92Contrato_Ativo, H00IZ37_n92Contrato_Ativo, H00IZ37_A456ContagemResultado_Codigo, H00IZ37_A83Contrato_DataVigenciaTermino, H00IZ37_n83Contrato_DataVigenciaTermino
               }
               , new Object[] {
               H00IZ38_A1078ContratoGestor_ContratoCod, H00IZ38_A1079ContratoGestor_UsuarioCod, H00IZ38_A1446ContratoGestor_ContratadaAreaCod, H00IZ38_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               H00IZ39_A468ContagemResultado_NaoCnfDmnCod, H00IZ39_n468ContagemResultado_NaoCnfDmnCod, H00IZ39_A1553ContagemResultado_CntSrvCod, H00IZ39_n1553ContagemResultado_CntSrvCod, H00IZ39_A1603ContagemResultado_CntCod, H00IZ39_n1603ContagemResultado_CntCod, H00IZ39_A39Contratada_Codigo, H00IZ39_n39Contratada_Codigo, H00IZ39_A428NaoConformidade_AreaTrabalhoCod, H00IZ39_n428NaoConformidade_AreaTrabalhoCod,
               H00IZ39_A429NaoConformidade_Tipo, H00IZ39_n429NaoConformidade_Tipo, H00IZ39_A92Contrato_Ativo, H00IZ39_n92Contrato_Ativo, H00IZ39_A456ContagemResultado_Codigo, H00IZ39_A1604ContagemResultado_CntPrpCod, H00IZ39_n1604ContagemResultado_CntPrpCod, H00IZ39_A83Contrato_DataVigenciaTermino, H00IZ39_n83Contrato_DataVigenciaTermino
               }
               , new Object[] {
               H00IZ40_A1078ContratoGestor_ContratoCod, H00IZ40_A1079ContratoGestor_UsuarioCod, H00IZ40_A1446ContratoGestor_ContratadaAreaCod, H00IZ40_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               H00IZ43_A74Contrato_Codigo, H00IZ43_A39Contratada_Codigo, H00IZ43_A92Contrato_Ativo, H00IZ43_A1013Contrato_PrepostoCod, H00IZ43_n1013Contrato_PrepostoCod, H00IZ43_A843Contrato_DataFimTA, H00IZ43_n843Contrato_DataFimTA, H00IZ43_A83Contrato_DataVigenciaTermino
               }
               , new Object[] {
               H00IZ44_A1078ContratoGestor_ContratoCod, H00IZ44_A1079ContratoGestor_UsuarioCod, H00IZ44_A1446ContratoGestor_ContratadaAreaCod, H00IZ44_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               H00IZ46_A1636ContagemResultado_ServicoSS, H00IZ46_n1636ContagemResultado_ServicoSS, H00IZ46_A484ContagemResultado_StatusDmn, H00IZ46_n484ContagemResultado_StatusDmn, H00IZ46_A490ContagemResultado_ContratadaCod, H00IZ46_n490ContagemResultado_ContratadaCod, H00IZ46_A456ContagemResultado_Codigo, H00IZ46_A1326ContagemResultado_ContratadaTipoFab, H00IZ46_n1326ContagemResultado_ContratadaTipoFab, H00IZ46_A531ContagemResultado_StatusUltCnt,
               H00IZ46_A682ContagemResultado_PFBFMUltima, H00IZ46_A683ContagemResultado_PFLFMUltima, H00IZ46_A684ContagemResultado_PFBFSUltima, H00IZ46_A685ContagemResultado_PFLFSUltima
               }
               , new Object[] {
               H00IZ48_A1553ContagemResultado_CntSrvCod, H00IZ48_n1553ContagemResultado_CntSrvCod, H00IZ48_A1636ContagemResultado_ServicoSS, H00IZ48_n1636ContagemResultado_ServicoSS, H00IZ48_A484ContagemResultado_StatusDmn, H00IZ48_n484ContagemResultado_StatusDmn, H00IZ48_A1593ContagemResultado_CntSrvTpVnc, H00IZ48_n1593ContagemResultado_CntSrvTpVnc, H00IZ48_A490ContagemResultado_ContratadaCod, H00IZ48_n490ContagemResultado_ContratadaCod,
               H00IZ48_A602ContagemResultado_OSVinculada, H00IZ48_n602ContagemResultado_OSVinculada, H00IZ48_A1603ContagemResultado_CntCod, H00IZ48_n1603ContagemResultado_CntCod, H00IZ48_A803ContagemResultado_ContratadaSigla, H00IZ48_n803ContagemResultado_ContratadaSigla, H00IZ48_A456ContagemResultado_Codigo, H00IZ48_A1326ContagemResultado_ContratadaTipoFab, H00IZ48_n1326ContagemResultado_ContratadaTipoFab, H00IZ48_A531ContagemResultado_StatusUltCnt,
               H00IZ48_A682ContagemResultado_PFBFMUltima, H00IZ48_A683ContagemResultado_PFLFMUltima, H00IZ48_A684ContagemResultado_PFBFSUltima, H00IZ48_A685ContagemResultado_PFLFSUltima
               }
               , new Object[] {
               H00IZ49_A468ContagemResultado_NaoCnfDmnCod, H00IZ49_n468ContagemResultado_NaoCnfDmnCod, H00IZ49_A490ContagemResultado_ContratadaCod, H00IZ49_n490ContagemResultado_ContratadaCod, H00IZ49_A1553ContagemResultado_CntSrvCod, H00IZ49_n1553ContagemResultado_CntSrvCod, H00IZ49_A1603ContagemResultado_CntCod, H00IZ49_n1603ContagemResultado_CntCod, H00IZ49_A1481Contratada_UsaOSistema, H00IZ49_n1481Contratada_UsaOSistema,
               H00IZ49_A39Contratada_Codigo, H00IZ49_n39Contratada_Codigo, H00IZ49_A428NaoConformidade_AreaTrabalhoCod, H00IZ49_n428NaoConformidade_AreaTrabalhoCod, H00IZ49_A429NaoConformidade_Tipo, H00IZ49_n429NaoConformidade_Tipo, H00IZ49_A92Contrato_Ativo, H00IZ49_n92Contrato_Ativo, H00IZ49_A1797LogResponsavel_Codigo, H00IZ49_A896LogResponsavel_Owner,
               H00IZ49_A891LogResponsavel_UsuarioCod, H00IZ49_n891LogResponsavel_UsuarioCod, H00IZ49_A892LogResponsavel_DemandaCod, H00IZ49_n892LogResponsavel_DemandaCod, H00IZ49_A83Contrato_DataVigenciaTermino, H00IZ49_n83Contrato_DataVigenciaTermino
               }
               , new Object[] {
               H00IZ50_A69ContratadaUsuario_UsuarioCod, H00IZ50_A66ContratadaUsuario_ContratadaCod
               }
               , new Object[] {
               H00IZ51_A1481Contratada_UsaOSistema, H00IZ51_n1481Contratada_UsaOSistema, H00IZ51_A66ContratadaUsuario_ContratadaCod, H00IZ51_A69ContratadaUsuario_UsuarioCod
               }
               , new Object[] {
               H00IZ52_A1797LogResponsavel_Codigo, H00IZ52_A896LogResponsavel_Owner, H00IZ52_A892LogResponsavel_DemandaCod, H00IZ52_n892LogResponsavel_DemandaCod
               }
               , new Object[] {
               H00IZ53_A1078ContratoGestor_ContratoCod, H00IZ53_A1079ContratoGestor_UsuarioCod, H00IZ53_A1446ContratoGestor_ContratadaAreaCod, H00IZ53_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               H00IZ54_A468ContagemResultado_NaoCnfDmnCod, H00IZ54_n468ContagemResultado_NaoCnfDmnCod, H00IZ54_A1553ContagemResultado_CntSrvCod, H00IZ54_n1553ContagemResultado_CntSrvCod, H00IZ54_A1603ContagemResultado_CntCod, H00IZ54_n1603ContagemResultado_CntCod, H00IZ54_A39Contratada_Codigo, H00IZ54_n39Contratada_Codigo, H00IZ54_A428NaoConformidade_AreaTrabalhoCod, H00IZ54_n428NaoConformidade_AreaTrabalhoCod,
               H00IZ54_A429NaoConformidade_Tipo, H00IZ54_n429NaoConformidade_Tipo, H00IZ54_A92Contrato_Ativo, H00IZ54_n92Contrato_Ativo, H00IZ54_A894LogResponsavel_Acao, H00IZ54_A1797LogResponsavel_Codigo, H00IZ54_A896LogResponsavel_Owner, H00IZ54_A892LogResponsavel_DemandaCod, H00IZ54_n892LogResponsavel_DemandaCod, H00IZ54_A83Contrato_DataVigenciaTermino,
               H00IZ54_n83Contrato_DataVigenciaTermino
               }
               , new Object[] {
               H00IZ55_A69ContratadaUsuario_UsuarioCod, H00IZ55_A66ContratadaUsuario_ContratadaCod
               }
               , new Object[] {
               H00IZ58_A160ContratoServicos_Codigo, H00IZ58_A74Contrato_Codigo, H00IZ58_n74Contrato_Codigo, H00IZ58_A1013Contrato_PrepostoCod, H00IZ58_n1013Contrato_PrepostoCod, H00IZ58_A39Contratada_Codigo, H00IZ58_A92Contrato_Ativo, H00IZ58_A155Servico_Codigo, H00IZ58_A843Contrato_DataFimTA, H00IZ58_n843Contrato_DataFimTA,
               H00IZ58_A83Contrato_DataVigenciaTermino
               }
               , new Object[] {
               H00IZ59_A1078ContratoGestor_ContratoCod, H00IZ59_A1079ContratoGestor_UsuarioCod
               }
               , new Object[] {
               H00IZ60_A1136ContratoGestor_ContratadaCod, H00IZ60_n1136ContratoGestor_ContratadaCod, H00IZ60_A52Contratada_AreaTrabalhoCod, H00IZ60_n52Contratada_AreaTrabalhoCod, H00IZ60_A1595Contratada_AreaTrbSrvPdr, H00IZ60_n1595Contratada_AreaTrbSrvPdr, H00IZ60_A1446ContratoGestor_ContratadaAreaCod, H00IZ60_n1446ContratoGestor_ContratadaAreaCod, H00IZ60_A1079ContratoGestor_UsuarioCod, H00IZ60_A632Servico_Ativo,
               H00IZ60_n632Servico_Ativo, H00IZ60_A157ServicoGrupo_Codigo, H00IZ60_n157ServicoGrupo_Codigo, H00IZ60_A1078ContratoGestor_ContratoCod, H00IZ60_A158ServicoGrupo_Descricao, H00IZ60_A92Contrato_Ativo, H00IZ60_n92Contrato_Ativo, H00IZ60_A43Contratada_Ativo, H00IZ60_n43Contratada_Ativo, H00IZ60_A83Contrato_DataVigenciaTermino,
               H00IZ60_n83Contrato_DataVigenciaTermino
               }
               , new Object[] {
               H00IZ61_A160ContratoServicos_Codigo, H00IZ61_A74Contrato_Codigo, H00IZ61_A638ContratoServicos_Ativo
               }
               , new Object[] {
               H00IZ62_A52Contratada_AreaTrabalhoCod, H00IZ62_A1595Contratada_AreaTrbSrvPdr, H00IZ62_n1595Contratada_AreaTrbSrvPdr, H00IZ62_A75Contrato_AreaTrabalhoCod, H00IZ62_n75Contrato_AreaTrabalhoCod, H00IZ62_A1825ContratoAuxiliar_UsuarioCod, H00IZ62_A632Servico_Ativo, H00IZ62_n632Servico_Ativo, H00IZ62_A157ServicoGrupo_Codigo, H00IZ62_n157ServicoGrupo_Codigo,
               H00IZ62_A1824ContratoAuxiliar_ContratoCod, H00IZ62_A158ServicoGrupo_Descricao, H00IZ62_A39Contratada_Codigo, H00IZ62_n39Contratada_Codigo, H00IZ62_A92Contrato_Ativo, H00IZ62_n92Contrato_Ativo, H00IZ62_A43Contratada_Ativo, H00IZ62_A83Contrato_DataVigenciaTermino, H00IZ62_n83Contrato_DataVigenciaTermino
               }
               , new Object[] {
               H00IZ63_A160ContratoServicos_Codigo, H00IZ63_A74Contrato_Codigo, H00IZ63_A638ContratoServicos_Ativo, H00IZ63_A155Servico_Codigo
               }
               , new Object[] {
               H00IZ64_A1136ContratoGestor_ContratadaCod, H00IZ64_n1136ContratoGestor_ContratadaCod, H00IZ64_A52Contratada_AreaTrabalhoCod, H00IZ64_n52Contratada_AreaTrabalhoCod, H00IZ64_A1595Contratada_AreaTrbSrvPdr, H00IZ64_n1595Contratada_AreaTrbSrvPdr, H00IZ64_A1446ContratoGestor_ContratadaAreaCod, H00IZ64_n1446ContratoGestor_ContratadaAreaCod, H00IZ64_A1079ContratoGestor_UsuarioCod, H00IZ64_A632Servico_Ativo,
               H00IZ64_n632Servico_Ativo, H00IZ64_A157ServicoGrupo_Codigo, H00IZ64_n157ServicoGrupo_Codigo, H00IZ64_A1078ContratoGestor_ContratoCod, H00IZ64_A92Contrato_Ativo, H00IZ64_n92Contrato_Ativo, H00IZ64_A43Contratada_Ativo, H00IZ64_n43Contratada_Ativo, H00IZ64_A83Contrato_DataVigenciaTermino, H00IZ64_n83Contrato_DataVigenciaTermino
               }
               , new Object[] {
               H00IZ65_A160ContratoServicos_Codigo, H00IZ65_A74Contrato_Codigo, H00IZ65_A638ContratoServicos_Ativo, H00IZ65_A155Servico_Codigo, H00IZ65_A1858ContratoServicos_Alias, H00IZ65_n1858ContratoServicos_Alias
               }
               , new Object[] {
               H00IZ66_A52Contratada_AreaTrabalhoCod, H00IZ66_A1595Contratada_AreaTrbSrvPdr, H00IZ66_n1595Contratada_AreaTrbSrvPdr, H00IZ66_A75Contrato_AreaTrabalhoCod, H00IZ66_n75Contrato_AreaTrabalhoCod, H00IZ66_A1825ContratoAuxiliar_UsuarioCod, H00IZ66_A632Servico_Ativo, H00IZ66_n632Servico_Ativo, H00IZ66_A157ServicoGrupo_Codigo, H00IZ66_n157ServicoGrupo_Codigo,
               H00IZ66_A1824ContratoAuxiliar_ContratoCod, H00IZ66_A39Contratada_Codigo, H00IZ66_n39Contratada_Codigo, H00IZ66_A92Contrato_Ativo, H00IZ66_n92Contrato_Ativo, H00IZ66_A43Contratada_Ativo, H00IZ66_A83Contrato_DataVigenciaTermino, H00IZ66_n83Contrato_DataVigenciaTermino
               }
               , new Object[] {
               H00IZ67_A160ContratoServicos_Codigo, H00IZ67_A74Contrato_Codigo, H00IZ67_A638ContratoServicos_Ativo, H00IZ67_A155Servico_Codigo, H00IZ67_A1858ContratoServicos_Alias, H00IZ67_n1858ContratoServicos_Alias
               }
               , new Object[] {
               H00IZ70_A160ContratoServicos_Codigo, H00IZ70_A74Contrato_Codigo, H00IZ70_n74Contrato_Codigo, H00IZ70_A40Contratada_PessoaCod, H00IZ70_A75Contrato_AreaTrabalhoCod, H00IZ70_A155Servico_Codigo, H00IZ70_A43Contratada_Ativo, H00IZ70_A92Contrato_Ativo, H00IZ70_A638ContratoServicos_Ativo, H00IZ70_A83Contrato_DataVigenciaTermino,
               H00IZ70_A39Contratada_Codigo, H00IZ70_A41Contratada_PessoaNom, H00IZ70_n41Contratada_PessoaNom, H00IZ70_A843Contrato_DataFimTA, H00IZ70_n843Contrato_DataFimTA
               }
               , new Object[] {
               H00IZ71_A57Usuario_PessoaCod, H00IZ71_A1Usuario_Codigo, H00IZ71_A58Usuario_PessoaNom, H00IZ71_n58Usuario_PessoaNom
               }
               , new Object[] {
               H00IZ74_A160ContratoServicos_Codigo, H00IZ74_A39Contratada_Codigo, H00IZ74_A92Contrato_Ativo, H00IZ74_A155Servico_Codigo, H00IZ74_A74Contrato_Codigo, H00IZ74_n74Contrato_Codigo, H00IZ74_A843Contrato_DataFimTA, H00IZ74_n843Contrato_DataFimTA, H00IZ74_A83Contrato_DataVigenciaTermino
               }
               , new Object[] {
               H00IZ75_A160ContratoServicos_Codigo, H00IZ75_A1749Artefatos_Codigo, H00IZ75_A1751Artefatos_Descricao
               }
               , new Object[] {
               H00IZ76_A2005ContagemResultadoRequisito_Codigo, H00IZ76_A2003ContagemResultadoRequisito_OSCod, H00IZ76_A2004ContagemResultadoRequisito_ReqCod
               }
               , new Object[] {
               H00IZ79_A77Contrato_Numero, H00IZ79_A74Contrato_Codigo, H00IZ79_A39Contratada_Codigo, H00IZ79_A92Contrato_Ativo, H00IZ79_A1953Contratada_CntPadrao, H00IZ79_n1953Contratada_CntPadrao, H00IZ79_A843Contrato_DataFimTA, H00IZ79_n843Contrato_DataFimTA, H00IZ79_A83Contrato_DataVigenciaTermino
               }
               , new Object[] {
               H00IZ80_A74Contrato_Codigo, H00IZ80_A638ContratoServicos_Ativo, H00IZ80_A155Servico_Codigo, H00IZ80_A160ContratoServicos_Codigo
               }
               , new Object[] {
               H00IZ83_A92Contrato_Ativo, H00IZ83_A83Contrato_DataVigenciaTermino, H00IZ83_A39Contratada_Codigo, H00IZ83_A74Contrato_Codigo, H00IZ83_A843Contrato_DataFimTA, H00IZ83_n843Contrato_DataFimTA
               }
            }
         );
         WebComp_Webcomp2 = new GeneXus.Http.GXNullWebComponent();
         AV191Pgmname = "WP_Tramitacao";
         /* GeneXus formulas. */
         AV191Pgmname = "WP_Tramitacao";
         context.Gx_err = 0;
         edtavDescricao_Enabled = 0;
         edtavPfb_Enabled = 0;
         edtavPfl_Enabled = 0;
         edtavPfbof_Enabled = 0;
         edtavPflof_Enabled = 0;
         cmbavStatuscontagem.Enabled = 0;
         edtavPfbfmultima_Enabled = 0;
         edtavPfbfsultima_Enabled = 0;
         edtavPflfmultima_Enabled = 0;
         edtavPflfsultima_Enabled = 0;
         edtavOldpfb_Enabled = 0;
         edtavOldpfl_Enabled = 0;
      }

      private short nRcdExists_22 ;
      private short nIsMod_22 ;
      private short nRcdExists_23 ;
      private short nIsMod_23 ;
      private short nRcdExists_24 ;
      private short nIsMod_24 ;
      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short AV93DiasAnalise ;
      private short AV73PrazoInicialDias ;
      private short A1227ContagemResultado_PrazoInicialDias ;
      private short A1237ContagemResultado_PrazoMaisDias ;
      private short A531ContagemResultado_StatusUltCnt ;
      private short A1618ContagemResultado_PrzRsp ;
      private short AV42StatusContagem ;
      private short wbEnd ;
      private short wbStart ;
      private short nCmpId ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short A429NaoConformidade_Tipo ;
      private short AV103QtdNaoAcatado ;
      private short AV150i ;
      private short cV150i ;
      private short cV103QtdNaoAcatado ;
      private short A1230CheckList_De ;
      private short AV201GXLvl372 ;
      private short this_Displaymode ;
      private short AV212GXLvl778 ;
      private short AV217GXLvl1220 ;
      private short AV219GXLvl1246 ;
      private short AV26Dias ;
      private short AV32Hours ;
      private short AV33Minutes ;
      private short AV221GXLvl1327 ;
      private short AV223GXLvl1336 ;
      private short AV225GXLvl1355 ;
      private short AV227GXLvl1368 ;
      private short GXt_int13 ;
      private short AV228GXLvl1406 ;
      private short AV229GXLvl1432 ;
      private short AV233GXLvl1520 ;
      private short AV235GXLvl1541 ;
      private short AV163Count ;
      private short nGXWrapped ;
      private short wbTemp ;
      private int AV9Codigo ;
      private int wcpOAV9Codigo ;
      private int AV5Usuario_Codigo ;
      private int edtavDescricao_Enabled ;
      private int edtavPfb_Enabled ;
      private int edtavPfl_Enabled ;
      private int edtavPfbof_Enabled ;
      private int edtavPflof_Enabled ;
      private int edtavPfbfmultima_Enabled ;
      private int edtavPfbfsultima_Enabled ;
      private int edtavPflfmultima_Enabled ;
      private int edtavPflfsultima_Enabled ;
      private int edtavOldpfb_Enabled ;
      private int edtavOldpfl_Enabled ;
      private int AV27Prestadora_Codigo ;
      private int AV12UserId ;
      private int A1446ContratoGestor_ContratadaAreaCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int A74Contrato_Codigo ;
      private int A1078ContratoGestor_ContratoCod ;
      private int A157ServicoGrupo_Codigo ;
      private int A75Contrato_AreaTrabalhoCod ;
      private int A1825ContratoAuxiliar_UsuarioCod ;
      private int A1824ContratoAuxiliar_ContratoCod ;
      private int A155Servico_Codigo ;
      private int A39Contratada_Codigo ;
      private int AV116ContratoServicos_Codigo ;
      private int AV15AtribuidoA_Codigo ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A1603ContagemResultado_CntCod ;
      private int AV24Contratada_Codigo ;
      private int A2003ContagemResultadoRequisito_OSCod ;
      private int A2004ContagemResultadoRequisito_ReqCod ;
      private int AV59OsVinculada ;
      private int AV165Contrato_Codigo ;
      private int AV65Responsavel_Codigo ;
      private int A891LogResponsavel_UsuarioCod ;
      private int AV187ContagemResultado_Codigo ;
      private int A1013Contrato_PrepostoCod ;
      private int AV22Servico_Codigo ;
      private int AV115ContratoOrigem_Codigo ;
      private int AV66ContratadaOrigem_Codigo ;
      private int AV21Selecionada ;
      private int A456ContagemResultado_Codigo ;
      private int A1604ContagemResultado_CntPrpCod ;
      private int A601ContagemResultado_Servico ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int AV31AreaTrabalho_Codigo ;
      private int A1953Contratada_CntPadrao ;
      private int A160ContratoServicos_Codigo ;
      private int A1749Artefatos_Codigo ;
      private int A892LogResponsavel_DemandaCod ;
      private int A896LogResponsavel_Owner ;
      private int lblTbjava_Visible ;
      private int AV177ServicoGrupo_Codigo ;
      private int AV182NewCntSrvCod ;
      private int AV168CriarOSPara_Codigo ;
      private int AV174Contrato ;
      private int gxdynajaxindex ;
      private int AV188ContratoFS ;
      private int AV183NaoConformidade_Codigo ;
      private int AV40Contratante_Codigo ;
      private int A146Modulo_Codigo ;
      private int A468ContagemResultado_NaoCnfDmnCod ;
      private int A127Sistema_Codigo ;
      private int A489ContagemResultado_SistemaCod ;
      private int A1831Sistema_Responsavel ;
      private int A428NaoConformidade_AreaTrabalhoCod ;
      private int A602ContagemResultado_OSVinculada ;
      private int A890ContagemResultado_Responsavel ;
      private int A805ContagemResultado_ContratadaOrigemCod ;
      private int A1627ContagemResultado_CntSrvVncCod ;
      private int A1622ContagemResultado_CntVncCod ;
      private int A1636ContagemResultado_ServicoSS ;
      private int A1452ContagemResultado_SS ;
      private int A508ContagemResultado_Owner ;
      private int A1829Sistema_GpoObjCtrlCod ;
      private int A1830Sistema_GpoObjCtrlRsp ;
      private int AV47ResponsavelAtual ;
      private int AV143Sistema_Codigo ;
      private int AV130CntSrvCodOrigem_Codigo ;
      private int AV120ServicoSS_Codigo ;
      private int AV123ContagemResultado_SS ;
      private int AV131ContagemResultado_CntCod ;
      private int AV128Owner_Codigo ;
      private int AV148Servico_Responsavel ;
      private int A1219LogResponsavel_OwnerPessoaCod ;
      private int AV153Remetente ;
      private int A758CheckList_Codigo ;
      private int A1868ContagemResultadoChckLst_OSCod ;
      private int AV49Emissor ;
      private int edtavPrazoresposta_Visible ;
      private int tblTblinvisible_Visible ;
      private int tblTblnaoacata_Visible ;
      private int lblTbnaocnf_Visible ;
      private int tblTblanexos_Visible ;
      private int lblTbprazo_Visible ;
      private int bttBtnenter_Visible ;
      private int bttBtnartefatos_Visible ;
      private int lblTextblockcontrato_Visible ;
      private int tblTblcriaros_Visible ;
      private int lblTbcontratofs_Visible ;
      private int lblTbdescricao_Visible ;
      private int edtavDescricao_Visible ;
      private int A70ContratadaUsuario_UsuarioPessoaCod ;
      private int A828UsuarioServicos_UsuarioCod ;
      private int A829UsuarioServicos_ServicoCod ;
      private int A1118Contagem_ContratadaCod ;
      private int A1551Servico_Responsavel ;
      private int A57Usuario_PessoaCod ;
      private int A1Usuario_Codigo ;
      private int A1136ContratoGestor_ContratadaCod ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A61ContratanteUsuario_UsuarioPessoaCod ;
      private int lblTbservico_Visible ;
      private int AV211GXV1 ;
      private int AV215GXV2 ;
      private int A40000ContagemResultado_ContadorFMCod ;
      private int GXt_int15 ;
      private int GXt_int12 ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A1595Contratada_AreaTrbSrvPdr ;
      private int AV243GXV3 ;
      private int AV248GXV4 ;
      private int A40Contratada_PessoaCod ;
      private int AV178Contrato_Padrao ;
      private int idxLst ;
      private long A1797LogResponsavel_Codigo ;
      private long AV185LogResponsavel_Codigo ;
      private decimal A684ContagemResultado_PFBFSUltima ;
      private decimal A798ContagemResultado_PFBFSImp ;
      private decimal AV29Unidades ;
      private decimal AV57PFB ;
      private decimal AV58PFL ;
      private decimal AV70PFBOF ;
      private decimal AV71PFLOF ;
      private decimal AV81PFBFMUltima ;
      private decimal AV83PFBFSUltima ;
      private decimal AV82PFLFMUltima ;
      private decimal AV80PFLFSUltima ;
      private decimal AV63NewPFB ;
      private decimal AV64NewPFL ;
      private decimal AV104OldPFB ;
      private decimal AV105OldPFL ;
      private decimal A685ContagemResultado_PFLFSUltima ;
      private decimal A682ContagemResultado_PFBFMUltima ;
      private decimal A683ContagemResultado_PFLFMUltima ;
      private String AV10Destino_Nome ;
      private String wcpOAV10Destino_Nome ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String AV191Pgmname ;
      private String edtavDescricao_Internalname ;
      private String edtavPfb_Internalname ;
      private String edtavPfl_Internalname ;
      private String edtavPfbof_Internalname ;
      private String edtavPflof_Internalname ;
      private String cmbavStatuscontagem_Internalname ;
      private String edtavPfbfmultima_Internalname ;
      private String edtavPfbfsultima_Internalname ;
      private String edtavPflfmultima_Internalname ;
      private String edtavPflfsultima_Internalname ;
      private String edtavOldpfb_Internalname ;
      private String edtavOldpfl_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV16StatusDemanda ;
      private String AV114Caller ;
      private String AV110Origem_Sigla ;
      private String AV107TipoDias ;
      private String A1858ContratoServicos_Alias ;
      private String AV43NovoStatus ;
      private String AV98StatusDemandaVnc ;
      private String A894LogResponsavel_Acao ;
      private String AV68Resultado ;
      private String A484ContagemResultado_StatusDmn ;
      private String A1611ContagemResultado_PrzTpDias ;
      private String A1130LogResponsavel_Status ;
      private String A1234LogResponsavel_NovoStatus ;
      private String A41Contratada_PessoaNom ;
      private String A77Contrato_Numero ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String lblTbjava_Internalname ;
      private String lblTbjava_Caption ;
      private String lblTbjava_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String WebComp_Webcomp2_Component ;
      private String chkavRemetenteinterno_Internalname ;
      private String cmbavUsuario_codigo_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String edtavPrazoresposta_Internalname ;
      private String dynavContratofs_Internalname ;
      private String cmbavServicogrupo_codigo_Internalname ;
      private String cmbavNewcntsrvcod_Internalname ;
      private String cmbavCriarospara_codigo_Internalname ;
      private String cmbavContrato_Internalname ;
      private String dynavNaoconformidade_codigo_Internalname ;
      private String edtavObservacao_Internalname ;
      private String AV46Deferido ;
      private String radavDeferido_Internalname ;
      private String edtavNewpfb_Internalname ;
      private String edtavNewpfl_Internalname ;
      private String hsh ;
      private String AV67Contratada_Sigla ;
      private String A1593ContagemResultado_CntSrvTpVnc ;
      private String A803ContagemResultado_ContratadaSigla ;
      private String A866ContagemResultado_ContratadaOrigemSigla ;
      private String A1326ContagemResultado_ContratadaTipoFab ;
      private String AV142Prestadora_Sigla ;
      private String AV77TipoFabrica ;
      private String A1222LogResponsavel_OwnerPessoaNom ;
      private String AV149Remetente_Nome ;
      private String lblTbdestino_Caption ;
      private String lblTbdestino_Internalname ;
      private String lblTbobservacoes_Caption ;
      private String lblTbobservacoes_Internalname ;
      private String A762ContagemResultadoChckLst_Cumpre ;
      private String tblTblinvisible_Internalname ;
      private String tblTblnaoacata_Internalname ;
      private String lblTbnaocnf_Internalname ;
      private String tblTblanexos_Internalname ;
      private String lblTbprazo_Internalname ;
      private String bttBtnenter_Internalname ;
      private String bttBtnartefatos_Internalname ;
      private String lblTextblockcontrato_Internalname ;
      private String tblTblcriaros_Internalname ;
      private String lblTbcontratofs_Internalname ;
      private String lblTbdescricao_Internalname ;
      private String bttBtnartefatos_Caption ;
      private String A71ContratadaUsuario_UsuarioPessoaNom ;
      private String GXt_char1 ;
      private String AV129Owner_Nome ;
      private String bttBtnenter_Caption ;
      private String GXt_char4 ;
      private String GXt_char3 ;
      private String GXt_char2 ;
      private String A58Usuario_PessoaNom ;
      private String A1223ContratoGestor_ContratadaSigla ;
      private String A62ContratanteUsuario_UsuarioPessoaNom ;
      private String lblTbdescpfb_Caption ;
      private String lblTbdescpfb_Internalname ;
      private String lblTbdescpfl_Caption ;
      private String lblTbdescpfl_Internalname ;
      private String lblTbservico_Internalname ;
      private String lblTbprazo_Caption ;
      private String AV17Acao ;
      private String GXt_char10 ;
      private String GXt_char9 ;
      private String GXt_char8 ;
      private String GXt_char7 ;
      private String GXt_char6 ;
      private String GXt_char5 ;
      private String GXt_char14 ;
      private String sStyleString ;
      private String TempTags ;
      private String edtavPfb_Jsonclick ;
      private String edtavPfl_Jsonclick ;
      private String edtavPfbof_Jsonclick ;
      private String edtavPflof_Jsonclick ;
      private String cmbavStatuscontagem_Jsonclick ;
      private String edtavPfbfmultima_Jsonclick ;
      private String edtavPfbfsultima_Jsonclick ;
      private String edtavPflfmultima_Jsonclick ;
      private String edtavPflfsultima_Jsonclick ;
      private String edtavOldpfb_Jsonclick ;
      private String edtavOldpfl_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String tblTable1_Internalname ;
      private String lblTbdestino_Jsonclick ;
      private String cmbavUsuario_codigo_Jsonclick ;
      private String lblTbprazo_Jsonclick ;
      private String edtavPrazoresposta_Jsonclick ;
      private String lblTbcontratofs_Jsonclick ;
      private String dynavContratofs_Jsonclick ;
      private String lblTbnaocnf_Jsonclick ;
      private String dynavNaoconformidade_codigo_Jsonclick ;
      private String lblTbdescricao_Jsonclick ;
      private String lblTbobservacoes_Jsonclick ;
      private String radavDeferido_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String bttBtnartefatos_Jsonclick ;
      private String bttBtnenter_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String lblTbdestino5_Internalname ;
      private String lblTbdestino5_Jsonclick ;
      private String lblTbdescpfb_Jsonclick ;
      private String edtavNewpfb_Jsonclick ;
      private String lblTbdescpfl_Jsonclick ;
      private String edtavNewpfl_Jsonclick ;
      private String lblTbservico5_Internalname ;
      private String lblTbservico5_Jsonclick ;
      private String cmbavServicogrupo_codigo_Jsonclick ;
      private String lblTbservico_Jsonclick ;
      private String cmbavNewcntsrvcod_Jsonclick ;
      private String lblTbservico4_Internalname ;
      private String lblTbservico4_Jsonclick ;
      private String cmbavCriarospara_codigo_Jsonclick ;
      private String lblTextblockcontrato_Jsonclick ;
      private String cmbavContrato_Jsonclick ;
      private DateTime AV50PrazoInicial ;
      private DateTime AV45PrazoEntrega ;
      private DateTime AV18PrazoExecucao ;
      private DateTime AV186PrazoAtual ;
      private DateTime AV75PrazoAnalise ;
      private DateTime A912ContagemResultado_HoraEntrega ;
      private DateTime A1351ContagemResultado_DataPrevista ;
      private DateTime A1177LogResponsavel_Prazo ;
      private DateTime AV102HoraEntrega ;
      private DateTime AV180PrazoPrevisto ;
      private DateTime GXt_dtime16 ;
      private DateTime AV92dateTime ;
      private DateTime AV44DataSolicitacao ;
      private DateTime GXt_dtime17 ;
      private DateTime AV51PrazoAnterior ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private DateTime A83Contrato_DataVigenciaTermino ;
      private DateTime A843Contrato_DataFimTA ;
      private DateTime A1869Contrato_DataTermino ;
      private DateTime AV39PrazoResposta ;
      private bool AV11PodeAtribuirDmn ;
      private bool AV38ParaContratante ;
      private bool wcpOAV11PodeAtribuirDmn ;
      private bool wcpOAV38ParaContratante ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool AV97EmDivergencia ;
      private bool AV61UsuarioDaPrestadora ;
      private bool AV8UserEhContratante ;
      private bool AV154EhSolicitacaoSS ;
      private bool A92Contrato_Ativo ;
      private bool A43Contratada_Ativo ;
      private bool A638ContratoServicos_Ativo ;
      private bool A632Servico_Ativo ;
      private bool A1908Usuario_DeFerias ;
      private bool AV184OSEntregue ;
      private bool A1135ContratoGestor_UsuarioEhContratante ;
      private bool A1481Contratada_UsaOSistema ;
      private bool A1148LogResponsavel_UsuarioEhContratante ;
      private bool AV166UsaOSistema ;
      private bool AV113Ok ;
      private bool AV69NovaContagem ;
      private bool AV72EmReuniao ;
      private bool AV74AnaliseImplicito ;
      private bool AV94ExecucaoImplicita ;
      private bool A1149LogResponsavel_OwnerEhContratante ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV151RemetenteInterno ;
      private bool returnInSub ;
      private bool AV152RetornaSS ;
      private bool n146Modulo_Codigo ;
      private bool n468ContagemResultado_NaoCnfDmnCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n1831Sistema_Responsavel ;
      private bool n428NaoConformidade_AreaTrabalhoCod ;
      private bool n429NaoConformidade_Tipo ;
      private bool n92Contrato_Ativo ;
      private bool n457ContagemResultado_Demanda ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n1593ContagemResultado_CntSrvTpVnc ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n601ContagemResultado_Servico ;
      private bool n803ContagemResultado_ContratadaSigla ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n805ContagemResultado_ContratadaOrigemCod ;
      private bool n1627ContagemResultado_CntSrvVncCod ;
      private bool n1622ContagemResultado_CntVncCod ;
      private bool n866ContagemResultado_ContratadaOrigemSigla ;
      private bool n1326ContagemResultado_ContratadaTipoFab ;
      private bool n1611ContagemResultado_PrzTpDias ;
      private bool n1636ContagemResultado_ServicoSS ;
      private bool n1452ContagemResultado_SS ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n494ContagemResultado_Descricao ;
      private bool n83Contrato_DataVigenciaTermino ;
      private bool n1829Sistema_GpoObjCtrlCod ;
      private bool AV78TemVnc ;
      private bool n1219LogResponsavel_OwnerPessoaCod ;
      private bool n39Contratada_Codigo ;
      private bool n1222LogResponsavel_OwnerPessoaNom ;
      private bool n892LogResponsavel_DemandaCod ;
      private bool n1230CheckList_De ;
      private bool AV41AnalisePendencia ;
      private bool n1177LogResponsavel_Prazo ;
      private bool AV101TemGestorOrigem ;
      private bool AV155NotPreferencial ;
      private bool n70ContratadaUsuario_UsuarioPessoaCod ;
      private bool n71ContratadaUsuario_UsuarioPessoaNom ;
      private bool A1394ContratadaUsuario_UsuarioAtivo ;
      private bool n1394ContratadaUsuario_UsuarioAtivo ;
      private bool n1908Usuario_DeFerias ;
      private bool n1118Contagem_ContratadaCod ;
      private bool n945Contagem_Demanda ;
      private bool n58Usuario_PessoaNom ;
      private bool n1136ContratoGestor_ContratadaCod ;
      private bool n1223ContratoGestor_ContratadaSigla ;
      private bool A54Usuario_Ativo ;
      private bool n61ContratanteUsuario_UsuarioPessoaCod ;
      private bool n54Usuario_Ativo ;
      private bool n62ContratanteUsuario_UsuarioPessoaNom ;
      private bool n1447ContratoGestor_ContratadaAreaDes ;
      private bool n1446ContratoGestor_ContratadaAreaCod ;
      private bool AV56SemUsuarioContratante ;
      private bool n891LogResponsavel_UsuarioCod ;
      private bool AV106RetornoParaPrestadora ;
      private bool AV181UpdDataPrevista ;
      private bool n1237ContagemResultado_PrazoMaisDias ;
      private bool n1227ContagemResultado_PrazoInicialDias ;
      private bool n912ContagemResultado_HoraEntrega ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n1351ContagemResultado_DataPrevista ;
      private bool n798ContagemResultado_PFBFSImp ;
      private bool n40000ContagemResultado_ContadorFMCod ;
      private bool n1130LogResponsavel_Status ;
      private bool n1234LogResponsavel_NovoStatus ;
      private bool n1618ContagemResultado_PrzRsp ;
      private bool n1604ContagemResultado_CntPrpCod ;
      private bool n74Contrato_Codigo ;
      private bool n1013Contrato_PrepostoCod ;
      private bool n843Contrato_DataFimTA ;
      private bool n1481Contratada_UsaOSistema ;
      private bool GXt_boolean11 ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n1595Contratada_AreaTrbSrvPdr ;
      private bool n632Servico_Ativo ;
      private bool n157ServicoGrupo_Codigo ;
      private bool n43Contratada_Ativo ;
      private bool n75Contrato_AreaTrabalhoCod ;
      private bool n1858ContratoServicos_Alias ;
      private bool n41Contratada_PessoaNom ;
      private bool AV144ServicoTemArtefatos ;
      private bool n1953Contratada_CntPadrao ;
      private String AV6Observacao ;
      private String AV147Observ ;
      private String A158ServicoGrupo_Descricao ;
      private String A1751Artefatos_Descricao ;
      private String AV140Descricao ;
      private String A457ContagemResultado_Demanda ;
      private String A494ContagemResultado_Descricao ;
      private String AV76Demanda ;
      private String A945Contagem_Demanda ;
      private String A1447ContratoGestor_ContratadaAreaDes ;
      private IGxSession AV30WebSession ;
      private GXWebComponent WebComp_Webcomp2 ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP3_Destino_Nome ;
      private GXCombobox cmbavUsuario_codigo ;
      private GXCombobox dynavContratofs ;
      private GXCombobox cmbavServicogrupo_codigo ;
      private GXCombobox cmbavNewcntsrvcod ;
      private GXCombobox cmbavCriarospara_codigo ;
      private GXCombobox cmbavContrato ;
      private GXCombobox dynavNaoconformidade_codigo ;
      private GXRadio radavDeferido ;
      private GXCombobox cmbavStatuscontagem ;
      private GXCheckbox chkavRemetenteinterno ;
      private IDataStoreProvider pr_default ;
      private int[] H00IZ4_A74Contrato_Codigo ;
      private bool[] H00IZ4_n74Contrato_Codigo ;
      private String[] H00IZ4_A77Contrato_Numero ;
      private int[] H00IZ4_A39Contratada_Codigo ;
      private bool[] H00IZ4_n39Contratada_Codigo ;
      private bool[] H00IZ4_A92Contrato_Ativo ;
      private bool[] H00IZ4_n92Contrato_Ativo ;
      private DateTime[] H00IZ4_A1869Contrato_DataTermino ;
      private int[] H00IZ5_A426NaoConformidade_Codigo ;
      private String[] H00IZ5_A427NaoConformidade_Nome ;
      private short[] H00IZ5_A429NaoConformidade_Tipo ;
      private bool[] H00IZ5_n429NaoConformidade_Tipo ;
      private int[] H00IZ5_A428NaoConformidade_AreaTrabalhoCod ;
      private bool[] H00IZ5_n428NaoConformidade_AreaTrabalhoCod ;
      private int[] H00IZ7_A146Modulo_Codigo ;
      private bool[] H00IZ7_n146Modulo_Codigo ;
      private int[] H00IZ7_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] H00IZ7_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] H00IZ7_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00IZ7_n1553ContagemResultado_CntSrvCod ;
      private int[] H00IZ7_A456ContagemResultado_Codigo ;
      private int[] H00IZ7_A127Sistema_Codigo ;
      private int[] H00IZ7_A489ContagemResultado_SistemaCod ;
      private bool[] H00IZ7_n489ContagemResultado_SistemaCod ;
      private int[] H00IZ7_A1831Sistema_Responsavel ;
      private bool[] H00IZ7_n1831Sistema_Responsavel ;
      private int[] H00IZ7_A428NaoConformidade_AreaTrabalhoCod ;
      private bool[] H00IZ7_n428NaoConformidade_AreaTrabalhoCod ;
      private short[] H00IZ7_A429NaoConformidade_Tipo ;
      private bool[] H00IZ7_n429NaoConformidade_Tipo ;
      private bool[] H00IZ7_A92Contrato_Ativo ;
      private bool[] H00IZ7_n92Contrato_Ativo ;
      private String[] H00IZ7_A457ContagemResultado_Demanda ;
      private bool[] H00IZ7_n457ContagemResultado_Demanda ;
      private String[] H00IZ7_A484ContagemResultado_StatusDmn ;
      private bool[] H00IZ7_n484ContagemResultado_StatusDmn ;
      private String[] H00IZ7_A1593ContagemResultado_CntSrvTpVnc ;
      private bool[] H00IZ7_n1593ContagemResultado_CntSrvTpVnc ;
      private int[] H00IZ7_A602ContagemResultado_OSVinculada ;
      private bool[] H00IZ7_n602ContagemResultado_OSVinculada ;
      private int[] H00IZ7_A890ContagemResultado_Responsavel ;
      private bool[] H00IZ7_n890ContagemResultado_Responsavel ;
      private int[] H00IZ7_A601ContagemResultado_Servico ;
      private bool[] H00IZ7_n601ContagemResultado_Servico ;
      private String[] H00IZ7_A803ContagemResultado_ContratadaSigla ;
      private bool[] H00IZ7_n803ContagemResultado_ContratadaSigla ;
      private int[] H00IZ7_A490ContagemResultado_ContratadaCod ;
      private bool[] H00IZ7_n490ContagemResultado_ContratadaCod ;
      private int[] H00IZ7_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] H00IZ7_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] H00IZ7_A1627ContagemResultado_CntSrvVncCod ;
      private bool[] H00IZ7_n1627ContagemResultado_CntSrvVncCod ;
      private int[] H00IZ7_A1622ContagemResultado_CntVncCod ;
      private bool[] H00IZ7_n1622ContagemResultado_CntVncCod ;
      private String[] H00IZ7_A866ContagemResultado_ContratadaOrigemSigla ;
      private bool[] H00IZ7_n866ContagemResultado_ContratadaOrigemSigla ;
      private String[] H00IZ7_A1326ContagemResultado_ContratadaTipoFab ;
      private bool[] H00IZ7_n1326ContagemResultado_ContratadaTipoFab ;
      private String[] H00IZ7_A1611ContagemResultado_PrzTpDias ;
      private bool[] H00IZ7_n1611ContagemResultado_PrzTpDias ;
      private int[] H00IZ7_A1636ContagemResultado_ServicoSS ;
      private bool[] H00IZ7_n1636ContagemResultado_ServicoSS ;
      private int[] H00IZ7_A1452ContagemResultado_SS ;
      private bool[] H00IZ7_n1452ContagemResultado_SS ;
      private int[] H00IZ7_A1603ContagemResultado_CntCod ;
      private bool[] H00IZ7_n1603ContagemResultado_CntCod ;
      private int[] H00IZ7_A508ContagemResultado_Owner ;
      private String[] H00IZ7_A494ContagemResultado_Descricao ;
      private bool[] H00IZ7_n494ContagemResultado_Descricao ;
      private short[] H00IZ7_A531ContagemResultado_StatusUltCnt ;
      private decimal[] H00IZ7_A684ContagemResultado_PFBFSUltima ;
      private decimal[] H00IZ7_A685ContagemResultado_PFLFSUltima ;
      private decimal[] H00IZ7_A682ContagemResultado_PFBFMUltima ;
      private decimal[] H00IZ7_A683ContagemResultado_PFLFMUltima ;
      private DateTime[] H00IZ7_A83Contrato_DataVigenciaTermino ;
      private bool[] H00IZ7_n83Contrato_DataVigenciaTermino ;
      private int[] H00IZ7_A1829Sistema_GpoObjCtrlCod ;
      private bool[] H00IZ7_n1829Sistema_GpoObjCtrlCod ;
      private int[] H00IZ8_A127Sistema_Codigo ;
      private int[] H00IZ9_A1219LogResponsavel_OwnerPessoaCod ;
      private bool[] H00IZ9_n1219LogResponsavel_OwnerPessoaCod ;
      private int[] H00IZ9_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00IZ9_n1553ContagemResultado_CntSrvCod ;
      private int[] H00IZ9_A1603ContagemResultado_CntCod ;
      private bool[] H00IZ9_n1603ContagemResultado_CntCod ;
      private int[] H00IZ9_A39Contratada_Codigo ;
      private bool[] H00IZ9_n39Contratada_Codigo ;
      private String[] H00IZ9_A1222LogResponsavel_OwnerPessoaNom ;
      private bool[] H00IZ9_n1222LogResponsavel_OwnerPessoaNom ;
      private long[] H00IZ9_A1797LogResponsavel_Codigo ;
      private int[] H00IZ9_A896LogResponsavel_Owner ;
      private int[] H00IZ9_A892LogResponsavel_DemandaCod ;
      private bool[] H00IZ9_n892LogResponsavel_DemandaCod ;
      private short[] H00IZ10_AV150i ;
      private short[] H00IZ11_AV103QtdNaoAcatado ;
      private short[] H00IZ12_A761ContagemResultadoChckLst_Codigo ;
      private int[] H00IZ12_A758CheckList_Codigo ;
      private short[] H00IZ12_A1230CheckList_De ;
      private bool[] H00IZ12_n1230CheckList_De ;
      private String[] H00IZ12_A762ContagemResultadoChckLst_Cumpre ;
      private int[] H00IZ12_A1868ContagemResultadoChckLst_OSCod ;
      private int[] H00IZ13_A892LogResponsavel_DemandaCod ;
      private bool[] H00IZ13_n892LogResponsavel_DemandaCod ;
      private DateTime[] H00IZ13_A1177LogResponsavel_Prazo ;
      private bool[] H00IZ13_n1177LogResponsavel_Prazo ;
      private int[] H00IZ13_A896LogResponsavel_Owner ;
      private long[] H00IZ13_A1797LogResponsavel_Codigo ;
      private int[] H00IZ14_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] H00IZ14_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] H00IZ14_A66ContratadaUsuario_ContratadaCod ;
      private int[] H00IZ14_A69ContratadaUsuario_UsuarioCod ;
      private String[] H00IZ14_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] H00IZ14_n71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] H00IZ14_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] H00IZ14_n1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] H00IZ14_A1908Usuario_DeFerias ;
      private bool[] H00IZ14_n1908Usuario_DeFerias ;
      private int[] H00IZ15_A828UsuarioServicos_UsuarioCod ;
      private int[] H00IZ15_A829UsuarioServicos_ServicoCod ;
      private int[] H00IZ16_A192Contagem_Codigo ;
      private int[] H00IZ16_A1118Contagem_ContratadaCod ;
      private bool[] H00IZ16_n1118Contagem_ContratadaCod ;
      private String[] H00IZ16_A945Contagem_Demanda ;
      private bool[] H00IZ16_n945Contagem_Demanda ;
      private int[] H00IZ17_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] H00IZ17_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] H00IZ17_A69ContratadaUsuario_UsuarioCod ;
      private String[] H00IZ17_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] H00IZ17_n71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] H00IZ17_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] H00IZ17_n1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] H00IZ17_A1908Usuario_DeFerias ;
      private bool[] H00IZ17_n1908Usuario_DeFerias ;
      private int[] H00IZ17_A66ContratadaUsuario_ContratadaCod ;
      private int[] H00IZ18_A828UsuarioServicos_UsuarioCod ;
      private int[] H00IZ18_A829UsuarioServicos_ServicoCod ;
      private int[] H00IZ19_A155Servico_Codigo ;
      private int[] H00IZ20_A57Usuario_PessoaCod ;
      private int[] H00IZ20_A1Usuario_Codigo ;
      private String[] H00IZ20_A58Usuario_PessoaNom ;
      private bool[] H00IZ20_n58Usuario_PessoaNom ;
      private int[] H00IZ21_A1078ContratoGestor_ContratoCod ;
      private int[] H00IZ21_A1136ContratoGestor_ContratadaCod ;
      private bool[] H00IZ21_n1136ContratoGestor_ContratadaCod ;
      private String[] H00IZ21_A1223ContratoGestor_ContratadaSigla ;
      private bool[] H00IZ21_n1223ContratoGestor_ContratadaSigla ;
      private int[] H00IZ22_A61ContratanteUsuario_UsuarioPessoaCod ;
      private bool[] H00IZ22_n61ContratanteUsuario_UsuarioPessoaCod ;
      private bool[] H00IZ22_A54Usuario_Ativo ;
      private bool[] H00IZ22_n54Usuario_Ativo ;
      private bool[] H00IZ22_A1908Usuario_DeFerias ;
      private bool[] H00IZ22_n1908Usuario_DeFerias ;
      private int[] H00IZ22_A60ContratanteUsuario_UsuarioCod ;
      private int[] H00IZ22_A63ContratanteUsuario_ContratanteCod ;
      private String[] H00IZ22_A62ContratanteUsuario_UsuarioPessoaNom ;
      private bool[] H00IZ22_n62ContratanteUsuario_UsuarioPessoaNom ;
      private int[] H00IZ23_A1078ContratoGestor_ContratoCod ;
      private int[] H00IZ23_A1136ContratoGestor_ContratadaCod ;
      private bool[] H00IZ23_n1136ContratoGestor_ContratadaCod ;
      private String[] H00IZ23_A1447ContratoGestor_ContratadaAreaDes ;
      private bool[] H00IZ23_n1447ContratoGestor_ContratadaAreaDes ;
      private int[] H00IZ23_A1079ContratoGestor_UsuarioCod ;
      private int[] H00IZ23_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] H00IZ23_n1446ContratoGestor_ContratadaAreaCod ;
      private int[] H00IZ24_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] H00IZ24_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] H00IZ24_A69ContratadaUsuario_UsuarioCod ;
      private String[] H00IZ24_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] H00IZ24_n71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] H00IZ24_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] H00IZ24_n1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] H00IZ24_A1908Usuario_DeFerias ;
      private bool[] H00IZ24_n1908Usuario_DeFerias ;
      private int[] H00IZ24_A66ContratadaUsuario_ContratadaCod ;
      private int[] H00IZ25_A828UsuarioServicos_UsuarioCod ;
      private int[] H00IZ25_A829UsuarioServicos_ServicoCod ;
      private int[] H00IZ26_A891LogResponsavel_UsuarioCod ;
      private bool[] H00IZ26_n891LogResponsavel_UsuarioCod ;
      private int[] H00IZ26_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] H00IZ26_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] H00IZ26_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00IZ26_n1553ContagemResultado_CntSrvCod ;
      private int[] H00IZ26_A1603ContagemResultado_CntCod ;
      private bool[] H00IZ26_n1603ContagemResultado_CntCod ;
      private bool[] H00IZ26_A1908Usuario_DeFerias ;
      private bool[] H00IZ26_n1908Usuario_DeFerias ;
      private int[] H00IZ26_A490ContagemResultado_ContratadaCod ;
      private bool[] H00IZ26_n490ContagemResultado_ContratadaCod ;
      private int[] H00IZ26_A39Contratada_Codigo ;
      private bool[] H00IZ26_n39Contratada_Codigo ;
      private int[] H00IZ26_A428NaoConformidade_AreaTrabalhoCod ;
      private bool[] H00IZ26_n428NaoConformidade_AreaTrabalhoCod ;
      private short[] H00IZ26_A429NaoConformidade_Tipo ;
      private bool[] H00IZ26_n429NaoConformidade_Tipo ;
      private bool[] H00IZ26_A92Contrato_Ativo ;
      private bool[] H00IZ26_n92Contrato_Ativo ;
      private long[] H00IZ26_A1797LogResponsavel_Codigo ;
      private int[] H00IZ26_A896LogResponsavel_Owner ;
      private int[] H00IZ26_A892LogResponsavel_DemandaCod ;
      private bool[] H00IZ26_n892LogResponsavel_DemandaCod ;
      private DateTime[] H00IZ26_A83Contrato_DataVigenciaTermino ;
      private bool[] H00IZ26_n83Contrato_DataVigenciaTermino ;
      private int[] H00IZ27_A69ContratadaUsuario_UsuarioCod ;
      private int[] H00IZ27_A66ContratadaUsuario_ContratadaCod ;
      private int[] H00IZ28_A1078ContratoGestor_ContratoCod ;
      private int[] H00IZ28_A1079ContratoGestor_UsuarioCod ;
      private int[] H00IZ31_A456ContagemResultado_Codigo ;
      private int[] H00IZ31_A490ContagemResultado_ContratadaCod ;
      private bool[] H00IZ31_n490ContagemResultado_ContratadaCod ;
      private int[] H00IZ31_A601ContagemResultado_Servico ;
      private bool[] H00IZ31_n601ContagemResultado_Servico ;
      private String[] H00IZ31_A484ContagemResultado_StatusDmn ;
      private bool[] H00IZ31_n484ContagemResultado_StatusDmn ;
      private DateTime[] H00IZ31_A471ContagemResultado_DataDmn ;
      private short[] H00IZ31_A1237ContagemResultado_PrazoMaisDias ;
      private bool[] H00IZ31_n1237ContagemResultado_PrazoMaisDias ;
      private short[] H00IZ31_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] H00IZ31_n1227ContagemResultado_PrazoInicialDias ;
      private DateTime[] H00IZ31_A912ContagemResultado_HoraEntrega ;
      private bool[] H00IZ31_n912ContagemResultado_HoraEntrega ;
      private DateTime[] H00IZ31_A472ContagemResultado_DataEntrega ;
      private bool[] H00IZ31_n472ContagemResultado_DataEntrega ;
      private DateTime[] H00IZ31_A1351ContagemResultado_DataPrevista ;
      private bool[] H00IZ31_n1351ContagemResultado_DataPrevista ;
      private String[] H00IZ31_A1611ContagemResultado_PrzTpDias ;
      private bool[] H00IZ31_n1611ContagemResultado_PrzTpDias ;
      private int[] H00IZ31_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00IZ31_n1553ContagemResultado_CntSrvCod ;
      private decimal[] H00IZ31_A798ContagemResultado_PFBFSImp ;
      private bool[] H00IZ31_n798ContagemResultado_PFBFSImp ;
      private short[] H00IZ31_A531ContagemResultado_StatusUltCnt ;
      private decimal[] H00IZ31_A684ContagemResultado_PFBFSUltima ;
      private int[] H00IZ31_A40000ContagemResultado_ContadorFMCod ;
      private bool[] H00IZ31_n40000ContagemResultado_ContadorFMCod ;
      private String[] H00IZ32_A1130LogResponsavel_Status ;
      private bool[] H00IZ32_n1130LogResponsavel_Status ;
      private String[] H00IZ32_A894LogResponsavel_Acao ;
      private long[] H00IZ32_A1797LogResponsavel_Codigo ;
      private int[] H00IZ32_A891LogResponsavel_UsuarioCod ;
      private bool[] H00IZ32_n891LogResponsavel_UsuarioCod ;
      private int[] H00IZ32_A892LogResponsavel_DemandaCod ;
      private bool[] H00IZ32_n892LogResponsavel_DemandaCod ;
      private String[] H00IZ33_A1234LogResponsavel_NovoStatus ;
      private bool[] H00IZ33_n1234LogResponsavel_NovoStatus ;
      private int[] H00IZ33_A892LogResponsavel_DemandaCod ;
      private bool[] H00IZ33_n892LogResponsavel_DemandaCod ;
      private DateTime[] H00IZ33_A1177LogResponsavel_Prazo ;
      private bool[] H00IZ33_n1177LogResponsavel_Prazo ;
      private long[] H00IZ33_A1797LogResponsavel_Codigo ;
      private String[] H00IZ34_A894LogResponsavel_Acao ;
      private DateTime[] H00IZ34_A1177LogResponsavel_Prazo ;
      private bool[] H00IZ34_n1177LogResponsavel_Prazo ;
      private long[] H00IZ34_A1797LogResponsavel_Codigo ;
      private int[] H00IZ34_A896LogResponsavel_Owner ;
      private int[] H00IZ34_A891LogResponsavel_UsuarioCod ;
      private bool[] H00IZ34_n891LogResponsavel_UsuarioCod ;
      private int[] H00IZ34_A892LogResponsavel_DemandaCod ;
      private bool[] H00IZ34_n892LogResponsavel_DemandaCod ;
      private int[] H00IZ35_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00IZ35_n1553ContagemResultado_CntSrvCod ;
      private int[] H00IZ35_A456ContagemResultado_Codigo ;
      private short[] H00IZ35_A1618ContagemResultado_PrzRsp ;
      private bool[] H00IZ35_n1618ContagemResultado_PrzRsp ;
      private String[] H00IZ35_A1611ContagemResultado_PrzTpDias ;
      private bool[] H00IZ35_n1611ContagemResultado_PrzTpDias ;
      private long[] H00IZ36_A1797LogResponsavel_Codigo ;
      private int[] H00IZ36_A896LogResponsavel_Owner ;
      private int[] H00IZ36_A892LogResponsavel_DemandaCod ;
      private bool[] H00IZ36_n892LogResponsavel_DemandaCod ;
      private int[] H00IZ37_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] H00IZ37_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] H00IZ37_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00IZ37_n1553ContagemResultado_CntSrvCod ;
      private int[] H00IZ37_A1603ContagemResultado_CntCod ;
      private bool[] H00IZ37_n1603ContagemResultado_CntCod ;
      private int[] H00IZ37_A39Contratada_Codigo ;
      private bool[] H00IZ37_n39Contratada_Codigo ;
      private int[] H00IZ37_A428NaoConformidade_AreaTrabalhoCod ;
      private bool[] H00IZ37_n428NaoConformidade_AreaTrabalhoCod ;
      private short[] H00IZ37_A429NaoConformidade_Tipo ;
      private bool[] H00IZ37_n429NaoConformidade_Tipo ;
      private bool[] H00IZ37_A92Contrato_Ativo ;
      private bool[] H00IZ37_n92Contrato_Ativo ;
      private int[] H00IZ37_A456ContagemResultado_Codigo ;
      private DateTime[] H00IZ37_A83Contrato_DataVigenciaTermino ;
      private bool[] H00IZ37_n83Contrato_DataVigenciaTermino ;
      private int[] H00IZ38_A1078ContratoGestor_ContratoCod ;
      private int[] H00IZ38_A1079ContratoGestor_UsuarioCod ;
      private int[] H00IZ38_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] H00IZ38_n1446ContratoGestor_ContratadaAreaCod ;
      private int[] H00IZ39_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] H00IZ39_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] H00IZ39_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00IZ39_n1553ContagemResultado_CntSrvCod ;
      private int[] H00IZ39_A1603ContagemResultado_CntCod ;
      private bool[] H00IZ39_n1603ContagemResultado_CntCod ;
      private int[] H00IZ39_A39Contratada_Codigo ;
      private bool[] H00IZ39_n39Contratada_Codigo ;
      private int[] H00IZ39_A428NaoConformidade_AreaTrabalhoCod ;
      private bool[] H00IZ39_n428NaoConformidade_AreaTrabalhoCod ;
      private short[] H00IZ39_A429NaoConformidade_Tipo ;
      private bool[] H00IZ39_n429NaoConformidade_Tipo ;
      private bool[] H00IZ39_A92Contrato_Ativo ;
      private bool[] H00IZ39_n92Contrato_Ativo ;
      private int[] H00IZ39_A456ContagemResultado_Codigo ;
      private int[] H00IZ39_A1604ContagemResultado_CntPrpCod ;
      private bool[] H00IZ39_n1604ContagemResultado_CntPrpCod ;
      private DateTime[] H00IZ39_A83Contrato_DataVigenciaTermino ;
      private bool[] H00IZ39_n83Contrato_DataVigenciaTermino ;
      private int[] H00IZ40_A1078ContratoGestor_ContratoCod ;
      private int[] H00IZ40_A1079ContratoGestor_UsuarioCod ;
      private int[] H00IZ40_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] H00IZ40_n1446ContratoGestor_ContratadaAreaCod ;
      private int[] H00IZ43_A74Contrato_Codigo ;
      private bool[] H00IZ43_n74Contrato_Codigo ;
      private int[] H00IZ43_A39Contratada_Codigo ;
      private bool[] H00IZ43_n39Contratada_Codigo ;
      private bool[] H00IZ43_A92Contrato_Ativo ;
      private bool[] H00IZ43_n92Contrato_Ativo ;
      private int[] H00IZ43_A1013Contrato_PrepostoCod ;
      private bool[] H00IZ43_n1013Contrato_PrepostoCod ;
      private DateTime[] H00IZ43_A843Contrato_DataFimTA ;
      private bool[] H00IZ43_n843Contrato_DataFimTA ;
      private DateTime[] H00IZ43_A83Contrato_DataVigenciaTermino ;
      private bool[] H00IZ43_n83Contrato_DataVigenciaTermino ;
      private int[] H00IZ44_A1078ContratoGestor_ContratoCod ;
      private int[] H00IZ44_A1079ContratoGestor_UsuarioCod ;
      private int[] H00IZ44_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] H00IZ44_n1446ContratoGestor_ContratadaAreaCod ;
      private int[] H00IZ46_A1636ContagemResultado_ServicoSS ;
      private bool[] H00IZ46_n1636ContagemResultado_ServicoSS ;
      private String[] H00IZ46_A484ContagemResultado_StatusDmn ;
      private bool[] H00IZ46_n484ContagemResultado_StatusDmn ;
      private int[] H00IZ46_A490ContagemResultado_ContratadaCod ;
      private bool[] H00IZ46_n490ContagemResultado_ContratadaCod ;
      private int[] H00IZ46_A456ContagemResultado_Codigo ;
      private String[] H00IZ46_A1326ContagemResultado_ContratadaTipoFab ;
      private bool[] H00IZ46_n1326ContagemResultado_ContratadaTipoFab ;
      private short[] H00IZ46_A531ContagemResultado_StatusUltCnt ;
      private decimal[] H00IZ46_A682ContagemResultado_PFBFMUltima ;
      private decimal[] H00IZ46_A683ContagemResultado_PFLFMUltima ;
      private decimal[] H00IZ46_A684ContagemResultado_PFBFSUltima ;
      private decimal[] H00IZ46_A685ContagemResultado_PFLFSUltima ;
      private int[] H00IZ48_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00IZ48_n1553ContagemResultado_CntSrvCod ;
      private int[] H00IZ48_A1636ContagemResultado_ServicoSS ;
      private bool[] H00IZ48_n1636ContagemResultado_ServicoSS ;
      private String[] H00IZ48_A484ContagemResultado_StatusDmn ;
      private bool[] H00IZ48_n484ContagemResultado_StatusDmn ;
      private String[] H00IZ48_A1593ContagemResultado_CntSrvTpVnc ;
      private bool[] H00IZ48_n1593ContagemResultado_CntSrvTpVnc ;
      private int[] H00IZ48_A490ContagemResultado_ContratadaCod ;
      private bool[] H00IZ48_n490ContagemResultado_ContratadaCod ;
      private int[] H00IZ48_A602ContagemResultado_OSVinculada ;
      private bool[] H00IZ48_n602ContagemResultado_OSVinculada ;
      private int[] H00IZ48_A1603ContagemResultado_CntCod ;
      private bool[] H00IZ48_n1603ContagemResultado_CntCod ;
      private String[] H00IZ48_A803ContagemResultado_ContratadaSigla ;
      private bool[] H00IZ48_n803ContagemResultado_ContratadaSigla ;
      private int[] H00IZ48_A456ContagemResultado_Codigo ;
      private String[] H00IZ48_A1326ContagemResultado_ContratadaTipoFab ;
      private bool[] H00IZ48_n1326ContagemResultado_ContratadaTipoFab ;
      private short[] H00IZ48_A531ContagemResultado_StatusUltCnt ;
      private decimal[] H00IZ48_A682ContagemResultado_PFBFMUltima ;
      private decimal[] H00IZ48_A683ContagemResultado_PFLFMUltima ;
      private decimal[] H00IZ48_A684ContagemResultado_PFBFSUltima ;
      private decimal[] H00IZ48_A685ContagemResultado_PFLFSUltima ;
      private int[] H00IZ49_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] H00IZ49_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] H00IZ49_A490ContagemResultado_ContratadaCod ;
      private bool[] H00IZ49_n490ContagemResultado_ContratadaCod ;
      private int[] H00IZ49_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00IZ49_n1553ContagemResultado_CntSrvCod ;
      private int[] H00IZ49_A1603ContagemResultado_CntCod ;
      private bool[] H00IZ49_n1603ContagemResultado_CntCod ;
      private bool[] H00IZ49_A1481Contratada_UsaOSistema ;
      private bool[] H00IZ49_n1481Contratada_UsaOSistema ;
      private int[] H00IZ49_A39Contratada_Codigo ;
      private bool[] H00IZ49_n39Contratada_Codigo ;
      private int[] H00IZ49_A428NaoConformidade_AreaTrabalhoCod ;
      private bool[] H00IZ49_n428NaoConformidade_AreaTrabalhoCod ;
      private short[] H00IZ49_A429NaoConformidade_Tipo ;
      private bool[] H00IZ49_n429NaoConformidade_Tipo ;
      private bool[] H00IZ49_A92Contrato_Ativo ;
      private bool[] H00IZ49_n92Contrato_Ativo ;
      private long[] H00IZ49_A1797LogResponsavel_Codigo ;
      private int[] H00IZ49_A896LogResponsavel_Owner ;
      private int[] H00IZ49_A891LogResponsavel_UsuarioCod ;
      private bool[] H00IZ49_n891LogResponsavel_UsuarioCod ;
      private int[] H00IZ49_A892LogResponsavel_DemandaCod ;
      private bool[] H00IZ49_n892LogResponsavel_DemandaCod ;
      private DateTime[] H00IZ49_A83Contrato_DataVigenciaTermino ;
      private bool[] H00IZ49_n83Contrato_DataVigenciaTermino ;
      private int[] H00IZ50_A69ContratadaUsuario_UsuarioCod ;
      private int[] H00IZ50_A66ContratadaUsuario_ContratadaCod ;
      private bool[] H00IZ51_A1481Contratada_UsaOSistema ;
      private bool[] H00IZ51_n1481Contratada_UsaOSistema ;
      private int[] H00IZ51_A66ContratadaUsuario_ContratadaCod ;
      private int[] H00IZ51_A69ContratadaUsuario_UsuarioCod ;
      private long[] H00IZ52_A1797LogResponsavel_Codigo ;
      private int[] H00IZ52_A896LogResponsavel_Owner ;
      private int[] H00IZ52_A892LogResponsavel_DemandaCod ;
      private bool[] H00IZ52_n892LogResponsavel_DemandaCod ;
      private int[] H00IZ53_A1078ContratoGestor_ContratoCod ;
      private int[] H00IZ53_A1079ContratoGestor_UsuarioCod ;
      private int[] H00IZ53_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] H00IZ53_n1446ContratoGestor_ContratadaAreaCod ;
      private int[] H00IZ54_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] H00IZ54_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] H00IZ54_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00IZ54_n1553ContagemResultado_CntSrvCod ;
      private int[] H00IZ54_A1603ContagemResultado_CntCod ;
      private bool[] H00IZ54_n1603ContagemResultado_CntCod ;
      private int[] H00IZ54_A39Contratada_Codigo ;
      private bool[] H00IZ54_n39Contratada_Codigo ;
      private int[] H00IZ54_A428NaoConformidade_AreaTrabalhoCod ;
      private bool[] H00IZ54_n428NaoConformidade_AreaTrabalhoCod ;
      private short[] H00IZ54_A429NaoConformidade_Tipo ;
      private bool[] H00IZ54_n429NaoConformidade_Tipo ;
      private bool[] H00IZ54_A92Contrato_Ativo ;
      private bool[] H00IZ54_n92Contrato_Ativo ;
      private String[] H00IZ54_A894LogResponsavel_Acao ;
      private long[] H00IZ54_A1797LogResponsavel_Codigo ;
      private int[] H00IZ54_A896LogResponsavel_Owner ;
      private int[] H00IZ54_A892LogResponsavel_DemandaCod ;
      private bool[] H00IZ54_n892LogResponsavel_DemandaCod ;
      private DateTime[] H00IZ54_A83Contrato_DataVigenciaTermino ;
      private bool[] H00IZ54_n83Contrato_DataVigenciaTermino ;
      private int[] H00IZ55_A69ContratadaUsuario_UsuarioCod ;
      private int[] H00IZ55_A66ContratadaUsuario_ContratadaCod ;
      private int[] H00IZ58_A160ContratoServicos_Codigo ;
      private int[] H00IZ58_A74Contrato_Codigo ;
      private bool[] H00IZ58_n74Contrato_Codigo ;
      private int[] H00IZ58_A1013Contrato_PrepostoCod ;
      private bool[] H00IZ58_n1013Contrato_PrepostoCod ;
      private int[] H00IZ58_A39Contratada_Codigo ;
      private bool[] H00IZ58_n39Contratada_Codigo ;
      private bool[] H00IZ58_A92Contrato_Ativo ;
      private bool[] H00IZ58_n92Contrato_Ativo ;
      private int[] H00IZ58_A155Servico_Codigo ;
      private DateTime[] H00IZ58_A843Contrato_DataFimTA ;
      private bool[] H00IZ58_n843Contrato_DataFimTA ;
      private DateTime[] H00IZ58_A83Contrato_DataVigenciaTermino ;
      private bool[] H00IZ58_n83Contrato_DataVigenciaTermino ;
      private int[] H00IZ59_A1078ContratoGestor_ContratoCod ;
      private int[] H00IZ59_A1079ContratoGestor_UsuarioCod ;
      private int[] H00IZ60_A1136ContratoGestor_ContratadaCod ;
      private bool[] H00IZ60_n1136ContratoGestor_ContratadaCod ;
      private int[] H00IZ60_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00IZ60_n52Contratada_AreaTrabalhoCod ;
      private int[] H00IZ60_A1595Contratada_AreaTrbSrvPdr ;
      private bool[] H00IZ60_n1595Contratada_AreaTrbSrvPdr ;
      private int[] H00IZ60_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] H00IZ60_n1446ContratoGestor_ContratadaAreaCod ;
      private int[] H00IZ60_A1079ContratoGestor_UsuarioCod ;
      private bool[] H00IZ60_A632Servico_Ativo ;
      private bool[] H00IZ60_n632Servico_Ativo ;
      private int[] H00IZ60_A157ServicoGrupo_Codigo ;
      private bool[] H00IZ60_n157ServicoGrupo_Codigo ;
      private int[] H00IZ60_A1078ContratoGestor_ContratoCod ;
      private String[] H00IZ60_A158ServicoGrupo_Descricao ;
      private bool[] H00IZ60_A92Contrato_Ativo ;
      private bool[] H00IZ60_n92Contrato_Ativo ;
      private bool[] H00IZ60_A43Contratada_Ativo ;
      private bool[] H00IZ60_n43Contratada_Ativo ;
      private DateTime[] H00IZ60_A83Contrato_DataVigenciaTermino ;
      private bool[] H00IZ60_n83Contrato_DataVigenciaTermino ;
      private int[] H00IZ61_A160ContratoServicos_Codigo ;
      private int[] H00IZ61_A74Contrato_Codigo ;
      private bool[] H00IZ61_n74Contrato_Codigo ;
      private bool[] H00IZ61_A638ContratoServicos_Ativo ;
      private int[] H00IZ62_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00IZ62_n52Contratada_AreaTrabalhoCod ;
      private int[] H00IZ62_A1595Contratada_AreaTrbSrvPdr ;
      private bool[] H00IZ62_n1595Contratada_AreaTrbSrvPdr ;
      private int[] H00IZ62_A75Contrato_AreaTrabalhoCod ;
      private bool[] H00IZ62_n75Contrato_AreaTrabalhoCod ;
      private int[] H00IZ62_A1825ContratoAuxiliar_UsuarioCod ;
      private bool[] H00IZ62_A632Servico_Ativo ;
      private bool[] H00IZ62_n632Servico_Ativo ;
      private int[] H00IZ62_A157ServicoGrupo_Codigo ;
      private bool[] H00IZ62_n157ServicoGrupo_Codigo ;
      private int[] H00IZ62_A1824ContratoAuxiliar_ContratoCod ;
      private String[] H00IZ62_A158ServicoGrupo_Descricao ;
      private int[] H00IZ62_A39Contratada_Codigo ;
      private bool[] H00IZ62_n39Contratada_Codigo ;
      private bool[] H00IZ62_A92Contrato_Ativo ;
      private bool[] H00IZ62_n92Contrato_Ativo ;
      private bool[] H00IZ62_A43Contratada_Ativo ;
      private bool[] H00IZ62_n43Contratada_Ativo ;
      private DateTime[] H00IZ62_A83Contrato_DataVigenciaTermino ;
      private bool[] H00IZ62_n83Contrato_DataVigenciaTermino ;
      private int[] H00IZ63_A160ContratoServicos_Codigo ;
      private int[] H00IZ63_A74Contrato_Codigo ;
      private bool[] H00IZ63_n74Contrato_Codigo ;
      private bool[] H00IZ63_A638ContratoServicos_Ativo ;
      private int[] H00IZ63_A155Servico_Codigo ;
      private int[] H00IZ64_A1136ContratoGestor_ContratadaCod ;
      private bool[] H00IZ64_n1136ContratoGestor_ContratadaCod ;
      private int[] H00IZ64_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00IZ64_n52Contratada_AreaTrabalhoCod ;
      private int[] H00IZ64_A1595Contratada_AreaTrbSrvPdr ;
      private bool[] H00IZ64_n1595Contratada_AreaTrbSrvPdr ;
      private int[] H00IZ64_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] H00IZ64_n1446ContratoGestor_ContratadaAreaCod ;
      private int[] H00IZ64_A1079ContratoGestor_UsuarioCod ;
      private bool[] H00IZ64_A632Servico_Ativo ;
      private bool[] H00IZ64_n632Servico_Ativo ;
      private int[] H00IZ64_A157ServicoGrupo_Codigo ;
      private bool[] H00IZ64_n157ServicoGrupo_Codigo ;
      private int[] H00IZ64_A1078ContratoGestor_ContratoCod ;
      private bool[] H00IZ64_A92Contrato_Ativo ;
      private bool[] H00IZ64_n92Contrato_Ativo ;
      private bool[] H00IZ64_A43Contratada_Ativo ;
      private bool[] H00IZ64_n43Contratada_Ativo ;
      private DateTime[] H00IZ64_A83Contrato_DataVigenciaTermino ;
      private bool[] H00IZ64_n83Contrato_DataVigenciaTermino ;
      private int[] H00IZ65_A160ContratoServicos_Codigo ;
      private int[] H00IZ65_A74Contrato_Codigo ;
      private bool[] H00IZ65_n74Contrato_Codigo ;
      private bool[] H00IZ65_A638ContratoServicos_Ativo ;
      private int[] H00IZ65_A155Servico_Codigo ;
      private String[] H00IZ65_A1858ContratoServicos_Alias ;
      private bool[] H00IZ65_n1858ContratoServicos_Alias ;
      private int[] H00IZ66_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00IZ66_n52Contratada_AreaTrabalhoCod ;
      private int[] H00IZ66_A1595Contratada_AreaTrbSrvPdr ;
      private bool[] H00IZ66_n1595Contratada_AreaTrbSrvPdr ;
      private int[] H00IZ66_A75Contrato_AreaTrabalhoCod ;
      private bool[] H00IZ66_n75Contrato_AreaTrabalhoCod ;
      private int[] H00IZ66_A1825ContratoAuxiliar_UsuarioCod ;
      private bool[] H00IZ66_A632Servico_Ativo ;
      private bool[] H00IZ66_n632Servico_Ativo ;
      private int[] H00IZ66_A157ServicoGrupo_Codigo ;
      private bool[] H00IZ66_n157ServicoGrupo_Codigo ;
      private int[] H00IZ66_A1824ContratoAuxiliar_ContratoCod ;
      private int[] H00IZ66_A39Contratada_Codigo ;
      private bool[] H00IZ66_n39Contratada_Codigo ;
      private bool[] H00IZ66_A92Contrato_Ativo ;
      private bool[] H00IZ66_n92Contrato_Ativo ;
      private bool[] H00IZ66_A43Contratada_Ativo ;
      private bool[] H00IZ66_n43Contratada_Ativo ;
      private DateTime[] H00IZ66_A83Contrato_DataVigenciaTermino ;
      private bool[] H00IZ66_n83Contrato_DataVigenciaTermino ;
      private int[] H00IZ67_A160ContratoServicos_Codigo ;
      private int[] H00IZ67_A74Contrato_Codigo ;
      private bool[] H00IZ67_n74Contrato_Codigo ;
      private bool[] H00IZ67_A638ContratoServicos_Ativo ;
      private int[] H00IZ67_A155Servico_Codigo ;
      private String[] H00IZ67_A1858ContratoServicos_Alias ;
      private bool[] H00IZ67_n1858ContratoServicos_Alias ;
      private int[] H00IZ70_A160ContratoServicos_Codigo ;
      private int[] H00IZ70_A74Contrato_Codigo ;
      private bool[] H00IZ70_n74Contrato_Codigo ;
      private int[] H00IZ70_A40Contratada_PessoaCod ;
      private int[] H00IZ70_A75Contrato_AreaTrabalhoCod ;
      private bool[] H00IZ70_n75Contrato_AreaTrabalhoCod ;
      private int[] H00IZ70_A155Servico_Codigo ;
      private bool[] H00IZ70_A43Contratada_Ativo ;
      private bool[] H00IZ70_n43Contratada_Ativo ;
      private bool[] H00IZ70_A92Contrato_Ativo ;
      private bool[] H00IZ70_n92Contrato_Ativo ;
      private bool[] H00IZ70_A638ContratoServicos_Ativo ;
      private DateTime[] H00IZ70_A83Contrato_DataVigenciaTermino ;
      private bool[] H00IZ70_n83Contrato_DataVigenciaTermino ;
      private int[] H00IZ70_A39Contratada_Codigo ;
      private bool[] H00IZ70_n39Contratada_Codigo ;
      private String[] H00IZ70_A41Contratada_PessoaNom ;
      private bool[] H00IZ70_n41Contratada_PessoaNom ;
      private DateTime[] H00IZ70_A843Contrato_DataFimTA ;
      private bool[] H00IZ70_n843Contrato_DataFimTA ;
      private int[] H00IZ71_A57Usuario_PessoaCod ;
      private int[] H00IZ71_A1Usuario_Codigo ;
      private String[] H00IZ71_A58Usuario_PessoaNom ;
      private bool[] H00IZ71_n58Usuario_PessoaNom ;
      private int[] H00IZ74_A160ContratoServicos_Codigo ;
      private int[] H00IZ74_A39Contratada_Codigo ;
      private bool[] H00IZ74_n39Contratada_Codigo ;
      private bool[] H00IZ74_A92Contrato_Ativo ;
      private bool[] H00IZ74_n92Contrato_Ativo ;
      private int[] H00IZ74_A155Servico_Codigo ;
      private int[] H00IZ74_A74Contrato_Codigo ;
      private bool[] H00IZ74_n74Contrato_Codigo ;
      private DateTime[] H00IZ74_A843Contrato_DataFimTA ;
      private bool[] H00IZ74_n843Contrato_DataFimTA ;
      private DateTime[] H00IZ74_A83Contrato_DataVigenciaTermino ;
      private bool[] H00IZ74_n83Contrato_DataVigenciaTermino ;
      private int[] H00IZ75_A160ContratoServicos_Codigo ;
      private int[] H00IZ75_A1749Artefatos_Codigo ;
      private String[] H00IZ75_A1751Artefatos_Descricao ;
      private int[] H00IZ76_A2005ContagemResultadoRequisito_Codigo ;
      private int[] H00IZ76_A2003ContagemResultadoRequisito_OSCod ;
      private int[] H00IZ76_A2004ContagemResultadoRequisito_ReqCod ;
      private String[] H00IZ79_A77Contrato_Numero ;
      private int[] H00IZ79_A74Contrato_Codigo ;
      private bool[] H00IZ79_n74Contrato_Codigo ;
      private int[] H00IZ79_A39Contratada_Codigo ;
      private bool[] H00IZ79_n39Contratada_Codigo ;
      private bool[] H00IZ79_A92Contrato_Ativo ;
      private bool[] H00IZ79_n92Contrato_Ativo ;
      private int[] H00IZ79_A1953Contratada_CntPadrao ;
      private bool[] H00IZ79_n1953Contratada_CntPadrao ;
      private DateTime[] H00IZ79_A843Contrato_DataFimTA ;
      private bool[] H00IZ79_n843Contrato_DataFimTA ;
      private DateTime[] H00IZ79_A83Contrato_DataVigenciaTermino ;
      private bool[] H00IZ79_n83Contrato_DataVigenciaTermino ;
      private int[] H00IZ80_A74Contrato_Codigo ;
      private bool[] H00IZ80_n74Contrato_Codigo ;
      private bool[] H00IZ80_A638ContratoServicos_Ativo ;
      private int[] H00IZ80_A155Servico_Codigo ;
      private int[] H00IZ80_A160ContratoServicos_Codigo ;
      private bool[] H00IZ83_A92Contrato_Ativo ;
      private bool[] H00IZ83_n92Contrato_Ativo ;
      private DateTime[] H00IZ83_A83Contrato_DataVigenciaTermino ;
      private bool[] H00IZ83_n83Contrato_DataVigenciaTermino ;
      private int[] H00IZ83_A39Contratada_Codigo ;
      private bool[] H00IZ83_n39Contratada_Codigo ;
      private int[] H00IZ83_A74Contrato_Codigo ;
      private bool[] H00IZ83_n74Contrato_Codigo ;
      private DateTime[] H00IZ83_A843Contrato_DataFimTA ;
      private bool[] H00IZ83_n843Contrato_DataFimTA ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV20Selecionadas ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV179Requisitos ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV172Servicos ;
      [ObjectCollection(ItemType=typeof( SdtSDT_ContagemResultadoEvidencias_Arquivo ))]
      private IGxCollection AV87Sdt_ContagemResultadoEvidencias ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Artefatos ))]
      private IGxCollection AV145Sdt_Artefatos ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Codigos ))]
      private IGxCollection AV171SDT_Servicos ;
      private SdtSDT_Artefatos AV146sdt_Artefato ;
      private SdtSDT_Codigos AV170SDT_Servico ;
      private wwpbaseobjects.SdtWWPContext AV53WWPContext ;
   }

   public class wp_tramitacao__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00IZ17( IGxContext context ,
                                              bool AV11PodeAtribuirDmn ,
                                              int A69ContratadaUsuario_UsuarioCod ,
                                              int AV12UserId ,
                                              int AV47ResponsavelAtual ,
                                              bool A1908Usuario_DeFerias ,
                                              bool A1394ContratadaUsuario_UsuarioAtivo ,
                                              int AV24Contratada_Codigo ,
                                              int A66ContratadaUsuario_ContratadaCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int18 ;
         GXv_int18 = new short [3] ;
         Object[] GXv_Object19 ;
         GXv_Object19 = new Object [2] ;
         scmdbuf = "SELECT T2.[Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPesso, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T3.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso, T2.[Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo, T2.[Usuario_DeFerias], T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCo FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratadaUsuario_ContratadaCod] = @AV24Contratada_Codigo)";
         scmdbuf = scmdbuf + " and (T1.[ContratadaUsuario_UsuarioCod] <> @AV47ResponsavelAtual)";
         scmdbuf = scmdbuf + " and (Not T2.[Usuario_DeFerias] = 1)";
         scmdbuf = scmdbuf + " and (T2.[Usuario_Ativo] = 1)";
         if ( ! AV11PodeAtribuirDmn )
         {
            sWhereString = sWhereString + " and (T1.[ContratadaUsuario_UsuarioCod] = @AV12UserId)";
         }
         else
         {
            GXv_int18[2] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( AV11PodeAtribuirDmn )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContratadaUsuario_ContratadaCod], T3.[Pessoa_Nome]";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContratadaUsuario_ContratadaCod], T1.[ContratadaUsuario_UsuarioCod]";
         }
         GXv_Object19[0] = scmdbuf;
         GXv_Object19[1] = GXv_int18;
         return GXv_Object19 ;
      }

      protected Object[] conditional_H00IZ22( IGxContext context ,
                                              bool AV11PodeAtribuirDmn ,
                                              int A60ContratanteUsuario_UsuarioCod ,
                                              int AV12UserId ,
                                              int AV47ResponsavelAtual ,
                                              bool A1908Usuario_DeFerias ,
                                              bool A54Usuario_Ativo ,
                                              int A63ContratanteUsuario_ContratanteCod ,
                                              int AV40Contratante_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int20 ;
         GXv_int20 = new short [3] ;
         Object[] GXv_Object21 ;
         GXv_Object21 = new Object [2] ;
         scmdbuf = "SELECT T2.[Usuario_PessoaCod] AS ContratanteUsuario_UsuarioPess, T2.[Usuario_Ativo], T2.[Usuario_DeFerias], T1.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, T1.[ContratanteUsuario_ContratanteCod], T3.[Pessoa_Nome] AS ContratanteUsuario_UsuarioPess FROM (([ContratanteUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratanteUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratanteUsuario_UsuarioCod] <> @AV47ResponsavelAtual)";
         scmdbuf = scmdbuf + " and (Not T2.[Usuario_DeFerias] = 1)";
         scmdbuf = scmdbuf + " and (T2.[Usuario_Ativo] = 1)";
         scmdbuf = scmdbuf + " and (T1.[ContratanteUsuario_ContratanteCod] = @AV40Contratante_Codigo)";
         if ( ! AV11PodeAtribuirDmn )
         {
            sWhereString = sWhereString + " and (T1.[ContratanteUsuario_UsuarioCod] = @AV12UserId)";
         }
         else
         {
            GXv_int20[2] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( AV11PodeAtribuirDmn )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContratanteUsuario_ContratanteCod], T3.[Pessoa_Nome]";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContratanteUsuario_ContratanteCod], T1.[ContratanteUsuario_UsuarioCod]";
         }
         GXv_Object21[0] = scmdbuf;
         GXv_Object21[1] = GXv_int20;
         return GXv_Object21 ;
      }

      protected Object[] conditional_H00IZ23( IGxContext context ,
                                              bool AV154EhSolicitacaoSS ,
                                              int A1136ContratoGestor_ContratadaCod ,
                                              int AV27Prestadora_Codigo ,
                                              int A1079ContratoGestor_UsuarioCod ,
                                              int AV128Owner_Codigo ,
                                              bool A1135ContratoGestor_UsuarioEhContratante )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int22 ;
         GXv_int22 = new short [2] ;
         Object[] GXv_Object23 ;
         GXv_Object23 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T2.[Contratada_Codigo] AS ContratoGestor_ContratadaCod, T3.[AreaTrabalho_Descricao] AS ContratoGestor_ContratadaAreaDes, T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM (([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) LEFT JOIN [AreaTrabalho] T3 WITH (NOLOCK) ON T3.[AreaTrabalho_Codigo] = T2.[Contrato_AreaTrabalhoCod])";
         if ( ! AV154EhSolicitacaoSS )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contratada_Codigo] = @AV27Prestadora_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contratada_Codigo] = @AV27Prestadora_Codigo)";
            }
         }
         else
         {
            GXv_int22[0] = 1;
         }
         if ( AV154EhSolicitacaoSS )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGestor_UsuarioCod] = @AV128Owner_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGestor_UsuarioCod] = @AV128Owner_Codigo)";
            }
         }
         else
         {
            GXv_int22[1] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ! AV154EhSolicitacaoSS )
         {
            scmdbuf = scmdbuf + " ORDER BY T2.[Contratada_Codigo]";
         }
         else if ( AV154EhSolicitacaoSS )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContratoGestor_UsuarioCod]";
         }
         GXv_Object23[0] = scmdbuf;
         GXv_Object23[1] = GXv_int22;
         return GXv_Object23 ;
      }

      protected Object[] conditional_H00IZ24( IGxContext context ,
                                              String AV114Caller ,
                                              bool AV11PodeAtribuirDmn ,
                                              int A66ContratadaUsuario_ContratadaCod ,
                                              int AV24Contratada_Codigo ,
                                              int AV27Prestadora_Codigo ,
                                              int A69ContratadaUsuario_UsuarioCod ,
                                              int AV12UserId ,
                                              int AV47ResponsavelAtual ,
                                              bool A1908Usuario_DeFerias ,
                                              bool A1394ContratadaUsuario_UsuarioAtivo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int24 ;
         GXv_int24 = new short [4] ;
         Object[] GXv_Object25 ;
         GXv_Object25 = new Object [2] ;
         scmdbuf = "SELECT T2.[Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPesso, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T3.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso, T2.[Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo, T2.[Usuario_DeFerias], T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCo FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratadaUsuario_UsuarioCod] <> @AV47ResponsavelAtual)";
         scmdbuf = scmdbuf + " and (Not T2.[Usuario_DeFerias] = 1)";
         scmdbuf = scmdbuf + " and (T2.[Usuario_Ativo] = 1)";
         if ( ! ( StringUtil.StrCmp(AV114Caller, "Gestao") == 0 ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratadaUsuario_ContratadaCod] = @AV24Contratada_Codigo)";
         }
         else
         {
            GXv_int24[1] = 1;
         }
         if ( StringUtil.StrCmp(AV114Caller, "Gestao") == 0 )
         {
            sWhereString = sWhereString + " and (T1.[ContratadaUsuario_ContratadaCod] = @AV27Prestadora_Codigo)";
         }
         else
         {
            GXv_int24[2] = 1;
         }
         if ( ! AV11PodeAtribuirDmn )
         {
            sWhereString = sWhereString + " and (T1.[ContratadaUsuario_UsuarioCod] = @AV12UserId)";
         }
         else
         {
            GXv_int24[3] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( AV11PodeAtribuirDmn )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContratadaUsuario_ContratadaCod], T3.[Pessoa_Nome]";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContratadaUsuario_ContratadaCod], T1.[ContratadaUsuario_UsuarioCod]";
         }
         GXv_Object25[0] = scmdbuf;
         GXv_Object25[1] = GXv_int24;
         return GXv_Object25 ;
      }

      protected Object[] conditional_H00IZ55( IGxContext context ,
                                              bool AV61UsuarioDaPrestadora ,
                                              int A66ContratadaUsuario_ContratadaCod ,
                                              int AV66ContratadaOrigem_Codigo ,
                                              int AV27Prestadora_Codigo ,
                                              int A896LogResponsavel_Owner ,
                                              int A69ContratadaUsuario_UsuarioCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int26 ;
         GXv_int26 = new short [3] ;
         Object[] GXv_Object27 ;
         GXv_Object27 = new Object [2] ;
         scmdbuf = "SELECT TOP 1 [ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, [ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCo FROM [ContratadaUsuario] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([ContratadaUsuario_UsuarioCod] = @LogResponsavel_Owner)";
         if ( AV61UsuarioDaPrestadora )
         {
            sWhereString = sWhereString + " and ([ContratadaUsuario_ContratadaCod] = @AV66ContratadaOrigem_Codigo)";
         }
         else
         {
            GXv_int26[1] = 1;
         }
         if ( ! AV61UsuarioDaPrestadora )
         {
            sWhereString = sWhereString + " and ([ContratadaUsuario_ContratadaCod] = @AV27Prestadora_Codigo)";
         }
         else
         {
            GXv_int26[2] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [ContratadaUsuario_UsuarioCod]";
         GXv_Object27[0] = scmdbuf;
         GXv_Object27[1] = GXv_int26;
         return GXv_Object27 ;
      }

      protected Object[] conditional_H00IZ58( IGxContext context ,
                                              bool AV61UsuarioDaPrestadora ,
                                              int A74Contrato_Codigo ,
                                              int AV115ContratoOrigem_Codigo ,
                                              int A39Contratada_Codigo ,
                                              int AV27Prestadora_Codigo ,
                                              int A155Servico_Codigo ,
                                              int AV22Servico_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int28 ;
         GXv_int28 = new short [3] ;
         Object[] GXv_Object29 ;
         GXv_Object29 = new Object [2] ;
         scmdbuf = "SELECT TOP 1 T1.[ContratoServicos_Codigo], T1.[Contrato_Codigo], T2.[Contrato_PrepostoCod], T2.[Contratada_Codigo], T2.[Contrato_Ativo], T1.[Servico_Codigo], COALESCE( T3.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) AS Contrato_DataFimTA, T2.[Contrato_DataVigenciaTermino] FROM (([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) LEFT JOIN (SELECT T4.[ContratoTermoAditivo_DataFim], T4.[Contrato_Codigo], T4.[ContratoTermoAditivo_Codigo], T5.[GXC10] AS GXC10 FROM ([ContratoTermoAditivo] T4 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC10, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T5 ON T5.[Contrato_Codigo] = T4.[Contrato_Codigo]) WHERE T4.[ContratoTermoAditivo_Codigo] = T5.[GXC10] ) T3 ON T3.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         if ( AV61UsuarioDaPrestadora )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] = @AV115ContratoOrigem_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] = @AV115ContratoOrigem_Codigo)";
            }
         }
         else
         {
            GXv_int28[0] = 1;
         }
         if ( ! AV61UsuarioDaPrestadora )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contratada_Codigo] = @AV27Prestadora_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contratada_Codigo] = @AV27Prestadora_Codigo)";
            }
         }
         else
         {
            GXv_int28[1] = 1;
         }
         if ( ! AV61UsuarioDaPrestadora )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Codigo] = @AV22Servico_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Codigo] = @AV22Servico_Codigo)";
            }
         }
         else
         {
            GXv_int28[2] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoServicos_Codigo]";
         GXv_Object29[0] = scmdbuf;
         GXv_Object29[1] = GXv_int28;
         return GXv_Object29 ;
      }

      protected Object[] conditional_H00IZ59( IGxContext context ,
                                              int A1013Contrato_PrepostoCod ,
                                              int A1078ContratoGestor_ContratoCod ,
                                              int A74Contrato_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int30 ;
         GXv_int30 = new short [1] ;
         Object[] GXv_Object31 ;
         GXv_Object31 = new Object [2] ;
         scmdbuf = "SELECT TOP 1 [ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, [ContratoGestor_UsuarioCod] FROM [ContratoGestor] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([ContratoGestor_ContratoCod] = @Contrato_Codigo)";
         if ( ! (0==A1013Contrato_PrepostoCod) )
         {
            sWhereString = sWhereString + " and ([ContratoGestor_ContratoCod] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [ContratoGestor_ContratoCod]";
         GXv_Object31[0] = scmdbuf;
         GXv_Object31[1] = GXv_int30;
         return GXv_Object31 ;
      }

      protected Object[] conditional_H00IZ61( IGxContext context ,
                                              int A157ServicoGrupo_Codigo ,
                                              IGxCollection AV172Servicos ,
                                              bool A638ContratoServicos_Ativo ,
                                              bool A632Servico_Ativo ,
                                              int A1078ContratoGestor_ContratoCod ,
                                              int A74Contrato_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int32 ;
         GXv_int32 = new short [2] ;
         Object[] GXv_Object33 ;
         GXv_Object33 = new Object [2] ;
         scmdbuf = "SELECT [ContratoServicos_Codigo], [Contrato_Codigo], [ContratoServicos_Ativo] FROM [ContratoServicos] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Contrato_Codigo] = @ContratoGestor_ContratoCod)";
         scmdbuf = scmdbuf + " and ([ContratoServicos_Ativo] = 1)";
         scmdbuf = scmdbuf + " and (@Servico_Ativo = 1)";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [Contrato_Codigo]";
         GXv_Object33[0] = scmdbuf;
         GXv_Object33[1] = GXv_int32;
         return GXv_Object33 ;
      }

      protected Object[] conditional_H00IZ63( IGxContext context ,
                                              int A157ServicoGrupo_Codigo ,
                                              IGxCollection AV172Servicos ,
                                              bool A638ContratoServicos_Ativo ,
                                              bool A632Servico_Ativo ,
                                              int A1824ContratoAuxiliar_ContratoCod ,
                                              int A74Contrato_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int34 ;
         GXv_int34 = new short [2] ;
         Object[] GXv_Object35 ;
         GXv_Object35 = new Object [2] ;
         scmdbuf = "SELECT [ContratoServicos_Codigo], [Contrato_Codigo], [ContratoServicos_Ativo], [Servico_Codigo] FROM [ContratoServicos] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Contrato_Codigo] = @ContratoAuxiliar_ContratoCod)";
         scmdbuf = scmdbuf + " and ([ContratoServicos_Ativo] = 1)";
         scmdbuf = scmdbuf + " and (@Servico_Ativo = 1)";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [Contrato_Codigo]";
         GXv_Object35[0] = scmdbuf;
         GXv_Object35[1] = GXv_int34;
         return GXv_Object35 ;
      }

      protected Object[] conditional_H00IZ65( IGxContext context ,
                                              int A155Servico_Codigo ,
                                              IGxCollection AV172Servicos ,
                                              int AV177ServicoGrupo_Codigo ,
                                              int A157ServicoGrupo_Codigo ,
                                              bool A638ContratoServicos_Ativo ,
                                              bool A632Servico_Ativo ,
                                              int A1078ContratoGestor_ContratoCod ,
                                              int A74Contrato_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int36 ;
         GXv_int36 = new short [2] ;
         Object[] GXv_Object37 ;
         GXv_Object37 = new Object [2] ;
         scmdbuf = "SELECT [ContratoServicos_Codigo], [Contrato_Codigo], [ContratoServicos_Ativo], [Servico_Codigo], [ContratoServicos_Alias] FROM [ContratoServicos] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Contrato_Codigo] = @ContratoGestor_ContratoCod)";
         scmdbuf = scmdbuf + " and (Not " + new GxDbmsUtils( new GxSqlServer()).ValueList(AV172Servicos, "[Servico_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and ([ContratoServicos_Ativo] = 1)";
         scmdbuf = scmdbuf + " and (@Servico_Ativo = 1)";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [Contrato_Codigo]";
         GXv_Object37[0] = scmdbuf;
         GXv_Object37[1] = GXv_int36;
         return GXv_Object37 ;
      }

      protected Object[] conditional_H00IZ67( IGxContext context ,
                                              int A155Servico_Codigo ,
                                              IGxCollection AV172Servicos ,
                                              int AV177ServicoGrupo_Codigo ,
                                              int A157ServicoGrupo_Codigo ,
                                              bool A638ContratoServicos_Ativo ,
                                              bool A632Servico_Ativo ,
                                              int A1824ContratoAuxiliar_ContratoCod ,
                                              int A74Contrato_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int38 ;
         GXv_int38 = new short [2] ;
         Object[] GXv_Object39 ;
         GXv_Object39 = new Object [2] ;
         scmdbuf = "SELECT [ContratoServicos_Codigo], [Contrato_Codigo], [ContratoServicos_Ativo], [Servico_Codigo], [ContratoServicos_Alias] FROM [ContratoServicos] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Contrato_Codigo] = @ContratoAuxiliar_ContratoCod)";
         scmdbuf = scmdbuf + " and (Not " + new GxDbmsUtils( new GxSqlServer()).ValueList(AV172Servicos, "[Servico_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and ([ContratoServicos_Ativo] = 1)";
         scmdbuf = scmdbuf + " and (@Servico_Ativo = 1)";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [Contrato_Codigo]";
         GXv_Object39[0] = scmdbuf;
         GXv_Object39[1] = GXv_int38;
         return GXv_Object39 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 12 :
                     return conditional_H00IZ17(context, (bool)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (bool)dynConstraints[4] , (bool)dynConstraints[5] , (int)dynConstraints[6] , (int)dynConstraints[7] );
               case 17 :
                     return conditional_H00IZ22(context, (bool)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (bool)dynConstraints[4] , (bool)dynConstraints[5] , (int)dynConstraints[6] , (int)dynConstraints[7] );
               case 18 :
                     return conditional_H00IZ23(context, (bool)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (bool)dynConstraints[5] );
               case 19 :
                     return conditional_H00IZ24(context, (String)dynConstraints[0] , (bool)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (int)dynConstraints[6] , (int)dynConstraints[7] , (bool)dynConstraints[8] , (bool)dynConstraints[9] );
               case 44 :
                     return conditional_H00IZ55(context, (bool)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] );
               case 45 :
                     return conditional_H00IZ58(context, (bool)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (int)dynConstraints[6] );
               case 46 :
                     return conditional_H00IZ59(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] );
               case 48 :
                     return conditional_H00IZ61(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (bool)dynConstraints[2] , (bool)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] );
               case 50 :
                     return conditional_H00IZ63(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (bool)dynConstraints[2] , (bool)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] );
               case 52 :
                     return conditional_H00IZ65(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (bool)dynConstraints[4] , (bool)dynConstraints[5] , (int)dynConstraints[6] , (int)dynConstraints[7] );
               case 54 :
                     return conditional_H00IZ67(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (bool)dynConstraints[4] , (bool)dynConstraints[5] , (int)dynConstraints[6] , (int)dynConstraints[7] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
         ,new ForEachCursor(def[27])
         ,new ForEachCursor(def[28])
         ,new ForEachCursor(def[29])
         ,new ForEachCursor(def[30])
         ,new ForEachCursor(def[31])
         ,new ForEachCursor(def[32])
         ,new ForEachCursor(def[33])
         ,new ForEachCursor(def[34])
         ,new ForEachCursor(def[35])
         ,new ForEachCursor(def[36])
         ,new ForEachCursor(def[37])
         ,new ForEachCursor(def[38])
         ,new ForEachCursor(def[39])
         ,new ForEachCursor(def[40])
         ,new ForEachCursor(def[41])
         ,new ForEachCursor(def[42])
         ,new ForEachCursor(def[43])
         ,new ForEachCursor(def[44])
         ,new ForEachCursor(def[45])
         ,new ForEachCursor(def[46])
         ,new ForEachCursor(def[47])
         ,new ForEachCursor(def[48])
         ,new ForEachCursor(def[49])
         ,new ForEachCursor(def[50])
         ,new ForEachCursor(def[51])
         ,new ForEachCursor(def[52])
         ,new ForEachCursor(def[53])
         ,new ForEachCursor(def[54])
         ,new ForEachCursor(def[55])
         ,new ForEachCursor(def[56])
         ,new ForEachCursor(def[57])
         ,new ForEachCursor(def[58])
         ,new ForEachCursor(def[59])
         ,new ForEachCursor(def[60])
         ,new ForEachCursor(def[61])
         ,new ForEachCursor(def[62])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00IZ4 ;
          prmH00IZ4 = new Object[] {
          new Object[] {"@AV5Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ5 ;
          prmH00IZ5 = new Object[] {
          new Object[] {"@AV53WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ7 ;
          prmH00IZ7 = new Object[] {
          new Object[] {"@AV9Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferH00IZ7 ;
          cmdBufferH00IZ7=" SELECT T1.[Modulo_Codigo], T1.[ContagemResultado_NaoCnfDmnCod] AS ContagemResultado_NaoCnfDmnCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Codigo], T2.[Sistema_Codigo], T1.[ContagemResultado_SistemaCod], T3.[Sistema_Responsavel], T4.[NaoConformidade_AreaTrabalhoCod], T4.[NaoConformidade_Tipo], T6.[Contrato_Ativo], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_StatusDmn], T5.[ContratoServicos_TipoVnc] AS ContagemResultado_CntSrvTpVnc, T1.[ContagemResultado_OSVinculada] AS ContagemResultado_OSVinculada, T1.[ContagemResultado_Responsavel], T5.[Servico_Codigo] AS ContagemResultado_Servico, T10.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_ContratadaOrigemCod] AS ContagemResultado_ContratadaOr, T8.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvVncCod, T9.[Contrato_Codigo] AS ContagemResultado_CntVncCod, T11.[Contratada_Sigla] AS ContagemResultado_ContratadaOr, T10.[Contratada_TipoFabrica] AS ContagemResultado_ContratadaTipoFab, T5.[ContratoServicos_PrazoTpDias] AS ContagemResultado_PrzTpDias, T1.[ContagemResultado_ServicoSS], T1.[ContagemResultado_SS], T5.[Contrato_Codigo] AS ContagemResultado_CntCod, T1.[ContagemResultado_Owner], T1.[ContagemResultado_Descricao], COALESCE( T7.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt, COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) AS ContagemResultado_PFLFSUltima, COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) AS ContagemResultado_PFLFMUltima, T6.[Contrato_DataVigenciaTermino], T3.[Sistema_GpoObjCtrlCod] "
          + " FROM (((((((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Modulo] T2 WITH (NOLOCK) ON T2.[Modulo_Codigo] = T1.[Modulo_Codigo]) LEFT JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T2.[Sistema_Codigo]) LEFT JOIN [NaoConformidade] T4 WITH (NOLOCK) ON T4.[NaoConformidade_Codigo] = T1.[ContagemResultado_NaoCnfDmnCod]) LEFT JOIN [ContratoServicos] T5 WITH (NOLOCK) ON T5.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contrato] T6 WITH (NOLOCK) ON T6.[Contrato_Codigo] = T5.[Contrato_Codigo]) LEFT JOIN (SELECT MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt, [ContagemResultado_Codigo], MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, MIN([ContagemResultado_PFLFS]) AS ContagemResultado_PFLFSUltima, MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, MIN([ContagemResultado_PFLFM]) AS ContagemResultado_PFLFMUltima FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T7 ON T7.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [ContagemResultado] T8 WITH (NOLOCK) ON T8.[ContagemResultado_Codigo] = T1.[ContagemResultado_OSVinculada]) LEFT JOIN [ContratoServicos] T9 WITH (NOLOCK) ON T9.[ContratoServicos_Codigo] = T8.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contratada] T10 WITH (NOLOCK) ON T10.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [Contratada] T11 WITH (NOLOCK) ON T11.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaOrigemCod]) WHERE T1.[ContagemResultado_Codigo] = @AV9Codigo ORDER BY T1.[ContagemResultado_Codigo]" ;
          Object[] prmH00IZ8 ;
          prmH00IZ8 = new Object[] {
          new Object[] {"@ContagemResultado_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ9 ;
          prmH00IZ9 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ10 ;
          prmH00IZ10 = new Object[] {
          new Object[] {"@AV53WWPC_2Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@LogResponsavel_Owner",SqlDbType.Int,6,0} ,
          new Object[] {"@AV53WWPContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00IZ11 ;
          prmH00IZ11 = new Object[] {
          new Object[] {"@AV9Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ12 ;
          prmH00IZ12 = new Object[] {
          new Object[] {"@AV9Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ13 ;
          prmH00IZ13 = new Object[] {
          new Object[] {"@AV9Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ14 ;
          prmH00IZ14 = new Object[] {
          new Object[] {"@AV24Contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV47ResponsavelAtual",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ15 ;
          prmH00IZ15 = new Object[] {
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV22Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ16 ;
          prmH00IZ16 = new Object[] {
          new Object[] {"@AV76Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV27Prestadora_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ18 ;
          prmH00IZ18 = new Object[] {
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV22Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ19 ;
          prmH00IZ19 = new Object[] {
          new Object[] {"@AV120ServicoSS_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ20 ;
          prmH00IZ20 = new Object[] {
          new Object[] {"@Servico_Responsavel",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ21 ;
          prmH00IZ21 = new Object[] {
          new Object[] {"@AV27Prestadora_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV66ContratadaOrigem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ25 ;
          prmH00IZ25 = new Object[] {
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV22Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ26 ;
          prmH00IZ26 = new Object[] {
          new Object[] {"@AV9Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ27 ;
          prmH00IZ27 = new Object[] {
          new Object[] {"@LogResponsavel_Owner",SqlDbType.Int,6,0} ,
          new Object[] {"@AV5Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ28 ;
          prmH00IZ28 = new Object[] {
          new Object[] {"@ContagemResultado_CntCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ31 ;
          prmH00IZ31 = new Object[] {
          new Object[] {"@AV21Selecionada",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ32 ;
          prmH00IZ32 = new Object[] {
          new Object[] {"@AV21Selecionada",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ33 ;
          prmH00IZ33 = new Object[] {
          new Object[] {"@AV21Selecionada",SqlDbType.Int,6,0} ,
          new Object[] {"@AV43NovoStatus",SqlDbType.Char,1,0}
          } ;
          Object[] prmH00IZ34 ;
          prmH00IZ34 = new Object[] {
          new Object[] {"@AV21Selecionada",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ35 ;
          prmH00IZ35 = new Object[] {
          new Object[] {"@AV21Selecionada",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ36 ;
          prmH00IZ36 = new Object[] {
          new Object[] {"@AV21Selecionada",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ37 ;
          prmH00IZ37 = new Object[] {
          new Object[] {"@AV21Selecionada",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ38 ;
          prmH00IZ38 = new Object[] {
          new Object[] {"@ContagemResultado_CntCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ39 ;
          prmH00IZ39 = new Object[] {
          new Object[] {"@AV21Selecionada",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ40 ;
          prmH00IZ40 = new Object[] {
          new Object[] {"@ContagemResultado_CntCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ43 ;
          prmH00IZ43 = new Object[] {
          new Object[] {"@AV188ContratoFS",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ44 ;
          prmH00IZ44 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ46 ;
          prmH00IZ46 = new Object[] {
          new Object[] {"@AV59OsVinculada",SqlDbType.Int,6,0} ,
          new Object[] {"@AV27Prestadora_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferH00IZ46 ;
          cmdBufferH00IZ46=" SELECT TOP 1 T1.[ContagemResultado_ServicoSS], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_Codigo], T2.[Contratada_TipoFabrica] AS ContagemResultado_ContratadaTipoFab, COALESCE( T3.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt, COALESCE( T3.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T3.[ContagemResultado_PFLFMUltima], 0) AS ContagemResultado_PFLFMUltima, COALESCE( T3.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T3.[ContagemResultado_PFLFSUltima], 0) AS ContagemResultado_PFLFSUltima FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt, [ContagemResultado_Codigo], MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, MIN([ContagemResultado_PFLFM]) AS ContagemResultado_PFLFMUltima, MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, MIN([ContagemResultado_PFLFS]) AS ContagemResultado_PFLFSUltima FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T3 ON T3.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE (T1.[ContagemResultado_Codigo] = @AV59OsVinculada) AND (T1.[ContagemResultado_ContratadaCod] <> @AV27Prestadora_Codigo) AND (Not ( T1.[ContagemResultado_StatusDmn] = 'D' or T1.[ContagemResultado_StatusDmn] = 'H' or T1.[ContagemResultado_StatusDmn] = 'O' or T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L')) AND (T1.[ContagemResultado_ServicoSS] IS NULL) AND (COALESCE( T3.[ContagemResultado_StatusUltCnt], "
          + " 0) = 7) ORDER BY T1.[ContagemResultado_Codigo]" ;
          Object[] prmH00IZ48 ;
          prmH00IZ48 = new Object[] {
          new Object[] {"@AV9Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV27Prestadora_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferH00IZ48 ;
          cmdBufferH00IZ48=" SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_ServicoSS], T1.[ContagemResultado_StatusDmn], T2.[ContratoServicos_TipoVnc] AS ContagemResultado_CntSrvTpVnc, T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_OSVinculada] AS ContagemResultado_OSVinculada, T2.[Contrato_Codigo] AS ContagemResultado_CntCod, T3.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, T1.[ContagemResultado_Codigo], T3.[Contratada_TipoFabrica] AS ContagemResultado_ContratadaTipoFab, COALESCE( T4.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt, COALESCE( T4.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T4.[ContagemResultado_PFLFMUltima], 0) AS ContagemResultado_PFLFMUltima, COALESCE( T4.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T4.[ContagemResultado_PFLFSUltima], 0) AS ContagemResultado_PFLFSUltima FROM ((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt, [ContagemResultado_Codigo], MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, MIN([ContagemResultado_PFLFM]) AS ContagemResultado_PFLFMUltima, MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, MIN([ContagemResultado_PFLFS]) AS ContagemResultado_PFLFSUltima FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T4 ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) "
          + " WHERE (T1.[ContagemResultado_OSVinculada] = @AV9Codigo) AND (T1.[ContagemResultado_ContratadaCod] <> @AV27Prestadora_Codigo) AND (T2.[ContratoServicos_TipoVnc] = 'C' or T2.[ContratoServicos_TipoVnc] = 'A') AND (Not ( T1.[ContagemResultado_StatusDmn] = 'D' or T1.[ContagemResultado_StatusDmn] = 'H' or T1.[ContagemResultado_StatusDmn] = 'O' or T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L')) AND (T1.[ContagemResultado_ServicoSS] IS NULL) AND (COALESCE( T4.[ContagemResultado_StatusUltCnt], 0) = 7) ORDER BY T1.[ContagemResultado_OSVinculada]" ;
          Object[] prmH00IZ49 ;
          prmH00IZ49 = new Object[] {
          new Object[] {"@AV187ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ50 ;
          prmH00IZ50 = new Object[] {
          new Object[] {"@AV66ContratadaOrigem_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Contratada_UsaOSistema",SqlDbType.Bit,4,0} ,
          new Object[] {"@LogResponsavel_Owner",SqlDbType.Int,6,0} ,
          new Object[] {"@LogResponsavel_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ51 ;
          prmH00IZ51 = new Object[] {
          new Object[] {"@AV66ContratadaOrigem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ52 ;
          prmH00IZ52 = new Object[] {
          new Object[] {"@AV9Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ53 ;
          prmH00IZ53 = new Object[] {
          new Object[] {"@AV165Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ54 ;
          prmH00IZ54 = new Object[] {
          new Object[] {"@AV9Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ60 ;
          prmH00IZ60 = new Object[] {
          new Object[] {"@AV53WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV53WWPContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00IZ62 ;
          prmH00IZ62 = new Object[] {
          new Object[] {"@AV53WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV53WWPContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00IZ64 ;
          prmH00IZ64 = new Object[] {
          new Object[] {"@AV53WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV53WWPContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00IZ66 ;
          prmH00IZ66 = new Object[] {
          new Object[] {"@AV53WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV53WWPContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00IZ70 ;
          prmH00IZ70 = new Object[] {
          new Object[] {"@AV53WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV182NewCntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ71 ;
          prmH00IZ71 = new Object[] {
          new Object[] {"@AV128Owner_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ74 ;
          prmH00IZ74 = new Object[] {
          new Object[] {"@AV174Contrato",SqlDbType.Int,6,0} ,
          new Object[] {"@AV182NewCntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ75 ;
          prmH00IZ75 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ76 ;
          prmH00IZ76 = new Object[] {
          new Object[] {"@AV9Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ79 ;
          prmH00IZ79 = new Object[] {
          new Object[] {"@AV168CriarOSPara_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ80 ;
          prmH00IZ80 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV182NewCntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ83 ;
          prmH00IZ83 = new Object[] {
          new Object[] {"@AV5Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ17 ;
          prmH00IZ17 = new Object[] {
          new Object[] {"@AV24Contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV47ResponsavelAtual",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12UserId",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ22 ;
          prmH00IZ22 = new Object[] {
          new Object[] {"@AV47ResponsavelAtual",SqlDbType.Int,6,0} ,
          new Object[] {"@AV40Contratante_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12UserId",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ23 ;
          prmH00IZ23 = new Object[] {
          new Object[] {"@AV27Prestadora_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV128Owner_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ24 ;
          prmH00IZ24 = new Object[] {
          new Object[] {"@AV47ResponsavelAtual",SqlDbType.Int,6,0} ,
          new Object[] {"@AV24Contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV27Prestadora_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12UserId",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ55 ;
          prmH00IZ55 = new Object[] {
          new Object[] {"@LogResponsavel_Owner",SqlDbType.Int,6,0} ,
          new Object[] {"@AV66ContratadaOrigem_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV27Prestadora_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ58 ;
          prmH00IZ58 = new Object[] {
          new Object[] {"@AV115ContratoOrigem_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV27Prestadora_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV22Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ59 ;
          prmH00IZ59 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00IZ61 ;
          prmH00IZ61 = new Object[] {
          new Object[] {"@ContratoGestor_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_Ativo",SqlDbType.Bit,4,0}
          } ;
          Object[] prmH00IZ63 ;
          prmH00IZ63 = new Object[] {
          new Object[] {"@ContratoAuxiliar_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_Ativo",SqlDbType.Bit,4,0}
          } ;
          Object[] prmH00IZ65 ;
          prmH00IZ65 = new Object[] {
          new Object[] {"@ContratoGestor_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_Ativo",SqlDbType.Bit,4,0}
          } ;
          Object[] prmH00IZ67 ;
          prmH00IZ67 = new Object[] {
          new Object[] {"@ContratoAuxiliar_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_Ativo",SqlDbType.Bit,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00IZ4", "SELECT T1.[Contrato_Codigo], T1.[Contrato_Numero], T1.[Contratada_Codigo], T1.[Contrato_Ativo], CASE  WHEN T1.[Contrato_DataVigenciaTermino] > COALESCE( T2.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) THEN T1.[Contrato_DataVigenciaTermino] ELSE COALESCE( T2.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) END AS Contrato_DataTermino FROM ([Contrato] T1 WITH (NOLOCK) LEFT JOIN (SELECT T3.[ContratoTermoAditivo_DataFim], T3.[Contrato_Codigo], T3.[ContratoTermoAditivo_Codigo], T4.[GXC10] AS GXC10 FROM ([ContratoTermoAditivo] T3 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC10, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T4 ON T4.[Contrato_Codigo] = T3.[Contrato_Codigo]) WHERE T3.[ContratoTermoAditivo_Codigo] = T4.[GXC10] ) T2 ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE (T1.[Contratada_Codigo] = ( @AV5Usuario_Codigo - 900000)) AND (T1.[Contrato_Ativo] = 1) ORDER BY T1.[Contrato_Numero] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ4,0,0,true,false )
             ,new CursorDef("H00IZ5", "SELECT [NaoConformidade_Codigo], [NaoConformidade_Nome], [NaoConformidade_Tipo], [NaoConformidade_AreaTrabalhoCod] FROM [NaoConformidade] WITH (NOLOCK) WHERE ([NaoConformidade_Tipo] = 1) AND ([NaoConformidade_AreaTrabalhoCod] = @AV53WWPC_1Areatrabalho_codigo) ORDER BY [NaoConformidade_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ5,0,0,true,false )
             ,new CursorDef("H00IZ7", cmdBufferH00IZ7,false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ7,1,0,true,true )
             ,new CursorDef("H00IZ8", "SELECT TOP 1 [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @ContagemResultado_SistemaCod ORDER BY [Sistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ8,1,0,false,true )
             ,new CursorDef("H00IZ9", "SELECT TOP 1 T2.[Usuario_PessoaCod] AS LogResponsavel_OwnerPessoaCod, T4.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T5.[Contrato_Codigo] AS ContagemResultado_CntCod, T6.[Contratada_Codigo], T3.[Pessoa_Nome] AS LogResponsavel_OwnerPessoaNom, T1.[LogResponsavel_Codigo], T1.[LogResponsavel_Owner] AS LogResponsavel_Owner, T1.[LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod FROM ((((([LogResponsavel] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[LogResponsavel_Owner]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) LEFT JOIN [ContagemResultado] T4 WITH (NOLOCK) ON T4.[ContagemResultado_Codigo] = T1.[LogResponsavel_DemandaCod]) LEFT JOIN [ContratoServicos] T5 WITH (NOLOCK) ON T5.[ContratoServicos_Codigo] = T4.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contrato] T6 WITH (NOLOCK) ON T6.[Contrato_Codigo] = T5.[Contrato_Codigo]) WHERE T1.[LogResponsavel_DemandaCod] = @ContagemResultado_Codigo ORDER BY T1.[LogResponsavel_DemandaCod], T1.[LogResponsavel_Codigo] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ9,1,0,true,true )
             ,new CursorDef("H00IZ10", "SELECT COUNT(*) FROM [ContratadaUsuario] WITH (NOLOCK) WHERE ([ContratadaUsuario_ContratadaCod] = @AV53WWPC_2Contratada_codigo) AND ([ContratadaUsuario_UsuarioCod] = @LogResponsavel_Owner or [ContratadaUsuario_UsuarioCod] = @AV53WWPContext__Userid) ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ10,1,0,true,false )
             ,new CursorDef("H00IZ11", "SELECT COUNT(*) FROM [LogResponsavel] WITH (NOLOCK) WHERE ([LogResponsavel_DemandaCod] = @AV9Codigo) AND ([LogResponsavel_Acao] = 'N') ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ11,1,0,true,false )
             ,new CursorDef("H00IZ12", "SELECT TOP 1 T1.[ContagemResultadoChckLst_Codigo], T1.[CheckList_Codigo], T2.[CheckList_De], T1.[ContagemResultadoChckLst_Cumpre], T1.[ContagemResultadoChckLst_OSCod] FROM ([ContagemResultadoChckLst] T1 WITH (NOLOCK) INNER JOIN [CheckList] T2 WITH (NOLOCK) ON T2.[CheckList_Codigo] = T1.[CheckList_Codigo]) WHERE (T1.[ContagemResultadoChckLst_OSCod] = @AV9Codigo) AND (T1.[ContagemResultadoChckLst_Cumpre] = 'N') AND (T2.[CheckList_De] = 2) ORDER BY T1.[ContagemResultadoChckLst_OSCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ12,1,0,false,true )
             ,new CursorDef("H00IZ13", "SELECT TOP 1 [LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod, [LogResponsavel_Prazo], [LogResponsavel_Owner] AS LogResponsavel_Owner, [LogResponsavel_Codigo] FROM [LogResponsavel] WITH (NOLOCK) WHERE [LogResponsavel_DemandaCod] = @AV9Codigo ORDER BY [LogResponsavel_DemandaCod], [LogResponsavel_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ13,1,0,false,true )
             ,new CursorDef("H00IZ14", "SELECT T2.[Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPesso, T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCo, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T3.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso, T2.[Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo, T2.[Usuario_DeFerias] FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE (T1.[ContratadaUsuario_ContratadaCod] = @AV24Contratada_Codigo) AND (T1.[ContratadaUsuario_UsuarioCod] <> @AV47ResponsavelAtual) AND (Not T2.[Usuario_DeFerias] = 1) AND (T2.[Usuario_Ativo] = 1) ORDER BY T1.[ContratadaUsuario_ContratadaCod], T3.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ14,100,0,true,false )
             ,new CursorDef("H00IZ15", "SELECT [UsuarioServicos_UsuarioCod], [UsuarioServicos_ServicoCod] FROM [UsuarioServicos] WITH (NOLOCK) WHERE [UsuarioServicos_UsuarioCod] = @ContratadaUsuario_UsuarioCod and [UsuarioServicos_ServicoCod] = @AV22Servico_Codigo ORDER BY [UsuarioServicos_UsuarioCod], [UsuarioServicos_ServicoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ15,1,0,true,true )
             ,new CursorDef("H00IZ16", "SELECT TOP 1 [Contagem_Codigo], [Contagem_ContratadaCod], [Contagem_Demanda] FROM [Contagem] WITH (NOLOCK) WHERE [Contagem_Demanda] = @AV76Demanda and [Contagem_ContratadaCod] = @AV27Prestadora_Codigo ORDER BY [Contagem_Demanda], [Contagem_ContratadaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ16,1,0,false,true )
             ,new CursorDef("H00IZ17", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ17,100,0,true,false )
             ,new CursorDef("H00IZ18", "SELECT TOP 1 [UsuarioServicos_UsuarioCod], [UsuarioServicos_ServicoCod] FROM [UsuarioServicos] WITH (NOLOCK) WHERE [UsuarioServicos_UsuarioCod] = @ContratadaUsuario_UsuarioCod and [UsuarioServicos_ServicoCod] = @AV22Servico_Codigo ORDER BY [UsuarioServicos_UsuarioCod], [UsuarioServicos_ServicoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ18,1,0,true,true )
             ,new CursorDef("H00IZ19", "SELECT TOP 1 [Servico_Codigo] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @AV120ServicoSS_Codigo ORDER BY [Servico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ19,1,0,true,true )
             ,new CursorDef("H00IZ20", "SELECT TOP 1 T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Usuario_Codigo], T2.[Pessoa_Nome] AS Usuario_PessoaNom FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) WHERE T1.[Usuario_Codigo] = @Servico_Responsavel ORDER BY T1.[Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ20,1,0,false,true )
             ,new CursorDef("H00IZ21", "SELECT DISTINCT NULL AS [ContratoGestor_ContratoCod], [ContratoGestor_ContratadaCod], [ContratoGestor_ContratadaSigla] FROM ( SELECT TOP(100) PERCENT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T2.[Contratada_Codigo] AS ContratoGestor_ContratadaCod, T3.[Contratada_Sigla] AS ContratoGestor_ContratadaSigla FROM (([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) WHERE T2.[Contratada_Codigo] = @AV27Prestadora_Codigo or T2.[Contratada_Codigo] = @AV66ContratadaOrigem_Codigo ORDER BY T2.[Contratada_Codigo]) DistinctT ORDER BY [ContratoGestor_ContratadaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ21,100,0,true,false )
             ,new CursorDef("H00IZ22", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ22,100,0,true,false )
             ,new CursorDef("H00IZ23", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ23,100,0,true,false )
             ,new CursorDef("H00IZ24", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ24,100,0,true,false )
             ,new CursorDef("H00IZ25", "SELECT TOP 1 [UsuarioServicos_UsuarioCod], [UsuarioServicos_ServicoCod] FROM [UsuarioServicos] WITH (NOLOCK) WHERE [UsuarioServicos_UsuarioCod] = @ContratadaUsuario_UsuarioCod and [UsuarioServicos_ServicoCod] = @AV22Servico_Codigo ORDER BY [UsuarioServicos_UsuarioCod], [UsuarioServicos_ServicoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ25,1,0,true,true )
             ,new CursorDef("H00IZ26", "SELECT T1.[LogResponsavel_UsuarioCod] AS LogResponsavel_UsuarioCod, T3.[ContagemResultado_NaoCnfDmnCod] AS ContagemResultado_NaoCnfDmnCod, T3.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T5.[Contrato_Codigo] AS ContagemResultado_CntCod, T2.[Usuario_DeFerias], T3.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T6.[Contratada_Codigo], T4.[NaoConformidade_AreaTrabalhoCod], T4.[NaoConformidade_Tipo], T6.[Contrato_Ativo], T1.[LogResponsavel_Codigo], T1.[LogResponsavel_Owner] AS LogResponsavel_Owner, T1.[LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod, T6.[Contrato_DataVigenciaTermino] FROM ((((([LogResponsavel] T1 WITH (NOLOCK) LEFT JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[LogResponsavel_UsuarioCod]) LEFT JOIN [ContagemResultado] T3 WITH (NOLOCK) ON T3.[ContagemResultado_Codigo] = T1.[LogResponsavel_DemandaCod]) LEFT JOIN [NaoConformidade] T4 WITH (NOLOCK) ON T4.[NaoConformidade_Codigo] = T3.[ContagemResultado_NaoCnfDmnCod]) LEFT JOIN [ContratoServicos] T5 WITH (NOLOCK) ON T5.[ContratoServicos_Codigo] = T3.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contrato] T6 WITH (NOLOCK) ON T6.[Contrato_Codigo] = T5.[Contrato_Codigo]) WHERE T1.[LogResponsavel_DemandaCod] = @AV9Codigo ORDER BY T1.[LogResponsavel_DemandaCod], T1.[LogResponsavel_Codigo] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ26,100,0,true,false )
             ,new CursorDef("H00IZ27", "SELECT TOP 1 [ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, [ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCo FROM [ContratadaUsuario] WITH (NOLOCK) WHERE ([ContratadaUsuario_UsuarioCod] = @LogResponsavel_Owner) AND ([ContratadaUsuario_ContratadaCod] = ( @AV5Usuario_Codigo - 900000)) ORDER BY [ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ27,1,0,true,true )
             ,new CursorDef("H00IZ28", "SELECT TOP 1 [ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, [ContratoGestor_UsuarioCod] FROM [ContratoGestor] WITH (NOLOCK) WHERE [ContratoGestor_ContratoCod] = @ContagemResultado_CntCod ORDER BY [ContratoGestor_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ28,1,0,false,true )
             ,new CursorDef("H00IZ31", "SELECT TOP 1 T1.[ContagemResultado_Codigo], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T4.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_PrazoMaisDias], T1.[ContagemResultado_PrazoInicialDias], T1.[ContagemResultado_HoraEntrega], T1.[ContagemResultado_DataEntrega], T1.[ContagemResultado_DataPrevista], T4.[ContratoServicos_PrazoTpDias] AS ContagemResultado_PrzTpDias, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_PFBFSImp], COALESCE( T2.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt, COALESCE( T2.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T3.[ContagemResultado_ContadorFMCod], 0) AS ContagemResultado_ContadorFMCod FROM ((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt, [ContagemResultado_Codigo], MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T2 ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN (SELECT MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFMCod, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) GROUP BY [ContagemResultado_Codigo] ) T3 ON T3.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [ContratoServicos] T4 WITH (NOLOCK) ON T4.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) WHERE T1.[ContagemResultado_Codigo] = @AV21Selecionada ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ31,1,0,false,true )
             ,new CursorDef("H00IZ32", "SELECT [LogResponsavel_Status], [LogResponsavel_Acao], [LogResponsavel_Codigo], [LogResponsavel_UsuarioCod] AS LogResponsavel_UsuarioCod, [LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod FROM [LogResponsavel] WITH (NOLOCK) WHERE ([LogResponsavel_DemandaCod] = @AV21Selecionada) AND ([LogResponsavel_Status] <> 'D') AND ([LogResponsavel_Acao] = 'R') ORDER BY [LogResponsavel_DemandaCod], [LogResponsavel_Codigo] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ32,100,0,true,false )
             ,new CursorDef("H00IZ33", "SELECT TOP 1 [LogResponsavel_NovoStatus], [LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod, [LogResponsavel_Prazo], [LogResponsavel_Codigo] FROM [LogResponsavel] WITH (NOLOCK) WHERE ([LogResponsavel_DemandaCod] = @AV21Selecionada) AND ([LogResponsavel_NovoStatus] = @AV43NovoStatus) ORDER BY [LogResponsavel_DemandaCod], [LogResponsavel_Codigo] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ33,1,0,false,true )
             ,new CursorDef("H00IZ34", "SELECT TOP 1 [LogResponsavel_Acao], [LogResponsavel_Prazo], [LogResponsavel_Codigo], [LogResponsavel_Owner] AS LogResponsavel_Owner, [LogResponsavel_UsuarioCod] AS LogResponsavel_UsuarioCod, [LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod FROM [LogResponsavel] WITH (NOLOCK) WHERE ([LogResponsavel_DemandaCod] = @AV21Selecionada) AND ([LogResponsavel_Acao] <> 'A') ORDER BY [LogResponsavel_DemandaCod], [LogResponsavel_Codigo] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ34,1,0,true,true )
             ,new CursorDef("H00IZ35", "SELECT TOP 1 T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Codigo], T2.[ContratoServicos_PrazoResposta] AS ContagemResultado_PrzRsp, T2.[ContratoServicos_PrazoTpDias] AS ContagemResultado_PrzTpDias FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) WHERE T1.[ContagemResultado_Codigo] = @AV21Selecionada ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ35,1,0,false,true )
             ,new CursorDef("H00IZ36", "SELECT [LogResponsavel_Codigo], [LogResponsavel_Owner] AS LogResponsavel_Owner, [LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod FROM [LogResponsavel] WITH (NOLOCK) WHERE [LogResponsavel_DemandaCod] = @AV21Selecionada ORDER BY [LogResponsavel_DemandaCod], [LogResponsavel_Codigo] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ36,100,0,true,false )
             ,new CursorDef("H00IZ37", "SELECT TOP 1 T1.[ContagemResultado_NaoCnfDmnCod] AS ContagemResultado_NaoCnfDmnCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T3.[Contrato_Codigo] AS ContagemResultado_CntCod, T4.[Contratada_Codigo], T2.[NaoConformidade_AreaTrabalhoCod], T2.[NaoConformidade_Tipo], T4.[Contrato_Ativo], T1.[ContagemResultado_Codigo], T4.[Contrato_DataVigenciaTermino] FROM ((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [NaoConformidade] T2 WITH (NOLOCK) ON T2.[NaoConformidade_Codigo] = T1.[ContagemResultado_NaoCnfDmnCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contrato] T4 WITH (NOLOCK) ON T4.[Contrato_Codigo] = T3.[Contrato_Codigo]) WHERE T1.[ContagemResultado_Codigo] = @AV21Selecionada ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ37,1,0,true,true )
             ,new CursorDef("H00IZ38", "SELECT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) WHERE T1.[ContratoGestor_ContratoCod] = @ContagemResultado_CntCod ORDER BY T1.[ContratoGestor_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ38,100,0,true,false )
             ,new CursorDef("H00IZ39", "SELECT TOP 1 T1.[ContagemResultado_NaoCnfDmnCod] AS ContagemResultado_NaoCnfDmnCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T3.[Contrato_Codigo] AS ContagemResultado_CntCod, T4.[Contratada_Codigo], T2.[NaoConformidade_AreaTrabalhoCod], T2.[NaoConformidade_Tipo], T4.[Contrato_Ativo], T1.[ContagemResultado_Codigo], T4.[Contrato_PrepostoCod] AS ContagemResultado_CntPrpCod, T4.[Contrato_DataVigenciaTermino] FROM ((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [NaoConformidade] T2 WITH (NOLOCK) ON T2.[NaoConformidade_Codigo] = T1.[ContagemResultado_NaoCnfDmnCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contrato] T4 WITH (NOLOCK) ON T4.[Contrato_Codigo] = T3.[Contrato_Codigo]) WHERE T1.[ContagemResultado_Codigo] = @AV21Selecionada ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ39,1,0,true,true )
             ,new CursorDef("H00IZ40", "SELECT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) WHERE T1.[ContratoGestor_ContratoCod] = @ContagemResultado_CntCod ORDER BY T1.[ContratoGestor_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ40,100,0,true,false )
             ,new CursorDef("H00IZ43", "SELECT TOP 1 T1.[Contrato_Codigo], T1.[Contratada_Codigo], T1.[Contrato_Ativo], T1.[Contrato_PrepostoCod], COALESCE( T2.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) AS Contrato_DataFimTA, T1.[Contrato_DataVigenciaTermino] FROM ([Contrato] T1 WITH (NOLOCK) LEFT JOIN (SELECT T3.[ContratoTermoAditivo_DataFim], T3.[Contrato_Codigo], T3.[ContratoTermoAditivo_Codigo], T4.[GXC10] AS GXC10 FROM ([ContratoTermoAditivo] T3 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC10, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T4 ON T4.[Contrato_Codigo] = T3.[Contrato_Codigo]) WHERE T3.[ContratoTermoAditivo_Codigo] = T4.[GXC10] ) T2 ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE T1.[Contrato_Codigo] = @AV188ContratoFS ORDER BY T1.[Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ43,1,0,true,true )
             ,new CursorDef("H00IZ44", "SELECT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) WHERE T1.[ContratoGestor_ContratoCod] = @Contrato_Codigo ORDER BY T1.[ContratoGestor_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ44,100,0,true,false )
             ,new CursorDef("H00IZ46", cmdBufferH00IZ46,false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ46,1,0,false,true )
             ,new CursorDef("H00IZ48", cmdBufferH00IZ48,false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ48,100,0,false,false )
             ,new CursorDef("H00IZ49", "SELECT T2.[ContagemResultado_NaoCnfDmnCod] AS ContagemResultado_NaoCnfDmnCod, T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T5.[Contrato_Codigo] AS ContagemResultado_CntCod, T4.[Contratada_UsaOSistema], T6.[Contratada_Codigo], T3.[NaoConformidade_AreaTrabalhoCod], T3.[NaoConformidade_Tipo], T6.[Contrato_Ativo], T1.[LogResponsavel_Codigo], T1.[LogResponsavel_Owner] AS LogResponsavel_Owner, T1.[LogResponsavel_UsuarioCod] AS LogResponsavel_UsuarioCod, T1.[LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod, T6.[Contrato_DataVigenciaTermino] FROM ((((([LogResponsavel] T1 WITH (NOLOCK) LEFT JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[LogResponsavel_DemandaCod]) LEFT JOIN [NaoConformidade] T3 WITH (NOLOCK) ON T3.[NaoConformidade_Codigo] = T2.[ContagemResultado_NaoCnfDmnCod]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod]) LEFT JOIN [ContratoServicos] T5 WITH (NOLOCK) ON T5.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contrato] T6 WITH (NOLOCK) ON T6.[Contrato_Codigo] = T5.[Contrato_Codigo]) WHERE T1.[LogResponsavel_DemandaCod] = @AV187ContagemResultado_Codigo ORDER BY T1.[LogResponsavel_DemandaCod], T1.[LogResponsavel_Codigo] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ49,100,0,true,false )
             ,new CursorDef("H00IZ50", "SELECT TOP 1 [ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, [ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCo FROM [ContratadaUsuario] WITH (NOLOCK) WHERE ([ContratadaUsuario_ContratadaCod] = @AV66ContratadaOrigem_Codigo) AND (@Contratada_UsaOSistema = 1) AND ([ContratadaUsuario_UsuarioCod] = @LogResponsavel_Owner or [ContratadaUsuario_UsuarioCod] = @LogResponsavel_UsuarioCod) ORDER BY [ContratadaUsuario_ContratadaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ50,1,0,false,true )
             ,new CursorDef("H00IZ51", "SELECT TOP 1 T2.[Contratada_UsaOSistema], T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCo, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod FROM ([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) WHERE (T1.[ContratadaUsuario_ContratadaCod] = @AV66ContratadaOrigem_Codigo) AND (T2.[Contratada_UsaOSistema] = 1) ORDER BY T1.[ContratadaUsuario_ContratadaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ51,1,0,false,true )
             ,new CursorDef("H00IZ52", "SELECT [LogResponsavel_Codigo], [LogResponsavel_Owner] AS LogResponsavel_Owner, [LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod FROM [LogResponsavel] WITH (NOLOCK) WHERE [LogResponsavel_DemandaCod] = @AV9Codigo ORDER BY [LogResponsavel_DemandaCod], [LogResponsavel_Codigo] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ52,100,0,true,false )
             ,new CursorDef("H00IZ53", "SELECT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) WHERE T1.[ContratoGestor_ContratoCod] = @AV165Contrato_Codigo ORDER BY T1.[ContratoGestor_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ53,100,0,true,false )
             ,new CursorDef("H00IZ54", "SELECT T2.[ContagemResultado_NaoCnfDmnCod] AS ContagemResultado_NaoCnfDmnCod, T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T4.[Contrato_Codigo] AS ContagemResultado_CntCod, T5.[Contratada_Codigo], T3.[NaoConformidade_AreaTrabalhoCod], T3.[NaoConformidade_Tipo], T5.[Contrato_Ativo], T1.[LogResponsavel_Acao], T1.[LogResponsavel_Codigo], T1.[LogResponsavel_Owner] AS LogResponsavel_Owner, T1.[LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod, T5.[Contrato_DataVigenciaTermino] FROM (((([LogResponsavel] T1 WITH (NOLOCK) LEFT JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[LogResponsavel_DemandaCod]) LEFT JOIN [NaoConformidade] T3 WITH (NOLOCK) ON T3.[NaoConformidade_Codigo] = T2.[ContagemResultado_NaoCnfDmnCod]) LEFT JOIN [ContratoServicos] T4 WITH (NOLOCK) ON T4.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contrato] T5 WITH (NOLOCK) ON T5.[Contrato_Codigo] = T4.[Contrato_Codigo]) WHERE (T1.[LogResponsavel_DemandaCod] = @AV9Codigo) AND (T1.[LogResponsavel_Acao] = 'D' or T1.[LogResponsavel_Acao] = 'N') ORDER BY T1.[LogResponsavel_DemandaCod], T1.[LogResponsavel_Codigo] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ54,100,0,true,false )
             ,new CursorDef("H00IZ55", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ55,1,0,false,true )
             ,new CursorDef("H00IZ58", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ58,1,0,true,true )
             ,new CursorDef("H00IZ59", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ59,1,0,false,true )
             ,new CursorDef("H00IZ60", "SELECT T2.[Contratada_Codigo] AS ContratoGestor_ContratadaCod, T3.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T4.[AreaTrabalho_ServicoPadrao] AS Contratada_AreaTrbSrvPdr, T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod, T1.[ContratoGestor_UsuarioCod], T5.[Servico_Ativo], T5.[ServicoGrupo_Codigo], T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T6.[ServicoGrupo_Descricao], T2.[Contrato_Ativo], T3.[Contratada_Ativo], T2.[Contrato_DataVigenciaTermino] FROM ((((([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) LEFT JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = T3.[Contratada_AreaTrabalhoCod]) LEFT JOIN [Servico] T5 WITH (NOLOCK) ON T5.[Servico_Codigo] = T4.[AreaTrabalho_ServicoPadrao]) LEFT JOIN [ServicoGrupo] T6 WITH (NOLOCK) ON T6.[ServicoGrupo_Codigo] = T5.[ServicoGrupo_Codigo]) WHERE (T2.[Contrato_AreaTrabalhoCod] = @AV53WWPC_1Areatrabalho_codigo and T1.[ContratoGestor_UsuarioCod] = @AV53WWPContext__Userid) AND (T2.[Contrato_Ativo] = 1) AND (T3.[Contratada_Ativo] = 1) ORDER BY T2.[Contrato_AreaTrabalhoCod], T1.[ContratoGestor_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ60,100,0,true,false )
             ,new CursorDef("H00IZ61", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ61,100,0,false,false )
             ,new CursorDef("H00IZ62", "SELECT T3.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T4.[AreaTrabalho_ServicoPadrao] AS Contratada_AreaTrbSrvPdr, T2.[Contrato_AreaTrabalhoCod], T1.[ContratoAuxiliar_UsuarioCod], T5.[Servico_Ativo], T5.[ServicoGrupo_Codigo], T1.[ContratoAuxiliar_ContratoCod] AS ContratoAuxiliar_ContratoCod, T6.[ServicoGrupo_Descricao], T2.[Contratada_Codigo], T2.[Contrato_Ativo], T3.[Contratada_Ativo], T2.[Contrato_DataVigenciaTermino] FROM ((((([ContratoAuxiliar] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoAuxiliar_ContratoCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) LEFT JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = T3.[Contratada_AreaTrabalhoCod]) LEFT JOIN [Servico] T5 WITH (NOLOCK) ON T5.[Servico_Codigo] = T4.[AreaTrabalho_ServicoPadrao]) LEFT JOIN [ServicoGrupo] T6 WITH (NOLOCK) ON T6.[ServicoGrupo_Codigo] = T5.[ServicoGrupo_Codigo]) WHERE (T2.[Contrato_AreaTrabalhoCod] = @AV53WWPC_1Areatrabalho_codigo and T1.[ContratoAuxiliar_UsuarioCod] = @AV53WWPContext__Userid) AND (T2.[Contrato_Ativo] = 1) AND (T3.[Contratada_Ativo] = 1) ORDER BY T2.[Contrato_AreaTrabalhoCod], T1.[ContratoAuxiliar_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ62,100,0,true,false )
             ,new CursorDef("H00IZ63", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ63,100,0,false,false )
             ,new CursorDef("H00IZ64", "SELECT T2.[Contratada_Codigo] AS ContratoGestor_ContratadaCod, T3.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T4.[AreaTrabalho_ServicoPadrao] AS Contratada_AreaTrbSrvPdr, T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod, T1.[ContratoGestor_UsuarioCod], T5.[Servico_Ativo], T5.[ServicoGrupo_Codigo], T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T2.[Contrato_Ativo], T3.[Contratada_Ativo], T2.[Contrato_DataVigenciaTermino] FROM (((([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) LEFT JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = T3.[Contratada_AreaTrabalhoCod]) LEFT JOIN [Servico] T5 WITH (NOLOCK) ON T5.[Servico_Codigo] = T4.[AreaTrabalho_ServicoPadrao]) WHERE (T2.[Contrato_AreaTrabalhoCod] = @AV53WWPC_1Areatrabalho_codigo and T1.[ContratoGestor_UsuarioCod] = @AV53WWPContext__Userid) AND (T2.[Contrato_Ativo] = 1) AND (T3.[Contratada_Ativo] = 1) ORDER BY T2.[Contrato_AreaTrabalhoCod], T1.[ContratoGestor_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ64,100,0,true,false )
             ,new CursorDef("H00IZ65", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ65,100,0,false,false )
             ,new CursorDef("H00IZ66", "SELECT T3.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T4.[AreaTrabalho_ServicoPadrao] AS Contratada_AreaTrbSrvPdr, T2.[Contrato_AreaTrabalhoCod], T1.[ContratoAuxiliar_UsuarioCod], T5.[Servico_Ativo], T5.[ServicoGrupo_Codigo], T1.[ContratoAuxiliar_ContratoCod] AS ContratoAuxiliar_ContratoCod, T2.[Contratada_Codigo], T2.[Contrato_Ativo], T3.[Contratada_Ativo], T2.[Contrato_DataVigenciaTermino] FROM (((([ContratoAuxiliar] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoAuxiliar_ContratoCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) LEFT JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = T3.[Contratada_AreaTrabalhoCod]) LEFT JOIN [Servico] T5 WITH (NOLOCK) ON T5.[Servico_Codigo] = T4.[AreaTrabalho_ServicoPadrao]) WHERE (T2.[Contrato_AreaTrabalhoCod] = @AV53WWPC_1Areatrabalho_codigo and T1.[ContratoAuxiliar_UsuarioCod] = @AV53WWPContext__Userid) AND (T2.[Contrato_Ativo] = 1) AND (T3.[Contratada_Ativo] = 1) ORDER BY T2.[Contrato_AreaTrabalhoCod], T1.[ContratoAuxiliar_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ66,100,0,true,false )
             ,new CursorDef("H00IZ67", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ67,100,0,false,false )
             ,new CursorDef("H00IZ70", "SELECT T1.[ContratoServicos_Codigo], T1.[Contrato_Codigo], T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, T2.[Contrato_AreaTrabalhoCod], T1.[Servico_Codigo], T3.[Contratada_Ativo], T2.[Contrato_Ativo], T1.[ContratoServicos_Ativo], T2.[Contrato_DataVigenciaTermino], T2.[Contratada_Codigo], T4.[Pessoa_Nome] AS Contratada_PessoaNom, COALESCE( T5.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) AS Contrato_DataFimTA FROM (((([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod]) LEFT JOIN (SELECT T6.[ContratoTermoAditivo_DataFim], T6.[Contrato_Codigo], T6.[ContratoTermoAditivo_Codigo], T7.[GXC10] AS GXC10 FROM ([ContratoTermoAditivo] T6 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC10, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T7 ON T7.[Contrato_Codigo] = T6.[Contrato_Codigo]) WHERE T6.[ContratoTermoAditivo_Codigo] = T7.[GXC10] ) T5 ON T5.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE (T1.[ContratoServicos_Ativo] = 1) AND (T2.[Contrato_Ativo] = 1) AND (T3.[Contratada_Ativo] = 1) AND (T2.[Contrato_AreaTrabalhoCod] = @AV53WWPC_1Areatrabalho_codigo) AND (T1.[Servico_Codigo] = @AV182NewCntSrvCod) ORDER BY T4.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ70,100,0,true,false )
             ,new CursorDef("H00IZ71", "SELECT TOP 1 T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Usuario_Codigo], T2.[Pessoa_Nome] AS Usuario_PessoaNom FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) WHERE T1.[Usuario_Codigo] = @AV128Owner_Codigo ORDER BY T1.[Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ71,1,0,false,true )
             ,new CursorDef("H00IZ74", "SELECT TOP 1 T1.[ContratoServicos_Codigo], T2.[Contratada_Codigo], T2.[Contrato_Ativo], T1.[Servico_Codigo], T1.[Contrato_Codigo], COALESCE( T3.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) AS Contrato_DataFimTA, T2.[Contrato_DataVigenciaTermino] FROM (([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) LEFT JOIN (SELECT T4.[ContratoTermoAditivo_DataFim], T4.[Contrato_Codigo], T4.[ContratoTermoAditivo_Codigo], T5.[GXC10] AS GXC10 FROM ([ContratoTermoAditivo] T4 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC10, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T5 ON T5.[Contrato_Codigo] = T4.[Contrato_Codigo]) WHERE T4.[ContratoTermoAditivo_Codigo] = T5.[GXC10] ) T3 ON T3.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE (T1.[Contrato_Codigo] = @AV174Contrato) AND (T1.[Servico_Codigo] = @AV182NewCntSrvCod) ORDER BY T1.[Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ74,1,0,true,true )
             ,new CursorDef("H00IZ75", "SELECT T1.[ContratoServicos_Codigo], T1.[Artefatos_Codigo], T2.[Artefatos_Descricao] FROM ([ContratoServicosArtefatos] T1 WITH (NOLOCK) INNER JOIN [Artefatos] T2 WITH (NOLOCK) ON T2.[Artefatos_Codigo] = T1.[Artefatos_Codigo]) WHERE T1.[ContratoServicos_Codigo] = @ContratoServicos_Codigo ORDER BY T1.[ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ75,100,0,false,false )
             ,new CursorDef("H00IZ76", "SELECT [ContagemResultadoRequisito_Codigo], [ContagemResultadoRequisito_OSCod], [ContagemResultadoRequisito_ReqCod] FROM [ContagemResultadoRequisito] WITH (NOLOCK) WHERE [ContagemResultadoRequisito_OSCod] = @AV9Codigo ORDER BY [ContagemResultadoRequisito_OSCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ76,100,0,false,false )
             ,new CursorDef("H00IZ79", "SELECT T1.[Contrato_Numero], T1.[Contrato_Codigo], T1.[Contratada_Codigo], T1.[Contrato_Ativo], T3.[Contratada_CntPadrao], COALESCE( T2.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) AS Contrato_DataFimTA, T1.[Contrato_DataVigenciaTermino] FROM (([Contrato] T1 WITH (NOLOCK) LEFT JOIN (SELECT T4.[ContratoTermoAditivo_DataFim], T4.[Contrato_Codigo], T4.[ContratoTermoAditivo_Codigo], T5.[GXC10] AS GXC10 FROM ([ContratoTermoAditivo] T4 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC10, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T5 ON T5.[Contrato_Codigo] = T4.[Contrato_Codigo]) WHERE T4.[ContratoTermoAditivo_Codigo] = T5.[GXC10] ) T2 ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[Contratada_Codigo]) WHERE (T1.[Contratada_Codigo] = @AV168CriarOSPara_Codigo) AND (T1.[Contrato_Ativo] = 1) ORDER BY T1.[Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ79,100,0,true,false )
             ,new CursorDef("H00IZ80", "SELECT [Contrato_Codigo], [ContratoServicos_Ativo], [Servico_Codigo], [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE ([Contrato_Codigo] = @Contrato_Codigo) AND ([ContratoServicos_Ativo] = 1) AND ([Servico_Codigo] = @AV182NewCntSrvCod) ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ80,100,0,false,false )
             ,new CursorDef("H00IZ83", "SELECT T1.[Contrato_Ativo], T1.[Contrato_DataVigenciaTermino], T1.[Contratada_Codigo], T1.[Contrato_Codigo], COALESCE( T2.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) AS Contrato_DataFimTA FROM ([Contrato] T1 WITH (NOLOCK) LEFT JOIN (SELECT T3.[ContratoTermoAditivo_DataFim], T3.[Contrato_Codigo], T3.[ContratoTermoAditivo_Codigo], T4.[GXC10] AS GXC10 FROM ([ContratoTermoAditivo] T3 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC10, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T4 ON T4.[Contrato_Codigo] = T3.[Contrato_Codigo]) WHERE T3.[ContratoTermoAditivo_Codigo] = T4.[GXC10] ) T2 ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE (T1.[Contratada_Codigo] = @AV5Usuario_Codigo - 900000) AND (T1.[Contrato_Ativo] = 1) ORDER BY T1.[Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IZ83,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((short[]) buf[14])[0] = rslt.getShort(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((bool[]) buf[16])[0] = rslt.getBool(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((String[]) buf[20])[0] = rslt.getString(12, 1) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((String[]) buf[22])[0] = rslt.getString(13, 1) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((int[]) buf[24])[0] = rslt.getInt(14) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((int[]) buf[26])[0] = rslt.getInt(15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((int[]) buf[28])[0] = rslt.getInt(16) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((String[]) buf[30])[0] = rslt.getString(17, 15) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((int[]) buf[32])[0] = rslt.getInt(18) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(18);
                ((int[]) buf[34])[0] = rslt.getInt(19) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(19);
                ((int[]) buf[36])[0] = rslt.getInt(20) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(20);
                ((int[]) buf[38])[0] = rslt.getInt(21) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(21);
                ((String[]) buf[40])[0] = rslt.getString(22, 15) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(22);
                ((String[]) buf[42])[0] = rslt.getString(23, 1) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(23);
                ((String[]) buf[44])[0] = rslt.getString(24, 1) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(24);
                ((int[]) buf[46])[0] = rslt.getInt(25) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(25);
                ((int[]) buf[48])[0] = rslt.getInt(26) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(26);
                ((int[]) buf[50])[0] = rslt.getInt(27) ;
                ((bool[]) buf[51])[0] = rslt.wasNull(27);
                ((int[]) buf[52])[0] = rslt.getInt(28) ;
                ((String[]) buf[53])[0] = rslt.getVarchar(29) ;
                ((bool[]) buf[54])[0] = rslt.wasNull(29);
                ((short[]) buf[55])[0] = rslt.getShort(30) ;
                ((decimal[]) buf[56])[0] = rslt.getDecimal(31) ;
                ((decimal[]) buf[57])[0] = rslt.getDecimal(32) ;
                ((decimal[]) buf[58])[0] = rslt.getDecimal(33) ;
                ((decimal[]) buf[59])[0] = rslt.getDecimal(34) ;
                ((DateTime[]) buf[60])[0] = rslt.getGXDate(35) ;
                ((bool[]) buf[61])[0] = rslt.wasNull(35);
                ((int[]) buf[62])[0] = rslt.getInt(36) ;
                ((bool[]) buf[63])[0] = rslt.wasNull(36);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((long[]) buf[10])[0] = rslt.getLong(6) ;
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                return;
             case 5 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 6 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 7 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 1) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((long[]) buf[5])[0] = rslt.getLong(4) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((bool[]) buf[8])[0] = rslt.getBool(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((bool[]) buf[5])[0] = rslt.getBool(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((bool[]) buf[7])[0] = rslt.getBool(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((bool[]) buf[4])[0] = rslt.getBool(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((String[]) buf[8])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((bool[]) buf[5])[0] = rslt.getBool(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((bool[]) buf[7])[0] = rslt.getBool(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((bool[]) buf[8])[0] = rslt.getBool(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((short[]) buf[16])[0] = rslt.getShort(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((bool[]) buf[18])[0] = rslt.getBool(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((long[]) buf[20])[0] = rslt.getLong(11) ;
                ((int[]) buf[21])[0] = rslt.getInt(12) ;
                ((int[]) buf[22])[0] = rslt.getInt(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((DateTime[]) buf[24])[0] = rslt.getGXDate(14) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 24 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(5) ;
                ((short[]) buf[8])[0] = rslt.getShort(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((short[]) buf[10])[0] = rslt.getShort(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[12])[0] = rslt.getGXDateTime(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[14])[0] = rslt.getGXDate(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[16])[0] = rslt.getGXDateTime(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((int[]) buf[20])[0] = rslt.getInt(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((decimal[]) buf[22])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((short[]) buf[24])[0] = rslt.getShort(14) ;
                ((decimal[]) buf[25])[0] = rslt.getDecimal(15) ;
                ((int[]) buf[26])[0] = rslt.getInt(16) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(16);
                return;
             case 25 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 20) ;
                ((long[]) buf[3])[0] = rslt.getLong(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 26 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[4])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((long[]) buf[6])[0] = rslt.getLong(4) ;
                return;
             case 27 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((long[]) buf[3])[0] = rslt.getLong(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                return;
             case 28 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
             case 29 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
       }
       getresults30( cursor, rslt, buf) ;
    }

    public void getresults30( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 30 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((short[]) buf[10])[0] = rslt.getShort(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((bool[]) buf[12])[0] = rslt.getBool(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                ((DateTime[]) buf[15])[0] = rslt.getGXDate(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                return;
             case 31 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 32 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((short[]) buf[10])[0] = rslt.getShort(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((bool[]) buf[12])[0] = rslt.getBool(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[17])[0] = rslt.getGXDate(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                return;
             case 33 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 34 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(6) ;
                return;
             case 35 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 36 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((String[]) buf[7])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((short[]) buf[9])[0] = rslt.getShort(6) ;
                ((decimal[]) buf[10])[0] = rslt.getDecimal(7) ;
                ((decimal[]) buf[11])[0] = rslt.getDecimal(8) ;
                ((decimal[]) buf[12])[0] = rslt.getDecimal(9) ;
                ((decimal[]) buf[13])[0] = rslt.getDecimal(10) ;
                return;
             case 37 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((String[]) buf[14])[0] = rslt.getString(8, 15) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((int[]) buf[16])[0] = rslt.getInt(9) ;
                ((String[]) buf[17])[0] = rslt.getString(10, 1) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((short[]) buf[19])[0] = rslt.getShort(11) ;
                ((decimal[]) buf[20])[0] = rslt.getDecimal(12) ;
                ((decimal[]) buf[21])[0] = rslt.getDecimal(13) ;
                ((decimal[]) buf[22])[0] = rslt.getDecimal(14) ;
                ((decimal[]) buf[23])[0] = rslt.getDecimal(15) ;
                return;
             case 38 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((bool[]) buf[8])[0] = rslt.getBool(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((short[]) buf[14])[0] = rslt.getShort(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((bool[]) buf[16])[0] = rslt.getBool(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((long[]) buf[18])[0] = rslt.getLong(10) ;
                ((int[]) buf[19])[0] = rslt.getInt(11) ;
                ((int[]) buf[20])[0] = rslt.getInt(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((int[]) buf[22])[0] = rslt.getInt(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((DateTime[]) buf[24])[0] = rslt.getGXDate(14) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                return;
             case 39 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 40 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 41 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 42 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 43 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((short[]) buf[10])[0] = rslt.getShort(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((bool[]) buf[12])[0] = rslt.getBool(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((String[]) buf[14])[0] = rslt.getString(8, 20) ;
                ((long[]) buf[15])[0] = rslt.getLong(9) ;
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((int[]) buf[17])[0] = rslt.getInt(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[19])[0] = rslt.getGXDate(12) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                return;
             case 44 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 45 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[10])[0] = rslt.getGXDate(8) ;
                return;
             case 46 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 47 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.getBool(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((String[]) buf[14])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[15])[0] = rslt.getBool(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((bool[]) buf[17])[0] = rslt.getBool(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[19])[0] = rslt.getGXDate(12) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                return;
             case 48 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 49 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((String[]) buf[11])[0] = rslt.getVarchar(8) ;
                ((int[]) buf[12])[0] = rslt.getInt(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((bool[]) buf[14])[0] = rslt.getBool(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((bool[]) buf[16])[0] = rslt.getBool(11) ;
                ((DateTime[]) buf[17])[0] = rslt.getGXDate(12) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                return;
             case 50 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 51 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.getBool(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((bool[]) buf[14])[0] = rslt.getBool(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((bool[]) buf[16])[0] = rslt.getBool(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((DateTime[]) buf[18])[0] = rslt.getGXDate(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                return;
             case 52 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                return;
             case 53 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((bool[]) buf[13])[0] = rslt.getBool(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((bool[]) buf[15])[0] = rslt.getBool(10) ;
                ((DateTime[]) buf[16])[0] = rslt.getGXDate(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                return;
             case 54 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                return;
             case 55 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((bool[]) buf[6])[0] = rslt.getBool(6) ;
                ((bool[]) buf[7])[0] = rslt.getBool(7) ;
                ((bool[]) buf[8])[0] = rslt.getBool(8) ;
                ((DateTime[]) buf[9])[0] = rslt.getGXDate(9) ;
                ((int[]) buf[10])[0] = rslt.getInt(10) ;
                ((String[]) buf[11])[0] = rslt.getString(11, 100) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[13])[0] = rslt.getGXDate(12) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(12);
                return;
             case 56 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 57 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(7) ;
                return;
             case 58 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                return;
             case 59 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
       }
       getresults60( cursor, rslt, buf) ;
    }

    public void getresults60( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 60 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(7) ;
                return;
             case 61 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 62 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 11 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 12 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 17 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                return;
             case 18 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                return;
             case 19 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[4]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                return;
             case 20 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 22 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 23 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 24 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 25 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 26 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 27 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 28 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 29 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
       setparameters30( cursor, stmt, parms) ;
    }

    public void setparameters30( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 30 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 31 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 32 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 33 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 34 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 35 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 36 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 37 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 38 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 39 :
                stmt.SetParameter(1, (int)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(2, (bool)parms[2]);
                }
                stmt.SetParameter(3, (int)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[5]);
                }
                return;
             case 40 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 41 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 42 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 43 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 44 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                return;
             case 45 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                return;
             case 46 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   if ( (bool)parms[1] )
                   {
                      stmt.setNull( sIdx , SqlDbType.Int );
                   }
                   else
                   {
                      stmt.SetParameter(sIdx, (int)parms[2]);
                   }
                }
                return;
             case 47 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 48 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   if ( (bool)parms[3] )
                   {
                      stmt.setNull( sIdx , SqlDbType.Bit );
                   }
                   else
                   {
                      stmt.SetParameter(sIdx, (bool)parms[4]);
                   }
                }
                return;
             case 49 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 50 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   if ( (bool)parms[3] )
                   {
                      stmt.setNull( sIdx , SqlDbType.Bit );
                   }
                   else
                   {
                      stmt.SetParameter(sIdx, (bool)parms[4]);
                   }
                }
                return;
             case 51 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 52 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   if ( (bool)parms[3] )
                   {
                      stmt.setNull( sIdx , SqlDbType.Bit );
                   }
                   else
                   {
                      stmt.SetParameter(sIdx, (bool)parms[4]);
                   }
                }
                return;
             case 53 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 54 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   if ( (bool)parms[3] )
                   {
                      stmt.setNull( sIdx , SqlDbType.Bit );
                   }
                   else
                   {
                      stmt.SetParameter(sIdx, (bool)parms[4]);
                   }
                }
                return;
             case 55 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 56 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 57 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 58 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 59 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
       setparameters60( cursor, stmt, parms) ;
    }

    public void setparameters60( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 60 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 61 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 62 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
