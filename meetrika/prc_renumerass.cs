/*
               File: PRC_RenumeraSS
        Description: Renumera SS
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:28.35
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_renumerass : GXProcedure
   {
      public prc_renumerass( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_renumerass( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_LoteAceiteCod ,
                           ref int aP1_Contratada_NumeroSS )
      {
         this.AV9ContagemResultado_LoteAceiteCod = aP0_ContagemResultado_LoteAceiteCod;
         this.AV8Contratada_NumeroSS = aP1_Contratada_NumeroSS;
         initialize();
         executePrivate();
         aP0_ContagemResultado_LoteAceiteCod=this.AV9ContagemResultado_LoteAceiteCod;
         aP1_Contratada_NumeroSS=this.AV8Contratada_NumeroSS;
      }

      public int executeUdp( ref int aP0_ContagemResultado_LoteAceiteCod )
      {
         this.AV9ContagemResultado_LoteAceiteCod = aP0_ContagemResultado_LoteAceiteCod;
         this.AV8Contratada_NumeroSS = aP1_Contratada_NumeroSS;
         initialize();
         executePrivate();
         aP0_ContagemResultado_LoteAceiteCod=this.AV9ContagemResultado_LoteAceiteCod;
         aP1_Contratada_NumeroSS=this.AV8Contratada_NumeroSS;
         return AV8Contratada_NumeroSS ;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_LoteAceiteCod ,
                                 ref int aP1_Contratada_NumeroSS )
      {
         prc_renumerass objprc_renumerass;
         objprc_renumerass = new prc_renumerass();
         objprc_renumerass.AV9ContagemResultado_LoteAceiteCod = aP0_ContagemResultado_LoteAceiteCod;
         objprc_renumerass.AV8Contratada_NumeroSS = aP1_Contratada_NumeroSS;
         objprc_renumerass.context.SetSubmitInitialConfig(context);
         objprc_renumerass.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_renumerass);
         aP0_ContagemResultado_LoteAceiteCod=this.AV9ContagemResultado_LoteAceiteCod;
         aP1_Contratada_NumeroSS=this.AV8Contratada_NumeroSS;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_renumerass)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00AH2 */
         pr_default.execute(0, new Object[] {AV9ContagemResultado_LoteAceiteCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A489ContagemResultado_SistemaCod = P00AH2_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00AH2_n489ContagemResultado_SistemaCod[0];
            A597ContagemResultado_LoteAceiteCod = P00AH2_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = P00AH2_n597ContagemResultado_LoteAceiteCod[0];
            A509ContagemrResultado_SistemaSigla = P00AH2_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00AH2_n509ContagemrResultado_SistemaSigla[0];
            A515ContagemResultado_SistemaCoord = P00AH2_A515ContagemResultado_SistemaCoord[0];
            n515ContagemResultado_SistemaCoord = P00AH2_n515ContagemResultado_SistemaCoord[0];
            A509ContagemrResultado_SistemaSigla = P00AH2_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00AH2_n509ContagemrResultado_SistemaSigla[0];
            A515ContagemResultado_SistemaCoord = P00AH2_A515ContagemResultado_SistemaCoord[0];
            n515ContagemResultado_SistemaCoord = P00AH2_n515ContagemResultado_SistemaCoord[0];
            AV8Contratada_NumeroSS = (int)(AV8Contratada_NumeroSS+1);
            AV10ContagemrResultado_SistemaSigla = A509ContagemrResultado_SistemaSigla;
            /* Execute user subroutine: 'NUMERA' */
            S111 ();
            if ( returnInSub )
            {
               pr_default.close(0);
               this.cleanup();
               if (true) return;
            }
            context.CommitDataStores( "PRC_RenumeraSS");
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'NUMERA' Routine */
         /* Using cursor P00AH3 */
         pr_default.execute(1, new Object[] {AV9ContagemResultado_LoteAceiteCod, AV10ContagemrResultado_SistemaSigla});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A489ContagemResultado_SistemaCod = P00AH3_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00AH3_n489ContagemResultado_SistemaCod[0];
            A509ContagemrResultado_SistemaSigla = P00AH3_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00AH3_n509ContagemrResultado_SistemaSigla[0];
            A597ContagemResultado_LoteAceiteCod = P00AH3_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = P00AH3_n597ContagemResultado_LoteAceiteCod[0];
            A1452ContagemResultado_SS = P00AH3_A1452ContagemResultado_SS[0];
            n1452ContagemResultado_SS = P00AH3_n1452ContagemResultado_SS[0];
            A456ContagemResultado_Codigo = P00AH3_A456ContagemResultado_Codigo[0];
            A509ContagemrResultado_SistemaSigla = P00AH3_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00AH3_n509ContagemrResultado_SistemaSigla[0];
            A1452ContagemResultado_SS = AV8Contratada_NumeroSS;
            n1452ContagemResultado_SS = false;
            BatchSize = 100;
            pr_default.initializeBatch( 2, BatchSize, this, "Executebatchp00ah4");
            /* Using cursor P00AH4 */
            pr_default.addRecord(2, new Object[] {n1452ContagemResultado_SS, A1452ContagemResultado_SS, A456ContagemResultado_Codigo});
            if ( pr_default.recordCount(2) == pr_default.getBatchSize(2) )
            {
               Executebatchp00ah4( ) ;
            }
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            pr_default.readNext(1);
         }
         if ( pr_default.getBatchSize(2) > 0 )
         {
            Executebatchp00ah4( ) ;
         }
         pr_default.close(1);
      }

      protected void Executebatchp00ah4( )
      {
         /* Using cursor P00AH4 */
         pr_default.executeBatch(2);
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_RenumeraSS");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(2);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00AH2_A489ContagemResultado_SistemaCod = new int[1] ;
         P00AH2_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00AH2_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P00AH2_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P00AH2_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P00AH2_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P00AH2_A515ContagemResultado_SistemaCoord = new String[] {""} ;
         P00AH2_n515ContagemResultado_SistemaCoord = new bool[] {false} ;
         A509ContagemrResultado_SistemaSigla = "";
         A515ContagemResultado_SistemaCoord = "";
         AV10ContagemrResultado_SistemaSigla = "";
         P00AH3_A489ContagemResultado_SistemaCod = new int[1] ;
         P00AH3_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00AH3_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P00AH3_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P00AH3_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P00AH3_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P00AH3_A1452ContagemResultado_SS = new int[1] ;
         P00AH3_n1452ContagemResultado_SS = new bool[] {false} ;
         P00AH3_A456ContagemResultado_Codigo = new int[1] ;
         P00AH4_A1452ContagemResultado_SS = new int[1] ;
         P00AH4_n1452ContagemResultado_SS = new bool[] {false} ;
         P00AH4_A456ContagemResultado_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_renumerass__default(),
            new Object[][] {
                new Object[] {
               P00AH2_A489ContagemResultado_SistemaCod, P00AH2_n489ContagemResultado_SistemaCod, P00AH2_A597ContagemResultado_LoteAceiteCod, P00AH2_n597ContagemResultado_LoteAceiteCod, P00AH2_A509ContagemrResultado_SistemaSigla, P00AH2_n509ContagemrResultado_SistemaSigla, P00AH2_A515ContagemResultado_SistemaCoord, P00AH2_n515ContagemResultado_SistemaCoord
               }
               , new Object[] {
               P00AH3_A489ContagemResultado_SistemaCod, P00AH3_n489ContagemResultado_SistemaCod, P00AH3_A509ContagemrResultado_SistemaSigla, P00AH3_n509ContagemrResultado_SistemaSigla, P00AH3_A597ContagemResultado_LoteAceiteCod, P00AH3_n597ContagemResultado_LoteAceiteCod, P00AH3_A1452ContagemResultado_SS, P00AH3_n1452ContagemResultado_SS, P00AH3_A456ContagemResultado_Codigo
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV9ContagemResultado_LoteAceiteCod ;
      private int AV8Contratada_NumeroSS ;
      private int A489ContagemResultado_SistemaCod ;
      private int A597ContagemResultado_LoteAceiteCod ;
      private int A1452ContagemResultado_SS ;
      private int A456ContagemResultado_Codigo ;
      private int BatchSize ;
      private String scmdbuf ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String AV10ContagemrResultado_SistemaSigla ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n597ContagemResultado_LoteAceiteCod ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool n515ContagemResultado_SistemaCoord ;
      private bool returnInSub ;
      private bool n1452ContagemResultado_SS ;
      private String A515ContagemResultado_SistemaCoord ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_LoteAceiteCod ;
      private int aP1_Contratada_NumeroSS ;
      private IDataStoreProvider pr_default ;
      private int[] P00AH2_A489ContagemResultado_SistemaCod ;
      private bool[] P00AH2_n489ContagemResultado_SistemaCod ;
      private int[] P00AH2_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P00AH2_n597ContagemResultado_LoteAceiteCod ;
      private String[] P00AH2_A509ContagemrResultado_SistemaSigla ;
      private bool[] P00AH2_n509ContagemrResultado_SistemaSigla ;
      private String[] P00AH2_A515ContagemResultado_SistemaCoord ;
      private bool[] P00AH2_n515ContagemResultado_SistemaCoord ;
      private int[] P00AH3_A489ContagemResultado_SistemaCod ;
      private bool[] P00AH3_n489ContagemResultado_SistemaCod ;
      private String[] P00AH3_A509ContagemrResultado_SistemaSigla ;
      private bool[] P00AH3_n509ContagemrResultado_SistemaSigla ;
      private int[] P00AH3_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P00AH3_n597ContagemResultado_LoteAceiteCod ;
      private int[] P00AH3_A1452ContagemResultado_SS ;
      private bool[] P00AH3_n1452ContagemResultado_SS ;
      private int[] P00AH3_A456ContagemResultado_Codigo ;
      private int[] P00AH4_A1452ContagemResultado_SS ;
      private bool[] P00AH4_n1452ContagemResultado_SS ;
      private int[] P00AH4_A456ContagemResultado_Codigo ;
   }

   public class prc_renumerass__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new BatchUpdateCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00AH2 ;
          prmP00AH2 = new Object[] {
          new Object[] {"@AV9ContagemResultado_LoteAceiteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00AH3 ;
          prmP00AH3 = new Object[] {
          new Object[] {"@AV9ContagemResultado_LoteAceiteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV10ContagemrResultado_SistemaSigla",SqlDbType.Char,25,0}
          } ;
          Object[] prmP00AH4 ;
          prmP00AH4 = new Object[] {
          new Object[] {"@ContagemResultado_SS",SqlDbType.Int,8,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00AH2", "SELECT DISTINCT NULL AS [ContagemResultado_SistemaCod], NULL AS [ContagemResultado_LoteAceiteCod], [ContagemrResultado_SistemaSigla], [ContagemResultado_SistemaCoord] FROM ( SELECT TOP(100) PERCENT T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_LoteAceiteCod], T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T2.[Sistema_Coordenacao] AS ContagemResultado_SistemaCoord FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) WHERE T1.[ContagemResultado_LoteAceiteCod] = @AV9ContagemResultado_LoteAceiteCod ORDER BY T2.[Sistema_Coordenacao], T2.[Sistema_Sigla]) DistinctT ORDER BY [ContagemResultado_SistemaCoord], [ContagemrResultado_SistemaSigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00AH2,100,0,true,false )
             ,new CursorDef("P00AH3", "SELECT T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T1.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_SS], T1.[ContagemResultado_Codigo] FROM ([ContagemResultado] T1 WITH (UPDLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) WHERE (T1.[ContagemResultado_LoteAceiteCod] = @AV9ContagemResultado_LoteAceiteCod) AND (T2.[Sistema_Sigla] = @AV10ContagemrResultado_SistemaSigla) ORDER BY T1.[ContagemResultado_LoteAceiteCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00AH3,1,0,true,false )
             ,new CursorDef("P00AH4", "UPDATE [ContagemResultado] SET [ContagemResultado_SS]=@ContagemResultado_SS  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00AH4)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 25) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 25) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
