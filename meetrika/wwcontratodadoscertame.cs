/*
               File: WWContratoDadosCertame
        Description:  Contrato Dados Certame
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:36:51.45
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwcontratodadoscertame : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwcontratodadoscertame( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwcontratodadoscertame( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_115 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_115_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_115_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV37ContratoDadosCertame_Data1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ContratoDadosCertame_Data1", context.localUtil.Format(AV37ContratoDadosCertame_Data1, "99/99/99"));
               AV38ContratoDadosCertame_Data_To1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ContratoDadosCertame_Data_To1", context.localUtil.Format(AV38ContratoDadosCertame_Data_To1, "99/99/99"));
               AV17ContratoDadosCertame_Modalidade1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoDadosCertame_Modalidade1", AV17ContratoDadosCertame_Modalidade1);
               AV39ContratoDadosCertame_Numero1 = (long)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ContratoDadosCertame_Numero1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39ContratoDadosCertame_Numero1), 10, 0)));
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
               AV40ContratoDadosCertame_Data2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ContratoDadosCertame_Data2", context.localUtil.Format(AV40ContratoDadosCertame_Data2, "99/99/99"));
               AV41ContratoDadosCertame_Data_To2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ContratoDadosCertame_Data_To2", context.localUtil.Format(AV41ContratoDadosCertame_Data_To2, "99/99/99"));
               AV21ContratoDadosCertame_Modalidade2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoDadosCertame_Modalidade2", AV21ContratoDadosCertame_Modalidade2);
               AV42ContratoDadosCertame_Numero2 = (long)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ContratoDadosCertame_Numero2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42ContratoDadosCertame_Numero2), 10, 0)));
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
               AV43ContratoDadosCertame_Data3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ContratoDadosCertame_Data3", context.localUtil.Format(AV43ContratoDadosCertame_Data3, "99/99/99"));
               AV44ContratoDadosCertame_Data_To3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ContratoDadosCertame_Data_To3", context.localUtil.Format(AV44ContratoDadosCertame_Data_To3, "99/99/99"));
               AV25ContratoDadosCertame_Modalidade3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoDadosCertame_Modalidade3", AV25ContratoDadosCertame_Modalidade3);
               AV45ContratoDadosCertame_Numero3 = (long)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ContratoDadosCertame_Numero3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45ContratoDadosCertame_Numero3), 10, 0)));
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV49TFContrato_Numero = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContrato_Numero", AV49TFContrato_Numero);
               AV50TFContrato_Numero_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContrato_Numero_Sel", AV50TFContrato_Numero_Sel);
               AV53TFContratoDadosCertame_Data = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContratoDadosCertame_Data", context.localUtil.Format(AV53TFContratoDadosCertame_Data, "99/99/99"));
               AV54TFContratoDadosCertame_Data_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContratoDadosCertame_Data_To", context.localUtil.Format(AV54TFContratoDadosCertame_Data_To, "99/99/99"));
               AV59TFContratoDadosCertame_Modalidade = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContratoDadosCertame_Modalidade", AV59TFContratoDadosCertame_Modalidade);
               AV60TFContratoDadosCertame_Modalidade_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratoDadosCertame_Modalidade_Sel", AV60TFContratoDadosCertame_Modalidade_Sel);
               AV63TFContratoDadosCertame_Numero = (long)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFContratoDadosCertame_Numero", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63TFContratoDadosCertame_Numero), 10, 0)));
               AV64TFContratoDadosCertame_Numero_To = (long)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFContratoDadosCertame_Numero_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV64TFContratoDadosCertame_Numero_To), 10, 0)));
               AV67TFContratoDadosCertame_Site = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFContratoDadosCertame_Site", AV67TFContratoDadosCertame_Site);
               AV68TFContratoDadosCertame_Site_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFContratoDadosCertame_Site_Sel", AV68TFContratoDadosCertame_Site_Sel);
               AV71TFContratoDadosCertame_Uasg = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContratoDadosCertame_Uasg", StringUtil.LTrim( StringUtil.Str( (decimal)(AV71TFContratoDadosCertame_Uasg), 4, 0)));
               AV72TFContratoDadosCertame_Uasg_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFContratoDadosCertame_Uasg_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV72TFContratoDadosCertame_Uasg_To), 4, 0)));
               AV75TFContratoDadosCertame_DataHomologacao = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFContratoDadosCertame_DataHomologacao", context.localUtil.Format(AV75TFContratoDadosCertame_DataHomologacao, "99/99/99"));
               AV76TFContratoDadosCertame_DataHomologacao_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFContratoDadosCertame_DataHomologacao_To", context.localUtil.Format(AV76TFContratoDadosCertame_DataHomologacao_To, "99/99/99"));
               AV81TFContratoDadosCertame_DataAdjudicacao = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFContratoDadosCertame_DataAdjudicacao", context.localUtil.Format(AV81TFContratoDadosCertame_DataAdjudicacao, "99/99/99"));
               AV82TFContratoDadosCertame_DataAdjudicacao_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFContratoDadosCertame_DataAdjudicacao_To", context.localUtil.Format(AV82TFContratoDadosCertame_DataAdjudicacao_To, "99/99/99"));
               AV51ddo_Contrato_NumeroTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ddo_Contrato_NumeroTitleControlIdToReplace", AV51ddo_Contrato_NumeroTitleControlIdToReplace);
               AV57ddo_ContratoDadosCertame_DataTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_ContratoDadosCertame_DataTitleControlIdToReplace", AV57ddo_ContratoDadosCertame_DataTitleControlIdToReplace);
               AV61ddo_ContratoDadosCertame_ModalidadeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_ContratoDadosCertame_ModalidadeTitleControlIdToReplace", AV61ddo_ContratoDadosCertame_ModalidadeTitleControlIdToReplace);
               AV65ddo_ContratoDadosCertame_NumeroTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ddo_ContratoDadosCertame_NumeroTitleControlIdToReplace", AV65ddo_ContratoDadosCertame_NumeroTitleControlIdToReplace);
               AV69ddo_ContratoDadosCertame_SiteTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ddo_ContratoDadosCertame_SiteTitleControlIdToReplace", AV69ddo_ContratoDadosCertame_SiteTitleControlIdToReplace);
               AV73ddo_ContratoDadosCertame_UasgTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ddo_ContratoDadosCertame_UasgTitleControlIdToReplace", AV73ddo_ContratoDadosCertame_UasgTitleControlIdToReplace);
               AV79ddo_ContratoDadosCertame_DataHomologacaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79ddo_ContratoDadosCertame_DataHomologacaoTitleControlIdToReplace", AV79ddo_ContratoDadosCertame_DataHomologacaoTitleControlIdToReplace);
               AV85ddo_ContratoDadosCertame_DataAdjudicacaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85ddo_ContratoDadosCertame_DataAdjudicacaoTitleControlIdToReplace", AV85ddo_ContratoDadosCertame_DataAdjudicacaoTitleControlIdToReplace);
               AV130Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               A314ContratoDadosCertame_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A314ContratoDadosCertame_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A314ContratoDadosCertame_Codigo), 6, 0)));
               A74Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
               A309ContratoDadosCertame_Site = GetNextPar( );
               n309ContratoDadosCertame_Site = false;
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV37ContratoDadosCertame_Data1, AV38ContratoDadosCertame_Data_To1, AV17ContratoDadosCertame_Modalidade1, AV39ContratoDadosCertame_Numero1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV40ContratoDadosCertame_Data2, AV41ContratoDadosCertame_Data_To2, AV21ContratoDadosCertame_Modalidade2, AV42ContratoDadosCertame_Numero2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV43ContratoDadosCertame_Data3, AV44ContratoDadosCertame_Data_To3, AV25ContratoDadosCertame_Modalidade3, AV45ContratoDadosCertame_Numero3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV49TFContrato_Numero, AV50TFContrato_Numero_Sel, AV53TFContratoDadosCertame_Data, AV54TFContratoDadosCertame_Data_To, AV59TFContratoDadosCertame_Modalidade, AV60TFContratoDadosCertame_Modalidade_Sel, AV63TFContratoDadosCertame_Numero, AV64TFContratoDadosCertame_Numero_To, AV67TFContratoDadosCertame_Site, AV68TFContratoDadosCertame_Site_Sel, AV71TFContratoDadosCertame_Uasg, AV72TFContratoDadosCertame_Uasg_To, AV75TFContratoDadosCertame_DataHomologacao, AV76TFContratoDadosCertame_DataHomologacao_To, AV81TFContratoDadosCertame_DataAdjudicacao, AV82TFContratoDadosCertame_DataAdjudicacao_To, AV51ddo_Contrato_NumeroTitleControlIdToReplace, AV57ddo_ContratoDadosCertame_DataTitleControlIdToReplace, AV61ddo_ContratoDadosCertame_ModalidadeTitleControlIdToReplace, AV65ddo_ContratoDadosCertame_NumeroTitleControlIdToReplace, AV69ddo_ContratoDadosCertame_SiteTitleControlIdToReplace, AV73ddo_ContratoDadosCertame_UasgTitleControlIdToReplace, AV79ddo_ContratoDadosCertame_DataHomologacaoTitleControlIdToReplace, AV85ddo_ContratoDadosCertame_DataAdjudicacaoTitleControlIdToReplace, AV130Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A314ContratoDadosCertame_Codigo, A74Contrato_Codigo, A309ContratoDadosCertame_Site) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA7G2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START7G2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117365240");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwcontratodadoscertame.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATODADOSCERTAME_DATA1", context.localUtil.Format(AV37ContratoDadosCertame_Data1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATODADOSCERTAME_DATA_TO1", context.localUtil.Format(AV38ContratoDadosCertame_Data_To1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATODADOSCERTAME_MODALIDADE1", AV17ContratoDadosCertame_Modalidade1);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATODADOSCERTAME_NUMERO1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39ContratoDadosCertame_Numero1), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATODADOSCERTAME_DATA2", context.localUtil.Format(AV40ContratoDadosCertame_Data2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATODADOSCERTAME_DATA_TO2", context.localUtil.Format(AV41ContratoDadosCertame_Data_To2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATODADOSCERTAME_MODALIDADE2", AV21ContratoDadosCertame_Modalidade2);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATODADOSCERTAME_NUMERO2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV42ContratoDadosCertame_Numero2), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATODADOSCERTAME_DATA3", context.localUtil.Format(AV43ContratoDadosCertame_Data3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATODADOSCERTAME_DATA_TO3", context.localUtil.Format(AV44ContratoDadosCertame_Data_To3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATODADOSCERTAME_MODALIDADE3", AV25ContratoDadosCertame_Modalidade3);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATODADOSCERTAME_NUMERO3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV45ContratoDadosCertame_Numero3), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_NUMERO", StringUtil.RTrim( AV49TFContrato_Numero));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_NUMERO_SEL", StringUtil.RTrim( AV50TFContrato_Numero_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATODADOSCERTAME_DATA", context.localUtil.Format(AV53TFContratoDadosCertame_Data, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATODADOSCERTAME_DATA_TO", context.localUtil.Format(AV54TFContratoDadosCertame_Data_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATODADOSCERTAME_MODALIDADE", AV59TFContratoDadosCertame_Modalidade);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATODADOSCERTAME_MODALIDADE_SEL", AV60TFContratoDadosCertame_Modalidade_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATODADOSCERTAME_NUMERO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV63TFContratoDadosCertame_Numero), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATODADOSCERTAME_NUMERO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV64TFContratoDadosCertame_Numero_To), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATODADOSCERTAME_SITE", AV67TFContratoDadosCertame_Site);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATODADOSCERTAME_SITE_SEL", AV68TFContratoDadosCertame_Site_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATODADOSCERTAME_UASG", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV71TFContratoDadosCertame_Uasg), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATODADOSCERTAME_UASG_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV72TFContratoDadosCertame_Uasg_To), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO", context.localUtil.Format(AV75TFContratoDadosCertame_DataHomologacao, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO_TO", context.localUtil.Format(AV76TFContratoDadosCertame_DataHomologacao_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATODADOSCERTAME_DATAADJUDICACAO", context.localUtil.Format(AV81TFContratoDadosCertame_DataAdjudicacao, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATODADOSCERTAME_DATAADJUDICACAO_TO", context.localUtil.Format(AV82TFContratoDadosCertame_DataAdjudicacao_To, "99/99/99"));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_115", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_115), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV88GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV89GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV86DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV86DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATO_NUMEROTITLEFILTERDATA", AV48Contrato_NumeroTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATO_NUMEROTITLEFILTERDATA", AV48Contrato_NumeroTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATODADOSCERTAME_DATATITLEFILTERDATA", AV52ContratoDadosCertame_DataTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATODADOSCERTAME_DATATITLEFILTERDATA", AV52ContratoDadosCertame_DataTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATODADOSCERTAME_MODALIDADETITLEFILTERDATA", AV58ContratoDadosCertame_ModalidadeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATODADOSCERTAME_MODALIDADETITLEFILTERDATA", AV58ContratoDadosCertame_ModalidadeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATODADOSCERTAME_NUMEROTITLEFILTERDATA", AV62ContratoDadosCertame_NumeroTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATODADOSCERTAME_NUMEROTITLEFILTERDATA", AV62ContratoDadosCertame_NumeroTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATODADOSCERTAME_SITETITLEFILTERDATA", AV66ContratoDadosCertame_SiteTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATODADOSCERTAME_SITETITLEFILTERDATA", AV66ContratoDadosCertame_SiteTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATODADOSCERTAME_UASGTITLEFILTERDATA", AV70ContratoDadosCertame_UasgTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATODADOSCERTAME_UASGTITLEFILTERDATA", AV70ContratoDadosCertame_UasgTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATODADOSCERTAME_DATAHOMOLOGACAOTITLEFILTERDATA", AV74ContratoDadosCertame_DataHomologacaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATODADOSCERTAME_DATAHOMOLOGACAOTITLEFILTERDATA", AV74ContratoDadosCertame_DataHomologacaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATODADOSCERTAME_DATAADJUDICACAOTITLEFILTERDATA", AV80ContratoDadosCertame_DataAdjudicacaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATODADOSCERTAME_DATAADJUDICACAOTITLEFILTERDATA", AV80ContratoDadosCertame_DataAdjudicacaoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV130Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "CONTRATODADOSCERTAME_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A314ContratoDadosCertame_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Caption", StringUtil.RTrim( Ddo_contrato_numero_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Tooltip", StringUtil.RTrim( Ddo_contrato_numero_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Cls", StringUtil.RTrim( Ddo_contrato_numero_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filteredtext_set", StringUtil.RTrim( Ddo_contrato_numero_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Selectedvalue_set", StringUtil.RTrim( Ddo_contrato_numero_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contrato_numero_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contrato_numero_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includesortasc", StringUtil.BoolToStr( Ddo_contrato_numero_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includesortdsc", StringUtil.BoolToStr( Ddo_contrato_numero_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Sortedstatus", StringUtil.RTrim( Ddo_contrato_numero_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includefilter", StringUtil.BoolToStr( Ddo_contrato_numero_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filtertype", StringUtil.RTrim( Ddo_contrato_numero_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filterisrange", StringUtil.BoolToStr( Ddo_contrato_numero_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includedatalist", StringUtil.BoolToStr( Ddo_contrato_numero_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Datalisttype", StringUtil.RTrim( Ddo_contrato_numero_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Datalistproc", StringUtil.RTrim( Ddo_contrato_numero_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contrato_numero_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Sortasc", StringUtil.RTrim( Ddo_contrato_numero_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Sortdsc", StringUtil.RTrim( Ddo_contrato_numero_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Loadingdata", StringUtil.RTrim( Ddo_contrato_numero_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Cleanfilter", StringUtil.RTrim( Ddo_contrato_numero_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Noresultsfound", StringUtil.RTrim( Ddo_contrato_numero_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Searchbuttontext", StringUtil.RTrim( Ddo_contrato_numero_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATA_Caption", StringUtil.RTrim( Ddo_contratodadoscertame_data_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATA_Tooltip", StringUtil.RTrim( Ddo_contratodadoscertame_data_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATA_Cls", StringUtil.RTrim( Ddo_contratodadoscertame_data_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATA_Filteredtext_set", StringUtil.RTrim( Ddo_contratodadoscertame_data_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATA_Filteredtextto_set", StringUtil.RTrim( Ddo_contratodadoscertame_data_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratodadoscertame_data_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratodadoscertame_data_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATA_Includesortasc", StringUtil.BoolToStr( Ddo_contratodadoscertame_data_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATA_Includesortdsc", StringUtil.BoolToStr( Ddo_contratodadoscertame_data_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATA_Sortedstatus", StringUtil.RTrim( Ddo_contratodadoscertame_data_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATA_Includefilter", StringUtil.BoolToStr( Ddo_contratodadoscertame_data_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATA_Filtertype", StringUtil.RTrim( Ddo_contratodadoscertame_data_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATA_Filterisrange", StringUtil.BoolToStr( Ddo_contratodadoscertame_data_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATA_Includedatalist", StringUtil.BoolToStr( Ddo_contratodadoscertame_data_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATA_Sortasc", StringUtil.RTrim( Ddo_contratodadoscertame_data_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATA_Sortdsc", StringUtil.RTrim( Ddo_contratodadoscertame_data_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATA_Cleanfilter", StringUtil.RTrim( Ddo_contratodadoscertame_data_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATA_Rangefilterfrom", StringUtil.RTrim( Ddo_contratodadoscertame_data_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATA_Rangefilterto", StringUtil.RTrim( Ddo_contratodadoscertame_data_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATA_Searchbuttontext", StringUtil.RTrim( Ddo_contratodadoscertame_data_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_MODALIDADE_Caption", StringUtil.RTrim( Ddo_contratodadoscertame_modalidade_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_MODALIDADE_Tooltip", StringUtil.RTrim( Ddo_contratodadoscertame_modalidade_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_MODALIDADE_Cls", StringUtil.RTrim( Ddo_contratodadoscertame_modalidade_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_MODALIDADE_Filteredtext_set", StringUtil.RTrim( Ddo_contratodadoscertame_modalidade_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_MODALIDADE_Selectedvalue_set", StringUtil.RTrim( Ddo_contratodadoscertame_modalidade_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_MODALIDADE_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratodadoscertame_modalidade_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_MODALIDADE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratodadoscertame_modalidade_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_MODALIDADE_Includesortasc", StringUtil.BoolToStr( Ddo_contratodadoscertame_modalidade_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_MODALIDADE_Includesortdsc", StringUtil.BoolToStr( Ddo_contratodadoscertame_modalidade_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_MODALIDADE_Sortedstatus", StringUtil.RTrim( Ddo_contratodadoscertame_modalidade_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_MODALIDADE_Includefilter", StringUtil.BoolToStr( Ddo_contratodadoscertame_modalidade_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_MODALIDADE_Filtertype", StringUtil.RTrim( Ddo_contratodadoscertame_modalidade_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_MODALIDADE_Filterisrange", StringUtil.BoolToStr( Ddo_contratodadoscertame_modalidade_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_MODALIDADE_Includedatalist", StringUtil.BoolToStr( Ddo_contratodadoscertame_modalidade_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_MODALIDADE_Datalisttype", StringUtil.RTrim( Ddo_contratodadoscertame_modalidade_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_MODALIDADE_Datalistproc", StringUtil.RTrim( Ddo_contratodadoscertame_modalidade_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_MODALIDADE_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratodadoscertame_modalidade_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_MODALIDADE_Sortasc", StringUtil.RTrim( Ddo_contratodadoscertame_modalidade_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_MODALIDADE_Sortdsc", StringUtil.RTrim( Ddo_contratodadoscertame_modalidade_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_MODALIDADE_Loadingdata", StringUtil.RTrim( Ddo_contratodadoscertame_modalidade_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_MODALIDADE_Cleanfilter", StringUtil.RTrim( Ddo_contratodadoscertame_modalidade_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_MODALIDADE_Noresultsfound", StringUtil.RTrim( Ddo_contratodadoscertame_modalidade_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_MODALIDADE_Searchbuttontext", StringUtil.RTrim( Ddo_contratodadoscertame_modalidade_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_NUMERO_Caption", StringUtil.RTrim( Ddo_contratodadoscertame_numero_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_NUMERO_Tooltip", StringUtil.RTrim( Ddo_contratodadoscertame_numero_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_NUMERO_Cls", StringUtil.RTrim( Ddo_contratodadoscertame_numero_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_NUMERO_Filteredtext_set", StringUtil.RTrim( Ddo_contratodadoscertame_numero_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_NUMERO_Filteredtextto_set", StringUtil.RTrim( Ddo_contratodadoscertame_numero_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_NUMERO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratodadoscertame_numero_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_NUMERO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratodadoscertame_numero_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_NUMERO_Includesortasc", StringUtil.BoolToStr( Ddo_contratodadoscertame_numero_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_NUMERO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratodadoscertame_numero_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_NUMERO_Sortedstatus", StringUtil.RTrim( Ddo_contratodadoscertame_numero_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_NUMERO_Includefilter", StringUtil.BoolToStr( Ddo_contratodadoscertame_numero_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_NUMERO_Filtertype", StringUtil.RTrim( Ddo_contratodadoscertame_numero_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_NUMERO_Filterisrange", StringUtil.BoolToStr( Ddo_contratodadoscertame_numero_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_NUMERO_Includedatalist", StringUtil.BoolToStr( Ddo_contratodadoscertame_numero_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_NUMERO_Sortasc", StringUtil.RTrim( Ddo_contratodadoscertame_numero_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_NUMERO_Sortdsc", StringUtil.RTrim( Ddo_contratodadoscertame_numero_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_NUMERO_Cleanfilter", StringUtil.RTrim( Ddo_contratodadoscertame_numero_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_NUMERO_Rangefilterfrom", StringUtil.RTrim( Ddo_contratodadoscertame_numero_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_NUMERO_Rangefilterto", StringUtil.RTrim( Ddo_contratodadoscertame_numero_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_NUMERO_Searchbuttontext", StringUtil.RTrim( Ddo_contratodadoscertame_numero_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_SITE_Caption", StringUtil.RTrim( Ddo_contratodadoscertame_site_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_SITE_Tooltip", StringUtil.RTrim( Ddo_contratodadoscertame_site_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_SITE_Cls", StringUtil.RTrim( Ddo_contratodadoscertame_site_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_SITE_Filteredtext_set", StringUtil.RTrim( Ddo_contratodadoscertame_site_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_SITE_Selectedvalue_set", StringUtil.RTrim( Ddo_contratodadoscertame_site_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_SITE_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratodadoscertame_site_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_SITE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratodadoscertame_site_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_SITE_Includesortasc", StringUtil.BoolToStr( Ddo_contratodadoscertame_site_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_SITE_Includesortdsc", StringUtil.BoolToStr( Ddo_contratodadoscertame_site_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_SITE_Sortedstatus", StringUtil.RTrim( Ddo_contratodadoscertame_site_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_SITE_Includefilter", StringUtil.BoolToStr( Ddo_contratodadoscertame_site_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_SITE_Filtertype", StringUtil.RTrim( Ddo_contratodadoscertame_site_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_SITE_Filterisrange", StringUtil.BoolToStr( Ddo_contratodadoscertame_site_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_SITE_Includedatalist", StringUtil.BoolToStr( Ddo_contratodadoscertame_site_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_SITE_Datalisttype", StringUtil.RTrim( Ddo_contratodadoscertame_site_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_SITE_Datalistproc", StringUtil.RTrim( Ddo_contratodadoscertame_site_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_SITE_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratodadoscertame_site_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_SITE_Sortasc", StringUtil.RTrim( Ddo_contratodadoscertame_site_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_SITE_Sortdsc", StringUtil.RTrim( Ddo_contratodadoscertame_site_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_SITE_Loadingdata", StringUtil.RTrim( Ddo_contratodadoscertame_site_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_SITE_Cleanfilter", StringUtil.RTrim( Ddo_contratodadoscertame_site_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_SITE_Noresultsfound", StringUtil.RTrim( Ddo_contratodadoscertame_site_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_SITE_Searchbuttontext", StringUtil.RTrim( Ddo_contratodadoscertame_site_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_UASG_Caption", StringUtil.RTrim( Ddo_contratodadoscertame_uasg_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_UASG_Tooltip", StringUtil.RTrim( Ddo_contratodadoscertame_uasg_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_UASG_Cls", StringUtil.RTrim( Ddo_contratodadoscertame_uasg_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_UASG_Filteredtext_set", StringUtil.RTrim( Ddo_contratodadoscertame_uasg_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_UASG_Filteredtextto_set", StringUtil.RTrim( Ddo_contratodadoscertame_uasg_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_UASG_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratodadoscertame_uasg_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_UASG_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratodadoscertame_uasg_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_UASG_Includesortasc", StringUtil.BoolToStr( Ddo_contratodadoscertame_uasg_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_UASG_Includesortdsc", StringUtil.BoolToStr( Ddo_contratodadoscertame_uasg_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_UASG_Sortedstatus", StringUtil.RTrim( Ddo_contratodadoscertame_uasg_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_UASG_Includefilter", StringUtil.BoolToStr( Ddo_contratodadoscertame_uasg_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_UASG_Filtertype", StringUtil.RTrim( Ddo_contratodadoscertame_uasg_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_UASG_Filterisrange", StringUtil.BoolToStr( Ddo_contratodadoscertame_uasg_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_UASG_Includedatalist", StringUtil.BoolToStr( Ddo_contratodadoscertame_uasg_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_UASG_Sortasc", StringUtil.RTrim( Ddo_contratodadoscertame_uasg_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_UASG_Sortdsc", StringUtil.RTrim( Ddo_contratodadoscertame_uasg_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_UASG_Cleanfilter", StringUtil.RTrim( Ddo_contratodadoscertame_uasg_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_UASG_Rangefilterfrom", StringUtil.RTrim( Ddo_contratodadoscertame_uasg_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_UASG_Rangefilterto", StringUtil.RTrim( Ddo_contratodadoscertame_uasg_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_UASG_Searchbuttontext", StringUtil.RTrim( Ddo_contratodadoscertame_uasg_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Caption", StringUtil.RTrim( Ddo_contratodadoscertame_datahomologacao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Tooltip", StringUtil.RTrim( Ddo_contratodadoscertame_datahomologacao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Cls", StringUtil.RTrim( Ddo_contratodadoscertame_datahomologacao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Filteredtext_set", StringUtil.RTrim( Ddo_contratodadoscertame_datahomologacao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Filteredtextto_set", StringUtil.RTrim( Ddo_contratodadoscertame_datahomologacao_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratodadoscertame_datahomologacao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratodadoscertame_datahomologacao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Includesortasc", StringUtil.BoolToStr( Ddo_contratodadoscertame_datahomologacao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratodadoscertame_datahomologacao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Sortedstatus", StringUtil.RTrim( Ddo_contratodadoscertame_datahomologacao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Includefilter", StringUtil.BoolToStr( Ddo_contratodadoscertame_datahomologacao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Filtertype", StringUtil.RTrim( Ddo_contratodadoscertame_datahomologacao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Filterisrange", StringUtil.BoolToStr( Ddo_contratodadoscertame_datahomologacao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Includedatalist", StringUtil.BoolToStr( Ddo_contratodadoscertame_datahomologacao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Sortasc", StringUtil.RTrim( Ddo_contratodadoscertame_datahomologacao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Sortdsc", StringUtil.RTrim( Ddo_contratodadoscertame_datahomologacao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Cleanfilter", StringUtil.RTrim( Ddo_contratodadoscertame_datahomologacao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Rangefilterfrom", StringUtil.RTrim( Ddo_contratodadoscertame_datahomologacao_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Rangefilterto", StringUtil.RTrim( Ddo_contratodadoscertame_datahomologacao_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Searchbuttontext", StringUtil.RTrim( Ddo_contratodadoscertame_datahomologacao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Caption", StringUtil.RTrim( Ddo_contratodadoscertame_dataadjudicacao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Tooltip", StringUtil.RTrim( Ddo_contratodadoscertame_dataadjudicacao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Cls", StringUtil.RTrim( Ddo_contratodadoscertame_dataadjudicacao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Filteredtext_set", StringUtil.RTrim( Ddo_contratodadoscertame_dataadjudicacao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Filteredtextto_set", StringUtil.RTrim( Ddo_contratodadoscertame_dataadjudicacao_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratodadoscertame_dataadjudicacao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratodadoscertame_dataadjudicacao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Includesortasc", StringUtil.BoolToStr( Ddo_contratodadoscertame_dataadjudicacao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratodadoscertame_dataadjudicacao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Sortedstatus", StringUtil.RTrim( Ddo_contratodadoscertame_dataadjudicacao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Includefilter", StringUtil.BoolToStr( Ddo_contratodadoscertame_dataadjudicacao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Filtertype", StringUtil.RTrim( Ddo_contratodadoscertame_dataadjudicacao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Filterisrange", StringUtil.BoolToStr( Ddo_contratodadoscertame_dataadjudicacao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Includedatalist", StringUtil.BoolToStr( Ddo_contratodadoscertame_dataadjudicacao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Sortasc", StringUtil.RTrim( Ddo_contratodadoscertame_dataadjudicacao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Sortdsc", StringUtil.RTrim( Ddo_contratodadoscertame_dataadjudicacao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Cleanfilter", StringUtil.RTrim( Ddo_contratodadoscertame_dataadjudicacao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Rangefilterfrom", StringUtil.RTrim( Ddo_contratodadoscertame_dataadjudicacao_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Rangefilterto", StringUtil.RTrim( Ddo_contratodadoscertame_dataadjudicacao_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Searchbuttontext", StringUtil.RTrim( Ddo_contratodadoscertame_dataadjudicacao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Activeeventkey", StringUtil.RTrim( Ddo_contrato_numero_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filteredtext_get", StringUtil.RTrim( Ddo_contrato_numero_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Selectedvalue_get", StringUtil.RTrim( Ddo_contrato_numero_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATA_Activeeventkey", StringUtil.RTrim( Ddo_contratodadoscertame_data_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATA_Filteredtext_get", StringUtil.RTrim( Ddo_contratodadoscertame_data_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATA_Filteredtextto_get", StringUtil.RTrim( Ddo_contratodadoscertame_data_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_MODALIDADE_Activeeventkey", StringUtil.RTrim( Ddo_contratodadoscertame_modalidade_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_MODALIDADE_Filteredtext_get", StringUtil.RTrim( Ddo_contratodadoscertame_modalidade_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_MODALIDADE_Selectedvalue_get", StringUtil.RTrim( Ddo_contratodadoscertame_modalidade_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_NUMERO_Activeeventkey", StringUtil.RTrim( Ddo_contratodadoscertame_numero_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_NUMERO_Filteredtext_get", StringUtil.RTrim( Ddo_contratodadoscertame_numero_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_NUMERO_Filteredtextto_get", StringUtil.RTrim( Ddo_contratodadoscertame_numero_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_SITE_Activeeventkey", StringUtil.RTrim( Ddo_contratodadoscertame_site_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_SITE_Filteredtext_get", StringUtil.RTrim( Ddo_contratodadoscertame_site_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_SITE_Selectedvalue_get", StringUtil.RTrim( Ddo_contratodadoscertame_site_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_UASG_Activeeventkey", StringUtil.RTrim( Ddo_contratodadoscertame_uasg_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_UASG_Filteredtext_get", StringUtil.RTrim( Ddo_contratodadoscertame_uasg_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_UASG_Filteredtextto_get", StringUtil.RTrim( Ddo_contratodadoscertame_uasg_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Activeeventkey", StringUtil.RTrim( Ddo_contratodadoscertame_datahomologacao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Filteredtext_get", StringUtil.RTrim( Ddo_contratodadoscertame_datahomologacao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Filteredtextto_get", StringUtil.RTrim( Ddo_contratodadoscertame_datahomologacao_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Activeeventkey", StringUtil.RTrim( Ddo_contratodadoscertame_dataadjudicacao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Filteredtext_get", StringUtil.RTrim( Ddo_contratodadoscertame_dataadjudicacao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Filteredtextto_get", StringUtil.RTrim( Ddo_contratodadoscertame_dataadjudicacao_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE7G2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT7G2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwcontratodadoscertame.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWContratoDadosCertame" ;
      }

      public override String GetPgmdesc( )
      {
         return " Contrato Dados Certame" ;
      }

      protected void WB7G0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_7G2( true) ;
         }
         else
         {
            wb_table1_2_7G2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_7G2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 130,'',false,'" + sGXsfl_115_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(130, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,130);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 131,'',false,'" + sGXsfl_115_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(131, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,131);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 132,'',false,'" + sGXsfl_115_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_numero_Internalname, StringUtil.RTrim( AV49TFContrato_Numero), StringUtil.RTrim( context.localUtil.Format( AV49TFContrato_Numero, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,132);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_numero_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_numero_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoDadosCertame.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 133,'',false,'" + sGXsfl_115_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_numero_sel_Internalname, StringUtil.RTrim( AV50TFContrato_Numero_Sel), StringUtil.RTrim( context.localUtil.Format( AV50TFContrato_Numero_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,133);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_numero_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_numero_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoDadosCertame.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 134,'',false,'" + sGXsfl_115_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratodadoscertame_data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratodadoscertame_data_Internalname, context.localUtil.Format(AV53TFContratoDadosCertame_Data, "99/99/99"), context.localUtil.Format( AV53TFContratoDadosCertame_Data, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,134);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratodadoscertame_data_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratodadoscertame_data_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoDadosCertame.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratodadoscertame_data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratodadoscertame_data_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoDadosCertame.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 135,'',false,'" + sGXsfl_115_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratodadoscertame_data_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratodadoscertame_data_to_Internalname, context.localUtil.Format(AV54TFContratoDadosCertame_Data_To, "99/99/99"), context.localUtil.Format( AV54TFContratoDadosCertame_Data_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,135);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratodadoscertame_data_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratodadoscertame_data_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoDadosCertame.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratodadoscertame_data_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratodadoscertame_data_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoDadosCertame.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contratodadoscertame_dataauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 137,'',false,'" + sGXsfl_115_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratodadoscertame_dataauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratodadoscertame_dataauxdate_Internalname, context.localUtil.Format(AV55DDO_ContratoDadosCertame_DataAuxDate, "99/99/99"), context.localUtil.Format( AV55DDO_ContratoDadosCertame_DataAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,137);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratodadoscertame_dataauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoDadosCertame.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratodadoscertame_dataauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoDadosCertame.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 138,'',false,'" + sGXsfl_115_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratodadoscertame_dataauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratodadoscertame_dataauxdateto_Internalname, context.localUtil.Format(AV56DDO_ContratoDadosCertame_DataAuxDateTo, "99/99/99"), context.localUtil.Format( AV56DDO_ContratoDadosCertame_DataAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,138);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratodadoscertame_dataauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoDadosCertame.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratodadoscertame_dataauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoDadosCertame.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 139,'',false,'" + sGXsfl_115_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratodadoscertame_modalidade_Internalname, AV59TFContratoDadosCertame_Modalidade, StringUtil.RTrim( context.localUtil.Format( AV59TFContratoDadosCertame_Modalidade, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,139);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratodadoscertame_modalidade_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratodadoscertame_modalidade_Visible, 1, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoDadosCertame.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 140,'',false,'" + sGXsfl_115_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratodadoscertame_modalidade_sel_Internalname, AV60TFContratoDadosCertame_Modalidade_Sel, StringUtil.RTrim( context.localUtil.Format( AV60TFContratoDadosCertame_Modalidade_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,140);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratodadoscertame_modalidade_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratodadoscertame_modalidade_sel_Visible, 1, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoDadosCertame.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 141,'',false,'" + sGXsfl_115_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratodadoscertame_numero_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV63TFContratoDadosCertame_Numero), 10, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV63TFContratoDadosCertame_Numero), "ZZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,141);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratodadoscertame_numero_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratodadoscertame_numero_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoDadosCertame.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 142,'',false,'" + sGXsfl_115_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratodadoscertame_numero_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV64TFContratoDadosCertame_Numero_To), 10, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV64TFContratoDadosCertame_Numero_To), "ZZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,142);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratodadoscertame_numero_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratodadoscertame_numero_to_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoDadosCertame.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 143,'',false,'" + sGXsfl_115_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratodadoscertame_site_Internalname, AV67TFContratoDadosCertame_Site, StringUtil.RTrim( context.localUtil.Format( AV67TFContratoDadosCertame_Site, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,143);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratodadoscertame_site_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratodadoscertame_site_Visible, 1, 0, "text", "", 360, "px", 1, "row", 1000, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoDadosCertame.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 144,'',false,'" + sGXsfl_115_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratodadoscertame_site_sel_Internalname, AV68TFContratoDadosCertame_Site_Sel, StringUtil.RTrim( context.localUtil.Format( AV68TFContratoDadosCertame_Site_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,144);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratodadoscertame_site_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratodadoscertame_site_sel_Visible, 1, 0, "text", "", 360, "px", 1, "row", 1000, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoDadosCertame.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 145,'',false,'" + sGXsfl_115_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratodadoscertame_uasg_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV71TFContratoDadosCertame_Uasg), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV71TFContratoDadosCertame_Uasg), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,145);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratodadoscertame_uasg_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratodadoscertame_uasg_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoDadosCertame.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 146,'',false,'" + sGXsfl_115_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratodadoscertame_uasg_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV72TFContratoDadosCertame_Uasg_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV72TFContratoDadosCertame_Uasg_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,146);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratodadoscertame_uasg_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratodadoscertame_uasg_to_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoDadosCertame.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 147,'',false,'" + sGXsfl_115_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratodadoscertame_datahomologacao_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratodadoscertame_datahomologacao_Internalname, context.localUtil.Format(AV75TFContratoDadosCertame_DataHomologacao, "99/99/99"), context.localUtil.Format( AV75TFContratoDadosCertame_DataHomologacao, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,147);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratodadoscertame_datahomologacao_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratodadoscertame_datahomologacao_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoDadosCertame.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratodadoscertame_datahomologacao_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratodadoscertame_datahomologacao_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoDadosCertame.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 148,'',false,'" + sGXsfl_115_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratodadoscertame_datahomologacao_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratodadoscertame_datahomologacao_to_Internalname, context.localUtil.Format(AV76TFContratoDadosCertame_DataHomologacao_To, "99/99/99"), context.localUtil.Format( AV76TFContratoDadosCertame_DataHomologacao_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,148);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratodadoscertame_datahomologacao_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratodadoscertame_datahomologacao_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoDadosCertame.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratodadoscertame_datahomologacao_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratodadoscertame_datahomologacao_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoDadosCertame.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contratodadoscertame_datahomologacaoauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 150,'',false,'" + sGXsfl_115_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratodadoscertame_datahomologacaoauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratodadoscertame_datahomologacaoauxdate_Internalname, context.localUtil.Format(AV77DDO_ContratoDadosCertame_DataHomologacaoAuxDate, "99/99/99"), context.localUtil.Format( AV77DDO_ContratoDadosCertame_DataHomologacaoAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,150);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratodadoscertame_datahomologacaoauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoDadosCertame.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratodadoscertame_datahomologacaoauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoDadosCertame.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 151,'',false,'" + sGXsfl_115_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratodadoscertame_datahomologacaoauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratodadoscertame_datahomologacaoauxdateto_Internalname, context.localUtil.Format(AV78DDO_ContratoDadosCertame_DataHomologacaoAuxDateTo, "99/99/99"), context.localUtil.Format( AV78DDO_ContratoDadosCertame_DataHomologacaoAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,151);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratodadoscertame_datahomologacaoauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoDadosCertame.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratodadoscertame_datahomologacaoauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoDadosCertame.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 152,'',false,'" + sGXsfl_115_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratodadoscertame_dataadjudicacao_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratodadoscertame_dataadjudicacao_Internalname, context.localUtil.Format(AV81TFContratoDadosCertame_DataAdjudicacao, "99/99/99"), context.localUtil.Format( AV81TFContratoDadosCertame_DataAdjudicacao, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,152);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratodadoscertame_dataadjudicacao_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratodadoscertame_dataadjudicacao_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoDadosCertame.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratodadoscertame_dataadjudicacao_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratodadoscertame_dataadjudicacao_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoDadosCertame.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 153,'',false,'" + sGXsfl_115_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratodadoscertame_dataadjudicacao_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratodadoscertame_dataadjudicacao_to_Internalname, context.localUtil.Format(AV82TFContratoDadosCertame_DataAdjudicacao_To, "99/99/99"), context.localUtil.Format( AV82TFContratoDadosCertame_DataAdjudicacao_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,153);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratodadoscertame_dataadjudicacao_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratodadoscertame_dataadjudicacao_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoDadosCertame.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratodadoscertame_dataadjudicacao_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratodadoscertame_dataadjudicacao_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoDadosCertame.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contratodadoscertame_dataadjudicacaoauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 155,'',false,'" + sGXsfl_115_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratodadoscertame_dataadjudicacaoauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratodadoscertame_dataadjudicacaoauxdate_Internalname, context.localUtil.Format(AV83DDO_ContratoDadosCertame_DataAdjudicacaoAuxDate, "99/99/99"), context.localUtil.Format( AV83DDO_ContratoDadosCertame_DataAdjudicacaoAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,155);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratodadoscertame_dataadjudicacaoauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoDadosCertame.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratodadoscertame_dataadjudicacaoauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoDadosCertame.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 156,'',false,'" + sGXsfl_115_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratodadoscertame_dataadjudicacaoauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratodadoscertame_dataadjudicacaoauxdateto_Internalname, context.localUtil.Format(AV84DDO_ContratoDadosCertame_DataAdjudicacaoAuxDateTo, "99/99/99"), context.localUtil.Format( AV84DDO_ContratoDadosCertame_DataAdjudicacaoAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,156);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratodadoscertame_dataadjudicacaoauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoDadosCertame.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratodadoscertame_dataadjudicacaoauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoDadosCertame.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATO_NUMEROContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 158,'',false,'" + sGXsfl_115_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname, AV51ddo_Contrato_NumeroTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,158);\"", 0, edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoDadosCertame.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATODADOSCERTAME_DATAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 160,'',false,'" + sGXsfl_115_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratodadoscertame_datatitlecontrolidtoreplace_Internalname, AV57ddo_ContratoDadosCertame_DataTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,160);\"", 0, edtavDdo_contratodadoscertame_datatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoDadosCertame.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATODADOSCERTAME_MODALIDADEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 162,'',false,'" + sGXsfl_115_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratodadoscertame_modalidadetitlecontrolidtoreplace_Internalname, AV61ddo_ContratoDadosCertame_ModalidadeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,162);\"", 0, edtavDdo_contratodadoscertame_modalidadetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoDadosCertame.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATODADOSCERTAME_NUMEROContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 164,'',false,'" + sGXsfl_115_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratodadoscertame_numerotitlecontrolidtoreplace_Internalname, AV65ddo_ContratoDadosCertame_NumeroTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,164);\"", 0, edtavDdo_contratodadoscertame_numerotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoDadosCertame.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATODADOSCERTAME_SITEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 166,'',false,'" + sGXsfl_115_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratodadoscertame_sitetitlecontrolidtoreplace_Internalname, AV69ddo_ContratoDadosCertame_SiteTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,166);\"", 0, edtavDdo_contratodadoscertame_sitetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoDadosCertame.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATODADOSCERTAME_UASGContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 168,'',false,'" + sGXsfl_115_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratodadoscertame_uasgtitlecontrolidtoreplace_Internalname, AV73ddo_ContratoDadosCertame_UasgTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,168);\"", 0, edtavDdo_contratodadoscertame_uasgtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoDadosCertame.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 170,'',false,'" + sGXsfl_115_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratodadoscertame_datahomologacaotitlecontrolidtoreplace_Internalname, AV79ddo_ContratoDadosCertame_DataHomologacaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,170);\"", 0, edtavDdo_contratodadoscertame_datahomologacaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoDadosCertame.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATODADOSCERTAME_DATAADJUDICACAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 172,'',false,'" + sGXsfl_115_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratodadoscertame_dataadjudicacaotitlecontrolidtoreplace_Internalname, AV85ddo_ContratoDadosCertame_DataAdjudicacaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,172);\"", 0, edtavDdo_contratodadoscertame_dataadjudicacaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoDadosCertame.htm");
         }
         wbLoad = true;
      }

      protected void START7G2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Contrato Dados Certame", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP7G0( ) ;
      }

      protected void WS7G2( )
      {
         START7G2( ) ;
         EVT7G2( ) ;
      }

      protected void EVT7G2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E117G2 */
                              E117G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATO_NUMERO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E127G2 */
                              E127G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATODADOSCERTAME_DATA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E137G2 */
                              E137G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATODADOSCERTAME_MODALIDADE.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E147G2 */
                              E147G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATODADOSCERTAME_NUMERO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E157G2 */
                              E157G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATODADOSCERTAME_SITE.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E167G2 */
                              E167G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATODADOSCERTAME_UASG.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E177G2 */
                              E177G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E187G2 */
                              E187G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E197G2 */
                              E197G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E207G2 */
                              E207G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E217G2 */
                              E217G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E227G2 */
                              E227G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E237G2 */
                              E237G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E247G2 */
                              E247G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E257G2 */
                              E257G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E267G2 */
                              E267G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E277G2 */
                              E277G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E287G2 */
                              E287G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E297G2 */
                              E297G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E307G2 */
                              E307G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_115_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_115_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_115_idx), 4, 0)), 4, "0");
                              SubsflControlProps_1152( ) ;
                              AV28Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV128Update_GXI : context.convertURL( context.PathToRelativeUrl( AV28Update))));
                              AV29Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV129Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV29Delete))));
                              A77Contrato_Numero = cgiGet( edtContrato_Numero_Internalname);
                              A311ContratoDadosCertame_Data = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContratoDadosCertame_Data_Internalname), 0));
                              A307ContratoDadosCertame_Modalidade = cgiGet( edtContratoDadosCertame_Modalidade_Internalname);
                              A308ContratoDadosCertame_Numero = (long)(context.localUtil.CToN( cgiGet( edtContratoDadosCertame_Numero_Internalname), ",", "."));
                              n308ContratoDadosCertame_Numero = false;
                              A309ContratoDadosCertame_Site = cgiGet( edtContratoDadosCertame_Site_Internalname);
                              n309ContratoDadosCertame_Site = false;
                              A310ContratoDadosCertame_Uasg = (short)(context.localUtil.CToN( cgiGet( edtContratoDadosCertame_Uasg_Internalname), ",", "."));
                              n310ContratoDadosCertame_Uasg = false;
                              A312ContratoDadosCertame_DataHomologacao = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContratoDadosCertame_DataHomologacao_Internalname), 0));
                              n312ContratoDadosCertame_DataHomologacao = false;
                              A313ContratoDadosCertame_DataAdjudicacao = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContratoDadosCertame_DataAdjudicacao_Internalname), 0));
                              n313ContratoDadosCertame_DataAdjudicacao = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E317G2 */
                                    E317G2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E327G2 */
                                    E327G2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E337G2 */
                                    E337G2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratodadoscertame_data1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATODADOSCERTAME_DATA1"), 0) != AV37ContratoDadosCertame_Data1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratodadoscertame_data_to1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATODADOSCERTAME_DATA_TO1"), 0) != AV38ContratoDadosCertame_Data_To1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratodadoscertame_modalidade1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATODADOSCERTAME_MODALIDADE1"), AV17ContratoDadosCertame_Modalidade1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratodadoscertame_numero1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATODADOSCERTAME_NUMERO1"), ",", ".") != Convert.ToDecimal( AV39ContratoDadosCertame_Numero1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratodadoscertame_data2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATODADOSCERTAME_DATA2"), 0) != AV40ContratoDadosCertame_Data2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratodadoscertame_data_to2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATODADOSCERTAME_DATA_TO2"), 0) != AV41ContratoDadosCertame_Data_To2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratodadoscertame_modalidade2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATODADOSCERTAME_MODALIDADE2"), AV21ContratoDadosCertame_Modalidade2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratodadoscertame_numero2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATODADOSCERTAME_NUMERO2"), ",", ".") != Convert.ToDecimal( AV42ContratoDadosCertame_Numero2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratodadoscertame_data3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATODADOSCERTAME_DATA3"), 0) != AV43ContratoDadosCertame_Data3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratodadoscertame_data_to3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATODADOSCERTAME_DATA_TO3"), 0) != AV44ContratoDadosCertame_Data_To3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratodadoscertame_modalidade3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATODADOSCERTAME_MODALIDADE3"), AV25ContratoDadosCertame_Modalidade3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratodadoscertame_numero3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATODADOSCERTAME_NUMERO3"), ",", ".") != Convert.ToDecimal( AV45ContratoDadosCertame_Numero3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontrato_numero Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO"), AV49TFContrato_Numero) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontrato_numero_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO_SEL"), AV50TFContrato_Numero_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratodadoscertame_data Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATODADOSCERTAME_DATA"), 0) != AV53TFContratoDadosCertame_Data )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratodadoscertame_data_to Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATODADOSCERTAME_DATA_TO"), 0) != AV54TFContratoDadosCertame_Data_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratodadoscertame_modalidade Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATODADOSCERTAME_MODALIDADE"), AV59TFContratoDadosCertame_Modalidade) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratodadoscertame_modalidade_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATODADOSCERTAME_MODALIDADE_SEL"), AV60TFContratoDadosCertame_Modalidade_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratodadoscertame_numero Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATODADOSCERTAME_NUMERO"), ",", ".") != Convert.ToDecimal( AV63TFContratoDadosCertame_Numero )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratodadoscertame_numero_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATODADOSCERTAME_NUMERO_TO"), ",", ".") != Convert.ToDecimal( AV64TFContratoDadosCertame_Numero_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratodadoscertame_site Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATODADOSCERTAME_SITE"), AV67TFContratoDadosCertame_Site) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratodadoscertame_site_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATODADOSCERTAME_SITE_SEL"), AV68TFContratoDadosCertame_Site_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratodadoscertame_uasg Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATODADOSCERTAME_UASG"), ",", ".") != Convert.ToDecimal( AV71TFContratoDadosCertame_Uasg )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratodadoscertame_uasg_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATODADOSCERTAME_UASG_TO"), ",", ".") != Convert.ToDecimal( AV72TFContratoDadosCertame_Uasg_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratodadoscertame_datahomologacao Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO"), 0) != AV75TFContratoDadosCertame_DataHomologacao )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratodadoscertame_datahomologacao_to Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO_TO"), 0) != AV76TFContratoDadosCertame_DataHomologacao_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratodadoscertame_dataadjudicacao Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATODADOSCERTAME_DATAADJUDICACAO"), 0) != AV81TFContratoDadosCertame_DataAdjudicacao )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratodadoscertame_dataadjudicacao_to Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATODADOSCERTAME_DATAADJUDICACAO_TO"), 0) != AV82TFContratoDadosCertame_DataAdjudicacao_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE7G2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA7G2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTRATODADOSCERTAME_DATA", "Data", 0);
            cmbavDynamicfiltersselector1.addItem("CONTRATODADOSCERTAME_MODALIDADE", "Modalidade", 0);
            cmbavDynamicfiltersselector1.addItem("CONTRATODADOSCERTAME_NUMERO", "Numero", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTRATODADOSCERTAME_DATA", "Data", 0);
            cmbavDynamicfiltersselector2.addItem("CONTRATODADOSCERTAME_MODALIDADE", "Modalidade", 0);
            cmbavDynamicfiltersselector2.addItem("CONTRATODADOSCERTAME_NUMERO", "Numero", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CONTRATODADOSCERTAME_DATA", "Data", 0);
            cmbavDynamicfiltersselector3.addItem("CONTRATODADOSCERTAME_MODALIDADE", "Modalidade", 0);
            cmbavDynamicfiltersselector3.addItem("CONTRATODADOSCERTAME_NUMERO", "Numero", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_1152( ) ;
         while ( nGXsfl_115_idx <= nRC_GXsfl_115 )
         {
            sendrow_1152( ) ;
            nGXsfl_115_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_115_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_115_idx+1));
            sGXsfl_115_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_115_idx), 4, 0)), 4, "0");
            SubsflControlProps_1152( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       DateTime AV37ContratoDadosCertame_Data1 ,
                                       DateTime AV38ContratoDadosCertame_Data_To1 ,
                                       String AV17ContratoDadosCertame_Modalidade1 ,
                                       long AV39ContratoDadosCertame_Numero1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       short AV20DynamicFiltersOperator2 ,
                                       DateTime AV40ContratoDadosCertame_Data2 ,
                                       DateTime AV41ContratoDadosCertame_Data_To2 ,
                                       String AV21ContratoDadosCertame_Modalidade2 ,
                                       long AV42ContratoDadosCertame_Numero2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       short AV24DynamicFiltersOperator3 ,
                                       DateTime AV43ContratoDadosCertame_Data3 ,
                                       DateTime AV44ContratoDadosCertame_Data_To3 ,
                                       String AV25ContratoDadosCertame_Modalidade3 ,
                                       long AV45ContratoDadosCertame_Numero3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       String AV49TFContrato_Numero ,
                                       String AV50TFContrato_Numero_Sel ,
                                       DateTime AV53TFContratoDadosCertame_Data ,
                                       DateTime AV54TFContratoDadosCertame_Data_To ,
                                       String AV59TFContratoDadosCertame_Modalidade ,
                                       String AV60TFContratoDadosCertame_Modalidade_Sel ,
                                       long AV63TFContratoDadosCertame_Numero ,
                                       long AV64TFContratoDadosCertame_Numero_To ,
                                       String AV67TFContratoDadosCertame_Site ,
                                       String AV68TFContratoDadosCertame_Site_Sel ,
                                       short AV71TFContratoDadosCertame_Uasg ,
                                       short AV72TFContratoDadosCertame_Uasg_To ,
                                       DateTime AV75TFContratoDadosCertame_DataHomologacao ,
                                       DateTime AV76TFContratoDadosCertame_DataHomologacao_To ,
                                       DateTime AV81TFContratoDadosCertame_DataAdjudicacao ,
                                       DateTime AV82TFContratoDadosCertame_DataAdjudicacao_To ,
                                       String AV51ddo_Contrato_NumeroTitleControlIdToReplace ,
                                       String AV57ddo_ContratoDadosCertame_DataTitleControlIdToReplace ,
                                       String AV61ddo_ContratoDadosCertame_ModalidadeTitleControlIdToReplace ,
                                       String AV65ddo_ContratoDadosCertame_NumeroTitleControlIdToReplace ,
                                       String AV69ddo_ContratoDadosCertame_SiteTitleControlIdToReplace ,
                                       String AV73ddo_ContratoDadosCertame_UasgTitleControlIdToReplace ,
                                       String AV79ddo_ContratoDadosCertame_DataHomologacaoTitleControlIdToReplace ,
                                       String AV85ddo_ContratoDadosCertame_DataAdjudicacaoTitleControlIdToReplace ,
                                       String AV130Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving ,
                                       int A314ContratoDadosCertame_Codigo ,
                                       int A74Contrato_Codigo ,
                                       String A309ContratoDadosCertame_Site )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF7G2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATODADOSCERTAME_DATA", GetSecureSignedToken( "", A311ContratoDadosCertame_Data));
         GxWebStd.gx_hidden_field( context, "CONTRATODADOSCERTAME_DATA", context.localUtil.Format(A311ContratoDadosCertame_Data, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATODADOSCERTAME_MODALIDADE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A307ContratoDadosCertame_Modalidade, ""))));
         GxWebStd.gx_hidden_field( context, "CONTRATODADOSCERTAME_MODALIDADE", A307ContratoDadosCertame_Modalidade);
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATODADOSCERTAME_NUMERO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A308ContratoDadosCertame_Numero), "ZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATODADOSCERTAME_NUMERO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A308ContratoDadosCertame_Numero), 10, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATODADOSCERTAME_SITE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A309ContratoDadosCertame_Site, ""))));
         GxWebStd.gx_hidden_field( context, "CONTRATODADOSCERTAME_SITE", A309ContratoDadosCertame_Site);
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATODADOSCERTAME_UASG", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A310ContratoDadosCertame_Uasg), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATODADOSCERTAME_UASG", StringUtil.LTrim( StringUtil.NToC( (decimal)(A310ContratoDadosCertame_Uasg), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATODADOSCERTAME_DATAHOMOLOGACAO", GetSecureSignedToken( "", A312ContratoDadosCertame_DataHomologacao));
         GxWebStd.gx_hidden_field( context, "CONTRATODADOSCERTAME_DATAHOMOLOGACAO", context.localUtil.Format(A312ContratoDadosCertame_DataHomologacao, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATODADOSCERTAME_DATAADJUDICACAO", GetSecureSignedToken( "", A313ContratoDadosCertame_DataAdjudicacao));
         GxWebStd.gx_hidden_field( context, "CONTRATODADOSCERTAME_DATAADJUDICACAO", context.localUtil.Format(A313ContratoDadosCertame_DataAdjudicacao, "99/99/99"));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF7G2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV130Pgmname = "WWContratoDadosCertame";
         context.Gx_err = 0;
      }

      protected void RF7G2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 115;
         /* Execute user event: E327G2 */
         E327G2 ();
         nGXsfl_115_idx = 1;
         sGXsfl_115_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_115_idx), 4, 0)), 4, "0");
         SubsflControlProps_1152( ) ;
         nGXsfl_115_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_1152( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV92WWContratoDadosCertameDS_1_Dynamicfiltersselector1 ,
                                                 AV94WWContratoDadosCertameDS_3_Contratodadoscertame_data1 ,
                                                 AV95WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1 ,
                                                 AV93WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 ,
                                                 AV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1 ,
                                                 AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1 ,
                                                 AV98WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 ,
                                                 AV99WWContratoDadosCertameDS_8_Dynamicfiltersselector2 ,
                                                 AV101WWContratoDadosCertameDS_10_Contratodadoscertame_data2 ,
                                                 AV102WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2 ,
                                                 AV100WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 ,
                                                 AV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2 ,
                                                 AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2 ,
                                                 AV105WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 ,
                                                 AV106WWContratoDadosCertameDS_15_Dynamicfiltersselector3 ,
                                                 AV108WWContratoDadosCertameDS_17_Contratodadoscertame_data3 ,
                                                 AV109WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3 ,
                                                 AV107WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 ,
                                                 AV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3 ,
                                                 AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3 ,
                                                 AV113WWContratoDadosCertameDS_22_Tfcontrato_numero_sel ,
                                                 AV112WWContratoDadosCertameDS_21_Tfcontrato_numero ,
                                                 AV114WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data ,
                                                 AV115WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to ,
                                                 AV117WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel ,
                                                 AV116WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade ,
                                                 AV118WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero ,
                                                 AV119WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to ,
                                                 AV121WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel ,
                                                 AV120WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site ,
                                                 AV122WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg ,
                                                 AV123WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to ,
                                                 AV124WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao ,
                                                 AV125WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to ,
                                                 AV126WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao ,
                                                 AV127WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to ,
                                                 A311ContratoDadosCertame_Data ,
                                                 A307ContratoDadosCertame_Modalidade ,
                                                 A308ContratoDadosCertame_Numero ,
                                                 A77Contrato_Numero ,
                                                 A309ContratoDadosCertame_Site ,
                                                 A310ContratoDadosCertame_Uasg ,
                                                 A312ContratoDadosCertame_DataHomologacao ,
                                                 A313ContratoDadosCertame_DataAdjudicacao ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.LONG, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                                 TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.LONG, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.LONG,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.LONG, TypeConstants.LONG, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.LONG, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                                 TypeConstants.BOOLEAN
                                                 }
            });
            lV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1 = StringUtil.Concat( StringUtil.RTrim( AV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1), "%", "");
            lV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1 = StringUtil.Concat( StringUtil.RTrim( AV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1), "%", "");
            lV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2 = StringUtil.Concat( StringUtil.RTrim( AV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2), "%", "");
            lV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2 = StringUtil.Concat( StringUtil.RTrim( AV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2), "%", "");
            lV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3 = StringUtil.Concat( StringUtil.RTrim( AV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3), "%", "");
            lV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3 = StringUtil.Concat( StringUtil.RTrim( AV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3), "%", "");
            lV112WWContratoDadosCertameDS_21_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV112WWContratoDadosCertameDS_21_Tfcontrato_numero), 20, "%");
            lV116WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade = StringUtil.Concat( StringUtil.RTrim( AV116WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade), "%", "");
            lV120WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site = StringUtil.Concat( StringUtil.RTrim( AV120WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site), "%", "");
            /* Using cursor H007G2 */
            pr_default.execute(0, new Object[] {AV94WWContratoDadosCertameDS_3_Contratodadoscertame_data1, AV95WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1, lV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1, lV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1, AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1, AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1, AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1, AV101WWContratoDadosCertameDS_10_Contratodadoscertame_data2, AV102WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2, lV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2, lV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2, AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2, AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2, AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2, AV108WWContratoDadosCertameDS_17_Contratodadoscertame_data3, AV109WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3, lV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3, lV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3, AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3, AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3, AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3, lV112WWContratoDadosCertameDS_21_Tfcontrato_numero, AV113WWContratoDadosCertameDS_22_Tfcontrato_numero_sel, AV114WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data, AV115WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to, lV116WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade, AV117WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel, AV118WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero, AV119WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to, lV120WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site, AV121WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel, AV122WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg, AV123WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to, AV124WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao, AV125WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to, AV126WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao, AV127WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_115_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A74Contrato_Codigo = H007G2_A74Contrato_Codigo[0];
               A314ContratoDadosCertame_Codigo = H007G2_A314ContratoDadosCertame_Codigo[0];
               A313ContratoDadosCertame_DataAdjudicacao = H007G2_A313ContratoDadosCertame_DataAdjudicacao[0];
               n313ContratoDadosCertame_DataAdjudicacao = H007G2_n313ContratoDadosCertame_DataAdjudicacao[0];
               A312ContratoDadosCertame_DataHomologacao = H007G2_A312ContratoDadosCertame_DataHomologacao[0];
               n312ContratoDadosCertame_DataHomologacao = H007G2_n312ContratoDadosCertame_DataHomologacao[0];
               A310ContratoDadosCertame_Uasg = H007G2_A310ContratoDadosCertame_Uasg[0];
               n310ContratoDadosCertame_Uasg = H007G2_n310ContratoDadosCertame_Uasg[0];
               A309ContratoDadosCertame_Site = H007G2_A309ContratoDadosCertame_Site[0];
               n309ContratoDadosCertame_Site = H007G2_n309ContratoDadosCertame_Site[0];
               A308ContratoDadosCertame_Numero = H007G2_A308ContratoDadosCertame_Numero[0];
               n308ContratoDadosCertame_Numero = H007G2_n308ContratoDadosCertame_Numero[0];
               A307ContratoDadosCertame_Modalidade = H007G2_A307ContratoDadosCertame_Modalidade[0];
               A311ContratoDadosCertame_Data = H007G2_A311ContratoDadosCertame_Data[0];
               A77Contrato_Numero = H007G2_A77Contrato_Numero[0];
               A77Contrato_Numero = H007G2_A77Contrato_Numero[0];
               /* Execute user event: E337G2 */
               E337G2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 115;
            WB7G0( ) ;
         }
         nGXsfl_115_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV92WWContratoDadosCertameDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV93WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV94WWContratoDadosCertameDS_3_Contratodadoscertame_data1 = AV37ContratoDadosCertame_Data1;
         AV95WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1 = AV38ContratoDadosCertame_Data_To1;
         AV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1 = AV17ContratoDadosCertame_Modalidade1;
         AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1 = AV39ContratoDadosCertame_Numero1;
         AV98WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV99WWContratoDadosCertameDS_8_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV100WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV101WWContratoDadosCertameDS_10_Contratodadoscertame_data2 = AV40ContratoDadosCertame_Data2;
         AV102WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2 = AV41ContratoDadosCertame_Data_To2;
         AV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2 = AV21ContratoDadosCertame_Modalidade2;
         AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2 = AV42ContratoDadosCertame_Numero2;
         AV105WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV106WWContratoDadosCertameDS_15_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV107WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV108WWContratoDadosCertameDS_17_Contratodadoscertame_data3 = AV43ContratoDadosCertame_Data3;
         AV109WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3 = AV44ContratoDadosCertame_Data_To3;
         AV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3 = AV25ContratoDadosCertame_Modalidade3;
         AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3 = AV45ContratoDadosCertame_Numero3;
         AV112WWContratoDadosCertameDS_21_Tfcontrato_numero = AV49TFContrato_Numero;
         AV113WWContratoDadosCertameDS_22_Tfcontrato_numero_sel = AV50TFContrato_Numero_Sel;
         AV114WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data = AV53TFContratoDadosCertame_Data;
         AV115WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to = AV54TFContratoDadosCertame_Data_To;
         AV116WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade = AV59TFContratoDadosCertame_Modalidade;
         AV117WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel = AV60TFContratoDadosCertame_Modalidade_Sel;
         AV118WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero = AV63TFContratoDadosCertame_Numero;
         AV119WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to = AV64TFContratoDadosCertame_Numero_To;
         AV120WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site = AV67TFContratoDadosCertame_Site;
         AV121WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel = AV68TFContratoDadosCertame_Site_Sel;
         AV122WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg = AV71TFContratoDadosCertame_Uasg;
         AV123WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to = AV72TFContratoDadosCertame_Uasg_To;
         AV124WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao = AV75TFContratoDadosCertame_DataHomologacao;
         AV125WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to = AV76TFContratoDadosCertame_DataHomologacao_To;
         AV126WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao = AV81TFContratoDadosCertame_DataAdjudicacao;
         AV127WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to = AV82TFContratoDadosCertame_DataAdjudicacao_To;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV92WWContratoDadosCertameDS_1_Dynamicfiltersselector1 ,
                                              AV94WWContratoDadosCertameDS_3_Contratodadoscertame_data1 ,
                                              AV95WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1 ,
                                              AV93WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 ,
                                              AV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1 ,
                                              AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1 ,
                                              AV98WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 ,
                                              AV99WWContratoDadosCertameDS_8_Dynamicfiltersselector2 ,
                                              AV101WWContratoDadosCertameDS_10_Contratodadoscertame_data2 ,
                                              AV102WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2 ,
                                              AV100WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 ,
                                              AV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2 ,
                                              AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2 ,
                                              AV105WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 ,
                                              AV106WWContratoDadosCertameDS_15_Dynamicfiltersselector3 ,
                                              AV108WWContratoDadosCertameDS_17_Contratodadoscertame_data3 ,
                                              AV109WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3 ,
                                              AV107WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 ,
                                              AV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3 ,
                                              AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3 ,
                                              AV113WWContratoDadosCertameDS_22_Tfcontrato_numero_sel ,
                                              AV112WWContratoDadosCertameDS_21_Tfcontrato_numero ,
                                              AV114WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data ,
                                              AV115WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to ,
                                              AV117WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel ,
                                              AV116WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade ,
                                              AV118WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero ,
                                              AV119WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to ,
                                              AV121WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel ,
                                              AV120WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site ,
                                              AV122WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg ,
                                              AV123WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to ,
                                              AV124WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao ,
                                              AV125WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to ,
                                              AV126WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao ,
                                              AV127WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to ,
                                              A311ContratoDadosCertame_Data ,
                                              A307ContratoDadosCertame_Modalidade ,
                                              A308ContratoDadosCertame_Numero ,
                                              A77Contrato_Numero ,
                                              A309ContratoDadosCertame_Site ,
                                              A310ContratoDadosCertame_Uasg ,
                                              A312ContratoDadosCertame_DataHomologacao ,
                                              A313ContratoDadosCertame_DataAdjudicacao ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.LONG, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.LONG, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.LONG,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.LONG, TypeConstants.LONG, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.LONG, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                              TypeConstants.BOOLEAN
                                              }
         });
         lV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1 = StringUtil.Concat( StringUtil.RTrim( AV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1), "%", "");
         lV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1 = StringUtil.Concat( StringUtil.RTrim( AV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1), "%", "");
         lV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2 = StringUtil.Concat( StringUtil.RTrim( AV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2), "%", "");
         lV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2 = StringUtil.Concat( StringUtil.RTrim( AV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2), "%", "");
         lV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3 = StringUtil.Concat( StringUtil.RTrim( AV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3), "%", "");
         lV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3 = StringUtil.Concat( StringUtil.RTrim( AV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3), "%", "");
         lV112WWContratoDadosCertameDS_21_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV112WWContratoDadosCertameDS_21_Tfcontrato_numero), 20, "%");
         lV116WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade = StringUtil.Concat( StringUtil.RTrim( AV116WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade), "%", "");
         lV120WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site = StringUtil.Concat( StringUtil.RTrim( AV120WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site), "%", "");
         /* Using cursor H007G3 */
         pr_default.execute(1, new Object[] {AV94WWContratoDadosCertameDS_3_Contratodadoscertame_data1, AV95WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1, lV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1, lV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1, AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1, AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1, AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1, AV101WWContratoDadosCertameDS_10_Contratodadoscertame_data2, AV102WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2, lV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2, lV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2, AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2, AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2, AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2, AV108WWContratoDadosCertameDS_17_Contratodadoscertame_data3, AV109WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3, lV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3, lV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3, AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3, AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3, AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3, lV112WWContratoDadosCertameDS_21_Tfcontrato_numero, AV113WWContratoDadosCertameDS_22_Tfcontrato_numero_sel, AV114WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data, AV115WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to, lV116WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade, AV117WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel, AV118WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero, AV119WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to, lV120WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site, AV121WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel, AV122WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg, AV123WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to, AV124WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao, AV125WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to, AV126WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao, AV127WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to});
         GRID_nRecordCount = H007G3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV92WWContratoDadosCertameDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV93WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV94WWContratoDadosCertameDS_3_Contratodadoscertame_data1 = AV37ContratoDadosCertame_Data1;
         AV95WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1 = AV38ContratoDadosCertame_Data_To1;
         AV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1 = AV17ContratoDadosCertame_Modalidade1;
         AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1 = AV39ContratoDadosCertame_Numero1;
         AV98WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV99WWContratoDadosCertameDS_8_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV100WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV101WWContratoDadosCertameDS_10_Contratodadoscertame_data2 = AV40ContratoDadosCertame_Data2;
         AV102WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2 = AV41ContratoDadosCertame_Data_To2;
         AV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2 = AV21ContratoDadosCertame_Modalidade2;
         AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2 = AV42ContratoDadosCertame_Numero2;
         AV105WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV106WWContratoDadosCertameDS_15_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV107WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV108WWContratoDadosCertameDS_17_Contratodadoscertame_data3 = AV43ContratoDadosCertame_Data3;
         AV109WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3 = AV44ContratoDadosCertame_Data_To3;
         AV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3 = AV25ContratoDadosCertame_Modalidade3;
         AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3 = AV45ContratoDadosCertame_Numero3;
         AV112WWContratoDadosCertameDS_21_Tfcontrato_numero = AV49TFContrato_Numero;
         AV113WWContratoDadosCertameDS_22_Tfcontrato_numero_sel = AV50TFContrato_Numero_Sel;
         AV114WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data = AV53TFContratoDadosCertame_Data;
         AV115WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to = AV54TFContratoDadosCertame_Data_To;
         AV116WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade = AV59TFContratoDadosCertame_Modalidade;
         AV117WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel = AV60TFContratoDadosCertame_Modalidade_Sel;
         AV118WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero = AV63TFContratoDadosCertame_Numero;
         AV119WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to = AV64TFContratoDadosCertame_Numero_To;
         AV120WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site = AV67TFContratoDadosCertame_Site;
         AV121WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel = AV68TFContratoDadosCertame_Site_Sel;
         AV122WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg = AV71TFContratoDadosCertame_Uasg;
         AV123WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to = AV72TFContratoDadosCertame_Uasg_To;
         AV124WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao = AV75TFContratoDadosCertame_DataHomologacao;
         AV125WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to = AV76TFContratoDadosCertame_DataHomologacao_To;
         AV126WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao = AV81TFContratoDadosCertame_DataAdjudicacao;
         AV127WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to = AV82TFContratoDadosCertame_DataAdjudicacao_To;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV37ContratoDadosCertame_Data1, AV38ContratoDadosCertame_Data_To1, AV17ContratoDadosCertame_Modalidade1, AV39ContratoDadosCertame_Numero1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV40ContratoDadosCertame_Data2, AV41ContratoDadosCertame_Data_To2, AV21ContratoDadosCertame_Modalidade2, AV42ContratoDadosCertame_Numero2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV43ContratoDadosCertame_Data3, AV44ContratoDadosCertame_Data_To3, AV25ContratoDadosCertame_Modalidade3, AV45ContratoDadosCertame_Numero3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV49TFContrato_Numero, AV50TFContrato_Numero_Sel, AV53TFContratoDadosCertame_Data, AV54TFContratoDadosCertame_Data_To, AV59TFContratoDadosCertame_Modalidade, AV60TFContratoDadosCertame_Modalidade_Sel, AV63TFContratoDadosCertame_Numero, AV64TFContratoDadosCertame_Numero_To, AV67TFContratoDadosCertame_Site, AV68TFContratoDadosCertame_Site_Sel, AV71TFContratoDadosCertame_Uasg, AV72TFContratoDadosCertame_Uasg_To, AV75TFContratoDadosCertame_DataHomologacao, AV76TFContratoDadosCertame_DataHomologacao_To, AV81TFContratoDadosCertame_DataAdjudicacao, AV82TFContratoDadosCertame_DataAdjudicacao_To, AV51ddo_Contrato_NumeroTitleControlIdToReplace, AV57ddo_ContratoDadosCertame_DataTitleControlIdToReplace, AV61ddo_ContratoDadosCertame_ModalidadeTitleControlIdToReplace, AV65ddo_ContratoDadosCertame_NumeroTitleControlIdToReplace, AV69ddo_ContratoDadosCertame_SiteTitleControlIdToReplace, AV73ddo_ContratoDadosCertame_UasgTitleControlIdToReplace, AV79ddo_ContratoDadosCertame_DataHomologacaoTitleControlIdToReplace, AV85ddo_ContratoDadosCertame_DataAdjudicacaoTitleControlIdToReplace, AV130Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A314ContratoDadosCertame_Codigo, A74Contrato_Codigo, A309ContratoDadosCertame_Site) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV92WWContratoDadosCertameDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV93WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV94WWContratoDadosCertameDS_3_Contratodadoscertame_data1 = AV37ContratoDadosCertame_Data1;
         AV95WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1 = AV38ContratoDadosCertame_Data_To1;
         AV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1 = AV17ContratoDadosCertame_Modalidade1;
         AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1 = AV39ContratoDadosCertame_Numero1;
         AV98WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV99WWContratoDadosCertameDS_8_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV100WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV101WWContratoDadosCertameDS_10_Contratodadoscertame_data2 = AV40ContratoDadosCertame_Data2;
         AV102WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2 = AV41ContratoDadosCertame_Data_To2;
         AV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2 = AV21ContratoDadosCertame_Modalidade2;
         AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2 = AV42ContratoDadosCertame_Numero2;
         AV105WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV106WWContratoDadosCertameDS_15_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV107WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV108WWContratoDadosCertameDS_17_Contratodadoscertame_data3 = AV43ContratoDadosCertame_Data3;
         AV109WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3 = AV44ContratoDadosCertame_Data_To3;
         AV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3 = AV25ContratoDadosCertame_Modalidade3;
         AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3 = AV45ContratoDadosCertame_Numero3;
         AV112WWContratoDadosCertameDS_21_Tfcontrato_numero = AV49TFContrato_Numero;
         AV113WWContratoDadosCertameDS_22_Tfcontrato_numero_sel = AV50TFContrato_Numero_Sel;
         AV114WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data = AV53TFContratoDadosCertame_Data;
         AV115WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to = AV54TFContratoDadosCertame_Data_To;
         AV116WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade = AV59TFContratoDadosCertame_Modalidade;
         AV117WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel = AV60TFContratoDadosCertame_Modalidade_Sel;
         AV118WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero = AV63TFContratoDadosCertame_Numero;
         AV119WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to = AV64TFContratoDadosCertame_Numero_To;
         AV120WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site = AV67TFContratoDadosCertame_Site;
         AV121WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel = AV68TFContratoDadosCertame_Site_Sel;
         AV122WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg = AV71TFContratoDadosCertame_Uasg;
         AV123WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to = AV72TFContratoDadosCertame_Uasg_To;
         AV124WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao = AV75TFContratoDadosCertame_DataHomologacao;
         AV125WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to = AV76TFContratoDadosCertame_DataHomologacao_To;
         AV126WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao = AV81TFContratoDadosCertame_DataAdjudicacao;
         AV127WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to = AV82TFContratoDadosCertame_DataAdjudicacao_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV37ContratoDadosCertame_Data1, AV38ContratoDadosCertame_Data_To1, AV17ContratoDadosCertame_Modalidade1, AV39ContratoDadosCertame_Numero1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV40ContratoDadosCertame_Data2, AV41ContratoDadosCertame_Data_To2, AV21ContratoDadosCertame_Modalidade2, AV42ContratoDadosCertame_Numero2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV43ContratoDadosCertame_Data3, AV44ContratoDadosCertame_Data_To3, AV25ContratoDadosCertame_Modalidade3, AV45ContratoDadosCertame_Numero3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV49TFContrato_Numero, AV50TFContrato_Numero_Sel, AV53TFContratoDadosCertame_Data, AV54TFContratoDadosCertame_Data_To, AV59TFContratoDadosCertame_Modalidade, AV60TFContratoDadosCertame_Modalidade_Sel, AV63TFContratoDadosCertame_Numero, AV64TFContratoDadosCertame_Numero_To, AV67TFContratoDadosCertame_Site, AV68TFContratoDadosCertame_Site_Sel, AV71TFContratoDadosCertame_Uasg, AV72TFContratoDadosCertame_Uasg_To, AV75TFContratoDadosCertame_DataHomologacao, AV76TFContratoDadosCertame_DataHomologacao_To, AV81TFContratoDadosCertame_DataAdjudicacao, AV82TFContratoDadosCertame_DataAdjudicacao_To, AV51ddo_Contrato_NumeroTitleControlIdToReplace, AV57ddo_ContratoDadosCertame_DataTitleControlIdToReplace, AV61ddo_ContratoDadosCertame_ModalidadeTitleControlIdToReplace, AV65ddo_ContratoDadosCertame_NumeroTitleControlIdToReplace, AV69ddo_ContratoDadosCertame_SiteTitleControlIdToReplace, AV73ddo_ContratoDadosCertame_UasgTitleControlIdToReplace, AV79ddo_ContratoDadosCertame_DataHomologacaoTitleControlIdToReplace, AV85ddo_ContratoDadosCertame_DataAdjudicacaoTitleControlIdToReplace, AV130Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A314ContratoDadosCertame_Codigo, A74Contrato_Codigo, A309ContratoDadosCertame_Site) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV92WWContratoDadosCertameDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV93WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV94WWContratoDadosCertameDS_3_Contratodadoscertame_data1 = AV37ContratoDadosCertame_Data1;
         AV95WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1 = AV38ContratoDadosCertame_Data_To1;
         AV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1 = AV17ContratoDadosCertame_Modalidade1;
         AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1 = AV39ContratoDadosCertame_Numero1;
         AV98WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV99WWContratoDadosCertameDS_8_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV100WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV101WWContratoDadosCertameDS_10_Contratodadoscertame_data2 = AV40ContratoDadosCertame_Data2;
         AV102WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2 = AV41ContratoDadosCertame_Data_To2;
         AV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2 = AV21ContratoDadosCertame_Modalidade2;
         AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2 = AV42ContratoDadosCertame_Numero2;
         AV105WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV106WWContratoDadosCertameDS_15_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV107WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV108WWContratoDadosCertameDS_17_Contratodadoscertame_data3 = AV43ContratoDadosCertame_Data3;
         AV109WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3 = AV44ContratoDadosCertame_Data_To3;
         AV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3 = AV25ContratoDadosCertame_Modalidade3;
         AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3 = AV45ContratoDadosCertame_Numero3;
         AV112WWContratoDadosCertameDS_21_Tfcontrato_numero = AV49TFContrato_Numero;
         AV113WWContratoDadosCertameDS_22_Tfcontrato_numero_sel = AV50TFContrato_Numero_Sel;
         AV114WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data = AV53TFContratoDadosCertame_Data;
         AV115WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to = AV54TFContratoDadosCertame_Data_To;
         AV116WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade = AV59TFContratoDadosCertame_Modalidade;
         AV117WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel = AV60TFContratoDadosCertame_Modalidade_Sel;
         AV118WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero = AV63TFContratoDadosCertame_Numero;
         AV119WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to = AV64TFContratoDadosCertame_Numero_To;
         AV120WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site = AV67TFContratoDadosCertame_Site;
         AV121WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel = AV68TFContratoDadosCertame_Site_Sel;
         AV122WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg = AV71TFContratoDadosCertame_Uasg;
         AV123WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to = AV72TFContratoDadosCertame_Uasg_To;
         AV124WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao = AV75TFContratoDadosCertame_DataHomologacao;
         AV125WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to = AV76TFContratoDadosCertame_DataHomologacao_To;
         AV126WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao = AV81TFContratoDadosCertame_DataAdjudicacao;
         AV127WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to = AV82TFContratoDadosCertame_DataAdjudicacao_To;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV37ContratoDadosCertame_Data1, AV38ContratoDadosCertame_Data_To1, AV17ContratoDadosCertame_Modalidade1, AV39ContratoDadosCertame_Numero1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV40ContratoDadosCertame_Data2, AV41ContratoDadosCertame_Data_To2, AV21ContratoDadosCertame_Modalidade2, AV42ContratoDadosCertame_Numero2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV43ContratoDadosCertame_Data3, AV44ContratoDadosCertame_Data_To3, AV25ContratoDadosCertame_Modalidade3, AV45ContratoDadosCertame_Numero3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV49TFContrato_Numero, AV50TFContrato_Numero_Sel, AV53TFContratoDadosCertame_Data, AV54TFContratoDadosCertame_Data_To, AV59TFContratoDadosCertame_Modalidade, AV60TFContratoDadosCertame_Modalidade_Sel, AV63TFContratoDadosCertame_Numero, AV64TFContratoDadosCertame_Numero_To, AV67TFContratoDadosCertame_Site, AV68TFContratoDadosCertame_Site_Sel, AV71TFContratoDadosCertame_Uasg, AV72TFContratoDadosCertame_Uasg_To, AV75TFContratoDadosCertame_DataHomologacao, AV76TFContratoDadosCertame_DataHomologacao_To, AV81TFContratoDadosCertame_DataAdjudicacao, AV82TFContratoDadosCertame_DataAdjudicacao_To, AV51ddo_Contrato_NumeroTitleControlIdToReplace, AV57ddo_ContratoDadosCertame_DataTitleControlIdToReplace, AV61ddo_ContratoDadosCertame_ModalidadeTitleControlIdToReplace, AV65ddo_ContratoDadosCertame_NumeroTitleControlIdToReplace, AV69ddo_ContratoDadosCertame_SiteTitleControlIdToReplace, AV73ddo_ContratoDadosCertame_UasgTitleControlIdToReplace, AV79ddo_ContratoDadosCertame_DataHomologacaoTitleControlIdToReplace, AV85ddo_ContratoDadosCertame_DataAdjudicacaoTitleControlIdToReplace, AV130Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A314ContratoDadosCertame_Codigo, A74Contrato_Codigo, A309ContratoDadosCertame_Site) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV92WWContratoDadosCertameDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV93WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV94WWContratoDadosCertameDS_3_Contratodadoscertame_data1 = AV37ContratoDadosCertame_Data1;
         AV95WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1 = AV38ContratoDadosCertame_Data_To1;
         AV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1 = AV17ContratoDadosCertame_Modalidade1;
         AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1 = AV39ContratoDadosCertame_Numero1;
         AV98WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV99WWContratoDadosCertameDS_8_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV100WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV101WWContratoDadosCertameDS_10_Contratodadoscertame_data2 = AV40ContratoDadosCertame_Data2;
         AV102WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2 = AV41ContratoDadosCertame_Data_To2;
         AV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2 = AV21ContratoDadosCertame_Modalidade2;
         AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2 = AV42ContratoDadosCertame_Numero2;
         AV105WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV106WWContratoDadosCertameDS_15_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV107WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV108WWContratoDadosCertameDS_17_Contratodadoscertame_data3 = AV43ContratoDadosCertame_Data3;
         AV109WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3 = AV44ContratoDadosCertame_Data_To3;
         AV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3 = AV25ContratoDadosCertame_Modalidade3;
         AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3 = AV45ContratoDadosCertame_Numero3;
         AV112WWContratoDadosCertameDS_21_Tfcontrato_numero = AV49TFContrato_Numero;
         AV113WWContratoDadosCertameDS_22_Tfcontrato_numero_sel = AV50TFContrato_Numero_Sel;
         AV114WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data = AV53TFContratoDadosCertame_Data;
         AV115WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to = AV54TFContratoDadosCertame_Data_To;
         AV116WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade = AV59TFContratoDadosCertame_Modalidade;
         AV117WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel = AV60TFContratoDadosCertame_Modalidade_Sel;
         AV118WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero = AV63TFContratoDadosCertame_Numero;
         AV119WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to = AV64TFContratoDadosCertame_Numero_To;
         AV120WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site = AV67TFContratoDadosCertame_Site;
         AV121WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel = AV68TFContratoDadosCertame_Site_Sel;
         AV122WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg = AV71TFContratoDadosCertame_Uasg;
         AV123WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to = AV72TFContratoDadosCertame_Uasg_To;
         AV124WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao = AV75TFContratoDadosCertame_DataHomologacao;
         AV125WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to = AV76TFContratoDadosCertame_DataHomologacao_To;
         AV126WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao = AV81TFContratoDadosCertame_DataAdjudicacao;
         AV127WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to = AV82TFContratoDadosCertame_DataAdjudicacao_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV37ContratoDadosCertame_Data1, AV38ContratoDadosCertame_Data_To1, AV17ContratoDadosCertame_Modalidade1, AV39ContratoDadosCertame_Numero1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV40ContratoDadosCertame_Data2, AV41ContratoDadosCertame_Data_To2, AV21ContratoDadosCertame_Modalidade2, AV42ContratoDadosCertame_Numero2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV43ContratoDadosCertame_Data3, AV44ContratoDadosCertame_Data_To3, AV25ContratoDadosCertame_Modalidade3, AV45ContratoDadosCertame_Numero3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV49TFContrato_Numero, AV50TFContrato_Numero_Sel, AV53TFContratoDadosCertame_Data, AV54TFContratoDadosCertame_Data_To, AV59TFContratoDadosCertame_Modalidade, AV60TFContratoDadosCertame_Modalidade_Sel, AV63TFContratoDadosCertame_Numero, AV64TFContratoDadosCertame_Numero_To, AV67TFContratoDadosCertame_Site, AV68TFContratoDadosCertame_Site_Sel, AV71TFContratoDadosCertame_Uasg, AV72TFContratoDadosCertame_Uasg_To, AV75TFContratoDadosCertame_DataHomologacao, AV76TFContratoDadosCertame_DataHomologacao_To, AV81TFContratoDadosCertame_DataAdjudicacao, AV82TFContratoDadosCertame_DataAdjudicacao_To, AV51ddo_Contrato_NumeroTitleControlIdToReplace, AV57ddo_ContratoDadosCertame_DataTitleControlIdToReplace, AV61ddo_ContratoDadosCertame_ModalidadeTitleControlIdToReplace, AV65ddo_ContratoDadosCertame_NumeroTitleControlIdToReplace, AV69ddo_ContratoDadosCertame_SiteTitleControlIdToReplace, AV73ddo_ContratoDadosCertame_UasgTitleControlIdToReplace, AV79ddo_ContratoDadosCertame_DataHomologacaoTitleControlIdToReplace, AV85ddo_ContratoDadosCertame_DataAdjudicacaoTitleControlIdToReplace, AV130Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A314ContratoDadosCertame_Codigo, A74Contrato_Codigo, A309ContratoDadosCertame_Site) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV92WWContratoDadosCertameDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV93WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV94WWContratoDadosCertameDS_3_Contratodadoscertame_data1 = AV37ContratoDadosCertame_Data1;
         AV95WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1 = AV38ContratoDadosCertame_Data_To1;
         AV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1 = AV17ContratoDadosCertame_Modalidade1;
         AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1 = AV39ContratoDadosCertame_Numero1;
         AV98WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV99WWContratoDadosCertameDS_8_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV100WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV101WWContratoDadosCertameDS_10_Contratodadoscertame_data2 = AV40ContratoDadosCertame_Data2;
         AV102WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2 = AV41ContratoDadosCertame_Data_To2;
         AV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2 = AV21ContratoDadosCertame_Modalidade2;
         AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2 = AV42ContratoDadosCertame_Numero2;
         AV105WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV106WWContratoDadosCertameDS_15_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV107WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV108WWContratoDadosCertameDS_17_Contratodadoscertame_data3 = AV43ContratoDadosCertame_Data3;
         AV109WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3 = AV44ContratoDadosCertame_Data_To3;
         AV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3 = AV25ContratoDadosCertame_Modalidade3;
         AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3 = AV45ContratoDadosCertame_Numero3;
         AV112WWContratoDadosCertameDS_21_Tfcontrato_numero = AV49TFContrato_Numero;
         AV113WWContratoDadosCertameDS_22_Tfcontrato_numero_sel = AV50TFContrato_Numero_Sel;
         AV114WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data = AV53TFContratoDadosCertame_Data;
         AV115WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to = AV54TFContratoDadosCertame_Data_To;
         AV116WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade = AV59TFContratoDadosCertame_Modalidade;
         AV117WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel = AV60TFContratoDadosCertame_Modalidade_Sel;
         AV118WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero = AV63TFContratoDadosCertame_Numero;
         AV119WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to = AV64TFContratoDadosCertame_Numero_To;
         AV120WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site = AV67TFContratoDadosCertame_Site;
         AV121WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel = AV68TFContratoDadosCertame_Site_Sel;
         AV122WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg = AV71TFContratoDadosCertame_Uasg;
         AV123WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to = AV72TFContratoDadosCertame_Uasg_To;
         AV124WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao = AV75TFContratoDadosCertame_DataHomologacao;
         AV125WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to = AV76TFContratoDadosCertame_DataHomologacao_To;
         AV126WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao = AV81TFContratoDadosCertame_DataAdjudicacao;
         AV127WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to = AV82TFContratoDadosCertame_DataAdjudicacao_To;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV37ContratoDadosCertame_Data1, AV38ContratoDadosCertame_Data_To1, AV17ContratoDadosCertame_Modalidade1, AV39ContratoDadosCertame_Numero1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV40ContratoDadosCertame_Data2, AV41ContratoDadosCertame_Data_To2, AV21ContratoDadosCertame_Modalidade2, AV42ContratoDadosCertame_Numero2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV43ContratoDadosCertame_Data3, AV44ContratoDadosCertame_Data_To3, AV25ContratoDadosCertame_Modalidade3, AV45ContratoDadosCertame_Numero3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV49TFContrato_Numero, AV50TFContrato_Numero_Sel, AV53TFContratoDadosCertame_Data, AV54TFContratoDadosCertame_Data_To, AV59TFContratoDadosCertame_Modalidade, AV60TFContratoDadosCertame_Modalidade_Sel, AV63TFContratoDadosCertame_Numero, AV64TFContratoDadosCertame_Numero_To, AV67TFContratoDadosCertame_Site, AV68TFContratoDadosCertame_Site_Sel, AV71TFContratoDadosCertame_Uasg, AV72TFContratoDadosCertame_Uasg_To, AV75TFContratoDadosCertame_DataHomologacao, AV76TFContratoDadosCertame_DataHomologacao_To, AV81TFContratoDadosCertame_DataAdjudicacao, AV82TFContratoDadosCertame_DataAdjudicacao_To, AV51ddo_Contrato_NumeroTitleControlIdToReplace, AV57ddo_ContratoDadosCertame_DataTitleControlIdToReplace, AV61ddo_ContratoDadosCertame_ModalidadeTitleControlIdToReplace, AV65ddo_ContratoDadosCertame_NumeroTitleControlIdToReplace, AV69ddo_ContratoDadosCertame_SiteTitleControlIdToReplace, AV73ddo_ContratoDadosCertame_UasgTitleControlIdToReplace, AV79ddo_ContratoDadosCertame_DataHomologacaoTitleControlIdToReplace, AV85ddo_ContratoDadosCertame_DataAdjudicacaoTitleControlIdToReplace, AV130Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A314ContratoDadosCertame_Codigo, A74Contrato_Codigo, A309ContratoDadosCertame_Site) ;
         }
         return (int)(0) ;
      }

      protected void STRUP7G0( )
      {
         /* Before Start, stand alone formulas. */
         AV130Pgmname = "WWContratoDadosCertame";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E317G2 */
         E317G2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV86DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATO_NUMEROTITLEFILTERDATA"), AV48Contrato_NumeroTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATODADOSCERTAME_DATATITLEFILTERDATA"), AV52ContratoDadosCertame_DataTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATODADOSCERTAME_MODALIDADETITLEFILTERDATA"), AV58ContratoDadosCertame_ModalidadeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATODADOSCERTAME_NUMEROTITLEFILTERDATA"), AV62ContratoDadosCertame_NumeroTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATODADOSCERTAME_SITETITLEFILTERDATA"), AV66ContratoDadosCertame_SiteTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATODADOSCERTAME_UASGTITLEFILTERDATA"), AV70ContratoDadosCertame_UasgTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATODADOSCERTAME_DATAHOMOLOGACAOTITLEFILTERDATA"), AV74ContratoDadosCertame_DataHomologacaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATODADOSCERTAME_DATAADJUDICACAOTITLEFILTERDATA"), AV80ContratoDadosCertame_DataAdjudicacaoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratodadoscertame_data1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Dados Certame_Data1"}), 1, "vCONTRATODADOSCERTAME_DATA1");
               GX_FocusControl = edtavContratodadoscertame_data1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV37ContratoDadosCertame_Data1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ContratoDadosCertame_Data1", context.localUtil.Format(AV37ContratoDadosCertame_Data1, "99/99/99"));
            }
            else
            {
               AV37ContratoDadosCertame_Data1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratodadoscertame_data1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ContratoDadosCertame_Data1", context.localUtil.Format(AV37ContratoDadosCertame_Data1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratodadoscertame_data_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Dados Certame_Data_To1"}), 1, "vCONTRATODADOSCERTAME_DATA_TO1");
               GX_FocusControl = edtavContratodadoscertame_data_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV38ContratoDadosCertame_Data_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ContratoDadosCertame_Data_To1", context.localUtil.Format(AV38ContratoDadosCertame_Data_To1, "99/99/99"));
            }
            else
            {
               AV38ContratoDadosCertame_Data_To1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratodadoscertame_data_to1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ContratoDadosCertame_Data_To1", context.localUtil.Format(AV38ContratoDadosCertame_Data_To1, "99/99/99"));
            }
            AV17ContratoDadosCertame_Modalidade1 = cgiGet( edtavContratodadoscertame_modalidade1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoDadosCertame_Modalidade1", AV17ContratoDadosCertame_Modalidade1);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContratodadoscertame_numero1_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratodadoscertame_numero1_Internalname), ",", ".") > Convert.ToDecimal( 9999999999L )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATODADOSCERTAME_NUMERO1");
               GX_FocusControl = edtavContratodadoscertame_numero1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV39ContratoDadosCertame_Numero1 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ContratoDadosCertame_Numero1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39ContratoDadosCertame_Numero1), 10, 0)));
            }
            else
            {
               AV39ContratoDadosCertame_Numero1 = (long)(context.localUtil.CToN( cgiGet( edtavContratodadoscertame_numero1_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ContratoDadosCertame_Numero1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39ContratoDadosCertame_Numero1), 10, 0)));
            }
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratodadoscertame_data2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Dados Certame_Data2"}), 1, "vCONTRATODADOSCERTAME_DATA2");
               GX_FocusControl = edtavContratodadoscertame_data2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV40ContratoDadosCertame_Data2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ContratoDadosCertame_Data2", context.localUtil.Format(AV40ContratoDadosCertame_Data2, "99/99/99"));
            }
            else
            {
               AV40ContratoDadosCertame_Data2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratodadoscertame_data2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ContratoDadosCertame_Data2", context.localUtil.Format(AV40ContratoDadosCertame_Data2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratodadoscertame_data_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Dados Certame_Data_To2"}), 1, "vCONTRATODADOSCERTAME_DATA_TO2");
               GX_FocusControl = edtavContratodadoscertame_data_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV41ContratoDadosCertame_Data_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ContratoDadosCertame_Data_To2", context.localUtil.Format(AV41ContratoDadosCertame_Data_To2, "99/99/99"));
            }
            else
            {
               AV41ContratoDadosCertame_Data_To2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratodadoscertame_data_to2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ContratoDadosCertame_Data_To2", context.localUtil.Format(AV41ContratoDadosCertame_Data_To2, "99/99/99"));
            }
            AV21ContratoDadosCertame_Modalidade2 = cgiGet( edtavContratodadoscertame_modalidade2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoDadosCertame_Modalidade2", AV21ContratoDadosCertame_Modalidade2);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContratodadoscertame_numero2_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratodadoscertame_numero2_Internalname), ",", ".") > Convert.ToDecimal( 9999999999L )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATODADOSCERTAME_NUMERO2");
               GX_FocusControl = edtavContratodadoscertame_numero2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV42ContratoDadosCertame_Numero2 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ContratoDadosCertame_Numero2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42ContratoDadosCertame_Numero2), 10, 0)));
            }
            else
            {
               AV42ContratoDadosCertame_Numero2 = (long)(context.localUtil.CToN( cgiGet( edtavContratodadoscertame_numero2_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ContratoDadosCertame_Numero2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42ContratoDadosCertame_Numero2), 10, 0)));
            }
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratodadoscertame_data3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Dados Certame_Data3"}), 1, "vCONTRATODADOSCERTAME_DATA3");
               GX_FocusControl = edtavContratodadoscertame_data3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV43ContratoDadosCertame_Data3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ContratoDadosCertame_Data3", context.localUtil.Format(AV43ContratoDadosCertame_Data3, "99/99/99"));
            }
            else
            {
               AV43ContratoDadosCertame_Data3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratodadoscertame_data3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ContratoDadosCertame_Data3", context.localUtil.Format(AV43ContratoDadosCertame_Data3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratodadoscertame_data_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Dados Certame_Data_To3"}), 1, "vCONTRATODADOSCERTAME_DATA_TO3");
               GX_FocusControl = edtavContratodadoscertame_data_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV44ContratoDadosCertame_Data_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ContratoDadosCertame_Data_To3", context.localUtil.Format(AV44ContratoDadosCertame_Data_To3, "99/99/99"));
            }
            else
            {
               AV44ContratoDadosCertame_Data_To3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratodadoscertame_data_to3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ContratoDadosCertame_Data_To3", context.localUtil.Format(AV44ContratoDadosCertame_Data_To3, "99/99/99"));
            }
            AV25ContratoDadosCertame_Modalidade3 = cgiGet( edtavContratodadoscertame_modalidade3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoDadosCertame_Modalidade3", AV25ContratoDadosCertame_Modalidade3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContratodadoscertame_numero3_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratodadoscertame_numero3_Internalname), ",", ".") > Convert.ToDecimal( 9999999999L )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATODADOSCERTAME_NUMERO3");
               GX_FocusControl = edtavContratodadoscertame_numero3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV45ContratoDadosCertame_Numero3 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ContratoDadosCertame_Numero3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45ContratoDadosCertame_Numero3), 10, 0)));
            }
            else
            {
               AV45ContratoDadosCertame_Numero3 = (long)(context.localUtil.CToN( cgiGet( edtavContratodadoscertame_numero3_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ContratoDadosCertame_Numero3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45ContratoDadosCertame_Numero3), 10, 0)));
            }
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            AV49TFContrato_Numero = cgiGet( edtavTfcontrato_numero_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContrato_Numero", AV49TFContrato_Numero);
            AV50TFContrato_Numero_Sel = cgiGet( edtavTfcontrato_numero_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContrato_Numero_Sel", AV50TFContrato_Numero_Sel);
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratodadoscertame_data_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Dados Certame_Data"}), 1, "vTFCONTRATODADOSCERTAME_DATA");
               GX_FocusControl = edtavTfcontratodadoscertame_data_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV53TFContratoDadosCertame_Data = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContratoDadosCertame_Data", context.localUtil.Format(AV53TFContratoDadosCertame_Data, "99/99/99"));
            }
            else
            {
               AV53TFContratoDadosCertame_Data = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratodadoscertame_data_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContratoDadosCertame_Data", context.localUtil.Format(AV53TFContratoDadosCertame_Data, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratodadoscertame_data_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Dados Certame_Data_To"}), 1, "vTFCONTRATODADOSCERTAME_DATA_TO");
               GX_FocusControl = edtavTfcontratodadoscertame_data_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV54TFContratoDadosCertame_Data_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContratoDadosCertame_Data_To", context.localUtil.Format(AV54TFContratoDadosCertame_Data_To, "99/99/99"));
            }
            else
            {
               AV54TFContratoDadosCertame_Data_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratodadoscertame_data_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContratoDadosCertame_Data_To", context.localUtil.Format(AV54TFContratoDadosCertame_Data_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratodadoscertame_dataauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Dados Certame_Data Aux Date"}), 1, "vDDO_CONTRATODADOSCERTAME_DATAAUXDATE");
               GX_FocusControl = edtavDdo_contratodadoscertame_dataauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV55DDO_ContratoDadosCertame_DataAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55DDO_ContratoDadosCertame_DataAuxDate", context.localUtil.Format(AV55DDO_ContratoDadosCertame_DataAuxDate, "99/99/99"));
            }
            else
            {
               AV55DDO_ContratoDadosCertame_DataAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratodadoscertame_dataauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55DDO_ContratoDadosCertame_DataAuxDate", context.localUtil.Format(AV55DDO_ContratoDadosCertame_DataAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratodadoscertame_dataauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Dados Certame_Data Aux Date To"}), 1, "vDDO_CONTRATODADOSCERTAME_DATAAUXDATETO");
               GX_FocusControl = edtavDdo_contratodadoscertame_dataauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV56DDO_ContratoDadosCertame_DataAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56DDO_ContratoDadosCertame_DataAuxDateTo", context.localUtil.Format(AV56DDO_ContratoDadosCertame_DataAuxDateTo, "99/99/99"));
            }
            else
            {
               AV56DDO_ContratoDadosCertame_DataAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratodadoscertame_dataauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56DDO_ContratoDadosCertame_DataAuxDateTo", context.localUtil.Format(AV56DDO_ContratoDadosCertame_DataAuxDateTo, "99/99/99"));
            }
            AV59TFContratoDadosCertame_Modalidade = cgiGet( edtavTfcontratodadoscertame_modalidade_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContratoDadosCertame_Modalidade", AV59TFContratoDadosCertame_Modalidade);
            AV60TFContratoDadosCertame_Modalidade_Sel = cgiGet( edtavTfcontratodadoscertame_modalidade_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratoDadosCertame_Modalidade_Sel", AV60TFContratoDadosCertame_Modalidade_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratodadoscertame_numero_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratodadoscertame_numero_Internalname), ",", ".") > Convert.ToDecimal( 9999999999L )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATODADOSCERTAME_NUMERO");
               GX_FocusControl = edtavTfcontratodadoscertame_numero_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV63TFContratoDadosCertame_Numero = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFContratoDadosCertame_Numero", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63TFContratoDadosCertame_Numero), 10, 0)));
            }
            else
            {
               AV63TFContratoDadosCertame_Numero = (long)(context.localUtil.CToN( cgiGet( edtavTfcontratodadoscertame_numero_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFContratoDadosCertame_Numero", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63TFContratoDadosCertame_Numero), 10, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratodadoscertame_numero_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratodadoscertame_numero_to_Internalname), ",", ".") > Convert.ToDecimal( 9999999999L )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATODADOSCERTAME_NUMERO_TO");
               GX_FocusControl = edtavTfcontratodadoscertame_numero_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV64TFContratoDadosCertame_Numero_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFContratoDadosCertame_Numero_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV64TFContratoDadosCertame_Numero_To), 10, 0)));
            }
            else
            {
               AV64TFContratoDadosCertame_Numero_To = (long)(context.localUtil.CToN( cgiGet( edtavTfcontratodadoscertame_numero_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFContratoDadosCertame_Numero_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV64TFContratoDadosCertame_Numero_To), 10, 0)));
            }
            AV67TFContratoDadosCertame_Site = cgiGet( edtavTfcontratodadoscertame_site_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFContratoDadosCertame_Site", AV67TFContratoDadosCertame_Site);
            AV68TFContratoDadosCertame_Site_Sel = cgiGet( edtavTfcontratodadoscertame_site_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFContratoDadosCertame_Site_Sel", AV68TFContratoDadosCertame_Site_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratodadoscertame_uasg_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratodadoscertame_uasg_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATODADOSCERTAME_UASG");
               GX_FocusControl = edtavTfcontratodadoscertame_uasg_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV71TFContratoDadosCertame_Uasg = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContratoDadosCertame_Uasg", StringUtil.LTrim( StringUtil.Str( (decimal)(AV71TFContratoDadosCertame_Uasg), 4, 0)));
            }
            else
            {
               AV71TFContratoDadosCertame_Uasg = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratodadoscertame_uasg_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContratoDadosCertame_Uasg", StringUtil.LTrim( StringUtil.Str( (decimal)(AV71TFContratoDadosCertame_Uasg), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratodadoscertame_uasg_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratodadoscertame_uasg_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATODADOSCERTAME_UASG_TO");
               GX_FocusControl = edtavTfcontratodadoscertame_uasg_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV72TFContratoDadosCertame_Uasg_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFContratoDadosCertame_Uasg_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV72TFContratoDadosCertame_Uasg_To), 4, 0)));
            }
            else
            {
               AV72TFContratoDadosCertame_Uasg_To = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratodadoscertame_uasg_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFContratoDadosCertame_Uasg_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV72TFContratoDadosCertame_Uasg_To), 4, 0)));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratodadoscertame_datahomologacao_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Dados Certame_Data Homologacao"}), 1, "vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO");
               GX_FocusControl = edtavTfcontratodadoscertame_datahomologacao_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV75TFContratoDadosCertame_DataHomologacao = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFContratoDadosCertame_DataHomologacao", context.localUtil.Format(AV75TFContratoDadosCertame_DataHomologacao, "99/99/99"));
            }
            else
            {
               AV75TFContratoDadosCertame_DataHomologacao = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratodadoscertame_datahomologacao_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFContratoDadosCertame_DataHomologacao", context.localUtil.Format(AV75TFContratoDadosCertame_DataHomologacao, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratodadoscertame_datahomologacao_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Dados Certame_Data Homologacao_To"}), 1, "vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO_TO");
               GX_FocusControl = edtavTfcontratodadoscertame_datahomologacao_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV76TFContratoDadosCertame_DataHomologacao_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFContratoDadosCertame_DataHomologacao_To", context.localUtil.Format(AV76TFContratoDadosCertame_DataHomologacao_To, "99/99/99"));
            }
            else
            {
               AV76TFContratoDadosCertame_DataHomologacao_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratodadoscertame_datahomologacao_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFContratoDadosCertame_DataHomologacao_To", context.localUtil.Format(AV76TFContratoDadosCertame_DataHomologacao_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratodadoscertame_datahomologacaoauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Dados Certame_Data Homologacao Aux Date"}), 1, "vDDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAOAUXDATE");
               GX_FocusControl = edtavDdo_contratodadoscertame_datahomologacaoauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV77DDO_ContratoDadosCertame_DataHomologacaoAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77DDO_ContratoDadosCertame_DataHomologacaoAuxDate", context.localUtil.Format(AV77DDO_ContratoDadosCertame_DataHomologacaoAuxDate, "99/99/99"));
            }
            else
            {
               AV77DDO_ContratoDadosCertame_DataHomologacaoAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratodadoscertame_datahomologacaoauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77DDO_ContratoDadosCertame_DataHomologacaoAuxDate", context.localUtil.Format(AV77DDO_ContratoDadosCertame_DataHomologacaoAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratodadoscertame_datahomologacaoauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Dados Certame_Data Homologacao Aux Date To"}), 1, "vDDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAOAUXDATETO");
               GX_FocusControl = edtavDdo_contratodadoscertame_datahomologacaoauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV78DDO_ContratoDadosCertame_DataHomologacaoAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78DDO_ContratoDadosCertame_DataHomologacaoAuxDateTo", context.localUtil.Format(AV78DDO_ContratoDadosCertame_DataHomologacaoAuxDateTo, "99/99/99"));
            }
            else
            {
               AV78DDO_ContratoDadosCertame_DataHomologacaoAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratodadoscertame_datahomologacaoauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78DDO_ContratoDadosCertame_DataHomologacaoAuxDateTo", context.localUtil.Format(AV78DDO_ContratoDadosCertame_DataHomologacaoAuxDateTo, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratodadoscertame_dataadjudicacao_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Dados Certame_Data Adjudicacao"}), 1, "vTFCONTRATODADOSCERTAME_DATAADJUDICACAO");
               GX_FocusControl = edtavTfcontratodadoscertame_dataadjudicacao_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV81TFContratoDadosCertame_DataAdjudicacao = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFContratoDadosCertame_DataAdjudicacao", context.localUtil.Format(AV81TFContratoDadosCertame_DataAdjudicacao, "99/99/99"));
            }
            else
            {
               AV81TFContratoDadosCertame_DataAdjudicacao = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratodadoscertame_dataadjudicacao_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFContratoDadosCertame_DataAdjudicacao", context.localUtil.Format(AV81TFContratoDadosCertame_DataAdjudicacao, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratodadoscertame_dataadjudicacao_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Dados Certame_Data Adjudicacao_To"}), 1, "vTFCONTRATODADOSCERTAME_DATAADJUDICACAO_TO");
               GX_FocusControl = edtavTfcontratodadoscertame_dataadjudicacao_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV82TFContratoDadosCertame_DataAdjudicacao_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFContratoDadosCertame_DataAdjudicacao_To", context.localUtil.Format(AV82TFContratoDadosCertame_DataAdjudicacao_To, "99/99/99"));
            }
            else
            {
               AV82TFContratoDadosCertame_DataAdjudicacao_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratodadoscertame_dataadjudicacao_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFContratoDadosCertame_DataAdjudicacao_To", context.localUtil.Format(AV82TFContratoDadosCertame_DataAdjudicacao_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratodadoscertame_dataadjudicacaoauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Dados Certame_Data Adjudicacao Aux Date"}), 1, "vDDO_CONTRATODADOSCERTAME_DATAADJUDICACAOAUXDATE");
               GX_FocusControl = edtavDdo_contratodadoscertame_dataadjudicacaoauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV83DDO_ContratoDadosCertame_DataAdjudicacaoAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83DDO_ContratoDadosCertame_DataAdjudicacaoAuxDate", context.localUtil.Format(AV83DDO_ContratoDadosCertame_DataAdjudicacaoAuxDate, "99/99/99"));
            }
            else
            {
               AV83DDO_ContratoDadosCertame_DataAdjudicacaoAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratodadoscertame_dataadjudicacaoauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83DDO_ContratoDadosCertame_DataAdjudicacaoAuxDate", context.localUtil.Format(AV83DDO_ContratoDadosCertame_DataAdjudicacaoAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratodadoscertame_dataadjudicacaoauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Dados Certame_Data Adjudicacao Aux Date To"}), 1, "vDDO_CONTRATODADOSCERTAME_DATAADJUDICACAOAUXDATETO");
               GX_FocusControl = edtavDdo_contratodadoscertame_dataadjudicacaoauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV84DDO_ContratoDadosCertame_DataAdjudicacaoAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84DDO_ContratoDadosCertame_DataAdjudicacaoAuxDateTo", context.localUtil.Format(AV84DDO_ContratoDadosCertame_DataAdjudicacaoAuxDateTo, "99/99/99"));
            }
            else
            {
               AV84DDO_ContratoDadosCertame_DataAdjudicacaoAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratodadoscertame_dataadjudicacaoauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84DDO_ContratoDadosCertame_DataAdjudicacaoAuxDateTo", context.localUtil.Format(AV84DDO_ContratoDadosCertame_DataAdjudicacaoAuxDateTo, "99/99/99"));
            }
            AV51ddo_Contrato_NumeroTitleControlIdToReplace = cgiGet( edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ddo_Contrato_NumeroTitleControlIdToReplace", AV51ddo_Contrato_NumeroTitleControlIdToReplace);
            AV57ddo_ContratoDadosCertame_DataTitleControlIdToReplace = cgiGet( edtavDdo_contratodadoscertame_datatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_ContratoDadosCertame_DataTitleControlIdToReplace", AV57ddo_ContratoDadosCertame_DataTitleControlIdToReplace);
            AV61ddo_ContratoDadosCertame_ModalidadeTitleControlIdToReplace = cgiGet( edtavDdo_contratodadoscertame_modalidadetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_ContratoDadosCertame_ModalidadeTitleControlIdToReplace", AV61ddo_ContratoDadosCertame_ModalidadeTitleControlIdToReplace);
            AV65ddo_ContratoDadosCertame_NumeroTitleControlIdToReplace = cgiGet( edtavDdo_contratodadoscertame_numerotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ddo_ContratoDadosCertame_NumeroTitleControlIdToReplace", AV65ddo_ContratoDadosCertame_NumeroTitleControlIdToReplace);
            AV69ddo_ContratoDadosCertame_SiteTitleControlIdToReplace = cgiGet( edtavDdo_contratodadoscertame_sitetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ddo_ContratoDadosCertame_SiteTitleControlIdToReplace", AV69ddo_ContratoDadosCertame_SiteTitleControlIdToReplace);
            AV73ddo_ContratoDadosCertame_UasgTitleControlIdToReplace = cgiGet( edtavDdo_contratodadoscertame_uasgtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ddo_ContratoDadosCertame_UasgTitleControlIdToReplace", AV73ddo_ContratoDadosCertame_UasgTitleControlIdToReplace);
            AV79ddo_ContratoDadosCertame_DataHomologacaoTitleControlIdToReplace = cgiGet( edtavDdo_contratodadoscertame_datahomologacaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79ddo_ContratoDadosCertame_DataHomologacaoTitleControlIdToReplace", AV79ddo_ContratoDadosCertame_DataHomologacaoTitleControlIdToReplace);
            AV85ddo_ContratoDadosCertame_DataAdjudicacaoTitleControlIdToReplace = cgiGet( edtavDdo_contratodadoscertame_dataadjudicacaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85ddo_ContratoDadosCertame_DataAdjudicacaoTitleControlIdToReplace", AV85ddo_ContratoDadosCertame_DataAdjudicacaoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_115 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_115"), ",", "."));
            AV88GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV89GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contrato_numero_Caption = cgiGet( "DDO_CONTRATO_NUMERO_Caption");
            Ddo_contrato_numero_Tooltip = cgiGet( "DDO_CONTRATO_NUMERO_Tooltip");
            Ddo_contrato_numero_Cls = cgiGet( "DDO_CONTRATO_NUMERO_Cls");
            Ddo_contrato_numero_Filteredtext_set = cgiGet( "DDO_CONTRATO_NUMERO_Filteredtext_set");
            Ddo_contrato_numero_Selectedvalue_set = cgiGet( "DDO_CONTRATO_NUMERO_Selectedvalue_set");
            Ddo_contrato_numero_Dropdownoptionstype = cgiGet( "DDO_CONTRATO_NUMERO_Dropdownoptionstype");
            Ddo_contrato_numero_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATO_NUMERO_Titlecontrolidtoreplace");
            Ddo_contrato_numero_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includesortasc"));
            Ddo_contrato_numero_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includesortdsc"));
            Ddo_contrato_numero_Sortedstatus = cgiGet( "DDO_CONTRATO_NUMERO_Sortedstatus");
            Ddo_contrato_numero_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includefilter"));
            Ddo_contrato_numero_Filtertype = cgiGet( "DDO_CONTRATO_NUMERO_Filtertype");
            Ddo_contrato_numero_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Filterisrange"));
            Ddo_contrato_numero_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includedatalist"));
            Ddo_contrato_numero_Datalisttype = cgiGet( "DDO_CONTRATO_NUMERO_Datalisttype");
            Ddo_contrato_numero_Datalistproc = cgiGet( "DDO_CONTRATO_NUMERO_Datalistproc");
            Ddo_contrato_numero_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATO_NUMERO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contrato_numero_Sortasc = cgiGet( "DDO_CONTRATO_NUMERO_Sortasc");
            Ddo_contrato_numero_Sortdsc = cgiGet( "DDO_CONTRATO_NUMERO_Sortdsc");
            Ddo_contrato_numero_Loadingdata = cgiGet( "DDO_CONTRATO_NUMERO_Loadingdata");
            Ddo_contrato_numero_Cleanfilter = cgiGet( "DDO_CONTRATO_NUMERO_Cleanfilter");
            Ddo_contrato_numero_Noresultsfound = cgiGet( "DDO_CONTRATO_NUMERO_Noresultsfound");
            Ddo_contrato_numero_Searchbuttontext = cgiGet( "DDO_CONTRATO_NUMERO_Searchbuttontext");
            Ddo_contratodadoscertame_data_Caption = cgiGet( "DDO_CONTRATODADOSCERTAME_DATA_Caption");
            Ddo_contratodadoscertame_data_Tooltip = cgiGet( "DDO_CONTRATODADOSCERTAME_DATA_Tooltip");
            Ddo_contratodadoscertame_data_Cls = cgiGet( "DDO_CONTRATODADOSCERTAME_DATA_Cls");
            Ddo_contratodadoscertame_data_Filteredtext_set = cgiGet( "DDO_CONTRATODADOSCERTAME_DATA_Filteredtext_set");
            Ddo_contratodadoscertame_data_Filteredtextto_set = cgiGet( "DDO_CONTRATODADOSCERTAME_DATA_Filteredtextto_set");
            Ddo_contratodadoscertame_data_Dropdownoptionstype = cgiGet( "DDO_CONTRATODADOSCERTAME_DATA_Dropdownoptionstype");
            Ddo_contratodadoscertame_data_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATODADOSCERTAME_DATA_Titlecontrolidtoreplace");
            Ddo_contratodadoscertame_data_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATODADOSCERTAME_DATA_Includesortasc"));
            Ddo_contratodadoscertame_data_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATODADOSCERTAME_DATA_Includesortdsc"));
            Ddo_contratodadoscertame_data_Sortedstatus = cgiGet( "DDO_CONTRATODADOSCERTAME_DATA_Sortedstatus");
            Ddo_contratodadoscertame_data_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATODADOSCERTAME_DATA_Includefilter"));
            Ddo_contratodadoscertame_data_Filtertype = cgiGet( "DDO_CONTRATODADOSCERTAME_DATA_Filtertype");
            Ddo_contratodadoscertame_data_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATODADOSCERTAME_DATA_Filterisrange"));
            Ddo_contratodadoscertame_data_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATODADOSCERTAME_DATA_Includedatalist"));
            Ddo_contratodadoscertame_data_Sortasc = cgiGet( "DDO_CONTRATODADOSCERTAME_DATA_Sortasc");
            Ddo_contratodadoscertame_data_Sortdsc = cgiGet( "DDO_CONTRATODADOSCERTAME_DATA_Sortdsc");
            Ddo_contratodadoscertame_data_Cleanfilter = cgiGet( "DDO_CONTRATODADOSCERTAME_DATA_Cleanfilter");
            Ddo_contratodadoscertame_data_Rangefilterfrom = cgiGet( "DDO_CONTRATODADOSCERTAME_DATA_Rangefilterfrom");
            Ddo_contratodadoscertame_data_Rangefilterto = cgiGet( "DDO_CONTRATODADOSCERTAME_DATA_Rangefilterto");
            Ddo_contratodadoscertame_data_Searchbuttontext = cgiGet( "DDO_CONTRATODADOSCERTAME_DATA_Searchbuttontext");
            Ddo_contratodadoscertame_modalidade_Caption = cgiGet( "DDO_CONTRATODADOSCERTAME_MODALIDADE_Caption");
            Ddo_contratodadoscertame_modalidade_Tooltip = cgiGet( "DDO_CONTRATODADOSCERTAME_MODALIDADE_Tooltip");
            Ddo_contratodadoscertame_modalidade_Cls = cgiGet( "DDO_CONTRATODADOSCERTAME_MODALIDADE_Cls");
            Ddo_contratodadoscertame_modalidade_Filteredtext_set = cgiGet( "DDO_CONTRATODADOSCERTAME_MODALIDADE_Filteredtext_set");
            Ddo_contratodadoscertame_modalidade_Selectedvalue_set = cgiGet( "DDO_CONTRATODADOSCERTAME_MODALIDADE_Selectedvalue_set");
            Ddo_contratodadoscertame_modalidade_Dropdownoptionstype = cgiGet( "DDO_CONTRATODADOSCERTAME_MODALIDADE_Dropdownoptionstype");
            Ddo_contratodadoscertame_modalidade_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATODADOSCERTAME_MODALIDADE_Titlecontrolidtoreplace");
            Ddo_contratodadoscertame_modalidade_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATODADOSCERTAME_MODALIDADE_Includesortasc"));
            Ddo_contratodadoscertame_modalidade_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATODADOSCERTAME_MODALIDADE_Includesortdsc"));
            Ddo_contratodadoscertame_modalidade_Sortedstatus = cgiGet( "DDO_CONTRATODADOSCERTAME_MODALIDADE_Sortedstatus");
            Ddo_contratodadoscertame_modalidade_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATODADOSCERTAME_MODALIDADE_Includefilter"));
            Ddo_contratodadoscertame_modalidade_Filtertype = cgiGet( "DDO_CONTRATODADOSCERTAME_MODALIDADE_Filtertype");
            Ddo_contratodadoscertame_modalidade_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATODADOSCERTAME_MODALIDADE_Filterisrange"));
            Ddo_contratodadoscertame_modalidade_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATODADOSCERTAME_MODALIDADE_Includedatalist"));
            Ddo_contratodadoscertame_modalidade_Datalisttype = cgiGet( "DDO_CONTRATODADOSCERTAME_MODALIDADE_Datalisttype");
            Ddo_contratodadoscertame_modalidade_Datalistproc = cgiGet( "DDO_CONTRATODADOSCERTAME_MODALIDADE_Datalistproc");
            Ddo_contratodadoscertame_modalidade_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATODADOSCERTAME_MODALIDADE_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratodadoscertame_modalidade_Sortasc = cgiGet( "DDO_CONTRATODADOSCERTAME_MODALIDADE_Sortasc");
            Ddo_contratodadoscertame_modalidade_Sortdsc = cgiGet( "DDO_CONTRATODADOSCERTAME_MODALIDADE_Sortdsc");
            Ddo_contratodadoscertame_modalidade_Loadingdata = cgiGet( "DDO_CONTRATODADOSCERTAME_MODALIDADE_Loadingdata");
            Ddo_contratodadoscertame_modalidade_Cleanfilter = cgiGet( "DDO_CONTRATODADOSCERTAME_MODALIDADE_Cleanfilter");
            Ddo_contratodadoscertame_modalidade_Noresultsfound = cgiGet( "DDO_CONTRATODADOSCERTAME_MODALIDADE_Noresultsfound");
            Ddo_contratodadoscertame_modalidade_Searchbuttontext = cgiGet( "DDO_CONTRATODADOSCERTAME_MODALIDADE_Searchbuttontext");
            Ddo_contratodadoscertame_numero_Caption = cgiGet( "DDO_CONTRATODADOSCERTAME_NUMERO_Caption");
            Ddo_contratodadoscertame_numero_Tooltip = cgiGet( "DDO_CONTRATODADOSCERTAME_NUMERO_Tooltip");
            Ddo_contratodadoscertame_numero_Cls = cgiGet( "DDO_CONTRATODADOSCERTAME_NUMERO_Cls");
            Ddo_contratodadoscertame_numero_Filteredtext_set = cgiGet( "DDO_CONTRATODADOSCERTAME_NUMERO_Filteredtext_set");
            Ddo_contratodadoscertame_numero_Filteredtextto_set = cgiGet( "DDO_CONTRATODADOSCERTAME_NUMERO_Filteredtextto_set");
            Ddo_contratodadoscertame_numero_Dropdownoptionstype = cgiGet( "DDO_CONTRATODADOSCERTAME_NUMERO_Dropdownoptionstype");
            Ddo_contratodadoscertame_numero_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATODADOSCERTAME_NUMERO_Titlecontrolidtoreplace");
            Ddo_contratodadoscertame_numero_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATODADOSCERTAME_NUMERO_Includesortasc"));
            Ddo_contratodadoscertame_numero_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATODADOSCERTAME_NUMERO_Includesortdsc"));
            Ddo_contratodadoscertame_numero_Sortedstatus = cgiGet( "DDO_CONTRATODADOSCERTAME_NUMERO_Sortedstatus");
            Ddo_contratodadoscertame_numero_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATODADOSCERTAME_NUMERO_Includefilter"));
            Ddo_contratodadoscertame_numero_Filtertype = cgiGet( "DDO_CONTRATODADOSCERTAME_NUMERO_Filtertype");
            Ddo_contratodadoscertame_numero_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATODADOSCERTAME_NUMERO_Filterisrange"));
            Ddo_contratodadoscertame_numero_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATODADOSCERTAME_NUMERO_Includedatalist"));
            Ddo_contratodadoscertame_numero_Sortasc = cgiGet( "DDO_CONTRATODADOSCERTAME_NUMERO_Sortasc");
            Ddo_contratodadoscertame_numero_Sortdsc = cgiGet( "DDO_CONTRATODADOSCERTAME_NUMERO_Sortdsc");
            Ddo_contratodadoscertame_numero_Cleanfilter = cgiGet( "DDO_CONTRATODADOSCERTAME_NUMERO_Cleanfilter");
            Ddo_contratodadoscertame_numero_Rangefilterfrom = cgiGet( "DDO_CONTRATODADOSCERTAME_NUMERO_Rangefilterfrom");
            Ddo_contratodadoscertame_numero_Rangefilterto = cgiGet( "DDO_CONTRATODADOSCERTAME_NUMERO_Rangefilterto");
            Ddo_contratodadoscertame_numero_Searchbuttontext = cgiGet( "DDO_CONTRATODADOSCERTAME_NUMERO_Searchbuttontext");
            Ddo_contratodadoscertame_site_Caption = cgiGet( "DDO_CONTRATODADOSCERTAME_SITE_Caption");
            Ddo_contratodadoscertame_site_Tooltip = cgiGet( "DDO_CONTRATODADOSCERTAME_SITE_Tooltip");
            Ddo_contratodadoscertame_site_Cls = cgiGet( "DDO_CONTRATODADOSCERTAME_SITE_Cls");
            Ddo_contratodadoscertame_site_Filteredtext_set = cgiGet( "DDO_CONTRATODADOSCERTAME_SITE_Filteredtext_set");
            Ddo_contratodadoscertame_site_Selectedvalue_set = cgiGet( "DDO_CONTRATODADOSCERTAME_SITE_Selectedvalue_set");
            Ddo_contratodadoscertame_site_Dropdownoptionstype = cgiGet( "DDO_CONTRATODADOSCERTAME_SITE_Dropdownoptionstype");
            Ddo_contratodadoscertame_site_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATODADOSCERTAME_SITE_Titlecontrolidtoreplace");
            Ddo_contratodadoscertame_site_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATODADOSCERTAME_SITE_Includesortasc"));
            Ddo_contratodadoscertame_site_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATODADOSCERTAME_SITE_Includesortdsc"));
            Ddo_contratodadoscertame_site_Sortedstatus = cgiGet( "DDO_CONTRATODADOSCERTAME_SITE_Sortedstatus");
            Ddo_contratodadoscertame_site_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATODADOSCERTAME_SITE_Includefilter"));
            Ddo_contratodadoscertame_site_Filtertype = cgiGet( "DDO_CONTRATODADOSCERTAME_SITE_Filtertype");
            Ddo_contratodadoscertame_site_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATODADOSCERTAME_SITE_Filterisrange"));
            Ddo_contratodadoscertame_site_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATODADOSCERTAME_SITE_Includedatalist"));
            Ddo_contratodadoscertame_site_Datalisttype = cgiGet( "DDO_CONTRATODADOSCERTAME_SITE_Datalisttype");
            Ddo_contratodadoscertame_site_Datalistproc = cgiGet( "DDO_CONTRATODADOSCERTAME_SITE_Datalistproc");
            Ddo_contratodadoscertame_site_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATODADOSCERTAME_SITE_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratodadoscertame_site_Sortasc = cgiGet( "DDO_CONTRATODADOSCERTAME_SITE_Sortasc");
            Ddo_contratodadoscertame_site_Sortdsc = cgiGet( "DDO_CONTRATODADOSCERTAME_SITE_Sortdsc");
            Ddo_contratodadoscertame_site_Loadingdata = cgiGet( "DDO_CONTRATODADOSCERTAME_SITE_Loadingdata");
            Ddo_contratodadoscertame_site_Cleanfilter = cgiGet( "DDO_CONTRATODADOSCERTAME_SITE_Cleanfilter");
            Ddo_contratodadoscertame_site_Noresultsfound = cgiGet( "DDO_CONTRATODADOSCERTAME_SITE_Noresultsfound");
            Ddo_contratodadoscertame_site_Searchbuttontext = cgiGet( "DDO_CONTRATODADOSCERTAME_SITE_Searchbuttontext");
            Ddo_contratodadoscertame_uasg_Caption = cgiGet( "DDO_CONTRATODADOSCERTAME_UASG_Caption");
            Ddo_contratodadoscertame_uasg_Tooltip = cgiGet( "DDO_CONTRATODADOSCERTAME_UASG_Tooltip");
            Ddo_contratodadoscertame_uasg_Cls = cgiGet( "DDO_CONTRATODADOSCERTAME_UASG_Cls");
            Ddo_contratodadoscertame_uasg_Filteredtext_set = cgiGet( "DDO_CONTRATODADOSCERTAME_UASG_Filteredtext_set");
            Ddo_contratodadoscertame_uasg_Filteredtextto_set = cgiGet( "DDO_CONTRATODADOSCERTAME_UASG_Filteredtextto_set");
            Ddo_contratodadoscertame_uasg_Dropdownoptionstype = cgiGet( "DDO_CONTRATODADOSCERTAME_UASG_Dropdownoptionstype");
            Ddo_contratodadoscertame_uasg_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATODADOSCERTAME_UASG_Titlecontrolidtoreplace");
            Ddo_contratodadoscertame_uasg_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATODADOSCERTAME_UASG_Includesortasc"));
            Ddo_contratodadoscertame_uasg_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATODADOSCERTAME_UASG_Includesortdsc"));
            Ddo_contratodadoscertame_uasg_Sortedstatus = cgiGet( "DDO_CONTRATODADOSCERTAME_UASG_Sortedstatus");
            Ddo_contratodadoscertame_uasg_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATODADOSCERTAME_UASG_Includefilter"));
            Ddo_contratodadoscertame_uasg_Filtertype = cgiGet( "DDO_CONTRATODADOSCERTAME_UASG_Filtertype");
            Ddo_contratodadoscertame_uasg_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATODADOSCERTAME_UASG_Filterisrange"));
            Ddo_contratodadoscertame_uasg_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATODADOSCERTAME_UASG_Includedatalist"));
            Ddo_contratodadoscertame_uasg_Sortasc = cgiGet( "DDO_CONTRATODADOSCERTAME_UASG_Sortasc");
            Ddo_contratodadoscertame_uasg_Sortdsc = cgiGet( "DDO_CONTRATODADOSCERTAME_UASG_Sortdsc");
            Ddo_contratodadoscertame_uasg_Cleanfilter = cgiGet( "DDO_CONTRATODADOSCERTAME_UASG_Cleanfilter");
            Ddo_contratodadoscertame_uasg_Rangefilterfrom = cgiGet( "DDO_CONTRATODADOSCERTAME_UASG_Rangefilterfrom");
            Ddo_contratodadoscertame_uasg_Rangefilterto = cgiGet( "DDO_CONTRATODADOSCERTAME_UASG_Rangefilterto");
            Ddo_contratodadoscertame_uasg_Searchbuttontext = cgiGet( "DDO_CONTRATODADOSCERTAME_UASG_Searchbuttontext");
            Ddo_contratodadoscertame_datahomologacao_Caption = cgiGet( "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Caption");
            Ddo_contratodadoscertame_datahomologacao_Tooltip = cgiGet( "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Tooltip");
            Ddo_contratodadoscertame_datahomologacao_Cls = cgiGet( "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Cls");
            Ddo_contratodadoscertame_datahomologacao_Filteredtext_set = cgiGet( "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Filteredtext_set");
            Ddo_contratodadoscertame_datahomologacao_Filteredtextto_set = cgiGet( "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Filteredtextto_set");
            Ddo_contratodadoscertame_datahomologacao_Dropdownoptionstype = cgiGet( "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Dropdownoptionstype");
            Ddo_contratodadoscertame_datahomologacao_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Titlecontrolidtoreplace");
            Ddo_contratodadoscertame_datahomologacao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Includesortasc"));
            Ddo_contratodadoscertame_datahomologacao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Includesortdsc"));
            Ddo_contratodadoscertame_datahomologacao_Sortedstatus = cgiGet( "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Sortedstatus");
            Ddo_contratodadoscertame_datahomologacao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Includefilter"));
            Ddo_contratodadoscertame_datahomologacao_Filtertype = cgiGet( "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Filtertype");
            Ddo_contratodadoscertame_datahomologacao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Filterisrange"));
            Ddo_contratodadoscertame_datahomologacao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Includedatalist"));
            Ddo_contratodadoscertame_datahomologacao_Sortasc = cgiGet( "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Sortasc");
            Ddo_contratodadoscertame_datahomologacao_Sortdsc = cgiGet( "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Sortdsc");
            Ddo_contratodadoscertame_datahomologacao_Cleanfilter = cgiGet( "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Cleanfilter");
            Ddo_contratodadoscertame_datahomologacao_Rangefilterfrom = cgiGet( "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Rangefilterfrom");
            Ddo_contratodadoscertame_datahomologacao_Rangefilterto = cgiGet( "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Rangefilterto");
            Ddo_contratodadoscertame_datahomologacao_Searchbuttontext = cgiGet( "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Searchbuttontext");
            Ddo_contratodadoscertame_dataadjudicacao_Caption = cgiGet( "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Caption");
            Ddo_contratodadoscertame_dataadjudicacao_Tooltip = cgiGet( "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Tooltip");
            Ddo_contratodadoscertame_dataadjudicacao_Cls = cgiGet( "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Cls");
            Ddo_contratodadoscertame_dataadjudicacao_Filteredtext_set = cgiGet( "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Filteredtext_set");
            Ddo_contratodadoscertame_dataadjudicacao_Filteredtextto_set = cgiGet( "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Filteredtextto_set");
            Ddo_contratodadoscertame_dataadjudicacao_Dropdownoptionstype = cgiGet( "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Dropdownoptionstype");
            Ddo_contratodadoscertame_dataadjudicacao_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Titlecontrolidtoreplace");
            Ddo_contratodadoscertame_dataadjudicacao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Includesortasc"));
            Ddo_contratodadoscertame_dataadjudicacao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Includesortdsc"));
            Ddo_contratodadoscertame_dataadjudicacao_Sortedstatus = cgiGet( "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Sortedstatus");
            Ddo_contratodadoscertame_dataadjudicacao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Includefilter"));
            Ddo_contratodadoscertame_dataadjudicacao_Filtertype = cgiGet( "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Filtertype");
            Ddo_contratodadoscertame_dataadjudicacao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Filterisrange"));
            Ddo_contratodadoscertame_dataadjudicacao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Includedatalist"));
            Ddo_contratodadoscertame_dataadjudicacao_Sortasc = cgiGet( "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Sortasc");
            Ddo_contratodadoscertame_dataadjudicacao_Sortdsc = cgiGet( "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Sortdsc");
            Ddo_contratodadoscertame_dataadjudicacao_Cleanfilter = cgiGet( "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Cleanfilter");
            Ddo_contratodadoscertame_dataadjudicacao_Rangefilterfrom = cgiGet( "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Rangefilterfrom");
            Ddo_contratodadoscertame_dataadjudicacao_Rangefilterto = cgiGet( "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Rangefilterto");
            Ddo_contratodadoscertame_dataadjudicacao_Searchbuttontext = cgiGet( "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contrato_numero_Activeeventkey = cgiGet( "DDO_CONTRATO_NUMERO_Activeeventkey");
            Ddo_contrato_numero_Filteredtext_get = cgiGet( "DDO_CONTRATO_NUMERO_Filteredtext_get");
            Ddo_contrato_numero_Selectedvalue_get = cgiGet( "DDO_CONTRATO_NUMERO_Selectedvalue_get");
            Ddo_contratodadoscertame_data_Activeeventkey = cgiGet( "DDO_CONTRATODADOSCERTAME_DATA_Activeeventkey");
            Ddo_contratodadoscertame_data_Filteredtext_get = cgiGet( "DDO_CONTRATODADOSCERTAME_DATA_Filteredtext_get");
            Ddo_contratodadoscertame_data_Filteredtextto_get = cgiGet( "DDO_CONTRATODADOSCERTAME_DATA_Filteredtextto_get");
            Ddo_contratodadoscertame_modalidade_Activeeventkey = cgiGet( "DDO_CONTRATODADOSCERTAME_MODALIDADE_Activeeventkey");
            Ddo_contratodadoscertame_modalidade_Filteredtext_get = cgiGet( "DDO_CONTRATODADOSCERTAME_MODALIDADE_Filteredtext_get");
            Ddo_contratodadoscertame_modalidade_Selectedvalue_get = cgiGet( "DDO_CONTRATODADOSCERTAME_MODALIDADE_Selectedvalue_get");
            Ddo_contratodadoscertame_numero_Activeeventkey = cgiGet( "DDO_CONTRATODADOSCERTAME_NUMERO_Activeeventkey");
            Ddo_contratodadoscertame_numero_Filteredtext_get = cgiGet( "DDO_CONTRATODADOSCERTAME_NUMERO_Filteredtext_get");
            Ddo_contratodadoscertame_numero_Filteredtextto_get = cgiGet( "DDO_CONTRATODADOSCERTAME_NUMERO_Filteredtextto_get");
            Ddo_contratodadoscertame_site_Activeeventkey = cgiGet( "DDO_CONTRATODADOSCERTAME_SITE_Activeeventkey");
            Ddo_contratodadoscertame_site_Filteredtext_get = cgiGet( "DDO_CONTRATODADOSCERTAME_SITE_Filteredtext_get");
            Ddo_contratodadoscertame_site_Selectedvalue_get = cgiGet( "DDO_CONTRATODADOSCERTAME_SITE_Selectedvalue_get");
            Ddo_contratodadoscertame_uasg_Activeeventkey = cgiGet( "DDO_CONTRATODADOSCERTAME_UASG_Activeeventkey");
            Ddo_contratodadoscertame_uasg_Filteredtext_get = cgiGet( "DDO_CONTRATODADOSCERTAME_UASG_Filteredtext_get");
            Ddo_contratodadoscertame_uasg_Filteredtextto_get = cgiGet( "DDO_CONTRATODADOSCERTAME_UASG_Filteredtextto_get");
            Ddo_contratodadoscertame_datahomologacao_Activeeventkey = cgiGet( "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Activeeventkey");
            Ddo_contratodadoscertame_datahomologacao_Filteredtext_get = cgiGet( "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Filteredtext_get");
            Ddo_contratodadoscertame_datahomologacao_Filteredtextto_get = cgiGet( "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO_Filteredtextto_get");
            Ddo_contratodadoscertame_dataadjudicacao_Activeeventkey = cgiGet( "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Activeeventkey");
            Ddo_contratodadoscertame_dataadjudicacao_Filteredtext_get = cgiGet( "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Filteredtext_get");
            Ddo_contratodadoscertame_dataadjudicacao_Filteredtextto_get = cgiGet( "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATODADOSCERTAME_DATA1"), 0) != AV37ContratoDadosCertame_Data1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATODADOSCERTAME_DATA_TO1"), 0) != AV38ContratoDadosCertame_Data_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATODADOSCERTAME_MODALIDADE1"), AV17ContratoDadosCertame_Modalidade1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATODADOSCERTAME_NUMERO1"), ",", ".") != Convert.ToDecimal( AV39ContratoDadosCertame_Numero1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATODADOSCERTAME_DATA2"), 0) != AV40ContratoDadosCertame_Data2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATODADOSCERTAME_DATA_TO2"), 0) != AV41ContratoDadosCertame_Data_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATODADOSCERTAME_MODALIDADE2"), AV21ContratoDadosCertame_Modalidade2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATODADOSCERTAME_NUMERO2"), ",", ".") != Convert.ToDecimal( AV42ContratoDadosCertame_Numero2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATODADOSCERTAME_DATA3"), 0) != AV43ContratoDadosCertame_Data3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATODADOSCERTAME_DATA_TO3"), 0) != AV44ContratoDadosCertame_Data_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATODADOSCERTAME_MODALIDADE3"), AV25ContratoDadosCertame_Modalidade3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATODADOSCERTAME_NUMERO3"), ",", ".") != Convert.ToDecimal( AV45ContratoDadosCertame_Numero3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO"), AV49TFContrato_Numero) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO_SEL"), AV50TFContrato_Numero_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATODADOSCERTAME_DATA"), 0) != AV53TFContratoDadosCertame_Data )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATODADOSCERTAME_DATA_TO"), 0) != AV54TFContratoDadosCertame_Data_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATODADOSCERTAME_MODALIDADE"), AV59TFContratoDadosCertame_Modalidade) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATODADOSCERTAME_MODALIDADE_SEL"), AV60TFContratoDadosCertame_Modalidade_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATODADOSCERTAME_NUMERO"), ",", ".") != Convert.ToDecimal( AV63TFContratoDadosCertame_Numero )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATODADOSCERTAME_NUMERO_TO"), ",", ".") != Convert.ToDecimal( AV64TFContratoDadosCertame_Numero_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATODADOSCERTAME_SITE"), AV67TFContratoDadosCertame_Site) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATODADOSCERTAME_SITE_SEL"), AV68TFContratoDadosCertame_Site_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATODADOSCERTAME_UASG"), ",", ".") != Convert.ToDecimal( AV71TFContratoDadosCertame_Uasg )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATODADOSCERTAME_UASG_TO"), ",", ".") != Convert.ToDecimal( AV72TFContratoDadosCertame_Uasg_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO"), 0) != AV75TFContratoDadosCertame_DataHomologacao )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO_TO"), 0) != AV76TFContratoDadosCertame_DataHomologacao_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATODADOSCERTAME_DATAADJUDICACAO"), 0) != AV81TFContratoDadosCertame_DataAdjudicacao )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATODADOSCERTAME_DATAADJUDICACAO_TO"), 0) != AV82TFContratoDadosCertame_DataAdjudicacao_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E317G2 */
         E317G2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E317G2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "CONTRATODADOSCERTAME_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "CONTRATODADOSCERTAME_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "CONTRATODADOSCERTAME_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfcontrato_numero_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_numero_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_numero_Visible), 5, 0)));
         edtavTfcontrato_numero_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_numero_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_numero_sel_Visible), 5, 0)));
         edtavTfcontratodadoscertame_data_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratodadoscertame_data_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratodadoscertame_data_Visible), 5, 0)));
         edtavTfcontratodadoscertame_data_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratodadoscertame_data_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratodadoscertame_data_to_Visible), 5, 0)));
         edtavTfcontratodadoscertame_modalidade_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratodadoscertame_modalidade_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratodadoscertame_modalidade_Visible), 5, 0)));
         edtavTfcontratodadoscertame_modalidade_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratodadoscertame_modalidade_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratodadoscertame_modalidade_sel_Visible), 5, 0)));
         edtavTfcontratodadoscertame_numero_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratodadoscertame_numero_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratodadoscertame_numero_Visible), 5, 0)));
         edtavTfcontratodadoscertame_numero_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratodadoscertame_numero_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratodadoscertame_numero_to_Visible), 5, 0)));
         edtavTfcontratodadoscertame_site_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratodadoscertame_site_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratodadoscertame_site_Visible), 5, 0)));
         edtavTfcontratodadoscertame_site_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratodadoscertame_site_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratodadoscertame_site_sel_Visible), 5, 0)));
         edtavTfcontratodadoscertame_uasg_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratodadoscertame_uasg_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratodadoscertame_uasg_Visible), 5, 0)));
         edtavTfcontratodadoscertame_uasg_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratodadoscertame_uasg_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratodadoscertame_uasg_to_Visible), 5, 0)));
         edtavTfcontratodadoscertame_datahomologacao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratodadoscertame_datahomologacao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratodadoscertame_datahomologacao_Visible), 5, 0)));
         edtavTfcontratodadoscertame_datahomologacao_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratodadoscertame_datahomologacao_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratodadoscertame_datahomologacao_to_Visible), 5, 0)));
         edtavTfcontratodadoscertame_dataadjudicacao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratodadoscertame_dataadjudicacao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratodadoscertame_dataadjudicacao_Visible), 5, 0)));
         edtavTfcontratodadoscertame_dataadjudicacao_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratodadoscertame_dataadjudicacao_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratodadoscertame_dataadjudicacao_to_Visible), 5, 0)));
         Ddo_contrato_numero_Titlecontrolidtoreplace = subGrid_Internalname+"_Contrato_Numero";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "TitleControlIdToReplace", Ddo_contrato_numero_Titlecontrolidtoreplace);
         AV51ddo_Contrato_NumeroTitleControlIdToReplace = Ddo_contrato_numero_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ddo_Contrato_NumeroTitleControlIdToReplace", AV51ddo_Contrato_NumeroTitleControlIdToReplace);
         edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratodadoscertame_data_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoDadosCertame_Data";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_data_Internalname, "TitleControlIdToReplace", Ddo_contratodadoscertame_data_Titlecontrolidtoreplace);
         AV57ddo_ContratoDadosCertame_DataTitleControlIdToReplace = Ddo_contratodadoscertame_data_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_ContratoDadosCertame_DataTitleControlIdToReplace", AV57ddo_ContratoDadosCertame_DataTitleControlIdToReplace);
         edtavDdo_contratodadoscertame_datatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratodadoscertame_datatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratodadoscertame_datatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratodadoscertame_modalidade_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoDadosCertame_Modalidade";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_modalidade_Internalname, "TitleControlIdToReplace", Ddo_contratodadoscertame_modalidade_Titlecontrolidtoreplace);
         AV61ddo_ContratoDadosCertame_ModalidadeTitleControlIdToReplace = Ddo_contratodadoscertame_modalidade_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_ContratoDadosCertame_ModalidadeTitleControlIdToReplace", AV61ddo_ContratoDadosCertame_ModalidadeTitleControlIdToReplace);
         edtavDdo_contratodadoscertame_modalidadetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratodadoscertame_modalidadetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratodadoscertame_modalidadetitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratodadoscertame_numero_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoDadosCertame_Numero";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_numero_Internalname, "TitleControlIdToReplace", Ddo_contratodadoscertame_numero_Titlecontrolidtoreplace);
         AV65ddo_ContratoDadosCertame_NumeroTitleControlIdToReplace = Ddo_contratodadoscertame_numero_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ddo_ContratoDadosCertame_NumeroTitleControlIdToReplace", AV65ddo_ContratoDadosCertame_NumeroTitleControlIdToReplace);
         edtavDdo_contratodadoscertame_numerotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratodadoscertame_numerotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratodadoscertame_numerotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratodadoscertame_site_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoDadosCertame_Site";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_site_Internalname, "TitleControlIdToReplace", Ddo_contratodadoscertame_site_Titlecontrolidtoreplace);
         AV69ddo_ContratoDadosCertame_SiteTitleControlIdToReplace = Ddo_contratodadoscertame_site_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ddo_ContratoDadosCertame_SiteTitleControlIdToReplace", AV69ddo_ContratoDadosCertame_SiteTitleControlIdToReplace);
         edtavDdo_contratodadoscertame_sitetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratodadoscertame_sitetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratodadoscertame_sitetitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratodadoscertame_uasg_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoDadosCertame_Uasg";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_uasg_Internalname, "TitleControlIdToReplace", Ddo_contratodadoscertame_uasg_Titlecontrolidtoreplace);
         AV73ddo_ContratoDadosCertame_UasgTitleControlIdToReplace = Ddo_contratodadoscertame_uasg_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ddo_ContratoDadosCertame_UasgTitleControlIdToReplace", AV73ddo_ContratoDadosCertame_UasgTitleControlIdToReplace);
         edtavDdo_contratodadoscertame_uasgtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratodadoscertame_uasgtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratodadoscertame_uasgtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratodadoscertame_datahomologacao_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoDadosCertame_DataHomologacao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_datahomologacao_Internalname, "TitleControlIdToReplace", Ddo_contratodadoscertame_datahomologacao_Titlecontrolidtoreplace);
         AV79ddo_ContratoDadosCertame_DataHomologacaoTitleControlIdToReplace = Ddo_contratodadoscertame_datahomologacao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79ddo_ContratoDadosCertame_DataHomologacaoTitleControlIdToReplace", AV79ddo_ContratoDadosCertame_DataHomologacaoTitleControlIdToReplace);
         edtavDdo_contratodadoscertame_datahomologacaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratodadoscertame_datahomologacaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratodadoscertame_datahomologacaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratodadoscertame_dataadjudicacao_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoDadosCertame_DataAdjudicacao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_dataadjudicacao_Internalname, "TitleControlIdToReplace", Ddo_contratodadoscertame_dataadjudicacao_Titlecontrolidtoreplace);
         AV85ddo_ContratoDadosCertame_DataAdjudicacaoTitleControlIdToReplace = Ddo_contratodadoscertame_dataadjudicacao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85ddo_ContratoDadosCertame_DataAdjudicacaoTitleControlIdToReplace", AV85ddo_ContratoDadosCertame_DataAdjudicacaoTitleControlIdToReplace);
         edtavDdo_contratodadoscertame_dataadjudicacaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratodadoscertame_dataadjudicacaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratodadoscertame_dataadjudicacaotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Contrato Dados Certame";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Data", 0);
         cmbavOrderedby.addItem("2", "Modalidade", 0);
         cmbavOrderedby.addItem("3", "Numero", 0);
         cmbavOrderedby.addItem("4", "N� Contrato", 0);
         cmbavOrderedby.addItem("5", "Site", 0);
         cmbavOrderedby.addItem("6", "Uasg", 0);
         cmbavOrderedby.addItem("7", "de homologa��o", 0);
         cmbavOrderedby.addItem("8", "de adjudica��o", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV86DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV86DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E327G2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV48Contrato_NumeroTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV52ContratoDadosCertame_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV58ContratoDadosCertame_ModalidadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV62ContratoDadosCertame_NumeroTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV66ContratoDadosCertame_SiteTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV70ContratoDadosCertame_UasgTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV74ContratoDadosCertame_DataHomologacaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV80ContratoDadosCertame_DataAdjudicacaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         cmbavDynamicfiltersoperator1.removeAllItems();
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATODADOSCERTAME_MODALIDADE") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATODADOSCERTAME_NUMERO") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator1.addItem("2", ">", 0);
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            cmbavDynamicfiltersoperator2.removeAllItems();
            if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATODADOSCERTAME_MODALIDADE") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATODADOSCERTAME_NUMERO") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "<", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "=", 0);
               cmbavDynamicfiltersoperator2.addItem("2", ">", 0);
            }
            if ( AV22DynamicFiltersEnabled3 )
            {
               cmbavDynamicfiltersoperator3.removeAllItems();
               if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATODADOSCERTAME_MODALIDADE") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATODADOSCERTAME_NUMERO") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "<", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "=", 0);
                  cmbavDynamicfiltersoperator3.addItem("2", ">", 0);
               }
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContrato_Numero_Titleformat = 2;
         edtContrato_Numero_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "N� Contrato", AV51ddo_Contrato_NumeroTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Numero_Internalname, "Title", edtContrato_Numero_Title);
         edtContratoDadosCertame_Data_Titleformat = 2;
         edtContratoDadosCertame_Data_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Data", AV57ddo_ContratoDadosCertame_DataTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoDadosCertame_Data_Internalname, "Title", edtContratoDadosCertame_Data_Title);
         edtContratoDadosCertame_Modalidade_Titleformat = 2;
         edtContratoDadosCertame_Modalidade_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Modalidade", AV61ddo_ContratoDadosCertame_ModalidadeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoDadosCertame_Modalidade_Internalname, "Title", edtContratoDadosCertame_Modalidade_Title);
         edtContratoDadosCertame_Numero_Titleformat = 2;
         edtContratoDadosCertame_Numero_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Numero", AV65ddo_ContratoDadosCertame_NumeroTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoDadosCertame_Numero_Internalname, "Title", edtContratoDadosCertame_Numero_Title);
         edtContratoDadosCertame_Site_Titleformat = 2;
         edtContratoDadosCertame_Site_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Site", AV69ddo_ContratoDadosCertame_SiteTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoDadosCertame_Site_Internalname, "Title", edtContratoDadosCertame_Site_Title);
         edtContratoDadosCertame_Uasg_Titleformat = 2;
         edtContratoDadosCertame_Uasg_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Uasg", AV73ddo_ContratoDadosCertame_UasgTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoDadosCertame_Uasg_Internalname, "Title", edtContratoDadosCertame_Uasg_Title);
         edtContratoDadosCertame_DataHomologacao_Titleformat = 2;
         edtContratoDadosCertame_DataHomologacao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "de homologa��o", AV79ddo_ContratoDadosCertame_DataHomologacaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoDadosCertame_DataHomologacao_Internalname, "Title", edtContratoDadosCertame_DataHomologacao_Title);
         edtContratoDadosCertame_DataAdjudicacao_Titleformat = 2;
         edtContratoDadosCertame_DataAdjudicacao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "de adjudica��o", AV85ddo_ContratoDadosCertame_DataAdjudicacaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoDadosCertame_DataAdjudicacao_Internalname, "Title", edtContratoDadosCertame_DataAdjudicacao_Title);
         AV88GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV88GridCurrentPage), 10, 0)));
         AV89GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV89GridPageCount), 10, 0)));
         AV92WWContratoDadosCertameDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV93WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV94WWContratoDadosCertameDS_3_Contratodadoscertame_data1 = AV37ContratoDadosCertame_Data1;
         AV95WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1 = AV38ContratoDadosCertame_Data_To1;
         AV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1 = AV17ContratoDadosCertame_Modalidade1;
         AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1 = AV39ContratoDadosCertame_Numero1;
         AV98WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV99WWContratoDadosCertameDS_8_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV100WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV101WWContratoDadosCertameDS_10_Contratodadoscertame_data2 = AV40ContratoDadosCertame_Data2;
         AV102WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2 = AV41ContratoDadosCertame_Data_To2;
         AV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2 = AV21ContratoDadosCertame_Modalidade2;
         AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2 = AV42ContratoDadosCertame_Numero2;
         AV105WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV106WWContratoDadosCertameDS_15_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV107WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV108WWContratoDadosCertameDS_17_Contratodadoscertame_data3 = AV43ContratoDadosCertame_Data3;
         AV109WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3 = AV44ContratoDadosCertame_Data_To3;
         AV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3 = AV25ContratoDadosCertame_Modalidade3;
         AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3 = AV45ContratoDadosCertame_Numero3;
         AV112WWContratoDadosCertameDS_21_Tfcontrato_numero = AV49TFContrato_Numero;
         AV113WWContratoDadosCertameDS_22_Tfcontrato_numero_sel = AV50TFContrato_Numero_Sel;
         AV114WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data = AV53TFContratoDadosCertame_Data;
         AV115WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to = AV54TFContratoDadosCertame_Data_To;
         AV116WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade = AV59TFContratoDadosCertame_Modalidade;
         AV117WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel = AV60TFContratoDadosCertame_Modalidade_Sel;
         AV118WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero = AV63TFContratoDadosCertame_Numero;
         AV119WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to = AV64TFContratoDadosCertame_Numero_To;
         AV120WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site = AV67TFContratoDadosCertame_Site;
         AV121WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel = AV68TFContratoDadosCertame_Site_Sel;
         AV122WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg = AV71TFContratoDadosCertame_Uasg;
         AV123WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to = AV72TFContratoDadosCertame_Uasg_To;
         AV124WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao = AV75TFContratoDadosCertame_DataHomologacao;
         AV125WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to = AV76TFContratoDadosCertame_DataHomologacao_To;
         AV126WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao = AV81TFContratoDadosCertame_DataAdjudicacao;
         AV127WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to = AV82TFContratoDadosCertame_DataAdjudicacao_To;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV48Contrato_NumeroTitleFilterData", AV48Contrato_NumeroTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV52ContratoDadosCertame_DataTitleFilterData", AV52ContratoDadosCertame_DataTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV58ContratoDadosCertame_ModalidadeTitleFilterData", AV58ContratoDadosCertame_ModalidadeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV62ContratoDadosCertame_NumeroTitleFilterData", AV62ContratoDadosCertame_NumeroTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV66ContratoDadosCertame_SiteTitleFilterData", AV66ContratoDadosCertame_SiteTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV70ContratoDadosCertame_UasgTitleFilterData", AV70ContratoDadosCertame_UasgTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV74ContratoDadosCertame_DataHomologacaoTitleFilterData", AV74ContratoDadosCertame_DataHomologacaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV80ContratoDadosCertame_DataAdjudicacaoTitleFilterData", AV80ContratoDadosCertame_DataAdjudicacaoTitleFilterData);
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E117G2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV87PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV87PageToGo) ;
         }
      }

      protected void E127G2( )
      {
         /* Ddo_contrato_numero_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contrato_numero_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contrato_numero_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_numero_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contrato_numero_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_numero_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV49TFContrato_Numero = Ddo_contrato_numero_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContrato_Numero", AV49TFContrato_Numero);
            AV50TFContrato_Numero_Sel = Ddo_contrato_numero_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContrato_Numero_Sel", AV50TFContrato_Numero_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E137G2( )
      {
         /* Ddo_contratodadoscertame_data_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratodadoscertame_data_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratodadoscertame_data_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_data_Internalname, "SortedStatus", Ddo_contratodadoscertame_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratodadoscertame_data_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratodadoscertame_data_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_data_Internalname, "SortedStatus", Ddo_contratodadoscertame_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratodadoscertame_data_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV53TFContratoDadosCertame_Data = context.localUtil.CToD( Ddo_contratodadoscertame_data_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContratoDadosCertame_Data", context.localUtil.Format(AV53TFContratoDadosCertame_Data, "99/99/99"));
            AV54TFContratoDadosCertame_Data_To = context.localUtil.CToD( Ddo_contratodadoscertame_data_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContratoDadosCertame_Data_To", context.localUtil.Format(AV54TFContratoDadosCertame_Data_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E147G2( )
      {
         /* Ddo_contratodadoscertame_modalidade_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratodadoscertame_modalidade_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratodadoscertame_modalidade_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_modalidade_Internalname, "SortedStatus", Ddo_contratodadoscertame_modalidade_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratodadoscertame_modalidade_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratodadoscertame_modalidade_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_modalidade_Internalname, "SortedStatus", Ddo_contratodadoscertame_modalidade_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratodadoscertame_modalidade_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV59TFContratoDadosCertame_Modalidade = Ddo_contratodadoscertame_modalidade_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContratoDadosCertame_Modalidade", AV59TFContratoDadosCertame_Modalidade);
            AV60TFContratoDadosCertame_Modalidade_Sel = Ddo_contratodadoscertame_modalidade_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratoDadosCertame_Modalidade_Sel", AV60TFContratoDadosCertame_Modalidade_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E157G2( )
      {
         /* Ddo_contratodadoscertame_numero_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratodadoscertame_numero_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratodadoscertame_numero_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_numero_Internalname, "SortedStatus", Ddo_contratodadoscertame_numero_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratodadoscertame_numero_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratodadoscertame_numero_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_numero_Internalname, "SortedStatus", Ddo_contratodadoscertame_numero_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratodadoscertame_numero_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV63TFContratoDadosCertame_Numero = (long)(NumberUtil.Val( Ddo_contratodadoscertame_numero_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFContratoDadosCertame_Numero", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63TFContratoDadosCertame_Numero), 10, 0)));
            AV64TFContratoDadosCertame_Numero_To = (long)(NumberUtil.Val( Ddo_contratodadoscertame_numero_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFContratoDadosCertame_Numero_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV64TFContratoDadosCertame_Numero_To), 10, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E167G2( )
      {
         /* Ddo_contratodadoscertame_site_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratodadoscertame_site_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratodadoscertame_site_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_site_Internalname, "SortedStatus", Ddo_contratodadoscertame_site_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratodadoscertame_site_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratodadoscertame_site_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_site_Internalname, "SortedStatus", Ddo_contratodadoscertame_site_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratodadoscertame_site_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV67TFContratoDadosCertame_Site = Ddo_contratodadoscertame_site_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFContratoDadosCertame_Site", AV67TFContratoDadosCertame_Site);
            AV68TFContratoDadosCertame_Site_Sel = Ddo_contratodadoscertame_site_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFContratoDadosCertame_Site_Sel", AV68TFContratoDadosCertame_Site_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E177G2( )
      {
         /* Ddo_contratodadoscertame_uasg_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratodadoscertame_uasg_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratodadoscertame_uasg_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_uasg_Internalname, "SortedStatus", Ddo_contratodadoscertame_uasg_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratodadoscertame_uasg_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratodadoscertame_uasg_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_uasg_Internalname, "SortedStatus", Ddo_contratodadoscertame_uasg_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratodadoscertame_uasg_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV71TFContratoDadosCertame_Uasg = (short)(NumberUtil.Val( Ddo_contratodadoscertame_uasg_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContratoDadosCertame_Uasg", StringUtil.LTrim( StringUtil.Str( (decimal)(AV71TFContratoDadosCertame_Uasg), 4, 0)));
            AV72TFContratoDadosCertame_Uasg_To = (short)(NumberUtil.Val( Ddo_contratodadoscertame_uasg_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFContratoDadosCertame_Uasg_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV72TFContratoDadosCertame_Uasg_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E187G2( )
      {
         /* Ddo_contratodadoscertame_datahomologacao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratodadoscertame_datahomologacao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratodadoscertame_datahomologacao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_datahomologacao_Internalname, "SortedStatus", Ddo_contratodadoscertame_datahomologacao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratodadoscertame_datahomologacao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratodadoscertame_datahomologacao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_datahomologacao_Internalname, "SortedStatus", Ddo_contratodadoscertame_datahomologacao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratodadoscertame_datahomologacao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV75TFContratoDadosCertame_DataHomologacao = context.localUtil.CToD( Ddo_contratodadoscertame_datahomologacao_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFContratoDadosCertame_DataHomologacao", context.localUtil.Format(AV75TFContratoDadosCertame_DataHomologacao, "99/99/99"));
            AV76TFContratoDadosCertame_DataHomologacao_To = context.localUtil.CToD( Ddo_contratodadoscertame_datahomologacao_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFContratoDadosCertame_DataHomologacao_To", context.localUtil.Format(AV76TFContratoDadosCertame_DataHomologacao_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E197G2( )
      {
         /* Ddo_contratodadoscertame_dataadjudicacao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratodadoscertame_dataadjudicacao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratodadoscertame_dataadjudicacao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_dataadjudicacao_Internalname, "SortedStatus", Ddo_contratodadoscertame_dataadjudicacao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratodadoscertame_dataadjudicacao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratodadoscertame_dataadjudicacao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_dataadjudicacao_Internalname, "SortedStatus", Ddo_contratodadoscertame_dataadjudicacao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratodadoscertame_dataadjudicacao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV81TFContratoDadosCertame_DataAdjudicacao = context.localUtil.CToD( Ddo_contratodadoscertame_dataadjudicacao_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFContratoDadosCertame_DataAdjudicacao", context.localUtil.Format(AV81TFContratoDadosCertame_DataAdjudicacao, "99/99/99"));
            AV82TFContratoDadosCertame_DataAdjudicacao_To = context.localUtil.CToD( Ddo_contratodadoscertame_dataadjudicacao_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFContratoDadosCertame_DataAdjudicacao_To", context.localUtil.Format(AV82TFContratoDadosCertame_DataAdjudicacao_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E337G2( )
      {
         /* Grid_Load Routine */
         AV28Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV28Update);
         AV128Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("contratodadoscertame.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A314ContratoDadosCertame_Codigo) + "," + UrlEncode("" +A74Contrato_Codigo);
         AV29Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV29Delete);
         AV129Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("contratodadoscertame.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A314ContratoDadosCertame_Codigo) + "," + UrlEncode("" +A74Contrato_Codigo);
         edtContratoDadosCertame_Modalidade_Link = formatLink("viewcontratodadoscertame.aspx") + "?" + UrlEncode("" +A314ContratoDadosCertame_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtContratoDadosCertame_Site_Linktarget = "_blank";
         edtContratoDadosCertame_Site_Link = A309ContratoDadosCertame_Site;
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 115;
         }
         sendrow_1152( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_115_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(115, GridRow);
         }
      }

      protected void E207G2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E267G2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E217G2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV37ContratoDadosCertame_Data1, AV38ContratoDadosCertame_Data_To1, AV17ContratoDadosCertame_Modalidade1, AV39ContratoDadosCertame_Numero1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV40ContratoDadosCertame_Data2, AV41ContratoDadosCertame_Data_To2, AV21ContratoDadosCertame_Modalidade2, AV42ContratoDadosCertame_Numero2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV43ContratoDadosCertame_Data3, AV44ContratoDadosCertame_Data_To3, AV25ContratoDadosCertame_Modalidade3, AV45ContratoDadosCertame_Numero3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV49TFContrato_Numero, AV50TFContrato_Numero_Sel, AV53TFContratoDadosCertame_Data, AV54TFContratoDadosCertame_Data_To, AV59TFContratoDadosCertame_Modalidade, AV60TFContratoDadosCertame_Modalidade_Sel, AV63TFContratoDadosCertame_Numero, AV64TFContratoDadosCertame_Numero_To, AV67TFContratoDadosCertame_Site, AV68TFContratoDadosCertame_Site_Sel, AV71TFContratoDadosCertame_Uasg, AV72TFContratoDadosCertame_Uasg_To, AV75TFContratoDadosCertame_DataHomologacao, AV76TFContratoDadosCertame_DataHomologacao_To, AV81TFContratoDadosCertame_DataAdjudicacao, AV82TFContratoDadosCertame_DataAdjudicacao_To, AV51ddo_Contrato_NumeroTitleControlIdToReplace, AV57ddo_ContratoDadosCertame_DataTitleControlIdToReplace, AV61ddo_ContratoDadosCertame_ModalidadeTitleControlIdToReplace, AV65ddo_ContratoDadosCertame_NumeroTitleControlIdToReplace, AV69ddo_ContratoDadosCertame_SiteTitleControlIdToReplace, AV73ddo_ContratoDadosCertame_UasgTitleControlIdToReplace, AV79ddo_ContratoDadosCertame_DataHomologacaoTitleControlIdToReplace, AV85ddo_ContratoDadosCertame_DataAdjudicacaoTitleControlIdToReplace, AV130Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A314ContratoDadosCertame_Codigo, A74Contrato_Codigo, A309ContratoDadosCertame_Site) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E277G2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E287G2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E227G2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV37ContratoDadosCertame_Data1, AV38ContratoDadosCertame_Data_To1, AV17ContratoDadosCertame_Modalidade1, AV39ContratoDadosCertame_Numero1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV40ContratoDadosCertame_Data2, AV41ContratoDadosCertame_Data_To2, AV21ContratoDadosCertame_Modalidade2, AV42ContratoDadosCertame_Numero2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV43ContratoDadosCertame_Data3, AV44ContratoDadosCertame_Data_To3, AV25ContratoDadosCertame_Modalidade3, AV45ContratoDadosCertame_Numero3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV49TFContrato_Numero, AV50TFContrato_Numero_Sel, AV53TFContratoDadosCertame_Data, AV54TFContratoDadosCertame_Data_To, AV59TFContratoDadosCertame_Modalidade, AV60TFContratoDadosCertame_Modalidade_Sel, AV63TFContratoDadosCertame_Numero, AV64TFContratoDadosCertame_Numero_To, AV67TFContratoDadosCertame_Site, AV68TFContratoDadosCertame_Site_Sel, AV71TFContratoDadosCertame_Uasg, AV72TFContratoDadosCertame_Uasg_To, AV75TFContratoDadosCertame_DataHomologacao, AV76TFContratoDadosCertame_DataHomologacao_To, AV81TFContratoDadosCertame_DataAdjudicacao, AV82TFContratoDadosCertame_DataAdjudicacao_To, AV51ddo_Contrato_NumeroTitleControlIdToReplace, AV57ddo_ContratoDadosCertame_DataTitleControlIdToReplace, AV61ddo_ContratoDadosCertame_ModalidadeTitleControlIdToReplace, AV65ddo_ContratoDadosCertame_NumeroTitleControlIdToReplace, AV69ddo_ContratoDadosCertame_SiteTitleControlIdToReplace, AV73ddo_ContratoDadosCertame_UasgTitleControlIdToReplace, AV79ddo_ContratoDadosCertame_DataHomologacaoTitleControlIdToReplace, AV85ddo_ContratoDadosCertame_DataAdjudicacaoTitleControlIdToReplace, AV130Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A314ContratoDadosCertame_Codigo, A74Contrato_Codigo, A309ContratoDadosCertame_Site) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E297G2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         AV20DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E237G2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV37ContratoDadosCertame_Data1, AV38ContratoDadosCertame_Data_To1, AV17ContratoDadosCertame_Modalidade1, AV39ContratoDadosCertame_Numero1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV40ContratoDadosCertame_Data2, AV41ContratoDadosCertame_Data_To2, AV21ContratoDadosCertame_Modalidade2, AV42ContratoDadosCertame_Numero2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV43ContratoDadosCertame_Data3, AV44ContratoDadosCertame_Data_To3, AV25ContratoDadosCertame_Modalidade3, AV45ContratoDadosCertame_Numero3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV49TFContrato_Numero, AV50TFContrato_Numero_Sel, AV53TFContratoDadosCertame_Data, AV54TFContratoDadosCertame_Data_To, AV59TFContratoDadosCertame_Modalidade, AV60TFContratoDadosCertame_Modalidade_Sel, AV63TFContratoDadosCertame_Numero, AV64TFContratoDadosCertame_Numero_To, AV67TFContratoDadosCertame_Site, AV68TFContratoDadosCertame_Site_Sel, AV71TFContratoDadosCertame_Uasg, AV72TFContratoDadosCertame_Uasg_To, AV75TFContratoDadosCertame_DataHomologacao, AV76TFContratoDadosCertame_DataHomologacao_To, AV81TFContratoDadosCertame_DataAdjudicacao, AV82TFContratoDadosCertame_DataAdjudicacao_To, AV51ddo_Contrato_NumeroTitleControlIdToReplace, AV57ddo_ContratoDadosCertame_DataTitleControlIdToReplace, AV61ddo_ContratoDadosCertame_ModalidadeTitleControlIdToReplace, AV65ddo_ContratoDadosCertame_NumeroTitleControlIdToReplace, AV69ddo_ContratoDadosCertame_SiteTitleControlIdToReplace, AV73ddo_ContratoDadosCertame_UasgTitleControlIdToReplace, AV79ddo_ContratoDadosCertame_DataHomologacaoTitleControlIdToReplace, AV85ddo_ContratoDadosCertame_DataAdjudicacaoTitleControlIdToReplace, AV130Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A314ContratoDadosCertame_Codigo, A74Contrato_Codigo, A309ContratoDadosCertame_Site) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E307G2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         AV24DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E247G2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E257G2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("contratodadoscertame.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S182( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contrato_numero_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
         Ddo_contratodadoscertame_data_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_data_Internalname, "SortedStatus", Ddo_contratodadoscertame_data_Sortedstatus);
         Ddo_contratodadoscertame_modalidade_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_modalidade_Internalname, "SortedStatus", Ddo_contratodadoscertame_modalidade_Sortedstatus);
         Ddo_contratodadoscertame_numero_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_numero_Internalname, "SortedStatus", Ddo_contratodadoscertame_numero_Sortedstatus);
         Ddo_contratodadoscertame_site_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_site_Internalname, "SortedStatus", Ddo_contratodadoscertame_site_Sortedstatus);
         Ddo_contratodadoscertame_uasg_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_uasg_Internalname, "SortedStatus", Ddo_contratodadoscertame_uasg_Sortedstatus);
         Ddo_contratodadoscertame_datahomologacao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_datahomologacao_Internalname, "SortedStatus", Ddo_contratodadoscertame_datahomologacao_Sortedstatus);
         Ddo_contratodadoscertame_dataadjudicacao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_dataadjudicacao_Internalname, "SortedStatus", Ddo_contratodadoscertame_dataadjudicacao_Sortedstatus);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 4 )
         {
            Ddo_contrato_numero_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_contratodadoscertame_data_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_data_Internalname, "SortedStatus", Ddo_contratodadoscertame_data_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_contratodadoscertame_modalidade_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_modalidade_Internalname, "SortedStatus", Ddo_contratodadoscertame_modalidade_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_contratodadoscertame_numero_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_numero_Internalname, "SortedStatus", Ddo_contratodadoscertame_numero_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_contratodadoscertame_site_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_site_Internalname, "SortedStatus", Ddo_contratodadoscertame_site_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_contratodadoscertame_uasg_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_uasg_Internalname, "SortedStatus", Ddo_contratodadoscertame_uasg_Sortedstatus);
         }
         else if ( AV13OrderedBy == 7 )
         {
            Ddo_contratodadoscertame_datahomologacao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_datahomologacao_Internalname, "SortedStatus", Ddo_contratodadoscertame_datahomologacao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 8 )
         {
            Ddo_contratodadoscertame_dataadjudicacao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_dataadjudicacao_Internalname, "SortedStatus", Ddo_contratodadoscertame_dataadjudicacao_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         tblTablemergeddynamicfilterscontratodadoscertame_data1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratodadoscertame_data1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratodadoscertame_data1_Visible), 5, 0)));
         edtavContratodadoscertame_modalidade1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratodadoscertame_modalidade1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratodadoscertame_modalidade1_Visible), 5, 0)));
         edtavContratodadoscertame_numero1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratodadoscertame_numero1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratodadoscertame_numero1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATODADOSCERTAME_DATA") == 0 )
         {
            tblTablemergeddynamicfilterscontratodadoscertame_data1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratodadoscertame_data1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratodadoscertame_data1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATODADOSCERTAME_MODALIDADE") == 0 )
         {
            edtavContratodadoscertame_modalidade1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratodadoscertame_modalidade1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratodadoscertame_modalidade1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATODADOSCERTAME_NUMERO") == 0 )
         {
            edtavContratodadoscertame_numero1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratodadoscertame_numero1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratodadoscertame_numero1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         tblTablemergeddynamicfilterscontratodadoscertame_data2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratodadoscertame_data2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratodadoscertame_data2_Visible), 5, 0)));
         edtavContratodadoscertame_modalidade2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratodadoscertame_modalidade2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratodadoscertame_modalidade2_Visible), 5, 0)));
         edtavContratodadoscertame_numero2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratodadoscertame_numero2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratodadoscertame_numero2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATODADOSCERTAME_DATA") == 0 )
         {
            tblTablemergeddynamicfilterscontratodadoscertame_data2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratodadoscertame_data2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratodadoscertame_data2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATODADOSCERTAME_MODALIDADE") == 0 )
         {
            edtavContratodadoscertame_modalidade2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratodadoscertame_modalidade2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratodadoscertame_modalidade2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATODADOSCERTAME_NUMERO") == 0 )
         {
            edtavContratodadoscertame_numero2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratodadoscertame_numero2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratodadoscertame_numero2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         tblTablemergeddynamicfilterscontratodadoscertame_data3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratodadoscertame_data3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratodadoscertame_data3_Visible), 5, 0)));
         edtavContratodadoscertame_modalidade3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratodadoscertame_modalidade3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratodadoscertame_modalidade3_Visible), 5, 0)));
         edtavContratodadoscertame_numero3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratodadoscertame_numero3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratodadoscertame_numero3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATODADOSCERTAME_DATA") == 0 )
         {
            tblTablemergeddynamicfilterscontratodadoscertame_data3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratodadoscertame_data3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratodadoscertame_data3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATODADOSCERTAME_MODALIDADE") == 0 )
         {
            edtavContratodadoscertame_modalidade3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratodadoscertame_modalidade3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratodadoscertame_modalidade3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATODADOSCERTAME_NUMERO") == 0 )
         {
            edtavContratodadoscertame_numero3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratodadoscertame_numero3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratodadoscertame_numero3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "CONTRATODADOSCERTAME_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV40ContratoDadosCertame_Data2 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ContratoDadosCertame_Data2", context.localUtil.Format(AV40ContratoDadosCertame_Data2, "99/99/99"));
         AV41ContratoDadosCertame_Data_To2 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ContratoDadosCertame_Data_To2", context.localUtil.Format(AV41ContratoDadosCertame_Data_To2, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "CONTRATODADOSCERTAME_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV43ContratoDadosCertame_Data3 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ContratoDadosCertame_Data3", context.localUtil.Format(AV43ContratoDadosCertame_Data3, "99/99/99"));
         AV44ContratoDadosCertame_Data_To3 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ContratoDadosCertame_Data_To3", context.localUtil.Format(AV44ContratoDadosCertame_Data_To3, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'CLEANFILTERS' Routine */
         AV49TFContrato_Numero = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContrato_Numero", AV49TFContrato_Numero);
         Ddo_contrato_numero_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "FilteredText_set", Ddo_contrato_numero_Filteredtext_set);
         AV50TFContrato_Numero_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContrato_Numero_Sel", AV50TFContrato_Numero_Sel);
         Ddo_contrato_numero_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SelectedValue_set", Ddo_contrato_numero_Selectedvalue_set);
         AV53TFContratoDadosCertame_Data = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContratoDadosCertame_Data", context.localUtil.Format(AV53TFContratoDadosCertame_Data, "99/99/99"));
         Ddo_contratodadoscertame_data_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_data_Internalname, "FilteredText_set", Ddo_contratodadoscertame_data_Filteredtext_set);
         AV54TFContratoDadosCertame_Data_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContratoDadosCertame_Data_To", context.localUtil.Format(AV54TFContratoDadosCertame_Data_To, "99/99/99"));
         Ddo_contratodadoscertame_data_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_data_Internalname, "FilteredTextTo_set", Ddo_contratodadoscertame_data_Filteredtextto_set);
         AV59TFContratoDadosCertame_Modalidade = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContratoDadosCertame_Modalidade", AV59TFContratoDadosCertame_Modalidade);
         Ddo_contratodadoscertame_modalidade_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_modalidade_Internalname, "FilteredText_set", Ddo_contratodadoscertame_modalidade_Filteredtext_set);
         AV60TFContratoDadosCertame_Modalidade_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratoDadosCertame_Modalidade_Sel", AV60TFContratoDadosCertame_Modalidade_Sel);
         Ddo_contratodadoscertame_modalidade_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_modalidade_Internalname, "SelectedValue_set", Ddo_contratodadoscertame_modalidade_Selectedvalue_set);
         AV63TFContratoDadosCertame_Numero = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFContratoDadosCertame_Numero", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63TFContratoDadosCertame_Numero), 10, 0)));
         Ddo_contratodadoscertame_numero_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_numero_Internalname, "FilteredText_set", Ddo_contratodadoscertame_numero_Filteredtext_set);
         AV64TFContratoDadosCertame_Numero_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFContratoDadosCertame_Numero_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV64TFContratoDadosCertame_Numero_To), 10, 0)));
         Ddo_contratodadoscertame_numero_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_numero_Internalname, "FilteredTextTo_set", Ddo_contratodadoscertame_numero_Filteredtextto_set);
         AV67TFContratoDadosCertame_Site = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFContratoDadosCertame_Site", AV67TFContratoDadosCertame_Site);
         Ddo_contratodadoscertame_site_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_site_Internalname, "FilteredText_set", Ddo_contratodadoscertame_site_Filteredtext_set);
         AV68TFContratoDadosCertame_Site_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFContratoDadosCertame_Site_Sel", AV68TFContratoDadosCertame_Site_Sel);
         Ddo_contratodadoscertame_site_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_site_Internalname, "SelectedValue_set", Ddo_contratodadoscertame_site_Selectedvalue_set);
         AV71TFContratoDadosCertame_Uasg = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContratoDadosCertame_Uasg", StringUtil.LTrim( StringUtil.Str( (decimal)(AV71TFContratoDadosCertame_Uasg), 4, 0)));
         Ddo_contratodadoscertame_uasg_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_uasg_Internalname, "FilteredText_set", Ddo_contratodadoscertame_uasg_Filteredtext_set);
         AV72TFContratoDadosCertame_Uasg_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFContratoDadosCertame_Uasg_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV72TFContratoDadosCertame_Uasg_To), 4, 0)));
         Ddo_contratodadoscertame_uasg_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_uasg_Internalname, "FilteredTextTo_set", Ddo_contratodadoscertame_uasg_Filteredtextto_set);
         AV75TFContratoDadosCertame_DataHomologacao = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFContratoDadosCertame_DataHomologacao", context.localUtil.Format(AV75TFContratoDadosCertame_DataHomologacao, "99/99/99"));
         Ddo_contratodadoscertame_datahomologacao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_datahomologacao_Internalname, "FilteredText_set", Ddo_contratodadoscertame_datahomologacao_Filteredtext_set);
         AV76TFContratoDadosCertame_DataHomologacao_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFContratoDadosCertame_DataHomologacao_To", context.localUtil.Format(AV76TFContratoDadosCertame_DataHomologacao_To, "99/99/99"));
         Ddo_contratodadoscertame_datahomologacao_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_datahomologacao_Internalname, "FilteredTextTo_set", Ddo_contratodadoscertame_datahomologacao_Filteredtextto_set);
         AV81TFContratoDadosCertame_DataAdjudicacao = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFContratoDadosCertame_DataAdjudicacao", context.localUtil.Format(AV81TFContratoDadosCertame_DataAdjudicacao, "99/99/99"));
         Ddo_contratodadoscertame_dataadjudicacao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_dataadjudicacao_Internalname, "FilteredText_set", Ddo_contratodadoscertame_dataadjudicacao_Filteredtext_set);
         AV82TFContratoDadosCertame_DataAdjudicacao_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFContratoDadosCertame_DataAdjudicacao_To", context.localUtil.Format(AV82TFContratoDadosCertame_DataAdjudicacao_To, "99/99/99"));
         Ddo_contratodadoscertame_dataadjudicacao_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_dataadjudicacao_Internalname, "FilteredTextTo_set", Ddo_contratodadoscertame_dataadjudicacao_Filteredtextto_set);
         AV15DynamicFiltersSelector1 = "CONTRATODADOSCERTAME_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV37ContratoDadosCertame_Data1 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ContratoDadosCertame_Data1", context.localUtil.Format(AV37ContratoDadosCertame_Data1, "99/99/99"));
         AV38ContratoDadosCertame_Data_To1 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ContratoDadosCertame_Data_To1", context.localUtil.Format(AV38ContratoDadosCertame_Data_To1, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV30Session.Get(AV130Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV130Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV30Session.Get(AV130Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S232( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV131GXV1 = 1;
         while ( AV131GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV131GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO") == 0 )
            {
               AV49TFContrato_Numero = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContrato_Numero", AV49TFContrato_Numero);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFContrato_Numero)) )
               {
                  Ddo_contrato_numero_Filteredtext_set = AV49TFContrato_Numero;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "FilteredText_set", Ddo_contrato_numero_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO_SEL") == 0 )
            {
               AV50TFContrato_Numero_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContrato_Numero_Sel", AV50TFContrato_Numero_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50TFContrato_Numero_Sel)) )
               {
                  Ddo_contrato_numero_Selectedvalue_set = AV50TFContrato_Numero_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SelectedValue_set", Ddo_contrato_numero_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATODADOSCERTAME_DATA") == 0 )
            {
               AV53TFContratoDadosCertame_Data = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContratoDadosCertame_Data", context.localUtil.Format(AV53TFContratoDadosCertame_Data, "99/99/99"));
               AV54TFContratoDadosCertame_Data_To = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContratoDadosCertame_Data_To", context.localUtil.Format(AV54TFContratoDadosCertame_Data_To, "99/99/99"));
               if ( ! (DateTime.MinValue==AV53TFContratoDadosCertame_Data) )
               {
                  Ddo_contratodadoscertame_data_Filteredtext_set = context.localUtil.DToC( AV53TFContratoDadosCertame_Data, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_data_Internalname, "FilteredText_set", Ddo_contratodadoscertame_data_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV54TFContratoDadosCertame_Data_To) )
               {
                  Ddo_contratodadoscertame_data_Filteredtextto_set = context.localUtil.DToC( AV54TFContratoDadosCertame_Data_To, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_data_Internalname, "FilteredTextTo_set", Ddo_contratodadoscertame_data_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATODADOSCERTAME_MODALIDADE") == 0 )
            {
               AV59TFContratoDadosCertame_Modalidade = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContratoDadosCertame_Modalidade", AV59TFContratoDadosCertame_Modalidade);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59TFContratoDadosCertame_Modalidade)) )
               {
                  Ddo_contratodadoscertame_modalidade_Filteredtext_set = AV59TFContratoDadosCertame_Modalidade;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_modalidade_Internalname, "FilteredText_set", Ddo_contratodadoscertame_modalidade_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATODADOSCERTAME_MODALIDADE_SEL") == 0 )
            {
               AV60TFContratoDadosCertame_Modalidade_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratoDadosCertame_Modalidade_Sel", AV60TFContratoDadosCertame_Modalidade_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFContratoDadosCertame_Modalidade_Sel)) )
               {
                  Ddo_contratodadoscertame_modalidade_Selectedvalue_set = AV60TFContratoDadosCertame_Modalidade_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_modalidade_Internalname, "SelectedValue_set", Ddo_contratodadoscertame_modalidade_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATODADOSCERTAME_NUMERO") == 0 )
            {
               AV63TFContratoDadosCertame_Numero = (long)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFContratoDadosCertame_Numero", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63TFContratoDadosCertame_Numero), 10, 0)));
               AV64TFContratoDadosCertame_Numero_To = (long)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFContratoDadosCertame_Numero_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV64TFContratoDadosCertame_Numero_To), 10, 0)));
               if ( ! (0==AV63TFContratoDadosCertame_Numero) )
               {
                  Ddo_contratodadoscertame_numero_Filteredtext_set = StringUtil.Str( (decimal)(AV63TFContratoDadosCertame_Numero), 10, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_numero_Internalname, "FilteredText_set", Ddo_contratodadoscertame_numero_Filteredtext_set);
               }
               if ( ! (0==AV64TFContratoDadosCertame_Numero_To) )
               {
                  Ddo_contratodadoscertame_numero_Filteredtextto_set = StringUtil.Str( (decimal)(AV64TFContratoDadosCertame_Numero_To), 10, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_numero_Internalname, "FilteredTextTo_set", Ddo_contratodadoscertame_numero_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATODADOSCERTAME_SITE") == 0 )
            {
               AV67TFContratoDadosCertame_Site = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFContratoDadosCertame_Site", AV67TFContratoDadosCertame_Site);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67TFContratoDadosCertame_Site)) )
               {
                  Ddo_contratodadoscertame_site_Filteredtext_set = AV67TFContratoDadosCertame_Site;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_site_Internalname, "FilteredText_set", Ddo_contratodadoscertame_site_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATODADOSCERTAME_SITE_SEL") == 0 )
            {
               AV68TFContratoDadosCertame_Site_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFContratoDadosCertame_Site_Sel", AV68TFContratoDadosCertame_Site_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68TFContratoDadosCertame_Site_Sel)) )
               {
                  Ddo_contratodadoscertame_site_Selectedvalue_set = AV68TFContratoDadosCertame_Site_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_site_Internalname, "SelectedValue_set", Ddo_contratodadoscertame_site_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATODADOSCERTAME_UASG") == 0 )
            {
               AV71TFContratoDadosCertame_Uasg = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContratoDadosCertame_Uasg", StringUtil.LTrim( StringUtil.Str( (decimal)(AV71TFContratoDadosCertame_Uasg), 4, 0)));
               AV72TFContratoDadosCertame_Uasg_To = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFContratoDadosCertame_Uasg_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV72TFContratoDadosCertame_Uasg_To), 4, 0)));
               if ( ! (0==AV71TFContratoDadosCertame_Uasg) )
               {
                  Ddo_contratodadoscertame_uasg_Filteredtext_set = StringUtil.Str( (decimal)(AV71TFContratoDadosCertame_Uasg), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_uasg_Internalname, "FilteredText_set", Ddo_contratodadoscertame_uasg_Filteredtext_set);
               }
               if ( ! (0==AV72TFContratoDadosCertame_Uasg_To) )
               {
                  Ddo_contratodadoscertame_uasg_Filteredtextto_set = StringUtil.Str( (decimal)(AV72TFContratoDadosCertame_Uasg_To), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_uasg_Internalname, "FilteredTextTo_set", Ddo_contratodadoscertame_uasg_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATODADOSCERTAME_DATAHOMOLOGACAO") == 0 )
            {
               AV75TFContratoDadosCertame_DataHomologacao = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFContratoDadosCertame_DataHomologacao", context.localUtil.Format(AV75TFContratoDadosCertame_DataHomologacao, "99/99/99"));
               AV76TFContratoDadosCertame_DataHomologacao_To = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFContratoDadosCertame_DataHomologacao_To", context.localUtil.Format(AV76TFContratoDadosCertame_DataHomologacao_To, "99/99/99"));
               if ( ! (DateTime.MinValue==AV75TFContratoDadosCertame_DataHomologacao) )
               {
                  Ddo_contratodadoscertame_datahomologacao_Filteredtext_set = context.localUtil.DToC( AV75TFContratoDadosCertame_DataHomologacao, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_datahomologacao_Internalname, "FilteredText_set", Ddo_contratodadoscertame_datahomologacao_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV76TFContratoDadosCertame_DataHomologacao_To) )
               {
                  Ddo_contratodadoscertame_datahomologacao_Filteredtextto_set = context.localUtil.DToC( AV76TFContratoDadosCertame_DataHomologacao_To, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_datahomologacao_Internalname, "FilteredTextTo_set", Ddo_contratodadoscertame_datahomologacao_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATODADOSCERTAME_DATAADJUDICACAO") == 0 )
            {
               AV81TFContratoDadosCertame_DataAdjudicacao = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFContratoDadosCertame_DataAdjudicacao", context.localUtil.Format(AV81TFContratoDadosCertame_DataAdjudicacao, "99/99/99"));
               AV82TFContratoDadosCertame_DataAdjudicacao_To = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFContratoDadosCertame_DataAdjudicacao_To", context.localUtil.Format(AV82TFContratoDadosCertame_DataAdjudicacao_To, "99/99/99"));
               if ( ! (DateTime.MinValue==AV81TFContratoDadosCertame_DataAdjudicacao) )
               {
                  Ddo_contratodadoscertame_dataadjudicacao_Filteredtext_set = context.localUtil.DToC( AV81TFContratoDadosCertame_DataAdjudicacao, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_dataadjudicacao_Internalname, "FilteredText_set", Ddo_contratodadoscertame_dataadjudicacao_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV82TFContratoDadosCertame_DataAdjudicacao_To) )
               {
                  Ddo_contratodadoscertame_dataadjudicacao_Filteredtextto_set = context.localUtil.DToC( AV82TFContratoDadosCertame_DataAdjudicacao_To, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratodadoscertame_dataadjudicacao_Internalname, "FilteredTextTo_set", Ddo_contratodadoscertame_dataadjudicacao_Filteredtextto_set);
               }
            }
            AV131GXV1 = (int)(AV131GXV1+1);
         }
      }

      protected void S212( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATODADOSCERTAME_DATA") == 0 )
            {
               AV37ContratoDadosCertame_Data1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ContratoDadosCertame_Data1", context.localUtil.Format(AV37ContratoDadosCertame_Data1, "99/99/99"));
               AV38ContratoDadosCertame_Data_To1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ContratoDadosCertame_Data_To1", context.localUtil.Format(AV38ContratoDadosCertame_Data_To1, "99/99/99"));
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATODADOSCERTAME_MODALIDADE") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17ContratoDadosCertame_Modalidade1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoDadosCertame_Modalidade1", AV17ContratoDadosCertame_Modalidade1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATODADOSCERTAME_NUMERO") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV39ContratoDadosCertame_Numero1 = (long)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ContratoDadosCertame_Numero1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39ContratoDadosCertame_Numero1), 10, 0)));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATODADOSCERTAME_DATA") == 0 )
               {
                  AV40ContratoDadosCertame_Data2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ContratoDadosCertame_Data2", context.localUtil.Format(AV40ContratoDadosCertame_Data2, "99/99/99"));
                  AV41ContratoDadosCertame_Data_To2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ContratoDadosCertame_Data_To2", context.localUtil.Format(AV41ContratoDadosCertame_Data_To2, "99/99/99"));
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATODADOSCERTAME_MODALIDADE") == 0 )
               {
                  AV20DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
                  AV21ContratoDadosCertame_Modalidade2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoDadosCertame_Modalidade2", AV21ContratoDadosCertame_Modalidade2);
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATODADOSCERTAME_NUMERO") == 0 )
               {
                  AV20DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
                  AV42ContratoDadosCertame_Numero2 = (long)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ContratoDadosCertame_Numero2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42ContratoDadosCertame_Numero2), 10, 0)));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATODADOSCERTAME_DATA") == 0 )
                  {
                     AV43ContratoDadosCertame_Data3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ContratoDadosCertame_Data3", context.localUtil.Format(AV43ContratoDadosCertame_Data3, "99/99/99"));
                     AV44ContratoDadosCertame_Data_To3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ContratoDadosCertame_Data_To3", context.localUtil.Format(AV44ContratoDadosCertame_Data_To3, "99/99/99"));
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATODADOSCERTAME_MODALIDADE") == 0 )
                  {
                     AV24DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
                     AV25ContratoDadosCertame_Modalidade3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoDadosCertame_Modalidade3", AV25ContratoDadosCertame_Modalidade3);
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATODADOSCERTAME_NUMERO") == 0 )
                  {
                     AV24DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
                     AV45ContratoDadosCertame_Numero3 = (long)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ContratoDadosCertame_Numero3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45ContratoDadosCertame_Numero3), 10, 0)));
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV30Session.Get(AV130Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFContrato_Numero)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATO_NUMERO";
            AV11GridStateFilterValue.gxTpr_Value = AV49TFContrato_Numero;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50TFContrato_Numero_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATO_NUMERO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV50TFContrato_Numero_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV53TFContratoDadosCertame_Data) && (DateTime.MinValue==AV54TFContratoDadosCertame_Data_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATODADOSCERTAME_DATA";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV53TFContratoDadosCertame_Data, 2, "/");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV54TFContratoDadosCertame_Data_To, 2, "/");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59TFContratoDadosCertame_Modalidade)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATODADOSCERTAME_MODALIDADE";
            AV11GridStateFilterValue.gxTpr_Value = AV59TFContratoDadosCertame_Modalidade;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFContratoDadosCertame_Modalidade_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATODADOSCERTAME_MODALIDADE_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV60TFContratoDadosCertame_Modalidade_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV63TFContratoDadosCertame_Numero) && (0==AV64TFContratoDadosCertame_Numero_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATODADOSCERTAME_NUMERO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV63TFContratoDadosCertame_Numero), 10, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV64TFContratoDadosCertame_Numero_To), 10, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67TFContratoDadosCertame_Site)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATODADOSCERTAME_SITE";
            AV11GridStateFilterValue.gxTpr_Value = AV67TFContratoDadosCertame_Site;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68TFContratoDadosCertame_Site_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATODADOSCERTAME_SITE_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV68TFContratoDadosCertame_Site_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV71TFContratoDadosCertame_Uasg) && (0==AV72TFContratoDadosCertame_Uasg_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATODADOSCERTAME_UASG";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV71TFContratoDadosCertame_Uasg), 4, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV72TFContratoDadosCertame_Uasg_To), 4, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV75TFContratoDadosCertame_DataHomologacao) && (DateTime.MinValue==AV76TFContratoDadosCertame_DataHomologacao_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATODADOSCERTAME_DATAHOMOLOGACAO";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV75TFContratoDadosCertame_DataHomologacao, 2, "/");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV76TFContratoDadosCertame_DataHomologacao_To, 2, "/");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV81TFContratoDadosCertame_DataAdjudicacao) && (DateTime.MinValue==AV82TFContratoDadosCertame_DataAdjudicacao_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATODADOSCERTAME_DATAADJUDICACAO";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV81TFContratoDadosCertame_DataAdjudicacao, 2, "/");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV82TFContratoDadosCertame_DataAdjudicacao_To, 2, "/");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV130Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S192( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATODADOSCERTAME_DATA") == 0 ) && ! ( (DateTime.MinValue==AV37ContratoDadosCertame_Data1) && (DateTime.MinValue==AV38ContratoDadosCertame_Data_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV37ContratoDadosCertame_Data1, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV38ContratoDadosCertame_Data_To1, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17ContratoDadosCertame_Modalidade1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17ContratoDadosCertame_Modalidade1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ! (0==AV39ContratoDadosCertame_Numero1) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV39ContratoDadosCertame_Numero1), 10, 0);
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATODADOSCERTAME_DATA") == 0 ) && ! ( (DateTime.MinValue==AV40ContratoDadosCertame_Data2) && (DateTime.MinValue==AV41ContratoDadosCertame_Data_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV40ContratoDadosCertame_Data2, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV41ContratoDadosCertame_Data_To2, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV21ContratoDadosCertame_Modalidade2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV21ContratoDadosCertame_Modalidade2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV20DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ! (0==AV42ContratoDadosCertame_Numero2) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV42ContratoDadosCertame_Numero2), 10, 0);
               AV12GridStateDynamicFilter.gxTpr_Operator = AV20DynamicFiltersOperator2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATODADOSCERTAME_DATA") == 0 ) && ! ( (DateTime.MinValue==AV43ContratoDadosCertame_Data3) && (DateTime.MinValue==AV44ContratoDadosCertame_Data_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV43ContratoDadosCertame_Data3, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV44ContratoDadosCertame_Data_To3, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ContratoDadosCertame_Modalidade3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV25ContratoDadosCertame_Modalidade3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV24DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ! (0==AV45ContratoDadosCertame_Numero3) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV45ContratoDadosCertame_Numero3), 10, 0);
               AV12GridStateDynamicFilter.gxTpr_Operator = AV24DynamicFiltersOperator3;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV130Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ContratoDadosCertame";
         AV30Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_7G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_7G2( true) ;
         }
         else
         {
            wb_table2_8_7G2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_7G2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_109_7G2( true) ;
         }
         else
         {
            wb_table3_109_7G2( false) ;
         }
         return  ;
      }

      protected void wb_table3_109_7G2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_7G2e( true) ;
         }
         else
         {
            wb_table1_2_7G2e( false) ;
         }
      }

      protected void wb_table3_109_7G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "TableGridHeader", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_112_7G2( true) ;
         }
         else
         {
            wb_table4_112_7G2( false) ;
         }
         return  ;
      }

      protected void wb_table4_112_7G2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_109_7G2e( true) ;
         }
         else
         {
            wb_table3_109_7G2e( false) ;
         }
      }

      protected void wb_table4_112_7G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"115\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(110), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContrato_Numero_Titleformat == 0 )
               {
                  context.SendWebValue( edtContrato_Numero_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContrato_Numero_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoDadosCertame_Data_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoDadosCertame_Data_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoDadosCertame_Data_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoDadosCertame_Modalidade_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoDadosCertame_Modalidade_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoDadosCertame_Modalidade_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoDadosCertame_Numero_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoDadosCertame_Numero_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoDadosCertame_Numero_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoDadosCertame_Site_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoDadosCertame_Site_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoDadosCertame_Site_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoDadosCertame_Uasg_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoDadosCertame_Uasg_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoDadosCertame_Uasg_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoDadosCertame_DataHomologacao_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoDadosCertame_DataHomologacao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoDadosCertame_DataHomologacao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoDadosCertame_DataAdjudicacao_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoDadosCertame_DataAdjudicacao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoDadosCertame_DataAdjudicacao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A77Contrato_Numero));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContrato_Numero_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_Numero_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A311ContratoDadosCertame_Data, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoDadosCertame_Data_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoDadosCertame_Data_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A307ContratoDadosCertame_Modalidade);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoDadosCertame_Modalidade_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoDadosCertame_Modalidade_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContratoDadosCertame_Modalidade_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A308ContratoDadosCertame_Numero), 10, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoDadosCertame_Numero_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoDadosCertame_Numero_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A309ContratoDadosCertame_Site);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoDadosCertame_Site_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoDadosCertame_Site_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContratoDadosCertame_Site_Link));
               GridColumn.AddObjectProperty("Linktarget", StringUtil.RTrim( edtContratoDadosCertame_Site_Linktarget));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A310ContratoDadosCertame_Uasg), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoDadosCertame_Uasg_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoDadosCertame_Uasg_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A312ContratoDadosCertame_DataHomologacao, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoDadosCertame_DataHomologacao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoDadosCertame_DataHomologacao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A313ContratoDadosCertame_DataAdjudicacao, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoDadosCertame_DataAdjudicacao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoDadosCertame_DataAdjudicacao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 115 )
         {
            wbEnd = 0;
            nRC_GXsfl_115 = (short)(nGXsfl_115_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_112_7G2e( true) ;
         }
         else
         {
            wb_table4_112_7G2e( false) ;
         }
      }

      protected void wb_table2_8_7G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratodadoscertametitle_Internalname, "Dados Certame", "", "", lblContratodadoscertametitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='Width100'>") ;
            wb_table5_13_7G2( true) ;
         }
         else
         {
            wb_table5_13_7G2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_7G2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_115_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWContratoDadosCertame.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_115_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_23_7G2( true) ;
         }
         else
         {
            wb_table6_23_7G2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_7G2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_7G2e( true) ;
         }
         else
         {
            wb_table2_8_7G2e( false) ;
         }
      }

      protected void wb_table6_23_7G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_7G2( true) ;
         }
         else
         {
            wb_table7_28_7G2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_7G2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_7G2e( true) ;
         }
         else
         {
            wb_table6_23_7G2e( false) ;
         }
      }

      protected void wb_table7_28_7G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_115_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWContratoDadosCertame.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_37_7G2( true) ;
         }
         else
         {
            wb_table8_37_7G2( false) ;
         }
         return  ;
      }

      protected void wb_table8_37_7G2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoDadosCertame.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_115_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", "", true, "HLP_WWContratoDadosCertame.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_63_7G2( true) ;
         }
         else
         {
            wb_table9_63_7G2( false) ;
         }
         return  ;
      }

      protected void wb_table9_63_7G2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoDadosCertame.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'',false,'" + sGXsfl_115_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,85);\"", "", true, "HLP_WWContratoDadosCertame.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_89_7G2( true) ;
         }
         else
         {
            wb_table10_89_7G2( false) ;
         }
         return  ;
      }

      protected void wb_table10_89_7G2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_7G2e( true) ;
         }
         else
         {
            wb_table7_28_7G2e( false) ;
         }
      }

      protected void wb_table10_89_7G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'" + sGXsfl_115_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,92);\"", "", true, "HLP_WWContratoDadosCertame.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table11_94_7G2( true) ;
         }
         else
         {
            wb_table11_94_7G2( false) ;
         }
         return  ;
      }

      protected void wb_table11_94_7G2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_115_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratodadoscertame_modalidade3_Internalname, AV25ContratoDadosCertame_Modalidade3, StringUtil.RTrim( context.localUtil.Format( AV25ContratoDadosCertame_Modalidade3, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,102);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratodadoscertame_modalidade3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratodadoscertame_modalidade3_Visible, 1, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoDadosCertame.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_115_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratodadoscertame_numero3_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV45ContratoDadosCertame_Numero3), 10, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV45ContratoDadosCertame_Numero3), "ZZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,103);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratodadoscertame_numero3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratodadoscertame_numero3_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_89_7G2e( true) ;
         }
         else
         {
            wb_table10_89_7G2e( false) ;
         }
      }

      protected void wb_table11_94_7G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontratodadoscertame_data3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontratodadoscertame_data3_Internalname, tblTablemergeddynamicfilterscontratodadoscertame_data3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_115_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratodadoscertame_data3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratodadoscertame_data3_Internalname, context.localUtil.Format(AV43ContratoDadosCertame_Data3, "99/99/99"), context.localUtil.Format( AV43ContratoDadosCertame_Data3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,97);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratodadoscertame_data3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoDadosCertame.htm");
            GxWebStd.gx_bitmap( context, edtavContratodadoscertame_data3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoDadosCertame.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontratodadoscertame_data_rangemiddletext3_Internalname, "at�", "", "", lblDynamicfilterscontratodadoscertame_data_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_115_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratodadoscertame_data_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratodadoscertame_data_to3_Internalname, context.localUtil.Format(AV44ContratoDadosCertame_Data_To3, "99/99/99"), context.localUtil.Format( AV44ContratoDadosCertame_Data_To3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,101);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratodadoscertame_data_to3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoDadosCertame.htm");
            GxWebStd.gx_bitmap( context, edtavContratodadoscertame_data_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoDadosCertame.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_94_7G2e( true) ;
         }
         else
         {
            wb_table11_94_7G2e( false) ;
         }
      }

      protected void wb_table9_63_7G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'" + sGXsfl_115_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,66);\"", "", true, "HLP_WWContratoDadosCertame.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table12_68_7G2( true) ;
         }
         else
         {
            wb_table12_68_7G2( false) ;
         }
         return  ;
      }

      protected void wb_table12_68_7G2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'" + sGXsfl_115_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratodadoscertame_modalidade2_Internalname, AV21ContratoDadosCertame_Modalidade2, StringUtil.RTrim( context.localUtil.Format( AV21ContratoDadosCertame_Modalidade2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,76);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratodadoscertame_modalidade2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratodadoscertame_modalidade2_Visible, 1, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoDadosCertame.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'" + sGXsfl_115_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratodadoscertame_numero2_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV42ContratoDadosCertame_Numero2), 10, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV42ContratoDadosCertame_Numero2), "ZZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,77);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratodadoscertame_numero2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratodadoscertame_numero2_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_63_7G2e( true) ;
         }
         else
         {
            wb_table9_63_7G2e( false) ;
         }
      }

      protected void wb_table12_68_7G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontratodadoscertame_data2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontratodadoscertame_data2_Internalname, tblTablemergeddynamicfilterscontratodadoscertame_data2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'" + sGXsfl_115_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratodadoscertame_data2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratodadoscertame_data2_Internalname, context.localUtil.Format(AV40ContratoDadosCertame_Data2, "99/99/99"), context.localUtil.Format( AV40ContratoDadosCertame_Data2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,71);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratodadoscertame_data2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoDadosCertame.htm");
            GxWebStd.gx_bitmap( context, edtavContratodadoscertame_data2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoDadosCertame.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontratodadoscertame_data_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfilterscontratodadoscertame_data_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'" + sGXsfl_115_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratodadoscertame_data_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratodadoscertame_data_to2_Internalname, context.localUtil.Format(AV41ContratoDadosCertame_Data_To2, "99/99/99"), context.localUtil.Format( AV41ContratoDadosCertame_Data_To2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,75);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratodadoscertame_data_to2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoDadosCertame.htm");
            GxWebStd.gx_bitmap( context, edtavContratodadoscertame_data_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoDadosCertame.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table12_68_7G2e( true) ;
         }
         else
         {
            wb_table12_68_7G2e( false) ;
         }
      }

      protected void wb_table8_37_7G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_115_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "", true, "HLP_WWContratoDadosCertame.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table13_42_7G2( true) ;
         }
         else
         {
            wb_table13_42_7G2( false) ;
         }
         return  ;
      }

      protected void wb_table13_42_7G2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_115_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratodadoscertame_modalidade1_Internalname, AV17ContratoDadosCertame_Modalidade1, StringUtil.RTrim( context.localUtil.Format( AV17ContratoDadosCertame_Modalidade1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratodadoscertame_modalidade1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratodadoscertame_modalidade1_Visible, 1, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoDadosCertame.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_115_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratodadoscertame_numero1_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39ContratoDadosCertame_Numero1), 10, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV39ContratoDadosCertame_Numero1), "ZZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,51);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratodadoscertame_numero1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratodadoscertame_numero1_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_37_7G2e( true) ;
         }
         else
         {
            wb_table8_37_7G2e( false) ;
         }
      }

      protected void wb_table13_42_7G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontratodadoscertame_data1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontratodadoscertame_data1_Internalname, tblTablemergeddynamicfilterscontratodadoscertame_data1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'" + sGXsfl_115_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratodadoscertame_data1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratodadoscertame_data1_Internalname, context.localUtil.Format(AV37ContratoDadosCertame_Data1, "99/99/99"), context.localUtil.Format( AV37ContratoDadosCertame_Data1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,45);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratodadoscertame_data1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoDadosCertame.htm");
            GxWebStd.gx_bitmap( context, edtavContratodadoscertame_data1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoDadosCertame.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontratodadoscertame_data_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfilterscontratodadoscertame_data_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_115_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratodadoscertame_data_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratodadoscertame_data_to1_Internalname, context.localUtil.Format(AV38ContratoDadosCertame_Data_To1, "99/99/99"), context.localUtil.Format( AV38ContratoDadosCertame_Data_To1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratodadoscertame_data_to1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoDadosCertame.htm");
            GxWebStd.gx_bitmap( context, edtavContratodadoscertame_data_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoDadosCertame.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table13_42_7G2e( true) ;
         }
         else
         {
            wb_table13_42_7G2e( false) ;
         }
      }

      protected void wb_table5_13_7G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_7G2e( true) ;
         }
         else
         {
            wb_table5_13_7G2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA7G2( ) ;
         WS7G2( ) ;
         WE7G2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117371264");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwcontratodadoscertame.js", "?20203117371265");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_1152( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_115_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_115_idx;
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO_"+sGXsfl_115_idx;
         edtContratoDadosCertame_Data_Internalname = "CONTRATODADOSCERTAME_DATA_"+sGXsfl_115_idx;
         edtContratoDadosCertame_Modalidade_Internalname = "CONTRATODADOSCERTAME_MODALIDADE_"+sGXsfl_115_idx;
         edtContratoDadosCertame_Numero_Internalname = "CONTRATODADOSCERTAME_NUMERO_"+sGXsfl_115_idx;
         edtContratoDadosCertame_Site_Internalname = "CONTRATODADOSCERTAME_SITE_"+sGXsfl_115_idx;
         edtContratoDadosCertame_Uasg_Internalname = "CONTRATODADOSCERTAME_UASG_"+sGXsfl_115_idx;
         edtContratoDadosCertame_DataHomologacao_Internalname = "CONTRATODADOSCERTAME_DATAHOMOLOGACAO_"+sGXsfl_115_idx;
         edtContratoDadosCertame_DataAdjudicacao_Internalname = "CONTRATODADOSCERTAME_DATAADJUDICACAO_"+sGXsfl_115_idx;
      }

      protected void SubsflControlProps_fel_1152( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_115_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_115_fel_idx;
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO_"+sGXsfl_115_fel_idx;
         edtContratoDadosCertame_Data_Internalname = "CONTRATODADOSCERTAME_DATA_"+sGXsfl_115_fel_idx;
         edtContratoDadosCertame_Modalidade_Internalname = "CONTRATODADOSCERTAME_MODALIDADE_"+sGXsfl_115_fel_idx;
         edtContratoDadosCertame_Numero_Internalname = "CONTRATODADOSCERTAME_NUMERO_"+sGXsfl_115_fel_idx;
         edtContratoDadosCertame_Site_Internalname = "CONTRATODADOSCERTAME_SITE_"+sGXsfl_115_fel_idx;
         edtContratoDadosCertame_Uasg_Internalname = "CONTRATODADOSCERTAME_UASG_"+sGXsfl_115_fel_idx;
         edtContratoDadosCertame_DataHomologacao_Internalname = "CONTRATODADOSCERTAME_DATAHOMOLOGACAO_"+sGXsfl_115_fel_idx;
         edtContratoDadosCertame_DataAdjudicacao_Internalname = "CONTRATODADOSCERTAME_DATAADJUDICACAO_"+sGXsfl_115_fel_idx;
      }

      protected void sendrow_1152( )
      {
         SubsflControlProps_1152( ) ;
         WB7G0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_115_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_115_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_115_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV28Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV128Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV128Update_GXI : context.PathToRelativeUrl( AV28Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV28Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV29Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV129Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV129Delete_GXI : context.PathToRelativeUrl( AV29Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV29Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_Numero_Internalname,StringUtil.RTrim( A77Contrato_Numero),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_Numero_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)110,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)115,(short)1,(short)-1,(short)-1,(bool)true,(String)"NumeroContrato",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoDadosCertame_Data_Internalname,context.localUtil.Format(A311ContratoDadosCertame_Data, "99/99/99"),context.localUtil.Format( A311ContratoDadosCertame_Data, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoDadosCertame_Data_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)115,(short)1,(short)-1,(short)0,(bool)true,(String)"Data",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoDadosCertame_Modalidade_Internalname,(String)A307ContratoDadosCertame_Modalidade,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtContratoDadosCertame_Modalidade_Link,(String)"",(String)"",(String)"",(String)edtContratoDadosCertame_Modalidade_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)40,(short)0,(short)0,(short)115,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoDadosCertame_Numero_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A308ContratoDadosCertame_Numero), 10, 0, ",", "")),context.localUtil.Format( (decimal)(A308ContratoDadosCertame_Numero), "ZZZZZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoDadosCertame_Numero_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)115,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoDadosCertame_Site_Internalname,(String)A309ContratoDadosCertame_Site,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtContratoDadosCertame_Site_Link,(String)edtContratoDadosCertame_Site_Linktarget,(String)"",(String)"",(String)edtContratoDadosCertame_Site_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(short)1000,(short)0,(short)0,(short)115,(short)1,(short)-1,(short)-1,(bool)true,(String)"URLString",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoDadosCertame_Uasg_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A310ContratoDadosCertame_Uasg), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A310ContratoDadosCertame_Uasg), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoDadosCertame_Uasg_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)115,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoDadosCertame_DataHomologacao_Internalname,context.localUtil.Format(A312ContratoDadosCertame_DataHomologacao, "99/99/99"),context.localUtil.Format( A312ContratoDadosCertame_DataHomologacao, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoDadosCertame_DataHomologacao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)115,(short)1,(short)-1,(short)0,(bool)true,(String)"Data",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoDadosCertame_DataAdjudicacao_Internalname,context.localUtil.Format(A313ContratoDadosCertame_DataAdjudicacao, "99/99/99"),context.localUtil.Format( A313ContratoDadosCertame_DataAdjudicacao, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoDadosCertame_DataAdjudicacao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)115,(short)1,(short)-1,(short)0,(bool)true,(String)"Data",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATODADOSCERTAME_DATA"+"_"+sGXsfl_115_idx, GetSecureSignedToken( sGXsfl_115_idx, A311ContratoDadosCertame_Data));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATODADOSCERTAME_MODALIDADE"+"_"+sGXsfl_115_idx, GetSecureSignedToken( sGXsfl_115_idx, StringUtil.RTrim( context.localUtil.Format( A307ContratoDadosCertame_Modalidade, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATODADOSCERTAME_NUMERO"+"_"+sGXsfl_115_idx, GetSecureSignedToken( sGXsfl_115_idx, context.localUtil.Format( (decimal)(A308ContratoDadosCertame_Numero), "ZZZZZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATODADOSCERTAME_SITE"+"_"+sGXsfl_115_idx, GetSecureSignedToken( sGXsfl_115_idx, StringUtil.RTrim( context.localUtil.Format( A309ContratoDadosCertame_Site, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATODADOSCERTAME_UASG"+"_"+sGXsfl_115_idx, GetSecureSignedToken( sGXsfl_115_idx, context.localUtil.Format( (decimal)(A310ContratoDadosCertame_Uasg), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATODADOSCERTAME_DATAHOMOLOGACAO"+"_"+sGXsfl_115_idx, GetSecureSignedToken( sGXsfl_115_idx, A312ContratoDadosCertame_DataHomologacao));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATODADOSCERTAME_DATAADJUDICACAO"+"_"+sGXsfl_115_idx, GetSecureSignedToken( sGXsfl_115_idx, A313ContratoDadosCertame_DataAdjudicacao));
            GridContainer.AddRow(GridRow);
            nGXsfl_115_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_115_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_115_idx+1));
            sGXsfl_115_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_115_idx), 4, 0)), 4, "0");
            SubsflControlProps_1152( ) ;
         }
         /* End function sendrow_1152 */
      }

      protected void init_default_properties( )
      {
         lblContratodadoscertametitle_Internalname = "CONTRATODADOSCERTAMETITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavContratodadoscertame_data1_Internalname = "vCONTRATODADOSCERTAME_DATA1";
         lblDynamicfilterscontratodadoscertame_data_rangemiddletext1_Internalname = "DYNAMICFILTERSCONTRATODADOSCERTAME_DATA_RANGEMIDDLETEXT1";
         edtavContratodadoscertame_data_to1_Internalname = "vCONTRATODADOSCERTAME_DATA_TO1";
         tblTablemergeddynamicfilterscontratodadoscertame_data1_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATODADOSCERTAME_DATA1";
         edtavContratodadoscertame_modalidade1_Internalname = "vCONTRATODADOSCERTAME_MODALIDADE1";
         edtavContratodadoscertame_numero1_Internalname = "vCONTRATODADOSCERTAME_NUMERO1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavContratodadoscertame_data2_Internalname = "vCONTRATODADOSCERTAME_DATA2";
         lblDynamicfilterscontratodadoscertame_data_rangemiddletext2_Internalname = "DYNAMICFILTERSCONTRATODADOSCERTAME_DATA_RANGEMIDDLETEXT2";
         edtavContratodadoscertame_data_to2_Internalname = "vCONTRATODADOSCERTAME_DATA_TO2";
         tblTablemergeddynamicfilterscontratodadoscertame_data2_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATODADOSCERTAME_DATA2";
         edtavContratodadoscertame_modalidade2_Internalname = "vCONTRATODADOSCERTAME_MODALIDADE2";
         edtavContratodadoscertame_numero2_Internalname = "vCONTRATODADOSCERTAME_NUMERO2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavContratodadoscertame_data3_Internalname = "vCONTRATODADOSCERTAME_DATA3";
         lblDynamicfilterscontratodadoscertame_data_rangemiddletext3_Internalname = "DYNAMICFILTERSCONTRATODADOSCERTAME_DATA_RANGEMIDDLETEXT3";
         edtavContratodadoscertame_data_to3_Internalname = "vCONTRATODADOSCERTAME_DATA_TO3";
         tblTablemergeddynamicfilterscontratodadoscertame_data3_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATODADOSCERTAME_DATA3";
         edtavContratodadoscertame_modalidade3_Internalname = "vCONTRATODADOSCERTAME_MODALIDADE3";
         edtavContratodadoscertame_numero3_Internalname = "vCONTRATODADOSCERTAME_NUMERO3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO";
         edtContratoDadosCertame_Data_Internalname = "CONTRATODADOSCERTAME_DATA";
         edtContratoDadosCertame_Modalidade_Internalname = "CONTRATODADOSCERTAME_MODALIDADE";
         edtContratoDadosCertame_Numero_Internalname = "CONTRATODADOSCERTAME_NUMERO";
         edtContratoDadosCertame_Site_Internalname = "CONTRATODADOSCERTAME_SITE";
         edtContratoDadosCertame_Uasg_Internalname = "CONTRATODADOSCERTAME_UASG";
         edtContratoDadosCertame_DataHomologacao_Internalname = "CONTRATODADOSCERTAME_DATAHOMOLOGACAO";
         edtContratoDadosCertame_DataAdjudicacao_Internalname = "CONTRATODADOSCERTAME_DATAADJUDICACAO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfcontrato_numero_Internalname = "vTFCONTRATO_NUMERO";
         edtavTfcontrato_numero_sel_Internalname = "vTFCONTRATO_NUMERO_SEL";
         edtavTfcontratodadoscertame_data_Internalname = "vTFCONTRATODADOSCERTAME_DATA";
         edtavTfcontratodadoscertame_data_to_Internalname = "vTFCONTRATODADOSCERTAME_DATA_TO";
         edtavDdo_contratodadoscertame_dataauxdate_Internalname = "vDDO_CONTRATODADOSCERTAME_DATAAUXDATE";
         edtavDdo_contratodadoscertame_dataauxdateto_Internalname = "vDDO_CONTRATODADOSCERTAME_DATAAUXDATETO";
         divDdo_contratodadoscertame_dataauxdates_Internalname = "DDO_CONTRATODADOSCERTAME_DATAAUXDATES";
         edtavTfcontratodadoscertame_modalidade_Internalname = "vTFCONTRATODADOSCERTAME_MODALIDADE";
         edtavTfcontratodadoscertame_modalidade_sel_Internalname = "vTFCONTRATODADOSCERTAME_MODALIDADE_SEL";
         edtavTfcontratodadoscertame_numero_Internalname = "vTFCONTRATODADOSCERTAME_NUMERO";
         edtavTfcontratodadoscertame_numero_to_Internalname = "vTFCONTRATODADOSCERTAME_NUMERO_TO";
         edtavTfcontratodadoscertame_site_Internalname = "vTFCONTRATODADOSCERTAME_SITE";
         edtavTfcontratodadoscertame_site_sel_Internalname = "vTFCONTRATODADOSCERTAME_SITE_SEL";
         edtavTfcontratodadoscertame_uasg_Internalname = "vTFCONTRATODADOSCERTAME_UASG";
         edtavTfcontratodadoscertame_uasg_to_Internalname = "vTFCONTRATODADOSCERTAME_UASG_TO";
         edtavTfcontratodadoscertame_datahomologacao_Internalname = "vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO";
         edtavTfcontratodadoscertame_datahomologacao_to_Internalname = "vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO_TO";
         edtavDdo_contratodadoscertame_datahomologacaoauxdate_Internalname = "vDDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAOAUXDATE";
         edtavDdo_contratodadoscertame_datahomologacaoauxdateto_Internalname = "vDDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAOAUXDATETO";
         divDdo_contratodadoscertame_datahomologacaoauxdates_Internalname = "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAOAUXDATES";
         edtavTfcontratodadoscertame_dataadjudicacao_Internalname = "vTFCONTRATODADOSCERTAME_DATAADJUDICACAO";
         edtavTfcontratodadoscertame_dataadjudicacao_to_Internalname = "vTFCONTRATODADOSCERTAME_DATAADJUDICACAO_TO";
         edtavDdo_contratodadoscertame_dataadjudicacaoauxdate_Internalname = "vDDO_CONTRATODADOSCERTAME_DATAADJUDICACAOAUXDATE";
         edtavDdo_contratodadoscertame_dataadjudicacaoauxdateto_Internalname = "vDDO_CONTRATODADOSCERTAME_DATAADJUDICACAOAUXDATETO";
         divDdo_contratodadoscertame_dataadjudicacaoauxdates_Internalname = "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAOAUXDATES";
         Ddo_contrato_numero_Internalname = "DDO_CONTRATO_NUMERO";
         edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE";
         Ddo_contratodadoscertame_data_Internalname = "DDO_CONTRATODADOSCERTAME_DATA";
         edtavDdo_contratodadoscertame_datatitlecontrolidtoreplace_Internalname = "vDDO_CONTRATODADOSCERTAME_DATATITLECONTROLIDTOREPLACE";
         Ddo_contratodadoscertame_modalidade_Internalname = "DDO_CONTRATODADOSCERTAME_MODALIDADE";
         edtavDdo_contratodadoscertame_modalidadetitlecontrolidtoreplace_Internalname = "vDDO_CONTRATODADOSCERTAME_MODALIDADETITLECONTROLIDTOREPLACE";
         Ddo_contratodadoscertame_numero_Internalname = "DDO_CONTRATODADOSCERTAME_NUMERO";
         edtavDdo_contratodadoscertame_numerotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATODADOSCERTAME_NUMEROTITLECONTROLIDTOREPLACE";
         Ddo_contratodadoscertame_site_Internalname = "DDO_CONTRATODADOSCERTAME_SITE";
         edtavDdo_contratodadoscertame_sitetitlecontrolidtoreplace_Internalname = "vDDO_CONTRATODADOSCERTAME_SITETITLECONTROLIDTOREPLACE";
         Ddo_contratodadoscertame_uasg_Internalname = "DDO_CONTRATODADOSCERTAME_UASG";
         edtavDdo_contratodadoscertame_uasgtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATODADOSCERTAME_UASGTITLECONTROLIDTOREPLACE";
         Ddo_contratodadoscertame_datahomologacao_Internalname = "DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO";
         edtavDdo_contratodadoscertame_datahomologacaotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE";
         Ddo_contratodadoscertame_dataadjudicacao_Internalname = "DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO";
         edtavDdo_contratodadoscertame_dataadjudicacaotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATODADOSCERTAME_DATAADJUDICACAOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContratoDadosCertame_DataAdjudicacao_Jsonclick = "";
         edtContratoDadosCertame_DataHomologacao_Jsonclick = "";
         edtContratoDadosCertame_Uasg_Jsonclick = "";
         edtContratoDadosCertame_Site_Jsonclick = "";
         edtContratoDadosCertame_Numero_Jsonclick = "";
         edtContratoDadosCertame_Modalidade_Jsonclick = "";
         edtContratoDadosCertame_Data_Jsonclick = "";
         edtContrato_Numero_Jsonclick = "";
         edtavContratodadoscertame_data_to1_Jsonclick = "";
         edtavContratodadoscertame_data1_Jsonclick = "";
         edtavContratodadoscertame_numero1_Jsonclick = "";
         edtavContratodadoscertame_modalidade1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavContratodadoscertame_data_to2_Jsonclick = "";
         edtavContratodadoscertame_data2_Jsonclick = "";
         edtavContratodadoscertame_numero2_Jsonclick = "";
         edtavContratodadoscertame_modalidade2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavContratodadoscertame_data_to3_Jsonclick = "";
         edtavContratodadoscertame_data3_Jsonclick = "";
         edtavContratodadoscertame_numero3_Jsonclick = "";
         edtavContratodadoscertame_modalidade3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtContratoDadosCertame_Site_Linktarget = "";
         edtContratoDadosCertame_Site_Link = "";
         edtContratoDadosCertame_Modalidade_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtContratoDadosCertame_DataAdjudicacao_Titleformat = 0;
         edtContratoDadosCertame_DataHomologacao_Titleformat = 0;
         edtContratoDadosCertame_Uasg_Titleformat = 0;
         edtContratoDadosCertame_Site_Titleformat = 0;
         edtContratoDadosCertame_Numero_Titleformat = 0;
         edtContratoDadosCertame_Modalidade_Titleformat = 0;
         edtContratoDadosCertame_Data_Titleformat = 0;
         edtContrato_Numero_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavContratodadoscertame_numero3_Visible = 1;
         edtavContratodadoscertame_modalidade3_Visible = 1;
         tblTablemergeddynamicfilterscontratodadoscertame_data3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavContratodadoscertame_numero2_Visible = 1;
         edtavContratodadoscertame_modalidade2_Visible = 1;
         tblTablemergeddynamicfilterscontratodadoscertame_data2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavContratodadoscertame_numero1_Visible = 1;
         edtavContratodadoscertame_modalidade1_Visible = 1;
         tblTablemergeddynamicfilterscontratodadoscertame_data1_Visible = 1;
         edtContratoDadosCertame_DataAdjudicacao_Title = "de adjudica��o";
         edtContratoDadosCertame_DataHomologacao_Title = "de homologa��o";
         edtContratoDadosCertame_Uasg_Title = "Uasg";
         edtContratoDadosCertame_Site_Title = "Site";
         edtContratoDadosCertame_Numero_Title = "Numero";
         edtContratoDadosCertame_Modalidade_Title = "Modalidade";
         edtContratoDadosCertame_Data_Title = "Data";
         edtContrato_Numero_Title = "N� Contrato";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_contratodadoscertame_dataadjudicacaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratodadoscertame_datahomologacaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratodadoscertame_uasgtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratodadoscertame_sitetitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratodadoscertame_numerotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratodadoscertame_modalidadetitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratodadoscertame_datatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratodadoscertame_dataadjudicacaoauxdateto_Jsonclick = "";
         edtavDdo_contratodadoscertame_dataadjudicacaoauxdate_Jsonclick = "";
         edtavTfcontratodadoscertame_dataadjudicacao_to_Jsonclick = "";
         edtavTfcontratodadoscertame_dataadjudicacao_to_Visible = 1;
         edtavTfcontratodadoscertame_dataadjudicacao_Jsonclick = "";
         edtavTfcontratodadoscertame_dataadjudicacao_Visible = 1;
         edtavDdo_contratodadoscertame_datahomologacaoauxdateto_Jsonclick = "";
         edtavDdo_contratodadoscertame_datahomologacaoauxdate_Jsonclick = "";
         edtavTfcontratodadoscertame_datahomologacao_to_Jsonclick = "";
         edtavTfcontratodadoscertame_datahomologacao_to_Visible = 1;
         edtavTfcontratodadoscertame_datahomologacao_Jsonclick = "";
         edtavTfcontratodadoscertame_datahomologacao_Visible = 1;
         edtavTfcontratodadoscertame_uasg_to_Jsonclick = "";
         edtavTfcontratodadoscertame_uasg_to_Visible = 1;
         edtavTfcontratodadoscertame_uasg_Jsonclick = "";
         edtavTfcontratodadoscertame_uasg_Visible = 1;
         edtavTfcontratodadoscertame_site_sel_Jsonclick = "";
         edtavTfcontratodadoscertame_site_sel_Visible = 1;
         edtavTfcontratodadoscertame_site_Jsonclick = "";
         edtavTfcontratodadoscertame_site_Visible = 1;
         edtavTfcontratodadoscertame_numero_to_Jsonclick = "";
         edtavTfcontratodadoscertame_numero_to_Visible = 1;
         edtavTfcontratodadoscertame_numero_Jsonclick = "";
         edtavTfcontratodadoscertame_numero_Visible = 1;
         edtavTfcontratodadoscertame_modalidade_sel_Jsonclick = "";
         edtavTfcontratodadoscertame_modalidade_sel_Visible = 1;
         edtavTfcontratodadoscertame_modalidade_Jsonclick = "";
         edtavTfcontratodadoscertame_modalidade_Visible = 1;
         edtavDdo_contratodadoscertame_dataauxdateto_Jsonclick = "";
         edtavDdo_contratodadoscertame_dataauxdate_Jsonclick = "";
         edtavTfcontratodadoscertame_data_to_Jsonclick = "";
         edtavTfcontratodadoscertame_data_to_Visible = 1;
         edtavTfcontratodadoscertame_data_Jsonclick = "";
         edtavTfcontratodadoscertame_data_Visible = 1;
         edtavTfcontrato_numero_sel_Jsonclick = "";
         edtavTfcontrato_numero_sel_Visible = 1;
         edtavTfcontrato_numero_Jsonclick = "";
         edtavTfcontrato_numero_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_contratodadoscertame_dataadjudicacao_Searchbuttontext = "Pesquisar";
         Ddo_contratodadoscertame_dataadjudicacao_Rangefilterto = "At�";
         Ddo_contratodadoscertame_dataadjudicacao_Rangefilterfrom = "Desde";
         Ddo_contratodadoscertame_dataadjudicacao_Cleanfilter = "Limpar pesquisa";
         Ddo_contratodadoscertame_dataadjudicacao_Sortdsc = "Ordenar de Z � A";
         Ddo_contratodadoscertame_dataadjudicacao_Sortasc = "Ordenar de A � Z";
         Ddo_contratodadoscertame_dataadjudicacao_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratodadoscertame_dataadjudicacao_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratodadoscertame_dataadjudicacao_Filtertype = "Date";
         Ddo_contratodadoscertame_dataadjudicacao_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratodadoscertame_dataadjudicacao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratodadoscertame_dataadjudicacao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratodadoscertame_dataadjudicacao_Titlecontrolidtoreplace = "";
         Ddo_contratodadoscertame_dataadjudicacao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratodadoscertame_dataadjudicacao_Cls = "ColumnSettings";
         Ddo_contratodadoscertame_dataadjudicacao_Tooltip = "Op��es";
         Ddo_contratodadoscertame_dataadjudicacao_Caption = "";
         Ddo_contratodadoscertame_datahomologacao_Searchbuttontext = "Pesquisar";
         Ddo_contratodadoscertame_datahomologacao_Rangefilterto = "At�";
         Ddo_contratodadoscertame_datahomologacao_Rangefilterfrom = "Desde";
         Ddo_contratodadoscertame_datahomologacao_Cleanfilter = "Limpar pesquisa";
         Ddo_contratodadoscertame_datahomologacao_Sortdsc = "Ordenar de Z � A";
         Ddo_contratodadoscertame_datahomologacao_Sortasc = "Ordenar de A � Z";
         Ddo_contratodadoscertame_datahomologacao_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratodadoscertame_datahomologacao_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratodadoscertame_datahomologacao_Filtertype = "Date";
         Ddo_contratodadoscertame_datahomologacao_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratodadoscertame_datahomologacao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratodadoscertame_datahomologacao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratodadoscertame_datahomologacao_Titlecontrolidtoreplace = "";
         Ddo_contratodadoscertame_datahomologacao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratodadoscertame_datahomologacao_Cls = "ColumnSettings";
         Ddo_contratodadoscertame_datahomologacao_Tooltip = "Op��es";
         Ddo_contratodadoscertame_datahomologacao_Caption = "";
         Ddo_contratodadoscertame_uasg_Searchbuttontext = "Pesquisar";
         Ddo_contratodadoscertame_uasg_Rangefilterto = "At�";
         Ddo_contratodadoscertame_uasg_Rangefilterfrom = "Desde";
         Ddo_contratodadoscertame_uasg_Cleanfilter = "Limpar pesquisa";
         Ddo_contratodadoscertame_uasg_Sortdsc = "Ordenar de Z � A";
         Ddo_contratodadoscertame_uasg_Sortasc = "Ordenar de A � Z";
         Ddo_contratodadoscertame_uasg_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratodadoscertame_uasg_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratodadoscertame_uasg_Filtertype = "Numeric";
         Ddo_contratodadoscertame_uasg_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratodadoscertame_uasg_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratodadoscertame_uasg_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratodadoscertame_uasg_Titlecontrolidtoreplace = "";
         Ddo_contratodadoscertame_uasg_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratodadoscertame_uasg_Cls = "ColumnSettings";
         Ddo_contratodadoscertame_uasg_Tooltip = "Op��es";
         Ddo_contratodadoscertame_uasg_Caption = "";
         Ddo_contratodadoscertame_site_Searchbuttontext = "Pesquisar";
         Ddo_contratodadoscertame_site_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratodadoscertame_site_Cleanfilter = "Limpar pesquisa";
         Ddo_contratodadoscertame_site_Loadingdata = "Carregando dados...";
         Ddo_contratodadoscertame_site_Sortdsc = "Ordenar de Z � A";
         Ddo_contratodadoscertame_site_Sortasc = "Ordenar de A � Z";
         Ddo_contratodadoscertame_site_Datalistupdateminimumcharacters = 0;
         Ddo_contratodadoscertame_site_Datalistproc = "GetWWContratoDadosCertameFilterData";
         Ddo_contratodadoscertame_site_Datalisttype = "Dynamic";
         Ddo_contratodadoscertame_site_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratodadoscertame_site_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratodadoscertame_site_Filtertype = "Character";
         Ddo_contratodadoscertame_site_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratodadoscertame_site_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratodadoscertame_site_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratodadoscertame_site_Titlecontrolidtoreplace = "";
         Ddo_contratodadoscertame_site_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratodadoscertame_site_Cls = "ColumnSettings";
         Ddo_contratodadoscertame_site_Tooltip = "Op��es";
         Ddo_contratodadoscertame_site_Caption = "";
         Ddo_contratodadoscertame_numero_Searchbuttontext = "Pesquisar";
         Ddo_contratodadoscertame_numero_Rangefilterto = "At�";
         Ddo_contratodadoscertame_numero_Rangefilterfrom = "Desde";
         Ddo_contratodadoscertame_numero_Cleanfilter = "Limpar pesquisa";
         Ddo_contratodadoscertame_numero_Sortdsc = "Ordenar de Z � A";
         Ddo_contratodadoscertame_numero_Sortasc = "Ordenar de A � Z";
         Ddo_contratodadoscertame_numero_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratodadoscertame_numero_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratodadoscertame_numero_Filtertype = "Numeric";
         Ddo_contratodadoscertame_numero_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratodadoscertame_numero_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratodadoscertame_numero_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratodadoscertame_numero_Titlecontrolidtoreplace = "";
         Ddo_contratodadoscertame_numero_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratodadoscertame_numero_Cls = "ColumnSettings";
         Ddo_contratodadoscertame_numero_Tooltip = "Op��es";
         Ddo_contratodadoscertame_numero_Caption = "";
         Ddo_contratodadoscertame_modalidade_Searchbuttontext = "Pesquisar";
         Ddo_contratodadoscertame_modalidade_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratodadoscertame_modalidade_Cleanfilter = "Limpar pesquisa";
         Ddo_contratodadoscertame_modalidade_Loadingdata = "Carregando dados...";
         Ddo_contratodadoscertame_modalidade_Sortdsc = "Ordenar de Z � A";
         Ddo_contratodadoscertame_modalidade_Sortasc = "Ordenar de A � Z";
         Ddo_contratodadoscertame_modalidade_Datalistupdateminimumcharacters = 0;
         Ddo_contratodadoscertame_modalidade_Datalistproc = "GetWWContratoDadosCertameFilterData";
         Ddo_contratodadoscertame_modalidade_Datalisttype = "Dynamic";
         Ddo_contratodadoscertame_modalidade_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratodadoscertame_modalidade_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratodadoscertame_modalidade_Filtertype = "Character";
         Ddo_contratodadoscertame_modalidade_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratodadoscertame_modalidade_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratodadoscertame_modalidade_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratodadoscertame_modalidade_Titlecontrolidtoreplace = "";
         Ddo_contratodadoscertame_modalidade_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratodadoscertame_modalidade_Cls = "ColumnSettings";
         Ddo_contratodadoscertame_modalidade_Tooltip = "Op��es";
         Ddo_contratodadoscertame_modalidade_Caption = "";
         Ddo_contratodadoscertame_data_Searchbuttontext = "Pesquisar";
         Ddo_contratodadoscertame_data_Rangefilterto = "At�";
         Ddo_contratodadoscertame_data_Rangefilterfrom = "Desde";
         Ddo_contratodadoscertame_data_Cleanfilter = "Limpar pesquisa";
         Ddo_contratodadoscertame_data_Sortdsc = "Ordenar de Z � A";
         Ddo_contratodadoscertame_data_Sortasc = "Ordenar de A � Z";
         Ddo_contratodadoscertame_data_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratodadoscertame_data_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratodadoscertame_data_Filtertype = "Date";
         Ddo_contratodadoscertame_data_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratodadoscertame_data_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratodadoscertame_data_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratodadoscertame_data_Titlecontrolidtoreplace = "";
         Ddo_contratodadoscertame_data_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratodadoscertame_data_Cls = "ColumnSettings";
         Ddo_contratodadoscertame_data_Tooltip = "Op��es";
         Ddo_contratodadoscertame_data_Caption = "";
         Ddo_contrato_numero_Searchbuttontext = "Pesquisar";
         Ddo_contrato_numero_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contrato_numero_Cleanfilter = "Limpar pesquisa";
         Ddo_contrato_numero_Loadingdata = "Carregando dados...";
         Ddo_contrato_numero_Sortdsc = "Ordenar de Z � A";
         Ddo_contrato_numero_Sortasc = "Ordenar de A � Z";
         Ddo_contrato_numero_Datalistupdateminimumcharacters = 0;
         Ddo_contrato_numero_Datalistproc = "GetWWContratoDadosCertameFilterData";
         Ddo_contrato_numero_Datalisttype = "Dynamic";
         Ddo_contrato_numero_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contrato_numero_Filtertype = "Character";
         Ddo_contrato_numero_Includefilter = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Titlecontrolidtoreplace = "";
         Ddo_contrato_numero_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contrato_numero_Cls = "ColumnSettings";
         Ddo_contrato_numero_Tooltip = "Op��es";
         Ddo_contrato_numero_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Contrato Dados Certame";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A314ContratoDadosCertame_Codigo',fld:'CONTRATODADOSCERTAME_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A309ContratoDadosCertame_Site',fld:'CONTRATODADOSCERTAME_SITE',pic:'',hsh:true,nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV51ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoDadosCertame_DataTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoDadosCertame_ModalidadeTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_MODALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoDadosCertame_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ContratoDadosCertame_SiteTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_SITETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoDadosCertame_UasgTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_UASGTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratoDadosCertame_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_ContratoDadosCertame_DataAdjudicacaoTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATAADJUDICACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV37ContratoDadosCertame_Data1',fld:'vCONTRATODADOSCERTAME_DATA1',pic:'',nv:''},{av:'AV38ContratoDadosCertame_Data_To1',fld:'vCONTRATODADOSCERTAME_DATA_TO1',pic:'',nv:''},{av:'AV17ContratoDadosCertame_Modalidade1',fld:'vCONTRATODADOSCERTAME_MODALIDADE1',pic:'',nv:''},{av:'AV39ContratoDadosCertame_Numero1',fld:'vCONTRATODADOSCERTAME_NUMERO1',pic:'ZZZZZZZZZ9',nv:0},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV40ContratoDadosCertame_Data2',fld:'vCONTRATODADOSCERTAME_DATA2',pic:'',nv:''},{av:'AV41ContratoDadosCertame_Data_To2',fld:'vCONTRATODADOSCERTAME_DATA_TO2',pic:'',nv:''},{av:'AV21ContratoDadosCertame_Modalidade2',fld:'vCONTRATODADOSCERTAME_MODALIDADE2',pic:'',nv:''},{av:'AV42ContratoDadosCertame_Numero2',fld:'vCONTRATODADOSCERTAME_NUMERO2',pic:'ZZZZZZZZZ9',nv:0},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV43ContratoDadosCertame_Data3',fld:'vCONTRATODADOSCERTAME_DATA3',pic:'',nv:''},{av:'AV44ContratoDadosCertame_Data_To3',fld:'vCONTRATODADOSCERTAME_DATA_TO3',pic:'',nv:''},{av:'AV25ContratoDadosCertame_Modalidade3',fld:'vCONTRATODADOSCERTAME_MODALIDADE3',pic:'',nv:''},{av:'AV45ContratoDadosCertame_Numero3',fld:'vCONTRATODADOSCERTAME_NUMERO3',pic:'ZZZZZZZZZ9',nv:0},{av:'AV49TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV50TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV53TFContratoDadosCertame_Data',fld:'vTFCONTRATODADOSCERTAME_DATA',pic:'',nv:''},{av:'AV54TFContratoDadosCertame_Data_To',fld:'vTFCONTRATODADOSCERTAME_DATA_TO',pic:'',nv:''},{av:'AV59TFContratoDadosCertame_Modalidade',fld:'vTFCONTRATODADOSCERTAME_MODALIDADE',pic:'',nv:''},{av:'AV60TFContratoDadosCertame_Modalidade_Sel',fld:'vTFCONTRATODADOSCERTAME_MODALIDADE_SEL',pic:'',nv:''},{av:'AV63TFContratoDadosCertame_Numero',fld:'vTFCONTRATODADOSCERTAME_NUMERO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV64TFContratoDadosCertame_Numero_To',fld:'vTFCONTRATODADOSCERTAME_NUMERO_TO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV67TFContratoDadosCertame_Site',fld:'vTFCONTRATODADOSCERTAME_SITE',pic:'',nv:''},{av:'AV68TFContratoDadosCertame_Site_Sel',fld:'vTFCONTRATODADOSCERTAME_SITE_SEL',pic:'',nv:''},{av:'AV71TFContratoDadosCertame_Uasg',fld:'vTFCONTRATODADOSCERTAME_UASG',pic:'ZZZ9',nv:0},{av:'AV72TFContratoDadosCertame_Uasg_To',fld:'vTFCONTRATODADOSCERTAME_UASG_TO',pic:'ZZZ9',nv:0},{av:'AV75TFContratoDadosCertame_DataHomologacao',fld:'vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO',pic:'',nv:''},{av:'AV76TFContratoDadosCertame_DataHomologacao_To',fld:'vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO_TO',pic:'',nv:''},{av:'AV81TFContratoDadosCertame_DataAdjudicacao',fld:'vTFCONTRATODADOSCERTAME_DATAADJUDICACAO',pic:'',nv:''},{av:'AV82TFContratoDadosCertame_DataAdjudicacao_To',fld:'vTFCONTRATODADOSCERTAME_DATAADJUDICACAO_TO',pic:'',nv:''},{av:'AV130Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV48Contrato_NumeroTitleFilterData',fld:'vCONTRATO_NUMEROTITLEFILTERDATA',pic:'',nv:null},{av:'AV52ContratoDadosCertame_DataTitleFilterData',fld:'vCONTRATODADOSCERTAME_DATATITLEFILTERDATA',pic:'',nv:null},{av:'AV58ContratoDadosCertame_ModalidadeTitleFilterData',fld:'vCONTRATODADOSCERTAME_MODALIDADETITLEFILTERDATA',pic:'',nv:null},{av:'AV62ContratoDadosCertame_NumeroTitleFilterData',fld:'vCONTRATODADOSCERTAME_NUMEROTITLEFILTERDATA',pic:'',nv:null},{av:'AV66ContratoDadosCertame_SiteTitleFilterData',fld:'vCONTRATODADOSCERTAME_SITETITLEFILTERDATA',pic:'',nv:null},{av:'AV70ContratoDadosCertame_UasgTitleFilterData',fld:'vCONTRATODADOSCERTAME_UASGTITLEFILTERDATA',pic:'',nv:null},{av:'AV74ContratoDadosCertame_DataHomologacaoTitleFilterData',fld:'vCONTRATODADOSCERTAME_DATAHOMOLOGACAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV80ContratoDadosCertame_DataAdjudicacaoTitleFilterData',fld:'vCONTRATODADOSCERTAME_DATAADJUDICACAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtContrato_Numero_Titleformat',ctrl:'CONTRATO_NUMERO',prop:'Titleformat'},{av:'edtContrato_Numero_Title',ctrl:'CONTRATO_NUMERO',prop:'Title'},{av:'edtContratoDadosCertame_Data_Titleformat',ctrl:'CONTRATODADOSCERTAME_DATA',prop:'Titleformat'},{av:'edtContratoDadosCertame_Data_Title',ctrl:'CONTRATODADOSCERTAME_DATA',prop:'Title'},{av:'edtContratoDadosCertame_Modalidade_Titleformat',ctrl:'CONTRATODADOSCERTAME_MODALIDADE',prop:'Titleformat'},{av:'edtContratoDadosCertame_Modalidade_Title',ctrl:'CONTRATODADOSCERTAME_MODALIDADE',prop:'Title'},{av:'edtContratoDadosCertame_Numero_Titleformat',ctrl:'CONTRATODADOSCERTAME_NUMERO',prop:'Titleformat'},{av:'edtContratoDadosCertame_Numero_Title',ctrl:'CONTRATODADOSCERTAME_NUMERO',prop:'Title'},{av:'edtContratoDadosCertame_Site_Titleformat',ctrl:'CONTRATODADOSCERTAME_SITE',prop:'Titleformat'},{av:'edtContratoDadosCertame_Site_Title',ctrl:'CONTRATODADOSCERTAME_SITE',prop:'Title'},{av:'edtContratoDadosCertame_Uasg_Titleformat',ctrl:'CONTRATODADOSCERTAME_UASG',prop:'Titleformat'},{av:'edtContratoDadosCertame_Uasg_Title',ctrl:'CONTRATODADOSCERTAME_UASG',prop:'Title'},{av:'edtContratoDadosCertame_DataHomologacao_Titleformat',ctrl:'CONTRATODADOSCERTAME_DATAHOMOLOGACAO',prop:'Titleformat'},{av:'edtContratoDadosCertame_DataHomologacao_Title',ctrl:'CONTRATODADOSCERTAME_DATAHOMOLOGACAO',prop:'Title'},{av:'edtContratoDadosCertame_DataAdjudicacao_Titleformat',ctrl:'CONTRATODADOSCERTAME_DATAADJUDICACAO',prop:'Titleformat'},{av:'edtContratoDadosCertame_DataAdjudicacao_Title',ctrl:'CONTRATODADOSCERTAME_DATAADJUDICACAO',prop:'Title'},{av:'AV88GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV89GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E117G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV37ContratoDadosCertame_Data1',fld:'vCONTRATODADOSCERTAME_DATA1',pic:'',nv:''},{av:'AV38ContratoDadosCertame_Data_To1',fld:'vCONTRATODADOSCERTAME_DATA_TO1',pic:'',nv:''},{av:'AV17ContratoDadosCertame_Modalidade1',fld:'vCONTRATODADOSCERTAME_MODALIDADE1',pic:'',nv:''},{av:'AV39ContratoDadosCertame_Numero1',fld:'vCONTRATODADOSCERTAME_NUMERO1',pic:'ZZZZZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV40ContratoDadosCertame_Data2',fld:'vCONTRATODADOSCERTAME_DATA2',pic:'',nv:''},{av:'AV41ContratoDadosCertame_Data_To2',fld:'vCONTRATODADOSCERTAME_DATA_TO2',pic:'',nv:''},{av:'AV21ContratoDadosCertame_Modalidade2',fld:'vCONTRATODADOSCERTAME_MODALIDADE2',pic:'',nv:''},{av:'AV42ContratoDadosCertame_Numero2',fld:'vCONTRATODADOSCERTAME_NUMERO2',pic:'ZZZZZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV43ContratoDadosCertame_Data3',fld:'vCONTRATODADOSCERTAME_DATA3',pic:'',nv:''},{av:'AV44ContratoDadosCertame_Data_To3',fld:'vCONTRATODADOSCERTAME_DATA_TO3',pic:'',nv:''},{av:'AV25ContratoDadosCertame_Modalidade3',fld:'vCONTRATODADOSCERTAME_MODALIDADE3',pic:'',nv:''},{av:'AV45ContratoDadosCertame_Numero3',fld:'vCONTRATODADOSCERTAME_NUMERO3',pic:'ZZZZZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV50TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV53TFContratoDadosCertame_Data',fld:'vTFCONTRATODADOSCERTAME_DATA',pic:'',nv:''},{av:'AV54TFContratoDadosCertame_Data_To',fld:'vTFCONTRATODADOSCERTAME_DATA_TO',pic:'',nv:''},{av:'AV59TFContratoDadosCertame_Modalidade',fld:'vTFCONTRATODADOSCERTAME_MODALIDADE',pic:'',nv:''},{av:'AV60TFContratoDadosCertame_Modalidade_Sel',fld:'vTFCONTRATODADOSCERTAME_MODALIDADE_SEL',pic:'',nv:''},{av:'AV63TFContratoDadosCertame_Numero',fld:'vTFCONTRATODADOSCERTAME_NUMERO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV64TFContratoDadosCertame_Numero_To',fld:'vTFCONTRATODADOSCERTAME_NUMERO_TO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV67TFContratoDadosCertame_Site',fld:'vTFCONTRATODADOSCERTAME_SITE',pic:'',nv:''},{av:'AV68TFContratoDadosCertame_Site_Sel',fld:'vTFCONTRATODADOSCERTAME_SITE_SEL',pic:'',nv:''},{av:'AV71TFContratoDadosCertame_Uasg',fld:'vTFCONTRATODADOSCERTAME_UASG',pic:'ZZZ9',nv:0},{av:'AV72TFContratoDadosCertame_Uasg_To',fld:'vTFCONTRATODADOSCERTAME_UASG_TO',pic:'ZZZ9',nv:0},{av:'AV75TFContratoDadosCertame_DataHomologacao',fld:'vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO',pic:'',nv:''},{av:'AV76TFContratoDadosCertame_DataHomologacao_To',fld:'vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO_TO',pic:'',nv:''},{av:'AV81TFContratoDadosCertame_DataAdjudicacao',fld:'vTFCONTRATODADOSCERTAME_DATAADJUDICACAO',pic:'',nv:''},{av:'AV82TFContratoDadosCertame_DataAdjudicacao_To',fld:'vTFCONTRATODADOSCERTAME_DATAADJUDICACAO_TO',pic:'',nv:''},{av:'AV51ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoDadosCertame_DataTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoDadosCertame_ModalidadeTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_MODALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoDadosCertame_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ContratoDadosCertame_SiteTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_SITETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoDadosCertame_UasgTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_UASGTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratoDadosCertame_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_ContratoDadosCertame_DataAdjudicacaoTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATAADJUDICACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A314ContratoDadosCertame_Codigo',fld:'CONTRATODADOSCERTAME_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A309ContratoDadosCertame_Site',fld:'CONTRATODADOSCERTAME_SITE',pic:'',hsh:true,nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTRATO_NUMERO.ONOPTIONCLICKED","{handler:'E127G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV37ContratoDadosCertame_Data1',fld:'vCONTRATODADOSCERTAME_DATA1',pic:'',nv:''},{av:'AV38ContratoDadosCertame_Data_To1',fld:'vCONTRATODADOSCERTAME_DATA_TO1',pic:'',nv:''},{av:'AV17ContratoDadosCertame_Modalidade1',fld:'vCONTRATODADOSCERTAME_MODALIDADE1',pic:'',nv:''},{av:'AV39ContratoDadosCertame_Numero1',fld:'vCONTRATODADOSCERTAME_NUMERO1',pic:'ZZZZZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV40ContratoDadosCertame_Data2',fld:'vCONTRATODADOSCERTAME_DATA2',pic:'',nv:''},{av:'AV41ContratoDadosCertame_Data_To2',fld:'vCONTRATODADOSCERTAME_DATA_TO2',pic:'',nv:''},{av:'AV21ContratoDadosCertame_Modalidade2',fld:'vCONTRATODADOSCERTAME_MODALIDADE2',pic:'',nv:''},{av:'AV42ContratoDadosCertame_Numero2',fld:'vCONTRATODADOSCERTAME_NUMERO2',pic:'ZZZZZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV43ContratoDadosCertame_Data3',fld:'vCONTRATODADOSCERTAME_DATA3',pic:'',nv:''},{av:'AV44ContratoDadosCertame_Data_To3',fld:'vCONTRATODADOSCERTAME_DATA_TO3',pic:'',nv:''},{av:'AV25ContratoDadosCertame_Modalidade3',fld:'vCONTRATODADOSCERTAME_MODALIDADE3',pic:'',nv:''},{av:'AV45ContratoDadosCertame_Numero3',fld:'vCONTRATODADOSCERTAME_NUMERO3',pic:'ZZZZZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV50TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV53TFContratoDadosCertame_Data',fld:'vTFCONTRATODADOSCERTAME_DATA',pic:'',nv:''},{av:'AV54TFContratoDadosCertame_Data_To',fld:'vTFCONTRATODADOSCERTAME_DATA_TO',pic:'',nv:''},{av:'AV59TFContratoDadosCertame_Modalidade',fld:'vTFCONTRATODADOSCERTAME_MODALIDADE',pic:'',nv:''},{av:'AV60TFContratoDadosCertame_Modalidade_Sel',fld:'vTFCONTRATODADOSCERTAME_MODALIDADE_SEL',pic:'',nv:''},{av:'AV63TFContratoDadosCertame_Numero',fld:'vTFCONTRATODADOSCERTAME_NUMERO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV64TFContratoDadosCertame_Numero_To',fld:'vTFCONTRATODADOSCERTAME_NUMERO_TO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV67TFContratoDadosCertame_Site',fld:'vTFCONTRATODADOSCERTAME_SITE',pic:'',nv:''},{av:'AV68TFContratoDadosCertame_Site_Sel',fld:'vTFCONTRATODADOSCERTAME_SITE_SEL',pic:'',nv:''},{av:'AV71TFContratoDadosCertame_Uasg',fld:'vTFCONTRATODADOSCERTAME_UASG',pic:'ZZZ9',nv:0},{av:'AV72TFContratoDadosCertame_Uasg_To',fld:'vTFCONTRATODADOSCERTAME_UASG_TO',pic:'ZZZ9',nv:0},{av:'AV75TFContratoDadosCertame_DataHomologacao',fld:'vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO',pic:'',nv:''},{av:'AV76TFContratoDadosCertame_DataHomologacao_To',fld:'vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO_TO',pic:'',nv:''},{av:'AV81TFContratoDadosCertame_DataAdjudicacao',fld:'vTFCONTRATODADOSCERTAME_DATAADJUDICACAO',pic:'',nv:''},{av:'AV82TFContratoDadosCertame_DataAdjudicacao_To',fld:'vTFCONTRATODADOSCERTAME_DATAADJUDICACAO_TO',pic:'',nv:''},{av:'AV51ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoDadosCertame_DataTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoDadosCertame_ModalidadeTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_MODALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoDadosCertame_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ContratoDadosCertame_SiteTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_SITETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoDadosCertame_UasgTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_UASGTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratoDadosCertame_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_ContratoDadosCertame_DataAdjudicacaoTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATAADJUDICACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A314ContratoDadosCertame_Codigo',fld:'CONTRATODADOSCERTAME_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A309ContratoDadosCertame_Site',fld:'CONTRATODADOSCERTAME_SITE',pic:'',hsh:true,nv:''},{av:'Ddo_contrato_numero_Activeeventkey',ctrl:'DDO_CONTRATO_NUMERO',prop:'ActiveEventKey'},{av:'Ddo_contrato_numero_Filteredtext_get',ctrl:'DDO_CONTRATO_NUMERO',prop:'FilteredText_get'},{av:'Ddo_contrato_numero_Selectedvalue_get',ctrl:'DDO_CONTRATO_NUMERO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'AV49TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV50TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'Ddo_contratodadoscertame_data_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_DATA',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_modalidade_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_MODALIDADE',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_numero_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_site_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_SITE',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_uasg_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_UASG',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_datahomologacao_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_dataadjudicacao_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATODADOSCERTAME_DATA.ONOPTIONCLICKED","{handler:'E137G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV37ContratoDadosCertame_Data1',fld:'vCONTRATODADOSCERTAME_DATA1',pic:'',nv:''},{av:'AV38ContratoDadosCertame_Data_To1',fld:'vCONTRATODADOSCERTAME_DATA_TO1',pic:'',nv:''},{av:'AV17ContratoDadosCertame_Modalidade1',fld:'vCONTRATODADOSCERTAME_MODALIDADE1',pic:'',nv:''},{av:'AV39ContratoDadosCertame_Numero1',fld:'vCONTRATODADOSCERTAME_NUMERO1',pic:'ZZZZZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV40ContratoDadosCertame_Data2',fld:'vCONTRATODADOSCERTAME_DATA2',pic:'',nv:''},{av:'AV41ContratoDadosCertame_Data_To2',fld:'vCONTRATODADOSCERTAME_DATA_TO2',pic:'',nv:''},{av:'AV21ContratoDadosCertame_Modalidade2',fld:'vCONTRATODADOSCERTAME_MODALIDADE2',pic:'',nv:''},{av:'AV42ContratoDadosCertame_Numero2',fld:'vCONTRATODADOSCERTAME_NUMERO2',pic:'ZZZZZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV43ContratoDadosCertame_Data3',fld:'vCONTRATODADOSCERTAME_DATA3',pic:'',nv:''},{av:'AV44ContratoDadosCertame_Data_To3',fld:'vCONTRATODADOSCERTAME_DATA_TO3',pic:'',nv:''},{av:'AV25ContratoDadosCertame_Modalidade3',fld:'vCONTRATODADOSCERTAME_MODALIDADE3',pic:'',nv:''},{av:'AV45ContratoDadosCertame_Numero3',fld:'vCONTRATODADOSCERTAME_NUMERO3',pic:'ZZZZZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV50TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV53TFContratoDadosCertame_Data',fld:'vTFCONTRATODADOSCERTAME_DATA',pic:'',nv:''},{av:'AV54TFContratoDadosCertame_Data_To',fld:'vTFCONTRATODADOSCERTAME_DATA_TO',pic:'',nv:''},{av:'AV59TFContratoDadosCertame_Modalidade',fld:'vTFCONTRATODADOSCERTAME_MODALIDADE',pic:'',nv:''},{av:'AV60TFContratoDadosCertame_Modalidade_Sel',fld:'vTFCONTRATODADOSCERTAME_MODALIDADE_SEL',pic:'',nv:''},{av:'AV63TFContratoDadosCertame_Numero',fld:'vTFCONTRATODADOSCERTAME_NUMERO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV64TFContratoDadosCertame_Numero_To',fld:'vTFCONTRATODADOSCERTAME_NUMERO_TO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV67TFContratoDadosCertame_Site',fld:'vTFCONTRATODADOSCERTAME_SITE',pic:'',nv:''},{av:'AV68TFContratoDadosCertame_Site_Sel',fld:'vTFCONTRATODADOSCERTAME_SITE_SEL',pic:'',nv:''},{av:'AV71TFContratoDadosCertame_Uasg',fld:'vTFCONTRATODADOSCERTAME_UASG',pic:'ZZZ9',nv:0},{av:'AV72TFContratoDadosCertame_Uasg_To',fld:'vTFCONTRATODADOSCERTAME_UASG_TO',pic:'ZZZ9',nv:0},{av:'AV75TFContratoDadosCertame_DataHomologacao',fld:'vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO',pic:'',nv:''},{av:'AV76TFContratoDadosCertame_DataHomologacao_To',fld:'vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO_TO',pic:'',nv:''},{av:'AV81TFContratoDadosCertame_DataAdjudicacao',fld:'vTFCONTRATODADOSCERTAME_DATAADJUDICACAO',pic:'',nv:''},{av:'AV82TFContratoDadosCertame_DataAdjudicacao_To',fld:'vTFCONTRATODADOSCERTAME_DATAADJUDICACAO_TO',pic:'',nv:''},{av:'AV51ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoDadosCertame_DataTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoDadosCertame_ModalidadeTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_MODALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoDadosCertame_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ContratoDadosCertame_SiteTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_SITETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoDadosCertame_UasgTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_UASGTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratoDadosCertame_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_ContratoDadosCertame_DataAdjudicacaoTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATAADJUDICACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A314ContratoDadosCertame_Codigo',fld:'CONTRATODADOSCERTAME_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A309ContratoDadosCertame_Site',fld:'CONTRATODADOSCERTAME_SITE',pic:'',hsh:true,nv:''},{av:'Ddo_contratodadoscertame_data_Activeeventkey',ctrl:'DDO_CONTRATODADOSCERTAME_DATA',prop:'ActiveEventKey'},{av:'Ddo_contratodadoscertame_data_Filteredtext_get',ctrl:'DDO_CONTRATODADOSCERTAME_DATA',prop:'FilteredText_get'},{av:'Ddo_contratodadoscertame_data_Filteredtextto_get',ctrl:'DDO_CONTRATODADOSCERTAME_DATA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratodadoscertame_data_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_DATA',prop:'SortedStatus'},{av:'AV53TFContratoDadosCertame_Data',fld:'vTFCONTRATODADOSCERTAME_DATA',pic:'',nv:''},{av:'AV54TFContratoDadosCertame_Data_To',fld:'vTFCONTRATODADOSCERTAME_DATA_TO',pic:'',nv:''},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_modalidade_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_MODALIDADE',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_numero_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_site_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_SITE',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_uasg_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_UASG',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_datahomologacao_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_dataadjudicacao_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATODADOSCERTAME_MODALIDADE.ONOPTIONCLICKED","{handler:'E147G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV37ContratoDadosCertame_Data1',fld:'vCONTRATODADOSCERTAME_DATA1',pic:'',nv:''},{av:'AV38ContratoDadosCertame_Data_To1',fld:'vCONTRATODADOSCERTAME_DATA_TO1',pic:'',nv:''},{av:'AV17ContratoDadosCertame_Modalidade1',fld:'vCONTRATODADOSCERTAME_MODALIDADE1',pic:'',nv:''},{av:'AV39ContratoDadosCertame_Numero1',fld:'vCONTRATODADOSCERTAME_NUMERO1',pic:'ZZZZZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV40ContratoDadosCertame_Data2',fld:'vCONTRATODADOSCERTAME_DATA2',pic:'',nv:''},{av:'AV41ContratoDadosCertame_Data_To2',fld:'vCONTRATODADOSCERTAME_DATA_TO2',pic:'',nv:''},{av:'AV21ContratoDadosCertame_Modalidade2',fld:'vCONTRATODADOSCERTAME_MODALIDADE2',pic:'',nv:''},{av:'AV42ContratoDadosCertame_Numero2',fld:'vCONTRATODADOSCERTAME_NUMERO2',pic:'ZZZZZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV43ContratoDadosCertame_Data3',fld:'vCONTRATODADOSCERTAME_DATA3',pic:'',nv:''},{av:'AV44ContratoDadosCertame_Data_To3',fld:'vCONTRATODADOSCERTAME_DATA_TO3',pic:'',nv:''},{av:'AV25ContratoDadosCertame_Modalidade3',fld:'vCONTRATODADOSCERTAME_MODALIDADE3',pic:'',nv:''},{av:'AV45ContratoDadosCertame_Numero3',fld:'vCONTRATODADOSCERTAME_NUMERO3',pic:'ZZZZZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV50TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV53TFContratoDadosCertame_Data',fld:'vTFCONTRATODADOSCERTAME_DATA',pic:'',nv:''},{av:'AV54TFContratoDadosCertame_Data_To',fld:'vTFCONTRATODADOSCERTAME_DATA_TO',pic:'',nv:''},{av:'AV59TFContratoDadosCertame_Modalidade',fld:'vTFCONTRATODADOSCERTAME_MODALIDADE',pic:'',nv:''},{av:'AV60TFContratoDadosCertame_Modalidade_Sel',fld:'vTFCONTRATODADOSCERTAME_MODALIDADE_SEL',pic:'',nv:''},{av:'AV63TFContratoDadosCertame_Numero',fld:'vTFCONTRATODADOSCERTAME_NUMERO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV64TFContratoDadosCertame_Numero_To',fld:'vTFCONTRATODADOSCERTAME_NUMERO_TO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV67TFContratoDadosCertame_Site',fld:'vTFCONTRATODADOSCERTAME_SITE',pic:'',nv:''},{av:'AV68TFContratoDadosCertame_Site_Sel',fld:'vTFCONTRATODADOSCERTAME_SITE_SEL',pic:'',nv:''},{av:'AV71TFContratoDadosCertame_Uasg',fld:'vTFCONTRATODADOSCERTAME_UASG',pic:'ZZZ9',nv:0},{av:'AV72TFContratoDadosCertame_Uasg_To',fld:'vTFCONTRATODADOSCERTAME_UASG_TO',pic:'ZZZ9',nv:0},{av:'AV75TFContratoDadosCertame_DataHomologacao',fld:'vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO',pic:'',nv:''},{av:'AV76TFContratoDadosCertame_DataHomologacao_To',fld:'vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO_TO',pic:'',nv:''},{av:'AV81TFContratoDadosCertame_DataAdjudicacao',fld:'vTFCONTRATODADOSCERTAME_DATAADJUDICACAO',pic:'',nv:''},{av:'AV82TFContratoDadosCertame_DataAdjudicacao_To',fld:'vTFCONTRATODADOSCERTAME_DATAADJUDICACAO_TO',pic:'',nv:''},{av:'AV51ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoDadosCertame_DataTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoDadosCertame_ModalidadeTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_MODALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoDadosCertame_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ContratoDadosCertame_SiteTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_SITETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoDadosCertame_UasgTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_UASGTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratoDadosCertame_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_ContratoDadosCertame_DataAdjudicacaoTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATAADJUDICACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A314ContratoDadosCertame_Codigo',fld:'CONTRATODADOSCERTAME_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A309ContratoDadosCertame_Site',fld:'CONTRATODADOSCERTAME_SITE',pic:'',hsh:true,nv:''},{av:'Ddo_contratodadoscertame_modalidade_Activeeventkey',ctrl:'DDO_CONTRATODADOSCERTAME_MODALIDADE',prop:'ActiveEventKey'},{av:'Ddo_contratodadoscertame_modalidade_Filteredtext_get',ctrl:'DDO_CONTRATODADOSCERTAME_MODALIDADE',prop:'FilteredText_get'},{av:'Ddo_contratodadoscertame_modalidade_Selectedvalue_get',ctrl:'DDO_CONTRATODADOSCERTAME_MODALIDADE',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratodadoscertame_modalidade_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_MODALIDADE',prop:'SortedStatus'},{av:'AV59TFContratoDadosCertame_Modalidade',fld:'vTFCONTRATODADOSCERTAME_MODALIDADE',pic:'',nv:''},{av:'AV60TFContratoDadosCertame_Modalidade_Sel',fld:'vTFCONTRATODADOSCERTAME_MODALIDADE_SEL',pic:'',nv:''},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_data_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_DATA',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_numero_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_site_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_SITE',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_uasg_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_UASG',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_datahomologacao_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_dataadjudicacao_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATODADOSCERTAME_NUMERO.ONOPTIONCLICKED","{handler:'E157G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV37ContratoDadosCertame_Data1',fld:'vCONTRATODADOSCERTAME_DATA1',pic:'',nv:''},{av:'AV38ContratoDadosCertame_Data_To1',fld:'vCONTRATODADOSCERTAME_DATA_TO1',pic:'',nv:''},{av:'AV17ContratoDadosCertame_Modalidade1',fld:'vCONTRATODADOSCERTAME_MODALIDADE1',pic:'',nv:''},{av:'AV39ContratoDadosCertame_Numero1',fld:'vCONTRATODADOSCERTAME_NUMERO1',pic:'ZZZZZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV40ContratoDadosCertame_Data2',fld:'vCONTRATODADOSCERTAME_DATA2',pic:'',nv:''},{av:'AV41ContratoDadosCertame_Data_To2',fld:'vCONTRATODADOSCERTAME_DATA_TO2',pic:'',nv:''},{av:'AV21ContratoDadosCertame_Modalidade2',fld:'vCONTRATODADOSCERTAME_MODALIDADE2',pic:'',nv:''},{av:'AV42ContratoDadosCertame_Numero2',fld:'vCONTRATODADOSCERTAME_NUMERO2',pic:'ZZZZZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV43ContratoDadosCertame_Data3',fld:'vCONTRATODADOSCERTAME_DATA3',pic:'',nv:''},{av:'AV44ContratoDadosCertame_Data_To3',fld:'vCONTRATODADOSCERTAME_DATA_TO3',pic:'',nv:''},{av:'AV25ContratoDadosCertame_Modalidade3',fld:'vCONTRATODADOSCERTAME_MODALIDADE3',pic:'',nv:''},{av:'AV45ContratoDadosCertame_Numero3',fld:'vCONTRATODADOSCERTAME_NUMERO3',pic:'ZZZZZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV50TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV53TFContratoDadosCertame_Data',fld:'vTFCONTRATODADOSCERTAME_DATA',pic:'',nv:''},{av:'AV54TFContratoDadosCertame_Data_To',fld:'vTFCONTRATODADOSCERTAME_DATA_TO',pic:'',nv:''},{av:'AV59TFContratoDadosCertame_Modalidade',fld:'vTFCONTRATODADOSCERTAME_MODALIDADE',pic:'',nv:''},{av:'AV60TFContratoDadosCertame_Modalidade_Sel',fld:'vTFCONTRATODADOSCERTAME_MODALIDADE_SEL',pic:'',nv:''},{av:'AV63TFContratoDadosCertame_Numero',fld:'vTFCONTRATODADOSCERTAME_NUMERO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV64TFContratoDadosCertame_Numero_To',fld:'vTFCONTRATODADOSCERTAME_NUMERO_TO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV67TFContratoDadosCertame_Site',fld:'vTFCONTRATODADOSCERTAME_SITE',pic:'',nv:''},{av:'AV68TFContratoDadosCertame_Site_Sel',fld:'vTFCONTRATODADOSCERTAME_SITE_SEL',pic:'',nv:''},{av:'AV71TFContratoDadosCertame_Uasg',fld:'vTFCONTRATODADOSCERTAME_UASG',pic:'ZZZ9',nv:0},{av:'AV72TFContratoDadosCertame_Uasg_To',fld:'vTFCONTRATODADOSCERTAME_UASG_TO',pic:'ZZZ9',nv:0},{av:'AV75TFContratoDadosCertame_DataHomologacao',fld:'vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO',pic:'',nv:''},{av:'AV76TFContratoDadosCertame_DataHomologacao_To',fld:'vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO_TO',pic:'',nv:''},{av:'AV81TFContratoDadosCertame_DataAdjudicacao',fld:'vTFCONTRATODADOSCERTAME_DATAADJUDICACAO',pic:'',nv:''},{av:'AV82TFContratoDadosCertame_DataAdjudicacao_To',fld:'vTFCONTRATODADOSCERTAME_DATAADJUDICACAO_TO',pic:'',nv:''},{av:'AV51ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoDadosCertame_DataTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoDadosCertame_ModalidadeTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_MODALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoDadosCertame_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ContratoDadosCertame_SiteTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_SITETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoDadosCertame_UasgTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_UASGTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratoDadosCertame_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_ContratoDadosCertame_DataAdjudicacaoTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATAADJUDICACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A314ContratoDadosCertame_Codigo',fld:'CONTRATODADOSCERTAME_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A309ContratoDadosCertame_Site',fld:'CONTRATODADOSCERTAME_SITE',pic:'',hsh:true,nv:''},{av:'Ddo_contratodadoscertame_numero_Activeeventkey',ctrl:'DDO_CONTRATODADOSCERTAME_NUMERO',prop:'ActiveEventKey'},{av:'Ddo_contratodadoscertame_numero_Filteredtext_get',ctrl:'DDO_CONTRATODADOSCERTAME_NUMERO',prop:'FilteredText_get'},{av:'Ddo_contratodadoscertame_numero_Filteredtextto_get',ctrl:'DDO_CONTRATODADOSCERTAME_NUMERO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratodadoscertame_numero_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_NUMERO',prop:'SortedStatus'},{av:'AV63TFContratoDadosCertame_Numero',fld:'vTFCONTRATODADOSCERTAME_NUMERO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV64TFContratoDadosCertame_Numero_To',fld:'vTFCONTRATODADOSCERTAME_NUMERO_TO',pic:'ZZZZZZZZZ9',nv:0},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_data_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_DATA',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_modalidade_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_MODALIDADE',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_site_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_SITE',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_uasg_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_UASG',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_datahomologacao_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_dataadjudicacao_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATODADOSCERTAME_SITE.ONOPTIONCLICKED","{handler:'E167G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV37ContratoDadosCertame_Data1',fld:'vCONTRATODADOSCERTAME_DATA1',pic:'',nv:''},{av:'AV38ContratoDadosCertame_Data_To1',fld:'vCONTRATODADOSCERTAME_DATA_TO1',pic:'',nv:''},{av:'AV17ContratoDadosCertame_Modalidade1',fld:'vCONTRATODADOSCERTAME_MODALIDADE1',pic:'',nv:''},{av:'AV39ContratoDadosCertame_Numero1',fld:'vCONTRATODADOSCERTAME_NUMERO1',pic:'ZZZZZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV40ContratoDadosCertame_Data2',fld:'vCONTRATODADOSCERTAME_DATA2',pic:'',nv:''},{av:'AV41ContratoDadosCertame_Data_To2',fld:'vCONTRATODADOSCERTAME_DATA_TO2',pic:'',nv:''},{av:'AV21ContratoDadosCertame_Modalidade2',fld:'vCONTRATODADOSCERTAME_MODALIDADE2',pic:'',nv:''},{av:'AV42ContratoDadosCertame_Numero2',fld:'vCONTRATODADOSCERTAME_NUMERO2',pic:'ZZZZZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV43ContratoDadosCertame_Data3',fld:'vCONTRATODADOSCERTAME_DATA3',pic:'',nv:''},{av:'AV44ContratoDadosCertame_Data_To3',fld:'vCONTRATODADOSCERTAME_DATA_TO3',pic:'',nv:''},{av:'AV25ContratoDadosCertame_Modalidade3',fld:'vCONTRATODADOSCERTAME_MODALIDADE3',pic:'',nv:''},{av:'AV45ContratoDadosCertame_Numero3',fld:'vCONTRATODADOSCERTAME_NUMERO3',pic:'ZZZZZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV50TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV53TFContratoDadosCertame_Data',fld:'vTFCONTRATODADOSCERTAME_DATA',pic:'',nv:''},{av:'AV54TFContratoDadosCertame_Data_To',fld:'vTFCONTRATODADOSCERTAME_DATA_TO',pic:'',nv:''},{av:'AV59TFContratoDadosCertame_Modalidade',fld:'vTFCONTRATODADOSCERTAME_MODALIDADE',pic:'',nv:''},{av:'AV60TFContratoDadosCertame_Modalidade_Sel',fld:'vTFCONTRATODADOSCERTAME_MODALIDADE_SEL',pic:'',nv:''},{av:'AV63TFContratoDadosCertame_Numero',fld:'vTFCONTRATODADOSCERTAME_NUMERO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV64TFContratoDadosCertame_Numero_To',fld:'vTFCONTRATODADOSCERTAME_NUMERO_TO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV67TFContratoDadosCertame_Site',fld:'vTFCONTRATODADOSCERTAME_SITE',pic:'',nv:''},{av:'AV68TFContratoDadosCertame_Site_Sel',fld:'vTFCONTRATODADOSCERTAME_SITE_SEL',pic:'',nv:''},{av:'AV71TFContratoDadosCertame_Uasg',fld:'vTFCONTRATODADOSCERTAME_UASG',pic:'ZZZ9',nv:0},{av:'AV72TFContratoDadosCertame_Uasg_To',fld:'vTFCONTRATODADOSCERTAME_UASG_TO',pic:'ZZZ9',nv:0},{av:'AV75TFContratoDadosCertame_DataHomologacao',fld:'vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO',pic:'',nv:''},{av:'AV76TFContratoDadosCertame_DataHomologacao_To',fld:'vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO_TO',pic:'',nv:''},{av:'AV81TFContratoDadosCertame_DataAdjudicacao',fld:'vTFCONTRATODADOSCERTAME_DATAADJUDICACAO',pic:'',nv:''},{av:'AV82TFContratoDadosCertame_DataAdjudicacao_To',fld:'vTFCONTRATODADOSCERTAME_DATAADJUDICACAO_TO',pic:'',nv:''},{av:'AV51ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoDadosCertame_DataTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoDadosCertame_ModalidadeTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_MODALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoDadosCertame_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ContratoDadosCertame_SiteTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_SITETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoDadosCertame_UasgTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_UASGTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratoDadosCertame_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_ContratoDadosCertame_DataAdjudicacaoTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATAADJUDICACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A314ContratoDadosCertame_Codigo',fld:'CONTRATODADOSCERTAME_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A309ContratoDadosCertame_Site',fld:'CONTRATODADOSCERTAME_SITE',pic:'',hsh:true,nv:''},{av:'Ddo_contratodadoscertame_site_Activeeventkey',ctrl:'DDO_CONTRATODADOSCERTAME_SITE',prop:'ActiveEventKey'},{av:'Ddo_contratodadoscertame_site_Filteredtext_get',ctrl:'DDO_CONTRATODADOSCERTAME_SITE',prop:'FilteredText_get'},{av:'Ddo_contratodadoscertame_site_Selectedvalue_get',ctrl:'DDO_CONTRATODADOSCERTAME_SITE',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratodadoscertame_site_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_SITE',prop:'SortedStatus'},{av:'AV67TFContratoDadosCertame_Site',fld:'vTFCONTRATODADOSCERTAME_SITE',pic:'',nv:''},{av:'AV68TFContratoDadosCertame_Site_Sel',fld:'vTFCONTRATODADOSCERTAME_SITE_SEL',pic:'',nv:''},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_data_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_DATA',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_modalidade_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_MODALIDADE',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_numero_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_uasg_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_UASG',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_datahomologacao_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_dataadjudicacao_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATODADOSCERTAME_UASG.ONOPTIONCLICKED","{handler:'E177G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV37ContratoDadosCertame_Data1',fld:'vCONTRATODADOSCERTAME_DATA1',pic:'',nv:''},{av:'AV38ContratoDadosCertame_Data_To1',fld:'vCONTRATODADOSCERTAME_DATA_TO1',pic:'',nv:''},{av:'AV17ContratoDadosCertame_Modalidade1',fld:'vCONTRATODADOSCERTAME_MODALIDADE1',pic:'',nv:''},{av:'AV39ContratoDadosCertame_Numero1',fld:'vCONTRATODADOSCERTAME_NUMERO1',pic:'ZZZZZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV40ContratoDadosCertame_Data2',fld:'vCONTRATODADOSCERTAME_DATA2',pic:'',nv:''},{av:'AV41ContratoDadosCertame_Data_To2',fld:'vCONTRATODADOSCERTAME_DATA_TO2',pic:'',nv:''},{av:'AV21ContratoDadosCertame_Modalidade2',fld:'vCONTRATODADOSCERTAME_MODALIDADE2',pic:'',nv:''},{av:'AV42ContratoDadosCertame_Numero2',fld:'vCONTRATODADOSCERTAME_NUMERO2',pic:'ZZZZZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV43ContratoDadosCertame_Data3',fld:'vCONTRATODADOSCERTAME_DATA3',pic:'',nv:''},{av:'AV44ContratoDadosCertame_Data_To3',fld:'vCONTRATODADOSCERTAME_DATA_TO3',pic:'',nv:''},{av:'AV25ContratoDadosCertame_Modalidade3',fld:'vCONTRATODADOSCERTAME_MODALIDADE3',pic:'',nv:''},{av:'AV45ContratoDadosCertame_Numero3',fld:'vCONTRATODADOSCERTAME_NUMERO3',pic:'ZZZZZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV50TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV53TFContratoDadosCertame_Data',fld:'vTFCONTRATODADOSCERTAME_DATA',pic:'',nv:''},{av:'AV54TFContratoDadosCertame_Data_To',fld:'vTFCONTRATODADOSCERTAME_DATA_TO',pic:'',nv:''},{av:'AV59TFContratoDadosCertame_Modalidade',fld:'vTFCONTRATODADOSCERTAME_MODALIDADE',pic:'',nv:''},{av:'AV60TFContratoDadosCertame_Modalidade_Sel',fld:'vTFCONTRATODADOSCERTAME_MODALIDADE_SEL',pic:'',nv:''},{av:'AV63TFContratoDadosCertame_Numero',fld:'vTFCONTRATODADOSCERTAME_NUMERO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV64TFContratoDadosCertame_Numero_To',fld:'vTFCONTRATODADOSCERTAME_NUMERO_TO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV67TFContratoDadosCertame_Site',fld:'vTFCONTRATODADOSCERTAME_SITE',pic:'',nv:''},{av:'AV68TFContratoDadosCertame_Site_Sel',fld:'vTFCONTRATODADOSCERTAME_SITE_SEL',pic:'',nv:''},{av:'AV71TFContratoDadosCertame_Uasg',fld:'vTFCONTRATODADOSCERTAME_UASG',pic:'ZZZ9',nv:0},{av:'AV72TFContratoDadosCertame_Uasg_To',fld:'vTFCONTRATODADOSCERTAME_UASG_TO',pic:'ZZZ9',nv:0},{av:'AV75TFContratoDadosCertame_DataHomologacao',fld:'vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO',pic:'',nv:''},{av:'AV76TFContratoDadosCertame_DataHomologacao_To',fld:'vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO_TO',pic:'',nv:''},{av:'AV81TFContratoDadosCertame_DataAdjudicacao',fld:'vTFCONTRATODADOSCERTAME_DATAADJUDICACAO',pic:'',nv:''},{av:'AV82TFContratoDadosCertame_DataAdjudicacao_To',fld:'vTFCONTRATODADOSCERTAME_DATAADJUDICACAO_TO',pic:'',nv:''},{av:'AV51ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoDadosCertame_DataTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoDadosCertame_ModalidadeTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_MODALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoDadosCertame_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ContratoDadosCertame_SiteTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_SITETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoDadosCertame_UasgTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_UASGTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratoDadosCertame_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_ContratoDadosCertame_DataAdjudicacaoTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATAADJUDICACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A314ContratoDadosCertame_Codigo',fld:'CONTRATODADOSCERTAME_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A309ContratoDadosCertame_Site',fld:'CONTRATODADOSCERTAME_SITE',pic:'',hsh:true,nv:''},{av:'Ddo_contratodadoscertame_uasg_Activeeventkey',ctrl:'DDO_CONTRATODADOSCERTAME_UASG',prop:'ActiveEventKey'},{av:'Ddo_contratodadoscertame_uasg_Filteredtext_get',ctrl:'DDO_CONTRATODADOSCERTAME_UASG',prop:'FilteredText_get'},{av:'Ddo_contratodadoscertame_uasg_Filteredtextto_get',ctrl:'DDO_CONTRATODADOSCERTAME_UASG',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratodadoscertame_uasg_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_UASG',prop:'SortedStatus'},{av:'AV71TFContratoDadosCertame_Uasg',fld:'vTFCONTRATODADOSCERTAME_UASG',pic:'ZZZ9',nv:0},{av:'AV72TFContratoDadosCertame_Uasg_To',fld:'vTFCONTRATODADOSCERTAME_UASG_TO',pic:'ZZZ9',nv:0},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_data_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_DATA',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_modalidade_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_MODALIDADE',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_numero_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_site_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_SITE',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_datahomologacao_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_dataadjudicacao_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO.ONOPTIONCLICKED","{handler:'E187G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV37ContratoDadosCertame_Data1',fld:'vCONTRATODADOSCERTAME_DATA1',pic:'',nv:''},{av:'AV38ContratoDadosCertame_Data_To1',fld:'vCONTRATODADOSCERTAME_DATA_TO1',pic:'',nv:''},{av:'AV17ContratoDadosCertame_Modalidade1',fld:'vCONTRATODADOSCERTAME_MODALIDADE1',pic:'',nv:''},{av:'AV39ContratoDadosCertame_Numero1',fld:'vCONTRATODADOSCERTAME_NUMERO1',pic:'ZZZZZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV40ContratoDadosCertame_Data2',fld:'vCONTRATODADOSCERTAME_DATA2',pic:'',nv:''},{av:'AV41ContratoDadosCertame_Data_To2',fld:'vCONTRATODADOSCERTAME_DATA_TO2',pic:'',nv:''},{av:'AV21ContratoDadosCertame_Modalidade2',fld:'vCONTRATODADOSCERTAME_MODALIDADE2',pic:'',nv:''},{av:'AV42ContratoDadosCertame_Numero2',fld:'vCONTRATODADOSCERTAME_NUMERO2',pic:'ZZZZZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV43ContratoDadosCertame_Data3',fld:'vCONTRATODADOSCERTAME_DATA3',pic:'',nv:''},{av:'AV44ContratoDadosCertame_Data_To3',fld:'vCONTRATODADOSCERTAME_DATA_TO3',pic:'',nv:''},{av:'AV25ContratoDadosCertame_Modalidade3',fld:'vCONTRATODADOSCERTAME_MODALIDADE3',pic:'',nv:''},{av:'AV45ContratoDadosCertame_Numero3',fld:'vCONTRATODADOSCERTAME_NUMERO3',pic:'ZZZZZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV50TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV53TFContratoDadosCertame_Data',fld:'vTFCONTRATODADOSCERTAME_DATA',pic:'',nv:''},{av:'AV54TFContratoDadosCertame_Data_To',fld:'vTFCONTRATODADOSCERTAME_DATA_TO',pic:'',nv:''},{av:'AV59TFContratoDadosCertame_Modalidade',fld:'vTFCONTRATODADOSCERTAME_MODALIDADE',pic:'',nv:''},{av:'AV60TFContratoDadosCertame_Modalidade_Sel',fld:'vTFCONTRATODADOSCERTAME_MODALIDADE_SEL',pic:'',nv:''},{av:'AV63TFContratoDadosCertame_Numero',fld:'vTFCONTRATODADOSCERTAME_NUMERO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV64TFContratoDadosCertame_Numero_To',fld:'vTFCONTRATODADOSCERTAME_NUMERO_TO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV67TFContratoDadosCertame_Site',fld:'vTFCONTRATODADOSCERTAME_SITE',pic:'',nv:''},{av:'AV68TFContratoDadosCertame_Site_Sel',fld:'vTFCONTRATODADOSCERTAME_SITE_SEL',pic:'',nv:''},{av:'AV71TFContratoDadosCertame_Uasg',fld:'vTFCONTRATODADOSCERTAME_UASG',pic:'ZZZ9',nv:0},{av:'AV72TFContratoDadosCertame_Uasg_To',fld:'vTFCONTRATODADOSCERTAME_UASG_TO',pic:'ZZZ9',nv:0},{av:'AV75TFContratoDadosCertame_DataHomologacao',fld:'vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO',pic:'',nv:''},{av:'AV76TFContratoDadosCertame_DataHomologacao_To',fld:'vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO_TO',pic:'',nv:''},{av:'AV81TFContratoDadosCertame_DataAdjudicacao',fld:'vTFCONTRATODADOSCERTAME_DATAADJUDICACAO',pic:'',nv:''},{av:'AV82TFContratoDadosCertame_DataAdjudicacao_To',fld:'vTFCONTRATODADOSCERTAME_DATAADJUDICACAO_TO',pic:'',nv:''},{av:'AV51ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoDadosCertame_DataTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoDadosCertame_ModalidadeTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_MODALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoDadosCertame_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ContratoDadosCertame_SiteTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_SITETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoDadosCertame_UasgTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_UASGTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratoDadosCertame_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_ContratoDadosCertame_DataAdjudicacaoTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATAADJUDICACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A314ContratoDadosCertame_Codigo',fld:'CONTRATODADOSCERTAME_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A309ContratoDadosCertame_Site',fld:'CONTRATODADOSCERTAME_SITE',pic:'',hsh:true,nv:''},{av:'Ddo_contratodadoscertame_datahomologacao_Activeeventkey',ctrl:'DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO',prop:'ActiveEventKey'},{av:'Ddo_contratodadoscertame_datahomologacao_Filteredtext_get',ctrl:'DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO',prop:'FilteredText_get'},{av:'Ddo_contratodadoscertame_datahomologacao_Filteredtextto_get',ctrl:'DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratodadoscertame_datahomologacao_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO',prop:'SortedStatus'},{av:'AV75TFContratoDadosCertame_DataHomologacao',fld:'vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO',pic:'',nv:''},{av:'AV76TFContratoDadosCertame_DataHomologacao_To',fld:'vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO_TO',pic:'',nv:''},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_data_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_DATA',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_modalidade_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_MODALIDADE',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_numero_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_site_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_SITE',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_uasg_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_UASG',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_dataadjudicacao_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO.ONOPTIONCLICKED","{handler:'E197G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV37ContratoDadosCertame_Data1',fld:'vCONTRATODADOSCERTAME_DATA1',pic:'',nv:''},{av:'AV38ContratoDadosCertame_Data_To1',fld:'vCONTRATODADOSCERTAME_DATA_TO1',pic:'',nv:''},{av:'AV17ContratoDadosCertame_Modalidade1',fld:'vCONTRATODADOSCERTAME_MODALIDADE1',pic:'',nv:''},{av:'AV39ContratoDadosCertame_Numero1',fld:'vCONTRATODADOSCERTAME_NUMERO1',pic:'ZZZZZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV40ContratoDadosCertame_Data2',fld:'vCONTRATODADOSCERTAME_DATA2',pic:'',nv:''},{av:'AV41ContratoDadosCertame_Data_To2',fld:'vCONTRATODADOSCERTAME_DATA_TO2',pic:'',nv:''},{av:'AV21ContratoDadosCertame_Modalidade2',fld:'vCONTRATODADOSCERTAME_MODALIDADE2',pic:'',nv:''},{av:'AV42ContratoDadosCertame_Numero2',fld:'vCONTRATODADOSCERTAME_NUMERO2',pic:'ZZZZZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV43ContratoDadosCertame_Data3',fld:'vCONTRATODADOSCERTAME_DATA3',pic:'',nv:''},{av:'AV44ContratoDadosCertame_Data_To3',fld:'vCONTRATODADOSCERTAME_DATA_TO3',pic:'',nv:''},{av:'AV25ContratoDadosCertame_Modalidade3',fld:'vCONTRATODADOSCERTAME_MODALIDADE3',pic:'',nv:''},{av:'AV45ContratoDadosCertame_Numero3',fld:'vCONTRATODADOSCERTAME_NUMERO3',pic:'ZZZZZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV50TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV53TFContratoDadosCertame_Data',fld:'vTFCONTRATODADOSCERTAME_DATA',pic:'',nv:''},{av:'AV54TFContratoDadosCertame_Data_To',fld:'vTFCONTRATODADOSCERTAME_DATA_TO',pic:'',nv:''},{av:'AV59TFContratoDadosCertame_Modalidade',fld:'vTFCONTRATODADOSCERTAME_MODALIDADE',pic:'',nv:''},{av:'AV60TFContratoDadosCertame_Modalidade_Sel',fld:'vTFCONTRATODADOSCERTAME_MODALIDADE_SEL',pic:'',nv:''},{av:'AV63TFContratoDadosCertame_Numero',fld:'vTFCONTRATODADOSCERTAME_NUMERO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV64TFContratoDadosCertame_Numero_To',fld:'vTFCONTRATODADOSCERTAME_NUMERO_TO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV67TFContratoDadosCertame_Site',fld:'vTFCONTRATODADOSCERTAME_SITE',pic:'',nv:''},{av:'AV68TFContratoDadosCertame_Site_Sel',fld:'vTFCONTRATODADOSCERTAME_SITE_SEL',pic:'',nv:''},{av:'AV71TFContratoDadosCertame_Uasg',fld:'vTFCONTRATODADOSCERTAME_UASG',pic:'ZZZ9',nv:0},{av:'AV72TFContratoDadosCertame_Uasg_To',fld:'vTFCONTRATODADOSCERTAME_UASG_TO',pic:'ZZZ9',nv:0},{av:'AV75TFContratoDadosCertame_DataHomologacao',fld:'vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO',pic:'',nv:''},{av:'AV76TFContratoDadosCertame_DataHomologacao_To',fld:'vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO_TO',pic:'',nv:''},{av:'AV81TFContratoDadosCertame_DataAdjudicacao',fld:'vTFCONTRATODADOSCERTAME_DATAADJUDICACAO',pic:'',nv:''},{av:'AV82TFContratoDadosCertame_DataAdjudicacao_To',fld:'vTFCONTRATODADOSCERTAME_DATAADJUDICACAO_TO',pic:'',nv:''},{av:'AV51ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoDadosCertame_DataTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoDadosCertame_ModalidadeTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_MODALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoDadosCertame_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ContratoDadosCertame_SiteTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_SITETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoDadosCertame_UasgTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_UASGTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratoDadosCertame_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_ContratoDadosCertame_DataAdjudicacaoTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATAADJUDICACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A314ContratoDadosCertame_Codigo',fld:'CONTRATODADOSCERTAME_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A309ContratoDadosCertame_Site',fld:'CONTRATODADOSCERTAME_SITE',pic:'',hsh:true,nv:''},{av:'Ddo_contratodadoscertame_dataadjudicacao_Activeeventkey',ctrl:'DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO',prop:'ActiveEventKey'},{av:'Ddo_contratodadoscertame_dataadjudicacao_Filteredtext_get',ctrl:'DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO',prop:'FilteredText_get'},{av:'Ddo_contratodadoscertame_dataadjudicacao_Filteredtextto_get',ctrl:'DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratodadoscertame_dataadjudicacao_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO',prop:'SortedStatus'},{av:'AV81TFContratoDadosCertame_DataAdjudicacao',fld:'vTFCONTRATODADOSCERTAME_DATAADJUDICACAO',pic:'',nv:''},{av:'AV82TFContratoDadosCertame_DataAdjudicacao_To',fld:'vTFCONTRATODADOSCERTAME_DATAADJUDICACAO_TO',pic:'',nv:''},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_data_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_DATA',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_modalidade_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_MODALIDADE',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_numero_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_site_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_SITE',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_uasg_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_UASG',prop:'SortedStatus'},{av:'Ddo_contratodadoscertame_datahomologacao_Sortedstatus',ctrl:'DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E337G2',iparms:[{av:'A314ContratoDadosCertame_Codigo',fld:'CONTRATODADOSCERTAME_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A309ContratoDadosCertame_Site',fld:'CONTRATODADOSCERTAME_SITE',pic:'',hsh:true,nv:''}],oparms:[{av:'AV28Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV29Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'edtContratoDadosCertame_Modalidade_Link',ctrl:'CONTRATODADOSCERTAME_MODALIDADE',prop:'Link'},{av:'edtContratoDadosCertame_Site_Linktarget',ctrl:'CONTRATODADOSCERTAME_SITE',prop:'Linktarget'},{av:'edtContratoDadosCertame_Site_Link',ctrl:'CONTRATODADOSCERTAME_SITE',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E207G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV37ContratoDadosCertame_Data1',fld:'vCONTRATODADOSCERTAME_DATA1',pic:'',nv:''},{av:'AV38ContratoDadosCertame_Data_To1',fld:'vCONTRATODADOSCERTAME_DATA_TO1',pic:'',nv:''},{av:'AV17ContratoDadosCertame_Modalidade1',fld:'vCONTRATODADOSCERTAME_MODALIDADE1',pic:'',nv:''},{av:'AV39ContratoDadosCertame_Numero1',fld:'vCONTRATODADOSCERTAME_NUMERO1',pic:'ZZZZZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV40ContratoDadosCertame_Data2',fld:'vCONTRATODADOSCERTAME_DATA2',pic:'',nv:''},{av:'AV41ContratoDadosCertame_Data_To2',fld:'vCONTRATODADOSCERTAME_DATA_TO2',pic:'',nv:''},{av:'AV21ContratoDadosCertame_Modalidade2',fld:'vCONTRATODADOSCERTAME_MODALIDADE2',pic:'',nv:''},{av:'AV42ContratoDadosCertame_Numero2',fld:'vCONTRATODADOSCERTAME_NUMERO2',pic:'ZZZZZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV43ContratoDadosCertame_Data3',fld:'vCONTRATODADOSCERTAME_DATA3',pic:'',nv:''},{av:'AV44ContratoDadosCertame_Data_To3',fld:'vCONTRATODADOSCERTAME_DATA_TO3',pic:'',nv:''},{av:'AV25ContratoDadosCertame_Modalidade3',fld:'vCONTRATODADOSCERTAME_MODALIDADE3',pic:'',nv:''},{av:'AV45ContratoDadosCertame_Numero3',fld:'vCONTRATODADOSCERTAME_NUMERO3',pic:'ZZZZZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV50TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV53TFContratoDadosCertame_Data',fld:'vTFCONTRATODADOSCERTAME_DATA',pic:'',nv:''},{av:'AV54TFContratoDadosCertame_Data_To',fld:'vTFCONTRATODADOSCERTAME_DATA_TO',pic:'',nv:''},{av:'AV59TFContratoDadosCertame_Modalidade',fld:'vTFCONTRATODADOSCERTAME_MODALIDADE',pic:'',nv:''},{av:'AV60TFContratoDadosCertame_Modalidade_Sel',fld:'vTFCONTRATODADOSCERTAME_MODALIDADE_SEL',pic:'',nv:''},{av:'AV63TFContratoDadosCertame_Numero',fld:'vTFCONTRATODADOSCERTAME_NUMERO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV64TFContratoDadosCertame_Numero_To',fld:'vTFCONTRATODADOSCERTAME_NUMERO_TO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV67TFContratoDadosCertame_Site',fld:'vTFCONTRATODADOSCERTAME_SITE',pic:'',nv:''},{av:'AV68TFContratoDadosCertame_Site_Sel',fld:'vTFCONTRATODADOSCERTAME_SITE_SEL',pic:'',nv:''},{av:'AV71TFContratoDadosCertame_Uasg',fld:'vTFCONTRATODADOSCERTAME_UASG',pic:'ZZZ9',nv:0},{av:'AV72TFContratoDadosCertame_Uasg_To',fld:'vTFCONTRATODADOSCERTAME_UASG_TO',pic:'ZZZ9',nv:0},{av:'AV75TFContratoDadosCertame_DataHomologacao',fld:'vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO',pic:'',nv:''},{av:'AV76TFContratoDadosCertame_DataHomologacao_To',fld:'vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO_TO',pic:'',nv:''},{av:'AV81TFContratoDadosCertame_DataAdjudicacao',fld:'vTFCONTRATODADOSCERTAME_DATAADJUDICACAO',pic:'',nv:''},{av:'AV82TFContratoDadosCertame_DataAdjudicacao_To',fld:'vTFCONTRATODADOSCERTAME_DATAADJUDICACAO_TO',pic:'',nv:''},{av:'AV51ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoDadosCertame_DataTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoDadosCertame_ModalidadeTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_MODALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoDadosCertame_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ContratoDadosCertame_SiteTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_SITETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoDadosCertame_UasgTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_UASGTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratoDadosCertame_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_ContratoDadosCertame_DataAdjudicacaoTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATAADJUDICACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A314ContratoDadosCertame_Codigo',fld:'CONTRATODADOSCERTAME_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A309ContratoDadosCertame_Site',fld:'CONTRATODADOSCERTAME_SITE',pic:'',hsh:true,nv:''}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E267G2',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E217G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV37ContratoDadosCertame_Data1',fld:'vCONTRATODADOSCERTAME_DATA1',pic:'',nv:''},{av:'AV38ContratoDadosCertame_Data_To1',fld:'vCONTRATODADOSCERTAME_DATA_TO1',pic:'',nv:''},{av:'AV17ContratoDadosCertame_Modalidade1',fld:'vCONTRATODADOSCERTAME_MODALIDADE1',pic:'',nv:''},{av:'AV39ContratoDadosCertame_Numero1',fld:'vCONTRATODADOSCERTAME_NUMERO1',pic:'ZZZZZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV40ContratoDadosCertame_Data2',fld:'vCONTRATODADOSCERTAME_DATA2',pic:'',nv:''},{av:'AV41ContratoDadosCertame_Data_To2',fld:'vCONTRATODADOSCERTAME_DATA_TO2',pic:'',nv:''},{av:'AV21ContratoDadosCertame_Modalidade2',fld:'vCONTRATODADOSCERTAME_MODALIDADE2',pic:'',nv:''},{av:'AV42ContratoDadosCertame_Numero2',fld:'vCONTRATODADOSCERTAME_NUMERO2',pic:'ZZZZZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV43ContratoDadosCertame_Data3',fld:'vCONTRATODADOSCERTAME_DATA3',pic:'',nv:''},{av:'AV44ContratoDadosCertame_Data_To3',fld:'vCONTRATODADOSCERTAME_DATA_TO3',pic:'',nv:''},{av:'AV25ContratoDadosCertame_Modalidade3',fld:'vCONTRATODADOSCERTAME_MODALIDADE3',pic:'',nv:''},{av:'AV45ContratoDadosCertame_Numero3',fld:'vCONTRATODADOSCERTAME_NUMERO3',pic:'ZZZZZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV50TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV53TFContratoDadosCertame_Data',fld:'vTFCONTRATODADOSCERTAME_DATA',pic:'',nv:''},{av:'AV54TFContratoDadosCertame_Data_To',fld:'vTFCONTRATODADOSCERTAME_DATA_TO',pic:'',nv:''},{av:'AV59TFContratoDadosCertame_Modalidade',fld:'vTFCONTRATODADOSCERTAME_MODALIDADE',pic:'',nv:''},{av:'AV60TFContratoDadosCertame_Modalidade_Sel',fld:'vTFCONTRATODADOSCERTAME_MODALIDADE_SEL',pic:'',nv:''},{av:'AV63TFContratoDadosCertame_Numero',fld:'vTFCONTRATODADOSCERTAME_NUMERO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV64TFContratoDadosCertame_Numero_To',fld:'vTFCONTRATODADOSCERTAME_NUMERO_TO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV67TFContratoDadosCertame_Site',fld:'vTFCONTRATODADOSCERTAME_SITE',pic:'',nv:''},{av:'AV68TFContratoDadosCertame_Site_Sel',fld:'vTFCONTRATODADOSCERTAME_SITE_SEL',pic:'',nv:''},{av:'AV71TFContratoDadosCertame_Uasg',fld:'vTFCONTRATODADOSCERTAME_UASG',pic:'ZZZ9',nv:0},{av:'AV72TFContratoDadosCertame_Uasg_To',fld:'vTFCONTRATODADOSCERTAME_UASG_TO',pic:'ZZZ9',nv:0},{av:'AV75TFContratoDadosCertame_DataHomologacao',fld:'vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO',pic:'',nv:''},{av:'AV76TFContratoDadosCertame_DataHomologacao_To',fld:'vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO_TO',pic:'',nv:''},{av:'AV81TFContratoDadosCertame_DataAdjudicacao',fld:'vTFCONTRATODADOSCERTAME_DATAADJUDICACAO',pic:'',nv:''},{av:'AV82TFContratoDadosCertame_DataAdjudicacao_To',fld:'vTFCONTRATODADOSCERTAME_DATAADJUDICACAO_TO',pic:'',nv:''},{av:'AV51ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoDadosCertame_DataTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoDadosCertame_ModalidadeTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_MODALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoDadosCertame_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ContratoDadosCertame_SiteTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_SITETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoDadosCertame_UasgTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_UASGTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratoDadosCertame_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_ContratoDadosCertame_DataAdjudicacaoTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATAADJUDICACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A314ContratoDadosCertame_Codigo',fld:'CONTRATODADOSCERTAME_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A309ContratoDadosCertame_Site',fld:'CONTRATODADOSCERTAME_SITE',pic:'',hsh:true,nv:''}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40ContratoDadosCertame_Data2',fld:'vCONTRATODADOSCERTAME_DATA2',pic:'',nv:''},{av:'AV41ContratoDadosCertame_Data_To2',fld:'vCONTRATODADOSCERTAME_DATA_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV43ContratoDadosCertame_Data3',fld:'vCONTRATODADOSCERTAME_DATA3',pic:'',nv:''},{av:'AV44ContratoDadosCertame_Data_To3',fld:'vCONTRATODADOSCERTAME_DATA_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cmbavDynamicfiltersoperator3'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV37ContratoDadosCertame_Data1',fld:'vCONTRATODADOSCERTAME_DATA1',pic:'',nv:''},{av:'AV38ContratoDadosCertame_Data_To1',fld:'vCONTRATODADOSCERTAME_DATA_TO1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoDadosCertame_Modalidade1',fld:'vCONTRATODADOSCERTAME_MODALIDADE1',pic:'',nv:''},{av:'AV39ContratoDadosCertame_Numero1',fld:'vCONTRATODADOSCERTAME_NUMERO1',pic:'ZZZZZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoDadosCertame_Modalidade2',fld:'vCONTRATODADOSCERTAME_MODALIDADE2',pic:'',nv:''},{av:'AV42ContratoDadosCertame_Numero2',fld:'vCONTRATODADOSCERTAME_NUMERO2',pic:'ZZZZZZZZZ9',nv:0},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoDadosCertame_Modalidade3',fld:'vCONTRATODADOSCERTAME_MODALIDADE3',pic:'',nv:''},{av:'AV45ContratoDadosCertame_Numero3',fld:'vCONTRATODADOSCERTAME_NUMERO3',pic:'ZZZZZZZZZ9',nv:0},{av:'tblTablemergeddynamicfilterscontratodadoscertame_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATODADOSCERTAME_DATA2',prop:'Visible'},{av:'edtavContratodadoscertame_modalidade2_Visible',ctrl:'vCONTRATODADOSCERTAME_MODALIDADE2',prop:'Visible'},{av:'edtavContratodadoscertame_numero2_Visible',ctrl:'vCONTRATODADOSCERTAME_NUMERO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratodadoscertame_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATODADOSCERTAME_DATA3',prop:'Visible'},{av:'edtavContratodadoscertame_modalidade3_Visible',ctrl:'vCONTRATODADOSCERTAME_MODALIDADE3',prop:'Visible'},{av:'edtavContratodadoscertame_numero3_Visible',ctrl:'vCONTRATODADOSCERTAME_NUMERO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratodadoscertame_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATODADOSCERTAME_DATA1',prop:'Visible'},{av:'edtavContratodadoscertame_modalidade1_Visible',ctrl:'vCONTRATODADOSCERTAME_MODALIDADE1',prop:'Visible'},{av:'edtavContratodadoscertame_numero1_Visible',ctrl:'vCONTRATODADOSCERTAME_NUMERO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E277G2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'tblTablemergeddynamicfilterscontratodadoscertame_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATODADOSCERTAME_DATA1',prop:'Visible'},{av:'edtavContratodadoscertame_modalidade1_Visible',ctrl:'vCONTRATODADOSCERTAME_MODALIDADE1',prop:'Visible'},{av:'edtavContratodadoscertame_numero1_Visible',ctrl:'vCONTRATODADOSCERTAME_NUMERO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E287G2',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E227G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV37ContratoDadosCertame_Data1',fld:'vCONTRATODADOSCERTAME_DATA1',pic:'',nv:''},{av:'AV38ContratoDadosCertame_Data_To1',fld:'vCONTRATODADOSCERTAME_DATA_TO1',pic:'',nv:''},{av:'AV17ContratoDadosCertame_Modalidade1',fld:'vCONTRATODADOSCERTAME_MODALIDADE1',pic:'',nv:''},{av:'AV39ContratoDadosCertame_Numero1',fld:'vCONTRATODADOSCERTAME_NUMERO1',pic:'ZZZZZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV40ContratoDadosCertame_Data2',fld:'vCONTRATODADOSCERTAME_DATA2',pic:'',nv:''},{av:'AV41ContratoDadosCertame_Data_To2',fld:'vCONTRATODADOSCERTAME_DATA_TO2',pic:'',nv:''},{av:'AV21ContratoDadosCertame_Modalidade2',fld:'vCONTRATODADOSCERTAME_MODALIDADE2',pic:'',nv:''},{av:'AV42ContratoDadosCertame_Numero2',fld:'vCONTRATODADOSCERTAME_NUMERO2',pic:'ZZZZZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV43ContratoDadosCertame_Data3',fld:'vCONTRATODADOSCERTAME_DATA3',pic:'',nv:''},{av:'AV44ContratoDadosCertame_Data_To3',fld:'vCONTRATODADOSCERTAME_DATA_TO3',pic:'',nv:''},{av:'AV25ContratoDadosCertame_Modalidade3',fld:'vCONTRATODADOSCERTAME_MODALIDADE3',pic:'',nv:''},{av:'AV45ContratoDadosCertame_Numero3',fld:'vCONTRATODADOSCERTAME_NUMERO3',pic:'ZZZZZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV50TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV53TFContratoDadosCertame_Data',fld:'vTFCONTRATODADOSCERTAME_DATA',pic:'',nv:''},{av:'AV54TFContratoDadosCertame_Data_To',fld:'vTFCONTRATODADOSCERTAME_DATA_TO',pic:'',nv:''},{av:'AV59TFContratoDadosCertame_Modalidade',fld:'vTFCONTRATODADOSCERTAME_MODALIDADE',pic:'',nv:''},{av:'AV60TFContratoDadosCertame_Modalidade_Sel',fld:'vTFCONTRATODADOSCERTAME_MODALIDADE_SEL',pic:'',nv:''},{av:'AV63TFContratoDadosCertame_Numero',fld:'vTFCONTRATODADOSCERTAME_NUMERO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV64TFContratoDadosCertame_Numero_To',fld:'vTFCONTRATODADOSCERTAME_NUMERO_TO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV67TFContratoDadosCertame_Site',fld:'vTFCONTRATODADOSCERTAME_SITE',pic:'',nv:''},{av:'AV68TFContratoDadosCertame_Site_Sel',fld:'vTFCONTRATODADOSCERTAME_SITE_SEL',pic:'',nv:''},{av:'AV71TFContratoDadosCertame_Uasg',fld:'vTFCONTRATODADOSCERTAME_UASG',pic:'ZZZ9',nv:0},{av:'AV72TFContratoDadosCertame_Uasg_To',fld:'vTFCONTRATODADOSCERTAME_UASG_TO',pic:'ZZZ9',nv:0},{av:'AV75TFContratoDadosCertame_DataHomologacao',fld:'vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO',pic:'',nv:''},{av:'AV76TFContratoDadosCertame_DataHomologacao_To',fld:'vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO_TO',pic:'',nv:''},{av:'AV81TFContratoDadosCertame_DataAdjudicacao',fld:'vTFCONTRATODADOSCERTAME_DATAADJUDICACAO',pic:'',nv:''},{av:'AV82TFContratoDadosCertame_DataAdjudicacao_To',fld:'vTFCONTRATODADOSCERTAME_DATAADJUDICACAO_TO',pic:'',nv:''},{av:'AV51ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoDadosCertame_DataTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoDadosCertame_ModalidadeTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_MODALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoDadosCertame_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ContratoDadosCertame_SiteTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_SITETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoDadosCertame_UasgTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_UASGTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratoDadosCertame_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_ContratoDadosCertame_DataAdjudicacaoTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATAADJUDICACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A314ContratoDadosCertame_Codigo',fld:'CONTRATODADOSCERTAME_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A309ContratoDadosCertame_Site',fld:'CONTRATODADOSCERTAME_SITE',pic:'',hsh:true,nv:''}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40ContratoDadosCertame_Data2',fld:'vCONTRATODADOSCERTAME_DATA2',pic:'',nv:''},{av:'AV41ContratoDadosCertame_Data_To2',fld:'vCONTRATODADOSCERTAME_DATA_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV43ContratoDadosCertame_Data3',fld:'vCONTRATODADOSCERTAME_DATA3',pic:'',nv:''},{av:'AV44ContratoDadosCertame_Data_To3',fld:'vCONTRATODADOSCERTAME_DATA_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cmbavDynamicfiltersoperator3'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV37ContratoDadosCertame_Data1',fld:'vCONTRATODADOSCERTAME_DATA1',pic:'',nv:''},{av:'AV38ContratoDadosCertame_Data_To1',fld:'vCONTRATODADOSCERTAME_DATA_TO1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoDadosCertame_Modalidade1',fld:'vCONTRATODADOSCERTAME_MODALIDADE1',pic:'',nv:''},{av:'AV39ContratoDadosCertame_Numero1',fld:'vCONTRATODADOSCERTAME_NUMERO1',pic:'ZZZZZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoDadosCertame_Modalidade2',fld:'vCONTRATODADOSCERTAME_MODALIDADE2',pic:'',nv:''},{av:'AV42ContratoDadosCertame_Numero2',fld:'vCONTRATODADOSCERTAME_NUMERO2',pic:'ZZZZZZZZZ9',nv:0},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoDadosCertame_Modalidade3',fld:'vCONTRATODADOSCERTAME_MODALIDADE3',pic:'',nv:''},{av:'AV45ContratoDadosCertame_Numero3',fld:'vCONTRATODADOSCERTAME_NUMERO3',pic:'ZZZZZZZZZ9',nv:0},{av:'tblTablemergeddynamicfilterscontratodadoscertame_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATODADOSCERTAME_DATA2',prop:'Visible'},{av:'edtavContratodadoscertame_modalidade2_Visible',ctrl:'vCONTRATODADOSCERTAME_MODALIDADE2',prop:'Visible'},{av:'edtavContratodadoscertame_numero2_Visible',ctrl:'vCONTRATODADOSCERTAME_NUMERO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratodadoscertame_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATODADOSCERTAME_DATA3',prop:'Visible'},{av:'edtavContratodadoscertame_modalidade3_Visible',ctrl:'vCONTRATODADOSCERTAME_MODALIDADE3',prop:'Visible'},{av:'edtavContratodadoscertame_numero3_Visible',ctrl:'vCONTRATODADOSCERTAME_NUMERO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratodadoscertame_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATODADOSCERTAME_DATA1',prop:'Visible'},{av:'edtavContratodadoscertame_modalidade1_Visible',ctrl:'vCONTRATODADOSCERTAME_MODALIDADE1',prop:'Visible'},{av:'edtavContratodadoscertame_numero1_Visible',ctrl:'vCONTRATODADOSCERTAME_NUMERO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E297G2',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'tblTablemergeddynamicfilterscontratodadoscertame_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATODADOSCERTAME_DATA2',prop:'Visible'},{av:'edtavContratodadoscertame_modalidade2_Visible',ctrl:'vCONTRATODADOSCERTAME_MODALIDADE2',prop:'Visible'},{av:'edtavContratodadoscertame_numero2_Visible',ctrl:'vCONTRATODADOSCERTAME_NUMERO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E237G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV37ContratoDadosCertame_Data1',fld:'vCONTRATODADOSCERTAME_DATA1',pic:'',nv:''},{av:'AV38ContratoDadosCertame_Data_To1',fld:'vCONTRATODADOSCERTAME_DATA_TO1',pic:'',nv:''},{av:'AV17ContratoDadosCertame_Modalidade1',fld:'vCONTRATODADOSCERTAME_MODALIDADE1',pic:'',nv:''},{av:'AV39ContratoDadosCertame_Numero1',fld:'vCONTRATODADOSCERTAME_NUMERO1',pic:'ZZZZZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV40ContratoDadosCertame_Data2',fld:'vCONTRATODADOSCERTAME_DATA2',pic:'',nv:''},{av:'AV41ContratoDadosCertame_Data_To2',fld:'vCONTRATODADOSCERTAME_DATA_TO2',pic:'',nv:''},{av:'AV21ContratoDadosCertame_Modalidade2',fld:'vCONTRATODADOSCERTAME_MODALIDADE2',pic:'',nv:''},{av:'AV42ContratoDadosCertame_Numero2',fld:'vCONTRATODADOSCERTAME_NUMERO2',pic:'ZZZZZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV43ContratoDadosCertame_Data3',fld:'vCONTRATODADOSCERTAME_DATA3',pic:'',nv:''},{av:'AV44ContratoDadosCertame_Data_To3',fld:'vCONTRATODADOSCERTAME_DATA_TO3',pic:'',nv:''},{av:'AV25ContratoDadosCertame_Modalidade3',fld:'vCONTRATODADOSCERTAME_MODALIDADE3',pic:'',nv:''},{av:'AV45ContratoDadosCertame_Numero3',fld:'vCONTRATODADOSCERTAME_NUMERO3',pic:'ZZZZZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV50TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV53TFContratoDadosCertame_Data',fld:'vTFCONTRATODADOSCERTAME_DATA',pic:'',nv:''},{av:'AV54TFContratoDadosCertame_Data_To',fld:'vTFCONTRATODADOSCERTAME_DATA_TO',pic:'',nv:''},{av:'AV59TFContratoDadosCertame_Modalidade',fld:'vTFCONTRATODADOSCERTAME_MODALIDADE',pic:'',nv:''},{av:'AV60TFContratoDadosCertame_Modalidade_Sel',fld:'vTFCONTRATODADOSCERTAME_MODALIDADE_SEL',pic:'',nv:''},{av:'AV63TFContratoDadosCertame_Numero',fld:'vTFCONTRATODADOSCERTAME_NUMERO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV64TFContratoDadosCertame_Numero_To',fld:'vTFCONTRATODADOSCERTAME_NUMERO_TO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV67TFContratoDadosCertame_Site',fld:'vTFCONTRATODADOSCERTAME_SITE',pic:'',nv:''},{av:'AV68TFContratoDadosCertame_Site_Sel',fld:'vTFCONTRATODADOSCERTAME_SITE_SEL',pic:'',nv:''},{av:'AV71TFContratoDadosCertame_Uasg',fld:'vTFCONTRATODADOSCERTAME_UASG',pic:'ZZZ9',nv:0},{av:'AV72TFContratoDadosCertame_Uasg_To',fld:'vTFCONTRATODADOSCERTAME_UASG_TO',pic:'ZZZ9',nv:0},{av:'AV75TFContratoDadosCertame_DataHomologacao',fld:'vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO',pic:'',nv:''},{av:'AV76TFContratoDadosCertame_DataHomologacao_To',fld:'vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO_TO',pic:'',nv:''},{av:'AV81TFContratoDadosCertame_DataAdjudicacao',fld:'vTFCONTRATODADOSCERTAME_DATAADJUDICACAO',pic:'',nv:''},{av:'AV82TFContratoDadosCertame_DataAdjudicacao_To',fld:'vTFCONTRATODADOSCERTAME_DATAADJUDICACAO_TO',pic:'',nv:''},{av:'AV51ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoDadosCertame_DataTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoDadosCertame_ModalidadeTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_MODALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoDadosCertame_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ContratoDadosCertame_SiteTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_SITETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoDadosCertame_UasgTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_UASGTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratoDadosCertame_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_ContratoDadosCertame_DataAdjudicacaoTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATAADJUDICACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A314ContratoDadosCertame_Codigo',fld:'CONTRATODADOSCERTAME_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A309ContratoDadosCertame_Site',fld:'CONTRATODADOSCERTAME_SITE',pic:'',hsh:true,nv:''}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40ContratoDadosCertame_Data2',fld:'vCONTRATODADOSCERTAME_DATA2',pic:'',nv:''},{av:'AV41ContratoDadosCertame_Data_To2',fld:'vCONTRATODADOSCERTAME_DATA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV43ContratoDadosCertame_Data3',fld:'vCONTRATODADOSCERTAME_DATA3',pic:'',nv:''},{av:'AV44ContratoDadosCertame_Data_To3',fld:'vCONTRATODADOSCERTAME_DATA_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cmbavDynamicfiltersoperator3'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV37ContratoDadosCertame_Data1',fld:'vCONTRATODADOSCERTAME_DATA1',pic:'',nv:''},{av:'AV38ContratoDadosCertame_Data_To1',fld:'vCONTRATODADOSCERTAME_DATA_TO1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoDadosCertame_Modalidade1',fld:'vCONTRATODADOSCERTAME_MODALIDADE1',pic:'',nv:''},{av:'AV39ContratoDadosCertame_Numero1',fld:'vCONTRATODADOSCERTAME_NUMERO1',pic:'ZZZZZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoDadosCertame_Modalidade2',fld:'vCONTRATODADOSCERTAME_MODALIDADE2',pic:'',nv:''},{av:'AV42ContratoDadosCertame_Numero2',fld:'vCONTRATODADOSCERTAME_NUMERO2',pic:'ZZZZZZZZZ9',nv:0},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoDadosCertame_Modalidade3',fld:'vCONTRATODADOSCERTAME_MODALIDADE3',pic:'',nv:''},{av:'AV45ContratoDadosCertame_Numero3',fld:'vCONTRATODADOSCERTAME_NUMERO3',pic:'ZZZZZZZZZ9',nv:0},{av:'tblTablemergeddynamicfilterscontratodadoscertame_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATODADOSCERTAME_DATA2',prop:'Visible'},{av:'edtavContratodadoscertame_modalidade2_Visible',ctrl:'vCONTRATODADOSCERTAME_MODALIDADE2',prop:'Visible'},{av:'edtavContratodadoscertame_numero2_Visible',ctrl:'vCONTRATODADOSCERTAME_NUMERO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratodadoscertame_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATODADOSCERTAME_DATA3',prop:'Visible'},{av:'edtavContratodadoscertame_modalidade3_Visible',ctrl:'vCONTRATODADOSCERTAME_MODALIDADE3',prop:'Visible'},{av:'edtavContratodadoscertame_numero3_Visible',ctrl:'vCONTRATODADOSCERTAME_NUMERO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratodadoscertame_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATODADOSCERTAME_DATA1',prop:'Visible'},{av:'edtavContratodadoscertame_modalidade1_Visible',ctrl:'vCONTRATODADOSCERTAME_MODALIDADE1',prop:'Visible'},{av:'edtavContratodadoscertame_numero1_Visible',ctrl:'vCONTRATODADOSCERTAME_NUMERO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E307G2',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'tblTablemergeddynamicfilterscontratodadoscertame_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATODADOSCERTAME_DATA3',prop:'Visible'},{av:'edtavContratodadoscertame_modalidade3_Visible',ctrl:'vCONTRATODADOSCERTAME_MODALIDADE3',prop:'Visible'},{av:'edtavContratodadoscertame_numero3_Visible',ctrl:'vCONTRATODADOSCERTAME_NUMERO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E247G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV37ContratoDadosCertame_Data1',fld:'vCONTRATODADOSCERTAME_DATA1',pic:'',nv:''},{av:'AV38ContratoDadosCertame_Data_To1',fld:'vCONTRATODADOSCERTAME_DATA_TO1',pic:'',nv:''},{av:'AV17ContratoDadosCertame_Modalidade1',fld:'vCONTRATODADOSCERTAME_MODALIDADE1',pic:'',nv:''},{av:'AV39ContratoDadosCertame_Numero1',fld:'vCONTRATODADOSCERTAME_NUMERO1',pic:'ZZZZZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV40ContratoDadosCertame_Data2',fld:'vCONTRATODADOSCERTAME_DATA2',pic:'',nv:''},{av:'AV41ContratoDadosCertame_Data_To2',fld:'vCONTRATODADOSCERTAME_DATA_TO2',pic:'',nv:''},{av:'AV21ContratoDadosCertame_Modalidade2',fld:'vCONTRATODADOSCERTAME_MODALIDADE2',pic:'',nv:''},{av:'AV42ContratoDadosCertame_Numero2',fld:'vCONTRATODADOSCERTAME_NUMERO2',pic:'ZZZZZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV43ContratoDadosCertame_Data3',fld:'vCONTRATODADOSCERTAME_DATA3',pic:'',nv:''},{av:'AV44ContratoDadosCertame_Data_To3',fld:'vCONTRATODADOSCERTAME_DATA_TO3',pic:'',nv:''},{av:'AV25ContratoDadosCertame_Modalidade3',fld:'vCONTRATODADOSCERTAME_MODALIDADE3',pic:'',nv:''},{av:'AV45ContratoDadosCertame_Numero3',fld:'vCONTRATODADOSCERTAME_NUMERO3',pic:'ZZZZZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV50TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV53TFContratoDadosCertame_Data',fld:'vTFCONTRATODADOSCERTAME_DATA',pic:'',nv:''},{av:'AV54TFContratoDadosCertame_Data_To',fld:'vTFCONTRATODADOSCERTAME_DATA_TO',pic:'',nv:''},{av:'AV59TFContratoDadosCertame_Modalidade',fld:'vTFCONTRATODADOSCERTAME_MODALIDADE',pic:'',nv:''},{av:'AV60TFContratoDadosCertame_Modalidade_Sel',fld:'vTFCONTRATODADOSCERTAME_MODALIDADE_SEL',pic:'',nv:''},{av:'AV63TFContratoDadosCertame_Numero',fld:'vTFCONTRATODADOSCERTAME_NUMERO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV64TFContratoDadosCertame_Numero_To',fld:'vTFCONTRATODADOSCERTAME_NUMERO_TO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV67TFContratoDadosCertame_Site',fld:'vTFCONTRATODADOSCERTAME_SITE',pic:'',nv:''},{av:'AV68TFContratoDadosCertame_Site_Sel',fld:'vTFCONTRATODADOSCERTAME_SITE_SEL',pic:'',nv:''},{av:'AV71TFContratoDadosCertame_Uasg',fld:'vTFCONTRATODADOSCERTAME_UASG',pic:'ZZZ9',nv:0},{av:'AV72TFContratoDadosCertame_Uasg_To',fld:'vTFCONTRATODADOSCERTAME_UASG_TO',pic:'ZZZ9',nv:0},{av:'AV75TFContratoDadosCertame_DataHomologacao',fld:'vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO',pic:'',nv:''},{av:'AV76TFContratoDadosCertame_DataHomologacao_To',fld:'vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO_TO',pic:'',nv:''},{av:'AV81TFContratoDadosCertame_DataAdjudicacao',fld:'vTFCONTRATODADOSCERTAME_DATAADJUDICACAO',pic:'',nv:''},{av:'AV82TFContratoDadosCertame_DataAdjudicacao_To',fld:'vTFCONTRATODADOSCERTAME_DATAADJUDICACAO_TO',pic:'',nv:''},{av:'AV51ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoDadosCertame_DataTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoDadosCertame_ModalidadeTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_MODALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoDadosCertame_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ContratoDadosCertame_SiteTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_SITETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoDadosCertame_UasgTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_UASGTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratoDadosCertame_DataHomologacaoTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_ContratoDadosCertame_DataAdjudicacaoTitleControlIdToReplace',fld:'vDDO_CONTRATODADOSCERTAME_DATAADJUDICACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A314ContratoDadosCertame_Codigo',fld:'CONTRATODADOSCERTAME_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A309ContratoDadosCertame_Site',fld:'CONTRATODADOSCERTAME_SITE',pic:'',hsh:true,nv:''}],oparms:[{av:'AV49TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'Ddo_contrato_numero_Filteredtext_set',ctrl:'DDO_CONTRATO_NUMERO',prop:'FilteredText_set'},{av:'AV50TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'Ddo_contrato_numero_Selectedvalue_set',ctrl:'DDO_CONTRATO_NUMERO',prop:'SelectedValue_set'},{av:'AV53TFContratoDadosCertame_Data',fld:'vTFCONTRATODADOSCERTAME_DATA',pic:'',nv:''},{av:'Ddo_contratodadoscertame_data_Filteredtext_set',ctrl:'DDO_CONTRATODADOSCERTAME_DATA',prop:'FilteredText_set'},{av:'AV54TFContratoDadosCertame_Data_To',fld:'vTFCONTRATODADOSCERTAME_DATA_TO',pic:'',nv:''},{av:'Ddo_contratodadoscertame_data_Filteredtextto_set',ctrl:'DDO_CONTRATODADOSCERTAME_DATA',prop:'FilteredTextTo_set'},{av:'AV59TFContratoDadosCertame_Modalidade',fld:'vTFCONTRATODADOSCERTAME_MODALIDADE',pic:'',nv:''},{av:'Ddo_contratodadoscertame_modalidade_Filteredtext_set',ctrl:'DDO_CONTRATODADOSCERTAME_MODALIDADE',prop:'FilteredText_set'},{av:'AV60TFContratoDadosCertame_Modalidade_Sel',fld:'vTFCONTRATODADOSCERTAME_MODALIDADE_SEL',pic:'',nv:''},{av:'Ddo_contratodadoscertame_modalidade_Selectedvalue_set',ctrl:'DDO_CONTRATODADOSCERTAME_MODALIDADE',prop:'SelectedValue_set'},{av:'AV63TFContratoDadosCertame_Numero',fld:'vTFCONTRATODADOSCERTAME_NUMERO',pic:'ZZZZZZZZZ9',nv:0},{av:'Ddo_contratodadoscertame_numero_Filteredtext_set',ctrl:'DDO_CONTRATODADOSCERTAME_NUMERO',prop:'FilteredText_set'},{av:'AV64TFContratoDadosCertame_Numero_To',fld:'vTFCONTRATODADOSCERTAME_NUMERO_TO',pic:'ZZZZZZZZZ9',nv:0},{av:'Ddo_contratodadoscertame_numero_Filteredtextto_set',ctrl:'DDO_CONTRATODADOSCERTAME_NUMERO',prop:'FilteredTextTo_set'},{av:'AV67TFContratoDadosCertame_Site',fld:'vTFCONTRATODADOSCERTAME_SITE',pic:'',nv:''},{av:'Ddo_contratodadoscertame_site_Filteredtext_set',ctrl:'DDO_CONTRATODADOSCERTAME_SITE',prop:'FilteredText_set'},{av:'AV68TFContratoDadosCertame_Site_Sel',fld:'vTFCONTRATODADOSCERTAME_SITE_SEL',pic:'',nv:''},{av:'Ddo_contratodadoscertame_site_Selectedvalue_set',ctrl:'DDO_CONTRATODADOSCERTAME_SITE',prop:'SelectedValue_set'},{av:'AV71TFContratoDadosCertame_Uasg',fld:'vTFCONTRATODADOSCERTAME_UASG',pic:'ZZZ9',nv:0},{av:'Ddo_contratodadoscertame_uasg_Filteredtext_set',ctrl:'DDO_CONTRATODADOSCERTAME_UASG',prop:'FilteredText_set'},{av:'AV72TFContratoDadosCertame_Uasg_To',fld:'vTFCONTRATODADOSCERTAME_UASG_TO',pic:'ZZZ9',nv:0},{av:'Ddo_contratodadoscertame_uasg_Filteredtextto_set',ctrl:'DDO_CONTRATODADOSCERTAME_UASG',prop:'FilteredTextTo_set'},{av:'AV75TFContratoDadosCertame_DataHomologacao',fld:'vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO',pic:'',nv:''},{av:'Ddo_contratodadoscertame_datahomologacao_Filteredtext_set',ctrl:'DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO',prop:'FilteredText_set'},{av:'AV76TFContratoDadosCertame_DataHomologacao_To',fld:'vTFCONTRATODADOSCERTAME_DATAHOMOLOGACAO_TO',pic:'',nv:''},{av:'Ddo_contratodadoscertame_datahomologacao_Filteredtextto_set',ctrl:'DDO_CONTRATODADOSCERTAME_DATAHOMOLOGACAO',prop:'FilteredTextTo_set'},{av:'AV81TFContratoDadosCertame_DataAdjudicacao',fld:'vTFCONTRATODADOSCERTAME_DATAADJUDICACAO',pic:'',nv:''},{av:'Ddo_contratodadoscertame_dataadjudicacao_Filteredtext_set',ctrl:'DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO',prop:'FilteredText_set'},{av:'AV82TFContratoDadosCertame_DataAdjudicacao_To',fld:'vTFCONTRATODADOSCERTAME_DATAADJUDICACAO_TO',pic:'',nv:''},{av:'Ddo_contratodadoscertame_dataadjudicacao_Filteredtextto_set',ctrl:'DDO_CONTRATODADOSCERTAME_DATAADJUDICACAO',prop:'FilteredTextTo_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV37ContratoDadosCertame_Data1',fld:'vCONTRATODADOSCERTAME_DATA1',pic:'',nv:''},{av:'AV38ContratoDadosCertame_Data_To1',fld:'vCONTRATODADOSCERTAME_DATA_TO1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'tblTablemergeddynamicfilterscontratodadoscertame_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATODADOSCERTAME_DATA1',prop:'Visible'},{av:'edtavContratodadoscertame_modalidade1_Visible',ctrl:'vCONTRATODADOSCERTAME_MODALIDADE1',prop:'Visible'},{av:'edtavContratodadoscertame_numero1_Visible',ctrl:'vCONTRATODADOSCERTAME_NUMERO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40ContratoDadosCertame_Data2',fld:'vCONTRATODADOSCERTAME_DATA2',pic:'',nv:''},{av:'AV41ContratoDadosCertame_Data_To2',fld:'vCONTRATODADOSCERTAME_DATA_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV43ContratoDadosCertame_Data3',fld:'vCONTRATODADOSCERTAME_DATA3',pic:'',nv:''},{av:'AV44ContratoDadosCertame_Data_To3',fld:'vCONTRATODADOSCERTAME_DATA_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cmbavDynamicfiltersoperator3'},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoDadosCertame_Modalidade1',fld:'vCONTRATODADOSCERTAME_MODALIDADE1',pic:'',nv:''},{av:'AV39ContratoDadosCertame_Numero1',fld:'vCONTRATODADOSCERTAME_NUMERO1',pic:'ZZZZZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoDadosCertame_Modalidade2',fld:'vCONTRATODADOSCERTAME_MODALIDADE2',pic:'',nv:''},{av:'AV42ContratoDadosCertame_Numero2',fld:'vCONTRATODADOSCERTAME_NUMERO2',pic:'ZZZZZZZZZ9',nv:0},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoDadosCertame_Modalidade3',fld:'vCONTRATODADOSCERTAME_MODALIDADE3',pic:'',nv:''},{av:'AV45ContratoDadosCertame_Numero3',fld:'vCONTRATODADOSCERTAME_NUMERO3',pic:'ZZZZZZZZZ9',nv:0},{av:'tblTablemergeddynamicfilterscontratodadoscertame_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATODADOSCERTAME_DATA2',prop:'Visible'},{av:'edtavContratodadoscertame_modalidade2_Visible',ctrl:'vCONTRATODADOSCERTAME_MODALIDADE2',prop:'Visible'},{av:'edtavContratodadoscertame_numero2_Visible',ctrl:'vCONTRATODADOSCERTAME_NUMERO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratodadoscertame_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATODADOSCERTAME_DATA3',prop:'Visible'},{av:'edtavContratodadoscertame_modalidade3_Visible',ctrl:'vCONTRATODADOSCERTAME_MODALIDADE3',prop:'Visible'},{av:'edtavContratodadoscertame_numero3_Visible',ctrl:'vCONTRATODADOSCERTAME_NUMERO3',prop:'Visible'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E257G2',iparms:[{av:'A314ContratoDadosCertame_Codigo',fld:'CONTRATODADOSCERTAME_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_contrato_numero_Activeeventkey = "";
         Ddo_contrato_numero_Filteredtext_get = "";
         Ddo_contrato_numero_Selectedvalue_get = "";
         Ddo_contratodadoscertame_data_Activeeventkey = "";
         Ddo_contratodadoscertame_data_Filteredtext_get = "";
         Ddo_contratodadoscertame_data_Filteredtextto_get = "";
         Ddo_contratodadoscertame_modalidade_Activeeventkey = "";
         Ddo_contratodadoscertame_modalidade_Filteredtext_get = "";
         Ddo_contratodadoscertame_modalidade_Selectedvalue_get = "";
         Ddo_contratodadoscertame_numero_Activeeventkey = "";
         Ddo_contratodadoscertame_numero_Filteredtext_get = "";
         Ddo_contratodadoscertame_numero_Filteredtextto_get = "";
         Ddo_contratodadoscertame_site_Activeeventkey = "";
         Ddo_contratodadoscertame_site_Filteredtext_get = "";
         Ddo_contratodadoscertame_site_Selectedvalue_get = "";
         Ddo_contratodadoscertame_uasg_Activeeventkey = "";
         Ddo_contratodadoscertame_uasg_Filteredtext_get = "";
         Ddo_contratodadoscertame_uasg_Filteredtextto_get = "";
         Ddo_contratodadoscertame_datahomologacao_Activeeventkey = "";
         Ddo_contratodadoscertame_datahomologacao_Filteredtext_get = "";
         Ddo_contratodadoscertame_datahomologacao_Filteredtextto_get = "";
         Ddo_contratodadoscertame_dataadjudicacao_Activeeventkey = "";
         Ddo_contratodadoscertame_dataadjudicacao_Filteredtext_get = "";
         Ddo_contratodadoscertame_dataadjudicacao_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV37ContratoDadosCertame_Data1 = DateTime.MinValue;
         AV38ContratoDadosCertame_Data_To1 = DateTime.MinValue;
         AV17ContratoDadosCertame_Modalidade1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV40ContratoDadosCertame_Data2 = DateTime.MinValue;
         AV41ContratoDadosCertame_Data_To2 = DateTime.MinValue;
         AV21ContratoDadosCertame_Modalidade2 = "";
         AV23DynamicFiltersSelector3 = "";
         AV43ContratoDadosCertame_Data3 = DateTime.MinValue;
         AV44ContratoDadosCertame_Data_To3 = DateTime.MinValue;
         AV25ContratoDadosCertame_Modalidade3 = "";
         AV49TFContrato_Numero = "";
         AV50TFContrato_Numero_Sel = "";
         AV53TFContratoDadosCertame_Data = DateTime.MinValue;
         AV54TFContratoDadosCertame_Data_To = DateTime.MinValue;
         AV59TFContratoDadosCertame_Modalidade = "";
         AV60TFContratoDadosCertame_Modalidade_Sel = "";
         AV67TFContratoDadosCertame_Site = "";
         AV68TFContratoDadosCertame_Site_Sel = "";
         AV75TFContratoDadosCertame_DataHomologacao = DateTime.MinValue;
         AV76TFContratoDadosCertame_DataHomologacao_To = DateTime.MinValue;
         AV81TFContratoDadosCertame_DataAdjudicacao = DateTime.MinValue;
         AV82TFContratoDadosCertame_DataAdjudicacao_To = DateTime.MinValue;
         AV51ddo_Contrato_NumeroTitleControlIdToReplace = "";
         AV57ddo_ContratoDadosCertame_DataTitleControlIdToReplace = "";
         AV61ddo_ContratoDadosCertame_ModalidadeTitleControlIdToReplace = "";
         AV65ddo_ContratoDadosCertame_NumeroTitleControlIdToReplace = "";
         AV69ddo_ContratoDadosCertame_SiteTitleControlIdToReplace = "";
         AV73ddo_ContratoDadosCertame_UasgTitleControlIdToReplace = "";
         AV79ddo_ContratoDadosCertame_DataHomologacaoTitleControlIdToReplace = "";
         AV85ddo_ContratoDadosCertame_DataAdjudicacaoTitleControlIdToReplace = "";
         AV130Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         A309ContratoDadosCertame_Site = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV86DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV48Contrato_NumeroTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV52ContratoDadosCertame_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV58ContratoDadosCertame_ModalidadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV62ContratoDadosCertame_NumeroTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV66ContratoDadosCertame_SiteTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV70ContratoDadosCertame_UasgTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV74ContratoDadosCertame_DataHomologacaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV80ContratoDadosCertame_DataAdjudicacaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_contrato_numero_Filteredtext_set = "";
         Ddo_contrato_numero_Selectedvalue_set = "";
         Ddo_contrato_numero_Sortedstatus = "";
         Ddo_contratodadoscertame_data_Filteredtext_set = "";
         Ddo_contratodadoscertame_data_Filteredtextto_set = "";
         Ddo_contratodadoscertame_data_Sortedstatus = "";
         Ddo_contratodadoscertame_modalidade_Filteredtext_set = "";
         Ddo_contratodadoscertame_modalidade_Selectedvalue_set = "";
         Ddo_contratodadoscertame_modalidade_Sortedstatus = "";
         Ddo_contratodadoscertame_numero_Filteredtext_set = "";
         Ddo_contratodadoscertame_numero_Filteredtextto_set = "";
         Ddo_contratodadoscertame_numero_Sortedstatus = "";
         Ddo_contratodadoscertame_site_Filteredtext_set = "";
         Ddo_contratodadoscertame_site_Selectedvalue_set = "";
         Ddo_contratodadoscertame_site_Sortedstatus = "";
         Ddo_contratodadoscertame_uasg_Filteredtext_set = "";
         Ddo_contratodadoscertame_uasg_Filteredtextto_set = "";
         Ddo_contratodadoscertame_uasg_Sortedstatus = "";
         Ddo_contratodadoscertame_datahomologacao_Filteredtext_set = "";
         Ddo_contratodadoscertame_datahomologacao_Filteredtextto_set = "";
         Ddo_contratodadoscertame_datahomologacao_Sortedstatus = "";
         Ddo_contratodadoscertame_dataadjudicacao_Filteredtext_set = "";
         Ddo_contratodadoscertame_dataadjudicacao_Filteredtextto_set = "";
         Ddo_contratodadoscertame_dataadjudicacao_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV55DDO_ContratoDadosCertame_DataAuxDate = DateTime.MinValue;
         AV56DDO_ContratoDadosCertame_DataAuxDateTo = DateTime.MinValue;
         AV77DDO_ContratoDadosCertame_DataHomologacaoAuxDate = DateTime.MinValue;
         AV78DDO_ContratoDadosCertame_DataHomologacaoAuxDateTo = DateTime.MinValue;
         AV83DDO_ContratoDadosCertame_DataAdjudicacaoAuxDate = DateTime.MinValue;
         AV84DDO_ContratoDadosCertame_DataAdjudicacaoAuxDateTo = DateTime.MinValue;
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Update = "";
         AV128Update_GXI = "";
         AV29Delete = "";
         AV129Delete_GXI = "";
         A77Contrato_Numero = "";
         A311ContratoDadosCertame_Data = DateTime.MinValue;
         A307ContratoDadosCertame_Modalidade = "";
         A312ContratoDadosCertame_DataHomologacao = DateTime.MinValue;
         A313ContratoDadosCertame_DataAdjudicacao = DateTime.MinValue;
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1 = "";
         lV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2 = "";
         lV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3 = "";
         lV112WWContratoDadosCertameDS_21_Tfcontrato_numero = "";
         lV116WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade = "";
         lV120WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site = "";
         AV92WWContratoDadosCertameDS_1_Dynamicfiltersselector1 = "";
         AV94WWContratoDadosCertameDS_3_Contratodadoscertame_data1 = DateTime.MinValue;
         AV95WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1 = DateTime.MinValue;
         AV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1 = "";
         AV99WWContratoDadosCertameDS_8_Dynamicfiltersselector2 = "";
         AV101WWContratoDadosCertameDS_10_Contratodadoscertame_data2 = DateTime.MinValue;
         AV102WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2 = DateTime.MinValue;
         AV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2 = "";
         AV106WWContratoDadosCertameDS_15_Dynamicfiltersselector3 = "";
         AV108WWContratoDadosCertameDS_17_Contratodadoscertame_data3 = DateTime.MinValue;
         AV109WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3 = DateTime.MinValue;
         AV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3 = "";
         AV113WWContratoDadosCertameDS_22_Tfcontrato_numero_sel = "";
         AV112WWContratoDadosCertameDS_21_Tfcontrato_numero = "";
         AV114WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data = DateTime.MinValue;
         AV115WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to = DateTime.MinValue;
         AV117WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel = "";
         AV116WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade = "";
         AV121WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel = "";
         AV120WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site = "";
         AV124WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao = DateTime.MinValue;
         AV125WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to = DateTime.MinValue;
         AV126WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao = DateTime.MinValue;
         AV127WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to = DateTime.MinValue;
         H007G2_A74Contrato_Codigo = new int[1] ;
         H007G2_A314ContratoDadosCertame_Codigo = new int[1] ;
         H007G2_A313ContratoDadosCertame_DataAdjudicacao = new DateTime[] {DateTime.MinValue} ;
         H007G2_n313ContratoDadosCertame_DataAdjudicacao = new bool[] {false} ;
         H007G2_A312ContratoDadosCertame_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         H007G2_n312ContratoDadosCertame_DataHomologacao = new bool[] {false} ;
         H007G2_A310ContratoDadosCertame_Uasg = new short[1] ;
         H007G2_n310ContratoDadosCertame_Uasg = new bool[] {false} ;
         H007G2_A309ContratoDadosCertame_Site = new String[] {""} ;
         H007G2_n309ContratoDadosCertame_Site = new bool[] {false} ;
         H007G2_A308ContratoDadosCertame_Numero = new long[1] ;
         H007G2_n308ContratoDadosCertame_Numero = new bool[] {false} ;
         H007G2_A307ContratoDadosCertame_Modalidade = new String[] {""} ;
         H007G2_A311ContratoDadosCertame_Data = new DateTime[] {DateTime.MinValue} ;
         H007G2_A77Contrato_Numero = new String[] {""} ;
         H007G3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV30Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblContratodadoscertametitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblDynamicfilterscontratodadoscertame_data_rangemiddletext3_Jsonclick = "";
         lblDynamicfilterscontratodadoscertame_data_rangemiddletext2_Jsonclick = "";
         lblDynamicfilterscontratodadoscertame_data_rangemiddletext1_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwcontratodadoscertame__default(),
            new Object[][] {
                new Object[] {
               H007G2_A74Contrato_Codigo, H007G2_A314ContratoDadosCertame_Codigo, H007G2_A313ContratoDadosCertame_DataAdjudicacao, H007G2_n313ContratoDadosCertame_DataAdjudicacao, H007G2_A312ContratoDadosCertame_DataHomologacao, H007G2_n312ContratoDadosCertame_DataHomologacao, H007G2_A310ContratoDadosCertame_Uasg, H007G2_n310ContratoDadosCertame_Uasg, H007G2_A309ContratoDadosCertame_Site, H007G2_n309ContratoDadosCertame_Site,
               H007G2_A308ContratoDadosCertame_Numero, H007G2_n308ContratoDadosCertame_Numero, H007G2_A307ContratoDadosCertame_Modalidade, H007G2_A311ContratoDadosCertame_Data, H007G2_A77Contrato_Numero
               }
               , new Object[] {
               H007G3_AGRID_nRecordCount
               }
            }
         );
         AV130Pgmname = "WWContratoDadosCertame";
         /* GeneXus formulas. */
         AV130Pgmname = "WWContratoDadosCertame";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_115 ;
      private short nGXsfl_115_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV20DynamicFiltersOperator2 ;
      private short AV24DynamicFiltersOperator3 ;
      private short AV71TFContratoDadosCertame_Uasg ;
      private short AV72TFContratoDadosCertame_Uasg_To ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A310ContratoDadosCertame_Uasg ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_115_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV93WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 ;
      private short AV100WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 ;
      private short AV107WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 ;
      private short AV122WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg ;
      private short AV123WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to ;
      private short edtContrato_Numero_Titleformat ;
      private short edtContratoDadosCertame_Data_Titleformat ;
      private short edtContratoDadosCertame_Modalidade_Titleformat ;
      private short edtContratoDadosCertame_Numero_Titleformat ;
      private short edtContratoDadosCertame_Site_Titleformat ;
      private short edtContratoDadosCertame_Uasg_Titleformat ;
      private short edtContratoDadosCertame_DataHomologacao_Titleformat ;
      private short edtContratoDadosCertame_DataAdjudicacao_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int A314ContratoDadosCertame_Codigo ;
      private int A74Contrato_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_contrato_numero_Datalistupdateminimumcharacters ;
      private int Ddo_contratodadoscertame_modalidade_Datalistupdateminimumcharacters ;
      private int Ddo_contratodadoscertame_site_Datalistupdateminimumcharacters ;
      private int edtavTfcontrato_numero_Visible ;
      private int edtavTfcontrato_numero_sel_Visible ;
      private int edtavTfcontratodadoscertame_data_Visible ;
      private int edtavTfcontratodadoscertame_data_to_Visible ;
      private int edtavTfcontratodadoscertame_modalidade_Visible ;
      private int edtavTfcontratodadoscertame_modalidade_sel_Visible ;
      private int edtavTfcontratodadoscertame_numero_Visible ;
      private int edtavTfcontratodadoscertame_numero_to_Visible ;
      private int edtavTfcontratodadoscertame_site_Visible ;
      private int edtavTfcontratodadoscertame_site_sel_Visible ;
      private int edtavTfcontratodadoscertame_uasg_Visible ;
      private int edtavTfcontratodadoscertame_uasg_to_Visible ;
      private int edtavTfcontratodadoscertame_datahomologacao_Visible ;
      private int edtavTfcontratodadoscertame_datahomologacao_to_Visible ;
      private int edtavTfcontratodadoscertame_dataadjudicacao_Visible ;
      private int edtavTfcontratodadoscertame_dataadjudicacao_to_Visible ;
      private int edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratodadoscertame_datatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratodadoscertame_modalidadetitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratodadoscertame_numerotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratodadoscertame_sitetitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratodadoscertame_uasgtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratodadoscertame_datahomologacaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratodadoscertame_dataadjudicacaotitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV87PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int tblTablemergeddynamicfilterscontratodadoscertame_data1_Visible ;
      private int edtavContratodadoscertame_modalidade1_Visible ;
      private int edtavContratodadoscertame_numero1_Visible ;
      private int tblTablemergeddynamicfilterscontratodadoscertame_data2_Visible ;
      private int edtavContratodadoscertame_modalidade2_Visible ;
      private int edtavContratodadoscertame_numero2_Visible ;
      private int tblTablemergeddynamicfilterscontratodadoscertame_data3_Visible ;
      private int edtavContratodadoscertame_modalidade3_Visible ;
      private int edtavContratodadoscertame_numero3_Visible ;
      private int AV131GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long AV39ContratoDadosCertame_Numero1 ;
      private long AV42ContratoDadosCertame_Numero2 ;
      private long AV45ContratoDadosCertame_Numero3 ;
      private long AV63TFContratoDadosCertame_Numero ;
      private long AV64TFContratoDadosCertame_Numero_To ;
      private long GRID_nFirstRecordOnPage ;
      private long AV88GridCurrentPage ;
      private long AV89GridPageCount ;
      private long A308ContratoDadosCertame_Numero ;
      private long GRID_nCurrentRecord ;
      private long AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1 ;
      private long AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2 ;
      private long AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3 ;
      private long AV118WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero ;
      private long AV119WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contrato_numero_Activeeventkey ;
      private String Ddo_contrato_numero_Filteredtext_get ;
      private String Ddo_contrato_numero_Selectedvalue_get ;
      private String Ddo_contratodadoscertame_data_Activeeventkey ;
      private String Ddo_contratodadoscertame_data_Filteredtext_get ;
      private String Ddo_contratodadoscertame_data_Filteredtextto_get ;
      private String Ddo_contratodadoscertame_modalidade_Activeeventkey ;
      private String Ddo_contratodadoscertame_modalidade_Filteredtext_get ;
      private String Ddo_contratodadoscertame_modalidade_Selectedvalue_get ;
      private String Ddo_contratodadoscertame_numero_Activeeventkey ;
      private String Ddo_contratodadoscertame_numero_Filteredtext_get ;
      private String Ddo_contratodadoscertame_numero_Filteredtextto_get ;
      private String Ddo_contratodadoscertame_site_Activeeventkey ;
      private String Ddo_contratodadoscertame_site_Filteredtext_get ;
      private String Ddo_contratodadoscertame_site_Selectedvalue_get ;
      private String Ddo_contratodadoscertame_uasg_Activeeventkey ;
      private String Ddo_contratodadoscertame_uasg_Filteredtext_get ;
      private String Ddo_contratodadoscertame_uasg_Filteredtextto_get ;
      private String Ddo_contratodadoscertame_datahomologacao_Activeeventkey ;
      private String Ddo_contratodadoscertame_datahomologacao_Filteredtext_get ;
      private String Ddo_contratodadoscertame_datahomologacao_Filteredtextto_get ;
      private String Ddo_contratodadoscertame_dataadjudicacao_Activeeventkey ;
      private String Ddo_contratodadoscertame_dataadjudicacao_Filteredtext_get ;
      private String Ddo_contratodadoscertame_dataadjudicacao_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_115_idx="0001" ;
      private String AV49TFContrato_Numero ;
      private String AV50TFContrato_Numero_Sel ;
      private String AV130Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contrato_numero_Caption ;
      private String Ddo_contrato_numero_Tooltip ;
      private String Ddo_contrato_numero_Cls ;
      private String Ddo_contrato_numero_Filteredtext_set ;
      private String Ddo_contrato_numero_Selectedvalue_set ;
      private String Ddo_contrato_numero_Dropdownoptionstype ;
      private String Ddo_contrato_numero_Titlecontrolidtoreplace ;
      private String Ddo_contrato_numero_Sortedstatus ;
      private String Ddo_contrato_numero_Filtertype ;
      private String Ddo_contrato_numero_Datalisttype ;
      private String Ddo_contrato_numero_Datalistproc ;
      private String Ddo_contrato_numero_Sortasc ;
      private String Ddo_contrato_numero_Sortdsc ;
      private String Ddo_contrato_numero_Loadingdata ;
      private String Ddo_contrato_numero_Cleanfilter ;
      private String Ddo_contrato_numero_Noresultsfound ;
      private String Ddo_contrato_numero_Searchbuttontext ;
      private String Ddo_contratodadoscertame_data_Caption ;
      private String Ddo_contratodadoscertame_data_Tooltip ;
      private String Ddo_contratodadoscertame_data_Cls ;
      private String Ddo_contratodadoscertame_data_Filteredtext_set ;
      private String Ddo_contratodadoscertame_data_Filteredtextto_set ;
      private String Ddo_contratodadoscertame_data_Dropdownoptionstype ;
      private String Ddo_contratodadoscertame_data_Titlecontrolidtoreplace ;
      private String Ddo_contratodadoscertame_data_Sortedstatus ;
      private String Ddo_contratodadoscertame_data_Filtertype ;
      private String Ddo_contratodadoscertame_data_Sortasc ;
      private String Ddo_contratodadoscertame_data_Sortdsc ;
      private String Ddo_contratodadoscertame_data_Cleanfilter ;
      private String Ddo_contratodadoscertame_data_Rangefilterfrom ;
      private String Ddo_contratodadoscertame_data_Rangefilterto ;
      private String Ddo_contratodadoscertame_data_Searchbuttontext ;
      private String Ddo_contratodadoscertame_modalidade_Caption ;
      private String Ddo_contratodadoscertame_modalidade_Tooltip ;
      private String Ddo_contratodadoscertame_modalidade_Cls ;
      private String Ddo_contratodadoscertame_modalidade_Filteredtext_set ;
      private String Ddo_contratodadoscertame_modalidade_Selectedvalue_set ;
      private String Ddo_contratodadoscertame_modalidade_Dropdownoptionstype ;
      private String Ddo_contratodadoscertame_modalidade_Titlecontrolidtoreplace ;
      private String Ddo_contratodadoscertame_modalidade_Sortedstatus ;
      private String Ddo_contratodadoscertame_modalidade_Filtertype ;
      private String Ddo_contratodadoscertame_modalidade_Datalisttype ;
      private String Ddo_contratodadoscertame_modalidade_Datalistproc ;
      private String Ddo_contratodadoscertame_modalidade_Sortasc ;
      private String Ddo_contratodadoscertame_modalidade_Sortdsc ;
      private String Ddo_contratodadoscertame_modalidade_Loadingdata ;
      private String Ddo_contratodadoscertame_modalidade_Cleanfilter ;
      private String Ddo_contratodadoscertame_modalidade_Noresultsfound ;
      private String Ddo_contratodadoscertame_modalidade_Searchbuttontext ;
      private String Ddo_contratodadoscertame_numero_Caption ;
      private String Ddo_contratodadoscertame_numero_Tooltip ;
      private String Ddo_contratodadoscertame_numero_Cls ;
      private String Ddo_contratodadoscertame_numero_Filteredtext_set ;
      private String Ddo_contratodadoscertame_numero_Filteredtextto_set ;
      private String Ddo_contratodadoscertame_numero_Dropdownoptionstype ;
      private String Ddo_contratodadoscertame_numero_Titlecontrolidtoreplace ;
      private String Ddo_contratodadoscertame_numero_Sortedstatus ;
      private String Ddo_contratodadoscertame_numero_Filtertype ;
      private String Ddo_contratodadoscertame_numero_Sortasc ;
      private String Ddo_contratodadoscertame_numero_Sortdsc ;
      private String Ddo_contratodadoscertame_numero_Cleanfilter ;
      private String Ddo_contratodadoscertame_numero_Rangefilterfrom ;
      private String Ddo_contratodadoscertame_numero_Rangefilterto ;
      private String Ddo_contratodadoscertame_numero_Searchbuttontext ;
      private String Ddo_contratodadoscertame_site_Caption ;
      private String Ddo_contratodadoscertame_site_Tooltip ;
      private String Ddo_contratodadoscertame_site_Cls ;
      private String Ddo_contratodadoscertame_site_Filteredtext_set ;
      private String Ddo_contratodadoscertame_site_Selectedvalue_set ;
      private String Ddo_contratodadoscertame_site_Dropdownoptionstype ;
      private String Ddo_contratodadoscertame_site_Titlecontrolidtoreplace ;
      private String Ddo_contratodadoscertame_site_Sortedstatus ;
      private String Ddo_contratodadoscertame_site_Filtertype ;
      private String Ddo_contratodadoscertame_site_Datalisttype ;
      private String Ddo_contratodadoscertame_site_Datalistproc ;
      private String Ddo_contratodadoscertame_site_Sortasc ;
      private String Ddo_contratodadoscertame_site_Sortdsc ;
      private String Ddo_contratodadoscertame_site_Loadingdata ;
      private String Ddo_contratodadoscertame_site_Cleanfilter ;
      private String Ddo_contratodadoscertame_site_Noresultsfound ;
      private String Ddo_contratodadoscertame_site_Searchbuttontext ;
      private String Ddo_contratodadoscertame_uasg_Caption ;
      private String Ddo_contratodadoscertame_uasg_Tooltip ;
      private String Ddo_contratodadoscertame_uasg_Cls ;
      private String Ddo_contratodadoscertame_uasg_Filteredtext_set ;
      private String Ddo_contratodadoscertame_uasg_Filteredtextto_set ;
      private String Ddo_contratodadoscertame_uasg_Dropdownoptionstype ;
      private String Ddo_contratodadoscertame_uasg_Titlecontrolidtoreplace ;
      private String Ddo_contratodadoscertame_uasg_Sortedstatus ;
      private String Ddo_contratodadoscertame_uasg_Filtertype ;
      private String Ddo_contratodadoscertame_uasg_Sortasc ;
      private String Ddo_contratodadoscertame_uasg_Sortdsc ;
      private String Ddo_contratodadoscertame_uasg_Cleanfilter ;
      private String Ddo_contratodadoscertame_uasg_Rangefilterfrom ;
      private String Ddo_contratodadoscertame_uasg_Rangefilterto ;
      private String Ddo_contratodadoscertame_uasg_Searchbuttontext ;
      private String Ddo_contratodadoscertame_datahomologacao_Caption ;
      private String Ddo_contratodadoscertame_datahomologacao_Tooltip ;
      private String Ddo_contratodadoscertame_datahomologacao_Cls ;
      private String Ddo_contratodadoscertame_datahomologacao_Filteredtext_set ;
      private String Ddo_contratodadoscertame_datahomologacao_Filteredtextto_set ;
      private String Ddo_contratodadoscertame_datahomologacao_Dropdownoptionstype ;
      private String Ddo_contratodadoscertame_datahomologacao_Titlecontrolidtoreplace ;
      private String Ddo_contratodadoscertame_datahomologacao_Sortedstatus ;
      private String Ddo_contratodadoscertame_datahomologacao_Filtertype ;
      private String Ddo_contratodadoscertame_datahomologacao_Sortasc ;
      private String Ddo_contratodadoscertame_datahomologacao_Sortdsc ;
      private String Ddo_contratodadoscertame_datahomologacao_Cleanfilter ;
      private String Ddo_contratodadoscertame_datahomologacao_Rangefilterfrom ;
      private String Ddo_contratodadoscertame_datahomologacao_Rangefilterto ;
      private String Ddo_contratodadoscertame_datahomologacao_Searchbuttontext ;
      private String Ddo_contratodadoscertame_dataadjudicacao_Caption ;
      private String Ddo_contratodadoscertame_dataadjudicacao_Tooltip ;
      private String Ddo_contratodadoscertame_dataadjudicacao_Cls ;
      private String Ddo_contratodadoscertame_dataadjudicacao_Filteredtext_set ;
      private String Ddo_contratodadoscertame_dataadjudicacao_Filteredtextto_set ;
      private String Ddo_contratodadoscertame_dataadjudicacao_Dropdownoptionstype ;
      private String Ddo_contratodadoscertame_dataadjudicacao_Titlecontrolidtoreplace ;
      private String Ddo_contratodadoscertame_dataadjudicacao_Sortedstatus ;
      private String Ddo_contratodadoscertame_dataadjudicacao_Filtertype ;
      private String Ddo_contratodadoscertame_dataadjudicacao_Sortasc ;
      private String Ddo_contratodadoscertame_dataadjudicacao_Sortdsc ;
      private String Ddo_contratodadoscertame_dataadjudicacao_Cleanfilter ;
      private String Ddo_contratodadoscertame_dataadjudicacao_Rangefilterfrom ;
      private String Ddo_contratodadoscertame_dataadjudicacao_Rangefilterto ;
      private String Ddo_contratodadoscertame_dataadjudicacao_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfcontrato_numero_Internalname ;
      private String edtavTfcontrato_numero_Jsonclick ;
      private String edtavTfcontrato_numero_sel_Internalname ;
      private String edtavTfcontrato_numero_sel_Jsonclick ;
      private String edtavTfcontratodadoscertame_data_Internalname ;
      private String edtavTfcontratodadoscertame_data_Jsonclick ;
      private String edtavTfcontratodadoscertame_data_to_Internalname ;
      private String edtavTfcontratodadoscertame_data_to_Jsonclick ;
      private String divDdo_contratodadoscertame_dataauxdates_Internalname ;
      private String edtavDdo_contratodadoscertame_dataauxdate_Internalname ;
      private String edtavDdo_contratodadoscertame_dataauxdate_Jsonclick ;
      private String edtavDdo_contratodadoscertame_dataauxdateto_Internalname ;
      private String edtavDdo_contratodadoscertame_dataauxdateto_Jsonclick ;
      private String edtavTfcontratodadoscertame_modalidade_Internalname ;
      private String edtavTfcontratodadoscertame_modalidade_Jsonclick ;
      private String edtavTfcontratodadoscertame_modalidade_sel_Internalname ;
      private String edtavTfcontratodadoscertame_modalidade_sel_Jsonclick ;
      private String edtavTfcontratodadoscertame_numero_Internalname ;
      private String edtavTfcontratodadoscertame_numero_Jsonclick ;
      private String edtavTfcontratodadoscertame_numero_to_Internalname ;
      private String edtavTfcontratodadoscertame_numero_to_Jsonclick ;
      private String edtavTfcontratodadoscertame_site_Internalname ;
      private String edtavTfcontratodadoscertame_site_Jsonclick ;
      private String edtavTfcontratodadoscertame_site_sel_Internalname ;
      private String edtavTfcontratodadoscertame_site_sel_Jsonclick ;
      private String edtavTfcontratodadoscertame_uasg_Internalname ;
      private String edtavTfcontratodadoscertame_uasg_Jsonclick ;
      private String edtavTfcontratodadoscertame_uasg_to_Internalname ;
      private String edtavTfcontratodadoscertame_uasg_to_Jsonclick ;
      private String edtavTfcontratodadoscertame_datahomologacao_Internalname ;
      private String edtavTfcontratodadoscertame_datahomologacao_Jsonclick ;
      private String edtavTfcontratodadoscertame_datahomologacao_to_Internalname ;
      private String edtavTfcontratodadoscertame_datahomologacao_to_Jsonclick ;
      private String divDdo_contratodadoscertame_datahomologacaoauxdates_Internalname ;
      private String edtavDdo_contratodadoscertame_datahomologacaoauxdate_Internalname ;
      private String edtavDdo_contratodadoscertame_datahomologacaoauxdate_Jsonclick ;
      private String edtavDdo_contratodadoscertame_datahomologacaoauxdateto_Internalname ;
      private String edtavDdo_contratodadoscertame_datahomologacaoauxdateto_Jsonclick ;
      private String edtavTfcontratodadoscertame_dataadjudicacao_Internalname ;
      private String edtavTfcontratodadoscertame_dataadjudicacao_Jsonclick ;
      private String edtavTfcontratodadoscertame_dataadjudicacao_to_Internalname ;
      private String edtavTfcontratodadoscertame_dataadjudicacao_to_Jsonclick ;
      private String divDdo_contratodadoscertame_dataadjudicacaoauxdates_Internalname ;
      private String edtavDdo_contratodadoscertame_dataadjudicacaoauxdate_Internalname ;
      private String edtavDdo_contratodadoscertame_dataadjudicacaoauxdate_Jsonclick ;
      private String edtavDdo_contratodadoscertame_dataadjudicacaoauxdateto_Internalname ;
      private String edtavDdo_contratodadoscertame_dataadjudicacaoauxdateto_Jsonclick ;
      private String edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratodadoscertame_datatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratodadoscertame_modalidadetitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratodadoscertame_numerotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratodadoscertame_sitetitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratodadoscertame_uasgtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratodadoscertame_datahomologacaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratodadoscertame_dataadjudicacaotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String A77Contrato_Numero ;
      private String edtContrato_Numero_Internalname ;
      private String edtContratoDadosCertame_Data_Internalname ;
      private String edtContratoDadosCertame_Modalidade_Internalname ;
      private String edtContratoDadosCertame_Numero_Internalname ;
      private String edtContratoDadosCertame_Site_Internalname ;
      private String edtContratoDadosCertame_Uasg_Internalname ;
      private String edtContratoDadosCertame_DataHomologacao_Internalname ;
      private String edtContratoDadosCertame_DataAdjudicacao_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV112WWContratoDadosCertameDS_21_Tfcontrato_numero ;
      private String AV113WWContratoDadosCertameDS_22_Tfcontrato_numero_sel ;
      private String AV112WWContratoDadosCertameDS_21_Tfcontrato_numero ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavContratodadoscertame_data1_Internalname ;
      private String edtavContratodadoscertame_data_to1_Internalname ;
      private String edtavContratodadoscertame_modalidade1_Internalname ;
      private String edtavContratodadoscertame_numero1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavContratodadoscertame_data2_Internalname ;
      private String edtavContratodadoscertame_data_to2_Internalname ;
      private String edtavContratodadoscertame_modalidade2_Internalname ;
      private String edtavContratodadoscertame_numero2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavContratodadoscertame_data3_Internalname ;
      private String edtavContratodadoscertame_data_to3_Internalname ;
      private String edtavContratodadoscertame_modalidade3_Internalname ;
      private String edtavContratodadoscertame_numero3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_contrato_numero_Internalname ;
      private String Ddo_contratodadoscertame_data_Internalname ;
      private String Ddo_contratodadoscertame_modalidade_Internalname ;
      private String Ddo_contratodadoscertame_numero_Internalname ;
      private String Ddo_contratodadoscertame_site_Internalname ;
      private String Ddo_contratodadoscertame_uasg_Internalname ;
      private String Ddo_contratodadoscertame_datahomologacao_Internalname ;
      private String Ddo_contratodadoscertame_dataadjudicacao_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtContrato_Numero_Title ;
      private String edtContratoDadosCertame_Data_Title ;
      private String edtContratoDadosCertame_Modalidade_Title ;
      private String edtContratoDadosCertame_Numero_Title ;
      private String edtContratoDadosCertame_Site_Title ;
      private String edtContratoDadosCertame_Uasg_Title ;
      private String edtContratoDadosCertame_DataHomologacao_Title ;
      private String edtContratoDadosCertame_DataAdjudicacao_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtContratoDadosCertame_Modalidade_Link ;
      private String edtContratoDadosCertame_Site_Linktarget ;
      private String edtContratoDadosCertame_Site_Link ;
      private String tblTablemergeddynamicfilterscontratodadoscertame_data1_Internalname ;
      private String tblTablemergeddynamicfilterscontratodadoscertame_data2_Internalname ;
      private String tblTablemergeddynamicfilterscontratodadoscertame_data3_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblContratodadoscertametitle_Internalname ;
      private String lblContratodadoscertametitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavContratodadoscertame_modalidade3_Jsonclick ;
      private String edtavContratodadoscertame_numero3_Jsonclick ;
      private String edtavContratodadoscertame_data3_Jsonclick ;
      private String lblDynamicfilterscontratodadoscertame_data_rangemiddletext3_Internalname ;
      private String lblDynamicfilterscontratodadoscertame_data_rangemiddletext3_Jsonclick ;
      private String edtavContratodadoscertame_data_to3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavContratodadoscertame_modalidade2_Jsonclick ;
      private String edtavContratodadoscertame_numero2_Jsonclick ;
      private String edtavContratodadoscertame_data2_Jsonclick ;
      private String lblDynamicfilterscontratodadoscertame_data_rangemiddletext2_Internalname ;
      private String lblDynamicfilterscontratodadoscertame_data_rangemiddletext2_Jsonclick ;
      private String edtavContratodadoscertame_data_to2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavContratodadoscertame_modalidade1_Jsonclick ;
      private String edtavContratodadoscertame_numero1_Jsonclick ;
      private String edtavContratodadoscertame_data1_Jsonclick ;
      private String lblDynamicfilterscontratodadoscertame_data_rangemiddletext1_Internalname ;
      private String lblDynamicfilterscontratodadoscertame_data_rangemiddletext1_Jsonclick ;
      private String edtavContratodadoscertame_data_to1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_115_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContrato_Numero_Jsonclick ;
      private String edtContratoDadosCertame_Data_Jsonclick ;
      private String edtContratoDadosCertame_Modalidade_Jsonclick ;
      private String edtContratoDadosCertame_Numero_Jsonclick ;
      private String edtContratoDadosCertame_Site_Jsonclick ;
      private String edtContratoDadosCertame_Uasg_Jsonclick ;
      private String edtContratoDadosCertame_DataHomologacao_Jsonclick ;
      private String edtContratoDadosCertame_DataAdjudicacao_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV37ContratoDadosCertame_Data1 ;
      private DateTime AV38ContratoDadosCertame_Data_To1 ;
      private DateTime AV40ContratoDadosCertame_Data2 ;
      private DateTime AV41ContratoDadosCertame_Data_To2 ;
      private DateTime AV43ContratoDadosCertame_Data3 ;
      private DateTime AV44ContratoDadosCertame_Data_To3 ;
      private DateTime AV53TFContratoDadosCertame_Data ;
      private DateTime AV54TFContratoDadosCertame_Data_To ;
      private DateTime AV75TFContratoDadosCertame_DataHomologacao ;
      private DateTime AV76TFContratoDadosCertame_DataHomologacao_To ;
      private DateTime AV81TFContratoDadosCertame_DataAdjudicacao ;
      private DateTime AV82TFContratoDadosCertame_DataAdjudicacao_To ;
      private DateTime AV55DDO_ContratoDadosCertame_DataAuxDate ;
      private DateTime AV56DDO_ContratoDadosCertame_DataAuxDateTo ;
      private DateTime AV77DDO_ContratoDadosCertame_DataHomologacaoAuxDate ;
      private DateTime AV78DDO_ContratoDadosCertame_DataHomologacaoAuxDateTo ;
      private DateTime AV83DDO_ContratoDadosCertame_DataAdjudicacaoAuxDate ;
      private DateTime AV84DDO_ContratoDadosCertame_DataAdjudicacaoAuxDateTo ;
      private DateTime A311ContratoDadosCertame_Data ;
      private DateTime A312ContratoDadosCertame_DataHomologacao ;
      private DateTime A313ContratoDadosCertame_DataAdjudicacao ;
      private DateTime AV94WWContratoDadosCertameDS_3_Contratodadoscertame_data1 ;
      private DateTime AV95WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1 ;
      private DateTime AV101WWContratoDadosCertameDS_10_Contratodadoscertame_data2 ;
      private DateTime AV102WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2 ;
      private DateTime AV108WWContratoDadosCertameDS_17_Contratodadoscertame_data3 ;
      private DateTime AV109WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3 ;
      private DateTime AV114WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data ;
      private DateTime AV115WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to ;
      private DateTime AV124WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao ;
      private DateTime AV125WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to ;
      private DateTime AV126WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao ;
      private DateTime AV127WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool n309ContratoDadosCertame_Site ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contrato_numero_Includesortasc ;
      private bool Ddo_contrato_numero_Includesortdsc ;
      private bool Ddo_contrato_numero_Includefilter ;
      private bool Ddo_contrato_numero_Filterisrange ;
      private bool Ddo_contrato_numero_Includedatalist ;
      private bool Ddo_contratodadoscertame_data_Includesortasc ;
      private bool Ddo_contratodadoscertame_data_Includesortdsc ;
      private bool Ddo_contratodadoscertame_data_Includefilter ;
      private bool Ddo_contratodadoscertame_data_Filterisrange ;
      private bool Ddo_contratodadoscertame_data_Includedatalist ;
      private bool Ddo_contratodadoscertame_modalidade_Includesortasc ;
      private bool Ddo_contratodadoscertame_modalidade_Includesortdsc ;
      private bool Ddo_contratodadoscertame_modalidade_Includefilter ;
      private bool Ddo_contratodadoscertame_modalidade_Filterisrange ;
      private bool Ddo_contratodadoscertame_modalidade_Includedatalist ;
      private bool Ddo_contratodadoscertame_numero_Includesortasc ;
      private bool Ddo_contratodadoscertame_numero_Includesortdsc ;
      private bool Ddo_contratodadoscertame_numero_Includefilter ;
      private bool Ddo_contratodadoscertame_numero_Filterisrange ;
      private bool Ddo_contratodadoscertame_numero_Includedatalist ;
      private bool Ddo_contratodadoscertame_site_Includesortasc ;
      private bool Ddo_contratodadoscertame_site_Includesortdsc ;
      private bool Ddo_contratodadoscertame_site_Includefilter ;
      private bool Ddo_contratodadoscertame_site_Filterisrange ;
      private bool Ddo_contratodadoscertame_site_Includedatalist ;
      private bool Ddo_contratodadoscertame_uasg_Includesortasc ;
      private bool Ddo_contratodadoscertame_uasg_Includesortdsc ;
      private bool Ddo_contratodadoscertame_uasg_Includefilter ;
      private bool Ddo_contratodadoscertame_uasg_Filterisrange ;
      private bool Ddo_contratodadoscertame_uasg_Includedatalist ;
      private bool Ddo_contratodadoscertame_datahomologacao_Includesortasc ;
      private bool Ddo_contratodadoscertame_datahomologacao_Includesortdsc ;
      private bool Ddo_contratodadoscertame_datahomologacao_Includefilter ;
      private bool Ddo_contratodadoscertame_datahomologacao_Filterisrange ;
      private bool Ddo_contratodadoscertame_datahomologacao_Includedatalist ;
      private bool Ddo_contratodadoscertame_dataadjudicacao_Includesortasc ;
      private bool Ddo_contratodadoscertame_dataadjudicacao_Includesortdsc ;
      private bool Ddo_contratodadoscertame_dataadjudicacao_Includefilter ;
      private bool Ddo_contratodadoscertame_dataadjudicacao_Filterisrange ;
      private bool Ddo_contratodadoscertame_dataadjudicacao_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n308ContratoDadosCertame_Numero ;
      private bool n310ContratoDadosCertame_Uasg ;
      private bool n312ContratoDadosCertame_DataHomologacao ;
      private bool n313ContratoDadosCertame_DataAdjudicacao ;
      private bool AV98WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 ;
      private bool AV105WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Update_IsBlob ;
      private bool AV29Delete_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV17ContratoDadosCertame_Modalidade1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV21ContratoDadosCertame_Modalidade2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV25ContratoDadosCertame_Modalidade3 ;
      private String AV59TFContratoDadosCertame_Modalidade ;
      private String AV60TFContratoDadosCertame_Modalidade_Sel ;
      private String AV67TFContratoDadosCertame_Site ;
      private String AV68TFContratoDadosCertame_Site_Sel ;
      private String AV51ddo_Contrato_NumeroTitleControlIdToReplace ;
      private String AV57ddo_ContratoDadosCertame_DataTitleControlIdToReplace ;
      private String AV61ddo_ContratoDadosCertame_ModalidadeTitleControlIdToReplace ;
      private String AV65ddo_ContratoDadosCertame_NumeroTitleControlIdToReplace ;
      private String AV69ddo_ContratoDadosCertame_SiteTitleControlIdToReplace ;
      private String AV73ddo_ContratoDadosCertame_UasgTitleControlIdToReplace ;
      private String AV79ddo_ContratoDadosCertame_DataHomologacaoTitleControlIdToReplace ;
      private String AV85ddo_ContratoDadosCertame_DataAdjudicacaoTitleControlIdToReplace ;
      private String A309ContratoDadosCertame_Site ;
      private String AV128Update_GXI ;
      private String AV129Delete_GXI ;
      private String A307ContratoDadosCertame_Modalidade ;
      private String lV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1 ;
      private String lV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2 ;
      private String lV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3 ;
      private String lV116WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade ;
      private String lV120WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site ;
      private String AV92WWContratoDadosCertameDS_1_Dynamicfiltersselector1 ;
      private String AV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1 ;
      private String AV99WWContratoDadosCertameDS_8_Dynamicfiltersselector2 ;
      private String AV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2 ;
      private String AV106WWContratoDadosCertameDS_15_Dynamicfiltersselector3 ;
      private String AV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3 ;
      private String AV117WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel ;
      private String AV116WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade ;
      private String AV121WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel ;
      private String AV120WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site ;
      private String AV28Update ;
      private String AV29Delete ;
      private IGxSession AV30Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H007G2_A74Contrato_Codigo ;
      private int[] H007G2_A314ContratoDadosCertame_Codigo ;
      private DateTime[] H007G2_A313ContratoDadosCertame_DataAdjudicacao ;
      private bool[] H007G2_n313ContratoDadosCertame_DataAdjudicacao ;
      private DateTime[] H007G2_A312ContratoDadosCertame_DataHomologacao ;
      private bool[] H007G2_n312ContratoDadosCertame_DataHomologacao ;
      private short[] H007G2_A310ContratoDadosCertame_Uasg ;
      private bool[] H007G2_n310ContratoDadosCertame_Uasg ;
      private String[] H007G2_A309ContratoDadosCertame_Site ;
      private bool[] H007G2_n309ContratoDadosCertame_Site ;
      private long[] H007G2_A308ContratoDadosCertame_Numero ;
      private bool[] H007G2_n308ContratoDadosCertame_Numero ;
      private String[] H007G2_A307ContratoDadosCertame_Modalidade ;
      private DateTime[] H007G2_A311ContratoDadosCertame_Data ;
      private String[] H007G2_A77Contrato_Numero ;
      private long[] H007G3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV48Contrato_NumeroTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV52ContratoDadosCertame_DataTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV58ContratoDadosCertame_ModalidadeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV62ContratoDadosCertame_NumeroTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV66ContratoDadosCertame_SiteTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV70ContratoDadosCertame_UasgTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV74ContratoDadosCertame_DataHomologacaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV80ContratoDadosCertame_DataAdjudicacaoTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV86DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwcontratodadoscertame__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H007G2( IGxContext context ,
                                             String AV92WWContratoDadosCertameDS_1_Dynamicfiltersselector1 ,
                                             DateTime AV94WWContratoDadosCertameDS_3_Contratodadoscertame_data1 ,
                                             DateTime AV95WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1 ,
                                             short AV93WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 ,
                                             String AV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1 ,
                                             long AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1 ,
                                             bool AV98WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 ,
                                             String AV99WWContratoDadosCertameDS_8_Dynamicfiltersselector2 ,
                                             DateTime AV101WWContratoDadosCertameDS_10_Contratodadoscertame_data2 ,
                                             DateTime AV102WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2 ,
                                             short AV100WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 ,
                                             String AV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2 ,
                                             long AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2 ,
                                             bool AV105WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 ,
                                             String AV106WWContratoDadosCertameDS_15_Dynamicfiltersselector3 ,
                                             DateTime AV108WWContratoDadosCertameDS_17_Contratodadoscertame_data3 ,
                                             DateTime AV109WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3 ,
                                             short AV107WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 ,
                                             String AV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3 ,
                                             long AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3 ,
                                             String AV113WWContratoDadosCertameDS_22_Tfcontrato_numero_sel ,
                                             String AV112WWContratoDadosCertameDS_21_Tfcontrato_numero ,
                                             DateTime AV114WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data ,
                                             DateTime AV115WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to ,
                                             String AV117WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel ,
                                             String AV116WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade ,
                                             long AV118WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero ,
                                             long AV119WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to ,
                                             String AV121WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel ,
                                             String AV120WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site ,
                                             short AV122WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg ,
                                             short AV123WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to ,
                                             DateTime AV124WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao ,
                                             DateTime AV125WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to ,
                                             DateTime AV126WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao ,
                                             DateTime AV127WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to ,
                                             DateTime A311ContratoDadosCertame_Data ,
                                             String A307ContratoDadosCertame_Modalidade ,
                                             long A308ContratoDadosCertame_Numero ,
                                             String A77Contrato_Numero ,
                                             String A309ContratoDadosCertame_Site ,
                                             short A310ContratoDadosCertame_Uasg ,
                                             DateTime A312ContratoDadosCertame_DataHomologacao ,
                                             DateTime A313ContratoDadosCertame_DataAdjudicacao ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [42] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[Contrato_Codigo], T1.[ContratoDadosCertame_Codigo], T1.[ContratoDadosCertame_DataAdjudicacao], T1.[ContratoDadosCertame_DataHomologacao], T1.[ContratoDadosCertame_Uasg], T1.[ContratoDadosCertame_Site], T1.[ContratoDadosCertame_Numero], T1.[ContratoDadosCertame_Modalidade], T1.[ContratoDadosCertame_Data], T2.[Contrato_Numero]";
         sFromString = " FROM ([ContratoDadosCertame] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV92WWContratoDadosCertameDS_1_Dynamicfiltersselector1, "CONTRATODADOSCERTAME_DATA") == 0 ) && ( ! (DateTime.MinValue==AV94WWContratoDadosCertameDS_3_Contratodadoscertame_data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] >= @AV94WWContratoDadosCertameDS_3_Contratodadoscertame_data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] >= @AV94WWContratoDadosCertameDS_3_Contratodadoscertame_data1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV92WWContratoDadosCertameDS_1_Dynamicfiltersselector1, "CONTRATODADOSCERTAME_DATA") == 0 ) && ( ! (DateTime.MinValue==AV95WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] <= @AV95WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] <= @AV95WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV92WWContratoDadosCertameDS_1_Dynamicfiltersselector1, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV93WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like @lV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like @lV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV92WWContratoDadosCertameDS_1_Dynamicfiltersselector1, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV93WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV92WWContratoDadosCertameDS_1_Dynamicfiltersselector1, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV93WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! (0==AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] < @AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] < @AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV92WWContratoDadosCertameDS_1_Dynamicfiltersselector1, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV93WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! (0==AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] = @AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] = @AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV92WWContratoDadosCertameDS_1_Dynamicfiltersselector1, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV93WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 == 2 ) && ( ! (0==AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] > @AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] > @AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV98WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV99WWContratoDadosCertameDS_8_Dynamicfiltersselector2, "CONTRATODADOSCERTAME_DATA") == 0 ) && ( ! (DateTime.MinValue==AV101WWContratoDadosCertameDS_10_Contratodadoscertame_data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] >= @AV101WWContratoDadosCertameDS_10_Contratodadoscertame_data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] >= @AV101WWContratoDadosCertameDS_10_Contratodadoscertame_data2)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( AV98WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV99WWContratoDadosCertameDS_8_Dynamicfiltersselector2, "CONTRATODADOSCERTAME_DATA") == 0 ) && ( ! (DateTime.MinValue==AV102WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] <= @AV102WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] <= @AV102WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( AV98WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV99WWContratoDadosCertameDS_8_Dynamicfiltersselector2, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV100WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like @lV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like @lV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( AV98WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV99WWContratoDadosCertameDS_8_Dynamicfiltersselector2, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV100WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( AV98WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV99WWContratoDadosCertameDS_8_Dynamicfiltersselector2, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV100WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 == 0 ) && ( ! (0==AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] < @AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] < @AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( AV98WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV99WWContratoDadosCertameDS_8_Dynamicfiltersselector2, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV100WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 == 1 ) && ( ! (0==AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] = @AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] = @AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( AV98WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV99WWContratoDadosCertameDS_8_Dynamicfiltersselector2, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV100WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 == 2 ) && ( ! (0==AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] > @AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] > @AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( AV105WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV106WWContratoDadosCertameDS_15_Dynamicfiltersselector3, "CONTRATODADOSCERTAME_DATA") == 0 ) && ( ! (DateTime.MinValue==AV108WWContratoDadosCertameDS_17_Contratodadoscertame_data3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] >= @AV108WWContratoDadosCertameDS_17_Contratodadoscertame_data3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] >= @AV108WWContratoDadosCertameDS_17_Contratodadoscertame_data3)";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( AV105WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV106WWContratoDadosCertameDS_15_Dynamicfiltersselector3, "CONTRATODADOSCERTAME_DATA") == 0 ) && ( ! (DateTime.MinValue==AV109WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] <= @AV109WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] <= @AV109WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3)";
            }
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( AV105WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV106WWContratoDadosCertameDS_15_Dynamicfiltersselector3, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV107WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like @lV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like @lV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3)";
            }
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( AV105WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV106WWContratoDadosCertameDS_15_Dynamicfiltersselector3, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV107WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3)";
            }
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( AV105WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV106WWContratoDadosCertameDS_15_Dynamicfiltersselector3, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV107WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 == 0 ) && ( ! (0==AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] < @AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] < @AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3)";
            }
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( AV105WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV106WWContratoDadosCertameDS_15_Dynamicfiltersselector3, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV107WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 == 1 ) && ( ! (0==AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] = @AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] = @AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3)";
            }
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( AV105WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV106WWContratoDadosCertameDS_15_Dynamicfiltersselector3, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV107WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 == 2 ) && ( ! (0==AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] > @AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] > @AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3)";
            }
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV113WWContratoDadosCertameDS_22_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV112WWContratoDadosCertameDS_21_Tfcontrato_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV112WWContratoDadosCertameDS_21_Tfcontrato_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV112WWContratoDadosCertameDS_21_Tfcontrato_numero)";
            }
         }
         else
         {
            GXv_int2[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113WWContratoDadosCertameDS_22_Tfcontrato_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV113WWContratoDadosCertameDS_22_Tfcontrato_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV113WWContratoDadosCertameDS_22_Tfcontrato_numero_sel)";
            }
         }
         else
         {
            GXv_int2[22] = 1;
         }
         if ( ! (DateTime.MinValue==AV114WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] >= @AV114WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] >= @AV114WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data)";
            }
         }
         else
         {
            GXv_int2[23] = 1;
         }
         if ( ! (DateTime.MinValue==AV115WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] <= @AV115WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] <= @AV115WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to)";
            }
         }
         else
         {
            GXv_int2[24] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV117WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV116WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like @lV116WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like @lV116WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade)";
            }
         }
         else
         {
            GXv_int2[25] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] = @AV117WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] = @AV117WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel)";
            }
         }
         else
         {
            GXv_int2[26] = 1;
         }
         if ( ! (0==AV118WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] >= @AV118WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] >= @AV118WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero)";
            }
         }
         else
         {
            GXv_int2[27] = 1;
         }
         if ( ! (0==AV119WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] <= @AV119WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] <= @AV119WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to)";
            }
         }
         else
         {
            GXv_int2[28] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV121WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV120WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Site] like @lV120WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Site] like @lV120WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site)";
            }
         }
         else
         {
            GXv_int2[29] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV121WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Site] = @AV121WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Site] = @AV121WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel)";
            }
         }
         else
         {
            GXv_int2[30] = 1;
         }
         if ( ! (0==AV122WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Uasg] >= @AV122WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Uasg] >= @AV122WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg)";
            }
         }
         else
         {
            GXv_int2[31] = 1;
         }
         if ( ! (0==AV123WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Uasg] <= @AV123WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Uasg] <= @AV123WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to)";
            }
         }
         else
         {
            GXv_int2[32] = 1;
         }
         if ( ! (DateTime.MinValue==AV124WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_DataHomologacao] >= @AV124WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_DataHomologacao] >= @AV124WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao)";
            }
         }
         else
         {
            GXv_int2[33] = 1;
         }
         if ( ! (DateTime.MinValue==AV125WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_DataHomologacao] <= @AV125WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_DataHomologacao] <= @AV125WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to)";
            }
         }
         else
         {
            GXv_int2[34] = 1;
         }
         if ( ! (DateTime.MinValue==AV126WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_DataAdjudicacao] >= @AV126WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_DataAdjudicacao] >= @AV126WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao)";
            }
         }
         else
         {
            GXv_int2[35] = 1;
         }
         if ( ! (DateTime.MinValue==AV127WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_DataAdjudicacao] <= @AV127WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_DataAdjudicacao] <= @AV127WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to)";
            }
         }
         else
         {
            GXv_int2[36] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoDadosCertame_Data]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoDadosCertame_Data] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoDadosCertame_Modalidade]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoDadosCertame_Modalidade] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoDadosCertame_Numero]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoDadosCertame_Numero] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Numero]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Numero] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoDadosCertame_Site]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoDadosCertame_Site] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoDadosCertame_Uasg]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoDadosCertame_Uasg] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoDadosCertame_DataHomologacao]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoDadosCertame_DataHomologacao] DESC";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoDadosCertame_DataAdjudicacao]";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoDadosCertame_DataAdjudicacao] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoDadosCertame_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H007G3( IGxContext context ,
                                             String AV92WWContratoDadosCertameDS_1_Dynamicfiltersselector1 ,
                                             DateTime AV94WWContratoDadosCertameDS_3_Contratodadoscertame_data1 ,
                                             DateTime AV95WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1 ,
                                             short AV93WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 ,
                                             String AV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1 ,
                                             long AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1 ,
                                             bool AV98WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 ,
                                             String AV99WWContratoDadosCertameDS_8_Dynamicfiltersselector2 ,
                                             DateTime AV101WWContratoDadosCertameDS_10_Contratodadoscertame_data2 ,
                                             DateTime AV102WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2 ,
                                             short AV100WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 ,
                                             String AV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2 ,
                                             long AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2 ,
                                             bool AV105WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 ,
                                             String AV106WWContratoDadosCertameDS_15_Dynamicfiltersselector3 ,
                                             DateTime AV108WWContratoDadosCertameDS_17_Contratodadoscertame_data3 ,
                                             DateTime AV109WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3 ,
                                             short AV107WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 ,
                                             String AV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3 ,
                                             long AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3 ,
                                             String AV113WWContratoDadosCertameDS_22_Tfcontrato_numero_sel ,
                                             String AV112WWContratoDadosCertameDS_21_Tfcontrato_numero ,
                                             DateTime AV114WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data ,
                                             DateTime AV115WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to ,
                                             String AV117WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel ,
                                             String AV116WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade ,
                                             long AV118WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero ,
                                             long AV119WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to ,
                                             String AV121WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel ,
                                             String AV120WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site ,
                                             short AV122WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg ,
                                             short AV123WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to ,
                                             DateTime AV124WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao ,
                                             DateTime AV125WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to ,
                                             DateTime AV126WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao ,
                                             DateTime AV127WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to ,
                                             DateTime A311ContratoDadosCertame_Data ,
                                             String A307ContratoDadosCertame_Modalidade ,
                                             long A308ContratoDadosCertame_Numero ,
                                             String A77Contrato_Numero ,
                                             String A309ContratoDadosCertame_Site ,
                                             short A310ContratoDadosCertame_Uasg ,
                                             DateTime A312ContratoDadosCertame_DataHomologacao ,
                                             DateTime A313ContratoDadosCertame_DataAdjudicacao ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [37] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([ContratoDadosCertame] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         if ( ( StringUtil.StrCmp(AV92WWContratoDadosCertameDS_1_Dynamicfiltersselector1, "CONTRATODADOSCERTAME_DATA") == 0 ) && ( ! (DateTime.MinValue==AV94WWContratoDadosCertameDS_3_Contratodadoscertame_data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] >= @AV94WWContratoDadosCertameDS_3_Contratodadoscertame_data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] >= @AV94WWContratoDadosCertameDS_3_Contratodadoscertame_data1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV92WWContratoDadosCertameDS_1_Dynamicfiltersselector1, "CONTRATODADOSCERTAME_DATA") == 0 ) && ( ! (DateTime.MinValue==AV95WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] <= @AV95WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] <= @AV95WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV92WWContratoDadosCertameDS_1_Dynamicfiltersselector1, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV93WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like @lV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like @lV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV92WWContratoDadosCertameDS_1_Dynamicfiltersselector1, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV93WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV92WWContratoDadosCertameDS_1_Dynamicfiltersselector1, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV93WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! (0==AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] < @AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] < @AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV92WWContratoDadosCertameDS_1_Dynamicfiltersselector1, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV93WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! (0==AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] = @AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] = @AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV92WWContratoDadosCertameDS_1_Dynamicfiltersselector1, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV93WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 == 2 ) && ( ! (0==AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] > @AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] > @AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV98WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV99WWContratoDadosCertameDS_8_Dynamicfiltersselector2, "CONTRATODADOSCERTAME_DATA") == 0 ) && ( ! (DateTime.MinValue==AV101WWContratoDadosCertameDS_10_Contratodadoscertame_data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] >= @AV101WWContratoDadosCertameDS_10_Contratodadoscertame_data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] >= @AV101WWContratoDadosCertameDS_10_Contratodadoscertame_data2)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV98WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV99WWContratoDadosCertameDS_8_Dynamicfiltersselector2, "CONTRATODADOSCERTAME_DATA") == 0 ) && ( ! (DateTime.MinValue==AV102WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] <= @AV102WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] <= @AV102WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( AV98WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV99WWContratoDadosCertameDS_8_Dynamicfiltersselector2, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV100WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like @lV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like @lV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( AV98WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV99WWContratoDadosCertameDS_8_Dynamicfiltersselector2, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV100WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( AV98WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV99WWContratoDadosCertameDS_8_Dynamicfiltersselector2, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV100WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 == 0 ) && ( ! (0==AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] < @AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] < @AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( AV98WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV99WWContratoDadosCertameDS_8_Dynamicfiltersselector2, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV100WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 == 1 ) && ( ! (0==AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] = @AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] = @AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( AV98WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV99WWContratoDadosCertameDS_8_Dynamicfiltersselector2, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV100WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 == 2 ) && ( ! (0==AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] > @AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] > @AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( AV105WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV106WWContratoDadosCertameDS_15_Dynamicfiltersselector3, "CONTRATODADOSCERTAME_DATA") == 0 ) && ( ! (DateTime.MinValue==AV108WWContratoDadosCertameDS_17_Contratodadoscertame_data3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] >= @AV108WWContratoDadosCertameDS_17_Contratodadoscertame_data3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] >= @AV108WWContratoDadosCertameDS_17_Contratodadoscertame_data3)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( AV105WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV106WWContratoDadosCertameDS_15_Dynamicfiltersselector3, "CONTRATODADOSCERTAME_DATA") == 0 ) && ( ! (DateTime.MinValue==AV109WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] <= @AV109WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] <= @AV109WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( AV105WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV106WWContratoDadosCertameDS_15_Dynamicfiltersselector3, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV107WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like @lV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like @lV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3)";
            }
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( AV105WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV106WWContratoDadosCertameDS_15_Dynamicfiltersselector3, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV107WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3)";
            }
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( AV105WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV106WWContratoDadosCertameDS_15_Dynamicfiltersselector3, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV107WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 == 0 ) && ( ! (0==AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] < @AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] < @AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3)";
            }
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( AV105WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV106WWContratoDadosCertameDS_15_Dynamicfiltersselector3, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV107WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 == 1 ) && ( ! (0==AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] = @AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] = @AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3)";
            }
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( AV105WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV106WWContratoDadosCertameDS_15_Dynamicfiltersselector3, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV107WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 == 2 ) && ( ! (0==AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] > @AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] > @AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3)";
            }
         }
         else
         {
            GXv_int4[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV113WWContratoDadosCertameDS_22_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV112WWContratoDadosCertameDS_21_Tfcontrato_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV112WWContratoDadosCertameDS_21_Tfcontrato_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV112WWContratoDadosCertameDS_21_Tfcontrato_numero)";
            }
         }
         else
         {
            GXv_int4[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113WWContratoDadosCertameDS_22_Tfcontrato_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV113WWContratoDadosCertameDS_22_Tfcontrato_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV113WWContratoDadosCertameDS_22_Tfcontrato_numero_sel)";
            }
         }
         else
         {
            GXv_int4[22] = 1;
         }
         if ( ! (DateTime.MinValue==AV114WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] >= @AV114WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] >= @AV114WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data)";
            }
         }
         else
         {
            GXv_int4[23] = 1;
         }
         if ( ! (DateTime.MinValue==AV115WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] <= @AV115WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] <= @AV115WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to)";
            }
         }
         else
         {
            GXv_int4[24] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV117WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV116WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like @lV116WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like @lV116WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade)";
            }
         }
         else
         {
            GXv_int4[25] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] = @AV117WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] = @AV117WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel)";
            }
         }
         else
         {
            GXv_int4[26] = 1;
         }
         if ( ! (0==AV118WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] >= @AV118WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] >= @AV118WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero)";
            }
         }
         else
         {
            GXv_int4[27] = 1;
         }
         if ( ! (0==AV119WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] <= @AV119WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] <= @AV119WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to)";
            }
         }
         else
         {
            GXv_int4[28] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV121WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV120WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Site] like @lV120WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Site] like @lV120WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site)";
            }
         }
         else
         {
            GXv_int4[29] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV121WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Site] = @AV121WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Site] = @AV121WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel)";
            }
         }
         else
         {
            GXv_int4[30] = 1;
         }
         if ( ! (0==AV122WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Uasg] >= @AV122WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Uasg] >= @AV122WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg)";
            }
         }
         else
         {
            GXv_int4[31] = 1;
         }
         if ( ! (0==AV123WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Uasg] <= @AV123WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Uasg] <= @AV123WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to)";
            }
         }
         else
         {
            GXv_int4[32] = 1;
         }
         if ( ! (DateTime.MinValue==AV124WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_DataHomologacao] >= @AV124WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_DataHomologacao] >= @AV124WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao)";
            }
         }
         else
         {
            GXv_int4[33] = 1;
         }
         if ( ! (DateTime.MinValue==AV125WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_DataHomologacao] <= @AV125WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_DataHomologacao] <= @AV125WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to)";
            }
         }
         else
         {
            GXv_int4[34] = 1;
         }
         if ( ! (DateTime.MinValue==AV126WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_DataAdjudicacao] >= @AV126WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_DataAdjudicacao] >= @AV126WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao)";
            }
         }
         else
         {
            GXv_int4[35] = 1;
         }
         if ( ! (DateTime.MinValue==AV127WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_DataAdjudicacao] <= @AV127WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_DataAdjudicacao] <= @AV127WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to)";
            }
         }
         else
         {
            GXv_int4[36] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H007G2(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (long)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (short)dynConstraints[10] , (String)dynConstraints[11] , (long)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (short)dynConstraints[17] , (String)dynConstraints[18] , (long)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (DateTime)dynConstraints[22] , (DateTime)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (long)dynConstraints[26] , (long)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (short)dynConstraints[30] , (short)dynConstraints[31] , (DateTime)dynConstraints[32] , (DateTime)dynConstraints[33] , (DateTime)dynConstraints[34] , (DateTime)dynConstraints[35] , (DateTime)dynConstraints[36] , (String)dynConstraints[37] , (long)dynConstraints[38] , (String)dynConstraints[39] , (String)dynConstraints[40] , (short)dynConstraints[41] , (DateTime)dynConstraints[42] , (DateTime)dynConstraints[43] , (short)dynConstraints[44] , (bool)dynConstraints[45] );
               case 1 :
                     return conditional_H007G3(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (long)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (short)dynConstraints[10] , (String)dynConstraints[11] , (long)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (short)dynConstraints[17] , (String)dynConstraints[18] , (long)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (DateTime)dynConstraints[22] , (DateTime)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (long)dynConstraints[26] , (long)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (short)dynConstraints[30] , (short)dynConstraints[31] , (DateTime)dynConstraints[32] , (DateTime)dynConstraints[33] , (DateTime)dynConstraints[34] , (DateTime)dynConstraints[35] , (DateTime)dynConstraints[36] , (String)dynConstraints[37] , (long)dynConstraints[38] , (String)dynConstraints[39] , (String)dynConstraints[40] , (short)dynConstraints[41] , (DateTime)dynConstraints[42] , (DateTime)dynConstraints[43] , (short)dynConstraints[44] , (bool)dynConstraints[45] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH007G2 ;
          prmH007G2 = new Object[] {
          new Object[] {"@AV94WWContratoDadosCertameDS_3_Contratodadoscertame_data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV95WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV101WWContratoDadosCertameDS_10_Contratodadoscertame_data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV102WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV108WWContratoDadosCertameDS_17_Contratodadoscertame_data3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV109WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3",SqlDbType.Decimal,10,0} ,
          new Object[] {"@lV112WWContratoDadosCertameDS_21_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV113WWContratoDadosCertameDS_22_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV114WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV115WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV116WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV117WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV118WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV119WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to",SqlDbType.Decimal,10,0} ,
          new Object[] {"@lV120WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@AV121WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@AV122WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV123WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV124WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV125WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV126WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV127WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH007G3 ;
          prmH007G3 = new Object[] {
          new Object[] {"@AV94WWContratoDadosCertameDS_3_Contratodadoscertame_data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV95WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV96WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV97WWContratoDadosCertameDS_6_Contratodadoscertame_numero1",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV101WWContratoDadosCertameDS_10_Contratodadoscertame_data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV102WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV103WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV104WWContratoDadosCertameDS_13_Contratodadoscertame_numero2",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV108WWContratoDadosCertameDS_17_Contratodadoscertame_data3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV109WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV110WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV111WWContratoDadosCertameDS_20_Contratodadoscertame_numero3",SqlDbType.Decimal,10,0} ,
          new Object[] {"@lV112WWContratoDadosCertameDS_21_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV113WWContratoDadosCertameDS_22_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV114WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV115WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV116WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV117WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV118WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV119WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to",SqlDbType.Decimal,10,0} ,
          new Object[] {"@lV120WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@AV121WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@AV122WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV123WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV124WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV125WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV126WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV127WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to",SqlDbType.DateTime,8,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H007G2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH007G2,11,0,true,false )
             ,new CursorDef("H007G3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH007G3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((short[]) buf[6])[0] = rslt.getShort(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((long[]) buf[10])[0] = rslt.getLong(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getVarchar(8) ;
                ((DateTime[]) buf[13])[0] = rslt.getGXDate(9) ;
                ((String[]) buf[14])[0] = rslt.getString(10, 20) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[42]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[43]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[46]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[47]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[48]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[49]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[50]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[53]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[54]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[55]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[56]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[57]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[58]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[60]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[61]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[62]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[65]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[66]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[67]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[68]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[69]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[70]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[71]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[72]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[73]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[74]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[75]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[76]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[77]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[78]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[79]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[80]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[81]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[82]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[83]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[41]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[42]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[43]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[44]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[45]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[48]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[49]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[50]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[51]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[52]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[55]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[56]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[57]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[58]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[60]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[61]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[64]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[65]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[66]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[67]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[68]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[69]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[70]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[71]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[72]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[73]);
                }
                return;
       }
    }

 }

}
